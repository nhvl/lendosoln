using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.PdfGenerator;

namespace LendersOffice.Pdf
{
	public class CItemizationOfAmountFinancePDF : AbstractLegalPDF
	{

        // 5/26/2004 dd - NHC Webservices GetLoanForm expected this value.
        public override string Name 
        {
            get { return "ItemizationOfAmountFinanced"; }
        }
        public override string Description 
        {
            get { return "Itemization Of Amount Financed"; }
        }
        public override string PdfFile 
        {
            get { return "ItemizationOfAmountFinanced.pdf"; }
        } 

        private int m_lineNumber = 0;
        private LosConvert m_convertLos;
        private ArrayList m_otherList;


        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string applicants = dataApp.aBNm;
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                applicants += " & " + dataApp.aCNm;
            }

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            
            AddFormFieldData("Applicants", applicants);

            m_otherList = new ArrayList();
            m_convertLos = dataLoan.m_convertLos;

            

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            AddFormFieldData("GfeTil", Tools.FormatAddress(gfeTil.CompanyName, gfeTil.StreetAddr, gfeTil.City, gfeTil.State, gfeTil.Zip, gfeTil.PhoneOfCompany));
            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);


            WriteLine("ITEMIZATION OF PREPAID FINANCE CHARGE");
            SkipLine();
            string[] data = null;
            string str = null;

            if (dataLoan.sLOrigFPc_rep != "" || dataLoan.sLOrigFMb_rep != "") 
            {
                str = string.Format("{0}% + ${1,20}", dataLoan.sLOrigFPc_rep, dataLoan.sLOrigFMb_rep);
            } 
            else 
            {
                str = "";
            }
            data = new string[4] {
                                     "Loan Origination Fee", 
                                     str, 
                                     dataLoan.sLOrigF_rep, 
                                     ""
                                 };
            Process(dataLoan.sLOrigFProps, data);

            if (dataLoan.sGfeOriginatorCompF_rep != "")
            {
                data = new string[4] {
                    dataLoan.sGfeOriginatorCompFDesc,
                    "",
                    dataLoan.sGfeOriginatorCompF_rep,
                    ""
                };
                Process(dataLoan.sGfeOriginatorCompFProps, data);
            }


            if (dataLoan.sGfeDiscountPointFPc_rep != "") 
            {
                str = string.Format("{0}%", dataLoan.sGfeDiscountPointFPc_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "Loan Discount",
                                     str,
                                     dataLoan.sGfeDiscountPointF_rep,
                                     ""
                                 };
            Process(dataLoan.sGfeDiscountPointFProps, data);

            if (dataLoan.sGfeLenderCreditFPc_rep != "")
            {
                str = string.Format("{0}%", dataLoan.sGfeLenderCreditFPc_rep);
            }
            else
            {
                str = "";
            }
            data = new String[4] {
                                     "Lender Credit",
                                     str,
                                     dataLoan.sGfeLenderCreditF_rep,
                                     ""
                                 };
            Process(dataLoan.sGfeLenderCreditFProps, data);

            data = new String[4] {
                                     "Appraisal Fee",
                                     "",
                                     dataLoan.sApprF_rep + (dataLoan.sApprFPaid ? " (PAID)" : ""),
                                     ""
                                 };
            Process(dataLoan.sApprFProps, data);

            data = new String[4] {
                                     "Credit Report",
                                     "",
                                     dataLoan.sCrF_rep + (dataLoan.sCrFPaid ? " (PAID)" : ""),
                                     ""
                                 };
            Process(dataLoan.sCrFProps, data);

            data = new String[4] {
                                     "Lender's Inspection Fee",
                                     "",
                                     dataLoan.sInspectF_rep,
                                     ""
                                 };
            Process(dataLoan.sInspectFProps, data);

            if (dataLoan.sMBrokFPc_rep != "" || dataLoan.sMBrokFMb_rep != "") 
            {
                str = string.Format("{0}% + ${1,20}", dataLoan.sMBrokFPc_rep, dataLoan.sMBrokFMb_rep);
            } 
            else 
            {
                str = "";
            }

            data = new String[4] {
                                     "Mortgage Broker Fee",
                                     str,
                                     dataLoan.sMBrokF_rep,
                                     ""
                                 };
            Process(dataLoan.sMBrokFProps, data);

            data = new string[4] {
                "Flood Certification",
                "",
                dataLoan.sFloodCertificationF_rep,
                ""
            };
            Process(dataLoan.sFloodCertificationFProps, data);
            data = new String[4] {
                                     "Tax Related Service Fee",
                                     "",
                                     dataLoan.sTxServF_rep,
                                     ""
                                 };
            Process(dataLoan.sTxServFProps, data);

            data = new String[4] {
                                     "Processing Fee",
                                     "",
                                     dataLoan.sProcF_rep + (dataLoan.sProcFPaid ? " (PAID)" : ""),
                                     ""
                                 };
            Process(dataLoan.sProcFProps, data);

            data = new String[4] {
                                     "Underwriting Fee",
                                     "",
                                     dataLoan.sUwF_rep,
                                     ""
                                 };
            Process(dataLoan.sUwFProps, data);
            data = new String[4] {
                                     "Wire Transfer Fee",
                                     "",
                                     dataLoan.sWireF_rep,
                                     ""
                                 };
            Process(dataLoan.sWireFProps, data);

            if (dataLoan.s800U1F_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.s800U1FDesc,
                                         "",
                                         dataLoan.s800U1F_rep,
                                         ""
                                     };
                Process(dataLoan.s800U1FProps, data);
            }
            if (dataLoan.s800U2F_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.s800U2FDesc,
                                         "",
                                         dataLoan.s800U2F_rep,
                                         ""
                                     };
                Process(dataLoan.s800U2FProps, data);
            }
            if (dataLoan.s800U3F_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.s800U3FDesc,
                                         "",
                                         dataLoan.s800U3F_rep,
                                         ""
                                     };
                Process(dataLoan.s800U3FProps, data);
            }
            if (dataLoan.s800U4F_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.s800U4FDesc,
                                         "",
                                         dataLoan.s800U4F_rep,
                                         ""
                                     };
                Process(dataLoan.s800U4FProps, data);
            }
            if (dataLoan.s800U5F_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.s800U5FDesc,
                                         "",
                                         dataLoan.s800U5F_rep,
                                         ""
                                     };
                Process(dataLoan.s800U5FProps, data);
            }

            if (dataLoan.sIPiaDy_rep != "" || dataLoan.sIPerDay_rep != "") 
            {
                str = string.Format("{0} days @ {1,20} per day", dataLoan.sIPiaDy_rep, dataLoan.sIPerDay_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "Interest for",
                                     str,
                                     dataLoan.sIPia_rep,
                                     ""
                                 };
            Process(dataLoan.sIPiaProps, data);

            data = new String[4] {
                                     "Mortgage Insurance Premium",
                                     "",
                                     dataLoan.sMipPia_rep,
                                     ""
                                 };
            Process(dataLoan.sMipPiaProps, data);

            data = new String[4] {
                                     "Hazard Insurance Premium",
                                     "",
                                     dataLoan.sHazInsPia_rep,
                                     ""
                                 };
            Process(dataLoan.sHazInsPiaProps, data);

            data = new String[4] {
                                     dataLoan.s904PiaDesc,
                                     "",
                                     dataLoan.s904Pia_rep,
                                     ""
                                 };
            Process(dataLoan.s904PiaProps, data);

            data = new String[4] {
                                     "VA Funding Fee",
                                     "",
                                     dataLoan.sVaFf_rep,
                                     ""
                                 };
            Process(dataLoan.sVaFfProps, data);

            data = new String[4] {
                                     dataLoan.s900U1PiaDesc,
                                     "",
                                     dataLoan.s900U1Pia_rep,
                                     ""
                                 };
            Process(dataLoan.s900U1PiaProps, data);

            string format = "{0} months @ $ {1,20} per month";

            if (dataLoan.sHazInsRsrvMon_rep != "" || dataLoan.sProHazIns_rep != "") 
            {
                str = string.Format(format, dataLoan.sHazInsRsrvMon_rep, dataLoan.sProHazIns_rep);
            } 
            else 
            {
                str = "";
            }

            data = new String[4] {
                                     "Hazard Insurance Premiums",
                                     str,
                                     dataLoan.sHazInsRsrv_rep,
                                     ""
                                 };
            Process(dataLoan.sHazInsRsrvProps, data);

            if (dataLoan.sMInsRsrvMon_rep != "" || dataLoan.sProMIns_rep != "") 
            {
                str = string.Format(format, dataLoan.sMInsRsrvMon_rep, dataLoan.sProMIns_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "Mortgage Ins. Premium Reserves",
                                     str,
                                     dataLoan.sMInsRsrv_rep,
                                     ""
                                 };
            Process(dataLoan.sMInsRsrvProps, data);

            if (dataLoan.sSchoolTxRsrvMon_rep != "" || dataLoan.sProSchoolTx_rep != "") 
            {
                str = string.Format(format, dataLoan.sSchoolTxRsrvMon_rep, dataLoan.sProSchoolTx_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "School Tax",
                                     str,
                                     dataLoan.sSchoolTxRsrv_rep,
                                     ""
                                 };
            Process(dataLoan.sSchoolTxRsrvProps, data);

            if (dataLoan.sRealETxRsrvMon_rep != "" || dataLoan.sProRealETx_rep != "") 
            {
                str = string.Format(format, dataLoan.sRealETxRsrvMon_rep, dataLoan.sProRealETx_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "Taxes and Assessment Reserves",
                                     str,
                                     dataLoan.sRealETxRsrv_rep,
                                     ""
                                 };
            Process(dataLoan.sRealETxRsrvProps, data);

            if (dataLoan.sFloodInsRsrvMon_rep != "" || dataLoan.sProFloodIns_rep != "") 
            {
                str = string.Format(format, dataLoan.sFloodInsRsrvMon_rep, dataLoan.sProFloodIns_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "Flood Insurance Reserves",
                                     str,
                                     dataLoan.sFloodInsRsrv_rep,
                                     ""
                                 };
            Process(dataLoan.sFloodInsRsrvProps, data);

            if (dataLoan.s1006Rsrv_rep != "") 
            {
                str = string.Format(format, dataLoan.s1006RsrvMon_rep, dataLoan.s1006ProHExp_rep);
                data = new string[4] {
                                         dataLoan.s1006ProHExpDesc,
                                         str,
                                         dataLoan.s1006Rsrv_rep,
                                         ""
                                     };
                Process(dataLoan.s1006RsrvProps, data);
            }
            if (dataLoan.s1007Rsrv_rep != "") 
            {
                str = string.Format(format, dataLoan.s1007RsrvMon_rep, dataLoan.s1007ProHExp_rep);
                data = new string[4] {
                                         dataLoan.s1007ProHExpDesc,
                                         str,
                                         dataLoan.s1007Rsrv_rep,
                                         ""
                                     };
                Process(dataLoan.s1007RsrvProps, data);
            }

            data = new string[4] {
                    "Aggregate Adjustment",
                    "",
                    dataLoan.sAggregateAdjRsrv_rep,
                    ""
            };
            Process(dataLoan.sAggregateAdjRsrvProps, data);

            data = new String[4] {
                                     "Closing or Escrow Fee:",
                                     "",
                                     dataLoan.sEscrowF_rep,
                                     ""
                                 };
            Process(dataLoan.sEscrowFProps, data);

            data = new String[4] {
                                     "Document Preparation Fee",
                                     "",
                                     dataLoan.sDocPrepF_rep,
                                     ""
                                 };
            Process(dataLoan.sDocPrepFProps, data);

            data = new String[4] {
                                     "Notary Fees",
                                     "",
                                     dataLoan.sNotaryF_rep,
                                     ""
                                 };
            Process(dataLoan.sNotaryFProps, data);
            data = new String[4] {
                                     "Attorney Fees",
                                     "",
                                     dataLoan.sAttorneyF_rep,
                                     ""
                                 };
            Process(dataLoan.sAttorneyFProps, data);

            data = new string[4] {
                "Owner's Title Insurance",
                "",
                dataLoan.sOwnerTitleInsF_rep,
                ""
            };
            Process(dataLoan.sOwnerTitleInsProps, data);

            data = new String[4] {
                                     "Lender's Title Insurance",
                                     "",
                                     dataLoan.sTitleInsF_rep,
                                     ""
                                 };
            Process(dataLoan.sTitleInsFProps, data);

            data = new String[4] {
                                     dataLoan.sU1TcDesc,
                                     "",
                                     dataLoan.sU1Tc_rep,
                                     ""
                                 };
            Process(dataLoan.sU1TcProps, data);

            data = new String[4] {
                                     dataLoan.sU2TcDesc,
                                     "",
                                     dataLoan.sU2Tc_rep,
                                     ""
                                 };
            Process(dataLoan.sU2TcProps, data);

            data = new String[4] {
                                     dataLoan.sU3TcDesc,
                                     "",
                                     dataLoan.sU3Tc_rep,
                                     ""
                                 };
            Process(dataLoan.sU3TcProps, data);
            data = new String[4] {
                                     dataLoan.sU4TcDesc,
                                     "",
                                     dataLoan.sU4Tc_rep,
                                     ""
                                 };
            Process(dataLoan.sU4TcProps, data);

            data = new String[4] {
                                     "Recording Fees:",
                                     "",
                                     dataLoan.sRecF_rep,
                                     ""
                                 };
            Process(dataLoan.sRecFProps, data);
            data = new String[4] {
                                     "City/County Tax/Stamps:",
                                     "",
                                     dataLoan.sCountyRtc_rep,
                                     ""
                                 };
            Process(dataLoan.sCountyRtcProps, data);

            data = new String[4] {
                                     "State Tax/Stamps:",
                                     "",
                                     dataLoan.sStateRtc_rep,
                                     ""
                                 };
            Process(dataLoan.sStateRtcProps, data);

            if (dataLoan.sU1GovRtc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU1GovRtcDesc,
                                         "",
                                         dataLoan.sU1GovRtc_rep,
                                         ""
                                     };
                Process(dataLoan.sU1GovRtcProps, data);
            }
            if (dataLoan.sU2GovRtc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU2GovRtcDesc,
                                         "",
                                         dataLoan.sU2GovRtc_rep,
                                         ""
                                     };
                Process(dataLoan.sU2GovRtcProps, data);
            }
            if (dataLoan.sU3GovRtc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU3GovRtcDesc,
                                         "",
                                         dataLoan.sU3GovRtc_rep,
                                         ""
                                     };
                Process(dataLoan.sU3GovRtcProps, data);
            }
            data = new String[4] {
                                     "Pest Inspection",
                                     "",
                                     dataLoan.sPestInspectF_rep,
                                     ""
                                 };
            Process(dataLoan.sPestInspectFProps, data);
            if (dataLoan.sU1Sc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU1ScDesc,
                                         "",
                                         dataLoan.sU1Sc_rep,
                                         ""
                                     };
                Process(dataLoan.sU1ScProps, data);
            }
            if (dataLoan.sU2Sc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU2ScDesc,
                                         "",
                                         dataLoan.sU2Sc_rep,
                                         ""
                                     };
                Process(dataLoan.sU2ScProps, data);
            }
            if (dataLoan.sU3Sc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU3ScDesc,
                                         "",
                                         dataLoan.sU3Sc_rep,
                                         ""
                                     };
                Process(dataLoan.sU3ScProps, data);
            }
            if (dataLoan.sU4Sc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU4ScDesc,
                                         "",
                                         dataLoan.sU4Sc_rep,
                                         ""
                                     };
                Process(dataLoan.sU4ScProps, data);
            }
            if (dataLoan.sU5Sc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU5ScDesc,
                                         "",
                                         dataLoan.sU5Sc_rep,
                                         ""
                                     };
                Process(dataLoan.sU5ScProps, data);
            }
            WriteLine("Total Prepaid Finance Charge", "", "", dataLoan.sAprIncludedCc_rep);
            SkipLine(1);
            WriteLine("AMOUNT PAID ON YOUR ACCOUNT / PAID TO OTHERS ON YOUR BEHALF");
            SkipLine(1);

            foreach (string[] d in m_otherList) 
            {
                WriteLine(d);
            }

            SkipLine(2);
            // 1/7/2010 dd - OPM 44239 - Use Settlement Charge display from the new GFE 2010.
            WriteLine("Total Estimated Settlement Charge", "", "", dataLoan.sGfeTotalEstimateSettlementCharge_rep); // Dominic - related to case 3748
            AddFormFieldData("sAprIncludedCc", dataLoan.sAprIncludedCc_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sFinancedAmt", dataLoan.sFinancedAmt_rep);


        }

        private bool IsPrepaidFinanceCharge(int props) 
        {
            return LosConvert.GfeItemProps_Apr(props);
        }
        private bool IsPOC(int props) 
        {
            return LosConvert.GfeItemProps_Poc(props);
        }
        private void WriteLine(string columnA) 
        {
            WriteLine(columnA, "", "", "");
        }
        private void WriteLine(string columnA, string columnB) 
        {
            WriteLine(columnA, columnB, "", "");
        }
        private void WriteLine(string columnA, string columnB, string columnC) 
        {
            WriteLine(columnA, columnB, columnC, "");
        }

        private void WriteLine(string columnA, string columnB, string columnC, string columnD) 
        {
            AddFormFieldData("Line" + m_lineNumber + "a", columnA);
            AddFormFieldData("Line" + m_lineNumber + "b", columnB);
            AddFormFieldData("Line" + m_lineNumber + "c", columnC);
            AddFormFieldData("Line" + m_lineNumber + "d", columnD);
            m_lineNumber++;
        }
        private void WriteLine(string[] columns) 
        {
            AddFormFieldData("Line" + m_lineNumber + "a", columns[0]);
            AddFormFieldData("Line" + m_lineNumber + "b", columns[1]);
            AddFormFieldData("Line" + m_lineNumber + "c", columns[2]);
            AddFormFieldData("Line" + m_lineNumber + "d", columns[3]);
            m_lineNumber++;

        }

        private void Process(int props, string[] data) 
        {
            // 06/06/05 dd - If GFE item is POC then put in C column, else put in D column.
            if (!IsPOC(props)) 
            {
                data[3] = data[2];
                data[2] = "";
            }

            if (IsPrepaidFinanceCharge(props) )
            {
                WriteLine(data);
            } 
            else 
            {
                m_otherList.Add(data);
            }
        }

        /// <summary>
        /// Skip n lines.
        /// </summary>
        /// <param name="skip"></param>
        private void SkipLine(int skip) 
        {
            m_lineNumber += skip;
        }

        private void SkipLine() 
        {
            SkipLine(1);
        }


	}

    public class CItemizationOfAmountFinance_OldPDF : AbstractLegalPDF
    {

        public override string Description 
        {
            get { return "Itemization Of Amount Financed"; }
        }
        public override string PdfFile 
        {
            get { return "ItemizationOfAmountFinanced_Old.pdf"; }
        } 
        
        private int m_lineNumber = 0;
        private LosConvert m_convertLos;
        private ArrayList m_otherList;


        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string applicants = dataApp.aBNm;
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                applicants += " & " + dataApp.aCNm;
            }

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            
            AddFormFieldData("Applicants", applicants);

            m_otherList = new ArrayList();
            m_convertLos = dataLoan.m_convertLos;

            

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            AddFormFieldData("GfeTil", Tools.FormatAddress(gfeTil.CompanyName, gfeTil.StreetAddr, gfeTil.City, gfeTil.State, gfeTil.Zip, gfeTil.PhoneOfCompany));
            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);


            WriteLine("ITEMIZATION OF PREPAID FINANCE CHARGE");
            SkipLine();
            string[] data = null;
            string str = null;

            if (dataLoan.sLOrigFPc_rep != "" || dataLoan.sLOrigFMb_rep != "") 
            {
                str = string.Format("{0}% + ${1,20}", dataLoan.sLOrigFPc_rep, dataLoan.sLOrigFMb_rep);
            } 
            else 
            {
                str = "";
            }
            data = new string[4] {
                                     "Loan Origination Fee", 
                                     str, 
                                     dataLoan.sLOrigF_rep, 
                                     ""
                                 };
            Process(dataLoan.sLOrigFProps, data);

            if (dataLoan.sGfeDiscountPointFPc_rep != "") 
            {
                str = string.Format("{0}%", dataLoan.sGfeDiscountPointFPc_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "Loan Discount",
                                     str,
                                     dataLoan.sGfeDiscountPointF_rep,
                                     ""
                                 };
            Process(dataLoan.sGfeDiscountPointFProps, data);

            if (dataLoan.sGfeLenderCreditFPc_rep != "")
            {
                str = string.Format("{0}%", dataLoan.sGfeLenderCreditFPc_rep);
            }
            else
            {
                str = "";
            }
            data = new String[4] {
                                     "Lender Credit",
                                     str,
                                     dataLoan.sGfeLenderCreditF_rep,
                                     ""
                                 };
            Process(dataLoan.sGfeLenderCreditFProps, data);

            data = new String[4] {
                                     "Appraisal Fee",
                                     "",
                                     dataLoan.sApprF_rep,
                                     dataLoan.sApprFPaid ? "(PAID)" : ""
                                 };
            Process(dataLoan.sApprFProps, data);

            data = new String[4] {
                                     "Credit Report",
                                     "",
                                     dataLoan.sCrF_rep,
                                     dataLoan.sCrFPaid ? "(PAID)" : ""
                                 };
            Process(dataLoan.sCrFProps, data);

            data = new String[4] {
                                     "Lender's Inspection Fee",
                                     "",
                                     dataLoan.sInspectF_rep,
                                     ""
                                 };
            Process(dataLoan.sInspectFProps, data);

            if (dataLoan.sMBrokFPc_rep != "" || dataLoan.sMBrokFMb_rep != "") 
            {
                str = string.Format("{0}% + ${1,20}", dataLoan.sMBrokFPc_rep, dataLoan.sMBrokFMb_rep);
            } 
            else 
            {
                str = "";
            }

            data = new String[4] {
                                     "Mortgage Broker Fee",
                                     str,
                                     dataLoan.sMBrokF_rep,
                                     ""
                                 };
            Process(dataLoan.sMBrokFProps, data);

            data = new String[4] {
                                     "Tax Related Service Fee",
                                     "",
                                     dataLoan.sTxServF_rep,
                                     ""
                                 };
            Process(dataLoan.sTxServFProps, data);

            data = new String[4] {
                                     "Processing Fee",
                                     "",
                                     dataLoan.sProcF_rep,
                                     dataLoan.sProcFPaid ? "(PAID)" : ""
                                 };
            Process(dataLoan.sProcFProps, data);

            data = new String[4] {
                                     "Underwriting Fee",
                                     "",
                                     dataLoan.sUwF_rep,
                                     ""
                                 };
            Process(dataLoan.sUwFProps, data);
            data = new String[4] {
                                     "Wire Transfer Fee",
                                     "",
                                     dataLoan.sWireF_rep,
                                     ""
                                 };
            Process(dataLoan.sWireFProps, data);

            if (dataLoan.s800U1F_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.s800U1FDesc,
                                         "",
                                         dataLoan.s800U1F_rep,
                                         ""
                                     };
                Process(dataLoan.s800U1FProps, data);
            }
            if (dataLoan.s800U2F_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.s800U2FDesc,
                                         "",
                                         dataLoan.s800U2F_rep,
                                         ""
                                     };
                Process(dataLoan.s800U2FProps, data);
            }
            if (dataLoan.s800U3F_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.s800U3FDesc,
                                         "",
                                         dataLoan.s800U3F_rep,
                                         ""
                                     };
                Process(dataLoan.s800U3FProps, data);
            }
            if (dataLoan.s800U4F_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.s800U4FDesc,
                                         "",
                                         dataLoan.s800U4F_rep,
                                         ""
                                     };
                Process(dataLoan.s800U4FProps, data);
            }
            if (dataLoan.s800U5F_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.s800U5FDesc,
                                         "",
                                         dataLoan.s800U5F_rep,
                                         ""
                                     };
                Process(dataLoan.s800U1FProps, data);
            }

            if (dataLoan.sIPiaDy_rep != "" || dataLoan.sIPerDay_rep != "") 
            {
                str = string.Format("{0} days @ {1,20} per day", dataLoan.sIPiaDy_rep, dataLoan.sIPerDay_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "Interest for",
                                     str,
                                     dataLoan.sIPia_rep,
                                     ""
                                 };
            Process(dataLoan.sIPiaProps, data);

            data = new String[4] {
                                     "Mortgage Insurance Premium",
                                     "",
                                     dataLoan.sMipPia_rep,
                                     ""
                                 };
            Process(dataLoan.sMipPiaProps, data);

            data = new String[4] {
                                     "Hazard Insurance Premium",
                                     "",
                                     dataLoan.sHazInsPia_rep,
                                     ""
                                 };
            Process(dataLoan.sHazInsPiaProps, data);

            data = new String[4] {
                                     dataLoan.s904PiaDesc,
                                     "",
                                     dataLoan.s904Pia_rep,
                                     ""
                                 };
            Process(dataLoan.s904PiaProps, data);

            data = new String[4] {
                                     "VA Funding Fee",
                                     "",
                                     dataLoan.sVaFf_rep,
                                     ""
                                 };
            Process(dataLoan.sVaFfProps, data);

            data = new String[4] {
                                     dataLoan.s900U1PiaDesc,
                                     "",
                                     dataLoan.s900U1Pia_rep,
                                     ""
                                 };
            Process(dataLoan.s900U1PiaProps, data);

            string format = "{0} months @ $ {1,20} per month";

            if (dataLoan.sHazInsRsrvMon_rep != "" || dataLoan.sProHazIns_rep != "") 
            {
                str = string.Format(format, dataLoan.sHazInsRsrvMon_rep, dataLoan.sProHazIns_rep);
            } 
            else 
            {
                str = "";
            }

            data = new String[4] {
                                     "Hazard Insurance Premiums",
                                     str,
                                     dataLoan.sHazInsRsrv_rep,
                                     ""
                                 };
            Process(dataLoan.sHazInsRsrvProps, data);

            if (dataLoan.sMInsRsrvMon_rep != "" || dataLoan.sProMIns_rep != "") 
            {
                str = string.Format(format, dataLoan.sMInsRsrvMon_rep, dataLoan.sProMIns_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "Mortgage Ins. Premium Reserves",
                                     str,
                                     dataLoan.sMInsRsrv_rep,
                                     ""
                                 };
            Process(dataLoan.sMInsRsrvProps, data);

            if (dataLoan.sSchoolTxRsrvMon_rep != "" || dataLoan.sProSchoolTx_rep != "") 
            {
                str = string.Format(format, dataLoan.sSchoolTxRsrvMon_rep, dataLoan.sProSchoolTx_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "School Tax",
                                     str,
                                     dataLoan.sSchoolTxRsrv_rep,
                                     ""
                                 };
            Process(dataLoan.sSchoolTxRsrvProps, data);

            if (dataLoan.sRealETxRsrvMon_rep != "" || dataLoan.sProRealETx_rep != "") 
            {
                str = string.Format(format, dataLoan.sRealETxRsrvMon_rep, dataLoan.sProRealETx_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "Taxes and Assessment Reserves",
                                     str,
                                     dataLoan.sRealETxRsrv_rep,
                                     ""
                                 };
            Process(dataLoan.sRealETxRsrvProps, data);

            if (dataLoan.sFloodInsRsrvMon_rep != "" || dataLoan.sProFloodIns_rep != "") 
            {
                str = string.Format(format, dataLoan.sFloodInsRsrvMon_rep, dataLoan.sProFloodIns_rep);
            } 
            else 
            {
                str = "";
            }
            data = new String[4] {
                                     "Flood Insurance Reserves",
                                     str,
                                     dataLoan.sFloodInsRsrv_rep,
                                     ""
                                 };
            Process(dataLoan.sFloodInsRsrvProps, data);

            if (dataLoan.s1006Rsrv_rep != "") 
            {
                str = string.Format(format, dataLoan.s1006RsrvMon_rep, dataLoan.s1006ProHExp_rep);
                data = new string[4] {
                                         dataLoan.s1006ProHExpDesc,
                                         str,
                                         dataLoan.s1006Rsrv_rep,
                                         ""
                                     };
                Process(dataLoan.s1006RsrvProps, data);
            }
            if (dataLoan.s1007Rsrv_rep != "") 
            {
                str = string.Format(format, dataLoan.s1007RsrvMon_rep, dataLoan.s1007ProHExp_rep);
                data = new string[4] {
                                         dataLoan.s1007ProHExpDesc,
                                         str,
                                         dataLoan.s1007Rsrv_rep,
                                         ""
                                     };
                Process(dataLoan.s1006RsrvProps, data);
            }

            data = new String[4] {
                                     "Closing or Escrow Fee:",
                                     "",
                                     dataLoan.sEscrowF_rep,
                                     ""
                                 };
            Process(dataLoan.sEscrowFProps, data);

            data = new String[4] {
                                     "Document Preparation Fee",
                                     "",
                                     dataLoan.sDocPrepF_rep,
                                     ""
                                 };
            Process(dataLoan.sDocPrepFProps, data);

            data = new String[4] {
                                     "Notary Fees",
                                     "",
                                     dataLoan.sNotaryF_rep,
                                     ""
                                 };
            Process(dataLoan.sNotaryFProps, data);
            data = new String[4] {
                                     "Attorney Fees",
                                     "",
                                     dataLoan.sAttorneyF_rep,
                                     ""
                                 };
            Process(dataLoan.sAttorneyFProps, data);

            data = new String[4] {
                                     "Title Insurance:",
                                     "",
                                     dataLoan.sTitleInsF_rep,
                                     ""
                                 };
            Process(dataLoan.sTitleInsFProps, data);

            data = new String[4] {
                                     dataLoan.sU1TcDesc,
                                     "",
                                     dataLoan.sU1Tc_rep,
                                     ""
                                 };
            Process(dataLoan.sU1TcProps, data);

            data = new String[4] {
                                     dataLoan.sU2TcDesc,
                                     "",
                                     dataLoan.sU2Tc_rep,
                                     ""
                                 };
            Process(dataLoan.sU2TcProps, data);

            data = new String[4] {
                                     dataLoan.sU3TcDesc,
                                     "",
                                     dataLoan.sU3Tc_rep,
                                     ""
                                 };
            Process(dataLoan.sU3TcProps, data);
            data = new String[4] {
                                     dataLoan.sU4TcDesc,
                                     "",
                                     dataLoan.sU4Tc_rep,
                                     ""
                                 };
            Process(dataLoan.sU1TcProps, data);

            data = new String[4] {
                                     "Recording Fees:",
                                     "",
                                     dataLoan.sRecF_rep,
                                     ""
                                 };
            Process(dataLoan.sRecFProps, data);
            data = new String[4] {
                                     "City/County Tax/Stamps:",
                                     "",
                                     dataLoan.sCountyRtc_rep,
                                     ""
                                 };
            Process(dataLoan.sCountyRtcProps, data);

            data = new String[4] {
                                     "State Tax/Stamps:",
                                     "",
                                     dataLoan.sStateRtc_rep,
                                     ""
                                 };
            Process(dataLoan.sStateRtcProps, data);

            if (dataLoan.sU1GovRtc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU1GovRtcDesc,
                                         "",
                                         dataLoan.sU1GovRtc_rep,
                                         ""
                                     };
                Process(dataLoan.sU1GovRtcProps, data);
            }
            if (dataLoan.sU2GovRtc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU2GovRtcDesc,
                                         "",
                                         dataLoan.sU2GovRtc_rep,
                                         ""
                                     };
                Process(dataLoan.sU2GovRtcProps, data);
            }
            if (dataLoan.sU3GovRtc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU3GovRtcDesc,
                                         "",
                                         dataLoan.sU3GovRtc_rep,
                                         ""
                                     };
                Process(dataLoan.sU3GovRtcProps, data);
            }
            data = new String[4] {
                                     "Pest Inspection",
                                     "",
                                     dataLoan.sPestInspectF_rep,
                                     ""
                                 };
            Process(dataLoan.sPestInspectFProps, data);
            if (dataLoan.sU1Sc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU1ScDesc,
                                         "",
                                         dataLoan.sU1Sc_rep,
                                         ""
                                     };
                Process(dataLoan.sU1ScProps, data);
            }
            if (dataLoan.sU2Sc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU2ScDesc,
                                         "",
                                         dataLoan.sU2Sc_rep,
                                         ""
                                     };
                Process(dataLoan.sU2ScProps, data);
            }
            if (dataLoan.sU3Sc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU3ScDesc,
                                         "",
                                         dataLoan.sU3Sc_rep,
                                         ""
                                     };
                Process(dataLoan.sU3ScProps, data);
            }
            if (dataLoan.sU4Sc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU4ScDesc,
                                         "",
                                         dataLoan.sU4Sc_rep,
                                         ""
                                     };
                Process(dataLoan.sU4ScProps, data);
            }
            if (dataLoan.sU5Sc_rep != "") 
            {
                data = new string[4] {
                                         dataLoan.sU5ScDesc,
                                         "",
                                         dataLoan.sU5Sc_rep,
                                         ""
                                     };
                Process(dataLoan.sU5ScProps, data);
            }
            WriteLine("Total Prepaid Finance Charge", "", dataLoan.sAprIncludedCc_rep);
            SkipLine(1);
            WriteLine("AMOUNT PAID ON YOUR ACCOUNT / PAID TO OTHERS ON YOUR BEHALF");
            SkipLine(1);

            foreach (string[] d in m_otherList) 
            {
                WriteLine(d);
            }

            SkipLine(2);
            // 1/7/2010 dd - OPM 44239 - Use Settlement Charge display from the new GFE 2010.
            WriteLine("Total Estimated Settlement Charge", "", dataLoan.sGfeTotalEstimateSettlementCharge_rep); // Dominic - related to case 3748
            AddFormFieldData("sAprIncludedCc", dataLoan.sAprIncludedCc_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sFinancedAmt", dataLoan.sFinancedAmt_rep);


        }

        private bool IsPrepaidFinanceCharge(int props) 
        {
            return LosConvert.GfeItemProps_Apr(props);
        }
        private void WriteLine(string columnA) 
        {
            WriteLine(columnA, "", "", "");
        }
        private void WriteLine(string columnA, string columnB) 
        {
            WriteLine(columnA, columnB, "", "");
        }
        private void WriteLine(string columnA, string columnB, string columnC) 
        {
            WriteLine(columnA, columnB, columnC, "");
        }

        private void WriteLine(string columnA, string columnB, string columnC, string columnD) 
        {
            AddFormFieldData("Line" + m_lineNumber + "a", columnA);
            AddFormFieldData("Line" + m_lineNumber + "b", columnB);
            AddFormFieldData("Line" + m_lineNumber + "c", columnC);
            AddFormFieldData("Line" + m_lineNumber + "d", columnD);
            m_lineNumber++;
        }
        private void WriteLine(string[] columns) 
        {
            AddFormFieldData("Line" + m_lineNumber + "a", columns[0]);
            AddFormFieldData("Line" + m_lineNumber + "b", columns[1]);
            AddFormFieldData("Line" + m_lineNumber + "c", columns[2]);
            AddFormFieldData("Line" + m_lineNumber + "d", columns[3]);
            m_lineNumber++;

        }

        private void Process(int props, string[] data) 
        {
            if (IsPrepaidFinanceCharge(props) )
            {
                WriteLine(data);
            } 
            else 
            {
                m_otherList.Add(data);
            }
        }

        /// <summary>
        /// Skip n lines.
        /// </summary>
        /// <param name="skip"></param>
        private void SkipLine(int skip) 
        {
            m_lineNumber += skip;
        }

        private void SkipLine() 
        {
            SkipLine(1);
        }


    }
}
