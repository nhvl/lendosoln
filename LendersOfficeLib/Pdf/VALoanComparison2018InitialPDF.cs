﻿namespace LendersOffice.Pdf
{
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Data mapping class for the VA Loan Comparison 2018 Initial PDF.
    /// </summary>
    public class VALoanComparison2018InitialPDF : AbstractLetterPDF
    {
        /// <summary>
        /// PDF File Name.
        /// </summary>
        public override string PdfFile
        {
            get { return "VALoanComparison2018Initial.pdf"; }            
        }

        /// <summary>
        /// PDF File Description.
        /// </summary>
        public override string Description
        {
            get { return "VA Loan Comparison (Initial Statement)"; }
        }

        /// <summary>
        /// PDF File Edit Link.
        /// </summary>
        public override string EditLink
        {
            get { return "/newlos/VA/VALoanComparison2018.aspx"; }
        }

        /// <summary>
        /// Applies data to the form.
        /// </summary>
        /// <param name="dataLoan">Data loan object.</param>
        /// <param name="dataApp">Data app object.</param>
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            var initialLEDates = dataLoan.sLoanEstimateDatesInfo.InitialLoanEstimate;

            if (initialLEDates != null)
            {
                dataLoan.ApplyClosingCostArchive(dataLoan.sClosingCostArchive.FirstOrDefault(a => a.Id == initialLEDates.ArchiveId));
            }

            // existing fields from original VA Loan Comparison PDF
            CAgentFields preparer = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            this.AddFormFieldData("CompanyName", preparer.CompanyName);

            this.AddFormFieldData("sSpAddr_Multiline", dataLoan.sSpAddr_MultiLine);
            this.AddFormFieldData("Applicants", dataApp.aBNm_aCNm);
            this.AddFormFieldData("aBNm", dataApp.aBNm);
            this.AddFormFieldData("aCNm", dataApp.aCNm);

            this.AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            this.AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            this.AddFormFieldData("sRateAmortT", dataLoan.sRateAmortT);
            this.AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            this.AddFormFieldData("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            this.AddFormFieldData("aVaTotalClosingCost", dataLoan.aVaTotalClosingCost_rep);
            this.AddFormFieldData("sVaLoanNumCurrentLoan", dataLoan.sVaLoanNumCurrentLoan);
            this.AddFormFieldData("sVaLoanAmtCurrentLoan", dataLoan.sVaLoanAmtCurrentLoan_rep);
            this.AddFormFieldData("sVaNoteIrCurrentLoan", dataLoan.sVaNoteIrCurrentLoan_rep);
            this.AddFormFieldData("sVaTermCurrentLoan", dataLoan.sVaTermCurrentLoan_rep);
            this.AddFormFieldData("sVaMonthlyPmtCurrentLoan", dataLoan.sVaMonthlyPmtCurrentLoan_rep);
            this.AddFormFieldData("sVaNewPmtQualified", dataLoan.sVaNewPmtQualified);
            this.AddFormFieldData("sVaLoanCompMonthlyPmtDecreaseAmt", dataLoan.sVaLoanCompMonthlyPmtDecreaseAmt_rep);
            this.AddFormFieldData("sVaLoanCompRecoupCostsTime", dataLoan.sVaLoanCompRecoupCostsTime_rep);
            this.AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            this.AddFormFieldData("sVaPICurrentLoan", dataLoan.sVaPICurrentLoan_rep);

            // new fields for the VA Loan Comparison 2018 Initial PDF
            DataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
            this.AddFormFieldData("sVaInitialFundingFee", dataLoan.sVaInitialFundingFee_rep);
            this.AddFormFieldData("sVaInitialLoanCompRecoupCostsTime", Tools.IsStringEmptyOrNegativeInt(dataLoan.sVaInitialLoanCompRecoupCostsTime_rep) ? "N/A" : dataLoan.sVaInitialLoanCompRecoupCostsTime_rep);
            this.AddFormFieldData("sVaInitialStatementLoanAmt", dataLoan.sFinalLAmt_rep);
            this.AddFormFieldData("sVaInitialStatementTerm", dataLoan.sTerm_rep);
            this.AddFormFieldData("sVaInitialOtherFee", dataLoan.sVaInitialOtherFee_rep);
            this.AddFormFieldData("sVaInitialServYouCannotShopFor", dataLoan.sVaInitialServYouCannotShopFor_rep);
            this.AddFormFieldData("sVaInitialLoanCompMonthlyPmtDecreaseAmt", dataLoan.sVaInitialLoanCompMonthlyPmtDecreaseAmt_rep);
            this.AddFormFieldData("sVaInitialTotalClosingCost", dataLoan.sVaInitialTotalClosingCost_rep);
            this.AddFormFieldData("sVaInitialStatementNoteIR", dataLoan.sRateAmortT);
            this.AddFormFieldData("sVaInitialOriginationCharge", dataLoan.sVaInitialOriginationCharge_rep);
            this.AddFormFieldData("sVaInitialStatementPI", dataLoan.sProThisMPmt_rep);
            this.AddFormFieldData("sVaInitialTaxesOtherGovFee", dataLoan.sVaInitialTaxesOtherGovFee_rep);
            this.AddFormFieldData("sVaInitialStatementMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            this.AddFormFieldData("sVaInitialServYouCanShopFor", dataLoan.sVaInitialServYouCanShopFor_rep);
            this.AddFormFieldData("sTRIDLoanEstimateLenderCredits", dataLoan.sTRIDLoanEstimateLenderCredits_rep);

            if (initialLEDates != null)
            {
                dataLoan.ApplyClosingCostArchive(null); // undo archive and use live values
            }
        }        
    }
}
