using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{

	public class CCreditScoreInformationDisclosurePDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "CreditScoreInformationDisclosure.pdf"; }
        }

        public override string Description 
        {
            get { return "Credit Score Disclosure"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Disclosure/CreditScoreDisclosure.aspx"; }
        }
        public override E_AppPrintModeT AppPrintModeT
        {
            get
            {
                return E_AppPrintModeT.BorrowerAndSpouseSeparately;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            if (BorrType == "C")
            {
                dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
            }
            
            AddFormFieldData("sExperianScoreFrom", dataLoan.sExperianScoreFrom_rep);
            AddFormFieldData("sExperianScoreTo", dataLoan.sExperianScoreTo_rep);
            AddFormFieldData("sEquifaxScoreFrom", dataLoan.sEquifaxScoreFrom_rep);
            AddFormFieldData("sEquifaxScoreTo", dataLoan.sEquifaxScoreTo_rep);
            AddFormFieldData("sTransUnionScoreFrom", dataLoan.sTransUnionScoreFrom_rep);
            AddFormFieldData("sTransUnionScoreTo", dataLoan.sTransUnionScoreTo_rep);

            AddFormFieldData("sExperianModelName", dataApp.aExperianModelName);
            AddFormFieldData("sEquifaxModelName", dataApp.aEquifaxModelName);
            AddFormFieldData("sTransUnionModelName", dataApp.aTransUnionModelName);

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                AddFormFieldData("BorrowerInfo", Tools.FormatAddress(dataApp.aBNm, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
                AddFormFieldData("aNm", dataApp.aBNm);

                AddFormFieldData("aExperianCreatedD", dataApp.aBExperianCreatedD_rep);
                AddFormFieldData("aEquifaxCreatedD", dataApp.aBEquifaxCreatedD_rep);
                AddFormFieldData("aTransUnionCreatedD", dataApp.aBTransUnionCreatedD_rep);

                AddFormFieldData("aExperianScore", dataApp.aBExperianScore_rep);
                AddFormFieldData("aEquifaxScore", dataApp.aBEquifaxScore_rep);
                AddFormFieldData("aTransUnionScore", dataApp.aBTransUnionScore_rep);

                AddFormFieldData("aExperianFactors", dataApp.aBExperianFactors);
                AddFormFieldData("aEquifaxFactors", dataApp.aBEquifaxFactors);
                AddFormFieldData("aTransUnionFactors", dataApp.aBTransUnionFactors);
            }
            else
            {
                AddFormFieldData("BorrowerInfo", Tools.FormatAddress(dataApp.aCNm, dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip));
                AddFormFieldData("aNm", dataApp.aCNm);

                AddFormFieldData("aExperianCreatedD", dataApp.aCExperianCreatedD_rep);
                AddFormFieldData("aEquifaxCreatedD", dataApp.aCEquifaxCreatedD_rep);
                AddFormFieldData("aTransUnionCreatedD", dataApp.aCTransUnionCreatedD_rep);

                AddFormFieldData("aExperianScore", dataApp.aCExperianScore_rep);
                AddFormFieldData("aEquifaxScore", dataApp.aCEquifaxScore_rep);
                AddFormFieldData("aTransUnionScore", dataApp.aCTransUnionScore_rep);

                AddFormFieldData("aExperianFactors", dataApp.aCExperianFactors);
                AddFormFieldData("aEquifaxFactors", dataApp.aCEquifaxFactors);
                AddFormFieldData("aTransUnionFactors", dataApp.aCTransUnionFactors);
            }
            

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditExperian, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("CreditExperian", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, preparer.PhoneOfCompany));

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditEquifax, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("CreditEquifax", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, preparer.PhoneOfCompany));

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditTransUnion, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("CreditTransUnion", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, preparer.PhoneOfCompany));

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditDisclosureLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("CreditDisclosureLender", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));

        }
	}
}
