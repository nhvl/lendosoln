using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CRe885_1PDF : AbstractMldsPDF 
    {
        public override string PdfFile 
        {
            get { return "RE_885_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDSRE885.aspx?pg=0"; }
        }
    }
    public class CRe885_2PDF : AbstractMldsPDF 
    {
        public override string PdfFile 
        {
            get { return "RE_885_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDSRE885.aspx?pg=1"; }
        }
    }
    public class CRe885_3PDF : AbstractMldsPDF 
    {
        public override string PdfFile 
        {
            get { return "RE_885_3.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 3"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDSRE885.aspx?pg=2"; }
        }
    }
    public class CRe885_4PDF : AbstractMldsPDF 
    {
        public override string PdfFile 
        {
            get { return "RE_885_4.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 4"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDSRE885.aspx?pg=3"; }
        }
    }
    public class CRe885_ContinuationPDF : AbstractMldsPDF
    {
        public override string PdfFile
        {
            get { return "RE_Continuation.pdf"; }
        }
        public override string Description
        {
            get { return "RE 885 Continuation Sheet"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/CAMLDSRE885.aspx?pg=1"; }
        }

        public override bool IsVisible
        {
            get { return DataLoan.sHasReContinuationSheet; }
        }
    }

    public class CRe885PDF : AbstractBatchPDF
    {
        public override string Description 
        {
            get { return "CA Mortgage Loan Disclosure Statement/Good Faith Estimate, Nontraditional Mortgage Product (RE 885)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDSRE885.aspx"; }
        }
 
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CRe885_1PDF(),
                                                 new CRe885_2PDF(),
                                                 new CRe885_3PDF(),
                                                 new CRe885_4PDF(),
                                                 new CRe885_ContinuationPDF()
                                             };
            }
        }
    }
}
