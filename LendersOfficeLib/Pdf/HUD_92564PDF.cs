using System;
using System.Collections;
using System.Collections.Specialized;

using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    public class CHUD_92564_CNPDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "HUD-92564-CN.pdf"; }
        }
        public override string Description 
        {
            get { return "FHA For Your Protection : Get A Home Inspection (HUD-92564-CN)"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string name = dataApp.aBNm;

            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "")
                name += " & " + dataApp.aCNm;

            AddFormFieldData("BorrowerName", name);
            AddFormFieldData("sSpAddr", dataLoan.sSpAddr);
            AddFormFieldData("sSpCityStateZip", Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
        }
    }

    public class CHUD_92564_HSPDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "HUD-92564-HS.pdf"; }
        }
        public override string Description 
        {
            get { return "FHA Homebuyer Summary (HUD-92564-HS)"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {


            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sSpAddr", dataLoan.sSpAddr);
            AddFormFieldData("sSpCityStateZip", Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
        }
    }
}
