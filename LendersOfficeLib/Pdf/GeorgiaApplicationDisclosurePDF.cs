﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CGeorgiaApplicationDisclosurePDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "GeorgiaApplicationDisclosure.pdf"; }
        }
        public override string Description
        {
            get { return "GA Application Disclosure"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm(E_PreparerFormT.GeorgiaDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("LenderName", broker.CompanyName);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("PropertyAddress", string.Format("{0}{1}{2}{1}", dataLoan.sSpAddr, Environment.NewLine, Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip)));
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("date", DateTime.Today.ToShortDateString());
        }
    }
}