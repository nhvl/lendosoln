﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Reflection;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.PdfGenerator;

    /// <summary>
    /// Provides utility functionality for manipulating batch PDFs.
    /// </summary>
    public class BatchPdfHelper
    {
        /// <summary>
        /// Creates an instance of <see cref="GenericBatchPDF"/> from a specified <see cref="PdfPrintList"/>.
        /// </summary>
        /// <param name="printList">The list of PDFs the user has selected to print.</param>
        /// <returns>A batch PDF for capturing all the specified PDFs as one file.</returns>
        public GenericBatchPDF CreateGenericBatchPdfInstance(Async.PdfPrintList printList)
        {
            IReadOnlyList<IPDFPrintItem> printItems = this.GetPrintItems(printList);
            HashSet<string> dependencies = new HashSet<string>();
            foreach (var item in printItems)
            {
                dependencies.UnionWith(item.DependencyFields.CoalesceWithEmpty());
            }

            var dataLoan = new CPageData(printList.sLId, dependencies);
            dataLoan.InitLoad();

            GenericBatchPDF genericBatch = new GenericBatchPDF();

            BatchPDFControl pdfLayoutBatch = null;

            foreach (IPDFGenerator pdf in printItems)
            {
                ((IPDFPrintItem)pdf).DataLoan = dataLoan;
                if (!((IPDFPrintItem)pdf).HasPdf)
                {
                    continue;
                }

                if (pdf is LendersOffice.PdfGenerator.AbstractPDFFile && !(pdf is LendersOffice.Pdf.CCustomPDF))
                {
                    // 2018-02 tj - While I'd love to include custom PDFs in this more efficient printing process,
                    // BatchPDFControl cannot handle constructing a layout for multi-page PDFs; it will assume that
                    // every PDF is only one page, as is the case for standard forms.
                    if (pdfLayoutBatch == null)
                    {
                        pdfLayoutBatch = new BatchPDFControl();
                        genericBatch.Add(pdfLayoutBatch);
                    }

                    pdfLayoutBatch.Add((LendersOffice.PdfGenerator.AbstractPDFFile)pdf);
                }
                else
                {
                    if (pdfLayoutBatch != null)
                    {
                        pdfLayoutBatch = null;
                    }

                    genericBatch.Add(pdf);
                }
            }

            return genericBatch;
        }

        /// <summary>
        /// Converts the <see cref="PdfPrintList"/> into a list of PDF print items.
        /// </summary>
        /// <param name="printList">The list of PDFs the user has selected to print.</param>
        /// <returns>A list of the PDF print items specified in the print list.</returns>
        private IReadOnlyList<IPDFPrintItem> GetPrintItems(Async.PdfPrintList printList)
        {
            List<IPDFPrintItem> list = new List<IPDFPrintItem>(printList.ItemList.Count);
            foreach (Async.PdfPrintItem item in printList.ItemList)
            {
                string itemType = item.Type;
                if (itemType == "custompdf")
                {
                    CCustomPDF customPdf = new CCustomPDF();

                    NameValueCollection arguments = new NameValueCollection(4);
                    arguments["printoptions"] = printList.PrintOptions;
                    arguments["custompdfid"] = item.ID;
                    arguments["pagesize"] = item.PageSize;
                    arguments["applicationid"] = item.ApplicationID.ToString();

                    customPdf.Arguments = arguments;
                    list.Add(customPdf);
                }
                else
                {
                    // 5/27/2004 dd - PDF printint class need to be in namespace LendersOffice.Pdf
                    string type = "LendersOffice.Pdf." + item.ID;

                    ConstructorInfo c = PDFClassHashTable.GetConstructor(type);
                    if (c != null)
                    {
                        Guid appid = item.ApplicationID;
                        Guid recordid = item.RecordID;

                        NameValueCollection arguments = new NameValueCollection(3);
                        arguments["printoptions"] = printList.PrintOptions;

                        arguments["applicationid"] = appid.ToString();

                        if (recordid != Guid.Empty)
                        {
                            arguments["recordid"] = recordid.ToString();
                        }

                        if (!string.IsNullOrEmpty(item.BorrType))
                        {
                            arguments["borrtype"] = item.BorrType;
                        }

                        if (!string.IsNullOrEmpty(item.IsAddUli))
                        {
                            arguments["isadduli"] = item.IsAddUli;
                        }

                        IPDFPrintItem printItem = (IPDFPrintItem)c.Invoke(new object[0]);
                        printItem.Arguments = arguments;

                        list.Add(printItem);
                    }
                }
            }

            return list;
        }
    }
}
