﻿namespace LendersOffice.Pdf
{
    /// <summary>
    /// PDF class for Standard 203k Purchase Transaction Form.
    /// </summary>
    public class Standard203kPurchaseTransaction : Abstract203kMaxMortgagePdf
    {
        /// <summary>
        /// PDF Description.
        /// </summary>
        public override string Description
        {
            get
            {
                return "Standard 203k Purchase Transaction";
            }
        }

        /// <summary>
        /// PDF File Name.
        /// </summary>
        public override string PdfFile
        {
            get
            {
                return "203(k)_MM_Standard_Purchase.pdf";
            }
        }
    }
}
