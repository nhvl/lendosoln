using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CConsumerChoicePDF : AbstractLetterPDF
    {
        public override string Description 
        {
            get { return "FHA Consumer Choice Disclosure Notice (Before 2009)"; }
        }
        public override string PdfFile 
        {
            get { return "ConsumerChoice.pdf"; }
        }
		public override bool OnlyPrintPrimary
		{
			get { return true; }
		}
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            AddFormFieldData("GfeTilPrepareDate", dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject).PrepareDate_rep);
        }
    }
}