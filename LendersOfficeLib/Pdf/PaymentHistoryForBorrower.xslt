﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="1.0" encoding="utf-16" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates select="main_content"/>
  </xsl:template>
  <xsl:template match="main_content">
    <html>
      <head>
        <title>Payment History</title>
        <style>
          * td, * h1, * h2, * th, * caption {
          font-size: .9em;
          font-family: helvetica, verdana, sans-serif;
          }

          .Label {
          font-weight:bold;
          text-align: left;
          min-width: 5em;
          }

          .Spacer {
          border: 1px solid black;
          width: 75%;
          padding: 5px;
          margin-bottom: 0.5em;
          }
          .BrokerTable {
          }
          .BrokerTable td, .BrokerTable th {
          text-align: left;
          padding-right: 1em;
          }

          .BorderTable {

          }
          .BorderTable td {
          vertical-align: top;
          width: 15em;
          padding: 0px;
          }

          #Payments {
          border: 1px solid black;
          width: 100%;
          padding: 5px;
          }
          .PaymentTable {
          width: 100%;
          border-spacing: 0px;
          border-collapse: collapse;
          margin-bottom: 1.5em;
          }
          .PaymentTable td, .PaymentTable th {
          text-align: right;
          padding-right: 12px;
          vertical-align: text-top
          }
          tr.Payment td {
          border-bottom: 1px dashed black;
          }
          td.Notes, th.Notes, td.Payee, th.Payee {
          width: 16em;
          text-align: right;
          padding-right: 5px;
          }
          td.First, th.First {
          width: 13.5em;
          text-align: left;
          }

          td.RowCount {
          width: 12em;
          text-align: left;
          }
          tr.HeaderRow th {
          border-bottom: 1px solid black;
          }

          tr.TotalsRow td {
          border-top: 2px solid black;
          }

          #MonthlyPayment {

          }

          #EscrowDisbursements {

          }

          #OtherPayments {

          }

          caption {
          text-align: left;
          font-weight: bold;
          margin-bottom: 0.25em;
          }
          table {
          margin-bottom: 0.5em;
          }
          h1 {
          font-size: 2em;
          }
          h2 {
          font-size: 1.5em;
          }
        </style>
      </head>
      <body>
        <h1>
          <xsl:value-of select="loan/Broker/Name"/>
        </h1>
        <table class="BrokerTable">
          <tr>
            <td rowspan="2">
              <xsl:value-of select="loan/Broker/Address"/>
              <br />
              <xsl:value-of select="loan/Broker/City"/>,
              <xsl:value-of select="loan/Broker/State"/><xsl:text> </xsl:text>
              <xsl:value-of select="loan/Broker/Zipcode"/>
            </td>
            <td class="Label">Phone</td>
            <td>
              <xsl:value-of select="loan/Broker/Phone"/>
            </td>
          </tr>
          <tr>
            <td class="Label">Fax</td>
            <td>
              <xsl:value-of select="loan/Broker/Fax"/>
            </td>
          </tr>
        </table>

        <h2>Payment History</h2>
        <div class="Spacer">
          <table class="BorderTable">
            <tr>
              <td class="Label">Loan number</td>
              <td>
                <xsl:value-of select="loan/sLNm"/>
              </td>
              <td class="Label">Current date</td>
              <td>
                <xsl:value-of select="loan/sTodayDate"/>
              </td>
            </tr>
            <tr>
              <td rowspan="2" class="Label">Borrower</td>
              <td rowspan="2">
                <xsl:value-of select="app/aBNm"/>
                <br />
                <xsl:value-of select="app/aCNm"/>
              </td>
              <td class="Label">Loan closed</td>
              <td>
                <xsl:value-of select="loan/sClosedD_rep"/>
              </td>
            </tr>
            <tr>
              <td class="Label">Loan funded</td>
              <td>
                <xsl:value-of select="loan/sFundD_rep"/>
              </td>
            </tr>
            <tr>
              <td rowspan="2" class="Label">Property</td>
              <td rowspan="2">
                <xsl:value-of select="loan/sSpAddr"/>
                <br />
                <xsl:value-of select="loan/sSpCity"/>,
                <xsl:value-of select="loan/sSpState"/><xsl:text> </xsl:text>
                <xsl:value-of select="loan/sSpZip"/>
              </td>
              <td class="Label">First payment due</td>
              <td>
                <xsl:value-of select="loan/sSchedDueD1_rep"/>
              </td>
            </tr>
          </table>
        </div>
        <div class="Spacer">
          <table class="BorderTable">
            <tr>
              <td class="Label">Loan amount</td>
              <td>
                <xsl:value-of select="loan/sFinalLAmt_rep"/>
              </td>
              <td class="Label">Unpaid principal balance</td>
              <td>
                <xsl:value-of select="loan/sServicingTotalDueFunds_Principal_rep_neg"/>
              </td>
            </tr>
            <tr>
              <td class="Label">Rate</td>
              <td>
                <xsl:value-of select="loan/sNoteIR_rep"/>
              </td>
              <td class="Label">Servicing interest received</td>
              <td>
                <xsl:value-of select="loan/sServicingCollectedAndDisbursedDueFunds_Interest_rep"/>
              </td>
            </tr>
            <tr>
              <td class="Label">Original term / due</td>
              <td>
                <xsl:value-of select="loan/sTerm_rep"/>
                <xsl:text> / </xsl:text>
                <xsl:value-of select="loan/sDue_rep"/>
              </td>
              <td class="Label">Escrow account balance</td>
              <td>
                <xsl:value-of select="loan/sServicingCurrentTotalFunds_Escrow_rep"/>
              </td>
            </tr>
            <tr>
              <td class="Label">Escrow balance at closing</td>
              <td>
                <xsl:value-of select="loan/sInitialDeposit_Escrow_rep"/>
              </td>
            </tr>
          </table>
        </div>

        <xsl:apply-templates select="loan/Payments" />

      </body>
    </html>
  </xsl:template>

  <xsl:template match="loan/Payments">
    <div id="Payments">
      <table id="MonthlyPayments" class="PaymentTable">
        <caption>Monthly Payments</caption>
        <xsl:copy-of select="$HeaderRowXSLT"/>
        <xsl:apply-templates select="Payment[@ServicingTransactionT='Monthly Payment']" />
        <xsl:apply-templates select="Total[@ServicingTransactionT='Monthly Payment Totals']" />
      </table>

      <table id="EscrowDisbursements" class="PaymentTable">
        <caption>Escrow Disbursements</caption>
        <xsl:copy-of select="$HeaderRowXSLT"/>
        <xsl:apply-templates select="Payment[@ServicingTransactionT='Escrow Disbursement']" />
        <xsl:apply-templates select="Total[@ServicingTransactionT='Escrow Disbursement Totals']" />

      </table>

      <table id="LateFeeCharges" class="PaymentTable">
        <caption>Late Fees Charged</caption>
        <xsl:copy-of select="$HeaderRowXSLT"/>
        <xsl:apply-templates select="Payment[@ServicingTransactionT='Late fee charged']" />
        <xsl:apply-templates select="Total[@ServicingTransactionT='Late Fee Totals']" />

      </table>

      <table id="OtherPayments" class="PaymentTable">
        <caption>Other Payments</caption>
        <xsl:copy-of select="$HeaderRowXSLT"/>
        <xsl:apply-templates select="Payment[@ServicingTransactionT!='Monthly Payment' and @ServicingTransactionT!='Escrow Disbursement' and @ServicingTransactionT!='Late fee charged']" />
        <xsl:apply-templates select="Total[@ServicingTransactionT='Other Payment Totals']" />
      </table>
    </div>
  </xsl:template>

  <xsl:variable name="HeaderRowXSLT">
    <tr class="HeaderRow">
      <th class="First">Payment type</th>
      <th>Due date</th>
      <th>Payment received date</th>
      <th>Payment credited date</th>
      <th>Payment amt</th>
      <th>Principal</th>
      <th>Interest</th>
      <th>Escrow</th>
      <th>Late Fee</th>
      <th>Other</th>
      <th class="Payee">Payee</th>
    </tr>
  </xsl:variable>

  <xsl:template match="Payment">
    <tr class="Payment">
      <td class="First">
        <xsl:value-of select="@ServicingTransactionT" />
      </td>
      <td>
        <xsl:value-of select="DueD_rep" />
      </td>
      <td>
        <xsl:value-of select="PaymentReceivedD_rep"/>
      </td>
      <td>
        <xsl:value-of select="PaymentD_rep" />
      </td>
      <td>
        <xsl:value-of select="PaymentAmt_rep" />
      </td>
      <td>
        <xsl:value-of select="Principal_rep" />
      </td>
      <td>
        <xsl:value-of select="Interest_rep" />
      </td>
      <td>
        <xsl:value-of select="Escrow_rep" />
      </td>
      <td>
        <xsl:value-of select="LateFee_rep" />
      </td>
      <td>
        <xsl:value-of select="Other_rep" />
      </td>
      <td class="Payee">
        <xsl:value-of select="Payee"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="Total">
    <tr class="TotalsRow">
      <td class="First">Totals</td>
      <td colspan="3" class="RowCount">
        <xsl:value-of select="RowCountText"/>
      </td>
      <td>
        <xsl:value-of select="PaymentAmt_rep" />
      </td>
      <td>
        <xsl:value-of select="Principal_rep" />
      </td>
      <td>
        <xsl:value-of select="Interest_rep" />
      </td>
      <td>
        <xsl:value-of select="Escrow_rep" />
      </td>
      <td>
        <xsl:value-of select="LateFee_rep" />
      </td>
      <td>
        <xsl:value-of select="Other_rep" />
      </td>
      <td>
        
      </td>
      <td>

      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
