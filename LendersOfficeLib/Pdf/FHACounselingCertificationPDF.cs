using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CFHACounselingCertificationPDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "FHACounselingCertification.pdf";        	 }
        }
        public override string Description 
        {
            get { return "FHA Counseling Certification"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("CompanyName", agent.CompanyName);

            string applicants = dataApp.aBNm;
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "")
                applicants += " & " + dataApp.aCNm;

            AddFormFieldData("Applicants", applicants);

        }
	}
}
