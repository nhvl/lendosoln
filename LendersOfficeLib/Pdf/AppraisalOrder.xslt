﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="1.0" encoding="utf-16" indent="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates select="APPRAISAL_ORDER"/>
    </xsl:template>

    <xsl:template match="APPRAISAL_ORDER">
        <xsl:comment>Just going to whip up a quick and dirty table-in-table page. Not pretty, but time is low.</xsl:comment>
        <html>
            <head>
                <title>Appraisal Order</title>
                <style>
                    table {
                    width:100%;
                    border-spacing: 0px;
                    border-collapse: collapse;
                    }
                    th {
                    background-color: #C00;
                    color: white;
                    text-align: left;
                    }
                    .infotable td {
                    min-height: 1em;
                    border-bottom: 1px solid black;
                    width: 25%;
                    }
                    .fieldValue {
                    
                    }
                </style>
            </head>
            <body>
                <h3>
                    Appraisal Order
                </h3>
                <table id="AppraisalOrderTable">
                    <tr>
                        <td>
                            <table class="infotable">
                                <tr>
                                    <th colspan="4">
                                        Reference Info
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        File #
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="FileNumber"></xsl:value-of>
                                        </span>
                                    </td>
                                    <td>
                                        Case #
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="CaseNumber"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Loan # / Ref #
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="LoanNumber"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="4">
                                        Client Info
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Client:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="ClientName"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Client Address:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="ClientAddress1"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <xsl:if test="ClientCSZ != ''">
                                    <tr>
                                        <td>

                                        </td>
                                        <td colspan="3">
                                            <span class="fieldValue">
                                                <xsl:value-of select="ClientCSZ"></xsl:value-of>
                                            </span>
                                        </td>
                                    </tr>
                                </xsl:if>
                                <tr>
                                    <td>
                                        Processor:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="ProcessorID"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Processor 2:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="ProcessorID2"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date Needed:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="DateNeeded_String"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Lender:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="LenderName"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="4">
                                        Borrower Info
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Name:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="BorrowerName"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Address:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="Address1"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        City:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="City"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        State:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="State"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Zip:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="Zip"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        County:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="County"></xsl:value-of>
                                        </span>
                                    </td>
                                    <td>
                                        Township/Borough:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="TownshipBoro"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Legal Description:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="LegalDescription"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Borrower Email:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="BorrowerEmail"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Total Land Area:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="TotalLandArea"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="4">
                                        Assignment Info
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Property Type:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="PropertyType"></xsl:value-of>
                                        </span>
                                    </td>
                                    <td>
                                        Product 1:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="ProductID"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        FHA:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="FHAFlag"></xsl:value-of>
                                        </span>
                                    </td>
                                    <td>
                                        Product 2:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="ProductID2"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Intended Use/Loan Type:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="LoanTypeName"></xsl:value-of>
                                        </span>
                                    </td>
                                    <td>
                                        Product 3:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="ProductID3"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Purchase/Sale Price:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="sPurchPrice"></xsl:value-of>
                                        </span>
                                    </td>
                                    <td>
                                        Product 4:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="ProductID4"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Estimated Value:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="EstimatedValue"></xsl:value-of>
                                        </span>
                                    </td>
                                    <td>
                                        Product 5:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="ProductID5"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Loan Amount:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="sLAmtCalc"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Occupancy:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="Occupancy"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table style="page-break-before: always">
                    <tr>
                        <td>
                            <table class="infotable">
                                <tr>
                                    <th colspan="4">
                                        Billing Info
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        Payment Method:
                                        <span class="fieldValue">
                                            <xsl:value-of select="BillingMethodName"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        Truth in Lending Date:
                                        <span class="fieldValue">
                                            <xsl:value-of select="TILDate_String"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="4">
                                        Contact Info
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Contact Name:
                                        <span class="fieldValue">
                                            <xsl:value-of select="ContactName"></xsl:value-of>
                                        </span>
                                    </td>
                                    <td colspan="3">
                                        Home Phone:
                                        <span class="fieldValue">
                                            <xsl:value-of select="ContactNumber"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Work Phone:
                                        <span class="fieldValue">
                                            <xsl:value-of select="ContactWorkNumber"></xsl:value-of>
                                        </span>
                                    </td>
                                    <td colspan="3">
                                        Other Phone:
                                        <span class="fieldValue">
                                            <xsl:value-of select="ContactOtherNumber"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="4">
                                        Status Info
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Order Status:
                                        <span class="fieldValue">
                                            <xsl:value-of select="StatusName"></xsl:value-of>
                                        </span>
                                    </td>
                                    <td colspan="3">
                                        Estimated Delivery:
                                        <span class="fieldValue">
                                            <xsl:value-of select="DateEstCompletion_String"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        Inspection Scheduled:
                                        <span class="fieldValue">
                                            <xsl:value-of select="InspectionStartDate_String"></xsl:value-of>
                                        </span>
                                        <xsl:if test="InspectionStartDate_String != ''">
                                            To
                                            <span class="fieldValue">
                                                <xsl:value-of select="InspectionEndDate_String"></xsl:value-of>
                                            </span>
                                            At
                                            <span class="fieldValue">
                                                <xsl:value-of select="InspectionTime"></xsl:value-of>
                                            </span>
                                        </xsl:if>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        Office Notes:
                                        <span class="fieldValue">
                                            <xsl:value-of select="Notes"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        Date Completed:
                                        <span class="fieldValue">
                                            <xsl:value-of select="DateCompleted_String"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        Processed By:
                                        <span class="fieldValue">
                                            <xsl:value-of select="ProcessedBy"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        Status History
                                        <br />
                                        <xsl:apply-templates select="StatusHistoryItem" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="StatusHistoryItem">
        <xsl:value-of select="DateTimeStamp_String"></xsl:value-of>
        <xsl:value-of select="StatusName"></xsl:value-of>-
        <xsl:value-of select="StatusDescription"></xsl:value-of>
        <br />
    </xsl:template>

</xsl:stylesheet>

