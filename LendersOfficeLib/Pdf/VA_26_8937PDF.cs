namespace LendersOffice.Pdf
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    public class CVA_26_8937PDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "VA-26-8937.pdf";        	 }
        }
        public override string Description 
        {
            get { return "VA Verification of Benefit (VA 26-8937)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/VA/VAVerificationBenefit.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_8937Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_8937Lender", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBDob", dataApp.aBDob_rep);
            AddFormFieldData("aBAddrFull", Tools.FormatAddress(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("aBSsn", dataApp.aBSsn);

            AddFormFieldData("aVaClaimFolderNum", dataApp.aVaClaimFolderNum);
            AddFormFieldData("aVaServiceNum", dataApp.aVaServiceNum);
            AddFormFieldData("aVaIndebtCertifyTri", dataApp.aVaIndebtCertifyTri);
            AddFormFieldData("aVAFileClaimDisabilityCertifyTri", dataApp.aVAFileClaimDisabilityCertifyTri);

        }
	}
}
