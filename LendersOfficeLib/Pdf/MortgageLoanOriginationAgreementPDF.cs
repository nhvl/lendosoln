namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;

    public class CMortgageLoanOriginationAgreementPDF : AbstractLetterPDF
	{
 
        public override string PdfFile 
        {
            get { return "MortgageLoanOriginationAgreement.pdf"; }
        }
        public override string Description 
        {
            get { return "Mortgage Loan Origination Agreement"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/MortgageLoanOriginationAgreement.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.MortgageLoanOrigAgreement, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 

            string borrowerName = dataApp.aBNm;
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                borrowerName += " & " + dataApp.aCNm;
            }
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("BorrowerName", borrowerName);
            AddFormFieldData("BorrowerAddress", dataApp.aBAddr);
            AddFormFieldData("BorrowerAddress1", Tools.CombineCityStateZip(dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("MortgageLoanOrigAgreement_CompanyName", broker.CompanyName);
            AddFormFieldData("MortgageLoanOrigAgreement_StreetAddr", broker.StreetAddr);
            AddFormFieldData("BrokerAddress1", Tools.CombineCityStateZip(broker.City, broker.State, broker.Zip));
            AddFormFieldData("MortgageLoanOrigAgreement_PhoneOfCompany", string.Format("{0} / {1}", broker.PhoneOfCompany, broker.FaxOfCompany));
            AddFormFieldData("MortgageLoanOrigAgreement_PrepareDate", broker.PrepareDate_rep);
            AddFormFieldData("sMBrokerNmOfLaw", dataLoan.sMBrokerNmOfLaw);



        }

	}
}
