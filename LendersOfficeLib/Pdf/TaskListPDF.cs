﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using System.Xml.Xsl;
    using DataAccess;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.PdfGenerator;
    using LendersOffice.Security;

    public class CTaskListPDF : AbstractXsltPdf
    {
        private Dictionary<int, ConditionCategory> m_conditionCategories;
        protected override void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp)
        {
            writer.WriteStartElement("TASKLIST");

            List<Task> tasks;

            string cacheId = Arguments["cacheId"];
            string loanId = Arguments["loanId"];
            string isCondition = Arguments["conditionmode"];
            bool needConditionCategories = !string.IsNullOrEmpty(isCondition) && isCondition.Equals("true", StringComparison.OrdinalIgnoreCase);
            
            if (needConditionCategories)
            {
                writer.WriteAttributeString("Heading", "Condition");
                writer.WriteAttributeString("Title", "Condition List - " + dataLoan.sLNm);
                PdfName = "ConditionList_" + dataLoan.sLNm;
            }
            else
            {
                writer.WriteAttributeString("Heading", "Task");
                writer.WriteAttributeString("Title", "Task List - " + dataLoan.sLNm);
                PdfName = "TaskList_" + dataLoan.sLNm; 
            }

            writer.WriteAttributeString("BrokerName", this.GetBrokerName);

            if (string.IsNullOrEmpty(cacheId))
            {
                // If there's no cacheId, then get all of the tasks
                tasks = GetAllTasksByEmployeeAccess(loanId);
            }
            else
            {
                // Otherwise, get the filtered list of tasks according to the cache
                tasks = GetTasksUsingCacheId(loanId, cacheId);
            }

            if (needConditionCategories)
            {
                m_conditionCategories = ConditionCategory.GetCategories(PrincipalFactory.CurrentPrincipal.BrokerId).ToDictionary(p => p.Id);
            }
            else
            {
                m_conditionCategories = new Dictionary<int, ConditionCategory>();
            }

            foreach (var task in tasks)
            {
                WriteTaskInformation(needConditionCategories, writer, dataLoan, task);
            }

            writer.WriteEndElement(); // </TASKLIST>
        }

        private List<Task> GetTasksUsingCacheId(string loanId, string cacheId)
        {
            // If we ever change the method of task selection, make sure that each ID is distinct
            string taskIDsRaw = AutoExpiredTextCache.GetFromCache(Arguments["cacheId"]);

            if (taskIDsRaw == null)
            {
                throw CBaseException.GenericException("The task IDs are no longer in the cache");
            }

            string[] taskIDs = taskIDsRaw.Split(',');

            var allTasks = GetAllTasksByEmployeeAccess(loanId);

            allTasks.Sort((t1, t2) => t1.TaskId.CompareTo(t2.TaskId));

            List<Task> resultTasks = new List<Task>();

            foreach (string taskID in taskIDs) // O(m log n), where m is num tasks to get, n is num total tasks
            {
                resultTasks.Add(allTasks.Find((t) => t.TaskId == taskID));
            }

            return resultTasks;
        }

        private List<Task> GetAllTasksByEmployeeAccess(string loanId)
        {
            List<Task> allTasks = new List<Task>();
            if (HttpContext.Current == null)
            {
                var principal = PrincipalFactory.CurrentPrincipal;
                allTasks = Task.GetTasksByEmployeeAccess(principal.BrokerId, new Guid(loanId), principal.UserId);
            }
            else if (HttpContext.Current.User is BrokerUserPrincipal)
            {
                allTasks = Task.GetTasksByEmployeeAccess(
                    BrokerUserPrincipal.CurrentPrincipal.BrokerId,
                    new Guid(loanId),
                    BrokerUserPrincipal.CurrentPrincipal.EmployeeId
                );
            }
            else
            {
                AbstractUserPrincipal p = (AbstractUserPrincipal)HttpContext.Current.User;
                allTasks = Task.GetTasksByEmployeeAccess(p.BrokerId, new Guid(loanId), p.UserId);
            }
            return allTasks;
        }

        private void WriteTaskInformation(bool includeCategory, XmlWriter writer, CPageData dataLoan, Task task) // pass in the rows here, possibly
        {
            writer.WriteStartElement("Task");
            writer.WriteElementString("TaskID", task.TaskId.ToString());
            writer.WriteElementString("Subject", task.TaskSubject.ToString());
            
            // convert these dates to short
            string dueDate = "";
            string followupDate = "";
            string isStyleDueDate = "0"; // 0 for false, 1 for true
            string isStyleFollowupDate = "0";
            if (task.TaskDueDate != null)
            {
                DateTime date = (DateTime)task.TaskDueDate;
                dueDate = date.ToString("MM/dd/yyyy");
                if (date.Date <= DateTime.Now.Date)
                {
                    isStyleDueDate = "1";
                }
            }
            else
            {
                dueDate = task.TaskSingleDueDate;
            }
            if (task.TaskFollowUpDate != null)
            {
                DateTime date = (DateTime) task.TaskFollowUpDate;
                followupDate = date.ToString("MM/dd/yyyy");
                if (date.Date <= DateTime.Now.Date)
                {
                    isStyleFollowupDate = "1";
                }
            }
            writer.WriteElementString("DueDate", dueDate);
            writer.WriteElementString("isStyleDueDate", isStyleDueDate);
            writer.WriteElementString("FollowupDate", followupDate);
            writer.WriteElementString("isStyleFollowupDate", isStyleFollowupDate);
            if (includeCategory)
            {
                if (task.TaskIsCondition && task.CondCategoryId.HasValue && m_conditionCategories.ContainsKey(task.CondCategoryId.Value))
                {
                    writer.WriteElementString("Category", m_conditionCategories[task.CondCategoryId.Value].Category);
                }
                else
                {
                    writer.WriteElementString("Category", "");
                }
            }
            
            writer.WriteEndElement(); // </Task>
        }

        public override string Description
        {
            get { return "Task List"; }
        }

        public override PDFPageSize PageSize
        {
            get { return PDFPageSize.Letter; }
        }

        private string GetPmlLenderSiteId(string loanId)
        {
            AbstractUserPrincipal p = PrincipalFactory.CurrentPrincipal;
            if (loanId != null)
            {
                return Tools.GetFileDBKeyForPmlLogoWithLoanId(p.BrokerDB.PmlSiteID, new Guid(loanId));
            }
            else
            {
                return Tools.GetFileDBKeyForPmlLogoWithLoanId(p.BrokerDB.PmlSiteID, Guid.Empty);
            }
        }

        private string GetBrokerName
        {
            get
            {
                AbstractUserPrincipal p = PrincipalFactory.CurrentPrincipal;
                return p.BrokerDB.Name;
            }
        }
        protected override XsltArgumentList XsltParams
        {
            get
            {
                string loanId = Arguments["loanId"];
                string m_pmlLenderSiteId = GetPmlLenderSiteId(loanId);

                XsltArgumentList args = new XsltArgumentList();
                if (FileDBTools.DoesFileExist(E_FileDB.Normal, m_pmlLenderSiteId.ToLower() + ".logo.gif"))
                {
                    args.AddParam("LogoSource", "", "LogoPL.aspx?id=" + m_pmlLenderSiteId);
                }

                return args;
            }
        }
    }
}
