using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;


namespace LendersOffice.Pdf
{
	public class CPrivacyPolicyDisclosurePDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "PrivacyPolicyDisclosure.pdf"; }
        }
        public override string Description 
        {
            get { return "Privacy Policy Disclosure"; }
        }
		public override string EditLink 
		{
			get { return "/newlos/Disclosure/PrivacyPolicyDisclosure.aspx"; }
		}


        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.PrivacyPolicyDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 


            string borrowerName = dataApp.aBNm;
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                borrowerName += " & " + dataApp.aCNm;
            }
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("BrokerName", broker.CompanyName);
            AddFormFieldData("BrokerAddress", broker.StreetAddr);
            AddFormFieldData("BrokerAddress1", broker.CityStateZip);
            AddFormFieldData("BrokerPhone", broker.PhoneOfCompany);
            AddFormFieldData("BorrowerName", borrowerName);
            AddFormFieldData("aBAddr", dataApp.aBAddr);
            AddFormFieldData("Address1", Tools.CombineCityStateZip(dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("aBHPhone", dataApp.aBHPhone);


        
        }

	}
}
