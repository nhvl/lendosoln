using System;
using System.Collections;
using System.Collections.Specialized;

using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    /// <summary>
    /// Summary description for HUD_92800_5B.
    /// </summary>
    public abstract class AbstractHUD_92800_5BPDF : AbstractLetterPDF
    {
		protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sFHACondCommUnderNationalHousingAct", dataLoan.sFHACondCommUnderNationalHousingAct);
            AddFormFieldData("sFHACondCommNationalHousingActSection", dataLoan.sFHACondCommNationalHousingActSection);
            AddFormFieldData("sFHACondCommSeeBelow", dataLoan.sFHACondCommSeeBelow);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            IPreparerFields agent = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAConditionalCommitment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("FHAConditionalCommitmentPreparerName", agent.PreparerName);
            AddFormFieldData("FHAConditionalCommitmentNamePrepareDate", agent.PrepareDate_rep);
            AddFormFieldData("sFHALenderIdCode", dataLoan.sFHALenderIdCode);
            AddFormFieldData("sFHASponsorAgentIdCode", dataLoan.sFHASponsorAgentIdCode);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFHACondCommInstCaseRef", dataLoan.sFHACondCommInstCaseRef);
            AddFormFieldData("ConstructionTypeExisting", dataLoan.sFHAConstructionT == E_sFHAConstructionT.Existing || dataLoan.sFHAConstructionT == E_sFHAConstructionT.New);
            AddFormFieldData("ConstructionTypeProposed", dataLoan.sFHAConstructionT == E_sFHAConstructionT.Proposed);
            AddFormFieldData("sProHazIns", dataLoan.sProHazIns_rep);
            AddFormFieldData("sProRealETx", dataLoan.sProRealETx_rep);
            AddFormFieldData("sFHACondCommIssuedD", dataLoan.sFHACondCommIssuedD_rep);
            AddFormFieldData("sFHACondCommExpiredD", dataLoan.sFHACondCommExpiredD_rep);
            AddFormFieldData("sFHACondCommImprovedLivingArea", dataLoan.sFHACondCommImprovedLivingArea);
            AddFormFieldData("sFHACondCommCondoComExp", dataLoan.sProHoAssocDues_rep);
            AddFormFieldData("sFHACondCommMonExpenseEstimate", dataLoan.sFHACondCommMonExpenseEstimate_rep);
            IPreparerFields mortgagee = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACondCommitMortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("FHACondCommitMortgagee", Tools.FormatAddress(mortgagee.CompanyName, mortgagee.StreetAddr, mortgagee.City, mortgagee.State, mortgagee.Zip));
            AddFormFieldData("sFHACondCommRemainEconLife", dataLoan.sFHACondCommRemainEconLife);
            AddFormFieldData("sFHACondCommMaxFinanceEligibleTri", dataLoan.sFHACondCommMaxFinanceEligibleTri);
            AddFormFieldData("sFHACondCommIsManufacturedHousing", dataLoan.sFHACondCommIsManufacturedHousing);
            AddFormFieldData("sFHACondCommIsSection221d", dataLoan.sFHACondCommIsSection221d);
            AddFormFieldData("sFHACondCommSection221dAmt", dataLoan.sFHACondCommSection221dAmt_rep);
            AddFormFieldData("sFHACondCommIsAssuranceOfCompletion", dataLoan.sFHACondCommIsAssuranceOfCompletion);
            AddFormFieldData("sFHACondCommAssuranceOfCompletionAmt", dataLoan.sFHACondCommAssuranceOfCompletionAmt_rep);
            AddFormFieldData("sFHACondCommSeeAttached", dataLoan.sFHACondCommSeeAttached);
            AddFormFieldData("sFHACondCommSeeAttachedDesc", dataLoan.sFHACondCommSeeAttachedDesc);
            AddFormFieldData("sFHACondCommSeeFollowingCondsOnBack", dataLoan.sFHACondCommSeeFollowingCondsOnBack);
            AddFormFieldData("sFHAHousingActSection", dataLoan.sFHAHousingActSection); 
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);

            string str = dataLoan.sFHACondCommCondsOnBack1;
            if (dataLoan.sFHACondCommCondsOnBack2.TrimWhitespaceAndBOM() != "")
                str += Environment.NewLine + dataLoan.sFHACondCommCondsOnBack2;
            if (dataLoan.sFHACondCommCondsOnBack3.TrimWhitespaceAndBOM() != "")
                str += Environment.NewLine + dataLoan.sFHACondCommCondsOnBack3;
            if (dataLoan.sFHACondCommCondsOnBack4.TrimWhitespaceAndBOM() != "")
                str += Environment.NewLine + dataLoan.sFHACondCommCondsOnBack4;
            if (dataLoan.sFHACondCommCondsOnBack5.TrimWhitespaceAndBOM() != "")
                str += Environment.NewLine + dataLoan.sFHACondCommCondsOnBack5;
            if (dataLoan.sFHACondCommCondsOnBack6.TrimWhitespaceAndBOM() != "")
                str += Environment.NewLine + dataLoan.sFHACondCommCondsOnBack6;
            if (dataLoan.sFHACondCommCondsOnBack7.TrimWhitespaceAndBOM() != "")
                str += Environment.NewLine + dataLoan.sFHACondCommCondsOnBack7;
            AddFormFieldData("sFHACondCommCondsOnBack", str);




        }

    }
    public class CHUD_92800_5B_1PDF : AbstractHUD_92800_5BPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92800-5B_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }



    }
    public class CHUD_92800_5B_2PDF : AbstractHUD_92800_5BPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92800-5B_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }

    }
    public class CHUD_92800_5B_3PDF : AbstractHUD_92800_5BPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92800-5B_3.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 3"; }
        }

    }
    public class CHUD_92800_5B_4PDF : AbstractHUD_92800_5BPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92800-5B_4.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 4"; }
        }

    }
    public class CHUD_92800_5B_5PDF : AbstractHUD_92800_5BPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92800-5B_5.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 5"; }
        }

    }
    public class CHUD_92800_5B_6PDF : AbstractHUD_92800_5BPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92800-5B_6.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 6"; }
        }

    }
    public class CHUD_92800_5BPDF : AbstractBatchPDF 
    {
        public override string Description 
        {
            get { return "FHA D.E Statement of Appraised Value (HUD-92800.5B)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/FHA/FHAAppraisedValue.aspx"; }
        }
		public override bool OnlyPrintPrimary
		{
			get { return true; }
		}

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CHUD_92800_5B_1PDF(),
                                                 new CHUD_92800_5B_2PDF(),
                                                 new CHUD_92800_5B_3PDF(),
                                                 new CHUD_92800_5B_4PDF(),
                                                 new CHUD_92800_5B_5PDF(),
                                                 new CHUD_92800_5B_6PDF()
                                             };
            }
        }


    }

}
