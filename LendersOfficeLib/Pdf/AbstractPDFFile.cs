namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Text;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;

    public enum E_PdfPrintOptions 
    {
        FormWithData = 0,
        BlankForms = 1
    }
    public interface IPDFPrintItem 
    {
        string Name { get; }
        string ID { get; set; }
        string Description { get; }
        string EditLink { get; }
        string PreviewLink { get; }
        CPageData DataLoan { set; get; }
        NameValueCollection Arguments { get; set; }
        bool IsVisible { get; }
        void RenderPrintLink(StringBuilder sb);
        bool HasPrintPermission { get; }
        IEnumerable<string> DependencyFields { get; }
        
        /// <summary>
        /// Print items may not always have a PDF, such as Approval Certificate, Rate Lock Confirmation.
        /// </summary>
        bool HasPdf { get; }
    }

    /// <summary>
    /// The base class for standard PDFs (and custom PDFs via <see cref="CCustomPDF"/>).  This provides
    /// access to the loan data as well as generation of print list UI.
    /// </summary>
    /// <seealso cref="AbstractLegalPDF"/>
    /// <seealso cref="AbstractLetterPDF"/>
    /// <seealso cref="CCustomPDF"/>
    public abstract class AbstractPDFFile : LendersOffice.PdfGenerator.AbstractPDFFile, IPDFPrintItem
    {
        private NameValueCollection m_arguments = null;
        private Guid m_loanID = Guid.Empty;
        private Guid m_applicationID = Guid.Empty;
        private CPageData m_dataLoan = null; 
        private E_PdfPrintOptions m_printOptions = E_PdfPrintOptions.FormWithData;
        protected CAppData m_dataApp = null;
        private string m_script = "";
        private string m_id = "";

        public virtual string Name 
        {
            get 
            { 
                // 5/24/2004 dd - If class name has following format C{name}PDF then return unique code as {name}
                string name = GetType().Name;
                if (name.StartsWith("C") && name.EndsWith("PDF")) 
                {
                    name = name.Substring(1, name.Length - 4);
                }
                return name; 
            }
        }

        public virtual Guid RecordID 
        {
            get { return Guid.Empty; }
        }

        public virtual string Description 
        {
            get { return GetType().FullName; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PDF has its values edited on a page that has Ulad variants.
        /// </summary>
        public virtual bool IsUlad
        {
            get
            {
                return false;
            }
        }

        public virtual string EditLink 
        {
            get { return ""; }
        }

        public virtual string PreviewLink 
        {
            get 
            {
                return GeneratePreviewLink(ID);

            }
        }

        public virtual bool FormPerBorrower { get; } = false;

        private string GeneratePreviewLink(string id)
        {
            return string.Format(
                @"(<a href=""#"" onclick=""return _preview('{0}.aspx', {1});"">preview</a>)", 
                AspxTools.JsStringUnquoted(Name), 
                AspxTools.JsString(id)); 
            
        }

        public virtual E_AppPrintModeT AppPrintModeT
        {
            get
            {
                return E_AppPrintModeT.Joint;
            }
        }

        /// <summary>
        /// Gets or sets whether the PDF should render the Co-Borrower version of this form, if applicable.
        /// </summary>
        public virtual bool ShouldRenderCoBorrower
        {
            get
            {
                return false;
            }
        }

        public string ID 
        {
            get { return m_id; }
            set { m_id = value; }
        }

        public string Script 
        {
            get { return m_script; }
            set { m_script = value; }
        }

        public virtual bool IsVisible 
        {
            get { return true; }
        }

        public virtual bool HasPdf
        {
            get { return true; }
        }

        public CPageData DataLoan 
        {
            get 
            {
                if (m_dataLoan == null)
                {
                    InitializeDataLoan();
                }
                return m_dataLoan; 
            }
            set 
            { 
                m_dataLoan = value; 
                m_loanID = m_dataLoan.sLId;
            }
        }

        private Guid GetSafeGuid(string v)
        {
            if (string.IsNullOrEmpty(v))
            {
                return Guid.Empty;
            }

            Guid result = Guid.Empty;

            if (Guid.TryParse(v, out result))
            {
                return result;
            }
            else
            {
                return Guid.Empty;
            }
        }

        public NameValueCollection Arguments 
        {
            get
            {
                return m_arguments; 
            }
            set 
            { 
                m_arguments = value;

                if (m_arguments == null)
                {
                    return;
                }

                m_loanID = GetSafeGuid(m_arguments["loanid"]);

                m_applicationID = GetSafeGuid(m_arguments["applicationid"]);
                
                if (m_arguments["printoptions"] != null && m_arguments["printoptions"] != "")
                {
                    if (m_arguments["printoptions"] == "2")
                    {
                        IsTestMode = true;
                        m_printOptions = E_PdfPrintOptions.BlankForms;
                    }
                    else
                    {
                        int argPrintOptions;

                        if (int.TryParse(this.m_arguments["printoptions"], out argPrintOptions))
                        {
                            this.m_printOptions = (E_PdfPrintOptions)argPrintOptions;
                        }
                    }
                }

                string s = m_arguments["borrtype"];
                if (!string.IsNullOrEmpty(s))
                {
                    BorrType = s;
                }

            }
        }

        public string BorrType { get; set; }
        private void InitializeDataLoan() 
        {
            if (m_loanID == Guid.Empty)
            {
                m_dataLoan = null;
            }
            else
            {
                m_dataLoan = new CPageData(m_loanID, "AbstractPDFFile", DependencyFields);

                m_dataLoan.InitLoad();
            }
        }
        public void SetCurrentDataApp(CAppData dataApp) 
        {
            m_dataApp = dataApp;
            m_applicationID = dataApp.aAppId;
        }
        public void SetPdfPrintOptions(E_PdfPrintOptions options) 
        {
            m_printOptions = options;
        }
        public void RenderPrintLink(StringBuilder sb, bool isIndent) 
        {
            if (!IsVisible) return;

            if (null == m_dataLoan)
                InitializeDataLoan();

            bool isAppSpecific = Guid.Empty != m_applicationID;

            int nApps = (OnlyPrintPrimary || isAppSpecific) ? 1 : m_dataLoan.nApps;

            string oldID = this.ID;

            for (int i = 0; i < nApps; i++) 
            {
                string idPrefix = nApps == 1 ? "" : "_" + i;
                this.ID = oldID + idPrefix;

                if (isAppSpecific) 
                {
                    SetCurrentDataApp(m_dataLoan.GetAppData(m_applicationID));
                } 
                else 
                {
                    SetCurrentDataApp(m_dataLoan.GetAppData(i));
                }

                if (AppPrintModeT == E_AppPrintModeT.Joint)
                {
                    string edit = EditLink;
                    if (edit != "")
                    {
                        if(this.IsUlad)
                        {
                            edit = $"(<a onclick=\"redirectToUladPage('{edit}');\">edit</a>)";
                        }
                        else
                        {
                        edit = string.Format(@"(<a href=""javascript:_link('{0}{1}', '{2}');"">edit</a>)", Tools.VRoot, edit, this.ID);
                        }
                    }
                    string desc = Description;
                    if (nApps > 1)
                    {
                        desc += " - " + Utilities.SafeHtmlString(m_dataApp.aBNm);
                        if (m_dataApp.aCNm != "")
                            desc += " & " + Utilities.SafeHtmlString(m_dataApp.aCNm);
                    }
                    RenderPrintLink(sb, ID, BorrType, desc, m_script, edit, PreviewLink, isIndent);
                }
                else if (AppPrintModeT == E_AppPrintModeT.BorrowerAndSpouseSeparately)
                {
                    // Borrower.
                    string edit = string.Empty;
                    string printId = ID + "_B";

                    if (EditLink != "")
                    {
                        if(IsUlad)
                        {
                            edit = $"(<a onclick=\"redirectToUladPage('{EditLink}');\">edit</a>)";
                        }
                        else
                        {
                            edit = string.Format(@"(<a href=""javascript:_link('{0}{1}', '{2}');"">edit</a>)", Tools.VRoot, EditLink, printId);
                        }
                    }

                    string desc = Description;
                    desc += " - " + Utilities.SafeHtmlString(m_dataApp.aBNm);
                    RenderPrintLink(sb, printId, "B", desc, m_script, edit, GeneratePreviewLink(printId), isIndent);

                    // Spouse
                    if (string.IsNullOrEmpty(m_dataApp.aCNm) == false || this.ShouldRenderCoBorrower)
                    {

                        desc = Description;
                        desc += " - " + Utilities.SafeHtmlString(m_dataApp.aCNm);
                        printId = ID  + "_C";
                        if (EditLink != "")
                        {
                            edit = string.Format(@"(<a href=""javascript:_link('{0}{1}', '{2}');"">edit</a>)", Tools.VRoot, EditLink, printId);

                        }
                        RenderPrintLink(sb, printId, "C", desc, m_script, edit, GeneratePreviewLink(printId), isIndent);
                    }
                }
            }

        }
        public void RenderPrintLink(StringBuilder sb) 
        {
            RenderPrintLink(sb, false);
        }

        protected void RenderPrintLink(StringBuilder sb, string id, string borrType, string desc, string script, string editLink, string previewLink, bool isIndent) 
        {
            
            string extra = "appid='" + m_applicationID + "'";
            if (RecordID != Guid.Empty)
                extra += " recordid='" + RecordID + "'";
            if (!string.IsNullOrEmpty(borrType))
            {
                extra += " borrtype='" + borrType + "'";
            }
            extra += " pageSize='" + (this.PageSize == LendersOffice.PdfGenerator.PDFPageSize.Letter ? "0" : "1") + "'";

            if (this.Arguments != null)
            {
                extra += " isadduli= '" + this.Arguments["isadduli"] + "'";
            }

            string disabledCheckbox = string.Empty;
            string accessDeniedMsg = string.Empty;

            if (!HasPrintPermission)
            {
                disabledCheckbox = "disabled='disabled'";
                previewLink = string.Empty;
                accessDeniedMsg = "<span style='font-weight:bold;color:red'>" + ErrorMessages.PdfPrintAccessDenied + "</span>";

            }
            string checkBoxHtml = string.Format("<input name='{0}' type='checkbox' {1} pdf='{2}' {3} {4} {5}>",
                id, // 0
                script, // 1
                GetType().Name, // 2
                extra, // 3
                OnlyPrintPrimary ? " primaryOnly='t' " : "", // 4
                disabledCheckbox // 5
                );

            string pdfNameHtml = string.Format("{0} {1} {2}", desc, editLink, previewLink);
            if (isIndent)
            {
                sb.AppendFormat("<tr><td>&nbsp;</td><td>{0}&nbsp;{1}&nbsp;{2}</td></tr>",
                    checkBoxHtml, // 0
                    pdfNameHtml, // 1
                    accessDeniedMsg // 2
                    );
            }
            else
            {
                sb.AppendFormat("<tr><td>{0}</td><td>{1}&nbsp;{2}</td></tr>", 
                    checkBoxHtml, // 0
                    pdfNameHtml, // 1
                    accessDeniedMsg // 2
                    );
            }
        }

        /// <summary>
        /// Set whether to printout only primary borrower. Default value is false.
        /// </summary>
        public virtual bool OnlyPrintPrimary
        {
            get { return false; }
        }

        public virtual IEnumerable<string> DependencyFields
        {
            get
            {
                List<string> fieldList = new List<string>();
                Type t = this.GetType();
                while (t != null)
                {

                    IEnumerable<string> _list = CPageData.GetCPageBaseAndCAppDataDependencyList(t);
                    foreach (string f in _list)
                    {
                        if (!fieldList.Contains(f))
                        {
                            fieldList.Add(f);
                        }
                    }
                    t = t.BaseType;
                }

                if(this.FormPerBorrower && !fieldList.Contains("aCSsn"))
                {
                    fieldList.Add("aCSsn");
                }
                
                return fieldList;
            }
        }
        public AbstractPDFFile() 
        {
        }

        protected override string PdfFileName 
        {
            get { return m_root + @"\" + PdfFile; }
        }
        protected override string PdfFileLayoutName
        {
            get { return m_root + @"\" + PdfFile + ".xml.config"; }
        }
        private string m_root 
        {
            get { return System.AppDomain.CurrentDomain.BaseDirectory + "/los/pdf"; }
        }


        protected override void ApplyData() 
        {
            if (m_printOptions == E_PdfPrintOptions.BlankForms)
                return; // 1/11/2005 dd - User want to print out blank PDF form.

            if (null == m_dataLoan) 
            {
                InitializeDataLoan();
            } 

            m_dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);
            if (OnlyPrintPrimary) 
            {
                ApplyData(m_dataLoan, m_dataLoan.GetAppData(0));
            } 
            else if (m_applicationID == Guid.Empty) 
            {
                // Print all available apps.
                for (int i = 0; i < m_dataLoan.nApps; i++) 
                {
                    if (i != 0)
                        AddNewPage();

                    ApplyData(m_dataLoan, m_dataLoan.GetAppData(i));
                }

            } 
            else 
            {
                // ApplicationID specify. Print out this specify app.
                ApplyData(m_dataLoan, m_dataLoan.GetAppData(m_applicationID));
            }
        }

        protected abstract void ApplyData(CPageData dataLoan, CAppData dataApp);

        //protected abstract string PdfFile 
        //{ 
        //    get;
        //}

        public virtual bool HasPrintPermission
        {
            get { return true; }
        }
    }

    /// <summary>
    /// Facilitates letter page size for standard PDF forms.
    /// </summary>
    public abstract class AbstractLetterPDF : AbstractPDFFile 
    {
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize 
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
    }

    /// <summary>
    /// Facilitates legal page size for standard PDF forms.
    /// </summary>
    public abstract class AbstractLegalPDF : AbstractPDFFile 
    {
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize 
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Legal; }
        }

    }
    public enum E_AppPrintModeT
    {
        Joint = 0,
        BorrowerAndSpouseSeparately = 1
    }

}
