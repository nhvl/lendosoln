using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{

	public class CFloodHazardNoticePDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "FloodHazardNotice.pdf";        	 }
        }
        
        public override string Description 
        {
            get { return "Flood Hazard Notice"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/FloodHazardNotice.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string name = dataApp.aBNm;
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                name += " & " + dataApp.aCNm;
            }
            AddFormFieldData("Applicants", Tools.FormatAddress(name, dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.FloodHazardNotice, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("FloodHazardNotice", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("sLAmt", dataLoan.sLAmtCalc_rep);
            AddFormFieldData("PropertyAddress", dataLoan.sSpAddr + ", " + Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sFloodHazardBuilding", dataLoan.sFloodHazardBuilding);
            AddFormFieldData("sFloodHazardMobileHome", dataLoan.sFloodHazardMobileHome);
            AddFormFieldData("sFloodHazardCommunityDesc", dataLoan.sFloodHazardCommunityDesc);
            AddFormFieldData("sFloodHazardFedInsAvail", dataLoan.sFloodHazardFedInsAvail);
            AddFormFieldData("sFloodHazardFedInsNotAvail", dataLoan.sFloodHazardFedInsNotAvail);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
	}
}
