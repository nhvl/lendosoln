namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Common;

    public class CLoanSubmissionPDF : AbstractLegalPDF
	{

        public override string PdfFile 
        {
            get { return "LoanSubmission.pdf"; }
        } 
        
        public override string Description 
        {
            get { return "Loan Submission"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/LoanSubmission.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.LoanSubmission, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 

            AddFormFieldData("BrokerAddress", Tools.FormatAddress(broker.CompanyName, broker.StreetAddr, broker.City, broker.State, broker.Zip));



            int count = dataLoan.GetAgentRecordCount();
            // If there are multiple record of appraiser, escrow etc, then use the first
            // record.
            bool isAppraiser = false;
            bool isEscrow = false;
            bool isTitle = false;
            bool isLender = false;
            bool isLoanOfficer = false;
            bool isProcessor = false;

            for (int i = 0; i < count; i++) 
            {
                CAgentFields field = dataLoan.GetAgentFields(i);
                switch (field.AgentRoleT) 
                {
                    case E_AgentRoleT.Processor:
                        if (!isProcessor) 
                        {
                            AddFormFieldData("Processor", field.AgentName);
                            isProcessor = true;
                        }
                        break;
                    case E_AgentRoleT.LoanOfficer:
                        if (!isLoanOfficer) 
                        {
                            AddFormFieldData("LoanOfficer", field.AgentName);
                            AddFormFieldData("LoanOfficerPhone", field.Phone);
                            isLoanOfficer = true;
                        }
                        break;
                    case E_AgentRoleT.Appraiser:
                        if (!isAppraiser) 
                        {
                            AddFormFieldData("AppraisalAddress", Tools.FormatAddress(field.CompanyName, field.StreetAddr, field.City, field.State, field.Zip));
                            AddFormFieldData("AppraisalOfficer", field.AgentName);
                            AddFormFieldData("AppraisalPhone", field.Phone);
                            AddFormFieldData("AppraisalFax", field.FaxNum);
                            AddFormFieldData("AppraisalLicense", field.LicenseNumOfAgent);
                            isAppraiser = true;
                        }
                        break;
                    case E_AgentRoleT.Escrow:
                        if (!isEscrow) 
                        {
                            AddFormFieldData("EscrowAddress", Tools.FormatAddress(field.CompanyName, field.StreetAddr, field.City, field.State, field.Zip));
                            AddFormFieldData("EscrowOfficer", field.AgentName);
                            AddFormFieldData("EscrowPhone", field.Phone);
                            AddFormFieldData("EscrowFax", field.FaxNum);
                            AddFormFieldData("EscrowNumber", field.CaseNum);

                            isEscrow = true;
                        }
                        break;
                    case E_AgentRoleT.Title:
                        if (!isTitle) 
                        {
                            AddFormFieldData("TitleAddress", Tools.FormatAddress(field.CompanyName, field.StreetAddr, field.City, field.State, field.Zip));
                            AddFormFieldData("TitleOfficer", field.AgentName);
                            AddFormFieldData("TitlePhone", field.Phone);
                            AddFormFieldData("TitleFax", field.FaxNum);
                            AddFormFieldData("TitleNumber", field.CaseNum);
                            isTitle = true;
                        }
                        break;
                    case E_AgentRoleT.Lender:
                        if (!isLender) 
                        {
                            AddFormFieldData("LenderAddress", string.Format("{0}", field.StreetAddr));
                            AddFormFieldData("LenderAddress2", field.CityStateZip);
                            AddFormFieldData("LenderName", field.CompanyName);
                            AddFormFieldData("LenderOfficer", field.AgentName);
                            AddFormFieldData("LenderPhone", field.Phone);
                            AddFormFieldData("LenderFax", field.FaxNum);
                            isLender = true;
                        }
                        break;
                }
            }
            AddFormFieldData("Applicants", string.Format("{0} {1}{4}{2} {3}", dataApp.aBFirstNm, dataApp.aBLastNm, dataApp.aCFirstNm, dataApp.aCLastNm, Environment.NewLine));
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
            AddFormFieldData("sLpTemplateNm", dataLoan.sLpTemplateNm);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sSpCounty", dataLoan.sSpCounty);
            AddFormFieldData("sUnitsNum", dataLoan.sUnitsNum_rep);

            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sQualIR", dataLoan.sQualIR_rep);
			AddFormFieldData("TermDue", dataLoan.sTerm_rep + " / " + dataLoan.sDue_rep);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sLtvR", dataLoan.sLtvR_rep);
            AddFormFieldData("sCltVR", dataLoan.sCltvR_rep);
            AddFormFieldData("sEquity", dataLoan.sEquityCalc_rep);
            AddFormFieldData("sRAdjCapMon", dataLoan.sRAdjCapMon_rep);
            AddFormFieldData("sRAdjCapR", dataLoan.sRAdjCapR_rep);
            AddFormFieldData("sRAdjIndexR", dataLoan.sRAdjIndexR_rep);
            AddFormFieldData("sRAdjLifeCapR", dataLoan.sRAdjLifeCapR_rep);
            AddFormFieldData("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);
            AddFormFieldData("sLienPosT", dataLoan.sLienPosT);
            
            E_sLPurposeT sLPurposeT = dataLoan.sLPurposeT;
            if (sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                sLPurposeT = E_sLPurposeT.RefinCashout;
            }
            if (sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT == E_sLPurposeT.VaIrrrl)
                sLPurposeT = E_sLPurposeT.Refin;

            AddFormFieldData("sLPurposeT", sLPurposeT);

            AddFormFieldData("aOccT", dataApp.aOccT);
            AddFormFieldData("sLOrigF", dataLoan.sLOrigF_rep);
            AddFormFieldData("sLDiscnt", dataLoan.sLDiscnt_rep);
            AddFormFieldData("sApprF", dataLoan.sApprF_rep);
            AddFormFieldData("sCrF", dataLoan.sCrF_rep);
            AddFormFieldData("sProcF", dataLoan.sProcF_rep);
            AddFormFieldData("sLOrigFPc", dataLoan.sLOrigFPc_rep);
            AddFormFieldData("sLOrigFMb", dataLoan.sLOrigFMb_rep);
            AddFormFieldData("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            AddFormFieldData("sLDiscntFMb", dataLoan.sLDiscntFMb_rep);
            AddFormFieldData("sHmdaCensusTract", dataLoan.sHmdaCensusTract);
            AddFormFieldData("sSubmitProgramCode", dataLoan.sSubmitProgramCode);
            AddFormFieldData("sSubmitDocFull", dataLoan.sSubmitDocFull);
            AddFormFieldData("sSubmitDocOther", dataLoan.sSubmitDocOther);
            AddFormFieldData("sSubmitImpoundTaxes", dataLoan.sSubmitImpoundTaxes);
            AddFormFieldData("sSubmitImpoundHazard", dataLoan.sSubmitImpoundHazard);
            AddFormFieldData("sSubmitImpoundMI", dataLoan.sSubmitImpoundMI);
            AddFormFieldData("sSubmitImpoundFlood", dataLoan.sSubmitImpoundFlood);
            AddFormFieldData("sSubmitImpoundOther", dataLoan.sSubmitImpoundOther);
            AddFormFieldData("sSubmitImpoundOtherDesc", dataLoan.sSubmitImpoundOtherDesc);
            AddFormFieldData("sSubmitAmortDesc", dataLoan.sSubmitAmortDesc);
            AddFormFieldData("sSubmitBuydownDesc", dataLoan.sSubmitBuydownDesc);
            AddFormFieldData("sSubmitInitPmtCapR", dataLoan.sSubmitInitPmtCapR_rep);
            AddFormFieldData("sSubmitRAdjustOtherDesc", dataLoan.sSubmitRAdjustOtherDesc);
            AddFormFieldData("sRLckdD", dataLoan.sRLckdD_rep);
            AddFormFieldData("sRLckdNumOfDays", dataLoan.sRLckdNumOfDays_rep);
            AddFormFieldData("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep);
            AddFormFieldData("sSubmitMITypeDesc", dataLoan.sSubmitMITypeDesc);
            AddFormFieldData("sSubmitMIYes", dataLoan.sSubmitMIYes);
            AddFormFieldData("sRLckdIsLocked", dataLoan.sRLckdIsLocked);
            AddFormFieldData("sSubmitBrokerFax", dataLoan.sSubmitBrokerFax);
            AddFormFieldData("sDemandNotes", dataLoan.sDemandNotes);
            AddFormFieldData("sSubmitPropTDesc", dataLoan.sSubmitPropTDesc);
            AddFormFieldData("sDemandApprFeeLenderAmt", dataLoan.sDemandApprFeeLenderAmt_rep);
            AddFormFieldData("sDemandApprFeeBrokerPaidAmt", dataLoan.sDemandApprFeeBrokerPaidAmt_rep);
            AddFormFieldData("sDemandApprFeeBrokerDueAmt", dataLoan.sDemandApprFeeBrokerDueAmt_rep);
            AddFormFieldData("sDemandCreditReportFeeLenderAmt", dataLoan.sDemandCreditReportFeeLenderAmt_rep);
            AddFormFieldData("sDemandCreditReportFeeBrokerPaidAmt", dataLoan.sDemandCreditReportFeeBrokerPaidAmt_rep);
            AddFormFieldData("sDemandCreditReportFeeBrokerDueAmt", dataLoan.sDemandCreditReportFeeBrokerDueAmt_rep);
            AddFormFieldData("sDemandProcessingFeeLenderAmt", dataLoan.sDemandProcessingFeeLenderAmt_rep);
            AddFormFieldData("sDemandProcessingFeeBrokerPaidAmt", dataLoan.sDemandProcessingFeeBrokerPaidAmt_rep);
            AddFormFieldData("sDemandProcessingFeeBrokerDueAmt", dataLoan.sDemandProcessingFeeBrokerDueAmt_rep);
            AddFormFieldData("sDemandLoanDocFeeLenderAmt", dataLoan.sDemandLoanDocFeeLenderAmt_rep);
            AddFormFieldData("sDemandLoanDocFeeBrokerPaidAmt", dataLoan.sDemandLoanDocFeeBrokerPaidAmt_rep);
            AddFormFieldData("sDemandLoanDocFeeBrokerDueAmt", dataLoan.sDemandLoanDocFeeBrokerDueAmt_rep);
            AddFormFieldData("sDemandLoanDocFeeBorrowerAmt", dataLoan.sDemandLoanDocFeeBorrowerAmt_rep);
            AddFormFieldData("sDemandU1LenderDesc", dataLoan.sDemandU1LenderDesc);
            AddFormFieldData("sDemandU1LenderAmt", dataLoan.sDemandU1LenderAmt_rep);
            AddFormFieldData("sDemandU1BrokerPaidAmt", dataLoan.sDemandU1BrokerPaidAmt_rep);
            AddFormFieldData("sDemandU1BrokerDueAmt", dataLoan.sDemandU1BrokerDueAmt_rep);
            AddFormFieldData("sDemandU1BorrowerAmt", dataLoan.sDemandU1BorrowerAmt_rep);
            AddFormFieldData("sDemandU2LenderDesc", dataLoan.sDemandU2LenderDesc);
            AddFormFieldData("sDemandU2LenderAmt", dataLoan.sDemandU2LenderAmt_rep);
            AddFormFieldData("sDemandU2BrokerPaidAmt", dataLoan.sDemandU2BrokerPaidAmt_rep);
            AddFormFieldData("sDemandU2BrokerDueAmt", dataLoan.sDemandU2BrokerDueAmt_rep);
            AddFormFieldData("sDemandU2BorrowerAmt", dataLoan.sDemandU2BorrowerAmt_rep);
            AddFormFieldData("sDemandU3LenderDesc", dataLoan.sDemandU3LenderDesc);
            AddFormFieldData("sDemandU3LenderAmt", dataLoan.sDemandU3LenderAmt_rep);
            AddFormFieldData("sDemandU3BrokerPaidAmt", dataLoan.sDemandU3BrokerPaidAmt_rep);
            AddFormFieldData("sDemandU3BrokerDueAmt", dataLoan.sDemandU3BrokerDueAmt_rep);
            AddFormFieldData("sDemandU3BorrowerAmt", dataLoan.sDemandU3BorrowerAmt_rep);
            AddFormFieldData("sDemandU4LenderDesc", dataLoan.sDemandU4LenderDesc);
            AddFormFieldData("sDemandU4LenderAmt", dataLoan.sDemandU4LenderAmt_rep);
            AddFormFieldData("sDemandU4BrokerPaidAmt", dataLoan.sDemandU4BrokerPaidAmt_rep);
            AddFormFieldData("sDemandU4BrokerDueAmt", dataLoan.sDemandU4BrokerDueAmt_rep);
            AddFormFieldData("sDemandU4BorrowerAmt", dataLoan.sDemandU4BorrowerAmt_rep);
            AddFormFieldData("sDemandU5LenderDesc", dataLoan.sDemandU5LenderDesc);
            AddFormFieldData("sDemandU5LenderAmt", dataLoan.sDemandU5LenderAmt_rep);
            AddFormFieldData("sDemandU5BrokerPaidAmt", dataLoan.sDemandU5BrokerPaidAmt_rep);
            AddFormFieldData("sDemandU5BrokerDueAmt", dataLoan.sDemandU5BrokerDueAmt_rep);
            AddFormFieldData("sDemandU5BorrowerAmt", dataLoan.sDemandU5BorrowerAmt_rep);
            AddFormFieldData("sDemandU6LenderDesc", dataLoan.sDemandU6LenderDesc);
            AddFormFieldData("sDemandU6LenderAmt", dataLoan.sDemandU6LenderAmt_rep);
            AddFormFieldData("sDemandU6BrokerPaidAmt", dataLoan.sDemandU6BrokerPaidAmt_rep);
            AddFormFieldData("sDemandU6BrokerDueAmt", dataLoan.sDemandU6BrokerDueAmt_rep);
            AddFormFieldData("sDemandU6BorrowerAmt", dataLoan.sDemandU6BorrowerAmt_rep);
            AddFormFieldData("sDemandU7LenderDesc", dataLoan.sDemandU7LenderDesc);
            AddFormFieldData("sDemandU7LenderAmt", dataLoan.sDemandU7LenderAmt_rep);
            AddFormFieldData("sDemandU7BrokerPaidAmt", dataLoan.sDemandU7BrokerPaidAmt_rep);
            AddFormFieldData("sDemandU7BrokerDueAmt", dataLoan.sDemandU7BrokerDueAmt_rep);
            AddFormFieldData("sDemandU7BorrowerAmt", dataLoan.sDemandU7BorrowerAmt_rep);
            
            AddFormFieldData("sDemandLOrigFeeLenderPc", dataLoan.sDemandLOrigFeeLenderPc_rep);
            AddFormFieldData("sDemandLOrigFeeLenderMb", dataLoan.sDemandLOrigFeeLenderMb_rep);
            AddFormFieldData("sDemandLOrigFeeLenderAmt", dataLoan.sDemandLOrigFeeLenderAmt_rep);

            AddFormFieldData("sDemandLDiscountLenderPc", dataLoan.sDemandLDiscountLenderPc_rep);
            AddFormFieldData("sDemandLDiscountLenderMb", dataLoan.sDemandLDiscountLenderMb_rep);
            AddFormFieldData("sDemandLDiscountLenderAmt", dataLoan.sDemandLDiscountLenderAmt_rep);

            AddFormFieldData("sDemandLOrigFeeBrokerPc", dataLoan.sDemandLOrigFeeBrokerPc_rep);
            AddFormFieldData("sDemandLOrigFeeBrokerMb", dataLoan.sDemandLOrigFeeBrokerMb_rep);
            AddFormFieldData("sDemandLOrigFeeBrokerAmt", dataLoan.sDemandLOrigFeeBrokerAmt_rep);

            AddFormFieldData("sDemandLDiscountBrokerPc", dataLoan.sDemandLDiscountBrokerPc_rep);
            AddFormFieldData("sDemandLDiscountBrokerMb", dataLoan.sDemandLDiscountBrokerMb_rep);
            AddFormFieldData("sDemandLDiscountBrokerAmt", dataLoan.sDemandLDiscountBrokerAmt_rep);

            AddFormFieldData("sDemandYieldSpreadPremiumLenderPc", dataLoan.sDemandYieldSpreadPremiumLenderPc_rep);
            AddFormFieldData("sDemandYieldSpreadPremiumLenderMb", dataLoan.sDemandYieldSpreadPremiumLenderMb_rep);
            AddFormFieldData("sDemandYieldSpreadPremiumLenderAmt", dataLoan.sDemandYieldSpreadPremiumLenderAmt_rep);

            AddFormFieldData("sDemandYieldSpreadPremiumBrokerPc", dataLoan.sDemandYieldSpreadPremiumBrokerPc_rep);
            AddFormFieldData("sDemandYieldSpreadPremiumBrokerMb", dataLoan.sDemandYieldSpreadPremiumBrokerMb_rep);
            AddFormFieldData("sDemandYieldSpreadPremiumBrokerAmt", dataLoan.sDemandYieldSpreadPremiumBrokerAmt_rep);

            AddFormFieldData("sDemandYieldSpreadPremiumBorrowerPc", dataLoan.sDemandYieldSpreadPremiumBorrowerPc_rep);
            AddFormFieldData("sDemandYieldSpreadPremiumBorrowerMb", dataLoan.sDemandYieldSpreadPremiumBorrowerMb_rep);
            AddFormFieldData("sDemandYieldSpreadPremiumBorrowerAmt", dataLoan.sDemandYieldSpreadPremiumBorrowerAmt_rep);
            AddFormFieldData("sDemandTotalBorrowerAmt", dataLoan.sDemandTotalBorrowerAmt_rep);
            AddFormFieldData("sDemandTotalBrokerDueAmt", dataLoan.sDemandTotalBrokerDueAmt_rep);
            AddFormFieldData("sDemandTotalBrokerPaidAmt", dataLoan.sDemandTotalBrokerPaidAmt_rep);
            AddFormFieldData("sDemandTotalLenderAmt", dataLoan.sDemandTotalLenderAmt_rep);
            AddFormFieldData("sEstCloseD", dataLoan.sEstCloseD_rep);

        }

	}
}
