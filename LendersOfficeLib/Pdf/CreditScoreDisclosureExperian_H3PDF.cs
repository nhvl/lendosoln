
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    public class CreditScoreDisclosureExperian_H3_1PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosureExperian_H3_1.pdf"; }
        }

        public override string Description
        {
            get { return "Page 1"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

            dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            if (BorrType == "C")
            {
                dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
            }

            AddFormFieldData("aExperianCreatedD" , dataApp.aExperianCreatedD_rep);
            AddFormFieldData("aExperianFactors" , dataApp.aExperianFactors);
            AddFormFieldData("aExperianPercentile" , dataApp.aExperianPercentile_rep);
            AddFormFieldData("aExperianScore" , dataApp.aExperianScore_rep);
            AddFormFieldData("aNm" , dataApp.aNm);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditExperian, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("ExperianCreditScorePreparerInfo", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("aExperianSourceName", preparer.CompanyName);

            AddFormFieldData("sExperianScoreFrom" , dataApp.LoanData.sExperianScoreFrom_rep);
            AddFormFieldData("sExperianScoreTo" , dataApp.LoanData.sExperianScoreTo_rep);

        }
    }

    public class CreditScoreDisclosureExperian_H3_2PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosureExperian_H3_2.pdf"; }
        }

        public override string Description
        {
            get { return "Page 2"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

    public class CreditScoreDisclosureExperian_H3_3PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosureExperian_H3_3.pdf"; }
        }

        public override string Description
        {
            get { return "Page 3"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

    public class CreditScoreDisclosureExperian_H3PDF : AbstractBatchPDF
    {
        protected override E_AppPrintModeT AppPrintModeT
        {
            get { return E_AppPrintModeT.BorrowerAndSpouseSeparately; }
        }

        public override string EditLink
        {
            get { return "/newlos/Disclosure/CreditScoreDisclosure.aspx"; }
        }
        public override string Description
        {
            get
            {
                return "Experian Credit Score Disclosure - H-3";
            }
        }
        protected override AbstractPDFFile[] ChildPages
        {
            get 
            {
                return new AbstractPDFFile[] {
                    new CreditScoreDisclosureExperian_H3_1PDF(),
                    new CreditScoreDisclosureExperian_H3_2PDF(),
                    new CreditScoreDisclosureExperian_H3_3PDF(),

                };
            }
        }
    }
}
