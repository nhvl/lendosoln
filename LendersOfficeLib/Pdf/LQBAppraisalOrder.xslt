﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" version="1.0" encoding="utf-16" indent="yes"/>

    <xsl:template match="/">
        <xsl:apply-templates select="APPRAISAL_ORDER"/>
    </xsl:template>

    <xsl:template match="APPRAISAL_ORDER">
        <xsl:comment>Just going to whip up a quick and dirty table-in-table page. Not pretty, but time is low.</xsl:comment>
        <html>
            <head>
                <title>Appraisal Order</title>
                <style>
                  body {
                  font-family: Arial, Helvetica, sans-serif;
                  font-size: 16px;
                  }
                  table {
                  width:100%;
                  border-spacing: 0px;
                  border-collapse: collapse;
                  }
                  th {
                  background-color: maroon;
                  color: white;
                  text-align: left;
                  padding: 2px 0px 2px 5px;
                  }
                  .infotable td {
                  min-height: 1em;
                  padding: 5px 0px 5px 5px;
                  width: 25%;
                  }
                  .fieldLabel {
                  font-weight: bold;
                  }
                  .fieldValue {

                  }
                </style>
            </head>
            <body>
                <div style="text-align:center; font-size:30px; font-weight:bold">
                      Appraisal Order
                </div>
                <table id="AppraisalOrderTable">
                    <tr>
                        <td>
                            <table class="infotable">
                              <tr><td>&#160;</td></tr>
                                <tr>
                                    <th colspan="4">
                                        Loan Info
                                    </th>
                                </tr>
                                <tr>
                                    <td class="fieldLabel">
                                        Order #
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="OrderNumber"></xsl:value-of>
                                        </span>
                                    </td>
                                  <td class="fieldLabel">
                                        Case #
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="CaseNumber"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                  <td class="fieldLabel">
                                        Loan #
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="LoanNumber"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                              <tr>
                                <td>&#160;</td>
                              </tr>
                              <tr>
                                    <th colspan="4">
                                        Borrower Info
                                    </th>
                                </tr>
								<xsl:for-each select="BorrowerInfo">
									<tr>
										<tr>
											<td class="fieldLabel">
												Borrower Name:
											</td>
											<td>
												<span class="fieldValue">
												<xsl:value-of select="BorrowerName"></xsl:value-of>
												</span>
											</td>
											<td class="fieldLabel">
												Email:
											</td>
											<td>
												<span class="fieldValue">
												<xsl:value-of select="BorrowerEmail"></xsl:value-of>
												</span>
											</td>
										</tr>
										<tr>
											<td class="fieldLabel">
												Coborrower Name:
											</td>
											<td colspan="3">
												<span class="fieldValue">
												<xsl:value-of select="CoborrName"></xsl:value-of>
												</span>
											</td>
										</tr>
									</tr>
							  </xsl:for-each>
                              <tr>
                                <td>&#160;</td>
                              </tr>
                              <tr>
                                    <th colspan="4">
                                        Contact Info
                                    </th>
                                </tr>
                                <tr>
                                  <td class="fieldLabel">
                                        Contact Name:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="ContactName"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                  <td class="fieldLabel">
                                        Phone:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="ContactPhone"></xsl:value-of>
                                        </span>
                                    </td>
                                  <td class="fieldLabel">
                                        Work Phone:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="ContactWork"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                  <td class="fieldLabel">
                                        Email:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="ContactEmail"/>
                                        </span>
                                    </td>
                                  <td class="fieldLabel">
                                        Other:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="ContactOther"/>
                                        </span>
                                    </td>
                                </tr>
                              <tr>
                                <td>&#160;</td>
                              </tr>
                              <tr>
                                    <th colspan="4">
                                        Property Info
                                    </th>
                                </tr>
                                <tr>
                                  <td class="fieldLabel">
                                    Address:
                                  </td>
                                  <td rowspan="2">
                                    <span class="fieldValue">
                                      <xsl:value-of select="Addr1"/><br/>
                                      <xsl:value-of select="Addr2"/>
                                    </span>
                                  </td>
                                  <td class="fieldLabel">
                                        Property Type:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="PropertyType"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <xsl:if test="MhAdvantage != ''">
                                  <tr>
                                    <td colspan="2"></td>
                                    <td class="fieldLabel">
                                      Is home MH Advantage?
                                    </td>
                                    <td>
                                      <xsl:value-of select="MhAdvantage"></xsl:value-of>
                                    </td>
                                  </tr>
                                </xsl:if>
                                <tr>
                                    <td colspan="2"></td>
                                  <td class="fieldLabel">
                                        Occupancy Type:
                                    </td>
                                    <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="Occupancy"/>
                                        </span>                                      
                                    </td>
                                </tr>
                                <tr>
                                  <td class="fieldLabel">
                                        County:
                                    </td>
                                    <td colspan="3">
                                        <span class="fieldValue">
                                            <xsl:value-of select="County"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                  <td class="fieldLabel">
                                        Legal Description:
                                    </td>
                                    <td colspan="3" rowspan="2">
                                        <span class="fieldValue">
                                            <xsl:value-of select="LegalDescription"></xsl:value-of>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <!--<td colspan="3"></td>-->
                                </tr>
                              <tr>
                                <td>&#160;</td>
                              </tr>
                              <tr>
                                <th colspan="4">
                                  Order Info
                                </th>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Appraisal Needed:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="DateNeeded_String"></xsl:value-of>
                                  </span>
                                </td>
                                <td class="fieldLabel">
                                  Product 1:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="ProductID1"></xsl:value-of>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Intended Use:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="IntendedUse"></xsl:value-of>
                                  </span>
                                </td>
                                <td class="fieldLabel">
                                  Product 2:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="ProductID2"></xsl:value-of>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Purchase Price:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="sPurchPrice"></xsl:value-of>
                                  </span>
                                </td>
                                <td class="fieldLabel">
                                  Product 3:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="ProductID3"></xsl:value-of>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Loan Amount:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="sLAmtCalc"></xsl:value-of>
                                  </span>
                                </td>
                                <td class="fieldLabel">
                                  Product 4:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="ProductID4"></xsl:value-of>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Loan Type:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="LoanTypeName"></xsl:value-of>
                                  </span>
                                </td>
                                <td class="fieldLabel">
                                  Product 5:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="ProductID5"></xsl:value-of>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td>&#160;</td>
                              </tr>
                              <tr>
                                <th colspan="4">
                                  Appraiser Information
                                </th>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Appraiser:
                                </td>
                                <td colspan="3">
                                  <span class="fieldValue">
                                    <xsl:value-of select="AppraiserName"/>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Company Name:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="AppraiserCompany"/>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Address:
                                </td>
                                <td rowspan="2">
                                  <span class="fieldValue">
                                    <xsl:value-of select="AppraiserAddr1"/><br/>
                                    <xsl:value-of select="AppraiserAddr2"/>
                                  </span>
                                </td>
                                <td class="fieldLabel">
                                  Phone:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="AppraiserPhone"/>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td></td>
                                <!--<td></td>-->
                                <td class="fieldLabel">
                                  Fax:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="AppraiserFax"/>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Appraiser License #:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="AppraiserLicNum"/>
                                  </span>
                                </td>
                                <td class="fieldLabel">
                                  State:
                                </td>
                                <td>
                                  <span class="fieldValue">
                                    <xsl:value-of select="AppraiserLicState"/>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Expiration Date:
                                </td>
                                <td colspan="3">
                                  <span class="fieldValue">
                                    <xsl:value-of select="AppraiserLicExpD"/>
                                  </span>
                                </td>
                              </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table style="page-break-before: always">
                  <tr>
                    <td>
                      <table class="infotable">
                        <tr>
                          <th colspan="4">
                            AMC Contact Info
                          </th>
                        </tr>
                        <tr>
                          <td class="fieldLabel">
                            Contact Name :
                          </td>
                        
                        <td>
                          <span class="fieldValue">
                            <xsl:value-of select="AMCName"/>
                          </span>
                        </td>
                        <td class="fieldLabel">
                          Phone:
                        </td>
                        <td>
                          <span class="fieldValue">
                            <xsl:value-of select="AMCPhone"/>
                          </span>
                        </td>
                        </tr>
                        <tr>
                          <td class="fieldLabel">
                            Email:
                          </td>
                          <td>
                            <span class="fieldValue">
                              <xsl:value-of select="AMCEmail"/>
                            </span>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                    <tr>
                        <td>
                            <table class="infotable">
                                <tr>
                                    <th colspan="4">
                                        Order Status
                                    </th>
                                </tr>
                                <tr>
                                  <td class="fieldlabel">
                                    Estimated Completion:
                                  </td>
                                  <td colspan="3">
                                    <span class="fieldValue">
                                      <xsl:value-of select="DateEstCompletion"></xsl:value-of>
                                    </span>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="fieldLabel">
                                    Status:
                                  </td>
                                  <td>
                                        <span class="fieldValue">
                                            <xsl:value-of select="StatusName"></xsl:value-of>
                                        </span>
                                  </td>
                                  <td class="fieldLabel">
                                    Status Date:
                                  </td>
                                  <td>
                                    <span class="fieldValue">
                                      <xsl:value-of select="StatusDate"></xsl:value-of>
                                    </span>
                                  </td>
                                </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Comments:
                                </td>
                                <td rowspan="2" colspan="3">
                                  <span class="fieldValue">
                                    <xsl:value-of select="StatusComments"/>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td></td>
                                <!--<td colspan="3"></td>-->
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Inspection Scheduled:
                                </td>
                                <td colspan="3">
                                  <span class="fieldValue">
                                    <xsl:value-of select="InspectionScheduled"/>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Inspection Time:
                                </td>
                                <td colspan="3">
                                  <span class="fieldValue">
                                    <xsl:value-of select="InspectionTime"/>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Reviewer's Comments:
                                </td>
                                <td colspan="3" rowspan="2">
                                  <span class="fieldValue">
                                    <xsl:value-of select="ReviewerComments"/>
                                  </span>
                                </td>
                              </tr>
                              <tr>
                                <td></td>
                                <!--<td colspan="3"></td>-->
                              </tr>
                              <tr>
                                <td class="fieldLabel">
                                  Estimated Value:
                                </td>
                                <td colspan="3">
                                  <span class="fieldValue">
                                    <xsl:value-of select="EstimatedValue"/>
                                  </span>
                                </td>
                              </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                      <td colspan="4">
                        <span style="text-decoration:underline" class="fieldLabel">Status History</span>
                        <br />
                        <xsl:apply-templates select="StatusHistoryItem" />
                      </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="StatusHistoryItem">
        <xsl:value-of select="DateTimeStamp_String"></xsl:value-of>&#160;
        <xsl:value-of select="StatusName"></xsl:value-of>&#160;-&#160;
        <xsl:value-of select="StatusDescription"></xsl:value-of>
        <br /><br/>
    </xsl:template>
</xsl:stylesheet>

