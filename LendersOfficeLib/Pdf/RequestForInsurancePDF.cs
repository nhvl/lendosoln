namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Common;

    public class CRequestForInsurancePDF : AbstractLetterPDF
	{
         public override string PdfFile 
        {
            get { return "RequestForInsurance.pdf"; }
        }
        
        public override string Description 
        {
            get { return "Request For Insurance"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/RequestForms/RequestForInsurance.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparerField = dataLoan.GetPreparerOfForm(E_PreparerFormT.RequestOfInsurance, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            string brokerAddress = string.Format("{7}{8}{0}{8}{1}{8}{2}, {3} {4}{8}Phone: {5} Fax: {6}",
                preparerField.CompanyName, preparerField.StreetAddr, preparerField.City, preparerField.State, preparerField.Zip,
                preparerField.Phone, preparerField.FaxNum, preparerField.PreparerName, Environment.NewLine);

            if (preparerField.EmailAddr != "")
                brokerAddress += Environment.NewLine + "Email: " + preparerField.EmailAddr;

            string address = String.Format("{0}{5}{1}, {2} {3}{5}County: {4}", dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip, dataLoan.sSpCounty, Environment.NewLine);

            if (dataApp.aBNm.TrimWhitespaceAndBOM() != "") 
            {
                string str = Tools.FormatAddress(dataApp.aBNm, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip);
                AddFormFieldData("BorrowerAddress", str);
            }
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                string str = Tools.FormatAddress(dataApp.aCNm, dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip);
                AddFormFieldData("CoborrowerAddress", str);
            }
            AddFormFieldData("LenderAddress", brokerAddress);
            AddFormFieldData("PropertyAddress", address);
            AddFormFieldData("sSpLegalDesc", dataLoan.sSpLegalDesc);
            AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
            AddFormFieldData("Title", preparerField.Title);
            AddFormFieldData("sSpT", dataLoan.sSpT);
            
            E_sLPurposeT sLPurposeT = dataLoan.sLPurposeT;
            if (sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT == E_sLPurposeT.VaIrrrl)
                sLPurposeT = E_sLPurposeT.Refin;
            if (sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                sLPurposeT = E_sLPurposeT.RefinCashout;
            }

            AddFormFieldData("sLPurposeT", sLPurposeT);

            AddFormFieldData("sLienPosT", dataLoan.sLienPosT);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep); //opm 27443 fs 02/10/09
            AddFormFieldData("sInsReqReplacement", dataLoan.sInsReqReplacement);
            AddFormFieldData("sInsReqFlood", dataLoan.sInsReqFlood);
            AddFormFieldData("sInsReqWind", dataLoan.sInsReqWind);
            AddFormFieldData("sInsReqHazard", dataLoan.sInsReqHazard);
            AddFormFieldData("sInsReqEscrow", dataLoan.sInsReqEscrow);
            AddFormFieldData("sInsReqComments", dataLoan.sInsReqComments);
            AddFormFieldData("sEstCloseD", dataLoan.sEstCloseD_rep);
            AddFormFieldData("sInsReqPrepD", preparerField.PrepareDate_rep);

            CAgentFields lender = null;
            if (dataLoan.sUseLenderAgentForInsRequest)
                lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            else 
                lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Mortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (lender.IsValid) 
            {
                string str = lender.AgentName + Environment.NewLine + Tools.FormatAddress(lender.CompanyName, lender.StreetAddr, lender.City, lender.State, lender.Zip) + Environment.NewLine;
                if (lender.Phone.TrimWhitespaceAndBOM() != "") 
                {
                    str += " Phone: " + lender.Phone;
                }
                if (lender.FaxNum.TrimWhitespaceAndBOM() != "") 
                {
                    str += " Fax: " + lender.FaxNum;
                }
                if (lender.CellPhone.TrimWhitespaceAndBOM() != "") 
                {
                    str += " Cell: " + lender.CellPhone;
                }
                if (lender.EmailAddr.TrimWhitespaceAndBOM() != "") 
                {
                    str += Environment.NewLine + "Email: " + lender.EmailAddr;
                }
                    
                AddFormFieldData("LenderMortgagee", str);
            }
            CAgentFields insurance = dataLoan.GetAgentOfRole(E_AgentRoleT.HazardInsurance, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (insurance.IsValid) 
            {
                string str = insurance.AgentName + Environment.NewLine + Tools.FormatAddress(insurance.CompanyName, insurance.StreetAddr, insurance.City, insurance.State, insurance.Zip) + Environment.NewLine;
                if (insurance.Phone.TrimWhitespaceAndBOM() != "") 
                {
                    str += " Phone: " + insurance.Phone;
                }
                if (insurance.FaxNum.TrimWhitespaceAndBOM() != "") 
                {
                    str += " Fax: " + insurance.FaxNum;
                }
                if (insurance.CellPhone.TrimWhitespaceAndBOM() != "") 
                {
                    str += " Cell: " + insurance.CellPhone;
                }
                if (insurance.EmailAddr.TrimWhitespaceAndBOM() != "") 
                {
                    str += Environment.NewLine + "Email: " + insurance.EmailAddr;
                }
                    
                AddFormFieldData("InsuranceCompany", str);
            }

        }

	}
}
