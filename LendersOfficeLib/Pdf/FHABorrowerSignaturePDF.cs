using System;
using System.Collections.Specialized;
using System.Data;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CFHABorrowerSignaturePDF : AbstractLetterPDF
	{
        public override string Description 
        {
            get { return "FHA Borrower's Blanket Signature Authorization"; }
        }
        public override string PdfFile 
        {
            get { return "FHABorrowerSignature.pdf"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            // POINT use broker name from 1003 interviewer company name 
            
            IPreparerFields prep = dataLoan.GetPreparerOfForm( E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject );

            AddFormFieldData("LenderName", prep.CompanyName);

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
	}
}
