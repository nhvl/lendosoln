using System;
using System.Collections;
using System.Collections.Specialized;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CHUD_92900_B_1PDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-B_1.pdf"; }
        }
        
        public override string Description 
        {
            get { return "Page 1"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
        }

    }
    public class CHUD_92900_B_2PDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-B_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
        }

    }

    public class CHUD_92900_BPDF : AbstractBatchPDF 
    {
        public override string Description 
        {
            get { return "FHA Important Notice to Borrower (HUD-92900-B)"; }
        }
		public override bool OnlyPrintPrimary
		{
			get { return true; }
		}
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CHUD_92900_B_1PDF(),
                                                 new CHUD_92900_B_2PDF()
                                             };
            }
        }
    }
}
