namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Specialized;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Reminders;
    using LendersOffice.Admin;
    using LendersOffice.Security;
    using LendersOffice.ObjLib.Task;
    using System.Collections.Generic;

    public class CMortgageLoanCommitment_1PDF : AbstractLegalPDF
	{
        private BrokerDB m_brokerDB = null;

        private Boolean UseUnderwritingConditions
        {
            get
            {
                BrokerUserPrincipal brokerUser = System.Threading.Thread.CurrentPrincipal as BrokerUserPrincipal;

                // 12/2/2004 dd - This class can be call from NHC GetLoanForm webservice.
                // Therefore BrokerUserPrincipal can be null. As a result, just assume
                // this user doesn't have PriceMyLoan.
                if (null == brokerUser)
                    return false;

                // 2/8/2005 kb - PML is not the key.  We use lender default features now.

                if (m_brokerDB == null)
                {
                    try
                    {

                        m_brokerDB = BrokerDB.RetrieveById(brokerUser.BrokerId);
                    }
                    catch( Exception e )
                    {
                        Tools.LogError( "Failed to load broker access info." , e );
                    }
                }

                if (m_brokerDB != null && m_brokerDB.HasLenderDefaultFeatures == true)
                {
                    return true;
                }

                return false;
            }
        }

        public override string PdfFile 
        {
            get { return "MortgageLoanCommitment_1.pdf"; }
        }

        public override string Description 
        {
            get { return "Page 1"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageLoanCommitment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("MortgageLoanCommitment", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("MortgageLoanCommitmentPrepareDate", preparer.PrepareDate_rep);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageLoanCommitmentAlternateLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("MortgageLoanCommitmentAlternateLender", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));

            AddFormFieldData("sCommitReturnToFollowAddr", dataLoan.sCommitReturnToFollowAddr);
            AddFormFieldData("sCommitReturnToAboveAddr", dataLoan.sCommitReturnToAboveAddr);
            AddFormFieldData("sCommitReturnWithinDays", dataLoan.sCommitReturnWithinDays_rep);
            AddFormFieldData("sCommitTitleEvidence", dataLoan.sCommitTitleEvidence);
            AddFormFieldData("sCommitRepayTermsDesc", dataLoan.sCommitRepayTermsDesc);
            AddFormFieldData("sCommitExpD", dataLoan.sCommitExpD_rep);

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep + " / " + dataLoan.sDue_rep);
            AddFormFieldData("sLtvR", dataLoan.sLtvR_rep);
            AddFormFieldData("sCltvR", dataLoan.sCltvR_rep);

            StringBuilder sb = new StringBuilder();

            BrokerDB db = dataLoan.BrokerDB;
            if (db.IsUseNewTaskSystem)
            {
                List<Task> conditions = Task.GetActiveConditionsByLoanId(db.BrokerID, dataLoan.sLId, false);
                foreach (Task condition in conditions)
                {
                    if (condition.TaskStatus == E_TaskStatus.Closed)
                    {
                        continue;
                    }

                    sb.Append(condition.TaskSubject).Append(Environment.NewLine);
                }
            }
            else
            {
                if (UseUnderwritingConditions)
                {
                    // 10/31/06 mf. Since this is for read-only, I converted this to
                    // use CConditionSet instead of LoanConditionSet to reduce overhead.

                    CConditionSetObsolete conditionSet = new CConditionSetObsolete(dataLoan.sLId);

                    foreach (CConditionObsolete condition in conditionSet)
                    {
                        if (condition.IsDone == false) // OPM 24013 - Only show outstanding Conditions.
                            sb.Append(condition.CondDesc).Append(Environment.NewLine);
                    }

                }
                else
                {
                    int count = dataLoan.GetCondRecordCount();
                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete field = dataLoan.GetCondFieldsObsolete(i);
                        if (field.IsDone == false) // OPM 24013 - Only show outstanding Conditions.
                            sb.Append(field.CondDesc).Append(Environment.NewLine);
                    }
                }
            }
            AddFormFieldData("Conditions", sb.ToString());
            
        }
	}
    public class CMortgageLoanCommitment_2PDF : AbstractLegalPDF
    {

        public override string PdfFile 
        {
            get { return "MortgageLoanCommitment_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
        }
    }

    public class CMortgageLoanCommitmentPDF : AbstractBatchPDF
    {

        public override string Description 
        {
            get { return "Mortgage Loan Commitment"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/MortgageLoanCommitment.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CMortgageLoanCommitment_1PDF(),
                                                 new CMortgageLoanCommitment_2PDF()
                                             };
            }
        }

    }
}
