using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CRequestForTitlePDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "RequestForTitle.pdf"; }
        }
        
        public override string Description 
        {
            get { return "Request For Title"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/RequestForms/RequestForTitle.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparerField = dataLoan.GetPreparerOfForm(E_PreparerFormT.RequestOfTitle, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            string brokerAddress = string.Format("{7}{8}{0}{8}{1}{8}{2}, {3} {4}{8}Phone: {5} Fax: {6}",
                preparerField.CompanyName, preparerField.StreetAddr, preparerField.City, preparerField.State, preparerField.Zip,
                preparerField.Phone, preparerField.FaxNum, preparerField.PreparerName, Environment.NewLine);

            if (preparerField.EmailAddr != "")
                brokerAddress += Environment.NewLine + "Email: " + preparerField.EmailAddr;

            string address = String.Format("{0}{5}{1}, {2} {3}{5}County: {4}", dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip, dataLoan.sSpCounty, Environment.NewLine);

            if (dataApp.aBNm.TrimWhitespaceAndBOM() != "") 
            {
                string str = Tools.FormatAddress(dataApp.aBNm, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip) + Environment.NewLine;
                if (dataApp.aBSsn.TrimWhitespaceAndBOM() != "") 
                {
                    str += "SSN: " + dataApp.aBSsn + "   ";
                }
                if (dataApp.aBDob_rep.TrimWhitespaceAndBOM() != "") 
                {
                    str += "DOB: " + dataApp.aBDob_rep;
                }

                AddFormFieldData("BorrowerAddress", str);
            }
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                string str = Tools.FormatAddress(dataApp.aCNm, dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip) + Environment.NewLine;
                if (dataApp.aCSsn.TrimWhitespaceAndBOM() != "") 
                {
                    str += "SSN: " + dataApp.aCSsn + "   ";
                }
                if (dataApp.aCDob_rep.TrimWhitespaceAndBOM() != "") 
                {
                    str += "DOB: " + dataApp.aCDob_rep;
                }
                AddFormFieldData("CoborrowerAddress", str);
            }
            AddFormFieldData("LenderAddress", brokerAddress);
            AddFormFieldData("PropertyAddress", address);
            AddFormFieldData("sSpLegalDesc", dataLoan.sSpLegalDesc);
            AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
            AddFormFieldData("Title", preparerField.Title);
            AddFormFieldData("sSpT", dataLoan.sSpT);
            
            E_sLPurposeT sLPurposeT = dataLoan.sLPurposeT;
            if (sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT == E_sLPurposeT.VaIrrrl)
                sLPurposeT = E_sLPurposeT.Refin;
            if (sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                sLPurposeT = E_sLPurposeT.RefinCashout;
            }

            AddFormFieldData("sLPurposeT", sLPurposeT);

            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("aOccT", dataApp.aOccT);
            AddFormFieldData("sEstCloseD", dataLoan.sEstCloseD_rep);

            AddFormFieldData("sTitleReqPrepD", preparerField.PrepareDate_rep);
            AddFormFieldData("sTitleReqOwner", dataLoan.sTitleReqOwnerNm + " / " + dataLoan.sTitleReqOwnerPhone);
            CAgentFields title = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (title.IsValid) 
            {
                string str = title.AgentName + Environment.NewLine + Tools.FormatAddress(title.CompanyName, title.StreetAddr, title.City, title.State, title.Zip) + Environment.NewLine;
                if (title.Phone.TrimWhitespaceAndBOM() != "") 
                {
                    str += "Phone: " + title.Phone;
                }
                if (title.FaxNum.TrimWhitespaceAndBOM() != "") 
                {
                    str += " Fax: " + title.FaxNum;
                }
                if (title.CellPhone.TrimWhitespaceAndBOM() != "") 
                {
                    str += " Cell: " + title.CellPhone;
                }
                if (title.EmailAddr.TrimWhitespaceAndBOM() != "") 
                {
                    str += Environment.NewLine + "Email: " + title.EmailAddr;
                }
                    
                AddFormFieldData("TitleAddress", str);

            }

            CAgentFields seller = dataLoan.GetAgentOfRole(E_AgentRoleT.Seller, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (seller.IsValid) 
            {
                AddFormFieldData("SellerAddress", Tools.FormatAddress(seller.AgentName, seller.StreetAddr, seller.City, seller.State, seller.Zip));
            }
            
            CAgentFields mortgagee = dataLoan.GetAgentOfRole(E_AgentRoleT.Mortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (mortgagee.IsValid) 
            {
                AddFormFieldData("MortgageeAddress", mortgagee.AgentName + Environment.NewLine + Tools.FormatAddress(mortgagee.CompanyName, mortgagee.StreetAddr, mortgagee.City, mortgagee.State, mortgagee.Zip));
            }
            AddFormFieldData("sTitleReqPriorPolicy", dataLoan.sTitleReqPriorPolicy);
            AddFormFieldData("sTitleReqWarrantyDeed", dataLoan.sTitleReqWarrantyDeed);
            AddFormFieldData("sTitleReqInsRequirements", dataLoan.sTitleReqInsRequirements);
            AddFormFieldData("sTitleReqSurvey", dataLoan.sTitleReqSurvey);
            AddFormFieldData("sTitleReqContract", dataLoan.sTitleReqContract);
            AddFormFieldData("sTitleReqPolicyTypeDesc", dataLoan.sTitleReqPolicyTypeDesc);
            AddFormFieldData("sTitleReqMailAway", dataLoan.sTitleReqMailAway);
            AddFormFieldData("sTitleReqInstruction", dataLoan.sTitleReqInstruction);
        }
	}
}
