using System;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;


namespace LendersOffice.Pdf
{
    public class CWAMortgageDisclosurePDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "WAMortgageDisclosure.pdf"; }
        }
        public override string Description 
        {
            get { return "Washington State - Mortgage Broker Practices Act"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);

            // OPM 9909
            string brokerName = "";
            IPreparerFields agent = dataLoan.GetPreparerOfForm(E_PreparerFormT.BorrCertification, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.IsValid)
            {
                brokerName = agent.CompanyName;
            }

            AddFormFieldData("BrokerName", brokerName);
        }

    }
}
