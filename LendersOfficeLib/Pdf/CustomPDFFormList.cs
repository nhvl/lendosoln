using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Xml;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.PdfForm;

namespace LendersOffice.Pdf
{

	public class CustomPDFFormList : IPDFPrintItem
	{
        private Guid m_brokerID;
        private Guid m_loanID = Guid.Empty;
        private Guid m_applicationID = Guid.Empty;

        private NameValueCollection m_arguments;

        public string Name 
        {
            get { return "WordCustomForm"; }
        }

        public string ID 
        {
            get { return "WordCustomForm"; }
            set { }
        }

        public string Description 
        {
            get { return "Word Custom Forms"; }
        }
        public string EditLink
        {
            get { return ""; }
        }

        public string PreviewLink
        {
            get { return ""; }
        }

        private CPageData m_dataLoan = null;
        public CPageData DataLoan
        {
            get
            {
                if (m_dataLoan == null)
                {
                    m_dataLoan = new CPageData(m_loanID, "CustomPDFFormList", DependencyFields);
                    m_dataLoan.InitLoad();
                }
                return m_dataLoan;
            }
            set 
            {
                m_dataLoan = value;
                m_loanID = m_dataLoan.sLId;
            }
        }

        public NameValueCollection Arguments
        {
            get { return m_arguments; }
            set 
            {
                m_arguments = value;
                m_brokerID = GetSafeGuid(m_arguments["brokerid"]);
                m_loanID = GetSafeGuid(m_arguments["loanid"]);
                m_applicationID = GetSafeGuid(m_arguments["applicationid"]);
            }
        }

        private Guid GetSafeGuid(string v)
        {
            if (string.IsNullOrEmpty(v))
            {
                return Guid.Empty;
            }
            try
            {
                return new Guid(v);
            }
            catch (FormatException)
            {
            }
            catch (OverflowException) { }
            return Guid.Empty;
        }
        public bool IsVisible
        {
            get { return true; }
        }

        public bool HasPdf
        {
            get { return true; }
        }

        public void RenderPrintLink(StringBuilder sb) 
        {

            var list = PdfForm.PdfForm.RetrieveCustomFormsOfCurrentBroker();
            
            int nApps = DataLoan.nApps;

            foreach (var o in list)
            {
                string extra = string.Empty;

                string aBNm_aCNm = string.Empty;
                for (int i = 0; i < nApps; i++)
                {
                    if (nApps > 1)
                    {
                        // 9/28/2011 dd - Handle multiple apps OPM 69600.
                        CAppData dataApp = DataLoan.GetAppData(i);
                        extra = "appid='" + dataApp.aAppId + "'";
                        aBNm_aCNm = " - " + dataApp.aBNm_aCNm;
                    }
                    string id = "_" + o.FormId.ToString("N") + "_" + i;
                    sb.Append(string.Format(@"<tr><td><input id='{0}' name='{0}' type=checkbox pageSize='{3}' custompdfform='t' custompdfid='{2}' {4}></td><td>{1}{5} (<a href=""#"" onclick=""downloadCustomPdf('{0}', {3});"">preview</a>)</td></tr>"
                    , id // 0
                    , Utilities.SafeHtmlString(o.Description) // 1
                    , o.FormId // 2
                    , 0 // Letter size // 3
                    , extra // 4
                    , Utilities.SafeHtmlString(aBNm_aCNm) // 5
                    ));
                }

            }
        }


        #region IPDFPrintItem Members


        public virtual bool HasPrintPermission
        {
            get { return true; }
        }

        #endregion

        #region IPDFPrintItem Members


        public IEnumerable<string> DependencyFields
        {
            get
            {
                return new List<string>() 
            {
                "aBNm","aCNm"
            };
            }
        }

        #endregion
    }
}
