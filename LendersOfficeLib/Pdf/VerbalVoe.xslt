﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="VerbalVoe">
    <html>
      <head>

        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <style type="text/css" media="all">
          
          body
            {
            font-size: 20px;
            }
            
            div
            {
              margin-bottom: 15px;
            }
          .header
          {
          font-size:33px;
          height:50px;
          text-align:center;
          border-bottom:5px solid black;
          font-weight:bold;
          }

          .section-header
          {
          font-size:23px;
          text-align:center;
          border-top:solid 2px black;
          border-bottom:solid 2px black;
          margin-top:60px;
          font-weight: bold;
          }
          
          .separator
          {
            margin-top: 40px;
          }

          .left
          {
          float:left;
          width:49%;
          }

          .right
          {
          float:right;
          width: 49%;
          }

          .right + div
          {
          clear: both;
          }
          
      label
      {
        font-weight: bold;
      }
        </style>
      </head>
      <body>
        <div class="header">Verbal Verification of Employment</div>
        <div>

          <div class="left">
            <label>
              Borrower Name:
            </label>
            <xsl:value-of select="BorrowerName"/>
          </div>

          <div class="right">
            <label>
              Loan Number:
            </label>
            <xsl:value-of select="sLId"/>
          </div>
          <div style="height: 0px;">&#160;</div>
          <div class="section-header">Employer Information</div>
          <div>
            <label>
              Employer:
            </label>
            <xsl:value-of select="EmployerName"/>
          </div>
          <div>
            <label>
              Employer Address:
            </label>
            <xsl:value-of select="EmployerAddress"/>
          </div>
          <div class="separator">
            <label>
              Employer Phone Number:
            </label>
            <xsl:value-of select="EmployerPhone"/>
          </div>
          <div>
            <label>
              3rd Party Source for Phone Number:
            </label>
            <xsl:value-of select="ThirdPartySource"/>
          </div>

          <div class="section-header">Employment Verification</div>
          <div class="left" >
            <label>
              Date of Hire:
            </label>
            <xsl:value-of select="DateOfHire"/>
          </div>
          <div class="right">
            <label>
              Date of Termination:
            </label>
            <xsl:value-of select="DateOfTermination"/>
          </div>
          <div>
            <label>
              Borrower's Title Position:
            </label>
            <xsl:value-of select="BorrowerTitle"/>
          </div>
          <div>
            <label>
              Employment Status:
            </label>
            <xsl:value-of select="EmploymentStatus"/>
          </div>
          <div  class="separator">
            <label>
              Verified By:
            </label>
            <xsl:value-of select="EmployerContactName"/>
          </div>
          <div>
            <label>
              Title:
            </label>
            <xsl:value-of select="EmployerContactTitle"/>
          </div>

          <div class="section-header">Preparer Information</div>
          <div>
            <label>
              Prepared By:
            </label>
            <xsl:value-of select="PreparedBy"/>
          </div>
          <div>
            <label>
              Title:
            </label>
            <xsl:value-of select="PreparedByTitle"/>
          </div>
          <div  class="separator">
            <label>
              Date of Verification:
            </label>
            <xsl:value-of select="DateOfCall"/>
          </div>

        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>