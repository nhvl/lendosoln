﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CMassachusettsTangibleNetBenefitWorksheetPDF : CTangibleNetBenefitWorksheetPDF
    {
        public override string Description
        {
            get { return "Massachusetts Tangible Net Benefit Worksheet"; }
        }
    }
}