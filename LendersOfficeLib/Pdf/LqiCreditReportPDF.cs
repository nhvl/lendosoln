// <copyright file="LqiCreditReportPDF.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: jhairoe
//    Date:   07/12/2015 4:55:48 PM 
// </summary>
namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.CreditReport;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.PdfGenerator;

    /// <summary>
    /// LQI Credit Report PDF.
    /// </summary>
    public class LqiCreditReportPDF : IPDFGenerator, IPDFPrintItem
    {
        /// <summary>
        /// Name-value collection of arguments.
        /// </summary>
        private NameValueCollection arguments;

        /// <summary>
        /// The loan ID.
        /// </summary>
        private Guid loanID;

        /// <summary>
        /// The application ID.
        /// </summary>
        private Guid applicationID = Guid.Empty;

        /// <summary>
        /// Gets the page size.
        /// </summary>
        /// <value>The page size.</value>
        public PDFPageSize PageSize 
        {
            get { return PDFPageSize.Legal; }
        }

        /// <summary>
        /// Gets a value indicating whether <see cref="PdfFormLayout"/> has data.
        /// </summary>
        public bool HasPdfFormLayout => false;

        /// <summary>
        /// Gets the layout of the form data applied to the current form.
        /// </summary>
        public LendersOffice.PdfLayout.PdfFormLayout PdfFormLayout
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets or sets the PDF name.
        /// </summary>
        /// <value>The PDF name.</value>
        public string PdfName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the PDF Name.
        /// </summary>
        /// <value>The PDF Name.</value>
        public string Name
        {
            get { return "LqiCreditReport"; }
        }

        /// <summary>
        /// Gets or sets the string ID.
        /// </summary>
        /// <value>String ID.</value>
        public string ID
        {
            get
            {
                return null;
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The Description.</value>
        public string Description
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the edit link.
        /// </summary>
        /// <value>Edit Link.</value>
        public string EditLink
        {
            get { return null; }
        }

        /// <summary>
        /// Gets the preview link.
        /// </summary>
        /// <value>Preview Link.</value>
        public string PreviewLink
        {
            get { return null; }
        }

        /// <summary>
        /// Gets or sets the loan data.
        /// </summary>
        /// <value>Loan data.</value>
        public DataAccess.CPageData DataLoan
        {
            get { return null; }
            set { }
        }

        /// <summary>
        ///  Gets or sets the arguments.
        /// </summary>
        /// <value>The Arguments.</value>
        public System.Collections.Specialized.NameValueCollection Arguments
        {
            get
            {
                return this.arguments;
            }

            set
            {
                this.arguments = value; 

                if (!string.IsNullOrEmpty(this.arguments["loanid"])) 
                {
                    this.loanID = new Guid(this.arguments["loanid"]);
                }

                if (!string.IsNullOrEmpty(this.arguments["applicationid"])) 
                {
                    this.applicationID = new Guid(this.arguments["applicationid"]);
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this object is visible.
        /// </summary>
        /// <value>Whether this object is visible.</value>
        public bool IsVisible
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether this object has a PDF.
        /// </summary>
        /// <value>Whether this object has a PDF.</value>
        public bool HasPdf
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating whether this object has print permission.
        /// </summary>
        /// <value>Whether this object has print permission.</value>
        public virtual bool HasPrintPermission
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the dependency fields.
        /// </summary>
        /// <value>The dependency fields.</value>
        public IEnumerable<string> DependencyFields
        {
            get { return new List<string>(); }
        }

        /// <summary>
        /// Render Print Link.
        /// </summary>
        /// <param name="sb">String builder.</param>
        public void RenderPrintLink(System.Text.StringBuilder sb)
        {
        }

        /// <summary>
        /// Save PDF to file.
        /// </summary>
        /// <param name="outputFileName">Name of file to save PDF to.</param>
        /// <param name="ownerPassword">Owner password.</param>
        /// <param name="userPassword">User password.</param>
        public void GeneratePDFInFile(string outputFileName, string ownerPassword, string userPassword)
        {
            byte[] buffer = this.GeneratePDF(ownerPassword, userPassword);
            BinaryFileHelper.WriteAllBytes(outputFileName, buffer);
        }

        /// <summary>
        /// Output PDF to stream.
        /// </summary>
        /// <param name="outputStream">Output stream.</param>
        /// <param name="ownerPassword">Owner password.</param>
        /// <param name="userPassword">User Password.</param>
        public void GeneratePDF(Stream outputStream, string ownerPassword, string userPassword)
        {
            byte[] buffer = this.GeneratePDF(ownerPassword, userPassword);
            outputStream.Write(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// Generates PDF.
        /// </summary>
        /// <param name="ownerPassword">Owner password.</param>
        /// <param name="userPassword">User Password.</param>
        /// <remarks>Return null if unable to pull credit report in PDF format.</remarks>
        /// <returns>PDF as byte array.</returns>
        public byte[] GeneratePDF(string ownerPassword, string userPassword)
        {
            byte[] buffer = null;
            CPageData dataLoan = new CCreditReportViewData(this.loanID);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(this.applicationID);

            ICreditReportView creditReportView = dataApp.LqiCreditReportView.Value;

            if (creditReportView != null && creditReportView.HasPdf)
            {
                buffer = Convert.FromBase64String(creditReportView.PdfContent);
            }

            return buffer;
        }
    }
}
