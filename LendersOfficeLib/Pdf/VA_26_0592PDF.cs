using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CVA_26_0592PDF : AbstractLetterPDF
	{

        public override string Description 
        {
            get { return "VA Counseling Checklist (VA 26-0592)"; }
        }
        public override string PdfFile 
        {
            get { return "VA-26-0592.pdf"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
        }
	}
}
