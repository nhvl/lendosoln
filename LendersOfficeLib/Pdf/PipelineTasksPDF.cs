﻿// <summary>
// Author: Eric Mallare
// </summary>
namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using System.Xml.Xsl;
    using DataAccess;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.PdfGenerator;
    using LendersOffice.Security;

    /// <summary>
    /// Generate PDF for all tasks/conditions in pipeline.
    /// </summary>
    public class CPipelineTasksPDF : AbstractXsltPdf
    {
        /// <summary>
        /// Holds map from category id to category name.
        /// </summary>
        private Dictionary<int, ConditionCategory> conditionCategories;

        /// <summary>
        /// Returns description of the PDF.
        /// </summary>
        /// <value>
        /// Will return "Pipeline Tasks".
        /// </value>
        public override string Description
        {
            get { return "Pipeline Tasks"; }
        }

        /// <summary>
        /// Indicate whether the paper size of this PDF.
        /// </summary>
        /// <value>
        /// Will always return PDFPageSize.Letter.
        /// </value>
        public override PDFPageSize PageSize
        {
            get { return PDFPageSize.Letter; }
        }

        /// <summary>
        /// Checks if the PDF will use loan data.
        /// </summary>
        /// <value>
        /// False since it uses multiple loans. 
        /// </value>
        protected override bool UsesDataLoan
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Override Xslt argument list.
        /// </summary>
        /// <value>
        /// Will return XsltArgumentList with LogoSource as parameter.
        /// </value>
        protected override XsltArgumentList XsltParams
        {
            get
            {
                string m_pmlLenderSiteId = this.GetPmlLenderSiteId();

                XsltArgumentList args = new XsltArgumentList();
                if (FileDBTools.DoesFileExist(E_FileDB.Normal, m_pmlLenderSiteId.ToLower() + ".logo.gif"))
                {
                    args.AddParam("LogoSource", string.Empty, "LogoPL.aspx?id=" + m_pmlLenderSiteId);
                }

                return args;
            }
        }

        /// <summary>
        /// Gets Broker Name from current user.
        /// </summary>
        /// <returns>Broker name.</returns>
        private string GetBrokerName
        {
            get
            {
                AbstractUserPrincipal p = PrincipalFactory.CurrentPrincipal;
                return p.BrokerDB.Name;
            }
        }

        /// <summary>
        /// Generates the XML data.
        /// </summary>
        /// <param name="writer">The XML writer.</param>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="dataApp">The app data.</param>
        protected override void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp)
        {
            writer.WriteStartElement("TASKLIST");

            List<Task> tasks = null;

            string loanCacheId = Arguments["loanCacheId"];
            string tasksCacheId = Arguments["tasksCacheId"];
            string isCondition = Arguments["conditionmode"];

            bool needConditionCategories = !string.IsNullOrEmpty(isCondition) && isCondition.Equals("true", StringComparison.OrdinalIgnoreCase);

            if (needConditionCategories)
            {
                writer.WriteAttributeString("Heading", "Condition");
                this.PdfName = "ConditionList";
            }
            else
            {
                writer.WriteAttributeString("Heading", "Task");
                this.PdfName = "TaskList";
            }

            writer.WriteAttributeString("BrokerName", this.GetBrokerName);

            if (string.IsNullOrEmpty(loanCacheId) || string.IsNullOrEmpty(tasksCacheId))
            {
                // No cache id = no go
                throw CBaseException.GenericException("Cache ids were null or empty");
            }
            else
            {
                tasks = this.GetTasksUsingCacheId(loanCacheId, tasksCacheId);
            }

            if (needConditionCategories)
            {
                this.conditionCategories = ConditionCategory.GetCategories(PrincipalFactory.CurrentPrincipal.BrokerId).ToDictionary(p => p.Id);
            }
            else
            {
                this.conditionCategories = new Dictionary<int, ConditionCategory>();
            }

            foreach (var task in tasks)
            {
                this.WriteTaskInformation(needConditionCategories, writer, dataLoan, task);
            }

            writer.WriteEndElement(); // </TASKLIST>
        }

        /// <summary>
        /// Get all tasks accessible to an employee for a single loan.
        /// </summary>
        /// <param name="loanId">The loan to get tasks for.</param>
        /// <param name="currentTasks">Running list of tasks to process.</param>
        private void GetAllTasksByEmployeeAccess(string loanId, List<Task> currentTasks)
        {
            List<Task> allTasks = new List<Task>();
            if (HttpContext.Current == null)
            {
                var principal = PrincipalFactory.CurrentPrincipal;
                allTasks = Task.GetTasksByEmployeeAccess(principal.BrokerId, new Guid(loanId), principal.UserId);
            }
            else if (HttpContext.Current.User is BrokerUserPrincipal)
            {
                allTasks = Task.GetTasksByEmployeeAccess(BrokerUserPrincipal.CurrentPrincipal.BrokerId, new Guid(loanId), BrokerUserPrincipal.CurrentPrincipal.EmployeeId);
            }
            else
            {
                AbstractUserPrincipal p = (AbstractUserPrincipal)HttpContext.Current.User;
                allTasks = Task.GetTasksByEmployeeAccess(p.BrokerId, new Guid(loanId), p.UserId);
            }

            currentTasks.AddRange(allTasks);
        }

        /// <summary>
        /// Get all tasks for all loans according to task ids stored in the cache.
        /// </summary>
        /// <param name="loanCacheId">Id of cache holding loan ids.</param>
        /// <param name="tasksCacheId">Id of cache holding task ids.</param>
        /// <returns>Returns list of all tasks.</returns>
        private List<Task> GetTasksUsingCacheId(string loanCacheId, string tasksCacheId)
        {
            List<Task> allTasks = new List<Task>();

            string taskIdsRaw = AutoExpiredTextCache.GetFromCache(tasksCacheId);
            if (taskIdsRaw == null)
            {
                throw CBaseException.GenericException("The task IDs are no longer in the cache");
            }

            string loanIdsRaw = AutoExpiredTextCache.GetFromCache(loanCacheId);
            if (loanIdsRaw == null)
            {
                throw CBaseException.GenericException("The loan IDs are no longer in the cache");
            }

            string[] loanIds = loanIdsRaw.Split(',');

            foreach (string id in loanIds)
            {
                this.GetAllTasksByEmployeeAccess(id, allTasks);
            }

            allTasks.Sort((t1, t2) => t1.TaskId.CompareTo(t2.TaskId));

            List<Task> resultTasks = new List<Task>();
            string[] taskIDs = taskIdsRaw.Split(',');

            // O(m log n), where m is num tasks to get, n is num total tasks
            foreach (string taskID in taskIDs)
            {
                resultTasks.Add(allTasks.Find((t) => t.TaskId == taskID));
            }

            return resultTasks;
        }

        /// <summary>
        /// Writes task information into writer. 
        /// </summary>
        /// <param name="includeCategory">If category is required or not.</param>
        /// <param name="writer">The XML writer.</param>
        /// <param name="dataLoan">The loan data. Null.</param>
        /// <param name="task">The current task to write out.</param>
        private void WriteTaskInformation(bool includeCategory, XmlWriter writer, CPageData dataLoan, Task task)
        {
            writer.WriteStartElement("Task");
            writer.WriteElementString("TaskID", task.TaskId.ToString());
            writer.WriteElementString("Subject", task.TaskSubject.ToString());
            writer.WriteElementString("LoanNumber", task.LoanNumCached);

            // convert these dates to short
            string dueDate = string.Empty;
            string followupDate = string.Empty;
            string isStyleDueDate = "0"; // 0 for false, 1 for true
            string isStyleFollowupDate = "0";
            if (task.TaskDueDate != null)
            {
                DateTime date = (DateTime)task.TaskDueDate;
                dueDate = date.ToString("MM/dd/yyyy");
                if (date.Date <= DateTime.Now.Date)
                {
                    isStyleDueDate = "1";
                }
            }
            else
            {
                dueDate = task.TaskSingleDueDate;
            }

            if (task.TaskFollowUpDate != null)
            {
                DateTime date = (DateTime)task.TaskFollowUpDate;
                followupDate = date.ToString("MM/dd/yyyy");
                if (date.Date <= DateTime.Now.Date)
                {
                    isStyleFollowupDate = "1";
                }
            }

            writer.WriteElementString("DueDate", dueDate);
            writer.WriteElementString("isStyleDueDate", isStyleDueDate);
            writer.WriteElementString("FollowupDate", followupDate);
            writer.WriteElementString("isStyleFollowupDate", isStyleFollowupDate);
            if (includeCategory)
            {
                if (task.TaskIsCondition && task.CondCategoryId.HasValue && this.conditionCategories.ContainsKey(task.CondCategoryId.Value))
                {
                    writer.WriteElementString("Category", this.conditionCategories[task.CondCategoryId.Value].Category);
                }
                else
                {
                    writer.WriteElementString("Category", string.Empty);
                }
            }

            writer.WriteEndElement(); // </Task>
        }

        /// <summary>
        /// Retrieve PMLLenderSiteId based on loanId.
        /// </summary>
        /// <returns>PML Lender site id.</returns>
        private string GetPmlLenderSiteId()
        {
            AbstractUserPrincipal p = PrincipalFactory.CurrentPrincipal;
            return Tools.GetFileDBKeyForPmlLogoWithLoanId(p.BrokerDB.PmlSiteID, Guid.Empty);
        }
    }
}
