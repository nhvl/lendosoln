namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Common;

    public class CLockInConfirmationPDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "LockInConfirmation.pdf"; }
        }
        public override string Description 
        {
            get { return "Lock-In Confirmation"; }
        }
        
        public override string EditLink 
        {
            get { return "/newlos/LockInConfirmation.aspx"; }
        }


        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sLT", dataLoan.sLT_rep);

			string sLAmt = dataLoan.sLAmtCalc_rep;
			if (sLAmt.Length > 0) sLAmt = "$" + sLAmt;
            AddFormFieldData("sLAmt", sLAmt);

			AddFormFieldData("sLpTemplateNm", dataLoan.sLpTemplateNm);
            AddFormFieldData("TermDue", dataLoan.sTerm_rep + " / " + dataLoan.sDue_rep);

			string sNoteIR = dataLoan.sNoteIR_rep;
			if (sNoteIR.Length > 0) sNoteIR = sNoteIR + "%";
			AddFormFieldData("sNoteIR", sNoteIR);
            
			AddFormFieldData("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep);
            AddFormFieldData("sLOrigFPc", dataLoan.sLOrigFPc_rep);
            AddFormFieldData("sLOrigFMb", dataLoan.sLOrigFMb_rep);
            AddFormFieldData("sLOrigF", dataLoan.sLOrigF_rep);
            AddFormFieldData("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            AddFormFieldData("sLDiscntFMb", dataLoan.sLDiscntFMb_rep);
            AddFormFieldData("sLDiscnt", dataLoan.sLDiscnt_rep);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.LockInConfirmation, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string phone = "";

            if (preparer.PhoneOfCompany.TrimWhitespaceAndBOM() != "") 
            {
                phone = "Phone: " + preparer.PhoneOfCompany;
            }
            AddFormFieldData("LenderAddress", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, phone));
            AddFormFieldData("PreparedDate", preparer.PrepareDate_rep);
            AddFormFieldData("CompanyName", preparer.CompanyName);
        }

	}
}
