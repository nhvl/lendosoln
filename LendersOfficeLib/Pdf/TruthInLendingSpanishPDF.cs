using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.PdfGenerator;

namespace LendersOffice.Pdf
{

	public class CTruthInLendingSpanishPDF : CTruthInLendingPDF
	{
        public override string PdfFile 
        {
            get { return "TruthInLendingSpanish.pdf"; }
        } 

        public override string Description 
        {
            get { return "Truth In Lending (Spanish Version)"; }
        }
        public override string EstimateSymbol 
        {
            get { return "e "; }
        }

	}
}
