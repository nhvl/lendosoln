using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.PdfGenerator;

namespace LendersOffice.Pdf
{
	public class CBrokerConfirmationPDF : AbstractLegalPDF
	{
        public override string Description 
        {
            get { return "Broker Confirmation"; }
        }
        public override string PdfFile 
        {
            get { return "BrokerConfirmation.pdf"; }
        }
	    

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string aBNm_aCNm = dataApp.aBNm;
            if (dataApp.aCNm != "")
                aBNm_aCNm += " & " + dataApp.aCNm;
            AddFormFieldData("aBNm_aCNm", aBNm_aCNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("sSpAddr_Multiline", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));

			string sTerm = dataLoan.sTerm_rep;
			if (sTerm.Length > 0)
				sTerm += " MONTHS";
			AddFormFieldData("sTerm", sTerm);

			AddFormFieldData("sProdDocT", dataLoan.sProdDocT.ToString());

			string sNoteIR = dataLoan.sNoteIR_rep;
			if (sNoteIR.Length > 0)
				sNoteIR += " %";
			AddFormFieldData("sNoteIR", sNoteIR);

			string sFinalLAmt = dataLoan.sFinalLAmt_rep;
			if (sFinalLAmt.Length > 0)
				sFinalLAmt = "$ " + sFinalLAmt;
			AddFormFieldData("sFinalLAmt", sFinalLAmt);

			string sCltvR = dataLoan.sCltvR_rep;
			if (sCltvR.Length > 0)
				sCltvR += " %";
			AddFormFieldData("sCltvR", sCltvR);

			string sLtvR = dataLoan.sLtvR_rep;
			if (sLtvR.Length > 0)
				sLtvR += " %";
			AddFormFieldData("sLtvR", sLtvR);

			string sRAdjMarginR = dataLoan.sRAdjMarginR_rep;
			if (sRAdjMarginR.Length > 0)
				sRAdjMarginR += " %";
			AddFormFieldData("sRAdjMarginR", sRAdjMarginR);

			string sRealETxRsrvMon = dataLoan.sRealETxRsrvMon_rep;
			if (sRealETxRsrvMon.Length > 0)
				sRealETxRsrvMon += " MONTHS";
			AddFormFieldData("sRealETxRsrvMon", sRealETxRsrvMon);

			string sHazInsRsrvMon = dataLoan.sHazInsRsrvMon_rep;
			if (sHazInsRsrvMon.Length > 0)
				sHazInsRsrvMon += " MONTHS";
			AddFormFieldData("sHazInsRsrvMon", sHazInsRsrvMon);

			AddFormFieldData("sPpmtPenaltyMon", dataLoan.sPpmtPenaltyMon_rep);
            AddFormFieldData("sLPurposeT_rep", dataLoan.sLPurposeT_rep);

			string sQualBottomR = dataLoan.sQualBottomR_rep;
			if (sQualBottomR.Length > 0)
				sQualBottomR += " %";
			AddFormFieldData("sQualBottomR", sQualBottomR);

			AddFormFieldData("sLpTemplateNm", dataLoan.sLpTemplateNm);

            string desc = "";
            switch (dataApp.aOccT) 
            {
                case E_aOccT.PrimaryResidence: desc = "Primary Residence"; break;
                case E_aOccT.SecondaryResidence: desc = "Secondary Residence"; break;
                case E_aOccT.Investment: desc = "Investment"; break;
            }
            AddFormFieldData("aOccT_rep", desc);
            AddFormFieldData("sSchedDueD1", dataLoan.sSchedDueD1_rep);
            AddFormFieldData("sBrokComp1", dataLoan.sBrokComp1_rep);
            AddFormFieldData("sLDiscnt", dataLoan.sLDiscnt_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sLDiscntProps)) 
            {
                AddFormFieldData("sLDiscntProps", "Broker");
            }
            AddFormFieldData("sMBrokF", dataLoan.sMBrokF_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sMBrokFProps)) 
            {
                AddFormFieldData("sMBrokFProps", "Broker");
            }
            AddFormFieldData("sProcF", dataLoan.sProcF_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sProcFProps)) 
            {
                AddFormFieldData("sProcFProps", "Broker");
            }
            AddFormFieldData("sUwF", dataLoan.sUwF_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sUwFProps)) 
            {
                AddFormFieldData("sUwFProps", "Broker");
            }
            AddFormFieldData("s800U1F", dataLoan.s800U1F_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U1FProps)) 
            {
                AddFormFieldData("s800U1FProps", "Broker");
            }
            AddFormFieldData("s800U2F", dataLoan.s800U2F_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U2FProps)) 
            {
                AddFormFieldData("s800U2FProps", "Broker");
            }
            AddFormFieldData("s800U3F", dataLoan.s800U3F_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U3FProps)) 
            {
                AddFormFieldData("s800U3FProps", "Broker");
            }
            AddFormFieldData("s800U4F", dataLoan.s800U4F_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U4FProps)) 
            {
                AddFormFieldData("s800U4FProps", "Broker");
            }
            AddFormFieldData("s800U5F", dataLoan.s800U5F_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U5FProps)) 
            {
                AddFormFieldData("s800U5FProps", "Broker");
            }

            AddFormFieldData("s800U1FDesc", dataLoan.s800U1FDesc);
            AddFormFieldData("s800U2FDesc", dataLoan.s800U2FDesc);
            AddFormFieldData("s800U3FDesc", dataLoan.s800U3FDesc);
            AddFormFieldData("s800U4FDesc", dataLoan.s800U4FDesc);
            AddFormFieldData("s800U5FDesc", dataLoan.s800U5FDesc);
            AddFormFieldData("sU1ScDesc", dataLoan.sU1ScDesc);
            AddFormFieldData("sU1Sc", dataLoan.sU1Sc_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU1ScProps)) 
            {
                AddFormFieldData("sU1ScProps", "Broker");
            }

            AddFormFieldData("sApprF", dataLoan.sApprF_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sApprFProps)) 
            {
                AddFormFieldData("sApprFProps", "Broker");
            }
            AddFormFieldData("sCrF", dataLoan.sCrF_rep);
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sCrFProps)) 
            {
                AddFormFieldData("sCrFProps", "Broker");
            }
			AddFormFieldData("sLOrigF", dataLoan.sLOrigF_rep);
			if (LosConvert.GfeItemProps_ToBr(dataLoan.sLOrigFProps))
			{
				AddFormFieldData("sLOrigFProps", "Broker");
			}

			string sTransNetCash = dataLoan.sTransNetCash_rep;
			if (sTransNetCash.Length > 0)
			{
				if (sTransNetCash.StartsWith("(")) // negative amount
					sTransNetCash = sTransNetCash.Substring(0,1) + "$ " + sTransNetCash.Substring(1); // place '$' inside parentheses
				else
					sTransNetCash = "$ " + sTransNetCash;
			}
			AddFormFieldData("sTransNetCash", sTransNetCash);

            int count = dataApp.aLiaCollection.CountRegular;

            for (int i = 0; i < count && i < 14; i++) 
            {
                ILiabilityRegular field = dataApp.aLiaCollection.GetRegularRecordAt(i);
                AddFormFieldData("Lia" + i + "_ComNm", field.ComNm);
                AddFormFieldData("Lia" + i + "_Bal", field.Bal_rep);
                AddFormFieldData("Lia" + i + "_WillBePdOff", field.WillBePdOff ? "Yes" : "No");
            }

            CAgentFields agent = null;

            agent = dataLoan.GetAgentOfRole( E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("Title_AgentName", agent.AgentName);
            AddFormFieldData("Title_Phone", agent.Phone);
            AddFormFieldData("Title_Fax", agent.FaxNum);
            AddFormFieldData("Title_PhoneOfCompany", agent.PhoneOfCompany);
            AddFormFieldData("Title_FaxOfCompany", agent.FaxOfCompany);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("Processor_AgentName", agent.AgentName);
            AddFormFieldData("Processor_CompanyName", agent.CompanyName);
            AddFormFieldData("Processor_PhoneOfCompany", agent.PhoneOfCompany);
            AddFormFieldData("Processor_FaxOfCompany", agent.FaxOfCompany);
        }
	}
}
