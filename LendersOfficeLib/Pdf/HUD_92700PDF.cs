using System;
using System.Collections;
using System.Collections.Specialized;

using DataAccess;
using LendersOffice.Common;


namespace LendersOffice.Pdf
{
	public abstract class CAbstractHUD_92700PDF : AbstractLetterPDF
	{        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            bool isPurchase = false;
            bool isRefinance = false;

            switch (dataLoan.sLPurposeT)
            {
                case E_sLPurposeT.Purchase:
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                case E_sLPurposeT.Other:
                    isPurchase = true;
                    break;
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.HomeEquity:
                    isRefinance = true;
                    break;
                default:
                    throw new UnhandledEnumException(dataLoan.sLPurposeT);
            }

            // Page 1
            string name = dataApp.aBNm;
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                name += " & " + dataApp.aCNm;
            }

            name += Environment.NewLine + Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip);

            AddFormFieldData("BorrowerInfo", name);

            E_sLPurposeT sLPurposeT = dataLoan.sLPurposeT;
            if (sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT == E_sLPurposeT.VaIrrrl)
                sLPurposeT = E_sLPurposeT.Refin;
            AddFormFieldData("sLPurposeT", sLPurposeT);

            AddFormFieldData("sUnitsNum", dataLoan.sUnitsNum_rep);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFHA203kSpHudOwnedTri", dataLoan.sFHA203kSpHudOwnedTri);
            AddFormFieldData("sFHAIsNonProfit", dataLoan.sFHAIsNonProfit);
            AddFormFieldData("aOccT", dataApp.aOccT);
            AddFormFieldData("sFHAIsGovAgency", dataLoan.sFHAIsGovAgency);
            AddFormFieldData("sFHAExistingMLien", dataLoan.sFHAExistingMLien_rep);
            AddFormFieldData("sFHASpAsIsVal", dataLoan.sFHASpAsIsVal_rep);
            AddFormFieldData("sFHASpAfterImprovedVal110pc", dataLoan.sFHASpAfterImprovedVal110pc_rep);
            AddFormFieldData("sFHASpAfterImprovedVal", dataLoan.sFHASpAfterImprovedVal_rep);
            AddFormFieldData("sFHA203kBorPdCc", dataLoan.sFHA203kBorPdCc_rep);
            AddFormFieldData("sFHA203kAllowEnergyImprovement", dataLoan.sFHA203kAllowEnergyImprovement_rep);
            AddFormFieldData("sFHAIsExistingDebt", dataLoan.sFHAIsExistingDebt);
            AddFormFieldData("sFHA203kRepairCost", dataLoan.sFHA203kRepairCost_rep);
            AddFormFieldData("sFHA203kRepairCostReserveR", dataLoan.sFHA203kRepairCostReserveR_rep);
            AddFormFieldData("sFHA203kRepairCostReserve", dataLoan.sFHA203kRepairCostReserve_rep);
            AddFormFieldData("sFHA203kTitleDrawCount", dataLoan.sFHA203kTitleDrawCount_rep);
            AddFormFieldData("sFHA203kInspectCount", dataLoan.sFHA203kInspectCount_rep);
            AddFormFieldData("sFHA203kCostPerTitleDraw", dataLoan.sFHA203kCostPerTitleDraw_rep);
            AddFormFieldData("sFHA203kCostPerInspect", dataLoan.sFHA203kCostPerInspect_rep);
            AddFormFieldData("sFHA203kInspectAndTitleFee", dataLoan.sFHA203kInspectAndTitleFee_rep);
            AddFormFieldData("sFHA203kPmtEscrowed", dataLoan.sFHA203kPmtEscrowed_rep);
            AddFormFieldData("sFHA203kPmtEscrowedMonths", dataLoan.sFHA203kPmtEscrowedMonths_rep);
            AddFormFieldData("sFHA203kMonhtlyPmtEscrowed", dataLoan.sFHA203kMonhtlyPmtEscrowed_rep);
            AddFormFieldData("sFHA203kRehabEscrowAccountSubtot", dataLoan.sFHA203kRehabEscrowAccountSubtot_rep);
            AddFormFieldData("sFHA203kEngineeringFee", dataLoan.sFHA203kEngineeringFee_rep);
            AddFormFieldData("sFHA203kConsultantFee", dataLoan.sFHA203kConsultantFee_rep);
            AddFormFieldData("sFHA203kOtherFee", dataLoan.sFHA203kOtherFee_rep);
            AddFormFieldData("sFHA203kPlanReviewerFee", dataLoan.sFHA203kPlanReviewerFee_rep);
            AddFormFieldData("sFHA203kB5toB9Subtot", dataLoan.sFHA203kB5toB9Subtot_rep);
            AddFormFieldData("sFHA203kSupplementOrigFee", dataLoan.sFHA203kSupplementOrigFee_rep);
            AddFormFieldData("sFHA203kRepairDiscount", dataLoan.sFHA203kRepairDiscount_rep);
            AddFormFieldData("sFHA203kRepairDiscountFeePt", dataLoan.sFHA203kRepairDiscountFeePt_rep);
            AddFormFieldData("sFHA203kReleaseAtClosingSubtot", dataLoan.sFHA203kReleaseAtClosingSubtot_rep);
            
            AddFormFieldData("sFHA203kRehabCostTot", dataLoan.sFHA203kRehabCostTot_rep);

            AddFormFieldData("sFHA203kC1PlusC2", dataLoan.sFHA203kC1PlusC2_rep);
            AddFormFieldData("sFHA203kC5Part1", dataLoan.sFHA203kC5Part1_rep);
            AddFormFieldData("sFHA203kC5Part2", dataLoan.sFHA203kC5Part2_rep);
            AddFormFieldData("sFHA203kActualCashInvRequired", dataLoan.sFHA203kActualCashInvRequired_rep);
            AddFormFieldData("sFHA203kFHAMipRefund", dataLoan.sFHA203kFHAMipRefund_rep);
            AddFormFieldData("sFHA203kA2PlusB14", dataLoan.sFHA203kA2PlusB14_rep);
            if (isPurchase)
            {
                AddFormFieldData("purchase_sFHA203kSalesPriceOrAsIsVal", dataLoan.sFHA203kSalesPriceOrAsIsVal_rep);
                AddFormFieldData("purchase_sFHA203kRehabCostTot", dataLoan.sFHA203kRehabCostTot_rep);
                AddFormFieldData("purchase_sFHA203kC1PlusC2OrA4", dataLoan.sFHA203kC1PlusC2OrA4_rep);
                AddFormFieldData("purchase_sFHA203kInvestmentRequired", dataLoan.sFHA203kInvestmentRequired_rep);
                AddFormFieldData("purchase_sFHA203kAdjMaxMAmt", dataLoan.sFHA203kAdjMaxMAmt_rep);
                AddFormFieldData("purchase_sFHA203kMaxMAmtC5", dataLoan.sFHA203kMaxMAmtC5_rep);
            }
            if (isRefinance)
            {
                AddFormFieldData("refinance_sFHA203kBorrRequiredInv", dataLoan.sFHA203kBorrRequiredInv_rep);
                AddFormFieldData("refinance_sFHA203kD4", dataLoan.sFHA203kD4_rep);
                AddFormFieldData("refinance_sFHA203kMaxMAmtD5", dataLoan.sFHA203kMaxMAmtD5_rep);
                AddFormFieldData("refinance_sFHA203kD2", dataLoan.sFHA203kD2_rep);
                AddFormFieldData("refinance_sFHA203kD1", dataLoan.sFHA203kD1_rep);
            }
            AddFormFieldData("sFHA203kD2", dataLoan.sFHA203kD2_rep);
            AddFormFieldData("sFHA203kEnergyEfficientMAmt", dataLoan.sFHA203kEnergyEfficientMAmt_rep);

            if (sLPurposeT == E_sLPurposeT.Purchase ||
                sLPurposeT == E_sLPurposeT.Construct ||
                sLPurposeT == E_sLPurposeT.ConstructPerm ||
                sLPurposeT == E_sLPurposeT.Other)
            {
                if (dataLoan.sFHA203kAllowEnergyImprovement > 0)
                {
                    AddFormFieldData("TotalMortgageAmountWithUFMIP", dataLoan.sFHA203kEnergyEfficientMAmtWithUfmip_rep);
                }
                else
                {
                    AddFormFieldData("TotalMortgageAmountWithUFMIP", dataLoan.sFHA203kMaxMAmtC5WithUfmip_rep);
                }
            }
            else
            {
                AddFormFieldData("TotalMortgageAmountWithUFMIP", dataLoan.sFHA203kMaxMAmtD5WithUfmip_rep);
            }

            AddFormFieldData("sFfUfmipR", dataLoan.sFfUfmipR_rep);

            AddFormFieldData("sFHA203kRemarks", dataLoan.sFHA203kRemarks);
            AddFormFieldData("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            AddFormFieldData("sFHA203kEscrowedFundsTot", dataLoan.sFHA203kEscrowedFundsTot_rep);

            //AddFormFieldData("aBSignature", dataLoan.aBSignature);
            //AddFormFieldData("aCSignature", dataApp.aCSignature);

            // Page 2

            if (isRefinance)
            {
                AddFormFieldData("refinance_sFHAExistingMLien", dataLoan.sFHAExistingMLien_rep);
                AddFormFieldData("refinance_sFHAImprovements", dataLoan.sFHAImprovements_rep);
                AddFormFieldData("refinance_sFHACcPbb", dataLoan.sFHACcPbb_rep);
                AddFormFieldData("refinance_sFHASalesConcessions", dataLoan.sFHASalesConcessions_rep);
                AddFormFieldData("refinance_sFHA203kD1", dataLoan.sFHA203kD1_rep);
                AddFormFieldData("refinance_sLAmt", dataLoan.sLAmtCalc_rep);
                AddFormFieldData("refinance_sFHA203kBorrRequiredInv", dataLoan.sFHA203kBorrRequiredInv_rep);
                AddFormFieldData("refinance_sFHADiscountPoints", dataLoan.sFHADiscountPoints_rep);
                AddFormFieldData("refinance_sFHAPrepaidExp", dataLoan.sFHAPrepaidExp_rep);
                AddFormFieldData("refinance_sUfCashPd", dataLoan.sUfCashPd_rep);
                AddFormFieldData("refinance_sFHANonrealty", dataLoan.sFHANonrealty_rep);
                AddFormFieldData("refinance_sFHAReqTot", dataLoan.sFHAReqTot_rep);
                AddFormFieldData("refinance_sFHAAmtPaid", dataLoan.sFHAAmtPaid_rep);
                AddFormFieldData("refinance_sFHAAmtToBePd", dataLoan.sFHAAmtToBePd_rep);
                AddFormFieldData("refinance_sFHAAssetAvail", dataLoan.sFHAAssetAvail_rep);
                AddFormFieldData("refinance_sFHA2ndMAmt", dataLoan.sFHA2ndMAmt_rep);
            }
            if (isPurchase)
            {
                AddFormFieldData("purchase_sPurchPrice", dataLoan.sPurchPrice_rep);
                AddFormFieldData("purchase_sFHACcPbb", dataLoan.sFHACcPbb_rep);
                AddFormFieldData("purchase_sFHAUnadjAcquisition", dataLoan.sFHAUnadjAcquisition_rep);
                AddFormFieldData("purchase_sFHAStatutoryInvestReq", dataLoan.sFHAStatutoryInvestReq_rep);
                AddFormFieldData("purchase_sFHAHouseVal", dataLoan.sFHAHouseVal_rep);
                AddFormFieldData("purchase_sFHAReqAdj", dataLoan.sFHAReqAdj_rep);
                AddFormFieldData("purchase_sFHAMBasis", dataLoan.sFHAMBasis_rep);
                AddFormFieldData("purchase_sFHAMAmt", dataLoan.sFHAMAmt_rep);
                AddFormFieldData("purchase_sFHAMinDownPmt", dataLoan.sFHAMinDownPmt_rep);
            }

            AddFormFieldData("sFHACreditAnalysisRemarks", dataLoan.sFHACreditAnalysisRemarks);


            AddFormFieldData("FHA203kWorksheetUnderwriterLicenseNumOfAgent", dataLoan.sFHA203kUnderwriterChumsId);
        }

	}

    public class CHUD_92700Obsolete_1PDF : CAbstractHUD_92700PDF 
    {

        public override string Description 
        {
            get { return "Page 1"; }
        }
        public override string PdfFile 
        {
            get { return "HUD-92700Obsolete_1.pdf"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Obsolete/FHA203kWorksheet.aspx"; }
        }

    }
    public class CHUD_92700Obsolete_2PDF : CAbstractHUD_92700PDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92700Obsolete_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Obsolete/FHA203kWorksheet.aspx"; }
        }

    }
    public class CHUD_92700ObsoletePDF : AbstractBatchPDF
    {
        public override string Description 
        {
            get { return "FHA 203k Maximum Mortgage Worksheet (HUD-92700) (OBSOLETE) (exp 4/2004)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Obsolete/FHA203kWorksheet.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CHUD_92700Obsolete_1PDF(),
                                                 new CHUD_92700Obsolete_2PDF()
                                             };
            }
        }



    }

    public class CHUD_92700_1PDF : CAbstractHUD_92700PDF
    {

        public override string Description
        {
            get { return "Page 1"; }
        }
        public override string PdfFile
        {
            get { return "HUD-92700_1.pdf"; }
        }

        public override string EditLink
        {
            get { return "/newlos/FHA/FHA203kWorksheet.aspx"; }
        }

    }
    public class CHUD_92700_2PDF : CAbstractHUD_92700PDF
    {
        public override string PdfFile
        {
            get { return "HUD-92700_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }

        public override string EditLink
        {
            get { return "/newlos/FHA/FHA203kWorksheet.aspx"; }
        }

    }
    public class CHUD_92700PDF : AbstractBatchPDF
    {
        public override string Description
        {
            get { return "FHA 203k Maximum Mortgage Worksheet (HUD-92700) (exp 7/2017)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/FHA/FHA203kWorksheet.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CHUD_92700_1PDF(),
                                                 new CHUD_92700_2PDF()
                                             };
            }
        }



    }
    public class CHUD_92700_APDF : AbstractLetterPDF 
    {
        public override string Description 
        {
            get { return "FHA 203k Borrower's Acknowledgement (HUD-92700-A)"; }
        }
        public override string PdfFile 
        {
            get { return "HUD-92700-A.pdf"; }
        }
		public override bool OnlyPrintPrimary
		{
			get { return true; }
		}
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sFHA203kBorrAcknowledgeT", dataLoan.sFHA203kBorrAcknowledgeT);
            AddFormFieldData("sFHA203kBorrAcknowledgeDesc", dataLoan.sFHA203kBorrAcknowledgeDesc);
        }

    }
}
