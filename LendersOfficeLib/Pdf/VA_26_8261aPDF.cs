using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CVA_26_8261aPDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "VA-26-8261a.pdf";        	 }
        }
        public override string Description 
        {
            get { return "VA Request For Certificate of Veteran Status (VA 26-8261a)"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/VA/VARequestVeteranStatus.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBAddrFull", Tools.FormatAddress(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("aBDob", dataApp.aBDob_rep);

            AddFormFieldData("aActiveMilitaryStatTri", dataApp.aActiveMilitaryStatTri);
            AddFormFieldData("aVaClaimNum", dataApp.aVaClaimNum);

            AddFormFieldData("aVaServ1BranchNum", dataApp.aVaServ1BranchNum);
            AddFormFieldData("aVaServ1EndD", dataApp.aVaServ1EndD_rep);
            AddFormFieldData("aVaServ1FullNm", dataApp.aVaServ1FullNm);
            AddFormFieldData("aVaServ1Num", dataApp.aVaServ1Num);
            AddFormFieldData("aVaServ1Ssn", dataApp.aVaServ1Ssn);
            AddFormFieldData("aVaServ1StartD", dataApp.aVaServ1StartD_rep);
            AddFormFieldData("aVaServ2BranchNum", dataApp.aVaServ2BranchNum);
            AddFormFieldData("aVaServ2EndD", dataApp.aVaServ2EndD_rep);
            AddFormFieldData("aVaServ2FullNm", dataApp.aVaServ2FullNm);
            AddFormFieldData("aVaServ2Num", dataApp.aVaServ2Num);
            AddFormFieldData("aVaServ2Ssn", dataApp.aVaServ2Ssn);
            AddFormFieldData("aVaServ2StartD", dataApp.aVaServ2StartD_rep);
            AddFormFieldData("aWereActiveMilitaryDutyDayAfterTri", dataApp.aWereActiveMilitaryDutyDayAfterTri);
        }
	}
}
