﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;

    public class CQMSummaryFormPDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "QMSummaryForm.pdf"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Underwriting/QM/Main.aspx"; }
        }

        public override string Description
        {
            get { return "QM Results Summary"; }
        }

        protected override void ApplyData(DataAccess.CPageData dataLoan, DataAccess.CAppData dataApp)
        {
            dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
            AddFormFieldData("sQMLoanPassesPointAndFeesTest", dataLoan.sQMLoanPassesPointAndFeesTest);
            AddFormFieldData("sQMExcessUpfrontMIP", dataLoan.sQMExcessUpfrontMIP_rep);
            AddFormFieldData("sQMMaxPointAndFeesAllowedAmt", dataLoan.sQMMaxPointAndFeesAllowedAmt_rep);
            AddFormFieldData("sQMMaxPointAndFeesAllowedPc", dataLoan.sQMMaxPointAndFeesAllowedPc_rep);
            AddFormFieldData("sQMIsVerifiedIncomeAndAssets", dataLoan.sQMIsVerifiedIncomeAndAssets);
            AddFormFieldData("sQMLoanDoesNotHaveAmortTermOver30Yr", dataLoan.sQMLoanDoesNotHaveAmortTermOver30Yr);
            AddFormFieldData("sTodayD", dataLoan.sTodayD_rep);
            AddFormFieldData("sDocMagicClosingD", dataLoan.sDocMagicClosingD_rep);
            AddFormFieldData("sGfeOriginatorCompF", dataLoan.sGfeOriginatorCompF_rep);
            AddFormFieldData("sQMTotFeeAmt", dataLoan.sQMTotFeeAmt_rep);
            AddFormFieldData("sQMTotFeePc", dataLoan.sQMTotFeePc_rep);
            AddFormFieldData("sQMTotFeeAmount_rep", dataLoan.sQMTotFeeAmount_rep);
            AddFormFieldData("sQMExcessDiscntF", dataLoan.sQMExcessDiscntF_rep);
            AddFormFieldData("sQMParR", dataLoan.sQMParR_rep);
            AddFormFieldData("sQMAprRSpread_rep", dataLoan.sQMAprRSpread_rep);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sQMExclDiscntPnt", dataLoan.sQMExclDiscntPnt_rep);
            AddFormFieldData("sQMTotFinFeeAmount", dataLoan.sQMTotFinFeeAmount_rep);
            AddFormFieldData("sQMAveragePrimeOfferR", dataLoan.sQMAveragePrimeOfferR_rep);
            AddFormFieldData("sQMLoanPassedComplianceChecks", dataLoan.sQMLoanPassedComplianceChecks);
            AddFormFieldData("sQMMaxPrePmntPenalty", dataLoan.sQMMaxPrePmntPenalty_rep);
            AddFormFieldData("sQMLAmt", dataLoan.sQMLAmt_rep);
            AddFormFieldData("sQMParRSpread", dataLoan.sQMParRSpread_rep);
            AddFormFieldData("sQMLoanPurchaseAgency", CPageBase.sQMLoanPurchaseAgency_map_rep(dataLoan.sQMLoanPurchaseAgency));
            AddFormFieldData("sLRLckD", dataLoan.sRLckdD_rep);
            AddFormFieldData("sQMLoanDoesNotHaveBalloonFeature", dataLoan.sQMLoanDoesNotHaveBalloonFeature);
            AddFormFieldData("sQMQualBottom", dataLoan.sQMQualBottom_rep);
            AddFormFieldData("sQMLoanHasDtiLessThan43Pc", dataLoan.sQMLoanHasDtiLessThan43Pc);
            AddFormFieldData("sQMQualR", dataLoan.sQMQualR_rep);
            AddFormFieldData("sQMIsEligibleByLoanPurchaseAgency", dataLoan.sQMIsEligibleByLoanPurchaseAgency);
            AddFormFieldData("sQMLoanDoesNotHaveNegativeAmort", dataLoan.sQMLoanDoesNotHaveNegativeAmort);
            AddFormFieldData("sQMStatusT", CPageBase.sQMStatusT_map_rep(dataLoan.sQMStatusT));
            AddFormFieldData("sQMTotFeeAmount", dataLoan.sQMTotFeeAmount_rep);
            AddFormFieldData("sQMFhaUfmipAtClosing", dataLoan.sQMFhaUfmipAtClosing_rep);
            AddFormFieldData("sQMDiscntBuyDownR", dataLoan.sQMDiscntBuyDownR_rep);
        }
    }
}
