using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CVORListPDF : AbstractVerificationListPDF 
    {
        public override string Description 
        {
            get { return "Verification of Rent"; }
        }

        private void RenderVORLink(StringBuilder sb, CAppData dataApp, Guid recordID, string displayName) 
        {
            NameValueCollection c = null;
            if (null == Arguments)
                c = new NameValueCollection();
            else
                c = new NameValueCollection(Arguments);

            c["recordid"] = recordID.ToString();
            c["displayname"] = Utilities.SafeHtmlString(displayName);
            AbstractPDFFile pdf = new CVORPDF();
            pdf.ID = this.ID + "_" + this.CurrentIndex;

            pdf.Arguments = c;

            RenderPDFFile(sb, pdf, dataApp);
            this.CurrentIndex++;
        }

        protected override IEnumerable<PdfVerificationItem> GetItems(CAppData dataApp)
        {
            List<PdfVerificationItem> list = new List<PdfVerificationItem>();

            
            string addr = dataApp.aBAddr_SingleLine;

            if (dataApp.aBAddrT == E_aBAddrT.Rent && addr.TrimWhitespaceAndBOM() != "") 
            {
                list.Add(new PdfVerificationItem() { 
                    RecordId = new Guid("11111111-1111-1111-1111-111111111111"),
                    DisplayName = Utilities.SafeHtmlString(dataApp.aBNm + ", " + addr)
                });
            }

            addr = dataApp.aBPrev1Addr_SingleLine;
            if (dataApp.aBPrev1AddrT == E_aBPrev1AddrT.Rent && addr.TrimWhitespaceAndBOM() != "") 
            {
                list.Add(new PdfVerificationItem() { 
                    RecordId = new Guid("22222222-2222-2222-2222-222222222222"),
                    DisplayName = Utilities.SafeHtmlString(dataApp.aBNm + ", " + addr)
                });
            }
            addr = dataApp.aBPrev2Addr_SingleLine;
            if (dataApp.aBPrev2AddrT == E_aBPrev2AddrT.Rent && addr.TrimWhitespaceAndBOM() != "") 
            {
                list.Add(new PdfVerificationItem() { 
                    RecordId = new Guid("33333333-3333-3333-3333-333333333333"),
                    DisplayName = Utilities.SafeHtmlString(dataApp.aBNm + ", " + addr)
                });

            }

            addr = dataApp.aCAddr_SingleLine;
            if (dataApp.aCAddrT == E_aCAddrT.Rent && addr.TrimWhitespaceAndBOM() != "") 
            {
                list.Add(new PdfVerificationItem() { 
                    RecordId = new Guid("44444444-4444-4444-4444-444444444444"),
                    DisplayName = Utilities.SafeHtmlString(dataApp.aCNm + ", " + addr)
                });
            }

            addr = dataApp.aCPrev1Addr_SingleLine;
            if (dataApp.aCPrev1AddrT == E_aCPrev1AddrT.Rent && addr.TrimWhitespaceAndBOM() != "") 
            {
                list.Add(new PdfVerificationItem() { 
                    RecordId = new Guid("55555555-5555-5555-5555-555555555555"),
                    DisplayName = Utilities.SafeHtmlString(dataApp.aCNm + ", " + addr)
                });
            }
            addr = dataApp.aCPrev2Addr_SingleLine;
            if (dataApp.aCPrev2AddrT == E_aCPrev2AddrT.Rent && addr.TrimWhitespaceAndBOM() != "") 
            {
                list.Add(new PdfVerificationItem() { 
                    RecordId = new Guid("66666666-6666-6666-6666-666666666666"),
                    DisplayName = Utilities.SafeHtmlString(dataApp.aCNm + ", " + addr)
                });
            }

            return list;
        }

        protected override AbstractPDFFile ContructPdfItem()
        {
            return new CVORPDF();
        }
    }

	public class CVORPDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "VOM.pdf"; }
        }

        public override string Description 
        {
            get { return "VOR: " + Arguments["displayname"]; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Verifications/VORRecord.aspx"; }
        }

        public override Guid RecordID
        {
            get
            {
                Guid id;

                if (Guid.TryParse(this.Arguments["recordid"], out id))
                {
                    return id;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string propertyAddress = "";

            Guid recordID = RecordID;
            if (recordID == new Guid("11111111-1111-1111-1111-111111111111")) 
            {
                propertyAddress = Tools.FormatAddress(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip);
            } 
            else if (recordID == new Guid("22222222-2222-2222-2222-222222222222")) 
            {
                propertyAddress = Tools.FormatAddress(dataApp.aBPrev1Addr, dataApp.aBPrev1City, dataApp.aBPrev1State, dataApp.aBPrev1Zip);
            }
            else if (recordID == new Guid("33333333-3333-3333-3333-333333333333")) 
            {
                propertyAddress = Tools.FormatAddress(dataApp.aBPrev2Addr, dataApp.aBPrev2City, dataApp.aBPrev2State, dataApp.aBPrev2Zip);
            }
            else if (recordID == new Guid("44444444-4444-4444-4444-444444444444")) 
            {
                propertyAddress = Tools.FormatAddress(dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip);
            }
            else if (recordID == new Guid("55555555-5555-5555-5555-555555555555")) 
            {
                propertyAddress = Tools.FormatAddress(dataApp.aCPrev1Addr, dataApp.aCPrev1City, dataApp.aCPrev1State, dataApp.aCPrev1Zip);
            }
            else if (recordID == new Guid("66666666-6666-6666-6666-666666666666")) 
            {
                propertyAddress = Tools.FormatAddress(dataApp.aCPrev2Addr, dataApp.aCPrev2City, dataApp.aCPrev2State, dataApp.aCPrev2Zip);
            }

            IVerificationOfRent field = dataApp.GetVorFields(recordID);

            if (dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp && field.VerifHasSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(dataLoan.sBrokerId, field.VerifSigningEmployeeId, dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);                
                AddFormFieldData("LenderSignature", signInfo.SignatureKey.ToString());
            }
            string accountName = field.AccountName;
            string creditorPhone = "";

            if (field.PhoneTo != "")
                creditorPhone = "Phone: " + field.PhoneTo + "   ";
            if (field.FaxTo != "")
                creditorPhone += "Fax: " + field.FaxTo;

            string creditorAddress = Tools.FormatAddress(field.Attention + Environment.NewLine + field.LandlordCreditorName, field.AddressTo, field.CityTo, field.StateTo, field.ZipTo, creditorPhone);

            bool isSeeAttachment = field.IsSeeAttachment;

            string borrowerName = dataApp.aBNmAndSsnForVerifications + Environment.NewLine;
            string coborrowerName = dataApp.aCNmAndSsnForVerifications.TrimWhitespaceAndBOM();
            if (!string.IsNullOrEmpty(coborrowerName)) coborrowerName += Environment.NewLine;

            string applicantAddress = string.Format("{0}{1}{2}", borrowerName, 
                coborrowerName, Tools.FormatAddress(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));


            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfRent, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string name = string.Format("{0}{2}{1}", preparer.PreparerName, preparer.CompanyName, Environment.NewLine);
            string phone = "";

            if (preparer.Phone != "")
                phone = "Phone: " + preparer.Phone + "   ";

            if (preparer.FaxNum != "")
                phone += "Fax: " + preparer.FaxNum;

            string brokerAddress = Tools.FormatAddress(name, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, phone);

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sLenderNumVerif", dataLoan.sLenderNumVerif);

            AddFormFieldData("CreditorAddress", creditorAddress);
            AddFormFieldData("BrokerAddress", brokerAddress);
            AddFormFieldData("Title", preparer.Title);
            AddFormFieldData("Date", preparer.PrepareDate_rep);
            AddFormFieldData("PropertyAddress", propertyAddress);
            AddFormFieldData("AccountName", accountName);
            AddFormFieldData("ApplicantAddress", applicantAddress);
            AddFormFieldData("AccountType", "R");

            if (isSeeAttachment) 
            {
                AddFormFieldData("ApplicantSignature", "SEE ATTACHMENT");
            }



        }
	}
}
