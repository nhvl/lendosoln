using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CTaxTranscriptsRequest_1PDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "TaxTranscriptsRequest_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
            AddFormFieldData("PresentAddress", Tools.FormatAddress(dataApp.aBNm, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
			if( !dataApp.aBHPhone.TrimWhitespaceAndBOM().Equals("") )
				AddFormFieldData("BorrowerPhone", dataApp.aBHPhone);
			else if( !dataApp.aCHPhone.TrimWhitespaceAndBOM().Equals("") )
				AddFormFieldData("BorrowerPhone", dataApp.aCHPhone);

        }
    }
    public class CTaxTranscriptsRequest_2PDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "TaxTranscriptsRequest_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
        }
    }

	public class CTaxTranscriptsRequestPDF : AbstractBatchPDF
	{

        public override string Description 
        {
            get { return "Request For Copy of Tax Return (4506)"; }
        }
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CTaxTranscriptsRequest_1PDF(),
                                                 new CTaxTranscriptsRequest_2PDF()
                                             };
            }
        }

	}
}
