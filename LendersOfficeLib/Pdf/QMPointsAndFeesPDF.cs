﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;

    public class CQMPointsAndFeesPDF : AbstractBatchPDF
    {
        public override string Description
        {
            get { return "QM Points and Fees"; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get 
            { 
                return new AbstractPDFFile[]
                {
                    new CQMPointsAndFeesPDF_1(),
                    new CQMPointsAndFeesPDF_2(),
                    new CQMPointsAndFeesPDF_3()
                };
            }
        }

        public override string EditLink
        {
            get {  return "/newlos/Underwriting/QM/Main.aspx?pg=1"; }
        }
    }

    public abstract class AbstractQMPointsAndFeesPDF : AbstractLetterPDF
    {
        protected string GetPayer(int payer)
        {
            switch (payer)
            {
                case 0: return "borr";
                case 1: return "borr fin";
                case 2: return "seller";
                case 3: return "lender";
                case 4: return "broker";
                default: throw new CBaseException(ErrorMessages.Generic, "Unhandled payer value.");
            }
        }
    }

    public class CQMPointsAndFeesPDF_1 : AbstractQMPointsAndFeesPDF
    {
        public override string PdfFile
        {
            get { return "QMPointsAndFees_1.pdf"; }
        }

        public override string Description
        {
            get { return "Page 1"; }
        }

        protected override void ApplyData(DataAccess.CPageData dataLoan, DataAccess.CAppData dataApp)
        {
            dataLoan.SetFormatTarget(DataAccess.FormatTarget.PrintoutImportantFields);

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sTodayD", dataLoan.sTodayD_rep);

            AddFormFieldData("sQMLOrigF", dataLoan.sQMLOrigF_rep);
            AddFormFieldData("sQMLOrigF_QMAmount", dataLoan.sQMLOrigF_QMAmount_rep);
            AddFormFieldData("sQMLOrigFProps_Apr", dataLoan.sQMLOrigFProps_Apr);
            AddFormFieldData("sQMLOrigFProps_Aff", dataLoan.sQMLOrigFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMLOrigFProps_TrdPty", dataLoan.sQMLOrigFProps_PaidToThirdParty);
            AddFormFieldData("sQMLOrigFProps_Payer", GetPayer(dataLoan.sQMLOrigFProps_Payer));

            AddFormFieldData("sQMGfeOriginatorCompF", dataLoan.sQMGfeOriginatorCompF_rep);
            AddFormFieldData("sQMGfeOriginatorCompF_QMAmount", dataLoan.sQMGfeOriginatorCompF_QMAmount_rep);
            AddFormFieldData("sQMGfeOriginatorCompFProps_Apr", dataLoan.sQMGfeOriginatorCompFProps_Apr);
            AddFormFieldData("sQMGfeOriginatorCompFProps_Aff", dataLoan.sQMGfeOriginatorCompFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMGfeOriginatorCompFProps_TrdPty", dataLoan.sQMGfeOriginatorCompFProps_PaidToThirdParty);
            AddFormFieldData("sQMGfeOriginatorCompFProps_Payer", GetPayer(dataLoan.sQMGfeOriginatorCompFProps_Payer));

            AddFormFieldData("sQMGfeDiscountPointF", dataLoan.sQMGfeDiscountPointF_rep);
            AddFormFieldData("sQMGfeDiscountPointF_QMAmount", dataLoan.sQMGfeDiscountPointF_QMAmount_rep);
            AddFormFieldData("sQMGfeDiscountPointFProps_Apr", dataLoan.sQMGfeDiscountPointFProps_Apr);
            AddFormFieldData("sQMGfeDiscountPointFProps_Payer", GetPayer(dataLoan.sQMGfeDiscountPointFProps_Payer));

            AddFormFieldData("sQMApprF", dataLoan.sQMApprF_rep);
            AddFormFieldData("sQMApprF_QMAmount", dataLoan.sQMApprF_QMAmount_rep);
            AddFormFieldData("sQMApprFProps_Apr", dataLoan.sQMApprFProps_Apr);
            AddFormFieldData("sQMApprFProps_Aff", dataLoan.sQMApprFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMApprFProps_TrdPty", dataLoan.sQMApprFProps_PaidToThirdParty);
            AddFormFieldData("sQMApprFProps_Payer", GetPayer(dataLoan.sQMApprFProps_Payer));

            AddFormFieldData("sQMCrF", dataLoan.sQMCrF_rep);
            AddFormFieldData("sQMCrF_QMAmount", dataLoan.sQMCrF_QMAmount_rep);
            AddFormFieldData("sQMCrFProps_Apr", dataLoan.sQMCrFProps_Apr);
            AddFormFieldData("sQMCrFProps_Aff", dataLoan.sQMCrFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMCrFProps_TrdPty", dataLoan.sQMCrFProps_PaidToThirdParty);
            AddFormFieldData("sQMCrFProps_Payer", GetPayer(dataLoan.sQMCrFProps_Payer));

            AddFormFieldData("sQMTxServF", dataLoan.sQMTxServF_rep);
            AddFormFieldData("sQMTxServF_QMAmount", dataLoan.sQMTxServF_QMAmount_rep);
            AddFormFieldData("sQMTxServFProps_Apr", dataLoan.sQMTxServFProps_Apr);
            AddFormFieldData("sQMTxServFProps_Aff", dataLoan.sQMTxServFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMTxServFProps_TrdPty", dataLoan.sQMTxServFProps_PaidToThirdParty);
            AddFormFieldData("sQMTxServFProps_Payer", GetPayer(dataLoan.sQMTxServFProps_Payer));

            AddFormFieldData("sQMFloodCertificationF", dataLoan.sQMFloodCertificationF_rep);
            AddFormFieldData("sQMFloodCertificationF_QMAmount", dataLoan.sQMFloodCertificationF_QMAmount_rep);
            AddFormFieldData("sQMFloodCertificationFProps_Apr", dataLoan.sQMFloodCertificationFProps_Apr);
            AddFormFieldData("sQMFloodCertificationFProps_Aff", dataLoan.sQMFloodCertificationFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMFloodCertificationFProps_TrdPty", dataLoan.sQMFloodCertificationFProps_PaidToThirdParty);
            AddFormFieldData("sQMFloodCertificationFProps_Payer", GetPayer(dataLoan.sQMFloodCertificationFProps_Payer));

            AddFormFieldData("sQMMBrokF", dataLoan.sQMMBrokF_rep);
            AddFormFieldData("sQMMBrokF_QMAmount", dataLoan.sQMMBrokF_QMAmount_rep);
            AddFormFieldData("sQMMBrokFProps_Apr", dataLoan.sQMMBrokFProps_Apr);
            AddFormFieldData("sQMMBrokFProps_Aff", dataLoan.sQMMBrokFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMMBrokFProps_TrdPty", dataLoan.sQMMBrokFProps_PaidToThirdParty);
            AddFormFieldData("sQMMBrokFProps_Payer", GetPayer(dataLoan.sQMMBrokFProps_Payer));

            AddFormFieldData("sQMInspectF", dataLoan.sQMInspectF_rep);
            AddFormFieldData("sQMInspectF_QMAmount", dataLoan.sQMInspectF_QMAmount_rep);
            AddFormFieldData("sQMInspectFProps_Apr", dataLoan.sQMInspectFProps_Apr);
            AddFormFieldData("sQMInspectFProps_Aff", dataLoan.sQMInspectFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMInspectFProps_TrdPty", dataLoan.sQMInspectFProps_PaidToThirdParty);
            AddFormFieldData("sQMInspectFProps_Payer", GetPayer(dataLoan.sQMInspectFProps_Payer));

            AddFormFieldData("sQMProcF", dataLoan.sQMProcF_rep);
            AddFormFieldData("sQMProcF_QMAmount", dataLoan.sQMProcF_QMAmount_rep);
            AddFormFieldData("sQMProcFProps_Apr", dataLoan.sQMProcFProps_Apr);
            AddFormFieldData("sQMProcFProps_Aff", dataLoan.sQMProcFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMProcFProps_TrdPty", dataLoan.sQMProcFProps_PaidToThirdParty);
            AddFormFieldData("sQMProcFProps_Payer", GetPayer(dataLoan.sQMProcFProps_Payer));

            AddFormFieldData("sQMUwF", dataLoan.sQMUwF_rep);
            AddFormFieldData("sQMUwF_QMAmount", dataLoan.sQMUwF_QMAmount_rep);
            AddFormFieldData("sQMUwFProps_Apr", dataLoan.sQMUwFProps_Apr);
            AddFormFieldData("sQMUwFProps_Aff", dataLoan.sQMUwFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMUwFProps_TrdPty", dataLoan.sQMUwFProps_PaidToThirdParty);
            AddFormFieldData("sQMUwFProps_Payer", GetPayer(dataLoan.sQMUwFProps_Payer));

            AddFormFieldData("sQMWireF", dataLoan.sQMWireF_rep);
            AddFormFieldData("sQMWireF_QMAmount", dataLoan.sQMWireF_QMAmount_rep);
            AddFormFieldData("sQMWireFProps_Apr", dataLoan.sQMWireFProps_Apr);
            AddFormFieldData("sQMWireFProps_Aff", dataLoan.sQMWireFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMWireFProps_TrdPty", dataLoan.sQMWireFProps_PaidToThirdParty);
            AddFormFieldData("sQMWireFProps_Payer", GetPayer(dataLoan.sQMWireFProps_Payer));

            AddFormFieldData("sQM800U1FDesc", dataLoan.sQM800U1FDesc);
            AddFormFieldData("sQM800U1F", dataLoan.sQM800U1F_rep);
            AddFormFieldData("sQM800U1F_QMAmount", dataLoan.sQM800U1F_QMAmount_rep);
            AddFormFieldData("sQM800U1FProps_Apr", dataLoan.sQM800U1FProps_Apr);
            AddFormFieldData("sQM800U1FProps_Aff", dataLoan.sQM800U1FProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQM800U1FProps_TrdPty", dataLoan.sQM800U1FProps_PaidToThirdParty);
            AddFormFieldData("sQM800U1FProps_Payer", GetPayer(dataLoan.sQM800U1FProps_Payer));

            AddFormFieldData("sQM800U2FDesc", dataLoan.sQM800U2FDesc);
            AddFormFieldData("sQM800U2F", dataLoan.sQM800U2F_rep);
            AddFormFieldData("sQM800U2F_QMAmount", dataLoan.sQM800U2F_QMAmount_rep);
            AddFormFieldData("sQM800U2FProps_Apr", dataLoan.sQM800U2FProps_Apr);
            AddFormFieldData("sQM800U2FProps_Aff", dataLoan.sQM800U2FProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQM800U2FProps_TrdPty", dataLoan.sQM800U2FProps_PaidToThirdParty);
            AddFormFieldData("sQM800U2FProps_Payer", GetPayer(dataLoan.sQM800U2FProps_Payer));

            AddFormFieldData("sQM800U3FDesc", dataLoan.sQM800U3FDesc);
            AddFormFieldData("sQM800U3F", dataLoan.sQM800U3F_rep);
            AddFormFieldData("sQM800U3F_QMAmount", dataLoan.sQM800U3F_QMAmount_rep);
            AddFormFieldData("sQM800U3FProps_Apr", dataLoan.sQM800U3FProps_Apr);
            AddFormFieldData("sQM800U3FProps_Aff", dataLoan.sQM800U3FProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQM800U3FProps_TrdPty", dataLoan.sQM800U3FProps_PaidToThirdParty);
            AddFormFieldData("sQM800U3FProps_Payer", GetPayer(dataLoan.sQM800U3FProps_Payer));

            AddFormFieldData("sQM800U4FDesc", dataLoan.sQM800U4FDesc);
            AddFormFieldData("sQM800U4F", dataLoan.sQM800U4F_rep);
            AddFormFieldData("sQM800U4F_QMAmount", dataLoan.sQM800U4F_QMAmount_rep);
            AddFormFieldData("sQM800U4FProps_Apr", dataLoan.sQM800U4FProps_Apr);
            AddFormFieldData("sQM800U4FProps_Aff", dataLoan.sQM800U4FProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQM800U4FProps_TrdPty", dataLoan.sQM800U4FProps_PaidToThirdParty);
            AddFormFieldData("sQM800U4FProps_Payer", GetPayer(dataLoan.sQM800U4FProps_Payer));

            AddFormFieldData("sQM800U5FDesc", dataLoan.sQM800U5FDesc);
            AddFormFieldData("sQM800U5F", dataLoan.sQM800U5F_rep);
            AddFormFieldData("sQM800U5F_QMAmount", dataLoan.sQM800U5F_QMAmount_rep);
            AddFormFieldData("sQM800U5FProps_Apr", dataLoan.sQM800U5FProps_Apr);
            AddFormFieldData("sQM800U5FProps_Aff", dataLoan.sQM800U5FProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQM800U5FProps_TrdPty", dataLoan.sQM800U5FProps_PaidToThirdParty);
            AddFormFieldData("sQM800U5FProps_Payer", GetPayer(dataLoan.sQM800U5FProps_Payer));

            AddFormFieldData("sQMIPia", dataLoan.sQMIPia_rep);
            AddFormFieldData("sQMIPia_QMAmount", dataLoan.sQMIPia_QMAmount_rep);
            AddFormFieldData("sQMIPiaProps_Apr", dataLoan.sQMIPiaProps_Apr);
            AddFormFieldData("sQMIPiaProps_Aff", dataLoan.sQMIPiaProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMIPiaProps_TrdPty", dataLoan.sQMIPiaProps_PaidToThirdParty);
            AddFormFieldData("sQMIPiaProps_Payer", GetPayer(dataLoan.sQMIPiaProps_Payer));

            AddFormFieldData("sQMMipPia", dataLoan.sQMMipPia_rep);
            AddFormFieldData("sQMMipPia_QMAmount", dataLoan.sQMMipPia_QMAmount_rep);
            AddFormFieldData("sQMMipPiaProps_Apr", dataLoan.sQMMipPiaProps_Apr);
            AddFormFieldData("sQMMipPiaProps_Payer", GetPayer(dataLoan.sQMMipPiaProps_Payer));

            AddFormFieldData("sQMHazInsPia", dataLoan.sQMHazInsPia_rep);
            AddFormFieldData("sQMHazInsPia_QMAmount", dataLoan.sQMHazInsPia_QMAmount_rep);
            AddFormFieldData("sQMHazInsPiaProps_Apr", dataLoan.sQMHazInsPiaProps_Apr);
            AddFormFieldData("sQMHazInsPiaProps_Aff", dataLoan.sQMHazInsPiaProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMHazInsPiaProps_TrdPty", dataLoan.sQMHazInsPiaProps_PaidToThirdParty);
            AddFormFieldData("sQMHazInsPiaProps_Payer", GetPayer(dataLoan.sQMHazInsPiaProps_Payer));

            AddFormFieldData("sQM904PiaDesc", dataLoan.sQM904PiaDesc);
            AddFormFieldData("sQM904Pia", dataLoan.sQM904Pia_rep);
            AddFormFieldData("sQM904Pia_QMAmount", dataLoan.sQM904Pia_QMAmount_rep);
            AddFormFieldData("sQM904PiaProps_Apr", dataLoan.sQM904PiaProps_Apr);
            AddFormFieldData("sQM904PiaProps_Aff", dataLoan.sQM904PiaProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQM904PiaProps_TrdPty", dataLoan.sQM904PiaProps_PaidToThirdParty);
            AddFormFieldData("sQM904PiaProps_Payer", GetPayer(dataLoan.sQM904PiaProps_Payer));

            AddFormFieldData("sQMVaFf", dataLoan.sQMVaFf_rep);
            AddFormFieldData("sQMVaFf_QMAmount", dataLoan.sQMVaFf_QMAmount_rep);
            AddFormFieldData("sQMVaFfProps_Apr", dataLoan.sQMVaFfProps_Apr);
            AddFormFieldData("sQMVaFfProps_Aff", dataLoan.sQMVaFfProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMVaFfProps_TrdPty", dataLoan.sQMVaFfProps_PaidToThirdParty);
            AddFormFieldData("sQMVaFfProps_Payer", GetPayer(dataLoan.sQMVaFfProps_Payer));

            AddFormFieldData("sQM900U1PiaDesc", dataLoan.sQM900U1PiaDesc);
            AddFormFieldData("sQM900U1Pia", dataLoan.sQM900U1Pia_rep);
            AddFormFieldData("sQM900U1Pia_QMAmount", dataLoan.sQM900U1Pia_QMAmount_rep);
            AddFormFieldData("sQM900U1PiaProps_Apr", dataLoan.sQM900U1PiaProps_Apr);
            AddFormFieldData("sQM900U1PiaProps_Aff", dataLoan.sQM900U1PiaProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQM900U1PiaProps_TrdPty", dataLoan.sQM900U1PiaProps_PaidToThirdParty);
            AddFormFieldData("sQM900U1PiaProps_Payer", GetPayer(dataLoan.sQM900U1PiaProps_Payer));

            AddFormFieldData("sQMHazInsRsrv", dataLoan.sQMHazInsRsrv_rep);
            AddFormFieldData("sQMHazInsRsrv_QMAmount", dataLoan.sQMHazInsRsrv_QMAmount_rep);
            AddFormFieldData("sQMHazInsRsrvProps_Apr", dataLoan.sQMHazInsRsrvProps_Apr);
            AddFormFieldData("sQMHazInsRsrvProps_Aff", dataLoan.sQMHazInsRsrvProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMHazInsRsrvProps_TrdPty", dataLoan.sQMHazInsRsrvProps_PaidToThirdParty);
            AddFormFieldData("sQMHazInsRsrvProps_Payer", GetPayer(dataLoan.sQMHazInsRsrvProps_Payer));

            AddFormFieldData("sQMMInsRsrv", dataLoan.sQMMInsRsrv_rep);
            AddFormFieldData("sQMMInsRsrv_QMAmount", dataLoan.sQMMInsRsrv_QMAmount_rep);
            AddFormFieldData("sQMMInsRsrvProps_Apr", dataLoan.sQMMInsRsrvProps_Apr);
            AddFormFieldData("sQMMInsRsrvProps_Aff", dataLoan.sQMMInsRsrvProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMMInsRsrvProps_TrdPty", dataLoan.sQMMInsRsrvProps_PaidToThirdParty);
            AddFormFieldData("sQMMInsRsrvProps_Payer", GetPayer(dataLoan.sQMMInsRsrvProps_Payer));

            AddFormFieldData("sQMRealETxRsrv", dataLoan.sQMRealETxRsrv_rep);
            AddFormFieldData("sQMRealETxRsrv_QMAmount", dataLoan.sQMRealETxRsrv_QMAmount_rep);
            AddFormFieldData("sQMRealETxRsrvProps_Apr", dataLoan.sQMRealETxRsrvProps_Apr);
            AddFormFieldData("sQMRealETxRsrvProps_Aff", dataLoan.sQMRealETxRsrvProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMRealETxRsrvProps_TrdPty", dataLoan.sQMRealETxRsrvProps_PaidToThirdParty);
            AddFormFieldData("sQMRealETxRsrvProps_Payer", GetPayer(dataLoan.sQMRealETxRsrvProps_Payer));

            AddFormFieldData("sQMSchoolTxRsrv", dataLoan.sQMSchoolTxRsrv_rep);
            AddFormFieldData("sQMSchoolTxRsrv_QMAmount", dataLoan.sQMSchoolTxRsrv_QMAmount_rep);
            AddFormFieldData("sQMSchoolTxRsrvProps_Apr", dataLoan.sQMSchoolTxRsrvProps_Apr);
            AddFormFieldData("sQMSchoolTxRsrvProps_Aff", dataLoan.sQMSchoolTxRsrvProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMSchoolTxRsrvProps_TrdPty", dataLoan.sQMSchoolTxRsrvProps_PaidToThirdParty);
            AddFormFieldData("sQMSchoolTxRsrvProps_Payer", GetPayer(dataLoan.sQMSchoolTxRsrvProps_Payer));

            AddFormFieldData("sQMFloodInsRsrv", dataLoan.sQMFloodInsRsrv_rep);
            AddFormFieldData("sQMFloodInsRsrv_QMAmount", dataLoan.sQMFloodInsRsrv_QMAmount_rep);
            AddFormFieldData("sQMFloodInsRsrvProps_Apr", dataLoan.sQMFloodInsRsrvProps_Apr);
            AddFormFieldData("sQMFloodInsRsrvProps_Aff", dataLoan.sQMFloodInsRsrvProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMFloodInsRsrvProps_TrdPty", dataLoan.sQMFloodInsRsrvProps_PaidToThirdParty);
            AddFormFieldData("sQMFloodInsRsrvProps_Payer", GetPayer(dataLoan.sQMFloodInsRsrvProps_Payer));

            AddFormFieldData("sQMAggregateAdjRsrv", dataLoan.sQMAggregateAdjRsrv_rep);
            AddFormFieldData("sQMAggregateAdjRsrv_QMAmount", dataLoan.sQMAggregateAdjRsrv_QMAmount_rep);
            AddFormFieldData("sQMAggregateAdjRsrvProps_Apr", dataLoan.sQMAggregateAdjRsrvProps_Apr);
            AddFormFieldData("sQMAggregateAdjRsrvProps_Aff", dataLoan.sQMAggregateAdjRsrvProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMAggregateAdjRsrvProps_TrdPty", dataLoan.sQMAggregateAdjRsrvProps_PaidToThirdParty);
            AddFormFieldData("sQMAggregateAdjRsrvProps_Payer", GetPayer(dataLoan.sQMAggregateAdjRsrvProps_Payer));

            AddFormFieldData("sQM1006RsrvDesc", dataLoan.sQM1006ProHExpDesc);
            AddFormFieldData("sQM1006Rsrv", dataLoan.sQM1006Rsrv_rep);
            AddFormFieldData("sQM1006Rsrv_QMAmount", dataLoan.sQM1006Rsrv_QMAmount_rep);
            AddFormFieldData("sQM1006RsrvProps_Apr", dataLoan.sQM1006RsrvProps_Apr);
            AddFormFieldData("sQM1006RsrvProps_Aff", dataLoan.sQM1006RsrvProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQM1006RsrvProps_TrdPty", dataLoan.sQM1006RsrvProps_PaidToThirdParty);
            AddFormFieldData("sQM1006RsrvProps_Payer", GetPayer(dataLoan.sQM1006RsrvProps_Payer));
        }
    }

    public class CQMPointsAndFeesPDF_2 : AbstractQMPointsAndFeesPDF
    {
        public override string PdfFile
        {
            get { return "QMPointsAndFees_2.pdf"; }
        }

        public override string Description
        {
            get { return "Page 2"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.SetFormatTarget(DataAccess.FormatTarget.PrintoutImportantFields);

            AddFormFieldData("sQM1007RsrvDesc", dataLoan.sQM1007ProHExpDesc);
            AddFormFieldData("sQM1007Rsrv", dataLoan.sQM1007Rsrv_rep);
            AddFormFieldData("sQM1007Rsrv_QMAmount", dataLoan.sQM1007Rsrv_QMAmount_rep);
            AddFormFieldData("sQM1007RsrvProps_Apr", dataLoan.sQM1007RsrvProps_Apr);
            AddFormFieldData("sQM1007RsrvProps_Aff", dataLoan.sQM1007RsrvProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQM1007RsrvProps_TrdPty", dataLoan.sQM1007RsrvProps_PaidToThirdParty);
            AddFormFieldData("sQM1007RsrvProps_Payer", GetPayer(dataLoan.sQM1007RsrvProps_Payer));

            AddFormFieldData("sQMEscrowF", dataLoan.sQMEscrowF_rep);
            AddFormFieldData("sQMEscrowF_QMAmount", dataLoan.sQMEscrowF_QMAmount_rep);
            AddFormFieldData("sQMEscrowFProps_Apr", dataLoan.sQMEscrowFProps_Apr);
            AddFormFieldData("sQMEscrowFProps_Aff", dataLoan.sQMEscrowFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMEscrowFProps_TrdPty", dataLoan.sQMEscrowFProps_PaidToThirdParty);
            AddFormFieldData("sQMEscrowFProps_Payer", GetPayer(dataLoan.sQMEscrowFProps_Payer));

            AddFormFieldData("sQMOwnerTitleInsF", dataLoan.sQMOwnerTitleInsF_rep);
            AddFormFieldData("sQMOwnerTitleInsF_QMAmount", dataLoan.sQMOwnerTitleInsF_QMAmount_rep);
            AddFormFieldData("sQMOwnerTitleInsFProps_Apr", dataLoan.sQMOwnerTitleInsFProps_Apr);
            AddFormFieldData("sQMOwnerTitleInsFProps_Aff", dataLoan.sQMOwnerTitleInsFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMOwnerTitleInsFProps_TrdPty", dataLoan.sQMOwnerTitleInsFProps_PaidToThirdParty);
            AddFormFieldData("sQMOwnerTitleInsFProps_Payer", GetPayer(dataLoan.sQMOwnerTitleInsFProps_Payer));

            AddFormFieldData("sQMTitleInsF", dataLoan.sQMTitleInsF_rep);
            AddFormFieldData("sQMTitleInsF_QMAmount", dataLoan.sQMTitleInsF_QMAmount_rep);
            AddFormFieldData("sQMTitleInsFProps_Apr", dataLoan.sQMTitleInsFProps_Apr);
            AddFormFieldData("sQMTitleInsFProps_Aff", dataLoan.sQMTitleInsFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMTitleInsFProps_TrdPty", dataLoan.sQMTitleInsFProps_PaidToThirdParty);
            AddFormFieldData("sQMTitleInsFProps_Payer", GetPayer(dataLoan.sQMTitleInsFProps_Payer));

            AddFormFieldData("sQMDocPrepF", dataLoan.sQMDocPrepF_rep);
            AddFormFieldData("sQMDocPrepF_QMAmount", dataLoan.sQMDocPrepF_QMAmount_rep);
            AddFormFieldData("sQMDocPrepFProps_Apr", dataLoan.sQMDocPrepFProps_Apr);
            AddFormFieldData("sQMDocPrepFProps_Aff", dataLoan.sQMDocPrepFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMDocPrepFProps_TrdPty", dataLoan.sQMDocPrepFProps_PaidToThirdParty);
            AddFormFieldData("sQMDocPrepFProps_Payer", GetPayer(dataLoan.sQMDocPrepFProps_Payer));

            AddFormFieldData("sQMNotaryF", dataLoan.sQMNotaryF_rep);
            AddFormFieldData("sQMNotaryF_QMAmount", dataLoan.sQMNotaryF_QMAmount_rep);
            AddFormFieldData("sQMNotaryFProps_Apr", dataLoan.sQMNotaryFProps_Apr);
            AddFormFieldData("sQMNotaryFProps_Aff", dataLoan.sQMNotaryFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMNotaryFProps_TrdPty", dataLoan.sQMNotaryFProps_PaidToThirdParty);
            AddFormFieldData("sQMNotaryFProps_Payer", GetPayer(dataLoan.sQMNotaryFProps_Payer));

            AddFormFieldData("sQMAttorneyF", dataLoan.sQMAttorneyF_rep);
            AddFormFieldData("sQMAttorneyF_QMAmount", dataLoan.sQMAttorneyF_QMAmount_rep);
            AddFormFieldData("sQMAttorneyFProps_Apr", dataLoan.sQMAttorneyFProps_Apr);
            AddFormFieldData("sQMAttorneyFProps_Aff", dataLoan.sQMAttorneyFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMAttorneyFProps_TrdPty", dataLoan.sQMAttorneyFProps_PaidToThirdParty);
            AddFormFieldData("sQMAttorneyFProps_Payer", GetPayer(dataLoan.sQMAttorneyFProps_Payer));

            AddFormFieldData("sQMU1TcDesc", dataLoan.sQMU1TcDesc);
            AddFormFieldData("sQMU1Tc", dataLoan.sQMU1Tc_rep);
            AddFormFieldData("sQMU1Tc_QMAmount", dataLoan.sQMU1Tc_QMAmount_rep);
            AddFormFieldData("sQMU1TcProps_Apr", dataLoan.sQMU1TcProps_Apr);
            AddFormFieldData("sQMU1TcProps_Aff", dataLoan.sQMU1TcProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU1TcProps_TrdPty", dataLoan.sQMU1TcProps_PaidToThirdParty);
            AddFormFieldData("sQMU1TcProps_Payer", GetPayer(dataLoan.sQMU1TcProps_Payer));

            AddFormFieldData("sQMU2TcDesc", dataLoan.sQMU2TcDesc);
            AddFormFieldData("sQMU2Tc", dataLoan.sQMU2Tc_rep);
            AddFormFieldData("sQMU2Tc_QMAmount", dataLoan.sQMU2Tc_QMAmount_rep);
            AddFormFieldData("sQMU2TcProps_Apr", dataLoan.sQMU2TcProps_Apr);
            AddFormFieldData("sQMU2TcProps_Aff", dataLoan.sQMU2TcProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU2TcProps_TrdPty", dataLoan.sQMU2TcProps_PaidToThirdParty);
            AddFormFieldData("sQMU2TcProps_Payer", GetPayer(dataLoan.sQMU2TcProps_Payer));

            AddFormFieldData("sQMU3TcDesc", dataLoan.sQMU3TcDesc);
            AddFormFieldData("sQMU3Tc", dataLoan.sQMU3Tc_rep);
            AddFormFieldData("sQMU3Tc_QMAmount", dataLoan.sQMU3Tc_QMAmount_rep);
            AddFormFieldData("sQMU3TcProps_Apr", dataLoan.sQMU3TcProps_Apr);
            AddFormFieldData("sQMU3TcProps_Aff", dataLoan.sQMU3TcProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU3TcProps_TrdPty", dataLoan.sQMU3TcProps_PaidToThirdParty);
            AddFormFieldData("sQMU3TcProps_Payer", GetPayer(dataLoan.sQMU3TcProps_Payer));

            AddFormFieldData("sQMU4TcDesc", dataLoan.sQMU4TcDesc);
            AddFormFieldData("sQMU4Tc", dataLoan.sQMU4Tc_rep);
            AddFormFieldData("sQMU4Tc_QMAmount", dataLoan.sQMU4Tc_QMAmount_rep);
            AddFormFieldData("sQMU4TcProps_Apr", dataLoan.sQMU4TcProps_Apr);
            AddFormFieldData("sQMU4TcProps_Aff", dataLoan.sQMU4TcProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU4TcProps_TrdPty", dataLoan.sQMU4TcProps_PaidToThirdParty);
            AddFormFieldData("sQMU4TcProps_Payer", GetPayer(dataLoan.sQMU4TcProps_Payer));

            AddFormFieldData("sQMRecF", dataLoan.sQMRecF_rep);
            AddFormFieldData("sQMRecF_QMAmount", dataLoan.sQMRecF_QMAmount_rep);
            AddFormFieldData("sQMRecFProps_Apr", dataLoan.sQMRecFProps_Apr);
            AddFormFieldData("sQMRecFProps_Aff", dataLoan.sQMRecFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMRecFProps_TrdPty", dataLoan.sQMRecFProps_PaidToThirdParty);
            AddFormFieldData("sQMRecFProps_Payer", GetPayer(dataLoan.sQMRecFProps_Payer));

            AddFormFieldData("sQMCountyRtc", dataLoan.sQMCountyRtc_rep);
            AddFormFieldData("sQMCountyRtc_QMAmount", dataLoan.sQMCountyRtc_QMAmount_rep);
            AddFormFieldData("sQMCountyRtcProps_Apr", dataLoan.sQMCountyRtcProps_Apr);
            AddFormFieldData("sQMCountyRtcProps_Aff", dataLoan.sQMCountyRtcProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMCountyRtcProps_TrdPty", dataLoan.sQMCountyRtcProps_PaidToThirdParty);
            AddFormFieldData("sQMCountyRtcProps_Payer", GetPayer(dataLoan.sQMCountyRtcProps_Payer));

            AddFormFieldData("sQMStateRtc", dataLoan.sQMStateRtc_rep);
            AddFormFieldData("sQMStateRtc_QMAmount", dataLoan.sQMStateRtc_QMAmount_rep);
            AddFormFieldData("sQMStateRtcProps_Apr", dataLoan.sQMStateRtcProps_Apr);
            AddFormFieldData("sQMStateRtcProps_Aff", dataLoan.sQMStateRtcProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMStateRtcProps_TrdPty", dataLoan.sQMStateRtcProps_PaidToThirdParty);
            AddFormFieldData("sQMStateRtcProps_Payer", GetPayer(dataLoan.sQMStateRtcProps_Payer));

            AddFormFieldData("sQMU1GovRtcDesc", dataLoan.sQMU1GovRtcDesc);
            AddFormFieldData("sQMU1GovRtc", dataLoan.sQMU1GovRtc_rep);
            AddFormFieldData("sQMU1GovRtc_QMAmount", dataLoan.sQMU1GovRtc_QMAmount_rep);
            AddFormFieldData("sQMU1GovRtcProps_Apr", dataLoan.sQMU1GovRtcProps_Apr);
            AddFormFieldData("sQMU1GovRtcProps_Aff", dataLoan.sQMU1GovRtcProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU1GovRtcProps_TrdPty", dataLoan.sQMU1GovRtcProps_PaidToThirdParty);
            AddFormFieldData("sQMU1GovRtcProps_Payer", GetPayer(dataLoan.sQMU1GovRtcProps_Payer));

            AddFormFieldData("sQMU2GovRtcDesc", dataLoan.sQMU2GovRtcDesc);
            AddFormFieldData("sQMU2GovRtc", dataLoan.sQMU2GovRtc_rep);
            AddFormFieldData("sQMU2GovRtc_QMAmount", dataLoan.sQMU2GovRtc_QMAmount_rep);
            AddFormFieldData("sQMU2GovRtcProps_Apr", dataLoan.sQMU2GovRtcProps_Apr);
            AddFormFieldData("sQMU2GovRtcProps_Aff", dataLoan.sQMU2GovRtcProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU2GovRtcProps_TrdPty", dataLoan.sQMU2GovRtcProps_PaidToThirdParty);
            AddFormFieldData("sQMU2GovRtcProps_Payer", GetPayer(dataLoan.sQMU2GovRtcProps_Payer));

            AddFormFieldData("sQMU3GovRtcDesc", dataLoan.sQMU3GovRtcDesc);
            AddFormFieldData("sQMU3GovRtc", dataLoan.sQMU3GovRtc_rep);
            AddFormFieldData("sQMU3GovRtc_QMAmount", dataLoan.sQMU3GovRtc_QMAmount_rep);
            AddFormFieldData("sQMU3GovRtcProps_Apr", dataLoan.sQMU3GovRtcProps_Apr);
            AddFormFieldData("sQMU3GovRtcProps_Aff", dataLoan.sQMU3GovRtcProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU3GovRtcProps_TrdPty", dataLoan.sQMU3GovRtcProps_PaidToThirdParty);
            AddFormFieldData("sQMU3GovRtcProps_Payer", GetPayer(dataLoan.sQMU3GovRtcProps_Payer));

            AddFormFieldData("sQMPestInspectF", dataLoan.sQMPestInspectF_rep);
            AddFormFieldData("sQMPestInspectF_QMAmount", dataLoan.sQMPestInspectF_QMAmount_rep);
            AddFormFieldData("sQMPestInspectFProps_Apr", dataLoan.sQMPestInspectFProps_Apr);
            AddFormFieldData("sQMPestInspectFProps_Aff", dataLoan.sQMPestInspectFProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMPestInspectFProps_TrdPty", dataLoan.sQMPestInspectFProps_PaidToThirdParty);
            AddFormFieldData("sQMPestInspectFProps_Payer", GetPayer(dataLoan.sQMPestInspectFProps_Payer));

            AddFormFieldData("sQMU1ScDesc", dataLoan.sQMU1ScDesc);
            AddFormFieldData("sQMU1Sc", dataLoan.sQMU1Sc_rep);
            AddFormFieldData("sQMU1Sc_QMAmount", dataLoan.sQMU1Sc_QMAmount_rep);
            AddFormFieldData("sQMU1ScProps_Apr", dataLoan.sQMU1ScProps_Apr);
            AddFormFieldData("sQMU1ScProps_Aff", dataLoan.sQMU1ScProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU1ScProps_TrdPty", dataLoan.sQMU1ScProps_PaidToThirdParty);
            AddFormFieldData("sQMU1ScProps_Payer", GetPayer(dataLoan.sQMU1ScProps_Payer));

            AddFormFieldData("sQMU2ScDesc", dataLoan.sQMU2ScDesc);
            AddFormFieldData("sQMU2Sc", dataLoan.sQMU2Sc_rep);
            AddFormFieldData("sQMU2Sc_QMAmount", dataLoan.sQMU2Sc_QMAmount_rep);
            AddFormFieldData("sQMU2ScProps_Apr", dataLoan.sQMU2ScProps_Apr);
            AddFormFieldData("sQMU2ScProps_Aff", dataLoan.sQMU2ScProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU2ScProps_TrdPty", dataLoan.sQMU2ScProps_PaidToThirdParty);
            AddFormFieldData("sQMU2ScProps_Payer", GetPayer(dataLoan.sQMU2ScProps_Payer));

            AddFormFieldData("sQMU3ScDesc", dataLoan.sQMU3ScDesc);
            AddFormFieldData("sQMU3Sc", dataLoan.sQMU3Sc_rep);
            AddFormFieldData("sQMU3Sc_QMAmount", dataLoan.sQMU3Sc_QMAmount_rep);
            AddFormFieldData("sQMU3ScProps_Apr", dataLoan.sQMU3ScProps_Apr);
            AddFormFieldData("sQMU3ScProps_Aff", dataLoan.sQMU3ScProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU3ScProps_TrdPty", dataLoan.sQMU3ScProps_PaidToThirdParty);
            AddFormFieldData("sQMU3ScProps_Payer", GetPayer(dataLoan.sQMU3ScProps_Payer));

            AddFormFieldData("sQMU4ScDesc", dataLoan.sQMU4ScDesc);
            AddFormFieldData("sQMU4Sc", dataLoan.sQMU4Sc_rep);
            AddFormFieldData("sQMU4Sc_QMAmount", dataLoan.sQMU4Sc_QMAmount_rep);
            AddFormFieldData("sQMU4Sc_Apr", dataLoan.sQMU4ScProps_Apr);
            AddFormFieldData("sQMU4Sc_Aff", dataLoan.sQMU4ScProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU4Sc_TrdPty", dataLoan.sQMU4ScProps_PaidToThirdParty);
            AddFormFieldData("sQMU4Sc_Payer", GetPayer(dataLoan.sQMU4ScProps_Payer));

            AddFormFieldData("sQMU5ScDesc", dataLoan.sQMU5ScDesc);
            AddFormFieldData("sQMU5Sc", dataLoan.sQMU5Sc_rep);
            AddFormFieldData("sQMU5Sc_QMAmount", dataLoan.sQMU5Sc_QMAmount_rep);
            AddFormFieldData("sQMU5Sc_Apr", dataLoan.sQMU5ScProps_Apr);
            AddFormFieldData("sQMU5Sc_Aff", dataLoan.sQMU5ScProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU5Sc_TrdPty", dataLoan.sQMU5ScProps_PaidToThirdParty);
            AddFormFieldData("sQMU5Sc_Payer", GetPayer(dataLoan.sQMU5ScProps_Payer));

            AddFormFieldData("sQMTotFeeAmount", dataLoan.sQMTotFeeAmount_rep);
            AddFormFieldData("sQMTotFinFeeAmount", dataLoan.sQMTotFinFeeAmount_rep);
        }
    }

    public class CQMPointsAndFeesPDF_3 : AbstractQMPointsAndFeesPDF
    {
        public override string PdfFile
        {
            get { return "QMPointsAndFeesAddendum.pdf"; }
        }

        public override string Description
        {
            get { return "Page 3"; }
        }

        public override bool IsVisible
        {
            get { return DataLoan.IsRequireInitialFeesWorksheetAddendum; }
        }

        protected override void ApplyData(DataAccess.CPageData dataLoan, DataAccess.CAppData dataApp)
        {
            dataLoan.SetFormatTarget(DataAccess.FormatTarget.PrintoutImportantFields);

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sTodayD", dataLoan.sTodayD_rep);

            AddFormFieldData("sQMU3RsrvDesc", dataLoan.sQMU3RsrvDesc);
            AddFormFieldData("sQMU3Rsrv", dataLoan.sQMU3Rsrv_rep);
            AddFormFieldData("sQMU3Rsrv_QMAmount", dataLoan.sQMU3Rsrv_QMAmount_rep);
            AddFormFieldData("sQMU3RsrvProps_Apr", dataLoan.sQMU3RsrvProps_Apr);
            AddFormFieldData("sQMU3RsrvProps_Aff", dataLoan.sQMU3RsrvProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU3RsrvProps_TrdPty", dataLoan.sQMU3RsrvProps_PaidToThirdParty);
            AddFormFieldData("sQMU3RsrvProps_Payer", GetPayer(dataLoan.sQMU3RsrvProps_Payer));

            AddFormFieldData("sQMU4RsrvDesc", dataLoan.sQMU4RsrvDesc);
            AddFormFieldData("sQMU4Rsrv", dataLoan.sQMU4Rsrv_rep);
            AddFormFieldData("sQMU4Rsrv_QMAmount", dataLoan.sQMU4Rsrv_QMAmount_rep);
            AddFormFieldData("sQMU4RsrvProps_Apr", dataLoan.sQMU4RsrvProps_Apr);
            AddFormFieldData("sQMU4RsrvProps_Aff", dataLoan.sQMU4RsrvProps_ThisPartyIsAffiliate);
            AddFormFieldData("sQMU4RsrvProps_TrdPty", dataLoan.sQMU4RsrvProps_PaidToThirdParty);
            AddFormFieldData("sQMU4RsrvProps_Payer", GetPayer(dataLoan.sQMU4RsrvProps_Payer));
        }
    }

}
