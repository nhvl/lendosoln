﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.Pdf.Async
{
    /// <summary>
    /// Encapsulates data for an asynchronous PDF generation response.
    /// </summary>
    public partial class AsyncPdfGenerationResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncPdfGenerationResult"/> class.
        /// </summary>
        /// <param name="status">The status of the generation result.</param>
        /// <param name="fileDbKey">The FileDB key of the generated PDF.</param>
        /// <param name="errorMessage">The error message for the generation, if any.</param>
        public AsyncPdfGenerationResult(LendersOffice.Pdf.Async.AsyncPdfGenerationStatus status, System.Guid? fileDbKey, string errorMessage)
        {
            this.Status = status;
            this.FileDbKey = fileDbKey;
            this.ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Gets the status of the generation result.
        /// </summary>
        public LendersOffice.Pdf.Async.AsyncPdfGenerationStatus Status { get; }

        /// <summary>
        /// Gets the FileDB key of the generated PDF.
        /// </summary>
        public System.Guid? FileDbKey { get; }

        /// <summary>
        /// Gets the error message for the generation, if any.
        /// </summary>
        public string ErrorMessage { get; }
    }
}