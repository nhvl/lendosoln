﻿namespace LendersOffice.Pdf.Async
{
    using System;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Represents a single PDF item to print.
    /// </summary>
    public class PdfPrintItem 
    {
        /// <summary>
        /// Gets or sets the type of the item.
        /// </summary>
        [XmlAttribute("t")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the ID of the item.
        /// </summary>
        [XmlAttribute("id")]
        public string ID { get; set; }

        /// <summary>
        /// Gets or sets the size of the page for the item.
        /// </summary>
        [XmlAttribute("p")]
        public string PageSize { get; set; }

        /// <summary>
        /// Gets or sets the application ID of the item.
        /// </summary>
        [XmlAttribute("appid")]
        public Guid ApplicationID { get; set; }

        /// <summary>
        /// Gets or sets the record ID of the item.
        /// </summary>
        [XmlAttribute("rid")]
        public Guid RecordID { get; set; }

        /// <summary>
        /// Gets or sets the record ID of the item.
        /// </summary>
        [XmlAttribute("borrtype")]
        public string BorrType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether ULIs are 
        /// added to the item.
        /// </summary>
        [XmlAttribute("isadduli")]
        public string IsAddUli { get; set; }
    }
}
