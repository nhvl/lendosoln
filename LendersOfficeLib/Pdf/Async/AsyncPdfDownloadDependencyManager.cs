﻿namespace LendersOffice.Pdf.Async
{
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// Registers the dependencies necessary for async
    /// PDF downloads on a page.
    /// </summary>
    public class AsyncPdfDownloadDependencyManager
    {
        /// <summary>
        /// Registers the dependencies for the page.
        /// </summary>
        /// <param name="page">
        /// The page to register dependencies.
        /// </param>
        /// <param name="includeSimplePopupDependencies">
        /// True to include dependencies for the simple popup
        /// library, false otherwise.
        /// </param>
        public void RegisterDependencies(BasePage page, bool includeSimplePopupDependencies = true)
        {
            if (includeSimplePopupDependencies)
            {
                page.RegisterJsScript("jquery.tmpl.js");
                page.RegisterJsScript("SimplePopups.js");
            }
            
            page.RegisterJsScript("LqbAsyncPdfDownloadHelper.js");

            page.RegisterJsGlobalVariables("SynchronousPdfFallbackQueryStringParameterName", ConstApp.SynchronousPdfFallbackQueryStringParameterName);
            page.RegisterJsGlobalVariables("UseSynchronousPdfGenerationFallback", ConstStage.UseSynchronousPdfGenerationFallback);

            page.RegisterJsGlobalVariables("PdfGenerationBackgroundJobPollingIntervalSeconds", ConstStage.PdfGenerationBackgroundJobPollingIntervalSeconds);
            page.RegisterJsGlobalVariables("PdfGenerationBackgroundJobMaxPollingAttempts", ConstStage.PdfGenerationBackgroundJobMaxPollingAttempts);
        }
    }
}
