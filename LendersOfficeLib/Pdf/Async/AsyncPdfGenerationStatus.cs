﻿namespace LendersOffice.Pdf.Async
{
    /// <summary>
    /// Represents the possible statuses for an async
    /// PDF generation job.
    /// </summary>
    public enum AsyncPdfGenerationStatus
    {
        Processing = 0,
        Complete = 1,
        Error = 2
    }
}
