﻿namespace LendersOffice.Pdf.Async
{
    using System;
    using System.Collections;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Wraps a set of PDF items to print with metadata,
    /// including loan ID and print options.
    /// </summary>
    [XmlRoot("PdfPrintList")]
    public class PdfPrintList 
    {
        /// <summary>
        /// The list of items in the print list.
        /// </summary>
        private ArrayList itemList = new ArrayList();

        /// <summary>
        /// Initializes a new instance of the <see cref="PdfPrintList"/> class.
        /// </summary>
        public PdfPrintList()
        {
        }

        /// <summary>
        /// Gets or sets the loan ID for the print list.
        /// </summary>
        [XmlAttribute("sLId")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "*", Justification = "Legacy code copied from batch print page.")]
        public Guid sLId { get; set; }

        /// <summary>
        /// Gets or sets the print options for the print list.
        /// </summary>
        [XmlAttribute("options")]
        public string PrintOptions { get; set; }

        /// <summary>
        /// Gets the list of print items.
        /// </summary>
        [XmlArray]
        [XmlArrayItem("items", typeof(PdfPrintItem))]
        public ArrayList ItemList 
        {
            get { return this.itemList; }
        }
    }
}
