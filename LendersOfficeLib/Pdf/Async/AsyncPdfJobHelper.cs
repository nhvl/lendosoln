﻿namespace LendersOffice.Pdf.Async
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.BackgroundJobs;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Provides utility functionality for submitting 
    /// PDF file generation jobs.
    /// </summary>
    public class AsyncPdfJobHelper
    {
        /// <summary>
        /// Creates a background job to generate a single PDF.
        /// </summary>
        /// <param name="principal">
        /// The user generating the PDF.
        /// </param>
        /// <param name="pdfName">
        /// The name of the PDF.
        /// </param>
        /// <param name="pdfArguments">
        /// The arguments to supply to the PDF.
        /// </param>
        /// <param name="pdfPassword">
        /// The password for the PDF, if specified.
        /// </param>
        /// <returns>
        /// The ID of the background job.
        /// </returns>
        public Guid CreateSinglePdfJob(AbstractUserPrincipal principal, string pdfName, NameValueCollection pdfArguments, string pdfPassword)
        {
            Guid encryptionKeyId;
            var encryptedPassword = this.GenerateEncryptedPdfPassword(pdfPassword, out encryptionKeyId);

            // 11/02/2018 - JE - OPM 474918 - This is not an optimal solution for getting the loan ID since it requires the dev who wrote the link to the
            // the PDF handler to remember to include the loan ID in the querystring.
            // In the future, we may want to audit all links leading to "pdf/*.aspx" and ensure that they are passing in the loan ID where applicable.
            Guid? loanId = pdfArguments["loanid"].ToNullable<Guid>(Guid.TryParse);

            var serializedData = this.GetSerializedData(pdfArguments);
            var settings = new PdfGenerationJobSettings(PdfGenerationJobType.SinglePdf, pdfName, serializedData, encryptionKeyId, encryptedPassword);
            return PdfGenerationJob.CreatePdfGenerationJob(principal, settings, loanId);
        }

        /// <summary>
        /// Creates a background job to generate a batch PDF.
        /// </summary>
        /// <param name="principal">
        /// The user generating the PDF.
        /// </param>
        /// <param name="printList">
        /// The list of items to print.
        /// </param>
        /// <param name="pdfPassword">
        /// The password for the batch PDFs.
        /// </param>
        /// <returns>
        /// The ID of the background job.
        /// </returns>
        public Guid CreateBatchPdfJob(AbstractUserPrincipal principal, PdfPrintList printList, string pdfPassword)
        {
            Guid encryptionKeyId;
            var encryptedPassword = this.GenerateEncryptedPdfPassword(pdfPassword, out encryptionKeyId);

            var serializedData = this.GetSerializedData(printList);
            var settings = new PdfGenerationJobSettings(PdfGenerationJobType.BatchPdf, null/*pdfName*/, serializedData, encryptionKeyId, encryptedPassword);
            return PdfGenerationJob.CreatePdfGenerationJob(principal, settings, printList.sLId);
        }

        /// <summary>
        /// Creates a background job to generate a Payment Statement PDF.
        /// </summary>
        /// <param name="principal">The user generating the PDF.</param>
        /// <param name="options">Options that affect PDF generation.</param>
        /// <returns>The ID of the background job.</returns>
        public Guid CreatePaymentStatementPdf(AbstractUserPrincipal principal, PaymentStatementPdfOptions options)
        {
            // 10/18/2018 - je - Payment Statement Generation does not really require a password.
            Guid encryptionKeyId;
            byte[] encryptedPassword = this.GenerateEncryptedPdfPassword(null, out encryptionKeyId);

            string serializedData = SerializationHelper.JsonNetSerialize(options);
            PdfGenerationJobSettings settings = new PdfGenerationJobSettings(PdfGenerationJobType.PaymentStatementPdf, null/*pdfName*/, serializedData, encryptionKeyId, encryptedPassword);
            return PdfGenerationJob.CreatePdfGenerationJob(principal, settings, options.LoanId);
        }

        /// <summary>
        /// Gets the status of a single PDF generation job by ID.
        /// </summary>
        /// <param name="jobId">
        /// The ID of the job.
        /// </param>
        /// <returns>
        /// The generation result.
        /// </returns>
        public AsyncPdfGenerationResult GetJobStatus(Guid jobId)
        {
            var job = PdfGenerationJob.GetPdfGenerationJob(jobId);
            switch (job.ProcessingStatus)
            {
                case ProcessingStatus.Invalid:
                    return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Error, fileDbKey: null, errorMessage: null);
                case ProcessingStatus.Incomplete:
                case ProcessingStatus.Processing:
                    return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Processing, fileDbKey: null, errorMessage: null);
                case ProcessingStatus.Completed:
                    return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Complete, fileDbKey: job.ResultFileDbKey, errorMessage: null);
                default:
                    throw new UnhandledEnumException(job.ProcessingStatus);
            }
        }

        /// <summary>
        /// Gets PDF arguments from serialized data.
        /// </summary>
        /// <param name="serializedData">
        /// The serialized data.
        /// </param>
        /// <returns>
        /// The PDF arguments.
        /// </returns>
        public NameValueCollection GetPdfArgumentsFromSerializedData(string serializedData)
        {
            var pdfArguments = new NameValueCollection();

            var dict = SerializationHelper.JsonNetDeserialize<Dictionary<string, string>>(serializedData);
            foreach (var pair in dict)
            {
                pdfArguments[pair.Key] = pair.Value;
            }

            return pdfArguments;
        }

        /// <summary>
        /// Gets a PDF print list from serialized data.
        /// </summary>
        /// <param name="serializedData">
        /// The serialized data.
        /// </param>
        /// <returns>
        /// The PDF print list.
        /// </returns>
        public PdfPrintList GetPdfPrintListFromSerializedData(string serializedData)
        {
            return SerializationHelper.XmlDeserialize<PdfPrintList>(serializedData);
        }

        /// <summary>
        /// Gets payment statement options from serialized data.
        /// </summary>
        /// <param name="serializedData">The serialized data.</param>
        /// <returns>An instance of <see cref="PaymentStatementPdfOptions"/>.</returns>
        public PaymentStatementPdfOptions GetPaymentStatementPdfOptionsFromSerializedData(string serializedData)
        {
            return SerializationHelper.JsonNetDeserialize<PaymentStatementPdfOptions>(serializedData);
        }

        /// <summary>
        /// Generates the encrypted PDF password.
        /// </summary>
        /// <param name="pdfPassword">
        /// The plaintext PDF password.
        /// </param>
        /// <param name="encryptionKeyId">
        /// The encryption key ID for the generated password.
        /// </param>
        /// <returns>
        /// The encrypted PDF password.
        /// </returns>
        private byte[] GenerateEncryptedPdfPassword(string pdfPassword, out Guid encryptionKeyId)
        {
            var factory = GenericLocator<IEncryptionKeyDriverFactory>.Factory;
            var driver = factory.Create();

            var encryptionKey = driver.GenerateKey().Key;
            encryptionKeyId = encryptionKey.Value;

            if (string.IsNullOrEmpty(pdfPassword))
            {
                return null;
            }

            return Drivers.Encryption.EncryptionHelper.EncryptString(encryptionKey, pdfPassword);
        }

        /// <summary>
        /// Gets the serialized version of PDF arguments.
        /// </summary>
        /// <param name="pdfArguments">
        /// The PDF arguments.
        /// </param>
        /// <returns>
        /// The serialized value.
        /// </returns>
        private string GetSerializedData(NameValueCollection pdfArguments)
        {
            var dict = pdfArguments.AllKeys.ToDictionary(key => key, key => pdfArguments[key]);
            return SerializationHelper.JsonNetSerialize(dict);
        }

        /// <summary>
        /// Gets the serialized version of a PDF print list.
        /// </summary>
        /// <param name="printList">
        /// The PDF print list.
        /// </param>
        /// <returns>
        /// The serialized value.
        /// </returns>
        private string GetSerializedData(PdfPrintList printList)
        {
            return SerializationHelper.XmlSerialize(printList);
        }
    }
}
