﻿namespace LendersOffice.Pdf.Async
{
    using System;
    using System.Collections.Specialized;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Reflection;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.PdfGenerator;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides utility functionality for asynchronously generating PDF files.
    /// </summary>
    public class AsyncPdfGenerationHelper
    {
        /// <summary>
        /// The repository of PDF generators.
        /// </summary>
        private readonly PdfGeneratorRepository repository = new PdfGeneratorRepository();

        /// <summary>
        /// Generates a single PDF.
        /// </summary>
        /// <param name="pdfName">
        /// The name of the PDF.
        /// </param>
        /// <param name="pdfArguments">
        /// The arguments for the PDF.
        /// </param>
        /// <param name="password">
        /// The password for the PDF.
        /// </param>
        /// <returns>
        /// An asynchronous PDF generation result.
        /// </returns>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Code extracted from legacy implementation with catch-all.")]
        public AsyncPdfGenerationResult GenerateSinglePdf(string pdfName, NameValueCollection pdfArguments, string password)
        {
            try
            {
                var pdfGenerator = this.repository.CreatePdfInstance(pdfName, pdfArguments);
                var fileDbKey = this.GeneratePdf(pdfGenerator, password);
                return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Complete, fileDbKey, errorMessage: null);
            }
            catch (InsufficientPdfPermissionException)
            {
                return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Error, fileDbKey: null, errorMessage: "Missing owner password to modify the pdf.");
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Error, fileDbKey: null, errorMessage: exc.UserMessage);
            }
            catch (TargetInvocationException exc)
            {
                // case 52601 : Handle permission error if the user doesn't have permission to access a protected field on the pdf
                if (exc.InnerException is PermissionException)
                {
                    Tools.LogErrorWithCriticalTracking(exc.InnerException);
                    return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Error, fileDbKey: null, errorMessage: ((PermissionException)exc.InnerException).UserMessage);
                }
                else
                {
                    Tools.LogErrorWithCriticalTracking(exc);
                    return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Error, fileDbKey: null, errorMessage: exc.Message);
                }
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Error, fileDbKey: null, errorMessage: exc.Message);
            }
        }

        /// <summary>
        /// Generates a batch PDF.
        /// </summary>
        /// <param name="printList">
        /// The list of PDFs to print.
        /// </param>
        /// <param name="password">
        /// The password for the PDFs.
        /// </param>
        /// <returns>
        /// An asynchronous PDF generation result.
        /// </returns>
        public AsyncPdfGenerationResult GenerateBatchPdf(PdfPrintList printList, string password)
        {
            var helper = new BatchPdfHelper();
            var pdfGenerator = helper.CreateGenericBatchPdfInstance(printList);

            for (int attempts = 0; attempts < 5; attempts++)
            {
                try
                {
                    var fileDbKey = this.GeneratePdf(pdfGenerator, password);
                    return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Complete, fileDbKey, errorMessage: null);
                }
                catch (IOException exc)
                {
                    // 8/23/2006 dd - I will retry 4 times before throw the exception.
                    if (attempts == 4)
                    {
                        return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Error, fileDbKey: null, errorMessage: exc.Message);
                    }
                    else
                    {
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(4000); // Sleep for 4 seconds.
                    }
                }
            }

            return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Error, fileDbKey: null, errorMessage: ErrorMessages.Generic);
        }

        /// <summary>
        /// Generates a Payment Statement PDF.
        /// </summary>
        /// <param name="options">Payment Statement PDF Options.</param>
        /// <returns>An asynchronous PDF generation result.</returns>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Catch-all maintains consistancy with other methods in this class.")]

        public AsyncPdfGenerationResult GeneratePaymentStatementPdf(PaymentStatementPdfOptions options)
        {
            try
            {
                PaymentStatementGenerator generator = new PaymentStatementGenerator(options);

                LocalFilePath path = generator.GeneratePdf();
                Guid fileDbKey = Guid.NewGuid();
                FileDBTools.WriteFile(E_FileDB.Normal, fileDbKey.ToString(), path.Value);
                Drivers.FileSystem.FileOperationHelper.Delete(path); // Cleanup temp file.

                return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Complete, fileDbKey, errorMessage: null);
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Error, fileDbKey: null, errorMessage: exc.UserMessage);
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                return new AsyncPdfGenerationResult(AsyncPdfGenerationStatus.Error, fileDbKey: null, errorMessage: exc.Message);
            }
        }

        /// <summary>
        /// Generates the PDF.
        /// </summary>
        /// <param name="pdfGenerator">
        /// The PDF generator.
        /// </param>
        /// <param name="password">
        /// The PDF password.
        /// </param>
        /// <returns>
        /// The FileDB key for the generated PDF.
        /// </returns>
        private Guid GeneratePdf(IPDFGenerator pdfGenerator, string password)
        {
            var filePath = TempFileUtils.NewTempFile();
            using (var stream = new FileStream(filePath.Value, FileMode.Create))
            {
                pdfGenerator.GeneratePDF(stream, string.Empty, password);
            }

            var fileDbKey = Guid.NewGuid();
            FileDBTools.WriteFile(E_FileDB.Normal, fileDbKey.ToString(), filePath.Value);

            // Cleanup temp file.
            Drivers.FileSystem.FileOperationHelper.Delete(filePath);
            return fileDbKey;
        }
    }
}
