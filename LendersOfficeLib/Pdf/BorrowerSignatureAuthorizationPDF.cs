using System;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using DataAccess;
using LendersOffice.Common;


namespace LendersOffice.Pdf
{
	public class CBorrowerSignatureAuthorizationPDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "BorrowerSignatureAuthorization.pdf"; }
        }

        public override string Description 
        {
            get { return "Borrower Authorization Form"; }
        }

		public override string EditLink 
		{
			get { return "/newlos/Disclosure/BorrowerAuthorizationForm.aspx"; }
		}

        protected override  void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.BorrSignatureAuthorization, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 

            string lenderInformation = Tools.FormatAddress(broker.CompanyName, broker.StreetAddr, broker.City, broker.State, broker.Zip);


            StringBuilder sb = new StringBuilder(30);
            sb.Append(dataApp.aBNm);
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                sb.AppendFormat(" & {0}", dataApp.aCNm);
            }

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("LenderInformation", lenderInformation);
            AddFormFieldData("BorrowerInformation", Tools.FormatAddress(sb.ToString(), dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);

        }



	}
}
