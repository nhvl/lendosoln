using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.PdfLayout;

namespace LendersOffice.Pdf
{
	public class CVA_26_6393PDF : AbstractLegalPDF
	{
        public override string Description 
        {
            get { return "Main Page"; }
        }

        public override string PdfFile 
        {
            get { return "VA-26-6393.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPrimaryEmploymentRecord aBPrimaryEmp = dataApp.aBEmpCollection.GetPrimaryEmp(false);
            IPrimaryEmploymentRecord aCPrimaryEmp = dataApp.aCEmpCollection.GetPrimaryEmp(false);

            AddFormFieldData("aAsstLiqTot", dataApp.aAsstLiqTot_rep);
            AddFormFieldData("aBAge", dataApp.aBAge_rep);
            AddFormFieldData("aBDependAges", dataApp.aBDependAges);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            if (null != aBPrimaryEmp) 
            {
                string str = "";
                if (aBPrimaryEmp.EmplmtLenInYrs_rep != "")
                    str += aBPrimaryEmp.EmplmtLenInYrs_rep + " yrs ";
                if (aBPrimaryEmp.EmplmtLenInMonths_rep != "")
                    str += aBPrimaryEmp.EmplmtLenInMonths_rep + " mths";

                AddFormFieldData("aBPrimaryEmpEmplmtLen", str);
                AddFormFieldData("aBPrimaryEmpJobTitle", aBPrimaryEmp.JobTitle);
            }
            AddFormFieldData("aCAge", dataApp.aCAge_rep);
            if (null != aCPrimaryEmp) 
            {
                string str = "";
                if (aCPrimaryEmp.EmplmtLenInYrs_rep != "")
                    str += aCPrimaryEmp.EmplmtLenInYrs_rep + " yrs ";
                if (aCPrimaryEmp.EmplmtLenInMonths_rep != "")
                    str += aCPrimaryEmp.EmplmtLenInMonths_rep + " mths";
                AddFormFieldData("aCPrimaryEmpEmplmtLen", str);
                AddFormFieldData("aCPrimaryEmpJobTitle", aCPrimaryEmp.JobTitle);
            }
            AddFormFieldData("aPresTotHExp", dataApp.aPresTotHExp_rep);
            AddFormFieldData("aVaBEmplmtI", dataApp.aVaBEmplmtI_rep);
            AddFormFieldData("aVaBFedITax", dataApp.aVaBFedITax_rep);
            AddFormFieldData("aVaBNetI", dataApp.aVaBNetI_rep);
            AddFormFieldData("aVaBOITax", dataApp.aVaBOITax_rep);
            AddFormFieldData("aVaBONetI", dataApp.aVaBONetI_rep);
            AddFormFieldData("aVaBSsnTax", dataApp.aVaBSsnTax_rep);
            AddFormFieldData("aVaBStateITax", dataApp.aVaBStateITax_rep);
            AddFormFieldData("aVaBTakehomeEmplmtI", dataApp.aVaBTakehomeEmplmtI_rep);
            AddFormFieldData("aVaBTotITax", dataApp.aVaBTotITax_rep);
            AddFormFieldData("aVaCEmplmtI", dataApp.aVaCEmplmtI_rep);
            AddFormFieldData("aVaCFedITax", dataApp.aVaCFedITax_rep);
            AddFormFieldData("aVaCNetI", dataApp.aVaCNetI_rep);
            AddFormFieldData("aVaCOITax", dataApp.aVaCOITax_rep);
            AddFormFieldData("aVaCONetI", dataApp.aVaCONetI_rep);
            AddFormFieldData("aVaCSsnTax", dataApp.aVaCSsnTax_rep);
            AddFormFieldData("aVaCStateITax", dataApp.aVaCStateITax_rep);
            AddFormFieldData("aVaCTakehomeEmplmtI", dataApp.aVaCTakehomeEmplmtI_rep);
            AddFormFieldData("aVaCTotITax", dataApp.aVaCTotITax_rep);
            AddFormFieldData("aVaCrRecordSatisfyTri", dataApp.aVaCrRecordSatisfyTri);
            AddFormFieldData("aVaFamilySupportBal", dataApp.aVaFamilySupportBal_rep);
            AddFormFieldData("aVaFamilySuportGuidelineAmt", dataApp.aVaFamilySuportGuidelineAmt_rep);
            AddFormFieldData("aVaLiaMonTotDeducted", dataApp.aVaLiaMonTotDeducted_rep);
            AddFormFieldData("aVaNetEffectiveI", dataApp.aVaNetEffectiveI_rep);
            AddFormFieldData("aVaNetI", dataApp.aVaNetI_rep);
            AddFormFieldData("aVaONetI", dataApp.aVaONetI_rep);
            AddFormFieldData("aVaONetIDesc", dataApp.aVaONetIDesc);
            AddFormFieldData("aVaRatio", dataApp.aVaRatio_rep);
            AddFormFieldData("aVaTakehomeEmplmtI", dataApp.aVaTakehomeEmplmtI_rep);
            AddFormFieldData("aVaTotEmplmtI", dataApp.aVaTotEmplmtI_rep);
            AddFormFieldData("aVaTotITax", dataApp.aVaTotITax_rep);
            AddFormFieldData("aVaUtilityIncludedTri", dataApp.aVaUtilityIncludedTri);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sTermInYr", dataLoan.sTermInYr_rep);
            AddFormFieldData("sVaCashdwnPmt", dataLoan.sVaCashdwnPmt_rep);
            AddFormFieldData("sVaMaintainAssessPmt", dataLoan.sVaMaintainAssessPmt_rep);
            AddFormFieldData("sVaProHazIns", dataLoan.sVaProHazIns_rep);
            AddFormFieldData("sVaProMaintenancePmt", dataLoan.sVaProMaintenancePmt_rep);
            AddFormFieldData("sVaProMonthlyPmt", dataLoan.sVaProMonthlyPmt_rep);
            AddFormFieldData("sVaProRealETx", dataLoan.sVaProRealETx_rep);
            AddFormFieldData("sVaProThisMPmt", dataLoan.sVaProThisMPmt_rep);
            AddFormFieldData("sVaProUtilityPmt", dataLoan.sVaProUtilityPmt_rep);
            AddFormFieldData("sVaSpecialAssessPmt", dataLoan.sVaSpecialAssessPmt_rep);

            AddFormFieldData("aVaLiaBalTotExisting", dataApp.aVaLiaBalTotExisting_rep);

            AddFormFieldData("aVaLiaMonTotExistingAndOpNegCf", dataApp.aVaLiaMonTotExistingAndOpNegCf_rep);
            
            AddFormFieldData("aVaLMeetCrStandardTri", dataApp.aVaLMeetCrStandardTri);
            AddFormFieldData("sVaProMaintenanceAndUtilityPmt", dataLoan.sVaProMaintenanceAndUtilityPmt_rep);

            var subCollection = dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.JobRelatedExpense);
            decimal pmt = 0.0M;
            bool usedInRatio = true;
            foreach (ILiabilityJobExpense record in subCollection) 
            {
                usedInRatio = !record.NotUsedInRatio;
                pmt += record.Pmt;

            }
            if (pmt > 0) 
            {
                AddFormFieldData("JobRelatedPmt", dataLoan.m_convertLos.ToMoneyString(pmt, FormatDirection.ToRep));
                AddFormFieldData("JobRelatedUsedInRatio", usedInRatio);
            }

            int index = 0;
            var alimony = dataApp.aLiaCollection.GetAlimony(false);
            if (null != alimony && alimony.Pmt_rep != "") 
            {
                AddFormFieldData("Liability" + index, "Alimony");
                AddFormFieldData("LiabilityUsedInRatio" + index, !alimony.NotUsedInRatio);
                AddFormFieldData("LiabilityPmt" + index, alimony.Pmt_rep);
                index++;
            }

            var childSupport = dataApp.aLiaCollection.GetChildSupport(false);
            if (childSupport != null && childSupport.Pmt_rep != string.Empty)
            {
                AddFormFieldData("Liability" + index, "Child Support");
                AddFormFieldData("LiabilityUsedInRatio" + index, !childSupport.NotUsedInRatio);
                AddFormFieldData("LiabilityPmt" + index, childSupport.Pmt_rep);
                index++;
            }

            if (dataApp.aOpNegCf_rep != "")
            {
                AddFormFieldData("Liability" + index, "NET NEGATIVE REO CASH FLOW");
                AddFormFieldData("LiabilityUsedInRatio" + index, true);
                AddFormFieldData("LiabilityPmt" + index, dataApp.aOpNegCf_rep);
                index++;

            }
            int count = dataApp.aLiaCollection.CountRegular;

            if ((count + index) > 7) 
            {
                AddFormFieldData("Liability3", "SEE ATTACHMENT");
            } 
            else 
            {
                subCollection = dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Regular);
                foreach (ILiabilityRegular record in subCollection) 
                {
                    AddFormFieldData("Liability" + index, record.ComNm);
                    AddFormFieldData("LiabilityUsedInRatio" + index, !record.NotUsedInRatio);
                    AddFormFieldData("LiabilityPmt" + index, record.Pmt_rep);
                    AddFormFieldData("LiabilityBal" + index, record.Bal_rep);
                    index++;
                }
            }
            AddFormFieldData("aFHABCaivrsNum", dataApp.aFHABCaivrsNum);
            AddFormFieldData("aFHACCaivrsNum", dataApp.aFHACCaivrsNum);

            AddFormFieldData("aVALAnalysisValue", dataApp.aVALAnalysisValue_rep);
            AddFormFieldData("aVALAnalysisExpirationD", dataApp.aVALAnalysisExpirationD_rep);
            AddFormFieldData("aVALAnalysisEconomicLifeYrs", dataApp.aVALAnalysisEconomicLifeYrs_rep);

            AddFormFieldData("sVaRecommendAppApprovedT", dataLoan.sVaRecommendAppApprovedT);
            AddFormFieldData("sVAFinalActionT", dataLoan.sVAFinalActionT);

            var remarksField = this.GetRemarksField();
            var remarks = PdfFormContainer.DoesValueFitInField(dataApp.aVaLAnalysisN, remarksField)
                ? dataApp.aVaLAnalysisN
                : "Please see additional sheet.";
            AddFormFieldData("aVaLAnalysisN", remarks);
        }

        public PdfField GetRemarksField()
        {
            return this.PdfFormLayout.FieldList.FirstOrDefault(f => f.Name == "aVaLAnalysisN");
        }
	}

    public class CVA_26_6393ContPDF : AbstractLegalPDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-6393Cont.pdf"; }
        }

        public override string Description 
        {
            get { return "Liabilities Continuation Sheet"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            int index = 0;
            var subCollection = dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Regular);
            foreach (ILiabilityRegular record in subCollection) 
            {
                if (index == 41) 
                {
                    index = 0;
                    AddNewPage();
                }

                AddFormFieldData("Liability" + index, record.ComNm);
                AddFormFieldData("LiabilityUsedInRatio" + index, !record.NotUsedInRatio);
                AddFormFieldData("LiabilityPmt" + index, record.Pmt_rep);
                AddFormFieldData("LiabilityBal" + index, record.Bal_rep);
                index++;
            }
        }

        public override bool IsVisible 
        {
            get {
                int count = m_dataApp.aLiaCollection.CountRegular;
                int extra = 0;
                var alimony = m_dataApp.aLiaCollection.GetAlimony(false);
                if (alimony != null && alimony.Pmt_rep != "" && alimony.Pmt_rep != "$0.00")
                {
                    extra++;
                }
                var childSupport = m_dataApp.aLiaCollection.GetChildSupport(false);
                if (childSupport != null && childSupport.Pmt_rep != string.Empty && childSupport.Pmt_rep != "$0.00")
                {
                    extra++;
                }
                if (m_dataApp.aOpNegCf_rep != "" && m_dataApp.aOpNegCf_rep != "$0.00")
                {
                    extra++;
                }


                return (count + extra) > 7;
            }
        }
    }

    public class CVA_26_6393RemarksPDF : AbstractLegalPDF
    {
        public override string PdfFile => "VA-26-6393Remarks.pdf";

        public override string Description => "Remarks Continuation Sheet";

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aVaLAnalysisN", dataApp.aVaLAnalysisN);
        }

        public override bool IsVisible
        {
            get
            {
                if (m_dataApp == null)
                {
                    return false;
                }

                var mainPdf = new CVA_26_6393PDF();
                var remarksField = mainPdf.GetRemarksField();

                if (remarksField == null)
                {
                    return false;
                }

                return !PdfFormContainer.DoesValueFitInField(m_dataApp.aVaLAnalysisN, remarksField);
            }
        }
    }

    public class CVA_26_6393CompletePDF : AbstractBatchPDF 
    {

        public override string Description 
        {
            get { return "VA Loan Analysis (VA 26-6393)"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/VA/VALoanAnalysis.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] 
                { 
                    new CVA_26_6393PDF(), 
                    new CVA_26_6393ContPDF(),
                    new CVA_26_6393RemarksPDF()
                };
            }
        }
    }
}
