﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Xml;
    using DataAccess;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.PdfGenerator;
    using LendersOffice.Security;

    public class CLOAdminTaskListPDF : AbstractXsltPdf
    {
        protected override bool UsesDataLoan
        {
            get { return false; }
        }

        protected override void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp)
        {
            // Let's check permissions first. The user must be an LOAdmin user or a system user.
            var currentUser = PrincipalFactory.CurrentPrincipal;
            if (!this.CurrentUserCanViewPdf(currentUser))
            {
                throw new PermissionException("Only LOAdmin or system users are allowed to view this document.");
            }
            
            writer.WriteStartElement("TASKLIST");

            // If we ever change the method of task selection, make sure that each ID is distinct
            string taskIDsRaw = AutoExpiredTextCache.GetFromCache(Arguments["cacheId"]);

            if (taskIDsRaw == null)
            {
                throw CBaseException.GenericException("The task IDs are no longer in the cache");
            }

            string[] taskIDs = taskIDsRaw.Split(',');

            List<TaskTriggerTemplate> tasks;

            tasks = TaskTriggerTemplate.GetTaskTriggerTemplatesByBroker(new Guid(Arguments["brokerId"])).ToList();

            tasks.Sort((t1, t2) => t1.AutoTaskTemplateId.CompareTo(t2.AutoTaskTemplateId));

            // It would be great if we profiled this against other methods
            //   such as filtering out tasks beforehand
            // Or querying the database for each individual task
            foreach (string taskID in taskIDs) // O(m log n), where m is num tasks to get, n is num total tasks
            {
                var task = tasks.Find((t) => t.AutoTaskTemplateId == int.Parse(taskID));
                WriteTaskInformation(writer, dataLoan, task);
            }

            writer.WriteEndElement(); // </TASKLIST>
        }

        /// <summary>
        /// Determines whether the current user can view the PDF.
        /// </summary>
        /// <param name="currentUser">
        /// The user attempting to view the PDF.
        /// </param>
        /// <returns>
        /// True if the current user can view, false otherwise.
        /// </returns>
        private bool CurrentUserCanViewPdf(AbstractUserPrincipal currentUser)
        {
            if (currentUser == null)
            {
                return false;
            }

            return currentUser is InternalUserPrincipal || currentUser is SystemUserPrincipal;
        }

        private void WriteTaskInformation(XmlWriter writer, CPageData dataLoan, TaskTriggerTemplate task) // pass in the rows here, possibly
        {
            writer.WriteStartElement("Task");
            writer.WriteElementString("Trigger", task.TriggerName);
            writer.WriteElementString("TaskID", task.AutoTaskTemplateId.ToString());
            writer.WriteElementString("Subject", task.Subject.ToString());
            
            string dueDate = "";
            dueDate = task.DueDateFriendlyDescription;
            writer.WriteElementString("DueDate", dueDate);

            writer.WriteElementString("Assigned", task.AssignedUserFullName);
            writer.WriteElementString("Owner", task.OwnerFullName);

            writer.WriteEndElement(); // </Task>
        }

        public override string Description
        {
            get { return "LOAdmin Task List"; }
        }

        public override PDFPageSize PageSize
        {
            get { return PDFPageSize.Letter; }
        }

    }
}
