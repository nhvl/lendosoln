﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using ObjLib.PDFGenerator;
    using PdfGenerator;

    /// <summary>
    /// Handles generating a payment statement pdf with any additional custom pdfs attached.
    /// </summary>
    public class PaymentStatementGenerator
    {
        /// <summary>
        /// The broker id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// The loan id.
        /// </summary>
        private Guid loanId;

        /// <summary>
        /// The payment statement options.
        /// </summary>
        private PaymentStatementPdfOptions options;

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentStatementGenerator"/> class.
        /// </summary>
        /// <param name="options">The options.</param>
        public PaymentStatementGenerator(PaymentStatementPdfOptions options)
        {
            this.brokerId = options.BrokerId;
            this.loanId = options.LoanId;
            this.options = options;
        }

        /// <summary>
        /// Generates the payment statement pdf and returns a path to the file.
        /// </summary>
        /// <returns>The path to the pdf.</returns>
        public LocalFilePath GeneratePdf()
        {
            var pdfs = this.GetPdfs();
            var dependencies = this.GetLoanFieldDependenciesFromPdfs(pdfs);

            var loan = new CPageData(this.loanId, dependencies);
            loan.InitLoad();

            var batchPdf = new GenericBatchPDF();
            foreach (var pdf in pdfs)
            {
                pdf.DataLoan = loan;
                batchPdf.Add(pdf);
            }

            LocalFilePath path = TempFileUtils.NewTempFile();
            using (var stream = new FileStream(path.Value, FileMode.Create))
            {
                batchPdf.GeneratePDF(stream, null, null);
            }

            return path;
        }

        /// <summary>
        /// Gets the pdfs that are supposed to be included in the payment statement.
        /// </summary>
        /// <returns>An enumerable of the pdfs that are to be included in the payment statement.</returns>
        private IEnumerable<IPDFPrintItemGenerator> GetPdfs()
        {
            var pdfs = new List<IPDFPrintItemGenerator>();

            var paymentStatement = this.GetPaymentStatement();
            pdfs.Add(paymentStatement);

            if (this.options.AppendCustomPdfs)
            {
                foreach (var customPdfId in this.options.SelectedCustomPdfIds)
                {
                    var customPdf = this.GetCustomPdf(customPdfId);
                    pdfs.Add(customPdf);
                }
            }

            return pdfs;
        }

        /// <summary>
        /// Gets the loan dependencies from the given pdfs.
        /// </summary>
        /// <param name="pdfs">The pdf items.</param>
        /// <returns>The dependencies of the pdfs.</returns>
        private IEnumerable<string> GetLoanFieldDependenciesFromPdfs(IEnumerable<IPDFPrintItemGenerator> pdfs)
        {
            var dependencies = new HashSet<string>();

            foreach (var pdf in pdfs)
            {
                if (pdf.DependencyFields != null)
                {
                    dependencies.UnionWith(pdf.DependencyFields);
                }
            }

            return dependencies;
        }

        /// <summary>
        /// Gets the pdf to use for the payment statement.
        /// </summary>
        /// <returns>A payment statement pdf which is ready to be generated.</returns>
        private PaymentStatement GetPaymentStatement()
        {
            var arguments = new NameValueCollection();
            arguments["loanid"] = this.loanId.ToString();

            var paymentStatement = new PaymentStatement(this.options);
            paymentStatement.Arguments = arguments;

            return paymentStatement;
        }

        /// <summary>
        /// Gets a custom pdf instance for the given custom pdf id.
        /// </summary>
        /// <param name="customPdfId">The custom pdf id.</param>
        /// <returns>A custom pdf that is ready to be generated.</returns>
        private CCustomPDF GetCustomPdf(Guid customPdfId)
        {
            NameValueCollection arguments = new NameValueCollection(4);
            arguments["loanid"] = this.loanId.ToString();
            arguments["custompdfid"] = customPdfId.ToString();

            // Based on my research, it looks like custom PDFs are always defaulted to letter.
            arguments["pagesize"] = "0";
            arguments["applicationid"] = Guid.Empty.ToString();

            var customPdf = new CCustomPDF();
            customPdf.Arguments = arguments;

            return customPdf;
        }
    }
}
