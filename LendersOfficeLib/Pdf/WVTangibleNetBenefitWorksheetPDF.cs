﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CWVTangibleNetBenefitWorksheetPDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "WVTangibleNetBenefitWorksheet.pdf"; }
        }
        public override string Description
        {
            get { return "West Virginia Tangible Net Benefit Worksheet"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("aBNm_aCNm", dataApp.aBNm_aCNm);
            AddFormFieldData("sSpAddr_SingleLine", dataLoan.sSpAddr_SingleLine);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sFinMethT", dataLoan.sFinMethT);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            AddFormFieldData("sLtvR", dataLoan.sLtvR_rep);
            AddFormFieldData("sQualBottomR", dataLoan.sQualBottomR_rep);
            AddFormFieldData("sLPurposeT_rep", dataLoan.sLPurposeT_rep);
        }
    }
}