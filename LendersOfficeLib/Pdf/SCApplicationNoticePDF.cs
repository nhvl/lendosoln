﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CSCApplicationNoticePDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "SCApplicationNotice.pdf"; }
        }
        public override string Description
        {
            get { return "South Carolina Application Notice"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            CAgentFields loanOfficer = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("Agent_LoanOfficer_CompanyName", loanOfficer.CompanyName);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sSpAddr_MultiLine", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sLNm", dataLoan.sLNm);
        }
    }
}