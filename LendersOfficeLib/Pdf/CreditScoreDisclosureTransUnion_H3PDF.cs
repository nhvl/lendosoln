
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    public class CreditScoreDisclosureTransUnion_H3_1PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosureTransUnion_H3_1.pdf"; }
        }

        public override string Description
        {
            get { return "Page 1"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

            dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            if (BorrType == "C")
            {
                dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
            }

            AddFormFieldData("aNm" , dataApp.aNm);
            AddFormFieldData("aTransUnionCreatedD" , dataApp.aTransUnionCreatedD_rep);
            AddFormFieldData("aTransUnionFactors" , dataApp.aTransUnionFactors);
            AddFormFieldData("aTransUnionPercentile" , dataApp.aTransUnionPercentile_rep);
            AddFormFieldData("aTransUnionScore" , dataApp.aTransUnionScore_rep);
            AddFormFieldData("sTransUnionScoreFrom" , dataApp.LoanData.sTransUnionScoreFrom_rep);
            AddFormFieldData("sTransUnionScoreTo" , dataApp.LoanData.sTransUnionScoreTo_rep);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditTransUnion, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("TransUnionCreditScorePreparerInfo", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("aTransUnionSourceName", preparer.CompanyName);


        }
    }

    public class CreditScoreDisclosureTransUnion_H3_2PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosureTransUnion_H3_2.pdf"; }
        }

        public override string Description
        {
            get { return "Page 2"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

    public class CreditScoreDisclosureTransUnion_H3_3PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosureTransUnion_H3_3.pdf"; }
        }

        public override string Description
        {
            get { return "Page 3"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

    public class CreditScoreDisclosureTransUnion_H3PDF : AbstractBatchPDF
    {
        protected override E_AppPrintModeT AppPrintModeT
        {
            get { return E_AppPrintModeT.BorrowerAndSpouseSeparately; }
        }

        public override string EditLink
        {
            get { return "/newlos/Disclosure/CreditScoreDisclosure.aspx"; }
        }
        public override string Description
        {
            get
            {
                return "TransUnion Credit Score Disclosure - H-3";
            }
        }
        protected override AbstractPDFFile[] ChildPages
        {
            get 
            {
                return new AbstractPDFFile[] {
                    new CreditScoreDisclosureTransUnion_H3_1PDF(),
                    new CreditScoreDisclosureTransUnion_H3_2PDF(),
                    new CreditScoreDisclosureTransUnion_H3_3PDF(),

                };
            }
        }
    }
}
