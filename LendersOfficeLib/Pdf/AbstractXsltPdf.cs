﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Xml;
    using System.Xml.Xsl;
    using DataAccess;
    using ExpertPdf.HtmlToPdf;
    using AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.PdfGenerator;
    using PdfRasterizerLib;

    /// <summary>
    /// This class will be a base class for using Xslt to render xml -> HTML and from HTML - PDF.
    /// </summary>
    public abstract class AbstractXsltPdf : IPDFGenerator, IPDFPrintItem
    {
        public virtual PDFPageOrientation Orientation
        {
            get { return PDFPageOrientation.Portrait; }
        }

        /// <summary>
        /// Set this to false in your constructor if you are making an AbstractXsltPdf outside of LO
        /// </summary>
        protected virtual bool UsesDataLoan
        {
            get { return true; }
        }

        public bool HasPdfFormLayout => false;

        public LendersOffice.PdfLayout.PdfFormLayout PdfFormLayout
        {
            get { throw new NotImplementedException(); }
        }

        private PDFConversionOptions PdfConversionOptions { get; set; }

        protected virtual string XsltEmbeddedResourceLocation 
        {
            get
            {
                // 2/4/2011 dd - Automatically infer the embedded resource from the PDF class.
                // If the PDF print class has name of C{xxxx}PDF then the xslt file must be locate in the same
                // namespace as PDF print class with name  {xxxx}.xslt.
                // I assume name space is "LendersOffice.Pdf"
                return "LendersOffice.Pdf." + Name + ".xslt";
            }
        }

        protected virtual XsltArgumentList XsltParams
        {
            get { return null; }
        }
        protected abstract void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp);

        private string GenerateHtml()
        {
            StringBuilder sb = new StringBuilder();
            XmlWriter writer = XmlTextWriter.Create(sb);

            if (UsesDataLoan)
            {
                if (this.DataLoan == null)
                {
                    InitializeDataLoan();
                }

                this.DataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

                CAppData dataApp = null;

                if (!string.IsNullOrEmpty(m_arguments["applicationid"]))
                {

                    m_applicationID = new Guid(m_arguments["applicationid"]);
                }

                if (m_applicationID.HasValue)
                {
                    dataApp = this.DataLoan.GetAppData(m_applicationID.Value);
                }
                else
                {
                    dataApp = this.DataLoan.GetAppData(0);
                }

                GenerateXmlData(writer, this.DataLoan, dataApp);
                UpdatePdfConversionOptions(this.DataLoan, dataApp, this.PdfConversionOptions);
            }
            else
            {
                GenerateXmlData(writer, null, null);
                UpdatePdfConversionOptions(null, null, this.PdfConversionOptions);
            }
            writer.Flush();
            
            StringBuilder htmlStringBuilder = new StringBuilder();
            using (TextReader inputReader = new StringReader(sb.ToString()))
            {
                using (TextWriter outputWriter = new StringWriter(htmlStringBuilder))
                {
                    XslTransformHelper.TransformFromEmbeddedResource(XsltEmbeddedResourceLocation, inputReader, outputWriter, this.XsltParams);
                }
            }

            return htmlStringBuilder.ToString();


        }
        #region IPDFGenerator Members

        public byte[] GeneratePDF(string ownerPassword, string userPassword)
        {
            this.PdfConversionOptions = new PDFConversionOptions()
            {
                BottomMargin = 36,
                LeftMargin = 36,
                RightMargin = 36,
                TopMargin = 36,
                ShowFooter = true,
                ShowPageNumber = true
            };

            if (string.IsNullOrEmpty(ownerPassword) == false)
            {
                this.PdfConversionOptions.OwnerPassword = ownerPassword;
            }
            if (string.IsNullOrEmpty(userPassword) == false)
            {
                this.PdfConversionOptions.UserPassword = userPassword;
            }

            switch (PageSize)
            {
                case PDFPageSize.Letter:
                    this.PdfConversionOptions.PageSize = (int)PdfPageSize.Letter;
                    break;
                case PDFPageSize.Legal:
                    this.PdfConversionOptions.PageSize = (int)PdfPageSize.Legal;
                    break;
                default:
                    throw new UnhandledEnumException(PageSize);
            }


            if (ConstStage.UsePDFRasterizerServiceForPDFConversion)
            {
                string htmlContent = GenerateHtml();

                string htmlContentFile = TempFileUtils.NewTempFilePath();
                TextFileHelper.WriteString(htmlContentFile, htmlContent, true);

                string pdfContentFile = Tools.ConvertHtmlFileToPdfFile(BaseUrl, htmlContentFile, this.PdfConversionOptions);
                byte[] pdfBytes = BinaryFileHelper.ReadAllBytes(pdfContentFile);
                FileOperationHelper.Delete(pdfContentFile);
                FileOperationHelper.Delete(htmlContentFile);
                return pdfBytes;
            }

            try
            {
                PdfConverter pdfConverter = new PdfConverter();
               
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = Orientation;

                string htmlContent = GenerateHtml();
                PDFConversionOptionsConverter.UpdatePdfConverterFromPdfConversionOptions(pdfConverter, this.PdfConversionOptions, ConstStage.PdfConverterLicenseKey);

                return pdfConverter.GetPdfBytesFromHtmlString(htmlContent, BaseUrl);
            }
            catch (Exception ex)
            {
                // Restart PDF Conversion Process
                Tools.RestartPdfConversionProcess();
                throw ex;
            }
        }

        protected virtual void UpdatePdfConversionOptions(CPageData dataLoan, CAppData dataApp, PDFConversionOptions options)
        {

        }


        protected virtual string BaseUrl
        {
            get
            {
                string url = string.Empty;
                if (HttpContext.Current != null)
                {
                    HttpRequest request = HttpContext.Current.Request;
                    url = request.IsSecureConnection ? "https://" : "http://";
                    url += request.Url.Host + Tools.VRoot + "/";
                }
                return url;
            }
        }

        public void GeneratePDF(Stream outputStream, string ownerPassword, string userPassword)
        {
            byte[] buffer = GeneratePDF(ownerPassword, userPassword);
            outputStream.Write(buffer, 0, buffer.Length);
        }


        public virtual PDFPageSize PageSize
        {
            get { return PDFPageSize.Letter; }
        }

        public virtual string PdfName
        {
            get;
            set;
        }

        #endregion

        #region IPDFPrintItem Members

        public string Name
        {
            get
            {
                // 5/24/2004 dd - If class name has following format C{name}PDF then return unique code as {name}
                string name = GetType().Name;
                if (name.StartsWith("C") && name.EndsWith("PDF"))
                {
                    name = name.Substring(1, name.Length - 4);
                }
                return name;
            }
        }

        public string ID { get; set; }

        public abstract string Description
        {
            get;
        }

        /// <summary>
        /// The url that the pdf's edit link will follow so that the data for the pdf can be generated.
        /// </summary>
        /// <value>The url to navigate to the data's editor.</value>
        public virtual string EditUrl
        {
            get { return ""; }
        }

        /// <summary>
        /// Gets the html needed to generate the edit link for the pdf for the printlist, only if an edit url has been specified.
        /// </summary>
        /// <value>The HTML needed to generate the edit link.</value>
        public virtual string EditLink
        {
            get {
                if (string.IsNullOrEmpty(EditUrl))
                {
                    return string.Empty;
                }
                else
                {
                    return string.Format(
                        @"(<a href=""#"" onclick=""return _link('{0}.aspx', {1});"">edit</a>)", 
                        AspxTools.JsStringUnquoted(EditUrl), 
                        AspxTools.JsString(ID + AppSuffix + RecordSuffix));
                }
            }
        }

        /// <summary>
        /// The application index.
        /// </summary>
        private int? AppIndex;

        /// <summary>
        /// If the pdf's correspond to an application, add append the current app's index to the ID to differentiate the pdf in the print list.
        /// </summary>
        /// <value>The index of the application if needed for the print list.</value>
        protected string AppSuffix
        {
            get
            {
                return ((AppIndex == null) ? string.Empty : "_" + AppIndex.Value);
            }
        }

        /// <summary>
        /// The record index.
        /// </summary>
        private int? RecordIndex;

        /// <summary>
        /// The suffix that will be added to the pdf's ID if it is part of a collection of records.  Else, it returns an empty string.
        /// </summary>
        /// 
        /// <value>The index of the record if needed for the print list.</value>
        protected string RecordSuffix
        {
            get
            {
                return (RecordIndex.HasValue) ? "_" + RecordIndex.Value : string.Empty; 
            }
        }

        /// <summary>
        /// Gets the Html that will be used to generate the preview link.
        /// </summary>
        /// <value>The HTML that will be used to generate the preview link.</value>
        public virtual string PreviewLink
        {
            get
            {
                return string.Format(
                    @"(<a href=""#"" onclick=""return _preview('{0}.aspx', {1});"">preview</a>)", 
                    AspxTools.JsStringUnquoted(Name), 
                    AspxTools.JsString(ID + AppSuffix + RecordSuffix));
            }
        }
        private CPageData m_dataLoan;

        public CPageData DataLoan
        {
            get
            {
                if (m_dataLoan == null)
                {
                    InitializeDataLoan();
                }
                return m_dataLoan;
            }
            set
            {
                m_dataLoan = value;
                if (m_dataLoan != null)
                {
                    m_loanID = m_dataLoan.sLId;
                }
                else
                {
                    m_loanID = Guid.Empty;
                }
            }
        }

        private void InitializeDataLoan()
        {
            if (m_loanID == Guid.Empty)
            {
                m_dataLoan = null;
            }
            else
            {
                m_dataLoan = new CPageData(m_loanID, "AbstractXsltPdf", DependencyFields);
                m_dataLoan.InitLoad();
            }
        }
        private NameValueCollection m_arguments;
        private Guid m_loanID;
        private Guid? m_applicationID;

        public NameValueCollection Arguments
        {
            get { return m_arguments; }
            set
            {
                m_arguments = value;
                string s = m_arguments["loanid"];
                if (string.IsNullOrEmpty(s) == false)
                {
                    m_loanID = new Guid(s);
                }
                s = m_arguments["applicationid"];
                if (string.IsNullOrEmpty(s) == false)
                {
                    m_applicationID = new Guid(s);
                }
            }
        }

        public virtual bool IsVisible
        {
            get { return true; }
        }

        public virtual bool HasPdf
        {
            get { return true; }
        }
        /// <summary>
        /// Set whether to printout only primary borrower. Default value is false.
        /// </summary>
        public virtual bool OnlyPrintPrimary
        {
            get { return false; }
        }

        /// <summary>
        /// This method is to be implemented whenever a pdf can be displayed multiple times for different types of records across the same application.
        /// </summary>
        /// <param name="dataApp">The dataApp to retrieve the record from.</param>
        /// <returns>A list of NameValueCollections.  Each collection corresponds to each record.</returns>
        public virtual List<NameValueCollection> GenerateRecordArguments(CAppData dataApp)
        {
                return null;
        }

        public void RenderPrintLink(StringBuilder sb)
        {
            if (!IsVisible)
            {
                return;
            }

            if (null == m_dataLoan)
            {
                InitializeDataLoan();
            }

            int nApps = OnlyPrintPrimary ? 1 : m_dataLoan.nApps;

            for (int i = 0; i < nApps; i++)
            {
                AppIndex = i;
                CAppData dataApp = m_dataLoan.GetAppData(i);
                string desc = Description;
                if (nApps > 1)
                {
                    desc += " - " + Utilities.SafeHtmlString(dataApp.aBNm);
                    if (dataApp.aCNm != "")
                    {
                        desc += " & " + Utilities.SafeHtmlString(dataApp.aCNm);
                    }
                }

                List<NameValueCollection> recordArguments = GenerateRecordArguments(dataApp);
                if (recordArguments != null)
                {
                    for(int recordIndex = 0; recordIndex < recordArguments.Count; recordIndex++)
                    {
                        this.RecordIndex = recordIndex;
                        this.Arguments = recordArguments.ElementAt(recordIndex);
                        string recordDescription = this.Arguments["RecordDescription"];
                        if (!string.IsNullOrEmpty(recordDescription))
                        {
                            desc = this.Description + ": " + recordDescription;
                        }

                        this.RenderPrintLinkImpl(sb, desc, dataApp.aAppId);
                    }
                }
                else
                {
                    RenderPrintLinkImpl(sb, desc, dataApp.aAppId);
                }
            }
            AppIndex = null;
        }

        private void RenderPrintLinkImpl(StringBuilder sb, string desc, Guid appId)
        {
            string disabledCheckbox = string.Empty;
            string accessDeniedMsg = string.Empty;

            string previewLink = PreviewLink;

            //bool onlyPrintPrimary = true;
            string extra = string.Empty;
            string script = string.Empty;
            if (HasPrintPermission == false)
            {
                disabledCheckbox = "disabled='disabled'";
                previewLink = string.Empty;
                accessDeniedMsg = "<span style='font-weight:bold;color:red'>" + ErrorMessages.PdfPrintAccessDenied + "</span>";
            }

            string recordParams = "";
            foreach (string key in Arguments.Keys)
            {
                recordParams += key + "='" + Arguments.Get(key) + "' ";
            }

            string checkBoxHtml = string.Format("<input name='{0}' type='checkbox' {1} pdf='{2}' {3} {4} {5} {6}>",
    this.ID + this.AppSuffix + this.RecordSuffix, // 0
    script, // 1
    GetType().Name, // 2
    extra, // 3
    OnlyPrintPrimary ? " primaryOnly='t' " : "appid="+appId, // 4
    disabledCheckbox, // 5,
    recordParams // 6
    );

            string pdfNameHtml = string.Format("{0} {1} {2}", desc, EditLink, previewLink);

            sb.AppendFormat("<tr><td>{0}</td><td>{1}&nbsp;{2}</td></tr>",
    checkBoxHtml, // 0
    pdfNameHtml, // 1
    accessDeniedMsg // 2
    );
        }
        public virtual bool HasPrintPermission
        {
            get { return true; }
        }

        public virtual IEnumerable<string> DependencyFields
        {
            get {
                List<string> fieldList = new List<string>();
                Type t = this.GetType();
                while (t != null)
                {

                    IEnumerable<string> _list = CPageData.GetCPageBaseAndCAppDataDependencyList(t);
                    foreach (string f in _list)
                    {
                        if (!fieldList.Contains(f))
                        {
                            fieldList.Add(f);
                        }
                    }
                    t = t.BaseType;
                }

                return fieldList;
            }
        }

        #endregion
    }
}
