﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Text;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.PdfGenerator;

    public abstract class AbstractFileDBBasedPDF : LendersOffice.PdfGenerator.IPDFGenerator, IPDFPrintItem
    {
        protected abstract string FileDbKey { get; }

        #region IPDFPrintItem Members

        public string Name
        {
            get
            {
                // 5/24/2004 dd - If class name has following format C{name}PDF then return unique code as {name}
                string name = GetType().Name;
                if (name.StartsWith("C") && name.EndsWith("PDF"))
                {
                    name = name.Substring(1, name.Length - 4);
                }
                return name;
            }
        }
        public bool OnlyPrintPrimary
        {
            get { return true; }
        }

        public string ID { get; set;}

        public abstract string Description
        {
            get;
        }

        public virtual string EditLink
        {
            get { return ""; }
        }

        public virtual string PreviewLink
        {
            get
            {
                return string.Format(
                    @"(<a href=""#"" onclick=""return _preview('{0}.aspx', {1});"">preview</a>)", 
                    AspxTools.JsStringUnquoted(Name), 
                    AspxTools.JsString(ID));
            }
        }

        public DataAccess.CPageData DataLoan
        {
            get
            {
                return null;
            }
            set 
            {
                m_sLId = value.sLId;
            }
        }

        private NameValueCollection m_arguments;
        private Guid m_sLId;
        protected Guid sLId
        {
            get { return m_sLId; }
        }
        public System.Collections.Specialized.NameValueCollection Arguments
        {
            get
            {
                return m_arguments;
            }
            set
            {
                m_arguments = value;
                string s = m_arguments["loanid"];

                if (string.IsNullOrEmpty(s) == false)
                {
                    m_sLId = new Guid(s);
                }
                
            }
        }

        public virtual bool IsVisible
        {
            get
            {
                return FileDBTools.DoesFileExist(E_FileDB.Normal, this.FileDbKey);
            }
        }

        public virtual bool HasPdf
        {
            get { return true; }
        }

        public void RenderPrintLink(StringBuilder sb)
        {
            if (!IsVisible)
            {
                return;
            }

            string edit = EditLink;
            if (edit != "")
            {
                edit = string.Format(@"(<a href=""javascript:_link('{0}{1}', '{2}');"">edit</a>)", Tools.VRoot, edit, this.ID);
            }


            string checkboxHtml = string.Format("<input name='{0}' type=checkbox pdf='{1}' {2}>"
    , ID // 0
    , GetType().Name // 1
    , OnlyPrintPrimary ? " primaryOnly='t' " : "" //2
    );
            sb.AppendFormat("<tr><td>{0}</td><td>{1} {2} {3}</td></tr>"
    , checkboxHtml // 0
    , Description // 1
    , edit // 2
    , PreviewLink // 3
    );

        }

        public bool HasPrintPermission
        {
            get { return true; }
        }

        public IEnumerable<string> DependencyFields
        {
            get { return new List<string>(); }
        }

        #endregion

        #region IPDFGenerator Members

        public byte[] GeneratePDF(string ownerPassword, string userPassword)
        {
            try
            {
                return FileDBTools.ReadData(E_FileDB.Normal, FileDbKey);
            }
            catch (FileNotFoundException)
            {
                throw;
            }
        }

        public void GeneratePDF(Stream outputStream, string ownerPassword, string userPassword)
        {
            byte[] buffer = GeneratePDF(ownerPassword, userPassword);

            outputStream.Write(buffer, 0, buffer.Length);
        }
        public void GeneratePDFInFile(string outputFileName, string ownerPassword, string userPassword)
        {
            byte[] buffer = GeneratePDF(ownerPassword, userPassword);
            BinaryFileHelper.WriteAllBytes(outputFileName, buffer);
        }

        public virtual PDFPageSize PageSize
        {
            get { return PDFPageSize.Legal; }
        }

        public bool HasPdfFormLayout => false;

        public LendersOffice.PdfLayout.PdfFormLayout PdfFormLayout
        {
            get { throw new NotImplementedException(); }
        }

        public string PdfName
        {
            get;
            set;
        }

        #endregion
    }
}
