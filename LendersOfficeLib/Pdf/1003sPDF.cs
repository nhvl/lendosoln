using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Text;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class C1003s_04_1PDF : Abstract1003PDF 
    {

        public override string PdfFile 
        {
            get { return "1003s_04_1.pdf"; }
        }

        public override string Description 
        {
            get { return "Page 1"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx"; }
        }

    }

    public class C1003s_04_2PDF : Abstract1003PDF 
    {

        public override string PdfFile 
        {
            get { return "1003s_04_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=1"; }
        }
    }

    public class C1003s_04_3PDF : Abstract1003PDF 
    {

        public override string PdfFile 
        {
            get { return "1003s_04_3.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 3"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=2"; }
        }

    }

    public class C1003s_04_4PDF : Abstract1003PDF
    {

        public override string PdfFile 
        {
            get { return "1003s_04_4.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 4"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=2"; }
        }

    }
    public class C1003s_04_5PDF : Abstract1003PDF
    {

        public override string PdfFile 
        {
            get { return "1003s_04_5.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 5"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=2"; }
        }

    }
    public class C1003s_04_6PDF : Abstract1003PDF
    {

        public override string PdfFile 
        {
            get { return "1003s_04_6.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 6"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=3"; }
        }

    }
    /// <summary>
    /// Assets &amp; Liabilities continuation sheet.
    /// </summary>
    public class C1003s_04_6aPDF : AbstractLegalPDF
    {
        public override string PdfFile 
        {
            get { return "1003s_04_6a.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 6 : Liabilities / Assets - "; }
        }

        public override bool IsUlad
        {
            get
            {
                return true;
            }
        }

        public override string EditLink
        {
            get { return "Liabilities"; }
        }

        public override bool IsVisible 
        {
            get 
            {
                if (null == m_dataApp) 
                {
                    return false;
                }
                var liabilityList = m_dataApp.aLiaCollection.ExtraNormalLiabilities;
                var assetList = m_dataApp.aAssetCollection.ExtraNormalAssets;

                return liabilityList.Count > 0 || assetList.Count > 0;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            var liabilityList = dataApp.aLiaCollection.ExtraNormalLiabilities;
            var assetList = dataApp.aAssetCollection.ExtraNormalAssets;

            int itemsPerPage = 8;

            int extraRecordCount = liabilityList.Count < assetList.Count ? assetList.Count : liabilityList.Count;

            for (int i = 0; i < extraRecordCount; i++) 
            {
                if (i % itemsPerPage == 0) 
                {
                    if (i != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", "Loan Number" + dataLoan.sLNm);
                    AddFormFieldData("aBNm", dataApp.aBNm); 
                    AddFormFieldData("aCNm", dataApp.aCNm); 

                }
                if (i < liabilityList.Count) 
                {
                    ILiabilityRegular field = (ILiabilityRegular) liabilityList.GetRecordAt(i);

                    string prefix = "Liability" + (i % itemsPerPage);

                    // If company name is blank then use description as first line, and blank out
                    // description on last line.
                    string name = field.ComNm.TrimWhitespaceAndBOM() == "" ? field.Desc : field.ComNm;
                    string description = field.ComNm.TrimWhitespaceAndBOM() == "" ? "" : field.Desc;

                    AddFormFieldData(prefix + "Name", Tools.FormatAddress(name, field.ComAddr, field.ComCity, field.ComState, field.ComZip, description));
                    AddFormFieldData(prefix + "AcctNumber", field.AccNum.Value);
                    AddFormFieldData(prefix + "Monthly", field.PmtRemainMons_rep1003);
                    AddFormFieldData(prefix + "Balance", field.Bal_rep);
                }
                if (i < assetList.Count) 
                {
                    var field = (IAssetRegular)assetList.GetRecordAt(i);
                    string prefix = "Asset" + (i % itemsPerPage);
                    //OPM 60497: Net Equity / Net Sales Proceed's "bank" is always "Net Equity"
                    if (field.AssetT == E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets)
                    {
                        AddFormFieldData(prefix + "Bank", "Net Equity");
                    }
                    else
                    {
                        AddFormFieldData(prefix + "Bank", Tools.FormatAddress(field.ComNm.TrimWhitespaceAndBOM() == "" ? field.Desc : field.ComNm, field.StAddr, field.City, field.State, field.Zip));
                    }
                    AddFormFieldData(prefix + "AcctNumber", field.AccNum.Value);
                    AddFormFieldData(prefix + "Value", field.Val_rep);
                }
            }
        }
    }

    /// <summary>
    /// REO continuation sheet.
    /// </summary>
    public class C1003s_04_6bPDF : AbstractLegalPDF 
    {

        public override string PdfFile 
        {
            get { return "1003s_04_6b.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 6 : Real Estate Owned - "; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PDF has its values edited on a page that has Ulad variants.
        /// </summary>
        public override bool IsUlad
        {
            get
            {
                return true;
            }
        }

        public override string EditLink 
        {
            get { return "REO"; }
        }

        public override bool IsVisible 
        {
            get 
            {
                if (null == m_dataApp) 
                {
                    return false;
                }
                return m_dataApp.aReCollection.HasExtraREO;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            var subcoll = dataApp.aReCollection.GetSubcollection( true, E_ReoGroupT.All );
            int extraRealEstateCount = subcoll.Count - 3;
            int itemsPerPage = 25;

            for (int index = 0; index < extraRealEstateCount; index++) 
            {
                if (index % itemsPerPage == 0) 
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                    AddFormFieldData("sLNm", "Loan Number " + dataLoan.sLNm);
                }
                var field = (IRealEstateOwned) subcoll.GetRecordAt( index + 3 );

                string prefix = "RE" + (index % itemsPerPage);
                string name = Tools.FormatAddress(field.Addr, field.City, field.State, field.Zip);

                AddFormFieldData(prefix, name);
                AddFormFieldData(prefix + "Val", field.Val_rep);
                AddFormFieldData(prefix + "MAmt", field.MAmt_rep);
                AddFormFieldData(prefix + "GrossRentI", field.GrossRentI_rep);
                AddFormFieldData(prefix + "MPmt", field.MPmt_rep);
                AddFormFieldData(prefix + "HExp", field.HExp_rep);
                AddFormFieldData(prefix + "NetRentI", field.NetRentI_rep);
                AddFormFieldData(prefix + "Type", field.Type);
                AddFormFieldData(prefix + "Stat", field.Stat);
            }
        }

    }


    /// <summary>
    /// Employment continuation sheet
    /// </summary>
    public class C1003s_04_6fPDF : AbstractLegalPDF
    {
        public override string PdfFile 
        {
            get { return "1003s_04_6f.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 6 : Employments - "; }
        }

        public override string EditLink 
        {
            get { return "/newlos/BorrowerInfo.aspx?pg=1"; }
        }

        public override bool IsVisible 
        {
            get 
            {
                if (null == m_dataApp) 
                {
                    return false;
                }
                var extraBorrowerEmployments = m_dataApp.aBEmpCollection.ExtraPreviousEmployments;
                var extraCoborrowerEmployments = m_dataApp.aCEmpCollection.ExtraPreviousEmployments;

                return extraBorrowerEmployments.Count > 0 || extraCoborrowerEmployments.Count > 0;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            var extraBorrowerEmployments = dataApp.aBEmpCollection.ExtraPreviousEmployments;
            var extraCoborrowerEmployments = dataApp.aCEmpCollection.ExtraPreviousEmployments;

            int itemsPerPage = 5;

            int extraEmploymentRecordCount = extraBorrowerEmployments.Count > extraCoborrowerEmployments.Count ? extraBorrowerEmployments.Count : extraCoborrowerEmployments.Count;

            for (int extraIndex = 0; extraIndex < extraEmploymentRecordCount; extraIndex++) 
            {
                if (extraIndex % itemsPerPage == 0) 
                {
                    if (extraIndex != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", "Loan Number " + dataLoan.sLNm);
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }

                for (int i = 0; i < 2; i++) 
                {
                    var list = (i == 0) ? extraBorrowerEmployments : extraCoborrowerEmployments;
                    if (extraIndex < list.Count) 
                    {
                        string ch = (i == 0) ? "B" : "C";
                        int j = extraIndex % itemsPerPage;
                        IRegularEmploymentRecord f = (IRegularEmploymentRecord) list.GetRecordAt(extraIndex);
                        AddFormFieldData("a" + ch + "IsSelfEmplmt" + j, f.IsSelfEmplmt);
                        AddFormFieldData("a" + ch + "EmplrBusPhone" + j, f.EmplrBusPhone);
                        AddFormFieldData("a" + ch + "JobTitle" + j, f.JobTitle);
                        AddFormFieldData("a" + ch + "EmplrAddr" + j, Tools.FormatAddress(f.EmplrNm, f.EmplrAddr, f.EmplrCity, f.EmplrState, f.EmplrZip));
                        AddFormFieldData("a" + ch + "EmplrDuration" + j, f.EmplmtStartD_rep + Environment.NewLine + f.EmplmtEndD_rep);
                        AddFormFieldData("a" + ch + "MonI" + j, f.MonI_rep);
                    }
                }
            }

        }

    }
    /// <summary>
    /// The second former address info for the new 1003.
    /// </summary>
    public class C1003s_04_6gPDF : AbstractLegalPDF 
    {

        public override string PdfFile 
        {
            get { return "1003s_04_6g.pdf"; }
        }

        public override string Description 
        {
            get { return "Page 6 : Former address - "; }
        }
        public override string EditLink 
        {
            get { return "/newlos/BorrowerInfo.aspx?pg=0"; }
        }
        public override bool IsVisible 
        {
            get 
            {
                return m_dataApp.aHasExtraFormerAddress;
            }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
            AddFormFieldData("sLNm", "Loan Number" + dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm); 
            AddFormFieldData("aCNm", dataApp.aCNm); 
            string address = Tools.FormatAddress(dataApp.aBPrev2Addr, dataApp.aBPrev2City, dataApp.aBPrev2State, dataApp.aBPrev2Zip);
            if (address.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aBPrev2Addr", address);

                if (dataApp.aBPrev2AddrT == E_aBPrev2AddrT.LivingRentFree)
                {
                    AddFormFieldData("aBPrev2AddrT", E_aBPrev2AddrT.Rent);
                }
                else
                {
                    AddFormFieldData("aBPrev2AddrT", dataApp.aBPrev2AddrT);
                }

                AddFormFieldData("aBPrev2AddrYrs", dataApp.aBPrev2AddrYrs);
            }
            address = Tools.FormatAddress(dataApp.aCPrev2Addr, dataApp.aCPrev2City, dataApp.aCPrev2State, dataApp.aCPrev2Zip);
            if (address.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aCPrev2Addr", address);

                if (dataApp.aCPrev2AddrT == E_aCPrev2AddrT.LivingRentFree)
                {
                    AddFormFieldData("aCPrev2AddrT", E_aCPrev2AddrT.Rent);
                }
                else
                {
                    AddFormFieldData("aCPrev2AddrT", dataApp.aCPrev2AddrT);
                }

                AddFormFieldData("aCPrev2AddrYrs", dataApp.aCPrev2AddrYrs);
            }
        }


    }
	public class C1003s_04PDF : AbstractBatchPDF
	{
        public override string Description 
        {
            get { return "1003 Loan Application (Spanish Version)."; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new C1003s_04_1PDF(),
                                                 new C1003s_04_2PDF(),
                                                 new C1003s_04_3PDF(),
                                                 new C1003s_04_4PDF(),
                                                 new C1003s_04_5PDF(),
                                                 new C1003s_04_6aPDF(),
                                                 new C1003s_04_6bPDF(),
                                                 new C1003s_04_6fPDF(),
                                                 new C1003s_04_6gPDF(),

                                                 new C1003s_04_6PDF()
                                             };
            }
        }

	}
}
