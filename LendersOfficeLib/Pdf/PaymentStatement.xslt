﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="PaymentStatement">
      <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>

      <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta charset="utf-8" />
          <title>Mortgage Statement</title>
          <style>
            body <xsl:text disable-output-escaping="yes">&gt;</xsl:text> table, body <xsl:text disable-output-escaping="yes">&gt;</xsl:text> div <xsl:text disable-output-escaping="yes">&gt;</xsl:text> table {
            width: 1000px;
            margin-bottom: 15px;
            font-family: Arial, Helvetica, sans-serif;
            }

            table {
            border-spacing: 0px;
            }

            table.margin-top {
            margin-top: 15px;
            }

            th {
            background-color: lightgray;
            border-bottom: 1px solid black;
            text-align: left;
            }

            .subheader th {
            background-color: white;
            }

            .align-top {
            vertical-align: top;
            }

            .align-right {
            text-align: right;
            }

            .border-right {
            border-right: 1px solid black;
            }

            .half {
            width: 50%;
            }

            .half.right {
              padding-left: 10px;
            }

            .half.left {
              padding-right: 10px;
            }

            .half table {
            width: 100%;
            }

            .outline {
            border: 1px solid black;
            }

            .disclaimer {
            font-size: smaller;
            font-style: italic;
            text-align: center;
            }

            span.disclaimer {
            display: inline-block;
            width: 100%;
            }

            .bold {
            font-weight: bold;
            }

            .disclaimer.not-bold {
            font-weight: normal;
            }

            .larger {
            font-size: larger;
            }

            .tearoff {
            padding-top: 50px;
            border-top: 1px dashed lightgray;
            }

            .fixed-height-container {
            <xsl:choose>
              <xsl:when test="Options/UseLegalSizePages = 'True'">
                height: 1525px;
              </xsl:when>
              <xsl:otherwise>
                height: 1100px;
              </xsl:otherwise>
            </xsl:choose>
            overflow: hidden;
            }

            .preserve-newlines {
            white-space: pre-wrap;
            }
          </style>
        </head>
        <body>
          <div class="fixed-height-container">
            <table>
              <tr class="bold">
                <td class="larger">
                  <xsl:value-of select="PaymentStatementServicer/CompanyName" />
                </td>
                <td class="align-right larger">Mortgage Statement</td>
              </tr>
              <tr>
                <td>
                  Customer Service: <xsl:value-of select="PaymentStatementServicer/PhoneOfCompany" />
                </td>
                <td class="align-right">
                  Statement Date: <xsl:value-of select="Payment/TransactionDate" />
                </td>
              </tr>
              <tr>
                <td>
                  <xsl:value-of select="PaymentStatementServicer/EmailAddr" />
                </td>
                <td></td>
              </tr>
            </table>
            <table>
              <tr>
                <td class="half left">
                  <xsl:value-of select="BorrowerMailingAddress/To" />
                  <br />
                  <xsl:value-of select="BorrowerMailingAddress/Street" />
                  <br />
                  <xsl:value-of select="BorrowerMailingAddress/CityStateZip" />
                </td>
                <td class="half right">
                  <table class="outline bold">
                    <tr>
                      <td>Account Number</td>
                      <td>
                        <xsl:value-of select="sLNm" />
                      </td>
                    </tr>
                    <tr>
                      <td>Payment Due Date</td>
                      <td>
                        <xsl:value-of select="Payment/DueDate" />
                      </td>
                    </tr>
                    <tr class="larger">
                      <td>Amount Due</td>
                      <td>
                        <xsl:value-of select="Payment/TotalAmountDue" />
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" class="disclaimer not-bold">
                        If payment is received after <xsl:value-of select="Options/LateFeeDate" />, <xsl:value-of select="Options/PotentialLateFee" /> late fee will be
                        charged.
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <table>
              <tr>
                <td class="align-top half left">
                  <table class="outline">
                    <thead>
                      <tr>
                        <th colspan="2">Account Information</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Outstanding Principal</td>
                        <td>
                          <xsl:value-of select="sServicingUnpaidPrincipalBalance" />
                        </td>
                      </tr>
                      <tr>
                        <td>Interest Rate</td>
                        <td>
                          <xsl:value-of select="sNoteIR" />
                          <xsl:if test="sRAdj1stD != ''">
                            (Until <xsl:value-of select="sRAdj1stD" />)
                          </xsl:if>
                        </td>
                      </tr>
                      <tr>
                        <td>Prepayment Penalty</td>
                        <td>
                          <xsl:value-of select="sHasPrepaymentPenalty" />
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td class="half right">
                  <table class="outline">
                    <thead>
                      <tr>
                        <th colspan="2">Explanation of Amount Due</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Amount to Principal</td>
                        <td>
                          <xsl:value-of select="Payment/AmountToPrincipal" />
                        </td>
                      </tr>
                      <tr>
                        <td>Amount to Interest</td>
                        <td>
                          <xsl:value-of select="Payment/AmountToInterest" />
                        </td>
                      </tr>
                      <tr>
                        <td>Amount to Escrow (Mortgage Insurance)</td>
                        <td>
                          <xsl:value-of select="Payment/AmountToEscrowForMortgageInsurance" />
                        </td>
                      </tr>
                      <tr>
                        <td>Amount to Escrow (Taxes and Insurance)</td>
                        <td>
                          <xsl:value-of select="Payment/AmountToEscrowForTaxesAndInsurance" />
                        </td>
                      </tr>
                      <tr class="bold">
                        <td>Total Regular Payment</td>
                        <td>
                          <xsl:value-of select="Payment/TotalRegularPayment" />
                        </td>
                      </tr>
                      <tr>
                        <td>Amount to Late Fees</td>
                        <td>
                          <xsl:value-of select="Payment/AmountToLateFees" />
                        </td>
                      </tr>
                      <tr>
                        <td>Amount to Other Charges</td>
                        <td>
                          <xsl:value-of select="Payment/AmountToOtherCharges" />
                        </td>
                      </tr>
                      <tr>
                        <td>Overdue Amounts</td>
                        <td>
                          <xsl:value-of select="Payment/AmountOverdue" />
                        </td>
                      </tr>
                      <tr class="bold">
                        <td>Total Payment</td>
                        <td>
                          <xsl:value-of select="Payment/TotalAmountDue" />
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </table>
            <table class="outline">
              <thead>
                <tr>
                  <th colspan="5">
                    Transaction Activity (<xsl:value-of select="Options/PeriodStart" />  to <xsl:value-of select="Options/PeriodEnd" />)
                  </th>
                </tr>
                <tr class="subheader">
                  <th>Transaction Date</th>
                  <th>Credited Date</th>
                  <th>Description</th>
                  <th>Charges</th>
                  <th>Payments</th>
                </tr>
              </thead>
              <tbody>
                <xsl:for-each select="TransactionActivity/Transaction">
                  <tr>
                    <td>
                      <xsl:value-of select="TransactionDate" />
                    </td>
                    <td>
                      <xsl:value-of select="PaymentCreditedDate" />
                    </td>
                    <td>
                      <xsl:value-of select="Description" />
                    </td>
                    <td>
                      <xsl:value-of select="Charges" />
                    </td>
                    <td>
                      <xsl:value-of select="Payments" />
                    </td>
                  </tr>
                </xsl:for-each>
              </tbody>
            </table>
            <table>
              <tr>
                <td class="half left">
                  <table class="outline">
                    <thead>
                      <tr>
                        <th colspan="4">Past Payments Breakdown</th>
                      </tr>
                      <tr class="subheader">
                        <th colspan="2" class="border-right"></th>
                        <th>Paid Last Month</th>
                        <th>Paid Year to Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td colspan="2" class="border-right">Amount to Principal</td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/WithinBillingPeriod/AmountToPrincipal" />
                        </td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/YearToDate/AmountToPrincipal" />
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="border-right">Amount to Interest</td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/WithinBillingPeriod/AmountToInterest" />
                        </td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/YearToDate/AmountToInterest" />
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="border-right">Amount to Escrow (Mortgage Insurance)</td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/WithinBillingPeriod/AmountToEscrowForMortgageInsurance" />
                        </td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/YearToDate/AmountToEscrowForMortgageInsurance" />
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="border-right">Amount to Escrow (Taxes and Insurance)</td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/WithinBillingPeriod/AmountToEscrowForTaxesAndInsurance" />
                        </td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/YearToDate/AmountToEscrowForTaxesAndInsurance" />
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="border-right">Total Regular Payment</td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/WithinBillingPeriod/TotalRegularPayment" />
                        </td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/YearToDate/TotalRegularPayment" />
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="border-right">Amount to Late Fees</td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/WithinBillingPeriod/AmountToLateFees" />
                        </td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/YearToDate/AmountToLateFees" />
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="border-right">Amount to Other Charges</td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/WithinBillingPeriod/AmountToOtherCharges" />
                        </td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/YearToDate/AmountToOtherCharges" />
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="border-right">Total Payment</td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/WithinBillingPeriod/TotalPayment" />
                        </td>
                        <td>
                          <xsl:value-of select="PastPaymentsBreakdown/YearToDate/TotalPayment" />
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td rowspan="2" class="half right align-top">
                  <xsl:if test="Options/IncludeDelinquencyNotice = 'True'">
                    <table class="outline">
                      <thead>
                        <tr>
                          <th>**Delinquency Notice**</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <span class="preserve-newlines">
                              <xsl:value-of select="sPaymentStatementDelinquencyNotice" />
                            </span>
                            <br />
                            Recent Account History
                            <ul>
                              <xsl:for-each select="RecentAccountHistory/Item">
                                <li>
                                  <xsl:value-of select="text()" />
                                </li>
                              </xsl:for-each>
                              <li>
                                Total:
                                <xsl:value-of select="Payment/TotalAmountDue" />
                                You must pay this amount to bring your loan current.
                              </li>
                            </ul>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </xsl:if>
                </td>
              </tr>
              <tr>
                <td class="half left">
                  <xsl:if test="Options/IncludeSuspenseAccountNotice = 'True'">
                    <table class="outline margin-top">
                      <thead>
                        <tr>
                          <th>Important Messages</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="preserve-newlines">
                            <xsl:value-of select="sPaymentStatementSuspenseAccountNotice" />
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </xsl:if>
                </td>
              </tr>
            </table>
            <xsl:if test="Options/IncludeOtherNotices = 'True'">
              <table class="outline">
                <thead>
                  <tr>
                    <th>Other Messages</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="preserve-newlines">
                      <xsl:value-of select="sPaymentStatementOtherNotices" />
                    </td>
                  </tr>
                </tbody>
              </table>
            </xsl:if>
          </div>
          <table class="tearoff">
            <tr>
              <td class="half left">
                <xsl:value-of select="PaymentStatementServicer/CompanyName" /><br />
                <xsl:value-of select="PaymentStatementServicer/StreetAddr" /><br />
                <xsl:value-of select="PaymentStatementServicer/CityStateZip" /><br />
              </td>
              <td class="half right">
                <table class="outline">
                  <thead>
                    <tr>
                      <th colspan="2">Amount Due</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Due by <xsl:value-of select="Payment/DueDate" />:</td>
                      <td><xsl:value-of select="Payment/TotalAmountDue" /></td>
                    </tr>
                    <tr>
                      <td colspan="2" class="disclaimer"><xsl:value-of select="Options/PotentialLateFee" /> late fee will be charged after <xsl:value-of select="Options/LateFeeDate" /></td>
                    </tr>
                    <tr>
                      <td>Additional Principal</td>
                      <td>$</td>
                    </tr>
                    <tr>
                      <td>Additional Escrow</td>
                      <td>$</td>
                    </tr>
                    <tr>
                      <td>Total Amount Enclosed</td>
                      <td>$</td>
                    </tr>
                  </tbody>
                </table>
                <span class="disclaimer">
                  Make check payable to
                  <xsl:choose>
                    <xsl:when test="Payment/Payee != ''">
                      <xsl:value-of select="Payment/Payee"></xsl:value-of>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="PaymentStatementServicer/CompanyName" />
                    </xsl:otherwise>
                  </xsl:choose>
              </span>
              </td>
            </tr>
          </table>

        </body>
      </html>
    </xsl:template>
</xsl:stylesheet>
