﻿namespace LendersOffice.Pdf
{
    using System;
    using DataAccess;

    /// <summary>
    /// PDF class for 1035 HomeStyle Form.
    /// </summary>
    public class HomeStyleMaxMortgage1035 : AbstractLegalPDF
    {
        /// <summary>
        /// PDF Description.
        /// </summary>
        public override string Description
        {
            get
            {
                return "1035 HomeStyle Form";
            }
        }

        /// <summary>
        /// Edit Link.
        /// </summary>
        public override string EditLink
        {
            get
            {
                return "/newlos/Renovation/FannieMaeHomeStyleMaximumMortgage.aspx";
            }
        }

        /// <summary>
        /// PDF File Name.
        /// </summary>
        public override string PdfFile
        {
            get
            {
                return "HomeStyle_MM_1035.pdf";
            }
        }

        /// <summary>
        /// Applies Data to PDF.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="dataApp">The data app.</param>
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            this.AddFormFieldData("sArchitectEngineerFee", dataLoan.sArchitectEngineerFee_rep);
            this.AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            this.AddFormFieldData("sCostConstrRepairsRehab", dataLoan.sCostConstrRepairsRehab_rep);
            this.AddFormFieldData("sRenovationLoanTgtLtv", dataLoan.sRenovationLoanTgtLtv_rep);
            this.AddFormFieldData("sConsultantFee", dataLoan.sConsultantFee_rep);
            this.AddFormFieldData("sFinanceContingencyRsrv", dataLoan.sFinanceContingencyRsrv_rep);
            this.AddFormFieldData("sTitleUpdateFee", dataLoan.sTitleUpdateFee_rep);
            this.AddFormFieldData("sRenovationPurchPricePlusTotalRenovationCosts", dataLoan.sRenovationPurchPricePlusTotalRenovationCosts_rep);
            this.AddFormFieldData("cToday", DateTime.Today.ToString("M/d/yyyy"));            
            this.AddFormFieldData("sOtherRenovationCost", dataLoan.sOtherRenovationCost_rep);
            this.AddFormFieldData("sPermitFee", dataLoan.sPermitFee_rep);
            this.AddFormFieldData("sTotalRenovationCosts", dataLoan.sTotalRenovationCosts_rep);
            this.AddFormFieldData("sRenovationConstrInspectFee", dataLoan.sRenovationConstrInspectFee_rep);
            this.AddFormFieldData("sFinanceMortgagePmntRsrv", dataLoan.sFinanceMortgagePmntRsrv_rep);
            this.AddFormFieldData("aBFullNm", dataApp.aBNm);
            this.AddFormFieldData("sHomeStyleMaxRenovCost", dataLoan.sHomeStyleMaxRenovCost_rep);
            this.AddFormFieldData("sHomeStyleTotOtherCosts", dataLoan.sHomeStyleTotOtherCosts_rep);

            switch (dataLoan.sOccT)
            {
                case E_sOccT.PrimaryResidence:
                    this.AddFormFieldData("sOCcTPrimaryResidence", true);
                    break;
                case E_sOccT.SecondaryResidence:
                    this.AddFormFieldData("sOCcTSecondaryResidence", true);
                    break;
                case E_sOccT.Investment:
                    this.AddFormFieldData("sOCcTInvestment", true);
                    break;
                default:
                    throw new UnhandledEnumException(dataLoan.sOccT);              
            }

            bool isPurchase = dataLoan.sLPurposeT == E_sLPurposeT.Purchase || dataLoan.sLPurposeT == E_sLPurposeT.Construct || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;
            string purposePostfix = isPurchase ? "_Purch" : "_Refi";

            // section D.4 & D.5
            this.AddFormFieldData("sRenovationMaxMtgAmt" + purposePostfix, DataLoan.sRenovationMaxMtgAmt_rep);

            // section E
            this.AddFormFieldData("sPurchPrice" + purposePostfix, dataLoan.sPurchPrice_rep);
            this.AddFormFieldData("sAltCost" + purposePostfix, dataLoan.sAltCost_rep);
            this.AddFormFieldData("sRefPdOffAmt1003" + purposePostfix, dataLoan.sRefPdOffAmt1003_rep);
            this.AddFormFieldData("sTotEstPp1003" + purposePostfix, dataLoan.sTotEstPp1003_rep);
            this.AddFormFieldData("sTotEstCcNoDiscnt1003" + purposePostfix, dataLoan.sTotEstCcNoDiscnt1003_rep);
            this.AddFormFieldData("sFfUfmipFinanced" + purposePostfix, dataLoan.sFfUfmipFinanced_rep);
            this.AddFormFieldData("sLDiscnt1003" + purposePostfix, dataLoan.sLDiscnt1003_rep);
            this.AddFormFieldData("sTotTransC" + purposePostfix, dataLoan.sTotTransC_rep);
            this.AddFormFieldData("sONewFinBal" + purposePostfix, dataLoan.sONewFinBal_rep);
            this.AddFormFieldData("sTotCcPbs" + purposePostfix, dataLoan.sTotCcPbs_rep);
            this.AddFormFieldData("sRenovationOtherCredits" + purposePostfix, dataLoan.sRenovationOtherCredits_rep);
            this.AddFormFieldData("sFinalLAmt" + purposePostfix, dataLoan.sFinalLAmt_rep);
            this.AddFormFieldData("sLAmtCalc" + purposePostfix, dataLoan.sLAmtCalc_rep);
            this.AddFormFieldData("sRenovationTtlFundsAvailableToBorrower" + purposePostfix, dataLoan.sRenovationTtlFundsAvailableToBorrower_rep);
            this.AddFormFieldData("sTransNetCash" + purposePostfix, dataLoan.sTransNetCash_rep);
        }
    }
}
