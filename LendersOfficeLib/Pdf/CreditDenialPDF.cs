namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Common;

    public class CCreditDenial1PDF : CCreditDenialData
    {
        public override string PdfFile
        {
            get { return "CreditDenial_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
    }

    public class CCreditDenial2PDF : CCreditDenialData
    {
        public override string PdfFile
        {
            get { return "CreditDenial_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
    }

	public class CCreditDenialPDF : AbstractBatchPDF
	{
        /// <summary>
        /// Gets or sets a value indicating whether each borrower on the form should generate a full, separate form.
        /// </summary>
        public override bool FormPerBorrower
        {
            get
            {
                return true;
            }

        }

        public override string Description 
        {
            get { return "Credit Denial Statement"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/CreditDenial.aspx"; }
        }
        
        protected override E_AppPrintModeT AppPrintModeT
        {
            get
            {
                return E_AppPrintModeT.BorrowerAndSpouseSeparately;             
            }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get 
            {
                return new AbstractPDFFile[] {
                    new CCreditDenial1PDF(),
                    new CCreditDenial2PDF()
                };
            }
        }
    }

    public abstract class CCreditDenialData : AbstractLegalPDF
    {
        public override bool FormPerBorrower
        {
            get
            {
                return true;
            }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sRejectD", dataLoan.sRejectD_rep);
            AddFormFieldData("sHmdaDeniedFormDoneD", dataLoan.sHmdaDeniedFormDoneD_rep);

            CAgentFields field;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditDenialStatement, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (preparer.IsValid) 
            {
                this.AddPreparerInformation(
                    preparer.PreparerName,
                    preparer.CompanyName,
                    preparer.StreetAddr,
                    preparer.City,
                    preparer.State,
                    preparer.Zip,
                    preparer.PhoneOfCompany);
            }
            else
            {
                // OPM 236902, 2/9/2016, ML
                field = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

                if (field.IsValid)
                {
                    this.AddPreparerInformation(
                        field.AgentName,
                        field.CompanyName,
                        field.StreetAddr,
                        field.City,
                        field.State,
                        field.Zip,
                        field.PhoneOfCompany);
                }
            }

            field = dataLoan.GetAgentOfRole(E_AgentRoleT.ECOA, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (field.IsValid) 
            {
                string addr1 = Tools.FormatAddress(field.CompanyName, field.StreetAddr, field.City, field.State, field.Zip);
                AddFormFieldData("ECOAInformation", addr1);
            }

            field = dataLoan.GetAgentOfRole(E_AgentRoleT.CreditReport, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (field.IsValid) 
            {
                string addr1 = field.CityStateZip.TrimWhitespaceAndBOM();
                if (field.StreetAddr != "") 
                {
                    addr1 = field.StreetAddr + ", " + addr1;
                }
                AddFormFieldData("CreditReportName", field.CompanyName);
                AddFormFieldData("CreditReportAddress", addr1);
                AddFormFieldData("CreditReportPhone", field.Phone);
            }

            AddFormFieldData("CreditReportAgency1_SingleLine", SingleLineAddress(E_AgentRoleT.CreditReport, dataLoan));
            AddFormFieldData("CreditReportAgency2_SingleLine", SingleLineAddress(E_AgentRoleT.CreditReportAgency2, dataLoan));
            AddFormFieldData("CreditReportAgency3_SingleLine", SingleLineAddress(E_AgentRoleT.CreditReportAgency3, dataLoan));

            IPreparerFields creditDenialScoreContact;
            if (this.BorrType == "B")
            {                
                string addr = Tools.FormatAddress(dataApp.aBNm, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip);
                AddFormFieldData("Applicants", addr);

                AddFormFieldData("aDenialNoCreditFile", dataApp.aDenialNoCreditFile);
                AddFormFieldData("aDenialInsufficientCreditRef", dataApp.aDenialInsufficientCreditRef);
                AddFormFieldData("aDenialInsufficientCreditFile", dataApp.aDenialInsufficientCreditFile);
                AddFormFieldData("aDenialUnableVerifyCreditRef", dataApp.aDenialUnableVerifyCreditRef);
                AddFormFieldData("aDenialGarnishment", dataApp.aDenialGarnishment);
                AddFormFieldData("aDenialExcessiveObligations", dataApp.aDenialExcessiveObligations);
                AddFormFieldData("aDenialInsufficientIncome", dataApp.aDenialInsufficientIncome);
                AddFormFieldData("aDenialUnacceptablePmtRecord", dataApp.aDenialUnacceptablePmtRecord);
                AddFormFieldData("aDenialLackOfCashReserves", dataApp.aDenialLackOfCashReserves);
                AddFormFieldData("aDenialDeliquentCreditObligations", dataApp.aDenialDeliquentCreditObligations);
                AddFormFieldData("aDenialBankruptcy", dataApp.aDenialBankruptcy);
                AddFormFieldData("aDenialInfoFromConsumerReportAgency", dataApp.aDenialInfoFromConsumerReportAgency);
                AddFormFieldData("aDenialUnableVerifyEmployment", dataApp.aDenialUnableVerifyEmployment);
                AddFormFieldData("aDenialLenOfEmployment", dataApp.aDenialLenOfEmployment);
                AddFormFieldData("aDenialTemporaryEmployment", dataApp.aDenialTemporaryEmployment);
                AddFormFieldData("aDenialInsufficientIncomeForMortgagePmt", dataApp.aDenialInsufficientIncomeForMortgagePmt);
                AddFormFieldData("aDenialUnableVerifyIncome", dataApp.aDenialUnableVerifyIncome);
                AddFormFieldData("aDenialTempResidence", dataApp.aDenialTempResidence);
                AddFormFieldData("aDenialShortResidencePeriod", dataApp.aDenialShortResidencePeriod);
                AddFormFieldData("aDenialUnableVerifyResidence", dataApp.aDenialUnableVerifyResidence);
                AddFormFieldData("aDenialByHUD", dataApp.aDenialByHUD);
                AddFormFieldData("aDenialByVA", dataApp.aDenialByVA);
                AddFormFieldData("aDenialByFedNationalMortAssoc", dataApp.aDenialByFedNationalMortAssoc);
                AddFormFieldData("aDenialByFedHomeLoanMortCorp", dataApp.aDenialByFedHomeLoanMortCorp);
                AddFormFieldData("aDenialByOther", dataApp.aDenialByOther);
                AddFormFieldData("aDenialByOtherDesc", dataApp.aDenialByOtherDesc);
                AddFormFieldData("aDenialInsufficientFundsToClose", dataApp.aDenialInsufficientFundsToClose);
                AddFormFieldData("aDenialCreditAppIncomplete", dataApp.aDenialCreditAppIncomplete);
                AddFormFieldData("aDenialInadequateCollateral", dataApp.aDenialInadequateCollateral);
                AddFormFieldData("aDenialUnacceptableProp", dataApp.aDenialUnacceptableProp);
                AddFormFieldData("aDenialInsufficientPropData", dataApp.aDenialInsufficientPropData);
                AddFormFieldData("aDenialUnacceptableAppraisal", dataApp.aDenialUnacceptableAppraisal);
                AddFormFieldData("aDenialUnacceptableLeasehold", dataApp.aDenialUnacceptableLeasehold);
                AddFormFieldData("aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds", dataApp.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds);
                AddFormFieldData("aDenialWithdrawnByApp", dataApp.aDenialWithdrawnByApp);
                AddFormFieldData("aDenialOtherReason1", dataApp.aDenialOtherReason1);
                AddFormFieldData("aDenialOtherReason1Desc", dataApp.aDenialOtherReason1Desc);
                AddFormFieldData("aDenialOtherReason2", dataApp.aDenialOtherReason2);
                AddFormFieldData("aDenialOtherReason2Desc", dataApp.aDenialOtherReason2Desc);
                AddFormFieldData("aDenialDecisionBasedOnReportAgency", dataApp.aDenialDecisionBasedOnReportAgency);
                AddFormFieldData("aDenialDecisionBasedOnCRA", dataApp.aDenialDecisionBasedOnCRA);
                AddFormFieldData("aDenialHasAdditionalStatement", dataApp.aDenialHasAdditionalStatement);
                AddFormFieldData("aDenialAdditionalStatement", dataApp.aDenialAdditionalStatement);

                AddFormFieldData("aCrRd", dataApp.aCrRd_rep);
                AddFormFieldData("aDenialWeObtainedCreditScoreFromCRA", dataApp.aDenialWeObtainedCreditScoreFromCRA);
                AddFormFieldData("aDenialCreditScore", dataApp.aDenialCreditScore_rep);
                AddFormFieldData("aDenialCreditScoreD", dataApp.aDenialCreditScoreD_rep);
                AddFormFieldData("aDenialCreditScoreRangeFrom", dataApp.aDenialCreditScoreRangeFrom_rep);
                AddFormFieldData("aDenialCreditScoreRangeTo", dataApp.aDenialCreditScoreRangeTo_rep);

                AddFormFieldData("aDenialCreditScoreFactorPrint", dataApp.aDenialCreditScoreFactorPrint);

                creditDenialScoreContact = dataApp.aDenialCreditScoreContact;
            }
            else
            {                
                string addr = Tools.FormatAddress(dataApp.aCNm, dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip);
                AddFormFieldData("Applicants", addr);

                AddFormFieldData("aDenialNoCreditFile", dataApp.aCDenialNoCreditFile);
                AddFormFieldData("aDenialInsufficientCreditRef", dataApp.aCDenialInsufficientCreditRef);
                AddFormFieldData("aDenialInsufficientCreditFile", dataApp.aCDenialInsufficientCreditFile);
                AddFormFieldData("aDenialUnableVerifyCreditRef", dataApp.aCDenialUnableVerifyCreditRef);
                AddFormFieldData("aDenialGarnishment", dataApp.aCDenialGarnishment);
                AddFormFieldData("aDenialExcessiveObligations", dataApp.aCDenialExcessiveObligations);
                AddFormFieldData("aDenialInsufficientIncome", dataApp.aCDenialInsufficientIncome);
                AddFormFieldData("aDenialUnacceptablePmtRecord", dataApp.aCDenialUnacceptablePmtRecord);
                AddFormFieldData("aDenialLackOfCashReserves", dataApp.aCDenialLackOfCashReserves);
                AddFormFieldData("aDenialDeliquentCreditObligations", dataApp.aCDenialDeliquentCreditObligations);
                AddFormFieldData("aDenialBankruptcy", dataApp.aCDenialBankruptcy);
                AddFormFieldData("aDenialInfoFromConsumerReportAgency", dataApp.aCDenialInfoFromConsumerReportAgency);
                AddFormFieldData("aDenialUnableVerifyEmployment", dataApp.aCDenialUnableVerifyEmployment);
                AddFormFieldData("aDenialLenOfEmployment", dataApp.aCDenialLenOfEmployment);
                AddFormFieldData("aDenialTemporaryEmployment", dataApp.aCDenialTemporaryEmployment);
                AddFormFieldData("aDenialInsufficientIncomeForMortgagePmt", dataApp.aCDenialInsufficientIncomeForMortgagePmt);
                AddFormFieldData("aDenialUnableVerifyIncome", dataApp.aCDenialUnableVerifyIncome);
                AddFormFieldData("aDenialTempResidence", dataApp.aCDenialTempResidence);
                AddFormFieldData("aDenialShortResidencePeriod", dataApp.aCDenialShortResidencePeriod);
                AddFormFieldData("aDenialUnableVerifyResidence", dataApp.aCDenialUnableVerifyResidence);
                AddFormFieldData("aDenialByHUD", dataApp.aCDenialByHUD);
                AddFormFieldData("aDenialByVA", dataApp.aCDenialByVA);
                AddFormFieldData("aDenialByFedNationalMortAssoc", dataApp.aCDenialByFedNationalMortAssoc);
                AddFormFieldData("aDenialByFedHomeLoanMortCorp", dataApp.aCDenialByFedHomeLoanMortCorp);
                AddFormFieldData("aDenialByOther", dataApp.aCDenialByOther);
                AddFormFieldData("aDenialByOtherDesc", dataApp.aCDenialByOtherDesc);
                AddFormFieldData("aDenialInsufficientFundsToClose", dataApp.aCDenialInsufficientFundsToClose);
                AddFormFieldData("aDenialCreditAppIncomplete", dataApp.aCDenialCreditAppIncomplete);
                AddFormFieldData("aDenialInadequateCollateral", dataApp.aCDenialInadequateCollateral);
                AddFormFieldData("aDenialUnacceptableProp", dataApp.aCDenialUnacceptableProp);
                AddFormFieldData("aDenialInsufficientPropData", dataApp.aCDenialInsufficientPropData);
                AddFormFieldData("aDenialUnacceptableAppraisal", dataApp.aCDenialUnacceptableAppraisal);
                AddFormFieldData("aDenialUnacceptableLeasehold", dataApp.aCDenialUnacceptableLeasehold);
                AddFormFieldData("aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds", dataApp.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds);
                AddFormFieldData("aDenialWithdrawnByApp", dataApp.aCDenialWithdrawnByApp);
                AddFormFieldData("aDenialOtherReason1", dataApp.aCDenialOtherReason1);
                AddFormFieldData("aDenialOtherReason1Desc", dataApp.aCDenialOtherReason1Desc);
                AddFormFieldData("aDenialOtherReason2", dataApp.aCDenialOtherReason2);
                AddFormFieldData("aDenialOtherReason2Desc", dataApp.aCDenialOtherReason2Desc);
                AddFormFieldData("aDenialDecisionBasedOnReportAgency", dataApp.aCDenialDecisionBasedOnReportAgency);
                AddFormFieldData("aDenialDecisionBasedOnCRA", dataApp.aCDenialDecisionBasedOnCRA);
                AddFormFieldData("aDenialHasAdditionalStatement", dataApp.aCDenialHasAdditionalStatement);
                AddFormFieldData("aDenialAdditionalStatement", dataApp.aCDenialAdditionalStatement);

                AddFormFieldData("aCrRd", dataApp.aCCrRd_rep);
                AddFormFieldData("aDenialWeObtainedCreditScoreFromCRA", dataApp.aCDenialWeObtainedCreditScoreFromCRA);
                AddFormFieldData("aDenialCreditScore", dataApp.aCDenialCreditScore_rep);
                AddFormFieldData("aDenialCreditScoreD", dataApp.aCDenialCreditScoreD_rep);
                AddFormFieldData("aDenialCreditScoreRangeFrom", dataApp.aCDenialCreditScoreRangeFrom_rep);
                AddFormFieldData("aDenialCreditScoreRangeTo", dataApp.aCDenialCreditScoreRangeTo_rep);

                // aCDenialCreditScoreFactorPrint
                AddFormFieldData("aDenialCreditScoreFactorPrint", dataApp.aCDenialCreditScoreFactorPrint);

                creditDenialScoreContact = dataApp.aCDenialCreditScoreContact;
            }

            string creditDenialScoreContactInfo = Tools.FormatAddress(creditDenialScoreContact.CompanyName,
                creditDenialScoreContact.StreetAddr, creditDenialScoreContact.City, creditDenialScoreContact.State, creditDenialScoreContact.Zip,
                "Phone: " + creditDenialScoreContact.PhoneOfCompany);
            AddFormFieldData("CreditDenialScoreContact", creditDenialScoreContactInfo);
        }

        protected string Append(string currentString, string addition)
        {
            if ("" != addition || null != addition)
            {
                if (currentString.Length > 0)
                    currentString += " / ";
            }
            return currentString + addition;
        }

        protected string SingleLineAddress(DataAccess.E_AgentRoleT role, CPageData dataLoan)
        {
            CAgentFields field = dataLoan.GetAgentOfRole(role, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string singleLineAddress = "";

            if (field.IsValid)
            {
                
                singleLineAddress = Append(singleLineAddress, field.CompanyName);
                singleLineAddress = Append(singleLineAddress, field.StreetAddr);
                singleLineAddress = Append(singleLineAddress, field.CityStateZip);
                singleLineAddress = Append(singleLineAddress, field.Phone);
            }

            return singleLineAddress;
        }

        private void AddPreparerInformation(
            string preparedByName, 
            string companyName, 
            string streetAddr, 
            string city, 
            string state, 
            string zip, 
            string companyPhone)
        {
            AddFormFieldData("PrepareBy", preparedByName);

            string addr = Tools.FormatAddress(companyName, streetAddr, city, state, zip);

            if (companyPhone.TrimWhitespaceAndBOM() != string.Empty)
            {
                addr += Environment.NewLine + "Phone: " + companyPhone;
            }

            AddFormFieldData("BrokerInformation", addr);
        }
	}
}
