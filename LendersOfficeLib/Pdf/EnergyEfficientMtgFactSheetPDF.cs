﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{

    public class CEnergyEfficientMtgFactSheetPDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "EnergyEfficientMtgFactSheet.pdf"; }
        }

        public override string Description
        {
            get { return "FHA Energy Efficient Mortgage Fact Sheet"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sSpAddr_MultiLine", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
        }
    }
}