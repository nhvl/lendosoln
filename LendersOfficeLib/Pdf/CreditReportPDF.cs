namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.CreditReport;
    using LendersOffice.CreditReport.Mcl;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.PdfGenerator;
    using LendersOffice.Security;

    public class CCreditReportPDF : IPDFGenerator, IPDFPrintItem
	{
        private NameValueCollection m_arguments;
        private Guid m_loanID;

        private Guid m_applicationID = Guid.Empty;

        public void GeneratePDFInFile(string outputFileName, string ownerPassword, string userPassword) 
        {
            byte[] buffer = GeneratePDF(ownerPassword, userPassword);
            BinaryFileHelper.WriteAllBytes(outputFileName, buffer);
        }
        public void GeneratePDF(Stream outputStream, string ownerPassword, string userPassword)
        {
            byte[] buffer = GeneratePDF(ownerPassword, userPassword);
            outputStream.Write(buffer, 0, buffer.Length);
        }
        /// <summary>
        /// Return null if unable to pull credit report in PDF format.
        /// </summary>
        /// <returns></returns>
        public byte[] GeneratePDF(string ownerPassword, string userPassword) 
        {
            byte[] buffer = null;
            CPageData dataLoan = new CCreditReportViewData(m_loanID);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(m_applicationID);

            ICreditReportView creditReportView = dataApp.CreditReportView.Value;

            if (null != creditReportView) 
            {
                if (creditReportView.HasPdf) 
                {
                    buffer = Convert.FromBase64String(creditReportView.PdfContent);
                }
            }
            return buffer;
        }

        public PDFPageSize PageSize 
        {
            get { return PDFPageSize.Legal; }
        }

        public bool HasPdfFormLayout => false;

        public LendersOffice.PdfLayout.PdfFormLayout PdfFormLayout
        {
            get { throw new NotImplementedException(); }
        }

        public string PdfName
        {
            get;
            set;
        }

        #region Implementation of IPDFPrintItem
        public void RenderPrintLink(System.Text.StringBuilder sb)
        {
        
        }

        public string Name
        {
            get { return "CreditReport"; }
        }

        public string ID
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public string Description
        {
            get
            {
                return null;
            }
        }

        public string EditLink
        {
            get { return null; }
        }

        public string PreviewLink
        {
            get { return null; }
        }

        public DataAccess.CPageData DataLoan
        {
            get { return null; }
            set { }
        }

        public NameValueCollection Arguments
        {
            get { return this.m_arguments; }
            set
            {
                this.m_arguments = value;

                if (this.m_arguments == null)
                {
                    return;
                }

                Guid argLoanId;

                if (Guid.TryParse(this.m_arguments["loanid"], out argLoanId))
                {
                    this.m_loanID = argLoanId;
                }

                Guid argAppId;

                if (Guid.TryParse(this.m_arguments["applicationid"], out argAppId))
                {
                    this.m_applicationID = argAppId;
                }
            }
        }

        public bool IsVisible
        {
            get { return false; }
        }

        public bool HasPdf
        {
            get { return true; }
        }
        #endregion



        #region IPDFPrintItem Members


        public virtual bool HasPrintPermission
        {
            get { return true; }
        }

        #endregion

        #region IPDFPrintItem Members


        public System.Collections.Generic.IEnumerable<string> DependencyFields
        {
            get { return new List<string>() ; }
        }

        #endregion
    }
}
