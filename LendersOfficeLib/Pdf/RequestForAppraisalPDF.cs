using System;
using System.Collections.Specialized;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CRequestForAppraisalPDF : AbstractLetterPDF
	{
         public override string PdfFile 
        {
            get { return "RequestForAppraisal.pdf"; }
        }
        public override string Description 
        {
            get { return "Request For Appraisal"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/RequestForms/RequestForAppraisal.aspx"; }
        }
        private string FormatLender(IPreparerFields f) 
        {
            StringBuilder sb = new StringBuilder();
            if (f.PreparerName != "")
                sb.Append(f.PreparerName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            if (f.CompanyName != "")
                sb.Append(f.CompanyName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            if (f.StreetAddr != "")
                sb.Append(f.StreetAddr.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            string str = Tools.CombineCityStateZip(f.City, f.State, f.Zip);
            if (str != "")
                sb.Append(str).Append(Environment.NewLine);

            if (f.Phone != "")
                sb.AppendFormat("(P): {0} ", f.Phone.TrimWhitespaceAndBOM());

            if (f.FaxNum != "")
                sb.AppendFormat("(F): {0} ", f.FaxNum.TrimWhitespaceAndBOM());

            if (f.EmailAddr != "") 
                sb.Append(Environment.NewLine).AppendFormat("Email: {0}", f.EmailAddr);


            return sb.ToString();

        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            ApplyAppraisalData(dataLoan, dataApp);
        }

        internal void ApplyAppraisalData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields preparerField = dataLoan.GetPreparerOfForm(E_PreparerFormT.RequestOfAppraisal, E_ReturnOptionIfNotExist.ReturnEmptyObject);


            string borrowerName = dataApp.aBNm;
            string coborrowerName = dataApp.aCNm;

            string borrowerInformation = borrowerName;
            if (coborrowerName.TrimWhitespaceAndBOM() != "") 
            {
                borrowerInformation += " & " + coborrowerName;
            }
            borrowerInformation = Tools.FormatAddress(borrowerInformation, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip);


            if (dataLoan.sIsPrintBorrPhoneOnRequestForAppraisal) 
            {
                borrowerInformation += Environment.NewLine;
                if (dataApp.aBHPhone.TrimWhitespaceAndBOM() != "")
                    borrowerInformation += "Home: " + dataApp.aBHPhone + " ";
                if (dataApp.aBCellPhone.TrimWhitespaceAndBOM() != "")
                    borrowerInformation += "Cell: " + dataApp.aBCellPhone.TrimWhitespaceAndBOM() + " ";
                if (dataApp.aBEmail.TrimWhitespaceAndBOM() != "")
                    borrowerInformation += "Email: " + dataApp.aBEmail.TrimWhitespaceAndBOM();
            }

            AddFormFieldData("BorrowerInformation", borrowerInformation);

            string address = Tools.FormatAddress(null, dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip, "County: " + dataLoan.sSpCounty);
            AddFormFieldData("LenderAddress", FormatLender(preparerField));
            AddFormFieldData("PropertyAddress", address);
            AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
            AddFormFieldData("sUnitsNum", dataLoan.sUnitsNum_rep);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);

            if (dataLoan.sIsDisplayLAmtApprValInRequestAppr)
            {
                AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
                AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);

            }
            else
            {
                AddFormFieldData("sApprVal", "");
                AddFormFieldData("sFinalLAmt", "");

            }

            AddFormFieldData("sLT", dataLoan.sLT.ToString("D"));
            AddFormFieldData("sLienPosT", dataLoan.sLienPosT);
            AddFormFieldData("Title", preparerField.Title);
            AddFormFieldData("sApprRprtPrepD", preparerField.PrepareDate_rep);
            AddFormFieldData("sApprRprtDueD", dataLoan.sApprRprtDueD_rep);
            AddFormFieldData("sApprContactForEntry", dataLoan.sApprContactForEntry);
            AddFormFieldData("sApprInfo", dataLoan.sApprInfo);
            AddFormFieldData("sApprFull", dataLoan.sApprFull);
            AddFormFieldData("sApprDriveBy", dataLoan.sApprDriveBy);
            AddFormFieldData("sApprMarketRentAnalysis", dataLoan.sApprMarketRentAnalysis);

            AddFormFieldData("sSpT", dataLoan.sSpT);
            AddFormFieldData("aOccT", dataApp.aOccT);
            
            E_sLPurposeT sLPurposeT = dataLoan.sLPurposeT;
            if (sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT == E_sLPurposeT.VaIrrrl)
                sLPurposeT = E_sLPurposeT.Refin;
            if (sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                sLPurposeT = E_sLPurposeT.RefinCashout;
            }

            AddFormFieldData("sLPurposeT", sLPurposeT);

            AddFormFieldData("LegalDescription", dataLoan.sSpLegalDesc);

            if (dataLoan.sGseRequestAppraisalT != E_GseRequestAppraisal.Unset)
            {
                if (dataLoan.sGseRequestAppraisalT == E_GseRequestAppraisal.Other)
                {
                    AddFormFieldData("FannieMae", dataLoan.sGseRequestAppraisalOther);
                }
                else
                {
                    string sGseReqAStr = dataLoan.sGseRequestAppraisalT.ToString();
                    sGseReqAStr = System.Text.RegularExpressions.Regex.Replace(sGseReqAStr, "^Fannie([0-9]+[A-Z]?)$", "Fannie Mae $1");
                    sGseReqAStr = System.Text.RegularExpressions.Regex.Replace(sGseReqAStr, "^Freddie([0-9]+[A-Z]?)$", "Freddie Mac $1");
                    sGseReqAStr = System.Text.RegularExpressions.Regex.Replace(sGseReqAStr, "^VA([0-9]{2})_([0-9]{4})$", "VA $1-$2");
                    AddFormFieldData("FannieMae", sGseReqAStr);
                }
            }

            int count = dataLoan.GetAgentRecordCount();
            // If there are multiple record of appraiser, escrow etc, then use the first
            // record.
            bool isAppraiser = false;
            bool isEscrow = false;
            bool isTitle = false;
            bool isListingAgent = false;
            bool isSellingAgent = false;
            for (int i = 0; i < count; i++) 
            {
                CAgentFields field = dataLoan.GetAgentFields(i);
                switch (field.AgentRoleT) 
                {
                    case E_AgentRoleT.Appraiser:
                        if (!isAppraiser) 
                        {
                            AddFormFieldData("AppraiserAddress", FormatAppraiserDisplay(field));
                            isAppraiser = true;
                        }
                        break;
                    case E_AgentRoleT.Escrow:
                        if (!isEscrow) 
                        {
                            AddFormFieldData("EscrowAddress", FormatAgentDisplay(field));
                            isEscrow = true;
                        }
                        break;
                    case E_AgentRoleT.Title:
                        if (!isTitle) 
                        {
                            AddFormFieldData("TitleAddress", FormatAgentDisplay(field));
                            isTitle = true;
                        }
                        break;
                    case E_AgentRoleT.ListingAgent:
                        if (!isListingAgent) 
                        {
                            AddFormFieldData("ListingAgentAddress", FormatAgentDisplay(field));
                            isListingAgent = true;
                        }
                        break;
                    case E_AgentRoleT.SellingAgent:
                        if (!isSellingAgent) 
                        {
                            AddFormFieldData("SellingAgentAddress", FormatAgentDisplay(field));
                            isSellingAgent = true;
                        }
                        break;
                }
            }

            
            if (dataLoan.sLT == E_sLT.FHA)
            {
                AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            }

            AddFormFieldData("sApprPmtMethodT", dataLoan.sApprPmtMethodT);

            switch (dataLoan.sApprPmtMethodT)
            {
                case E_sApprPmtMethodT.LeaveBlank:
                case E_sApprPmtMethodT.COD:
                case E_sApprPmtMethodT.CreditCard:
                case E_sApprPmtMethodT.InvoiceClient:
                    break; // Don't populate description
                case E_sApprPmtMethodT.Bill:
                    AddFormFieldData("sApprPmtMethodDesc_Bill", dataLoan.sApprPmtMethodDesc);
                    break;
                case E_sApprPmtMethodT.Other:
                    AddFormFieldData("sApprPmtMethodDesc_Other", dataLoan.sApprPmtMethodDesc);
                    break;
                default:
                    break;
            }
            
        }

        private string FormatAppraiserDisplay(CAgentFields field) 
        {
            StringBuilder sb = new StringBuilder();

            if (field.AgentName != "")
                sb.Append(field.AgentName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            if (field.CompanyName != "")
                sb.Append(field.CompanyName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            if (field.StreetAddr != "")
                sb.Append(field.StreetAddr.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            string str = Tools.CombineCityStateZip(field.City, field.State, field.Zip);
            if (str != "")
                sb.Append(str).Append(Environment.NewLine);

            if (field.Phone != "")
                sb.AppendFormat("(P): {0} ", field.Phone.TrimWhitespaceAndBOM());

            if (field.FaxNum != "")
                sb.AppendFormat("(F): {0} ", field.FaxNum.TrimWhitespaceAndBOM());

            if (field.CellPhone != "")
                sb.AppendFormat("(C): {0}", field.CellPhone.TrimWhitespaceAndBOM());

            if (field.EmailAddr != "")
                sb.Append(Environment.NewLine).AppendFormat("Email: {0}", field.EmailAddr.TrimWhitespaceAndBOM());

            return sb.ToString();

        }
        private string FormatAgentDisplay(CAgentFields field) 
        {
            StringBuilder sb = new StringBuilder();
            if (field.CaseNum != "") 
            {
                sb.AppendFormat("Case: {0}", field.CaseNum.TrimWhitespaceAndBOM());
                sb.Append(Environment.NewLine);
            }

            if (field.CompanyName != "")
                sb.Append(field.CompanyName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);
            if (field.AgentName != "")
                sb.Append(field.AgentName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            if (field.Phone != "")
                sb.AppendFormat("(P): {0} ", field.Phone.TrimWhitespaceAndBOM());

            if (field.FaxNum != "")
                sb.AppendFormat("(F): {0} ", field.FaxNum.TrimWhitespaceAndBOM());

            if (field.CellPhone != "")
                sb.AppendFormat("(C): {0}", field.CellPhone.TrimWhitespaceAndBOM());

            if (field.EmailAddr != "")
                sb.Append(Environment.NewLine).AppendFormat("Email: {0}", field.EmailAddr.TrimWhitespaceAndBOM());

            return sb.ToString();
                
        }
	}
}
