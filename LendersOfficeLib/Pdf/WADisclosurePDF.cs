﻿using System;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CWADisclosurePDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get 
            {
                try
                {
                    if (null == DataLoan)
                    {
                        return "WADisclosureArm.pdf";
                    }
                    else
                    {
                        E_sFinMethT sFinMethT = DataLoan.sFinMethT;
                        if (sFinMethT == E_sFinMethT.Fixed)
                        {
                            return "WADisclosureFixed.pdf";
                        }
                        else
                        {
                            return "WADisclosureArm.pdf";
                        }
                    }
                }
                catch (LoanNotFoundException)
                {
                    return "WADisclosureArm.pdf";
                }
            }
        }
        public override string Description
        {
            get
            {
                return "Washington State Disclosure";
            }
        }
        public override string EditLink
        {
            get
            {
                return "/newlos/Disclosure/WADisclosure.aspx";
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("sWADisclosurePrepareD", dataLoan.GetPreparerOfForm(E_PreparerFormT.WADisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject).PrepareDate_rep);
            AddFormFieldData("sWADisclosureVersionT", dataLoan.sWADisclosureVersionT);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aBNm_aCNm", dataApp.aBNm_aCNm);
            AddFormFieldData("sSpAddr_SingleLine", dataLoan.sSpAddr_SingleLine);
            AddFormFieldData("sSpAddr_MultiLine", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            AddFormFieldData("sFullyIndexedR", dataLoan.sFullyIndexedR_rep);
            AddFormFieldData("sFullyIndexedMPmt", dataLoan.sFullyIndexedMPmt_rep);
            AddFormFieldData("sRLifeCapR", dataLoan.sRLifeCapR_rep);
            AddFormFieldData("sGfeMaxProThisMPmtAndMIns", dataLoan.sGfeMaxProThisMPmtAndMIns_rep);
            AddFormFieldData("sRAdj1stD", dataLoan.sRAdj1stD_rep);
            AddFormFieldData("sIsRealEstateTaxIncludeInMPmt", dataLoan.sIsRealEstateTaxIncludeInMPmt);
            AddFormFieldData("sIsHazardInsuranceIncludeInMPmt", dataLoan.sIsHazardInsuranceIncludeInMPmt);
            AddFormFieldData("sIsMortgageInsuranceIncludeInMPmt", dataLoan.sIsMortgageInsuranceIncludeInMPmt);
            AddFormFieldData("sIsHOAIncludeInMPmt", dataLoan.sIsHOAIncludeInMPmt);
            AddFormFieldData("sLOrigF", dataLoan.sLOrigF_rep);
            AddFormFieldData("sMBrokF", dataLoan.sMBrokF_rep);
            AddFormFieldData("sLDiscntPc", dataLoan.sLDiscntPc_rep);
            AddFormFieldData("sLDiscnt", dataLoan.sLDiscnt_rep);
            AddFormFieldData("sWAOtherF", dataLoan.sWAOtherF_rep);
            AddFormFieldData("sGfeHavePpmtPenalty", dataLoan.sGfeHavePpmtPenalty);
            AddFormFieldData("sGfeIsBalloon", dataLoan.sGfeIsBalloon);
            AddFormFieldData("sIsGfeRateLocked", dataLoan.sIsGfeRateLocked);
            AddFormFieldData("sIsWAReduceDoc", dataLoan.sIsWAReduceDoc);
            AddFormFieldData("sBrokComp1", dataLoan.sBrokComp1_rep);
            AddFormFieldData("sIsWABrokerReceiveYsp", dataLoan.sIsWABrokerReceiveYsp);
        }
    }
}