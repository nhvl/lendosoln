using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CVA_26_1880_1PDF : AbstractLetterPDF
	{

        public override string Description 
        {
            get { return "Page 1"; }
        }
        public override string PdfFile 
        {
            get { return "VA-26-1880_1.pdf"; }
        }
	    

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBAddr", Tools.FormatAddress(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            //if (!dataApp.aBAddrMailUsePresentAddr) 
            if (dataApp.aBAddrMailSourceT != E_aAddrMailSourceT.PresentAddress)
            {
                AddFormFieldData("aBAddrMail", Tools.FormatAddress(dataApp.aBAddrMail, dataApp.aBCityMail, dataApp.aBStateMail, dataApp.aBZipMail));
            }
            AddFormFieldData("aBDob", dataApp.aBDob_rep);
            AddFormFieldData("aBBusPhone", dataApp.aBBusPhone);
            AddFormFieldData("aVaServ1StartD", dataApp.aVaServ1StartD_rep); 
            AddFormFieldData("aVaServ2StartD", dataApp.aVaServ2StartD_rep); 
            AddFormFieldData("aVaServ3StartD", dataApp.aVaServ3StartD_rep); 
            AddFormFieldData("aVaServ4StartD", dataApp.aVaServ4StartD_rep); 
            AddFormFieldData("aVaServ1EndD", dataApp.aVaServ1EndD_rep);
            AddFormFieldData("aVaServ2EndD", dataApp.aVaServ2EndD_rep); 
            AddFormFieldData("aVaServ3EndD", dataApp.aVaServ3EndD_rep); 
            AddFormFieldData("aVaServ4EndD", dataApp.aVaServ4EndD_rep); 
            AddFormFieldData("aVaServ1FullNm", dataApp.aVaServ1FullNm);
            AddFormFieldData("aVaServ2FullNm", dataApp.aVaServ2FullNm);
            AddFormFieldData("aVaServ3FullNm", dataApp.aVaServ3FullNm);
            AddFormFieldData("aVaServ4FullNm", dataApp.aVaServ4FullNm);
            AddFormFieldData("aVaServ1Ssn", dataApp.aVaServ1Ssn); 
            AddFormFieldData("aVaServ2Ssn", dataApp.aVaServ2Ssn);
            AddFormFieldData("aVaServ3Ssn", dataApp.aVaServ3Ssn);
            AddFormFieldData("aVaServ4Ssn", dataApp.aVaServ4Ssn);
            AddFormFieldData("aVaServ1Num", dataApp.aVaServ1Num);
            AddFormFieldData("aVaServ2Num", dataApp.aVaServ2Num); 
            AddFormFieldData("aVaServ3Num", dataApp.aVaServ3Num);
            AddFormFieldData("aVaServ4Num", dataApp.aVaServ4Num);
            AddFormFieldData("aVaServ1BranchNum", dataApp.aVaServ1BranchNum);
            AddFormFieldData("aVaServ2BranchNum", dataApp.aVaServ2BranchNum);
            AddFormFieldData("aVaServ3BranchNum", dataApp.aVaServ3BranchNum); 
            AddFormFieldData("aVaServ4BranchNum", dataApp.aVaServ4BranchNum);
            AddFormFieldData("aVaDischargedDisabilityIs", dataApp.aVaDischargedDisabilityIs);
            AddFormFieldData("aVaClaimNum", dataApp.aVaClaimNum);
            AddFormFieldData("aBEmail", dataApp.aBEmail);
            AddFormFieldData("aBSsn", dataApp.aBSsn);

            IVaPastLoanCollection collection = dataApp.aVaPastLCollection;

            int count = collection.CountRegular;

            // 4/8/2004 dd - Only display the first 6 records of CVaPastL
            for (int i = 0; i < count && i < 6; i++) 
            {
                IVaPastLoan record = collection.GetRegularRecordAt(i);


                //string addr = record.StAddr;
                //string cityStateZip = Tools.CombineCityStateZip(record.City, record.State, record.Zip);
                //if (cityStateZip.TrimWhitespaceAndBOM() != "")
                //    addr = addr + ", " + cityStateZip;

                //AddFormFieldData("CVaPastL_LoanTypeDesc_" + i, record.LoanTypeDesc);
                AddFormFieldData("CVaPastL_StAddr_" + i, record.StAddr);
                AddFormFieldData("CVaPastL_CityState_" + i, record.CityState);
                AddFormFieldData("CVaPastL_DateOfLoan_" + i, record.DateOfLoan);
                //AddFormFieldData("CVaPastL_LoanD_" + i, record.LoanD_rep);
                //AddFormFieldData("CVaPastL_PropSoldD_" + i, record.PropSoldD_rep);
                //AddFormFieldData("CVaPastL_VaLoanNum_" + i, record.VaLoanNum);
                //AddFormFieldData("CVaPastL_IsStillOwned_" + i, record.IsStillOwned == E_TriState.Yes ? "Yes" : (record.IsStillOwned == E_TriState.No ? "No" : ""));

            }

            AddFormFieldData("aVaServedAltNameIs", dataApp.aVaServedAltNameIs);
            AddFormFieldData("aVaServedAltName", dataApp.aVaServedAltName);
            AddFormFieldData("aActiveMilitaryStatTri", dataApp.aActiveMilitaryStatTri);
            AddFormFieldData("aVaServ1Title", dataApp.aVaServ1Title);
            AddFormFieldData("aVaServ2Title", dataApp.aVaServ2Title);
            AddFormFieldData("aVaServ3Title", dataApp.aVaServ3Title);
            AddFormFieldData("aVaServ4Title", dataApp.aVaServ4Title);

            AddFormFieldData("aVaServ5BranchNum", dataApp.aVaServ5BranchNum);
            AddFormFieldData("aVaServ5StartD", dataApp.aVaServ5StartD_rep);
            AddFormFieldData("aVaServ5EndD", dataApp.aVaServ5EndD_rep);
            AddFormFieldData("aVaServ5Title", dataApp.aVaServ5Title);
            AddFormFieldData("aVaServ5Num", dataApp.aVaServ5Num);

            AddFormFieldData("aVaServ6BranchNum", dataApp.aVaServ6BranchNum);
            AddFormFieldData("aVaServ6StartD", dataApp.aVaServ6StartD_rep);
            AddFormFieldData("aVaServ6EndD", dataApp.aVaServ6EndD_rep);
            AddFormFieldData("aVaServ6Title", dataApp.aVaServ6Title);
            AddFormFieldData("aVaServ6Num", dataApp.aVaServ6Num);

            AddFormFieldData("aVaServ7BranchNum", dataApp.aVaServ7BranchNum);
            AddFormFieldData("aVaServ7StartD", dataApp.aVaServ7StartD_rep);
            AddFormFieldData("aVaServ7EndD", dataApp.aVaServ7EndD_rep);
            AddFormFieldData("aVaServ7Title", dataApp.aVaServ7Title);
            AddFormFieldData("aVaServ7Num", dataApp.aVaServ7Num);

            AddFormFieldData("aVaOwnOtherVaLoanT", dataApp.aVaOwnOtherVaLoanT);
            AddFormFieldData("aVaApplyOneTimeRestorationTri", dataApp.aVaApplyOneTimeRestorationTri);
            AddFormFieldData("aVaApplyOneTimeRestorationDateOfLoan", dataApp.aVaApplyOneTimeRestorationDateOfLoan);
            AddFormFieldData("aVaApplyOneTimeRestorationStAddr", dataApp.aVaApplyOneTimeRestorationStAddr);
            AddFormFieldData("aVaApplyOneTimeRestorationCityState", dataApp.aVaApplyOneTimeRestorationCityState);
            AddFormFieldData("aVaApplyRegularRefiCashoutTri", dataApp.aVaApplyRegularRefiCashoutTri);
            AddFormFieldData("aVaApplyRegularRefiCashoutDateOfLoan", dataApp.aVaApplyRegularRefiCashoutDateOfLoan);
            AddFormFieldData("aVaApplyRegularRefiCashoutStAddr", dataApp.aVaApplyRegularRefiCashoutStAddr);
            AddFormFieldData("aVaApplyRegularRefiCashoutCityState", dataApp.aVaApplyRegularRefiCashoutCityState);

            AddFormFieldData("aVaApplyRefiNoCashoutTri", dataApp.aVaApplyRefiNoCashoutTri);
            AddFormFieldData("aVaApplyRefiNoCashoutDateOfLoan", dataApp.aVaApplyRefiNoCashoutDateOfLoan);
            AddFormFieldData("aVaApplyRefiNoCashoutStAddr", dataApp.aVaApplyRefiNoCashoutStAddr);
            AddFormFieldData("aVaApplyRefiNoCashoutCityState", dataApp.aVaApplyRefiNoCashoutCityState);

        }
	}

    public class CVA_26_1880_2PDF : AbstractLetterPDF
    {

        public override string Description 
        {
            get { return "Page 2"; }
        }
        public override string PdfFile 
        {
            get { return "VA-26-1880_2.pdf"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
        }
    }

    public class CVA_26_1880PDF : AbstractBatchPDF 
    {
 
        public override string Description 
        {
            get { return "VA Certificate of Eligibility (VA 26-1880)"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CVA_26_1880_1PDF(),
                                                 new CVA_26_1880_2PDF()
                                             };
            }
        }

    }
}
