using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.PdfGenerator;
using System.Text;

namespace LendersOffice.Pdf
{
	public class CAggregateEscrowDisclosurePDF : AbstractLegalPDF
	{
        public override string PdfFile 
        {
            get { return "AggregateEscrowDisclosure.pdf"; }
        }
        public override string Description 
        {
            get { return "Initial Escrow Account Disclosure Statement"; }
        }
		public override string EditLink 
		{
			get { return "/newlos/Forms/AggregateEscrowDisclosure.aspx"; }
		}
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            StringBuilder sb = new StringBuilder(30);
            sb.Append(dataApp.aBNm);
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                sb.AppendFormat(" & {0}", dataApp.aCNm);
            }

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.AggregateEscrowServicer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            string phone = "";
            if (preparer.PhoneOfCompany != "")
                phone = "Phone: " + preparer.PhoneOfCompany;

            AddFormFieldData("AggregateEscrowServicer", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, phone));

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("GfeTilPrepareDate", preparer.PrepareDate_rep);
            AddFormFieldData("BorrowerInformation", Tools.FormatAddress(sb.ToString(), dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(null, dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpCity, dataLoan.sSpZip));

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sEscrowPmt", dataLoan.sEscrowPmt_rep);
            AddFormFieldData("sAggrEscrowCushionRequired", dataLoan.sAggrEscrowCushionRequired_rep);
            AddFormFieldData("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);

            AggregateEscrowAccount escrowAccount = dataLoan.sAggregateEscrowAccount;
            AddFormFieldData("sAggrEscrowInitialDeposit", escrowAccount.InitialDeposit_rep);
            for (int i = 0; i < 12; i++) 
            {
                AddFormFieldData("sAggrEscrowMon" + i, escrowAccount.AggregateItems[i].Mon);
                AddFormFieldData("sAggrEscrowPmtFromEscrow" + i, escrowAccount.AggregateItems[i].PmtFromEscrow_rep);
                AddFormFieldData("sAggrEscrowDesc" + i, escrowAccount.AggregateItems[i].Desc);
                AddFormFieldData("sAggrEscrowBal" + i, escrowAccount.AggregateItems[i].Bal_rep);
            }
        }
	}
}
