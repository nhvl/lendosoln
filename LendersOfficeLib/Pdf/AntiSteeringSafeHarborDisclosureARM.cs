﻿using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.Pdf
{
    public class CAntiSteeringSafeHarborDisclosureARMPDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "AntiSteeringSafeHarborDisclosureARM.pdf"; }
        }

        public override string Description
        {
            get { return "Anti-Steering Safe Harbor Disclosure, Adjustable Rate Loans"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Disclosure/SafeHarborDisclosureAdjustable.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields safeHarborARM = dataLoan.GetPreparerOfForm(E_PreparerFormT.SafeHarborARM, E_ReturnOptionIfNotExist.CreateNew);
            AddFormFieldData("PreparerName", safeHarborARM.PreparerName);
            AddFormFieldData("LoanOriginatorIdentifier", safeHarborARM.LoanOriginatorIdentifier);
            AddFormFieldData("LicenseNumOfAgent", safeHarborARM.LicenseNumOfAgent);
            AddFormFieldData("Phone", safeHarborARM.Phone);
            AddFormFieldData("CompanyName", safeHarborARM.CompanyName);
            AddFormFieldData("CompanyLoanOriginatorIdentifier", safeHarborARM.CompanyLoanOriginatorIdentifier);
            AddFormFieldData("LicenseNumOfCompany", safeHarborARM.LicenseNumOfCompany);
            AddFormFieldData("PhoneOfCompany", safeHarborARM.PhoneOfCompany);
            AddFormFieldData("FaxOfCompany", safeHarborARM.FaxOfCompany);
            AddFormFieldData("StreetAddr_Multiline", safeHarborARM.StreetAddrMultiLines);

            AddFormFieldData("aBNm_aCNm", dataApp.aBNm_aCNm);
            AddFormFieldData("aBAddr_MultiLine", dataApp.aBAddr_MultiLine);

            if (dataLoan.sIncludeSafeHarborARMRecLoan)
            {
                dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
                AddFormFieldData("sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMRecLoan_rep);
                AddFormFieldData("sEquityCalcSafeHarborARMRecLoan", dataLoan.sEquityCalcSafeHarborARMRecLoan_rep);
                AddFormFieldData("sTransNetCashSafeHarborARMRecLoan", dataLoan.sTransNetCashSafeHarborARMRecLoan_rep);
                dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

                AddFormFieldData("sDueSafeHarborARMRecLoan", dataLoan.sDueSafeHarborARMRecLoan_rep);
                AddFormFieldData("sFinalLAmtSafeHarborARMRecLoan", dataLoan.sFinalLAmtSafeHarborARMRecLoan_rep);
                AddFormFieldData("sFullyIndexedRSafeHarborARMRecLoan", dataLoan.sFullyIndexedRSafeHarborARMRecLoan_rep);
                AddFormFieldData("sGfeProThisMPmtAndMInsSafeHarborARMRecLoan", dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMRecLoan_rep);
                AddFormFieldData("sLpTemplateNmSafeHarborARMRecLoan", dataLoan.sLpTemplateNmSafeHarborARMRecLoan);
                AddFormFieldData("sNoteIRSafeHarborARMRecLoan", dataLoan.sNoteIRSafeHarborARMRecLoan_rep);
                AddFormFieldData("sOriginatorCompensationTotalAmountSafeHarborARMRecLoan", dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMRecLoan_rep);
                AddFormFieldData("sRLifeCapRSafeHarborARMRecLoan", dataLoan.sRLifeCapRSafeHarborARMRecLoan_rep);
                AddFormFieldData("sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMRecLoan_rep);
            }

            if (dataLoan.sIncludeSafeHarborARMWithRiskyFeatures)
            {
                dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
                AddFormFieldData("sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithRiskyFeatures_rep);
                AddFormFieldData("sEquityCalcSafeHarborARMWithRiskyFeatures", dataLoan.sEquityCalcSafeHarborARMWithRiskyFeatures_rep);
                AddFormFieldData("sTransNetCashSafeHarborARMWithRiskyFeatures", dataLoan.sTransNetCashSafeHarborARMWithRiskyFeatures_rep);
                dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

                AddFormFieldData("sDueSafeHarborARMWithRiskyFeatures", dataLoan.sDueSafeHarborARMWithRiskyFeatures_rep);
                AddFormFieldData("sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures", dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMWithRiskyFeatures_rep);
                AddFormFieldData("sFullyIndexedRSafeHarborARMWithRiskyFeatures", dataLoan.sFullyIndexedRSafeHarborARMWithRiskyFeatures_rep);
                AddFormFieldData("sFinalLAmtSafeHarborARMWithRiskyFeatures", dataLoan.sFinalLAmtSafeHarborARMWithRiskyFeatures_rep);
                AddFormFieldData("sLpTemplateNmSafeHarborARMWithRiskyFeatures", dataLoan.sLpTemplateNmSafeHarborARMWithRiskyFeatures);
                AddFormFieldData("sNoteIRSafeHarborARMWithRiskyFeatures", dataLoan.sNoteIRSafeHarborARMWithRiskyFeatures_rep);
                AddFormFieldData("sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures", dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMWithRiskyFeatures_rep);
                AddFormFieldData("sRLifeCapRSafeHarborARMWithRiskyFeatures", dataLoan.sRLifeCapRSafeHarborARMWithRiskyFeatures_rep);
                AddFormFieldData("sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithRiskyFeatures_rep);
            }

            AddFormFieldData("sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMWithoutRiskyFeatures_rep);
            AddFormFieldData("sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborARMLowestCost_rep);

            dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
            AddFormFieldData("sEquityCalcSafeHarborARMLowestCost", dataLoan.sEquityCalcSafeHarborARMLowestCost_rep);
            AddFormFieldData("sEquityCalcSafeHarborARMWithoutRiskyFeatures", dataLoan.sEquityCalcSafeHarborARMWithoutRiskyFeatures_rep);
            AddFormFieldData("sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMLowestCost_rep);
            AddFormFieldData("sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborARMWithoutRiskyFeatures_rep);
            AddFormFieldData("sTransNetCashSafeHarborARMLowestCost", dataLoan.sTransNetCashSafeHarborARMLowestCost_rep);
            AddFormFieldData("sTransNetCashSafeHarborARMWithoutRiskyFeatures", dataLoan.sTransNetCashSafeHarborARMWithoutRiskyFeatures_rep);
            dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

            AddFormFieldData("sDueSafeHarborARMLowestCost", dataLoan.sDueSafeHarborARMLowestCost_rep);
            AddFormFieldData("sDueSafeHarborARMWithoutRiskyFeatures", dataLoan.sDueSafeHarborARMWithoutRiskyFeatures_rep);
            AddFormFieldData("sFinalLAmtSafeHarborARMLowestCost", dataLoan.sFinalLAmtSafeHarborARMLowestCost_rep);
            AddFormFieldData("sFinalLAmtSafeHarborARMWithoutRiskyFeatures", dataLoan.sFinalLAmtSafeHarborARMWithoutRiskyFeatures_rep);
            AddFormFieldData("sFullyIndexedRSafeHarborARMLowestCost", dataLoan.sFullyIndexedRSafeHarborARMLowestCost_rep);
            AddFormFieldData("sFullyIndexedRSafeHarborARMWithoutRiskyFeatures", dataLoan.sFullyIndexedRSafeHarborARMWithoutRiskyFeatures_rep);
            AddFormFieldData("sGfeProThisMPmtAndMInsSafeHarborARMLowestCost", dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMLowestCost_rep);
            AddFormFieldData("sGfeProThisMPmtAndMInsSafeHarborARMWithoutRiskyFeatures", dataLoan.sGfeProThisMPmtAndMInsSafeHarborARMWithoutRiskyFeatures_rep);
            AddFormFieldData("sLpTemplateNmSafeHarborARMLowestCost", dataLoan.sLpTemplateNmSafeHarborARMLowestCost);
            AddFormFieldData("sLpTemplateNmSafeHarborARMWithoutRiskyFeatures", dataLoan.sLpTemplateNmSafeHarborARMWithoutRiskyFeatures);
            AddFormFieldData("sNoteIRSafeHarborARMLowestCost", dataLoan.sNoteIRSafeHarborARMLowestCost_rep);
            AddFormFieldData("sNoteIRSafeHarborARMWithoutRiskyFeatures", dataLoan.sNoteIRSafeHarborARMWithoutRiskyFeatures_rep);
            AddFormFieldData("sOriginatorCompensationTotalAmountSafeHarborARMLowestCost", dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMLowestCost_rep);
            AddFormFieldData("sOriginatorCompensationTotalAmountSafeHarborARMWithoutRiskyFeatures", dataLoan.sOriginatorCompensationTotalAmountSafeHarborARMWithoutRiskyFeatures_rep);
            AddFormFieldData("sRLifeCapRSafeHarborARMLowestCost", dataLoan.sRLifeCapRSafeHarborARMLowestCost_rep);
            AddFormFieldData("sRLifeCapRSafeHarborARMWithoutRiskyFeatures", dataLoan.sRLifeCapRSafeHarborARMWithoutRiskyFeatures_rep);
            AddFormFieldData("sSafeHarborARMBalloonPmntIn7Yrs", dataLoan.sSafeHarborARMBalloonPmntIn7Yrs);
            AddFormFieldData("sSafeHarborARMDemandFeat", dataLoan.sSafeHarborARMDemandFeat);
            AddFormFieldData("sSafeHarborARMIntOnlyPmts", dataLoan.sSafeHarborARMIntOnlyPmts);
            AddFormFieldData("sSafeHarborARMNegAmort", dataLoan.sSafeHarborARMNegAmort);
            AddFormFieldData("sSafeHarborARMOurRecLowestCosts", dataLoan.sSafeHarborARMOurRecLowestCosts);
            AddFormFieldData("sSafeHarborARMOurRecOther", dataLoan.sSafeHarborARMOurRecOther);
            AddFormFieldData("sSafeHarborARMOurRecText", dataLoan.sSafeHarborARMOurRecText);
            AddFormFieldData("sSafeHarborARMOurRecWithoutRiskyFeatures", dataLoan.sSafeHarborARMOurRecWithoutRiskyFeatures);
            AddFormFieldData("sSafeHarborARMPrepayPenalty", dataLoan.sSafeHarborARMPrepayPenalty);
            AddFormFieldData("sSafeHarborARMSharedAppreciation", dataLoan.sSafeHarborARMSharedAppreciation);
            AddFormFieldData("sSafeHarborARMSharedEquity", dataLoan.sSafeHarborARMSharedEquity);
            AddFormFieldData("sSafeHarborARMOurRecLowestRate", dataLoan.sSafeHarborARMOurRecLowestRate);
        }
    }
}