using System;
using System.Collections.Specialized;
using System.Data;
using DataAccess;
using LendersOffice.Common;


namespace LendersOffice.Pdf
{

    public class CBorrowerCertificationPDF : AbstractLetterPDF
    {


        public override string Description 
        {
            get { return "Borrower's Certification & Authorization Form"; }
        }
        
		public override string PdfFile 
        {
            get { return "BorrowerCertification.pdf"; }
        }
	
		public override string EditLink
		{
			get { return "/newlos/Disclosure/BorrowerCertificationForm.aspx"; }
		}

        protected override  void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string brokerName = "";
            IPreparerFields agent = dataLoan.GetPreparerOfForm( E_PreparerFormT.BorrCertification, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.IsValid) 
            {
                brokerName = agent.CompanyName;
            }

            AddFormFieldData("BrokerName", brokerName);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
                

        }



    }
}
