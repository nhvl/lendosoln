﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Pdf
{
    public class CRateLockConfirmationPDF : AbstractFileDBBasedPDF
    {
        protected override string FileDbKey
        {
            get { return sLId.ToString("N") + "_ratelock.pdf"; }
        }

        public override string Description
        {
            get { return "Rate Lock Confirmation"; }
        }

    }
}
