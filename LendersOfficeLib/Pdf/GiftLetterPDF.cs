using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CGiftLetterPDF : AbstractLetterPDF
	{

        public override string PdfFile 
        {
            get { return "GiftLetter.pdf"; }
        }

        public override string Description 
        {
            get { return "Gift Letter"; }
        }
        protected override  void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            string applicants = Tools.FormatAddress(dataApp.aBNm + Environment.NewLine + dataApp.aCNm, 
                dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip);

            AddFormFieldData("Applicants", applicants);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("PropertyAddress", dataLoan.sSpAddr_SingleLine);
      //      AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
        }


	}
}
