﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CMDNetTangibleWorksheetPDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "MDNetTangibleWorksheet.pdf"; }
        }
        public override string Description
        {
            get { return "Maryland Net Tangible Benefit Worksheet"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sSpAddr", dataLoan.sSpAddr);
            AddFormFieldData("sSpCity", dataLoan.sSpCity);
            AddFormFieldData("sSpState", dataLoan.sSpState);
            AddFormFieldData("sSpZip", dataLoan.sSpZip);

            var agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].AgentName", agent.AgentName);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].CompanyName", agent.CompanyName);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].StreetAddr", agent.StreetAddr);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].State", agent.State);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].City", agent.City);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].Zip", agent.Zip);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].Phone", agent.Phone);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].FaxNum", agent.FaxNum);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].EmailAddr", agent.EmailAddr);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].LicenseNumOfCompany", agent.LicenseNumOfCompany);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].LicenseNumOfAgent", agent.LicenseNumOfAgent);


            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("GetAgentOfRole[Lender].CompanyName", agent.CompanyName);
            AddFormFieldData("GetAgentOfRole[Lender].FaxNum", agent.FaxNum);
            AddFormFieldData("GetAgentOfRole[Lender].StreetAddr", agent.StreetAddr);
            AddFormFieldData("GetAgentOfRole[Lender].State", agent.State);
            AddFormFieldData("GetAgentOfRole[Lender].City", agent.City);
            AddFormFieldData("GetAgentOfRole[Lender].Zip", agent.Zip);
            AddFormFieldData("GetAgentOfRole[Lender].EmailAddr", agent.EmailAddr);
            AddFormFieldData("GetAgentOfRole[Lender].Phone", agent.Phone);
            AddFormFieldData("GetAgentOfRole[Lender].LicenseNumOfCompany", agent.LicenseNumOfCompany);
            AddFormFieldData("GetAgentOfRole[Lender].LicenseNumOfAgent", agent.LicenseNumOfAgent);

            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.CreateNewDoNotFallBack);
            AddFormFieldData("GetAgentOfRole[Broker].CompanyName", agent.CompanyName);
            AddFormFieldData("GetAgentOfRole[Broker].StreetAddr", agent.StreetAddr);
            AddFormFieldData("GetAgentOfRole[Broker].State", agent.State);
            AddFormFieldData("GetAgentOfRole[Broker].City", agent.City);
            AddFormFieldData("GetAgentOfRole[Broker].Zip", agent.Zip);
            AddFormFieldData("GetAgentOfRole[Broker].Phone", agent.Phone);
            AddFormFieldData("GetAgentOfRole[Broker].FaxNum", agent.FaxNum);
            AddFormFieldData("GetAgentOfRole[Broker].EmailAddr", agent.EmailAddr);
            AddFormFieldData("GetAgentOfRole[Broker].LicenseNumOfCompany", agent.LicenseNumOfCompany);
            AddFormFieldData("GetAgentOfRole[Broker].LicenseNumOfAgent", agent.LicenseNumOfAgent);

        }
    }
}