using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CCAImpoundStatementPDF : AbstractLetterPDF
	{

		public override string EditLink
		{
			get { return "/newlos/Disclosure/CAImpoundStatement.aspx"; }
		}
        public override string PdfFile 
        {
            get { return "CAImpoundStatement.pdf"; }
        }
        public override string Description 
        {
            get { return "CA Impound Statement"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);

            IPreparerFields brokerInfo = dataLoan.GetPreparerOfForm( E_PreparerFormT.CACompoundStatement, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("BrokerName", brokerInfo.CompanyName);

            string addr = Tools.FormatAddress(brokerInfo.StreetAddr, brokerInfo.City, brokerInfo.State, brokerInfo.Zip);
            addr += Environment.NewLine;

            if (brokerInfo.PhoneOfCompany.TrimWhitespaceAndBOM() != "") 
            {
                addr += "Phone: " + brokerInfo.PhoneOfCompany + " ";
            }
            if (brokerInfo.FaxOfCompany.TrimWhitespaceAndBOM() != "") 
            {
                addr += "Fax: " + brokerInfo.FaxOfCompany;
            }
            AddFormFieldData("BrokerAddress", addr);

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }

	}
}
