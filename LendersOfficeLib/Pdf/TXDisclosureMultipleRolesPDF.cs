using System;
using System.Collections;
using System.Collections.Specialized;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
	public class CTXDisclosureMultipleRolesPDF : AbstractLetterPDF
	{

        public override string Description 
        {
            get { return "Texas - Disclosure of Multiple Roles"; }
        }
        public override string PdfFile 
        {
            get { return "TXDisclosureMultipleRoles.pdf"; }
        }
        public override string EditLink 
		{
			get { return "/newlos/Disclosure/TXDisclosureMultipleRoles.aspx"; }
		}

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);

            string name = dataApp.aBNm;
            if ("" != dataApp.aCNm.TrimWhitespaceAndBOM())
                name += " & " + dataApp.aCNm;

            AddFormFieldData("BorrowerName", name);

            CAgentFields loanOfficer = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("LoanOfficerName", loanOfficer.AgentName);
        }
	}
}
