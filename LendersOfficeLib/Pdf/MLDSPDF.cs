using System;
using System.Collections.Specialized;
using System.Globalization;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public abstract class AbstractMLDSPDF : AbstractLegalPDF 
    {
        private LosConvert m_convertLos;

        private bool isPaidToBroker(int props )
        {
            return LosConvert.GfeItemProps_ToBr( props );
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            m_convertLos = dataLoan.m_convertLos;

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 

            AddFormFieldData("GfeTilPreparerName",		gfeTil.PreparerName);
            AddFormFieldData("GfeTilLicenseNumOfAgent",	gfeTil.LicenseNumOfAgent);
            AddFormFieldData("GfeTilCompanyName",			gfeTil.CompanyName);
            AddFormFieldData("GfeTilStreetAddr",		gfeTil.StreetAddr);
            AddFormFieldData("GfeTilCityStateZip",		Tools.CombineCityStateZip(gfeTil.City, gfeTil.State, gfeTil.Zip));
            AddFormFieldData("GfeTilLicenseNumOfCompany",		gfeTil.LicenseNumOfCompany);
            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);

            CAgentFields lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("LenderName", lender.CompanyName);
            AddFormFieldData("IsLenderKnown", lender.CompanyName.TrimWhitespaceAndBOM() != "");
		

            string borrowerName = dataApp.aBNm;
            string coborrowerName = dataApp.aCNm;

            string propertyAddress = dataLoan.sSpAddr + ", " + dataLoan.sSpCity + ", " + dataLoan.sSpState + " " + dataLoan.sSpZip;

            AddFormFieldData("ApplicantName", borrowerName);
            string applicantsName = borrowerName;
            if (coborrowerName.TrimWhitespaceAndBOM() != "") 
            {
                applicantsName += " & " + coborrowerName;
            }
            AddFormFieldData("ApplicantsName", applicantsName);
            AddFormFieldData("CoapplicantName", coborrowerName);
            AddFormFieldData("PropertyAddress", propertyAddress);


            //// TODO: Need to put cost amount in the correct columns (paid to other, paid to borrower).
            AddFormFieldData("BorrowerName", borrowerName);

            string postfix = "O";

            if (dataLoan.sLOrigFMlds_rep != "") 
            {
                postfix = isPaidToBroker(dataLoan.sLOrigFProps) ? "B" : "O";
                AddFormFieldData("sLOrigF" + postfix, dataLoan.sLOrigFMlds_rep);
            }
            if (dataLoan.sLDiscnt != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sLDiscntProps) ? "B" : "O";
                AddFormFieldData("sLDiscnt" + postfix, dataLoan.sLDiscnt_rep);
            }
            if (dataLoan.sApprF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sApprFProps) ? "B" : "O";
                AddFormFieldData("sApprF" + postfix, dataLoan.sApprF_rep);
            }
            if (dataLoan.sCrF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sCrFProps) ? "B" : "O";
                AddFormFieldData("sCrF" + postfix, dataLoan.sCrF_rep);
            }
            if (dataLoan.sInspectF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sInspectFProps) ? "B" : "O";
                AddFormFieldData("sInspectF" + postfix, dataLoan.sInspectF_rep);
            }
            if (dataLoan.sMBrokF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sMBrokFProps) ? "B" : "O";
                AddFormFieldData("sMBrokF" + postfix, dataLoan.sMBrokF_rep);
            }
            if (dataLoan.sTxServF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sTxServFProps) ? "B" : "O";
                AddFormFieldData("sTxServF" + postfix, dataLoan.sTxServF_rep);
            }
            if (dataLoan.sProcF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sProcFProps) ? "B" : "O";
                AddFormFieldData("sProcF" + postfix, dataLoan.sProcF_rep);
            }
            if (dataLoan.sUwF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sUwFProps) ? "B" : "O";
                AddFormFieldData("sUwF" + postfix, dataLoan.sUwF_rep);
            }
            if (dataLoan.sWireF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sWireFProps) ? "B" : "O";
                AddFormFieldData("sWireF" + postfix, dataLoan.sWireF_rep);
            }
            if (dataLoan.s800U1F != 0) 
            {
                postfix = isPaidToBroker(dataLoan.s800U1FProps) ? "B" : "O";
                AddFormFieldData("s800U1F" + postfix, dataLoan.s800U1F_rep);
            }
            if (dataLoan.s800U2F != 0) 
            {
                postfix = isPaidToBroker(dataLoan.s800U2FProps) ? "B" : "O";
                AddFormFieldData("s800U2F" + postfix, dataLoan.s800U2F_rep);
            }
            if (dataLoan.s800U3F != 0) 
            {
                postfix = isPaidToBroker(dataLoan.s800U3FProps) ? "B" : "O";
                AddFormFieldData("s800U3F" + postfix, dataLoan.s800U3F_rep);
            }
            if (dataLoan.s800U4F != 0) 
            {
                postfix = isPaidToBroker(dataLoan.s800U4FProps) ? "B" : "O";
                AddFormFieldData("s800U4F" + postfix, dataLoan.s800U4F_rep);
            }
            if (dataLoan.s800U5F != 0) 
            {
                postfix = isPaidToBroker(dataLoan.s800U5FProps) ? "B" : "O";
                AddFormFieldData("s800U5F" + postfix, dataLoan.s800U5F_rep);
            }
            if (dataLoan.sEscrowF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sEscrowFProps) ? "B" : "O";
                AddFormFieldData("sEscrowF" + postfix, dataLoan.sEscrowF_rep);
            }
            if (dataLoan.sDocPrepF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sDocPrepFProps) ? "B" : "O";
                AddFormFieldData("sDocPrepF" + postfix, dataLoan.sDocPrepF_rep);
            }
            if (dataLoan.sNotaryF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sNotaryFProps) ? "B" : "O";
                AddFormFieldData("sNotaryF" + postfix, dataLoan.sNotaryF_rep);
            }
            if (dataLoan.sAttorneyF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sAttorneyFProps) ? "B" : "O";
                AddFormFieldData("sAttorneyF" + postfix, dataLoan.sAttorneyF_rep);
            }
            if (dataLoan.sTitleInsF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sTitleInsFProps) ? "B" : "O";
                AddFormFieldData("sTitleInsF" + postfix, dataLoan.sTitleInsF_rep);
            }
            if (dataLoan.sU1Tc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU1TcProps) ? "B" : "O";
                AddFormFieldData("sU1Tc" + postfix, dataLoan.sU1Tc_rep);
            }
            if (dataLoan.sU2Tc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU2TcProps) ? "B" : "O";
                AddFormFieldData("sU2Tc" + postfix, dataLoan.sU2Tc_rep);
            }
            if (dataLoan.sU3Tc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU3TcProps) ? "B" : "O";
                AddFormFieldData("sU3Tc" + postfix, dataLoan.sU3Tc_rep);
            }
            if (dataLoan.sU4Tc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU4TcProps) ? "B" : "O";
                AddFormFieldData("sU4Tc" + postfix, dataLoan.sU4Tc_rep);
            }
            if (dataLoan.sRecF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sRecFProps) ? "B" : "O";
                AddFormFieldData("sRecF" + postfix, dataLoan.sRecF_rep);
            }
            if (dataLoan.sCountyRtc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sCountyRtcProps) ? "B" : "O";
                AddFormFieldData("sCountyRtc" + postfix, dataLoan.sCountyRtc_rep);
            }
            if (dataLoan.sStateRtc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sStateRtcProps) ? "B" : "O";
                AddFormFieldData("sStateRtc" + postfix, dataLoan.sStateRtc_rep);
            }
            if (dataLoan.sU1GovRtc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU1GovRtcProps) ? "B" : "O";
                AddFormFieldData("sU1GovRtc" + postfix, dataLoan.sU1GovRtc_rep);
            }
            if (dataLoan.sU2GovRtc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU2GovRtcProps) ? "B" : "O";
                AddFormFieldData("sU2GovRtc" + postfix, dataLoan.sU2GovRtc_rep);
            }
            if (dataLoan.sU3GovRtc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU3GovRtcProps) ? "B" : "O";
                AddFormFieldData("sU3GovRtc" + postfix, dataLoan.sU3GovRtc_rep);
            }
            if (dataLoan.sPestInspectF != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sPestInspectFProps) ? "B" : "O";
                AddFormFieldData("sPestInspectF" + postfix, dataLoan.sPestInspectF_rep);
            }
            if (dataLoan.sU1Sc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU1ScProps) ? "B" : "O";
                AddFormFieldData("sU1Sc" + postfix, dataLoan.sU1Sc_rep);
            }
            if (dataLoan.sU2Sc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU2ScProps) ? "B" : "O";
                AddFormFieldData("sU2Sc" + postfix, dataLoan.sU2Sc_rep);
            }
            if (dataLoan.sU3Sc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU3ScProps) ? "B" : "O";
                AddFormFieldData("sU3Sc" + postfix, dataLoan.sU3Sc_rep);
            }
            if (dataLoan.sU4Sc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU4ScProps) ? "B" : "O";
                AddFormFieldData("sU4Sc" + postfix, dataLoan.sU4Sc_rep);
            }
            if (dataLoan.sU5Sc != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sU5ScProps) ? "B" : "O";
                AddFormFieldData("sU5Sc" + postfix, dataLoan.sU5Sc_rep);
            }
            if (dataLoan.sIPia != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sIPiaProps) ? "B" : "O";
                AddFormFieldData("sIPia" + postfix, dataLoan.sIPia_rep);
            }
            if (dataLoan.sMipPia != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sMipPiaProps) ? "B" : "O";
                AddFormFieldData("sMipPia" + postfix, dataLoan.sMipPia_rep);
            }
            if (dataLoan.sHazInsPia != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sHazInsPiaProps) ? "B" : "O";
                AddFormFieldData("sHazInsPia" + postfix, dataLoan.sHazInsPia_rep);
            }
            if (dataLoan.s904Pia != 0) 
            {
                postfix = isPaidToBroker(dataLoan.s904PiaProps) ? "B" : "O";
                AddFormFieldData("s904Pia" + postfix, dataLoan.s904Pia_rep);
            }
            if (dataLoan.sVaFf != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sVaFfProps) ? "B" : "O";
                AddFormFieldData("sVaFf" + postfix, dataLoan.sVaFf_rep);
            }
            if (dataLoan.s900U1Pia != 0) 
            {
                postfix = isPaidToBroker(dataLoan.s900U1PiaProps) ? "B" : "O";
                AddFormFieldData("s900U1Pia" + postfix, dataLoan.s900U1Pia_rep);
            }
            if (dataLoan.sHazInsRsrv != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sHazInsRsrvProps) ? "B" : "O";
                AddFormFieldData("sHazInsRsrv" + postfix, dataLoan.sHazInsRsrv_rep);
            }
            if (dataLoan.sMInsRsrv != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sMInsRsrvProps) ? "B" : "O";
                AddFormFieldData("sMInsRsrv" + postfix, dataLoan.sMInsRsrv_rep);
            }
            if (dataLoan.sSchoolTxRsrv != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sSchoolTxRsrvProps) ? "B" : "O";
                AddFormFieldData("sSchoolTxRsrv" + postfix, dataLoan.sSchoolTxRsrv_rep);
            }
            if (dataLoan.sRealETxRsrv != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sRealETxRsrvProps) ? "B" : "O";
                AddFormFieldData("sRealETxRsrv" + postfix, dataLoan.sRealETxRsrv_rep);
            }
            if (dataLoan.sFloodInsRsrv != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sFloodInsRsrvProps) ? "B" : "O";
                AddFormFieldData("sFloodInsRsrv" + postfix, dataLoan.sFloodInsRsrv_rep);
            }
            if (dataLoan.s1006Rsrv != 0) 
            {
                postfix = isPaidToBroker(dataLoan.s1006RsrvProps) ? "B" : "O";
                AddFormFieldData("s1006Rsrv" + postfix, dataLoan.s1006Rsrv_rep);
                AddFormFieldData("s1006", "1006");
            }
            if (dataLoan.s1007Rsrv != 0) 
            {
                postfix = isPaidToBroker(dataLoan.s1007RsrvProps) ? "B" : "O";
                AddFormFieldData("s1007Rsrv" + postfix, dataLoan.s1007Rsrv_rep);
                AddFormFieldData("s1007", "1007");
            }

            if (dataLoan.sAggregateAdjRsrv != 0) 
            {
                postfix = isPaidToBroker(dataLoan.sAggregateAdjRsrvProps) ? "B" : "O";
                AddFormFieldData("sAggregateAdjRsrv" + postfix, dataLoan.sAggregateAdjRsrv_rep);
                AddFormFieldData("sAggregateAdjRsrvDesc", "Aggregate Adjustment", false);
                AddFormFieldData("s1008", "1008");
            }

            AddFormFieldData("s800U1FCode", dataLoan.s800U1FCode);
            AddFormFieldData("s800U1FDesc", dataLoan.s800U1FDesc);
            AddFormFieldData("s800U2FCode", dataLoan.s800U2FCode);
            AddFormFieldData("s800U2FDesc", dataLoan.s800U2FDesc);
            AddFormFieldData("s800U3FCode", dataLoan.s800U3FCode);
            AddFormFieldData("s800U3FDesc", dataLoan.s800U3FDesc);
            AddFormFieldData("s800U4FCode", dataLoan.s800U4FCode);
            AddFormFieldData("s800U4FDesc", dataLoan.s800U4FDesc);
            AddFormFieldData("s800U5FCode", dataLoan.s800U5FCode);
            AddFormFieldData("s800U5FDesc", dataLoan.s800U5FDesc);
            AddFormFieldData("sU1TcCode", dataLoan.sU1TcCode);
            AddFormFieldData("sU1TcDesc", dataLoan.sU1TcDesc);
            AddFormFieldData("sU2TcCode", dataLoan.sU2TcCode);
            AddFormFieldData("sU2TcDesc", dataLoan.sU2TcDesc);
            AddFormFieldData("sU3TcCode", dataLoan.sU3TcCode);
            AddFormFieldData("sU3TcDesc", dataLoan.sU3TcDesc);
            AddFormFieldData("sU4TcCode", dataLoan.sU4TcCode);
            AddFormFieldData("sU4TcDesc", dataLoan.sU4TcDesc);
            AddFormFieldData("sU1GovRtcCode", dataLoan.sU1GovRtcCode);
            AddFormFieldData("sU1GovRtcDesc", dataLoan.sU1GovRtcDesc);
            AddFormFieldData("sU2GovRtcCode", dataLoan.sU2GovRtcCode);
            AddFormFieldData("sU2GovRtcDesc", dataLoan.sU2GovRtcDesc);
            AddFormFieldData("sU3GovRtcCode", dataLoan.sU3GovRtcCode);
            AddFormFieldData("sU3GovRtcDesc", dataLoan.sU3GovRtcDesc);
            AddFormFieldData("sU1ScCode", dataLoan.sU1ScCode);
            AddFormFieldData("sU1ScDesc", dataLoan.sU1ScDesc);
            AddFormFieldData("sU2ScCode", dataLoan.sU2ScCode);
            AddFormFieldData("sU2ScDesc", dataLoan.sU2ScDesc);
            AddFormFieldData("sU3ScCode", dataLoan.sU3ScCode);
            AddFormFieldData("sU3ScDesc", dataLoan.sU3ScDesc);
            AddFormFieldData("sU4ScCode", dataLoan.sU4ScCode);
            AddFormFieldData("sU4ScDesc", dataLoan.sU4ScDesc);
            AddFormFieldData("sU5ScCode", dataLoan.sU5ScCode);
            AddFormFieldData("sU5ScDesc", dataLoan.sU5ScDesc);
            AddFormFieldData("s904PiaDesc", dataLoan.s904PiaDesc);
            AddFormFieldData("s900U1PiaCode", dataLoan.s900U1PiaCode);
            AddFormFieldData("s900U1PiaDesc", dataLoan.s900U1PiaDesc);
            AddFormFieldData("s1006ProHExpDesc", dataLoan.s1006ProHExpDesc);
            if (dataLoan.s1006ProHExpDesc != "") 
            {
                AddFormFieldData("s1006", "1006");
            }
            AddFormFieldData("s1007ProHExpDesc", dataLoan.s1007ProHExpDesc);
            if (dataLoan.s1007ProHExpDesc.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("s1007", "1007");
            }
            AddFormFieldData("sIPiaDy", dataLoan.sIPiaDy_rep);
            AddFormFieldData("sIPerDay", dataLoan.sIPerDay_rep);
            AddFormFieldData("sProHazIns", dataLoan.sProHazIns_rep);
            AddFormFieldData("sProMIns", dataLoan.sProMIns_rep);
            AddFormFieldData("sProSchoolTx", dataLoan.sProSchoolTx_rep);
            AddFormFieldData("sProRealETx", dataLoan.sProRealETx_rep);
            AddFormFieldData("sProFloodIns", dataLoan.sProFloodIns_rep);
            AddFormFieldData("s1006ProHExp", dataLoan.s1006ProHExp_rep);
            AddFormFieldData("s1007ProHExp", dataLoan.s1007ProHExp_rep);
            AddFormFieldData("sHazInsRsrvMon", dataLoan.sHazInsRsrvMon_rep);
            AddFormFieldData("sMInsRsrvMon", dataLoan.sMInsRsrvMon_rep);
            AddFormFieldData("sSchoolTxRsrvMon", dataLoan.sSchoolTxRsrvMon_rep);
            AddFormFieldData("sRealETxRsrvMon", dataLoan.sRealETxRsrvMon_rep);
            AddFormFieldData("sFloodInsRsrvMon", dataLoan.sFloodInsRsrvMon_rep);
            AddFormFieldData("s1006RsrvMon", dataLoan.s1006RsrvMon_rep);
            AddFormFieldData("s1007RsrvMon", dataLoan.s1007RsrvMon_rep);
            string sLOrigFPc = "";
            if (dataLoan.sLOrigFPc != 0) 
            {
                sLOrigFPc = dataLoan.sLOrigFPc_rep + "%";
            }
            if (dataLoan.sLOrigFMb != 0) 
            {
                sLOrigFPc += " + " + (dataLoan.sLOrigFMb_rep);
            }
            AddFormFieldData("sLOrigFPc", sLOrigFPc);

            string sLDiscntPc = "";
            if (dataLoan.sLDiscntPc != 0) 
            {
                sLDiscntPc = dataLoan.sLDiscntPc_rep + "%";
            }
            if (dataLoan.sLDiscntFMb != 0) 
            {
                sLDiscntPc += " + " + (dataLoan.sLDiscntFMb_rep);
            }
            AddFormFieldData("sLDiscntPc", sLDiscntPc);
            string sMBrokFPc = "";
            if (dataLoan.sMBrokFPc != 0) 
            {
                sMBrokFPc = dataLoan.sMBrokFPc_rep + "%";
            }
            if (dataLoan.sMBrokFMb != 0) 
            {
                sMBrokFPc += " + " + (dataLoan.sMBrokFMb_rep);
            }
            AddFormFieldData("sMBrokFPc", sMBrokFPc);



            AddFormFieldData("sTotEstScMlds", dataLoan.sTotEstScMlds_rep);
            AddFormFieldData("sBrokComp1Mlds", dataLoan.sBrokComp1Mlds_rep);
            if (dataLoan.sHasBrokComp2Mlds) 
            {
                AddFormFieldData("isBrokComp2Mlds", true);
                AddFormFieldData("sBrokComp2Mlds", dataLoan.sBrokComp2Mlds_rep);
            } 
            else 
            {
                AddFormFieldData("isBrokComp2Mlds", false);
            }

            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sFinMethT", dataLoan.sFinMethT);
            AddFormFieldData("sPurchPrice_sRefPdOffAmtCamlds", dataLoan.sPurchPrice_sRefPdOffAmtCamlds_rep);
            AddFormFieldData("sDisabilityIns", dataLoan.sDisabilityIns_rep);
            if (dataLoan.sTotCcPboPbs != 0) 
            {
                AddFormFieldData("sU0FntcDesc", "Paid by Seller and Others (Credit)");
                AddFormFieldData("sTotCcPboPbs", "(" + dataLoan.sTotCcPboPbs_rep + ")");
            }
            if (dataLoan.sU1Fntc != 0) 
            {
                AddFormFieldData("sU1Fntc", dataLoan.sU1Fntc_rep);
                AddFormFieldData("sU1FntcDesc", dataLoan.sU1FntcDesc);
            }
            AddFormFieldData("sTotDeductFromFinalLAmt", dataLoan.sTotDeductFromFinalLAmt_rep);
            if (dataLoan.sTotEstFntcCamlds < 0)  // Cash back to borrower.
            {
                AddFormFieldData("sCashBackToBorrower", true);
            } 
            else // Cash borrower need to pay.
            {
                AddFormFieldData("sCashBackToBorrower", false);
            }
			dataLoan.SetFormatTarget( FormatTarget.PrintoutImportantFields );
			AddFormFieldData("sTotEstFntcCamlds", dataLoan.sTotEstFntcCamldsAbsVal_rep);
			dataLoan.SetFormatTarget( FormatTarget.PrintoutNormalFields );

            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep + " / " + dataLoan.sDue_rep);
            AddFormFieldData("sTerm1", dataLoan.sTerm_rep);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            AddFormFieldData("sBalloonPmt", dataLoan.sBalloonPmt);
            AddFormFieldData("sFinalBalloonPmtDueD", dataLoan.sFinalBalloonPmtDueD_rep);
            AddFormFieldData("sFinalBalloonPmt", dataLoan.sFinalBalloonPmt_rep);
            if (dataLoan.sLienholder1NmBefore.TrimWhitespaceAndBOM() == "" && dataLoan.sLienholder2NmBefore.TrimWhitespaceAndBOM() == "" && 
                dataLoan.sLienholder3NmBefore.TrimWhitespaceAndBOM() == "") 
            {
                AddFormFieldData("IsObligatedLien", false);
            } 
            else 
            {
                AddFormFieldData("IsObligatedLien", true);
            }
            if (dataLoan.sLienholder1NmBefore.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("sLienholder1NmBefore", dataLoan.sLienholder1NmBefore);
                AddFormFieldData("sLien1AmtBefore", dataLoan.sLien1AmtBefore_rep);
                AddFormFieldData("sLien1PriorityBefore", dataLoan.sLien1PriorityBefore);
            }
            if (dataLoan.sLienholder2NmBefore.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("sLienholder2NmBefore", dataLoan.sLienholder2NmBefore);
                AddFormFieldData("sLien2AmtBefore", dataLoan.sLien2AmtBefore_rep);
                AddFormFieldData("sLien2PriorityBefore", dataLoan.sLien2PriorityBefore);
            }
            if (dataLoan.sLienholder3NmBefore.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("sLienholder3NmBefore", dataLoan.sLienholder3NmBefore);
                AddFormFieldData("sLien3AmtBefore", dataLoan.sLien3AmtBefore_rep);
                AddFormFieldData("sLien3PriorityBefore", dataLoan.sLien3PriorityBefore);
            }
            if (dataLoan.sLienholder1NmAfter.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("sLienholder1NmAfter", dataLoan.sLienholder1NmAfter);
                AddFormFieldData("sLien1AmtAfter", dataLoan.sLien1AmtAfter_rep);
                AddFormFieldData("sLien1PriorityAfter", dataLoan.sLien1PriorityAfter);

            }
            if (dataLoan.sLienholder2NmAfter.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("sLienholder2NmAfter", dataLoan.sLienholder2NmAfter);
                AddFormFieldData("sLien2AmtAfter", dataLoan.sLien2AmtAfter_rep);
                AddFormFieldData("sLien2PriorityAfter", dataLoan.sLien2PriorityAfter);

            }
            if (dataLoan.sLienholder3NmAfter.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("sLienholder3NmAfter", dataLoan.sLienholder3NmAfter);
                AddFormFieldData("sLien3AmtAfter", dataLoan.sLien3AmtAfter_rep);
                AddFormFieldData("sLien3PriorityAfter", dataLoan.sLien3PriorityAfter);

            }
            AddFormFieldData("sTotCcPtb", dataLoan.sTotCcPtb_rep);
            AddFormFieldData("sTotCcPto", dataLoan.sTotCcPto_rep);
            AddFormFieldData("sBrokControlledFundT", dataLoan.sBrokControlledFundT);
            AddFormFieldData("sMldsPpmtT", dataLoan.sMldsPpmtT);
            if (dataLoan.sMldsPpmtT == E_sMldsPpmtT.Penalty) 
            {
                AddFormFieldData("sMldsPpmtBaseT", dataLoan.sMldsPpmtBaseT);
                AddFormFieldData("sMldsPpmtMonMax", dataLoan.sMldsPpmtMonMax_rep);
            }


        }

    }

    public class CMLDS_1PDF : AbstractMLDSPDF 
    {
        public override string PdfFile 
        {
            get { return "MLDS_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }

    }

    public class CMLDS_2PDF : AbstractMLDSPDF 
    {
        public override string PdfFile 
        {
            get { return "MLDS_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx?pg=1"; }
        }

    }
    public class CMLDSPDF : AbstractBatchPDF
    {

        public override string Description 
        {
            get { return "CA Mortgage Loan Disclosure Statement (MLDS)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
 
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CMLDS_1PDF(),
                                                 new CMLDS_2PDF()
                                             };
            }
        }
    }
    public class CMLDS_s_1PDF : AbstractMLDSPDF 
    {
        public override string PdfFile 
        {
            get { return "MLDS_s_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }

    }

    public class CMLDS_s_2PDF : AbstractMLDSPDF 
    {
        public override string PdfFile 
        {
            get { return "MLDS_s_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx?pg=1"; }
        }

    }
    public class CMLDS_sPDF : AbstractBatchPDF
    {

        public override string Description 
        {
            get { return "CA Mortgage Loan Disclosure Statement (MLDS Spanish Version)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
 
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CMLDS_s_1PDF(),
                                                 new CMLDS_s_2PDF()
                                             };
            }
        }
    }
}
