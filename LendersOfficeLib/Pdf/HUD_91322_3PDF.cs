using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CHUD_91322_3PDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "HUD-91322-3.pdf";        	 }
        }
        public override string Description 
        {
            get { return "FHA Appraised Value (HUD-91322-3)"; }
        }
		public override bool OnlyPrintPrimary
		{
			get { return true; }
		}
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            // 2/20/2004 dd - Currently there is no edit screen for this printout. Therefore I will use
            // mortgagee information from FHA D.E Statement of Appraised Value (HUD-92800.5B).
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACondCommitMortgagee, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            AddFormFieldData("FHACondCommitMortgagee", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            string sFHAHousingActSection = dataLoan.sFHAHousingActSection;

            AddFormFieldData("Section203b", sFHAHousingActSection.StartsWith("203(b)"));
            if (!sFHAHousingActSection.StartsWith("203(b)"))
                AddFormFieldData("sFHAHousingActSection", sFHAHousingActSection);

            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sProHazIns", dataLoan.sProHazIns_rep);
            AddFormFieldData("sProRealETx", dataLoan.sProRealETx_rep);
            AddFormFieldData("sProHoAssocDues", dataLoan.sProHoAssocDues_rep);
            AddFormFieldData("sFHACondCommIssueD", dataLoan.sFHACondCommIssuedD_rep);
            AddFormFieldData("sFHACondCommExpiredD", dataLoan.sFHACondCommExpiredD_rep);
            AddFormFieldData("sTotEstCc", dataLoan.sTotEstCc_rep);
        }
	}
}
