using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Collections.Generic;

namespace LendersOffice.Pdf
{
	public class IndividualCustomForm : IPDFPrintItem
	{
        private Guid m_brokerID;
        private NameValueCollection m_arguments;
        private Guid m_customFormID;

        public string Name 
        {
            get { return "Invidiual Name"; }
        }

        public string ID 
        {
            get { return "IndividualCustomForm"; }
            set { }
        }
        public Guid CustomFormID 
        {
            get { return m_customFormID; }
            set { m_customFormID = value; }
        }
        public string Description 
        {
            get { return m_customFormID.ToString(); }
        }
        public string EditLink 
        {
            get { return ""; }
        }

        public string PreviewLink
        {
            get { return ""; }
        }


        public CPageData DataLoan
        {
            get { return null; }
            set {  }
        }

        public NameValueCollection Arguments
        {
            get { return this.m_arguments; }
            set 
            {
                this.m_arguments = value;

                if (this.m_arguments == null)
                {
                    return;
                }

                Guid argBrokerId;

                if (Guid.TryParse(this.m_arguments["brokerid"], out argBrokerId))
                {
                    this.m_brokerID = argBrokerId;
                }
            }
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public bool HasPdf
        {
            get { return true; }
        }

        public void RenderPrintLink(StringBuilder sb) 
        {
            SqlParameter[] parameters = { 
                                            new SqlParameter("@BrokerID", m_brokerID),
                                            new SqlParameter("@CustomLetterID", m_customFormID)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerID, "RetrieveCustomForm", parameters)) 
            {
                if (reader.Read()) 
                {
                    string title = (string) reader["Title"];
                    sb.Append(string.Format(@"<tr><td><input name='_{0}' type=checkbox customform=t customletterid='{2}'></td><td>{1} (<a href=""#"" onclick=""downloadCustomForm('{2}');"">preview</a>)</td></tr>", m_customFormID.ToString("N"), Utilities.SafeHtmlString(title), m_customFormID));
                }
            }
        }



        #region IPDFPrintItem Members


        public virtual bool HasPrintPermission
        {
            get { return true; }
        }

        #endregion

        #region IPDFPrintItem Members


        public IEnumerable<string> DependencyFields
        {
            get { return new List<string>(); }
        }

        #endregion
    }
}
