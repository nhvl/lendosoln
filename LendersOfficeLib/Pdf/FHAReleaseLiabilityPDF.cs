using System;
using System.Collections.Specialized;
using System.Data;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CFHAReleaseLiabilityPDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "FHAReleaseLiability.pdf"; }
        }
        public override string Description 
        {
            get { return "FHA Release of Personal Liability"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            AddFormFieldData("sSpAddr", dataLoan.sSpAddr);
            AddFormFieldData("sSpCityStateZip", Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
    }
}
