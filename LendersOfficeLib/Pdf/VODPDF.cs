namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using System.Collections.Generic;
    using LendersOffice.Admin;

    public class CVODListPDF : AbstractVerificationListPDF 
    {
        public override string Description 
        {
            get { return "Verification of Deposit"; }
        }

        protected override IEnumerable<PdfVerificationItem> GetItems(CAppData dataApp)
        {
                List<PdfVerificationItem> list = new List<PdfVerificationItem>();

                var subCollection = dataApp.aAssetCollection.GetSubcollection(false, E_AssetGroupT.DontNeedVOD);
                Dictionary<string, bool> hash = new Dictionary<string,bool>(StringComparer.OrdinalIgnoreCase); // small hash table of depository name.

                string aBNm = Utilities.SafeHtmlString(dataApp.aBNm);
                string aCNm = Utilities.SafeHtmlString(dataApp.aCNm);
                string displayName = aBNm;
                foreach (var item in subCollection)
                {
                    var o = (IAssetRegular)item;
                    if (!hash.ContainsKey(o.ComNm))
                    {
                        switch (o.OwnerT)
                        {
                            case E_AssetOwnerT.Borrower: displayName = aBNm; break;
                            case E_AssetOwnerT.CoBorrower: displayName = aCNm; break;
                            case E_AssetOwnerT.Joint: displayName = aBNm + " & " + aCNm; break;
                            default:
                                throw new UnhandledEnumException(o.OwnerT);
                        }

                        hash.Add(o.ComNm, true);
                        NameValueCollection c = null;
                        if (null == Arguments)
                        {
                            c = new NameValueCollection();
                        }
                        else
                        {
                            c = new NameValueCollection(Arguments);
                        }


                        displayName = displayName + ", " + Utilities.SafeHtmlString(o.ComNm);

                        list.Add(new PdfVerificationItem() { RecordId = o.RecordId, DisplayName = displayName });

                    } // if (!hash.ContainsKey(o.ComNm.ToLower())) 
                } // foreach(var item in subCollection) 

                return list;

        }

        protected override AbstractPDFFile ContructPdfItem()
        {
            return new CVODPDF();
        }
    }

    public class CVODPDF : AbstractLetterPDF
    {

        public override string PdfFile 
        {
            get { return "vod.pdf"; }
        }

        public override string Description 
        {
            get { return "VOD: " + Arguments["displayname"]; }
        }

        public override Guid RecordID 
        {
            get 
            {
                Guid id;

                if (Guid.TryParse(this.Arguments["recordid"], out id))
                {
                    return id;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        public override string EditLink 
        {
            get { return "/newlos/Verifications/VODRecord.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            var field = dataApp.aAssetCollection.GetRegRecordOf( RecordID );

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfDeposit, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string title = "";
            string prepareName = "";
            string lenderAddress = "";
            string name = "";

            if (preparer.IsValid) 
            {
                prepareName =  preparer.PreparerName;

                name = string.Format("{0}{2}{1}", prepareName, preparer.CompanyName, Environment.NewLine);
                string phone = "";
                if (preparer.Phone != "")
                {
                    phone = "Phone: " + preparer.Phone + "  ";
                }
                if (preparer.FaxNum != "")
                {
                    phone += "Fax: " + preparer.FaxNum;
                }
                title = preparer.Title;
                lenderAddress = Tools.FormatAddress(name, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, phone);

            }
            
            string lenderNumber = dataLoan.sLenderNumVerif;

            string borrowerName = dataApp.aBNmAndSsnForVerifications;
            string coborrowerName = dataApp.aCNmAndSsnForVerifications;

            string displayName = borrowerName;

            if (field.OwnerT == E_AssetOwnerT.CoBorrower) 
            {
                displayName = coborrowerName;
            } 
            else if (field.OwnerT == E_AssetOwnerT.Joint) 
            {
                displayName = borrowerName + Environment.NewLine + coborrowerName;
            }

            string applicantAddress = Tools.FormatAddress(null, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip);

            name = string.Format("{0}{3}{1}{3}{2}", field.Attention, field.DepartmentName, field.ComNm, Environment.NewLine);
            string depositoryAddress = Tools.FormatAddress(name, field.StAddr, field.City, field.State, field.Zip);

            if (dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp && field.VerifHasSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(dataLoan.sBrokerId, field.VerifSigningEmployeeId, dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
                AddFormFieldData("LenderSignature", signInfo.SignatureKey.ToString());
            }

            AddFormFieldData("DepositoryAddress", depositoryAddress);
            AddFormFieldData("LenderAddress", lenderAddress);
            AddFormFieldData("Title", title);
            // 12/12/03 dd - Use PrepareDate from PreparerField instead of VerifSentD.
            AddFormFieldData("Date", preparer.PrepareDate_rep);
            AddFormFieldData("sLenderNumVerif", lenderNumber);
            AddFormFieldData("ApplicantAddress", string.Format("{0}{2}{1}", displayName, applicantAddress, Environment.NewLine));
            if (field.IsSeeAttachment) 
            {

                AddFormFieldData("ApplicantSignature", "SEE ATTACHMENT");
                if (dataApp.aCSsn != string.Empty)
                {
                    AddFormFieldData("CoApplicantSignature", "SEE ATTACHMENT");
                }
            }
            AddFormFieldData("sLNm", dataLoan.sLNm);

            // Loop through list of asset record.
            int currentIndex = 0;
            var assetSubcoll = dataApp.aAssetCollection.GetSubcollection( false, E_AssetGroupT.DontNeedVOD);// skipping folowing types
            //				E_AssetGroupT.Auto | E_AssetGroupT.Stocks | E_AssetGroupT.Bonds | E_AssetGroupT.LifeInsurance );
            foreach( var item in assetSubcoll )
            {
                // Match and group similar Depository name.
                var f = (IAssetRegular)item;
                if (f.ComNm.ToLower() == field.ComNm.ToLower()) 
                {
                    if (currentIndex != 0 && currentIndex % 3 == 0) 
                    {
                        AddNewPage();
                        AddFormFieldData("DepositoryAddress", depositoryAddress);
                        AddFormFieldData("LenderAddress", lenderAddress);
                        AddFormFieldData("Title", title);
                        AddFormFieldData("Date", field.VerifSentD_rep);
                        AddFormFieldData("sLenderNumVerif", lenderNumber);
                        
                        AddFormFieldData("ApplicantAddress", string.Format("{0}{2}{1}", displayName, applicantAddress, Environment.NewLine));
                        if (field.IsSeeAttachment) 
                        {
                            AddFormFieldData("ApplicantSignature", "SEE ATTACHMENT");
                            if (dataApp.aCSsn != string.Empty)
                            {
                                AddFormFieldData("CoApplicantSignature", "SEE ATTACHMENT");
                            }

                        }
                        AddFormFieldData("sLNm", dataLoan.sLNm);


                        

                    }

                    AddFormFieldData("AccountType" + (currentIndex % 3), f.AssetT_rep );
                    AddFormFieldData("AccountNumber" + (currentIndex % 3), f.AccNum.Value);
                    AddFormFieldData("BorrowerName" + (currentIndex % 3), f.AccNm);
                    AddFormFieldData("AccountBalance" + (currentIndex % 3), f.Val_rep);

                    currentIndex++;
                }
            }


        }
    }
}
