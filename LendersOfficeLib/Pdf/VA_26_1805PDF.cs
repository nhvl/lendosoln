using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;


namespace LendersOffice.Pdf
{
	/// <summary>
	/// Summary description for CVA_26_1805PDF.
	/// </summary>
	public class CVA_26_1805_1ObsoletePDF : AbstractVA_26_1805PDF
	{
        public override string PdfFile 
        {
            get { return "VA-26-1805_06-2001_1.pdf";        	 }
        }
	    
        public override string Description 
        {
            get { return "Instructions"; }
        }

	}
    public class CVA_26_1805_2ObsoletePDF : AbstractVA_26_1805PDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-1805_06-2001_2.pdf";        	 }
        }
        public override string Description 
        {
            get { return "Request of Value - VA File Worksheet (26-1805) (Jun 2001) (OBSOLETE)"; }
        }
    }
    public class CVA_26_1805_3ObsoletePDF : AbstractVA_26_1805PDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-1805_06-2001_3.pdf";        	 }
        }
        public override string Description 
        {
            get { return "Request of Value - VA File Copy 1 (26-1805) (Jun 2001) (OBSOLETE)"; }
        }
	    
    }
    public class CVA_26_1805_4ObsoletePDF : AbstractVA_26_1805PDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-1805_06-2001_4.pdf";        	 }
        }
        public override string Description 
        {
            get { return "Request of Value - Requestor's Copy 2 (26-1805) (Jun 2001) (OBSOLETE)"; }
        }
    }
    public class CVA_26_1805_5ObsoletePDF : AbstractVA_26_1805PDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-1805_06-2001_5.pdf";        	 }
        }
        public override string Description 
        {
            get { return "Certificate of Value - Requestor's Copy 3 (26-1843) (Jun 2001) (OBSOLETE)"; }
        }

    }
    public class CVA_26_1805_6ObsoletePDF : AbstractVA_26_1805PDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-1805_06-2001_6.pdf";        	 }
        }
	    
        public override string Description 
        {
            get { return "Certificate of Value - VA File Copy 4 (26-1843) (Jun 2001) (OBSOLETE)"; }
        }
    }
    public class CVA_26_1805_7ObsoletePDF : AbstractVA_26_1805PDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-1805_06-2001_7.pdf";        	 }
        }
        public override string Description 
        {
            get { return "Certificate of Value - Purchaser Copy 5 (26-1843) (Jun 2001) (OBSOLETE)"; }
        }
    }
    public class CVA_26_1805_8ObsoletePDF : AbstractVA_26_1805PDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-1805_06-2001_8.pdf";        	 }
        }
        public override string Description 
        {
            get { return "Request of Value - Appraiser's Copy 6 (26-1805) (Jun 2001) (OBSOLETE)"; }
        }
    }

    public abstract class AbstractVA_26_1805PDF : AbstractLegalPDF 
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aBNmFull", Tools.FormatAddress(dataApp.aBNm, dataApp.aBAddrMail, dataApp.aBCityMail, dataApp.aBStateMail, dataApp.aBZipMail));
            AddFormFieldData("sSpUtilElecT", dataLoan.sSpUtilElecT);
            AddFormFieldData("sSpUtilGasT", dataLoan.sSpUtilGasT);
            AddFormFieldData("sSpUtilSanSewerT", dataLoan.sSpUtilSanSewerT);
            AddFormFieldData("sSpUtilWaterT", dataLoan.sSpUtilWaterT);
            AddFormFieldData("sVaBuildingStatusT", dataLoan.sVaBuildingStatusT);
            AddFormFieldData("sVaBuildingT", dataLoan.sVaBuildingT);
            AddFormFieldData("sVaSpOccT", dataLoan.sVaSpOccT);
            AddFormFieldData("sLotPurchaseSeparatelyTri", dataLoan.sLotPurchaseSeparatelyTri);
            AddFormFieldData("sSpMineralRightsReservedTri", dataLoan.sSpMineralRightsReservedTri);
            AddFormFieldData("sVaConstructWarrantyTri", dataLoan.sVaConstructWarrantyTri);
            AddFormFieldData("sVaIsFactoryFabricatedTri", dataLoan.sVaIsFactoryFabricatedTri);
            AddFormFieldData("sVaSaleContractAttachedTri", dataLoan.sVaSaleContractAttachedTri);
            AddFormFieldData("sVaStreetAccessPrivateTri", dataLoan.sVaStreetAccessPrivateTri);
            AddFormFieldData("sVaStreetMaintenancePrivateTri", dataLoan.sVaStreetMaintenancePrivateTri);

            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFHACondCommInstCaseRef", dataLoan.sFHACondCommInstCaseRef);
            AddFormFieldData("sVALenderIdCode", dataLoan.sVALenderIdCode);
            AddFormFieldData("sVASponsorAgentIdCode", dataLoan.sVASponsorAgentIdCode);
            AddFormFieldData("sLeaseHoldExpireD", dataLoan.sLeaseHoldExpireD_rep);
            AddFormFieldData("sProRealETxPerYr", dataLoan.sProRealETxPerYr_rep);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            AddFormFieldData("sSpAddrFull", Tools.FormatAddress(null, dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpCity, dataLoan.sSpZip, dataLoan.sSpCounty));
            AddFormFieldData("sSpHasClothesWasher", dataLoan.sSpHasClothesWasher);
            AddFormFieldData("sSpHasDishWasher", dataLoan.sSpHasDishWasher);
            AddFormFieldData("sSpHasDryer", dataLoan.sSpHasDryer);
            AddFormFieldData("sSpHasGarbageDisposal", dataLoan.sSpHasGarbageDisposal);
            AddFormFieldData("sSpHasOtherEquip", dataLoan.sSpHasOtherEquip);
            AddFormFieldData("sSpHasOven", dataLoan.sSpHasOven);
            AddFormFieldData("sSpHasRefrig", dataLoan.sSpHasRefrig);
            AddFormFieldData("sSpHasVentFan", dataLoan.sSpHasVentFan);
            AddFormFieldData("sSpHasWwCarpet", dataLoan.sSpHasWwCarpet);
            AddFormFieldData("sSpLeaseAnnualGroundRent", dataLoan.sSpLeaseAnnualGroundRent_rep);
            AddFormFieldData("sSpLeaseIs99Yrs", dataLoan.sSpLeaseIs99Yrs);
            AddFormFieldData("sSpLeaseIsRenewable", dataLoan.sSpLeaseIsRenewable);
            AddFormFieldData("sSpLegalDesc", dataLoan.sSpLegalDesc);
            AddFormFieldData("sSpLotAcres", dataLoan.sSpLotAcres);
            AddFormFieldData("sSpLotDimension", dataLoan.sSpLotDimension);
            AddFormFieldData("sSpLotIrregularSqf", dataLoan.sSpLotIrregularSqf);
            AddFormFieldData("sSpLotIsAcres", dataLoan.sSpLotIsAcres);
            AddFormFieldData("sSpLotIsIrregular", dataLoan.sSpLotIsIrregular);
            AddFormFieldData("sSpMineralRightsReservedExplain", dataLoan.sSpMineralRightsReservedExplain);
            AddFormFieldData("sSpOtherEquipDesc", dataLoan.sSpOtherEquipDesc);
            AddFormFieldData("sSpState", dataLoan.sSpState);
            AddFormFieldData("sSpZip", dataLoan.sSpZip);
            AddFormFieldData("sVaBuildingStatusTLckd", dataLoan.sVaBuildingStatusTLckd);
            AddFormFieldData("sVaConstructCompleteD", dataLoan.sVaConstructCompleteD_rep);
            AddFormFieldData("sVaConstructExpiredD", dataLoan.sVaConstructExpiredD_rep);
            AddFormFieldData("sVaConstructWarrantyProgramNm", dataLoan.sVaConstructWarrantyProgramNm);
            AddFormFieldData("sVaNumOfBuildings", dataLoan.sVaNumOfBuildings);
            AddFormFieldData("sVaNumOfLivingUnits", dataLoan.sVaNumOfLivingUnits);
            AddFormFieldData("sVaOwnerNm", dataLoan.sVaOwnerNm);
            AddFormFieldData("sVaRefiAmt", dataLoan.sVaRefiAmt_rep);
            AddFormFieldData("sVaSpOccupantNm", dataLoan.sVaSpOccupantNm);
            AddFormFieldData("sVaSpOccupantPhone", dataLoan.sVaSpOccupantPhone);
            AddFormFieldData("sVaSpRentalMonthly", dataLoan.sVaSpRentalMonthly_rep);
            AddFormFieldData("sVaTitleLimitDesc", dataLoan.sVaTitleLimitDesc);
            AddFormFieldData("sVaTitleLimitIsCondo", dataLoan.sVaTitleLimitIsCondo);
            AddFormFieldData("sVaTitleLimitIsPud", dataLoan.sVaTitleLimitIsPud);


            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805FirmMakingRequest, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1805FirmMakingRequest", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("VA26_1805FirmMakingRequestNotificationEmail", preparer.EmailAddr);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Broker, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1805BrokerCompanyName", preparer.CompanyName);
            AddFormFieldData("VA26_1805BrokerPhoneOfCompany", preparer.PhoneOfCompany);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805KeyAt, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1805KeyAt", Tools.FormatAddress(preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));


            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Builder, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1805Builder", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("VA26_1805BuilderId", preparer.LicenseNumOfCompany);
            AddFormFieldData("VA26_1805BuilderPhoneOfCompany", preparer.PhoneOfCompany);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Warrantor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1805Warrantor", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("VA26_1805WarrantorPhoneOfCompany", preparer.PhoneOfCompany);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Authorize, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1805AuthorizeTitle", preparer.Title);
            AddFormFieldData("VA26_1805AuthorizePhone", preparer.Phone);
            AddFormFieldData("VA26_1805AuthorizePrepareDate", preparer.PrepareDate_rep);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string str = preparer.PreparerName;
            if (preparer.PreparerName.TrimWhitespaceAndBOM() != "")
                str += Environment.NewLine;

            if (preparer.CompanyName.TrimWhitespaceAndBOM() != "") 
                str +=preparer.CompanyName + Environment.NewLine;

            if (preparer.StreetAddr.TrimWhitespaceAndBOM() != "")
                str += preparer.StreetAddr + ", ";
            str += Tools.CombineCityStateZip(preparer.City, preparer.State, preparer.Zip);

            AddFormFieldData("VA26_1805Appraiser", str);
            AddFormFieldData("VA26_1805AppraiserPrepareDate", preparer.PrepareDate_rep);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1805ApplicablePointOfContact, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1805ApplicablePointOfContact", Tools.FormatAddress(preparer.PreparerName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip) + Environment.NewLine + preparer.Phone);

        }
    }
    public class CVA_26_1805_062001PDF : AbstractBatchPDF 
    {
        public override string Description 
        {
            get { return "VA Request for Determination of Reasonable Value (VA 26-1805 / 26-1843) (Jun 2001) (OBSOLETE)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/VA/VAReasonableValue.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CVA_26_1805_1ObsoletePDF(),
                                                 new CVA_26_1805_2ObsoletePDF(),
                                                 new CVA_26_1805_3ObsoletePDF(),
                                                 new CVA_26_1805_4ObsoletePDF(),
                                                 new CVA_26_1805_5ObsoletePDF(),
                                                 new CVA_26_1805_6ObsoletePDF(),
                                                 new CVA_26_1805_7ObsoletePDF(),
                                                 new CVA_26_1805_8ObsoletePDF()
                                             };
            }
        }


    }

    public class CVA_26_1805_1PDF : AbstractVA_26_1805PDF
    {
        public override string PdfFile
        {
            get { return "VA-26-1805_1.pdf"; }
        }

        public override string Description
        {
            get { return "Instructions"; }
        }

    }

    public class CVA_26_1805_2PDF : AbstractVA_26_1805PDF
    {
        public override string PdfFile
        {
            get { return "VA-26-1805_2.pdf"; }
        }

        public override string Description
        {
            get { return "Request of Value - VA File (26-1805) (Apr 2014)"; }
        }

    }

    public class CVA_26_1805PDF : AbstractBatchPDF
    {
        public override string Description
        {
            get { return "VA Request for Determination of Reasonable Value (VA 26-1805)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/VA/VAReasonableValue.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] 
                {
                    new CVA_26_1805_1PDF(),
                    new CVA_26_1805_2PDF()
                };
            }
        }
    }
}
