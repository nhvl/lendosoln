namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOffice.Constants;
    using LendersOffice.PdfLayout;
    using System.Linq;
    using LendersOffice.ObjLib.PDFGenerator;

    /// <summary>
    /// Represents a custom PDF as an extension of the standard PDF loading system.
    /// </summary>
    /// <remarks>
    /// Because this extends the standard PDF base class, some properties of the base class need to be overridden
    /// here, e.g. <see cref="PdfFileName"/>.
    /// </remarks>
    public class CCustomPDF : AbstractPDFFile, IPDFPrintItemGenerator
	{
        protected override string PdfFileName
        {
            get { return ConstAppDavid.CustomPdfProtocolPrefix + Arguments["custompdfid"]; }
        }
        public override string PdfFile
        {
            get { return Arguments["custompdfid"]; }
        }

        private PdfFormLayout x_layout = null;

        /// <summary>
        /// Gets the layout of the fields in the custom PDF.
        /// </summary>
        /// <remarks>
        /// The base implementation in <see cref="LendersOffice.PdfGenerator.AbstractPDFFile.PdfFormLayout"/>
        /// loads the standard PDF layout file.  For a custom PDF, this file will not exist.
        /// </remarks>
        public override PdfFormLayout PdfFormLayout
        {
            get
            {
                if (x_layout == null)
                {
                    Guid formId = new Guid(Arguments["custompdfid"]);
                    PdfForm.PdfForm pdfForm = PdfForm.PdfForm.LoadById(formId);

                    x_layout = pdfForm.Layout;
                }
                return x_layout;
            }
        }

        public override IEnumerable<string> DependencyFields
        {
            get
            {
                Guid formId = new Guid(Arguments["custompdfid"]);
                List<string> dependencyFields = CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(CCustomPDF)).ToList();

                foreach (var o in this.PdfFormLayout.FieldList)
                {
					try {
                    string name = PageDataUtilities.GetDependencyFieldIdName(o.Name);
                    if (!dependencyFields.Contains(name))
                    {
                        dependencyFields.Add(name);
                    }
					} catch (CBaseException exc) {
						if (exc.UserMessage == "Invalid Field Id") {
							continue;
						} else {
							throw;
						}
					}
                }
                return dependencyFields;
            }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize 
        {
            get { 
                if (Arguments["pagesize"] == "0")
                    return LendersOffice.PdfGenerator.PDFPageSize.Letter;
                else
                    return LendersOffice.PdfGenerator.PDFPageSize.Legal; 
            }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            bool hasSpecialFields = false;
            foreach (var o in this.PdfFormLayout.FieldList)
            {
                try
                {
                    if (o.Name == "a1003InterviewD")
                    {
                        throw new CBaseException(ErrorMessages.CustomPDF.InvalidField_a1003InterviewD, "Pdf Using Invalid field.");
                    }
                    if (o.Name == "sOutstandingConditionHtml")
                    {
                        // 7/11/2011 dd - Keep the html text as is.
                        AddFormFieldData(o.Name, dataLoan.sOutstandingConditionHtml.Value, false);
                    }
                    else if (o.Name.StartsWith("GetOutstandingConditionsOfCategory[", StringComparison.OrdinalIgnoreCase)
                        && o.Name.EndsWith("].Description"))
                    {
                        // 9/11/2013 dd - OPM 137004
                        // Add GetOutstandingConditionsOfCategory[Category_name].Description to 
                        // render a list of condition by category.
                        string categoryName = o.Name.Substring("GetOutstandingConditionsOfCategory[".Length,
                            o.Name.Length - "GetOutstandingConditionsOfCategory[".Length - "].Description".Length);

                        StringBuilder sb = new StringBuilder();
                        foreach (var s in dataLoan.GetOutstandingConditionsOfCategory(categoryName))
                        {
                            sb.AppendLine(s);
                        }
                        AddFormFieldData(o.Name, sb.ToString());
                    }
                    else if (o.Name == "GetOutstandingConsumerPortalConditions")
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var s in dataLoan.GetOutstandingConsumerPortalConditions())
                        {
                            sb.AppendLine(s);
                        }
                        AddFormFieldData(o.Name, sb.ToString());

                    }
                    else if (o.Type == PdfFieldType.Signature || o.Type == PdfFieldType.Initial || o.Type == PdfFieldType.SignatureDate)
                    {
                        hasSpecialFields = true;
                    }
                    else if (PageDataUtilities.ContainsField(o.Name))
                    {
                        if (LoanFieldSecurityManager.CanReadField(o.NameUseForRendering))
                        {
                            AddFormFieldData(o.Name, PageDataUtilities.GetValue(dataLoan, dataApp, o.Name));
                        }
                        else
                        {
                            AddFormFieldData(o.Name, "Restricted");
                        }
                    }
                    else
                    {
                        hasSpecialFields = true;
                    }
                }
                catch (CBaseException exc)
                {
                    if (exc.UserMessage == "Invalid Field Id")
                    {
                        continue;
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            if (!hasSpecialFields)
            {
                // 5/18/2010 dd - If the layout contains no special fields then we can finish printing code here.
                return;
            }
            //AddFormFieldData("TodayDate", DateTime.Now.ToShortDateString());


            //CAgentFields lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            //AddFormFieldData("Lender_Name", lender.CompanyName);


            switch (dataLoan.sGseSpT.ToString())
            {
                case "":
                case "LeaveBlank":
                    AddFormFieldData("sGseSpT", ""); break;
                case "DetachedCondominium":
                    AddFormFieldData("sGseSpT", "Detached Condominium"); break;
                case "HighRiseCondominium":
                    AddFormFieldData("sGseSpT", "High Rise Condominium"); break;
                case "ManufacturedHomeCondominium":
                    AddFormFieldData("sGseSpT", "Manufactured Home Condominium"); break;
                case "ManufacturedHomeMultiwide":
                    AddFormFieldData("sGseSpT", "Manufactured Home Multiwide"); break;
                case "ManufacturedHousing":
                    AddFormFieldData("sGseSpT", "Manufactured Housing"); break;
                case "ManufacturedHousingSingleWide":
                    AddFormFieldData("sGseSpT", "Manufactured Housing Single Wide"); break;
                default:
                    AddFormFieldData("sGseSpT", dataLoan.sGseSpT.ToString()); break;
            }
            E_sLPurposeT sLPurposeT = dataLoan.sLPurposeT;
            if (sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT == E_sLPurposeT.VaIrrrl)
            {
                sLPurposeT = E_sLPurposeT.Refin;
            }
            AddFormFieldData("sLPurposeT", sLPurposeT);
            AddFormFieldData("sLPurposeT_rep", dataLoan.sLPurposeT_rep);

            #region Keywords for to APB CTC Form

            #region sTotalEscrowFee - Sum of GFE 1004 + 1001 + 1005 + 1006 + 1007
            decimal sTotalEscrowFee = 0.0M;
            try { sTotalEscrowFee += dataLoan.sRealETxRsrv; } catch {}
            try { sTotalEscrowFee += dataLoan.sHazInsRsrv; } catch {}
            try { sTotalEscrowFee += dataLoan.sFloodInsRsrv; } catch {}
            try { sTotalEscrowFee += dataLoan.s1006Rsrv; } catch {}
            try { sTotalEscrowFee += dataLoan.s1007Rsrv; } catch {}
            AddFormFieldData("sTotalEscrowFee", dataLoan.m_convertLos.ToMoneyString(sTotalEscrowFee, FormatDirection.ToRep));

            #endregion

            #region sTotalTc = Sum of 1100 Title Sections.
            decimal sTotalTc = 0.0M;
            try { sTotalTc += dataLoan.sEscrowF; } catch {}
            try { sTotalTc += dataLoan.sDocPrepF; } catch {}
            try { sTotalTc += dataLoan.sNotaryF; } catch {}
            try { sTotalTc += dataLoan.sAttorneyF; } catch {}
            try { sTotalTc += dataLoan.sTitleInsF; } catch {}
            try { sTotalTc += dataLoan.sU1Tc; } catch {}
            try { sTotalTc += dataLoan.sU2Tc; } catch {}
            try { sTotalTc += dataLoan.sU3Tc; } catch {}
            try { sTotalTc += dataLoan.sU4Tc; } catch {}
            AddFormFieldData("sTotalTc", dataLoan.m_convertLos.ToMoneyString(sTotalTc, FormatDirection.ToRep));

            #endregion

            #region sTotalGovRtc = Sum of 1200 Government Sections
            decimal sTotalGovRtc = 0.0M;
            try { sTotalGovRtc += dataLoan.sRecF; } catch {}
            try { sTotalGovRtc += dataLoan.sCountyRtc; } catch {}
            try { sTotalGovRtc += dataLoan.sStateRtc; } catch {}
            try { sTotalGovRtc += dataLoan.sU1GovRtc; } catch {}
            try { sTotalGovRtc += dataLoan.sU2GovRtc; } catch {}
            try { sTotalGovRtc += dataLoan.sU3GovRtc; } catch {}
            AddFormFieldData("sTotalGovRtc", dataLoan.m_convertLos.ToMoneyString(sTotalGovRtc, FormatDirection.ToRep));
            #endregion

            #region sTotalSc = Sum of 1300 Additional Settlement Charges
            decimal sTotalSc = 0.0M;
            try { sTotalSc += dataLoan.sPestInspectF; } catch {}
            try { sTotalSc += dataLoan.sU1Sc; } catch {}
            try { sTotalSc += dataLoan.sU2Sc; } catch {}
            try { sTotalSc += dataLoan.sU3Sc; } catch {}
            try { sTotalSc += dataLoan.sU4Sc; } catch {}
            try { sTotalSc += dataLoan.sU5Sc; } catch {}
            AddFormFieldData("sTotalSc", dataLoan.m_convertLos.ToMoneyString(sTotalSc, FormatDirection.ToRep));
            #endregion
            #region sApprF
            bool isApprPOC = LosConvert.GfeItemProps_Poc(dataLoan.sApprFProps);
            if (isApprPOC) 
            {
                AddFormFieldData("sApprF_POC", dataLoan.sApprF_rep);
            } 
            else 
            {
                AddFormFieldData("sApprF_Closing", dataLoan.sApprF_rep);
            }
            #endregion

            #region sAPBFeeOther - Sum of GFE 804, 805, 808, 809, 812, s800U2F, s800U3F, s800U4F, s800U5F (if not POC), sLDiscnt
            decimal sAPBFeeOther = 0.0M;
            try 
            {
                if (!LosConvert.GfeItemProps_Poc(dataLoan.sCrFProps)) 
                {
                    sAPBFeeOther += dataLoan.sCrF;
                }
            } 
            catch {}
            try 
            {
                if (!LosConvert.GfeItemProps_Poc(dataLoan.sInspectFProps)) 
                {
                    sAPBFeeOther += dataLoan.sInspectF;
                }
            } 
            catch {}
            try 
            {
                if (!LosConvert.GfeItemProps_Poc(dataLoan.sMBrokFProps)) 
                {
                    sAPBFeeOther += dataLoan.sMBrokF;
                }
            } 
            catch {}
            try 
            {
                if (!LosConvert.GfeItemProps_Poc(dataLoan.sTxServFProps)) 
                {
                    sAPBFeeOther += dataLoan.sTxServF;
                }
            } 
            catch {}
            try 
            {
                if (!LosConvert.GfeItemProps_Poc(dataLoan.sWireFProps)) 
                {
                    sAPBFeeOther += dataLoan.sWireF;
                }
            } 
            catch {}

            try 
            {
                if (!LosConvert.GfeItemProps_Poc(dataLoan.s800U2FProps)) 
                {
                    sAPBFeeOther += dataLoan.s800U2F;
                }
            } 
            catch {}
            try 
            {
                if (!LosConvert.GfeItemProps_Poc(dataLoan.s800U3FProps)) 
                {
                    sAPBFeeOther += dataLoan.s800U3F;
                }
            } 
            catch {}
            try 
            {
                if (!LosConvert.GfeItemProps_Poc(dataLoan.s800U4FProps)) 
                {
                    sAPBFeeOther += dataLoan.s800U4F;
                }
            } 
            catch {}
            try 
            {
                if (!LosConvert.GfeItemProps_Poc(dataLoan.s800U5FProps)) 
                {
                    sAPBFeeOther += dataLoan.s800U5F;
                }
            } 
            catch {}
            try 
            {
                if (!LosConvert.GfeItemProps_Poc(dataLoan.sLDiscntProps)) 
                {
                    sAPBFeeOther += dataLoan.sLDiscnt;
                }
            } 
            catch {}
            AddFormFieldData("sAPBFeeOther", dataLoan.m_convertLos.ToMoneyString(sAPBFeeOther, FormatDirection.ToRep));
            #endregion

            #region sAPBFee - Sum of GFE 801 + 810 + 811 + First custom 800 + sApprF (only if POC)
            decimal sAPBFee = 0.0M;
            if (!isApprPOC) { try { sAPBFee += dataLoan.sApprF; } catch {} }
            try { sAPBFee += dataLoan.sLOrigF; } catch {}
            try { sAPBFee += dataLoan.sProcF; } catch {}
            try { sAPBFee += dataLoan.sUwF; } catch {}
            try { sAPBFee += dataLoan.s800U1F; } catch {}
            sAPBFee += sAPBFeeOther;
            AddFormFieldData("sAPBFee", dataLoan.m_convertLos.ToMoneyString(sAPBFee, FormatDirection.ToRep));

            #endregion

            #region sAPBPercentOfLoan
            decimal sAPBPercentOfLoan = 0.0M;
            try { sAPBPercentOfLoan = sAPBFee / dataLoan.sLAmtCalc * 100.0M; } catch {}
            if (sAPBPercentOfLoan > 0) 
            {
                AddFormFieldData("sAPBPercentOfLoan", dataLoan.m_convertLos.ToRateString(sAPBPercentOfLoan) + "%");
            }
            if (sAPBPercentOfLoan > 0 && sAPBPercentOfLoan <= 5) 
            {
                AddFormFieldData("sAPBPercentOfLoanStatus", "OK");
            } 
            else if (sAPBPercentOfLoan > 5 && sAPBPercentOfLoan <= 6.5M) 
            {
                AddFormFieldData("sAPBPercentOfLoanStatus", "SUB-PRIME");
            } 
            else if (sAPBPercentOfLoan > 6.5M) 
            {
                AddFormFieldData("sAPBPercentOfLoanStatus", "ERROR");

            }
            #endregion


            #region sAPBTotalLoanOfficerFee = sProfitRebate + sLOrigF
            decimal sBrokComp1Pc = 0.0M;
            try { sBrokComp1Pc = dataLoan.sBrokComp1Pc; } catch {}

            decimal sAPBTotalLoanOfficerFee = 0.0M;

            try { sAPBTotalLoanOfficerFee = dataLoan.sLOrigF; } catch {}
            try { sAPBTotalLoanOfficerFee += dataLoan.sProfitRebate; } catch {}

            AddFormFieldData("sAPBTotalLoanOfficerFee", dataLoan.m_convertLos.ToMoneyString(sAPBTotalLoanOfficerFee, FormatDirection.ToRep));
            #endregion

            #region Calculate Refi Paid Off
            decimal sAPBPdOffAmt = 0.0M;
            if (dataLoan.sLPurposeT == E_sLPurposeT.Refin || dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl) 
            {
                ILiaCollection liaColl = dataApp.aLiaCollection;
                int count = liaColl.CountRegular;
                int currentIndex = 0;
                decimal liaOther = 0.0M;
                bool hasOther = false;
                for (int i = 0; i < count; i++) 
                {
                    ILiabilityRegular lia = liaColl.GetRegularRecordAt(i);
                    if (lia.WillBePdOff) 
                    {
                        if (currentIndex < 7) 
                        {
                            AddFormFieldData("Lia" + currentIndex + "_Desc", lia.Desc);
                            AddFormFieldData("Lia" + currentIndex + "_Bal", lia.Bal_rep);
                            try { sAPBPdOffAmt += lia.Bal; } 
                            catch {}
                            currentIndex++;
                        } 
                        else 
                        {
                            try 
                            {
                                sAPBPdOffAmt += lia.Bal;
                                liaOther += lia.Bal;
                                hasOther = true;
                            } 
                            catch {}
                        }
                    }
                }
                if (hasOther) 
                {
                    AddFormFieldData("Lia7_Desc", "Other");
                    AddFormFieldData("Lia7_Bal", dataLoan.m_convertLos.ToMoneyString(liaOther, FormatDirection.ToRep));
                }
            } 
            else 
            {
                try { sAPBPdOffAmt = dataLoan.sPurchPrice; } catch {}
            }
            AddFormFieldData("sAPBPdOffAmt", dataLoan.m_convertLos.ToMoneyString(sAPBPdOffAmt, FormatDirection.ToRep));
            #endregion

            #endregion




            #region Add Agent Information
            //AddAgentInformation(dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            //AddAgentInformation(dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            //AddAgentInformation(dataLoan.GetAgentOfRole(E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            //AddAgentInformation(dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            //// OPM 34472
            //AddAgentInformation(dataLoan.GetAgentOfRole(E_AgentRoleT.FloodProvider, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            //AddAgentInformation(dataLoan.GetAgentOfRole(E_AgentRoleT.CreditReport, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            //AddAgentInformation(dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            
            #endregion

            #region Request For Appraisal Info (OPM 30172)
            // Note - the code for this function was copied from RequestForAppraisalPDF.cs, but this is inefficient
            // We should change that code so it can be used by muliple classes.
            ApplyAppraisalData(dataLoan, dataApp);
            #endregion

            // 06/01/2006 dd - Reason I need to retrieve Loan Officer info here is because
            // I need to get Address,City,State,Zip,ActualBrokerName for PML User Info.
            EmployeeDB loanOfficer = null;
            if (dataLoan.sEmployeeLoanRepId != Guid.Empty)
                loanOfficer = RetrieveEmployeeByID(dataLoan.sEmployeeLoanRepId);

            if (null != loanOfficer) 
            {
                AddFormFieldData("sEmployeeLoanRepFax", loanOfficer.Fax);
                AddFormFieldData("sEmployeeLoanRepActualBrokerName", dataLoan.sEmployeeLoanRepPmlUserBrokerNm);
                AddFormFieldData("sEmployeeLoanRepAddress", loanOfficer.Address.StreetAddress);
                AddFormFieldData("sEmployeeLoanRepCity", loanOfficer.Address.City);
                AddFormFieldData("sEmployeeLoanRepState", loanOfficer.Address.State);
                AddFormFieldData("sEmployeeLoanRepZipcode", loanOfficer.Address.Zipcode);
                AddFormFieldData("sEmployeeLoanRepAddress_MultiLine", Tools.FormatAddress(loanOfficer.Address.StreetAddress, loanOfficer.Address.City, loanOfficer.Address.State, loanOfficer.Address.Zipcode));
				AddFormFieldData("sEmployeeLoanRepAddress_SingleLine", Tools.FormatSingleLineAddress(loanOfficer.Address.StreetAddress, loanOfficer.Address.City, loanOfficer.Address.State, loanOfficer.Address.Zipcode));
            }




        }

        private void ApplyAppraisalData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields preparerField = dataLoan.GetPreparerOfForm(E_PreparerFormT.RequestOfAppraisal, E_ReturnOptionIfNotExist.ReturnEmptyObject);


            string borrowerName = dataApp.aBNm;
            string coborrowerName = dataApp.aCNm;

            string borrowerInformation = borrowerName;
            if (coborrowerName.TrimWhitespaceAndBOM() != "")
            {
                borrowerInformation += " & " + coborrowerName;
            }
            borrowerInformation = Tools.FormatAddress(borrowerInformation, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip);


            if (dataLoan.sIsPrintBorrPhoneOnRequestForAppraisal)
            {
                borrowerInformation += Environment.NewLine;
                if (dataApp.aBHPhone.TrimWhitespaceAndBOM() != "")
                    borrowerInformation += "Home: " + dataApp.aBHPhone + " ";
                if (dataApp.aBCellPhone.TrimWhitespaceAndBOM() != "")
                    borrowerInformation += "Cell: " + dataApp.aBCellPhone.TrimWhitespaceAndBOM() + " ";
                if (dataApp.aBEmail.TrimWhitespaceAndBOM() != "")
                    borrowerInformation += "Email: " + dataApp.aBEmail.TrimWhitespaceAndBOM();
            }

            AddFormFieldData("BorrowerInformation", borrowerInformation);

            string address = Tools.FormatAddress(null, dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip, "County: " + dataLoan.sSpCounty);
            AddFormFieldData("LenderAddress", FormatLender(preparerField));
            AddFormFieldData("PropertyAddress", address);
            AddFormFieldData("Title", preparerField.Title);
            AddFormFieldData("sApprRprtPrepD", preparerField.PrepareDate_rep);

            E_sLPurposeT sLPurposeT = dataLoan.sLPurposeT;
            if (sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT == E_sLPurposeT.VaIrrrl)
                sLPurposeT = E_sLPurposeT.Refin;


            if (dataLoan.sGseRequestAppraisalT != E_GseRequestAppraisal.Unset)
            {
                if (dataLoan.sGseRequestAppraisalT == E_GseRequestAppraisal.Other)
                {
                    AddFormFieldData("FannieMae", dataLoan.sGseRequestAppraisalOther);
                }
                else
                {
                    string sGseReqAStr = dataLoan.sGseRequestAppraisalT.ToString();
                    sGseReqAStr = System.Text.RegularExpressions.Regex.Replace(sGseReqAStr, "^Fannie([0-9]+[A-Z]?)$", "Fannie Mae $1");
                    sGseReqAStr = System.Text.RegularExpressions.Regex.Replace(sGseReqAStr, "^Freddie([0-9]+[A-Z]?)$", "Freddie Mac $1");
                    sGseReqAStr = System.Text.RegularExpressions.Regex.Replace(sGseReqAStr, "^VA([0-9]{2})_([0-9]{4})$", "VA $1-$2");
                    AddFormFieldData("FannieMae", sGseReqAStr);
                }
            }

            int count = dataLoan.GetAgentRecordCount();
            // If there are multiple record of appraiser, escrow etc, then use the first
            // record.
            bool isAppraiser = false;
            bool isEscrow = false;
            bool isTitle = false;
            bool isListingAgent = false;
            bool isSellingAgent = false;
            for (int i = 0; i < count; i++)
            {
                CAgentFields field = dataLoan.GetAgentFields(i);
                switch (field.AgentRoleT)
                {
                    case E_AgentRoleT.Appraiser:
                        if (!isAppraiser)
                        {
                            AddFormFieldData("AppraiserAddress", FormatAppraiserDisplay(field));
                            isAppraiser = true;
                        }
                        break;
                    case E_AgentRoleT.Escrow:
                        if (!isEscrow)
                        {
                            AddFormFieldData("EscrowAddress", FormatAgentDisplay(field));
                            isEscrow = true;
                        }
                        break;
                    case E_AgentRoleT.Title:
                        if (!isTitle)
                        {
                            AddFormFieldData("TitleAddress", FormatAgentDisplay(field));
                            isTitle = true;
                        }
                        break;
                    case E_AgentRoleT.ListingAgent:
                        if (!isListingAgent)
                        {
                            AddFormFieldData("ListingAgentAddress", FormatAgentDisplay(field));
                            isListingAgent = true;
                        }
                        break;
                    case E_AgentRoleT.SellingAgent:
                        if (!isSellingAgent)
                        {
                            AddFormFieldData("SellingAgentAddress", FormatAgentDisplay(field));
                            isSellingAgent = true;
                        }
                        break;
                } // switch (field.AgentRoleT) 
            } // for (int i = 0; i < count; i++) 

        }

        private string FormatAppraiserDisplay(CAgentFields field)
        {
            StringBuilder sb = new StringBuilder();

            if (field.AgentName != "")
                sb.Append(field.AgentName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            if (field.CompanyName != "")
                sb.Append(field.CompanyName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            if (field.StreetAddr != "")
                sb.Append(field.StreetAddr.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            string str = Tools.CombineCityStateZip(field.City, field.State, field.Zip);
            if (str != "")
                sb.Append(str).Append(Environment.NewLine);

            if (field.Phone != "")
                sb.AppendFormat("(P): {0} ", field.Phone.TrimWhitespaceAndBOM());

            if (field.FaxNum != "")
                sb.AppendFormat("(F): {0} ", field.FaxNum.TrimWhitespaceAndBOM());

            if (field.CellPhone != "")
                sb.AppendFormat("(C): {0}", field.CellPhone.TrimWhitespaceAndBOM());

            if (field.EmailAddr != "")
                sb.Append(Environment.NewLine).AppendFormat("Email: {0}", field.EmailAddr.TrimWhitespaceAndBOM());

            return sb.ToString();

        }
        
        
        private string FormatAgentDisplay(CAgentFields field)
        {
            StringBuilder sb = new StringBuilder();
            if (field.CaseNum != "")
            {
                sb.AppendFormat("Case: {0}", field.CaseNum.TrimWhitespaceAndBOM());
                sb.Append(Environment.NewLine);
            }

            if (field.CompanyName != "")
                sb.Append(field.CompanyName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);
            if (field.AgentName != "")
                sb.Append(field.AgentName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            if (field.Phone != "")
                sb.AppendFormat("(P): {0} ", field.Phone.TrimWhitespaceAndBOM());

            if (field.FaxNum != "")
                sb.AppendFormat("(F): {0} ", field.FaxNum.TrimWhitespaceAndBOM());

            if (field.CellPhone != "")
                sb.AppendFormat("(C): {0}", field.CellPhone.TrimWhitespaceAndBOM());

            if (field.EmailAddr != "")
                sb.Append(Environment.NewLine).AppendFormat("Email: {0}", field.EmailAddr.TrimWhitespaceAndBOM());

            return sb.ToString();

        }
        private string FormatLender(IPreparerFields f)
        {
            StringBuilder sb = new StringBuilder();
            if (f.PreparerName != "")
                sb.Append(f.PreparerName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            if (f.CompanyName != "")
                sb.Append(f.CompanyName.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            if (f.StreetAddr != "")
                sb.Append(f.StreetAddr.TrimWhitespaceAndBOM()).Append(Environment.NewLine);

            string str = Tools.CombineCityStateZip(f.City, f.State, f.Zip);
            if (str != "")
                sb.Append(str).Append(Environment.NewLine);

            if (f.Phone != "")
                sb.AppendFormat("(P): {0} ", f.Phone.TrimWhitespaceAndBOM());

            if (f.FaxNum != "")
                sb.AppendFormat("(F): {0} ", f.FaxNum.TrimWhitespaceAndBOM());

            if (f.EmailAddr != "")
                sb.Append(Environment.NewLine).AppendFormat("Email: {0}", f.EmailAddr);


            return sb.ToString();

        }
        /// <summary>
        /// paperSize - 0 - Letter size, 1 - Legal size
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="id"></param>
        /// <param name="description"></param>
        /// <param name="paperSize"></param>
        public void RenderPrintLink(StringBuilder sb, string id, string description, int pageSize) 
        {
            string idNoDash = id.Replace("-", "");

            string disabledCheckbox = string.Empty;
            string accessDeniedMsg = string.Empty;

            if (!HasPrintPermission)
            {
                disabledCheckbox = "disabled='disabled'";
                accessDeniedMsg = "<span style='font-weight:bold;color:red'>" + ErrorMessages.PdfPrintAccessDenied + "</span>";

            }

            string checkboxHtml = string.Format("<input id='{2}' name='_{0}' type='checkbox' pageSize='{1}' custompdfform='t' custompdfid='{2}' {3}>"
                , idNoDash // 0
                , pageSize // 1
                , id // 2
                , disabledCheckbox // 3
                );

            string previewLink = string.Empty;
            if (HasPrintPermission)
            {
                previewLink = string.Format(@"(<a href=""#"" onclick=""downloadCustomPdf('{0}', {1});"">preview</a>)"
                    , id // 0
                    , pageSize // 1
                    );
            }
            sb.Append(string.Format(@"<tr><td>{0}</td><td>{1} {2} {3}</td></tr>"
                , checkboxHtml // 0
                , Utilities.SafeHtmlString(description) //  1
                , previewLink // 2
                , accessDeniedMsg // 3
                ));

        }

        //private void AddAgentInformation(CAgentFields field) 
        //{
        //    if (!field.IsValid) return;
            
        //    string prefix = "Agent_" + field.AgentRoleT + "_";
            
        //    AddFormFieldData(prefix + "CompanyName", field.CompanyName);
        //    AddFormFieldData(prefix + "AgentName", field.AgentName);
        //    AddFormFieldData(prefix + "PhoneOfCompany", field.PhoneOfCompany);
        //    AddFormFieldData(prefix + "FaxOfCompany", field.FaxOfCompany);
        //    AddFormFieldData(prefix + "FaxNum", field.FaxNum);
        //    AddFormFieldData(prefix + "Phone", field.Phone);
        //    AddFormFieldData(prefix + "EmailAddr", field.EmailAddr);
            
        //    StringBuilder sb_AddressSingleLine = new StringBuilder();
        //    StringBuilder sb_AddressMultiLine = new StringBuilder();

        //    if (field.StreetAddr != "")
        //    {
        //        sb_AddressSingleLine.Append(field.StreetAddr.TrimWhitespaceAndBOM());
        //        sb_AddressMultiLine.Append(field.StreetAddr.TrimWhitespaceAndBOM()).Append(Environment.NewLine);
        //    }

        //    string str = Tools.CombineCityStateZip(field.City, field.State, field.Zip);
        //    if (str != "")
        //    {
        //        sb_AddressSingleLine.Append(str);
        //        sb_AddressMultiLine.Append(str);
        //    }

        //    AddFormFieldData(prefix + "CompanyAddress_SingleLine", sb_AddressSingleLine.ToString());
        //    AddFormFieldData(prefix + "CompanyAddress_MultiLine", sb_AddressMultiLine.ToString());
        //}
        private EmployeeDB RetrieveEmployeeByID(Guid id) 
        {
         
            string key = "CustomPDF_EmployeeDB_" + id;
            EmployeeDB employee = CurrentContextCache.Get(key) as EmployeeDB;
            if (null == employee) 
            {
                employee = new EmployeeDB(id, BrokerUserPrincipal.CurrentPrincipal.BrokerId );
                employee.Retrieve();
                CurrentContextCache.Set(key, employee);
            }

            return employee;
        }
	}
}
