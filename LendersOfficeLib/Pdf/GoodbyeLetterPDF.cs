﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    public class CGoodbyeLetterPDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "NoticeOfLoanTransfer.pdf"; }
        }
        public override string Description
        {
            get
            {
                return "Goodbye Letter";
            }
        }
        public override string EditLink
        {
            get
            {
                return "/newlos/Forms/GoodbyeLetter.aspx";
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            // Info that isn't in form.
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm_aCNm", dataApp.aBNm_aCNm);
            AddFormFieldData("sSpAddr_singleline", dataLoan.sSpAddr_SingleLine);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            
            // General
            AddFormFieldData("sGLServTransEffD", dataLoan.sGLServTransEffD_rep);
            IPreparerFields f = dataLoan.GetPreparerOfForm(E_PreparerFormT.GoodbyeLetter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("sGLCompletedBy", f.PreparerName);
            AddFormFieldData("CompanyName", f.CompanyName);

            // Previous Servicer
            AddFormFieldData("sGLPrevServNm", dataLoan.sGLPrevServNm);
            AddFormFieldData("sGLPrevServAddr_MultiLine", 
                Tools.FormatAddress(dataLoan.sGLPrevServAddr, dataLoan.sGLPrevServCity,
                                    dataLoan.sGLPrevServState, dataLoan.sGLPrevServZip));
            AddFormFieldData("sGLPrevServTFPhone", dataLoan.sGLPrevServTFPhone);
            AddFormFieldData("sGLPrevServContactNm", dataLoan.sGLPrevServContactNm);
            AddFormFieldData("sGLPrevServContactNum", dataLoan.sGLPrevServContactNum);
            AddFormFieldData("sGLPrevServBusHrAM", dataLoan.sGLPrevServBusHrAM);
            AddFormFieldData("sGLPrevServBusHrPM", dataLoan.sGLPrevServBusHrPM);

            // New Servicer
            AddFormFieldData("sGLNewServNm", dataLoan.sGLNewServNm);
            AddFormFieldData("sGLNewServAddr_MultiLine",
                Tools.FormatAddress(dataLoan.sGLNewServAddr, dataLoan.sGLNewServCity,
                                    dataLoan.sGLNewServState, dataLoan.sGLNewServZip));
            AddFormFieldData("sGLNewServTFPhone", dataLoan.sGLNewServTFPhone);
            AddFormFieldData("sGLNewServContactNm", dataLoan.sGLNewServContactNm);
            AddFormFieldData("sGLNewServContactNum", dataLoan.sGLNewServContactNum);
            AddFormFieldData("sGLNewServBusHrAM", dataLoan.sGLNewServBusHrAM);
            AddFormFieldData("sGLNewServBusHrPM", dataLoan.sGLNewServBusHrPM);
            AddFormFieldData("sGLNewServAccPmtD", dataLoan.sGLNewServAccPmtD_rep);
            AddFormFieldData("sGLNewServPmtAddr_MultiLine",
                Tools.FormatAddress(dataLoan.sGLNewServPmtAddr, dataLoan.sGLNewServPmtCity,
                                    dataLoan.sGLNewServPmtState, dataLoan.sGLNewServPmtZip));
            AddFormFieldData("sGLNewServFirstPmtSchedD", dataLoan.sGLNewServFirstPmtSchedD_rep);
        }
    }
}
