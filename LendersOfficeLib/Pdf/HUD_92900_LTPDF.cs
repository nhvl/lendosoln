using System;
using System.Collections.Specialized;
using System.Data;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{

    public class CHUD_92900_LT_1PDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-LT_1.pdf"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/FHA/FHAHud92900Lt.aspx"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
 
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {


            AddFormFieldData("sFHA92900LtFinMethT", dataLoan.sFHA92900LtFinMethT);
                AddFormFieldData("aFHANegCfRentalI", dataApp.aFHANegCfRentalI_rep);
            AddFormFieldData("aFHABLdpGsaTri", dataApp.aFHABLdpGsaTri);
            AddFormFieldData("aFHACLdpGsaTri", dataApp.aFHACLdpGsaTri);
            AddFormFieldData("sFHAPurposeIsPurchase", dataLoan.sFHAPurposeIsPurchase); 
            AddFormFieldData("sFHAPurposeIsNoCashoutRefi", dataLoan.sFHAPurposeIsNoCashoutRefi); 
            AddFormFieldData("sFHAPurposeIsCashoutRefi", dataLoan.sFHAPurposeIsCashoutRefi); 
            AddFormFieldData("sFHAPurposeIsStreamlineRefi", dataLoan.sFHAPurposeIsStreamlineRefi); 
            AddFormFieldData("sFHAPurposeIsStreamlineRefiWithAppr", dataLoan.sFHAPurposeIsStreamlineRefiWithAppr); 
            AddFormFieldData("sFHAPurposeIsStreamlineRefiWithoutAppr", dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr); 
            AddFormFieldData("sFHAPurposeIsConstToPerm", dataLoan.sFHAPurposeIsConstToPerm); 
            AddFormFieldData("sFHAPurposeIsEnergyEfficientMortgage", dataLoan.sFHAPurposeIsEnergyEfficientMortgage); 
            AddFormFieldData("sFHAPurposeIsBuildOnOwnLand", dataLoan.sFHAPurposeIsBuildOnOwnLand); 
            AddFormFieldData("sFHAPurposeIsHudReo", dataLoan.sFHAPurposeIsHudReo); 
            AddFormFieldData("sFHAPurposeIs203k", dataLoan.sFHAPurposeIs203k); 
            AddFormFieldData("sFHAPurposeIsOther", dataLoan.sFHAPurposeIsOther); 

            AddFormFieldData("sFHASecondaryFinancingIsGov", dataLoan.sFHASecondaryFinancingIsGov); 
            AddFormFieldData("sFHASecondaryFinancingIsNP", dataLoan.sFHASecondaryFinancingIsNP); 
            AddFormFieldData("sFHASecondaryFinancingIsFamily", dataLoan.sFHASecondaryFinancingIsFamily); 
            AddFormFieldData("sFHASecondaryFinancingIsOther", dataLoan.sFHASecondaryFinancingIsOther); 
            AddFormFieldData("sFHAIsSellerFundDAP", dataLoan.sFHAIsSellerFundDAP ? "Y" : "N"); 

            AddFormFieldData("sFHAGift1IsGov", dataLoan.sFHAGift1IsGov); 
            AddFormFieldData("sFHAGift1IsNP", dataLoan.sFHAGift1IsNP); 
            AddFormFieldData("sFHAGift1IsFamily", dataLoan.sFHAGift1IsFamily); 
            AddFormFieldData("sFHAGift1IsOther", dataLoan.sFHAGift1IsOther); 
            AddFormFieldData("sFHAGift2IsGov", dataLoan.sFHAGift2IsGov); 
            AddFormFieldData("sFHAGift2IsNP", dataLoan.sFHAGift2IsNP); 
            AddFormFieldData("sFHAGift2IsFamily", dataLoan.sFHAGift2IsFamily); 
            AddFormFieldData("sFHAGift2IsOther", dataLoan.sFHAGift2IsOther); 

            AddFormFieldData("sFHARiskClassAA", dataLoan.sFHARiskClassAA); 
            AddFormFieldData("sFHARiskClassRefer", dataLoan.sFHARiskClassRefer);




            AddFormFieldData("sFHASecondaryFinancingSource", dataLoan.sFHASecondaryFinancingSource);
            AddFormFieldData("sFHASecondaryFinancingOtherDesc", dataLoan.sFHASecondaryFinancingOtherDesc); 
            AddFormFieldData("sFHASecondaryFinancingAmt", dataLoan.sFHASecondaryFinancingAmt_rep); 


            AddFormFieldData("sFHAGift1Source", dataLoan.sFHAGift1Source);

            AddFormFieldData("sFHAGift1OtherDesc", dataLoan.sFHAGift1OtherDesc);
            AddFormFieldData("sFHAGift1gAmt", dataLoan.sFHAGift1gAmt_rep); 

            AddFormFieldData("sFHAGift2Source", dataLoan.sFHAGift2Source);
            AddFormFieldData("sFHAGift2OtherDesc", dataLoan.sFHAGift2OtherDesc);
            AddFormFieldData("sFHAGift2gAmt", dataLoan.sFHAGift2gAmt_rep); 

            AddFormFieldData("sFHAArmIndex", dataLoan.sFHAArmIndex);

            AddFormFieldData("sFHAScoreByTotalTri", dataLoan.sFHAScoreByTotalTri);  

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
            AddFormFieldData("aFHABBaseI", dataApp.aFHABBaseI_rep);
            AddFormFieldData("aFHACBaseI", dataApp.aFHACBaseI_rep);
            AddFormFieldData("aFHABCaivrsNum", dataApp.aFHABCaivrsNum);
            AddFormFieldData("aFHABOI", dataApp.aFHABOI_rep);
            AddFormFieldData("aFHACCaivrsNum", dataApp.aFHACCaivrsNum);
            AddFormFieldData("aFHACOI", dataApp.aFHACOI_rep);
            AddFormFieldData("aFHAChildSupportPmt", dataApp.aFHAChildSupportPmt_rep);
            AddFormFieldData("aFHADebtInstallBal", dataApp.aFHADebtInstallBal_rep);
            AddFormFieldData("aFHADebtInstallPmt", dataApp.aFHADebtInstallPmt_rep);
            AddFormFieldData("aFHAPmtFixedTot", dataApp.aFHAPmtFixedTot_rep);
            AddFormFieldData("aFHAFixedPmtToIRatio", dataApp.aFHAFixedPmtToIRatio_rep);
            AddFormFieldData("aFHAGrossMonI", dataApp.aFHAGrossMonI_rep);
            AddFormFieldData("aFHAMPmtToIRatio", dataApp.aFHAMPmtToIRatio_rep);
            AddFormFieldData("aFHAOtherDebtBal", dataApp.aFHAOtherDebtBal_rep);
            AddFormFieldData("aFHAOtherDebtPmt", dataApp.aFHAOtherDebtPmt_rep);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sCltvR", dataLoan.sHcltvR_rep);
            AddFormFieldData("sEstateHeldT", dataLoan.sEstateHeldT);
            AddFormFieldData("sFHAConstructionT", dataLoan.sFHAConstructionT);
            AddFormFieldData("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            AddFormFieldData("sFHAMonthlyPmt", dataLoan.sFHAMonthlyPmt_rep);
            AddFormFieldData("sFHAPro1stMPmt", dataLoan.sFHAPro1stMPmt_rep);
            AddFormFieldData("sFHAPro2ndFinPmt", dataLoan.sFHAPro2ndFinPmt_rep);
            AddFormFieldData("sFHAProGroundRent", dataLoan.sFHAProGroundRent_rep);
            AddFormFieldData("sFHAProHazIns", dataLoan.sFHAProHazIns_rep);
            AddFormFieldData("sFHAProHoAssocDues", dataLoan.sFHAProHoAssocDues_rep);
            AddFormFieldData("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            AddFormFieldData("sFHAProRealETx", dataLoan.sFHAProRealETx_rep);
            AddFormFieldData("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sFntcSrc", dataLoan.sFntcSrc);
            AddFormFieldData("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            AddFormFieldData("sLtvR", dataLoan.sLtvR_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            AddFormFieldData("sQualIR", dataLoan.sQualIR_rep);
            AddFormFieldData("sRsrvMonNumDesc", dataLoan.sRsrvMonNumDesc);
            AddFormFieldData("sSpAddrSingleLine", dataLoan.sSpAddr_SingleLine);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sTransmFntc", dataLoan.sTransmFntc_rep);
            if (dataLoan.sTransmUwerCommentsOnSeparatePage) 
            {
                AddFormFieldData("sTransmUwerComments", "SEE ATTACHMENT PAGE");
            } 
            else 
            {
                AddFormFieldData("sTransmUwerComments", dataLoan.sTransmUwerComments);
            }

            AddFormFieldData("sVerifAssetAmt", dataLoan.sVerifAssetAmt_rep);
            AddFormFieldData("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);
            AddFormFieldData("sGseSpHud92900LtT", dataLoan.sGseSpHud92900LtT);
            AddFormFieldData("sRAdj1stCapMon", dataLoan.sRAdj1stCapMon_rep);
            AddFormFieldData("sFHASellerContributionPc", dataLoan.sFHASellerContributionPc_rep);
            AddFormFieldData("sFHACcTot", dataLoan.sFHACcTot_rep);
            AddFormFieldData("sChumsIdReviewerAppraisal", dataLoan.sChumsIdReviewerAppraisal);
            AddFormFieldData("aFHANetRentalI", dataApp.aFHANetRentalI_rep);

            AddFormFieldData("aFHATotBaseI", dataApp.aFHATotBaseI_rep);
            AddFormFieldData("aFHATotOI", dataApp.aFHATotOI_rep);
            AddFormFieldData("aFHABTotI", dataApp.aFHABTotI_rep);
            AddFormFieldData("aFHACTotI", dataApp.aFHACTotI_rep);
            
            AddFormFieldData("FHA92900LtUnderwriterLicenseNumOfAgent", dataLoan.sFHA92900LtUnderwriterChumsId);
        }
    }

    public class CHUD_92900_LTContPDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-LTCont.pdf"; }
        }


        public override string Description 
        {
            get { return "Page 1 Continuation Page"; }
        }
        public override bool IsVisible 
        {
            get { return DataLoan.sTransmUwerCommentsOnSeparatePage; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFHAHousingActSection", dataLoan.sFHAHousingActSection);

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sTransmUwerComments", dataLoan.sTransmUwerComments);

        }
    }


    public class CHUD_92900_LT_Combined_1PDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-LT_1.pdf"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/FHA/FHAHud92900LtCombined.aspx"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
 
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            AddFormFieldData("sFHA92900LtFinMethT", dataLoan.sFHA92900LtFinMethT);
            AddFormFieldData("sFHAPurposeIsPurchase", dataLoan.sFHAPurposeIsPurchase); 
            AddFormFieldData("sFHAPurposeIsNoCashoutRefi", dataLoan.sFHAPurposeIsNoCashoutRefi); 
            AddFormFieldData("sFHAPurposeIsCashoutRefi", dataLoan.sFHAPurposeIsCashoutRefi); 
            AddFormFieldData("sFHAPurposeIsStreamlineRefi", dataLoan.sFHAPurposeIsStreamlineRefi); 
            AddFormFieldData("sFHAPurposeIsStreamlineRefiWithAppr", dataLoan.sFHAPurposeIsStreamlineRefiWithAppr); 
            AddFormFieldData("sFHAPurposeIsStreamlineRefiWithoutAppr", dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr); 
            AddFormFieldData("sFHAPurposeIsConstToPerm", dataLoan.sFHAPurposeIsConstToPerm); 
            AddFormFieldData("sFHAPurposeIsEnergyEfficientMortgage", dataLoan.sFHAPurposeIsEnergyEfficientMortgage); 
            AddFormFieldData("sFHAPurposeIsBuildOnOwnLand", dataLoan.sFHAPurposeIsBuildOnOwnLand); 
            AddFormFieldData("sFHAPurposeIsHudReo", dataLoan.sFHAPurposeIsHudReo); 
            AddFormFieldData("sFHAPurposeIs203k", dataLoan.sFHAPurposeIs203k); 
            AddFormFieldData("sFHAPurposeIsOther", dataLoan.sFHAPurposeIsOther); 

            AddFormFieldData("sFHASecondaryFinancingIsGov", dataLoan.sFHASecondaryFinancingIsGov); 
            AddFormFieldData("sFHASecondaryFinancingIsNP", dataLoan.sFHASecondaryFinancingIsNP); 
            AddFormFieldData("sFHASecondaryFinancingIsFamily", dataLoan.sFHASecondaryFinancingIsFamily); 
            AddFormFieldData("sFHASecondaryFinancingIsOther", dataLoan.sFHASecondaryFinancingIsOther); 

            AddFormFieldData("sFHAIsSellerFundDAP", dataLoan.sFHAIsSellerFundDAP ? "Y" : "N"); 
            AddFormFieldData("sFHAGift1IsGov", dataLoan.sFHAGift1IsGov); 
            AddFormFieldData("sFHAGift1IsNP", dataLoan.sFHAGift1IsNP); 
            AddFormFieldData("sFHAGift1IsFamily", dataLoan.sFHAGift1IsFamily); 
            AddFormFieldData("sFHAGift1IsOther", dataLoan.sFHAGift1IsOther); 
            AddFormFieldData("sFHAGift2IsGov", dataLoan.sFHAGift2IsGov); 
            AddFormFieldData("sFHAGift2IsNP", dataLoan.sFHAGift2IsNP); 
            AddFormFieldData("sFHAGift2IsFamily", dataLoan.sFHAGift2IsFamily); 
            AddFormFieldData("sFHAGift2IsOther", dataLoan.sFHAGift2IsOther); 

            AddFormFieldData("sFHARiskClassAA", dataLoan.sFHARiskClassAA); 
            AddFormFieldData("sFHARiskClassRefer", dataLoan.sFHARiskClassRefer);




            AddFormFieldData("sFHASecondaryFinancingSource", dataLoan.sFHASecondaryFinancingSource);
            AddFormFieldData("sFHASecondaryFinancingOtherDesc", dataLoan.sFHASecondaryFinancingOtherDesc); 
            AddFormFieldData("sFHASecondaryFinancingAmt", dataLoan.sFHASecondaryFinancingAmt_rep); 


            AddFormFieldData("sFHAGift1Source", dataLoan.sFHAGift1Source);

            AddFormFieldData("sFHAGift1OtherDesc", dataLoan.sFHAGift1OtherDesc);
            AddFormFieldData("sFHAGift1gAmt", dataLoan.sFHAGift1gAmt_rep); 

            AddFormFieldData("sFHAGift2Source", dataLoan.sFHAGift2Source);
            AddFormFieldData("sFHAGift2OtherDesc", dataLoan.sFHAGift2OtherDesc);
            AddFormFieldData("sFHAGift2gAmt", dataLoan.sFHAGift2gAmt_rep); 

            AddFormFieldData("sFHAArmIndex", dataLoan.sFHAArmIndex);

            AddFormFieldData("sFHAScoreByTotalTri", dataLoan.sFHAScoreByTotalTri);  

            AddFormFieldData("aFHANegCfRentalI", dataLoan.sFHANegCfRentalI_rep);
            AddFormFieldData("aFHABLdpGsaTri", dataLoan.sFHABLdpGsaTri);
            AddFormFieldData("aFHACLdpGsaTri", dataLoan.sFHACLdpGsaTri);

            AddFormFieldData("aBNm", dataLoan.sCombinedBorNm);
            AddFormFieldData("aBSsn", dataLoan.sCombinedBorSsn);
            AddFormFieldData("aCNm", dataLoan.sCombinedCoborNm);
            AddFormFieldData("aCSsn", dataLoan.sCombinedCoborSsn);
            AddFormFieldData("aFHABBaseI", dataLoan.sFHABBaseI_rep);
            AddFormFieldData("aFHACBaseI", dataLoan.sFHACBaseI_rep);

            AddFormFieldData("aFHABCaivrsNum", dataLoan.sFHABCaivrsNum);
            AddFormFieldData("aFHABOI", dataLoan.sFHABOI_rep);
            AddFormFieldData("aFHACCaivrsNum", dataLoan.sFHACCaivrsNum);
            AddFormFieldData("aFHACOI", dataLoan.sFHACOI_rep);
            AddFormFieldData("aFHAChildSupportPmt", dataLoan.sFHAChildSupportPmt_rep);
            AddFormFieldData("aFHADebtInstallBal", dataLoan.sFHADebtInstallBal_rep);
            AddFormFieldData("aFHADebtInstallPmt", dataLoan.sFHADebtInstallPmt_rep);
            AddFormFieldData("aFHAPmtFixedTot", dataLoan.sFHAPmtFixedTot_rep);
            AddFormFieldData("aFHAGrossMonI", dataLoan.sFHAGrossMonI_rep);
            AddFormFieldData("aFHAOtherDebtBal", dataLoan.sFHAOtherDebtBal_rep);
            AddFormFieldData("aFHAOtherDebtPmt", dataLoan.sFHAOtherDebtPmt_rep);

            AddFormFieldData("aFHAFixedPmtToIRatio", dataLoan.sFHAFixedPmtToIRatio_rep);
            AddFormFieldData("aFHAMPmtToIRatio", dataLoan.sFHAMPmtToIRatio_rep);


            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sCltvR", dataLoan.sHcltvR_rep);
            AddFormFieldData("sEstateHeldT", dataLoan.sEstateHeldT);
            AddFormFieldData("sFHAConstructionT", dataLoan.sFHAConstructionT);
            AddFormFieldData("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            AddFormFieldData("sFHAMonthlyPmt", dataLoan.sFHAMonthlyPmt_rep);
            AddFormFieldData("sFHAPro1stMPmt", dataLoan.sFHAPro1stMPmt_rep);
            AddFormFieldData("sFHAPro2ndFinPmt", dataLoan.sFHAPro2ndFinPmt_rep);
            AddFormFieldData("sFHAProGroundRent", dataLoan.sFHAProGroundRent_rep);
            AddFormFieldData("sFHAProHazIns", dataLoan.sFHAProHazIns_rep);
            AddFormFieldData("sFHAProHoAssocDues", dataLoan.sFHAProHoAssocDues_rep);
            AddFormFieldData("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            AddFormFieldData("sFHAProRealETx", dataLoan.sFHAProRealETx_rep);
            AddFormFieldData("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sFntcSrc", dataLoan.sFntcSrc);
            AddFormFieldData("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            AddFormFieldData("sLtvR", dataLoan.sLtvR_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            AddFormFieldData("sQualIR", dataLoan.sQualIR_rep);
            AddFormFieldData("sRsrvMonNumDesc", dataLoan.sRsrvMonNumDesc);
            AddFormFieldData("sSpAddrSingleLine", dataLoan.sSpAddr_SingleLine);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sTransmFntc", dataLoan.sTransmFntc_rep);
            if (dataLoan.sTransmUwerCommentsOnSeparatePage) 
            {
                AddFormFieldData("sTransmUwerComments", "SEE ATTACHMENT PAGE");
            } 
            else 
            {
                AddFormFieldData("sTransmUwerComments", dataLoan.sTransmUwerComments);
            }
            AddFormFieldData("sVerifAssetAmt", dataLoan.sVerifAssetAmt_rep);
            AddFormFieldData("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);
            AddFormFieldData("sGseSpHud92900LtT", dataLoan.sGseSpHud92900LtT);
            AddFormFieldData("sRAdj1stCapMon", dataLoan.sRAdj1stCapMon_rep);
            AddFormFieldData("sFHASellerContributionPc", dataLoan.sFHASellerContributionPc_rep);
            AddFormFieldData("sFHACcTot", dataLoan.sFHACcTot_rep);
            AddFormFieldData("sChumsIdReviewerAppraisal", dataLoan.sChumsIdReviewerAppraisal);
            AddFormFieldData("aFHANetRentalI", dataLoan.sFHANetRentalI_rep);

            AddFormFieldData("aFHATotBaseI", dataLoan.sFHATotBaseI_rep);
            AddFormFieldData("aFHATotOI", dataLoan.sFHATotOI_rep);
            AddFormFieldData("aFHABTotI", dataLoan.sFHABTotI_rep);
            AddFormFieldData("aFHACTotI", dataLoan.sFHACTotI_rep);
            
            AddFormFieldData("FHA92900LtUnderwriterLicenseNumOfAgent", dataLoan.sFHA92900LtUnderwriterChumsId);

        }
    }


    public class CHUD_92900_LT_CombinedContPDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-LTCont.pdf"; }
        }
        public override bool IsVisible 
        {
            get { return DataLoan.sTransmUwerCommentsOnSeparatePage; }
        }


        public override string Description 
        {
            get { return "Page 1 Continuation Page"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFHAHousingActSection", dataLoan.sFHAHousingActSection);

            AddFormFieldData("aBNm", dataLoan.sCombinedBorNm);
            AddFormFieldData("aCNm", dataLoan.sCombinedCoborNm);

            AddFormFieldData("sTransmUwerComments", dataLoan.sTransmUwerComments);



        }
    }
    public class CHUD_92900_LT_2PDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-LT_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
        }


    }

    public class CHUD_92900_LTCombinedPDF : AbstractBatchPDF
    {
        public override bool OnlyPrintPrimary
        {
            get { return true; }
        }
        public override string Description 
        {
            get { return "FHA Loan Combined Underwriting and Transmittal Summary (HUD-92900-LT)"; }
        }
                public override string EditLink 
                {
                    get { return "/newlos/FHA/FHAHud92900LtCombined.aspx"; }
                }
        
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CHUD_92900_LT_Combined_1PDF(),
                                                 new CHUD_92900_LT_CombinedContPDF(),
                                                 new CHUD_92900_LT_2PDF()
                                             };
            }
        }

    }

    public class CHUD_92900_LTPDF : AbstractBatchPDF
    {
 
        public override string Description 
        {
            get { return "FHA Loan Underwriting and Transmittal Summary (HUD-92900-LT)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/FHA/FHAHud92900Lt.aspx"; }
        }
        
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CHUD_92900_LT_1PDF(),
                                                 new CHUD_92900_LTContPDF(),
                                                 new CHUD_92900_LT_2PDF()
                                             };
            }
        }

    }

}