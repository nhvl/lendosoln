﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Pdf
{
    public class CPmlSummaryPDF : AbstractFileDBBasedPDF
    {
        protected override string FileDbKey
        {
            get { return sLId.ToString("N") + "_pmlsummary.pdf"; }
        }

        public override string Description
        {
            get { return "PML Summary"; }
        }
    }
}
