﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using DataAccess;
using LendersOffice.PdfGenerator;
using LendersOffice.ObjLib.Conversions.LQBAppraisal;
using LQBAppraisal.LQBAppraisalRequest;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CLQBAppraisalOrderPDF : AbstractXsltPdf
    {
        protected override void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp)
        {
            writer.WriteStartElement("APPRAISAL_ORDER");
            var order = LQBAppraisalOrder.Load(Arguments["loanid"], Arguments["orderNumber"]);
                        
            writer.WriteElementString("OrderNumber", order.OrderNumber);
            writer.WriteElementString("CaseNumber", order.AppraisalOrder.LoanInfo.CaseNumber);
            writer.WriteElementString("LoanNumber", order.AppraisalOrder.LoanInfo.LoanNumber);

            var borrowerInfoList = order.AppraisalOrder.BorrowerInfoList;
            for (int i = 0; i < borrowerInfoList.Count; i++)
            {
                writer.WriteStartElement("BorrowerInfo");
                writer.WriteElementString("BorrowerName", borrowerInfoList[i].BorrowerName);
                writer.WriteElementString("BorrowerEmail", borrowerInfoList[i].BorrowerEmail);
                writer.WriteElementString("CoborrName", borrowerInfoList[i].CoborrowerName);
                writer.WriteEndElement();
            }

            writer.WriteElementString("ContactName", order.AppraisalOrder.ContactInfo.ContactName);
            writer.WriteElementString("ContactPhone", order.AppraisalOrder.ContactInfo.ContactPhone);
            writer.WriteElementString("ContactWork", order.AppraisalOrder.ContactInfo.ContactWork);
            writer.WriteElementString("ContactOther", order.AppraisalOrder.ContactInfo.ContactOther);
            writer.WriteElementString("ContactEmail", order.AppraisalOrder.ContactInfo.ContactEmail);

            writer.WriteElementString("Addr1", order.AppraisalOrder.PropertyInfo.PropertyAddress);
            string addr2 = string.Format("{0}, {1} {2}",
                order.AppraisalOrder.PropertyInfo.PropertyCity,
                order.AppraisalOrder.PropertyInfo.PropertyState,
                order.AppraisalOrder.PropertyInfo.PropertyZIP);
            if (addr2.TrimWhitespaceAndBOM() == ",")
                addr2 = string.Empty;
            writer.WriteElementString("Addr2", addr2);
            writer.WriteElementString("PropertyType", EnumString(order.AppraisalOrder.PropertyInfo.PropertyType));
            writer.WriteElementString("Occupancy", EnumString(order.AppraisalOrder.PropertyInfo.OccupancyType));
            writer.WriteElementString("County", order.AppraisalOrder.PropertyInfo.PropertyCounty);
            writer.WriteElementString("LegalDescription", order.AppraisalOrder.PropertyInfo.PropertyLegalDescription);
            writer.WriteElementString("MhAdvantage", BoolString(order.AppraisalOrder.PropertyInfo.MhAdvantage));

            string dateNeeded = FormatDateString(order.AppraisalOrder.OrderInfo.AppraisalNeededDate);
            writer.WriteElementString("DateNeeded_String", dateNeeded);

            writer.WriteElementString("IntendedUse", EnumString(order.AppraisalOrder.LoanInfo.IntendedUse));
            writer.WriteElementString("sLAmtCalc", order.AppraisalOrder.LoanInfo.LoanAmount);
            writer.WriteElementString("sPurchPrice", order.AppraisalOrder.LoanInfo.PurchasePrice);
            writer.WriteElementString("LoanTypeName", EnumString(order.AppraisalOrder.LoanInfo.LoanType));
            var orders = order.AppraisalOrder.OrderInfo.OrderedProductList;
            for (int i = 0; i < 5; i++)
            {
                if (i < orders.Count)
                    writer.WriteElementString("ProductID" + (i + 1), orders[i].ProductName);
                else
                    writer.WriteElementString("ProductID" + (i + 1), "");
            }

            writer.WriteElementString("AppraiserName", order.OrderDataUpdate.AssignedAppraiser.AppraiserInfo.ApprName);
            writer.WriteElementString("AppraiserCompany", order.OrderDataUpdate.AssignedAppraiser.AppraiserCompanyInfo.ApprCompanyName);
            writer.WriteElementString("AppraiserAddr1", order.OrderDataUpdate.AssignedAppraiser.AppraiserCompanyInfo.ApprAddress);
            string appAddr2 = string.Format("{0}, {1} {2}",
                order.OrderDataUpdate.AssignedAppraiser.AppraiserCompanyInfo.ApprCity,
                order.OrderDataUpdate.AssignedAppraiser.AppraiserCompanyInfo.ApprState,
                order.OrderDataUpdate.AssignedAppraiser.AppraiserCompanyInfo.ApprZIP);
            if (appAddr2.TrimWhitespaceAndBOM() == ",")
                appAddr2 = string.Empty;
            writer.WriteElementString("AppraiserAddr2", appAddr2);
            writer.WriteElementString("AppraiserPhone", order.OrderDataUpdate.AssignedAppraiser.AppraiserCompanyInfo.ApprCompanyPhone);
            writer.WriteElementString("AppraiserFax", order.OrderDataUpdate.AssignedAppraiser.AppraiserCompanyInfo.ApprCompanyFax);

            var appraiserLicense = order.OrderDataUpdate.AssignedAppraiser.AppraiserLicenseList.FirstOrDefault() ?? new LQBAppraisal.AppraisalStatusUpdate.AppraiserLicense();
            writer.WriteElementString("AppraiserLicNum", appraiserLicense.ApprLicenseNum);
            writer.WriteElementString("AppraiserLicState", appraiserLicense.ApprLicenseState);
            writer.WriteElementString("AppraiserLicExpD", FormatDateString(appraiserLicense.ApprLicenseExpDate));
            

            writer.WriteElementString("AMCName", order.OrderDataUpdate.AMCContactInfo.ContactName);
            writer.WriteElementString("AMCPhone", order.OrderDataUpdate.AMCContactInfo.ContactPhone);
            writer.WriteElementString("AMCEmail", order.OrderDataUpdate.AMCContactInfo.ContactEmail);

            writer.WriteElementString("StatusName", order.Status);
            writer.WriteElementString("StatusDate", order.StatusDate_rep);
            writer.WriteElementString("StatusComments", order.OrderDataUpdate.OrderFields.StatusComments);

            string inspectD = FormatDateString(order.OrderDataUpdate.OrderFields.DateInspectionScheduled);
            writer.WriteElementString("InspectionScheduled", inspectD);
            
            writer.WriteElementString("InspectionTime", order.OrderDataUpdate.OrderFields.InspectionTime);
            writer.WriteElementString("ReviewerComments", order.OrderDataUpdate.OrderFields.ReviewersComments);
            writer.WriteElementString("EstimatedValue", order.OrderDataUpdate.OrderFields.EstimatedValue);
            
            string estCompleteD = FormatDateString(order.OrderDataUpdate.OrderFields.DateEstCompletion);
            writer.WriteElementString("DateEstCompletion", estCompleteD);

            foreach (var orderHistoryItem in order.OrderHistory)
            {
                writer.WriteStartElement("StatusHistoryItem");
                writer.WriteElementString("DateTimeStamp_String", orderHistoryItem.DateTimeStamp.ToShortDateString());
                writer.WriteElementString("StatusName", orderHistoryItem.StatusName);
                writer.WriteElementString("StatusDescription", orderHistoryItem.StatusDescription);
                writer.WriteEndElement(); // </StatusHistoryItem>
            }

            writer.WriteEndElement(); // </APPRAISAL_ORDER>
        }

        private string FormatDateString(string dateString)
        {
            //convert from dd-MM-yyyy to ShortDateString

            DateTime date;
            if (DateTime.TryParseExact(
                dateString,
                "dd-MM-yyyy",
                null,
                System.Globalization.DateTimeStyles.None,
                out date))
            {
                return date.ToShortDateString();
            }

            //default empty if can't parse
            return string.Empty;
        }

        private string EnumString(E_LoanInfoLoanType type)
        {
            switch(type)
            {
                case E_LoanInfoLoanType.Conventional: return "Conventional";
                case E_LoanInfoLoanType.FHA: return "FHA";
                case E_LoanInfoLoanType.Other: return "Other";
                case E_LoanInfoLoanType.USDARuralHousing: return "USDA Rural";
                case E_LoanInfoLoanType.VA: return "VA";
            }
            return "";
        }
        private string EnumString(E_PropertyInfoOccupancyType type)
        {
            switch(type)
            {
                case E_PropertyInfoOccupancyType.Investment: return "Investment";
                case E_PropertyInfoOccupancyType.PrimaryResidence: return "Primary Residence";
                case E_PropertyInfoOccupancyType.SecondaryResidence: return "Secondary Residence";
            }
            return "";
        }
        private string EnumString(E_PropertyInfoPropertyType type)
        {
            switch(type)
            {
                case E_PropertyInfoPropertyType.Attached: return "Attached";
                case E_PropertyInfoPropertyType.Condominium: return "Condominium";
                case E_PropertyInfoPropertyType.Cooperative: return "Cooperative";
                case E_PropertyInfoPropertyType.Detached: return "Detached";
                case E_PropertyInfoPropertyType.DetachedCondominium: return "Detached Condominium";
                case E_PropertyInfoPropertyType.HighRiseCondominium: return "High Rise Condominium";
                case E_PropertyInfoPropertyType.ManufacturedHomeCondominium: return "Manufactured Home Condominium";
                case E_PropertyInfoPropertyType.ManufacturedHomeMultiwide: return "Manufactured Home Multiwide";
                case E_PropertyInfoPropertyType.ManufacturedHousing: return "Manufactured Housing";
                case E_PropertyInfoPropertyType.ManufacturedHousingSingleWide: return "Manufactured Housing Single Wide";
                case E_PropertyInfoPropertyType.Modular: return "Modular";
                case E_PropertyInfoPropertyType.PUD: return "PUD";
            }
            return "";
        }
        private string EnumString(E_LoanInfoIntendedUse type)
        {
            switch(type)
            {
                case E_LoanInfoIntendedUse.Construction: return "Construction";
                case E_LoanInfoIntendedUse.ConstructionPerm: return "Construction Permanent";
                case E_LoanInfoIntendedUse.FHAStreamlineRefi: return "FHA Streamline Refinance";
                case E_LoanInfoIntendedUse.Other: return "Other";
                case E_LoanInfoIntendedUse.Purchase: return "Purchase";
                case E_LoanInfoIntendedUse.RefinanceCashout: return "Refinance Cash-out";
                case E_LoanInfoIntendedUse.RefiRateTerm: return "Refinance Rate/Term";
                case E_LoanInfoIntendedUse.VAIRRRL: return "VA IRRRL";
            }
            return "";
        }
        private string BoolString(bool? value)
        {
            if (value.HasValue)
            {
                return value.Value ? "Yes" : "No";
            }

            return string.Empty;
        }
        public override string Description
        {
            get { return "Appraisal Order"; }
        }

        public override PDFPageSize PageSize
        {
            get { return PDFPageSize.Letter; }
        }
    }
}
