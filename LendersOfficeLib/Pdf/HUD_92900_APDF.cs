using System;
using System.Collections;
using System.Collections.Specialized;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public abstract class AbstractHUD_92900_APDF : AbstractLetterPDF
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
                AddFormFieldData("aFHABorrCertOwnMoreThan4DwellingsTri", dataApp.aFHABorrCertOwnMoreThan4DwellingsTri);
                AddFormFieldData("sFHACondIfProposedConstBuilderCertified", dataLoan.sFHACondIfProposedConstBuilderCertified);
                AddFormFieldData("sFHACondIfNewConstLenderCertified", dataLoan.sFHACondIfNewConstLenderCertified);
                AddFormFieldData("sFHACondBuilderWarrantyRequired", dataLoan.sFHACondBuilderWarrantyRequired);
                AddFormFieldData("sFHACond10YrsWarranty", dataLoan.sFHACond10YrsWarranty);
                AddFormFieldData("sFHACondOther", dataLoan.sFHACondOther);
                AddFormFieldData("sFHACondOtherDesc", dataLoan.sFHACondOtherDesc);

            if(dataLoan.sIsRequire2016FhaAddendum)
            {
                AddFormFieldData("aFHABorrCertOtherPropToBeSoldNA", dataApp.aFhaBorrCertOtherProptoBeSoldNA);
                AddFormFieldData("sFHADirectEndorsementAllConditionsOfApprovalMet", dataLoan.sFhaDirectEndorsementAllConditionsOfApprovalMet);

                if(dataLoan.sLT != E_sLT.VA)
                {
                    AddFormFieldData("aFHABorrCertWillOccupyFor1YearTri", dataApp.aFhaBorrCertWillOccupyFor1YearTri);
                    AddFormFieldData("aFHABorrCertOccIsChildAsHome", dataApp.aFHABorrCertOccIsChildAsHome);
                    AddFormFieldData("aFHABorrCertOccIsChildAsHomePrev", dataApp.aFHABorrCertOccIsChildAsHomePrev);
                }
            }

            AddFormFieldData("sLT", dataLoan.sLT);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
            AddFormFieldData("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            string name = dataApp.aBNm;

            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "")
                name += " & \n" + dataApp.aCNm;

            AddFormFieldData("aBNm_aCNm", dataApp.aBNm_aCNm);

            AddFormFieldData("BorrowerInfo", Tools.FormatAddress(name, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("PropertyAddress", dataLoan.sSpAddr_SingleLine);
            AddFormFieldData("sSpLegalDesc", dataLoan.sSpLegalDesc);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sTermInYrRemainingMonths", dataLoan.sTermInYrRemainingMonths_rep);
            AddFormFieldData("sTermInYr", dataLoan.sTermInYr_rep);

            // OPM 109828: only populate when it is non-negative
            if (0.0M <= dataLoan.sFHALDiscnt)
            {
                // OPM 119474: want to print value even if it is 0
                var oldFormatTarget = dataLoan.GetFormatTarget();
                dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
                AddFormFieldData("sFHALDiscnt", dataLoan.sFHALDiscnt_rep);
                dataLoan.SetFormatTarget(oldFormatTarget);
            }
            
            AddFormFieldData("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            AddFormFieldData("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            AddFormFieldData("sFHAProMInsMon", dataLoan.sFHAProMInsMon_rep);
            AddFormFieldData("sFHALenderIdCode", dataLoan.sLT == E_sLT.VA ? dataLoan.sVALenderIdCode : dataLoan.sFHALenderIdCode); // misnomer when VA - opm 68873
            AddFormFieldData("sFHASponsorAgentIdCode", dataLoan.sLT == E_sLT.VA ? dataLoan.sVASponsorAgentIdCode : dataLoan.sFHASponsorAgentIdCode);

            IPreparerFields lender = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("FHAAddendumLender", Tools.FormatAddress(lender.CompanyName, lender.StreetAddr, lender.City, lender.State, lender.Zip)); 
            AddFormFieldData("FHAAddendumLenderPreparerName", lender.PreparerName); 
            AddFormFieldData("FHAAddendumLenderTitle", lender.Title); 
            AddFormFieldData("FHAAddendumLenderPhoneOfCompany", lender.PhoneOfCompany);
            if (dataLoan.sIsRequire2016FhaAddendum)
            {
                AddFormFieldData("FHAAddendumLenderNameAndTitle", lender.PreparerName + " " + lender.Title);
            }

            // OPM 32345
            AddFormFieldData("FHAAddendumLenderCompanyName", lender.CompanyName);
            
            IPreparerFields sponsor = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumSponsor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("FHAAddendumSponsor", Tools.FormatAddress(sponsor.CompanyName, sponsor.StreetAddr, sponsor.City, sponsor.State, sponsor.Zip));

            IPreparerFields duly = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumDulyAgent, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("FHAAddendumDulyAgentName", duly.PreparerName);
            AddFormFieldData("FHAAddendumDulyAgentAddress", Tools.FormatAddress(duly.StreetAddr, duly.City, duly.State, duly.Zip));
            AddFormFieldData("FHAAddendumDulyAgentTitle", duly.Title);

            AddFormFieldData("sFHAPurposeIsPurchaseExistHome", dataLoan.sFHAPurposeIsPurchaseExistHome);
            AddFormFieldData("sFHAPurposeIsFinanceImprovement", dataLoan.sFHAPurposeIsFinanceImprovement);
            AddFormFieldData("sFHAPurposeIsRefinance", dataLoan.sFHAPurposeIsRefinance);
            AddFormFieldData("sFHAPurposeIsPurchaseNewCondo", dataLoan.sFHAPurposeIsPurchaseNewCondo);
            AddFormFieldData("sFHAPurposeIsPurchaseExistCondo", dataLoan.sFHAPurposeIsPurchaseExistCondo);
            AddFormFieldData("sFHAPurposeIsPurchaseNewHome", dataLoan.sFHAPurposeIsPurchaseNewHome);
            AddFormFieldData("sFHAPurposeIsConstructHome", dataLoan.sFHAPurposeIsConstructHome);
            AddFormFieldData("sFHAPurposeIsFinanceCoopPurchase", dataLoan.sFHAPurposeIsFinanceCoopPurchase);
            AddFormFieldData("sFHAPurposeIsPurchaseManufacturedHome", dataLoan.sFHAPurposeIsPurchaseManufacturedHome);
            AddFormFieldData("sFHAPurposeIsManufacturedHomeAndLot", dataLoan.sFHAPurposeIsManufacturedHomeAndLot);
            AddFormFieldData("sFHAPurposeIsRefiManufacturedHomeToBuyLot", dataLoan.sFHAPurposeIsRefiManufacturedHomeToBuyLot);
            AddFormFieldData("sFHAPurposeIsRefiManufacturedHomeOrLotLoan", dataLoan.sFHAPurposeIsRefiManufacturedHomeOrLotLoan);
            AddFormFieldData("aVaVestTitleT", dataApp.aVaVestTitleT);
            AddFormFieldData("aVaVestTitleODesc", dataApp.aVaVestTitleODesc );

            AddFormFieldData("aHas1stTimeBuyer", dataApp.aHas1stTimeBuyerTri);
            AddFormFieldData("aFHABorrCertOtherPropToBeSoldTri", dataApp.aFHABorrCertOtherPropToBeSoldTri);
            AddFormFieldData("aFHABorrCertOtherPropSalesPrice", dataApp.aFHABorrCertOtherPropSalesPrice_rep);
            AddFormFieldData("aFHABorrCertOtherPropOrigMAmt", dataApp.aFHABorrCertOtherPropOrigMAmt_rep);
            AddFormFieldData("aFHABorrCertOtherPropStAddr", Tools.FormatAddress(dataApp.aFHABorrCertOtherPropStAddr, dataApp.aFHABorrCertOtherPropCity, dataApp.aFHABorrCertOtherPropState, dataApp.aFHABorrCertOtherPropZip));
            AddFormFieldData("aFHABorrCertOtherPropCoveredByThisLoanTri", dataApp.aFHABorrCertOtherPropCoveredByThisLoanTri);
            AddFormFieldData("aFHABorrCertInformedPropVal", dataApp.aFHABorrCertInformedPropVal_rep);
            AddFormFieldData("aFHABorrCertInformedPropValDeterminedByVA", dataApp.aFHABorrCertInformedPropValDeterminedByVA);
            AddFormFieldData("aFHABorrCertInformedPropValDeterminedByHUD", dataApp.aFHABorrCertInformedPropValDeterminedByHUD);
            AddFormFieldData("aFHABorrCertInformedPropValAwareAtContractSigning", dataApp.aFHABorrCertInformedPropValAwareAtContractSigning);
            AddFormFieldData("aFHABorrCertInformedPropValNotAwareAtContractSigning", dataApp.aFHABorrCertInformedPropValNotAwareAtContractSigning);
            AddFormFieldData("aFHABorrCertReceivedLeadPaintPoisonInfoTri", dataApp.aFHABorrCertReceivedLeadPaintPoisonInfoTri);
            AddFormFieldData("aFHABorrCertOwnOrSoldOtherFHAPropTri", dataApp.aFHABorrCertOwnOrSoldOtherFHAPropTri);

            AddFormFieldData("aVaBorrCertHadVaLoanTri", dataApp.aVaBorrCertHadVaLoanTri);
            if (dataLoan.sLT == E_sLT.VA)
            {
                AddFormFieldData("aFHABorrCertOccIsAsHome", dataApp.aFHABorrCertOccIsAsHome);
                AddFormFieldData("aFHABorrCertOccIsAsHomeForActiveSpouse", dataApp.aFHABorrCertOccIsAsHomeForActiveSpouse);
                AddFormFieldData("aFHABorrCertOccIsAsHomePrev", dataApp.aFHABorrCertOccIsAsHomePrev);
                AddFormFieldData("aFHABorrCertOccIsAsHomePrevForActiveSpouse", dataApp.aFHABorrCertOccIsAsHomePrevForActiveSpouse);
            }

            AddFormFieldData("sFHAApprovedSubj", dataLoan.sFHAApprovedSubj);
            AddFormFieldData("sFHAApprovedD", dataLoan.sFHAApprovedD_rep);
            AddFormFieldData("sFHAApprovedExpiresD", dataLoan.sFHAApprovedExpiresD_rep);
            AddFormFieldData("sFHAApprovedModified", dataLoan.sFHAApprovedModified);
            AddFormFieldData("sFHAModifiedFinalLAmt", dataLoan.sFHAModifiedFinalLAmt_rep);
            AddFormFieldData("sFHAModifiedNoteIR", dataLoan.sFHAModifiedNoteIR_rep);
            AddFormFieldData("sFHAModifiedTermInYr", dataLoan.sFHAModifiedTermInYr_rep);
            AddFormFieldData("sFHAModifiedTermInMonths", dataLoan.sFHAModifiedTermInMonths_rep);
            AddFormFieldData("sFHAModifiedFfUfmip1003", dataLoan.sFHAModifiedFfUfmip1003_rep);
            AddFormFieldData("sFHAModifiedProMInsMon", dataLoan.sFHAModifiedProMInsMon_rep);
            AddFormFieldData("sFHAModifiedProMIns", dataLoan.sFHAModifiedProMIns_rep);
            AddFormFieldData("sFHAModifiedMonthlyPmt", dataLoan.sFHAModifiedMonthlyPmt_rep);

            AddFormFieldData("sFHACondOwnerOccNotRequired", dataLoan.sFHACondOwnerOccNotRequired);
            AddFormFieldData("sFHARatedAcceptedByTotalScorecard", dataLoan.sFHARatedAcceptedByTotalScorecard);
            AddFormFieldData("sFHARatedReferByTotalScorecard", dataLoan.sFHARatedReferByTotalScorecard);
            AddFormFieldData("sFHAMortgageHaveFinanceInterestTri", dataLoan.sFHAMortgageHaveFinanceInterestTri);
            AddFormFieldData("sFHACondMortgageHighLtvForNonOccupant", dataLoan.sFHACondMortgageHighLtvForNonOccupant);
            
            AddFormFieldData("FHAAddendumUnderwriterPreparerName", dataLoan.sFHAAddendumUnderwriterName);
            AddFormFieldData("FHAAddendumUnderwriterLicenseNumOfAgent", dataLoan.sFHAAddendumUnderwriterChumsId);
        
            if (dataLoan.sFhaDirectEndorsementWasRatedAcceptAndUnderwritten)
            {
                AddFormFieldData("FHAAddendumUnderwriterLicenseNumOfAgentAccept", dataLoan.sFHAAddendumUnderwriterChumsId);
            }

            if (dataLoan.sFHARatedReferByTotalScorecard)
            {
                AddFormFieldData("FHAAddendumUnderwriterLicenseNumOfAgentRefer", dataLoan.sFHAAddendumUnderwriterChumsId);
            }
            
            AddFormFieldData("FHAAddendumMortgageePreparerName", dataLoan.sFHAAddendumMortgageeName);
            AddFormFieldData("FHAAddendumMortgageePreparerNameAndTitle", dataLoan.sFHAAddendumMortgageeName + " " + dataLoan.sFHAAddendumMortgageeTitle);

            AddFormFieldData("App1003InterviewerCompanyName", dataLoan.sFhaLoanOrigCompanyNm);
            AddFormFieldData("App1003InterviewerTaxID", dataLoan.sFHASponsoredOriginatorEIN);
            AddFormFieldData("App1003InterviewerCompanyLoanOriginatorIdentifier", dataLoan.sFhaLoanOrigCompanyNmlsId);
        }
    }
    public class CHUD_92900_A_1PDF : AbstractHUD_92900_APDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-A_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }

    }
    public class CHUD_92900_A_2PDF : AbstractHUD_92900_APDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-A_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }

    }
    public class CHUD_92900_A_3PDF : AbstractHUD_92900_APDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-A_3.pdf"; }
        }

        public override string Description 
        {
            get { return "Page 3"; }
        }

    }
    public class CHUD_92900_A_4PDF : AbstractHUD_92900_APDF 
    {

        public override string PdfFile 
        {
            get { return "HUD-92900-A_4.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 4"; }
        }

    }


    public class CHUD_92900_APDF : AbstractBatchPDF 
    {

        public override string Description 
        {
            get { return "FHA/VA Addendum to URLA (HUD-92900-A)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/FHA/FHAAddendum.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CHUD_92900_A_1PDF(),
                                                 new CHUD_92900_A_2PDF(),
                                                 new CHUD_92900_A_3PDF(),
                                                 new CHUD_92900_A_4PDF()
                                             };
            }
        }
    }

    #region 2016 HUD_92900_A
    public class CHUD_92900_A_2010_1PDF : AbstractHUD_92900_APDF
    {
        public override string PdfFile
        {
            get { return "HUD-92900-A_2010_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }

    }

    public class CHUD_92900_A_2010_2PDF : AbstractHUD_92900_APDF
    {
        public override string PdfFile
        {
            get { return "HUD-92900-A_2010_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }

    }
    public class CHUD_92900_A_2010_3PDF : AbstractHUD_92900_APDF
    {
        public override string PdfFile
        {
            get { return "HUD-92900-A_2010_3.pdf"; }
        }

        public override string Description
        {
            get { return "Page 3"; }
        }

    }
    public class CHUD_92900_A_2010_4PDF : AbstractHUD_92900_APDF
    {

        public override string PdfFile
        {
            get { return "HUD-92900-A_2010_4.pdf"; }
        }
        public override string Description
        {
            get { return "Page 4"; }
        }
    }

    public class CHUD_92900_A_2010_PDF : AbstractBatchPDF
    {

        public override string Description
        {
            get
            {
                if(DataLoan != null)
                {
                    return "FHA/VA Addendum to URLA (HUD-92900-A)" + (DataLoan.sIsRequire2016FhaAddendum ? " (2010)" : "");
                }
                else
                {
                    return "FHA/VA Addendum to URLA (HUD-92900-A) (2010)";
                }
            }
        }
        public override string EditLink
        {
            get { return "/newlos/FHA/FHAAddendum.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CHUD_92900_A_2010_1PDF(),
                                                 new CHUD_92900_A_2010_2PDF(),
                                                 new CHUD_92900_A_2010_3PDF(),
                                                 new CHUD_92900_A_2010_4PDF()
                                             };
            }
        }
    }
    #endregion


    #region Old HUD_92900_A
    public class CHUD_92900_A_2008_1PDF : AbstractHUD_92900_APDF
    {
        public override string PdfFile
        {
            get { return "HUD-92900-A_2008_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }

    }
    public class CHUD_92900_A_2008_2PDF : AbstractHUD_92900_APDF
    {
        public override string PdfFile
        {
            get { return "HUD-92900-A_2008_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }

    }
    public class CHUD_92900_A_2008_3PDF : AbstractHUD_92900_APDF
    {
        public override string PdfFile
        {
            get { return "HUD-92900-A_2008_3.pdf"; }
        }

        public override string Description
        {
            get { return "Page 3"; }
        }

    }
    public class CHUD_92900_A_2008_4PDF : AbstractHUD_92900_APDF
    {

        public override string PdfFile
        {
            get { return "HUD-92900-A_2008_4.pdf"; }
        }
        public override string Description
        {
            get { return "Page 4"; }
        }

    }


    public class CHUD_92900_2008_APDF : AbstractBatchPDF
    {

        public override string Description
        {
            get { return "FHA/VA Addendum to URLA (HUD-92900-A) (2008)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/FHA/FHAAddendum.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CHUD_92900_A_2008_1PDF(),
                                                 new CHUD_92900_A_2008_2PDF(),
                                                 new CHUD_92900_A_2008_3PDF(),
                                                 new CHUD_92900_A_2008_4PDF()
                                             };
            }
        }
    }
    #endregion
}
