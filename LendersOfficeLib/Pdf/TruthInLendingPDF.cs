namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Common;

    public class CTruthInLendingPDF : AbstractLegalPDF
	{
        public override string PdfFile 
        {
            get
            {
                if (DataLoan == null)
                {
                    return "TruthInLendingH4EFixedRate.pdf";
                }

                switch (DataLoan.sTILPDFMode)
                {
                        
                    case E_sTILPDFMode.FixedRateNoIntOnly:
                        return "TruthInLendingH4EFixedRate.pdf";
                    case E_sTILPDFMode.FixedRateWithIntOnly:
                        return "TruthInLendingH4HFixedInterestOnly.pdf";
                    case E_sTILPDFMode.ARMNoIntOnly:
                        return "TruthInLendingH4FArm.pdf";
                    case E_sTILPDFMode.ARMWithIntOnly:
                        return "TruthInLendingH4FArmWithInterestOnly.pdf";
                    case E_sTILPDFMode.NegARMOption:
                        return "TruthInLendingH4GNegArm.pdf";
                    default:
                        throw new UnhandledEnumException(DataLoan.sTILPDFMode);
                }
            }
        }

        public override bool IsVisible
        {
            get { return !DataLoan.sIsConstructionLoanWithConstructionRefiData; }
        }

        public virtual string EstimateSymbol 
        {
            get { return "* "; }
        }
        public override string Description 
        {
            get { return "Truth In Lending (<b>Effective after 01/30/2011</b>)"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/TruthInLending.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string borrowerName = dataApp.aBNm;
            borrowerName += Environment.NewLine + dataApp.aCNm;
            string loanNumber = dataLoan.sLNm;

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Til,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("GfeTil", Tools.FormatAddress(gfeTil.CompanyName, gfeTil.StreetAddr, gfeTil.City, gfeTil.State, gfeTil.Zip, gfeTil.PhoneOfCompany));

            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);
            AddFormFieldData("LenderName", gfeTil.CompanyName);
            AddFormFieldData("LoanNumber", loanNumber);
            AddFormFieldData("ApplicantName", borrowerName);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            string asterisk = dataLoan.sAsteriskEstimate ? EstimateSymbol : "";

            AddFormFieldData("sSpFullAddr", dataLoan.sSpFullAddr);
            AddFormFieldData("sApr", asterisk + dataLoan.sApr_rep, false);
            AddFormFieldData("sNoteIR", asterisk + dataLoan.sNoteIR_rep, false);
            AddFormFieldData("sFinCharge", asterisk + dataLoan.sFinCharge_rep, false);
            AddFormFieldData("sFinancedAmt", asterisk + dataLoan.sFinancedAmt_rep, false);
            AddFormFieldData("sSchedPmtTot", asterisk + dataLoan.sSchedPmtTot_rep, false);
            AddFormFieldData("sAprIncludesReqDeposit", dataLoan.sAprIncludesReqDeposit);
            AddFormFieldData("sHasDemandFeature", dataLoan.sHasDemandFeature);
            AddFormFieldData("sHasVarRFeature", dataLoan.sHasVarRFeature);
            AddFormFieldData("sPrepmtPenaltyT", dataLoan.sPrepmtPenaltyT);
            AddFormFieldData("sPrepmtRefundT", dataLoan.sPrepmtRefundT);
            AddFormFieldData("sAssumeLT", dataLoan.sAssumeLT);
            AddFormFieldData("sAsteriskEstimate", dataLoan.sAsteriskEstimate);
            AddFormFieldData("sOnlyLatePmtEstimate", dataLoan.sOnlyLatePmtEstimate);
            AddFormFieldData("sSecurityPurch", dataLoan.sSecurityPurch);
            AddFormFieldData("sSecurityCurrentOwn", dataLoan.sSecurityCurrentOwn);
            AddFormFieldData("sFilingF", dataLoan.sFilingF);
            AddFormFieldData("sLateDays", dataLoan.sLateDays );
            AddFormFieldData("sLateChargePc", dataLoan.sLateChargePc );
            AddFormFieldData("sLateChargeBaseDesc", dataLoan.sLateChargeBaseDesc);
            AddFormFieldData("sVarRNotes", dataLoan.sVarRNotes);

            try 
            {
                for (int i = 1; i <= dataLoan.sAmortTable.nRows; i++) 
                {
                    AddFormFieldData("sSchedPmtNum" + i, dataLoan.sSchedIR_Rep( i - 1, PmtInfoType.PmtNumber ));
                    AddFormFieldData("sSchedDueD" + i, dataLoan.sSchedIR_Rep( i - 1, PmtInfoType.DueDate ));
                    AddFormFieldData("sSchedPmt" + i, dataLoan.sSchedIR_Rep( i - 1, PmtInfoType.PmtAmount ));
                }
            } 
            catch 
            {
                // Do nothing if sAmortTable is not valid. Generally, this is a bad practice to rely
                // on exception being throw to determine if value is valid or not. However, currently
                // there is no way I could tell if this value is valid. dd 9/30/02
            }


            AddFormFieldData("sInsReqDesc", dataLoan.sInsReqDesc);
            AddFormFieldData("sReqCreditLifeIns", dataLoan.sReqCreditLifeIns);
            AddFormFieldData("sReqCreditDisabilityIns", dataLoan.sReqCreditDisabilityIns);
            AddFormFieldData("sReqPropIns", dataLoan.sReqPropIns);
            AddFormFieldData("sReqFloodIns", dataLoan.sReqFloodIns);
            AddFormFieldData("sIfPurchPropInsFrCreditor", dataLoan.sIfPurchPropInsFrCreditor);
            AddFormFieldData("sIfPurchInsFrCreditor", dataLoan.sIfPurchInsFrCreditor);
            AddFormFieldData("sIfPurchFloodInsFrCreditor", dataLoan.sIfPurchFloodInsFrCreditor);
            AddFormFieldData("sInsFrCreditorAmt", dataLoan.sInsFrCreditorAmt_rep);

            AddFormFieldData("sTaxesAndEscrowPmtNotes", dataLoan.sTaxesAndEscrowPmtNotes);
            AddFormFieldData("sMonthlyPaymentIntroPeriod", dataLoan.sMonthlyPaymentIntroPeriod.ToString());
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            AddFormFieldData("sTaxesAndEscrowPmtIntro", dataLoan.sTaxesAndEscrowPmtIntro_rep);
            AddFormFieldData("sTotalEstMonthlyPmtIntro", dataLoan.sTotalEstMonthlyPmtIntro_rep);
            AddFormFieldData("sMonthlyPaymentFirstAdjStartD", dataLoan.sMonthlyPaymentFirstAdjStartD_rep);
            AddFormFieldData("sInterestRateFirstAdj", dataLoan.sInterestRateFirstAdj_rep);
            AddFormFieldData("sPrincipalPmtFirstAdj", dataLoan.sPrincipalPmtFirstAdj_rep);
            AddFormFieldData("sInterestPmtFirstAdj", dataLoan.sInterestPmtFirstAdj_rep);
            AddFormFieldData("sTaxesAndEscrowPmtFirstAdj", dataLoan.sTaxesAndEscrowPmtFirstAdj_rep);
            AddFormFieldData("sTotalEstMonthlyPmtFirstAdj", dataLoan.sTotalEstMonthlyPmtFirstAdj_rep);
            AddFormFieldData("sMonthlyPaymentMaxIn5StartD", dataLoan.sMonthlyPaymentMaxIn5StartD_rep);
            AddFormFieldData("sInterestRateMaxIn5", dataLoan.sInterestRateMaxIn5_rep);
            AddFormFieldData("sPrincipalPmtMaxIn5", dataLoan.sPrincipalPmtMaxIn5_Print, false);
            AddFormFieldData("sInterestPmtMaxIn5", dataLoan.sInterestPmtMaxIn5_rep);
            AddFormFieldData("sTaxesAndEscrowPmtMaxIn5", dataLoan.sTaxesAndEscrowPmtMaxIn5_rep);
            AddFormFieldData("sTotalEstMonthlyPmtMaxIn5", dataLoan.sTotalEstMonthlyPmtMaxIn5_rep);
            AddFormFieldData("sMonthlyPaymentMaxEverStartD", dataLoan.sMonthlyPaymentMaxEverStartD_rep);
            AddFormFieldData("sInterestRateMaxEver", dataLoan.sInterestRateMaxEver_rep);
            AddFormFieldData("sPrincipalPmtMaxEver", dataLoan.sPrincipalPmtMaxEver_rep);
            AddFormFieldData("sInterestPmtMaxEver", dataLoan.sInterestPmtMaxEver_rep);
            AddFormFieldData("sTaxesAndEscrowPmtMaxEver", dataLoan.sTaxesAndEscrowPmtMaxEver_rep);
            AddFormFieldData("sTotalEstMonthlyPmtMaxEver", dataLoan.sTotalEstMonthlyPmtMaxEver_rep);
            AddFormFieldData("sIntroRateDisclosure", dataLoan.sIntroRateDisclosure);
            AddFormFieldData("sRAdj1stCapMon", dataLoan.sRAdj1stCapMon_rep);
            AddFormFieldData("sIOnlyMon", dataLoan.sIOnlyMon_rep);
            AddFormFieldData("sInterestRateMaxIn5", dataLoan.sInterestRateMaxIn5_rep);
            AddFormFieldData("sPIPmtMaxIn5", dataLoan.sPIPmtMaxIn5_rep);
            AddFormFieldData("sInterestRateMaxEver", dataLoan.sInterestRateMaxEver_rep);
            AddFormFieldData("sPIPmtMaxEver", dataLoan.sPIPmtMaxEver_rep);
            AddFormFieldData("sMonthlyPaymentOptionARMIntroPeriod", dataLoan.sMonthlyPaymentOptionARMIntroPeriod);
            AddFormFieldData("sMonthlyPaymentOptionARMIntroRate", dataLoan.sMonthlyPaymentOptionARMIntroRate);
            AddFormFieldData("sMonthlyMaxPmtIntro", dataLoan.sMonthlyMaxPmtIntro_rep);
            AddFormFieldData("sMonthlyMinPmtIntro", dataLoan.sMonthlyMinPmtIntro_rep);
            AddFormFieldData("sInterestRateFirstAdj", dataLoan.sInterestRateFirstAdj_rep);
            AddFormFieldData("sMonthlyMaxPmtFirstAdj", dataLoan.sMonthlyMaxPmtFirstAdj_rep);
            AddFormFieldData("sMonthlyMinPmtFirstAdj", dataLoan.sMonthlyMinPmtFirstAdj_rep);
            AddFormFieldData("sInterestRateSecondAdj", dataLoan.sInterestRateSecondAdj_rep);
            AddFormFieldData("sMonthlyMaxPmtSecondAdj", dataLoan.sMonthlyMaxPmtSecondAdj_rep);
            AddFormFieldData("sMonthlyMinPmtSecondAdj", dataLoan.sMonthlyMinPmtSecondAdj_rep);
            AddFormFieldData("sMonthlyPaymentSecondAdjStartD", dataLoan.sMonthlyPaymentSecondAdjStartD_rep);
            AddFormFieldData("sInterestRateMaxEver", dataLoan.sInterestRateMaxEver_rep);
            AddFormFieldData("sMonthlyMaxPmtMaxEver", dataLoan.sMonthlyMaxPmtMaxEver_rep);
            AddFormFieldData("sMonthlyMinPmtMaxEver", dataLoan.sMonthlyMinPmtMaxEver_rep);
            AddFormFieldData("sAdditionalBalanceMaxEverD", dataLoan.sAdditionalBalanceMaxEverD_rep);
            AddFormFieldData("sAdditionalBalanceMaxEver", dataLoan.sAdditionalBalanceMaxEver_rep);
            AddFormFieldData("sDuePeriod", dataLoan.sDuePeriod);
            AddFormFieldData("sTILBalloonPmtMsg", dataLoan.sTILBalloonPmtMsg);
            AddFormFieldData("sRAdjLifeCapR", dataLoan.sRAdjLifeCapR_rep);
            AddFormFieldData("sRLifeCapR", dataLoan.sRLifeCapR_rep);
        }
	}
}
