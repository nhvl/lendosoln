///
/// Author; David Dao
/// 
namespace LendersOffice.Pdf
{
    using System;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.HttpModule;
    using LendersOffice.Pdf.Async;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Handles requests for PDF downloads.
    /// </summary>
    public class PDFHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        /// <summary>
        /// Represents the name of the PDF returned for batch print requests.
        /// </summary>
        private const string BatchPdfName = "LOPrint";

        /// <summary>
        /// Gets a value indicating whether the handler is reuseable.
        /// </summary>
        public bool IsReusable => true;

        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <param name="context">
        /// The context for the request.
        /// </param>
        public void ProcessRequest(HttpContext context)
        {
            PerformanceMonitorItem item = PerformanceMonitorItem.Current;
            if (item != null)
            {
                item.IsEnabledPageSizeWarning = false; // 4/2/2008 dd - Disable pagesize warning for PDF.
            }

            var requestedOperation = context.Request["cmd"];
            if (requestedOperation == "submit")
            {
                this.HandleSubmitRequest(context);
            }
            else if (requestedOperation == "poll")
            {
                this.HandlePollRequest(context);
            }
            else if (requestedOperation == "retrieve")
            {
                this.HandleRetrieveRequest(context);
            }
            else
            {
                this.SendError(context.Response);
            }
        }

        /// <summary>
        /// Handles a request to submit a PDF job.
        /// </summary>
        /// <param name="context">
        /// The context for the request.
        /// </param>
        private void HandleSubmitRequest(HttpContext context)
        {
            var jobTypeStr = context.Request["JobType"];

            LendersOffice.ObjLib.BackgroundJobs.PdfGenerationJobType jobType;
            if (string.IsNullOrEmpty(jobTypeStr) || !jobTypeStr.TryParseDefine(out jobType, ignoreCase: true))
            {
                this.SendError(context.Response);
                return;
            }

            switch (jobType)
            {
                case ObjLib.BackgroundJobs.PdfGenerationJobType.SinglePdf:
                    this.SubmitSinglePdfJob(context);
                    break;
                case ObjLib.BackgroundJobs.PdfGenerationJobType.BatchPdf:
                    this.SubmitBatchPdfJob(context);
                    break;
                case ObjLib.BackgroundJobs.PdfGenerationJobType.PaymentStatementPdf:
                    this.SubmitPaymentStatementPdfJob(context);
                    break;
                default:
                    throw new UnhandledEnumException(jobType);
            }
        }

        /// <summary>
        /// Submits a single-PDF job.
        /// </summary>
        /// <param name="context">
        /// The context with the request.
        /// </param>
        private void SubmitSinglePdfJob(HttpContext context)
        {
            var pdfName = this.GetPdfNameFromUrl(context);

            var repository = new PdfGeneratorRepository();
            if (repository.HasPdfGenerator(pdfName))
            {
                var pdfArguments = context.Request.QueryString;
                var password = AutoExpiredTextCache.GetFromCache("PrintPassword" + RequestHelper.GetSafeQueryString("passwordid"));

                if (this.IsSynchronousGenerationRequest(context))
                {
                    var generationHelper = new AsyncPdfGenerationHelper();
                    var result = generationHelper.GenerateSinglePdf(pdfName, pdfArguments, password);
                    this.SendSynchronousPdfGenerationResult(context, $"{pdfName}.pdf", result);
                }
                else
                {
                    var jobHelper = new AsyncPdfJobHelper();
                    var jobId = jobHelper.CreateSinglePdfJob(context.User as AbstractUserPrincipal, pdfName, pdfArguments, password);

                    context.Response.Write(SerializationHelper.JsonNetAnonymousSerialize(new
                    {
                        JobId = jobId
                    }));
                }
            }
            else
            {
                this.SendError(context.Response);
            }
        }

        /// <summary>
        /// Submits a batch PDF job.
        /// </summary>
        /// <param name="context">
        /// The context for the request.
        /// </param>
        private void SubmitBatchPdfJob(HttpContext context)
        {
            var batchId = context.Request["id"];

            var xml = AutoExpiredTextCache.GetFromCache("Print" + batchId);
            if (xml == null)
            {
                this.SendError(context.Response);
                return;
            }

            var printList = (LendersOffice.Pdf.Async.PdfPrintList)SerializationHelper.XmlDeserialize(xml, typeof(LendersOffice.Pdf.Async.PdfPrintList));
            var password = AutoExpiredTextCache.GetFromCache("PrintPassword" + batchId);

            if (this.IsSynchronousGenerationRequest(context))
            {
                var generationHelper = new AsyncPdfGenerationHelper();
                var result = generationHelper.GenerateBatchPdf(printList, password);
                this.SendSynchronousPdfGenerationResult(context, $"{BatchPdfName}.pdf", result);
            }
            else
            {
                var jobHelper = new AsyncPdfJobHelper();
                var jobId = jobHelper.CreateBatchPdfJob(PrincipalFactory.CurrentPrincipal, printList, password);

                context.Response.Write(SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    JobId = jobId
                }));
            }
        }

        /// <summary>
        /// Submits a payment statement PDF job.
        /// </summary>
        /// <param name="context">
        /// The context with the request.
        /// </param>
        private void SubmitPaymentStatementPdfJob(HttpContext context)
        {
            PaymentStatementPdfOptions options = null;
            try
            {
                // Get PDF Options from request.
                string optionsJson = null;
                context.Request.InputStream.Position = 0;   // Need to reset stream position in order to read json.
                using (var inputStream = new System.IO.StreamReader(context.Request.InputStream))
                {
                    optionsJson = HttpUtility.UrlDecode(inputStream.ReadToEnd());
                }

                options = SerializationHelper.JsonNetDeserialize<PaymentStatementPdfOptions>(optionsJson);
            }
            catch (Exception e)
            {
                Tools.LogError("[PDFHandler::SubmitPaymentStatementPdfJob] Could not deserialize options.", e);
                this.SendError(context.Response);
                return;
            }
            
            if (this.IsSynchronousGenerationRequest(context))
            {
                var generationHelper = new AsyncPdfGenerationHelper();
                var result = generationHelper.GeneratePaymentStatementPdf(options);

                switch (result.Status)
                {
                    case AsyncPdfGenerationStatus.Complete:
                        context.Response.Write(SerializationHelper.JsonNetAnonymousSerialize(new { result.FileDbKey }));
                        break;
                    case AsyncPdfGenerationStatus.Processing:
                    case AsyncPdfGenerationStatus.Error:
                    default:
                        throw new UnhandledEnumException(result.Status);
                }
            }
            else
            {
                var jobHelper = new AsyncPdfJobHelper();
                var jobId = jobHelper.CreatePaymentStatementPdf(context.User as AbstractUserPrincipal, options);

                context.Response.Write(SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    JobId = jobId
                }));
            }
        }

        /// <summary>
        /// Handles a PDF job poll request.
        /// </summary>
        /// <param name="context">
        /// The context for the request.
        /// </param>
        private void HandlePollRequest(HttpContext context)
        {
            Guid jobId;
            if (!Guid.TryParse(context.Request["JobId"], out jobId))
            {
                this.SendError(context.Response);
                return;
            }

            var helper = new AsyncPdfJobHelper();
            var result = helper.GetJobStatus(jobId);

            bool hasError = false, isComplete = false;
            Guid? fileDbKey = null;

            switch (result.Status)
            {
                case AsyncPdfGenerationStatus.Processing:
                    break;
                case AsyncPdfGenerationStatus.Complete:
                    isComplete = true;
                    fileDbKey = result.FileDbKey.Value;
                    break;
                case AsyncPdfGenerationStatus.Error:
                    hasError = true;
                    break;
                default:
                    throw new UnhandledEnumException(result.Status);
            }

            context.Response.Write(SerializationHelper.JsonNetAnonymousSerialize(new
            {
                HasError = hasError,
                IsComplete = isComplete,
                FileDbKey = fileDbKey
            }));
        }

        /// <summary>
        /// Handles a request to retrieve a PDF
        /// once a job is complete.
        /// </summary>
        /// <param name="context">
        /// The context for the request.
        /// </param>
        private void HandleRetrieveRequest(HttpContext context)
        {
            Guid fileDbKey;
            if (!Guid.TryParse(context.Request["FileDbKey"], out fileDbKey))
            {
                this.SendError(context.Response);
                return;
            }

            var jobTypeStr = context.Request["JobType"];

            ObjLib.BackgroundJobs.PdfGenerationJobType jobType;
            if (string.IsNullOrEmpty(jobTypeStr) || !jobTypeStr.TryParseDefine(out jobType, ignoreCase: true))
            {
                this.SendError(context.Response);
                return;
            }

            var filePath = FileDBTools.CreateCopy(E_FileDB.Normal, fileDbKey.ToString());
            var pdfName = jobType == ObjLib.BackgroundJobs.PdfGenerationJobType.SinglePdf ? this.GetPdfNameFromUrl(context) : BatchPdfName;
            RequestHelper.SendFileToClient(context, filePath, "application/pdf", pdfName + ".pdf");

            // Cleanup temp file.
            Drivers.FileSystem.FileOperationHelper.Delete(LocalFilePath.Create(filePath).Value);
        }

        /// <summary>
        /// Gets the name of the PDF from the URL of the request
        /// for single-PDF downloads.
        /// </summary>
        /// <param name="context">
        /// The context with the request.
        /// </param>
        /// <returns>
        /// The PDF name.
        /// </returns>
        private string GetPdfNameFromUrl(HttpContext context)
        {
            // path = /LendersofficeApp/pdf/1003.aspx.
            HttpRequest request = context.Request;
            int start = request.Path.LastIndexOf("/");
            int end = request.Path.IndexOf(".aspx", start);
            return request.Path.Substring(start + 1, end - start - 1).ToLower();
        }

        /// <summary>
        /// Determines whether the PDF generation request should
        /// be completed synchronously.
        /// </summary>
        /// <param name="context">
        /// The context for the request.
        /// </param>
        /// <returns>
        /// True if the request should be completed synchronously,
        /// false otherwise.
        /// </returns>
        private bool IsSynchronousGenerationRequest(HttpContext context)
        {
            var queryStringParameter = context.Request.QueryString[ConstApp.SynchronousPdfFallbackQueryStringParameterName];
            return queryStringParameter.ToNullableBool().GetValueOrDefault(false);
        }

        /// <summary>
        /// Sends a synchronous PDF generation result back to the client.
        /// </summary>
        /// <param name="context">
        /// The context for the request.
        /// </param>
        /// <param name="filename">
        /// The name of the file to send.
        /// </param>
        /// <param name="result">
        /// The generation result.
        /// </param>
        private void SendSynchronousPdfGenerationResult(HttpContext context, string filename, AsyncPdfGenerationResult result)
        {
            switch (result.Status)
            {
                case AsyncPdfGenerationStatus.Complete:
                    var copyLocation = FileDBTools.CreateCopy(E_FileDB.Normal, result.FileDbKey.ToString());
                    RequestHelper.SendFileToClient(context, copyLocation, "application/pdf", filename);
                    break;
                case AsyncPdfGenerationStatus.Processing:
                case AsyncPdfGenerationStatus.Error:
                default:
                    throw new UnhandledEnumException(result.Status);
            }
        }

        /// <summary>
        /// Sends an error response to the client.
        /// </summary>
        /// <param name="response">
        /// The response to write the error value.
        /// </param>
        private void SendError(HttpResponse response)
        {
            response.Write(SerializationHelper.JsonNetAnonymousSerialize(new
            {
                HasError = true
            }));
        }
	}
}
