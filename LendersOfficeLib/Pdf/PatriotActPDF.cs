namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Common;

    public class CPatriotActPDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "PatriotAct_WithSig.pdf"; }
        }

        public override string Description 
        {
            get { return "Patriot Act Information Disclosure"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Disclosure/PatriotActDisclosure.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBDob", dataApp.aBDob_rep);
            string addr = dataApp.aBAddr;
            if (addr.TrimWhitespaceAndBOM() != "") addr += ",";
            addr += " " + Tools.CombineCityStateZip(dataApp.aBCity, dataApp.aBState, dataApp.aBZip);

            AddFormFieldData("aBPresentAddress", addr);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aCDob", dataApp.aCDob_rep);

            addr = dataApp.aCAddr;
            if (addr.TrimWhitespaceAndBOM() != "") addr += ",";
            addr += " " + Tools.CombineCityStateZip(dataApp.aCCity, dataApp.aCState, dataApp.aCZip);

            AddFormFieldData("aCPresentAddress", addr);

            AddFormFieldData("aIdResolution", dataApp.aIdResolution);
            var prepInfo = dataLoan.GetPreparerOfForm(E_PreparerFormT.PatriotActDisclosure,
                                                      E_ReturnOptionIfNotExist.CreateNew);

            AddFormFieldData("CompletedBy", prepInfo.PreparerName);
            AddFormFieldData("CompletedDate", prepInfo.PrepareDate_rep);

            string borrPdfPrimarySuffix = "_" + Enum.GetName(typeof(E_IdType), dataApp.aBIdT);
            string coborrPdfPrimarySuffix = "_" + Enum.GetName(typeof(E_IdType), dataApp.aCIdT);

            string borrPdfSecondarySuffix = "_" + Enum.GetName(typeof(E_IdType), dataApp.aB2ndIdT);
            string coborrPdfSecondarySuffix = "_" + Enum.GetName(typeof(E_IdType), dataApp.aC2ndIdT);

            AddFormFieldData("aBIdT", dataApp.aBIdT);
            AddFormFieldData("aCIdT", dataApp.aCIdT);

            //Since nothing happens if we try to fill out a field that doesn't exist,
            //try all the fields.

            if (dataApp.aBIdT != E_IdType.Blank)
            {
                AddFormFieldData("aBIdState" + borrPdfPrimarySuffix, dataApp.aBIdState);
                AddFormFieldData("aBIdNumber" + borrPdfPrimarySuffix, dataApp.aBIdNumber);
                AddFormFieldData("aBIdIssueD" + borrPdfPrimarySuffix, dataApp.aBIdIssueD_rep);
                AddFormFieldData("aBIdExpireD" + borrPdfPrimarySuffix, dataApp.aBIdExpireD_rep);
                AddFormFieldData("aBIdCountry" + borrPdfPrimarySuffix, dataApp.aBIdCountry);
                AddFormFieldData("aBIdGovBranch" + borrPdfPrimarySuffix, dataApp.aBIdGovBranch);
                AddFormFieldData("aBIdOther" + borrPdfPrimarySuffix, dataApp.aBIdOther);
            }

            if (dataApp.aB2ndIdT != E_IdType.Blank)
            {
                AddFormFieldData("aBIdState" + borrPdfSecondarySuffix, dataApp.aB2ndIdState);
                AddFormFieldData("aBIdNumber" + borrPdfSecondarySuffix, dataApp.aB2ndIdNumber);
                AddFormFieldData("aBIdIssueD" + borrPdfSecondarySuffix, dataApp.aB2ndIdIssueD_rep);
                AddFormFieldData("aBIdExpireD" + borrPdfSecondarySuffix, dataApp.aB2ndIdExpireD_rep);
                AddFormFieldData("aBIdCountry" + borrPdfSecondarySuffix, dataApp.aB2ndIdCountry);
                AddFormFieldData("aBIdGovBranch" + borrPdfSecondarySuffix, dataApp.aB2ndIdGovBranch);
                AddFormFieldData("aBIdOther" + borrPdfSecondarySuffix, dataApp.aB2ndIdOther);
            }

            if (dataApp.aCIdT != E_IdType.Blank)
            {
                AddFormFieldData("aCIdState" + coborrPdfPrimarySuffix, dataApp.aCIdState);
                AddFormFieldData("aCIdNumber" + coborrPdfPrimarySuffix, dataApp.aCIdNumber);
                AddFormFieldData("aCIdIssueD" + coborrPdfPrimarySuffix, dataApp.aCIdIssueD_rep);
                AddFormFieldData("aCIdExpireD" + coborrPdfPrimarySuffix, dataApp.aCIdExpireD_rep);
                AddFormFieldData("aCIdCountry" + coborrPdfPrimarySuffix, dataApp.aCIdCountry);
                AddFormFieldData("aCIdGovBranch" + coborrPdfPrimarySuffix, dataApp.aCIdGovBranch);
                AddFormFieldData("aCIdOther" + coborrPdfPrimarySuffix, dataApp.aCIdOther);
            }

            if (dataApp.aC2ndIdT != E_IdType.Blank)
            {
                AddFormFieldData("aCIdState" + coborrPdfSecondarySuffix, dataApp.aC2ndIdState);
                AddFormFieldData("aCIdNumber" + coborrPdfSecondarySuffix, dataApp.aC2ndIdNumber);
                AddFormFieldData("aCIdIssueD" + coborrPdfSecondarySuffix, dataApp.aC2ndIdIssueD_rep);
                AddFormFieldData("aCIdExpireD" + coborrPdfSecondarySuffix, dataApp.aC2ndIdExpireD_rep);
                AddFormFieldData("aCIdCountry" + coborrPdfSecondarySuffix, dataApp.aC2ndIdCountry);
                AddFormFieldData("aCIdGovBranch" + coborrPdfSecondarySuffix, dataApp.aC2ndIdGovBranch);
                AddFormFieldData("aCIdOther" + coborrPdfSecondarySuffix, dataApp.aC2ndIdOther);
            }
        }
	}

    public class CPatriotActNoSigPDF : CPatriotActPDF
    {
        public override string PdfFile
        {
            get { return "PatriotAct_NoSig.pdf"; }
        }

        public override string Description
        {
            get { return "Patriot Act Information Disclosure - No Signature"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Disclosure/PatriotActDisclosure.aspx"; }
        }
    }
}
