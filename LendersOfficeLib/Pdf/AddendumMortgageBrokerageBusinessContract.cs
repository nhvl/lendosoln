using System;
using System.Collections.Specialized;
using System.Data;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CAddendumMortgageBrokerageBusinessContractPDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "AddendumMortgageBrokerageBusinessContract.pdf"; }
        }

        protected override  void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
    }
}
