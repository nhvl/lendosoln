﻿namespace LendersOffice.Pdf
{
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public class CAntiSteeringSafeHarborDisclosureFixedPDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "AntiSteeringSafeHarborDisclosureFixed.pdf"; }
        }

        public override string Description
        {
            get { return "Anti-Steering Safe Harbor Disclosure, Fixed Rate Loans"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Disclosure/SafeHarborDisclosureFixed.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields SafeHarborFixed = dataLoan.GetPreparerOfForm(E_PreparerFormT.SafeHarborFixed, E_ReturnOptionIfNotExist.CreateNew);
            AddFormFieldData("PreparerName", SafeHarborFixed.PreparerName);
            AddFormFieldData("LoanOriginatorIdentifier", SafeHarborFixed.LoanOriginatorIdentifier);
            AddFormFieldData("LicenseNumOfAgent", SafeHarborFixed.LicenseNumOfAgent);
            AddFormFieldData("Phone", SafeHarborFixed.Phone);
            AddFormFieldData("CompanyName", SafeHarborFixed.CompanyName);
            AddFormFieldData("CompanyLoanOriginatorIdentifier", SafeHarborFixed.CompanyLoanOriginatorIdentifier);
            AddFormFieldData("LicenseNumOfCompany", SafeHarborFixed.LicenseNumOfCompany);
            AddFormFieldData("PhoneOfCompany", SafeHarborFixed.PhoneOfCompany);
            AddFormFieldData("FaxOfCompany", SafeHarborFixed.FaxOfCompany);
            AddFormFieldData("StreetAddr_Multiline", SafeHarborFixed.StreetAddrMultiLines);

            AddFormFieldData("aBNm_aCNm", dataApp.aBNm_aCNm);
            AddFormFieldData("aBAddr_MultiLine", dataApp.aBAddr_MultiLine);

            if (dataLoan.sIncludeSafeHarborFixedRecLoan)
            {
                dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
                AddFormFieldData("sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedRecLoan_rep);
                AddFormFieldData("sEquityCalcSafeHarborFixedRecLoan", dataLoan.sEquityCalcSafeHarborFixedRecLoan_rep);
                AddFormFieldData("sTransNetCashSafeHarborFixedRecLoan", dataLoan.sTransNetCashSafeHarborFixedRecLoan_rep);
                dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

                AddFormFieldData("sDueSafeHarborFixedRecLoan", dataLoan.sDueSafeHarborFixedRecLoan_rep);
                AddFormFieldData("sFinalLAmtSafeHarborFixedRecLoan", dataLoan.sFinalLAmtSafeHarborFixedRecLoan_rep);
                AddFormFieldData("sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan", dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedRecLoan_rep);
                AddFormFieldData("sLpTemplateNmSafeHarborFixedRecLoan", dataLoan.sLpTemplateNmSafeHarborFixedRecLoan);
                AddFormFieldData("sNoteIRSafeHarborFixedRecLoan", dataLoan.sNoteIRSafeHarborFixedRecLoan_rep);
                AddFormFieldData("sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan", dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedRecLoan_rep);
                AddFormFieldData("sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedRecLoan_rep);
            }

            if (dataLoan.sIncludeSafeHarborFixedWithRiskyFeatures)
            {
                dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
                AddFormFieldData("sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithRiskyFeatures_rep);
                AddFormFieldData("sEquityCalcSafeHarborFixedWithRiskyFeatures", dataLoan.sEquityCalcSafeHarborFixedWithRiskyFeatures_rep);
                AddFormFieldData("sTransNetCashSafeHarborFixedWithRiskyFeatures", dataLoan.sTransNetCashSafeHarborFixedWithRiskyFeatures_rep);
                dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

                AddFormFieldData("sDueSafeHarborFixedWithRiskyFeatures", dataLoan.sDueSafeHarborFixedWithRiskyFeatures_rep);
                AddFormFieldData("sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures", dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedWithRiskyFeatures_rep);
                AddFormFieldData("sFinalLAmtSafeHarborFixedWithRiskyFeatures", dataLoan.sFinalLAmtSafeHarborFixedWithRiskyFeatures_rep);
                AddFormFieldData("sLpTemplateNmSafeHarborFixedWithRiskyFeatures", dataLoan.sLpTemplateNmSafeHarborFixedWithRiskyFeatures);
                AddFormFieldData("sNoteIRSafeHarborFixedWithRiskyFeatures", dataLoan.sNoteIRSafeHarborFixedWithRiskyFeatures_rep);
                AddFormFieldData("sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures", dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedWithRiskyFeatures_rep);
                AddFormFieldData("sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithRiskyFeatures_rep);
            }

            AddFormFieldData("sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedWithoutRiskyFeatures_rep);
            AddFormFieldData("sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost", dataLoan.sTotalDiscountPointOriginationFeeSafeHarborFixedLowestCost_rep);

            dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
            AddFormFieldData("sEquityCalcSafeHarborFixedLowestCost", dataLoan.sEquityCalcSafeHarborFixedLowestCost_rep);
            AddFormFieldData("sEquityCalcSafeHarborFixedWithoutRiskyFeatures", dataLoan.sEquityCalcSafeHarborFixedWithoutRiskyFeatures_rep);
            AddFormFieldData("sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedLowestCost_rep);
            AddFormFieldData("sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures", dataLoan.sGfeTotalEstimateSettlementChargeSafeHarborFixedWithoutRiskyFeatures_rep);
            AddFormFieldData("sTransNetCashSafeHarborFixedLowestCost", dataLoan.sTransNetCashSafeHarborFixedLowestCost_rep);
            AddFormFieldData("sTransNetCashSafeHarborFixedWithoutRiskyFeatures", dataLoan.sTransNetCashSafeHarborFixedWithoutRiskyFeatures_rep);
            dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

            AddFormFieldData("sDueSafeHarborFixedLowestCost", dataLoan.sDueSafeHarborFixedLowestCost_rep);
            AddFormFieldData("sDueSafeHarborFixedWithoutRiskyFeatures", dataLoan.sDueSafeHarborFixedWithoutRiskyFeatures_rep);
            AddFormFieldData("sFinalLAmtSafeHarborFixedLowestCost", dataLoan.sFinalLAmtSafeHarborFixedLowestCost_rep);
            AddFormFieldData("sFinalLAmtSafeHarborFixedWithoutRiskyFeatures", dataLoan.sFinalLAmtSafeHarborFixedWithoutRiskyFeatures_rep);
            AddFormFieldData("sGfeProThisMPmtAndMInsSafeHarborFixedLowestCost", dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedLowestCost_rep);
            AddFormFieldData("sGfeProThisMPmtAndMInsSafeHarborFixedWithoutRiskyFeatures", dataLoan.sGfeProThisMPmtAndMInsSafeHarborFixedWithoutRiskyFeatures_rep);
            AddFormFieldData("sLpTemplateNmSafeHarborFixedLowestCost", dataLoan.sLpTemplateNmSafeHarborFixedLowestCost);
            AddFormFieldData("sLpTemplateNmSafeHarborFixedWithoutRiskyFeatures", dataLoan.sLpTemplateNmSafeHarborFixedWithoutRiskyFeatures);
            AddFormFieldData("sNoteIRSafeHarborFixedLowestCost", dataLoan.sNoteIRSafeHarborFixedLowestCost_rep);
            AddFormFieldData("sNoteIRSafeHarborFixedWithoutRiskyFeatures", dataLoan.sNoteIRSafeHarborFixedWithoutRiskyFeatures_rep);
            AddFormFieldData("sOriginatorCompensationTotalAmountSafeHarborFixedLowestCost", dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedLowestCost_rep);
            AddFormFieldData("sOriginatorCompensationTotalAmountSafeHarborFixedWithoutRiskyFeatures", dataLoan.sOriginatorCompensationTotalAmountSafeHarborFixedWithoutRiskyFeatures_rep);
            AddFormFieldData("sSafeHarborFixedBalloonPmntIn7Yrs", dataLoan.sSafeHarborFixedBalloonPmntIn7Yrs);
            AddFormFieldData("sSafeHarborFixedDemandFeat", dataLoan.sSafeHarborFixedDemandFeat);
            AddFormFieldData("sSafeHarborFixedIntOnlyPmts", dataLoan.sSafeHarborFixedIntOnlyPmts);
            AddFormFieldData("sSafeHarborFixedNegAmort", dataLoan.sSafeHarborFixedNegAmort);
            AddFormFieldData("sSafeHarborFixedOurRecLowestCosts", dataLoan.sSafeHarborFixedOurRecLowestCosts);
            AddFormFieldData("sSafeHarborFixedOurRecOther", dataLoan.sSafeHarborFixedOurRecOther);
            AddFormFieldData("sSafeHarborFixedOurRecText", dataLoan.sSafeHarborFixedOurRecText);
            AddFormFieldData("sSafeHarborFixedOurRecWithoutRiskyFeatures", dataLoan.sSafeHarborFixedOurRecWithoutRiskyFeatures);
            AddFormFieldData("sSafeHarborFixedPrepayPenalty", dataLoan.sSafeHarborFixedPrepayPenalty);
            AddFormFieldData("sSafeHarborFixedSharedAppreciation", dataLoan.sSafeHarborFixedSharedAppreciation);
            AddFormFieldData("sSafeHarborFixedSharedEquity", dataLoan.sSafeHarborFixedSharedEquity);
            AddFormFieldData("sSafeHarborFixedOurRecLowestRate", dataLoan.sSafeHarborFixedOurRecLowestRate);
        }
    }
}