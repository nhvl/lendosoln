using System;
using System.Collections;
using System.Collections.Specialized;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{

	public class CHUD_92051_1PDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "HUD-92051_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
        }
	}
    public class CHUD_92051_2PDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "HUD-92051_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
        }
    }
    public class CHUD_92051PDF : AbstractBatchPDF
    {
        public override string Description 
        {
            get { return "FHA Compliance Inspection Report (HUD-92051)"; }
        }
		public override bool OnlyPrintPrimary
		{
			get { return true; }
		}
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CHUD_92051_1PDF(),
                                                 new CHUD_92051_2PDF()
                                             };
            }
        }

    }

}
