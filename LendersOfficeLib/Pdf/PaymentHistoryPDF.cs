﻿namespace LendersOffice.Pdf
{
    using System.Threading;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.PdfGenerator;
    using LendersOffice.Security;

    public class CPaymentHistoryPDF : AbstractXsltPdf
    {
        protected override void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp)
        {

            writer.WriteStartElement("main_content");

            WriteLoanInformation(writer, dataLoan);

            WriteApplicationInformation(writer, dataApp);

            writer.WriteEndElement(); // </main_content>
        }
        public override bool HasPrintPermission
        {
            get
            {
                AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

                return base.HasPrintPermission && principal.HasPermission(Permission.AllowAccountantRead) && principal.HasPermission(Permission.AllowCloserRead);
            }
        }
        private void WriteLoanInformation(XmlWriter writer, CPageData dataLoan)
        {
            writer.WriteStartElement("loan");

            // Broker information
            BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            BrokerDB broker = BrokerDB.RetrieveById(brokerUser.BrokerId);
            writer.WriteStartElement("Broker");
            writer.WriteElementString("Name", broker.Name);
            writer.WriteElementString("Address", broker.Address.StreetAddress);
            writer.WriteElementString("City", broker.Address.City);
            writer.WriteElementString("State", broker.Address.State);
            writer.WriteElementString("Zipcode", broker.Address.Zipcode.TrimWhitespaceAndBOM());
            writer.WriteElementString("Phone", broker.Phone);
            writer.WriteElementString("Fax", broker.Fax);
            writer.WriteEndElement(); // </Broker>
            
            writer.WriteElementString("sLNm", dataLoan.sLNm);

            writer.WriteElementString("sTodayDate", dataLoan.sTodayDate);
            writer.WriteElementString("sClosedD_rep", dataLoan.sClosedD_rep);
            writer.WriteElementString("sFundD_rep", dataLoan.sFundD_rep);

            // Property
            writer.WriteElementString("sSpAddr", dataLoan.sSpAddr);
            writer.WriteElementString("sSpCity", dataLoan.sSpCity);
            writer.WriteElementString("sSpState", dataLoan.sSpState);
            writer.WriteElementString("sSpZip", dataLoan.sSpZip);
            
            writer.WriteElementString("sSchedDueD1_rep", dataLoan.sSchedDueD1_rep); // First payment due

            // Loan 
            writer.WriteElementString("sFinalLAmt_rep", dataLoan.sFinalLAmt_rep); // loan amount

            writer.WriteElementString("sServicingTotalDueFunds_Principal_rep_neg", dataLoan.sServicingTotalDueFunds_Principal_neg_rep); // unpaid principal balance
            
            writer.WriteElementString("sNoteIR_rep", dataLoan.sNoteIR_rep);

            writer.WriteElementString("sServicingCollectedAndDisbursedDueFunds_Interest_rep", dataLoan.sServicingCollectedAndDisbursedDueFunds_Interest_rep); // servicing interest received
            
            writer.WriteElementString("sTerm_rep", dataLoan.sTerm_rep);
            writer.WriteElementString("sDue_rep", dataLoan.sDue_rep);
            writer.WriteElementString("sServicingCurrentTotalFunds_Escrow_rep", dataLoan.sServicingCurrentTotalFunds_Escrow_rep); // escrow account balance
            writer.WriteElementString("sInitialDeposit_Escrow_rep", dataLoan.sInitialDeposit_Escrow_rep); // closing escrow balance

            WritePaymentTables(writer, dataLoan);

            writer.WriteEndElement(); //</loan>
        }

        private void WritePaymentTables(XmlWriter writer, CPageData dataLoan)
        {
            writer.WriteStartElement("Payments");
            foreach (var payment in dataLoan.sServicingPayments_rep)
            {
                WritePayment(writer, payment); // /loan/Payments/
            }
            foreach (var total in dataLoan.sServicingTotalAll)
            {
                WriteTotal(writer, total);
            }
            writer.WriteEndElement();

        }

        private void WritePayment(XmlWriter writer, CServicingPaymentFields payment)
        {
            writer.WriteStartElement("Payment");
            writer.WriteAttributeString("ServicingTransactionT", payment.ServicingTransactionT);
            
            writer.WriteElementString("DueAmt_rep", payment.DueAmt_rep);
            writer.WriteElementString("DueD_rep", payment.DueD_rep);
            writer.WriteElementString("Escrow_rep", payment.Escrow_rep);
            writer.WriteElementString("Interest_rep", payment.Interest_rep);
            writer.WriteElementString("LateFee_rep", payment.LateFee_rep);
            writer.WriteElementString("Notes", payment.Notes);
            writer.WriteElementString("Other_rep", payment.Other_rep);
            writer.WriteElementString("PaymentAmt_rep", payment.PaymentAmt_rep);
            writer.WriteElementString("PaymentD_rep", payment.PaymentD_rep);
            writer.WriteElementString("PaymentReceivedD_rep", payment.PaymentReceivedD_rep);
            writer.WriteElementString("Principal_rep", payment.Principal_rep);
            writer.WriteElementString("Payee", payment.Payee);
            writer.WriteEndElement(); //</Payment>
        }

        private void WriteTotal(XmlWriter writer, CServicingPaymentFields total)
        {
            writer.WriteStartElement("Total");
            writer.WriteAttributeString("ServicingTransactionT", total.ServicingTransactionT);
            
            string rowCountText = string.Format("{0} {1}{2}",
                total.RowNum,
                total.Notes,
                (total.RowNum == 1) ? "" : "s");
            writer.WriteElementString("RowCountText", rowCountText);

            writer.WriteElementString("DueAmt_rep", total.DueAmt_rep);
            writer.WriteElementString("DueD_rep", total.DueD_rep);
            writer.WriteElementString("Escrow_rep", total.Escrow_rep);
            writer.WriteElementString("Interest_rep", total.Interest_rep);
            writer.WriteElementString("LateFee_rep", total.LateFee_rep);
            writer.WriteElementString("Notes", total.Notes);
            writer.WriteElementString("Other_rep", total.Other_rep);
            writer.WriteElementString("PaymentAmt_rep", total.PaymentAmt_rep);
            writer.WriteElementString("PaymentD_rep", total.PaymentD_rep);
            writer.WriteElementString("Principal_rep", total.Principal_rep);
            writer.WriteEndElement(); //</Total>
        }

        private void WriteApplicationInformation(XmlWriter writer, CAppData dataApp)
        {
            writer.WriteStartElement("app");
            writer.WriteElementString("aBNm", dataApp.aBNm);
            writer.WriteElementString("aCNm", dataApp.aCNm);
            writer.WriteEndElement(); //</app>
        }

        public override string Description
        {
            get { return "Payment History Letter (with notes)"; }
        }

        public override PDFPageSize PageSize
        {
            get { return PDFPageSize.Letter; }
        }

    }
}
