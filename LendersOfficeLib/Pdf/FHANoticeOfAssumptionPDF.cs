﻿using System;
using System.Collections.Specialized;
using System.Data;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{

    public class CFHANoticeOfAssumptionPDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "FHANoticeOfAssumption.pdf"; }
        }

        public override string Description 
        {
            get { return "FHA Assumption Notice"; }
        }
 
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
        }
    }
}