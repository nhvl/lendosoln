﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Pdf
{
    public class CPmlApprovalPDF : AbstractFileDBBasedPDF
    {
        protected override string FileDbKey
        {
            get { return sLId.ToString("N") + "_approval.pdf"; }
        }

        public override string Description
        {
            get { return "Approval Certificate"; }
        }
    }
}
