using System;
using System.Collections;
using System.Collections.Specialized;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CHUD_92561PDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "HUD-92561.pdf"; }
        }
        public override string Description 
        {
            get { return "FHA Hotel and Transient Use of Property (HUD-92561)"; }
        }
        

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            
            string name = dataApp.aBNm;
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "")
                name += " & " + dataApp.aCNm;

            AddFormFieldData("BorrowerInfo", Tools.FormatAddress(name, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("PropertyInfo", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
        }
    }
}
