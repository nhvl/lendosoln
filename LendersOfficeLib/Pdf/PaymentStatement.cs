﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using Common;
    using DataAccess;
    using ExpertPdf.HtmlToPdf;
    using ObjLib.PDFGenerator;
    using PdfRasterizerLib;

    /// <summary>
    /// Class to support generating a compliant payment statement based on servicing data.
    /// </summary>
    public class PaymentStatement : AbstractXsltPdf, IPDFPrintItemGenerator
    {
        /// <summary>
        /// The options for pdf generation.
        /// </summary>
        private PaymentStatementPdfOptions options;

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentStatement"/> class.
        /// </summary>
        /// <param name="options">The pdf options.</param>
        public PaymentStatement(PaymentStatementPdfOptions options)
            : base()
        {
            this.options = options;
        }

        /// <summary>
        /// Gets the description of the pdf.
        /// </summary>
        /// <value>The description of the pdf.</value>
        public override string Description => "Mortgage Statement";

        /// <summary>
        /// Generates XML data for use in the XSLT transform.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        /// <param name="dataLoan">The loan file.</param>
        /// <param name="dataApp">The loan application.</param>
        protected override void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp)
        {
            var originalFormatTarget = dataLoan.GetFormatTarget();
            dataLoan.SetFormatTarget(FormatTarget.Webform);

            IEnumerable<Transaction> transactionsForThisStatement = this.GetTransactionsForThisStatement(dataLoan);
            Transaction primaryTransaction = transactionsForThisStatement.Last();
            DateTime statementDate = primaryTransaction.TransactionDate;

            writer.WriteStartElement("PaymentStatement");

            this.WriteOptionsElement(writer, dataLoan.m_convertLos);
            this.WritePaymentElement(writer, primaryTransaction, dataLoan.m_convertLos);
            this.WritePaymentStatementServicerElement(writer, dataLoan);
            this.WriteBorrowerMailingAddressElement(writer, dataApp);
            this.WriteTransactionActivityElement(writer, transactionsForThisStatement, dataLoan.m_convertLos);
            this.WritePastPaymentsBreakdownElement(writer, transactionsForThisStatement, statementDate, dataLoan.m_convertLos);

            writer.WriteElementString("sLNm", dataLoan.sLNm);
            writer.WriteElementString("sServicingUnpaidPrincipalBalance", dataLoan.sServicingUnpaidPrincipalBalance_rep);

            // This will be wrong if they have hit the first rate adjustment, but we do not intend to support
            // anything more than interim servicing.
            writer.WriteElementString("sNoteIR", dataLoan.sNoteIR_rep);
            writer.WriteElementString("sRAdj1stD", dataLoan.sRAdj1stD_rep);
            writer.WriteElementString("sHasPrepaymentPenalty", dataLoan.sHasPrepaymentPenalty ? "Yes" : "No");

            writer.WriteElementString("sPaymentStatementDelinquencyNotice", dataLoan.sPaymentStatementDelinquencyNotice);
            this.WriteRecentAccountHistory(writer, transactionsForThisStatement, primaryTransaction, dataLoan.m_convertLos);
            writer.WriteElementString("sPaymentStatementSuspenseAccountNotice", dataLoan.sPaymentStatementSuspenseAccountNotice);
            writer.WriteElementString("sPaymentStatementOtherNotices", dataLoan.sPaymentStatementOtherNotices);

            writer.WriteEndElement(); // PaymentStatement

            dataLoan.SetFormatTarget(originalFormatTarget);
        }

        /// <summary>
        /// Updates the PDF conversion options to not generate a footer.
        /// </summary>
        /// <param name="dataLoan">The loan file.</param>
        /// <param name="dataApp">The loan application.</param>
        /// <param name="options">The conversion options.</param>
        protected override void UpdatePdfConversionOptions(CPageData dataLoan, CAppData dataApp, PDFConversionOptions options)
        {
            options.ShowFooter = false;

            if (this.options.UseLegalPageSize)
            {
                options.PageSize = (int)PdfPageSize.Legal;
            }
        }

        /// <summary>
        /// Helper method for adding money amounts.
        /// </summary>
        /// <param name="moneyAmounts">The money amounts.</param>
        /// <returns>The sum of the given amounts.</returns>
        private static decimal SumMoney(params decimal[] moneyAmounts)
        {
            return Tools.SumMoney(moneyAmounts);
        }

        /// <summary>
        /// Gets an enumerable of transactions that are relevant for the statement.
        /// </summary>
        /// <remarks>
        /// This will exclude transactions that occur after the transaction the 
        /// statement is being generated for.
        /// </remarks>
        /// <param name="dataLoan">The loan file.</param>
        /// <returns>An enumerable of transactions that are relevant to the statement.</returns>
        private IEnumerable<Transaction> GetTransactionsForThisStatement(CPageData dataLoan)
        {
            if (this.options.PaymentIndex + 1 > this.DataLoan.sServicingPayments.Count)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Attempting to generate a payment statement for an invalid payment index.");
            }

            var transactions = this.DataLoan.sServicingPayments.Aggregate(
                new ServicingPaymentAggregateResult(),
                (result, servicingPayment) =>
                {
                    // The overdue calculation is very basic and will only handle
                    // the case where payments are made in full.
                    decimal existingOverdueAmount = result.Overdue;
                    decimal overdueFromPayment = 0;
                    if (servicingPayment.PaymentD == DateTime.MinValue)
                    {
                        overdueFromPayment = servicingPayment.DueAmt;
                    }

                    result.Overdue = SumMoney(existingOverdueAmount, overdueFromPayment);
                    result.Transactions.Add(new Transaction(servicingPayment, existingOverdueAmount, dataLoan.sProMIns));

                    return result;
                }).Transactions;

            return transactions.Take(this.options.PaymentIndex + 1);
        }

        /// <summary>
        /// Writes the pdf generation options to xml.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        /// <param name="convert">The converter.</param>
        private void WriteOptionsElement(XmlWriter writer, LosConvert convert)
        {
            writer.WriteStartElement("Options");
            writer.WriteElementString("IncludeDelinquencyNotice", this.options.IncludeDelinquencyNotice.ToString());
            writer.WriteElementString("IncludeSuspenseAccountNotice", this.options.IncludeSuspenseAccountNotice.ToString());
            writer.WriteElementString("IncludeOtherNotices", this.options.IncludeOtherNotices.ToString());
            writer.WriteElementString("PeriodStart", convert.ToDateTimeString(this.options.PeriodStart));
            writer.WriteElementString("PeriodEnd", convert.ToDateTimeString(this.options.PeriodEnd));
            writer.WriteElementString("PotentialLateFee", convert.ToMoneyString(this.options.PotentialLateFee, FormatDirection.ToRep));
            writer.WriteElementString("LateFeeDate", convert.ToDateTimeString(this.options.LateFeeDate));
            writer.WriteElementString("UseLegalSizePages", this.options.UseLegalPageSize.ToString());
            writer.WriteEndElement();
        }

        /// <summary>
        /// Writes the payment summary to xml.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="convert">The converter.</param>
        private void WritePaymentElement(XmlWriter writer, Transaction transaction, LosConvert convert)
        {
            var transactionRepper = new TransactionRepper(transaction, convert);

            writer.WriteStartElement("Payment");
            writer.WriteElementString("AmountToPrincipal", transactionRepper.AmountToPrincipal);
            writer.WriteElementString("AmountToInterest", transactionRepper.AmountToInterest);
            writer.WriteElementString("AmountToEscrowForMortgageInsurance", transactionRepper.AmountToEscrowForMortgageInsurance);
            writer.WriteElementString("AmountToEscrowForTaxesAndInsurance", transactionRepper.AmountToEscrowForTaxesAndInsurance);
            writer.WriteElementString("TotalRegularPayment", transactionRepper.TotalRegularPayment);
            writer.WriteElementString("AmountToLateFees", transactionRepper.AmountToLateFees);
            writer.WriteElementString("AmountToOtherCharges", transactionRepper.AmountToOtherCharges);
            writer.WriteElementString("AmountOverdue", transactionRepper.AmountOverdue);
            writer.WriteElementString("TotalAmountDue", transactionRepper.TotalAmountDue);
            writer.WriteElementString("DueDate", transactionRepper.DueDate);
            writer.WriteElementString("Payee", transactionRepper.Payee);
            writer.WriteElementString("TransactionDate", transactionRepper.TransactionDate);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Writes the payment statement servicer contact info to xml.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        /// <param name="dataLoan">The loan file.</param>
        private void WritePaymentStatementServicerElement(XmlWriter writer, CPageData dataLoan)
        {
            var preparer = dataLoan.GetPreparerOfForm(
                E_PreparerFormT.PaymentStatementServicer,
                E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteStartElement("PaymentStatementServicer");
            writer.WriteElementString("CompanyName", preparer.CompanyName);
            writer.WriteElementString("StreetAddr", preparer.StreetAddr);
            writer.WriteElementString("CityStateZip", preparer.CityStateZip);
            writer.WriteElementString("PhoneOfCompany", preparer.PhoneOfCompany);
            writer.WriteElementString("EmailAddr", preparer.EmailAddr);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Writes the borrower mailing address to xml.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        /// <param name="dataApp">The loan application.</param>
        private void WriteBorrowerMailingAddressElement(XmlWriter writer, CAppData dataApp)
        {
            writer.WriteStartElement("BorrowerMailingAddress");
            writer.WriteElementString("To", dataApp.aBNm);
            writer.WriteElementString("Street", dataApp.aBAddrPost);
            writer.WriteElementString("CityStateZip", Tools.CombineCityStateZip(dataApp.aBCityPost, dataApp.aBStatePost, dataApp.aBZipPost));
            writer.WriteEndElement();
        }

        /// <summary>
        /// Writes the transaction activity to xml.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        /// <param name="transactions">The transactions relevant to this statement. Not all will be included.</param>
        /// <param name="convert">The converter.</param>
        private void WriteTransactionActivityElement(XmlWriter writer, IEnumerable<Transaction> transactions, LosConvert convert)
        {
            writer.WriteStartElement("TransactionActivity");

            Func<DateTime, bool> withinBillingPeriod = date => date >= this.options.PeriodStart
                    && date <= this.options.PeriodEnd;
            var transactionsWithinPeriod = transactions
                .Where(t => withinBillingPeriod(t.TransactionDate)
                    || withinBillingPeriod(t.PaymentReceivedDate)
                    || withinBillingPeriod(t.PaymentCreditedDate))
                .Select(t => new TransactionRepper(t, convert));

            foreach (var trans in transactionsWithinPeriod)
            {
                writer.WriteStartElement("Transaction");
                writer.WriteElementString("TransactionDate", trans.TransactionDate);
                writer.WriteElementString("PaymentCreditedDate", trans.PaymentCreditedDate);
                writer.WriteElementString("Description", trans.Description);
                writer.WriteElementString("Charges", trans.AmountDueExcludingOverdue);
                writer.WriteElementString("Payments", trans.AmountPaid);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Writes the past payment breakdown with past month and YTD totals.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        /// <param name="transactions">The transactions relevant to this statement.</param>
        /// <param name="statementDate">The statement date.</param>
        /// <param name="convert">The converter.</param>
        private void WritePastPaymentsBreakdownElement(XmlWriter writer, IEnumerable<Transaction> transactions, DateTime statementDate, LosConvert convert)
        {
            writer.WriteStartElement("PastPaymentsBreakdown");

            var creditedTransactions = transactions.Where(t => t.PaymentCreditedDate != DateTime.MinValue);

            var transactionsCreditedWithinPeriod = creditedTransactions
                .Where(t => t.TransactionDate >= this.options.PeriodStart
                    && t.TransactionDate <= this.options.PeriodEnd);
            this.WriteTransactionTotalsElement(writer, "WithinBillingPeriod", transactionsCreditedWithinPeriod, convert);

            var creditedTransactionsYearToDate = creditedTransactions.Where(p => p.TransactionDate.Year == statementDate.Year && p.PaymentCreditedDate != DateTime.MinValue);
            this.WriteTransactionTotalsElement(writer, "YearToDate", creditedTransactionsYearToDate, convert);

            writer.WriteEndElement();
        }

        /// <summary>
        /// Performs totals calculations on the given transactions.
        /// </summary>
        /// <param name="transactions">The transactions to total.</param>
        /// <returns>The calculation result containing totals across the different payment attributes.</returns>
        private TransactionTotalAggregateResult GetTransactionTotals(IEnumerable<Transaction> transactions)
        {
            return transactions.Aggregate(
                new TransactionTotalAggregateResult(),
                (result, transaction) =>
                {
                    result.AmountToPrincipal = SumMoney(result.AmountToPrincipal, transaction.AmountToPrincipal);
                    result.AmountToInterest = SumMoney(result.AmountToInterest, transaction.AmountToInterest);
                    result.AmountToEscrowForMortgageInsurance = SumMoney(result.AmountToEscrowForMortgageInsurance, transaction.AmountToEscrowForMortgageInsurance);
                    result.AmountToEscrowForTaxesAndInsurance = SumMoney(result.AmountToEscrowForTaxesAndInsurance, transaction.AmountToEscrowForTaxesAndInsurance);
                    result.TotalRegularPayment = SumMoney(result.TotalRegularPayment, transaction.TotalRegularPayment);
                    result.AmountToLateFees = SumMoney(result.AmountToLateFees, transaction.AmountToLateFees);
                    result.AmountToOtherCharges = SumMoney(result.AmountToOtherCharges, transaction.AmountToOtherCharges);
                    result.TotalPayment = SumMoney(result.TotalPayment, transaction.AmountPaid);
                    return result;
                });
        }

        /// <summary>
        /// Writes transaction totals to xml.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        /// <param name="elementName">The element name that will wrap the totals.</param>
        /// <param name="transactions">The transactions to total.</param>
        /// <param name="convert">The converter.</param>
        private void WriteTransactionTotalsElement(XmlWriter writer, string elementName, IEnumerable<Transaction> transactions, LosConvert convert)
        {
            var totals = this.GetTransactionTotals(transactions);
            var aggregateTotalsRepper = new TransactionTotalAggregateResultRepper(totals, convert);
            writer.WriteStartElement(elementName);
            writer.WriteElementString("AmountToPrincipal", aggregateTotalsRepper.AmountToPrincipal);
            writer.WriteElementString("AmountToInterest", aggregateTotalsRepper.AmountToInterest);
            writer.WriteElementString("AmountToEscrowForMortgageInsurance", aggregateTotalsRepper.AmountToEscrowForMortgageInsurance);
            writer.WriteElementString("AmountToEscrowForTaxesAndInsurance", aggregateTotalsRepper.AmountToEscrowForTaxesAndInsurance);
            writer.WriteElementString("TotalRegularPayment", aggregateTotalsRepper.TotalRegularPayment);
            writer.WriteElementString("AmountToLateFees", aggregateTotalsRepper.AmountToLateFees);
            writer.WriteElementString("AmountToOtherCharges", aggregateTotalsRepper.AmountToOtherCharges);
            writer.WriteElementString("TotalPayment", aggregateTotalsRepper.TotalPayment);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Writes out the recent account history for the delinquency notice.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        /// <param name="transactions">The transactions relevant to this statement.</param>
        /// <param name="primaryTransaction">The primary transaction for which this statement is being generated.</param>
        /// <param name="convert">The converter.</param>
        private void WriteRecentAccountHistory(XmlWriter writer, IEnumerable<Transaction> transactions, Transaction primaryTransaction, LosConvert convert)
        {
            var transactionsForHistory = this.GetTransactionsForRecentAccountHistory(transactions);

            writer.WriteStartElement("RecentAccountHistory");

            foreach (var transaction in transactionsForHistory)
            {
                bool fullyPaid = transaction.AmountPaid >= transaction.TotalAmountDue
                    && transaction.PaymentCreditedDate != DateTime.MinValue;
                bool paidOnTime = transaction.PaymentCreditedDate != DateTime.MinValue
                    && transaction.PaymentCreditedDate <= transaction.DueDate;

                var historyItem = new StringBuilder();

                if (transaction.RowNum == primaryTransaction.RowNum)
                {
                    historyItem.Append($"Current payment due {convert.ToDateTimeString(transaction.DueDate)}: ");
                    historyItem.Append(convert.ToMoneyString(transaction.AmountDueExcludingOverdue, FormatDirection.ToRep));
                }
                else
                {
                    historyItem.Append($"Payment due {convert.ToDateTimeString(transaction.DueDate)}: ");

                    if (fullyPaid)
                    {
                        historyItem.Append("Fully paid on ");

                        if (paidOnTime)
                        {
                            historyItem.Append("time");
                        }
                        else
                        {
                            historyItem.Append(convert.ToDateTimeString(transaction.PaymentCreditedDate));
                        }
                    }
                    else
                    {
                        historyItem.Append("Unpaid balance of " + convert.ToMoneyString(transaction.AmountDueExcludingOverdue - transaction.AmountPaid, FormatDirection.ToRep));
                    }
                }

                writer.WriteElementString("Item", historyItem.ToString());
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Gets the transactions that should be included in the recent account history
        /// for the delinquency notice.
        /// </summary>
        /// <param name="transactions">The transactions relevant to this statement.</param>
        /// <returns>The transactions to use for the recent account history.</returns>
        private IEnumerable<Transaction> GetTransactionsForRecentAccountHistory(IEnumerable<Transaction> transactions)
        {
            bool encounteredTransactionWhereLoanCurrent = false;
            return transactions
                .Where(t => string.Equals(t.Description, "Monthly Payment", StringComparison.InvariantCultureIgnoreCase))
                .Reverse()
                .Take(6)
                .TakeWhile(t =>
                {
                    if (encounteredTransactionWhereLoanCurrent)
                    {
                        return false;
                    }

                    encounteredTransactionWhereLoanCurrent = t.AmountOverdue == 0
                        && t.PaymentReceivedDate != DateTime.MinValue
                        && t.PaymentReceivedDate <= t.DueDate;

                    return true;
                })
                .Reverse();
        }

        /// <summary>
        /// A wrapper for the servicing payment. Goal is to simplify the interface and 
        /// and implement some simple calculations needed for the payment statement.
        /// </summary>
        private class Transaction
        {
            /// <summary>
            /// The servicing payment that we're wrapping.
            /// </summary>
            private CServicingPaymentFields payment;

            /// <summary>
            /// The overdue amount from prior payments.
            /// </summary>
            private decimal overdue;

            /// <summary>
            /// The mortgage insurance amount for this payment.
            /// </summary>
            private decimal mortgageInsurance;

            /// <summary>
            /// Initializes a new instance of the <see cref="Transaction"/> class.
            /// </summary>
            /// <param name="payment">The payment to wrap.</param>
            /// <param name="overdue">The overdue amount from previous payments.</param>
            /// <param name="mortgageInsurance">The mortgage insurance amount.</param>
            internal Transaction(CServicingPaymentFields payment, decimal overdue, decimal mortgageInsurance)
            {
                this.payment = payment;
                this.overdue = overdue;
                this.mortgageInsurance = mortgageInsurance;
            }

            /// <summary>
            /// Gets the row number (identifier) of the servicing payment.
            /// </summary>
            public int RowNum => this.payment.RowNum;

            /// <summary>
            /// Gets the due date.
            /// </summary>
            public DateTime DueDate => this.payment.DueD;

            /// <summary>
            /// Gets the payment received date.
            /// </summary>
            public DateTime PaymentReceivedDate => this.payment.PaymentReceivedD;

            /// <summary>
            /// Gets the payment credited date.
            /// </summary>
            public DateTime PaymentCreditedDate => this.payment.PaymentD;

            /// <summary>
            /// Gets the transaction date.
            /// </summary>
            public DateTime TransactionDate => this.payment.TransactionD;

            /// <summary>
            /// Gets the description.
            /// </summary>
            public string Description => this.payment.ServicingTransactionT;

            /// <summary>
            /// Gets the payee.
            /// </summary>
            public string Payee => this.payment.Payee;

            /// <summary>
            /// Gets the amount of the payment that will be applied to principal.
            /// </summary>
            public decimal AmountToPrincipal => this.payment.Principal;

            /// <summary>
            /// Gets the amount of the payment that will be applied to interest.
            /// </summary>
            public decimal AmountToInterest => this.payment.Interest;

            /// <summary>
            /// Gets the amount of the payment that will be applied to mortgage insurance.
            /// </summary>
            public decimal AmountToEscrowForMortgageInsurance => this.mortgageInsurance;

            /// <summary>
            /// Gets the amount of the payment that will be applied to escrow for taxes and insurance, excluding mortgage insurance.
            /// </summary>
            public decimal AmountToEscrowForTaxesAndInsurance => SumMoney(this.payment.Escrow, -this.mortgageInsurance);

            /// <summary>
            /// Gets the total payment amount applied to principal, interest, and escrow.
            /// </summary>
            public decimal TotalRegularPayment => SumMoney(this.payment.Principal, this.payment.Interest, this.payment.Escrow);

            /// <summary>
            /// Gets the amount of the payment that will be applied to late fees.
            /// </summary>
            public decimal AmountToLateFees => this.payment.LateFee;

            /// <summary>
            /// Gets the amount of the payment that will be applied to other charges.
            /// </summary>
            public decimal AmountToOtherCharges => this.payment.Other;

            /// <summary>
            /// Gets the amount that is overdue from past payments.
            /// </summary>
            public decimal AmountOverdue => this.overdue;

            /// <summary>
            /// Gets the total amount due, including late fees, other charges, and the overdue amount from past payments.
            /// </summary>
            public decimal TotalAmountDue => SumMoney(this.payment.DueAmt, this.AmountOverdue);

            /// <summary>
            /// Gets the amount that was paid.
            /// </summary>
            public decimal AmountPaid => this.payment.PaymentAmt;

            /// <summary>
            /// Gets the amount due excluding the overdue amount from past payments.
            /// </summary>
            public decimal AmountDueExcludingOverdue => this.payment.DueAmt;
        }

        /// <summary>
        /// Wraps a <see cref="Transaction"/> and returns only strings.
        /// </summary>
        private class TransactionRepper
        {
            /// <summary>
            /// The transaction.
            /// </summary>
            private Transaction transaction;

            /// <summary>
            /// The converter that will be used to get string values from the transaction.
            /// </summary>
            private LosConvert convert;

            /// <summary>
            /// Initializes a new instance of the <see cref="TransactionRepper"/> class.
            /// </summary>
            /// <param name="transaction">The transaction.</param>
            /// <param name="convert">The converter.</param>
            internal TransactionRepper(Transaction transaction, LosConvert convert)
            {
                this.transaction = transaction;
                this.convert = convert;
            }

            /// <summary>
            /// Gets the due date.
            /// </summary>
            public string DueDate => this.convert.ToDateTimeString(this.transaction.DueDate);

            /// <summary>
            /// Gets the transaction date.
            /// </summary>
            public string TransactionDate => this.convert.ToDateTimeString(this.transaction.TransactionDate);

            /// <summary>
            /// Gets the payment credited date.
            /// </summary>
            public string PaymentCreditedDate => this.convert.ToDateTimeString(this.transaction.PaymentCreditedDate);

            /// <summary>
            /// Gets the description.
            /// </summary>
            public string Description => this.transaction.Description;

            /// <summary>
            /// Gets the payee.
            /// </summary>
            public string Payee => this.transaction.Payee;

            /// <summary>
            /// Gets the amount of the payment that will be applied to principal.
            /// </summary>
            public string AmountToPrincipal => this.convert.ToMoneyString(this.transaction.AmountToPrincipal, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount of the payment that will be applied to interest.
            /// </summary>
            public string AmountToInterest => this.convert.ToMoneyString(this.transaction.AmountToInterest, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount of the payment that will be applied to mortgage insurance.
            /// </summary>
            public string AmountToEscrowForMortgageInsurance => this.convert.ToMoneyString(this.transaction.AmountToEscrowForMortgageInsurance, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount of the payment that will be applied to escrow for taxes and insurance, excluding mortgage insurance.
            /// </summary>
            public string AmountToEscrowForTaxesAndInsurance => this.convert.ToMoneyString(this.transaction.AmountToEscrowForTaxesAndInsurance, FormatDirection.ToRep);

            /// <summary>
            /// Gets the total payment amount applied to principal, interest, and escrow.
            /// </summary>
            public string TotalRegularPayment => this.convert.ToMoneyString(this.transaction.TotalRegularPayment, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount of the payment that will be applied to late fees.
            /// </summary>
            public string AmountToLateFees => this.convert.ToMoneyString(this.transaction.AmountToLateFees, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount of the payment that will be applied to other charges.
            /// </summary>
            public string AmountToOtherCharges => this.convert.ToMoneyString(this.transaction.AmountToOtherCharges, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount that is overdue from past payments.
            /// </summary>
            public string AmountOverdue => this.convert.ToMoneyString(this.transaction.AmountOverdue, FormatDirection.ToRep);

            /// <summary>
            /// Gets the total amount due, including late fees, other charges, and the overdue amount from past payments.
            /// </summary>
            public string TotalAmountDue => this.convert.ToMoneyString(this.transaction.TotalAmountDue, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount that was paid.
            /// </summary>
            public string AmountPaid => this.convert.ToMoneyString(this.transaction.AmountPaid, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount due excluding the overdue amount from past payments.
            /// </summary>
            public string AmountDueExcludingOverdue => this.convert.ToMoneyString(this.transaction.AmountDueExcludingOverdue, FormatDirection.ToRep);
        }

        /// <summary>
        /// Class to contain payment totals.
        /// </summary>
        private class TransactionTotalAggregateResult
        {
            /// <summary>
            /// Gets or sets the amount applied to principal.
            /// </summary>
            public decimal AmountToPrincipal { get; set; }

            /// <summary>
            /// Gets or sets the amount applied to interest.
            /// </summary>
            public decimal AmountToInterest { get; set; }

            /// <summary>
            /// Gets or sets the amount applied to mortgage insurance.
            /// </summary>
            public decimal AmountToEscrowForMortgageInsurance { get; set; }

            /// <summary>
            /// Gets or sets the amount applied to taxes and insurance, excluding mortgage insurance.
            /// </summary>
            public decimal AmountToEscrowForTaxesAndInsurance { get; set; }

            /// <summary>
            /// Gets or sets the total regular payment, excluding late fees, other charges, and overdue amounts.
            /// </summary>
            public decimal TotalRegularPayment { get; set; }

            /// <summary>
            /// Gets or sets the amount applied to late fees.
            /// </summary>
            public decimal AmountToLateFees { get; set; }

            /// <summary>
            /// Gets or sets the amount applied to other charges.
            /// </summary>
            public decimal AmountToOtherCharges { get; set; }

            /// <summary>
            /// Gets or sets the total amount paid.
            /// </summary>
            public decimal TotalPayment { get; set; }
        }

        /// <summary>
        /// Class that converts payment totals to strings.
        /// </summary>
        private class TransactionTotalAggregateResultRepper
        {
            /// <summary>
            /// The totals calculation results.
            /// </summary>
            private TransactionTotalAggregateResult result;

            /// <summary>
            /// The converter.
            /// </summary>
            private LosConvert convert;

            /// <summary>
            /// Initializes a new instance of the <see cref="TransactionTotalAggregateResultRepper"/> class.
            /// </summary>
            /// <param name="result">The totals result.</param>
            /// <param name="convert">The converter.</param>
            public TransactionTotalAggregateResultRepper(TransactionTotalAggregateResult result, LosConvert convert)
            {
                this.result = result;
                this.convert = convert;
            }

            /// <summary>
            /// Gets the amount applied to principal.
            /// </summary>
            public string AmountToPrincipal => this.convert.ToMoneyString(this.result.AmountToPrincipal, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount applied to interest.
            /// </summary>
            public string AmountToInterest => this.convert.ToMoneyString(this.result.AmountToInterest, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount applied to mortgage insurance.
            /// </summary>
            public string AmountToEscrowForMortgageInsurance => this.convert.ToMoneyString(this.result.AmountToEscrowForMortgageInsurance, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount applied to taxes and insurance, excluding mortgage insurance.
            /// </summary>
            public string AmountToEscrowForTaxesAndInsurance => this.convert.ToMoneyString(this.result.AmountToEscrowForTaxesAndInsurance, FormatDirection.ToRep);

            /// <summary>
            /// Gets the total regular payment, excluding late fees, other charges, and overdue amounts.
            /// </summary>
            public string TotalRegularPayment => this.convert.ToMoneyString(this.result.TotalRegularPayment, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount applied to late fees.
            /// </summary>
            public string AmountToLateFees => this.convert.ToMoneyString(this.result.AmountToLateFees, FormatDirection.ToRep);

            /// <summary>
            /// Gets the amount applied to other charges.
            /// </summary>
            public string AmountToOtherCharges => this.convert.ToMoneyString(this.result.AmountToOtherCharges, FormatDirection.ToRep);

            /// <summary>
            /// Gets the total amount paid.
            /// </summary>
            public string TotalPayment => this.convert.ToMoneyString(this.result.TotalPayment, FormatDirection.ToRep);
        }

        /// <summary>
        /// Class to support building a list of transactions from servicing payments while
        /// accumulating an overdue amount.
        /// </summary>
        private class ServicingPaymentAggregateResult
        {
            /// <summary>
            /// Gets or sets the transactions.
            /// </summary>
            public List<Transaction> Transactions { get; set; } = new List<Transaction>();

            /// <summary>
            /// Gets or sets the overdue amount.
            /// </summary>
            public decimal Overdue { get; set; }
        }
    }
}
