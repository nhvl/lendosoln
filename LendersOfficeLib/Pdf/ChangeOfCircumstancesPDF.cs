﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Common.SerializationTypes;
using LendersOffice.PdfGenerator;

namespace LendersOffice.Pdf
{
    public class CChangeOfCircumstancesPDF : AbstractLetterPDF
    {
        public override string Name
        {
            get { return "Change Of Circumstances"; }
        }
        public override string PdfFile
        {
            get { return "ChangeOfCircumstance.pdf"; }
        }

        public override string Description
        {
            get { return "Change of Circumstances"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Forms/ChangeOfCircumstances.aspx"; }
        }
        private string ArchiveDate
        {
            get { return Arguments["ArchiveDate"]; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            var cocArchiveDateToDisplay = dataLoan.sGfeTilPrepareDate_rep;
            if (dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    if (!string.IsNullOrEmpty(ArchiveDate))
                    {

                        var cocas = from a in dataLoan.CoCArchives
                                    where a.DateArchived == ArchiveDate
                                    select a;
                        var coca = cocas.First();
                        dataLoan.ApplyCoCArchive(coca);
                        if(dataLoan.BrokerDB.IsRemovePreparedDates)
                        {
                            cocArchiveDateToDisplay = coca.DateArchived;
                        }
                    }
                    else
                    {
                        dataLoan.ApplyCoCArchive(dataLoan.LastDisclosedCoCArchive); // can't save loan after this call is made.                        
                        if (dataLoan.BrokerDB.IsRemovePreparedDates)
                        {
                            cocArchiveDateToDisplay = dataLoan.sLastDisclosedClosingCostCoCArchive.DateArchived;
                        }
                    }
                }
                else
                {
                    ClosingCostCoCArchive closingCostArchive = null;
                    if (!string.IsNullOrEmpty(ArchiveDate))
                    {
                        closingCostArchive = dataLoan.sClosingCostCoCArchive.FirstOrDefault(p => p.DateArchived == ArchiveDate);
                    }

                    if (closingCostArchive == null && dataLoan.sClosingCostCoCArchive.Count() != 0)
                    {
                        if (dataLoan.BrokerDB.IsRemovePreparedDates)
                        {
                            closingCostArchive = dataLoan.sClosingCostCoCArchive.First();
                        }
                    }

                    if (closingCostArchive != null )
                    {
                        dataLoan.ApplyClosingCostCoCData(closingCostArchive);
                    }
                }
            }
            
            AddFormFieldData("sCircumstanceChangeD", dataLoan.sCircumstanceChangeD_rep);
            AddFormFieldData("sGFERediscD", dataLoan.sGfeRedisclosureD_rep);
            AddFormFieldData("sCircumstanceChangeExplanation", dataLoan.sCircumstanceChangeExplanation);
            AddFormFieldData("sCircumstanceChange1Amount", dataLoan.sCircumstanceChange1Amount_rep);
            AddFormFieldData("sCircumstanceChange1Reason", dataLoan.sCircumstanceChange1Reason);
            AddFormFieldData("sCircumstanceChange2Amount", dataLoan.sCircumstanceChange2Amount_rep);
            AddFormFieldData("sCircumstanceChange2Reason", dataLoan.sCircumstanceChange2Reason);
            AddFormFieldData("sCircumstanceChange3Amount", dataLoan.sCircumstanceChange3Amount_rep);
            AddFormFieldData("sCircumstanceChange3Reason", dataLoan.sCircumstanceChange3Reason);
            AddFormFieldData("sCircumstanceChange4Amount", dataLoan.sCircumstanceChange4Amount_rep);
            AddFormFieldData("sCircumstanceChange4Reason", dataLoan.sCircumstanceChange4Reason);
            AddFormFieldData("sCircumstanceChange5Amount", dataLoan.sCircumstanceChange5Amount_rep);
            AddFormFieldData("sCircumstanceChange5Reason", dataLoan.sCircumstanceChange5Reason);
            AddFormFieldData("sCircumstanceChange6Amount", dataLoan.sCircumstanceChange6Amount_rep);
            AddFormFieldData("sCircumstanceChange6Reason", dataLoan.sCircumstanceChange6Reason);
            AddFormFieldData("sCircumstanceChange7Amount", dataLoan.sCircumstanceChange7Amount_rep);
            AddFormFieldData("sCircumstanceChange7Reason", dataLoan.sCircumstanceChange7Reason);
            AddFormFieldData("sCircumstanceChange8Amount", dataLoan.sCircumstanceChange8Amount_rep);
            AddFormFieldData("sCircumstanceChange8Reason", dataLoan.sCircumstanceChange8Reason);

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);

            AddFormFieldData("sSpAddr_Multiline", dataLoan.sSpAddr_MultiLine);
            AddFormFieldData("GfeTilPrepareDate", DateTime.Parse(cocArchiveDateToDisplay).ToString("MM/dd/yyyy"));
            AddFormFieldData("sLNm", dataLoan.sLNm);
        }
    }

    /// <summary>
    /// Generates a pdf that lists all fees in a change of circumstance archive.
    /// </summary>
    public class CChangeOfCircumstancesNewPDF : AbstractXsltPdf
    {
        /// <summary>
        /// Gets the filename for this pdf.
        /// </summary>
        /// <value>The filename for this pdf.</value>
        public override string PdfName
        {
            get
            {
                return "Change of Circumstance";
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets the url for the edit link in the print list.
        /// </summary>
        /// <value>The url for the edit link in the print list.</value>
        public override string EditUrl
        {
            get
            {
                return Tools.VRoot + "/newlos/Forms/ChangeOfCircumstancesNew";
            }
        }

        /// <summary>
        /// Gets the pdf description.
        /// </summary>
        /// <value>The pdf description.</value>
        public override string Description
        {
            get
            {
                return "Change of Circumstance";
            }
        }

        /// <summary>
        /// Gets the xslt location for this pdf.
        /// </summary>
        /// <value>The xslt location for this pdf.</value>
        protected override string XsltEmbeddedResourceLocation
        {
            get
            {
                return "LendersOffice.Pdf.DynamicCoC.xslt";
            }
        }

        /// <summary>
        /// Gets the archive date if it was passed in as an argument.
        /// </summary>
        /// <value>The archive date if it was passed in as an argument.</value>
        private string ArchiveDate
        {
            get
            {
                return this.Arguments["ArchiveDate"];
            }
        }
        public override PDFPageSize PageSize
        {
            get { return PDFPageSize.Letter; }
        }

        /// <summary>
        /// Generates the data for the xlst.
        /// </summary>
        /// <param name="writer">The writer to use.</param>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="dataApp">The app for this pdf.</param>
        protected override void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp)
        {
            writer.WriteStartElement("DynamicCoC");

            writer.WriteElementString("sSpAddr_Multiline", dataLoan.sSpAddr_MultiLine);
            writer.WriteElementString("sLNm", dataLoan.sLNm);

            writer.WriteElementString("aBNm", dataApp.aBNm);
            writer.WriteElementString("aCNm", dataApp.aCNm);
            var cocArchiveDateToDisplay = dataLoan.sGfeTilPrepareDate_rep;

            if (dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                // Tuple order: <fee name, fee amount>
                List<Tuple<string, string>> fees = new List<Tuple<string, string>>();
                if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    ICoCArchive chosenArchive;
                    if (!string.IsNullOrEmpty(this.ArchiveDate))
                    {
                        var cocArchives = from archive in dataLoan.CoCArchives
                                          where archive.DateArchived == this.ArchiveDate
                                          select archive;
                        chosenArchive = cocArchives.First();
                    }
                    else
                    {
                        chosenArchive = dataLoan.LastDisclosedCoCArchive;
                    }

                    if (chosenArchive != null)
                    {
                        if(dataLoan.BrokerDB.IsRemovePreparedDates)
                        {
                            cocArchiveDateToDisplay = DateTime.Parse(chosenArchive.DateArchived).ToString("MM/dd/yyyy");
                        }

                        writer.WriteElementString("sGFERediscD", chosenArchive.sGfeRedisclosureD);
                        writer.WriteElementString("sCircumstanceChangeExplanation", chosenArchive.sCircumstanceChangeExplanation);
                        writer.WriteElementString("sCircumstanceChangeD", chosenArchive.sCircumstanceChangeD);

                        this.PopulateFeeList(chosenArchive.sCircumstanceChange1Reason, chosenArchive.sCircumstanceChange1Amount, fees);
                        this.PopulateFeeList(chosenArchive.sCircumstanceChange2Reason, chosenArchive.sCircumstanceChange2Amount, fees);
                        this.PopulateFeeList(chosenArchive.sCircumstanceChange3Reason, chosenArchive.sCircumstanceChange3Amount, fees);
                        this.PopulateFeeList(chosenArchive.sCircumstanceChange4Reason, chosenArchive.sCircumstanceChange4Amount, fees);
                        this.PopulateFeeList(chosenArchive.sCircumstanceChange5Reason, chosenArchive.sCircumstanceChange5Amount, fees);
                        this.PopulateFeeList(chosenArchive.sCircumstanceChange6Reason, chosenArchive.sCircumstanceChange6Amount, fees);
                        this.PopulateFeeList(chosenArchive.sCircumstanceChange7Reason, chosenArchive.sCircumstanceChange7Amount, fees);
                        this.PopulateFeeList(chosenArchive.sCircumstanceChange8Reason, chosenArchive.sCircumstanceChange8Amount, fees);
                    }
                }
                else
                {
                    ClosingCostCoCArchive closingCostArchive = null;
                    if (!string.IsNullOrEmpty(this.ArchiveDate))
                    {
                        closingCostArchive = dataLoan.sClosingCostCoCArchive.FirstOrDefault(p => p.DateArchived == this.ArchiveDate);
                    }

                    if (closingCostArchive == null && dataLoan.sClosingCostCoCArchive.Count() != 0)
                    {
                        closingCostArchive = dataLoan.sClosingCostCoCArchive.First();
                    }

                    if (closingCostArchive != null)
                    {
                        writer.WriteElementString("sGFERediscD", closingCostArchive.RedisclosureD);
                        writer.WriteElementString("sCircumstanceChangeExplanation", closingCostArchive.CircumstanceChangeExplanation);
                        writer.WriteElementString("sCircumstanceChangeD", closingCostArchive.CircumstanceChangeD);

                        if(dataLoan.BrokerDB.IsRemovePreparedDates)
                        {
                            cocArchiveDateToDisplay = DateTime.Parse(closingCostArchive.DateArchived).ToString("MM/dd/yyyy");
                        }

                        foreach (var fee in closingCostArchive.Fees)
                        {
                            fees.Add(new Tuple<string, string>(fee.Description, "$" + dataLoan.m_convertLos.ToMoneyString(fee.Value, FormatDirection.ToRep)));
                        }
                    }
                }

                foreach (var tuple in fees)
                {
                    writer.WriteStartElement("cocFee");
                    writer.WriteElementString("description", tuple.Item1);
                    writer.WriteElementString("amount", tuple.Item2);
                    writer.WriteEndElement();
                }
            }

            writer.WriteElementString("sGfeTilPrepareDate", cocArchiveDateToDisplay);
            writer.WriteEndElement();
        }

        /// <summary>
        /// Populates the fee list if using the old coc archive.
        /// </summary>
        /// <param name="reason">The reason. I think this is actually the fee description.</param>
        /// <param name="amount">The fee amount.</param>
        /// <param name="feeList">The fee list to populate.</param>
        private void PopulateFeeList(string reason, string amount, List<Tuple<string, string>> feeList)
        {
            if (!string.IsNullOrEmpty(reason) && !string.IsNullOrEmpty(amount))
            {
                feeList.Add(new Tuple<string, string>(reason, amount));
            }
        }
    }
}