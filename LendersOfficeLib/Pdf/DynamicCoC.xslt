﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="DynamicCoC">
      <html>
        <head>
          <meta http-equiv="X-UA-Compatible" content="IE=edge" />
          <style type="text/css" media="all">
            body
            {
              font-family: Arial, Helvetica, sans-serif;
              font-size: 20px;
            }
            td
            {
              vertical-align: top;
            }
            div
            {
              padding: 0px;
              margin-bottom: 5px;
            }
            .header
            {
              font-size: 35px;
              height: 50px;
              text-align: center;
              font-weight: bold;
            }
            .Label
            {
              font-weight: bold;
            }
            #ApplicantsLabel
            {
              display: block;
              float: left;
            }
            .Applicants
            {
              padding: 0px;
              border: 0px;
              border-spacing 0px;
            }
            #FeesTable td
            {
              padding-left: 15px;
            }
          </style>
        </head>
        <body>
          <div class="header">
            RESPA Change of Circumstance
          </div>
          
          <div>
            <span class="Label">
              Loan Number:
            </span>
            <span>
              <xsl:value-of select="sLNm"></xsl:value-of>
            </span>
          </div>
          <div>
            <span class="Label" id="ApplicantsLabel">
              Applicant(s):
            </span>
            <table class="Applicants">
              <tr>
                <td><xsl:value-of select="aBNm"></xsl:value-of></td>
              </tr>
              <tr>
                <td><xsl:value-of select="aCNm"></xsl:value-of></td>
              </tr>
            </table>
          </div>
          <div>
            <span class="Label">
              Subject Property Address:
            </span>
            <span>
              <xsl:value-of select="sSpAddr_Multiline"></xsl:value-of>
            </span>
          </div>
          <div>
            <span class="Label">
              Date of Loan Estimate:
            </span>
            <span>
              <xsl:value-of select="sGfeTilPrepareDate"></xsl:value-of>
            </span>
          </div>
          <div>
            <span class="Label">
              Date of Change:
            </span>
            <span>
              <xsl:value-of select="sCircumstanceChangeD"></xsl:value-of>
            </span>
          </div>
          <div>
            <span class="Label">
              Date of Loan Estimate Redisclosure:
            </span>
            <span>
              <xsl:value-of select="sGFERediscD"></xsl:value-of>
            </span>
          </div>
          <div>
            <span class="Label">
              Explanation of Change:  
            </span>
            <div>
              <xsl:value-of select="sCircumstanceChangeExplanation"></xsl:value-of>
            </div>
          </div>
          <br />
          <div id="FeesSection">
            <span class="Label">
              Fee changes associated with Changed Circumstance:
            </span>
            <table id="FeesTable">
              <xsl:for-each select="cocFee">
                <tr>
                  <td>
                    <xsl:value-of select="amount"></xsl:value-of>
                  </td>
                  <td>
                    <xsl:value-of select="description"></xsl:value-of>
                  </td>
                </tr>
              </xsl:for-each>
            </table>
          </div>
        </body>
      </html>
    </xsl:template>
</xsl:stylesheet>
