using System;
using System.Collections.Specialized;
using System.Data;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CAppraisalDisclosurePDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "AppraisalDisclosure.pdf"; }
        }
        public override string Description 
        {
            get { return "Right To Receive Appraisal"; }
        }
		public override string EditLink
		{
			get { return "/newlos/Disclosure/AppraisalDisclosure.aspx"; }
		}

        protected override  void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.AppraisalDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 
            string brokerInformation = Tools.FormatAddress(broker.CompanyName, broker.StreetAddr, broker.City, broker.State, broker.Zip);
            string propertyAddress = Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip);

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("PropertyAddress", propertyAddress);
            AddFormFieldData("BrokerInformation", brokerInformation);
            AddFormFieldData("aBNm0", dataApp.aBNm);
            AddFormFieldData("aCNm0", dataApp.aCNm);

        }
	}
}
