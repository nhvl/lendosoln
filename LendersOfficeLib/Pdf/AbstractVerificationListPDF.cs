﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.Common;

    public abstract class AbstractVerificationListPDF : IPDFPrintItem
    {
        private string m_id;
        private CPageData m_dataLoan;
        private NameValueCollection m_arguments;
        private Guid m_loanID;
        private Guid m_applicationID;
        private int m_currentIndex = 0;

        public void GetInfo(CAppData dataApp, out IEnumerable<PdfVerificationItem> items, out AbstractPDFFile pdf)
        {
            if (null != dataApp)
            {
                items = GetItems(dataApp);
            }
            else
            {
                items = null;
            }
            pdf = ContructPdfItem();
        }

        protected int CurrentIndex
        {
            get { return m_currentIndex; }
            set { m_currentIndex = value; }
        }

        protected void RenderPDFFile(StringBuilder sb, AbstractPDFFile pdf, CAppData dataApp)
        {
            pdf.DataLoan = m_dataLoan;
            pdf.SetCurrentDataApp(dataApp);
            pdf.RenderPrintLink(sb);

        }

        protected void GenerateVerifList(StringBuilder sb, CAppData dataApp)
        {
            var list = GetItems(dataApp);

            foreach (var o in list)
            {
                NameValueCollection c = null;
                if (null == Arguments)
                {
                    c = new NameValueCollection();
                }
                else
                {
                    c = new NameValueCollection(Arguments);
                }
                c["recordid"] = o.RecordId.ToString();
                c["displayname"] = o.DisplayName;
                AbstractPDFFile pdf = ContructPdfItem();
                pdf.Arguments = c;
                pdf.ID = this.ID + "_" + this.CurrentIndex;
                RenderPDFFile(sb, pdf, dataApp);
                this.CurrentIndex++;
            }
        }

        protected abstract IEnumerable<PdfVerificationItem> GetItems(CAppData dataApp);

        protected abstract AbstractPDFFile ContructPdfItem();

        private void InitializeDataLoan()
        {
            m_dataLoan = Utilities.GetPDFPrintData(m_loanID);
            m_dataLoan.InitLoad();
        }

        #region Implementation of IPDFPrintItem
        public void RenderPrintLink(StringBuilder sb)
        {
            if (null == m_dataLoan)
                InitializeDataLoan();

            int nApps = m_dataLoan.nApps;
            for (int i = 0; i < nApps; i++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                GenerateVerifList(sb, dataApp);
            }

        }

        public virtual string Name
        {
            get
            {
                // 5/24/2004 dd - If class name has following format C{name}PDF then return unique code as {name}
                string name = GetType().Name;
                if (name.StartsWith("C") && name.EndsWith("PDF"))
                {
                    name = name.Substring(1, name.Length - 4);
                }
                return name;
            }
        }

        public virtual string ID
        {
            get { return m_id; }
            set { m_id = value; }
        }

        public virtual string Description
        {
            get
            {
                return null;
            }
        }

        public string EditLink
        {
            get { return ""; }
        }

        public string PreviewLink
        {
            get { return ""; }
        }

        public CPageData DataLoan
        {
            get { return m_dataLoan; }
            set { m_dataLoan = value; }
        }

        public NameValueCollection Arguments
        {
            get { return this.m_arguments; }
            set
            {
                this.m_arguments = value;

                if (this.m_arguments == null)
                {
                    return;
                }

                Guid argLoanId;

                if (Guid.TryParse(this.m_arguments["loanid"], out argLoanId))
                {
                    this.m_loanID = argLoanId;
                }

                Guid argAppId;

                if (Guid.TryParse(this.m_arguments["applicationid"], out argAppId))
                {
                    this.m_applicationID = argAppId;
                }
            }
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public bool HasPdf
        {
            get { return true; }
        }
        #endregion

        #region IPDFPrintItem Members


        public virtual bool HasPrintPermission
        {
            get { return true; }
        }

        #endregion

        #region IPDFPrintItem Members

        public IEnumerable<string> DependencyFields
        {
            get
            {
                List<string> fieldList = new List<string>();
                Type t = this.GetType();
                while (t != null)
                {

                    IEnumerable<string> _list = CPageData.GetCPageBaseAndCAppDataDependencyList(t);
                    foreach (string f in _list)
                    {
                        if (!fieldList.Contains(f))
                        {
                            fieldList.Add(f);
                        }
                    }
                    t = t.BaseType;
                }

                return fieldList;
            }
        }
        #endregion
    }
}
