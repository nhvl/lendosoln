using System;
using System.Collections;
using System.Collections.Specialized;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
	public class TX_NoticeOfPenaltiesPDF : AbstractLetterPDF
	{
		public override string PdfFile 
		{
			get { return "TX_NoticeOfPenalties.pdf"; }
		}
		public override string Description 
		{
			get { return "Texas - Notice of Penalties"; }
		}
		
		protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
		{
			AddFormFieldData("aBNm", dataApp.aBNm);
			AddFormFieldData("aCNm", dataApp.aCNm);
		}
	}
}