using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CRe883_1PDF : AbstractMldsPDF 
    {
        public override string PdfFile 
        {
            get { return "RE_883_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
    }
    public class CRe883_2PDF : AbstractMldsPDF 
    {
        public override string PdfFile 
        {
            get { return "RE_883_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            ApplyData(dataLoan, dataApp, true);
        }
    }
    public class CRe883_3PDF : AbstractMldsPDF 
    {
        public override string PdfFile 
        {
            get { return "RE_883_3.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 3"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
    }

    public class CRe883_ContinuationPDF : AbstractMldsPDF
    {
        public override string PdfFile
        {
            get { return "RE_Continuation.pdf"; }
        }
        public override string Description
        {
            get { return "RE 883 Continuation Sheet"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }

        public override bool IsVisible
        {
            get { return DataLoan.sHasReContinuationSheet; }
        }
    }
    public class CRe883PDF : AbstractBatchPDF
    {
        public override string Description 
        {
            get { return "CA Mortgage Loan Disclosure Statement/Good Faith Estimate (RE 883)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
 
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CRe883_1PDF(),
                                                 new CRe883_2PDF(),
                                                 new CRe883_3PDF(),
                                                 new CRe883_ContinuationPDF()
                                             };
            }
        }
    }
}
