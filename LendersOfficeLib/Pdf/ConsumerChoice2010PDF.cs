﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    public class CConsumerChoice2010PDF : AbstractLetterPDF
    {
        public override string Description
        {
            get { return "FHA Consumer Choice Disclosure Notice (2010)"; }
        }
        public override string PdfFile
        {
            get { return "ConsumerChoice2010.pdf"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);
        }
    }
}
