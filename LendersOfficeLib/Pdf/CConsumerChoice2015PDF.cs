﻿// <copyright file="CConsumerChoice2015PDF.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Kevin Gunnels
//  Date:   1/22/2015 5:00:39 PM
// </summary>
namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// FHA Consumer Choice PDF.
    /// </summary>
    public class CConsumerChoice2015PDF : AbstractLetterPDF
    {
        /// <summary>
        /// Gets the description of the PDF.
        /// </summary>
        /// <value>The description of the PDF.</value>
        public override string Description
        {
            get { return "FHA Consumer Choice Disclosure Notice"; }
        }

        /// <summary>
        /// Gets the file name of the PDF.
        /// </summary>
        /// <value>The file name of the PDF.</value>
        public override string PdfFile
        {
            get { return "ConsumerChoice2015.pdf"; }
        }

        /// <summary>
        /// Applies the data to the PDF.
        /// </summary>
        /// <param name="dataLoan">A loan object to pull data from.</param>
        /// <param name="dataApp">An app object to pull data from.</param>
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            this.AddFormFieldData("aBNm", dataApp.aBNm);
            this.AddFormFieldData("aCNm", dataApp.aCNm);
            this.AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);
        }
    }
}
