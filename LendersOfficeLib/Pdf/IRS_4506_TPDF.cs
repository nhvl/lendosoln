using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CIRS_4506_T_1PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "IRS-4506-T_1.pdf"; }
        }

        public override string Description
        {
            get { return "Page 1"; }
        }        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            if (this.BorrType == "C")
            {
                dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
            }

            AddFormFieldData("aB4506TNm", dataApp.aB4506TNmPdfDisplay);
            AddFormFieldData("aB4506TSsn", dataApp.aB4506TSsnTinEinCalc);
            AddFormFieldData("aC4506TNm", dataApp.aC4506TNmPdfDisplay);
            AddFormFieldData("aC4506TSsn", dataApp.aC4506TSsnTinEinCalc);
            AddFormFieldData("a4506TAddr", dataApp.a4506TAddr);
            AddFormFieldData("a4506TPrevAddr", dataApp.a4506TPrevAddr);
            AddFormFieldData("a4506TThirdPartyInfo", dataApp.a4506TThirdPartyInfo);
            AddFormFieldData("a4506TTranscript", dataApp.a4506TTranscript);
            AddFormFieldData("a4056TIsReturnTranscript", dataApp.a4056TIsReturnTranscript);
            AddFormFieldData("a4506TIsAccountTranscript", dataApp.a4506TIsAccountTranscript);
            AddFormFieldData("a4056TIsRecordAccountTranscript", dataApp.a4056TIsRecordAccountTranscript);
            AddFormFieldData("a4506TVerificationNonfiling", dataApp.a4506TVerificationNonfiling);
            AddFormFieldData("a4506TSeriesTranscript", dataApp.a4506TSeriesTranscript);

            if (!string.IsNullOrEmpty(dataApp.a4506TYear1_rep))
            {
                AddFormFieldData("a4506TMonth1", dataApp.a4506TYear1.ToString("MM"));
                AddFormFieldData("a4506TDay1", dataApp.a4506TYear1.ToString("dd"));
                AddFormFieldData("a4506TYear1", dataApp.a4506TYear1.ToString("yyyy"));
            }

            if (!string.IsNullOrEmpty(dataApp.a4506TYear2_rep))
            {
                AddFormFieldData("a4506TMonth2", dataApp.a4506TYear2.ToString("MM"));
                AddFormFieldData("a4506TDay2", dataApp.a4506TYear2.ToString("dd"));
                AddFormFieldData("a4506TYear2", dataApp.a4506TYear2.ToString("yyyy"));
            }

            if (!string.IsNullOrEmpty(dataApp.a4506TYear3_rep))
            {
                AddFormFieldData("a4506TMonth3", dataApp.a4506TYear3.ToString("MM"));
                AddFormFieldData("a4506TDay3", dataApp.a4506TYear3.ToString("dd"));
                AddFormFieldData("a4506TYear3", dataApp.a4506TYear3.ToString("yyyy"));
            }

            if (!string.IsNullOrEmpty(dataApp.a4506TYear4_rep))
            {
                AddFormFieldData("a4506TMonth4", dataApp.a4506TYear4.ToString("MM"));
                AddFormFieldData("a4506TDay4", dataApp.a4506TYear4.ToString("dd"));
                AddFormFieldData("a4506TYear4", dataApp.a4506TYear4.ToString("yyyy"));
            }

            AddFormFieldData("a4506TRequestYrHadIdentityTheft", dataApp.a4506TRequestYrHadIdentityTheft);
            AddFormFieldData("a4506TSignatoryAttested", dataApp.a4506TSignatoryAttested);
            AddFormFieldData("aBHPhone", dataApp.aHPhone);

        }

    }
    public class CIRS_4506_T_2PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "IRS-4506-T_2.pdf"; }
        }

        public override string Description
        {
            get { return "Page 2"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
    public class CIRS_4506_TPDF : AbstractBatchPDF
    {
        protected override E_AppPrintModeT AppPrintModeT
        {
            get
            {
                if (CurrentAppData.aIs4506TFiledTaxesSeparately)
                {
                    return E_AppPrintModeT.BorrowerAndSpouseSeparately;
                }
                else
                {
                    return E_AppPrintModeT.Joint;
                }
            }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/TaxReturn4506T.aspx"; }
        }
        public override string Description
        {
            get { return "Request for Transcript of Tax Return (4506-T)"; }
        }
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CIRS_4506_T_1PDF(),
                                                 new CIRS_4506_T_2PDF()
                                             };
            }
        }

    }
}
