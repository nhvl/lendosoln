﻿// <copyright file="FundingWorksheet2015.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   11/4/2015 4:15:52 PM
// </summary>
namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;

    /// <summary>
    /// Generates a Funding Worksheet form.
    /// </summary>
    public class CFundingWorksheet2015 : AbstractXsltPdf
    {
        /// <summary>
        /// Gets the page size that will be used in the form.
        /// </summary>
        /// <value>The page size.</value>
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize
        {
            get
            {
                return LendersOffice.PdfGenerator.PDFPageSize.Letter;
            }
        }

        /// <summary>
        /// Gets the description that will be shown in the print list.
        /// </summary>
        /// <value>The forms print list description.</value>
        public override string Description
        {
            get { return "Funding Worksheet"; }
        }

        /// <summary>
        /// Gets the location of the XLST that will be used to generate the form.
        /// </summary>
        /// <value>The namespace of the embedded XLST.</value>
        protected override string XsltEmbeddedResourceLocation
        {
            get
            {
                return "LendersOffice.Pdf.FundingWorksheet2015.xslt";
            }
        }

        /// <summary>
        /// Get the calculation description. Need to follow up with david on how some of these should work.
        /// </summary>
        /// <param name="loanData">The loan data.</param>
        /// <param name="fee">The fee to get a description for.</param>
        /// <returns>The calculation description.</returns>
        public string GetCalculationDescription(CPageData loanData, LoanClosingCostFee fee)
        {
            string description = string.Empty;

            switch (fee.FormulaT)
            {
                case E_ClosingCostFeeFormulaT.LeaveBlank:
                    throw new UnhandledEnumException(fee.FormulaT);
                case E_ClosingCostFeeFormulaT.Full:
                    description = string.Format("{0} + {1}", fee.Percent_rep, fee.BaseAmount_rep);
                    break;
                case E_ClosingCostFeeFormulaT.PercentOnly:
                    description = fee.Percent_rep;
                    break;
                case E_ClosingCostFeeFormulaT.MinBaseOnly:
                    break;
                case E_ClosingCostFeeFormulaT.MortgageInsurancePremiumRecurring:
                    description = loanData.sMipPiaMon_rep + " mo.";
                    break;
                case E_ClosingCostFeeFormulaT.MortgageInsurancePremiumUpfront:
                    break;
                case E_ClosingCostFeeFormulaT.VAFundingFee:
                    break;
                case E_ClosingCostFeeFormulaT.sHazInsPia:
                    break;
                case E_ClosingCostFeeFormulaT.sIPia:
                    description = string.Format("{0} per day from {1} to {2}", loanData.sIPerDay_rep, loanData.sConsummationD_rep, loanData.sPerDiemInterestEndD_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sLOrigFPc:
                    description = string.Format("{0} of Base + {1}", fee.Percent_rep, fee.BaseAmount_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sHazInsRsrv:
                    description = string.Format("{0} for {1} mo.", loanData.sProHazIns_rep, loanData.sHazInsRsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sMInsRsrv:
                    description = string.Format("{0} for {1} mo.", loanData.sProMIns_rep, loanData.sMInsRsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sRealETxRsrv:
                    description = string.Format("{0} for {1} mo.", loanData.sRealETxRsrv_rep, loanData.sRealETxRsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sSchoolTxRsrv:
                    description = string.Format("{0} for {1} mo.", loanData.sSchoolTxRsrv_rep, loanData.sSchoolTxRsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sFloodInsRsrv:
                    description = string.Format("{0} for {1} mo.", loanData.sFloodInsRsrv_rep, loanData.sFloodInsRsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sAggregateAdjRsrv:
                    description = string.Empty;
                    break;
                case E_ClosingCostFeeFormulaT.s1006Rsrv:
                    description = string.Format("{0} for {1} mo.", loanData.s1006Rsrv_rep, loanData.s1006RsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.s1007Rsrv:
                    description = string.Format("{0} for {1} mo.", loanData.s1007Rsrv_rep, loanData.s1007RsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sLDiscnt:
                    description = string.Format("{0} of Base", loanData.sGfeDiscountPointFPc_rep);
                    break;
                case E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing:
                    if (fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId)
                    {
                        description = string.Format("{0} per month for {1} mo.", loanData.sProMIns_rep, loanData.sMInsRsrvMon_rep);
                    }
                    else
                    {
                        description = GetDescriptionForHousingExpense(fee);
                    }
                    break;
                case E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses:
                    description = this.GetDescriptionForHousingExpense(fee);
                    break;
                case E_ClosingCostFeeFormulaT.sOriginatorCompensationTotalAmount:
                    description = string.Format("{0} of Base + {1}", loanData.sGfeOriginatorCompFPc_rep, loanData.sGfeOriginatorCompFMb_rep);
                    break;
                default:
                    throw new UnhandledEnumException(fee.FormulaT);
            }

            return description;
        }

        /// <summary>
        /// Writes the XML data needed to generate the HTML form.
        /// </summary>
        /// <param name="writer">The xml writer that will be used.</param>
        /// <param name="loanData">The loan data that the data will come out of.</param>
        /// <param name="dataApp">The app data that will be used.</param>
        protected override void GenerateXmlData(XmlWriter writer, CPageData loanData, CAppData dataApp)
        {
            FormatTarget originalTarget = loanData.GetFormatTarget();
            loanData.SetFormatTarget(FormatTarget.Webform);
            E_ClosingCostViewT formType;
            E_ClosingCostViewT sellerFormType;

            BrokerDB db = loanData.BrokerDB;

            string templateName = db.UseMergeGroupNameInsteadOfLoanProgramNameOnPrintedIFW ? loanData.sRateMergeGroupName : loanData.sLpTemplateNm;

            writer.WriteStartElement("FundingWorksheet");

            writer.WriteElementString("BrokerName", loanData.BrokerDB.Name);
            writer.WriteElementString("aBNm", dataApp.aBNm);
            writer.WriteElementString("sFinalLAmt", loanData.sFinalLAmt_rep);
            writer.WriteElementString("sLNm", loanData.sLNm);
            writer.WriteElementString("sWarehouseFunderDesc", loanData.sWarehouseFunderDesc);
            writer.WriteElementString("sWarehouseMaxFundAmt", loanData.sWarehouseMaxFundAmt_rep);
            writer.WriteElementString("sWarehouseMaxFundPc", loanData.sWarehouseMaxFundPc_rep);
            writer.WriteElementString("sConsummationD", loanData.sConsummationD_rep);
            writer.WriteElementString("sTrackingN", loanData.sTrackingN);
            writer.WriteElementString("sSettlementChargesDedFromLoanProc", loanData.sSettlementChargesDedFromLoanProc_rep);
            writer.WriteElementString("sSettlementTotalFundByLenderAtClosing", loanData.sSettlementTotalFundByLenderAtClosing_rep);
            writer.WriteElementString("sAdjustmentsOtherCreditsDeductedFromLoanProceeds", loanData.sAdjustmentsOtherCreditsDeductedFromLoanProceeds_rep);
            writer.WriteElementString("sAdjustmentsOtherCreditsFundedByLenderAtClosing", loanData.sAdjustmentsOtherCreditsFundedByLenderAtClosing_rep);
            writer.WriteElementString("sOtherFundAdj", loanData.sOtherFundAdj_rep);
            writer.WriteElementString("sAmtReqToFund", loanData.sAmtReqToFund_rep);
            writer.WriteElementString("sTotalAmtFund", loanData.sTotalAmtFund_rep);
            writer.WriteElementString("sAmtFundFromWarehouseLine", loanData.sAmtFundFromWarehouseLine_rep);
            writer.WriteElementString("sFundReqForShortfall", loanData.sFundReqForShortfall_rep);
            writer.WriteElementString("sChkDueFromClosing", loanData.sChkDueFromClosing_rep);
            writer.WriteElementString("sDisclosureRegulationT", loanData.sDisclosureRegulationT.ToString("D"));

            if (loanData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                formType = E_ClosingCostViewT.LoanClosingCost;
                sellerFormType = E_ClosingCostViewT.SellerResponsibleLenderTypeEstimate;
            }
            else
            {
                formType = E_ClosingCostViewT.LoanHud1;
                sellerFormType = E_ClosingCostViewT.SellerResponsibleLoanHud1;
            }

            decimal grossTotal = 0;
            decimal grossPaidToLender = 0;
            decimal grossPaidByLender = 0;

            writer.WriteStartElement("sections");

            IEnumerable<BorrowerClosingCostFeeSection> borrowerSections = loanData.sClosingCostSet.GetViewForSerialization(formType);
            IEnumerable<SellerClosingCostFeeSection> sellerSections = null;
            var isAtLeastV33 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(loanData.sLoanVersionT, LoanVersionT.V33_IncludeNonBorrowerResponsibleFeesInFundingWorksheet);

            if (isAtLeastV33)
            {
                sellerSections = loanData.sSellerResponsibleClosingCostSet.GetViewForSerialization(sellerFormType);
            }

            int index = 1;
            foreach (BorrowerClosingCostFeeSection section in borrowerSections)
            {
                var matchingSellerSection = sellerSections?.FirstOrDefault(s => s.SectionName == section.SectionName);

                writer.WriteStartElement("section");
                string[] parts = section.SectionName.Split('-');

                writer.WriteElementString("index", index.ToString());
                string description = parts.Last().Trim();
                if (description == "Other")
                {
                    description = "Other Closing Costs";
                }
                writer.WriteElementString("description", description);
                writer.WriteStartElement("fees");

                bool showPaidToTotal = false;
                bool showPaidByTotal = false;

                decimal sectionTotal = 0M;
                sectionTotal = loanData.m_convertLos.ToMoney(section.TotalAmount_rep);
                var feeList = section.FilteredClosingCostFeeList.ToList();

                if (isAtLeastV33 && matchingSellerSection != null)
                {
                    var sellerTotal = loanData.m_convertLos.ToMoney(matchingSellerSection.TotalAmount_rep);
                    sectionTotal = Tools.SumMoney(new List<decimal>() { sectionTotal, sellerTotal });
                    feeList = feeList.Concat(matchingSellerSection.FilteredClosingCostFeeList).ToList();
                    feeList.Sort(new BaseClosingCostFeeComparer(section.ClosingCostViewT));
                }

                if (section.SectionName.Contains("Origination Charges"))
                {
                    if (loanData.sClosingCostSet.StandaloneLenderOrigCompFee != null)
                    {
                        feeList.Add(loanData.sClosingCostSet.StandaloneLenderOrigCompFee);
                        sectionTotal = Tools.SumMoney(new List<decimal>() {sectionTotal, loanData.sClosingCostSet.StandaloneLenderOrigCompFee.TotalAmount });
                    }
                }

                grossTotal = Tools.SumMoney(new List<decimal>(){grossTotal, sectionTotal});
                decimal totalPaidToLender = 0;
                decimal totalPaidByLender = 0;

                foreach (LoanClosingCostFee fee in feeList)
                {
                    if (fee.TotalAmount == 0)
                    {
                        continue;
                    }

                    writer.WriteStartElement("fee");

                    writer.WriteElementString("description", fee.Description);
                    writer.WriteElementString("recipient", fee.BeneficiaryDescription);
                    writer.WriteElementString("calc", this.GetCalculationDescription(loanData, fee));

                    writer.WriteElementString("isapr", fee.IsApr ? "A" : string.Empty);

                    decimal paidByLenderAmt = 0;
                    IEnumerable<LoanClosingCostFeePayment> feePayments = fee.Payments;
                    foreach (LoanClosingCostFeePayment payment in feePayments)
                    {
                        if (payment.PaidByT == E_ClosingCostFeePaymentPaidByT.Lender)
                        {
                            paidByLenderAmt = Tools.SumMoney(new List<decimal> { paidByLenderAmt, payment.Amount });
                            writer.WriteElementString("paidByLenderAmt", loanData.m_convertLos.ToMoneyString(paidByLenderAmt, FormatDirection.ToRep));
                            showPaidByTotal = true;
                        }
                    }
                    totalPaidByLender = Tools.SumMoney(new List<decimal>() { totalPaidByLender, paidByLenderAmt });

                    if (fee.Dflp)
                    {
                        decimal paymentDFLPAmt = 0;
                        foreach (LoanClosingCostFeePayment payment in feePayments)
                        {
                            if (payment.GfeClosingCostFeePaymentTimingT != E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing)
                            {
                                paymentDFLPAmt += payment.Amount;
                                showPaidToTotal = true;
                            }
                        }

                        if (showPaidToTotal)
                        {
                            writer.WriteElementString("paidToLenderAmt", loanData.m_convertLos.ToMoneyString(paymentDFLPAmt, FormatDirection.ToRep));
                            totalPaidToLender = Tools.SumMoney(new List<decimal>() { totalPaidToLender, paymentDFLPAmt });
                        }
                    }

                    if (feePayments.Count() == 1)
                    {
                        string paidByDesc;
                        LoanClosingCostFeePayment payment = (LoanClosingCostFeePayment)feePayments.First();
                        E_ClosingCostFeePaymentPaidByT paidBy = payment.PaidByT;

                        switch (paidBy)
                        {
                            case E_ClosingCostFeePaymentPaidByT.Seller:
                                paidByDesc = "S";
                                break;
                            case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                            case E_ClosingCostFeePaymentPaidByT.Borrower:
                            case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                            case E_ClosingCostFeePaymentPaidByT.Lender:
                            case E_ClosingCostFeePaymentPaidByT.Other:
                                paidByDesc = string.Empty;
                                break;
                            case E_ClosingCostFeePaymentPaidByT.Broker:
                                paidByDesc = "B";
                                break;
                            default:
                                throw new UnhandledEnumException(paidBy);
                        }

                        bool ispoc = payment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing;
                        writer.WriteElementString("ispoc", ispoc ? "(poc)" : string.Empty);
                        writer.WriteElementString("paidby", paidByDesc);
                    }

                    writer.WriteElementString("total", fee.TotalAmount_rep);                    
                    writer.WriteEndElement(); // fee
                }
                writer.WriteEndElement(); // fees

                if (section.FilteredClosingCostFeeList.Count() > 0)
                {
                    writer.WriteElementString("total", loanData.m_convertLos.ToMoneyString(sectionTotal, FormatDirection.ToRep));

                    if (showPaidToTotal)
                    {
                        writer.WriteElementString("totalPaidToLender", loanData.m_convertLos.ToMoneyString(totalPaidToLender, FormatDirection.ToRep));
                    }

                    if (showPaidByTotal)
                    {
                        writer.WriteElementString("totalPaidByLender", loanData.m_convertLos.ToMoneyString(totalPaidByLender, FormatDirection.ToRep));
                    }
                }

                grossPaidByLender = Tools.SumMoney(new List<decimal>() { grossPaidByLender, totalPaidByLender });
                grossPaidToLender = Tools.SumMoney(new List<decimal>() { grossPaidToLender, totalPaidToLender });

                writer.WriteEndElement(); // section
            }

            writer.WriteEndElement(); // sections
            
            decimal adjTotal = 0;
            decimal adjDFLPTotal = 0;
            decimal adjpaidByLenderTotal = 0;
            
            if(loanData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                writer.WriteStartElement("adjustmentsList"); // adjustmentsList

                foreach (Adjustment adj in loanData.sAdjustmentList)
                {
                    if (adj.PaidFromParty == E_PartyT.Lender || adj.PaidToParty == E_PartyT.Lender)
                    {
                        writer.WriteStartElement("adjustment");
                        writer.WriteElementString("adjDesc", adj.Description);
                        writer.WriteElementString("adjTotalAmt", adj.Amount_rep);
                        adjTotal = Tools.SumMoney(new List<decimal>() { adjTotal, adj.Amount } );

                        if (adj.DFLP && adj.POC == false)
                        {
                            writer.WriteElementString("adjDFLPAmt", adj.Amount_rep);
                            adjDFLPTotal = Tools.SumMoney(new List<decimal>() { adjDFLPTotal, adj.Amount } );
                        }

                        if (adj.PaidFromParty == E_PartyT.Lender)
                        {
                            writer.WriteElementString("adjPaidByLenderAmt", adj.Amount_rep);
                            adjpaidByLenderTotal = Tools.SumMoney(new List<decimal>() { adjpaidByLenderTotal, adj.Amount } );
                        }

                        writer.WriteEndElement();
                    }
                }

                writer.WriteElementString("adjTotal", loanData.m_convertLos.ToMoneyString(adjTotal, FormatDirection.ToRep));
                writer.WriteElementString("adjDFLPTotal", loanData.m_convertLos.ToMoneyString(adjDFLPTotal, FormatDirection.ToRep));
                writer.WriteElementString("adjpaidByLenderTotal", loanData.m_convertLos.ToMoneyString(adjpaidByLenderTotal, FormatDirection.ToRep));

                writer.WriteEndElement(); // adjustmentsList
            }

            writer.WriteElementString("grossPaidByLender", loanData.m_convertLos.ToMoneyString(grossPaidByLender, FormatDirection.ToRep));
            writer.WriteElementString("grossPaidToLender", loanData.m_convertLos.ToMoneyString(grossPaidToLender, FormatDirection.ToRep));
            writer.WriteElementString("grossTotal", loanData.m_convertLos.ToMoneyString(grossTotal, FormatDirection.ToRep));

            decimal generalLenderCreditPaidByLender = Tools.SumMoney(new List<decimal>() { loanData.sSettlementTotalFundByLenderAtClosing, -loanData.sGfeTotalFundByLender, -loanData.sLenderPaidBrokerCompF, loanData.sGfeTotalFundByLenderPoc, loanData.sLenderPaidBrokerCompFPoc });
            decimal generalLenderCreditTotal = -1 * generalLenderCreditPaidByLender;
            writer.WriteElementString("generalLenderCreditPaidByLender", loanData.m_convertLos.ToMoneyString(generalLenderCreditPaidByLender, FormatDirection.ToRep));
            writer.WriteElementString("generalLenderCreditTotal", loanData.m_convertLos.ToMoneyString(generalLenderCreditTotal, FormatDirection.ToRep));

            decimal netTotal = Tools.SumMoney(new List<decimal>() { grossTotal, generalLenderCreditTotal });
            decimal netPaidByLender = Tools.SumMoney(new List<decimal>() { grossPaidByLender, generalLenderCreditPaidByLender });
            writer.WriteElementString("netTotal", loanData.m_convertLos.ToMoneyString(netTotal, FormatDirection.ToRep));
            writer.WriteElementString("netPaidByLender", loanData.m_convertLos.ToMoneyString(netPaidByLender, FormatDirection.ToRep));

            if (loanData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                decimal allTotal = Tools.SumMoney(new List<decimal>() { netTotal, adjTotal } );
                decimal allDFLPAmt = Tools.SumMoney(new List<decimal>() { grossPaidToLender, adjDFLPTotal } );
                decimal allPaidByLenderAmt = Tools.SumMoney(new List<decimal>() { netPaidByLender, adjpaidByLenderTotal } );

                writer.WriteElementString("allTotalAmt", loanData.m_convertLos.ToMoneyString(allTotal, FormatDirection.ToRep));
                writer.WriteElementString("allDFLPAmt", loanData.m_convertLos.ToMoneyString(allDFLPAmt, FormatDirection.ToRep));
                writer.WriteElementString("allPaidByLenderAmt", loanData.m_convertLos.ToMoneyString(allPaidByLenderAmt, FormatDirection.ToRep));
            }

            writer.WriteEndElement(); // funding worksheet
            loanData.SetFormatTarget(originalTarget);
        }

        /// <summary>
        /// Gets the description for Housing Expense fees.
        /// </summary>
        /// <param name="fee">The fee to get the description for.</param>
        /// <returns>The description of the fee.</returns>
        private string GetDescriptionForHousingExpense(LoanClosingCostFee fee)
        {
            // OPM 226095. mf. If the underlying data of the fee is a housing expense,
            // get the description data from the housing expense itself.
            string desc = string.Empty;

            var housingExpenseTypeT = ClosingCostSetUtils.GetHousingExpenseTypeTFromFeeId(fee.ClosingCostFeeTypeId);
            if (housingExpenseTypeT.HasValue)
            {
                BaseHousingExpense baseHousingExpense = null;

                if (housingExpenseTypeT.Value != E_HousingExpenseTypeT.Unassigned)
                {
                    baseHousingExpense = fee.GetHousingExpense(housingExpenseTypeT.Value);                    
                }
                else
                {
                    var expenseLine = ClosingCostSetUtils.GetLegacyCustomExpenseLineNumFromFeeTypeId(fee.ClosingCostFeeTypeId);

                    if (expenseLine.HasValue)
                    {
                        baseHousingExpense = fee.GetHousingExpense(expenseLine.Value);
                    }
                }

                if (baseHousingExpense != null)
                {
                    if (fee.FormulaT == E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing)
                    {
                        desc = string.Format("{0} per month for {1} mo.", baseHousingExpense.MonthlyAmtTotal_rep, baseHousingExpense.ReserveMonths_rep);
                    }
                    else if (fee.FormulaT == E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses)
                    {
                        desc = baseHousingExpense.PrepaidMonths_rep + " mo.";
                    }
                }
            }

            return desc;
        }
    }
}
