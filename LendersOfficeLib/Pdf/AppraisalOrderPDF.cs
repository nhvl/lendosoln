﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Linq;
    using System.Xml;
    using System.Web;
    using DataAccess;
    using LendersOffice.Security;
    using LendersOffice.PdfGenerator;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LendersOffice.Integration.Appraisals;
    public class CAppraisalOrderPDF : AbstractXsltPdf
    {
        private string GetSafeDate(string date)
        {
            if (!string.IsNullOrEmpty(date) && date == "1/1/1900")
            {
                return string.Empty;
            }
            else
            {
                return date;
            }
        }
        protected override void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp)
        {
            int fileNumber;
            if (!int.TryParse(Arguments["fileNumber"], out fileNumber))
            {
                // If there's no file number, we can't do anything
                return;
            }

            AbstractUserPrincipal p;
            if (HttpContext.Current != null)
            {
                p = (AbstractUserPrincipal)HttpContext.Current.User;
            }
            else
            {
                p = PrincipalFactory.CurrentPrincipal;
            }

            if (p == null)
            {
                return;
            }

            Guid vendorID = new Guid(Arguments["vendorID"]);
            var vendor = AppraisalVendorConfig.Retrieve(vendorID);

            string errorMessage;
            var credentials = vendor.GetGDMSCredentials(p.BrokerId, p.EmployeeId, out errorMessage);
            if (credentials == null)
            {
                return;
            }

            //string loanId = Arguments["loanId"];

            writer.WriteStartElement("APPRAISAL_ORDER");

            int processorId;
            int processorId2;
            int productId2;
            int productId3;
            int productId4;
            int productId5;
            using (var ordersClient = new OrdersClient(credentials)) {
                var order = ordersClient.GetOrder(fileNumber);

                writer.WriteElementString("sLAmtCalc", dataLoan.sLAmtCalc_rep);
                writer.WriteElementString("sPurchPrice", dataLoan.sPurchPrice_rep);

                writer.WriteElementString("FileNumber", order.FileNumber.ToString());
                writer.WriteElementString("CaseNumber", order.CaseNumber);
                writer.WriteElementString("LoanNumber", order.LoanNumber);

                writer.WriteElementString("ClientName", order.ClientName);
                writer.WriteElementString("ClientAddress1", order.ClientAddress1);
                writer.WriteElementString("ClientCity", order.ClientCity);
                writer.WriteElementString("ClientState", order.ClientState);
                writer.WriteElementString("ClientCSZ", order.ClientCSZ);
                processorId = order.ProcessorID;
                processorId2 = order.ProcessorID2;
                //writer.WriteElementString("ProcessorID", order.ProcessorID.ToString());
                //writer.WriteElementString("ProcessorID2", order.ProcessorID2.ToString());
                writer.WriteElementString("DateNeeded_String", order.DateNeeded_String);
                writer.WriteElementString("LenderName", order.LenderName);

                writer.WriteElementString("BorrowerName", order.BorrowerName);
                writer.WriteElementString("Address1", order.Address1);
                writer.WriteElementString("City", order.City);
                writer.WriteElementString("State", order.State);
                writer.WriteElementString("Zip", order.Zip);
                writer.WriteElementString("County", order.County);
                writer.WriteElementString("TownshipBoro", order.TownshipBoro);
                writer.WriteElementString("LegalDescription", order.LegalDescription);
                writer.WriteElementString("BorrowerEmail", order.BorrowerEmail);
                writer.WriteElementString("TotalLandArea", order.TotalLandArea);
                writer.WriteElementString("PropertyType", order.PropertyTypeName);
                writer.WriteElementString("FHAFlag", order.FHAFlag ? "Yes" : "No");
                writer.WriteElementString("ProductID", order.ProductName);
                productId2 = order.ProductID2;
                productId3 = order.ProductID3;
                productId4 = order.ProductID4;
                productId5 = order.ProductID5;
                //writer.WriteElementString("ProductID2", order.ProductID2.ToString());
                //writer.WriteElementString("ProductID3", order.ProductID3.ToString());
                //writer.WriteElementString("ProductID4", order.ProductID4.ToString());
                //writer.WriteElementString("ProductID5", order.ProductID5.ToString());
                writer.WriteElementString("LoanTypeName", order.LoanTypeName);

                writer.WriteElementString("EstimatedValue", order.EstimatedValue.ToString());
                writer.WriteElementString("Occupancy", order.OccupancyName);
                writer.WriteElementString("BillingMethodName", order.BillingMethodName);
                writer.WriteElementString("TILDate_String", GetSafeDate(order.TILDate_String));
                writer.WriteElementString("ContactName", order.ContactName);
                writer.WriteElementString("ContactNumber", order.ContactNumber);
                writer.WriteElementString("ContactWorkNumber", order.ContactWorkNumber);
                writer.WriteElementString("ContactOtherNumber", order.ContactOtherNumber);
                writer.WriteElementString("StatusName", order.StatusName);
                writer.WriteElementString("DateEstCompletion_String", GetSafeDate(order.DateEstCompletion_String));
                writer.WriteElementString("InspectionStartDate_String", GetSafeDate(order.InspectionStartDate_String));
                writer.WriteElementString("InspectionEndDate_String", GetSafeDate(order.InspectionEndDate_String));
                writer.WriteElementString("InspectionTime", order.InspectionTime);
                writer.WriteElementString("Notes", order.Notes);
                writer.WriteElementString("DateCompleted_String", GetSafeDate(order.DateCompleted_String));
                writer.WriteElementString("ProcessedBy", order.ProcessedBy);

                var orderHistory = new LendersOffice.GDMS.LookupMethods.OrderStatusHistory[] { };
                try
                {
                    orderHistory = ordersClient.GetOrderStatusHistory(fileNumber);
                }
                catch (NullReferenceException nre)
                {
                    Tools.LogWarning("No order history for fileNumber " + fileNumber, nre);
                }

                foreach (var orderHistoryItem in orderHistory)
                {
                    writer.WriteStartElement("StatusHistoryItem");
                    writer.WriteElementString("DateTimeStamp_String", orderHistoryItem.DateTimeStamp_String);
                    writer.WriteElementString("StatusName", orderHistoryItem.StatusName);
                    writer.WriteElementString("StatusDescription", orderHistoryItem.StatusDescription);
                    writer.WriteEndElement(); // </StatusHistoryItem>
                }
            }

            using (var lookupClient = new LookupMethodsClient(credentials))
            {
                var clientInfo = lookupClient.GetClientUsers(0)
                                             .ToDictionary(
                                                user => user.ClientUserID,
                                                user => user.UserFullName
                                             );

                if (clientInfo.ContainsKey(processorId))
                    writer.WriteElementString("ProcessorID", clientInfo[processorId]);

                if (clientInfo.ContainsKey(processorId2))
                    writer.WriteElementString("ProcessorID2", clientInfo[processorId2]);



                var products = lookupClient.GetProducts()
                                           .ToDictionary(
                                               prod => prod.ProductID,
                                               prod => prod.ProductName
                                           );

                if (products.ContainsKey(productId2))
                    writer.WriteElementString("ProductID2", products[productId2]);
                
                if (products.ContainsKey(productId3))
                    writer.WriteElementString("ProductID3", products[productId3]);

                if (products.ContainsKey(productId4))
                    writer.WriteElementString("ProductID4", products[productId4]);

                if (products.ContainsKey(productId5))
                    writer.WriteElementString("ProductID5", products[productId5]);
            }

            writer.WriteEndElement(); // </APPRAISAL_ORDER>
        }

        public override string Description
        {
            get { return "Appraisal Order"; }
        }

        public override PDFPageSize PageSize
        {
            get { return PDFPageSize.Letter; }
        }
    }
}
