﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    public class CCreditScoreDisclosureH5NoScorePDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosure_H5_NoScore.pdf"; }
        }

        public override string Description
        {
            get
            {
                return "Credit Score Disclosure - H-5 - Credit score is not available";
            }
        }
        public override string EditLink
        {
            get { return "/newlos/Disclosure/CreditScoreDisclosure.aspx"; }
        }
        public override E_AppPrintModeT AppPrintModeT
        {
            get
            {
                return E_AppPrintModeT.BorrowerAndSpouseSeparately;
            }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            if (BorrType == "C")
            {
                dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
            }

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditDisclosureLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("CreditScorePreparerInfo", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("aDecisionCreditSourceName", dataApp.aDecisionCreditSourceName);
        }
    }
}
