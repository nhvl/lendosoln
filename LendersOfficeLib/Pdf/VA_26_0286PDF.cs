using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{

    public abstract class AbstractVA_26_0286PDF : AbstractLetterPDF 
    {
		protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aAsstLiqTot",                          dataApp.aAsstLiqTot_rep);
            AddFormFieldData("aBDob",                                dataApp.aBDob_rep);
            AddFormFieldData("aBNm",                                 dataApp.aBNm);
            AddFormFieldData("aBFirstNm",                            dataApp.aBFirstNm);
            AddFormFieldData("aBGender",                             dataApp.aBGenderFallback);
            AddFormFieldData("aBHispanicT",                          dataApp.aBHispanicTFallback);
            AddFormFieldData("aBIsAmericanIndian",                   dataApp.aBIsAmericanIndian);
            AddFormFieldData("aBIsAsian",                            dataApp.aBIsAsian);
            AddFormFieldData("aBIsBlack",                            dataApp.aBIsBlack);
            AddFormFieldData("aBIsPacificIslander",                  dataApp.aBIsPacificIslander);
            AddFormFieldData("aBIsWhite",                            dataApp.aBIsWhite);
            AddFormFieldData("aBLastNm",                             dataApp.aBLastNm);
            AddFormFieldData("aBMidNm",                              dataApp.aBMidNm);
            AddFormFieldData("aBSsn",                                dataApp.aBSsn);
            AddFormFieldData("aBSuffix",                             dataApp.aBSuffix);
            AddFormFieldData("aVaCEmplmtI",                          dataApp.aVaCEmplmtI_rep);
            AddFormFieldData("aVaCEmplmtILckd",                      dataApp.aVaCEmplmtILckd);
            AddFormFieldData("aVaEntitleAmt",                        dataApp.aVaEntitleAmt_rep);
            AddFormFieldData("aVaEntitleCode",                       dataApp.aVaEntitleCode);
            AddFormFieldData("aVaFamilySuportGuidelineAmt",          dataApp.aVaFamilySuportGuidelineAmt_rep);
            AddFormFieldData("aVaFamilySupportBal",                  dataApp.aVaFamilySupportBal_rep);
            AddFormFieldData("aVaIsCoborIConsidered",                dataApp.aVaIsCoborIConsidered);
            AddFormFieldData("aVaIsVeteranFirstTimeBuyerTri",        dataApp.aVaIsVeteranFirstTimeBuyerTri);
            AddFormFieldData("aVaMilitaryStatT",                     dataApp.aVaMilitaryStatT);
            AddFormFieldData("aVaRatio",                             dataApp.aVaRatio_rep);
            AddFormFieldData("aVaServiceBranchT",                    dataApp.aVaServiceBranchT);
            AddFormFieldData("aVaTotMonGrossI",                      dataApp.aVaTotMonGrossI_rep);
            AddFormFieldData("sAgencyCaseNum",                       dataLoan.sAgencyCaseNum);
            AddFormFieldData("sApprVal",                             dataLoan.sApprVal_rep);
            AddFormFieldData("sClosedD",                             dataLoan.sClosedD_rep);
            AddFormFieldData("sVALenderIdCode",                     dataLoan.sVALenderIdCode);
            AddFormFieldData("sVASponsorAgentIdCode",               dataLoan.sVASponsorAgentIdCode);
            AddFormFieldData("sFinalLAmt",                           dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sIsProcessedUnderAutoUnderwritingTri", dataLoan.sIsProcessedUnderAutoUnderwritingTri);
            AddFormFieldData("sLenderCaseNum",                       dataLoan.sLenderCaseNum);
            AddFormFieldData("sNoteIR",                              dataLoan.sNoteIR_rep);
            AddFormFieldData("sPurchPrice",                          dataLoan.sPurchPrice_rep);
            AddFormFieldData("sSpAddr",                              dataLoan.sSpAddr);
            AddFormFieldData("sSpAge",                               dataLoan.sSpAge);
            AddFormFieldData("sSpBathCount",                         dataLoan.sSpBathCount);
            AddFormFieldData("sSpBedroomCount",                      dataLoan.sSpBedroomCount);
            AddFormFieldData("sSpCity",                              dataLoan.sSpCity);
            AddFormFieldData("sSpCounty",                            dataLoan.sSpCounty);
            AddFormFieldData("sSpLivingSqf",                         dataLoan.sSpLivingSqf);
            AddFormFieldData("sSpRoomCount",                         dataLoan.sSpRoomCount);
            AddFormFieldData("sSpState",                             dataLoan.sSpState);
            AddFormFieldData("sVaPriorLoanT", dataLoan.sVaPriorLoanT);
            // 5/11/2004 dd - On the printout. There are only 3 options, Neither, PUD, Condominium
            if (dataLoan.sSpT == E_sSpT.PUD || dataLoan.sSpT == E_sSpT.Condominium)
                AddFormFieldData("sSpT",                                 dataLoan.sSpT);
            else if (dataLoan.sSpT != E_sSpT.LeaveBlank)
                AddFormFieldData("sSpT", "0");

            AddFormFieldData("sSpZip",                               dataLoan.sSpZip);
            AddFormFieldData("sTerm",                                dataLoan.sTerm_rep);
            // 5/11/2004 dd - If number of units greater than 4 then check the last checkbox.
            if (dataLoan.sUnitsNum > 4)
                AddFormFieldData("sUnitsNum", "4");
            else
                AddFormFieldData("sUnitsNum",                            dataLoan.sUnitsNum_rep);
            AddFormFieldData("sVaApprT",                             dataLoan.sVaApprT);
            AddFormFieldData("sVaAppraisalOrSarAdjustmentTri",       dataLoan.sVaAppraisalOrSarAdjustmentTri);
            AddFormFieldData("sVaAutoUnderwritingT",                 dataLoan.sVaAutoUnderwritingT);
            AddFormFieldData("sVaEnergyImprovAmt",                   dataLoan.sVaEnergyImprovAmt_rep);
            AddFormFieldData("sVaEnergyImprovIsInsulation",          dataLoan.sVaEnergyImprovIsInsulation);
            AddFormFieldData("sVaEnergyImprovIsMajorSystem",         dataLoan.sVaEnergyImprovIsMajorSystem);
            AddFormFieldData("sVaEnergyImprovIsNewFeature",          dataLoan.sVaEnergyImprovIsNewFeature);
            AddFormFieldData("sVaEnergyImprovIsOther",               dataLoan.sVaEnergyImprovIsOther);
            AddFormFieldData("sVaEnergyImprovIsSolar",               dataLoan.sVaEnergyImprovIsSolar);
            AddFormFieldData("sVaEnergyImprovNone",                  dataLoan.sVaEnergyImprovNone);
            AddFormFieldData("sVaFinMethT",                          dataLoan.sVaFinMethT);
            AddFormFieldData("sVaFinMethTLckd",                      dataLoan.sVaFinMethTLckd);
            AddFormFieldData("sVaHybridArmT",                        dataLoan.sVaHybridArmT);
            AddFormFieldData("sVaIsAutoIrrrlProc",                   dataLoan.sVaIsAutoIrrrlProc);
            AddFormFieldData("sVaIsAutoProc",                        dataLoan.sVaIsAutoProc);
            AddFormFieldData("sVaIsPriorApprovalProc",               dataLoan.sVaIsPriorApprovalProc);
            AddFormFieldData("sVaLCodeT",                            dataLoan.sVaLCodeT);
            AddFormFieldData("sVaLCodeTLckd",                        dataLoan.sVaLCodeTLckd);
            AddFormFieldData("sVaLDiscntLckd",                       dataLoan.sVaLDiscntLckd);
            AddFormFieldData("sVaLDiscntPbbLckd",                    dataLoan.sVaLDiscntPbbLckd);
            AddFormFieldData("sVaLDiscntPbb",                        dataLoan.sVaLDiscntPbb_rep);
            AddFormFieldData("sVaLDiscntPcPbb",                      dataLoan.sVaLDiscntPcPbb_rep);
            AddFormFieldData("sVaLDiscntPc",                         dataLoan.sVaLDiscntPc_rep);
            AddFormFieldData("sVaLDiscnt",                           dataLoan.sVaLDiscnt_rep);
            AddFormFieldData("sVaLPurposeT",                         dataLoan.sVaLPurposeT);
            AddFormFieldData("sVaLPurposeTLckd",                     dataLoan.sVaLPurposeTLckd);
            AddFormFieldData("sVaLenSarId",                          dataLoan.sVaLenSarId);
            AddFormFieldData("sVaManufacturedHomeT",                 dataLoan.sVaManufacturedHomeT);
            AddFormFieldData("sVaMcrvNum",                           dataLoan.sVaMcrvNum);
            AddFormFieldData("sVaOwnershipT",                        dataLoan.sVaOwnershipT);
            AddFormFieldData("sVaPropDesignationT",                  dataLoan.sVaPropDesignationT);
            AddFormFieldData("sVaRiskT",                             dataLoan.sVaRiskT);
            AddFormFieldData("sVaSarNotifIssuedD",                   dataLoan.sVaSarNotifIssuedD_rep);
            AddFormFieldData("sVaStructureT",                        dataLoan.sVaStructureT);
            AddFormFieldData("sVaVetMedianCrScoreLckd",              dataLoan.sVaVetMedianCrScoreLckd);
            AddFormFieldData("sVaVetMedianCrScore",                  dataLoan.sVaVetMedianCrScore_rep);

            AddFormFieldData("sVaFfExemptTri", dataLoan.sVaFfExemptTri);
            AddFormFieldData("sVaIrrrlsUsedOnlyPdInFullLNum", dataLoan.sVaIrrrlsUsedOnlyPdInFullLNum);
            AddFormFieldData("sVaIrrrlsUsedOnlyOrigLAmt", dataLoan.sVaIrrrlsUsedOnlyOrigLAmt_rep);
            AddFormFieldData("sVaIrrrlsUsedOnlyOrigIR", dataLoan.sVaIrrrlsUsedOnlyOrigIR_rep);
            AddFormFieldData("sVaIrrrlsUsedOnlyRemarks", dataLoan.sVaIrrrlsUsedOnlyRemarks);
        }

    }

    public class CVA_26_0286_1PDF : AbstractVA_26_0286PDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-0286_1_08.pdf";        	 }
        }
	    
        public override string Description 
        {
            get { return "Page 1"; }
        }
    }

    public class CVA_26_0286_2PDF : AbstractVA_26_0286PDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-0286_2_08.pdf";        	 }
        }
	    
        public override string Description 
        {
            get { return "Page 2"; }
        }
    }

    public class CVA_26_0286PDF : AbstractBatchPDF 
    {
        public override string Description 
        {
            get { return "VA Loan Summary (VA 26-0286)"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/VA/VALoanSummary.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CVA_26_0286_1PDF(),
                                                 new CVA_26_0286_2PDF()
                                             };
            }
        }


    }
}
