﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Contains the options that can be configured by the user when generating
    /// a payment statement.
    /// </summary>
    public class PaymentStatementPdfOptions
    {
        /// <summary>
        /// Gets or sets the brokerId. 
        /// </summary>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the loanId.
        /// </summary>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the zero-based index of the payment in the payments list. 
        /// This is how we will identify the payment.
        /// </summary>
        /// <value>The zero-based index of the target servicing payment.</value>
        public int PaymentIndex { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the "Delinquency Notice" field should be included.
        /// </summary>
        /// <value>A value indicating whether the "Delinquency Notice" field should be included.</value>
        public bool IncludeDelinquencyNotice { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the "Suspense Account Notice" field should be included.
        /// </summary>
        /// <value>A value indicating whether the "Suspense Account Notice" field should be included.</value>
        public bool IncludeSuspenseAccountNotice { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the "Other Notices" field should be included.
        /// </summary>
        /// <value>A value indicating whether the "Other Notices" field should be included.</value>
        public bool IncludeOtherNotices { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we should append custom PDFs.
        /// </summary>
        /// <value>A value indicating whether we should append custom PDFs.</value>
        public bool AppendCustomPdfs { get; set; }

        /// <summary>
        /// Gets or sets the period start date.
        /// </summary>
        /// <value>The period start date.</value>
        public DateTime PeriodStart { get; set; }

        /// <summary>
        /// Gets or sets the period end date.
        /// </summary>
        /// <value>The period end date.</value>
        public DateTime PeriodEnd { get; set; }

        /// <summary>
        /// Gets or sets the potential late fee.
        /// </summary>
        /// <value>The potential late fee.</value>
        public decimal PotentialLateFee { get; set; }

        /// <summary>
        /// Gets or sets the late fee date.
        /// </summary>
        /// <value>The late fee date.</value>
        public DateTime LateFeeDate { get; set; }

        /// <summary>
        /// Gets or sets the custom pdf ids that should be appended to the main pdf.
        /// </summary>
        /// <value>The custom pdf ids that should be appended to the main pdf.</value>
        public IEnumerable<Guid> SelectedCustomPdfIds { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the payment statement should use
        /// the legal page size.
        /// </summary>
        /// <value>True if it should use legal size pages. False if it should use letter.</value>
        public bool UseLegalPageSize { get; set; }
    }
}
