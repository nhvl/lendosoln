﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public abstract class AbstractTax1098PDF : AbstractLetterPDF
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields tax1098 = dataLoan.GetPreparerOfForm(E_PreparerFormT.Tax1098, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("Tax1098Recipient", tax1098.CompanyName + Environment.NewLine + tax1098.StreetAddrMultiLines + Environment.NewLine + tax1098.Phone);
            AddFormFieldData("Tax1098TaxID", tax1098.TaxId);

            AddFormFieldData("sTax1098IsCorrected", dataLoan.sTax1098IsCorrected);

            AddFormFieldData("sTax1098PayerName", dataLoan.sTax1098PayerName);
            AddFormFieldData("sTax1098PayerAddr", dataLoan.sTax1098PayerAddr);
            AddFormFieldData("sTax1098PayerCity", dataLoan.sTax1098PayerCity);
            AddFormFieldData("sTax1098PayerState", dataLoan.sTax1098PayerState);
            AddFormFieldData("sTax1098PayerZip", dataLoan.sTax1098PayerZip);
            AddFormFieldData("sTax1098PayerSsn", dataLoan.sTax1098PayerSsn);

            AddFormFieldData("sTax1098AccountNum", dataLoan.sTax1098AccountNum);
            AddFormFieldData("sTax1098Interest", dataLoan.sTax1098Interest_rep);
            AddFormFieldData("sTax1098PointPaid", dataLoan.sTax1098PointPaid_rep);
            AddFormFieldData("sTax1098InterestRefund", dataLoan.sTax1098InterestRefund_rep);
            AddFormFieldData("sTax1098OtherInfo", dataLoan.sTax1098OtherInfo);
        }

    }

    public class CTax1098_1PDF : AbstractTax1098PDF
    {
        public override string PdfFile
        {
            get { return "Tax1098_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            base.ApplyData(dataLoan, dataApp);
            AddFormFieldData("sTax1098MIP", dataLoan.sTax1098MIP_rep);
        }
    }

    public class CTax1098_2PDF : AbstractTax1098PDF
    {

        public override string PdfFile
        {
            get { return "Tax1098_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CTaxForm_1098PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CTax1098_1PDF(),
                                                 new CTax1098_2PDF()
                                             };
            }

        }

        public override string Description
        {
            get { return "1098 Mortgage Interest Statement (2009)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/TaxForm_1098.aspx"; }
        }
    }

    #region Tax 1098 2010
    public class CTax1098_2010_1PDF : AbstractTax1098PDF
    {
        public override string PdfFile
        {
            get { return "Tax1098_2010_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            base.ApplyData(dataLoan, dataApp);
            AddFormFieldData("sTax1098MIP", dataLoan.sTax1098MIP_rep);
        }
    }

    public class CTax1098_2010_2PDF : AbstractTax1098PDF
    {

        public override string PdfFile
        {
            get { return "Tax1098_2010_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CTaxForm_1098_2010PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CTax1098_2010_1PDF(),
                                                 new CTax1098_2010_2PDF()
                                             };
            }

        }

        public override string Description
        {
            get { return "1098 Mortgage Interest Statement (2010)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/TaxForm_1098.aspx"; }
        }
    }
    #endregion

    #region Tax 1098 2011
    public class CTax1098_2011_1PDF : AbstractTax1098PDF
    {
        public override string PdfFile
        {
            get { return "Tax1098_2011_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            base.ApplyData(dataLoan, dataApp);
            AddFormFieldData("sTax1098MIP", dataLoan.sTax1098MIP_rep);
        }
    }

    public class CTax1098_2011_2PDF : AbstractTax1098PDF
    {

        public override string PdfFile
        {
            get { return "Tax1098_2011_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CTaxForm_1098_2011PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CTax1098_2011_1PDF(),
                                                 new CTax1098_2011_2PDF()
                                             };
            }

        }

        public override string Description
        {
            get { return "1098 Mortgage Interest Statement (2011)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/TaxForm_1098.aspx"; }
        }
    }
    #endregion

    #region Tax 1098 2012
    public class CTax1098_2012_1PDF : AbstractTax1098PDF
    {
        public override string PdfFile
        {
            get { return "Tax1098_2012_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
    }

    public class CTax1098_2012_2PDF : AbstractTax1098PDF
    {

        public override string PdfFile
        {
            get { return "Tax1098_2012_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CTaxForm_1098_2012PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CTax1098_2012_1PDF(),
                                                 new CTax1098_2012_2PDF()
                                             };
            }

        }

        public override string Description
        {
            get { return "1098 Mortgage Interest Statement (2012)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/TaxForm_1098.aspx"; }
        }
    }
    #endregion

    #region Tax 1098 2013
    public class CTax1098_2013_1PDF : AbstractTax1098PDF
    {
        public override string PdfFile
        {
            get { return "Tax1098_2013_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            base.ApplyData(dataLoan, dataApp);
            AddFormFieldData("sTax1098MIP", dataLoan.sTax1098MIP_rep);
        }
    }

    public class CTax1098_2013_2PDF : AbstractTax1098PDF
    {

        public override string PdfFile
        {
            get { return "Tax1098_2013_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CTaxForm_1098_2013PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CTax1098_2013_1PDF(),
                                                 new CTax1098_2013_2PDF()
                                             };
            }

        }

        public override string Description
        {
            get { return "1098 Mortgage Interest Statement (2013)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/TaxForm_1098.aspx"; }
        }
    }
    #endregion

    #region Tax 1098 2014
    public class CTax1098_2014_1PDF : AbstractTax1098PDF
    {
        public override string PdfFile
        {
            get { return "Tax1098_2014_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            base.ApplyData(dataLoan, dataApp);
            // 1/16/2014 gf - opm 106154 this may seem a little confusing,
            // but line 4 was formerly a line for MIP, now it's Other Info
            // so we are sending a different field id.
            AddFormFieldData("sTax1098MIP", dataLoan.sTax1098OtherInfo2);
        }
    }

    public class CTax1098_2014_2PDF : AbstractTax1098PDF
    {

        public override string PdfFile
        {
            get { return "Tax1098_2014_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CTaxForm_1098_2014PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CTax1098_2014_1PDF(),
                                                 new CTax1098_2014_2PDF()
                                             };
            }

        }

        public override string Description
        {
            get { return "1098 Mortgage Interest Statement (2014)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/TaxForm_1098.aspx"; }
        }
    }

    #endregion

    #region Tax 1098 2016
    public class CTax1098_2016_1PDF : AbstractTax1098PDF
    {
        public override string PdfFile
        {
            get { return "Tax1098_2016_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            base.ApplyData(dataLoan, dataApp);
            AddFormFieldData("sTax1098MIP", dataLoan.sTax1098MIP_rep);
            AddFormFieldData("sTax1098OutstandingPrincipal", dataLoan.sTax1098OutstandingPrincipal_rep);
            AddFormFieldData("sTax1098OriginationDate", dataLoan.sTax1098OriginationDate_rep);
            AddFormFieldData("sTax1098IsPropertyAddrBorrowerAddr", dataLoan.sTax1098IsPropertyAddrBorrowerAddr);
            if (!dataLoan.sTax1098IsPropertyAddrBorrowerAddr)
            {
                AddFormFieldData("PropertyAddress", dataLoan.sSpAddr_MultiLine);
                AddFormFieldData("sAssessorsParcelId", dataLoan.sAssessorsParcelId);
            }
        }
    }

    public class CTax1098_2016_2PDF : AbstractTax1098PDF
    {

        public override string PdfFile
        {
            get { return "Tax1098_2016_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CTaxForm_1098_2016PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CTax1098_2016_1PDF(),
                                                 new CTax1098_2016_2PDF()
                                             };
            }

        }

        public override string Description
        {
            get { return "1098 Mortgage Interest Statement (2016)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/TaxForm_1098.aspx"; }
        }
    }
    #endregion
    #region Tax 1098 2017
    public class CTax1098_2017_1PDF : AbstractTax1098PDF
    {
        public override string PdfFile
        {
            get { return "Tax1098_2017_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            base.ApplyData(dataLoan, dataApp);
            AddFormFieldData("sTax1098MIP", dataLoan.sTax1098MIP_rep);
            AddFormFieldData("sTax1098OutstandingPrincipal", dataLoan.sTax1098OutstandingPrincipal_rep);
            AddFormFieldData("sTax1098OriginationDate", dataLoan.sTax1098OriginationDate_rep);
            AddFormFieldData("sTax1098IsPropertyAddrBorrowerAddr", dataLoan.sTax1098IsPropertyAddrBorrowerAddr);
            AddFormFieldData("sTax1098NumberOfPropertiesSecuringThisMortgage", dataLoan.sTax1098NumberOfPropertiesSecuringThisMortgage_rep);

            if (!dataLoan.sTax1098IsPropertyAddrBorrowerAddr)
            {
                AddFormFieldData("PropertyAddress", dataLoan.sSpAddr_MultiLine);
                AddFormFieldData("sAssessorsParcelId", dataLoan.sAssessorsParcelId);
            }
        }
    }

    public class CTax1098_2017_2PDF : AbstractTax1098PDF
    {

        public override string PdfFile
        {
            get { return "Tax1098_2017_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CTaxForm_1098_2017PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CTax1098_2017_1PDF(),
                                                 new CTax1098_2017_2PDF()
                                             };
            }

        }

        public override string Description
        {
            get { return "1098 Mortgage Interest Statement (2017)"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/TaxForm_1098.aspx"; }
        }
    }
    #endregion

    #region Tax 1098 2018
    public class CTax1098_2018_1PDF : AbstractTax1098PDF
    {
        public override string PdfFile => "Tax1098_2018_1.pdf";

        public override string Description => "Page 1";

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            base.ApplyData(dataLoan, dataApp);

            AddFormFieldData("sTax1098MIP", dataLoan.sTax1098MIP_rep);
            AddFormFieldData("sTax1098OutstandingPrincipal", dataLoan.sTax1098OutstandingPrincipal_rep);
            AddFormFieldData("sTax1098OriginationDate", dataLoan.sTax1098OriginationDate_rep);
            AddFormFieldData("sTax1098IsPropertyAddrBorrowerAddr", dataLoan.sTax1098IsPropertyAddrBorrowerAddr);
            AddFormFieldData("sTax1098NumberOfPropertiesSecuringThisMortgage", dataLoan.sTax1098NumberOfPropertiesSecuringThisMortgage_rep);

            if (!dataLoan.sTax1098IsPropertyAddrBorrowerAddr)
            {
                AddFormFieldData("PropertyAddress", dataLoan.sSpAddr_MultiLine);
                AddFormFieldData("sAssessorsParcelId", dataLoan.sAssessorsParcelId);
            }
        }
    }

    public class CTax1098_2018_2PDF : AbstractTax1098PDF
    {
        public override string PdfFile => "Tax1098_2018_2.pdf";

        public override string Description => "Page 2";

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CTaxForm_1098_2018PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] 
                {
                    new CTax1098_2018_1PDF(),
                    new CTax1098_2018_2PDF()
                };
            }
        }

        public override string Description => "1098 Mortgage Interest Statement (2018)";

        public override string EditLink => "/newlos/Forms/TaxForm_1098.aspx";
    }
    #endregion
}
