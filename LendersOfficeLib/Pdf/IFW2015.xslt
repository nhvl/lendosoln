﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"
                doctype-public="-//W3C//DTD HTML 4.01//EN"
                doctype-system="http://www.w3.org/TR/html4/loose.dtd"
                encoding="utf-8"
                indent="yes"
                media-type="text/html"
                omit-xml-declaration="yes"
                version="4.01"/>

    <xsl:template match="sections">
        <xsl:for-each select="section">
        <table width="1200" border="0" class="marginbottom fees dontbreak">
            <colgroup>
                <col width="10"></col>
                <col width="200"></col>
                <col width="200"></col>
                <col width="225"></col>
                <col width="15"></col>
                <col width="30"></col>
                <col width="40"></col>
                <col width="80"></col>
            </colgroup>
            <thead>
                <tr>
                    <td colspan="7" class="sectionheader" width="600"  >
                        <xsl:value-of select="description"/>
                    </td>
                    <td class="sectionheader alignright" width="200"  >
                        <xsl:value-of select="total"/>
                    </td>
                </tr>
            </thead>
            <tbody>
                <xsl:for-each select="fees/fee">
                    <tr>
                        <td>&#160;</td>
                        <td class="feedesc"><xsl:value-of select="description"/></td>
                        <td class="feedesc"><xsl:value-of select="recipient"/></td>
                        <td><xsl:value-of select="calc"/></td>
                        <td><xsl:value-of select="isapr"/></td>
                        <td><xsl:value-of select="paidby"/></td>
                        <td><xsl:value-of select="ispoc"/></td>
                        <td class="alignright"><xsl:value-of select="total"/></td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="IFW">
        <html>
            <head>
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <title>Initial Fees Worksheet</title>
                <style type="text/css" media="all">
                  html, body { font-family: Verdana, Helvetica, sans-serif; font-size: 12pt;  }
                  .header { color: rgb(8,86,147); font-size: 20pt;  }
                  .center { text-align: center; }
                  .italic { font-style: italic; font-size:16pt }
                  .right { float: right; }
                  .left { float: left; }
                  .clear { clear:both; }
                  .alignright { text-align: right; }
                  .margintop { margin-top: 10px; }
                  .marginbottom { margin-bottom: 10px; }
                  .sectionheader { background-color: rgb(8,86,147); color: #FFF; font-weight: bold; padding: 3px;}
                  .sectionheaderblack { background-color: rgb(75,75,75); color: #FFF; font-weight: bold; padding: 3px;}
                  .feeheader { background-color: #ffeb8e; }
                  table { border-collapse: collapse;  }
                  .feedesc {   overflow:hidden; text-overflow: ellipsis; white-space: nowrap; }
                  table.fees { table-layout: fixed; }
                  .borderless td { border: none; }
                  .dontbreak { page-break-inside: avoid; }
                  .disclaimer2010 { padding-top: 20px; padding-bottom: 10px; }
                  .BlankRow { height: 20px; }
                </style>
            </head>
            <body>
                <table width="1200" border="0">
                    <thead>
                      <xsl:if test="show2015disclaimer='True'">
                        <tr>
                          <td class="center italic">
                            Your actual rate, payment, and costs could be higher. Get an official Loan Estimate before choosing a loan.
                          </td>
                        </tr>
                        <tr>
                          <td class="BlankRow"> </td>
                        </tr>
                      </xsl:if>
                      <tr>
                          <td class="header center">Initial Fees Worksheet</td>
                      </tr>
                      <tr><td class="BlankRow"> </td></tr>
                    </thead>
                    <tbody>
                        <table width="50%" border="0"  class="left">
                            <tr>
                                <td width="150">Borrower(s):</td>
                                <td>
                                    <xsl:value-of select="aBNm_aCNm"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Property Address:</td>
                                <td>
                                    <xsl:value-of select="sSpAddr_SingleLine"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Prepared by:</td>
                                <td>
                                    <xsl:value-of select="LenderName"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&#160;</td>
                                <td>
                                    <xsl:value-of select="LenderPhoneNum"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&#160;</td>
                                <td>
                                    <xsl:value-of select="LenderAddr"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&#160;</td>
                                <td>
                                    <xsl:value-of select="LenderCityStateZip"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Date Prepared:</td>
                                <td>
                                    <xsl:value-of select="PreparedDate"/>
                                </td>
                            </tr>
                          <tr><td class="BlankRow"> </td></tr>
                        </table>
                        <table  class="block right"  border="0"  >
                            <tr>
                                <td>Loan Number:</td>
                                <td>
                                    <xsl:value-of select="sLNm"/>
                                </td>
                            </tr>
                            <tr>
                              <td>Loan Program:</td>
                              <td>
                                <xsl:value-of select="sLpTemplateNm"/>
                              </td>
                            </tr>
                            <tr>
                              <td>Total Loan Amount:</td>
                              <td>
                                <xsl:value-of select="sFinalLAmt" />
                              </td>
                            </tr>
                            <tr>
                              <td>Loan Term:</td>
                              <td>
                                <xsl:value-of select="sTerm"/>/<xsl:value-of select="sDue"/> months
                              </td>
                            </tr>
                            <tr>
                              <td>Interest Rate:</td>
                              <td>
                                <xsl:value-of select="sNoteIR"/>
                              </td>
                            </tr>
                            <tr>
                              <td>APR:</td>
                              <td>
                                <xsl:value-of select="sApr" />
                              </td>
                            </tr>
                            <tr><td class="BlankRow"> </td></tr>
                        </table>
                        
                        <table class="clear margintop" width="1200"  border="0">
                            <xsl:if test="show2015disclaimer='False'">
                            <tr>
                              <td class="disclaimer2010" colspan="5">
                                The information provided below reflects estimates of the charges which you are likely to incur at the settlement of your loan. The fees listed are estimates; your actual charges may be more or less. Your transaction may not involve a fee for every item listed.
                              </td>
                            </tr>
                            </xsl:if>                         
                            <tr>
                                <td>
                                    A - APR
                                </td>
                                <td>
                                    S/B - Seller/Broker Paid
                                </td>
                            </tr>
                        </table>
                        <xsl:apply-templates select="sections"></xsl:apply-templates>

                      <table class="margintop borderless" width ="1200">
                        <tr style="border: 1px solid black;">
                          <td colspan="2">Estimated Gross Closing Costs</td>
                          <td class="alignright">
                            <xsl:value-of select="sTRIDLoanEstimateTotalAllCostsIncAA"></xsl:value-of>
                          </td>
                        </tr>
                        <tr>
                          <td width="10">&#160;</td>
                          <td>Lender Credit</td>
                          <td class="alignright">
                            <xsl:value-of select="sTRIDLoanEstimateLenderCredits_Neg"></xsl:value-of>
                          </td>
                        </tr>
                      </table>
                      
                      <table class="margintop borderless" width="1200">
                           <tr class="sectionheaderblack">
                                <td>
                                  Estimated Net Closing Costs
                                </td>
                                <td class="alignright">
                                    <xsl:value-of  select="sTRIDLoanEstimateTotalClosingCostsIncAA"/> 
                                </td>
                            </tr>
                        </table>

                        <table width="1200" border="0" class="margintop dontbreak">
                            <thead>
                                <tr class="sectionheader">
                                    <td colspan="9" >Total Estimated Funds Needed To Close</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>a.</td>
                                    <td>Purchase Price</td>
                                    <td align="right" ><xsl:value-of select="sPurchPrice"/></td>
                                    <td>&#160;&#160;j.</td>
                                    <td>Subordinate financing</td>
                                    <td align="right"><xsl:value-of select="sONewFinBal"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>Principal &amp; Interest</td>
                                    <td class="alignright"><xsl:value-of select="sProThisMPmt"/></td>
                                </tr>
                                <tr>
                                    <td>b.</td>
                                    <td>Alterations, improvements</td>
                                    <td align="right"><xsl:value-of select="sAltCost"/></td>
                                    <td>&#160;&#160;k.</td>
                                    <td>Closing costs paid by seller</td>
                                    <td align="right"><xsl:value-of select="sTotCcPbs"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>Other financing (P &amp; I)</td>
                                    <td class="alignright"><xsl:value-of select="sProOFinPmt"/></td>
                                </tr>
                                <tr>
                                    <td>c.</td>
                                    <td>Land</td>
                                    <td align="right"><xsl:value-of select="sLandCost"/></td>
                                    <td>&#160;&#160;l.</td>
                                    <td>Other credits</td>
                                    <td align="right">&#160;</td>
                                    <td>&#160;&#160;</td>
                                    <td>Hazard Insurance</td>
                                    <td class="alignright"><xsl:value-of select="sProHazIns"/></td>
                                </tr>
                                <tr>
                                    <td>d.</td>
                                    <td>Refi (incl debts to be paid off)</td>
                                    <td align="right"><xsl:value-of select="sRefPdOffAmt1003"/></td>
                                    <td>&#160;</td>
                                    <td><xsl:value-of select="sONewFinCcAsOCreditAmtDesc"/></td>
                                    <td align="right"><xsl:value-of select="sONewFinCcAsOCreditAmt"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>Real Estate Taxes</td>
                                    <td class="alignright"><xsl:value-of select="sProRealETx"/></td>
                                </tr>
                                <tr>
                                    <td>e.</td>
                                    <td>Estimated prepaid items</td>
                                    <td align="right"><xsl:value-of select="sTotEstPp1003"/></td>
                                    <td>&#160;</td>
                                    <td><xsl:value-of select="sOCredit1Desc"/></td>
                                    <td align="right"><xsl:value-of select="sOCredit1Amt"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>Mortgage Insurance</td>
                                    <td class="alignright"><xsl:value-of select="sProMIns"/></td>
                                </tr>
                                <tr>
                                    <td>f.</td>
                                    <td>Estimated closing costs</td>
                                    <td align="right"><xsl:value-of select="sTotEstCcNoDiscnt1003"/></td>
                                    <td>&#160;</td>
                                    <td><xsl:value-of select="sOCredit2Desc"/></td>
                                    <td align="right"><xsl:value-of select="sOCredit2Amt"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>Homeowner Assn. Dues</td>
                                    <td class="alignright"><xsl:value-of select="sProHoAssocDues"/></td>
                                </tr>
                                <tr>
                                    <td>g.</td>
                                    <td >PMI, MIP, Funding Fee</td>
                                    <td align="right"><xsl:value-of select="sFfUfmip1003"/></td>
                                    <td>&#160;</td>
                                    <td><xsl:value-of select="sOCredit3Desc"/></td>
                                    <td align="right"><xsl:value-of select="sOCredit3Amt"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>Other</td>
                                    <td class="alignright"><xsl:value-of select="sProOHExp"/></td>
                                </tr>
                                <tr>
                                    <td>h.</td>
                                    <td>Discount</td>
                                    <td align="right"><xsl:value-of select="sLDiscnt1003"/></td>
                                    <td>&#160;</td>
                                    <td><xsl:value-of select="sOCredit4Desc"/></td>
                                    <td align="right"><xsl:value-of select="sOCredit4Amt"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>&#160;</td>
                                    <td>&#160;</td>
                                </tr>
                                <tr>
                                    <td>i.</td>
                                    <td>Total Cost (sum a through h)</td>
                                    <td align="right"><xsl:value-of select="sTotTransC"/></td>
                                    <td>&#160;</td>
                                    <td>Lender Credit</td>
                                    <td align="right"><xsl:value-of select="sOCredit5Amt"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>&#160;</td>
                                    <td>&#160;</td>
                                </tr>
                                <tr>
                                    <td>&#160;</td>
                                    <td>&#160;</td>
                                    <td>&#160;</td>
                                    <td>&#160;&#160;m.</td>
                                    <td>Loan amount (exclude PMI, MIP)</td>
                                    <td align="right"><xsl:value-of select="sLAmt1003"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>&#160;</td>
                                    <td class="alignright">&#160;</td>
                                </tr>
                                <tr>
                                    <td>&#160;</td>
                                    <td>&#160;</td>
                                    <td>&#160;</td>
                                    <td>&#160;&#160;n.</td>
                                    <td>PMI, MIP, Funding Fee financed</td>
                                    <td align="right"><xsl:value-of select="sFfUfmipFinanced"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>&#160;</td>
                                    <td>&#160;</td>
                                </tr>
                                <tr>
                                    <td>&#160;</td>
                                    <td>&#160;</td>
                                    <td>&#160;</td>
                                    <td>&#160;&#160;o.</td>
                                    <td>Loan amount (add m &amp; n)</td>
                                    <td align="right"><xsl:value-of select="sFinalLAmt"/></td>
                                    <td>&#160;&#160;</td>
                                    <td>&#160;</td>
                                    <td>&#160;</td>
                                </tr>
                                <tr style="border: 1px solid black;">
                                    <td colspan="5">
                                        Total Est. Funds <xsl:value-of select="sTransNetCashFromToDesc"/> Borrower
                                    </td>
                                    <td align="right">
                                        <xsl:value-of select="sTransNetCashAbsVal"/>
                                    </td>
                                    <td>&#160;&#160;</td>
                                    <td >
                                        Total Monthly Payment
                                    </td>
                                    <td align="right">
                                        <xsl:value-of select="sMonthlyPmt"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>