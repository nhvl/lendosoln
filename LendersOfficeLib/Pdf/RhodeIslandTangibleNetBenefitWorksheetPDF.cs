﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CRhodeIslandTangibleNetBenefitWorksheetPDF : CTangibleNetBenefitWorksheetPDF
    {
        public override string Description
        {
            get { return "Rhode Island Tangible Net Benefit Worksheet"; }
        }
    }
}