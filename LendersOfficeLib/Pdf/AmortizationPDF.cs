namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using DataAccess;
    using LOPDFGenLib;

    public class CAmortizationPDF : AbstractFreeLayoutPDFFile
	{
        private const int COL0 = 40;
        private const int COL1 = 65;
        private const int COL2 = 140;
        private const int COL3 = 210;
        private const int COL4 = 300;
        private const int COL5 = 370;
        private const int COL6 = 440;
        private const int COL7 = 500;

        public override string Description 
        {
            get { return "Amortization Schedule"; }
        }

        protected override void AddTitle()
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 12;

            RowItem row = new RowItem(0, 40);
            row.Add(new TextItem(this.Description, 240, 0, font, fontSize));
            AddItem(row);
        }

        protected virtual E_AmortizationScheduleT ScheduleType
        {
            get { return E_AmortizationScheduleT.Standard; }
        }

        private decimal totalPrincipalPmts = 0;
        private decimal totalInterestPmts = 0;
        private decimal totalMortgageInsurancePmts = 0;

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddHeader(dataLoan, dataApp);
            AddTableHeader();
            AddAmortizationTable(dataLoan);
        }

        private void AddAmortizationTable(CPageData dataLoan) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 7;

            try
            {
                AmortTable table = dataLoan.GetAmortTable(this.ScheduleType, false);
                int count = 0;
                int numOfItemsInFirstPage = 62;
                int numOfItemsPerPage = 74;
                foreach (AmortItem item in table.Items) 
                {
                    string bal = item.Bal_rep;
                    if (bal == "")
                        bal = "0.00";

                    RowItem row = new RowItem();
                    row.Add(new TextItem(item.PmtIndex_rep, COL0, 0, 10, font, fontSize, HorizontalAlign.Right));
                    row.Add(new TextItem(item.DueDate_rep,  COL1, 0, 35, font, fontSize, HorizontalAlign.Right));
                    row.Add(new TextItem(item.Rate_rep, COL2, 0, 40, font, fontSize, HorizontalAlign.Right));
                    row.Add(new TextItem(item.Pmt_rep, COL3, 0, 50, font, fontSize, HorizontalAlign.Right));
                    row.Add(new TextItem(item.Eq_rep, COL4, 0, 30, font, fontSize, HorizontalAlign.Right));
                    row.Add(new TextItem(item.InterestPd_rep, COL5, 0, 30, font, fontSize, HorizontalAlign.Right));
                    row.Add(new TextItem(item.MI_rep, COL6, 0, 20, font, fontSize, HorizontalAlign.Right));
                    row.Add(new TextItem(bal, COL7, 0, 60, font, fontSize, HorizontalAlign.Right));

                    AddItem(row);
                    count++;

                    if ( count >= numOfItemsInFirstPage) 
                    {
                        if ((count - numOfItemsInFirstPage) % numOfItemsPerPage == 0) 
                        {
                            AddItem(new PageBreakItem());
                            AddTableHeader();

                        }
                    }

                    this.totalPrincipalPmts += item.Principal;
                    this.totalInterestPmts += item.Interest;
                    this.totalMortgageInsurancePmts += item.MI;
                }
          
                AddTotal(dataLoan, table);
            }
            catch
            {
                // do nothing here because interest rate, rate, or due are not valid causing the exception.
            }
        }

        private void AddTotal(CPageData dataLoan, AmortTable table) 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 7;
            RowItem row = new RowItem();
            row.Add(new TextItem("TOTAL", 30, 0, font, fontSize));

            Func<decimal, string> toMoneyString = (i) => dataLoan.m_convertLos.ToMoneyString(i, FormatDirection.ToRep);

            row.Add(new TextItem(toMoneyString(table.sSchedPmtTot), COL3, 0, 50, font, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem(toMoneyString(this.totalPrincipalPmts), COL4, 0, 30, font, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem(toMoneyString(this.totalInterestPmts), COL5, 0, 30, font, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem(toMoneyString(this.totalMortgageInsurancePmts), COL6, 0, 20, font, fontSize, HorizontalAlign.Right));

            AddItem(row);
        }

        private void AddHeader(CPageData dataLoan, CAppData dataApp) 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Loan Number: ", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sLNm, 120, 0, font, fontSize));
            row.Add(new TextItem("Date/Time: ", 450, 0, font, fontSize));
            row.Add(new TextItem(DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"), 500, 0, font, fontSize));

            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Borrower: " , 40, 0, font, fontSize));

            string name = dataApp.aBNm;
            if (dataApp.aCNm != "")
                name += " / " + dataApp.aCNm;
            row.Add(new TextItem(name, 120, 0, font, fontSize));

            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Property Address:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sSpAddr, 120, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem(Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip), 120, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Loan Amount:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sFinalLAmt_rep, 120, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Term / Due:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sTerm_rep + " / " + dataLoan.sDue_rep, 120, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Interest Rate:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sNoteIR_rep, 120, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Loan Program:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sLpTemplateNm, 120, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem(" ", 40, 0, font, fontSize));
            AddItem(row);
        }

        private void AddTableHeader() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("No.", COL0, 0, font, fontSize));
            row.Add(new TextItem("Pmt Date", COL1, 0, font, fontSize));
            row.Add(new TextItem("Interest Rate", COL2, 0, font, fontSize));
            row.Add(new TextItem("Monthly Payment", COL3, 0, font, fontSize));
            row.Add(new TextItem("Principal", COL4, 0, font, fontSize));
            row.Add(new TextItem("Interest", COL5, 0, font, fontSize));
            row.Add(new TextItem("MI", COL6, 0, font, fontSize));
            row.Add(new TextItem("Balance", COL7, 0, font, fontSize));

            AddItem(row);
            AddItem(new HorizontalLineItem());
        }
	}
}
