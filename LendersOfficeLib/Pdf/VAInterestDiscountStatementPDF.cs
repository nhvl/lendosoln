using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CVAInterestDiscountStatementPDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "VAInterestDiscountStatement.pdf";        	 }
        }
        public override string Description 
        {
            get { return "VA Interest Rate and Discount Statement"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
	}
}
