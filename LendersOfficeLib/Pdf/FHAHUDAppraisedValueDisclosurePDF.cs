using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CFHAHUDAppraisedValueDisclosurePDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "FHAHUDAppraisedValueDisclosure.pdf";        	 }
        }
        
        public override string Description 
        {
            get { return "FHA HUD Appraised Value Disclosure"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/FHA/FHAHUDAppraisedValueDisclosure.aspx"; }
        }
        

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            string applicants = dataApp.aBNm;
            if ("" != dataApp.aCNm)
                applicants += Environment.NewLine + dataApp.aCNm;
            AddFormFieldData("Applicants", applicants);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAppraisedValueDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("FHAAppraisedValueDisclosure", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, preparer.PhoneOfCompany));

        }
	}
}
