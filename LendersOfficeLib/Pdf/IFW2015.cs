﻿// <copyright file="IFW2015.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   5/1/2015 4:15:52 PM
// </summary>
namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using System.Xml;
    using LendersOffice.Admin;

    /// <summary>
    /// Generates a IFW Hud based form.
    /// </summary>
    public class CIFW2015 : AbstractXsltPdf
    {          
        /// <summary>
        /// Gets the location of the XLST that will be used to generate the form.
        /// </summary>
        /// <value>The namespace of the embedded XLST.</value>
        protected override string XsltEmbeddedResourceLocation
        {
            get
            {
                return "LendersOffice.Pdf.IFW2015.xslt";
            }
        }

        /// <summary>
        /// Gets the page size that will be used in the form.
        /// </summary>
        /// <value>The page size.</value>
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize
        {
            get
            {
                return LendersOffice.PdfGenerator.PDFPageSize.Letter;
            }
        }

        /// <summary>
        /// Writes the XML data needed to generate the HTML form.
        /// </summary>
        /// <param name="writer">The xml writer that will be used.</param>
        /// <param name="dataLoan">The loan data that the data will come out of.</param>
        /// <param name="dataApp">The app data that will be used.</param>
        protected override void GenerateXmlData(XmlWriter writer, CPageData loanData, CAppData dataApp)
        {
            FormatTarget originalTarget = loanData.GetFormatTarget();
            loanData.SetFormatTarget(FormatTarget.Webform);
            E_ClosingCostViewT formType;

            BrokerDB db = loanData.BrokerDB;

            string templateName = db.UseMergeGroupNameInsteadOfLoanProgramNameOnPrintedIFW ? loanData.sRateMergeGroupName : loanData.sLpTemplateNm;

            writer.WriteStartElement("IFW");

            writer.WriteElementString("sApr", loanData.sApr_rep);

            // spec'd in opm 217528
            bool show2015disclaimer = false;
            var messageTransitionTime = CPageBase.Trid2015EffectiveDate; // 9pm on 10/2/2015

            if (loanData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID || (loanData.sAppSubmittedD_rep == string.Empty && DateTime.Now > messageTransitionTime))
            {
                show2015disclaimer = true;
            }
            writer.WriteElementString("show2015disclaimer", show2015disclaimer.ToString());

            IPreparerFields preparer;
            if (loanData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                formType = E_ClosingCostViewT.LoanClosingCost;
                preparer = loanData.PreparerCollection.GetPreparerOfForm(E_PreparerFormT.TRIDClosingCosts_Lender, E_AgentRoleT.Lender, false);

                writer.WriteElementString("LenderName", preparer.CompanyName);
                writer.WriteElementString("LenderPhoneNum", preparer.PhoneOfCompany);
                writer.WriteElementString("LenderAddr", preparer.StreetAddr);
                writer.WriteElementString("LenderCityStateZip", preparer.CityStateZip);
            }
            else
            {
                formType = E_ClosingCostViewT.LoanHud1;
                preparer = loanData.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);

                writer.WriteElementString("LenderName", preparer.CompanyName);
                writer.WriteElementString("LenderPhoneNum", preparer.PhoneOfCompany);
                writer.WriteElementString("LenderAddr", preparer.StreetAddr);
                writer.WriteElementString("LenderCity", preparer.City);
                writer.WriteElementString("LenderState", preparer.State);
                writer.WriteElementString("LenderZip", preparer.Zip);
            }

            writer.WriteElementString("sDisclosureRegulationT", ((int)loanData.sDisclosureRegulationT).ToString());
            writer.WriteElementString("aBNm_aCNm", dataApp.aBNm_aCNm);
            writer.WriteElementString("sSpAddr_SingleLine", loanData.sSpAddr_SingleLine);
            writer.WriteElementString("sLNm", loanData.sLNm);
            writer.WriteElementString("PreparedDate", DateTime.Today.ToShortDateString());
            writer.WriteElementString("sLpTemplateNm", templateName);

            writer.WriteElementString("sPurchPrice", loanData.sPurchPrice_rep);
            writer.WriteElementString("sONewFinBal", loanData.sONewFinBal_rep);
            writer.WriteElementString("sAltCost", loanData.sAltCost_rep);
            writer.WriteElementString("sTotCcPbs", loanData.sTotCcPbs_rep);
            writer.WriteElementString("sProHazIns", loanData.sProHazIns_rep);
            writer.WriteElementString("sRefPdOffAmt1003", loanData.sRefPdOffAmt1003_rep);
            writer.WriteElementString("sONewFinCcAsOCreditAmtDesc", loanData.sONewFinCcAsOCreditAmtDesc);

            writer.WriteElementString("sProRealETx", loanData.sProRealETx_rep);
            writer.WriteElementString("sTotEstPp1003", loanData.sTotEstPp1003_rep);

            writer.WriteElementString("sOCredit1Desc", loanData.sOCredit1Desc);
            writer.WriteElementString("sOCredit2Desc", loanData.sOCredit2Desc);
            writer.WriteElementString("sOCredit3Desc", loanData.sOCredit3Desc);
            writer.WriteElementString("sOCredit4Desc", loanData.sOCredit4Desc);
            writer.WriteElementString("sOCredit5Desc", loanData.sOCredit5Desc);

            loanData.SetFormatTarget(FormatTarget.PrintoutNormalFields);
            writer.WriteElementString("sONewFinCcAsOCreditAmt", loanData.sONewFinCcAsOCreditAmt_rep);
            writer.WriteElementString("sOCredit1Amt", loanData.sOCredit1Amt_rep);
            writer.WriteElementString("sOCredit2Amt", loanData.sOCredit2Amt_rep);
            writer.WriteElementString("sOCredit3Amt", loanData.sOCredit3Amt_rep);
            writer.WriteElementString("sOCredit4Amt", loanData.sOCredit4Amt_rep);
            writer.WriteElementString("sOCredit5Amt", loanData.sOCredit5Amt_rep);

            loanData.SetFormatTarget(FormatTarget.Webform);

            writer.WriteElementString("sFfUfmipFinanced", loanData.sFfUfmipFinanced_rep);
            writer.WriteElementString("sLAmt1003", loanData.sLAmt1003_rep);
            writer.WriteElementString("sFinalLAmt", loanData.sFinalLAmt_rep);
            writer.WriteElementString("sNoteIR", loanData.sNoteIR_rep);
            writer.WriteElementString("sTerm", loanData.sTerm_rep);
            writer.WriteElementString("sDue", loanData.sDue_rep);
            writer.WriteElementString("sProMIns", loanData.sProMIns_rep);
            writer.WriteElementString("sTotEstCcNoDiscnt1003", loanData.sTotEstCcNoDiscnt1003_rep);
            writer.WriteElementString("sTotTransC", loanData.sTotTransC_rep);
            writer.WriteElementString("sTransNetCashFromToDesc", loanData.sTransNetCashFromToDesc.ToLower() == "to" ? "To" : "From");
            writer.WriteElementString("sTransNetCashAbsVal", loanData.sTransNetCashAbsVal_rep);
            writer.WriteElementString("sMonthlyPmt", loanData.sMonthlyPmt_rep);
            writer.WriteElementString("sLandCost", loanData.sLandCost_rep);
            writer.WriteElementString("sFfUfmip1003", loanData.sFfUfmip1003_rep);
            writer.WriteElementString("sLDiscnt1003", loanData.sLDiscnt1003_rep);
            writer.WriteElementString("sProThisMPmt", loanData.sProThisMPmt_rep);
            writer.WriteElementString("sProOFinPmt", loanData.sProOFinPmt_rep);
            writer.WriteElementString("sProHoAssocDues", loanData.sProHoAssocDues_rep);
            writer.WriteElementString("sProOHExp", loanData.sProOHExp_rep);
            writer.WriteElementString("sGfeTotalEstimateSettlementCharge", loanData.sGfeTotalEstimateSettlementCharge_rep);

            writer.WriteElementString("sTRIDLoanEstimateTotalClosingCostsIncAA", loanData.sTRIDLoanEstimateTotalClosingCostsIncAA_rep);
            writer.WriteElementString("sTRIDLoanEstimateLenderCredits_Neg", loanData.sTRIDLoanEstimateLenderCredits_Neg_rep);
            writer.WriteElementString("sTRIDLoanEstimateTotalAllCostsIncAA", loanData.sTRIDLoanEstimateTotalAllCostsIncAA_rep);

            writer.WriteStartElement("sections");

            IEnumerable<BorrowerClosingCostFeeSection> sections = loanData.sClosingCostSet.GetViewForSerialization(formType);

            int index = 1;
            foreach (BorrowerClosingCostFeeSection section in sections)
            {
                writer.WriteStartElement("section");
                string[] parts = section.SectionName.Split('-'); 

                writer.WriteElementString("index", index.ToString());
                writer.WriteElementString("description", parts.Last()); 
                writer.WriteElementString("total", section.TotalAmount_rep);
                writer.WriteElementString("total", section.TotalAmount_rep);
                writer.WriteStartElement("fees");

                foreach (BorrowerClosingCostFee fee in section.FilteredClosingCostFeeList)
                {
                    if (fee.TotalAmount == 0)
                    {
                        continue;
                    }

                    writer.WriteStartElement("fee");

                    writer.WriteElementString("description", fee.Description);
                    writer.WriteElementString("recipient", fee.BeneficiaryDescription);
                    writer.WriteElementString("calc", GetCalculationDescription(loanData, fee));
                    writer.WriteElementString("isapr", fee.IsApr ? "A" : string.Empty);

                    if (fee.Payments.Count() == 1)
                    {
                        string paidByDesc;
                        BorrowerClosingCostFeePayment payment = (BorrowerClosingCostFeePayment)fee.Payments.First();
                        E_ClosingCostFeePaymentPaidByT paidBy = payment.PaidByT;

                        switch (paidBy)
                        {
                            case E_ClosingCostFeePaymentPaidByT.Seller:
                                paidByDesc = "S";
                                break;
                            case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                            case E_ClosingCostFeePaymentPaidByT.Borrower:
                            case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                            case E_ClosingCostFeePaymentPaidByT.Lender:
                            case E_ClosingCostFeePaymentPaidByT.Other:
                                paidByDesc = string.Empty;
                                break;
                            case E_ClosingCostFeePaymentPaidByT.Broker:
                                paidByDesc = "B";
                                break;
                            default:
                                throw new UnhandledEnumException(paidBy);
                        }

                        bool ispoc = payment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing;
                        writer.WriteElementString("ispoc", ispoc ? "(poc)" : string.Empty);
                        writer.WriteElementString("paidby", paidByDesc);
                    }

                    writer.WriteElementString("total", fee.TotalAmount_rep);
                    writer.WriteEndElement(); // fee
                }

                writer.WriteEndElement(); // fees
                writer.WriteEndElement(); // section
            }
            writer.WriteEndElement(); // sections

            writer.WriteEndElement(); // ifw
            loanData.SetFormatTarget(originalTarget);
        }

        /// <summary>
        /// Gets the description that will be shown in the print list.
        /// </summary>
        /// <value>The forms print list description.</value>
        public override string Description
        {
            get { return "Initial Fees Worksheet"; }
        }

        /// <summary>
        /// Get the calculation description. Need to follow up with david on how some of these should work.
        /// </summary>
        /// <param name="loanData">The loan data.</param>
        /// <param name="fee">The fee to get a description for.</param>
        /// <returns>The calculation description.</returns>
        public string GetCalculationDescription(CPageData loanData, BorrowerClosingCostFee fee)
        {
            string description = string.Empty;

            switch (fee.FormulaT)
            {
                case E_ClosingCostFeeFormulaT.LeaveBlank:
                    throw new UnhandledEnumException(fee.FormulaT);
                case E_ClosingCostFeeFormulaT.Full:
                    description = string.Format("{0} + {1}", fee.Percent_rep, fee.BaseAmount_rep);
                    break;
                case E_ClosingCostFeeFormulaT.PercentOnly:
                    description = fee.Percent_rep;
                    break;
                case E_ClosingCostFeeFormulaT.MinBaseOnly:
                    break;
                case E_ClosingCostFeeFormulaT.MortgageInsurancePremiumRecurring:
                    description = loanData.sMipPiaMon_rep + " mo.";
                    break;
                case E_ClosingCostFeeFormulaT.MortgageInsurancePremiumUpfront:
                    break;
                case E_ClosingCostFeeFormulaT.VAFundingFee:
                    break;
                case E_ClosingCostFeeFormulaT.sHazInsPia:
                    break;
                case E_ClosingCostFeeFormulaT.sIPia:
                    description = string.Format("{0} per day for {1} days", loanData.sIPerDay_rep, loanData.sIPiaDy_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sLOrigFPc:
                    description = string.Format("{0} of Base + {1}", fee.Percent_rep, fee.BaseAmount_rep); 
                    break;
                case E_ClosingCostFeeFormulaT.sHazInsRsrv:
                    description = string.Format("{0} for {1} mo.", loanData.sProHazIns_rep, loanData.sHazInsRsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sMInsRsrv:
                    description = string.Format("{0} for {1} mo.", loanData.sProMIns_rep, loanData.sMInsRsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sRealETxRsrv:
                    description = string.Format("{0} for {1} mo.", loanData.sRealETxRsrv_rep, loanData.sRealETxRsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sSchoolTxRsrv:
                    description = string.Format("{0} for {1} mo.", loanData.sSchoolTxRsrv_rep, loanData.sSchoolTxRsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sFloodInsRsrv:
                    description = string.Format("{0} for {1} mo.", loanData.sFloodInsRsrv_rep, loanData.sFloodInsRsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sAggregateAdjRsrv:
                    description = string.Empty;
                    break;
                case E_ClosingCostFeeFormulaT.s1006Rsrv:
                    description = string.Format("{0} for {1} mo.", loanData.s1006Rsrv_rep, loanData.s1006RsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.s1007Rsrv:
                    description = string.Format("{0} for {1} mo.", loanData.s1007Rsrv_rep, loanData.s1007RsrvMon_rep);
                    break;
                case E_ClosingCostFeeFormulaT.sLDiscnt:
                    description = string.Format("{0} of {1}", loanData.sGfeDiscountPointFPc_rep, loanData.sLDiscntBaseAmt_rep);
                    break;
                case E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing:
                    if (fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId)
                    {
                        description = string.Format("{0} per month for {1} mo.", loanData.sProMIns_rep, loanData.sMInsRsrvMon_rep);
                    }
                    else
                    {
                        description = GetDescriptionForHousingExpense(fee);
                    }
                    break;
                case E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses:
                    description = GetDescriptionForHousingExpense(fee);
                    break;
                case E_ClosingCostFeeFormulaT.sOriginatorCompensationTotalAmount:
                    description = string.Format("{0} of Base + {1}", loanData.sGfeOriginatorCompFPc_rep, loanData.sGfeOriginatorCompFMb_rep);
                    break;
                default:
                    throw new UnhandledEnumException(fee.FormulaT);
            }

            return description;
        }

        private string GetDescriptionForHousingExpense(BorrowerClosingCostFee fee)
        {
            // OPM 226095. mf. If the underlying data of the fee is a housing expense,
            // get the description data from the housing expense itself.
            string desc = string.Empty;

            var housingExpenseTypeT = ClosingCostSetUtils.GetHousingExpenseTypeTFromFeeId(fee.ClosingCostFeeTypeId);
            if (housingExpenseTypeT.HasValue)
            {
                BaseHousingExpense baseHousingExpense = null;
                if (housingExpenseTypeT.Value != E_HousingExpenseTypeT.Unassigned)
                {
                    baseHousingExpense = fee.GetHousingExpense(housingExpenseTypeT.Value);
                }
                else
                {
                    var expenseLine = ClosingCostSetUtils.GetLegacyCustomExpenseLineNumFromFeeTypeId(fee.ClosingCostFeeTypeId);
                    if (expenseLine.HasValue)
                    {
                        baseHousingExpense = fee.GetHousingExpense(expenseLine.Value);
                    }
                }
                if (baseHousingExpense != null)
                {
                    if (fee.FormulaT == E_ClosingCostFeeFormulaT.InitialEscrowPaymentAtClosing)
                    {
                        desc = string.Format("{0} per month for {1} mo.", baseHousingExpense.MonthlyAmtTotal_rep, baseHousingExpense.ReserveMonths_rep);
                    }
                    else if (fee.FormulaT == E_ClosingCostFeeFormulaT.PrepaidAmountForExpenses)
                    {
                        desc = baseHousingExpense.PrepaidMonths_rep + " mo.";
                    }
                }
            }

            return desc;
        }

    }
}
