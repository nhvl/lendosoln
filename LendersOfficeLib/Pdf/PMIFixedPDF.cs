using System;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CPMIFixedPDF : AbstractLetterPDF
    {

        public override string PdfFile 
        {
            get { return "PrivateMortgageInsuranceDisclosureFixed.pdf"; }
        }

        public override string Description 
        {
            get { return "PMI Disclosure - Fixed Rate Mortgages"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("GfeTilPrepareDate",  dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject ).PrepareDate_rep );
            AddFormFieldData("sSpAddr", dataLoan.sSpAddr);
            AddFormFieldData("PropertyAddress2", Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sLoanBalanceReach78LtvD", dataLoan.sLoanBalanceReach78LtvD_rep);
            AddFormFieldData("sLoanBalanceReach80LtvD", dataLoan.sLoanBalanceReach80LtvD_rep);
        }

    }
}
