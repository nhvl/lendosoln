using System;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CRealEstateDisclosurePDF : AbstractLetterPDF
	{

		public override string EditLink
		{
			get { return "/newlos/Disclosure/RealEstateDisclosure.aspx"; }
		}
        public override string PdfFile 
        {
            get { return "RealEstateDisclosure.pdf"; }
        }
        public override string Description 
        {
            get { return "CA Real Estate Disclosure"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.RealEstateDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (broker.IsValid) 
            {
                AddFormFieldData("BrokerName", broker.CompanyName);
                AddFormFieldData("LicenseNumber", broker.LicenseNumOfCompany);

                string phone = "";
                if (broker.PhoneOfCompany.TrimWhitespaceAndBOM() != "") 
                {
                    phone = "Phone: " + broker.PhoneOfCompany + Environment.NewLine;
                }
                if (broker.FaxOfCompany.TrimWhitespaceAndBOM() != "") 
                {
                    phone += "Fax: " + broker.FaxOfCompany;
                }
                AddFormFieldData("BrokerAddress", Tools.FormatAddress(broker.CompanyName, broker.StreetAddr, broker.City, broker.State, broker.Zip, phone));
                AddFormFieldData("aBNm", dataApp.aBNm);
                AddFormFieldData("aCNm", dataApp.aCNm);
            }
        }
	}
}
