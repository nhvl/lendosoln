using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Xml;
using System.Reflection;
using LendersOffice.PdfGenerator;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class PDFClassHashTable 
    {
        private static Hashtable s_hashtable;
        private static Hashtable s_nameHashtable;
        private static Hashtable s_constructorHashtable;

        private static void Initialize() 
        {
            s_hashtable = new Hashtable();
            s_nameHashtable = new Hashtable();
            s_constructorHashtable = new Hashtable();
        }

        public static Type GetType(string typeName) 
        {
            if (null == s_hashtable) Initialize();

            Type t = (Type) s_hashtable[typeName];
            if (null == t) 
            {
                t = Type.GetType(typeName, throwOnError: false, ignoreCase: true);
                if (null == t)
                    throw new ArgumentException(typeName + " is not a valid type.");

                s_hashtable[typeName] = t;
            }
            return t;
        }
        /// <summary>
        /// Return the Name property of PDF class.
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static string GetName(string typeName) 
        {
            if (null == s_nameHashtable) Initialize();

            string name = (string) s_nameHashtable[typeName];

            if (null == name) 
            {
                Type t = GetType(typeName);
                FieldInfo f = t.GetField("Name", BindingFlags.Static | BindingFlags.Public);
                if (f != null) 
                {
                    name = (string) f.GetValue(null);
                    s_nameHashtable[typeName] = name;
                }
            }
            return name;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static ConstructorInfo GetConstructor(string typeName) 
        {
            if (null == s_constructorHashtable) Initialize();

            ConstructorInfo constructor = (ConstructorInfo) s_constructorHashtable[typeName];

            if (null == constructor) 
            {
                Type t = GetType(typeName);
                constructor = t.GetConstructor(new Type[0]);
                if (constructor == null)
                    throw new CBaseException( ErrorMessages.Generic, typeName + " does not have default construction.");
                s_constructorHashtable[typeName] = constructor;

            }
            return constructor;

        }

    }

}
