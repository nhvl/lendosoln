﻿namespace LendersOffice.Pdf
{
    using System;
    using DataAccess;

    /// <summary>
    /// Abstract class for 203k Maximum Mortgage Transaction PDFs.
    /// </summary>
    public abstract class Abstract203kMaxMortgagePdf : AbstractLegalPDF
    {
        /// <summary>
        /// Edit Link.
        /// </summary>
        public override string EditLink
        {
            get
            {
                return "/newlos/Renovation/FHA203kMaximumMortgage.aspx";
            }
        }

        /// <summary>
        /// Applies Data to PDF.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="dataApp">The data app.</param>
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            // Shared
            this.AddFormFieldData("sDiscntPointsOnRepairCostFeeInitDraw", dataLoan.sDiscntPointsOnRepairCostFeeInitDraw_rep);
            this.AddFormFieldData("sBorrOwnFundsContingencyRsrv", dataLoan.sBorrOwnFundsContingencyRsrv_rep);
            this.AddFormFieldData("sTotalRehabEscrowAmount", dataLoan.sTotalRehabEscrowAmount_rep);
            this.AddFormFieldData("sTotalRenovationCosts", dataLoan.sTotalRenovationCosts_rep);
            this.AddFormFieldData("sTotalRehabCostPlusAdjAsIsValue", dataLoan.sTotalRehabCostPlusAdjAsIsValue_rep);
            this.AddFormFieldData("s203kFinalMaxBaseLoanAmount", dataLoan.s203kFinalMaxBaseLoanAmount_rep);
            this.AddFormFieldData("s203kFHASpAdjAsIsValue", dataLoan.s203kFHASpAdjAsIsValue_rep);
            this.AddFormFieldData("sFHAMipLtv", dataLoan.sFHAMipLtv_rep);
            this.AddFormFieldData("s203kIntermediateMaxBaseLoanAmount", dataLoan.s203kIntermediateMaxBaseLoanAmount_rep);
            this.AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            this.AddFormFieldData("sFinanceOriginationFee", dataLoan.sFinanceOriginationFee_rep);
            this.AddFormFieldData("sCostConstrRepairsRehab", dataLoan.sCostConstrRepairsRehab_rep);
            this.AddFormFieldData("s50PcMatCostOrderedButNotPaidInitDraw", dataLoan.s50PcMatCostOrderedButNotPaidInitDraw_rep);
            this.AddFormFieldData("sLtv203KMaxMortgageEligibility", dataLoan.sLtv203KMaxMortgageEligibility_rep);
            this.AddFormFieldData("sDiscntPntOnRepairCostFee", dataLoan.sDiscntPntOnRepairCostFee_rep);
            this.AddFormFieldData("sFHAAdjPropTypeAfterImproveValue", dataLoan.sFHAAdjPropTypeAfterImproveValue_rep);
            this.AddFormFieldData("sNationWideMortgageLimitBy120Pc", dataLoan.sNationWideMortgageLimitBy120Pc_rep);
            this.AddFormFieldData("sTitleUpdateFee", dataLoan.sTitleUpdateFee_rep);
            this.AddFormFieldData("s203KRehabEscrowInitDrawAmt", dataLoan.s203KRehabEscrowInitDrawAmt_rep);
            this.AddFormFieldData("sFHASpAsIsVal", dataLoan.sFHASpAsIsVal_rep);
            this.AddFormFieldData("sRepairImprovementCostFee", dataLoan.sRepairImprovementCostFee_rep);
            this.AddFormFieldData("sAdjSolarEnergyAmount", dataLoan.sAdjSolarEnergyAmount_rep);
            this.AddFormFieldData("sRenovationConstrInspectFee", dataLoan.sRenovationConstrInspectFee_rep);
            this.AddFormFieldData("sEnergyImprovementAmount", dataLoan.sEnergyImprovementAmount_rep);
            this.AddFormFieldData("sSolarEnergySystemCost", dataLoan.sSolarEnergySystemCost_rep);
            this.AddFormFieldData("sFHA203ProductLtvMaxMortgageAndMinAppraisedValue", dataLoan.sFHA203ProductLtvMaxMortgageAndMinAppraisedValue_rep);
            this.AddFormFieldData("sFinanceContingencyRsrv", dataLoan.sFinanceContingencyRsrv_rep);
            this.AddFormFieldData("sFHASpAfterImproveValBy20Pc", dataLoan.sFHASpAfterImproveValBy20Pc_rep);
            this.AddFormFieldData("sNationWideMortgageLimit", dataLoan.sNationWideMortgageLimit_rep);
            this.AddFormFieldData("sPermitFee", dataLoan.sPermitFee_rep);
            this.AddFormFieldData("sOriginationFeeInitDraw", dataLoan.sOriginationFeeInitDraw_rep);
            this.AddFormFieldData("sPermitFeeInitDraw", dataLoan.sPermitFeeInitDraw_rep);
            this.AddFormFieldData("s203kInitMaxBaseLoanAmount", dataLoan.s203kInitMaxBaseLoanAmount_rep);
            this.AddFormFieldData("s203KRehabEscrowAmtTot", dataLoan.s203KRehabEscrowAmtTot_rep);
            this.AddFormFieldData("sFinanceMortgageFee", dataLoan.sFinanceMortgageFee_rep);
            this.AddFormFieldData("sPurchPriceLessInducement", dataLoan.sPurchPriceLessInducement_rep);
            this.AddFormFieldData("sFHA203kLeadPaintCredit", dataLoan.sFHA203kLeadPaintCredit_rep);
            this.AddFormFieldData("sInducementPurchPrice", dataLoan.sInducementPurchPrice_rep);
            this.AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            this.AddFormFieldData("sFinanceMortgagePmntRsrv", dataLoan.sFinanceMortgagePmntRsrv_rep);
            this.AddFormFieldData("sArchitectEngineerFeeInitDraw", dataLoan.sArchitectEngineerFeeInitDraw_rep);
            this.AddFormFieldData(
                "sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan",
                dataLoan.sFHA203kSum2AExistingDebtOnTheSubjectPropAnd2BTotalRehabilitationCostAnd2CFeeAssociatedWithTheNewLoan_rep);
            this.AddFormFieldData("sRefPdOffAmt1003", dataLoan.sRefPdOffAmt1003_rep);
            this.AddFormFieldData("sMatCostOrderedAndPrepaidInitDraw", dataLoan.sMatCostOrderedAndPrepaidInitDraw_rep);
            this.AddFormFieldData("s203kConsultantFeeInitDraw", dataLoan.s203kConsultantFeeInitDraw_rep);
            this.AddFormFieldData("sArchitectEngineerFee", dataLoan.sArchitectEngineerFee_rep);
            this.AddFormFieldData("sFeeNewLoan", dataLoan.sFeeNewLoan_rep);
            this.AddFormFieldData("sFeasibilityStudyFee", dataLoan.sFeasibilityStudyFee_rep);
            this.AddFormFieldData("sConsultantFee", dataLoan.sConsultantFee_rep);
        }
    }
}
