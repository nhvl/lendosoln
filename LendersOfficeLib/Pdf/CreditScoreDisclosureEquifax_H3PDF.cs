
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    public class CreditScoreDisclosureEquifax_H3_1PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosureEquifax_H3_1.pdf"; }
        }

        public override string Description
        {
            get { return "Page 1"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

            dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            if (BorrType == "C")
            {
                dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
            }

            AddFormFieldData("aEquifaxCreatedD" , dataApp.aEquifaxCreatedD_rep);
            AddFormFieldData("aEquifaxFactors" , dataApp.aEquifaxFactors);
            AddFormFieldData("aEquifaxPercentile" , dataApp.aEquifaxPercentile_rep);
            AddFormFieldData("aEquifaxScore" , dataApp.aEquifaxScore_rep);
            AddFormFieldData("aNm" , dataApp.aNm);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditEquifax, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("EquifaxCreditScorePreparerInfo", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("aEquifaxSourceName", preparer.CompanyName);

            AddFormFieldData("sEquifaxScoreFrom" , dataApp.LoanData.sEquifaxScoreFrom_rep);
            AddFormFieldData("sEquifaxScoreTo" , dataApp.LoanData.sEquifaxScoreTo_rep);

        }
    }

    public class CreditScoreDisclosureEquifax_H3_2PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosureEquifax_H3_2.pdf"; }
        }

        public override string Description
        {
            get { return "Page 2"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

    public class CreditScoreDisclosureEquifax_H3_3PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosureEquifax_H3_3.pdf"; }
        }

        public override string Description
        {
            get { return "Page 3"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

    public class CreditScoreDisclosureEquifax_H3PDF : AbstractBatchPDF
    {
        protected override E_AppPrintModeT AppPrintModeT
        {
            get { return E_AppPrintModeT.BorrowerAndSpouseSeparately; }
        }

        public override string EditLink
        {
            get { return "/newlos/Disclosure/CreditScoreDisclosure.aspx"; }
        }
        public override string Description
        {
            get
            {
                return "Equifax Credit Score Disclosure - H-3";
            }
        }
        protected override AbstractPDFFile[] ChildPages
        {
            get 
            {
                return new AbstractPDFFile[] {
                    new CreditScoreDisclosureEquifax_H3_1PDF(),
                    new CreditScoreDisclosureEquifax_H3_2PDF(),
                    new CreditScoreDisclosureEquifax_H3_3PDF(),

                };
            }
        }
    }
}
