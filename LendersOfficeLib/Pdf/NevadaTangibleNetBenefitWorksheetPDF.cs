﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CNevadaTangibleNetBenefitWorksheetPDF : CTangibleNetBenefitWorksheetPDF
    {
        public override string Description
        {
            get { return "Nevada Tangible Net Benefit Worksheet"; }
        }
    }
}