namespace LendersOffice.Pdf
{
    using DataAccess;

    public class CEqualCreditPDF : AbstractLetterPDF
	{

		public override string EditLink
		{
			get { return "/newlos/Disclosure/EqualCredit.aspx"; }
		}
        public override string PdfFile 
        {
            get { return "EqualCredit.pdf"; }
        }
        public override string Description 
        {
            get { return "Equal Credit Opportunity Act"; }
        }

        protected override  void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string propertyAddress = Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip);
            string brokerName = "";
            string ecoaAddr = "";

            IPreparerFields prep = dataLoan.GetPreparerOfForm( E_PreparerFormT.ECOA, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            brokerName = prep.CompanyName;


            // 4/19/2004 dd - If user did not specify ECOA address in Agent List then use the default ECOA address in broker setting.

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ECOA, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.CompanyName != "" || agent.StreetAddr != "" || agent.City != "" || agent.State != "" || agent.Zip != "") 
            {
                ecoaAddr = Tools.FormatAddress(agent.CompanyName, agent.StreetAddr, agent.City, agent.State, agent.Zip);
            } 
            else
            {
                ecoaAddr = dataLoan.BrokerDB.ECOAAddressDefault;
            }

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("ECOAAddr", ecoaAddr);
            AddFormFieldData("BrokerName", brokerName);
            AddFormFieldData("PropertyAddress", propertyAddress);

            AddFormFieldData("aBNm0", dataApp.aBNm);
            AddFormFieldData("aCNm0", dataApp.aCNm);
        }
	}
}
