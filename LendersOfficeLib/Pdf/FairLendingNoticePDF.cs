using System;
using System.Data;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
using System.Text;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CFairLendingNoticePDF : AbstractLetterPDF
	{

        public override string PdfFile 
        {
            get { return "FairLendingNotice.pdf"; }
        }
        public override string Description 
        {
            get { return "Fair Lending Notice"; }
        }
        private Guid GetBrokerID() 
        {
            Guid brokerID = Guid.Empty;
            if (Arguments["brokerid"] == null || Arguments["brokerid"] == "") 
            {
                    brokerID = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            }
            else 
            {
                brokerID = new Guid(Arguments["brokerid"]);
            }
            return brokerID;
        }

        protected override  void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.FairHousingLending, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string fairLendingAddr = "";

            if (agent.CompanyName != "" || agent.StreetAddr != "" || agent.City != "" || agent.State != "" || agent.Zip != "") 
            {
                // 4/6/2006 dd - If Fair Lending Housing Agent existed in Official Contact List then use it.
                fairLendingAddr = Tools.FormatAddress(agent.CompanyName, agent.StreetAddr, agent.City, agent.State, agent.Zip);
            } 
            else 
            {
                Guid brokerID = GetBrokerID();
                BrokerDB broker = BrokerDB.RetrieveById(brokerID);
                StringBuilder sb = new StringBuilder();
                foreach (string addr in broker.FairLendingNoticeAddress) 
                {
                    sb.Append(addr).Append(Environment.NewLine);
                }
                fairLendingAddr = sb.ToString();

            }

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 
            AddFormFieldData("GfeTilCompanyName", gfeTil.CompanyName);
            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("FairLendingNoticeAddr", fairLendingAddr);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
	}
}
