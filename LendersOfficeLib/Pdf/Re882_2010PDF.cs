﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public abstract class AbstractRe882_2010PDF : AbstractMldsPDF
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            base.ApplyImpound = false;
            base.ApplyData(dataLoan, dataApp);
            
            //Sets TotalToBroker, TotalToOther, Section 800-1300 EtcToBroker, Section 800 - 1300 EtcToOther
            #region Autogen stuff

            //Subtotals for: Total

            decimal TotalToBroker = 0;
            decimal TotalToOther = 0;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U5FProps)) TotalToBroker += dataLoan.s800U5F;
            else TotalToOther += dataLoan.s800U5F;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U4FProps)) TotalToBroker += dataLoan.s800U4F;
            else TotalToOther += dataLoan.s800U4F;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U3FProps)) TotalToBroker += dataLoan.s800U3F;
            else TotalToOther += dataLoan.s800U3F;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U2FProps)) TotalToBroker += dataLoan.s800U2F;
            else TotalToOther += dataLoan.s800U2F;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U1FProps)) TotalToBroker += dataLoan.s800U1F;
            else TotalToOther += dataLoan.s800U1F;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sWireFProps)) TotalToBroker += dataLoan.sWireF;
            else TotalToOther += dataLoan.sWireF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sUwFProps)) TotalToBroker += dataLoan.sUwF;
            else TotalToOther += dataLoan.sUwF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sProcFProps)) TotalToBroker += dataLoan.sProcF;
            else TotalToOther += dataLoan.sProcF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sTxServFProps)) TotalToBroker += dataLoan.sTxServF;
            else TotalToOther += dataLoan.sTxServF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sMBrokFProps)) TotalToBroker += dataLoan.sMBrokF;
            else TotalToOther += dataLoan.sMBrokF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sInspectFProps)) TotalToBroker += dataLoan.sInspectF;
            else TotalToOther += dataLoan.sInspectF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sCrFProps)) TotalToBroker += dataLoan.sCrF;
            else TotalToOther += dataLoan.sCrF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sApprFProps)) TotalToBroker += dataLoan.sApprF;
            else TotalToOther += dataLoan.sApprF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sLDiscntProps)) TotalToBroker += dataLoan.sLDiscnt;
            else TotalToOther += dataLoan.sLDiscnt;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sLOrigFProps)) TotalToBroker += dataLoan.sLOrigF;
            else TotalToOther += dataLoan.sLOrigF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s900U1PiaProps)) TotalToBroker += dataLoan.s900U1Pia;
            else TotalToOther += dataLoan.s900U1Pia;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sVaFfProps)) TotalToBroker += dataLoan.sVaFf;
            else TotalToOther += dataLoan.sVaFf;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s904PiaProps)) TotalToBroker += dataLoan.s904Pia;
            else TotalToOther += dataLoan.s904Pia;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sHazInsPiaProps)) TotalToBroker += dataLoan.sHazInsPia;
            else TotalToOther += dataLoan.sHazInsPia;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sMipPiaProps)) TotalToBroker += dataLoan.sMipPia;
            else TotalToOther += dataLoan.sMipPia;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sIPiaProps)) TotalToBroker += dataLoan.sIPia;
            else TotalToOther += dataLoan.sIPia;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sAggregateAdjRsrvProps)) TotalToBroker += dataLoan.sAggregateAdjRsrv;
            else TotalToOther += dataLoan.sAggregateAdjRsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s1007RsrvProps)) TotalToBroker += dataLoan.s1007Rsrv;
            else TotalToOther += dataLoan.s1007Rsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s1006RsrvProps)) TotalToBroker += dataLoan.s1006Rsrv;
            else TotalToOther += dataLoan.s1006Rsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sFloodInsRsrvProps)) TotalToBroker += dataLoan.sFloodInsRsrv;
            else TotalToOther += dataLoan.sFloodInsRsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sRealETxRsrvProps)) TotalToBroker += dataLoan.sRealETxRsrv;
            else TotalToOther += dataLoan.sRealETxRsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sSchoolTxRsrvProps)) TotalToBroker += dataLoan.sSchoolTxRsrv;
            else TotalToOther += dataLoan.sSchoolTxRsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sMInsRsrvProps)) TotalToBroker += dataLoan.sMInsRsrv;
            else TotalToOther += dataLoan.sMInsRsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sHazInsRsrvProps)) TotalToBroker += dataLoan.sHazInsRsrv;
            else TotalToOther += dataLoan.sHazInsRsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU4TcProps)) TotalToBroker += dataLoan.sU4Tc;
            else TotalToOther += dataLoan.sU4Tc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU3TcProps)) TotalToBroker += dataLoan.sU3Tc;
            else TotalToOther += dataLoan.sU3Tc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU2TcProps)) TotalToBroker += dataLoan.sU2Tc;
            else TotalToOther += dataLoan.sU2Tc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU1TcProps)) TotalToBroker += dataLoan.sU1Tc;
            else TotalToOther += dataLoan.sU1Tc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sTitleInsFProps)) TotalToBroker += dataLoan.sTitleInsF;
            else TotalToOther += dataLoan.sTitleInsF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sAttorneyFProps)) TotalToBroker += dataLoan.sAttorneyF;
            else TotalToOther += dataLoan.sAttorneyF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sNotaryFProps)) TotalToBroker += dataLoan.sNotaryF;
            else TotalToOther += dataLoan.sNotaryF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sDocPrepFProps)) TotalToBroker += dataLoan.sDocPrepF;
            else TotalToOther += dataLoan.sDocPrepF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sEscrowFProps)) TotalToBroker += dataLoan.sEscrowF;
            else TotalToOther += dataLoan.sEscrowF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU3GovRtcProps)) TotalToBroker += dataLoan.sU3GovRtc;
            else TotalToOther += dataLoan.sU3GovRtc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU2GovRtcProps)) TotalToBroker += dataLoan.sU2GovRtc;
            else TotalToOther += dataLoan.sU2GovRtc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU1GovRtcProps)) TotalToBroker += dataLoan.sU1GovRtc;
            else TotalToOther += dataLoan.sU1GovRtc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sStateRtcProps)) TotalToBroker += dataLoan.sStateRtc;
            else TotalToOther += dataLoan.sStateRtc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sCountyRtcProps)) TotalToBroker += dataLoan.sCountyRtc;
            else TotalToOther += dataLoan.sCountyRtc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sRecFProps)) TotalToBroker += dataLoan.sRecF;
            else TotalToOther += dataLoan.sRecF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU5ScProps)) TotalToBroker += dataLoan.sU5Sc;
            else TotalToOther += dataLoan.sU5Sc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU4ScProps)) TotalToBroker += dataLoan.sU4Sc;
            else TotalToOther += dataLoan.sU4Sc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU3ScProps)) TotalToBroker += dataLoan.sU3Sc;
            else TotalToOther += dataLoan.sU3Sc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU2ScProps)) TotalToBroker += dataLoan.sU2Sc;
            else TotalToOther += dataLoan.sU2Sc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU1ScProps)) TotalToBroker += dataLoan.sU1Sc;
            else TotalToOther += dataLoan.sU1Sc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sPestInspectFProps)) TotalToBroker += dataLoan.sPestInspectF;
            else TotalToOther += dataLoan.sPestInspectF;

            //Subtotals for: Section800Etc

            decimal Section800EtcToBroker = 0;
            decimal Section800EtcToOther = 0;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U1FProps)) Section800EtcToBroker += dataLoan.s800U1F;
            else Section800EtcToOther += dataLoan.s800U1F;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U2FProps)) Section800EtcToBroker += dataLoan.s800U2F;
            else Section800EtcToOther += dataLoan.s800U2F;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U3FProps)) Section800EtcToBroker += dataLoan.s800U3F;
            else Section800EtcToOther += dataLoan.s800U3F;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U4FProps)) Section800EtcToBroker += dataLoan.s800U4F;
            else Section800EtcToOther += dataLoan.s800U4F;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s800U5FProps)) Section800EtcToBroker += dataLoan.s800U5F;
            else Section800EtcToOther += dataLoan.s800U5F;

            //Subtotals for: Section900Etc

            decimal Section900EtcToBroker = 0;
            decimal Section900EtcToOther = 0;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s900U1PiaProps)) Section900EtcToBroker += dataLoan.s900U1Pia;
            else Section900EtcToOther += dataLoan.s900U1Pia;

            //Subtotals for: Section1000Etc

            decimal Section1000EtcToBroker = 0;
            decimal Section1000EtcToOther = 0;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sSchoolTxRsrvProps)) Section1000EtcToBroker += dataLoan.sSchoolTxRsrv;
            else Section1000EtcToOther += dataLoan.sSchoolTxRsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sFloodInsRsrvProps)) Section1000EtcToBroker += dataLoan.sFloodInsRsrv;
            else Section1000EtcToOther += dataLoan.sFloodInsRsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s1006RsrvProps)) Section1000EtcToBroker += dataLoan.s1006Rsrv;
            else Section1000EtcToOther += dataLoan.s1006Rsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.s1007RsrvProps)) Section1000EtcToBroker += dataLoan.s1007Rsrv;
            else Section1000EtcToOther += dataLoan.s1007Rsrv;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sAggregateAdjRsrvProps)) Section1000EtcToBroker += dataLoan.sAggregateAdjRsrv;
            else Section1000EtcToOther += dataLoan.sAggregateAdjRsrv;

            //Subtotals for: Section1100Etc

            decimal Section1100EtcToBroker = 0;
            decimal Section1100EtcToOther = 0;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sAttorneyFProps)) Section1100EtcToBroker += dataLoan.sAttorneyF;
            else Section1100EtcToOther += dataLoan.sAttorneyF;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU1TcProps)) Section1100EtcToBroker += dataLoan.sU1Tc;
            else Section1100EtcToOther += dataLoan.sU1Tc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU2TcProps)) Section1100EtcToBroker += dataLoan.sU2Tc;
            else Section1100EtcToOther += dataLoan.sU2Tc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU3TcProps)) Section1100EtcToBroker += dataLoan.sU3Tc;
            else Section1100EtcToOther += dataLoan.sU3Tc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU4TcProps)) Section1100EtcToBroker += dataLoan.sU4Tc;
            else Section1100EtcToOther += dataLoan.sU4Tc;

            //Subtotals for: Section1200Etc

            decimal Section1200EtcToBroker = 0;
            decimal Section1200EtcToOther = 0;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sStateRtcProps)) Section1200EtcToBroker += dataLoan.sStateRtc;
            else Section1200EtcToOther += dataLoan.sStateRtc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU1GovRtcProps)) Section1200EtcToBroker += dataLoan.sU1GovRtc;
            else Section1200EtcToOther += dataLoan.sU1GovRtc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU2GovRtcProps)) Section1200EtcToBroker += dataLoan.sU2GovRtc;
            else Section1200EtcToOther += dataLoan.sU2GovRtc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU3GovRtcProps)) Section1200EtcToBroker += dataLoan.sU3GovRtc;
            else Section1200EtcToOther += dataLoan.sU3GovRtc;

            //Subtotals for: Section1300Etc

            decimal Section1300EtcToBroker = 0;
            decimal Section1300EtcToOther = 0;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU1ScProps)) Section1300EtcToBroker += dataLoan.sU1Sc;
            else Section1300EtcToOther += dataLoan.sU1Sc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU2ScProps)) Section1300EtcToBroker += dataLoan.sU2Sc;
            else Section1300EtcToOther += dataLoan.sU2Sc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU3ScProps)) Section1300EtcToBroker += dataLoan.sU3Sc;
            else Section1300EtcToOther += dataLoan.sU3Sc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU4ScProps)) Section1300EtcToBroker += dataLoan.sU4Sc;
            else Section1300EtcToOther += dataLoan.sU4Sc;
            if (LosConvert.GfeItemProps_ToBr(dataLoan.sU5ScProps)) Section1300EtcToBroker += dataLoan.sU5Sc;
            else Section1300EtcToOther += dataLoan.sU5Sc;
            #endregion

            TotalToOther += dataLoan.sDisabilityIns;
            TotalToBroker += dataLoan.sGfeOriginatorCompF;

            AddFormFieldData("aBNm_aCNm", dataApp.aBNm_aCNm);
            AddFormFieldData("sSpAddr_SingleLine", dataLoan.sSpAddr_SingleLine);

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            CAgentFields lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            AddFormFieldData("GfeTilCompanyName", gfeTil.CompanyName);
            AddFormFieldData("GfeTilLicenseNumOfCompany", gfeTil.LicenseNumOfCompany);
            AddFormFieldData("GfeTilCompanyLoanOriginatorIdentifier", gfeTil.CompanyLoanOriginatorIdentifier);
            AddFormFieldData("GfeTilPreparerName", gfeTil.PreparerName);
            AddFormFieldData("GfeTilLoanOriginatorIdentifier", gfeTil.LoanOriginatorIdentifier);
            AddFormFieldData("GfeTilLicenseNumOfAgent", gfeTil.LicenseNumOfAgent);
            AddFormFieldData("GfeTilStreetAddr_SingleLine", gfeTil.StreetAddrSingleLine);
            AddFormFieldData("LenderCompanyName", lender.CompanyName);
            AddFormFieldData("IsLenderKnown", string.IsNullOrEmpty(lender.CompanyName) == false);

            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);

            AddFormFieldData("sLienholder1NmBefore", dataLoan.sLienholder1NmBefore);
            AddFormFieldData("sLien1AmtBefore", dataLoan.sLien1AmtBefore_rep);
            AddFormFieldData("sLien1PriorityBefore", dataLoan.sLien1PriorityBefore);
            AddFormFieldData("sLienholder2NmBefore", dataLoan.sLienholder2NmBefore);
            AddFormFieldData("sLien2AmtBefore", dataLoan.sLien2AmtBefore_rep);
            AddFormFieldData("sLien2PriorityBefore", dataLoan.sLien2PriorityBefore);
            AddFormFieldData("sLienholder3NmBefore", dataLoan.sLienholder3NmBefore);
            AddFormFieldData("sLien3AmtBefore", dataLoan.sLien3AmtBefore_rep);
            AddFormFieldData("sLien3PriorityBefore", dataLoan.sLien3PriorityBefore);
            AddFormFieldData("sLienholder1NmAfter", dataLoan.sLienholder1NmAfter);
            AddFormFieldData("sLien1AmtAfter", dataLoan.sLien1AmtAfter_rep);
            AddFormFieldData("sLien1PriorityAfter", dataLoan.sLien1PriorityAfter);
            AddFormFieldData("sLienholder2NmAfter", dataLoan.sLienholder2NmAfter);
            AddFormFieldData("sLien2AmtAfter", dataLoan.sLien2AmtAfter_rep);
            AddFormFieldData("sLien2PriorityAfter", dataLoan.sLien2PriorityAfter);
            AddFormFieldData("sLienholder3NmAfter", dataLoan.sLienholder3NmAfter);
            AddFormFieldData("sLien3AmtAfter", dataLoan.sLien3AmtAfter_rep);
            AddFormFieldData("sLien3PriorityAfter", dataLoan.sLien3PriorityAfter);
            AddFormFieldData("sBrokControlledFundT", dataLoan.sBrokControlledFundT);


            AddFormFieldData("sLenderPaidBrokerCompF", dataLoan.sLenderPaidBrokerCompF_rep);

            AddFormFieldData("sMldsIsNoDocTri", dataLoan.sMldsIsNoDocTri);
            AddFormFieldData("sMldsHasImpound", dataLoan.sMldsHasImpound);
            if (dataLoan.sMldsHasImpound)
            {
                AddFormFieldData("sMldsMonthlyImpoundPmt_Impound", dataLoan.sMldsMonthlyImpoundPmt_Impound);
                AddFormFieldData("sMldsMonthlyImpoundPmt_Impound", dataLoan.sMldsMonthlyImpoundPmt_Impound);
                AddFormFieldData("sMldsImpoundIncludeOther_Impound", dataLoan.sMldsImpoundIncludeOther_Impound);
                AddFormFieldData("sMldsImpoundIncludeFloodIns_Impound", dataLoan.sMldsImpoundIncludeFloodIns_Impound);
                AddFormFieldData("sMldsImpoundIncludeMIns_Impound", dataLoan.sMldsImpoundIncludeMIns_Impound);
                AddFormFieldData("sMldsImpoundIncludeRealETx_Impound", dataLoan.sMldsImpoundIncludeRealETx_Impound);
                AddFormFieldData("sMldsImpoundIncludeHazIns_Impound", dataLoan.sMldsImpoundIncludeHazIns_Impound);
                AddFormFieldData("sMldsImpoundOtherDesc_Impound", dataLoan.sMldsImpoundOtherDesc_Impound);

            }
            else
            {
                AddFormFieldData("sMldsMonthlyImpoundPmtPerYear_NoImpound", dataLoan.sMldsMonthlyImpoundPmtPerYear_NoImpound);
                AddFormFieldData("sMldsImpoundIncludeOther_NoImpound", dataLoan.sMldsImpoundIncludeOther_NoImpound);
                AddFormFieldData("sMldsImpoundIncludeFloodIns_NoImpound", dataLoan.sMldsImpoundIncludeFloodIns_NoImpound);
                AddFormFieldData("sMldsImpoundIncludeMIns_NoImpound", dataLoan.sMldsImpoundIncludeMIns_NoImpound);
                AddFormFieldData("sMldsImpoundIncludeRealETx_NoImpound", dataLoan.sMldsImpoundIncludeRealETx_NoImpound);
                AddFormFieldData("sMldsImpoundIncludeHazIns_NoImpound", dataLoan.sMldsImpoundIncludeHazIns_NoImpound);
                AddFormFieldData("sMldsImpoundOtherDesc_NoImpound", dataLoan.sMldsImpoundOtherDesc_NoImpound);

            }

            AddFormFieldData("sRE882010YSPServiceReleaseFromLender", dataLoan.sRE882010YSPServiceReleaseFromLender_rep);
            AddFormFieldData("sRE882010YSPServiceCreditedToBorrower", dataLoan.sRE882010YSPServiceCreditedToBorrower_rep);

            CAgentFields broker =  dataLoan.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            AddFormFieldData("BrokerLoanOriginatorIdentifier", broker.LoanOriginatorIdentifier);
            AddFormFieldData("BrokerCompanyLoanOriginatorIdentifier", broker.CompanyLoanOriginatorIdentifier);

            AddFormFieldData("sBalloonPmt", dataLoan.sBalloonPmt);
            AddFormFieldData("sFinalBalloonPmt", dataLoan.sFinalBalloonPmt_rep);
            AddFormFieldData("sFinalBalloonPmtDueD", dataLoan.sFinalBalloonPmtDueD_rep);
            AddFormFieldData("sFinMethT", dataLoan.sFinMethT);

            AddFormFieldData("sMldsPpmtMaxAmt", dataLoan.sMldsPpmtMaxAmt_rep);
            AddFormFieldData("sMldsPpmtPeriod_Ppmt", dataLoan.sMldsPpmtPeriod_Ppmt);
            AddFormFieldData("sMldsPpmtT", dataLoan.sMldsPpmtT);
            AddFormFieldData("sIPiaDy", dataLoan.sIPiaDy_rep);
            AddFormFieldData("sIPerDay", dataLoan.sIPerDay_rep);
            AddFormFieldData("sHazInsRsrvMon", dataLoan.sHazInsRsrvMon_rep);
            AddFormFieldData("sProHazIns", dataLoan.sProHazIns_rep);
            AddFormFieldData("sRealETxRsrvMon", dataLoan.sRealETxRsrvMon_rep);
            AddFormFieldData("sProRealETx", dataLoan.sProRealETx_rep);
            AddFormFieldData("sMInsRsrvMon", dataLoan.sMInsRsrvMon_rep);
            AddFormFieldData("sProMIns", dataLoan.sProMIns_rep);

            if (!LosConvert.GfeItemProps_ToBr(dataLoan.sLOrigFProps)) AddFormFieldData("sLOrigFO", dataLoan.sLOrigF_rep);
            if (!LosConvert.GfeItemProps_ToBr(dataLoan.sLDiscntProps)) AddFormFieldData("sLDiscntO", dataLoan.sLDiscnt_rep);
            AddFormFieldData("sGfeOriginatorCompF", dataLoan.sGfeOriginatorCompF_rep);
            AddFormFieldData("sDisabilityIns", dataLoan.sDisabilityIns_rep);

            //Leave the descriptions unset for now
            AddFormFieldData("s900MldsRe882B", ToRep(Section900EtcToBroker, dataLoan.m_convertLos));
            AddFormFieldData("s900MldsRe882O", ToRep(Section900EtcToOther, dataLoan.m_convertLos));
            //AddFormFieldData("s900OtherDescriptionMldsRe882", dataLoan.s900U1PiaDesc);

            AddFormFieldData("sMlds882OtherRsrvB", ToRep(Section1000EtcToBroker, dataLoan.m_convertLos));
            AddFormFieldData("sMlds882OtherRsrvO", ToRep(Section1000EtcToOther, dataLoan.m_convertLos));
            //AddFormFieldData("sOtherRsrvDescriptionMldsRe882", dataLoan.s1006ProHExpDesc);

            AddFormFieldData("sMlds882OtherTcB", ToRep(Section1100EtcToBroker, dataLoan.m_convertLos));
            AddFormFieldData("sMlds882OtherTcO", ToRep(Section1100EtcToOther, dataLoan.m_convertLos));
            //AddFormFieldData("sOtherTcDescriptionMldsRe882", dataLoan.sU1TcDesc);

            AddFormFieldData("sMlds882OtherGovRtcB", ToRep(Section1200EtcToBroker, dataLoan.m_convertLos));
            AddFormFieldData("sMlds882OtherGovRtcO", ToRep(Section1200EtcToOther, dataLoan.m_convertLos));
            //AddFormFieldData("sGovOtherDescriptionMldsRe882", dataLoan.sU1GovRtcDesc);

            AddFormFieldData("sMlds882TotalCostPaidToBroker", ToRep(TotalToBroker, dataLoan.m_convertLos));
            AddFormFieldData("sMlds882TotalCostPaidToOther", ToRep(TotalToOther, dataLoan.m_convertLos));
            AddFormFieldData("sMlds882TotalCost", ToRep(TotalToBroker + TotalToOther, dataLoan.m_convertLos));
        }

        private static string ToRep(decimal val, LosConvert c)
        {
            return c.ToMoneyString(val, FormatDirection.ToRep);
        }
    }

    

    public class CRe882_2010_1PDF : AbstractRe882_2010PDF
    {
        public override string PdfFile
        {
            get { return "RE_882_2010_1.pdf"; }
        }

        public override string Description
        {
            get { return "Page 1"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
    }
    public class CRe882_2010_2PDF : AbstractRe882_2010PDF
    {
        public override string PdfFile
        {
            get { return "RE_882_2010_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
    }
    public class CRe882_2010_3PDF : AbstractRe882_2010PDF
    {
        public override string PdfFile
        {
            get { return "RE_882_2010_3.pdf"; }
        }
        public override string Description
        {
            get { return "Page 3"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
    }

    public class CRe882_2010PDF : AbstractBatchPDF
    {
        public override string Description
        {
            get { return "CA Mortgage Loan Disclosure Statement (RE 882)"; }
        }

            public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
 
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CRe882_2010_1PDF(),
                                                 new CRe882_2010_2PDF(),
                                                 new CRe882_2010_3PDF()
                                             };
            }
        }
    }
}
