namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Text;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;

    public class CMortgageLoanCommitment_v2_1PDF : AbstractLegalPDF
	{
        public override string PdfFile 
        {
            get { return "MortgageLoanCommitment_v2_1.pdf"; }
        }

        public override string Description 
        {
            get { return "Page 1"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageLoanCommitment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("MortgageLoanCommitment", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip), false);
            AddFormFieldData("MortgageLoanCommitmentPrepareDate", preparer.PrepareDate_rep, false);

            AddFormFieldData("MortgageLoanCommitmentCompanyName", preparer.CompanyName, false);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageLoanCommitmentAlternateLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("MortgageLoanCommitmentAlternateLender", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip), false);

            AddFormFieldData("sCommitReturnToFollowAddr", dataLoan.sCommitReturnToFollowAddr);
            AddFormFieldData("sCommitReturnToAboveAddr", dataLoan.sCommitReturnToAboveAddr);
            AddFormFieldData("sCommitReturnWithinDays", dataLoan.sCommitReturnWithinDays_rep, false);
            AddFormFieldData("sCommitTitleEvidence", dataLoan.sCommitTitleEvidence, false);
            AddFormFieldData("sCommitRepayTermsDesc", dataLoan.sCommitRepayTermsDesc, false);
            AddFormFieldData("sCommitExpD", dataLoan.sCommitExpD_rep, false);

            AddFormFieldData("aBNm", dataApp.aBNm, false);
            AddFormFieldData("aCNm", dataApp.aCNm, false);
            AddFormFieldData("sLNm", dataLoan.sLNm, false);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep, false);

            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip), false);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep + " / " + dataLoan.sDue_rep, false);

            AddFormFieldData("sMaxR", dataLoan.sMaxR_rep, false);
            AddFormFieldData("sLpTemplateNm", dataLoan.sLpTemplateNm, false);
            AddFormFieldData("sIsRateLocked", dataLoan.sIsRateLocked);

            AddFormFieldData("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep, false);

            dataLoan.SetFormatTarget(FormatTarget.Webform); // show dollar signs

            AddFormFieldData("sLtvR", dataLoan.sLtvR_rep, false);
            AddFormFieldData("sCltvR", dataLoan.sCltvR_rep, false);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep, false);
            AddFormFieldData("sLDiscnt1003", dataLoan.sLDiscnt1003_rep, false);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep, false);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep, false);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep, false);

        }
	}
    public class CMortgageLoanCommitment_v2_2PDF : AbstractLegalPDF
    {
        private BrokerDB m_brokerDB = null;

        private Boolean UseUnderwritingConditions
        {
            get
            {
                BrokerUserPrincipal brokerUser = System.Threading.Thread.CurrentPrincipal as BrokerUserPrincipal;

                // 12/2/2004 dd - This class can be call from NHC GetLoanForm webservice.
                // Therefore BrokerUserPrincipal can be null. As a result, just assume
                // this user doesn't have PriceMyLoan.
                if (null == brokerUser)
                    return false;

                // 2/8/2005 kb - PML is not the key.  We use lender default features now.

                if (m_brokerDB == null)
                {
                    try
                    {
                        m_brokerDB = BrokerDB.RetrieveById(brokerUser.BrokerId);
                    }
                    catch (BrokerDBNotFoundException e)
                    {
                        Tools.LogError("Failed to load broker access info.", e);
                    }
                }

                if (m_brokerDB != null && m_brokerDB.HasLenderDefaultFeatures == true)
                {
                    return true;
                }

                return false;
            }
        }

        public override string PdfFile 
        {
            get { return "MortgageLoanCommitment_v2_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            StringBuilder sb = new StringBuilder();


            BrokerDB db = dataLoan.BrokerDB;
            if (db.IsUseNewTaskSystem)
            {
                List<Task> conditions = Task.GetActiveConditionsByLoanId(db.BrokerID, dataLoan.sLId, false);
                foreach (Task condition in conditions)
                {
                    if (condition.TaskStatus == E_TaskStatus.Closed)
                    {
                        continue;
                    }

                    sb.Append(condition.TaskSubject).Append(Environment.NewLine);
                }
            }
            else
            {
                if (UseUnderwritingConditions)
                {
                    // 10/31/06 mf. Since this is for read-only, I converted this to
                    // use CConditionSet instead of LoanConditionSet to reduce overhead.

                    CConditionSetObsolete conditionSet = new CConditionSetObsolete(dataLoan.sLId);

                    foreach (CConditionObsolete condition in conditionSet)
                    {
                        if (condition.IsDone == false) // OPM 24013 - Only show outstanding Conditions.
                            sb.Append(condition.CondDesc).Append(Environment.NewLine);
                    }

                }
                else
                {
                    int count = dataLoan.GetCondRecordCount();
                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete field = dataLoan.GetCondFieldsObsolete(i);
                        if (field.IsDone == false) // OPM 24013 - Only show outstanding Conditions.
                            sb.Append(field.CondDesc).Append(Environment.NewLine);
                    }
                }
            }
            AddFormFieldData("Conditions", sb.ToString(), false);
        }
    }

    public class CMortgageLoanCommitment_v2_3PDF : AbstractLegalPDF
    {

        public override string PdfFile
        {
            get { return "MortgageLoanCommitment_v2_3.pdf"; }
        }
        public override string Description
        {
            get { return "Page 3"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("sMldsHasImpound", dataLoan.sMldsHasImpound);
        }
    }

    public class CMortgageLoanCommitment_v2_4PDF : AbstractLegalPDF
    {

        public override string PdfFile
        {
            get { return "MortgageLoanCommitment_v2_4.pdf"; }
        }
        public override string Description
        {
            get { return "Page 4"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CMortgageLoanCommitment_v2_5PDF : AbstractLegalPDF
    {

        public override string PdfFile
        {
            get { return "MortgageLoanCommitment_v2_5.pdf"; }
        }
        public override string Description
        {
            get { return "Page 5"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            dataLoan.SetFormatTarget(FormatTarget.Webform); // show the number 0
            AddFormFieldData("sCommitReturnWithinDays", dataLoan.sCommitReturnWithinDays_rep, false);
            var preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.MortgageLoanCommitmentAlternateLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("MortgageLoanCommitmentAlternateLender", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip), false);
            //AddFormFieldData("aCSignature", ???)
            //AddFormFieldData("aBSignature", ???)
        }
    }

    public class CMortgageLoanCommitment_v2PDF : AbstractBatchPDF
    {

        public override string Description 
        {
            get { return "Mortgage Loan Commitment (rev. 6/11)"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/MortgageLoanCommitment.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CMortgageLoanCommitment_v2_1PDF(),
                                                 new CMortgageLoanCommitment_v2_2PDF(),
                                                 new CMortgageLoanCommitment_v2_3PDF(),
                                                 new CMortgageLoanCommitment_v2_4PDF(),
                                                 new CMortgageLoanCommitment_v2_5PDF()
                                             };
            }
        }

    }
}
