namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.PdfGenerator;

    public class CTruthInLending32PDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "TruthInLending32.pdf"; }
        }

        public override string Description 
        {
            get { return "Truth In Lending Section 32 Disclosure"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/TruthInLending32.aspx"; }
        }
 	
	    
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Til,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("GfeTil", Tools.FormatAddress(gfeTil.CompanyName, gfeTil.StreetAddr, gfeTil.City, gfeTil.State, gfeTil.Zip, gfeTil.PhoneOfCompany));
            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);

            string applicant = dataApp.aBNm;
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "")
                applicant += Environment.NewLine + dataApp.aCNm;

            applicant = Tools.FormatAddress(applicant, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip);
            AddFormFieldData("Applicant", applicant);
            AddFormFieldData("sSpFullAddr", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));

            AddFormFieldData("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            AddFormFieldData("sApr", dataLoan.sApr_rep);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sCreditInsIncludedTri", dataLoan.sCreditInsIncludedTri);
            AddFormFieldData("sSchedPmt1", dataLoan.sSchedPmt1_rep);
            AddFormFieldData("sBalloonPmt", dataLoan.sBalloonPmt);
            AddFormFieldData("sFinalBalloonPmt", dataLoan.sFinalBalloonPmt_rep);
            AddFormFieldData("sHasVarRFeature", dataLoan.sHasVarRFeature);


            if (dataLoan.sHasVarRFeature)
            {
                try
                {
                    bool groupAmortTableEntries = true;
                    var worstCaseAmort = dataLoan.GetAmortTable(
                        E_AmortizationScheduleT.WorstCase,
                        groupAmortTableEntries);

                    decimal maxPayment = 0.0M;

                    for (int i = 0; i < worstCaseAmort.nRows; i++)
                    {
                        maxPayment = Math.Max(maxPayment, worstCaseAmort.Items[i].Pmt);
                    }

                    AddFormFieldData("dollar", "$");
                    AddFormFieldData("MaxPayment", dataLoan.m_convertLos.ToMoneyString(maxPayment, FormatDirection.ToRep));
                } 
                catch {}
            }
        }
	}
}
