namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using LOPDFGenLib;

    public class CLoanSummaryPDF : AbstractFreeLayoutPDFFile
	{
		private BrokerDB m_brokerDB = null;

		private bool UseUnderwritingConditions
		{
			get
			{
				// 12/2/2004 dd - This class can be call from NHC GetLoanForm webservice.
				// Therefore BrokerUserPrincipal can be null. As a result, just assume
				// this user doesn't have PriceMyLoan.
				//
				// 2/8/2005 kb - PML is not the key.  We use lender default features now.

                if (this.m_brokerDB == null)
				{
					try
					{
						BrokerUserPrincipal brokerUser = System.Threading.Thread.CurrentPrincipal as BrokerUserPrincipal;

                        this.m_brokerDB = BrokerDB.RetrieveById(brokerUser.BrokerId);
					}
					catch( Exception e )
					{
						Tools.LogError( "Failed to load broker access info." , e );
					}
				}

                if (this.m_brokerDB != null && this.m_brokerDB.HasLenderDefaultFeatures == true)
				{
					return true;
				}

				return false;
			}
		}

		public override string Description 
        {
            get { return "Loan Summary"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            #region "Add Employee In Role"
            AddEmployeInRoleHeader();
            AddEmployeeInRole("Loan Opener", dataLoan.sEmployeeLoanOpener);
            AddEmployeeInRole("Call Center Agent", dataLoan.sEmployeeCallCenterAgent);
            AddEmployeeInRole("Processor", dataLoan.sEmployeeProcessor);
            AddEmployeeInRole("Manager", dataLoan.sEmployeeManager);
            AddEmployeeInRole("Loan Officer", dataLoan.sEmployeeLoanRep);
            AddEmployeeInRole("Real Estate Agent", dataLoan.sEmployeeRealEstateAgent);
            AddEmployeeInRole("Lender Account Executive", dataLoan.sEmployeeLenderAccExec);
			AddEmployeeInRole("Lock Desk", dataLoan.sEmployeeLockDesk);
			AddEmployeeInRole("Underwriter", dataLoan.sEmployeeUnderwriter);
            AddItem(new HorizontalLineItem());
            #endregion 

            #region "Add Agent List"
            AddAgentListHeader();
            CAgentFields field = null;

            field = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (field.IsValid) 
            {
                AddAgent(field.AgentRoleDescription, field.AgentName, field.Phone, field.FaxNum, field.CellPhone, field.EmailAddr);
            }

            field = dataLoan.GetAgentOfRole(E_AgentRoleT.Processor,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (field.IsValid) 
            {
                AddAgent(field.AgentRoleDescription, field.AgentName, field.Phone, field.FaxNum, field.CellPhone, field.EmailAddr);
            }

            int count = dataLoan.GetAgentRecordCount();
            for (int i = 0; i < count; i++) 
            {
                field = dataLoan.GetAgentFields(i);
                if (field.AgentRoleT != E_AgentRoleT.LoanOfficer &&
                    field.AgentRoleT != E_AgentRoleT.Processor) 
                {
                    AddAgent(field.AgentRoleDescription, field.CaseNum, field.AgentName, field.Phone, field.FaxNum, field.CompanyName, field.CellPhone, field.StreetAddr, field.CityStateZip, field.EmailAddr);

                }
            }
            AddItem(new HorizontalLineItem());
            #endregion 


            AddBorrowerInformationHeader();
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                AddBorrowerInformation(dataLoan.GetAppData(i));

                AddItem(new HorizontalLineItem());
            }


            #region "Add Property Information"
            AddPropertyInformation(dataLoan.sSpAddr, Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip), dataLoan.sSpCounty, dataLoan.sYrBuilt,
                dataLoan.sPurchPrice_rep, dataLoan.sApprVal_rep, dataLoan.sDownPmtPc_rep, dataLoan.sEquityCalc_rep);
            #endregion 

            AddMortgageInformation(dataLoan);
            AddUnderwritingInformation(dataLoan);
            AddNotes(dataLoan.sTrNotes);



            #region "Add Conditions"

			// 10/7/2004 kb - We now support 2 modes of condition management:
			// the old way when pml is disabled, and a new, task-based method
			// when pml is on.
			//
			// 2/11/2005 kb - Expect the result of the list-disc-logs-by-Z
			// method to return a set ordered by an arbitrary rank number,
			// and then by creation date.  We need to clean up this call to
			// centralize the access to a loan's task-conditions.
			//
			// 2/24/2005 kb - This implementation is way out of hand.  It's
			// time to bifurcate within this handler.  If any more changes
			// come in, then we'll do this right(er).

            AddConditionHeader();

            if (dataLoan.BrokerDB.IsUseNewTaskSystem)
            {
                List<Task> conditions = Task.GetActiveConditionsByLoanId(dataLoan.BrokerDB.BrokerID, dataLoan.sLId, false);
                foreach (Task condition in conditions)
                {
                    string done = condition.TaskStatus == E_TaskStatus.Closed ? "Yes" : "No";
                    string datedone = condition.TaskStatus == E_TaskStatus.Closed && condition.TaskClosedDate.HasValue ? condition.TaskClosedDate.Value.ToShortDateString() : "";
                    string required = "";
                    string priorToEventDesc = condition.CondCategoryId_rep;
                    string desc = condition.TaskSubject;
                    AddCondition(done, datedone, required, priorToEventDesc, desc);
                }
            }
            else
            {

                DataView dv = dataLoan.sCondDataSet.Value.Tables[0].DefaultView;

                if (UseUnderwritingConditions == true)
                {
                    // 10/7/2004 kb - Fetch the data set that contains all the
                    // current loan's conditions as tasks.  We use a simple list
                    // sproc that returns the task items in the correct format.

                    // 11/6/2006 mf. Modified to support new condition system.				
                    DataTable dT = BuildUnderwritingConditionList(dataLoan.sLId);

                    dv = dT.DefaultView;
                }

                dv.RowFilter = "IsRequired='True'";

                foreach (DataRow row in dv.Table.Rows)
                {
                    if (UseUnderwritingConditions == false || row["PriorToEventDesc"].ToString().ToLower().TrimWhitespaceAndBOM() != "notes")
                    {
                        AddCondition
                            (row["IsDone"].ToString() == "True" ? "Yes" : ""
                            , row["DoneDate"] is DateTime ? ((DateTime)row["DoneDate"]).ToShortDateString() : row["DoneDate"].ToString()
                            , ""
                            , row["PriorToEventDesc"].ToString()
                            , row["CondDesc"].ToString()
                            );
                    }
                    else
                    {
                        AddCondition
                            (""
                            , ""
                            , ""
                            , row["PriorToEventDesc"].ToString()
                            , row["CondDesc"].ToString()
                            );
                    }
                }
            }

            AddItem(new HorizontalLineItem());

            #endregion

            #region "Add Loan Status"
            AddLoanStatusHeader(dataLoan);

            AddLoanStatusItem("Rate Lock", dataLoan.sRLckdD_rep, dataLoan.sRLckdN);
            AddLoanStatusItem("Rate Lock Expired", dataLoan.sRLckdExpiredD_rep, dataLoan.sRLckdExpiredN);
            AddLoanStatusItem("Lead New", dataLoan.sLeadD_rep, dataLoan.sLeadN);
            AddLoanStatusItem("Opened", dataLoan.sOpenedD_rep, dataLoan.sOpenedN);
            AddLoanStatusItem("Pre-qual", dataLoan.sPreQualD_rep, dataLoan.sPreQualN);
            AddLoanStatusItem("Preapproved", dataLoan.sPreApprovD_rep, dataLoan.sPreApprovN);
            AddLoanStatusItem("Submitted", dataLoan.sSubmitD_rep, dataLoan.sSubmitN);
            AddLoanStatusItem("Approved", dataLoan.sApprovD_rep, dataLoan.sApprovN);
            AddLoanStatusItem("Est Closing", dataLoan.sEstCloseD_rep, dataLoan.sEstCloseN);
            AddLoanStatusItem("Documents", dataLoan.sDocsD_rep, dataLoan.sDocsN);
            AddLoanStatusItem("Funded", dataLoan.sFundD_rep, dataLoan.sFundN);
            AddLoanStatusItem("Schedule Fund", dataLoan.sSchedFundD_rep, dataLoan.sSchedFundN);
            AddLoanStatusItem("Recorded", dataLoan.sRecordedD_rep, dataLoan.sRecordedN);
            AddLoanStatusItem("Loan On-hold", dataLoan.sOnHoldD_rep, dataLoan.sOnHoldN);
            AddLoanStatusItem("Loan Canceled", dataLoan.sCanceledD_rep, dataLoan.sCanceledN);
            AddLoanStatusItem("Loan Denied", dataLoan.sRejectD_rep, dataLoan.sRejectN);
            AddLoanStatusItem("Loan Suspended", dataLoan.sSuspendedD_rep, dataLoan.sSuspendedN);
            AddLoanStatusItem("Loan Closed", dataLoan.sClosedD_rep, dataLoan.sClosedN);
            AddLoanStatusItem(dataLoan.sU1LStatDesc, dataLoan.sU1LStatD_rep, dataLoan.sU1LStatN);
            AddLoanStatusItem(dataLoan.sU2LStatDesc, dataLoan.sU2LStatD_rep, dataLoan.sU2LStatN);
            AddLoanStatusItem(dataLoan.sU3LStatDesc, dataLoan.sU3LStatD_rep, dataLoan.sU3LStatN);
            AddLoanStatusItem(dataLoan.sU4LStatDesc, dataLoan.sU4LStatD_rep, dataLoan.sU4LStatN);

            AddItem(new HorizontalLineItem());
            #endregion 

            #region "Add Basic Documents"
            AddBasicDocumentsHeader();

            AddBasicDocumentItem("Preliminary Report", dataLoan.sPrelimRprtOd_rep, dataLoan.sPrelimRprtRd_rep, dataLoan.sPrelimRprtN);
            AddBasicDocumentItem("Appraisal Report", dataLoan.sApprRprtOd_rep, dataLoan.sApprRprtRd_rep, dataLoan.sApprRprtN);
            AddBasicDocumentItem("TIL Disclosure / GFE", dataLoan.sTilGfeOd_rep, dataLoan.sTilGfeRd_rep, dataLoan.sTilGfeN);
            AddBasicDocumentItem(dataLoan.sU1DocStatDesc, dataLoan.sU1DocStatOd_rep, dataLoan.sU1DocStatRd_rep, dataLoan.sU1DocStatN);
            AddBasicDocumentItem(dataLoan.sU2DocStatDesc, dataLoan.sU2DocStatOd_rep, dataLoan.sU2DocStatRd_rep, dataLoan.sU2DocStatN);
            AddBasicDocumentItem(dataLoan.sU3DocStatDesc, dataLoan.sU3DocStatOd_rep, dataLoan.sU3DocStatRd_rep, dataLoan.sU3DocStatN);
            AddBasicDocumentItem(dataLoan.sU4DocStatDesc, dataLoan.sU4DocStatOd_rep, dataLoan.sU4DocStatRd_rep, dataLoan.sU4DocStatN);
            AddBasicDocumentItem(dataLoan.sU5DocStatDesc, dataLoan.sU5DocStatOd_rep, dataLoan.sU5DocStatRd_rep, dataLoan.sU5DocStatN);
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i); 
                AddBasicDocumentItem("Credit Report", d.aCrOd_rep, d.aCrRd_rep, d.aCrN);
                AddBasicDocumentItem("Business Credit Report", d.aBusCrOd_rep, d.aBusCrRd_rep, d.aBusCrN);
                AddBasicDocumentItem(d.aU1DocStatDesc, d.aU1DocStatOd_rep, d.aU1DocStatRd_rep, d.aU1DocStatN);
                AddBasicDocumentItem(d.aU2DocStatDesc, d.aU2DocStatOd_rep, d.aU2DocStatRd_rep, d.aU2DocStatN);

            }
            AddItem(new HorizontalLineItem());

            #endregion 

            #region "Add Verification of Employments"
            AddGenericDocumentHeader("Verification of Employment", "Employer");
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                IEmpCollection recordList = d.aBEmpCollection;
                var subCollection = recordList.GetSubcollection(true, E_EmpGroupT.All);
                foreach (IEmploymentRecord f in subCollection) 
                {
                    AddGenericDocumentItem("B " + f.EmplrNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);

                }

                recordList = d.aCEmpCollection;
                subCollection = recordList.GetSubcollection(true, E_EmpGroupT.All);
                foreach (IEmploymentRecord f in subCollection) 
                {
                    AddGenericDocumentItem("C " + f.EmplrNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

            }
            AddItem(new HorizontalLineItem());

            #endregion 
            #region "Add Verification of Deposits"
            AddGenericDocumentHeader("Verification of Deposit", "Depository");
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                var subcoll = d.aAssetCollection.GetSubcollection( false, E_AssetGroupT.DontNeedVOD );
                Hashtable hash = new Hashtable();
                foreach( var item in subcoll )
                {
                    var f = (IAssetRegular)item;
                    if (!hash.ContainsKey(f.ComNm.ToLower())) 
                    {
                        hash.Add(f.ComNm.ToLower(), f.ComNm.ToLower());
                        AddGenericDocumentItem(f.ComNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                    }
                }


            }
            AddItem(new HorizontalLineItem());
            #endregion 

            #region "Add verification of Rent"
            AddGenericDocumentHeader("Verification of Mortgage/Rent", "Creditor / Landlord");
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                IVerificationOfRent f = null;
                string addr = string.Format("{0}, {1}, {2} {3}", d.aBAddr, d.aBCity, d.aBState, d.aBZip);

                if (d.aBAddrT == E_aBAddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("11111111-1111-1111-1111-111111111111"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

                addr = string.Format("{0}, {1}, {2} {3}", d.aBPrev1Addr, d.aBPrev1City, d.aBPrev1State, d.aBPrev1Zip);
                if (d.aBPrev1AddrT == E_aBPrev1AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("22222222-2222-2222-2222-222222222222"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }
                addr = string.Format("{0}, {1}, {2} {3}", d.aBPrev2Addr, d.aBPrev2City, d.aBPrev2State, d.aBPrev2Zip);
                if (d.aBPrev2AddrT == E_aBPrev2AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("33333333-3333-3333-3333-333333333333"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

                addr = string.Format("{0}, {1}, {2} {3}", d.aCAddr, d.aCCity, d.aCState, d.aCZip);
                if (d.aCAddrT == E_aCAddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("44444444-4444-4444-4444-444444444444"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

                addr = string.Format("{0}, {1}, {2} {3}", d.aCPrev1Addr, d.aCPrev1City, d.aCPrev1State, d.aCPrev1Zip);
                if (d.aCPrev1AddrT == E_aCPrev1AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("55555555-5555-5555-5555-555555555555"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }
                addr = string.Format("{0}, {1}, {2} {3}", d.aCPrev2Addr, d.aCPrev2City, d.aCPrev2State, d.aCPrev2Zip);
                if (d.aCPrev2AddrT == E_aCPrev2AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("66666666-6666-6666-6666-666666666666"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

            }
            #endregion 

            #region "Add Verification of Mortgage"
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                ILiaCollection liaList = d.aLiaCollection;
                var liaSubcoll = liaList.GetSubcollection( true, E_DebtGroupT.Regular );
                foreach( ILiabilityRegular f in liaSubcoll )
                {
                    if (f.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc)) 
                    {
                        AddGenericDocumentItem(f.ComNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                    } 
                }
            }

            AddItem(new HorizontalLineItem());

            #endregion 

            AddGenericDocumentHeader("Verification of Loan", "Lender");

            #region "Add Verification of Loan"
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                ILiaCollection liaList = d.aLiaCollection;
                var liaSubcoll = liaList.GetSubcollection( true, E_DebtGroupT.Regular );
                foreach( ILiabilityRegular f in liaSubcoll )
                {
                    if (f.DebtT == E_DebtRegularT.Installment || f.DebtT == E_DebtRegularT.Revolving) 
                    {
                        AddGenericDocumentItem(f.ComNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                    }
                }
            }
            #endregion 
        }

		private DataTable BuildUnderwritingConditionList( Guid loanId )
		{
			// We create a datatable that we can bind to the list
			// of conditions to be compatible with both condition modes.
			DataTable conditionTable = new DataTable();

			CConditionSetObsolete conditions = new CConditionSetObsolete( loanId );
			conditionTable.Columns.Add("IsDone");
			conditionTable.Columns.Add("IsRequired");
			conditionTable.Columns.Add("DoneDate");
			conditionTable.Columns.Add("PriorToEventDesc");
			conditionTable.Columns.Add("CondDesc");

			foreach( CConditionObsolete condition in conditions )
			{
				DataRow row = conditionTable.NewRow();
				row["IsRequired"] = "True";
				if ( condition.IsDone )
				{
					row["IsDone"] = "True";
					row["DoneDate"] = condition.DateDone;
				}
				else
				{
					row["IsDone"] = "False";
				}
				row["PriorToEventDesc"] = condition.CondCategoryDesc;
				row["CondDesc"] = condition.CondDesc;

				conditionTable.Rows.Add( row );
			}

			return conditionTable;
		}

        protected override void AddTitle() 
        {
            FontName boldFont = FontName.HelveticaBold;
            short fontSize = 12;

            RowItem row = new RowItem(0, 40);
            row.Add(new TextItem("LOAN SUMMARY", 240, 0, boldFont, fontSize));
            AddItem(row);


        }
        private void AddBorrowerInformationHeader() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Borrower Information", 40, 0, font, fontSize));
            AddItem(row);

            row = new RowItem() ;
            row.Add(new TextItem("Primary", 40, 0, font, fontSize));
            row.Add(new TextItem("Co-Borrower", 310, 0, font, fontSize));
            AddItem(row);

        }
        private void AddBorrowerInformation(CAppData dataApp) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 8;
            short x1 = 40;
            short x2 = 110;
            short x3 = 310;
            short x4 = 380;

            RowItem row = new RowItem();
            row.Add(new TextItem("Name:", x1, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aBNm, x2, 0, font, fontSize));
            row.Add(new TextItem("Name:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aCNm, x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("SSN:", x1, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aBSsn, x2, 0, font, fontSize));
            row.Add(new TextItem("SSN:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aCSsn, x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("DOB:", x1, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aBDob_rep, x2, 0, font, fontSize));
            row.Add(new TextItem("Age:  " + dataApp.aBAge_rep, x2 + 80, 0, font, fontSize));
            row.Add(new TextItem("DOB:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aCDob_rep, x4, 0, font, fontSize));
            row.Add(new TextItem("Age:  " + dataApp.aCAge_rep, x4 + 80, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Email:", x1, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aBEmail, x2, 0, font, fontSize));
            row.Add(new TextItem("Email:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aCEmail, x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Home Phone:", x1, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aBHPhone, x2, 0, font, fontSize));
            row.Add(new TextItem("Home Phone:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aCHPhone, x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Cell Phone:", x1, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aBCellPhone, x2, 0, font, fontSize));
            row.Add(new TextItem("Cell Phone:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aCCellPhone, x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Work Phone:", x1, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aBBusPhone, x2, 0, font, fontSize));
            row.Add(new TextItem("Work Phone:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aCBusPhone, x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Fax:", x1, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aBFax, x2, 0, font, fontSize));
            row.Add(new TextItem("Fax:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aCFax, x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            string marital = "";
            row.Add(new TextItem("Marital Status:", x1, 0, font, fontSize));
            switch (dataApp.aBMaritalStatT) 
            {
                case E_aBMaritalStatT.Married: marital = "Married"; break;
                case E_aBMaritalStatT.NotMarried: marital = "Not Married"; break;
                case E_aBMaritalStatT.Separated: marital = "Separated"; break;
            }

            row.Add(new TextItem(marital, x2, 0, font, fontSize));

            marital = "";
            switch (dataApp.aBMaritalStatT) 
            {
                case E_aBMaritalStatT.Married: marital = "Married"; break;
                case E_aBMaritalStatT.NotMarried: marital = "Not Married"; break;
                case E_aBMaritalStatT.Separated: marital = "Separated"; break;
            }
            row.Add(new TextItem("Marital Status:", x3, 0, font, fontSize));
            row.Add(new TextItem(marital, x4, 0, font, fontSize));

            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Present Address:", x1, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aBAddr, x2, 0, font, fontSize));
            row.Add(new TextItem("Present Address:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataApp.aCAddr, x4, 0, font, fontSize));
            AddItem(row);

            string bCityStateZip = Tools.CombineCityStateZip(dataApp.aBCity, dataApp.aBState, dataApp.aBZip);
			string cCityStateZip = Tools.CombineCityStateZip(dataApp.aCCity, dataApp.aCState, dataApp.aCZip);
			if( bCityStateZip.Length > 0 || cCityStateZip.Length > 0 )
			{
				row = new RowItem();
				row.Add(new TextItem(bCityStateZip, x2, 0, font, fontSize));
				row.Add(new TextItem(cCityStateZip, x4, 0, font, fontSize));
				AddItem(row);
			}

            row = new RowItem();
            row.Add(new TextItem("Status:", x1, 0, font, fontSize));
            if (dataApp.aBAddrT != E_aBAddrT.LeaveBlank) 
            {
                row.Add(new TextItem(dataApp.aBAddrT.ToString(), x2, 0, font, fontSize));
            }
            row.Add(new TextItem("Years of Residence:  " + dataApp.aBAddrYrs, x2 + 50, 0, font, fontSize));

            row.Add(new TextItem("Status:", x3, 0, font, fontSize));
            if (dataApp.aCAddrT != E_aCAddrT.LeaveBlank) 
            {
                row.Add(new TextItem(dataApp.aCAddrT.ToString(), x4, 0, font, fontSize));
            }
            row.Add(new TextItem("Years of Residence:  " + dataApp.aCAddrYrs, x4 + 50, 0, font, fontSize));

            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Credit Scores:", x1, 0, font, fontSize));
            row.Add(new TextItem(string.Format("XP:  {0}     EQ:  {1}     TU:  {2}", dataApp.aBExperianScore_rep, dataApp.aBEquifaxScore_rep, dataApp.aBTransUnionScore_rep), x2, 0, font, fontSize));

            row.Add(new TextItem("Credit Scores:", x3, 0, font, fontSize));
            row.Add(new TextItem(string.Format("XP:  {0}     EQ:  {1}     TU:  {2}", dataApp.aCExperianScore_rep, dataApp.aCEquifaxScore_rep, dataApp.aCTransUnionScore_rep), x4, 0, font, fontSize));

            AddItem(row);

        }
        private void AddPropertyInformation(string sSpAddr, string CityStateZip, string sSpCounty, string sYrBuilt, string sPurchPrice, string sApprVal, string sDownPmtPc, string sEquity) 
        {
            FontName font = FontName.Helvetica;
            FontName boldFont = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Property Information", 40, 0, boldFont, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Property Address:", 40, 0, font, fontSize));
            row.Add(new TextItem(sSpAddr, 110, 0, font, fontSize));
            AddItem(row);

			if( CityStateZip.Length > 0 )
			{
				row = new RowItem();
				row.Add(new TextItem(CityStateZip, 110, 0, font, fontSize));
				AddItem(row);
			}

            row = new RowItem();
            row.Add(new TextItem("County:  " + sSpCounty, 110, 0, font, fontSize));
            row.Add(new TextItem("Year built:  " + sYrBuilt, 250, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
			string salesPrice = "Sales Price:  ";
			if (sPurchPrice.Length > 0)
				salesPrice += "$" + sPurchPrice;
            row.Add(new TextItem(salesPrice, 40, 0, font, fontSize));

			string apprVal = "Appraised Value:  ";
			if (sApprVal.Length > 0)
				apprVal += "$" + sApprVal;
			row.Add(new TextItem(apprVal, 150, 0, font, fontSize));

			row.Add(new TextItem("Down Payment:  " + sDownPmtPc + "%", 300, 0, font, fontSize));

			string equity = "Equity:  ";
			if (sEquity.Length > 0)
				equity += "$" + sEquity;
			row.Add(new TextItem(equity, 430, 0, font, fontSize));
            AddItem(row);
            AddItem(new HorizontalLineItem());
        }
        private void AddMortgageInformation(CPageData dataLoan) 
        {
            FontName boldFont = FontName.HelveticaBold;
            FontName font = FontName.Helvetica;
            short fontSize = 8;
            short x2 = 130;
            short x3 = 300;
            short x4 = 400;

            RowItem row = new RowItem();
            row.Add(new TextItem("Mortgage Information", 40, 0, boldFont, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Loan Number:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sLNm, x2, 0, font, fontSize));
            row.Add(new TextItem("Lien Position:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sLienPosT.ToString(), x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Loan Amount:", 40, 0, font, fontSize));
            row.Add(new TextItem("$" + dataLoan.sFinalLAmt_rep, x2, 0, font, fontSize));
            row.Add(new TextItem("Loan Program:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sLpTemplateNm, x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Loan Purpose:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sLPurposeT_rep, x2, 0, font, fontSize));
            row.Add(new TextItem("Loan Status:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sStatusT_rep, x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Note Rate:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sNoteIR_rep + "%", x2, 0, font, fontSize));
            row.Add(new TextItem("Lender Case Number:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sLenderCaseNum, x4, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Term / Due:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sTerm_rep + " / " + dataLoan.sDue_rep + " (months)", x2, 0, font, fontSize));
            row.Add(new TextItem("Agency Case Number:", x3, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sAgencyCaseNum, x4, 0, font, fontSize));
            AddItem(row);

            string interestOnlyLabel = dataLoan.sIOnlyMon_rep == "" ? "" : " (* Interest Only)";
            row = new RowItem();
            row.Add(new TextItem("Monthly Payment:", 40, 0, font, fontSize));
            row.Add(new TextItem("$" + dataLoan.sMonthlyPmt_rep + interestOnlyLabel, x2, 0, font, fontSize));
            AddItem(row);

            AddItem(new HorizontalLineItem());

        }
        private void AddNotes(string sTrNotes) 
        {
            FontName boldFont = FontName.HelveticaBold;
            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Notes", 40, 0, boldFont, fontSize));
            AddItem(row);

            row = new RowItem();
            if (sTrNotes.TrimWhitespaceAndBOM() == "") 
            {
                row.Add(new TextItem("N/A", 40, 0, 480, font, fontSize, HorizontalAlign.Center));
                AddItem(row);
            } 
            else 
            {
                // 2/25/2005 dd - Hack to get sTrNotes to printout across multiple pages.
                TextItem item = new TextItem(sTrNotes, 40, 0, 480, font, fontSize);
                item.GetPreferredSize();

                foreach (string str in item.Lines) 
                {
                    row = new RowItem();
                    row.Add(new TextItem(str, 40, 0, 480, font, fontSize));
                    AddItem(row);
                }

            }

            AddItem(new HorizontalLineItem());
        }
        private void AddUnderwritingInformation(CPageData dataLoan) 
        {
            FontName boldFont = FontName.HelveticaBold;
            FontName font = FontName.Helvetica;

            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Underwriting Information", 40, 0, boldFont, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Qualifying Ratios", 40, 0, boldFont, fontSize));
            row.Add(new TextItem("Loan-To-Value Ratios", 200, 0, boldFont, fontSize));

			row.Add(new TextItem("Total Income:", 380, 0, font, fontSize));
			string income = dataLoan.sLTotI_rep;
			if (income.Length > 0)
				income = "$" + income;
            row.Add(new TextItem(income, 480, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Housing:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sQualTopR_rep + "%", 80, 0, font, fontSize));
            row.Add(new TextItem("LTV:", 200, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sLtvR_rep + "%", 240, 0, font, fontSize));
            row.Add(new TextItem("Total Liabilities:", 380, 0, font, fontSize));
			string liabilities = dataLoan.sLiaMonLTot_rep;
			if (liabilities.Length > 0)
				liabilities = "$" + liabilities;
            row.Add(new TextItem(liabilities, 480, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Debt:", 40, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sQualBottomR_rep + "%", 80, 0, font, fontSize));
            row.Add(new TextItem("CLTV:", 200, 0, font, fontSize));
            row.Add(new TextItem(dataLoan.sCltvR_rep + "%", 240, 0, font, fontSize));
            row.Add(new TextItem("Present Housing Expense:", 380, 0, font, fontSize));
			string presHousExp = dataLoan.sPresLTotPersistentHExp_rep;
			if (presHousExp.Length > 0)
				presHousExp = "$" + presHousExp;
            row.Add(new TextItem(presHousExp, 480, 0, font, fontSize));
            AddItem(row);

            AddItem(new HorizontalLineItem());

        }
        private void AddLoanStatusHeader(CPageData dataLoan) 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Loan Status", 40, 0, font, fontSize));
            AddItem(row);

			row = new RowItem();
			row.Add(new TextItem("Lead Source:", 40, 0, FontName.Helvetica, fontSize));
			row.Add(new TextItem(dataLoan.sLeadSrcDesc, 100, 0, FontName.Helvetica, fontSize));
			AddItem(row);

			row = new RowItem();
            row.Add(new TextItem("Date", 138, 0, font, fontSize));
            row.Add(new TextItem("Comments", 210, 0, font, fontSize));
            AddItem(row);
        }

        private void AddLoanStatusItem(string status, string date, string comments) 
        {
            if (date.TrimWhitespaceAndBOM() == "" && comments.TrimWhitespaceAndBOM() == "") return;
            FontName font = FontName.Helvetica;
            short fontSize = 8;


            RowItem row = new RowItem();
            row.Add(new TextItem(status, 40, 0, 85, font, fontSize));
            row.Add(new TextItem(date, 138, 0, font, fontSize));
            row.Add(new TextItem(comments, 210, 570, font, fontSize));
            AddItem(row);

        }

        private void AddBasicDocumentsHeader() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Basic Documents", 40, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Ordered", 150, 0, font, fontSize));
            row.Add(new TextItem("Received", 200, 0, font, fontSize));
            row.Add(new TextItem("Comments", 250, 0, 190, font, fontSize));
            row.Add(new TextItem("Days out", 450, 0, font, fontSize));
            row.Add(new TextItem("Days in", 500, 0, font, fontSize));
            AddItem(row);
        }

        private void AddBasicDocumentItem(string description, string ordered, string received, string comments) 
        {
            if (ordered.TrimWhitespaceAndBOM() == "" && received.TrimWhitespaceAndBOM() == "" && comments.TrimWhitespaceAndBOM() == "")
            {
                return; // Skip empty entry.
            }

            // Calculate daysOut & daysIn.
            string daysOut = "";
            string daysIn = "";

            if (ordered.TrimWhitespaceAndBOM() != "") 
            {
                DateTime dt;

                if (DateTime.TryParse(ordered, out dt))
                {
                    daysOut = DateTime.Now.Subtract(dt).Days.ToString();
                }
            }

            if (received.TrimWhitespaceAndBOM() != "") 
            {
                DateTime dt;

                if (DateTime.TryParse(received, out dt))
                {
                    daysIn = DateTime.Now.Subtract(dt).Days.ToString();
                }
            }

            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem(description, 40, 0, font, fontSize));
            row.Add(new TextItem(ordered, 150, 0, font, fontSize));
            row.Add(new TextItem(received, 200, 0, font, fontSize));
            row.Add(new TextItem(comments, 250, 0, 190, font, fontSize));
            row.Add(new TextItem(daysOut, 450, 0, font, fontSize));
            row.Add(new TextItem(daysIn, 500, 0, font, fontSize));
            AddItem(row);

        }
        private void AddGenericDocumentHeader(string header, string desc) 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem(header, 40, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem(desc, 40, 0, font, fontSize));
            row.Add(new TextItem("Ordered", 300, 0, font, fontSize));
            row.Add(new TextItem("Re-order", 350, 0, font, fontSize));
            row.Add(new TextItem("Received", 400, 0, font, fontSize));
            row.Add(new TextItem("Days out", 450, 0, font, fontSize));
            row.Add(new TextItem("Days in", 500, 0, font, fontSize));
            AddItem(row);

        }
        private void AddGenericDocumentItem(string description, string ordered, string reordered, string received) 
        {
            if (ordered.TrimWhitespaceAndBOM() == "" && received.TrimWhitespaceAndBOM() == "" && reordered.TrimWhitespaceAndBOM() == "")
                return; // Skip empty entry.

            // Calculate daysOut & daysIn.
            string daysOut = "";
            string daysIn = "";

            if (ordered.TrimWhitespaceAndBOM() != "") 
            {
                DateTime dt;

                if (DateTime.TryParse(ordered, out dt))
                {
                    daysOut = DateTime.Now.Subtract(dt).Days.ToString();
                }
            }

            if (received.TrimWhitespaceAndBOM() != "") 
            {
                DateTime dt;

                if (DateTime.TryParse(received, out dt))
                {
                    daysIn = DateTime.Now.Subtract(dt).Days.ToString();
                }
            }

            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem(description, 40, 0, 250, font, fontSize));
            row.Add(new TextItem(ordered, 300, 0, font, fontSize));
            row.Add(new TextItem(reordered, 350, 0, font, fontSize));
            row.Add(new TextItem(received, 400, 0, font, fontSize));
            row.Add(new TextItem(daysOut, 450, 0, font, fontSize));
            row.Add(new TextItem(daysIn, 500, 0, font, fontSize));
            AddItem(row);
        }

        private void AddConditionHeader() 
        {
			FontName font = FontName.HelveticaBold;
			short fontSize = 8;

			RowItem row = new RowItem();
			row.Add(new TextItem("Conditions", 40, 0, font, fontSize));
			AddItem(row);

			row = new RowItem();
			row.Add(new TextItem("Done?", 40, 0, font, fontSize));

			if( UseUnderwritingConditions == true )
			{
				row.Add(new TextItem("Date Done", 80, 0, font, fontSize));
				row.Add(new TextItem("Category", 140, 0, 100, font, fontSize));
				row.Add(new TextItem("Description", 240, 0, 330, font, fontSize));
			}
			else
			{
				row.Add(new TextItem("Date Done", 80, 0, font, fontSize));
				row.Add(new TextItem("Prior To", 140, 0, 100, font, fontSize));
				row.Add(new TextItem("Description", 240, 0, 330, font, fontSize));
			}

			AddItem(row);
		}

        private void AddCondition(string done, string dateDone, string required, string priorTo, string description) 
        {
			FontName font = FontName.Helvetica;
			short fontSize = 8;

			RowItem row = new RowItem();
			row.Add(new TextItem(done, 40, 0, font, fontSize));

			if ( UseUnderwritingConditions == true )
			{
				string dateDoneFormatted = "";
                DateTime dt;

                if (DateTime.TryParse(dateDone, out dt))
                {
                    dateDoneFormatted = dt.ToShortDateString();
                }
                else
                {
                    dateDoneFormatted = dateDone;
                }

				row.Add(new TextItem(dateDoneFormatted, 80, 0, font, fontSize));
			}
			else
			{
				row.Add(new TextItem(dateDone, 80, 0, font, fontSize));
			}

			row.Add(new TextItem(priorTo, 140, 0, 100, font, fontSize));
			row.Add(new TextItem(description, 240, 0, 330, font, fontSize));

			AddItem(row);
		}

        private void AddAgentListHeader() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;
            RowItem row = new RowItem();
            row.Add(new TextItem("Agent List", 40, 0, font, fontSize));
            AddItem(row);
        }

		private void AddAgent(string agentDescription, string caseNumber, string agentName, string phone, string fax, string companyName, string cellphone, string streetAddr, string cityStateZip, string email) 
        {
            FontName font = FontName.Helvetica;
            FontName boldFont = FontName.HelveticaBold;
            short fontSize = 8;
            short x = 150;
            short x1 = 200;

            RowItem row = new RowItem();
            row.Add(new TextItem(agentDescription + ":", 40, 0, boldFont, fontSize));
            row.Add(new TextItem("Name:", x, 0, font, fontSize));
            row.Add(new TextItem(agentName, x1, 0, font, fontSize));

            row.Add(new TextItem("Case #:", 400, 0, font, fontSize));
            row.Add(new TextItem(caseNumber, 430, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Phone:", x, 0, font, fontSize));
            row.Add(new TextItem(phone, x1, 0, font, fontSize));
			row.Add(new TextItem("Cell:", 400, 0, font, fontSize));
			row.Add(new TextItem(cellphone, 430, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Email:", x, 0, font, fontSize));
            row.Add(new TextItem(email, x1, 0, font, fontSize));
            row.Add(new TextItem("Fax:", 400, 0, font, fontSize));
            row.Add(new TextItem(fax, 430, 0, font, fontSize));
            AddItem(row);


            row = new RowItem();
            row.Add(new TextItem("Company:", x, 0, font, fontSize));
            row.Add(new TextItem(companyName, x1, 0, font, fontSize));

			AddItem(row);

			row = new RowItem();
            row.Add(new TextItem("Address:", x, 0, font, fontSize));
            row.Add(new TextItem(streetAddr,x1,  0, font, fontSize));
            AddItem(row);

			if( cityStateZip.Length > 0 )
			{
				row = new RowItem();
				row.Add(new TextItem(cityStateZip, x1, 0, font, fontSize));
				AddItem(row);
			}


        }
        private void AddAgent(string agentDescription, string agentName, string phone, string fax, string cellphone, string email) 
        {
            FontName font = FontName.Helvetica;
            FontName boldFont = FontName.HelveticaBold;
            short fontSize = 8;
            short x = 150;
            short x1 = 200;

            RowItem row = new RowItem();
            row.Add(new TextItem(agentDescription + ":", 40, 0, boldFont, fontSize));
            row.Add(new TextItem("Name:", x, 0, font, fontSize));
            row.Add(new TextItem(agentName, x1, 0, font, fontSize));
			row.Add(new TextItem("Cell:", 400, 0, font, fontSize));
			row.Add(new TextItem(cellphone, 430, 0, font, fontSize));
			AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Phone:", x, 0, font, fontSize));
            row.Add(new TextItem(phone, x1, 0, font, fontSize));
			row.Add(new TextItem("Fax:", 400, 0, font, fontSize));
			row.Add(new TextItem(fax, 430, 0, font, fontSize));
			AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Email:", x, 0, font, fontSize));
            row.Add(new TextItem(email, x1, 0, font, fontSize));
            AddItem(row);

        }

        private void AddEmployeInRoleHeader() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("People Internally Assigned to this Loan", 40, 0, font, fontSize));
            AddItem(row);
        }
        private void AddEmployeeInRole(string roleModifiableDesc, CEmployeeFields employee) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 8;
            
            RowItem row = new RowItem();
            row.Add(new TextItem(roleModifiableDesc + ":", 40, 0, font, fontSize));
			row.Add(new TextItem(employee.FullName, 150, 0, font, fontSize));
            row.Add(new TextItem("Phone:  " + employee.Phone, 300, 0, font, fontSize));
            row.Add(new TextItem("Email:  " + employee.Email, 400, 0, font, fontSize));
            AddItem(row);


        }

	}
}
