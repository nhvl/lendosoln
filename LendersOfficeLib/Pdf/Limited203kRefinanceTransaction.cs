﻿namespace LendersOffice.Pdf
{
    /// <summary>
    /// PDF class for Limited 203k Refinance Transaction Form.
    /// </summary>
    public class Limited203kRefinanceTransaction : Abstract203kMaxMortgagePdf
    {
        /// <summary>
        /// PDF Description.
        /// </summary>
        public override string Description
        {
            get
            {
                return "Limited 203k Refinance Transaction";
            }
        }

        /// <summary>
        /// PDF File Name.
        /// </summary>
        public override string PdfFile
        {
            get
            {
                return "203(k)_MM_Limited_Refinance.pdf";
            }
        }
    }
}
