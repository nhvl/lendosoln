namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Threading;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LOPDFGenLib;

    public class CLoanTrackingPDF : AbstractFreeLayoutPDFFile
	{

        public override string Description 
        {
            get { return "Loan Tracking Report"; }
        }
        public override bool HasPrintPermission
        {
            get
            {
                // 3/10/2010 dd - This PDF use sCommissionTotal and sNetProfit which require user to have appropriate permission. Instead of throwing
                // exception when user print the form, we check for permission first.
                AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

                return base.HasPrintPermission && principal.HasPermission(Permission.AllowCloserRead) && principal.HasPermission(Permission.AllowAccountantRead);
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
//            IsDebug = true;
            AddHeader(dataLoan.sLNm, dataApp.aBNm, dataApp.aBHPhone, dataApp.aBBusPhone, dataApp.aBCellPhone, dataApp.aBEmail, dataLoan.sEstCloseD_rep, dataLoan.sOpenedD_rep);

            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                AddBorrowerInfo(dataApp.aCNm, dataApp.aCHPhone, dataApp.aCBusPhone, dataApp.aCEmail, dataApp.aCCellPhone);
            }
            for (int i = 1; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                if (d.aBNm.TrimWhitespaceAndBOM() != "") 
                {
                    AddBorrowerInfo(d.aBNm, d.aBHPhone, d.aBBusPhone, d.aBEmail, d.aBCellPhone);
                }
                if (d.aCNm.TrimWhitespaceAndBOM() != "") 
                {
                    AddBorrowerInfo(d.aCNm, d.aCHPhone, d.aCBusPhone, d.aCEmail, d.aCCellPhone);
                }
            }
            AddSubjectPropertyInfo(dataLoan.sSpAddr, Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip), dataLoan.sSpCounty);

            AddLoanAgent("Loan Officer", dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            AddLoanAgent("Processor", dataLoan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            AddLoanInformation(dataLoan.sLAmtCalc_rep, dataLoan.sFinalLAmt_rep, dataLoan.sNoteIR_rep, dataLoan.sTerm_rep, dataLoan.sDue_rep, 
                               dataLoan.sLtvR_rep, dataLoan.sCltvR_rep, dataLoan.sQualTopR_rep, dataLoan.sQualBottomR_rep);

            AddLeadSource(dataLoan.sLeadSrcDesc);

            #region "Add Loan Status"
            AddLoanStatusHeader();

            AddLoanStatusItem("Rate Lock", dataLoan.sRLckdD_rep, dataLoan.sRLckdN);
            AddLoanStatusItem("Rate Lock Expired", dataLoan.sRLckdExpiredD_rep, dataLoan.sRLckdExpiredN);
            AddLoanStatusItem("Lead New", dataLoan.sLeadD_rep, dataLoan.sLeadN);
            AddLoanStatusItem("Opened", dataLoan.sOpenedD_rep, dataLoan.sOpenedN);
            AddLoanStatusItem("Pre-qual", dataLoan.sPreQualD_rep, dataLoan.sPreQualN);
            AddLoanStatusItem("Preapproved", dataLoan.sPreApprovD_rep, dataLoan.sPreApprovN);
            AddLoanStatusItem("Submitted", dataLoan.sSubmitD_rep, dataLoan.sSubmitN);
            AddLoanStatusItem("Approved", dataLoan.sApprovD_rep, dataLoan.sApprovN);
            AddLoanStatusItem("Est Closing", dataLoan.sEstCloseD_rep, dataLoan.sEstCloseN);
            AddLoanStatusItem("Documents", dataLoan.sDocsD_rep, dataLoan.sDocsN);
            AddLoanStatusItem("Funded", dataLoan.sFundD_rep, dataLoan.sFundN);
            AddLoanStatusItem("Schedule Fund", dataLoan.sSchedFundD_rep, dataLoan.sSchedFundN);
            AddLoanStatusItem("Recorded", dataLoan.sRecordedD_rep, dataLoan.sRecordedN);
            AddLoanStatusItem("Loan On-hold", dataLoan.sOnHoldD_rep, dataLoan.sOnHoldN);
            AddLoanStatusItem("Loan Canceled", dataLoan.sCanceledD_rep, dataLoan.sCanceledN);
            AddLoanStatusItem("Loan Denied", dataLoan.sRejectD_rep, dataLoan.sRejectN);
            AddLoanStatusItem("Loan Suspended", dataLoan.sSuspendedD_rep, dataLoan.sSuspendedN);
            AddLoanStatusItem("Loan Closed", dataLoan.sClosedD_rep, dataLoan.sClosedN);
            AddLoanStatusItem(dataLoan.sU1LStatDesc, dataLoan.sU1LStatD_rep, dataLoan.sU1LStatN);
            AddLoanStatusItem(dataLoan.sU2LStatDesc, dataLoan.sU2LStatD_rep, dataLoan.sU2LStatN);
            AddLoanStatusItem(dataLoan.sU3LStatDesc, dataLoan.sU3LStatD_rep, dataLoan.sU3LStatN);
            AddLoanStatusItem(dataLoan.sU4LStatDesc, dataLoan.sU4LStatD_rep, dataLoan.sU4LStatN);

            AddItem(new HorizontalLineItem());
            #endregion 


            AddsTrNotes(dataLoan.sTrNotes);

            AddBrokerInformation(dataLoan.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            AddLenderInformation(dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject),
                dataLoan.sLenderCaseNum, dataLoan.sNoteIR_rep, dataLoan.sRLckdD_rep, dataLoan.sRLckdNumOfDays_rep, dataLoan.sRLckdExpiredD_rep,
                dataLoan.sProfitGrossBor_rep, dataLoan.sProfitGrossBorPnt_rep, dataLoan.sProfitGrossBorMb_rep, dataLoan.sProfitRebate_rep, 
                dataLoan.sProfitRebatePnt_rep, dataLoan.sProfitRebateMb_rep, dataLoan.sCommissionTotal_rep, dataLoan.sGrossProfit_rep, dataLoan.sNetProfit_rep);
            #region "Add Trust Accounts"
            AddTrustAccountHeader();

            AddTrustAccountItem(dataLoan.sTrust1Desc, dataLoan.sTrust1D_rep, dataLoan.sTrust1ReceivableChkNum.Value, 
                dataLoan.sTrust1ReceivableAmt_rep, dataLoan.sTrust1PayableChkNum.Value, dataLoan.sTrust1PayableAmt_rep, dataLoan.sTrust1ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust2Desc, dataLoan.sTrust2D_rep, dataLoan.sTrust2ReceivableChkNum.Value, 
                dataLoan.sTrust2ReceivableAmt_rep, dataLoan.sTrust2PayableChkNum.Value, dataLoan.sTrust2PayableAmt_rep, dataLoan.sTrust2ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust3Desc, dataLoan.sTrust3D_rep, dataLoan.sTrust3ReceivableChkNum.Value, 
                dataLoan.sTrust3ReceivableAmt_rep, dataLoan.sTrust3PayableChkNum.Value, dataLoan.sTrust3PayableAmt_rep, dataLoan.sTrust3ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust4Desc, dataLoan.sTrust4D_rep, dataLoan.sTrust4ReceivableChkNum.Value, 
                dataLoan.sTrust4ReceivableAmt_rep, dataLoan.sTrust4PayableChkNum.Value, dataLoan.sTrust4PayableAmt_rep, dataLoan.sTrust4ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust5Desc, dataLoan.sTrust5D_rep, dataLoan.sTrust5ReceivableChkNum.Value, 
                dataLoan.sTrust5ReceivableAmt_rep, dataLoan.sTrust5PayableChkNum.Value, dataLoan.sTrust5PayableAmt_rep, dataLoan.sTrust5ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust6Desc, dataLoan.sTrust6D_rep, dataLoan.sTrust6ReceivableChkNum.Value, 
                dataLoan.sTrust6ReceivableAmt_rep, dataLoan.sTrust6PayableChkNum.Value, dataLoan.sTrust6PayableAmt_rep, dataLoan.sTrust6ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust7Desc, dataLoan.sTrust7D_rep, dataLoan.sTrust7ReceivableChkNum.Value, 
                dataLoan.sTrust7ReceivableAmt_rep, dataLoan.sTrust7PayableChkNum.Value, dataLoan.sTrust7PayableAmt_rep, dataLoan.sTrust7ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust8Desc, dataLoan.sTrust8D_rep, dataLoan.sTrust8ReceivableChkNum.Value, 
                dataLoan.sTrust8ReceivableAmt_rep, dataLoan.sTrust8PayableChkNum.Value, dataLoan.sTrust8PayableAmt_rep, dataLoan.sTrust8ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust9Desc, dataLoan.sTrust9D_rep, dataLoan.sTrust9ReceivableChkNum.Value, 
                dataLoan.sTrust9ReceivableAmt_rep, dataLoan.sTrust9PayableChkNum.Value, dataLoan.sTrust9PayableAmt_rep, dataLoan.sTrust9ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust10Desc, dataLoan.sTrust10D_rep, dataLoan.sTrust10ReceivableChkNum.Value, 
                dataLoan.sTrust10ReceivableAmt_rep, dataLoan.sTrust10PayableChkNum.Value, dataLoan.sTrust10PayableAmt_rep, dataLoan.sTrust10ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust11Desc, dataLoan.sTrust11D_rep, dataLoan.sTrust11ReceivableChkNum.Value, 
                dataLoan.sTrust11ReceivableAmt_rep, dataLoan.sTrust11PayableChkNum.Value, dataLoan.sTrust11PayableAmt_rep, dataLoan.sTrust11ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust12Desc, dataLoan.sTrust12D_rep, dataLoan.sTrust12ReceivableChkNum.Value, 
                dataLoan.sTrust12ReceivableAmt_rep, dataLoan.sTrust12PayableChkNum.Value, dataLoan.sTrust12PayableAmt_rep, dataLoan.sTrust12ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust13Desc, dataLoan.sTrust13D_rep, dataLoan.sTrust13ReceivableChkNum.Value, 
                dataLoan.sTrust13ReceivableAmt_rep, dataLoan.sTrust13PayableChkNum.Value, dataLoan.sTrust13PayableAmt_rep, dataLoan.sTrust13ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust14Desc, dataLoan.sTrust14D_rep, dataLoan.sTrust14ReceivableChkNum.Value, 
                dataLoan.sTrust14ReceivableAmt_rep, dataLoan.sTrust14PayableChkNum.Value, dataLoan.sTrust14PayableAmt_rep, dataLoan.sTrust14ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust15Desc, dataLoan.sTrust15D_rep, dataLoan.sTrust15ReceivableChkNum.Value, 
                dataLoan.sTrust15ReceivableAmt_rep, dataLoan.sTrust15PayableChkNum.Value, dataLoan.sTrust15PayableAmt_rep, dataLoan.sTrust15ReceivableN);
            AddTrustAccountItem(dataLoan.sTrust16Desc, dataLoan.sTrust16D_rep, dataLoan.sTrust16ReceivableChkNum.Value, 
                dataLoan.sTrust16ReceivableAmt_rep, dataLoan.sTrust16PayableChkNum.Value, dataLoan.sTrust16PayableAmt_rep, dataLoan.sTrust16ReceivableN);
            AddTrustAccountFooter(dataLoan.sTrustReceivableTotal_rep, dataLoan.sTrustPayableTotal_rep, dataLoan.sTrustBalance_rep);

            #endregion 

            #region "Add Basic Documents"
            AddBasicDocumentsHeader();

            AddBasicDocumentItem("Preliminary Report", dataLoan.sPrelimRprtOd_rep, dataLoan.sPrelimRprtRd_rep, dataLoan.sPrelimRprtN);
            AddBasicDocumentItem("Appraisal Report", dataLoan.sApprRprtOd_rep, dataLoan.sApprRprtRd_rep, dataLoan.sApprRprtN);
            AddBasicDocumentItem("TIL Disclosure / GFE", dataLoan.sTilGfeOd_rep, dataLoan.sTilGfeRd_rep, dataLoan.sTilGfeN);
            AddBasicDocumentItem(dataLoan.sU1DocStatDesc, dataLoan.sU1DocStatOd_rep, dataLoan.sU1DocStatRd_rep, dataLoan.sU1DocStatN);
            AddBasicDocumentItem(dataLoan.sU2DocStatDesc, dataLoan.sU2DocStatOd_rep, dataLoan.sU2DocStatRd_rep, dataLoan.sU2DocStatN);
            AddBasicDocumentItem(dataLoan.sU3DocStatDesc, dataLoan.sU3DocStatOd_rep, dataLoan.sU3DocStatRd_rep, dataLoan.sU3DocStatN);
            AddBasicDocumentItem(dataLoan.sU4DocStatDesc, dataLoan.sU4DocStatOd_rep, dataLoan.sU4DocStatRd_rep, dataLoan.sU4DocStatN);
            AddBasicDocumentItem(dataLoan.sU5DocStatDesc, dataLoan.sU5DocStatOd_rep, dataLoan.sU5DocStatRd_rep, dataLoan.sU5DocStatN);
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i); 
                AddBasicDocumentItem("Credit Report", d.aCrOd_rep, d.aCrRd_rep, d.aCrN);
                AddBasicDocumentItem("Business Credit Report", d.aBusCrOd_rep, d.aBusCrRd_rep, d.aBusCrN);
                AddBasicDocumentItem(d.aU1DocStatDesc, d.aU1DocStatOd_rep, d.aU1DocStatRd_rep, d.aU1DocStatN);
                AddBasicDocumentItem(d.aU2DocStatDesc, d.aU2DocStatOd_rep, d.aU2DocStatRd_rep, d.aU2DocStatN);

            }
            AddItem(new HorizontalLineItem());

            #endregion 

            #region "Add Verification of Employments"
            AddGenericDocumentHeader("Verification of Employment:", "Employer");
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                IEmpCollection recordList = d.aBEmpCollection;
                var subCollection = recordList.GetSubcollection(true, E_EmpGroupT.All);
                foreach (IEmploymentRecord f in subCollection) 
                {
                    AddGenericDocumentItem("B " + f.EmplrNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);

                }

                recordList = d.aCEmpCollection;
                subCollection = recordList.GetSubcollection(true, E_EmpGroupT.All);
                foreach (IEmploymentRecord f in subCollection) 
                {
                    AddGenericDocumentItem("C " + f.EmplrNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

            }
            AddItem(new HorizontalLineItem());

            #endregion 
            #region "Add Verification of Deposits"
            AddGenericDocumentHeader("Verification of Deposit:", "Depository");
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                var subcoll = d.aAssetCollection.GetSubcollection( false, E_AssetGroupT.DontNeedVOD );
                Hashtable hash = new Hashtable();
                foreach( var item in subcoll )
                {
                    var f = (IAssetRegular)item;
                    if (!hash.ContainsKey(f.ComNm.ToLower())) 
                    {
                        hash.Add(f.ComNm.ToLower(), f.ComNm.ToLower());
                        AddGenericDocumentItem(f.ComNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                    }
                }


            }
            AddItem(new HorizontalLineItem());
            #endregion 

            #region "Add verification of Rent"
            AddGenericDocumentHeader("Verification of Mortgage/Rent", "Creditor / Landlord");
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                IVerificationOfRent f = null;
                string addr = string.Format("{0}, {1}, {2} {3}", d.aBAddr, d.aBCity, d.aBState, d.aBZip);

                if (d.aBAddrT == E_aBAddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("11111111-1111-1111-1111-111111111111"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

                addr = string.Format("{0}, {1}, {2} {3}", d.aBPrev1Addr, d.aBPrev1City, d.aBPrev1State, d.aBPrev1Zip);
                if (d.aBPrev1AddrT == E_aBPrev1AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("22222222-2222-2222-2222-222222222222"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }
                addr = string.Format("{0}, {1}, {2} {3}", d.aBPrev2Addr, d.aBPrev2City, d.aBPrev2State, d.aBPrev2Zip);
                if (d.aBPrev2AddrT == E_aBPrev2AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("33333333-3333-3333-3333-333333333333"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

                addr = string.Format("{0}, {1}, {2} {3}", d.aCAddr, d.aCCity, d.aCState, d.aCZip);
                if (d.aCAddrT == E_aCAddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("44444444-4444-4444-4444-444444444444"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

                addr = string.Format("{0}, {1}, {2} {3}", d.aCPrev1Addr, d.aCPrev1City, d.aCPrev1State, d.aCPrev1Zip);
                if (d.aCPrev1AddrT == E_aCPrev1AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("55555555-5555-5555-5555-555555555555"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }
                addr = string.Format("{0}, {1}, {2} {3}", d.aCPrev2Addr, d.aCPrev2City, d.aCPrev2State, d.aCPrev2Zip);
                if (d.aCPrev2AddrT == E_aCPrev2AddrT.Rent && addr.TrimWhitespaceAndBOM() != ", ,") 
                {
                    f = d.GetVorFields(new Guid("66666666-6666-6666-6666-666666666666"));
                    AddGenericDocumentItem(f.LandlordCreditorName, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

            }
            #endregion 

            #region "Add Verification of Mortgage"
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                ILiaCollection liaList = d.aLiaCollection;
                var liaSubcoll = liaList.GetSubcollection( true, E_DebtGroupT.Regular );
                foreach( ILiabilityRegular f in liaSubcoll )
                {
                    if (f.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc)) 
                    {
                        AddGenericDocumentItem(f.ComNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                    } 
                }
            }

            AddItem(new HorizontalLineItem());

            #endregion 

            AddGenericDocumentHeader("Verification of Loan", "Lender");

            #region "Add Verification of Loan"
            for (int i = 0; i < dataLoan.nApps; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                ILiaCollection liaList = d.aLiaCollection;
                var liaSubcoll = liaList.GetSubcollection( true, E_DebtGroupT.Regular );
                foreach( ILiabilityRegular f in liaSubcoll )
                {
                    if (f.DebtT == E_DebtRegularT.Installment || f.DebtT == E_DebtRegularT.Revolving) 
                    {
                        AddGenericDocumentItem(f.ComNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                    }
                }
            }
            #endregion 
        }


        protected override void AddTitle() 
        {
            FontName boldFont = FontName.HelveticaBold;
            short fontSize = 12;

            RowItem row = new RowItem(0, 40);
            row.Add(new TextItem("LOAN TRACKING REPORT", 240, 0, boldFont, fontSize));
            AddItem(row);

        }
        private void AddsTrNotes(string sTrNotes) 
        {
            FontName boldFont = FontName.HelveticaBold;
            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Notes:", 40, 0, boldFont, fontSize));
            AddItem(row);

            row = new RowItem();
            if (sTrNotes.TrimWhitespaceAndBOM() == "") 
            {
                row.Add(new TextItem("N/A", 40, 0, 480, font, fontSize, HorizontalAlign.Center));
                AddItem(row);
            } 
            else 
            {
                // 2/25/2005 dd - Hack to get sTrNotes to printout across multiple pages.
                TextItem item = new TextItem(sTrNotes, 50, 0, 480, font, fontSize);
                item.GetPreferredSize();

                foreach (string str in item.Lines) 
                {
                    row = new RowItem();
                    row.Add(new TextItem(str, 50, 0, 480, font, fontSize));
                    AddItem(row);
                }

            }

            AddItem(new HorizontalLineItem());

        }
        private void AddLeadSource(string sLeadSrcDesc) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 8;
            RowItem row = new RowItem();
            row.Add(new TextItem("Lead Source", 40, 0, font, fontSize));
            row.Add(new TextItem(sLeadSrcDesc, 138, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem(" ", 40, 0, font, fontSize));
            AddItem(row);
        }
        private void AddHeader(string sLNm, string aBNm, string aBHPhone, string aBBusPhone, string aBCellPhone, string aBEmail, string sEstCloseD, string sOpenedD) 
        {
            FontName boldFont = FontName.HelveticaBold;
            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Loan Number: ", 40, 0, font, fontSize));
            row.Add(new TextItem(sLNm, 100, 0, boldFont, fontSize));
            row.Add(new TextItem("Date/Time", 378, 0, font, fontSize));
            row.Add(new TextItem(" - " + DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"), 450, 0, font, fontSize));

            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Borrower: " , 40, 0, font, fontSize));
            row.Add(new TextItem(aBNm, 100, 0, boldFont, fontSize));
            row.Add(new TextItem("Est Closing", 378, 0, font, fontSize));
            row.Add(new TextItem(" - " + sEstCloseD, 450, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Phone: " + aBHPhone + " / Work: " + aBBusPhone, 100, 0, font, fontSize));
            row.Add(new TextItem("Days in process", 378, 0, font, fontSize));
            row.Add(new TextItem(" - " + DateTime.Now.Subtract(DateTime.Parse(sOpenedD)).Days.ToString(), 450, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Cell: " + aBCellPhone + " / Email: " + aBEmail, 100, 0, font, fontSize));
            AddItem(row);

        }
        private void AddBorrowerInfo(string Nm, string HPhone, string BusPhone, string Email, string Cellphone) 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8; 

            RowItem row = new RowItem();
            row.Add(new TextItem(Nm, 100, 0, font, fontSize));
            AddItem(row);
            font = FontName.Helvetica;

            row = new RowItem();
            row.Add(new TextItem("Phone: " + HPhone + " / Work: " + BusPhone, 100, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Cell: " + Cellphone + " / Email: " + Email, 100, 0, font, fontSize));
            AddItem(row);

        }

        private void AddSubjectPropertyInfo(string sSpAddr, string CityStateZip, string sSpCounty) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Property", 40, 0, font, fontSize));
            row.Add(new TextItem(sSpAddr, 100, 0, font, fontSize));

            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem(CityStateZip + " / County: " + sSpCounty, 100, 0, font, fontSize));
            AddItem(row);
        }
        private void AddLoanAgent(string description, CAgentFields agent) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 8;
            RowItem row = new RowItem();
            row.Add(new TextItem(description, 40, 0, font, fontSize));
            row.Add(new TextItem(" - " + agent.AgentName, 100, 0, font, fontSize));

            AddItem(row);
        }
        private void AddLoanInformation(string sLAmt, string sFinalLAmt, string sNoteIR, string sTerm, string sDue, string sLtvR, string sCltvR, string sQualTopR, string sQualBottomR) 
        {
            FontName font = FontName.Helvetica;
            FontName boldFont = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Loan Amount", 40, 0, font, fontSize));
            row.Add(new TextItem(sLAmt, 100, 0, 70, boldFont, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem("w / MIP, FF", 180, 0, font, fontSize));
            row.Add(new TextItem(sFinalLAmt, 230, 0, 70, boldFont, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem("Rate", 330, 0, font, fontSize));
            row.Add(new TextItem(sNoteIR, 360, 0, 70, boldFont, fontSize));
            row.Add(new TextItem("Term / Due", 400, 0, font, fontSize));
            row.Add(new TextItem(sTerm + " / " + sDue, 460, 0, boldFont, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("LTV / CLTV", 40, 0, font, fontSize));
            row.Add(new TextItem(sLtvR + " / " + sCltvR, 100, 0, boldFont, fontSize));
            row.Add(new TextItem("Top / Bottom", 190, 0, font, fontSize));
            row.Add(new TextItem(sQualTopR + " / " + sQualBottomR, 250, 0, boldFont, fontSize));
            AddItem(row);

            AddItem(new HorizontalLineItem());

        }
        private void AddLoanStatusHeader() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Loan Status: ", 40, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Date", 138, 0, font, fontSize));
            row.Add(new TextItem("Comments", 210, 0, font, fontSize));
            AddItem(row);
        }

        private void AddLoanStatusItem(string status, string date, string comments) 
        {
            if (date.TrimWhitespaceAndBOM() == "" && comments.TrimWhitespaceAndBOM() == "") return;
            FontName font = FontName.Helvetica;
            short fontSize = 8;


            RowItem row = new RowItem();
            row.Add(new TextItem(status, 40, 0, 85, font, fontSize));
            row.Add(new TextItem(date, 138, 0, font, fontSize));
            row.Add(new TextItem(comments, 210, 570, font, fontSize));
            AddItem(row);

        }

        private void AddBrokerInformation(CAgentFields agent) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Broker", 40, 0, font, fontSize));
            row.Add(new TextItem(" - " + agent.CompanyName, 100, 0, 200, font, fontSize));
            row.Add(new TextItem("License Number - " + agent.LicenseNumOfCompany, 280, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Contact Agent", 40, 0, font, fontSize));
            row.Add(new TextItem(" - " + agent.AgentName, 100, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Address", 40, 0, font, fontSize));
            row.Add(new TextItem(" - " + agent.StreetAddr, 100, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem(agent.CityStateZip, 100, 0, font, fontSize));
            AddItem(row);

            string str = "";
            if (agent.PhoneOfCompany.TrimWhitespaceAndBOM() != "") 
            {
                str = "Phone: " + agent.PhoneOfCompany + "     ";
            }

            if (agent.FaxOfCompany.TrimWhitespaceAndBOM() != "") 
            {
                str += "Fax: " + agent.FaxOfCompany;
            }

            if (str != "") 
            {
                row = new RowItem();
                row.Add(new TextItem(str, 100, 0, font, fontSize));
                AddItem(row);
            }

            row = new RowItem();
            row.Add(new TextItem("Comments ", 40, 0, font, fontSize));
            row.Add(new TextItem(" - " + agent.Notes, 100, 0, 500, font, fontSize));
            AddItem(row);

            AddItem(new HorizontalLineItem());

        }
        private void AddLenderInformation(CAgentFields agent, string sLenderCaseNum, string sNoteIR, string sRLckdD, string sRLckdNumOfDays,
            string sRLckdExpiredD, string sProfitGrossBor, string sProfitGrossBorPnt, string sProfitGrossBorMb, string sProfitRebate,
            string sProfitRebatePnt, string sProfitRebateMb, string sCommissionTotal, string sGrossProfit, string sNetProfit) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Lender", 40, 0, font, fontSize));
            row.Add(new TextItem(" - " + agent.CompanyName, 100, 0, 200, font, fontSize));
            row.Add(new TextItem("Case # - " + sLenderCaseNum, 280, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Contact Agent", 40, 0, font, fontSize));
            row.Add(new TextItem(" - " + agent.AgentName, 100, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Address", 40, 0, font, fontSize));
            row.Add(new TextItem(" - " + agent.StreetAddr, 100, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem(agent.CityStateZip, 100, 0, font, fontSize));
            AddItem(row);

            string str = "";
            if (agent.PhoneOfCompany.TrimWhitespaceAndBOM() != "") 
            {
                str = "Phone: " + agent.PhoneOfCompany + "     ";
            }

            if (agent.FaxOfCompany.TrimWhitespaceAndBOM() != "") 
            {
                str += "Fax: " + agent.FaxOfCompany;
            }

            if (str != "") 
            {
                row = new RowItem();
                row.Add(new TextItem(str, 100, 0, font, fontSize));
                AddItem(row);
            }


            row = new RowItem();
            row.Add(new TextItem("Rate - ", 40, 0, font, fontSize));
            row.Add(new TextItem(sNoteIR, 90, 0, font, fontSize));
            row.Add(new TextItem("Lock Date", 140, 0, font, fontSize));
            row.Add(new TextItem(sRLckdD, 190, 0, font, fontSize));
            row.Add(new TextItem("Lock Days", 230, 0, font, fontSize));
            row.Add(new TextItem(sRLckdNumOfDays, 270, 0, font, fontSize));
            row.Add(new TextItem("Expired", 310, 0, font, fontSize));
            row.Add(new TextItem(sRLckdExpiredD, 360, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem(" ", 40, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Borrower Points - ", 40, 0, font, fontSize));
            row.Add(new TextItem(sProfitGrossBorPnt + "%", 100, 0, 70, font, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem(" + ", 180, 0, font, fontSize));
            row.Add(new TextItem("$" + sProfitGrossBorMb, 210, 0, 70, font, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem(" = ", 290, 0, 0, font, fontSize));
            row.Add(new TextItem("$" + sProfitGrossBor, 320, 0, 70, font, fontSize, HorizontalAlign.Right));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Rebate Fee - ", 40, 0, font, fontSize));
            row.Add(new TextItem(sProfitRebatePnt + "%", 100, 0, 70, font, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem(" + ", 180, 0, font, fontSize));
            row.Add(new TextItem("$" + sProfitRebateMb, 210, 0, 70, font, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem(" = ", 290, 0, 0, font, fontSize));
            row.Add(new TextItem("$" + sProfitRebate, 320, 0, 70, font, fontSize, HorizontalAlign.Right));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Gross Profit - ", 40, 0, font, fontSize));
            row.Add(new TextItem(" = ", 290, 0, 0, font, fontSize));
            row.Add(new TextItem("$" + sGrossProfit, 320, 0, 70, font, fontSize, HorizontalAlign.Right));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Rep Commission - ", 40, 0, font, fontSize));
            row.Add(new TextItem(" = ", 290, 0, 0, font, fontSize));
            row.Add(new TextItem("$" + sCommissionTotal, 320, 0, 70, font, fontSize, HorizontalAlign.Right));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Net Profit - ", 40, 0, font, fontSize));
            row.Add(new TextItem(" = ", 290, 0, 0, font, fontSize));
            row.Add(new TextItem("$" + sNetProfit, 320, 0, 70, font, fontSize, HorizontalAlign.Right));
            AddItem(row);


            row = new RowItem();
            row.Add(new TextItem("Comments ", 40, 0, font, fontSize));
            row.Add(new TextItem(" - " + agent.Notes, 100, 0, 500, font, fontSize));
            AddItem(row);

            AddItem(new HorizontalLineItem());

        }
        private void AddTrustAccountHeader() 
        {
            FontName boldFont = FontName.HelveticaBold;

            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Trust Account", 40, 0, boldFont, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Funds Paid out", 280, 0, boldFont, fontSize));
            row.Add(new TextItem("Funds Received", 430, 0, boldFont, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Description", 40, 0, boldFont, fontSize));
            row.Add(new TextItem("Date", 200, 0, boldFont, fontSize));
            row.Add(new TextItem("Chk #", 270, 0, 50, boldFont, fontSize));
            row.Add(new TextItem("Amount", 330, 0, 80, boldFont, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem("Chk #", 430, 0, 50, boldFont, fontSize));
            row.Add(new TextItem("Amount", 480, 0, 80, boldFont, fontSize, HorizontalAlign.Right));
            AddItem(row);
        }

        private void AddTrustAccountItem(string sTrustDesc, string sTrustD, string sTrustReceivableChkNum, string sTrustReceivableAmt, string sTrustPayableChkNum, string sTrustPayableAmt, string sTrustReceivableN) 
        {
            if (sTrustDesc.TrimWhitespaceAndBOM() == "" && sTrustD.TrimWhitespaceAndBOM() == "" && sTrustReceivableChkNum.TrimWhitespaceAndBOM() == "" &&
                sTrustReceivableAmt == "" && sTrustPayableChkNum.TrimWhitespaceAndBOM() == "" && sTrustPayableAmt == "" &&
                sTrustReceivableN.TrimWhitespaceAndBOM() == "")
                return; // Skip empty entry.
            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem(sTrustDesc, 40, 0, 150, font, fontSize));
            row.Add(new TextItem(sTrustD, 200, 0, font, fontSize));
            row.Add(new TextItem(sTrustReceivableChkNum, 270, 0, 50, font, fontSize));
            row.Add(new TextItem(sTrustReceivableAmt, 330, 0, 80, font, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem(sTrustPayableChkNum, 430, 0, 50, font, fontSize));
            row.Add(new TextItem(sTrustPayableAmt, 480, 0, 80, font, fontSize, HorizontalAlign.Right));
            AddItem(row);

            if (sTrustReceivableN.TrimWhitespaceAndBOM() != "") 
            {
                row = new RowItem();
                row.Add(new TextItem(sTrustReceivableN, 140, 0, 460, font, fontSize));
                AddItem(row);
            }

        }

        private void AddTrustAccountFooter(string sTrustReceivableTotal, string sTrustPayableTotal, string sTrustBalance) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Total", 40, 0, font, fontSize));
            row.Add(new TextItem(sTrustReceivableTotal, 330, 0, 80, font, fontSize, HorizontalAlign.Right));
            row.Add(new TextItem(sTrustPayableTotal, 480, 0, 80, font, fontSize, HorizontalAlign.Right));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Balance", 40, 0, font, fontSize));
            row.Add(new TextItem(sTrustBalance, 480, 0, 80, font, fontSize, HorizontalAlign.Right));
            AddItem(row);
            AddItem(new HorizontalLineItem());

        }

        private void AddBasicDocumentsHeader() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Basic Documents:", 40, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Ordered", 150, 0, font, fontSize));
            row.Add(new TextItem("Received", 200, 0, font, fontSize));
            row.Add(new TextItem("Comments", 250, 0, 190, font, fontSize));
            row.Add(new TextItem("Days out", 450, 0, font, fontSize));
            row.Add(new TextItem("Days in", 500, 0, font, fontSize));
            AddItem(row);
        }

        private void AddBasicDocumentItem(string description, string ordered, string received, string comments) 
        {
            if (ordered.TrimWhitespaceAndBOM() == "" && received.TrimWhitespaceAndBOM() == "" && comments.TrimWhitespaceAndBOM() == "") 
                return; // Skip empty entry.

            // Calculate daysOut & daysIn.
            string daysOut = "";
            string daysIn = "";

            if (ordered.TrimWhitespaceAndBOM() != "")
            {
                DateTime dt;

                if (DateTime.TryParse(ordered, out dt))
                {
                    daysOut = DateTime.Now.Subtract(dt).Days.ToString();
                }
            } 

            if (received.TrimWhitespaceAndBOM() != "") 
            {
                DateTime dt;

                if (DateTime.TryParse(received, out dt))
                {
                    daysIn = DateTime.Now.Subtract(dt).Days.ToString();
                }
            }

            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem(description, 40, 0, font, fontSize));
            row.Add(new TextItem(ordered, 150, 0, font, fontSize));
            row.Add(new TextItem(received, 200, 0, font, fontSize));
            row.Add(new TextItem(comments, 250, 0, 190, font, fontSize));
            row.Add(new TextItem(daysOut, 450, 0, font, fontSize));
            row.Add(new TextItem(daysIn, 500, 0, font, fontSize));
            AddItem(row);

        }
        private void AddGenericDocumentHeader(string header, string desc) 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem(header, 40, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem(desc, 40, 0, font, fontSize));
            row.Add(new TextItem("Ordered", 300, 0, font, fontSize));
            row.Add(new TextItem("Re-order", 350, 0, font, fontSize));
            row.Add(new TextItem("Received", 400, 0, font, fontSize));
            row.Add(new TextItem("Days out", 450, 0, font, fontSize));
            row.Add(new TextItem("Days in", 500, 0, font, fontSize));
            AddItem(row);

        }
        private void AddGenericDocumentItem(string description, string ordered, string reordered, string received) 
        {
            if (ordered.TrimWhitespaceAndBOM() == "" && received.TrimWhitespaceAndBOM() == "" && reordered.TrimWhitespaceAndBOM() == "")
                return; // Skip empty entry.

            // Calculate daysOut & daysIn.
            string daysOut = "";
            string daysIn = "";

            if (ordered.TrimWhitespaceAndBOM() != "") 
            {
                DateTime dt;

                if (DateTime.TryParse(ordered, out dt))
                {
                    daysOut = DateTime.Now.Subtract(dt).Days.ToString();
                }
            }

            if (received.TrimWhitespaceAndBOM() != "") 
            {
                DateTime dt;

                if (DateTime.TryParse(received, out dt))
                {
                    daysIn = DateTime.Now.Subtract(dt).Days.ToString();
                }
            }

            FontName font = FontName.Helvetica;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem(description, 40, 0, 250, font, fontSize));
            row.Add(new TextItem(ordered, 300, 0, font, fontSize));
            row.Add(new TextItem(reordered, 350, 0, font, fontSize));
            row.Add(new TextItem(received, 400, 0, font, fontSize));
            row.Add(new TextItem(daysOut, 450, 0, font, fontSize));
            row.Add(new TextItem(daysIn, 500, 0, font, fontSize));
            AddItem(row);
        }
	}
}
