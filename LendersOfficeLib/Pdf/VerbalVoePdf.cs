﻿// <copyright file="VerbalVOEPdf.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   3/16/2016 4:15:52 PM
// </summary>
namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Generates a IFW Hud based form.
    /// </summary>
    public class VerbalVoePdf : AbstractXsltPdf
    {
        /// <summary>
        /// Gets the page size that will be used in the form.
        /// </summary>
        /// <value>The page size.</value>
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize
        {
            get
            {
                return LendersOffice.PdfGenerator.PDFPageSize.Letter;
            }
        }

        /// <summary>
        /// Gets the description that will be shown in the print list.
        /// </summary>
        /// <value>The forms print list description.</value>
        public override string Description
        {
            get { return "Verbal VOE"; }
        }

        /// <summary>
        /// Gets the id of the record that will be viewed in the pdf.
        /// </summary>
        /// <value>The id of the record.</value>
        public Guid RecordId
        {
            get
            {
                var recordId = this.Arguments["recordid"];

                if (!string.IsNullOrEmpty(recordId))
                {
                    return new Guid(recordId);
                }

                return Guid.Empty;
            }
        }

        /// <summary>
        /// Gets the link that will be used to navigate to the editor.
        /// </summary>
        /// <value>The link to navigate to the editor.</value>
        public override string EditUrl
        {
            get
            {
                return Tools.VRoot + "/newlos/Verifications/VerbalVoeEditor";
            }
        }

        /// <summary>
        /// Gets the location of the XLST that will be used to generate the form.
        /// </summary>
        /// <value>The namespace of the embedded XLST.</value>
        protected override string XsltEmbeddedResourceLocation
        {
            get
            {
                return "LendersOffice.Pdf.VerbalVoe.xslt";
            }
        }

        /// <summary>
        /// This method creates a list of arguments for each Verbal VOE record we have.  This will be used when generating the print list.
        /// </summary>
        /// <param name="dataApp">The application to retrieve the items from.</param>
        /// <returns>A list of items to display in the print list.</returns>
        public override List<NameValueCollection> GenerateRecordArguments(CAppData dataApp)
        {
            List<NameValueCollection> recordArguments = new List<NameValueCollection>();
            foreach (var record in dataApp.aVerbalVerificationsOfEmployment)
            {
                NameValueCollection args = new NameValueCollection();
                args.Add("recordid", record.Id.ToString());
                args.Add("RecordDescription", record.BorrowerName + ", " + record.EmployerName);
                recordArguments.Add(args);
            }

            return recordArguments;
        }

        /// <summary>
        /// Writes the XML data needed to generate the HTML form.
        /// </summary>
        /// <param name="writer">The xml writer that will be used.</param>
        /// <param name="dataLoan">The loan data that the data will come out of.</param>
        /// <param name="dataApp">The app data that will be used.</param>
        protected override void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp)
        {
            var records = dataApp.aVerbalVerificationsOfEmployment;
            var record = records.First(p => p.Id.Equals(this.RecordId));

            writer.WriteStartElement("VerbalVoe");

            writer.WriteElementString("BorrowerName", record.BorrowerName);
            writer.WriteElementString("sLId", dataLoan.sLNm);

            writer.WriteElementString("EmployerName", record.EmployerName);
            writer.WriteElementString("EmployerAddress", record.EmployerAddress);

            writer.WriteElementString("EmployerPhone", record.EmployerPhone);
            writer.WriteElementString("ThirdPartySource", record.ThirdPartySource);

            writer.WriteElementString("DateOfHire", record.DateOfHire_rep);
            writer.WriteElementString("DateOfTermination", record.DateOfTermination_rep);
            writer.WriteElementString("BorrowerTitle", record.BorrowerTitle);
            writer.WriteElementString("EmploymentStatus", record.EmploymentStatus);

            writer.WriteElementString("EmployerContactName", record.EmployerContactName);
            writer.WriteElementString("EmployerContactTitle", record.EmployerContactTitle);

            writer.WriteElementString("PreparedBy", record.PreparedBy);
            writer.WriteElementString("PreparedByTitle", record.PreparedByTitle);

            writer.WriteElementString("DateOfCall", record.DateOfCall_rep);

            writer.WriteEndElement();
        }
    }
}
