﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public abstract class AbstractCOLockInDisclosurePDF : AbstractLetterPDF
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("aBFirstNm", dataApp.aBFirstNm);
            AddFormFieldData("aBLastNm", dataApp.aBLastNm);
            AddFormFieldData("aCFirstNm", dataApp.aCFirstNm);
            AddFormFieldData("aCLastNm", dataApp.aCLastNm);

            AddFormFieldData("sSpAddr", dataLoan.sSpAddr);
            AddFormFieldData("sSpCity", dataLoan.sSpCity);
            AddFormFieldData("sSpState", dataLoan.sSpState);
            AddFormFieldData("sSpZip", dataLoan.sSpZip);
            AddFormFieldData("sRLckdD", dataLoan.sRLckdD_rep);
            AddFormFieldData("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);

            AddFormFieldData("sCOLockInDisclosureTermFixedInYrs", dataLoan.sCOLockInDisclosureTermFixedInYrs);
            AddFormFieldData("sCOLockInDisclosureTypeOfInterest", dataLoan.sCOLockInDisclosureTypeOfInterest);
            IPreparerFields prep = dataLoan.GetPreparerOfForm( E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject );
            AddFormFieldData("App1003InterviewerLoanOriginatorIdentifier", prep.LoanOriginatorIdentifier);
            AddFormFieldData("App1003InterviewerPreparerName", prep.PreparerName);

            switch (dataLoan.sRateLockStatusT)
            {
                case E_sRateLockStatusT.Locked:
                    AddFormFieldData("sRateLockStatusT_Locked", "X");
                    break;
                case E_sRateLockStatusT.NotLocked:
                case E_sRateLockStatusT.LockRequested:
                case E_sRateLockStatusT.LockSuspended:
                    AddFormFieldData("sRateLockStatusT_NotLocked", "X");
                    break;
                default:
                    throw new UnhandledEnumException(dataLoan.sRateLockStatusT);
            }
        }
    }
    public class CCOLockInDisclosure_1PDF : AbstractCOLockInDisclosurePDF
    {
        public override string PdfFile
        {
            get { return "COLockInDisclosure_1.pdf"; }
        }
        public override string Description
        {
            get
            {
                return "Page 1";
            }
        }

    }
    public class CCOLockInDisclosure_2PDF : AbstractCOLockInDisclosurePDF
    {
        public override string PdfFile
        {
            get 
            {
                return "COLockInDisclosure_2.pdf";
            }
        }
        public override string Description
        {
            get
            {
                return "Page 2";
            }
        }
    }
    public class CCOLockInDisclosurePDF : AbstractBatchPDF
    {
        public override string Description
        {
            get
            {
                return "CO Lock-In Disclosure";
            }
        }
        protected override AbstractPDFFile[] ChildPages
        {
            get 
            {
                return new AbstractPDFFile[] {
                    new CCOLockInDisclosure_1PDF(),
                    new CCOLockInDisclosure_2PDF()
                };
            }
        }
    }
}
