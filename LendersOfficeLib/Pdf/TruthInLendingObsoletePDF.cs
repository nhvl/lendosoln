﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Common;

    public class CTruthInLendingObsoletePDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "TruthInLending.pdf"; }
        }

        public virtual string EstimateSymbol
        {
            get { return "* "; }
        }
        public override string Description
        {
            get { return "Truth In Lending"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Forms/TruthInLending.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            string borrowerName = dataApp.aBNm;
            borrowerName += Environment.NewLine + dataApp.aCNm;
            string loanNumber = dataLoan.sLNm;

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Til, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("GfeTil", Tools.FormatAddress(gfeTil.CompanyName, gfeTil.StreetAddr, gfeTil.City, gfeTil.State, gfeTil.Zip, gfeTil.PhoneOfCompany));

            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);
            AddFormFieldData("LoanNumber", loanNumber);
            AddFormFieldData("ApplicantName", borrowerName);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            string asterisk = dataLoan.sAsteriskEstimate ? EstimateSymbol : "";

            AddFormFieldData("sSpFullAddr", dataLoan.sSpFullAddr);
            AddFormFieldData("sApr", asterisk + dataLoan.sApr_rep, false);
            AddFormFieldData("sNoteIR", asterisk + dataLoan.sNoteIR_rep, false);
            AddFormFieldData("sFinCharge", asterisk + dataLoan.sFinCharge_rep, false);
            AddFormFieldData("sFinancedAmt", asterisk + dataLoan.sFinancedAmt_rep, false);
            AddFormFieldData("sSchedPmtTot", asterisk + dataLoan.sSchedPmtTot_rep, false);
            AddFormFieldData("sAprIncludesReqDeposit", dataLoan.sAprIncludesReqDeposit);
            AddFormFieldData("sHasDemandFeature", dataLoan.sHasDemandFeature);
            AddFormFieldData("sHasVarRFeature", dataLoan.sHasVarRFeature);
            AddFormFieldData("sPrepmtPenaltyT", dataLoan.sPrepmtPenaltyT);
            AddFormFieldData("sPrepmtRefundT", dataLoan.sPrepmtRefundT);
            AddFormFieldData("sAssumeLT", dataLoan.sAssumeLT);
            AddFormFieldData("sAsteriskEstimate", dataLoan.sAsteriskEstimate);
            AddFormFieldData("sOnlyLatePmtEstimate", dataLoan.sOnlyLatePmtEstimate);
            AddFormFieldData("sSecurityPurch", dataLoan.sSecurityPurch);
            AddFormFieldData("sSecurityCurrentOwn", dataLoan.sSecurityCurrentOwn);
            AddFormFieldData("sFilingF", dataLoan.sFilingF);
            AddFormFieldData("sLateDays", dataLoan.sLateDays);
            AddFormFieldData("sLateChargePc", dataLoan.sLateChargePc);
            AddFormFieldData("sLateChargeBaseDesc", dataLoan.sLateChargeBaseDesc);
            AddFormFieldData("sVarRNotes", dataLoan.sVarRNotes);

            try
            {
                for (int i = 1; i <= dataLoan.sAmortTable.nRows; i++)
                {
                    AddFormFieldData("sSchedPmtNum" + i, dataLoan.sSchedIR_Rep(i - 1, PmtInfoType.PmtNumber));
                    AddFormFieldData("sSchedDueD" + i, dataLoan.sSchedIR_Rep(i - 1, PmtInfoType.DueDate));
                    AddFormFieldData("sSchedPmt" + i, dataLoan.sSchedIR_Rep(i - 1, PmtInfoType.PmtAmount));
                }
            }
            catch
            {
                // Do nothing if sAmortTable is not valid. Generally, this is a bad practice to rely
                // on exception being throw to determine if value is valid or not. However, currently
                // there is no way I could tell if this value is valid. dd 9/30/02
            }


            AddFormFieldData("sInsReqDesc", dataLoan.sInsReqDesc);
            AddFormFieldData("sReqCreditLifeIns", dataLoan.sReqCreditLifeIns);
            AddFormFieldData("sReqCreditDisabilityIns", dataLoan.sReqCreditDisabilityIns);
            AddFormFieldData("sReqPropIns", dataLoan.sReqPropIns);
            AddFormFieldData("sReqFloodIns", dataLoan.sReqFloodIns);
            AddFormFieldData("sIfPurchPropInsFrCreditor", dataLoan.sIfPurchPropInsFrCreditor);
            AddFormFieldData("sIfPurchInsFrCreditor", dataLoan.sIfPurchInsFrCreditor);
            AddFormFieldData("sIfPurchFloodInsFrCreditor", dataLoan.sIfPurchFloodInsFrCreditor);
            AddFormFieldData("sInsFrCreditorAmt", dataLoan.sInsFrCreditorAmt_rep);

        }

    }
}