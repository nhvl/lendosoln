﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using DataAccess;
using LendersOffice.Common;
using System.Linq;
using LendersOffice.Common.SerializationTypes;

namespace LendersOffice.Pdf
{
    public class CGoodFaithEstimate2010Archive_1PDF : AbstractGoodFaithEstimate2010ArchivePDF
    {
        public override string PdfFile
        {
            get { return "GoodFaithEstimate2010_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        public CGoodFaithEstimate2010Archive_1PDF(NameValueCollection arguments)
        {
            if (arguments != null)
            {
                Arguments = arguments;
            }
        }
        public CGoodFaithEstimate2010Archive_1PDF()
        {
        }
    }

    public class CGoodFaithEstimate2010Archive_2PDF : AbstractGoodFaithEstimate2010ArchivePDF
    {
        public override string PdfFile
        {
            get { return "GoodFaithEstimate2010_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        public CGoodFaithEstimate2010Archive_2PDF(NameValueCollection arguments)
        {
            if (arguments != null)
            {
                Arguments = arguments;
            }
        }
        public CGoodFaithEstimate2010Archive_2PDF()
        {
        }
    }

    public class CGoodFaithEstimate2010Archive_3PDF : AbstractGoodFaithEstimate2010ArchivePDF
    {
        public override string PdfFile
        {
            get { return "GoodFaithEstimate2010_3.pdf"; }
        }
        public override string Description
        {
            get { return "Page 3"; }
        }
        public CGoodFaithEstimate2010Archive_3PDF(NameValueCollection arguments)
        {
            if (arguments != null)
            {
                Arguments = arguments;
            }
        }
        public CGoodFaithEstimate2010Archive_3PDF()
        {
        }
    }

    public class CGoodFaithEstimate2010ArchivePDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CGoodFaithEstimate2010Archive_1PDF(Arguments)
                                                ,new CGoodFaithEstimate2010Archive_2PDF(Arguments)
                                                ,new CGoodFaithEstimate2010Archive_3PDF(Arguments)
                                             };
            }
        }

        public override string Description
        {
            get { return "Last Disclosed Good Faith Estimate"; }
        }
        
        public override bool IsVisible
        {
            get
            {
                return !DataLoan.sIsConstructionLoanWithConstructionRefiData && DataLoan.BrokerDB.IsGFEandCoCVersioningEnabled;
            }
        }
    }
    public abstract class AbstractGoodFaithEstimate2010PDFFromGFEArchive : AbstractLetterPDF
    {
        protected virtual void ApplyData(IGFEArchive gfe)
        {
            //CPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("GfeTilCompanyName", gfe.gfeTil_CompanyName);
            AddFormFieldData("GfeTillStreetAddr_Multiline", Tools.FormatAddress(gfe.gfeTil_StreetAddr, gfe.gfeTil_City, gfe.gfeTil_State, gfe.gfeTil_Zip));
            AddFormFieldData("GfeTilPhoneOfCompany", gfe.gfeTil_PhoneOfCompany);
            AddFormFieldData("GfeTilEmailAddr", gfe.gfeTil_EmailAddr);
            AddFormFieldData("aBNm_aCNm", gfe.dataApp_aBNm_aCNm);
            AddFormFieldData("sSpAddr_Multiline", Tools.FormatAddress(gfe.sSpAddr, gfe.sSpCity, gfe.sSpState, gfe.sSpZip));
            AddFormFieldData("GfeTilPrepareDate", gfe.gfeTil_PrepareDate);

            AddFormFieldData("sGfeNoteIRAvailTillD", gfe.sGfeNoteIRAvailTillD_DateTime);
            AddFormFieldData("sGfeEstScAvailTillD", gfe.sGfeEstScAvailTillD_DateTime);
            AddFormFieldData("sGfeRateLockPeriod", gfe.sGfeRateLockPeriod_rep);
            AddFormFieldData("sGfeLockPeriodBeforeSettlement", gfe.sGfeLockPeriodBeforeSettlement_rep);

            AddFormFieldData("sFinalLAmt", gfe.sFinalLAmt_rep);
            AddFormFieldData("sDueInYr", gfe.sDueInYr_rep);
            AddFormFieldData("sNoteIR", gfe.sNoteIR_rep);
            AddFormFieldData("sGfeProThisMPmtAndMIns", gfe.sGfeProThisMPmtAndMIns_rep);
            AddFormFieldData("sGfeCanRateIncrease", gfe.sGfeCanRateIncrease);
            AddFormFieldData("sRLifeCapR", gfe.sRLifeCapR_rep);
            AddFormFieldData("sGfeFirstInterestChangeIn", gfe.sGfeFirstInterestChangeIn);
            AddFormFieldData("sGfeFirstPaymentChangeIn", gfe.sGfeFirstPaymentChangeIn);

            AddFormFieldData("sGfeCanLoanBalanceIncrease", gfe.sGfeCanLoanBalanceIncrease);
            AddFormFieldData("sGfeMaxLoanBalance", gfe.sGfeMaxLoanBalance_rep);
            AddFormFieldData("sGfeFirstAdjProThisMPmtAndMIns", gfe.sGfeFirstAdjProThisMPmtAndMIns_rep);
            AddFormFieldData("sGfeMaxProThisMPmtAndMIns", gfe.sGfeMaxProThisMPmtAndMIns_rep);
            AddFormFieldData("sGfeHavePpmtPenalty", gfe.sGfeHavePpmtPenalty);
            AddFormFieldData("sGfeMaxPpmtPenaltyAmt", gfe.sGfeMaxPpmtPenaltyAmt_rep);
            AddFormFieldData("sGfeIsBalloon", gfe.sGfeIsBalloon);
            AddFormFieldData("sGfeBalloonPmt", gfe.sGfeBalloonPmt_rep);
            AddFormFieldData("sGfeBalloonDueInYrs", gfe.sGfeBalloonDueInYrs);
            AddFormFieldData("sMldsHasImpound", gfe.sMldsHasImpound);

            //FormatTarget oldTarget = dataLoan.GetFormatTarget();       
            //dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
            // 1/27/2010 dd - Always display dollar amount $0.00 instead of empty string.
            #region Always Printout value.
            AddFormFieldDataWithDefaultZeros("sGfeTotalOtherSettlementServiceFee", gfe.sGfeTotalOtherSettlementServiceFee_rep);
            AddFormFieldDataWithDefaultZeros("sGfeTotalEstimateSettlementCharge", gfe.sGfeTotalEstimateSettlementCharge_rep);

            AddFormFieldDataWithDefaultZeros("sGfeAdjOriginationCharge", gfe.sGfeAdjOriginationCharge_rep);

            AddFormFieldDataWithDefaultZeros("sLOrigF", gfe.sLOrigF_rep);
            AddFormFieldDataWithDefaultZeros("sGfeOriginationF", gfe.sGfeOriginationF_rep);
            AddFormFieldData("sGfeCreditChargeT", gfe.sGfeCreditChargeT);
            AddFormFieldDataWithDefaultZeros("sGfeNoCreditChargeNoteIR", gfe.sGfeNoCreditChargeNoteIR);
            AddFormFieldDataWithDefaultZeros("sGfeCreditNoteIR", gfe.sGfeCreditNoteIR);
            AddFormFieldDataWithDefaultZeros("sGfeCreditLDiscnt", gfe.sGfeCreditLDiscnt);
            AddFormFieldDataWithDefaultZeros("sGfeChargeNoteIR", gfe.sGfeChargeNoteIR);
            AddFormFieldDataWithDefaultZeros("sGfeChargeLDiscnt", gfe.sGfeChargeLDiscnt);
            AddFormFieldDataWithDefaultZeros("sGfeAdjOriginationCharge", gfe.sGfeAdjOriginationCharge_rep);
            AddFormFieldDataWithDefaultZeros("sLDiscnt", gfe.sLDiscnt_rep);
            AddFormFieldDataWithDefaultZeros("sGfeRequiredServicesTotalFee", gfe.sGfeRequiredServicesTotalFee_rep);
            AddFormFieldDataWithDefaultZeros("sGfeLenderTitleTotalFee", gfe.sGfeLenderTitleTotalFee_rep);
            AddFormFieldDataWithDefaultZeros("sGfeOwnerTitleTotalFee", gfe.sGfeOwnerTitleTotalFee_rep);
            AddFormFieldDataWithDefaultZeros("sGfeServicesYouShopTotalFee", gfe.sGfeServicesYouShopTotalFee_rep);
            AddFormFieldDataWithDefaultZeros("sGfeGovtRecTotalFee", gfe.sGfeGovtRecTotalFee_rep);
            AddFormFieldDataWithDefaultZeros("sGfeTransferTaxTotalFee", gfe.sGfeTransferTaxTotalFee_rep);
            AddFormFieldDataWithDefaultZeros("sGfeInitialImpoundDeposit", gfe.sGfeInitialImpoundDeposit_rep);
            AddFormFieldDataWithDefaultZeros("sGfeDailyInterestTotalFee", gfe.sGfeDailyInterestTotalFee_rep);
            AddFormFieldDataWithDefaultZeros("sGfeHomeOwnerInsuranceTotalFee", gfe.sGfeHomeOwnerInsuranceTotalFee_rep);

            #endregion
            // finish display 0.00 for empty string.
            //dataLoan.SetFormatTarget(oldTarget);
            AddFormFieldData("sGfeHasImpoundDeposit", gfe.sGfeHasImpoundDeposit);
            AddFormFieldData("sIPerDay", gfe.sIPerDay_rep);
            AddFormFieldData("sIPiaDy", gfe.sIPiaDy_rep);
            AddFormFieldData("sEstCloseD", gfe.sEstCloseD_rep);
            AddFormFieldData("sConsummationD", gfe.sConsummationD_rep); // OPM 51229, 52774

            AddFormFieldData("sGfeTradeOffLowerCCLoanAmt", gfe.sGfeTradeOffLowerCCLoanAmt_rep);
            AddFormFieldData("sGfeTradeOffLowerCCNoteIR", gfe.sGfeTradeOffLowerCCNoteIR_rep);
            AddFormFieldData("sGfeTradeOffLowerCCMPmtAndMIns", gfe.sGfeTradeOffLowerCCMPmtAndMIns_rep);
            AddFormFieldData("sGfeTradeOffLowerCCMPmtAndMInsDiff", gfe.sGfeTradeOffLowerCCMPmtAndMInsDiff_rep);
            AddFormFieldData("sGfeTradeOffLowerCCClosingCostDiff", gfe.sGfeTradeOffLowerCCClosingCostDiff_rep);
            AddFormFieldData("sGfeTradeOffLowerCCClosingCost", gfe.sGfeTradeOffLowerCCClosingCost_rep);
            AddFormFieldData("sGfeTradeOffLowerRateLoanAmt", gfe.sGfeTradeOffLowerRateLoanAmt_rep);
            AddFormFieldData("sGfeTradeOffLowerRateNoteIR", gfe.sGfeTradeOffLowerRateNoteIR_rep);
            AddFormFieldData("sGfeTradeOffLowerRateMPmtAndMIns", gfe.sGfeTradeOffLowerRateMPmtAndMIns_rep);
            AddFormFieldData("sGfeTradeOffLowerRateMPmtAndMInsDiff", gfe.sGfeTradeOffLowerRateMPmtAndMInsDiff_rep);
            AddFormFieldData("sGfeTradeOffLowerRateClosingCostDiff", gfe.sGfeTradeOffLowerRateClosingCostDiff_rep);
            AddFormFieldData("sGfeTradeOffLowerRateClosingCost", gfe.sGfeTradeOffLowerRateClosingCost_rep);

            AddFormFieldData("sTerm", gfe.sTerm_rep);
            AddFormFieldData("sGfeRateLockPeriod", gfe.sGfeRateLockPeriod_rep);
            AddFormFieldData("sGfeCanRateIncrease_rep", gfe.sGfeCanRateIncrease_rep);
            AddFormFieldData("sGfeCanLoanBalanceIncrease_rep", gfe.sGfeCanLoanBalanceIncrease_rep);
            AddFormFieldData("sGfeHavePpmtPenalty_rep", gfe.sGfeHavePpmtPenalty_rep);
            AddFormFieldData("sGfeIsBalloon_rep", gfe.sGfeIsBalloon_rep);

            AddFormFieldData("sGfeShoppingCartLoan1OriginatorName", gfe.sGfeShoppingCartLoan1OriginatorName);
            AddFormFieldData("sGfeShoppingCartLoan1LoanAmt", gfe.sGfeShoppingCartLoan1LoanAmt_rep);
            AddFormFieldData("sGfeShoppingCartLoan1LoanTerm", gfe.sGfeShoppingCartLoan1LoanTerm_rep);
            AddFormFieldData("sGfeShoppingCartLoan1NoteIR", gfe.sGfeShoppingCartLoan1NoteIR_rep);
            AddFormFieldData("sGfeShoppingCartLoan1InitialPmt", gfe.sGfeShoppingCartLoan1InitialPmt_rep);
            AddFormFieldData("sGfeShoppingCartLoan1RateLockPeriod", gfe.sGfeShoppingCartLoan1RateLockPeriod_rep);
            AddFormFieldData("sGfeShoppingCartLoan1CanRateIncreaseTri_rep", gfe.sGfeShoppingCartLoan1CanRateIncreaseTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri_rep", gfe.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri_rep", gfe.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan1HavePpmtPenaltyTri_rep", gfe.sGfeShoppingCartLoan1HavePpmtPenaltyTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan1IsBalloonTri_rep", gfe.sGfeShoppingCartLoan1IsBalloonTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan1TotalClosingCost", gfe.sGfeShoppingCartLoan1TotalClosingCost_rep);

            AddFormFieldData("sGfeShoppingCartLoan2OriginatorName", gfe.sGfeShoppingCartLoan2OriginatorName);
            AddFormFieldData("sGfeShoppingCartLoan2LoanAmt", gfe.sGfeShoppingCartLoan2LoanAmt_rep);
            AddFormFieldData("sGfeShoppingCartLoan2LoanTerm", gfe.sGfeShoppingCartLoan2LoanTerm_rep);
            AddFormFieldData("sGfeShoppingCartLoan2NoteIR", gfe.sGfeShoppingCartLoan2NoteIR_rep);
            AddFormFieldData("sGfeShoppingCartLoan2InitialPmt", gfe.sGfeShoppingCartLoan2InitialPmt_rep);
            AddFormFieldData("sGfeShoppingCartLoan2RateLockPeriod", gfe.sGfeShoppingCartLoan2RateLockPeriod_rep);
            AddFormFieldData("sGfeShoppingCartLoan2CanRateIncreaseTri_rep", gfe.sGfeShoppingCartLoan2CanRateIncreaseTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri_rep", gfe.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri_rep", gfe.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan2HavePpmtPenaltyTri_rep", gfe.sGfeShoppingCartLoan2HavePpmtPenaltyTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan2IsBalloonTri_rep", gfe.sGfeShoppingCartLoan2IsBalloonTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan2TotalClosingCost", gfe.sGfeShoppingCartLoan2TotalClosingCost_rep);

            AddFormFieldData("sGfeShoppingCartLoan3OriginatorName", gfe.sGfeShoppingCartLoan3OriginatorName);
            AddFormFieldData("sGfeShoppingCartLoan3LoanAmt", gfe.sGfeShoppingCartLoan3LoanAmt_rep);
            AddFormFieldData("sGfeShoppingCartLoan3LoanTerm", gfe.sGfeShoppingCartLoan3LoanTerm_rep);
            AddFormFieldData("sGfeShoppingCartLoan3NoteIR", gfe.sGfeShoppingCartLoan3NoteIR_rep);
            AddFormFieldData("sGfeShoppingCartLoan3InitialPmt", gfe.sGfeShoppingCartLoan3InitialPmt_rep);
            AddFormFieldData("sGfeShoppingCartLoan3RateLockPeriod", gfe.sGfeShoppingCartLoan3RateLockPeriod_rep);
            AddFormFieldData("sGfeShoppingCartLoan3CanRateIncreaseTri_rep", gfe.sGfeShoppingCartLoan3CanRateIncreaseTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri_rep", gfe.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri_rep", gfe.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan3HavePpmtPenaltyTri_rep", gfe.sGfeShoppingCartLoan3HavePpmtPenaltyTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan3IsBalloonTri_rep", gfe.sGfeShoppingCartLoan3IsBalloonTri_rep);
            AddFormFieldData("sGfeShoppingCartLoan3TotalClosingCost", gfe.sGfeShoppingCartLoan3TotalClosingCost_rep);

            AddFormFieldData("sGfeService1WeSelectName", gfe.sGfeService1WeSelectName);
            AddFormFieldData("sGfeService2WeSelectName", gfe.sGfeService2WeSelectName);
            AddFormFieldData("sGfeService3WeSelectName", gfe.sGfeService3WeSelectName);
            AddFormFieldData("sGfeService4WeSelectName", gfe.sGfeService4WeSelectName);
            AddFormFieldData("sGfeService5WeSelectName", gfe.sGfeService5WeSelectName);
            AddFormFieldData("sGfeService6WeSelectName", gfe.sGfeService6WeSelectName);
            AddFormFieldData("sGfeService7WeSelectName", gfe.sGfeService7WeSelectName);
            AddFormFieldData("sGfeService8WeSelectName", gfe.sGfeService8WeSelectName);
            AddFormFieldData("sGfeService9WeSelectName", gfe.sGfeService9WeSelectName);
            AddFormFieldData("sGfeService10WeSelectName", gfe.sGfeService10WeSelectName);
            AddFormFieldData("sGfeService11WeSelectName", gfe.sGfeService11WeSelectName);
            AddFormFieldData("sGfeService12WeSelectName", gfe.sGfeService12WeSelectName);
            AddFormFieldData("sGfeService13WeSelectName", gfe.sGfeService13WeSelectName);
            AddFormFieldData("sGfeService14WeSelectName", gfe.sGfeService14WeSelectName);
            AddFormFieldData("sGfeService15WeSelectName", gfe.sGfeService15WeSelectName);
            AddFormFieldData("sGfeService16WeSelectName", gfe.sGfeService16WeSelectName);
            AddFormFieldData("sGfeService17WeSelectName", gfe.sGfeService17WeSelectName);
            AddFormFieldData("sGfeService18WeSelectName", gfe.sGfeService18WeSelectName);

            AddFormFieldData("sGfeService1WeSelectFee", gfe.sGfeService1WeSelectFee);
            AddFormFieldData("sGfeService2WeSelectFee", gfe.sGfeService2WeSelectFee);
            AddFormFieldData("sGfeService3WeSelectFee", gfe.sGfeService3WeSelectFee);
            AddFormFieldData("sGfeService4WeSelectFee", gfe.sGfeService4WeSelectFee);
            AddFormFieldData("sGfeService5WeSelectFee", gfe.sGfeService5WeSelectFee);
            AddFormFieldData("sGfeService6WeSelectFee", gfe.sGfeService6WeSelectFee);
            AddFormFieldData("sGfeService7WeSelectFee", gfe.sGfeService7WeSelectFee);
            AddFormFieldData("sGfeService8WeSelectFee", gfe.sGfeService8WeSelectFee);
            AddFormFieldData("sGfeService9WeSelectFee", gfe.sGfeService9WeSelectFee);
            AddFormFieldData("sGfeService10WeSelectFee", gfe.sGfeService10WeSelectFee);
            AddFormFieldData("sGfeService11WeSelectFee", gfe.sGfeService11WeSelectFee);
            AddFormFieldData("sGfeService12WeSelectFee", gfe.sGfeService12WeSelectFee);
            AddFormFieldData("sGfeService13WeSelectFee", gfe.sGfeService13WeSelectFee);
            AddFormFieldData("sGfeService14WeSelectFee", gfe.sGfeService14WeSelectFee);
            AddFormFieldData("sGfeService15WeSelectFee", gfe.sGfeService15WeSelectFee);
            AddFormFieldData("sGfeService16WeSelectFee", gfe.sGfeService16WeSelectFee);
            AddFormFieldData("sGfeService17WeSelectFee", gfe.sGfeService17WeSelectFee);
            AddFormFieldData("sGfeService18WeSelectFee", gfe.sGfeService18WeSelectFee);

            AddFormFieldData("sGfeService1YouCanShopName", gfe.sGfeService1YouCanShopName);
            AddFormFieldData("sGfeService2YouCanShopName", gfe.sGfeService2YouCanShopName);
            AddFormFieldData("sGfeService3YouCanShopName", gfe.sGfeService3YouCanShopName);
            AddFormFieldData("sGfeService4YouCanShopName", gfe.sGfeService4YouCanShopName);
            AddFormFieldData("sGfeService5YouCanShopName", gfe.sGfeService5YouCanShopName);
            AddFormFieldData("sGfeService6YouCanShopName", gfe.sGfeService6YouCanShopName);
            AddFormFieldData("sGfeService7YouCanShopName", gfe.sGfeService7YouCanShopName);
            AddFormFieldData("sGfeService8YouCanShopName", gfe.sGfeService8YouCanShopName);
            AddFormFieldData("sGfeService9YouCanShopName", gfe.sGfeService9YouCanShopName);
            AddFormFieldData("sGfeService10YouCanShopName", gfe.sGfeService10YouCanShopName);
            AddFormFieldData("sGfeService11YouCanShopName", gfe.sGfeService11YouCanShopName);
            AddFormFieldData("sGfeService12YouCanShopName", gfe.sGfeService12YouCanShopName);
            AddFormFieldData("sGfeService13YouCanShopName", gfe.sGfeService13YouCanShopName);
            AddFormFieldData("sGfeService14YouCanShopName", gfe.sGfeService14YouCanShopName);
            AddFormFieldData("sGfeService15YouCanShopName", gfe.sGfeService15YouCanShopName);
            AddFormFieldData("sGfeService16YouCanShopName", gfe.sGfeService16YouCanShopName);





            AddFormFieldData("sGfeService1YouCanShopFee", gfe.sGfeService1YouCanShopFee);
            AddFormFieldData("sGfeService2YouCanShopFee", gfe.sGfeService2YouCanShopFee);
            AddFormFieldData("sGfeService3YouCanShopFee", gfe.sGfeService3YouCanShopFee);
            AddFormFieldData("sGfeService4YouCanShopFee", gfe.sGfeService4YouCanShopFee);
            AddFormFieldData("sGfeService5YouCanShopFee", gfe.sGfeService5YouCanShopFee);
            AddFormFieldData("sGfeService6YouCanShopFee", gfe.sGfeService6YouCanShopFee);

            AddFormFieldData("sGfeService7YouCanShopFee", gfe.sGfeService7YouCanShopFee);
            AddFormFieldData("sGfeService8YouCanShopFee", gfe.sGfeService8YouCanShopFee);
            AddFormFieldData("sGfeService9YouCanShopFee", gfe.sGfeService9YouCanShopFee);
            AddFormFieldData("sGfeService10YouCanShopFee", gfe.sGfeService10YouCanShopFee);
            AddFormFieldData("sGfeService11YouCanShopFee", gfe.sGfeService11YouCanShopFee);
            AddFormFieldData("sGfeService12YouCanShopFee", gfe.sGfeService12YouCanShopFee);
            AddFormFieldData("sGfeService13YouCanShopFee", gfe.sGfeService13YouCanShopFee);
            AddFormFieldData("sGfeService14YouCanShopFee", gfe.sGfeService14YouCanShopFee);
            AddFormFieldData("sGfeService15YouCanShopFee", gfe.sGfeService15YouCanShopFee);
            AddFormFieldData("sGfeService16YouCanShopFee", gfe.sGfeService16YouCanShopFee);

            AddFormFieldData("sHazInsPiaPaidToPrint", gfe.sHazInsPiaPaidToPrint);

            AddFormFieldData("sGfeHomeOwnerInsurance1Name", gfe.sGfeHomeOwnerInsurance1Name);
            AddFormFieldData("sGfeHomeOwnerInsurance1Fee", gfe.sGfeHomeOwnerInsurance1Fee);
            AddFormFieldData("sGfeHomeOwnerInsurance2Name", gfe.sGfeHomeOwnerInsurance2Name);
            AddFormFieldData("sGfeHomeOwnerInsurance2Fee", gfe.sGfeHomeOwnerInsurance2Fee);
            AddFormFieldData("sGfeHomeOwnerInsurance3Name", gfe.sGfeHomeOwnerInsurance3Name);
            AddFormFieldData("sGfeHomeOwnerInsurance3Fee", gfe.sGfeHomeOwnerInsurance3Fee);

            AddFormFieldData("sGfeHasImpoundDepositTax", gfe.sGfeHasImpoundDepositTax);
            AddFormFieldData("sGfeHasImpoundDepositInsurance", gfe.sGfeHasImpoundDepositInsurance);
            AddFormFieldData("sGfeHasImpoundDepositOther", gfe.sGfeHasImpoundDepositOther);
            AddFormFieldData("sGfeHasImpoundDepositDescription", gfe.sGfeHasImpoundDepositDescription);
        }

        protected void AddFormFieldDataWithDefaultZeros(string name, string value)
        {
            var newval = string.IsNullOrEmpty(value) ? "$0.00" : value;
            AddFormFieldData(name, newval);
        }
    }

    public abstract class AbstractGoodFaithEstimate2010ArchivePDF : AbstractGoodFaithEstimate2010PDFFromGFEArchive
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp2)
        {
            IGFEArchive gfe = null;
            if (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && GFEArchiveId != Guid.Empty)
            {
                var newGfe =
                dataLoan.sClosingCostArchive.FirstOrDefault(p => (p.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015)
                    && p.Id == GFEArchiveId);
                if (newGfe != null)
                {
                    dataLoan.ApplyClosingCostArchive(newGfe);
                    gfe = dataLoan.ExtractStaticGFEArchive();
                }
            }
            else if (GFEArchiveDate != null)
            {

                if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    gfe = (from a in dataLoan.GFEArchives
                           where a.DateArchived == GFEArchiveDate
                           select a).First();
                }
                else
                {
                    var newGfe =
                    dataLoan.sClosingCostArchive.FirstOrDefault(p => (p.ClosingCostArchiveType == ClosingCostArchive.E_ClosingCostArchiveType.Gfe2015)
                        && p.DateArchived == GFEArchiveDate);
                    if (newGfe != null)
                    {
                        dataLoan.ApplyClosingCostArchive(newGfe);
                        gfe = dataLoan.ExtractStaticGFEArchive();
                    }
                }
            }
            else
            {
                gfe = dataLoan.LastDisclosedGFEArchive;
            }

            if (gfe != null)
            {
                ApplyData(gfe);                
            }
        }


        protected string GFEArchiveDate
        {
            get 
            {
                if (Arguments != null)
                {
                    return Arguments["GFEArchiveDate"];
                }
                else
                {
                    return null;
                }
            }
        }

        protected Guid GFEArchiveId
        {
            get
            {
                if (Arguments != null)
                {
                    return (new LosConvert()).ToGuid(Arguments["GFEArchiveId"]);
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }
    }
}
