﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CCOTangibleNetBenefitDisclosurePDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "COTangibleNetBenefitDisclosure.pdf"; }
        }
        public override string Description
        {
            get { return "Colorado Net Tangible Benefit Worksheet"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sSpAddr_SingleLine", dataLoan.sSpAddr_SingleLine);
        }
    }
}