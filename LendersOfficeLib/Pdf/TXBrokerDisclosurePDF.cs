using System;
using System.Collections;
using System.Collections.Specialized;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
	public class CTXBrokerDisclosure_PDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "TXBroker_Disclosure.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            // 2/5/2004 dd - Not sure if should use company name and company license number.
            // From Lisa Williams email, looks like she use loan officer name.

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.TXMortgageBrokerDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("PreparerName", preparer.PreparerName);
            AddFormFieldData("PreparerLicenseNumber", preparer.LicenseNumOfAgent);

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sTexasDiscWillSubmitToLender", dataLoan.sTexasDiscWillSubmitToLender);
            AddFormFieldData("sTexasDiscAsIndependentContractor", dataLoan.sTexasDiscAsIndependentContractor);
            AddFormFieldData("sTexasDiscWillActAsFollowsDesc", dataLoan.sTexasDiscWillActAsFollowsDesc);
            AddFormFieldData("sTexasDiscCompensationIncluded", dataLoan.sTexasDiscCompensationIncluded);
            AddFormFieldData("sTexasDiscChargeVaried", dataLoan.sTexasDiscChargeVaried);
            AddFormFieldData("sTexasDiscReceivedF", dataLoan.sTexasDiscReceivedF_rep);
            AddFormFieldData("sTexasDiscAppFInc", dataLoan.sTexasDiscAppFInc);
            AddFormFieldData("sTexasDiscAppFAmt", dataLoan.sTexasDiscAppFAmt_rep);
            AddFormFieldData("sTexasDiscProcFInc", dataLoan.sTexasDiscProcFInc);
            AddFormFieldData("sTexasDiscProcF", dataLoan.sTexasDiscProcF_rep);
            AddFormFieldData("sTexasDiscApprFInc", dataLoan.sTexasDiscApprFInc);
            AddFormFieldData("sTexasDiscApprF", dataLoan.sTexasDiscApprF_rep);
            AddFormFieldData("sTexasDiscCrFInc", dataLoan.sTexasDiscCrFInc);
            AddFormFieldData("sTexasDiscCrF", dataLoan.sTexasDiscCrF_rep);
            AddFormFieldData("sTexasDiscAutoUnderwritingFInc", dataLoan.sTexasDiscAutoUnderwritingFInc);
            AddFormFieldData("sTexasDiscAutoUnderwritingFAmt", dataLoan.sTexasDiscAutoUnderwritingFAmt_rep);
            AddFormFieldData("sTexasDiscOF1IncDesc", dataLoan.sTexasDiscOF1IncDesc);
            AddFormFieldData("sTexasDiscOF1IncAmt", dataLoan.sTexasDiscOF1IncAmt_rep);
            AddFormFieldData("sTexasDiscOF2IncDesc", dataLoan.sTexasDiscOF2IncDesc);
            AddFormFieldData("sTexasDiscOF2IncAmt", dataLoan.sTexasDiscOF2IncAmt_rep);
            AddFormFieldData("sTexasDiscNonRefundAmt", dataLoan.sTexasDiscNonRefundAmt_rep);
            AddFormFieldData("sTexasDiscWillActAsFollows", dataLoan.sTexasDiscWillActAsFollows);
            AddFormFieldData("sTexasDiscChargeVariedDesc", dataLoan.sTexasDiscChargeVariedDesc);
        }
	}

    public class CTXBrokerDisclosurePDF : AbstractBatchPDF 
    {

        public override string Description 
        {
            get { return "Texas - Mortgage Broker Disclosure"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/TXMortgageBrokerDisclosure.aspx"; }
        }


        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CTXBrokerDisclosure_PDF(),
                                             };
            }
        }
    }
}
