using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Collections.Generic;

namespace LendersOffice.Pdf
{
	/// <summary>
	/// List available custom form for this broker.
	/// </summary>
	public class CustomFormList : IPDFPrintItem
	{
        private Guid m_brokerID;
        private NameValueCollection m_arguments;

        public string Name 
        {
            get { return "WordCustomForm"; }
        }

        public string ID 
        {
            get { return "WordCustomForm"; }
            set { }
        }

        public string Description 
        {
            get { return "Word Custom Forms"; }
        }
        public string EditLink
        {
            get { return ""; }
        }

        public string PreviewLink
        {
            get { return ""; }
        }

        public CPageData DataLoan
        {
            get { return null; }
            set {  }
        }

        public NameValueCollection Arguments
        {
            get { return this.m_arguments; }
            set 
            {
                this.m_arguments = value;

                if (this.m_arguments == null)
                {
                    return;
                }

                Guid argBrokerId;

                if (Guid.TryParse(this.m_arguments["brokerid"], out argBrokerId))
                {
                    this.m_brokerID = argBrokerId;
                }
            }
        }

        public bool IsVisible
        {
            get { return true; }
        }

        public bool HasPdf
        {
            get { return true; }
        }

        public void RenderPrintLink(StringBuilder sb) 
        {
            SqlParameter[] parameters = { new SqlParameter("@BrokerID", m_brokerID) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_brokerID, "ListCustomFormByBrokerID", parameters)) 
            {
                while (reader.Read()) 
                {
                    Guid customFormID = (Guid) reader["CustomLetterID"];
                    string title = (string) reader["Title"];
                    sb.Append(string.Format(@"<tr><td><input name='_{0}' type=checkbox customform=t customletterid='{2}'></td><td>{1} (<a href=""#"" onclick=""downloadCustomForm('{2}');"">preview</a>)</td></tr>", customFormID.ToString("N"), Utilities.SafeHtmlString(title), customFormID));
                }
            }
        }

        #region IPDFPrintItem Members


        public virtual bool HasPrintPermission
        {
            get { return true; }
        }

        #endregion

        #region IPDFPrintItem Members


        public System.Collections.Generic.IEnumerable<string> DependencyFields
        {
            get { return new List<string>(); }
        }

        #endregion
    }
}
