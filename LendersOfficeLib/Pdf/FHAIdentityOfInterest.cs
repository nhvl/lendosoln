﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    class CFHAIdentityOfInterestPDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "FHAIdentityOfInterest.pdf"; }
        }

        public override string Description
        {
            get { return "FHA Identity of Interest Certification"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aWillOccupyRefiResidence", dataApp.aWillOccupyRefiResidence);
        }

    }
}
