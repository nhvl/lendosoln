using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CGoodFaithEstimatePDF : AbstractLegalPDF
	{
        // 5/26/2004 dd - NHC Webservices GetLoanForm expected this value.
        public override string Name 
        {
            get { return "GFE"; }
        }
        public override string PdfFile 
        {
            get { return "GoodFaithEstimate.pdf"; }
        }

        public override string Description 
        {
            get { return "Obsolete Good Faith Estimate"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/GoodFaithEstimate.aspx"; }
        }
        private LosConvert m_convertLos;

        private void SetProps( string name, int props )
        {
            // A - cost affecting APR
            // B/S - costs to be paid by Broker/Seller
            string str = "";

            if (LosConvert.GfeItemProps_Apr( props )) 
            {
                str += "A ";
            }
            // m_convertLos.GfeItemProps_ToBr( props );
            int paidBy = LosConvert.GfeItemProps_Payer( props );
            if (paidBy == 2) 
            {
                str += "S ";
            } 
            else if (paidBy == 4) 
            {
                str += "B ";
            }

            if (LosConvert.GfeItemProps_Poc(props)) 
            {
                str += "(POC) ";
            }
            AddFormFieldData(name, str);
        }

        protected override  void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string borrowerName = dataApp.aBNm;
            string coborrowerName = dataApp.aCNm;

            string propertyAddress = dataLoan.sSpAddr + ", " + dataLoan.sSpCity + ", " + dataLoan.sSpState + " " + dataLoan.sSpZip;

            m_convertLos = dataLoan.m_convertLos;


            AddFormFieldData("ApplicantName", borrowerName);
            string applicantsName = borrowerName;
            if (coborrowerName.TrimWhitespaceAndBOM() != "") 
            {
                applicantsName += " & " + coborrowerName;
            }
            AddFormFieldData("ApplicantsName", applicantsName);
            AddFormFieldData("CoapplicantName", coborrowerName);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("PropertyAddress", propertyAddress);
            AddFormFieldData("sLpTemplateNm", dataLoan.sLpTemplateNm);
            AddFormFieldData("sLOrigF", dataLoan.sLOrigF_rep);
            SetProps("sLOrigFProps", dataLoan.sLOrigFProps);

            AddFormFieldData("sLDiscnt", dataLoan.sLDiscnt_rep);
            SetProps("sLDiscntProps", dataLoan.sLDiscntProps);

            AddFormFieldData("sApprF", dataLoan.sApprF_rep);
            SetProps("sApprFProps", dataLoan.sApprFProps);
            AddFormFieldData("sCrF", dataLoan.sCrF_rep);
            SetProps("sCrFProps", dataLoan.sCrFProps);

            AddFormFieldData("sInspectF", dataLoan.sInspectF_rep);
            SetProps("sInspectFProps", dataLoan.sInspectFProps);

            AddFormFieldData("sMBrokF", dataLoan.sMBrokF_rep);
            SetProps("sMBrokFProps", dataLoan.sMBrokFProps);

            AddFormFieldData("sTxServF", dataLoan.sTxServF_rep);
            SetProps("sTxServFProps", dataLoan.sTxServFProps);

            AddFormFieldData("sProcF", dataLoan.sProcF_rep);
            SetProps("sProcFProps", dataLoan.sProcFProps);

            AddFormFieldData("sUwF", dataLoan.sUwF_rep);
            SetProps("sUwFProps", dataLoan.sUwFProps);

            AddFormFieldData("sWireF", dataLoan.sWireF_rep);
            SetProps("sWireFProps", dataLoan.sWireFProps);

            AddFormFieldData("s800U1F", dataLoan.s800U1F_rep);
            SetProps("s800U1FProps", dataLoan.s800U1FProps);

            AddFormFieldData("s800U2F", dataLoan.s800U2F_rep);
            SetProps("s800U2FProps", dataLoan.s800U2FProps);

            AddFormFieldData("s800U3F", dataLoan.s800U3F_rep);
            SetProps("s800U3FProps", dataLoan.s800U3FProps);

            AddFormFieldData("s800U4F", dataLoan.s800U4F_rep);
            SetProps("s800U4FProps", dataLoan.s800U4FProps);

            AddFormFieldData("s800U5F", dataLoan.s800U5F_rep);
            SetProps("s800U5FProps", dataLoan.s800U5FProps);

            AddFormFieldData("sEscrowFTable", dataLoan.sEscrowFTable);
            AddFormFieldData("sEscrowF", dataLoan.sEscrowF_rep);
            SetProps("sEscrowFProps", dataLoan.sEscrowFProps);

            AddFormFieldData("sDocPrepF", dataLoan.sDocPrepF_rep);
            SetProps("sDocPrepFProps", dataLoan.sDocPrepFProps);

            AddFormFieldData("sNotaryF", dataLoan.sNotaryF_rep);
            SetProps("sNotaryFProps", dataLoan.sNotaryFProps);

            AddFormFieldData("sAttorneyF", dataLoan.sAttorneyF_rep);
            SetProps("sAttorneyFProps", dataLoan.sAttorneyFProps);

            AddFormFieldData("sTitleInsFTable", dataLoan.sTitleInsFTable);
            AddFormFieldData("sTitleInsF", dataLoan.sTitleInsF_rep);
            SetProps("sTitleInsFProps", dataLoan.sTitleInsFProps);

            AddFormFieldData("sU1Tc", dataLoan.sU1Tc_rep);
            SetProps("sU1TcProps", dataLoan.sU1TcProps);
            AddFormFieldData("sU2Tc", dataLoan.sU2Tc_rep);
            SetProps("sU2TcProps", dataLoan.sU2TcProps);
            AddFormFieldData("sU3Tc", dataLoan.sU3Tc_rep);
            SetProps("sU3TcProps", dataLoan.sU3TcProps);
            AddFormFieldData("sU4Tc", dataLoan.sU4Tc_rep);
            SetProps("sU4TcProps", dataLoan.sU4TcProps);
            AddFormFieldData("sRecFDesc", dataLoan.sRecFDesc);
            AddFormFieldData("sRecF", dataLoan.sRecF_rep);
            SetProps("sRecFProps", dataLoan.sRecFProps);
            AddFormFieldData("sCountyRtcDesc", dataLoan.sCountyRtcDesc);
            AddFormFieldData("sCountyRtc", dataLoan.sCountyRtc_rep);
            SetProps("sCountyRtcProps", dataLoan.sCountyRtcProps);
            AddFormFieldData("sStateRtcDesc", dataLoan.sStateRtcDesc);
            AddFormFieldData("sStateRtc", dataLoan.sStateRtc_rep);
            SetProps("sStateRtcProps", dataLoan.sStateRtcProps);
            AddFormFieldData("sU1GovRtc", dataLoan.sU1GovRtc_rep);
            SetProps("sU1GovRtcProps", dataLoan.sU1GovRtcProps);
            AddFormFieldData("sU2GovRtc", dataLoan.sU2GovRtc_rep);
            SetProps("sU2GovRtcProps", dataLoan.sU2GovRtcProps);
            AddFormFieldData("sU3GovRtc", dataLoan.sU3GovRtc_rep);
            SetProps("sU3GovRtcProps", dataLoan.sU3GovRtcProps);
            AddFormFieldData("sPestInspectF", dataLoan.sPestInspectF_rep);
            SetProps("sPestInspectFProps", dataLoan.sPestInspectFProps);
            AddFormFieldData("sU1Sc", dataLoan.sU1Sc_rep);
            SetProps("sU1ScProps", dataLoan.sU1ScProps);
            AddFormFieldData("sU2Sc", dataLoan.sU2Sc_rep);
            SetProps("sU2ScProps", dataLoan.sU2ScProps);
            AddFormFieldData("sU3Sc", dataLoan.sU3Sc_rep);
            SetProps("sU3ScProps", dataLoan.sU3ScProps);
            AddFormFieldData("sU4Sc", dataLoan.sU4Sc_rep);
            SetProps("sU4ScProps", dataLoan.sU4ScProps);
            AddFormFieldData("sU5Sc", dataLoan.sU5Sc_rep);
            SetProps("sU5ScProps", dataLoan.sU5ScProps);
            AddFormFieldData("sIPia", dataLoan.sIPia_rep);
            SetProps("sIPiaProps", dataLoan.sIPiaProps);
            AddFormFieldData("sMipPia", dataLoan.sMipPia_rep);
            SetProps("sMipPiaProps", dataLoan.sMipPiaProps);
            AddFormFieldData("sHazInsPia", dataLoan.sHazInsPia_rep);
            SetProps("sHazInsPiaProps", dataLoan.sHazInsPiaProps);
            AddFormFieldData("s904Pia", dataLoan.s904Pia_rep);
            SetProps("s904PiaProps", dataLoan.s904PiaProps);
            AddFormFieldData("sVaFf", dataLoan.sVaFf_rep);
            SetProps("sVaFfProps", dataLoan.sVaFfProps);
            AddFormFieldData("s900U1Pia", dataLoan.s900U1Pia_rep);
            SetProps("s900U1PiaProps", dataLoan.s900U1PiaProps);
            AddFormFieldData("sHazInsRsrv", dataLoan.sHazInsRsrv_rep);
            SetProps("sHazInsRsrvProps", dataLoan.sHazInsRsrvProps);
            AddFormFieldData("sMInsRsrv", dataLoan.sMInsRsrv_rep);
            SetProps("sMInsRsrvProps", dataLoan.sMInsRsrvProps);
            AddFormFieldData("sSchoolTxRsrv", dataLoan.sSchoolTxRsrv_rep);
            SetProps("sSchoolTxRsrvProps", dataLoan.sSchoolTxRsrvProps);
            AddFormFieldData("sRealETxRsrv", dataLoan.sRealETxRsrv_rep);
            SetProps("sRealETxRsrvProps", dataLoan.sRealETxRsrvProps);
            AddFormFieldData("sFloodInsRsrv", dataLoan.sFloodInsRsrv_rep);
            SetProps("sFloodInsRsrvProps", dataLoan.sFloodInsRsrvProps);
            AddFormFieldData("s1006Rsrv", dataLoan.s1006Rsrv_rep);
            SetProps("s1006RsrvProps", dataLoan.s1006RsrvProps);
            AddFormFieldData("s1007Rsrv", dataLoan.s1007Rsrv_rep);
            SetProps("s1007RsrvProps", dataLoan.s1007RsrvProps);
            AddFormFieldData("sProFirstMPmt", dataLoan.sProFirstMPmt_rep ); 
            AddFormFieldData("sProSecondMPmt", dataLoan.sProSecondMPmt_rep ); 
            AddFormFieldData("sProHoAssocDues", dataLoan.sProHoAssocDues_rep);
            AddFormFieldData("sProOHExp", dataLoan.sProOHExp_rep);

            AddFormFieldData("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);


            AddFormFieldData("s800U1FCode", dataLoan.s800U1FCode);
            AddFormFieldData("s800U1FDesc", dataLoan.s800U1FDesc);
            AddFormFieldData("s800U2FCode", dataLoan.s800U2FCode);
            AddFormFieldData("s800U2FDesc", dataLoan.s800U2FDesc);
            AddFormFieldData("s800U3FCode", dataLoan.s800U3FCode);
            AddFormFieldData("s800U3FDesc", dataLoan.s800U3FDesc);
            AddFormFieldData("s800U4FCode", dataLoan.s800U4FCode);
            AddFormFieldData("s800U4FDesc", dataLoan.s800U4FDesc);
            AddFormFieldData("s800U5FCode", dataLoan.s800U5FCode);
            AddFormFieldData("s800U5FDesc", dataLoan.s800U5FDesc);
            AddFormFieldData("sU1TcCode", dataLoan.sU1TcCode);
            AddFormFieldData("sU1TcDesc", dataLoan.sU1TcDesc);
            AddFormFieldData("sU2TcCode", dataLoan.sU2TcCode);
            AddFormFieldData("sU2TcDesc", dataLoan.sU2TcDesc);
            AddFormFieldData("sU3TcCode", dataLoan.sU3TcCode);
            AddFormFieldData("sU3TcDesc", dataLoan.sU3TcDesc);
            AddFormFieldData("sU4TcCode", dataLoan.sU4TcCode);
            AddFormFieldData("sU4TcDesc", dataLoan.sU4TcDesc);
            AddFormFieldData("sU1GovRtcCode", dataLoan.sU1GovRtcCode);
            AddFormFieldData("sU1GovRtcDesc", dataLoan.sU1GovRtcDesc);
            AddFormFieldData("sU2GovRtcCode", dataLoan.sU2GovRtcCode);
            AddFormFieldData("sU2GovRtcDesc", dataLoan.sU2GovRtcDesc);
            AddFormFieldData("sU3GovRtcCode", dataLoan.sU3GovRtcCode);
            AddFormFieldData("sU3GovRtcDesc", dataLoan.sU3GovRtcDesc);
            AddFormFieldData("sU1ScCode", dataLoan.sU1ScCode);
            AddFormFieldData("sU1ScDesc", dataLoan.sU1ScDesc);
            AddFormFieldData("sU2ScCode", dataLoan.sU2ScCode);
            AddFormFieldData("sU2ScDesc", dataLoan.sU2ScDesc);
            AddFormFieldData("sU3ScCode", dataLoan.sU3ScCode);
            AddFormFieldData("sU3ScDesc", dataLoan.sU3ScDesc);
            AddFormFieldData("sU4ScCode", dataLoan.sU4ScCode);
            AddFormFieldData("sU4ScDesc", dataLoan.sU4ScDesc);
            AddFormFieldData("sU5ScCode", dataLoan.sU5ScCode);
            AddFormFieldData("sU5ScDesc", dataLoan.sU5ScDesc);
            AddFormFieldData("s904PiaDesc", dataLoan.s904PiaDesc);
            AddFormFieldData("s900U1PiaCode", dataLoan.s900U1PiaCode);
            AddFormFieldData("s900U1PiaDesc", dataLoan.s900U1PiaDesc);
            AddFormFieldData("s1006ProHExpDesc", dataLoan.s1006ProHExpDesc);
            if (dataLoan.s1006ProHExpDesc.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("s1006", "1006");
            }
            AddFormFieldData("s1007ProHExpDesc", dataLoan.s1007ProHExpDesc);
            if (dataLoan.s1007ProHExpDesc.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("s1007", "1007");
            }

            AddFormFieldData("sAggregateAdjRsrv", dataLoan.sAggregateAdjRsrv_rep);
            SetProps("sAggregateAdjRsrvProps", dataLoan.sAggregateAdjRsrvProps);
            AddFormFieldData("sAggregateAdjRsrvDesc", "Aggregate Adjustment", false);

            if (dataLoan.sPrintCompensationOnGfe) 
            {
                // 5/10/2006 dd - See OPM 4620
				// 4/9/2007 nw - OPM 12321
				if (dataLoan.sBrokComp1Lckd)
				{
					dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
					AddFormFieldData("sBrokComp1", dataLoan.sBrokComp1_rep);
					dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);
				}
				else
					AddFormFieldData("sBrokComp1", dataLoan.sBrokComp1_rep);

				if (dataLoan.sBrokComp2Desc != "")
				{
					dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
					AddFormFieldData("sBrokComp2", dataLoan.sBrokComp2_rep);
					dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);
				}
				else
					AddFormFieldData("sBrokComp2", dataLoan.sBrokComp2_rep);

                AddFormFieldData("sBrokComp1Desc", dataLoan.sBrokComp1Desc);
                AddFormFieldData("sBrokComp2Desc", dataLoan.sBrokComp2Desc);
            }
            AddFormFieldData("sTotEstCc", dataLoan.sTotEstCc_rep);
            AddFormFieldData("sTotEstPp", dataLoan.sTotEstPp_rep);
            AddFormFieldData("sRefPdOffAmt", dataLoan.sRefPdOffAmtGfe_rep);

            AddFormFieldData("sTotCcPbs", dataLoan.sTotCcPbs_rep);

            // Use E_PreparerFormT.GfeTil
            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject );
            
            string phoneOfCompany = gfeTil.PhoneOfCompany.TrimWhitespaceAndBOM();
            string prepByFormat;
            // This if statement is required. Otherwise, empty phone number will cause information to display incorrectly.
            if( "" == phoneOfCompany )
                prepByFormat = "{0}{6}{2}{6}{3}, {4} {5}";
            else
                prepByFormat = "{0}          Ph {1}{6}{2}{6}{3}, {4} {5}";
				
            AddFormFieldData("GfeTil", string.Format( prepByFormat, 
                gfeTil.CompanyName, phoneOfCompany, gfeTil.StreetAddr, gfeTil.City, 
                gfeTil.State, gfeTil.Zip, Environment.NewLine));

            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep + "/" + dataLoan.sDue_rep);
            AddFormFieldData("sNoteR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            if (dataLoan.sGfeProvByBrok) 
            {
                AddFormFieldData("sGfeProvByBrok", true);
                AddFormFieldData("BrokerName", gfeTil.CompanyName);
            }
            if (dataLoan.sApprFPaid) 
            {
                AddFormFieldData("sApprFPaid", "(PAID)");
            }
            if (dataLoan.sCrFPaid) 
            {
                AddFormFieldData("sCrFPaid", "(PAID)");
            }
            if (dataLoan.sProcFPaid) 
            {
                AddFormFieldData("sProcFPaid", "(PAID)");
            }
            AddFormFieldData("sIPiaDy", dataLoan.sIPiaDy_rep);
            AddFormFieldData("sIPerDay", dataLoan.sIPerDay_rep);
            AddFormFieldData("sProHazIns", dataLoan.sProHazIns_rep);
            AddFormFieldData("sProMIns", dataLoan.sProMIns_rep);
            AddFormFieldData("sProSchoolTx", dataLoan.sProSchoolTx_rep);
            AddFormFieldData("sProRealETx", dataLoan.sProRealETx_rep);
            AddFormFieldData("sProFloodIns", dataLoan.sProFloodIns_rep);
            AddFormFieldData("s1006ProHExp", dataLoan.s1006ProHExp_rep);
            AddFormFieldData("s1007ProHExp", dataLoan.s1007ProHExp_rep);
            AddFormFieldData("sHazInsRsrvMon", dataLoan.sHazInsRsrvMon_rep);
            AddFormFieldData("sMInsRsrvMon", dataLoan.sMInsRsrvMon_rep);
            AddFormFieldData("sSchoolTxRsrvMon", dataLoan.sSchoolTxRsrvMon_rep);
            AddFormFieldData("sRealETxRsrvMon", dataLoan.sRealETxRsrvMon_rep);
            AddFormFieldData("sFloodInsRsrvMon", dataLoan.sFloodInsRsrvMon_rep);
            AddFormFieldData("s1006RsrvMon", dataLoan.s1006RsrvMon_rep);
            AddFormFieldData("s1007RsrvMon", dataLoan.s1007RsrvMon_rep);
            AddFormFieldData("sTotEstSc", dataLoan.sTotEstSc_rep);

            string sLOrigFPc = "";
            if (dataLoan.sLOrigFPc != 0) 
            {
                sLOrigFPc = dataLoan.sLOrigFPc_rep + "%";
            }
            if (dataLoan.sLOrigFMb != 0) 
            {
                sLOrigFPc += " + " + (dataLoan.sLOrigFMb_rep);
            }
            AddFormFieldData("sLOrigFPc", sLOrigFPc);

            string sLDiscntPc = "";
            if (dataLoan.sLDiscntPc != 0) 
            {
                sLDiscntPc = dataLoan.sLDiscntPc_rep + "%";
            }
            if (dataLoan.sLDiscntFMb != 0) 
            {
                sLDiscntPc += " + " + (dataLoan.sLDiscntFMb_rep);
            }
            AddFormFieldData("sLDiscntPc", sLDiscntPc);
            string sMBrokFPc = "";
            if (dataLoan.sMBrokFPc != 0) 
            {
                sMBrokFPc = dataLoan.sMBrokFPc_rep + "%";
            }
            if (dataLoan.sMBrokFMb != 0) 
            {
                sMBrokFPc += " + " + (dataLoan.sMBrokFMb_rep);
            }
            AddFormFieldData("sMBrokFPc", sMBrokFPc);

            if (dataLoan.sUseObsoleteGfeForm)
                DisplayObsoleteTotalEstimatedFundsNeedToClose(dataLoan);
            else
                DisplayTotalEstimatedFundsNeedToClose(dataLoan);

            #region OPM 33368
            int count = dataLoan.GetAgentRecordCount();
            // If there are multiple record of appraiser, escrow etc, then use the first
            // record.
            bool isAppraiser = false;
            bool isEscrow = false;
            bool isTitle = false;
            bool isCredit = false;
            bool isHomeInspection = false;
            bool isClosingAgent = false;
            string escrowCompanyName = "";
            string closingAgentName = "";

            for (int i = 0; i < count; i++)
            {
                CAgentFields field = dataLoan.GetAgentFields(i);
                switch (field.AgentRoleT)
                {
                    case E_AgentRoleT.CreditReport:
                        if (!isCredit)
                        {
                            isCredit = true;
                            AddFormFieldData("CreditReportCompanyName", field.CompanyName);
                        }
                        break;
                    case E_AgentRoleT.Appraiser:
                        if (!isAppraiser)
                        {
                            AddFormFieldData("AppraisalCompanyName", field.CompanyName);
                            isAppraiser = true;
                        }
                        break;
                    case E_AgentRoleT.Escrow:
                        if (!isEscrow)
                        {
                            escrowCompanyName = field.CompanyName;
                            if (!escrowCompanyName.TrimWhitespaceAndBOM().Equals(""))
                            {
                                isEscrow = true;
                            }
                        }
                        break;
                    case E_AgentRoleT.Title:
                        if (!isTitle)
                        {
                            AddFormFieldData("TitleCompanyName", field.CompanyName);
                            isTitle = true;
                        }
                        break;
                    case E_AgentRoleT.ClosingAgent:
                        if (!isClosingAgent)
                        {
                            closingAgentName = field.CompanyName;
                            if (!closingAgentName.TrimWhitespaceAndBOM().Equals(""))
                            {
                                isClosingAgent = true;
                            }
                        }
                        break;
                    case E_AgentRoleT.HomeInspection:
                        if (!isHomeInspection)
                        {
                            AddFormFieldData("HomeInspectionCompanyName", field.CompanyName);
                            isHomeInspection = true;
                        }
                        break;
                    default:
                        // We don't care about the agents we are not using for this form
                        break;
                }
            }

            // Per OPM 33368, if both the closing and escrow agent exist and they do not have the same company name,
            // we will append them together with a / between them
            
            if (isClosingAgent && !isEscrow)
            {
                AddFormFieldData("ClosingAgentEscrowCompanyName", closingAgentName);
            }
            else if (isEscrow && !isClosingAgent)
            {
                AddFormFieldData("ClosingAgentEscrowCompanyName", escrowCompanyName);
            }
            else if (isEscrow && isClosingAgent)
            {
                if (closingAgentName.TrimWhitespaceAndBOM().Equals(escrowCompanyName.TrimWhitespaceAndBOM()))
                {
                    AddFormFieldData("ClosingAgentEscrowCompanyName", closingAgentName);
                }
                else
                {
                    AddFormFieldData("ClosingAgentEscrowCompanyName", closingAgentName + " / " + escrowCompanyName);
                }
            }

            #endregion
        }

        public void DisplayTotalEstimatedFundsNeedToClose(CPageData dataLoan) 
        {
            ArrayList textList = new ArrayList();
            ArrayList valueList = new ArrayList();

            textList.Add("a. Purchase Price");
            valueList.Add(dataLoan.sPurchPrice_rep);
            textList.Add("b. Alterations, improvements");
            valueList.Add(dataLoan.sAltCost_rep);
            textList.Add("c. Land");
            valueList.Add(dataLoan.sLandCost_rep);
            textList.Add("d. Refi (incl debts to be paid off)");
            valueList.Add(dataLoan.sRefPdOffAmt1003_rep);
            textList.Add("e. Estimated prepaid items");
            valueList.Add(dataLoan.sTotEstPp1003_rep);
            textList.Add("f. Estimated closing costs");
            valueList.Add(dataLoan.sTotEstCcNoDiscnt1003_rep);
            textList.Add("g. PMI, MIP, Funding Fee");
            valueList.Add(dataLoan.sFfUfmip1003_rep);
            textList.Add("h. Discount");
            valueList.Add(dataLoan.sLDiscnt1003_rep);
            textList.Add("i. Total costs (sum a through h)");
            valueList.Add(dataLoan.sTotTransC_rep);
            textList.Add(""); // 3/18/2004 dd - Blank line
            valueList.Add(""); // 3/18/2004 dd - Blank line
            textList.Add(""); // 3/18/2004 dd - Blank line
            valueList.Add(""); // 3/18/2004 dd - Blank line
            textList.Add(""); // 3/18/2004 dd - Blank line
            valueList.Add(""); // 3/18/2004 dd - Blank line
            textList.Add("j. Subordinate financing");
            valueList.Add(dataLoan.sONewFinBal_rep);
            textList.Add("k. Closing Costs paid by Seller");
            valueList.Add(dataLoan.sTotCcPbs_rep);
            textList.Add("l. Other Credits");
            valueList.Add("");
			textList.Add("   " + dataLoan.sONewFinCcAsOCreditAmtDesc);
			valueList.Add(dataLoan.sONewFinCcAsOCreditAmt_rep);

            textList.Add("   " + dataLoan.sOCredit1Desc);
            valueList.Add(dataLoan.sOCredit1Amt_rep);

            textList.Add("   " + dataLoan.sOCredit2Desc);
            valueList.Add(dataLoan.sOCredit2Amt_rep);

            textList.Add("   " + dataLoan.sOCredit3Desc);
            valueList.Add(dataLoan.sOCredit3Amt_rep);

            textList.Add("   " + dataLoan.sOCredit4Desc);
            valueList.Add(dataLoan.sOCredit4Amt_rep);

            textList.Add("m. Loan amount (exclude PMI, MIP)");
            valueList.Add(dataLoan.sLAmt1003_rep);
            textList.Add("n. PMI, MIP, Funding Fee financed");
            valueList.Add(dataLoan.sFfUfmipFinanced_rep);
            textList.Add("o. Loan amount (add m & n)");
            valueList.Add(dataLoan.sFinalLAmt_rep);

            textList.Add(""); // 3/18/2004 dd - Blank line
            valueList.Add(""); // 3/18/2004 dd - Blank line

            for (int i = 0; i < textList.Count; i++) 
            {
                int j = i < 12 ? 0 : 2;

                AddFormFieldData("Item" + ((char) ('A' + j)).ToString() + (i % 12), (string) textList[i], false);
                AddFormFieldData("Item" + ((char) ('B' + j)).ToString() + (i % 12), (string) valueList[i]);
            }

            string placeholder = "Item" + (textList.Count < 12 ? "B12" : "D12");
            AddFormFieldData("sTransNetCashFromToDesc", dataLoan.sTransNetCashFromToDesc);
            AddFormFieldData(placeholder, dataLoan.sTransNetCashAbsVal_rep);

        }
        // 3/18/2004 dd - sUseObsoleteGfeForm is checked then we need to print out Total Estimated Funds section in old format.
        public void DisplayObsoleteTotalEstimatedFundsNeedToClose(CPageData dataLoan) 
        {
            // Display items in TOTAL ESTIMATED FUNDS NEED TO CLOSE section.
            ArrayList textList = new ArrayList();
            ArrayList valueList = new ArrayList();

            if (dataLoan.sLPurposeT == E_sLPurposeT.Purchase) 
            {
                textList.Add("Purchase Price (+)");
                valueList.Add(dataLoan.sPurchPrice_rep);
            } 
            else 
            {
                textList.Add("Payoff (+)");
                valueList.Add(dataLoan.sRefPdOffAmtGfe_rep);
            }

            textList.Add("Loan Amount (-)");
            valueList.Add(dataLoan.sFinalLAmt_rep);

            if (dataLoan.sLT == E_sLT.FHA || dataLoan.sLT == E_sLT.VA) 
            {
                textList.Add("(Base Loan Amount + UFMIP/FF)");
                valueList.Add(""); // (Base Loan Amount + UFMIP/FF) is just a description for Loan Amount.

                textList.Add("UFMIP/FF Financed (+)");
                valueList.Add(dataLoan.sFfUfmip1003_rep);
            }

            textList.Add("Est. CLosing Costs (+)");
            valueList.Add(dataLoan.sTotEstCc_rep);

            textList.Add("Est. Prepaid Items/Reserves (+)");
            valueList.Add(dataLoan.sTotEstPp_rep);

            textList.Add("Amount Paid by Seller (-)");
            valueList.Add(dataLoan.sTotCcPbs_rep);

            textList.Add("Amount Paid by Other (-)");
            valueList.Add(dataLoan.sTotCcPbo_rep);

            if (dataLoan.sLT == E_sLT.FHA || dataLoan.sLT == E_sLT.VA) 
            {
                textList.Add("Required Cash Investment (+)");
                valueList.Add(dataLoan.sFHAReqCashInv_rep);

                textList.Add("MIP Refund (-)");
                valueList.Add(dataLoan.sFHASalesConcessions_rep);

                textList.Add("203k Rehabilitation Cost (+)");
                valueList.Add(dataLoan.sFHA203kRehabCost_rep);

                textList.Add("EEM Improvements (+)");
                valueList.Add(dataLoan.sFHAEnergyEffImprov_rep);
            }

            textList.Add(dataLoan.sU1FntcDesc);
            valueList.Add(dataLoan.sU1Fntc_rep);

            for (int i = 0; i < textList.Count; i++) 
            {
                int j = i < 9 ? 0 : 2;

                AddFormFieldData("Item" + ((char) ('A' + j)).ToString() + (i % 9), (string) textList[i], false);
                AddFormFieldData("Item" + ((char) ('B' + j)).ToString() + (i % 9), (string) valueList[i]);
            }

            // Display either sTotEstFntc or sFHATotEstFntc in the total place holder depends on loan type.
            string placeholder = "Item" + (textList.Count < 9 ? "B12" : "D12");
            decimal value = (dataLoan.sLT == E_sLT.FHA  || dataLoan.sLT == E_sLT.VA) ? dataLoan.sFHATotEstFntc : dataLoan.sTotEstFntc;

            if (value < 0) 
            {
                AddFormFieldData("sTransNetCashFromToDesc", "TO");
                AddFormFieldData(placeholder, m_convertLos.ToMoneyString((-1 * value), FormatDirection.ToRep));
            } 
            else 
            {
                AddFormFieldData("sTransNetCashFromToDesc", "FROM");
                AddFormFieldData(placeholder, m_convertLos.ToMoneyString(value, FormatDirection.ToRep));
            }
        }


	}
}
