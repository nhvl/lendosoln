﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class SCAttorneyInsurancePreferencePDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "SCAttorneyInsurancePreference.pdf"; }
        }
        public override string Description
        {
            get { return "South Carolina Attorney/Insurance Preference Form"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }
}