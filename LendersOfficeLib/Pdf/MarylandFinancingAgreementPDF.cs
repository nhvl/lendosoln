﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CMarylandFinancingAgreementPDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "MarylandFinancingAgreement.pdf"; }
        }
        public override string Description
        {
            get { return "Maryland Financing Agreement"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            CAgentFields loanOfficer = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("Agent_LoanOfficer_CompanyName", loanOfficer.CompanyName);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sSpAddr_MultiLine", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sLT_rep", dataLoan.sLT_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sLDiscnt", dataLoan.sLDiscnt_rep);
            AddFormFieldData("sLDiscntPc", dataLoan.sLDiscntPc_rep);
        }
    }
}