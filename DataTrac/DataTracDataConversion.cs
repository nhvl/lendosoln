using System;
using System.Collections;
using System.Xml;


using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Reflection;

namespace DataTrac
{
	public class IntegrationFields
	{
		#region Variables
		private ArrayList m_thirdPartyFieldIds = null;
		private ArrayList m_pmlFieldIds = null;
		private ArrayList m_Fields = null;

		public ArrayList ThirdPartyFields
		{
			get { return m_thirdPartyFieldIds; }
		}

		public ArrayList PmlFieldsIDs
		{
			get { return m_pmlFieldIds; }
		}

		public ArrayList Fields
		{
			get { return m_Fields; }
		}
		#endregion

		public IntegrationFields()
		{
			m_thirdPartyFieldIds = new ArrayList();
			m_pmlFieldIds = new ArrayList();
			m_Fields = new ArrayList();
		}

		private bool IsDuplicate(string pmlfieldName, string thirdPartyFieldName)
		{
			// Return true if both the PML field and 3rd party field are already mapped.  If only one is, that's ok, some fields are used for multiple things.
			return m_pmlFieldIds.Contains(pmlfieldName) && m_thirdPartyFieldIds.Contains(thirdPartyFieldName);
		}

		public void AddField(IntegrationField iField)
		{
			if(!IsDuplicate(iField.PmlFieldName, iField.ThirdPartyFieldName))
			{
				m_Fields.Add(iField);
				m_pmlFieldIds.Add(iField.PmlFieldName);
				m_thirdPartyFieldIds.Add(iField.ThirdPartyFieldName);
			}
		}
	}
	
	public class IntegrationField
	{
		#region Variables
		private string		m_pmlFieldName = ""; //required
		private string		m_thirdPartyFieldName = ""; //required
		private string		m_description = "";
		private string		m_defaultPmlMapValue = "";
		private string		m_defaultThirdPartyMapValue = "";
		private string		m_whereToFindIt = "";
		private string		m_notes = "";
		private bool		m_isValid = true;
		private Hashtable	m_pmlFieldMap = null;
		private Hashtable	m_thirdPartyFieldMap = null;
		private bool		m_mappingExists = false;
		private string		m_thirdPartyValue = "";
		private string		m_pmlValue = "";

		public string PmlFieldName
		{
			get { return m_pmlFieldName; }
		}

		public string ThirdPartyFieldName
		{
			get { return m_thirdPartyFieldName; }
		}

		public string Description
		{
			get { return m_description; }
		}

		public string WhereToFindIt
		{
			get { return m_whereToFindIt; }
		}

		public string Notes
		{
			get { return m_notes; }
		}

		public bool IsValid
		{
			get { return m_isValid; }
		}

		// Represents the actual value from the third party loan data for this field
		public string ThirdPartyValue
		{
			get { return m_thirdPartyValue; }
			set { m_thirdPartyValue = value; }
		}

		// Represents the actual value from the pml loan data for this field
		public string PmlValue
		{
			get { return m_pmlValue; }
			set { m_pmlValue = value; }
		}

		#endregion

		public IntegrationField(string pmlFieldName, string thirdPartyFieldName, string description, string defaultPmlMapValue, string defaultThirdPartyMapValue, string whereToFindIt, string notes)
		{
			m_pmlFieldName = pmlFieldName;
			m_thirdPartyFieldName = thirdPartyFieldName;
			m_description = description;
			m_defaultPmlMapValue = defaultPmlMapValue;
			m_defaultThirdPartyMapValue = defaultThirdPartyMapValue;
			m_whereToFindIt = whereToFindIt;
			m_notes = notes;
			m_pmlFieldMap = new Hashtable();
			m_thirdPartyFieldMap = new Hashtable();

			ValidateFields();
		}

		public IntegrationField(string pmlFieldName, string thirdPartyFieldName, string pmlFieldValue, string thirdPartyFieldValue)
		{
			m_pmlFieldName = pmlFieldName;
			m_thirdPartyFieldName = thirdPartyFieldName;
			m_pmlValue = pmlFieldValue;
			m_thirdPartyValue = thirdPartyFieldValue;
			m_pmlFieldMap = new Hashtable();
			m_thirdPartyFieldMap = new Hashtable();
		}

		public void InsertPmlFieldMap(string pmlFieldValue, string thirdPartyFieldValue)
		{
			m_mappingExists = true;
			pmlFieldValue = pmlFieldValue.ToLower();
			thirdPartyFieldValue = thirdPartyFieldValue.ToLower();
			if(!m_pmlFieldMap.Contains(pmlFieldValue))
				m_pmlFieldMap.Add(pmlFieldValue, thirdPartyFieldValue);
		}

		public void InsertThirdPartyFieldMap(string pmlFieldValue, string thirdPartyFieldValue)
		{
			m_mappingExists = true;
			pmlFieldValue = pmlFieldValue.ToLower();
			thirdPartyFieldValue = thirdPartyFieldValue.ToLower();
			if(!m_thirdPartyFieldMap.Contains(thirdPartyFieldValue))
				m_thirdPartyFieldMap.Add(thirdPartyFieldValue, pmlFieldValue);
		}

		public string GetThirdPartyFieldValue(string pmlFieldValue)
		{
			pmlFieldValue = pmlFieldValue.Trim().ToLower();
			
			if(m_mappingExists == false)
			{
				return pmlFieldValue;
			}
			else if(m_pmlFieldMap.Contains(pmlFieldValue))
			{
				return m_pmlFieldMap[pmlFieldValue].ToString();
			}
			else
			{
				return m_defaultThirdPartyMapValue;
			}
		}

		public string GetPmlFieldValue(string thirdPartyFieldValue)
		{
            thirdPartyFieldValue = thirdPartyFieldValue.Trim().ToLower();
			if(m_mappingExists == false)
				return thirdPartyFieldValue;
			else if(m_thirdPartyFieldMap.Contains(thirdPartyFieldValue))
				return m_thirdPartyFieldMap[thirdPartyFieldValue].ToString();
			else
				return m_defaultPmlMapValue;
		}

		private void ValidateFields()
		{
			if(m_pmlFieldName.Trim().Equals(""))
			{
				m_isValid = false;
				string msg = String.Format("(DB) Unable to add field to Third Party integration field list from xml config file - the PmlField attribute is blank.{0}  Fields: {1}", Environment.NewLine, this.ToString());
				PaulBunyanHelper.Bug(msg);
			}
			if(m_thirdPartyFieldName.Trim().Equals(""))
			{
				m_isValid = false;
				string msg = String.Format("(DB) Unable to add field to Third Party integration field list from xml config file - the ThirdPartyField attribute is blank.{0}  Fields: {1}", Environment.NewLine, this.ToString());
				PaulBunyanHelper.Bug(msg);
			}
		}

		public override string ToString()
		{
			return String.Format("PmlFieldName = {0}, ThirdPartyFieldName = {1}, Description = {2}, WhereToFindIt = {3}, Notes = {4}", m_pmlFieldName, m_thirdPartyFieldName, m_description, m_whereToFindIt, m_notes);
		}
	}
	
	
	public class ThirdPartyConversion
	{
		#region Variables
		protected static string m_fieldXmlFilePath = "";
		private IntegrationFields m_integrationFields = null;

		public static string FieldXmlFilePath
		{
			get { return m_fieldXmlFilePath; }
			set { m_fieldXmlFilePath = value; }
		}

		public ArrayList PmlFieldIDs
		{
			get 
			{ 
				if(m_integrationFields == null)
					return new ArrayList();
				else
					return m_integrationFields.PmlFieldsIDs;
			}
		}

		public ArrayList Fields
		{
			get 
			{ 
				if(m_integrationFields == null)
					return new ArrayList();
				else
					return m_integrationFields.Fields;
			}
		}
		#endregion
		
		public ThirdPartyConversion(string xmlFieldsPath)
		{
			m_fieldXmlFilePath = xmlFieldsPath;

			m_integrationFields = new IntegrationFields();
			LoadFields();
		}

		private string GetFieldElementValue(XmlElement fieldElement, string fieldName)
		{
			if(fieldElement.HasAttribute(fieldName))
				return fieldElement.GetAttribute(fieldName);
			else
				return "";
		}

		private void LoadFields()
		{
			XmlDocument doc = new XmlDocument();

			doc.Load(FieldXmlFilePath);

			XmlElement root = (XmlElement) doc.SelectSingleNode("//IntegrationFields");
			
			foreach (XmlElement categoryElement in root.ChildNodes) //IntegrationFields//category
			{
				try
				{
					string categoryName = categoryElement.GetAttribute("name");
					if(categoryName.ToUpper().Equals("IGNORE"))
						continue;

					foreach(XmlElement fieldElement in categoryElement.SelectNodes("field")) //IntegrationFields//category//field
					{
						try
						{
							IntegrationField field = new IntegrationField(fieldElement.GetAttribute("PMLField"), fieldElement.GetAttribute("ThirdPartyField"), GetFieldElementValue(fieldElement, "Description"), GetFieldElementValue(fieldElement, "DefaultPmlMapValue"), GetFieldElementValue(fieldElement, "DefaultDTMapValue"), GetFieldElementValue(fieldElement, "WhereToFindIt"), GetFieldElementValue(fieldElement, "Notes"));		
							
							foreach (XmlElement optionElement in fieldElement.SelectNodes("value_map_type")) //IntegrationFields//category//field//value_map_type
							{
								string subCategoryName = optionElement.GetAttribute("Type");
								
								foreach (XmlElement mapElement in optionElement.SelectNodes("value_map")) //IntegrationFields//category//field//value_map_type//value_map
								{
									if(subCategoryName.Equals("PmlValueMap"))
									{
										field.InsertPmlFieldMap(mapElement.GetAttribute("PMLValue"), mapElement.GetAttribute("ThirdPartyValue"));
									}
									else if(subCategoryName.Equals("ThirdPartyValueMap"))
									{
										field.InsertThirdPartyFieldMap(mapElement.GetAttribute("PMLValue"), mapElement.GetAttribute("ThirdPartyValue"));
									}
									else
									{
										// do nothing - we don't know how to handle this element
										PaulBunyanHelper.Warning("(db) Unknown element type '" + subCategoryName + "' found in Third Party Conversion - ignoring.");
									}
								}
							}
							//TODO - remove, only for debugging
							//PaulBunyanHelper.Warning("Adding field from xml: " + field.PmlFieldName + ", dtfieldname: " + field.ThirdPartyFieldName);
							m_integrationFields.AddField(field);
						}
						catch(Exception e)
						{
							PaulBunyanHelper.Error("Error while loading a Pml/IntegrationFields field from the XML field list in file: " + FieldXmlFilePath, e);
						}
					}
				}
				catch(Exception e)
				{
					PaulBunyanHelper.Error("Error while loading a Pml/IntegrationFields category from the XML field list in file: " + FieldXmlFilePath, e);
				}
			}
		}
	}
}
