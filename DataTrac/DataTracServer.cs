using System;
using System.Collections;
using System.IO;
using System.Xml;

namespace DataTrac
{
	public enum E_DataTracExportType
	{
		POINT = 0, // It's preferable to use CALYXFULL since it contains more data
		CALYXFULL = 1, // Better than plain old Point
		DU = 2, // DU 3.0 files
		THTW = 3 // DU 3.2 files
	}
    
	public class DataTracServer
	{
		#region Variables
		DataTracInterfaceProxy m_dtInterface = null;
		private string m_loginName = "";
		private string m_password = "";
		private string m_pathToDataTrac = "";
		private ArrayList m_UserMessages = null;
		private ArrayList m_DevMessages = null;
		private ArrayList m_UserErrorMessages = null;
		private ArrayList m_DevErrorMessages = null;
		private string m_programId = "";
		private string m_fileContent = "";
        private double m_webserviceVersion = 1.0;
		protected string ErrorFromDTExport_Prefix = "DataTrac has encountered an error and may not have updated with all loan data.  \\nPlease check DataTrac to make sure the necessary data has been uploaded.  If errors continue, please contact DataTrac.\\n\\nDataTrac error: ";
        private static string ERROR_RESULT_PREFIX = "Error_Result: ";
        private static string NORMAL_RESULT_PREFIX = "Normal_Result: ";

		public ArrayList UserWarningMessages
		{
			get { return m_UserMessages; }
		}

		public ArrayList DevWarningMessages
		{
			get { return m_DevMessages; }
		}

		public ArrayList UserErrorMessages
		{
			get { return m_UserErrorMessages; }
		}

		public ArrayList DevErrorMessages
		{
			get { return m_DevErrorMessages; }
		}

        public double WebserviceVersion
        {
            get { return m_webserviceVersion; }
        }
		#endregion
		
		public DataTracServer(string loginName, string password, string pathToDataTrac, string webserviceURL)
		{
			m_dtInterface = new DataTracInterfaceProxy(); 
			m_dtInterface.Url = webserviceURL;
			m_loginName = loginName;
			m_password = password;
			m_pathToDataTrac = pathToDataTrac;

			m_UserMessages = new ArrayList();
			m_DevMessages = new ArrayList();
			m_UserErrorMessages = new ArrayList();
			m_DevErrorMessages = new ArrayList();

            m_webserviceVersion = GetWebserviceVersion();
        }

        #region Public methods that call webservice functions
        
        public double GetWebserviceVersion()
        {
            try
            {
                // As of 1/22/09, we do not need to pass actual login information for this function because we don't
                // currently have a way to retrieve the verison information for DataTrac or TracTools.
                string versionXml = m_dtInterface.GetVersionInformation("", "", "");
                XmlDocument errorXml = CreateXmlDoc(versionXml);
                XmlNode messageNode = errorXml.SelectSingleNode("Versions//Webservice_Version");
                return double.Parse(messageNode.InnerXml);
            }
            catch (Exception e)
            {
                PaulBunyanHelper.Error("(db) Unable to retrieve webservice version for DataTrac Webservice.  Exception details: " + e.ToString());
            }

            return 1.0;
        }
        

        public bool doesLoanExistInDataTrac(string dtLoanNumber)
		{
            bool loanExists = false;
            string loanExistsString = "";
			try
			{
                loanExistsString = m_dtInterface.IsLoanExistsInDataTrac(m_loginName, m_password, m_pathToDataTrac, dtLoanNumber);
                if (loanExistsString.StartsWith(ERROR_RESULT_PREFIX))
                {
                    DataTracException dtException = CreateDataTracExceptionFromResultXml(loanExistsString);
                    throw dtException;
                }
                else
                {
                    loanExists = bool.Parse(loanExistsString);
                }
            }
            catch (DataTracException dte)
            {
                throw dte;
            }
			catch(Exception e)
			{
                throw new DataTracException(GetStrippedSoapException(e), e);
			}

			return loanExists;
		}

        // Note - the DataTrac functions being called here refer to "export" because that is what they are in the DataTrac API,
        // since the API is written from DataTrac's perspective
        public string[] Import(string filepath, string dataTracLoanNumber, E_DataTracExportType exportType, ref ArrayList extraMappedFields, string borrowerSSN, XmlDocument gridRequestXml, XmlDocument importDataXML)
        {
            string[] result = new string[2] { "", "" };
            try
            {
                string sGridRequest = (gridRequestXml == null) ? "" : gridRequestXml.OuterXml;
                string sImportDataXML = (importDataXML == null) ? "" : importDataXML.OuterXml;
                if (m_webserviceVersion >= 1.6)
                {
                    result = m_dtInterface.LOSFileExport_V1_6(m_loginName, m_password, m_pathToDataTrac, exportType.ToString(), dataTracLoanNumber, filepath, borrowerSSN, sGridRequest, sImportDataXML);
                }
                else if (m_webserviceVersion >= 1.4)
                {
                    result = m_dtInterface.LOSFileExport_V1_4(m_loginName, m_password, m_pathToDataTrac, exportType.ToString(), dataTracLoanNumber, filepath, borrowerSSN, sGridRequest);
                }
                else
                {
                    result[0] = m_dtInterface.LOSFileExport(m_loginName, m_password, m_pathToDataTrac, exportType.ToString(), dataTracLoanNumber, filepath, borrowerSSN).ToString();
                }

                if (result[0].StartsWith(ERROR_RESULT_PREFIX))
                {
                    DataTracException dtException = CreateDataTracExceptionFromResultXml(result[0]);
                    throw dtException;
                }

                // Grab all the extra fields that do not exist in the exported file (along with their values)
                // and send back to LO so they can be set in an LO loan
                CreateExtraMappedFieldsRep(dataTracLoanNumber, ref extraMappedFields);
            }
            catch (DataTracException dte)
            {
                throw dte;
            }
            catch (Exception e)
            {
                throw new DataTracException(GetStrippedSoapException(e), e);
            }

            return result;
        }

        // OPM 25145
        public bool TestByCheckIfLoanExistsInDataTrac(string loanNumber)
        {
            if (string.IsNullOrEmpty(loanNumber))
                throw new DataTracException("Loan Number parameter cannot be empty.", "");

            string result = (string)m_dtInterface.IsLoanExistsInDataTrac(m_loginName, m_password, m_pathToDataTrac, loanNumber);

            if (result.StartsWith(ERROR_RESULT_PREFIX))
            {
                DataTracException dtException = CreateDataTracExceptionFromResultXml(result);
                throw dtException;
            }

            return bool.Parse(result);
        }
        
        public string GetProgramIdFromProgramName(string programName)
        {
            if ((programName == null) || programName.Trim().Equals(""))
                return "";

            string programIdResult = "";
            string sqlString = string.Format("SELECT programs_id FROM programs WHERE prog_name = '{0}'", programName);

            try
            {
                programIdResult = (string)m_dtInterface.GetProgramIdUsingSqlString(m_loginName, m_password, m_pathToDataTrac, sqlString, "recordset", null);

                if (programIdResult.StartsWith(ERROR_RESULT_PREFIX))
                {
                    DataTracException dtException = CreateDataTracExceptionFromResultXml(programIdResult);
                    throw dtException;
                }
            }
            catch (DataTracException dte)
            {
                throw dte;
            }
            catch (Exception e)
            {
                PaulBunyanHelper.Error("(db) Unable to get DataTrac program ID: " + GetStrippedSoapException(e));
                return "";
            }

            if (programIdResult.Trim().Equals(""))
                RecordErrorForUser("Warning: DataTrac loan program '" + programName + "' does not exist.", false);

            return programIdResult;
        }

        // Note - the DataTrac functions being called here refer to "import" because that is what they are in the DataTrac API,
        // since the API is written from DataTrac's perspective
        public void Export(string filepath, string dataTracLoanNumber, E_DataTracExportType exportType, bool forceAdd, ArrayList extraMappedFields, bool updateExistingLoan, XmlDocument gridFieldData, XmlDocument adjustmentData, XmlDocument exportDataXML)
        {
            string[] extraFields = CreateExtraMappedField1DArrayForDT(extraMappedFields);
            string sGridRequest = (gridFieldData == null) ? "" : gridFieldData.OuterXml;
            string sAdjustmentData = (adjustmentData == null) ? null : adjustmentData.OuterXml;
            string sExportDataXML = (exportDataXML == null) ? "" : exportDataXML.OuterXml;

            string traceMsg = string.Format("Exporting loan to DataTrac with DataTrac loan number '{0}'.  GridFieldData = {1}.  AdjustmentData = {2}.  ExportData = {3}.", dataTracLoanNumber, sGridRequest, sAdjustmentData, sExportDataXML);
            PaulBunyanHelper.Log(traceMsg);

            string result = "";
            try
            {
                if (m_webserviceVersion >= 1.6)
                    result = m_dtInterface.LOSFileImport3_V1_6(m_loginName, m_password, m_pathToDataTrac, exportType.ToString(), filepath, m_fileContent, dataTracLoanNumber, null, null, null, extraFields, m_programId, updateExistingLoan, sGridRequest, sAdjustmentData, sExportDataXML).ToString();
                else if(m_webserviceVersion >= 1.5)
                    result = m_dtInterface.LOSFileImport3_V1_5(m_loginName, m_password, m_pathToDataTrac, exportType.ToString(), filepath, m_fileContent, dataTracLoanNumber, null, null, null, extraFields, m_programId, updateExistingLoan, sGridRequest, sAdjustmentData).ToString();
                else if(m_webserviceVersion >= 1.4)
                    result = m_dtInterface.LOSFileImport3_V1_4(m_loginName, m_password, m_pathToDataTrac, exportType.ToString(), filepath, m_fileContent, dataTracLoanNumber, null, null, null, extraFields, m_programId, updateExistingLoan, sGridRequest).ToString();
                else
                    result = m_dtInterface.LOSFileImport3(m_loginName, m_password, m_pathToDataTrac, exportType.ToString(), filepath, m_fileContent, dataTracLoanNumber, null, null, null, extraFields, m_programId, updateExistingLoan).ToString();

                if (result != "")
                {
                    PaulBunyanHelper.Warning("Message returned from DataTrac export call is: " + result);
                    if (result.StartsWith(ERROR_RESULT_PREFIX))
                    {
                        DataTracException dtException = CreateDataTracExceptionFromResultXml(result);
                        throw dtException;
                    }
                    else if(result.StartsWith(NORMAL_RESULT_PREFIX))
                    {
                        // All is well, do nothing.
                    }
                    else
                    {
                        throw new DataTracException(ErrorFromDTExport_Prefix + result.Replace("\r\n", " ").Replace("\n", " "), "");
                    }
                }
            }
            catch (System.InvalidOperationException ioe)
            {
                throw new DataTracException("Unable to export to DataTrac - please contact us if this happens again.", ioe.ToString());
            }
            catch (DataTracException dte)
            {
                throw dte;
            }
            catch (Exception e)
            {
                throw new DataTracException(GetStrippedSoapException(e), e);
            }
        }
        #endregion

        public void WritePointDataToPath(string filePath, string pointData)
		{
			m_fileContent = pointData;
		}

        private string GetStrippedSoapException(Exception e)
        {
            string msg = e.Message;
            msg = msg.Replace("System.Web.Services.Protocols.SoapException: ", "");
            msg = msg.Replace("Server was unable to process request. --->", "");
            msg = msg.Replace("System.Exception: ", "");
            msg = msg.Replace("\r\n", "");
            msg = msg.Replace("\n", "");

            int index = msg.IndexOf("at DataTracInterface");
            if (index >= 0)
            {
                msg = msg.Substring(0, index);
            }

            return msg.Trim();
        }

        // Taken from LendersOffice LosUtils
        private static XmlDocument CreateXmlDoc(string xmlText)
        {
            XmlDocument xmlDoc = new XmlDocument();
            if (null == xmlText || xmlText.Trim() == "")
            {
                return xmlDoc;
            }

            XmlTextReader readerData = null;
            try
            {
                readerData = new XmlTextReader(new StringReader(xmlText));
                xmlDoc.Load(readerData);
            }
            finally
            {
                if (null != readerData)
                {
                    readerData.Close();
                }
            }
            return xmlDoc;
        }

        private static DataTracException CreateDataTracExceptionFromResultXml(string resultXml)
        {
            try
            {
                resultXml = resultXml.Replace(ERROR_RESULT_PREFIX, "");
                XmlDocument errorXml = CreateXmlDoc(resultXml);

                XmlNode messageNode = errorXml.SelectSingleNode("Result//Message");
                XmlNode stackTraceNode = errorXml.SelectSingleNode("Result//StackTrace");
                return new DataTracException(messageNode.InnerXml, stackTraceNode.InnerXml);
            }
            catch (Exception e)
            {
                PaulBunyanHelper.Error("Error while trying to create DataTracException.  The xml text used is: " + resultXml, e);
                return new DataTracException("DataTrac has encounted an error and may not have updated with all information.  Please contact us if this happens again.", "Unable to parse the following text when trying to create DataTracException: " + resultXml);
            }
        }
        
		private string[] CreateExtraMappedFieldNamesArray(ArrayList extraMappedFields)
		{
			string[] fieldNames = new string[extraMappedFields.Count];
			for(int i = 0; i < extraMappedFields.Count; ++i)
			{
				fieldNames[i] = ((IntegrationField)extraMappedFields[i]).ThirdPartyFieldName;
			}
			
			return fieldNames;
		}

		private void CreateExtraMappedFieldsRep(string dataTracLoanNumber, ref ArrayList extraMappedFields)
		{
            string[] fieldValues = m_dtInterface.GetFieldValues(m_loginName, m_password, m_pathToDataTrac, dataTracLoanNumber, CreateExtraMappedFieldNamesArray(extraMappedFields));

            if (fieldValues.Length == 0)
                return;
            
            if (fieldValues[0].StartsWith(ERROR_RESULT_PREFIX))
            {
                DataTracException dtException = CreateDataTracExceptionFromResultXml(fieldValues[0]);
                throw dtException;
            }

            if (fieldValues.Length != extraMappedFields.Count)
                throw new DataTracException("Unable to retrieve all data from DataTrac - some data may not have been updated.", "(db) - The number of extra mapped fields sent to the DataTrac webservice does not match the number returned back from it.  Fields sent: " + extraMappedFields.Count + ", fields returned: " + fieldValues.Length);
            
            for(int i = 0; i < extraMappedFields.Count; ++i)
			{
				IntegrationField field = (IntegrationField)extraMappedFields[i];
				field.ThirdPartyValue = fieldValues[i];
			}
		}

        private string[] CreateExtraMappedField1DArrayForDT(ArrayList extraMappedFields)
        {
            if (extraMappedFields.Count == 0)
                return null;

            string[] extraMappedFields1D = new string[extraMappedFields.Count];
            int cCount = 0;

            foreach (IntegrationField iField in extraMappedFields)
            {
                // For some reason, DataTrac throws an error when the program ID is passed in the normal
                // extra mapped fields object, but it works fine if it's manually mapped later, so that's what
                // we'll do for now, until DataTrac tells us why this behavior exists and how to fix it.

                if (iField.ThirdPartyFieldName == "gen.programs_id")
                    m_programId = iField.PmlValue;
                else
                {
                    string val = iField.ThirdPartyFieldName + "," + iField.PmlValue;
                    extraMappedFields1D[cCount++] = val;
                }
            }

            return extraMappedFields1D;
        }

		#region Record Messages
		// NOTE: These messages will get returned to the user 
		// Please use RecordMessage if you do not want the user to see the message
		private void RecordMessageForUser(string msg, bool logToPb)
		{
			m_UserMessages.Add(msg);
			if(logToPb)
				PaulBunyanHelper.Log(msg);
		}

		// NOTE: These messages will get returned to the user
		// Please use RecordError if you do not want the user to see the message
		private void RecordErrorForUser(string errorMsg, bool logToPb)
		{
			m_UserErrorMessages.Add(errorMsg);
			if(logToPb)
				PaulBunyanHelper.Error(errorMsg);
		}

		private void RecordError(string errorMsg, bool logToPb)
		{
			m_DevErrorMessages.Add(errorMsg);
			if(logToPb)
				PaulBunyanHelper.Error(errorMsg);
		}

		private void RecordMessage(string msg, bool logToPb)
		{
			m_DevMessages.Add(msg);
			if(logToPb)
				PaulBunyanHelper.Log(msg);
		}
		#endregion
	}
}