using System.Diagnostics;
using System;
using System.Web;
using PaulBunyan;

namespace DataTrac
{
	/// <summary>
	/// This class provides static methods that make PaulBunyan logging called easier.
	/// </summary>
	public class PaulBunyanHelper
	{
		private static PBLogger s_logger;

		private const string ComponentName = "DocMagic";
		public static PBLogger CreateLogger() 
		{
			PBLogger logger = new PBLoggerClass();
			logger.Component = ComponentName;

			return logger;
		}
		public static void LogContext(string context, string message) 
		{
			LogContext(context, "INFO", message);
		}

		public static void Log(string message) 
		{
			Log("INFO", message);
		}
		public static void Log(string category, string message) 
		{
			LogContext("", category, message);
		}
		public static void LogContext(string context, string category, string message) 
		{
			lock (typeof(PaulBunyanHelper)) 
			{
				if (null == s_logger)
					s_logger = CreateLogger();

				s_logger.Context = context;
				HttpContext httpContext = HttpContext.Current;
				if (httpContext != null) 
				{
					s_logger.File = httpContext.Request.Path;
				}

				s_logger.Log(category, message, 0);

			}
		}

		public static void Error( string msg, Exception exc )
		{
			Log("ERROR", string.Format( "Msg: {0}. Exception Details: {1}", msg, exc.ToString() ) );

		}
		public static void Error(Exception exc) 
		{
			Log("ERROR", exc.ToString());
		}

		public static void Error(string message) 
		{
			Log("ERROR", message);
		}

		public static void Warning(string msg, Exception exc )
		{
			Log("WARNING", string.Format( "Msg: {0}. Exception Details: {1}", msg, exc.ToString() ) );
		}
		public static void Warning(string message)
		{
			Log( "WARNING", message);
		}

		public static void Bug( string message)
		{
			Log( "BUG", message);
		}
		public static void RegTest( string message )
		{
			Log( "REGTEST", message );
		}
		public static void Error(string context, string message) 
		{
			LogContext(context, "ERROR", message);
		}

		public static void Verbose(string message) 
		{
			Log("VERBOSE", message);
		}
		public static void Verbose(string context, string message) 
		{
			LogContext(context, "VERBOSE", message);
		}


		/// <summary>
		/// There are time I want to log message in PB only for debugging purpose, but do not want to polute PB in production environment.
		/// This method will only log for DEBUG build and don't need to comment out trace statement when go to RELEASE build.
		/// </summary>
		[Conditional("DEBUG")]
		public static void Debug(string message) 
		{
			LogContext("DEBUG", "U0x2", message);
        
		}
	}
}
