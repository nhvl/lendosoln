using System;
using System.Threading;
using System.Collections;
using System.Data;
using System.Configuration;
using LendersOffice.Constants;
using System.Runtime.CompilerServices;
using DataAccess;
using System.Data.SqlClient;
using Toolbox.Distributed;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.RatePrice.FileBasedPricing;
using LendersOffice.Common;
using LendersOffice.Integration.ServiceMonitor;

namespace LpeRelease
{

    public class CController
    {
        public enum SnapshotThreadState { UnStarted, Alive, Finish };
        private class SnapshotCreatorThread
        {
            Thread m_thread;
            CController m_controller;
            string m_snapshotId;
            DateTime m_startTime;

            public SnapshotCreatorThread(string snapshotId)
            {
                m_snapshotId = snapshotId;
                m_controller = new CController(snapshotId);
                m_thread = new Thread(new ThreadStart(m_controller.Start));
                m_startTime = DateTime.Now;
                m_thread.Start();
            }

            [MethodImplAttribute(MethodImplOptions.Synchronized)]
            public void Abort()
            {

                if (m_thread != null)
                {
                    try
                    {
                        Tools.LogWarning(String.Format("<LpeRelease> There is an existing controller {0}, " +
                            "it's now aborted to start a new controller {0}.", m_snapshotId));
                        m_thread.Abort();
                    }
                    catch
                    {
                    }
                    finally
                    {
                        m_thread = null;
                        m_controller = null;
                    }
                }
            }

            public SnapshotThreadState State
            {
                get
                {
                    Thread thread = m_thread;
                    if (thread == null)
                        return SnapshotThreadState.Finish;

                    if (thread.IsAlive)
                        return SnapshotThreadState.Alive;

                    if ((thread.ThreadState & ThreadState.Unstarted) != 0)
                    {
                        TimeSpan duration = DateTime.Now - m_startTime;
                        if (duration.TotalSeconds > 45)
                        {
                            return SnapshotThreadState.Finish;
                        }

                        return SnapshotThreadState.UnStarted;
                    }

                    return SnapshotThreadState.Finish; // thread.ThreadState =  Abort or Stopped
                }

            }



        }


        #region STATIC MEMBERS

        private static SnapshotCreatorThread s_t1;
        private static SnapshotCreatorThread s_t2;
        public static double s_minDropOutOfDateSsAfterXMinutes = 6;
        public static double s_nDropOutOfDateSsAfterXMinutes = 7;
        public static double s_nDetectLpeSrcDataChangeAfterCurrentReleaseHasBeenInServiceForXMinutes = 23;
        public static double s_nTakeSsAfterCurrentReleaseHasBeenInServiceForXMinutes = 25;
        private static DateTime s_priceEngineVersionForLpeUpdate = DateTime.MinValue;

        private static Object s_lock = new Object();
        private static bool s_RequestSmokeTest = false;

        static string ReadConfigVar(string id)
        {
            string val = ConfigurationManager.AppSettings[id];
            if (val == null)
            {
                Tools.LogRegTest("<LpeRelease_Warning> web.config file doesn't contain the key " + id);
                return "";
            }
            return val;
        }

        static CController()
        {

            string minDestroyPoint = "";
            string destroyPoint = "";
            string createPoint = "";
            string releasePoint = "";

            try
            {
                minDestroyPoint = ReadConfigVar("MinDestroySnapshotPoint");
                destroyPoint = ReadConfigVar("DestroySnapshotPoint");
                createPoint = ReadConfigVar("CreateSnapshotPoint");
                releasePoint = ReadConfigVar("ReleaseSnapshotPoint");

                if (destroyPoint.Length == 0 || createPoint.Length == 0 || releasePoint.Length == 0)
                    return;

                int minDestroyTime = int.Parse(minDestroyPoint);
                int destroyTime = int.Parse(destroyPoint);
                int createTime = int.Parse(createPoint);
                int releaseTime = int.Parse(releasePoint);

                if (destroyTime < createTime && createTime <= releaseTime)
                {
                    s_minDropOutOfDateSsAfterXMinutes = minDestroyTime;
                    s_nDropOutOfDateSsAfterXMinutes = destroyTime;
                    s_nDetectLpeSrcDataChangeAfterCurrentReleaseHasBeenInServiceForXMinutes = createTime;
                    s_nTakeSsAfterCurrentReleaseHasBeenInServiceForXMinutes = releaseTime;
                }
                else
                    Tools.LogRegTest(string.Format("<LpeRelease_Warning>  we require " +
                                                    "( DestroySnapshotPoint = '{0}' < CreateSnapshotPoint = '{1}' <= ReleaseSnapshotPoint = '{2}'",
                                                    destroyPoint, createPoint, releasePoint));

            }
            catch (Exception exc)
            {
                Tools.LogError(string.Format("<LpeRelease_Warning>  web.config file contains at least one invalid key " +
                                              "( DestroySnapshotPoint = '{0}', CreateSnapshotPoint = '{1}', ReleaseSnapshotPoint = '{2}'",
                                              destroyPoint, createPoint, releasePoint),
                                exc);
            }
        }

        public static bool RequestSmokeTest
        {
            get
            {
                lock (s_lock) { return s_RequestSmokeTest; }
            }

            set
            {
                lock (s_lock)
                {
                    if (s_RequestSmokeTest != value)
                    {
                        Tools.LogRegTest("<SnapshotSmokeTest> The RequestSmokeTest' value is modified to the value " + value.ToString());
                        s_RequestSmokeTest = value;
                    }
                }
            }
        }

        public static DateTime PriceEngineVersionForLpeUpdate
        {
            get
            {
                lock (s_lock)
                {
                    return s_priceEngineVersionForLpeUpdate;
                }
            }
            set
            {
                lock (s_lock)
                {
                    if (s_priceEngineVersionForLpeUpdate < value)
                    {
                        DateTime priceEngineVersionDate = LpeTools.GetLpeDataLastModified(DataSrc.LpeSrc);
                        s_priceEngineVersionForLpeUpdate = (priceEngineVersionDate < value)
                                                            ? priceEngineVersionDate : value;
                        Tools.LogRegTest("FastReleaseSnapshotFor( priceEngineVersionDate = " + s_priceEngineVersionForLpeUpdate.ToString() + " )");

                    }
                }
            }
        }


        private static void DeclareNewVersion(string ssId, string ssName)
        {
            DateTime reportTime = DateTime.Now;
            ServiceMonitorServer monitor = new ServiceMonitorServer("LpeRelease");
            string monitorMessage = $"Snapshot Id: {ssId}, Snapshot Name: {ssName}";
            while (true)
            {
                try
                {
                    monitor.Record(StatusType.STARTED, monitorMessage);
                    Tools.LogRegTest($"<LpeRelease> {ssId}: Attempting to create Filebased Snapshot");

                    DataSrc dataSrc = CLpeReleaseInfo.GetDataSrcFrom(ssId);
                    DateTime dataLastModified = LpeTools.GetLpeDataLastModified(dataSrc);

                    var checksum = FileBasedPricingUtilities.CreateSnapshot(dataSrc, dataLastModified);

                    Tools.LogRegTest(string.Format("<LpeRelease> {0}: declaring new version now", ssId));

                    SqlParameter[] pars = new SqlParameter[] {
                                                                 new SqlParameter("@SnapshotId", ssId),
                                                                 new SqlParameter("@SnapshotName", ssName),
                                                                 new SqlParameter("@DataLastModifiedD", dataLastModified),
                                                                 new SqlParameter("@FileKey", checksum.Value)
                                                             };
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DeclareNewLpeVersion", 3, pars);
                    Tools.LogRegTest(string.Format("<LpeRelease> {0}: done declaring new version", ssId));
                    WriteLogIntoDb(ssId, string.Format("{0}: done declaring new version", ssId));
                    monitor.Record(StatusType.OK, monitorMessage);

                    return;
                }
                catch (ThreadAbortException)
                {
                    monitor.Record(StatusType.FAILED, monitorMessage);
                    throw;
                }
                catch (Exception ex)
                {
                    monitor.Record(StatusType.FAILED, monitorMessage);
                    string errMsg = string.Format("{0}: Failed to declare new version of {1}. Sleeping for 30 seconds and will try again", ssId, ssName);                    
                    Tools.LogErrorWithCriticalTracking(errMsg, ex);
                    if (DateTime.Now >= reportTime)
                    {
                        WriteLogIntoDb(ssId, errMsg);
                        reportTime = DateTime.Now.AddMinutes(2);
                    }
                    Thread.Sleep(30000);
                }
            }
        }

        private class HoldSingletonToolongException : CBaseException
        {
            public HoldSingletonToolongException(string errDevMsg) : base("Hold CSingletonDistributed too long", errDevMsg) { }
        }

        static string m_readSomeDataQuery = "SELECT top 2 * FROM LOAN_PRODUCT_FOLDER ; " +
            "SELECT top 2 * FROM LOAN_PROGRAM_TEMPLATE ; " +
            "SELECT top 2 * FROM PRICE_POLICY ; " +
            "SELECT top 2 * FROM PRODUCT_PRICE_ASSOCIATION ; " +
            "SELECT top 2 * FROM RATE_OPTIONS ; ";

        private static void ReadSomeDataFromSnapshot(DataSrc dataSrc)
        {
            LpeTools.GetLpeDataLastModified(dataSrc);
            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(dataSrc, ds, m_readSomeDataQuery, null, null);
        }

        private static void CreateSnapshot(string ssId, string ssName)
        {
            bool force = true;
            DateTime reportTime = DateTime.Now;
            while (true)
            {
                try
                {
                    // Wait for the singleton
                    using (CSingletonDistributed singleton = CSingletonDistributed.RequestSingleton(CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket, ssId, 30))
                    {
                        DateTime singletonStartD = DateTime.Now;

                        while (true)
                        {
                            Tools.LogRegTest(string.Format("<LpeRelease> {0}: Attempting to create snapshot {1} now", ssId, ssName));
                            Report(ssId, string.Format("<LpeRelease> {0}: Attempting to create snapshot {1} now", ssId, ssName), force, ref reportTime);
                            force = false;

                            // Create snapshot  within the same serializable transaction if it found a 30 seconds idle in data.
                            object obj = StoredProcedureHelper.ExecuteScalar(DataSrc.LpeSrc, "CreateLpeSsIfDataOldEnough",
                                new SqlParameter[] { new SqlParameter("@SnapshotName", ssName) });
                            if (obj == null || obj == DBNull.Value)
                            {
                                String strangeErrMsg = string.Format("<LpeRelease_Error> {0}: Strange thing happens in CController code, CreateLpeSsIfDataOldEnough store proc returns null", ssId);
                                Tools.LogError(strangeErrMsg);
                                WriteLogIntoDb(ssId, strangeErrMsg);
                                Thread.Sleep(60000); // sleeping for 60 seconds
                                continue;
                            }

                            int numberOfSecondsNeeded = (int)obj;

                            if (numberOfSecondsNeeded < 0)
                                throw new Exception(string.Format("{0}: Creating snapshot ran into error", ssId));
                            else if (numberOfSecondsNeeded > 0)
                            {
                                Report(ssId, string.Format("<LpeRelease> {0}: Data is dirty in db in the last 60 seconds, ss wasn't created. Need to sleep for {1} seconds.", ssId, numberOfSecondsNeeded.ToString()), force, ref reportTime);
                                Thread.Sleep(1000 * numberOfSecondsNeeded);

                                if (singletonStartD.AddMinutes(5).CompareTo(DateTime.Now) < 0)
                                {
                                    //singletonStartD = DateTime.MaxValue; // to avoid sending too many emails. Singleton feeding code already shows the total time.
                                    //Tools.LogErrorAndSendMail( string.Format( "{0}: this controller is holding singleton for more than 30 min.", ssId ) );
                                    throw new HoldSingletonToolongException(string.Format("{0}: this controller is holding singleton for more than 5 min.", ssId));
                                }
                                continue;
                            }

                            try
                            {
                                Thread.Sleep(10 * 1000); // sleep for new created snapshot is stable                                

                                Report(ssId, string.Format("<LpeRelease> {0}: try to read some data from {1}", ssId, ssName), true /*force*/, ref reportTime);
                                ReadSomeDataFromSnapshot(CLpeReleaseInfo.GetDataSrcFrom(ssId));
                                break;
                            }
                            catch (ThreadAbortException)
                            {
                                throw;
                            }
                            catch (Exception ex)
                            {
                                Tools.LogError(string.Format("<LpeRelease_Error> {0}: Although CreateLpeSsIfDataOldEnough was executed without error, the verfication process failed with an exception error. Sleeping for 10 seconds and try again.\r\nError:{1}\r\nStackTrace:{2}",
                                    ssId, ex.Message, ex.StackTrace));
                                WriteLogIntoDb(ssId, "Although CreateLpeSsIfDataOldEnough was executed without error, the verfication process failed with an exception error.");
                                Thread.Sleep(10 * 1000);
                            }
                        } // while( true )
                    } // using


                    // at here, we successly create new snapshot

                    Report(ssId, string.Format("<LpeRelease_Validate> {0}: try to load data from {1} now", ssId, ssName), true /*force*/, ref reportTime);

                    // OPM 11141 
                    ValidateLpeSnapshot validateSnapshotObj = new ValidateLpeSnapshot();
                    bool validatePolicyRel = false; // validatePolicyRel = RequestSmokeTest; 04/09/2008 access db too slow. Give up policy realtionship's validation.
                    validateSnapshotObj.Validate(CLpeReleaseInfo.GetDataSrcFrom(ssId), validatePolicyRel); // InvalidLpeSnapshotException can happend

                    DateTime dataModD = LpeTools.GetLpeDataLastModified(CLpeReleaseInfo.GetDataSrcFrom(ssId));
                    Tools.LogRegTest(string.Format("<LpeRelease> {0}: Snapshot {1} has been created and verified. Its data-last-modified-date is {2} on {3}{4}",
                                        ssId, ssName, dataModD.ToLongTimeString(), dataModD.ToShortDateString(),
                                        (validatePolicyRel == false ? ". Sleep 10 seconds" : ""))
                                    );
                    if (validatePolicyRel == false)
                        Thread.Sleep(10 * 1000);



                    return; // snapshot has been created

                } // try
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    if (ex is CLpeLoadingDeniedException)
                    {
                        Tools.LogRegTest(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 10 seconds. Error: {1} ", ssId, ex.Message));
                        Thread.Sleep(10 * 1000);
                    }
                    else if (ex is HoldSingletonToolongException)
                    {
                        Tools.LogRegTest(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 120 seconds. Error: {1} ", ssId, ex.Message));
                        Thread.Sleep(120 * 1000);
                    }
                    else
                    {
                        Tools.LogErrorWithCriticalTracking(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 1 min. ", ssId), ex);
                        Thread.Sleep(60000);
                    }
                }
            } // while( true )
        }

        private static string CreateSnapshotForDisableDropping(string ssId)
        {
            bool force = true;
            DateTime reportTime = DateTime.Now;
            string ssName = string.Empty;
            while (true)
            {
                try
                {
                    // Wait for the singleton
                    using (CSingletonDistributed singleton = CSingletonDistributed.RequestSingleton(CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket, ssId, 30))
                    {
                        DateTime singletonStartD = DateTime.Now;

                        while (true)
                        {
                            Report(ssId, $"<LpeRelease> {ssId}: Attempting to create snapshot now", force, ref reportTime);
                            force = false;

                            // Todo: the store procedure CreateLpeSsIfDataOldEnough return snapshot name.
                            ssName = $"{ssId}LendersOfficeLpe_{Tools.GetDBCurrentDateTime():yyyyMMdd_HHmmss}"; 

                            // Create snapshot  within the same serializable transaction if it found a 30 seconds idle in data.
                            object obj = StoredProcedureHelper.ExecuteScalar(DataSrc.LpeSrc, "CreateLpeSsIfDataOldEnough",
                                new SqlParameter[] { new SqlParameter("@SnapshotName", ssName) });
                            if (obj == null || obj == DBNull.Value)
                            {
                                String strangeErrMsg = string.Format("<LpeRelease_Error> {0}: Strange thing happens in CController code, CreateLpeSsIfDataOldEnough store proc returns null", ssId);
                                Tools.LogError(strangeErrMsg);
                                WriteLogIntoDb(ssId, strangeErrMsg);
                                Thread.Sleep(60000); // sleeping for 60 seconds
                                continue;
                            }

                            int numberOfSecondsNeeded = (int)obj;

                            if (numberOfSecondsNeeded < 0)
                                throw new Exception(string.Format("{0}: Creating snapshot ran into error", ssId));
                            else if (numberOfSecondsNeeded > 0)
                            {
                                Report(ssId, string.Format("<LpeRelease> {0}: Data is dirty in db in the last 60 seconds, ss wasn't created. Need to sleep for {1} seconds.", ssId, numberOfSecondsNeeded.ToString()), force, ref reportTime);
                                Thread.Sleep(1000 * numberOfSecondsNeeded);

                                if (singletonStartD.AddMinutes(5).CompareTo(DateTime.Now) < 0)
                                {
                                    //singletonStartD = DateTime.MaxValue; // to avoid sending too many emails. Singleton feeding code already shows the total time.
                                    //Tools.LogErrorAndSendMail( string.Format( "{0}: this controller is holding singleton for more than 30 min.", ssId ) );
                                    throw new HoldSingletonToolongException(string.Format("{0}: this controller is holding singleton for more than 5 min.", ssId));
                                }
                                continue;
                            }

                            try
                            {
                                Thread.Sleep(10 * 1000); // sleep for new created snapshot is stable                                

                                Report(ssId, string.Format("<LpeRelease> {0}: try to read some data from {1}", ssId, ssName), true /*force*/, ref reportTime);

                                DbAccessUtils.SetLpeDbSnapshotName(CLpeReleaseInfo.GetDataSrcFrom(ssId), ssName); // 5/7/2017 Thien: important
                                ReadSomeDataFromSnapshot(CLpeReleaseInfo.GetDataSrcFrom(ssId));
                                break;
                            }
                            catch (ThreadAbortException)
                            {
                                throw;
                            }
                            catch (Exception ex)
                            {
                                Tools.LogError(string.Format("<LpeRelease_Error> {0}: Although CreateLpeSsIfDataOldEnough was executed without error, the verfication process failed with an exception error. Sleeping for 10 seconds and try again.\r\nError:{1}\r\nStackTrace:{2}",
                                    ssId, ex.Message, ex.StackTrace));
                                WriteLogIntoDb(ssId, "Although CreateLpeSsIfDataOldEnough was executed without error, the verfication process failed with an exception error.");
                                Thread.Sleep(10 * 1000);
                            }
                        } // while( true )
                    } // using


                    // at here, we successly create new snapshot

                    Report(ssId, string.Format("<LpeRelease_Validate> {0}: try to load data from {1} now", ssId, ssName), true /*force*/, ref reportTime);

                    // OPM 11141 
                    ValidateLpeSnapshot validateSnapshotObj = new ValidateLpeSnapshot();
                    bool validatePolicyRel = false; // validatePolicyRel = RequestSmokeTest; 04/09/2008 access db too slow. Give up policy realtionship's validation.
                    validateSnapshotObj.Validate(CLpeReleaseInfo.GetDataSrcFrom(ssId), validatePolicyRel); // InvalidLpeSnapshotException can happend

                    DateTime dataModD = LpeTools.GetLpeDataLastModified(CLpeReleaseInfo.GetDataSrcFrom(ssId));
                    Tools.LogRegTest(string.Format("<LpeRelease> {0}: Snapshot {1} has been created and verified. Its data-last-modified-date is {2} on {3}{4}",
                                        ssId, ssName, dataModD.ToLongTimeString(), dataModD.ToShortDateString(),
                                        (validatePolicyRel == false ? ". Sleep 10 seconds" : ""))
                                    );
                    if (validatePolicyRel == false)
                        Thread.Sleep(10 * 1000);



                    return ssName; // snapshot has been created

                } // try
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    if (ex is CLpeLoadingDeniedException)
                    {
                        Tools.LogRegTest(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 10 seconds. Error: {1} ", ssId, ex.Message));
                        Thread.Sleep(10 * 1000);
                    }
                    else if (ex is HoldSingletonToolongException)
                    {
                        Tools.LogRegTest(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 120 seconds. Error: {1} ", ssId, ex.Message));
                        Thread.Sleep(120 * 1000);
                    }
                    else
                    {
                        Tools.LogErrorWithCriticalTracking(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 1 min. ", ssId), ex);
                        Thread.Sleep(60000);
                    }
                }
            } // while( true )
        }

        public static void ShutdownControllers()
        {
            lock (s_lock)
            {
                if (s_t1 != null)
                {
                    s_t1.Abort();
                    s_t1 = null;
                }

                if (s_t2 != null)
                {
                    s_t2.Abort();
                    s_t2 = null;
                }
            }
        }


        public static SnapshotThreadState ControllersState
        {
            get
            {
                lock (s_lock)
                {
                    if (s_t1 == null || s_t2 == null ||
                        s_t1.State == SnapshotThreadState.Finish ||
                        s_t2.State == SnapshotThreadState.Finish)
                    {
                        return SnapshotThreadState.Finish;
                    }

                    if (s_t1.State == SnapshotThreadState.Alive ||
                        s_t2.State == SnapshotThreadState.Alive)
                    {
                        return SnapshotThreadState.Alive;
                    }

                    return SnapshotThreadState.UnStarted;
                }

            }
        }

        public static void Wakeup()
        {
            if (ControllersState == SnapshotThreadState.Finish)
                CreateControllers();
        }

        public static void CreateControllers()
        {
            lock (s_lock)
            {
                CreateControllersWithoutLock();
            }
        }

        public static void CreateControllersWithoutLock()
        {
            WriteLogIntoDb("LpeRelease", "Call LpeRelease.CController.CreateControllers() at " + DateTime.Now);

            bool enableDroppingDbSnapshot = ConstStage.EnableDroppingDbSnapshot;

            #region Create the first release if there isn't one
            CLpeReleaseInfo latestRelease = CLpeReleaseInfo.GetLatestReleaseInfo();
            if (latestRelease == null)
            {	// This is the case when there hasn't been any snapshot created.

                Tools.LogWarning("<LpeRelease> No lpe db snapshot detected, attemp to create one now...");

                string snapshotId = enableDroppingDbSnapshot ? ConstAppThinh.LpeSnapshot1Id : ConstAppThinh.LpeUndropSnapshot1Id;
                string snapshotName = CLpeReleaseInfo.GetSnapshotNameFrom(snapshotId);
                //DataSrc dataSrc = CLpeReleaseInfo.GetDataSrcFrom(snapshotId);

                if (enableDroppingDbSnapshot)
                {
                    CreateSnapshot(snapshotId, snapshotName);
                    Tools.LogWarning(string.Format("<LpeRelease> {0}: 1st Lpe Db snapshot {1} has been successfully created.", snapshotId, snapshotName));
                    DeclareNewVersion(snapshotId, snapshotName);
                }
                else
                {
                    snapshotName = CreateSnapshotForDisableDropping(snapshotId);
                    Tools.LogWarning(string.Format("<LpeRelease> {0}: 1st Lpe Db snapshot {1} has been successfully created.", snapshotId, snapshotName));
                    DeclareNewVersion(snapshotName, snapshotName);
                }


            } // if( latestRelease == null )

            #endregion // Create the first release if there isn't one

            // Terminating existing controllers
            ShutdownControllers();

            #region Launching controllers

            try
            {
                s_t1 = new SnapshotCreatorThread(enableDroppingDbSnapshot ? ConstAppThinh.LpeSnapshot1Id : ConstAppThinh.LpeUndropSnapshot1Id);
            }
            catch (Exception ex)
            {
                Tools.LogErrorWithCriticalTracking("<LpeRelease_Exception> Encountered this error when starting the controller 1. Stop now. Won't start controller 2", ex);
                return;
            }

            try
            {
                s_t2 = new SnapshotCreatorThread(enableDroppingDbSnapshot ? ConstAppThinh.LpeSnapshot2Id : ConstAppThinh.LpeUndropSnapshot2Id);
            }
            catch (Exception ex)
            {
                s_t2 = null;
                Tools.LogErrorWithCriticalTracking("<LpeRelease_Exception> Encountered this error when starting the controller 2. Stopping controller 1 now", ex);
                s_t1.Abort();
                s_t1 = null;
            }
            #endregion // Launching controllers
        }

        #endregion // STATIC MEMBERS

        private string m_ssId; // snapshotId
        private string m_ssName; // snapshot name
        private DataSrc m_ssDataSrc;
        private string m_controllerInstanceId;
        private DateTime m_controllerBornDate = DateTime.Now;
        private CLpeReleaseInfo m_latestRelease = null;
        private int m_tokenId;
        private bool m_enableDroppingSnapshot; 

        static int GetTokenFrom(string snapshotId, out bool enableDroppingSnapshot)
        {
            // token 0 ,1 for ConstStage.EnableDroppingDbSnapshot = true
            // token 2, 3 for ConstStage.EnableDroppingDbSnapshot = false

            switch (snapshotId)
            {
            case ConstAppThinh.LpeSnapshot1Id:
                enableDroppingSnapshot = true;
                return 0;

            case ConstAppThinh.LpeSnapshot2Id:
                enableDroppingSnapshot = true;
                return 1;

            default:
                enableDroppingSnapshot = false;
                if (snapshotId.StartsWith(ConstAppThinh.LpeUndropSnapshot1Id /* "ss01" */) )
                {
                    return 2;
                }

                if (snapshotId.StartsWith(ConstAppThinh.LpeUndropSnapshot2Id /* "ss02" */))
                {
                    return 3;
                }

                string errMsg = string.Format("{0} is not a recognizable snapshot id", snapshotId);
                Tools.LogErrorWithCriticalTracking(errMsg);
                throw new CBaseException(ErrorMessages.Generic, errMsg);
            }

        }

        private CController(string snapshotId)
        {
            m_ssId = snapshotId;
            m_ssName = CLpeReleaseInfo.GetSnapshotNameFrom(m_ssId);
            m_ssDataSrc = CLpeReleaseInfo.GetDataSrcFrom(m_ssId);
            m_tokenId = GetTokenFrom(m_ssId, out m_enableDroppingSnapshot);

            CPmlFIdGenerator idG = new CPmlFIdGenerator();
            m_controllerInstanceId = idG.GenerateNewFriendlyId();
            Tools.LogRegTest(string.Format("<LpeRelease> Creating controller {0} with instance id {1}", m_ssId, m_controllerInstanceId));
        }

        internal string ControllerInstanceId
        {
            get { return m_controllerInstanceId; }
        }



        void MySleep(double minutesTimeout, double minSleepInMinutes)
        {
            const double MilisecondsPerMinute = 60 * 1000;
            if (minutesTimeout <= 0)
                return;
            if (minSleepInMinutes < 0)
                minSleepInMinutes = 0;

            string snapshotId = m_ssId;
            DateTime reportTime = DateTime.Now.AddMinutes(5);

            if (RequestSmokeTest)
            {
                Tools.LogRegTest(string.Format("<SnapshotSmokeTest> {0} doesn't need to sleep {1} minutes.", snapshotId, minutesTimeout.ToString("N2")));
                return;
            }

            DateTime target = DateTime.Now.AddMinutes(minutesTimeout);
            DateTime minTarget = DateTime.Now.AddMinutes(minSleepInMinutes);
            while (true)
            {
                int remainMilliseconds = 1 + (int)(target - DateTime.Now).TotalMilliseconds;
                if (remainMilliseconds <= 0)
                    return;

                if (RequestSmokeTest)
                {
                    Tools.LogRegTest(string.Format("<SnapshotSmokeTest> Unsafe Release Snapshot Process *** {0}  doesn't need to sleep {1} minutes.",
                                      snapshotId, (remainMilliseconds / MilisecondsPerMinute).ToString("N2")));
                    return;
                }


                if (m_latestRelease.DataLastModifiedD < PriceEngineVersionForLpeUpdate)
                {
                    if (minSleepInMinutes == 0)
                    {
                        Tools.LogRegTest(string.Format("<LpeRelease> {0} doesn't need to sleep {1} minutes.",
                            snapshotId, (remainMilliseconds / MilisecondsPerMinute).ToString("N2")));
                        return;
                    }
                    else if (minTarget < target)
                    {
                        target = minTarget;
                        remainMilliseconds = 1 + (int)(target - DateTime.Now).TotalMilliseconds;
                        Tools.LogRegTest(string.Format("<LpeRelease> Fast Relese Snapshot Process *** {0} SleepCountDown : {1} minutes ", snapshotId,
                                                        (remainMilliseconds / MilisecondsPerMinute).ToString("N2")));

                    }
                }

                if (reportTime < DateTime.Now)
                {
                    WriteLogIntoDb(snapshotId, string.Format("<LpeRelease> {0} SleepCountDown : {1} minutes.", snapshotId,
                                                     (remainMilliseconds / MilisecondsPerMinute).ToString("N2")));
                    reportTime = DateTime.Now;
                }

                remainMilliseconds = 1 + (int)(target - DateTime.Now).TotalMilliseconds;
                if (remainMilliseconds <= 0)
                    return;


                if (remainMilliseconds > 20 * 1000)
                    remainMilliseconds = 20 * 1000;

                Thread.Sleep(remainMilliseconds); // Sleep 20 second
            }
        }

        void MySleep(double minutesTimeout)
        {
            MySleep(minutesTimeout, 0);
        }
        //IF THE DATASET FOR THIS CONTROLER IS STILL IN SERVICE, KEEP WAITING
        CLpeReleaseInfo ServiceData()
        {
            while (true)
            {
                try
                {
                    CLpeReleaseInfo latestRelease = CLpeReleaseInfo.GetLatestReleaseInfo();
                    if (latestRelease == null)
                    {
                        string errMsg = string.Format("<LpeRelease_Error> {0}: Unexpected situation. Expected to have a release version declared at here", m_ssId);
                        Tools.LogErrorWithCriticalTracking(errMsg);
                        throw new Exception(errMsg);
                    }

                    if (NeedToWatchForCreateNewSnapshot(latestRelease) )
                    {
                        Tools.LogRegTest(string.Format("<LpeRelease> {0} is detected that it's not the latest release.", m_ssId));
                        return latestRelease; // time to go to the next stage.
                    }

                    Thread.Sleep(3000); // this data set is in service, sleep for another 3 seconds and check again.
                } // try
                catch (ThreadAbortException ex)
                {
                    Tools.LogError($"<LpeRelease_Exception> {m_ssId}: Thread was being aborted.", ex);
                    throw;
                }
                catch (Exception ex)
                {
                    Tools.LogError(string.Format("<LpeRelease_Exception> {0}: Will try again 5 seconds.", m_ssId), ex);
                    Thread.Sleep(5000);
                }
            } // while( true )
        }

        bool NeedToWatchForCreateNewSnapshot(CLpeReleaseInfo latestRelease)
        {
            // token 0 ,1 for ConstStage.EnableDroppingDbSnapshot = true
            // token 2, 3 for ConstStage.EnableDroppingDbSnapshot = false

            bool latestEnableDrop;
            int latestToken = GetTokenFrom(latestRelease.SnapshotId, out latestEnableDrop);

            if (latestEnableDrop == m_enableDroppingSnapshot)
            {
                if (latestToken != m_tokenId)
                {
                    // "The snapshot for lastestToken" in service
                    // m_tokenId needs to prepare for new snapshot
                    return true;
                }
            }
            else
            {
                //CLpeReleaseInfo.GetLatestReleaseInfo() associating with some snapshot that not matching with ConstStage.EnableDroppingSnapshot.
                // The first token for ConstStage.EnableDroppingDbSnapshot needs to watch data for make new snapshot
                int firstToken = m_enableDroppingSnapshot ? 0 : 2;
                if(m_tokenId == firstToken)
                {
                    return true;
                }
            }

            return false;
        }

        bool IsFastRelease()
        {
            return RequestSmokeTest == false && (m_latestRelease.DataLastModifiedD < PriceEngineVersionForLpeUpdate);
        }
        void PreprocessBeforeDropSnapshot() // Make sure That SNAPSHOT IS OUT-OF-DATE FOR 5 MIN OR MORE
        {
            if(!m_enableDroppingSnapshot)
            {
                return;
            }

            // Basically this is ( 5 Min - Release Age )

            //double minuteAge              = m_latestRelease.ReleaseAge.TotalMinutes;
            double minuteAgeSpecified = s_nDropOutOfDateSsAfterXMinutes;
            double nSleep = minuteAgeSpecified - m_latestRelease.ReleaseAge.TotalMinutes;
            double minSleep = s_minDropOutOfDateSsAfterXMinutes - m_latestRelease.ReleaseAge.TotalMinutes;
            if (nSleep > 0)
            {
                Report(string.Format("<LpeRelease> {0}: the other dataset is not in service for more than {3} min " +
                                       "so this controller will sleep for {1} min before dropping snapshot {2}",
                                       m_ssId, nSleep.ToString("N2"), m_ssName, minuteAgeSpecified));
                MySleep(nSleep, minSleep);

            } // if( minuteAge < 5 )

            while ((minuteAgeSpecified - m_latestRelease.ReleaseAge.TotalMinutes) > 0)
            {
                // the method MySleep doesn't sleep enough because someone as LpeUpdate requested to create new snapshot asap.

                try
                {
                    // returning the # of seconds left to reach the required age
                    int numberOfSecondsNeeded = (int)StoredProcedureHelper.ExecuteScalar(DataSrc.LpeSrc, "DataOldEnough");
                    if (numberOfSecondsNeeded <= 0)
                        break;

                    Report(string.Format("<LpeRelease> {0} : someone requested to create snapshot ASAP but data is dirty in db in the last 60 seconds [PreprocessBeforeDropSnapshot]",
                        m_ssId), false);
                    Thread.Sleep(1000 * numberOfSecondsNeeded);
                }
                catch (Exception exc)
                {
                    Tools.LogError("<LpeRelease> PreprocessBeforeDropSnapshot", exc);
                    Thread.Sleep(5 * 1000);
                }

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>returns true if succeeded, return false if not</returns>
        private void DropSnapshot()
        {
            if (!m_enableDroppingSnapshot)
            {
                return;
            }

            Tools.LogRegTest(string.Format("<LpeRelease> {0}: dropping snapshot with name {1}", m_ssId, m_ssName));

            for (int i = 0; i < 3; ++i)
            {
                try
                {
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "DropDbSnapshot", 1,
                        new SqlParameter[] { new SqlParameter("@SnapshotName", m_ssName) });

                    // Verifying the drop, if it has been dropped successfully, it should be no longer accessible, we should get an SqlException when trying to get some data.
                    try
                    {
                        DateTime lastModD = LpeTools.GetLpeDataLastModified(m_ssDataSrc);
                        string errorMsg = string.Format("{0}: Drop-verification shows that snapshot {1} didn't get dropped. It's still accessible. Its DataLastModifiedD is {2} on {3}",
                            lastModD.ToLongTimeString(), lastModD.ToLongDateString());
                        throw new Exception(errorMsg);
                    }
                    catch (Exception ex)
                    {
                        if (ex is SqlException)
                        {
                            //Tools.LogRegTest( string.Format( "{0}: Verified that snapshot name {1} had been dropped.", m_ssId, m_ssName ) );
                            Report(string.Format("<LpeRelease> {0}: Verified that snapshot name {1} had been dropped.", m_ssId, m_ssName));
                            return;
                        }
                        throw ex;
                    }
                }
                catch (ThreadAbortException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    Tools.LogWarning(string.Format("<LpeRelease_Exception> {0}: exception encountered during dropping snapshot {1}. Sleep for 5 seconds and retry", m_ssId, m_ssName), ex);
                    Thread.Sleep(5000);
                }
            }

            string errMsg = String.Format("<LpeRelease_Error> {0}: Critial error encountered. Failed to drop snapshot {1}", m_ssId, m_ssName);
            Tools.LogErrorWithCriticalTracking(errMsg);
            WriteLogIntoDb(m_ssId, errMsg);

            throw new Exception(errMsg);
        }


        private void Start()
        {
            Tools.LogRegTest(string.Format("<LpeRelease> Controller {0} has been started", m_ssId));
            try
            {
                while (true)
                {
                    try
                    {
                        m_latestRelease = ServiceData(); // IF THE DATASET FOR THIS CONTROLER IS STILL IN SERVICE, KEEP WAITING			
                        PreprocessBeforeDropSnapshot(); // Make sure that LATEST SNAPSHOT IS OUT-OF-DATE FOR 5 MIN OR MORE
                        DropSnapshot();

                        // At this point, the current release is the other dataset

                        #region Waiting for the latest release version of the other dataset to be out for more than 10 min

                        DateTime now = DateTime.Now; // To be accurate, we have to get the time from sql server, but not neccessary and not worth the perf hit on sql server.

                        // If( the latest release version been out for less than 10 min )
                        double minutesAge = m_latestRelease.ReleaseAge.TotalMinutes;
                        double minutesAgeSpecified = s_nDetectLpeSrcDataChangeAfterCurrentReleaseHasBeenInServiceForXMinutes;
                        if (minutesAge < minutesAgeSpecified)
                        {
                            double nSleep = minutesAgeSpecified - minutesAge;
                            Report(string.Format("<LpeRelease> {0}: the other dataset's been in service for less than {2} min so this controller will sleep for {1} minutes more",
                                m_ssId, nSleep.ToString("N1"), minutesAgeSpecified));
                            //Thread.Sleep( new TimeSpan( 0, nSleep, 0 ) );
                            MySleep(nSleep);
                        } // if 
                        #endregion //  Waiting for the latest release version of the other dataset to be out for more than 10 min

                        #region Waiting for Lpe data to be different from the current release
                        while (true)
                        {
                            DateTime srcModD = LpeTools.GetLpeDataLastModified(DataSrc.LpeSrc);

                            DateTime ssDataModD = m_latestRelease.DataLastModifiedD;

                            int compVal = ssDataModD.CompareTo(srcModD);
                            if (compVal == 0)
                            {
                                Report(string.Format("<LpeRelease> {0}: both latest release and src data have data with last modifed date {1} on {2}, will sleep for 30 seconds and check again.",
                                    m_ssId, ssDataModD.ToLongTimeString(), ssDataModD.ToShortDateString()), false);
                                Thread.Sleep(30 * 1000); // waiting for 20 seconds and check again
                            }
                            else if (compVal < 0)
                            {
                                Report(string.Format("<LpeRelease> {0}: latest release has data with last modifed date {1} on {2} but the source data was changed at {3} on {4}.  Need to make prepare for a new release.",
                                    m_ssId, ssDataModD.ToLongTimeString(), ssDataModD.ToShortDateString(), srcModD.ToLongTimeString(), srcModD.ToShortDateString()));
                                break; // Data has been changed, time to prepare for another version release
                            }
                            else
                            {
                                string errMsg = string.Format("{0}: encountered unexpected situation. Data in snapshot {1} {2} is more recent than data in source. LastModifiedD is {3} {4}",
                                                                m_ssId,
                                                                ssDataModD.ToLongTimeString(), ssDataModD.ToShortDateString(),
                                                                srcModD.ToLongTimeString(), srcModD.ToShortDateString());
                                WriteLogIntoDb(m_ssId, errMsg);
                                throw new Exception(errMsg);

                            }
                        } // while( true )
                        #endregion // Waiting for Lpe data to be different from the current release

                        string newSnapshotName = m_ssName;

                        if (m_enableDroppingSnapshot)
                        {
                            CreateSnapshot(m_ssId, m_ssName);
                        }
                        else
                        {
                            newSnapshotName = CreateSnapshotForDisableDropping(m_ssId);
                        }

                        #region Wiring - NO OP AT THIS POINT
                        // We want to do the wiring while holding the singleton to prevent db from having to maintain snapshot with many I/O operations
                        // FUTURE: Thien call the wiring code here. We don't need to do wiring for the first release
                        #endregion // Wiring

                        #region Waiting for the current release (of other dataset) to be at least 25 min
                        minutesAge = m_latestRelease.ReleaseAge.TotalMinutes; // getting the new age
                        minutesAgeSpecified = s_nTakeSsAfterCurrentReleaseHasBeenInServiceForXMinutes;
                        double nWait = (minutesAgeSpecified - minutesAge);

                        if (nWait > 0)
                        {
                            Report(string.Format("<LpeRelease> {0}: Going to sleep to  wait for the current release (of other dataset) for another {1} minutes.",
                                                    m_ssId, nWait.ToString("N1")));
                            //Thread.Sleep( new TimeSpan( 0, nWait, 0 ) ); 
                            MySleep(nWait);
                        }
                        #endregion // Waiting for the current release (of other dataset) to be at least 25 min


                        if (m_enableDroppingSnapshot)
                        {
                            DeclareNewVersion(m_ssId, m_ssName);
                        }
                        else
                        {
                            DeclareNewVersion(newSnapshotName, newSnapshotName);
                        }

                        if (RequestSmokeTest)
                        {
                            Tools.LogRegTest("<SnapshotSmokeTest> suscessly create snapshot " + m_ssId);
                            RequestSmokeTest = false;

                        }

                    } // try
                    catch (Exception ex)
                    {
                        if ((ex as System.Threading.ThreadAbortException) != null)
                            throw;
                        Tools.LogErrorWithCriticalTracking(string.Format("<LpeRelease_Exception> {0} with instance id {1} created on {2}: fatal error is encountered. Unless the thread is aborted, keep looping...", m_ssId, m_controllerInstanceId, m_controllerBornDate.ToShortDateString() + ":" + m_controllerBornDate.ToShortTimeString()), ex);
                    }
                } // while( true )
            }
            catch (Exception ex)
            {
                string errMsg = string.Format("<LpeRelease_Exception> {0} with instance id {1} created on {2}: fatal error is encountered. It's exiting now.", m_ssId, m_controllerInstanceId, m_controllerBornDate.ToShortDateString() + ":" + m_controllerBornDate.ToShortTimeString());
                if ((ex as System.Threading.ThreadAbortException) != null)
                {
                    Tools.LogError(errMsg, ex);
                    throw;  // 2/09/2019 Thien: we don't need "rethrow" because this function Start() will finish soon => thread will terminat.
                            // However I write this line for easy read only.
                }
                else
                    Tools.LogErrorWithCriticalTracking(errMsg, ex);

            }

        } // end of Start() method

        const int s_reportIntervalInSeconds = 90;
        DateTime m_reportTime = DateTime.Now.AddSeconds(s_reportIntervalInSeconds);

        void Report(string msg)
        {
            Report(msg, true);
        }

        void Report(string msg, bool force)
        {
            Report(m_ssId, msg, force, ref m_reportTime);
        }


        static void Report(string ssId, string msg, bool force, ref DateTime reportTime)
        {
            Tools.LogRegTest(msg);
            if (force == false && DateTime.Now < reportTime)
                return;

            if (WriteLogIntoDb(ssId, msg))
                reportTime = DateTime.Now.AddSeconds(s_reportIntervalInSeconds);
        }

        static bool WriteLogIntoDb(string snapshotId, string msg)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@SnapshotId", snapshotId),
                                            new SqlParameter("@Msg", msg)
                                        };
            try
            {
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LpeReleaseLog_InsertMsg", 3, parameters);
                return true;
            }
            catch
            {
                return false;
            }


        }
    } // end of class CController
} // namespace LpeRelease

namespace LendersOfficeApp.los.RatePrice
{

    public class InvalidLpeSnapshotException : CBaseException
    {
        public override string EmailSubjectCode
        {
            get { return "InvalidLpeSnapshotException"; }
        }

        public InvalidLpeSnapshotException(string message)
            : base("Invalid LPE snapshot", message)
        {

        }

    }


    // OPM 11141
    public class ValidateLpeSnapshot
    {
        string m_snapshotInfo = "";
        DataSrc m_dataSrc;
        DateTime m_versionD;
        PolicyRelationCenter m_policyRelationDb;
        Hashtable m_rateSheetIds = new Hashtable();
        Hashtable m_enableBorkers = new Hashtable();
        ArrayList m_programs = new ArrayList();

        bool m_validatePolicyRelationShip = true;



        void Init(DataSrc dataSrc)
        {
            m_dataSrc = dataSrc;
            m_snapshotInfo = dataSrc.ToString();
            m_versionD = LpeTools.GetLpeDataLastModified(dataSrc);

            m_snapshotInfo = string.Format("{0}  {1} {2}", dataSrc,
                m_versionD.ToShortTimeString(), m_versionD.ToShortDateString());

            m_policyRelationDb = null;
            m_rateSheetIds.Clear();
            m_enableBorkers.Clear();
            m_programs.Clear();

            LpeTools.ListLpeEnabledBrokers(m_enableBorkers);
        }


        public bool Validate_1(DataSrc dataSrc)
        {
            return Validate(dataSrc, true /* validatePolicyRelationShip */);
        }


        public bool Validate(DataSrc dataSrc, bool validatePolicyRelationShip)
        {
            m_validatePolicyRelationShip = validatePolicyRelationShip;
            m_snapshotInfo = dataSrc.ToString();
            try
            {
                Init(dataSrc);
                CreatePolicyRelationCenter();
                LoadRateSheetIds();
                LoadProgramIds();

                Tools.LogRegTest(string.Format("<LpeRelease_Validate> the snapshot {0} has {1} programs, {2} rateSheets, {3} policyies. ValidatePolicyRelationShip = {4}",
                    m_snapshotInfo, m_programs.Count.ToString("N0"), m_rateSheetIds.Count.ToString("N0"), m_policyRelationDb.Count.ToString("N0"), m_validatePolicyRelationShip));

                ArrayList ids = new ArrayList();
                const int blockSize = 200;
                for (int i = 0; i < m_programs.Count; i++)
                {
                    ids.Add(m_programs[i]);
                    if (ids.Count == blockSize)
                    {
                        ValidateLoadPrograms(ids);
                        ids.Clear();
                    }
                }

                if (ids.Count > 0)
                    ValidateLoadPrograms(ids);

                Tools.LogRegTest(string.Format("<LpeRelease_Validate> {0} : success", m_snapshotInfo));

                return true;
            }
            catch (Exception exc)
            {
                Tools.LogError("<LpeRelease_Error> " + m_snapshotInfo, exc);
                return false;
            }
        }

        void LoadProgramIds()
        {
            Tools.LogRegTest(string.Format("<LpeRelease_Validate> {0} : LoadProgramIds", m_snapshotInfo));

            // 11/21/2007 ThienNguyen - Reviewed and safe
            string sql = "SELECT lLpTemplateId\n" +
                "    FROM Loan_Program_Template\n" +
                "    WHERE IsMaster = 0 and IsEnabled = 1 and IsLpe = 1  "
                + " and BrokerId " + DbTools.InClauseForSql(m_enableBorkers.Keys);

            CDataSet ds = new CDataSet(60);
            ds.LoadWithSqlQueryDangerously(m_dataSrc, sql);

            m_programs.Clear();
            foreach (DataRow row in ds.Tables[0].Rows)
                m_programs.Add(row["lLpTemplateId"]);
        }

        void ValidateLoadPrograms(ArrayList prgIds)
        {
            // 11/21/2007 ThienNguyen - Reviewed and safe
            // get loan programs
            string sql = "SELECT lLpTemplateId, SrcRateoptionsProgId\n" +
                "    FROM Loan_Program_Template\n" +
                "    WHERE IsMaster = 0 and IsEnabled = 1 and IsLpe = 1  and lLpTemplateId "
                + DbTools.InClauseForSql(prgIds);

            CDataSet ds = new CDataSet(60);
            ds.LoadWithSqlQueryDangerously(m_dataSrc, sql);

            ArrayList programs = new ArrayList();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (m_rateSheetIds.Contains(row["SrcRateoptionsProgId"]) == false)
                {
                    throw new InvalidLpeSnapshotException(
                        String.Format("LpeRelease_Validate> {2} : loanProgram {0} contains a invalid  SrcRateoptionsProgId = {1}",
                        row["lLpTemplateId"], row["SrcRateoptionsProgId"], m_snapshotInfo));
                }
                programs.Add(row["lLpTemplateId"]);
            }
            ds = null;

            if (m_validatePolicyRelationShip == false)
                return;

            // 11/21/2007 ThienNguyen - Reviewed and safe
            // get policies
            string directlyAssociatedPoliciesSql =
                "\nSELECT pa.ProductId, p.PricePolicyId, p.PricePolicyDescription, p.MutualExclusiveExecSortedId, lLpTemplateNm " +
                "FROM ( Product_Price_Association pa JOIN Price_Policy p ON pa.PricePolicyID = p.PricePolicyId )              " +
                "   join loan_program_template product on pa.productId = product.lLpTemplateId                                " +
                "WHERE                               \n" +
                "           product.IsMaster = 0     \n" +
                "   and     product.IsEnabled = 1    \n" +
                "   and     product.IsLpe = 1        \n" +
                "   and     pa.AssocOverrideTri != 2 \n" +
                "   and ( pa.InheritVal = 1 or pa.AssocOverrideTri = 1 or pa.InheritValFromBaseProd = 1 ) \n" +
                "   and product.lLpTemplateId " + DbTools.InClauseForSql(programs); ;

            ds = new CDataSet(60);
            ds.LoadWithSqlQueryDangerously(m_dataSrc, directlyAssociatedPoliciesSql);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Guid productIdKey = (Guid)row["ProductId"];
                Guid policyId = (Guid)row["PricePolicyID"];

                if (m_policyRelationDb.Exist(policyId) == false)
                    throw new InvalidLpeSnapshotException(string.Format("LpeRelease> {2} : Loan Program {0} can not associate with the invalid policy {1}"
                        , productIdKey, policyId, m_snapshotInfo));

                ArrayList parents = m_policyRelationDb.GetAncestors(policyId);

                if (parents == null)
                    throw new InvalidLpeSnapshotException(string.Format("LpeRelease> {2} : Loan Program {0} can not associate with the invalid policy {1}"
                        , productIdKey, policyId, m_snapshotInfo));


                foreach (Guid ancestorPolicyId in parents)
                    if (m_policyRelationDb.Exist(ancestorPolicyId) == false)
                        throw new InvalidLpeSnapshotException(string.Format("<LpeRelease> {0} : policy {1} for Loan Program {2} has a invalid ancestor {3}.",
                            m_snapshotInfo, policyId, productIdKey, ancestorPolicyId));
            }

        }

        void LoadRateSheetIds()
        {
            CDataSet ds = new CDataSet();

            // 11/21/2007 ThienNguyen - Reviewed and safe
            ds.LoadWithSqlQueryDangerously(m_dataSrc, "select SrcProgId from Rate_Options");

            m_rateSheetIds.Clear();
            foreach (DataRow row in ds.Tables[0].Rows)
                if (row["SrcProgId"] != DBNull.Value)
                    m_rateSheetIds[row["SrcProgId"]] = null;
        }


        void CreatePolicyRelationCenter()
        {
            Tools.LogRegTest(string.Format("<LpeRelease_Validate> {0} : CreatePolicyRelationCenter", m_snapshotInfo));

            CDataSet ds = new CDataSet(60);

            // 11/21/2007 ThienNguyen - Reviewed and safe
            ds.LoadWithSqlQueryDangerously(m_dataSrc, "select PricePolicyId, ParentPolicyId, MutualExclusiveExecSortedId from price_policy");

            CRowHashtable rowsPolicyRelation = new CRowHashtable(ds.Tables[0].Rows, "PricePolicyId");
            Hashtable parentHashtable = CreateParentTable(rowsPolicyRelation);
            m_policyRelationDb = new PolicyRelationCenter(parentHashtable, CreateChildrenTable(rowsPolicyRelation));

        }

        // will move this method to PolicyRelationCenter
        static Hashtable CreateParentTable(CRowHashtable rowsPolicyRelation)
        {
            // compute parent hashtable.
            int rowCount = rowsPolicyRelation.Count;

            // the key is price policy id; each m_parentHashtable entry contains the ancestors of a given price policy
            Hashtable m_parentHashtable = new Hashtable(rowCount);


            ArrayList parents = new ArrayList(rowsPolicyRelation.Count);
            for (int iKeyRow = 0; iKeyRow < rowCount; ++iKeyRow)
            {
                DataRow keyRow = rowsPolicyRelation.GetRowByIndex(iKeyRow);
                //if( !( (bool) keyRow["IsPublished"] ) )
                //	continue;

                parents.Clear();

                Object policyId = (Guid)keyRow["PricePolicyId"];

                while (true)
                {
                    Object keyId = (Guid)keyRow["PricePolicyId"];
                    if (parents.Contains(keyId))
                    {
                        Tools.LogError("<LpeRelease_ValidateError> Don't expect this event. Need to review the policy : " + policyId);
                        break; // never happend
                    }
                    parents.Add(keyId);

                    if (DBNull.Value == keyRow["ParentPolicyId"])
                        break; // done with the particular tree

                    keyRow = rowsPolicyRelation.GetRowByKey(keyRow["ParentPolicyId"]);
                }

                ArrayList tmp = new ArrayList(parents.Count);
                foreach (Object obj in parents)
                    tmp.Add(obj);

                m_parentHashtable.Add(policyId, tmp);
            }

            return m_parentHashtable;
        }


        // will move this method to PolicyRelationCenter
        static Hashtable CreateChildrenTable(CRowHashtable rowsPolicyRelation)
        {
            // compute parent hashtable.
            int rowCount = rowsPolicyRelation.Count;

            // the key is price policy id; each m_parentHashtable entry contains the ancestors of a given price policy
            Hashtable childrenTable = new Hashtable(rowCount);


            for (int iKeyRow = 0; iKeyRow < rowCount; ++iKeyRow)
            {
                DataRow keyRow = rowsPolicyRelation.GetRowByIndex(iKeyRow);
                if (DBNull.Value == keyRow["ParentPolicyId"])
                    continue;
                Object parentPolicyId = keyRow["ParentPolicyId"];
                ArrayList children = (ArrayList)childrenTable[parentPolicyId];
                if (children == null)
                    childrenTable[parentPolicyId] = children = new ArrayList();

                Object policyId = (Guid)keyRow["PricePolicyId"];
                children.Add(policyId);
            }

            foreach (ArrayList children in childrenTable.Values)
                children.TrimToSize();

            return childrenTable;
        }


    }
}