using System;
using System.Configuration;
using DataAccess;
using LpeRelease.WebService;
using LqbGrammar;
using LqbGrammar.Exceptions;

namespace LpeRelease 
{
	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
        static string ReadConfigVar( string id )
        {
            string val = ConfigurationManager.AppSettings[id];
            if( val == null )
            {
                Tools.LogRegTest("<LpeRelease_Warning> web.config file doesn't contain the key " + id);
                return "";
            }
            return val;
        }

		public Global()
		{
			InitializeComponent();
		}	
		
		protected void Application_Start(Object sender, EventArgs e)
		{
			InitializeApplicationFramework();
			
            string lpeServiceEnabled = ReadConfigVar("WebServiceEnabled").ToLower();
            LpeReleaseService.Enabled = (lpeServiceEnabled == "true");
            Tools.LogRegTest("<LpeRelease> LpeRelease's WebService.Enables = " + LpeReleaseService.Enabled);
		}
		
		private static void InitializeApplicationFramework()
		{
			IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
			using (IApplicationInitialize iAppInit = LqbApplication.CreateInitializer(arrHandlers))
			{
				const string appName = "LpeRelease";
				iAppInit.SetName("LpeRelease");

				Adapter.ApplicationInitializer.RegisterFactories(appName, iAppInit);
				LendersOffice.ApplicationInitializer.RegisterFactories(appName, iAppInit);
			}
		}

        protected void Session_Start(Object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{

		}

		protected void Session_End(Object sender, EventArgs e)
		{

		}

		protected void Application_End(Object sender, EventArgs e)
		{

		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}

