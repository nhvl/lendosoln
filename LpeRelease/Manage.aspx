<%@ Page language="c#" Codebehind="Manage.aspx.cs" AutoEventWireup="false" Inherits="LpeRelease.Manage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Manage</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Manage" onsubmit="document.all.errorInfo.value = 'Please wait one moment...';" method="post" runat="server">
			<span id="Label1" style="font-weight: bold; width: 274px; Z-INDEX: 101; LEFT: 46px; POSITION: absolute; TOP: 42px">Lpe Release Management</span><asp:button id="StartBtn" style="Z-INDEX: 102; LEFT: 353px; POSITION: absolute; TOP: 34px" runat="server" Font-Bold="True" Text="Start!" onclick="StartBtn_Click"></asp:button>
			<asp:button id="RefreshBtn" style="Z-INDEX: 103; LEFT: 428px; POSITION: absolute; TOP: 34px" runat="server" Font-Bold="True" Text="Refresh" onclick="RefreshBtn_Click"></asp:button>
			<asp:button id="CreateSnapshotSmokeTestBtn" style="Z-INDEX: 104; LEFT: 519px; POSITION: absolute; TOP: 34px" runat="server" Font-Bold="True" Width="196px" Text="Create Snapshot SmokeTest" onclick="CreateSnapshotSmokeTestBtn_Click"></asp:button>
			<asp:Button id="EndBtn" style="Z-INDEX: 106; LEFT: 733px; POSITION: absolute; TOP: 34px" runat="server" Font-Bold="True" Width="81px" Text="End" onclick="EndBtn_Click"></asp:Button>
			<TABLE id="Table2" style="Z-INDEX: 105; LEFT: 44px; WIDTH: 829px; POSITION: absolute; TOP: 101px; HEIGHT: 550px" cellSpacing="1" cellPadding="1" width="829" border="1">
				<TR>
					<TD style="WIDTH: 141px"><STRONG>Current Release</STRONG></TD>
					<TD style="WIDTH: 218px"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px"></TD>
					<TD style="WIDTH: 218px">ReleaseTime</TD>
					<TD><asp:textbox id="curReleaseD" runat="server" Width="100%" Enabled="False"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px"></TD>
					<TD style="WIDTH: 218px">Snapshot Id</TD>
					<TD><asp:textbox id="curSsId" runat="server" Width="100%" Enabled="False"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px"></TD>
					<TD style="WIDTH: 218px">Snapshot Name</TD>
					<TD><asp:textbox id="curSsName" runat="server" Width="100%" Enabled="False"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px"></TD>
					<TD style="WIDTH: 218px">Data Last Modified Date</TD>
					<TD><asp:textbox id="releaseDataLastModifiedD" runat="server" Width="100%" Enabled="False"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px"><STRONG>Lpe Source Data</STRONG></TD>
					<TD style="WIDTH: 218px"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px"></TD>
					<TD style="WIDTH: 218px">Last Modified Date</TD>
					<TD><asp:textbox id="lpeDataLastModifiedD" runat="server" Width="100%" Enabled="False" ReadOnly="True"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px"><STRONG>Controller Options</STRONG></TD>
					<TD style="WIDTH: 218px"><asp:button id="SaveOptions_btn" runat="server" Font-Bold="True" Width="111px" Text="Save Options" ForeColor="Red" onclick="SaveOptions_btn_Click"></asp:button></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px" align="right">Minimun Drop Point&nbsp;&nbsp;</TD>
					<TD style="WIDTH: 218px" vAlign="top">Drop outofdate ss after the other ss has been 
						service for:</TD>
					<TD><asp:textbox id="MinOutOfDatePeriod_Ed" runat="server" Width="82px"></asp:textbox>&nbsp;minutes 
						(using for fast release snapshot). Default is&nbsp;6 min.&nbsp;</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px" align="right">Drop Point&nbsp;&nbsp;</TD>
					<TD style="WIDTH: 218px" vAlign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;as 
						above</TD>
					<TD><asp:textbox id="OutOfDatePeriod_Ed" runat="server" Width="82px"></asp:textbox>&nbsp;minutes 
						(using for&nbsp;normal&nbsp;release snapshot). Default is&nbsp;7 min.</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px" align="right">Create Point&nbsp;&nbsp;</TD>
					<TD style="WIDTH: 218px" vAlign="top">Detect for new data changes after current 
						release has been in service for:</TD>
					<TD><asp:textbox id="DetectDataChangePeriod_ed" runat="server" Width="82px"></asp:textbox>&nbsp;minutes. 
						Default is 10 minutes</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px" align="right">Release Point&nbsp;&nbsp;</TD>
					<TD style="WIDTH: 218px" vAlign="top">Take ss after current release has been in 
						service for:
					</TD>
					<TD><asp:textbox id="TakeSsPeriod_ed" runat="server" Width="82px"></asp:textbox>&nbsp;minutes. 
						Default is 25 minutes. <STRONG>Must be greater than the one above</STRONG>.</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px" align="right">Web Service Enable&nbsp;&nbsp;</TD>
					<TD style="WIDTH: 218px" vAlign="top"></TD>
					<TD>
						<asp:CheckBox id="m_chkWebService" runat="server" oncheckedchanged="m_chkWebService_CheckedChanged"></asp:CheckBox></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 141px"><STRONG>Status</STRONG></TD>
					<TD style="WIDTH: 218px" vAlign="top">Information</TD>
					<TD><asp:textbox id="errorInfo" runat="server" Width="100%" ReadOnly="True" Height="92px" TextMode="MultiLine"></asp:textbox></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
