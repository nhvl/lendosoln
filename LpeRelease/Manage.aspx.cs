using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.RatePrice;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using System.Xml;
using System.Text;

using DataAccess;
using LpeRelease.WebService;
using LendersOffice.Common;

namespace LpeRelease
{
    /// <summary>
    /// Summary description for Manage.
    /// </summary>
    public partial class Manage : System.Web.UI.Page
    {
	
        protected void PageLoad(object sender, System.EventArgs e)
        {
            errorInfo.Text                     = "";
            errorInfo.ForeColor                = Color.Black;

            string action = RequestHelper.GetSafeQueryString("action");
            if( action != null )
            {
                action = action.ToLower();
                switch( action )
                {
                case "start" : // start the creating snapshot process
                    StartBtn_Click( this, null);
                    return;
                        
                case "wakeup" :
                    CController.Wakeup();
                    return;

                }
            }

            if( !IsPostBack )
            {
                DisplayData();
            }
        }

        string OutOfDateString( TimeSpan duration)
        {
            if( duration.Days !=  0 )
                return string.Format(" [ Out of date at least {0} days {1} hours ]", duration.Days, duration.Hours);

            if( duration.TotalMinutes >= 100 )
                return string.Format(" [ Out of date about {0} hours {1} minutes ]", (long)duration.Hours, (long)duration.Minutes);


            return string.Format(" [ Out of date about {0} minutes {1} seconds ]", (long)duration.TotalMinutes, duration.Seconds);
        }
        
        string ReleaseAgeString( DateTime releaseTime )
        {
            try
            {
                TimeSpan duration = DataAccess.Tools.GetDBCurrentDateTime() - releaseTime;
                if( duration.Days !=  0 )
                    return string.Format(" [ at least {0} days {1} hours ago ]", duration.Days, duration.Hours);

                if( duration.TotalMinutes >= 100 )
                    return string.Format(" [ about {0} hours {1} minutes ago ]", (long)duration.Hours, (long)duration.Minutes);


                return string.Format(" [ about {0} minutes {1} seconds ago ]", (long)duration.TotalMinutes, duration.Seconds);


            }
            catch
            {
                return "";
            }
        }

        private void DisplayData()
        {
            try
            {
                errorInfo.Text = "";

                DateTime dataLastChangedD;
                using( DbDataReader r = DataAccess.StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "[GetPriceEngineVersionDate]" ) )
                {
                    r.Read(); // a row must be there.
			
                    dataLastChangedD  = Convert.ToDateTime( r["LastUpdatedD"] );
                }
                lpeDataLastModifiedD.Text = string.Format( "{0} on {1}", dataLastChangedD.ToLongTimeString(), dataLastChangedD.ToShortDateString() );

                
                CLpeReleaseInfo info = CLpeReleaseInfo.GetLatestReleaseInfo();
                if( info != null )
                {
                    curReleaseD.Text = string.Format( "{0} on {1} {2}", info.ReleaseVer.ToLongTimeString(), info.ReleaseVer.ToShortDateString(), 
                                                      ReleaseAgeString( info.ReleaseVer ) );
                    curSsId.Text     = info.SnapshotId;
                    curSsName.Text   = info.SnapshotName;

                    string dateStr   =  string.Format( "{0} on {1}", info.DataLastModifiedD.ToLongTimeString(), 
                                                info.DataLastModifiedD.ToShortDateString() );

                    if( dataLastChangedD == info.DataLastModifiedD )
                    {
                        releaseDataLastModifiedD.Text      = dateStr;
                        releaseDataLastModifiedD.ForeColor = Color.Black;
                        releaseDataLastModifiedD.Enabled   = false;
                    }
                    else
                    {
                        
                        TimeSpan duration = dataLastChangedD - info.DataLastModifiedD;


                        if( duration.TotalMinutes < 0 )
                        {
                            releaseDataLastModifiedD.Text      = dateStr + " [ Error : LPE Version in Snapshot > LPE Version in DB ]";
                            releaseDataLastModifiedD.ForeColor = Color.Red;
                        }
                        else if( duration.TotalMinutes <= 90 )
                        {
                            releaseDataLastModifiedD.Text      = dateStr + OutOfDateString( duration );
                            releaseDataLastModifiedD.ForeColor = Color.Black;
                        }
                        else
                        {
                            releaseDataLastModifiedD.Text      = dateStr + OutOfDateString( duration );
                            releaseDataLastModifiedD.ForeColor = Color.Red;
                        }
                        releaseDataLastModifiedD.Enabled   = true;
                    }

                }               


                DetectDataChangePeriod_ed.Text = CController.s_nDetectLpeSrcDataChangeAfterCurrentReleaseHasBeenInServiceForXMinutes.ToString();
                OutOfDatePeriod_Ed.Text        = CController.s_nDropOutOfDateSsAfterXMinutes.ToString();
                MinOutOfDatePeriod_Ed.Text     = CController.s_minDropOutOfDateSsAfterXMinutes.ToString();
                TakeSsPeriod_ed.Text           = CController.s_nTakeSsAfterCurrentReleaseHasBeenInServiceForXMinutes.ToString();
                m_chkWebService.Checked        = LpeReleaseService.Enabled;

                
                switch( CController.ControllersState )
                {
                    case CController.SnapshotThreadState.Finish :
                        errorInfo.Text = "The Snapshot Creator Threads don't run.";
                        break;

                    case CController.SnapshotThreadState.UnStarted :
                        errorInfo.Text = "The Snapshot Creator Threads are initializing.";
                        break;
 
                    case CController.SnapshotThreadState.Alive :
                        errorInfo.Text = "The Snapshot Creator Threads are running.  Feel free to check logs for LpeRelease_Exception while you wait, especially if longer than 5 minutes.";
                        break;
                }

            }
            catch( Exception ex )
            {
                errorInfo.Text      = ex.Message + "\n***CallStack:\n" + ex.StackTrace;
                errorInfo.ForeColor = Color.Red;
            }

        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

        protected void StartBtn_Click(object sender, System.EventArgs e)
        {
            CController.CreateControllers();
            DisplayData();
        }

        protected void SaveOptions_btn_Click(object sender, System.EventArgs e)
        {
            try
            {
                CController.s_nDetectLpeSrcDataChangeAfterCurrentReleaseHasBeenInServiceForXMinutes = double.Parse( DetectDataChangePeriod_ed.Text );
                CController.s_nDropOutOfDateSsAfterXMinutes   = double.Parse( OutOfDatePeriod_Ed.Text );
                CController.s_minDropOutOfDateSsAfterXMinutes = double.Parse( MinOutOfDatePeriod_Ed.Text );
                CController.s_nTakeSsAfterCurrentReleaseHasBeenInServiceForXMinutes = double.Parse( TakeSsPeriod_ed.Text );
                LpeReleaseService.Enabled                      = m_chkWebService.Checked;
                DisplayData();
            }
            catch( Exception ex )
            {
                errorInfo.Text       = ex.Message + "\n***CallStack:\n" + ex.StackTrace;
                errorInfo.ForeColor  = Color.Red;
            }
        }

        protected void RefreshBtn_Click(object sender, System.EventArgs e)
        {
            DisplayData();
        }

        protected void CreateSnapshotSmokeTestBtn_Click(object sender, System.EventArgs e)
        {
            try
            {
                CController.ShutdownControllers();

                // call the stored procedure UpdatePriceEngineVersionDate
                Toolbox.RatePrice.Invalidate();
                Thread.Sleep(10*1000);
                Tools.LogRegTest( "<SnapshotSmokeTest> Just called the UpdatePriceEngineVersionDate stored procedure and slept for 1 minute to ensure the data is old enough." );
                Thread.Sleep(50*1000);

                CLpeReleaseInfo orgRelease = CLpeReleaseInfo.GetLatestReleaseInfo(true);


                CController.RequestSmokeTest = true;
                CController.CreateControllers();

                const int timeOutInMinute = 4;
                DateTime end = DateTime.Now.AddMinutes( timeOutInMinute );

                bool ok = false;
                while( true )
                {
                    if( DateTime.Now >= end )
                        break;

                    CLpeReleaseInfo latestRelease = CLpeReleaseInfo.GetLatestReleaseInfo(true);
                    if( latestRelease != orgRelease )
                    {
                        ok = true;
                        break;
                    }
                    Thread.Sleep( 5*1000 );
                }

                DisplayData();
                string msg = errorInfo.Text;
                if( ok )
                {
                    errorInfo.Text = "Successly create snapshot.\n" + msg;
                    if( errorInfo.ForeColor != Color.Red )
                        errorInfo.ForeColor = Color.Blue;
                }
                else
                {
                    errorInfo.Text = "Timeout for creating snapshot.\n" + msg;
                    errorInfo.ForeColor  = Color.Red;
                }
            }
            catch( Exception exc )
            {
                DisplayData();
                errorInfo.Text       = "Snapshot smoke test error : \n" + exc.Message + "\n***CallStack:\n" + exc.StackTrace;
                errorInfo.ForeColor  = Color.Red;
            }
            finally
            {
                CController.RequestSmokeTest = false;
            }

        }

        protected void EndBtn_Click(object sender, System.EventArgs e)
        {
            CController.ShutdownControllers();
            for(int i=0; i < 5 && CController.ControllersState != CController.SnapshotThreadState.Finish; i++)
                Thread.Sleep(1000);

            DisplayData();
        }

        protected void m_chkWebService_CheckedChanged(object sender, System.EventArgs e)
        {
        
        }

    }
}