using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using DataAccess;

namespace LpeRelease.WebService
{
	/// <summary>
	/// Summary description for LpeReleaseService.
	/// </summary>    

    [WebService(Namespace="http://www.lendersoffice.com/LpeRelease/")]
	public class LpeReleaseService : System.Web.Services.WebService
	{
        static public bool s_enabled = true;
        static object s_lock = new object();

        public static bool Enabled
        {
            get 
            { 
                lock( s_lock )  {return s_enabled;} 
            }
            set 
            { 
                lock( s_lock )  
                {
                    if( s_enabled == value )
                        return;

                    s_enabled = value;
                    Tools.LogRegTest("LpeRelease's WebServiceEnabled = " + s_enabled.ToString() );
                }
            }    
        }

        static LpeReleaseService()
        {
        }


		public LpeReleaseService()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

        [WebMethod]
        public void FastReleaseSnapshotFor( DateTime priceEngineVersionDate )
        {
            if( Enabled )
                CController.PriceEngineVersionForLpeUpdate = priceEngineVersionDate;
        }

        [WebMethod]
        public void FastReleaseSnapshot()
        {
            if( Enabled )
                CController.PriceEngineVersionForLpeUpdate = DateTime.MaxValue;
        }


    }
}
