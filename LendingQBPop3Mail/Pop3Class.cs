﻿// <copyright file="Pop3Class.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is Pop3Class class.</summary>
namespace LendingQBPop3Mail
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Configuration;

    public class Pop3Class
    {
        private IPop3Class pop3 = null;
        public Pop3Class()
        {
            this.pop3 = new OpenPop3Class();
        }

        public static Pop3Class Create(string type)
        {
            Pop3Class pop3 = new Pop3Class();
            pop3.pop3 = new OpenPop3Class();
            return pop3;
        }

        public void Connect(string userName, string password, string smtpServer, int port, bool useSsl)
        {
            this.pop3.Connect(userName, password, smtpServer, port, useSsl);
        }

        public void Disconnect()
        {
            this.pop3.Disconnect();
        }

        public string Description
        {
            get { return this.pop3.Description; }
        }

        public int Count
        {
            get
            {
                return this.pop3.Count;
            }
        }

        public string GetMessageUID(int messageId)
        {
            return this.pop3.GetMessageUID(messageId);
        }

        /// <summary>
        /// Get message in pop3 using based 1 index.
        /// </summary>
        /// <param name="index">Based 1 index.</param>
        /// <returns>Message in inbox.</returns>
        public Pop3Message GetMessage(int index)
        {
            return this.pop3.GetMessage(index);
        }

        public void DeleteSingleMessage(int index)
        {
            this.pop3.DeleteSingleMessage(index);
        }

    }
}
