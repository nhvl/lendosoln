﻿// <copyright file="Pop3Headers.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is Pop3Headers class.</summary>
namespace LendingQBPop3Mail
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class Pop3Headers
    {
        public string Text { get; private set; }

        internal Pop3Headers(string text)
        {
            this.Text = text;
        }
    }
}