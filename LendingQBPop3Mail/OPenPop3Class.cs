﻿// <copyright file="OPenPop3Class.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is OPenPop3Class class.</summary>
namespace LendingQBPop3Mail
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    class OpenPop3Class : IPop3Class
    {
        private OpenPop.Pop3.Pop3Client pop3 = null;

        public OpenPop3Class()
        {
            this.pop3 = new OpenPop.Pop3.Pop3Client();
        }

        #region IPop3Class Members
        public string Description { get { return "OpenPop.Pop3"; } }

        public void Connect(string userName, string password, string smtpServer, int port, bool useSsl)
        {
            this.pop3.Connect(smtpServer, port, useSsl);
            this.pop3.Authenticate(userName, password, OpenPop.Pop3.AuthenticationMethod.UsernameAndPassword);
        }

        public void Disconnect()
        {
            this.pop3.Disconnect();
        }

        public int Count
        {
            get { return this.pop3.GetMessageCount(); }
        }

        public string GetMessageUID(int messageId)
        {
            return this.pop3.GetMessageUid(messageId);
        }

        public Pop3Message GetMessage(int index)
        {
            return new OpenPop3Message(this.pop3.GetMessage(index));
        }

        public void DeleteSingleMessage(int index)
        {
            this.pop3.DeleteMessage(index);
        }

        #endregion
    }

}
