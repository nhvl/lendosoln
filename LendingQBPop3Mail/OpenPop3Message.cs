﻿// <copyright file="OpenPop3Message.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is OpenPop3Message class.</summary>
// <author>david</author>
// <date>$date$</author>
namespace LendingQBPop3Mail
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    /// <summary>
    /// This class is used as a utility to extract the parts of an email
    /// message that has been retrieved from an email server.  It is not
    /// involved in sending email out.
    /// </summary>
    class OpenPop3Message : Pop3Message
    {
        private OpenPop.Mime.Message msg;

        private System.Net.Mail.MailMessage mailMsg;
        private List<Pop3Recipient> recipientList = null;
        private List<Pop3Attachment> attachmentList = null;
        private Pop3Headers headers = null;
        private string from = null;
        private string fromName = null;

        internal OpenPop3Message(OpenPop.Mime.Message _msg)
        {
            this.msg = _msg;
        }

        private System.Net.Mail.MailMessage GetMailMessage()
        {
            if (this.mailMsg == null)
            {
                this.mailMsg = msg.ToMailMessage();
            }

            return this.mailMsg;
        }

        public override string Subject
        {
            get
            {
                return this.msg.Headers.Subject;
            }
        }

        public override string From
        {
            get
            {
                if (from == null)
                {
                    if (this.msg.Headers.From != null && this.msg.Headers.From.HasValidMailAddress)
                    {
                        from = this.msg.Headers.From.Address;
                    }
                    else
                    {
                        from = string.Empty;
                    }
                }

                return from;
            }

            set
            {
                from = value;
            }
        }

        public override string FromName
        {
            get
            {
                if (fromName == null)
                {
                    if (this.msg.Headers.From != null && this.msg.Headers.From.HasValidMailAddress)
                    {
                        fromName = this.msg.Headers.From.DisplayName;
                    }
                    else
                    {
                        fromName = string.Empty;
                    }
                }

                return fromName;
            }

            set
            {
                fromName = value;
            }
        }


        public override string Body
        {
            get
            {
                return this.GetMailMessage().Body;
            }
        }

        public override string BodyText
        {
            get
            {
                var part = this.msg.FindFirstPlainTextVersion();
                if (part != null)
                {
                    if (part.IsMultiPart == false)
                    {
                        return part.GetBodyAsText();
                    }

                }

                return null;
            }
        }

        public override string HTMLBody
        {
            get
            {
                var mailmsg = this.GetMailMessage();
                if (mailMsg.IsBodyHtml)
                {
                    return mailMsg.Body;
                }
                else
                {
                    return null;
                }
            }
        }

        public override IEnumerable<Pop3Recipient> Recipients
        {
            get
            {
                // Lazy load the recipient list.
                if (this.recipientList == null)
                {
                    this.recipientList = new List<Pop3Recipient>();

                    foreach (var o in this.msg.Headers.To)
                    {
                        this.recipientList.Add(new Pop3Recipient(o.Address, Pop3RecipientType.To));
                    }

                    if (this.msg.Headers.Cc != null)
                    {
                        foreach (var o in this.msg.Headers.Cc)
                        {
                            this.recipientList.Add(new Pop3Recipient(o.Address, Pop3RecipientType.Cc));
                        }
                    }

                    if (this.msg.Headers.Bcc != null)
                    {
                        foreach (var o in this.msg.Headers.Bcc)
                        {
                            this.recipientList.Add(new Pop3Recipient(o.Address, Pop3RecipientType.Bcc));
                        }
                    }
                }

                return this.recipientList;
            }
        }

        public override IEnumerable<Pop3Attachment> Attachments
        {
            get
            {
                if (this.attachmentList == null)
                {
                    this.attachmentList = new List<Pop3Attachment>();

                    foreach (var attachment in this.msg.FindAllAttachments())
                    {
                        this.attachmentList.Add(new Pop3Attachment(attachment.FileName, attachment.ContentType.MediaType, attachment.Body));
                    }
                }

                return this.attachmentList;
            }
        }

        public override string MailData
        {
            get { return ASCIIEncoding.ASCII.GetString(this.msg.RawMessage); }
        }

        /// <summary>
        /// Gets the date sent. Note, this date may be different from Date in JMail component.
        /// </summary>
        public override DateTime Date
        {
            get { return this.msg.Headers.DateSent; }
        }

        public override string RecipientsString
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                foreach (var recipient in this.Recipients)
                {
                    sb.AppendLine(recipient.Email);
                }
                return sb.ToString();
            }
        }

        public override Pop3Headers Headers
        {
            get
            {
                if (this.headers == null)
                {
                    // 7/21/2015 - This is not a perfect with the jMail Headers.Text. jmail is exporting all values include subject, sender, etc...
                    // However in our code we only care about unknown header. So I am going to generate the unknown headers value.
                    // If we need have a perfect match then we can update this part of code.

                    StringBuilder sb = new StringBuilder();

                    if (this.msg.Headers.UnknownHeaders != null)
                    {
                        foreach (string key in this.msg.Headers.UnknownHeaders.Keys)
                        {
                            string v = this.msg.Headers.UnknownHeaders[key];

                            if (string.IsNullOrEmpty(v) == false)
                            {
                                sb.AppendLine(key + ": " + v);
                            }
                        }
                    }

                    this.headers = new Pop3Headers(sb.ToString());
                }

                return this.headers;
            }
        }
    }
}
