﻿// <copyright file="Pop3Message.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is Pop3Message class.</summary>
namespace LendingQBPop3Mail
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public abstract class Pop3Message
    {
        internal Pop3Message() { }

        public abstract string Subject { get; }
        public abstract string From { get; set; }
        public abstract string FromName { get; set; }
        public abstract string Body { get; }
        public abstract string BodyText { get; }
        public abstract string HTMLBody { get; }
        public abstract DateTime Date { get; }
        public abstract string MailData { get; }
        public abstract IEnumerable<Pop3Recipient> Recipients { get; }
        public abstract IEnumerable<Pop3Attachment> Attachments { get; }
        public abstract Pop3Headers Headers { get; }
        public abstract string RecipientsString { get; }
    }
}
