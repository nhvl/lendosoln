﻿// <copyright file="Pop3Attachment.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is Pop3Attachment class.</summary>
namespace LendingQBPop3Mail
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    public class Pop3Attachment
    {
        // dd 7/21/2015 - Only set either jmailAttachment or binaryData. But not both.
        public static Pop3Attachment EmptyAttachment = new Pop3Attachment(string.Empty, string.Empty, new byte[0]);

        private byte[] binaryData;

        public string Name { get; private set; }
        public string ContentType { get; private set; }

        internal Pop3Attachment(string name, string contentType, byte[] data)
        {
            this.Name = name;
            this.ContentType = contentType;
            this.binaryData = data;
        }

        public void SaveToFile(string fileName)
        {
            File.WriteAllBytes(fileName, this.binaryData);
        }
    }

}
