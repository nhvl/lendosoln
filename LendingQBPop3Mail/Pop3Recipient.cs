﻿// <copyright file="Pop3Recipient.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is Pop3Recipient class.</summary>
namespace LendingQBPop3Mail
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class Pop3Recipient
    {
        public Pop3RecipientType ReType { get; private set; }
        public string Email { get; private set; }

        internal Pop3Recipient(string email, Pop3RecipientType type)
        {
            this.Email = email;
            this.ReType = type;
        }
    }
}
