﻿// <copyright file="IPop3Class.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is IPop3Class class.</summary>
namespace LendingQBPop3Mail
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    interface IPop3Class
    {
        void Connect(string userName, string password, string smtpServer, int port, bool useSsl);
        void Disconnect();

        int Count { get; }
        string Description { get; }
        string GetMessageUID(int messageId);
        Pop3Message GetMessage(int index);
        void DeleteSingleMessage(int index);

    }
}
