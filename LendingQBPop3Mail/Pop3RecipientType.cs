﻿// <copyright file="Pop3RecipientType.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is Pop3RecipientType class.</summary>
namespace LendingQBPop3Mail
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public enum Pop3RecipientType
    {
        To = 0,
        Cc = 1,
        Bcc = 2
    }
}
