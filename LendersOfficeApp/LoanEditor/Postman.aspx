<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">

    <title>Post man</title>

    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/newdesign.main.css">
    <link rel="stylesheet" href="../css/newdesign.lendingqb.css">

    <style>*[hidden] { display: none; }</style>
    <script src="../inc/modernizr-2.8.3.js"></script>
</head>
<body ng-app="app">

<div class="container-fluid">
    <postman></postman>
</div>

<script src="../inc/console-stub.js"></script>

<script src="../inc/jquery-2.1.4.min.js"></script>
<script src="../inc/bootstrap-3.3.4.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0-rc.2/angular.min.js"></script>
<script src="../inc/LoanEditor.Postman.js"></script>

</body>
</html>
