using System;
using DataAccess;
using LendersOffice.Common;

namespace LUnit
{
    public class ShowMessageOnlyExc : CBaseException
    {
        public ShowMessageOnlyExc( string msg ) : base( ErrorMessages.Generic, msg )
        {
        }
    }

    public class LUnitTimeoutException:  ShowMessageOnlyExc 
    {
        public LUnitTimeoutException( string msg ) : base( msg )
        {
        }

        public LUnitTimeoutException() : base( "Time out" )
        {
        }
    }

}
