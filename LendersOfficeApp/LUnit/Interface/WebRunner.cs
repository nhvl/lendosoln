﻿namespace LUnit.Interface
{
    using System.Collections.Generic;

    /// <summary>
    /// Shim class to adapt the LUnit.Interface to the interface used by the web pages.
    /// </summary>
    public sealed class WebRunner : LUnit.WebShim.IWebRunner
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebRunner"/> class.
        /// </summary>
        /// <param name="runner">The interface that carries out the implementation.</param>
        public WebRunner(LUnit.Interface.IRunner runner)
        {
            this.Runner = runner;
            this.IdentifierMap = new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets or sets the interface that this class is shimming.
        /// </summary>
        /// <value>The interface that this class is shimming.</value>
        private LUnit.Interface.IRunner Runner { get; set; }

        /// <summary>
        /// Gets or sets a map of web page identifiers to internal identifiers.
        /// </summary>
        private Dictionary<string, string> IdentifierMap { get; set; }

        /// <summary>
        /// Retrieve all the test fixtures that will be run.
        /// </summary>
        /// <returns>All the test fixtures that will be run.</returns>
        public WebShim.TestClassData[] GetTestFixtures()
        {
            var list = new List<WebShim.TestClassData>();
            this.GetTestFixtures(list);
            return list.ToArray();
        }

        /// <summary>
        /// Run the test methods contained within the specified test fixture,
        /// or all test methods if fixtureId is null.
        /// </summary>
        /// <param name="fixtureId">The identifier for a test fixture, or null.</param>
        /// <returns>The results of running the test methods.</returns>
        public WebShim.TestClassResult[] RunFixture(int fixtureId)
        {
            // We need to initialize the identifier map
            this.GetTestFixtures(null);

            string identifier = (fixtureId == int.MaxValue) ? null : this.IdentifierMap[fixtureId.ToString()];
            var result = this.Runner.RunFixture(identifier);

            var converted = new WebShim.TestClassResult(result, fixtureId, null);
            return new WebShim.TestClassResult[] { converted };
        }

        /// <summary>
        /// Run a single test method, as indicated with the specified fixture and test identifiers.
        /// </summary>
        /// <param name="fixtureId">The identifier for the test fixture containine the test method.</param>
        /// <param name="testId">The identifier for the test method.</param>
        /// <returns>The result of running the test method.</returns>
        public WebShim.TestClassResult RunSingleTest(int fixtureId, int testId)
        {
            // We need to initialize the identifier map
            this.GetTestFixtures(null);

            string classId = this.IdentifierMap[fixtureId.ToString()];
            string methodId = this.IdentifierMap[this.GenerateKey(fixtureId, testId)];
            var result = this.Runner.RunSingleTest(classId, methodId);
            return new WebShim.TestClassResult(result, fixtureId, methodId);
        }

        /// <summary>
        /// Retrieve test fixtures and populate the identifier map.
        /// </summary>
        /// <param name="list">If not null, this will be populated as well.</param>
        private void GetTestFixtures(List<WebShim.TestClassData> list)
        {
            var result = this.Runner.GetTestFixtures();

            int index = -1;
            foreach (var item in result)
            {
                if (!item.IsRunnable)
                {
                    continue;
                }

                index++;
                if (list != null)
                {
                    var classData = new WebShim.TestClassData(item, index);
                    list.Add(classData);
                }

                this.PopulateIdentifierMap(index, item);

                int methodIndex = 0;
                foreach (var method in item.TestMethods)
                {
                    if (method.IsRunnable)
                    {
                        this.PopulateIdentifierMap(index, methodIndex++, method);
                    }
                }
            }
        }

        /// <summary>
        /// Add the fixture to the identifier map.
        /// </summary>
        /// <param name="fixtureId">The fixture id as seen by the web pages.</param>
        /// <param name="item">The fixture.</param>
        private void PopulateIdentifierMap(int fixtureId, TestClassData item)
        {
            this.IdentifierMap[fixtureId.ToString()] = item.Id;
        }

        /// <summary>
        /// Add the method to the identifier map.
        /// </summary>
        /// <param name="fixtureId">The fixture id as seen by the web pages.</param>
        /// <param name="methodId">The method id as seen by the web pages.</param>
        /// <param name="item">The test method.</param>
        private void PopulateIdentifierMap(int fixtureId, int methodId, TestMethodData item)
        {
            var key = this.GenerateKey(fixtureId, methodId);
            this.IdentifierMap[key] = item.Id;
        }

        /// <summary>
        /// Use index positions from the web pages to generate a key for retrieving the method's identifier.
        /// </summary>
        /// <param name="fixtureId">Index for the test fixture.</param>
        /// <param name="methodId">Index for the test method.</param>
        /// <returns>Key used to lookup a test method's identifier.</returns>
        private string GenerateKey(int fixtureId, int methodId)
        {
            return string.Format("{0}:{1}", fixtureId.ToString(), methodId.ToString());
        }
    }
}