﻿namespace LUnit.Interface
{
    /// <summary>
    /// Encapsulates the result of running an NUnit test fixture.
    /// </summary>
    public sealed class TestClassResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestClassResult"/> class.
        /// </summary>
        /// <param name="data">The the test fixture.</param>
        /// <param name="status">The result of running the test fixture.</param>
        /// <param name="message">A message from running the test fixture.</param>
        /// <param name="methodResults">The results for the test methods contained in the test fixture.</param>
        public TestClassResult(TestClassData data, ResultStatus status, string message, TestMethodResult[] methodResults)
        {
            this.Data = data;
            this.ResultStatus = status;
            this.TestMethodResults = methodResults;

            if (status == ResultStatus.Success)
            {
                this.Message = "pass";
            }
            else if (status == ResultStatus.Fail)
            {
                this.Message = "fail";
            }
            else
            {
                this.Message = message;
            }
        }

        /// <summary>
        /// Gets the identifier for the test fixture.
        /// </summary>
        /// <value>The identifier for the test fixture.</value>
        public string FixtureId
        {
            get
            {
                return this.Data.Id;
            }
        }

        /// <summary>
        /// Gets the fullname of the test fixture.
        /// </summary>
        /// <value>The fullname of the test fixture.</value>
        public string FullName
        {
            get
            {
                return this.Data.FullName;
            }
        }

        /// <summary>
        /// Gets the result of running the test fixture.
        /// </summary>
        /// <value>The result of running the test fixture.</value>
        public ResultStatus ResultStatus { get; private set; }

        /// <summary>
        /// Gets a message from running the test fixture.
        /// </summary>
        /// <value>A message from running the test fixture.</value>
        public string Message { get; private set; }

        /// <summary>
        /// Gets the results for the test methods contained in the test fixture.
        /// </summary>
        /// <value>The results for the test methods contained in the test fixture.</value>
        public TestMethodResult[] TestMethodResults { get; private set; }

        /// <summary>
        /// Gets the test class data.
        /// </summary>
        /// <value>The test class data.</value>
        internal TestClassData Data { get; private set; }
    }
}