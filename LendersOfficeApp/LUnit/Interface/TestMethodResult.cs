﻿namespace LUnit.Interface
{
    /// <summary>
    /// The result of running an NUnit test fixture/method.
    /// </summary>
    public enum ResultStatus
    {
        /// <summary>
        /// The test method was not executed, for whatever reason.
        /// </summary>
        Ignore,

        /// <summary>
        /// The test method failed.
        /// </summary>
        Fail,

        /// <summary>
        /// The test method succeeded.
        /// </summary>
        Success
    }

    /// <summary>
    /// Encapsulates the result of running a test method.
    /// </summary>
    public sealed class TestMethodResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestMethodResult"/> class.
        /// </summary>
        /// <param name="data">The data for the test method.</param>
        /// <param name="s">The result of running the test method.</param>
        /// <param name="m">A display message from running the test method.</param>
        public TestMethodResult(TestMethodData data, ResultStatus s, string m)
        {
            this.Data = data;
            this.ResultStatus = s;
            this.Message = (s == ResultStatus.Success) ? "pass" : m;
        }

        /// <summary>
        /// Gets the identifier of the test method.
        /// </summary>
        /// <value>The identifier of the test method.</value>
        public string MethodId
        {
            get
            {
                return this.Data.Id;
            }
        }

        /// <summary>
        /// Gets the result of running the test method.
        /// </summary>
        /// <value>The result of running the test method.</value>
        public ResultStatus ResultStatus { get; private set; }

        /// <summary>
        /// Gets a display message from running the test method.
        /// </summary>
        /// <value>A display message from running the test method.</value>
        public string Message { get; private set; }

        /// <summary>
        /// Gets the data for the test method.
        /// </summary>
        /// <value>The data for the test method.</value>
        public TestMethodData Data { get; private set; }
    }
}