﻿namespace LUnit.Interface
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Security.EasyAccessControl;

    /// <summary>
    /// Factory for creating implemenations of IRunner.
    /// </summary>
    public static class LUnitFactory
    {
        /// <summary>
        /// Cache the assembly since it is expensive to load.
        /// </summary>
        private static Assembly assembly;

        /// <summary>
        /// Initializes static members of the <see cref="LUnitFactory" /> class.
        /// </summary>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Static constructors cannot throw exceptions, else the application dies.")]
        static LUnitFactory()
        {
            LoadAssemblyHelper loadAssemblyHelper = null;
            try
            {
                MakeSureNUnitReady();

                loadAssemblyHelper = new LoadAssemblyHelper();
                AppDomain.CurrentDomain.AssemblyResolve += loadAssemblyHelper.ResolveEventHandler;

                assembly = Assembly.LoadFrom(ConstSite.LendOSolnTestDll);
            }
            catch (Exception exc)
            {
                string msg = "Can not create WebTestClasses.";
                if (loadAssemblyHelper != null && !string.IsNullOrEmpty(loadAssemblyHelper.ErrorMessage))
                {
                    msg += " *** " + loadAssemblyHelper.ErrorMessage;
                }

                Tools.LogErrorWithCriticalTracking(msg, exc);
            }
            finally
            {
                if (loadAssemblyHelper != null)
                {
                    AppDomain.CurrentDomain.AssemblyResolve -= loadAssemblyHelper.ResolveEventHandler;
                }
            }
        }

        /// <summary>
        /// Create a test runner.
        /// </summary>
        /// <param name="useNewLUnit">If true, the code in the Current folder is used, else the code in the Legacy folder is used.</param>
        /// <returns>A test runner.</returns>
        public static IRunner CreateRunner(bool useNewLUnit)
        {
            if (assembly == null)
            {
                return null;
            }

            if (useNewLUnit)
            {
                // Use the LUnit implementation in the 'Current' folder,
                // which is a total re-write.
                return new LUnit.Current.Shim(assembly);
            }
            else
            {
                // Use the LUnit implementation in the 'Legacy' folder,
                // which is basically the original code but with some
                // minor cleanup and adaptation to the 'Interface' folder.
                return new LUnit.Legacy.Shim(assembly);
            }
        }

        /// <summary>
        /// Ensure that NUnit is functional.
        /// </summary>
        private static void MakeSureNUnitReady()
        {
            NUnit.Framework.Assert.AreEqual(1, 1);
        }
    }
}