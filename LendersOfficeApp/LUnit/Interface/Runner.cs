﻿namespace LUnit.Interface
{
    /// <summary>
    /// Interface implemented by NUnit runners.  The methods conform closely
    /// to the pre-existing version of LUnit.
    /// </summary>
    public interface IRunner
    {
        /// <summary>
        /// Retrieve all the test fixtures that will be run.
        /// </summary>
        /// <returns>All the test fixtures that will be run.</returns>
        TestClassData[] GetTestFixtures();

        /// <summary>
        /// Run the test methods contained within the specified test fixture,
        /// or all test methods if fixtureId is null.
        /// </summary>
        /// <param name="fixtureId">The identifier for a test fixture, or null.</param>
        /// <returns>The results of running the test methods.</returns>
        TestClassResult RunFixture(string fixtureId);

        /// <summary>
        /// Run a single test method, as indicated with the specified fixture and test identifiers.
        /// </summary>
        /// <param name="fixtureId">The identifier for the test fixture containine the test method.</param>
        /// <param name="testId">The identifier for the test method.</param>
        /// <returns>The result of running the test method.</returns>
        TestClassResult RunSingleTest(string fixtureId, string testId);
    }
}
