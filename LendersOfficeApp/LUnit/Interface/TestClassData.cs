﻿namespace LUnit.Interface
{
    /// <summary>
    /// Encapsulate information about an NUnit test fixture.
    /// </summary>
    public sealed class TestClassData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestClassData"/> class.
        /// </summary>
        /// <param name="id">An Identifier for the test fixture.</param>
        /// <param name="name">The name for the test fixture.</param>
        /// <param name="namespace">The namespace for the test fixture.</param>
        /// <param name="fullName">The fullname for the test fixure.</param>
        /// <param name="isRunnable">True if the test fixture will be executed.</param>
        /// <param name="testMethods">The test methods contained in this test fixture.</param>
        public TestClassData(string id, string name, string @namespace, string fullName, bool isRunnable, TestMethodData[] testMethods)
        {
            this.Id = id;
            this.Name = name;
            this.Namespace = @namespace;
            this.FullName = fullName;
            this.IsRunnable = isRunnable;
            this.TestMethods = testMethods;
        }

        /// <summary>
        /// Gets an Identifier for the test fixture.
        /// </summary>
        /// <value>An Identifier for the test fixture.</value>
        public string Id { get; private set; }

        /// <summary>
        /// Gets the name for the test fixture.
        /// </summary>
        /// <value>The name for the test fixture.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the namespace for the test fixture.
        /// </summary>
        /// <value>The namespace for the test fixture.</value>
        public string Namespace { get; private set; }

        /// <summary>
        /// Gets the fullname for the test fixure.
        /// </summary>
        /// <value>The fullname for the test fixure.</value>
        public string FullName { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the test fixture will be executed.
        /// </summary>
        /// <value>A value indicating whether the test fixture will be executed.</value>
        public bool IsRunnable { get; private set; }

        /// <summary>
        /// Gets the test methods contained in this test fixture.
        /// </summary>
        /// <value>The test methods contained in this test fixture.</value>
        public TestMethodData[] TestMethods { get; private set; }
    }
}