﻿namespace LUnit.Interface
{
    /// <summary>
    /// Encapsulate information about an NUnit test method.
    /// </summary>
    public sealed class TestMethodData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestMethodData"/> class.
        /// </summary>
        /// <param name="id">An Identifier for the test fixture.</param>
        /// <param name="name">The name of the method.</param>
        /// <param name="friendlyName">The friendly name of the method.</param>
        /// <param name="isRunnable">True if the test method will be executed.</param>
        public TestMethodData(string id, string name, string friendlyName, bool isRunnable)
        {
            this.Id = id;
            this.Name = name;
            this.FriendlyName = friendlyName;
            this.IsRunnable = isRunnable;
        }

        /// <summary>
        /// Gets an Identifier for the test method.
        /// </summary>
        /// <value>An Identifier for the test method.</value>
        public string Id { get; private set; }

        /// <summary>
        /// Gets the name of the method.
        /// </summary>
        /// <value>The name of the method.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the friendly name of the method.
        /// </summary>
        /// <value>The friendly name of the method.</value>
        public string FriendlyName { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the test method will be executed.
        /// </summary>
        /// <value>A value indicating whether the test method will be executed.</value>
        public bool IsRunnable { get; private set; }
    }
}