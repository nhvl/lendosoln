﻿namespace LUnit
{
    using System;

    /// <summary>
    /// Enumeration that lists the various LUnit implementations.
    /// </summary>
    public enum LUnitType
    {
        /// <summary>
        /// Translation of the original code to support the LUnit.Interface and documented per StyleCop.
        /// </summary>
        Legacy,

        /// <summary>
        /// A complete re-write of LUnit that supports the LUnit.Interface and should be extendible to later versions of NUnit.
        /// </summary>
        Current
    }

    /// <summary>
    /// Factory for creating a shim for the web pages.
    /// </summary>
    public static class WebRunnerFactory
    {
        /// <summary>
        /// Create a shim for use by the web pages.
        /// </summary>
        /// <param name="type">The implementation of LUnit to use.</param>
        /// <returns>A shim for use by the web pages.</returns>
        public static WebShim.IWebRunner Create(LUnitType type)
        {
            switch (type)
            {
                case LUnitType.Legacy:
                    return CreateNewRunner(false);

                case LUnitType.Current:
                    return CreateNewRunner(true);

                default:
                    throw new Exception("Will never get here.");
            }
        }

        /// <summary>
        /// Create a web runner that wraps an interface runner.
        /// </summary>
        /// <param name="current">True if code in the Current folder should be used, else code in the Legacy folder will be used.</param>
        /// <returns>A shim for use by the web pages.</returns>
        private static WebShim.IWebRunner CreateNewRunner(bool current)
        {
            var runner = Interface.LUnitFactory.CreateRunner(current);
            return new Interface.WebRunner(runner);
        }
    }
}