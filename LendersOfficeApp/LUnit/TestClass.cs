using System;
using System.Collections;
using System.Reflection;
using System.Threading;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Constants;

namespace LUnit
{
    public class TestClass
    {
        public static bool ExpectedExceptionEnable = true;

        static ReflectionReader s_reader = new ReflectionReader();

        static int s_id = 0;
        static int GetGlobalId()
        {
            return s_id++;
        }


        Type m_fixtureType;
        string m_fullName = "";
        string m_name;
        int m_id = -1;

        TestMethod[] m_TestMethods;
        protected MethodInfo m_testSetUp;
        protected MethodInfo m_testTearDown;
        protected MethodInfo m_fixtureSetUp;
        protected MethodInfo m_fixtureTearDown;


        public TestMethod[] TestMethods
        {
            get { return m_TestMethods; }
        }

        public string FullName
        {
            get { return m_fullName; }
        }

        public string Name
        {
            get { return m_name; }
        }

        public int Id
        {
            get { return m_id; }
        }

        public string Namespace
        {
            get { return m_fixtureType.Namespace ?? ""; }
        }


        ConstructorInfo GetDefaultConstructor()
        {
            return m_fixtureType.GetConstructor(Type.EmptyTypes);
        }



        public TestClass(Type fixtureType)
        {
            m_fixtureType = fixtureType;
            m_fullName = fixtureType.FullName;
            m_name = fixtureType.Namespace != null
                ? m_fullName.Substring(m_fullName.LastIndexOf('.') + 1)
                : fixtureType.FullName;

            if (fixtureType.IsAbstract)
                m_name += " ( abstract class )";
            else if (GetDefaultConstructor() == null)
                m_name += " ( doesn't valid constructor)";
            else
            {
                m_id = GetGlobalId();
                m_testSetUp = s_reader.GetSetUpMethod(m_fixtureType);
                m_testTearDown = s_reader.GetTearDownMethod(m_fixtureType);
                m_fixtureSetUp = s_reader.GetFixtureSetUpMethod(m_fixtureType);
                m_fixtureTearDown = s_reader.GetFixtureTearDownMethod(m_fixtureType);
            }

            BuildTestMethods();
        }

        void BuildTestMethods()
        {
            ArrayList TestMethods = new ArrayList();

            foreach (MethodInfo method in s_reader.GetTestMethods(m_fixtureType))
            {
                string expectedExceptionName, expectedMessage;
                bool expecingException = s_reader.GetExpectedExceptionInfo(method, out expectedExceptionName, out expectedMessage);
                TestMethods.Add(new TestMethod(method, expectedExceptionName, expectedMessage, expecingException));
            }

            m_TestMethods = (TestMethod[])TestMethods.ToArray(typeof(TestMethod));
        }


        public void Run(ITestReport report, int methodIdx)
        {
            Run(report, methodIdx, null);

        }

        public void Run(ITestReport report, int methodIdx, object fixtureObj)
        {

            if (Id < 0)
                return;

            int start, end;

            if (methodIdx == int.MaxValue)
            {
                start = 0;
                end = TestMethods.Length;
            }
            else
            {
                start = methodIdx;
                end = methodIdx + 1;
            }

            if (start < 0 || start >= TestMethods.Length)
                return;

            report.TestClassStart(this);

            //int total  = 0;
            //int success = 0, fail = 0;

            int exceptionCount = 0;
            string alternateColor = "";
            string id = "";
            StringBuilder sb = null;
         

            int i;
            try
            {
                if (fixtureObj == null)
                {
                    ConstructorInfo ctor = GetDefaultConstructor();
                    fixtureObj = ctor.Invoke(Type.EmptyTypes);
                }

                if (m_fixtureSetUp != null)
                    m_fixtureSetUp.Invoke(fixtureObj, null);

            }
            catch (Exception exc)
            {
                exc = TestMethod.RemoveSomeException(exc);

                // MarkTestsFailed
                string msg = (fixtureObj == null) ? "Constructor fail" : "TestFixtureSetUp (one time setup) Failed";

                for (i = start; i < end; i++)
                    report.TestMethodFinish(this, i, ResultStatus.Fail, "fail");
                report.TestClassFinish(this, ResultStatus.Fail, msg + " : \r\n" + exc.ToString());

                // Log for Email
                if (sb != null)
                {
                    Guid searchStr = Guid.NewGuid();

                    string message = string.Format("{1}{0}Method Name: {3}{0}Method ID: {2}{0}Server: {5}{0}{4}", Environment.NewLine, id, searchStr, "TestFixtureSetUp", exc.ToString(), ConstAppDavid.ServerName);
                    Tools.LogError(message);

                    if (exceptionCount % 2 == 0)
                    {
                        sb.Append(makeEmailRow("Constructor fail", searchStr.ToString(), ""));
                    }
                    else
                    {
                        sb.Append(makeEmailRow("Constructor fail", searchStr.ToString(), alternateColor));
                    }
                }
                
                exceptionCount++;
                return;
            }


            bool isSuccess = true;
            for (i = start; i < end; i++)
            {
                Exception retException = null;
                try
                {
                    report.TestMethodStart(this, i);
                    retException = RunMethod(fixtureObj, TestMethods[i]);
                }
                catch (Exception exception)
                {
                    retException = TestMethod.RemoveSomeException(exception);
                }
                if (retException == null)
                {
                    report.TestMethodFinish(this, i, ResultStatus.Success, "pass");
                }
                else
                {
                    string errMsg = (retException is ShowMessageOnlyExc) ? retException.Message : retException.ToString();

                    report.TestMethodFinish(this, i, ResultStatus.Fail, errMsg);

                    // Log for email.
                    if (sb != null)
                    {
                        Guid searchStr = Guid.NewGuid();

                        string message = string.Format("{1}{0}Method Name: {3}{0}Method ID: {2}{0}Server: {5}{0}{4}", Environment.NewLine, id, searchStr, TestMethods[i].FriendlyName, errMsg, ConstAppDavid.ServerName);
                        Tools.LogError(message);

                        if (exceptionCount % 2 == 0)
                        {
                            sb.Append(makeEmailRow(TestMethods[i].FriendlyName, searchStr.ToString(), ""));
                        }
                        else
                        {
                            sb.Append(makeEmailRow(TestMethods[i].FriendlyName, searchStr.ToString(), alternateColor));
                        }
                    }
                    exceptionCount++;
                    isSuccess = false;
                }

            }

            try
            {
                if (m_fixtureTearDown != null)
                    m_fixtureTearDown.Invoke(fixtureObj, null);
            }
            catch (Exception tearDownException)
            {
                tearDownException = TestMethod.RemoveSomeException(tearDownException);
                string msg = "TestFixtureTearDown (one time teardown) fail : \r\n" + tearDownException.ToString();
                report.TestClassFinish(this, ResultStatus.Fail, msg);

                // Log for Email
                if (sb != null)
                {
                    Guid searchStr = Guid.NewGuid();

                    string message = string.Format("{1}{0}Method Name: {3}{0}Method ID: {2}{0}Server: {5}{0}{4}", Environment.NewLine, id, searchStr, "TestFixtureTearDown", tearDownException.ToString(), ConstAppDavid.ServerName);
                    Tools.LogError(message);

                    if (exceptionCount % 2 == 0)
                    {
                        sb.Append(makeEmailRow("TestFixtureTearDown", searchStr.ToString(), ""));
                    }
                    else
                    {
                        sb.Append(makeEmailRow("TestFixtureTearDown", searchStr.ToString(), alternateColor));
                    }
                }
                exceptionCount++;
                return;
            }

            if (isSuccess)
                report.TestClassFinish(this, ResultStatus.Success, "pass");
            else
                report.TestClassFinish(this, ResultStatus.Fail, "fail");
        }

        private string makeEmailRow(string name, string msg, string style)
        {
            return string.Format("<tr style='{2}'><td>{0}</td><td>{1}</td></tr>", name, msg, style);
        }

        Exception RunMethod(object fixtureObj, TestMethod testMethod)
        {
            return RunMethodProxy.RunMethod(fixtureObj, testMethod, m_testSetUp, m_testTearDown);
        }

        class RunMethodProxy
        {
            object m_fixtureObj;
            TestMethod m_testMethod;
            MethodInfo m_testSetUp;
            MethodInfo m_testTearDown;
            Exception m_retException;
            bool m_finish;

            RunMethodProxy(object fixtureObj, TestMethod testMethod,
                             MethodInfo testSetUp, MethodInfo testTearDown)
            {
                m_fixtureObj = fixtureObj;
                m_testMethod = testMethod;
                m_testSetUp = testSetUp;
                m_testTearDown = testTearDown;
            }

            void Run()
            {
                try
                {
                    m_retException = TestClass.RunMethodImp(m_fixtureObj, m_testMethod,
                                                             m_testSetUp, m_testTearDown);
                }
                catch (Exception exc)
                {
                    m_retException = TestMethod.RemoveSomeException(exc);
                }
                m_finish = true;
            }


            public static Exception RunMethod(object fixtureObj, TestMethod testMethod,
                                                  MethodInfo testSetUp, MethodInfo testTearDown)
            {
                if (testMethod.Timeout <= 0)
                    return TestClass.RunMethodImp(fixtureObj, testMethod, testSetUp, testTearDown);

                RunMethodProxy obj = new RunMethodProxy(fixtureObj, testMethod, testSetUp, testTearDown);
                Thread thread = new Thread(new ThreadStart(obj.Run));
                thread.Start();

                DateTime start = DateTime.Now;
                DateTime target = start.AddSeconds(testMethod.Timeout + 0.5);

                int count = 0;
                while (DateTime.Now < target && obj.m_finish == false)
                {
                    int remainMilliseconds = (int)(target - DateTime.Now).TotalMilliseconds;
                    if (remainMilliseconds <= 0)
                        break;
                    if (count++ < 10)
                    {
                        remainMilliseconds = 250;

                    }
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(remainMilliseconds >= 1500 ? 1500 : remainMilliseconds);
                }

                if (obj.m_finish)
                {
                    thread = null;
                    return obj.m_retException;
                }
                thread.Abort();
                int runTime = (int)(DateTime.Now - start).TotalSeconds;
                return new LUnitTimeoutException("Time out after " + runTime + " seconds");
            }
        }


        static Exception RunMethodImp(object fixtureObj, TestMethod testMethod,
                                      MethodInfo m_testSetUp, MethodInfo m_testTearDown)
        {
            Exception retException = null;
            try
            {
                if (m_testSetUp != null)
                    m_testSetUp.Invoke(fixtureObj, null);

                if (ExpectedExceptionEnable)
                    testMethod.Run(fixtureObj);
                else
                    testMethod.Method.Invoke(fixtureObj, null);
            }
            catch (Exception ex)
            {
                retException = TestMethod.RemoveSomeException(ex);
                //RecordException( ex, testResult );
            }
            finally
            {
                try
                {
                    if (m_testTearDown != null)
                        m_testTearDown.Invoke(fixtureObj, null);
                }
                catch (Exception ex)
                {
                    if (retException == null)
                        retException = TestMethod.RemoveSomeException(ex);
                    // TODO: What about ignore exceptions in teardown?
                    // RecordTearDownException(ex, testResult);
                }
            }
            return retException;

        }

        public static TestClass[] LoadTestClasses(Assembly assembly)
        {
            return assembly.GetTypes()
                .OrderBy(p => p.Namespace + "." + p.Name)
                .Where(testType => 
                    s_reader.IsTestFixture(testType) && 
                    s_reader.GetTestMethods(testType).Length != 0 &&
                    !testType.Name.Equals("DetectCatchAllExceptionTest", StringComparison.OrdinalIgnoreCase) &&
                    true)
                .Select(testType => new TestClass(testType))
                .ToArray();
        }
    }

}
