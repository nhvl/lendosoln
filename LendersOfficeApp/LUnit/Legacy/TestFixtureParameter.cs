﻿namespace LUnit.Legacy
{
    /// <summary>
    /// Container for information about a test fixture.
    /// </summary>
    public struct TestFixtureParameters
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestFixtureParameters"/> struct.
        /// </summary>
        /// <param name="requiredFramework">The name of the RequiredFramework attribute class.</param>
        /// <param name="namespace">The base namespace.</param>
        /// <param name="testFixtureType">The name of the TestFixture attribute class.</param>
        /// <param name="testFixturePattern">The name of the TestFixture pattern  property.</param>
        /// <param name="testCaseType">The name of the Test attribute class.</param>
        /// <param name="testCasePattern">The name of the Test pattern property.</param>
        /// <param name="expectedExceptionType">The name of the ExpectedException attribute class.</param>
        /// <param name="setUpType">The name of the SetUp attribute class.</param>
        /// <param name="tearDownType">The name of the TearDown attribute class.</param>
        /// <param name="fixtureSetUpType">The name of the FixtureSetUp attribute class.</param>
        /// <param name="fixtureTearDownType">The name of the FixtureTearDown attribute class.</param>
        /// <param name="ignoreType">The name of the Ignore attribute class.</param>
        /// <param name="categoryType">The name of the Category attribute class.</param>
        /// <param name="inheritTestFixtureType">A value indicating whether test fixtures can be inherited.</param>
        /// <param name="inheritTestCaseType">A value indicating whether test cases can be inherited.</param>
        /// <param name="inheritSetUpAndTearDownTypes">A value indicating whether setup and teardown types are inherited.</param>
        public TestFixtureParameters(
            string requiredFramework,
            string @namespace,
            string testFixtureType,
            string testFixturePattern,
            string testCaseType,
            string testCasePattern,
            string expectedExceptionType,
            string setUpType,
            string tearDownType,
            string fixtureSetUpType,
            string fixtureTearDownType,
            string ignoreType,
            string categoryType,
            bool inheritTestFixtureType,
            bool inheritTestCaseType,
            bool inheritSetUpAndTearDownTypes)
        {
            this.RequiredFramework = requiredFramework;
            this.TestFixtureType = @namespace + "." + testFixtureType;
            this.TestFixturePattern = testFixturePattern;
            this.TestCaseType = @namespace + "." + testCaseType;
            this.TestCasePattern = testCasePattern;
            this.ExpectedExceptionType = @namespace + "." + expectedExceptionType;
            this.SetUpType = @namespace + "." + setUpType;
            this.TearDownType = @namespace + "." + tearDownType;
            this.FixtureSetUpType = @namespace + "." + fixtureSetUpType;
            this.FixtureTearDownType = @namespace + "." + fixtureTearDownType;
            this.IgnoreType = @namespace + "." + ignoreType;
            this.CategoryType = @namespace + "." + categoryType;
            this.InheritTestFixtureType = inheritTestFixtureType;
            this.InheritTestCaseType = inheritTestCaseType;
            this.InheritSetUpAndTearDownTypes = inheritSetUpAndTearDownTypes;
        }

        /// <summary>
        /// Gets the name of the RequiredFramework attribute class.
        /// </summary>
        /// <value>The name of the RequiredFramework attribute class.</value>
        public string RequiredFramework { get; private set; }

        /// <summary>
        /// Gets the name of the TestFixture attribute class.
        /// </summary>
        /// <value>The name of the TestFixture attribute class.</value>
        public string TestFixtureType { get; private set; }

        /// <summary>
        /// Gets the name of the TestFixture pattern property.
        /// </summary>
        /// <value>The name of the TestFixture pattern  property.</value>
        public string TestFixturePattern { get; private set; }

        /// <summary>
        /// Gets the name of the Test attribute class.
        /// </summary>
        /// <value>The name of the Test attribute class.</value>
        public string TestCaseType { get; private set; }

        /// <summary>
        /// Gets the name of the Test pattern property.
        /// </summary>
        /// <value>The name of the Test pattern property.</value>
        public string TestCasePattern { get; private set; }

        /// <summary>
        /// Gets the name of the ExpectedException attribute class.
        /// </summary>
        /// <value>The name of the ExpectedException attribute class.</value>
        public string ExpectedExceptionType { get; private set; }

        /// <summary>
        /// Gets the name of the SetUp attribute class.
        /// </summary>
        /// <value>The name of the SetUp attribute class.</value>
        public string SetUpType { get; private set; }

        /// <summary>
        /// Gets the name of the TearDown attribute class.
        /// </summary>
        /// <value>The name of the TearDown attribute class.</value>
        public string TearDownType { get; private set; }

        /// <summary>
        /// Gets the name of the FixtureSetUp attribute class.
        /// </summary>
        /// <value>The name of the FixtureSetUp attribute class.</value>
        public string FixtureSetUpType { get; private set; }

        /// <summary>
        /// Gets the name of the FixtureTearDown attribute class.
        /// </summary>
        /// <value>The name of the FixtureTearDown attribute class.</value>
        public string FixtureTearDownType { get; private set; }

        /// <summary>
        /// Gets the name of the Ignore attribute class.
        /// </summary>
        /// <value>The name of the Ignore attribute class.</value>
        public string IgnoreType { get; private set; }

        /// <summary>
        /// Gets the name of the Category attribute class.
        /// </summary>
        /// <value>The name of the Category attribute class.</value>
        public string CategoryType { get; private set; }

        /// <summary>
        /// Gets a value indicating whether test fixtures can be inherited.
        /// </summary>
        /// <value>A value indicating whether test fixtures can be inherited.</value>
        public bool InheritTestFixtureType { get; private set; }

        /// <summary>
        /// Gets a value indicating whether test cases can be inherited.
        /// </summary>
        /// <value>A value indicating whether test cases can be inherited.</value>
        public bool InheritTestCaseType { get; private set; }

        /// <summary>
        /// Gets a value indicating whether setup and teardown types are inherited.
        /// </summary>
        /// <value>A value indicating whether setup and teardown types are inherited.</value>
        public bool InheritSetUpAndTearDownTypes { get; private set; }

        /// <summary>
        /// Gets a value indicating whether a valid required framework type has been specified.
        /// </summary>
        /// <value>A value indicating whether a valid required framework type has been specified.</value>
        public bool HasRequiredFramework
        {
            get { return this.IsValid(this.RequiredFramework); }
        }

        /// <summary>
        /// Gets a value indicating whether a valid test fixture type has been specified.
        /// </summary>
        /// <value>A value indicating whether a valid test fixture type has been specified.</value>
        public bool HasTestFixtureType
        {
            get { return this.IsValid(this.TestFixtureType); }
        }

        /// <summary>
        /// Gets a value indicating whether a valid test fixture pattern type has been specified.
        /// </summary>
        /// <value>A value indicating whether a valid test fixture pattern type has been specified.</value>
        public bool HasTestFixturePattern
        {
            get { return this.IsValid(this.TestFixturePattern); }
        }

        /// <summary>
        /// Gets a value indicating whether a valid test type has been specified.
        /// </summary>
        /// <value>A value indicating whether a valid test type has been specified.</value>
        public bool HasTestCaseType
        {
            get { return this.IsValid(this.TestCaseType); }
        }

        /// <summary>
        /// Gets a value indicating whether a valid test pattern type has been specified.
        /// </summary>
        /// <value>A value indicating whether a test pattern ignore type has been specified.</value>
        public bool HasTestCasePattern
        {
            get { return this.IsValid(this.TestCasePattern); }
        }

        /// <summary>
        /// Gets a value indicating whether a valid expected exception type has been specified.
        /// </summary>
        /// <value>A value indicating whether a valid expected exception type has been specified.</value>
        public bool HasExpectedExceptionType
        {
            get { return this.IsValid(this.ExpectedExceptionType); }
        }

        /// <summary>
        /// Gets a value indicating whether a valid setup type has been specified.
        /// </summary>
        /// <value>A value indicating whether a valid setup type has been specified.</value>
        public bool HasSetUpType
        {
            get { return this.IsValid(this.SetUpType); }
        }

        /// <summary>
        /// Gets a value indicating whether a valid teardown type has been specified.
        /// </summary>
        /// <value>A value indicating whether a valid teardown type has been specified.</value>
        public bool HasTearDownType
        {
            get { return this.IsValid(this.TearDownType); }
        }

        /// <summary>
        /// Gets a value indicating whether a valid fixture setup type has been specified.
        /// </summary>
        /// <value>A value indicating whether a valid fixture setup type has been specified.</value>
        public bool HasFixtureSetUpType
        {
            get { return this.IsValid(this.FixtureSetUpType); }
        }

        /// <summary>
        /// Gets a value indicating whether a valid fixture teardown type has been specified.
        /// </summary>
        /// <value>A value indicating whether a valid fixture teardown type has been specified.</value>
        public bool HasFixtureTearDownType
        {
            get { return this.IsValid(this.FixtureTearDownType); }
        }

        /// <summary>
        /// Gets a value indicating whether a valid ignore type has been specified.
        /// </summary>
        /// <value>A value indicating whether a valid ignore type has been specified.</value>
        public bool HasIgnoreType
        {
            get { return this.IsValid(this.IgnoreType); }
        }

        /// <summary>
        /// Validate a string to have content.
        /// </summary>
        /// <param name="s">The string getting validated.</param>
        /// <returns>True if the string has content, false otherwise.</returns>
        private bool IsValid(string s)
        {
            return s != null && s != string.Empty;
        }
    }
}