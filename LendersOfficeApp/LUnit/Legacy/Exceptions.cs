﻿namespace LUnit.Legacy
{
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Container class for the exceptions thrown by the LUnit runner.
    /// </summary>
    public static class Exceptions
    {
        /// <summary>
        /// Exception thrown when the GUI should only display a generic message.
        /// </summary>
        public class ShowMessageOnlyExc : CBaseException
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ShowMessageOnlyExc"/> class.
            /// </summary>
            /// <param name="msg">Message that is logged.</param>
            public ShowMessageOnlyExc(string msg) : base(ErrorMessages.Generic, msg)
            {
            }
        }

        /// <summary>
        /// Exception thrown when LUnit times out.
        /// </summary>
        public class LUnitTimeoutException : ShowMessageOnlyExc
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="LUnitTimeoutException"/> class.
            /// </summary>
            /// <param name="msg">Message that is logged.</param>
            public LUnitTimeoutException(string msg) : base(msg)
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="LUnitTimeoutException"/> class.
            /// </summary>
            public LUnitTimeoutException() : base("Time out")
            {
            }
        }
    }
}