﻿namespace LUnit.Legacy
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Email;
    using LUnit.Interface;

    /// <summary>
    /// A test report that generates and sends an email with the test results.
    /// </summary>
    public sealed class EmailTestReport : ITestReport
    {
        /// <summary>
        /// Collection of failures.
        /// </summary>
        private Dictionary<string, List<Tuple<string, string>>> failedResults;

        /// <summary>
        /// Count the failed tests.
        /// </summary>
        private int countFailedClassTest;

        /// <summary>
        /// Count the passed tests.
        /// </summary>
        private int countPassedClassTest;

        /// <summary>
        /// The email address where the report will be sent.
        /// </summary>
        private string emailToAddress;

        /// <summary>
        /// A description of the environment hosting the tests.
        /// </summary>
        private string environmentDescription;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTestReport"/> class.
        /// </summary>
        /// <param name="emailTo">Email address where the report is sent.</param>
        /// <param name="environmentDescription">Description of the environment hosting the tests.</param>
        public EmailTestReport(string emailTo, string environmentDescription)
        {
            this.failedResults = new Dictionary<string, List<Tuple<string, string>>>();
            this.countFailedClassTest = 0;
            this.countPassedClassTest = 0;
            this.emailToAddress = emailTo;
            this.environmentDescription = environmentDescription;
        }

        /// <summary>
        /// Gets all the results for running the tests.
        /// </summary>
        /// <value>All the results for running the tests.</value>
        public TestClassResult[] AllResults
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Called prior to running any tests.
        /// </summary>
        public void Start()
        {
        }

        /// <summary>
        /// Called after all tests have been run.
        /// </summary>
        public void Finish()
        {
            CBaseEmail cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid);
            CDateTime today = CDateTime.Create(DateTime.Today);
            cbe.To = ConstStage.SmokeTestEmailDestination;
            cbe.From = ConstStage.DefaultDoNotReplyAddress;

            if (this.countFailedClassTest == 0)
            {
                cbe.Subject = string.Format("Automated smoketest passed on {0} {{eom}}", this.environmentDescription);
                cbe.SendImmediately();
                return;
            }

            cbe.Subject = string.Format("Automated smoketest FAILED on {0}", this.environmentDescription);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("The following test failed:");

            foreach (KeyValuePair<string, List<Tuple<string, string>>> entry in this.failedResults.OrderBy(p => p.Key))
            {
                sb.AppendLine();
                sb.AppendLine(entry.Key);

                for (int i = 0; i < entry.Value.Count; i++)
                {
                    string name = entry.Value[i].Item1;
                    sb.AppendFormat("{0}. {1}{2}{3}{2}", i + 1, name, Environment.NewLine, entry.Value[i].Item2);
                }
            }

            cbe.Message = sb.ToString();
            cbe.SendImmediately();
        }

        /// <summary>
        /// Called prior to running a test fixture.
        /// </summary>
        /// <param name="testClass">The details of the test fixture.</param>
        public void TestClassStart(TestClass testClass)
        {
        }

        /// <summary>
        /// Called after running a test fixture.
        /// </summary>
        /// <param name="testClass">The details of the test fixture.</param>
        /// <param name="resultStatus">The result of running the test fixture.</param>
        /// <param name="message">A message returned by the test fixture run.</param>
        public void TestClassFinish(TestClass testClass, ResultStatus resultStatus, string message)
        {
        }

        /// <summary>
        /// Called prior to running a test method.
        /// </summary>
        /// <param name="testClass">The details of the containing test fixture.</param>
        /// <param name="testMethod">The details of the test method.</param>
        public void TestMethodStart(TestClass testClass, TestMethod testMethod)
        {
        }

        /// <summary>
        /// Called after running a test method.
        /// </summary>
        /// <param name="testClass">The details of the containing test fixture.</param>
        /// <param name="testMethod">The details of the test method.</param>
        /// <param name="resultStatus">The result of running the test method.</param>
        /// <param name="message">A message returned by the test method run.</param>
        public void TestMethodFinish(TestClass testClass, TestMethod testMethod, ResultStatus resultStatus, string message)
        {
            if (resultStatus != ResultStatus.Fail)
            {
                this.countPassedClassTest += 1;
                return;
            }

            List<Tuple<string, string>> testClassMethodResults;

            if (!this.failedResults.TryGetValue(testClass.FullName, out testClassMethodResults))
            {
                testClassMethodResults = new List<Tuple<string, string>>();
                this.failedResults.Add(testClass.FullName, testClassMethodResults);
            }

            Tools.LogWarning(string.Concat("AutomatedTestFailure ", testMethod.FullName, " Error :", Environment.NewLine, message));
            testClassMethodResults.Add(Tuple.Create(testMethod.Name, message));
            this.countFailedClassTest += 1;
        }
    }
}