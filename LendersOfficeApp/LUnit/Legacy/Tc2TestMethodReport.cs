﻿namespace LUnit.Legacy
{
    using System.Collections.Generic;
    using Interface;

    /// <summary>
    /// Reporter used by the Test Center.
    /// </summary>
    public sealed class Tc2TestMethodReport : ITestReport
    {
        /// <summary>
        /// List of test fixture results.
        /// </summary>
        private List<TestClassResult> testClassResults;

        /// <summary>
        /// List of test method results for a test fixture.
        /// </summary>
        private List<TestMethodResult> testMethodResults;

        /// <summary>
        /// Gets all the results for running the tests.
        /// </summary>
        /// <value>All the results for running the tests.</value>
        public TestClassResult[] AllResults
        {
            get
            {
                return this.testClassResults.ToArray();
            }
        }

        /// <summary>
        /// Called prior to running any tests.
        /// </summary>
        public void Start()
        {
        }

        /// <summary>
        /// Called after all tests have been run.
        /// </summary>
        public void Finish()
        {
        }

        /// <summary>
        /// Called prior to running a test fixture.
        /// </summary>
        /// <param name="testClass">The details of the test fixture.</param>
        public void TestClassStart(TestClass testClass)
        {
            this.testClassResults = new List<TestClassResult>();
            this.testMethodResults = new List<TestMethodResult>();
        }

        /// <summary>
        /// Called after running a test fixture.
        /// </summary>
        /// <param name="testClass">The details of the test fixture.</param>
        /// <param name="resultStatus">The result of running the test fixture.</param>
        /// <param name="message">A message returned by the test fixture run.</param>
        public void TestClassFinish(TestClass testClass, ResultStatus resultStatus, string message)
        {
            var data = testClass.Convert();
            var tcr = new TestClassResult(data, resultStatus, message, this.testMethodResults.ToArray());
            this.testClassResults.Add(tcr);
        }

        /// <summary>
        /// Called prior to running a test method.
        /// </summary>
        /// <param name="testClass">The details of the containing test fixture.</param>
        /// <param name="testMethod">The details of the test method.</param>
        public void TestMethodStart(TestClass testClass, TestMethod testMethod)
        {
        }

        /// <summary>
        /// Called after running a test method.
        /// </summary>
        /// <param name="testClass">The details of the containing test fixture.</param>
        /// <param name="testMethod">The details of the test method.</param>
        /// <param name="resultStatus">The result of running the test method.</param>
        /// <param name="message">A message returned by the test method run.</param>
        public void TestMethodFinish(TestClass testClass, TestMethod testMethod, ResultStatus resultStatus, string message)
        {
            var data = testMethod.Convert();
            var tmr = new TestMethodResult(data, resultStatus, message);
            this.testMethodResults.Add(tmr);
        }
    }
}