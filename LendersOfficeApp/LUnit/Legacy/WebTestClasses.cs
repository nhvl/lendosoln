﻿namespace LUnit.Legacy
{
    using System.Linq;
    using System.Reflection;
    using System.Security.Principal;
    using System.Text;
    using System.Web;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Entry point for the legacy version of LUnit.
    /// </summary>
    public sealed class WebTestClasses
    {
        /// <summary>
        /// Indicates whether the fulll set of tests are being run.
        /// </summary>
        private static volatile bool isRunningFull = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebTestClasses"/> class.
        /// </summary>
        /// <param name="assembly">An assembly containing unit tests.</param>
        public WebTestClasses(Assembly assembly)
        {
            this.TestClasses = TestClass.LoadTestClasses(assembly);
        }

        /// <summary>
        /// Gets the set of test fixtures available to run.
        /// </summary>
        /// <value>The set of test fixtures available to run.</value>
        public TestClass[] TestClasses { get; private set; }

        /// <summary>
        /// Pass out the test information in an appropriate format.
        /// </summary>
        /// <returns>The test information in an appropriate format.</returns>
        public LUnit.Interface.TestClassData[] Export()
        {
            return this.TestClasses.Select(c => c.Convert()).ToArray();
        }

        /// <summary>
        /// Run all the unit tests and add the results to the input report.
        /// </summary>
        /// <param name="report">The input report.</param>
        public void RunAll(ITestReport report)
        {
            if (WebTestClasses.isRunningFull)
            {
                Tools.LogInfo("Not running unit test again. Since it appears one is already running");
                return;
            }

            WebTestClasses.isRunningFull = true;

            IPrincipal orgPrincipal = System.Threading.Thread.CurrentPrincipal;
            string description = System.Environment.MachineName;

            if (HttpContext.Current != null)
            {
                description += "-" + HttpContext.Current.Request.Url.Host;
            }

            try
            {
                report.Start();
                for (int testClassIdx = 0; testClassIdx < this.TestClasses.Length; testClassIdx++)
                {
                    try
                    {
                        Tools.LogInfo("Running test class " + this.TestClasses[testClassIdx].FullName + " number " + testClassIdx + " out of " + this.TestClasses.Length);
                        this.RunTestClass(report, testClassIdx, int.MaxValue);
                    }
                    finally
                    {
                        System.Threading.Thread.CurrentPrincipal = orgPrincipal;
                    }
                }

                report.Finish();
            }
            finally
            {
                System.Threading.Thread.CurrentPrincipal = orgPrincipal;
                WebTestClasses.isRunningFull = false;
            }
        }

        /// <summary>
        /// Run unit tests according to the input indices and add the results to the input report.
        /// </summary>
        /// <param name="report">The input report.</param>
        /// <param name="testClassIdx">The index of the fixture.</param>
        /// <param name="methodIdx">The index of the method.</param>
        public void Run(ITestReport report, int testClassIdx, int methodIdx)
        {
            report.Start();
            this.RunTestClass(report, testClassIdx, methodIdx);
            report.Finish();
        }

        /// <summary>
        /// Run the tests contained in a test fixture and report the results.
        /// </summary>
        /// <param name="report">The object to which the results are reported.</param>
        /// <param name="testClassIdx">Index referencing the test fixture.</param>
        /// <param name="methodIdx">Index referencing a test method.</param>
        private void RunTestClass(ITestReport report, int testClassIdx, int methodIdx)
        {
            TestClass testClass = this.TestClasses[testClassIdx];
            testClass.Run(report, methodIdx, null);
        }
    }
}