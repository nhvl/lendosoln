﻿namespace LUnit.Legacy
{
    using System;
    using LUnit.Interface;

    /// <summary>
    /// Interface used by a test runner to record the results of running tests.
    /// </summary>
    public interface ITestReport
    {
        /// <summary>
        /// Gets all the results for running the tests.
        /// </summary>
        /// <value>All the results for running the tests.</value>
        TestClassResult[] AllResults { get; }

        /// <summary>
        /// Called prior to running any tests.
        /// </summary>
        void Start();

        /// <summary>
        /// Called after all tests have been run.
        /// </summary>
        void Finish();

        /// <summary>
        /// Called prior to running a test fixture.
        /// </summary>
        /// <param name="testClass">The details of the test fixture.</param>
        void TestClassStart(TestClass testClass);

        /// <summary>
        /// Called after running a test fixture.
        /// </summary>
        /// <param name="testClass">The details of the test fixture.</param>
        /// <param name="resultStatus">The result of running the test fixture.</param>
        /// <param name="message">A message returned by the test fixture run.</param>
        void TestClassFinish(TestClass testClass, ResultStatus resultStatus, string message);

        /// <summary>
        /// Called prior to running a test method.
        /// </summary>
        /// <param name="testClass">The details of the containing test fixture.</param>
        /// <param name="testMethod">The details of the test method.</param>
        void TestMethodStart(TestClass testClass, TestMethod testMethod);

        /// <summary>
        /// Called after running a test method.
        /// </summary>
        /// <param name="testClass">The details of the containing test fixture.</param>
        /// <param name="testMethod">The details of the test method.</param>
        /// <param name="resultStatus">The result of running the test method.</param>
        /// <param name="message">A message returned by the test method run.</param>
        void TestMethodFinish(TestClass testClass, TestMethod testMethod, ResultStatus resultStatus, string message);
    }
}