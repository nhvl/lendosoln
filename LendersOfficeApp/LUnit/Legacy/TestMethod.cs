﻿namespace LUnit.Legacy
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Encapsulate the information for a test method.
    /// </summary>
    public class TestMethod
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestMethod"/> class.
        /// </summary>
        /// <param name="method">The reflected method information.</param>
        /// <param name="expectedExceptionName">The name of the expected exception type.</param>
        /// <param name="expectedMessage">The expected exception message.</param>
        /// <param name="expectingException">A value indicating whether the method is expected to throw an exception.</param>
        public TestMethod(MethodInfo method, string expectedExceptionName, string expectedMessage, bool expectingException)
        {
            this.Name = method.DeclaringType == method.ReflectedType
                ? method.Name : method.DeclaringType.Name + "." + method.Name;
            this.FullName = method.ReflectedType.FullName + "." + method.Name;
            this.Method = method;

            int timeout;
            this.Description = ReflectionReader.GetDescription(method, out timeout);
            this.Timeout = timeout;
            this.ExpectedExceptionName = expectedExceptionName;
            this.ExpectedMessage = expectedMessage;
            this.ExpectingException = expectingException;
        }

        /// <summary>
        /// Gets or sets the identifier for this method.
        /// </summary>
        /// <value>The identifier for this method.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets a friendly name for a test method.
        /// </summary>
        /// <value>A friendly name for a test method.</value>
        public string FriendlyName
        {
            get
            {
                if (string.IsNullOrEmpty(this.Description))
                {
                    return this.Name;
                }
                else
                {
                    return this.Description;
                }
            }
        }

        /// <summary>
        /// Gets the fullname of the method.
        /// </summary>
        /// <value>The fullname of the method.</value>
        public string FullName { get; private set; }

        /// <summary>
        /// Gets the name of the method.
        /// </summary>
        /// <value>The name of the method.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the reflected method information.
        /// </summary>
        /// <value>The reflected method information.</value>
        public MethodInfo Method { get; private set; }

        /// <summary>
        /// Gets the timeout for the method.
        /// </summary>
        /// <value>The timeout for the method.</value>
        public int Timeout { get; private set; }

        /// <summary>
        /// Gets or sets the description for the method.
        /// </summary>
        /// <value>The description for the method.</value>
        private string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the method is expected to throw an exception.
        /// </summary>
        /// <value>A value indicating whether the method is expected to throw an exception.</value>
        private bool ExpectingException { get; set; }

        /// <summary>
        /// Gets or sets the name of the expected exception type.
        /// </summary>
        /// <value>The name of the expected exception type.</value>
        private string ExpectedExceptionName { get; set; }

        /// <summary>
        /// Gets or sets the expected exception message.
        /// </summary>
        /// <value>The expected exception message.</value>
        private string ExpectedMessage { get; set; }

        /// <summary>
        /// Extract the exception thrown by a test method, which may be wrapped by several
        /// layers of Reflection, NUnit and LUnit infrastructure exceptions.
        /// </summary>
        /// <param name="ex">A caught exception which may be wrapped several layers deep.</param>
        /// <returns>An exception thrown by a test method.</returns>
        public static Exception RemoveSomeException(Exception ex)
        {
            if (ex == null)
            {
                return null;
            }

            if (ex is TargetInvocationException)
            {
                return ex.InnerException;
            }

            while (ex.GetType().FullName == "NUnit.Core.NunitException")
            {
                if (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }
                else
                {
                    break;
                }
            }

            while (ex is LUnitException)
            {
                if (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }
                else
                {
                    break;
                }
            }

            return ex;
        }

        /// <summary>
        /// Convert this legacy TestMethod instance into a interface TestMethodData instance.
        /// </summary>
        /// <returns>An instance of the interface TestMethodData.</returns>
        public LUnit.Interface.TestMethodData Convert()
        {
            return new LUnit.Interface.TestMethodData(this.Id.ToString(), this.Name, this.FriendlyName, true);
        }

        /// <summary>
        /// Execute a test method.
        /// </summary>
        /// <param name="fixtureObj">The instance of the containing class.</param>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Unit test runners much catch and report on errors, rather than propagate them.")]
        public void Run(object fixtureObj)
        {
            Exception retException = null;
            try
            {
                this.Method.Invoke(fixtureObj, null);
            }
            catch (Exception ex)
            {
                retException = RemoveSomeException(ex);
            }

            this.ProcessException(retException);
        }

        /// <summary>
        /// Determinew whether the exception is of the expected type.
        /// </summary>
        /// <param name="exception">The exception to check.</param>
        /// <returns>True if the exception is of the expected type, false otherwise.</returns>
        private bool IsExpectedExceptionType(Exception exception)
        {
            if (this.ExpectingException)
            {
                if (!string.IsNullOrEmpty(this.ExpectedExceptionName))
                {
                    return this.ExpectedExceptionName.Equals(exception.GetType().FullName);
                }
                else
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Retrieve the stack trace from an exception.
        /// </summary>
        /// <param name="exception">The excepton.</param>
        /// <returns>The stack trace.</returns>
        private string GetStackTrace(Exception exception)
        {
            try
            {
                return exception.StackTrace;
            }
            catch (Exception)
            {
                return "No stack trace available";
            }
        }

        /// <summary>
        /// If an exception is expected then it is swallowed, otherwise it is wrapped into a CBaseException and thrown.
        /// </summary>
        /// <param name="exception">The exception to process.</param>
        private void ProcessException(Exception exception)
        {
            // no exception happened
            if (exception == null)
            {
                if (this.ExpectingException == false)
                {
                    return;
                }

                string expectedType = this.ExpectedExceptionName == null ? "An Exception" : this.ExpectedExceptionName;
                throw new CBaseException(ErrorMessages.Generic, expectedType + " was expected");
            }

            // exception happened
            if (this.ExpectingException == false)
            {
                throw new LUnitException("Don't expect any exception", exception);
            }

            if (this.IsExpectedExceptionType(exception))
            {
                if (this.ExpectedMessage != null && !this.ExpectedMessage.Equals(exception.Message))
                {
                    string message = string.Format(
                        "Expected exception to have message: \"{0}\" but received message \"{1}\"",
                        this.ExpectedMessage,
                        exception.Message);

                    throw new CBaseException(ErrorMessages.Generic, message + " " + exception.ToString());
                }
                else
                {
                    return;
                }
            }
            else
            {
                string message = "Expected: " + this.ExpectedExceptionName + " but was " + exception.GetType().FullName;
                throw new CBaseException(ErrorMessages.Generic, message + " " + exception.ToString());
            }
        }

        /// <summary>
        /// An exception class exclusive to this class.
        /// </summary>
        private class LUnitException : CBaseException
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="LUnitException"/> class.
            /// </summary>
            /// <param name="message">The error message.</param>
            /// <param name="inner">An exception wrapped by this exception.</param>
            public LUnitException(string message, Exception inner) :
                base(ErrorMessages.Generic, message + ".\r\n" + inner)
            {
            }
        }
    }
}