﻿namespace LUnit.Legacy
{
    using System;
    using System.Collections;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading;
    using DataAccess;
    using LendersOffice.Constants;
    using LUnit.Interface;

    /// <summary>
    /// Encapsulate the information for a test fixture.
    /// </summary>
    public sealed class TestClass
    {
        /// <summary>
        /// Maintain a monotonically increasing value that is used to assign identifiers to classes.
        /// </summary>
        private static int currentId;

        /// <summary>
        /// Initializes static members of the <see cref="TestClass" /> class.
        /// </summary>
        static TestClass()
        {
            currentId = -1;
            ExpectedExceptionEnable = true;
            Reader = new ReflectionReader();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestClass"/> class.
        /// </summary>
        /// <param name="fixtureType">The type of the class that is the test fixture under examination.</param>
        public TestClass(Type fixtureType)
        {
            this.Id = -1;
            this.FixtureType = fixtureType;
            this.FullName = fixtureType.FullName;
            this.Name = fixtureType.Namespace != null
                ? this.FullName.Substring(this.FullName.LastIndexOf('.') + 1)
                : fixtureType.FullName;

            if (fixtureType.IsAbstract)
            {
                this.Name += " ( abstract class )";
            }
            else if (this.GetDefaultConstructor() == null)
            {
                this.Name += " ( doesn't valid constructor)";
            }
            else
            {
                this.Id = GetGlobalId();
                this.TestSetUp = Reader.GetSetUpMethod(this.FixtureType);
                this.TestTearDown = Reader.GetTearDownMethod(this.FixtureType);
                this.FixtureSetUp = Reader.GetFixtureSetUpMethod(this.FixtureType);
                this.FixtureTearDown = Reader.GetFixtureTearDownMethod(this.FixtureType);
            }

            this.BuildTestMethods();
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not to work with expected exceptions.
        /// </summary>
        /// <value>An indicator of whether or not to work with expected exceptions.</value>
        public static bool ExpectedExceptionEnable { get; set; }

        /// <summary>
        /// Gets the test methods contained in this test fixture.
        /// </summary>
        /// <value>The test methods contained in this test fixture.</value>
        public TestMethod[] TestMethods { get; private set; }

        /// <summary>
        /// Gets the fullname of the class that is this test fixture.
        /// </summary>
        /// <value>The fullname of the class that is this test fixture.</value>
        public string FullName { get; private set; }

        /// <summary>
        /// Gets the name of the class that is this test fixture.
        /// </summary>
        /// <value>The name of the class that is this test fixture.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets an identifier for this test fixture.
        /// </summary>
        /// <value>An identifier for this test fixture.</value>
        public int Id { get; private set; }

        /// <summary>
        /// Gets the namespace for this test fixture.
        /// </summary>
        /// <value>The namespace for this test fixture.</value>
        public string Namespace
        {
            get { return this.FixtureType.Namespace ?? string.Empty; }
        }

        /// <summary>
        /// Gets or sets the reader used to extract data from an assembly.
        /// </summary>
        /// <value>The reader used to extract data from an assembly.</value>
        private static ReflectionReader Reader { get; set; }

        /// <summary>
        /// Gets or sets the method that is run prior to each test method.
        /// </summary>
        /// <value>The method that is run prior to each test method.</value>
        private MethodInfo TestSetUp { get; set; }

        /// <summary>
        /// Gets or sets the method that is run after each test method.
        /// </summary>
        /// <value>The method that is run after each test method.</value>
        private MethodInfo TestTearDown { get; set; }

        /// <summary>
        /// Gets or sets the method that is run once prior to any test methods.
        /// </summary>
        /// <value>The method that is run once prior to any test methods.</value>
        private MethodInfo FixtureSetUp { get; set; }

        /// <summary>
        /// Gets or sets the method that is run once after all test methods have run.
        /// </summary>
        /// <value>The method that is run once after all test methods have run.</value>
        private MethodInfo FixtureTearDown { get; set; }

        /// <summary>
        /// Gets or sets the type of the class that is this test fixture.
        /// </summary>
        /// <value>The type of the class that is this test fixture.</value>
        private Type FixtureType { get; set; }

        /// <summary>
        /// Use the reader to collect retrieve all the test classes from an assembly.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>The test classes.</returns>
        public static TestClass[] LoadTestClasses(Assembly assembly)
        {
            return assembly.GetTypes()
                .OrderBy(p => p.Namespace + "." + p.Name)
                .Where(testType =>
                    Reader.IsTestFixture(testType) &&
                    Reader.GetTestMethods(testType).Length != 0 &&
                    !testType.Name.Equals("DetectCatchAllExceptionTest", StringComparison.OrdinalIgnoreCase) &&
                    true)
                .Select(testType => new TestClass(testType))
                .ToArray();
        }

        /// <summary>
        /// Convert this legacy TestClass instance into a interface TestClassData instance.
        /// </summary>
        /// <returns>An instance of the interface TestClassData.</returns>
        public TestClassData Convert()
        {
            return new TestClassData(this.Id.ToString(), this.Name, this.Namespace, this.FullName, true, this.TestMethods.Select(testMethod => testMethod.Convert()).ToArray());
        }

        /// <summary>
        /// Run one or more test methods within this test fixture.
        /// </summary>
        /// <param name="report">The report where the results are recorded.</param>
        /// <param name="methodIdx">The identifier for the method to run, or if int.Max all with run.</param>
        /// <param name="fixtureObj">An instance of the class that is this test fixture.  If null then an instance will be created.</param>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Unit test runners much catch and report on errors, rather than propagate them.")]
        public void Run(ITestReport report, int methodIdx, object fixtureObj)
        {
            if (this.Id < 0)
            {
                return;
            }

            int start, end;

            if (methodIdx == int.MaxValue)
            {
                start = 0;
                end = this.TestMethods.Length;
            }
            else
            {
                start = methodIdx;
                end = methodIdx + 1;
            }

            if (start < 0 || start >= this.TestMethods.Length)
            {
                return;
            }

            report.TestClassStart(this);

            int exceptionCount = 0;
            string alternateColor = string.Empty;
            string id = string.Empty;
            StringBuilder sb = null;

            try
            {
                if (fixtureObj == null)
                {
                    ConstructorInfo ctor = this.GetDefaultConstructor();
                    fixtureObj = ctor.Invoke(Type.EmptyTypes);
                }

                if (this.FixtureSetUp != null)
                {
                    this.FixtureSetUp.Invoke(fixtureObj, null);
                }
            }
            catch (Exception exc)
            {
                exc = TestMethod.RemoveSomeException(exc);

                // MarkTestsFailed
                string msg = (fixtureObj == null) ? "Constructor fail" : "TestFixtureSetUp (one time setup) Failed";

                for (int i = start; i < end; i++)
                {
                    report.TestMethodFinish(this, this.TestMethods[i], ResultStatus.Fail, "fail");
                }

                report.TestClassFinish(this, ResultStatus.Fail, msg + " : \r\n" + exc.ToString());

                // Log for Email
                if (sb != null)
                {
                    Guid searchStr = Guid.NewGuid();

                    string message = string.Format("{1}{0}Method Name: {3}{0}Method ID: {2}{0}Server: {5}{0}{4}", Environment.NewLine, id, searchStr, "TestFixtureSetUp", exc.ToString(), ConstAppDavid.ServerName);
                    Tools.LogError(message);

                    if (exceptionCount % 2 == 0)
                    {
                        sb.Append(this.MakeEmailRow("Constructor fail", searchStr.ToString(), string.Empty));
                    }
                    else
                    {
                        sb.Append(this.MakeEmailRow("Constructor fail", searchStr.ToString(), alternateColor));
                    }
                }

                exceptionCount++;
                return;
            }

            bool isSuccess = true;
            for (int i = start; i < end; i++)
            {
                Exception retException = null;
                try
                {
                    report.TestMethodStart(this, this.TestMethods[i]);
                    retException = this.RunMethod(fixtureObj, this.TestMethods[i]);
                }
                catch (Exception exception)
                {
                    retException = TestMethod.RemoveSomeException(exception);
                }

                if (retException == null)
                {
                    report.TestMethodFinish(this, this.TestMethods[i], ResultStatus.Success, "pass");
                }
                else
                {
                    string errMsg = (retException is Exceptions.ShowMessageOnlyExc) ? retException.Message : retException.ToString();

                    report.TestMethodFinish(this, this.TestMethods[i], ResultStatus.Fail, errMsg);

                    // Log for email.
                    if (sb != null)
                    {
                        Guid searchStr = Guid.NewGuid();

                        string message = string.Format("{1}{0}Method Name: {3}{0}Method ID: {2}{0}Server: {5}{0}{4}", Environment.NewLine, id, searchStr, this.TestMethods[i].FriendlyName, errMsg, ConstAppDavid.ServerName);
                        Tools.LogError(message);

                        if (exceptionCount % 2 == 0)
                        {
                            sb.Append(this.MakeEmailRow(this.TestMethods[i].FriendlyName, searchStr.ToString(), string.Empty));
                        }
                        else
                        {
                            sb.Append(this.MakeEmailRow(this.TestMethods[i].FriendlyName, searchStr.ToString(), alternateColor));
                        }
                    }

                    exceptionCount++;
                    isSuccess = false;
                }
            }

            try
            {
                if (this.FixtureTearDown != null)
                {
                    this.FixtureTearDown.Invoke(fixtureObj, null);
                }
            }
            catch (Exception tearDownException)
            {
                tearDownException = TestMethod.RemoveSomeException(tearDownException);
                string msg = "TestFixtureTearDown (one time teardown) fail : \r\n" + tearDownException.ToString();
                report.TestClassFinish(this, ResultStatus.Fail, msg);

                // Log for Email
                if (sb != null)
                {
                    Guid searchStr = Guid.NewGuid();

                    string message = string.Format("{1}{0}Method Name: {3}{0}Method ID: {2}{0}Server: {5}{0}{4}", Environment.NewLine, id, searchStr, "TestFixtureTearDown", tearDownException.ToString(), ConstAppDavid.ServerName);
                    Tools.LogError(message);

                    if (exceptionCount % 2 == 0)
                    {
                        sb.Append(this.MakeEmailRow("TestFixtureTearDown", searchStr.ToString(), string.Empty));
                    }
                    else
                    {
                        sb.Append(this.MakeEmailRow("TestFixtureTearDown", searchStr.ToString(), alternateColor));
                    }
                }

                exceptionCount++;
                return;
            }

            if (isSuccess)
            {
                report.TestClassFinish(this, ResultStatus.Success, "pass");
            }
            else
            {
                report.TestClassFinish(this, ResultStatus.Fail, "fail");
            }
        }

        /// <summary>
        /// Retrieve the next identifier value.
        /// </summary>
        /// <returns>The next identifier value.</returns>
        private static int GetGlobalId()
        {
            return System.Threading.Interlocked.Increment(ref currentId);
        }

        /// <summary>
        /// Run a test method.
        /// </summary>
        /// <param name="fixtureObj">An instance of a class that is the test fixture.</param>
        /// <param name="testMethod">The test method.</param>
        /// <param name="testSetUp">The setup method of the test fixture.</param>
        /// <param name="testTearDown">The teardown method of the test fixture.</param>
        /// <returns>An exception if one was thrown by the test method.</returns>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Unit test runners much catch and report on errors, rather than propagate them.")]
        private static Exception RunMethodImp(
            object fixtureObj,
            TestMethod testMethod,
            MethodInfo testSetUp,
            MethodInfo testTearDown)
        {
            Exception retException = null;
            try
            {
                if (testSetUp != null)
                {
                    testSetUp.Invoke(fixtureObj, null);
                }

                if (ExpectedExceptionEnable)
                {
                    testMethod.Run(fixtureObj);
                }
                else
                {
                    testMethod.Method.Invoke(fixtureObj, null);
                }
            }
            catch (Exception ex)
            {
                retException = TestMethod.RemoveSomeException(ex);
            }
            finally
            {
                try
                {
                    if (testTearDown != null)
                    {
                        testTearDown.Invoke(fixtureObj, null);
                    }
                }
                catch (Exception ex)
                {
                    if (retException == null)
                    {
                        retException = TestMethod.RemoveSomeException(ex);
                        //// TODO: What about ignore exceptions in teardown?
                        //// RecordTearDownException(ex, testResult);
                    }
                }
            }

            return retException;
        }

        /// <summary>
        /// Retrieve the method that is the default constructor for the class that is this test fixture.
        /// </summary>
        /// <returns>The constructor.</returns>
        private ConstructorInfo GetDefaultConstructor()
        {
            return this.FixtureType.GetConstructor(Type.EmptyTypes);
        }

        /// <summary>
        /// Generate the test methods that are contained within this test fixture.
        /// </summary>
        private void BuildTestMethods()
        {
            ArrayList testMethods = new ArrayList();

            foreach (MethodInfo method in Reader.GetTestMethods(this.FixtureType))
            {
                string expectedExceptionName, expectedMessage;
                bool expecingException = Reader.GetExpectedExceptionInfo(method, out expectedExceptionName, out expectedMessage);
                testMethods.Add(new TestMethod(method, expectedExceptionName, expectedMessage, expecingException));
            }

            this.TestMethods = (TestMethod[])testMethods.ToArray(typeof(TestMethod));
            for (int i = 0; i < this.TestMethods.Length; ++i)
            {
                this.TestMethods[i].Id = i;
            }
        }

        /// <summary>
        /// Generate an html string that represents a row in a table, for an email report of the run results.
        /// </summary>
        /// <param name="name">The test method name.</param>
        /// <param name="msg">The message from the test method.</param>
        /// <param name="style">They css style to apply to the table row.</param>
        /// <returns>The html string.</returns>
        private string MakeEmailRow(string name, string msg, string style)
        {
            return string.Format("<tr style='{2}'><td>{0}</td><td>{1}</td></tr>", name, msg, style);
        }

        /// <summary>
        /// Run a test method via the proxy, which can handle a non-zero timeout.
        /// </summary>
        /// <param name="fixtureObj">An instance of the class that is this test fixture.</param>
        /// <param name="testMethod">The test method.</param>
        /// <returns>An exception if the test method throws one, null otherwise.</returns>
        private Exception RunMethod(object fixtureObj, TestMethod testMethod)
        {
            return RunMethodProxy.RunMethod(fixtureObj, testMethod, this.TestSetUp, this.TestTearDown);
        }

        /// <summary>
        /// Class used to run test methods that can be terminated based upon a timeout value.
        /// </summary>
        private class RunMethodProxy
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="RunMethodProxy"/> class.
            /// </summary>
            /// <param name="fixtureObj">An instance of the class that is the test fixture containing the test method.</param>
            /// <param name="testMethod">The test method.</param>
            /// <param name="testSetUp">The setup method.</param>
            /// <param name="testTearDown">The teardown method.</param>
            public RunMethodProxy(
                object fixtureObj,
                TestMethod testMethod,
                MethodInfo testSetUp,
                MethodInfo testTearDown)
            {
                this.FixtureObject = fixtureObj;
                this.TestMethod = testMethod;
                this.TestSetUp = testSetUp;
                this.TestTearDown = testTearDown;
            }

            /// <summary>
            /// Gets or sets an instance of the class that is the test fixture containing the test method.
            /// </summary>
            /// <value>An instance of the class that is the test fixture containing the test method.</value>
            private object FixtureObject { get; set; }

            /// <summary>
            /// Gets or sets the test method.
            /// </summary>
            /// <value>The test method.</value>
            private TestMethod TestMethod { get; set; }

            /// <summary>
            /// Gets or sets the setup method.
            /// </summary>
            /// <value>The setup method.</value>
            private MethodInfo TestSetUp { get; set; }

            /// <summary>
            /// Gets or sets the teardown method.
            /// </summary>
            /// <value>The teardown method.</value>
            private MethodInfo TestTearDown { get; set; }

            /// <summary>
            /// Gets or sets an exception thrown by the test method.
            /// </summary>
            /// <value>An exception thrown by the test method.</value>
            private Exception CaughtException { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the test method finished without a timeout.
            /// </summary>
            private bool Finished { get; set; }

            /// <summary>
            /// Run the test method and ensure that the timeout value is handled.
            /// </summary>
            /// <param name="fixtureObj">An instance of the class that is the test fixture containing the test method.</param>
            /// <param name="testMethod">The test method.</param>
            /// <param name="testSetUp">The setup method.</param>
            /// <param name="testTearDown">The teardown method.</param>
            /// <returns>An exception if one was thrown by the test method.</returns>
            public static Exception RunMethod(
                object fixtureObj,
                TestMethod testMethod,
                MethodInfo testSetUp,
                MethodInfo testTearDown)
            {
                if (testMethod.Timeout <= 0)
                {
                    return TestClass.RunMethodImp(fixtureObj, testMethod, testSetUp, testTearDown);
                }

                RunMethodProxy obj = new RunMethodProxy(fixtureObj, testMethod, testSetUp, testTearDown);
                Thread thread = new Thread(obj.Run);
                thread.Start();

                DateTime start = DateTime.Now;
                DateTime target = start.AddSeconds(testMethod.Timeout + 0.5);

                int count = 0;
                while (DateTime.Now < target && obj.Finished == false)
                {
                    int remainMilliseconds = (int)(target - DateTime.Now).TotalMilliseconds;
                    if (remainMilliseconds <= 0)
                    {
                        break;
                    }

                    if (count++ < 10)
                    {
                        remainMilliseconds = 250;
                    }

                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(remainMilliseconds >= 1500 ? 1500 : remainMilliseconds);
                }

                if (obj.Finished)
                {
                    thread = null;
                    return obj.CaughtException;
                }

                thread.Abort();
                int runTime = (int)(DateTime.Now - start).TotalSeconds;
                return new Exceptions.LUnitTimeoutException("Time out after " + runTime + " seconds");
            }

            /// <summary>
            /// Run the test method.
            /// </summary>
            [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Unit test runners much catch and report on errors, rather than propagate them.")]
            private void Run()
            {
                try
                {
                    this.CaughtException = TestClass.RunMethodImp(
                        this.FixtureObject,
                        this.TestMethod,
                        this.TestSetUp,
                        this.TestTearDown);
                }
                catch (Exception exc)
                {
                    this.CaughtException = TestMethod.RemoveSomeException(exc);
                }

                this.Finished = true;
            }
        }
    }
}