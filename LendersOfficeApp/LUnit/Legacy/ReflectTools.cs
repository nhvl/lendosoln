﻿namespace LUnit.Legacy
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// A set of tools used when reading an assebly.
    /// </summary>
    internal static class ReflectTools
    {
        /// <summary>
        /// Determines whether a reflected item has a particular attribute.
        /// </summary>
        /// <param name="member">The reflected item.</param>
        /// <param name="attrName">The name of the attribute.</param>
        /// <param name="inherit">Whether the attribute is inherited.</param>
        /// <returns>True if the reflected item has the indicated attribute.</returns>
        public static bool HasAttribute(MemberInfo member, string attrName, bool inherit)
        {
            object[] attributes = member.GetCustomAttributes(inherit);
            foreach (Attribute attribute in attributes)
            {
                if (attribute.GetType().FullName == attrName)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Examine a fixture type and return a method having a particular attribute.
        /// In the case of multiple methods, the first one found is returned.
        /// </summary>
        /// <param name="fixtureType">The type to examine.</param>
        /// <param name="attributeName">The FullName of the attribute to look for.</param>
        /// <param name="bindingFlags">BindingFlags to use in looking for method.</param>
        /// <param name="inherit">True indicates the attribute can be inherited.</param>
        /// <returns>A MethodInfo or null.</returns>
        public static MethodInfo GetMethodWithAttribute(Type fixtureType, string attributeName, BindingFlags bindingFlags, bool inherit)
        {
            foreach (MethodInfo method in fixtureType.GetMethods(bindingFlags))
            {
                if (HasAttribute(method, attributeName, inherit))
                {
                    return method;
                }
            }

            return null;
        }

        /// <summary>
        /// Get attribute of a given type on a member. If multiple attributes
        /// of a type are present, the first one found is returned.
        /// </summary>
        /// <param name="member">The member to examine.</param>
        /// <param name="attrName">The FullName of the attribute type to look for.</param>
        /// <param name="inherit">True to include inherited attributes.</param>
        /// <returns>The attribute or null.</returns>
        public static System.Attribute GetAttribute(MemberInfo member, string attrName, bool inherit)
        {
            object[] attributes = member.GetCustomAttributes(inherit);
            foreach (Attribute attribute in attributes)
            {
                if (attribute.GetType().FullName == attrName)
                {
                    return attribute;
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieve a set of attribute instances with a particular name.
        /// </summary>
        /// <param name="member">The member to examine.</param>
        /// <param name="attrName">The FullName of the attribute type to look for.</param>
        /// <param name="inherit">True to include inherited attributes.</param>
        /// <returns>The attribute instances.</returns>
        public static IEnumerable<Attribute> GetAttributes(MemberInfo member, string attrName, bool inherit)
        {
            object[] attributes = member.GetCustomAttributes(inherit);
            foreach (Attribute attribute in attributes)
            {
                if (attribute.GetType().FullName == attrName)
                {
                    yield return attribute;
                }
            }

            yield break;
        }

        /// <summary>
        /// Get the value of a named property on an object.
        /// </summary>
        /// <param name="obj">The object for which the property value is needed.</param>
        /// <param name="name">The name of a non-indexed property of the object.</param>
        /// <param name="bindingFlags">BindingFlags for use in determining which properties are needed.</param>param>
        /// <returns>The property's value, or null.</returns>
        public static object GetPropertyValue(object obj, string name, BindingFlags bindingFlags)
        {
            PropertyInfo property = GetNamedProperty(obj.GetType(), name, bindingFlags);
            if (property != null)
            {
                return property.GetValue(obj, null);
            }

            return null;
        }

        /// <summary>
        /// Examine a type and get a property with a particular name.
        /// In the case of overloads, the first one found is returned.
        /// </summary>
        /// <param name="type">The type to examine.</param>
        /// <param name="name">The name of the property.</param>
        /// <param name="bindingFlags">BindingFlags to use.</param>
        /// <returns>A PropertyInfo or null.</returns>
        public static PropertyInfo GetNamedProperty(Type type, string name, BindingFlags bindingFlags)
        {
            return type.GetProperty(name, bindingFlags);
        }

        /// <summary>
        /// Determines whether a reflected item contains a category attribute marking it as using mocks.
        /// </summary>
        /// <param name="type">The member to examine.</param>
        /// <param name="attrName">The FullName of the attribute type to look for.</param>
        /// <param name="inherit">True to include inherited attributes.</param>
        /// <returns>True if the item contains a mock category, false otherwise.</returns>
        public static bool IsMockTest(MemberInfo type, string attrName, bool inherit)
        {
            var categories = ReflectTools.GetAttributes(type, attrName, inherit);
            foreach (var item in categories)
            {
                var category = (NUnit.Framework.CategoryAttribute)item;
                if (IsMockAttribute(category.Name))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determine whether a category name indicates the usage of mocks.
        /// </summary>
        /// <param name="categoryName">The category name.</param>
        /// <returns>True if the category name indicates the usage of mocks, false otherwise.</returns>
        public static bool IsMockAttribute(string categoryName)
        {
            return string.Compare(categoryName, "LQB.Mocks", true) == 0;
        }
    }
}