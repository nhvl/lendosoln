﻿namespace LUnit.Legacy
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using LendersOffice.Constants;
    using LUnit.Interface;

    /// <summary>
    /// Read the test information within an assembly.
    /// </summary>
    public class ReflectionReader
    {
        /// <summary>
        /// Instanton reference.
        /// </summary>
        public static readonly ReflectionReader Instance;

        /// <summary>
        /// Parameters used when reading the assembly.
        /// </summary>
        private static readonly TestFixtureParameters Parameters;

        /// <summary>
        /// The full name of the LoTestAttribute class.
        /// </summary>
        private static string lendoTestAttributeName = typeof(LUnit.LoTestAttribute).FullName;

        /// <summary>
        /// The full name of the LoTestFixtureAttribute class.
        /// </summary>
        private string lendoTestFixtureAttributeFullName = typeof(LUnit.LoTestFixtureAttribute).FullName;

        /// <summary>
        /// Initializes static members of the <see cref="ReflectionReader" /> class.
        /// </summary>
        static ReflectionReader()
        {
            Parameters = new TestFixtureParameters(
                "NUnit",
                "NUnit.Framework",
                "TestFixtureAttribute",
                string.Empty,
                "TestAttribute",
                "^(?i:test)",
                "ExpectedExceptionAttribute",
                "SetUpAttribute",
                "TearDownAttribute",
                "TestFixtureSetUpAttribute",
                "TestFixtureTearDownAttribute",
                "IgnoreAttribute",
                "CategoryAttribute",
                true,
                false,
                true);

            Instance = new ReflectionReader();
        }

        /// <summary>
        /// Retrieve the description and timeout from an LoTestAttribute or an NUnit Test attribute.
        /// </summary>
        /// <param name="method">The test method.</param>
        /// <param name="timeoutInSec">The timeout in seconds.</param>
        /// <returns>The description.</returns>
        public static string GetDescription(MethodInfo method, out int timeoutInSec)
        {
            timeoutInSec = 0;
            Attribute attrib = ReflectTools.GetAttribute(method, lendoTestAttributeName, false);
            if (attrib != null)
            {
                timeoutInSec = ((LUnit.LoTestAttribute)attrib).Timeout;
            }
            else
            {
                attrib = ReflectTools.GetAttribute(method, Parameters.TestCaseType, false);
            }

            if (attrib != null)
            {
                return ReflectTools.GetPropertyValue(attrib, "Description", BindingFlags.Public | BindingFlags.Instance) as string;
            }

            return null;
        }

        /// <summary>
        /// Retrieve the test methods from a test fixture.
        /// </summary>
        /// <param name="fixtureType">The test fixture.</param>
        /// <returns>The test methods.</returns>
        public MethodInfo[] GetTestMethods(Type fixtureType)
        {
            IList<MethodInfo> testMethods = new List<MethodInfo>();

            IList<MethodInfo> methods = fixtureType.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
                /* | BindingFlags.Static*/);
            foreach (MethodInfo method in methods)
            {
                Attribute lendoTest = ReflectTools.GetAttribute(method, lendoTestAttributeName, false);
                Attribute test = ReflectTools.GetAttribute(method, Parameters.TestCaseType, false);
                Attribute ignoreAttribute = ReflectTools.GetAttribute(method, Parameters.IgnoreType, false);

                if (lendoTest == null && test == null)
                {
                    continue;
                }

                if (ignoreAttribute != null)
                {
                    continue;
                }

                if (!this.HasValidTestMethodSignature(method, lendoTest == null))
                {
                    continue;
                }

                if (ReflectTools.IsMockTest(method, Parameters.CategoryType, false))
                {
                    continue;
                }

                testMethods.Add(method);
            }

            return testMethods.ToArray<MethodInfo>();
        }

        /// <summary>
        /// Determine whether the input type is a text fixture.
        /// </summary>
        /// <param name="type">The type being examined.</param>
        /// <returns>True if the type is a test fixture, false otherwise.</returns>
        public bool IsTestFixture(Type type)
        {
            if (ReflectTools.HasAttribute(type, this.lendoTestFixtureAttributeFullName, true) == true)
            {
                if (ReflectTools.HasAttribute(type, Parameters.IgnoreType, true))
                {
                    return false;
                }

                if (ReflectTools.IsMockTest(type, Parameters.CategoryType, true))
                {
                    return false;
                }

                return true;
            }

            if (!type.IsPublic && !type.IsNestedPublic)
            {
                return false;
            }

            if (ReflectTools.HasAttribute(type, Parameters.IgnoreType, true))
            {
                return false;
            }

            if (ReflectTools.IsMockTest(type, Parameters.CategoryType, true))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Retrieve the setup method that runs prior to each test method.
        /// </summary>
        /// <param name="fixtureType">The type of the fixture class.</param>
        /// <returns>The setup method, or null.</returns>
        public virtual MethodInfo GetSetUpMethod(Type fixtureType)
        {
            return ReflectTools.GetMethodWithAttribute(
                fixtureType,
                Parameters.SetUpType,
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                Parameters.InheritSetUpAndTearDownTypes);
        }

        /// <summary>
        /// Retrieve the teardown method that runs after each test method.
        /// </summary>
        /// <param name="fixtureType">The type of the fixture.</param>
        /// <returns>The teardown method, or null.</returns>
        public virtual MethodInfo GetTearDownMethod(Type fixtureType)
        {
            return ReflectTools.GetMethodWithAttribute(
                fixtureType,
                Parameters.TearDownType,
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                Parameters.InheritSetUpAndTearDownTypes);
        }

        /// <summary>
        /// Retrieve the fixture setup method that runs once prior to any test methods in a fixture.
        /// </summary>
        /// <param name="fixtureType">The type of the fixture.</param>
        /// <returns>The fixture setup method, or null.</returns>
        public virtual MethodInfo GetFixtureSetUpMethod(Type fixtureType)
        {
            return ReflectTools.GetMethodWithAttribute(
                fixtureType,
                Parameters.FixtureSetUpType,
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                Parameters.InheritSetUpAndTearDownTypes);
        }

        /// <summary>
        /// Retrieve the fixture teardown method that runs once after all test methods in a fixture.
        /// </summary>
        /// <param name="fixtureType">The type of the fixture.</param>
        /// <returns>The fixture teardown method, or null.</returns>
        public virtual MethodInfo GetFixtureTearDownMethod(Type fixtureType)
        {
            return ReflectTools.GetMethodWithAttribute(
                fixtureType,
                Parameters.FixtureTearDownType,
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                Parameters.InheritSetUpAndTearDownTypes);
        }

        /// <summary>
        /// Retrieve the expected exception for a test method.
        /// </summary>
        /// <param name="method">The test method.</param>
        /// <param name="expectedExceptionName">The expected exception's type name.</param>
        /// <param name="expectedMessage">The message expected to be set in the expected exception.</param>
        /// <returns>True if there is an expected exception, false otherwise.</returns>
        public virtual bool GetExpectedExceptionInfo(MethodInfo method, out string expectedExceptionName, out string expectedMessage)
        {
            Type expectedException = null;
            expectedExceptionName = null;
            expectedMessage = null;

            if (Parameters.HasExpectedExceptionType)
            {
                Attribute attribute = ReflectTools.GetAttribute(method, Parameters.ExpectedExceptionType, false);
                if (attribute != null)
                {
                    expectedException = (System.Type)ReflectTools.GetPropertyValue(
                        attribute,
                        "ExceptionType",
                        BindingFlags.Public | BindingFlags.Instance);
                    if (expectedException != null)
                    {
                        expectedExceptionName = expectedException.FullName;
                    }
                    else
                    {
                        expectedExceptionName = (string)ReflectTools.GetPropertyValue(
                                                        attribute,
                                                        "ExceptionName",
                                                        BindingFlags.Public | BindingFlags.Instance);
                    }

                    expectedMessage = (string)ReflectTools.GetPropertyValue(
                        attribute,
                        "ExpectedMessage",
                        BindingFlags.Public | BindingFlags.Instance);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determine whether a test method or fixture is runnable and not ignored.
        /// </summary>
        /// <param name="method">The test method or fixture.</param>
        /// <param name="requirePublic">True if we only look at public items, false othwerwise.</param>
        /// <returns>True if the test method or fixture is runnable, false if ignored.</returns>
        protected virtual bool HasValidTestMethodSignature(MethodInfo method, bool requirePublic)
        {
            foreach (Attribute atr in method.GetCustomAttributes(true))
            {
                NUnit.Framework.CategoryAttribute nunitAttribute = atr as NUnit.Framework.CategoryAttribute;
                if (nunitAttribute == null)
                {
                    continue;
                }

                if (System.Text.RegularExpressions.Regex.IsMatch(nunitAttribute.Name, ConstApp.LoWebUnitTestIgnoreAttributesRegex, System.Text.RegularExpressions.RegexOptions.Compiled))
                {
                    return false;
                }
                else if (ReflectTools.IsMockAttribute(nunitAttribute.Name))
                {
                    return false;
                }
            }

            return !method.IsStatic
                && !method.IsAbstract
                && (!requirePublic || method.IsPublic)
                && method.GetParameters().Length == 0
                && method.ReturnType.Equals(typeof(void));
        }
    }
}