﻿namespace LUnit.Legacy
{
    using System.Reflection;
    using System.Threading;
    using System.Web;
    using DataAccess;
    using Interface;
    using LendersOffice.Constants;

    /// <summary>
    /// Entry point into the LUnitEngine feature.
    /// </summary>
    public sealed class Shim : IRunner
    {
        /// <summary>
        /// Lock for initialization of the cached implementation.
        /// </summary>
        private static readonly object ShimLock = new object();

        /// <summary>
        /// Initializes a new instance of the <see cref="Shim"/> class.
        /// </summary>
        /// <param name="assembly">The assembly containing unit tests.</param>
        public Shim(Assembly assembly)
        {
            var impl = CachedImpl;
            if (impl == null)
            {
                lock (ShimLock)
                {
                    impl = CachedImpl;
                    if (impl == null)
                    {
                        impl = new LUnit.Legacy.WebTestClasses(assembly);
                        CachedImpl = impl;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a cached parsing of the assembly into test classes.
        /// </summary>
        /// <value>A cached parsing of the assembly into test classes.</value>
        private static WebTestClasses CachedImpl { get; set; }

        /// <summary>
        /// Retrieve all the test fixtures that will be run.
        /// </summary>
        /// <returns>All the test fixtures that will be run.</returns>
        public TestClassData[] GetTestFixtures()
        {
            return CachedImpl.Export();
        }

        /// <summary>
        /// Run the test methods contained within the specified test fixture,
        /// or all test methods if fixtureId is null.
        /// </summary>
        /// <param name="fixtureId">The identifier for a test fixture, or null.</param>
        /// <returns>The results of running the test methods.</returns>
        public TestClassResult RunFixture(string fixtureId)
        {
            if (string.IsNullOrEmpty(fixtureId))
            {
                // index.aspx
                string email = ConstStage.SmokeTestEmailDestination;
                if (string.IsNullOrEmpty(email))
                {
                    return null;
                }

                string host = HttpContext.Current?.Request.Url.Host;
                string environmentDescription = System.Environment.MachineName;
                if (!string.IsNullOrWhiteSpace(host))
                {
                    environmentDescription = string.Concat(environmentDescription, " - ", host);
                }

                Tools.LogInfo("Started Executing Unit Test");
                ThreadPool.QueueUserWorkItem(
                    new WaitCallback(delegate(object state)
                    {
                        var report = new LUnit.Legacy.EmailTestReport(ConstStage.SmokeTestEmailDestination, email);
                        CachedImpl.RunAll(report);
                    }),
                null);

                return null;
            }
            else
            {
                // TestCenter.aspx
                var report = new Tc2TestMethodReport();
                int classId = int.Parse(fixtureId);
                CachedImpl.Run(report, classId, int.MaxValue);
                return report.AllResults[0];
            }
        }

        /// <summary>
        /// Run a single test method, as indicated with the specified fixture and test identifiers.
        /// </summary>
        /// <param name="fixtureId">The identifier for the test fixture containine the test method.</param>
        /// <param name="testId">The identifier for the test method.</param>
        /// <returns>The result of running the test method.</returns>
        public LUnit.Interface.TestClassResult RunSingleTest(string fixtureId, string testId)
        {
            var report = new Tc2TestMethodReport();
            int classId = int.Parse(fixtureId);
            int methodId = int.Parse(testId);
            CachedImpl.Run(report, classId, methodId);
            return report.AllResults[0];
        }
    }
}