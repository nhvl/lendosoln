using System;

namespace LUnit
{
	public enum ResultStatus
	{
		Ignore,
		Fail,
		Success
	}

	public interface ITestReport
	{
		void Start();
		void Finish();

		void TestClassStart( TestClass testClass  );
		//void TestClassFinish( TestClass testClass, bool isSuccess, string message );
		void TestClassFinish( TestClass testClass, ResultStatus resultStatus, string message );

		void TestMethodStart( TestClass testClass, int methodIdx );
		void TestMethodFinish( TestClass testClass, int methodIdx, ResultStatus resultStatus, string message );
	}
}
