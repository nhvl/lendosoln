
using System;

namespace LUnit
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple=false)]
    public class LoTestAttribute  : Attribute
    {
        private string m_description;

        /// <summary>
        /// Descriptive text for this test
        /// </summary>
        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }


        int m_timeout = 0;

        public int Timeout
        {
            get { return m_timeout; }
            set { m_timeout = value; }    
        }

       

        public LoTestAttribute()
        {
        }
        public LoTestAttribute( string description )
        {
            m_description = description;
        }

        public LoTestAttribute( string description, int timeoutInSec )
        {
            m_description = description;
            m_timeout     = timeoutInSec;
        }

        
    }

}
