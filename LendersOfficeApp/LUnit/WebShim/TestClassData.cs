﻿namespace LUnit.WebShim
{
    using System.Collections.Generic;

    /// <summary>
    /// Mimic the class that is shipped over the internet.
    /// </summary>
    public sealed class TestClassData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestClassData"/> class.
        /// </summary>
        /// <param name="data">An interface TestClassData instance.</param>
        /// <param name="classIndex">Index into an array that is used by the web pages.</param>
        public TestClassData(LUnit.Interface.TestClassData data, int classIndex)
        {
            this.Id = classIndex;
            this.Name = data.Name;
            this.Namespace = data.Namespace;
            this.FullName = data.FullName;

            var methods = new List<LUnit.WebShim.TestMethodData>();
            foreach (var item in data.TestMethods)
            {
                if (item.IsRunnable)
                {
                    var method = new LUnit.WebShim.TestMethodData(item.Name, item.FriendlyName);
                    methods.Add(method);
                }
            }

            this.TestMethods = methods.ToArray();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestClassData"/> class.
        /// </summary>
        /// <param name="testClass">An old LUnit TestClass instance.</param>
        public TestClassData(LUnit.TestClass testClass)
        {
            this.Id = testClass.Id;
            this.Name = testClass.Name;
            this.Namespace = testClass.Namespace;
            this.FullName = testClass.FullName;

            var methods = new LUnit.WebShim.TestMethodData[testClass.TestMethods.Length];
            for (int i = 0; i < testClass.TestMethods.Length; ++i)
            {
                var item = testClass.TestMethods[i];
                methods[i] = new LUnit.WebShim.TestMethodData(item.Name, item.FriendlyName);
            }

            this.TestMethods = methods;
        }

        /// <summary>
        /// Gets or sets the identifier for this fixture.
        /// </summary>
        /// <value>The identifier for this fixture.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name for this fixture.
        /// </summary>
        /// <value>The name for this fixture.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the namespace for this fixture.
        /// </summary>
        /// <value>The namespace for this fixture.</value>
        public string Namespace { get; set; }

        /// <summary>
        /// Gets or sets the fullname for this fixture.
        /// </summary>
        /// <value>The fullname for this fixture.</value>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets an array of test methods contained within this fixture.
        /// </summary>
        /// <value>An array of test methods contained within this fixture.</value>
        public TestMethodData[] TestMethods { get; set; }
    }
}