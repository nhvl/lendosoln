﻿namespace LUnit.WebShim
{
    /// <summary>
    /// Mimic the class is shipped over the internet.
    /// </summary>
    public sealed class TestMethodResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestMethodResult"/> class.
        /// </summary>
        /// <param name="id">Identifier for the test method.</param>
        /// <param name="s">The result for the method execution (or non-execution).</param>
        /// <param name="m">A message from the method execution.</param>
        public TestMethodResult(int id, ResultStatus s, string m)
        {
            this.MethodId = id;
            this.ResultStatus = s;
            this.Message = m;
        }

        /// <summary>
        /// Gets or sets the identifier for the method.
        /// </summary>
        /// <value>The identifier for the method.</value>
        public int MethodId { get; set; }

        /// <summary>
        /// Gets or sets the result status for the method.
        /// </summary>
        /// <value>The result status for the method.</value>
        public ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the message from executing the method.
        /// </summary>
        /// <value>The message from executing the method.</value>
        public string Message { get; set; }
    }
}