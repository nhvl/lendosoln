﻿namespace LUnit.WebShim
{
    /// <summary>
    /// Mimic the class that is shipped over the internet.
    /// </summary>
    public class TestMethodData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestMethodData"/> class.
        /// </summary>
        /// <param name="name">The name of the method.</param>
        /// <param name="friendlyName">A user-friendly name for the method.</param>
        public TestMethodData(string name, string friendlyName)
        {
            this.Name = name;
            this.FriendlyName = friendlyName;
        }

        /// <summary>
        /// Gets or sets the name of the method.
        /// </summary>
        /// <value>The name of the method.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a user-friendly name for the method.
        /// </summary>
        /// <value>A user-friendly name for the method.</value>
        public string FriendlyName { get; set; }
    }
}