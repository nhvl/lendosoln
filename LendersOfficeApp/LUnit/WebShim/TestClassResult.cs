﻿namespace LUnit.WebShim
{
    using System.Collections.Generic;

    /// <summary>
    /// Mimic the class that is shipped over the internet.
    /// </summary>
    public sealed class TestClassResult
    {
        /// <summary>
        /// Container used when old code builds up the result.
        /// </summary>
        private List<TestMethodResult> listMethods;

        /// <summary>
        /// Container used when new code adds everything at once.
        /// </summary>
        private TestMethodResult[] arrayMethods;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestClassResult"/> class.
        /// </summary>
        /// <param name="result">The internal result that this class is shimming.</param>
        /// <param name="classIndex">Index into an array that is used by the web pages.</param>
        /// <param name="methodId">Select only this method, or all if null.</param>
        public TestClassResult(LUnit.Interface.TestClassResult result, int classIndex, string methodId)
        {
            this.FixtureId = classIndex;
            this.ResultStatus = (ResultStatus)(int)result.ResultStatus;
            this.Message = result.Message;

            bool identifiersIncorrect = result.FixtureId != classIndex.ToString();
            this.SetTestMethods(identifiersIncorrect, result, methodId);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TestClassResult"/> class.
        /// </summary>
        /// <param name="testClass">An old LUnit TestClass instance.</param>
        public TestClassResult(LUnit.TestClass testClass)
        {
            this.FixtureId = testClass.Id;
            this.listMethods = new List<TestMethodResult>();
        }

        /// <summary>
        /// Gets or sets the identifier for the fixure.
        /// </summary>
        /// <value>The identifier for the fixure.</value>
        public int FixtureId { get; set; }

        /// <summary>
        /// Gets or sets the result status for the fixture.
        /// </summary>
        /// <value>The result status for the fixture.</value>
        public ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the message for the fixture.
        /// </summary>
        /// <value>The message for the fixture.</value>
        public string Message { get; set; }

        /// <summary>
        /// Gets the method results.
        /// </summary>
        /// <value>The method results.</value>
        public TestMethodResult[] TestMethodResults
        {
            get
            {
                if (this.listMethods != null)
                {
                    return this.listMethods.ToArray();
                }
                else
                {
                    return this.arrayMethods;
                }
            }
        }

        /// <summary>
        /// Add a method result item to the list.
        /// </summary>
        /// <param name="methodId">The method identifier.</param>
        /// <param name="resultStatus">The result status.</param>
        /// <param name="message">The message for the method.</param>
        public void Add(int methodId, ResultStatus resultStatus, string message)
        {
            var item = new TestMethodResult(methodId, resultStatus, message);
            this.listMethods.Add(item);
        }

        /// <summary>
        /// Extract the test method results and translate to the shimmed version.
        /// </summary>
        /// <param name="useIndex">True if the MethodId should not be trusted, false otherwise.</param>
        /// <param name="result">The result of running the test fixure.</param>
        /// <param name="methodId">Select only this method, or all if null.</param>
        private void SetTestMethods(bool useIndex, LUnit.Interface.TestClassResult result, string methodId)
        {
            var methods = new List<LUnit.WebShim.TestMethodResult>();
            for (int i = 0; i < result.TestMethodResults.Length; ++i)
            {
                var item = result.TestMethodResults[i];
                int index = useIndex ? i : int.Parse(item.MethodId);
                if (methodId == null || item.MethodId == methodId)
                {
                    methods.Add(new TestMethodResult(index, (ResultStatus)(int)item.ResultStatus, item.Message));
                }
            }

            this.arrayMethods = methods.ToArray();
        }
    }
}