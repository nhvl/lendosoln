﻿namespace LUnit.WebShim
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Report class moved here from TestCenter.aspx.
    /// </summary>
    public sealed class Tc2TestMethodReport : ITestReport
    {
        /// <summary>
        /// The results are stored here.
        /// </summary>
        private Dictionary<int, TestClassResult> testClassResults;

        /// <summary>
        /// This is called prior to other methods.
        /// </summary>
        public void Start()
        {
            this.Init();
        }

        /// <summary>
        /// This is called after all other methods.
        /// </summary>
        public void Finish()
        {
        }

        /// <summary>
        /// This is called prior to execution of any methods in a test fixture.
        /// </summary>
        /// <param name="testClass">The parameter is not used.</param>
        public void TestClassStart(TestClass testClass)
        {
        }

        /// <summary>
        /// This is called after execution of all methods in a test fixture.
        /// </summary>
        /// <param name="testClass">The test fixture.</param>
        /// <param name="resultStatus">The aggregate status for the test fixture.</param>
        /// <param name="message">A message from execution of the test fixture.</param>
        public void TestClassFinish(TestClass testClass, ResultStatus resultStatus, string message)
        {
            if (!this.testClassResults.ContainsKey(testClass.Id))
            {
                this.testClassResults.Add(testClass.Id, new TestClassResult(testClass));
            }

            var r = this.testClassResults[testClass.Id];
            r.ResultStatus = resultStatus;
            r.Message = message;
        }

        /// <summary>
        /// This is called prior to calling the (setup, test, tear down) method sequence for a test method.
        /// </summary>
        /// <param name="testClass">The parameter is not used.</param>
        /// <param name="methodIdx">The parameter is not used.</param>
        public void TestMethodStart(TestClass testClass, int methodIdx)
        {
        }

        /// <summary>
        /// This is called after the (setup, test, tear down) method sequence for a test method.
        /// </summary>
        /// <param name="testClass">The test fixture.</param>
        /// <param name="methodIdx">The identifier for the test method.</param>
        /// <param name="resultStatus">The status for the test method.</param>
        /// <param name="message">A message from execution of the test message.</param>
        public void TestMethodFinish(TestClass testClass, int methodIdx, ResultStatus resultStatus, string message)
        {
            if (!this.testClassResults.ContainsKey(testClass.Id))
            {
                this.testClassResults.Add(testClass.Id, new TestClassResult(testClass));
            }

            this.testClassResults[testClass.Id].Add(methodIdx, resultStatus, message);
        }

        /// <summary>
        /// Retrieve the results once all the unit tests have been executed.
        /// </summary>
        /// <returns>The results once all the unit tests have been executed.</returns>
        public TestClassResult[] ToClassResults()
        {
            return this.testClassResults.OrderBy(p => p.Key).Select(p => p.Value).ToArray();
        }

        /// <summary>
        /// Initialize the collection that holds the test results as they accumulate.
        /// </summary>
        private void Init()
        {
            this.testClassResults = new Dictionary<int, TestClassResult>();
        }
    }
}