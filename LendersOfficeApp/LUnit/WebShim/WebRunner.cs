﻿namespace LUnit.WebShim
{
    /// <summary>
    /// Shim interface that mimics the existing web methods used to call into LUnit.
    /// </summary>
    public interface IWebRunner
    {
        /// <summary>
        /// Retrieve all the test fixtures that will be run.
        /// </summary>
        /// <returns>All the test fixtures that will be run.</returns>
        TestClassData[] GetTestFixtures();

        /// <summary>
        /// Run the test methods contained within the specified test fixture,
        /// or all test methods if fixtureId is null.
        /// </summary>
        /// <param name="fixtureId">The identifier for a test fixture, or null.</param>
        /// <returns>The results of running the test methods.</returns>
        TestClassResult[] RunFixture(int fixtureId);

        /// <summary>
        /// Run a single test method, as indicated with the specified fixture and test identifiers.
        /// </summary>
        /// <param name="fixtureId">The identifier for the test fixture containine the test method.</param>
        /// <param name="testId">The identifier for the test method.</param>
        /// <returns>The result of running the test method.</returns>
        TestClassResult RunSingleTest(int fixtureId, int testId);
    }
}
