using System;
using System.Text;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using LendersOffice.Constants;
//using CommonLib;

namespace LUnit
{
    public struct TestFixtureParameters
    {
        public string RequiredFramework;
        public string TestFixtureType;
        public string TestFixturePattern;
        public string TestCaseType;
        public string TestCasePattern;
        public string ExpectedExceptionType;
        public string SetUpType;
        public string TearDownType;
        public string FixtureSetUpType;
        public string FixtureTearDownType;
        public string IgnoreType;
        public string CategoryType;
        public bool InheritTestFixtureType;
        public bool InheritTestCaseType;
        public bool InheritSetUpAndTearDownTypes;

        public TestFixtureParameters(
            string RequiredFramework,
            string Namespace,
            string TestFixtureType,
            string TestFixturePattern,
            string TestCaseType,
            string TestCasePattern,
            string ExpectedExceptionType,
            string SetUpType,
            string TearDownType,
            string FixtureSetUpType,
            string FixtureTearDownType,
            string IgnoreType,
            string CategoryType,
            bool InheritTestFixtureType,
            bool InheritTestCaseType,
            bool InheritSetUpAndTearDownTypes )
        {
            this.RequiredFramework = RequiredFramework;
            this.TestFixtureType = Namespace + "." + TestFixtureType;
            this.TestFixturePattern = TestFixturePattern;
            this.TestCaseType = Namespace + "." + TestCaseType;
            this.TestCasePattern = TestCasePattern;
            this.ExpectedExceptionType = Namespace + "." + ExpectedExceptionType;
            this.SetUpType = Namespace + "." + SetUpType;
            this.TearDownType = Namespace + "." + TearDownType;
            this.FixtureSetUpType = Namespace + "." + FixtureSetUpType;
            this.FixtureTearDownType = Namespace + "." + FixtureTearDownType;
            this.IgnoreType = Namespace + "." + IgnoreType;
            this.CategoryType = Namespace + "." + CategoryType;
            this.InheritTestFixtureType = InheritTestFixtureType;
            this.InheritTestCaseType = InheritTestCaseType;
            this.InheritSetUpAndTearDownTypes = InheritSetUpAndTearDownTypes;
        }

        public bool HasRequiredFramework
        {
            get { return IsValid( this.RequiredFramework ); }
        }

        public bool HasTestFixtureType
        {
            get { return IsValid( this.TestFixtureType ); }
        }

        public bool HasTestFixturePattern
        {
            get { return IsValid( this.TestFixturePattern ); }
        }

        public bool HasTestCaseType
        {
            get { return IsValid( this.TestCaseType ); }
        }

        public bool HasTestCasePattern
        {
            get { return IsValid( this.TestCasePattern ); }
        }

        public bool HasExpectedExceptionType
        {
            get { return IsValid( this.ExpectedExceptionType ); }
        }

        public bool HasSetUpType
        {
            get { return IsValid( this.SetUpType ); }
        }

        public bool HasTearDownType
        {
            get { return IsValid( this.TearDownType ); }
        }

        public bool HasFixtureSetUpType
        {
            get { return IsValid( this.FixtureSetUpType ); }
        }

        public bool HasFixtureTearDownType
        {
            get { return IsValid( this.FixtureTearDownType ); }
        }

        public bool HasIgnoreType
        {
            get { return IsValid( this.IgnoreType ); }
        }

        private bool IsValid( string s )
        {
            return s != null && s != string.Empty;
        }
    }

    public class ReflectionReader
    {
        public static readonly ReflectionReader Instance;
        static readonly TestFixtureParameters s_parameters;

        static ReflectionReader()
        {
            s_parameters  = new TestFixtureParameters
                (
                "NUnit",
                "NUnit.Framework",
                "TestFixtureAttribute",
                "",
                "TestAttribute",
                "^(?i:test)",
                "ExpectedExceptionAttribute",
                "SetUpAttribute",
                "TearDownAttribute",
                "TestFixtureSetUpAttribute",
                "TestFixtureTearDownAttribute",
                "IgnoreAttribute",
                "CategoryAttribute",
                true, false, true
                );

            Instance = new ReflectionReader();
        }

        static string s_LoTestAttributeName = typeof(LoTestAttribute).FullName;


        public static string GetDescription( MethodInfo method, out int timeoutInSec  )
        {
            timeoutInSec = 0;
            Attribute attrib = ReflectTools.GetAttribute( method, s_LoTestAttributeName, false );
            if( attrib != null )
                timeoutInSec = ((LoTestAttribute)attrib).Timeout;
            else
                attrib  = ReflectTools.GetAttribute( method, s_parameters.TestCaseType, false );


            if( attrib != null )
                return ReflectTools.GetPropertyValue( attrib, "Description",BindingFlags.Public | BindingFlags.Instance ) as string;
            return null;
        }
    
        public MethodInfo[] GetTestMethods(Type fixtureType)
        {
            IList<MethodInfo> testMethods = new List<MethodInfo>();

            IList<MethodInfo> methods = fixtureType.GetMethods( BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance 
                // | BindingFlags.Static 
                );
            foreach(MethodInfo method in methods)
            {   
                Attribute loTest = ReflectTools.GetAttribute( method, s_LoTestAttributeName, false );
                Attribute test   = ReflectTools.GetAttribute( method, s_parameters.TestCaseType, false );
                Attribute ignoreAttribute = ReflectTools.GetAttribute(method, s_parameters.IgnoreType, false);

                if( loTest == null && test == null )
                    continue;

                if (ignoreAttribute != null)
                    continue;

                if( !HasValidTestMethodSignature( method, loTest == null ) )
                    continue;

                if (ReflectTools.IsMockTest(method, s_parameters.CategoryType, false))
                {
                    continue;
                }

                testMethods.Add( method  );
            }
            return testMethods.ToArray<MethodInfo>();
        }

        protected virtual bool HasValidTestMethodSignature( MethodInfo method, bool requirePublic )
        {
           
            foreach (Attribute atr in method.GetCustomAttributes(true) )
            {
                NUnit.Framework.CategoryAttribute nunitAttribute = atr as NUnit.Framework.CategoryAttribute;
                if (nunitAttribute == null) { continue; }
                if (System.Text.RegularExpressions.Regex.IsMatch(nunitAttribute.Name, ConstApp.LoWebUnitTestIgnoreAttributesRegex, System.Text.RegularExpressions.RegexOptions.Compiled))
                {
                    return false;
                }
                else if (ReflectTools.IsMockAttribute(nunitAttribute.Name))
                {
                    return false;
                }
            }

            return !method.IsStatic
                && !method.IsAbstract
                && (!requirePublic || method.IsPublic)
                && method.GetParameters().Length == 0
                && method.ReturnType.Equals(typeof(void) );
        }


        string s_LoTestFixtureAttributeFullName = typeof(LoTestFixtureAttribute).FullName;
        public bool IsTestFixture(Type type)
        {
            if ( ReflectTools.HasAttribute( type, s_LoTestFixtureAttributeFullName, true ) == true )
            {
                if (ReflectTools.HasAttribute( type, s_parameters.IgnoreType, true ))
                {
                    return false;
                }
                
                if (ReflectTools.IsMockTest(type, s_parameters.CategoryType, true))
                {
                    return false;
                }

                return true;
            }

            if (!type.IsPublic && !type.IsNestedPublic)
            {
                return false;
            }

            if (ReflectTools.HasAttribute(type, s_parameters.IgnoreType, true))
            {
                return false;
            }
            
            if (ReflectTools.IsMockTest(type, s_parameters.CategoryType, true))
            {
                return false;
            }

            return true;
        }

        public virtual MethodInfo GetSetUpMethod(Type fixtureType)
        {
            return ReflectTools.GetMethodWithAttribute( fixtureType, s_parameters.SetUpType,
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                s_parameters.InheritSetUpAndTearDownTypes);
        }
        public virtual MethodInfo GetTearDownMethod(Type fixtureType)
        {
            return ReflectTools.GetMethodWithAttribute( fixtureType, s_parameters.TearDownType,
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance ,
                s_parameters.InheritSetUpAndTearDownTypes);
        }
        public virtual MethodInfo GetFixtureSetUpMethod(Type fixtureType)
        {
            return ReflectTools.GetMethodWithAttribute( fixtureType, s_parameters.FixtureSetUpType,
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                s_parameters.InheritSetUpAndTearDownTypes);
        }
        public virtual MethodInfo GetFixtureTearDownMethod(Type fixtureType)
        {
            return ReflectTools.GetMethodWithAttribute( fixtureType, s_parameters.FixtureTearDownType,
                BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance,
                s_parameters.InheritSetUpAndTearDownTypes);
        }

        public virtual bool GetExpectedExceptionInfo(MethodInfo method, out string expectedExceptionName, out string  expectedMessage)
        {
            Type expectedException = null;
            expectedExceptionName = null;
            expectedMessage = null;

            if( s_parameters.HasExpectedExceptionType )
            {
                Attribute attribute = ReflectTools.GetAttribute( method, s_parameters.ExpectedExceptionType, false );
                if ( attribute != null )
                {
                    expectedException = (System.Type)ReflectTools.GetPropertyValue( 
                        attribute, "ExceptionType",
                        BindingFlags.Public | BindingFlags.Instance );
                    if( expectedException != null )
                        expectedExceptionName = expectedException.FullName;
                    else
                        expectedExceptionName = (string)ReflectTools.GetPropertyValue(
                                                        attribute, "ExceptionName",
                                                        BindingFlags.Public | BindingFlags.Instance );
                    expectedMessage = (string)ReflectTools.GetPropertyValue(
                        attribute, "ExpectedMessage",
                        BindingFlags.Public | BindingFlags.Instance );
                    return true;
                }
            }

            return false;

        }

    }

    internal class ReflectTools
    {
        static public bool HasAttribute( MemberInfo member, string attrName, bool inherit )
        {
            object[] attributes = member.GetCustomAttributes( inherit );
            foreach( Attribute attribute in attributes )
                if ( attribute.GetType().FullName == attrName )
                    return true;
            return false;
        }

        /// <summary>
        /// Examine a fixture type and return a method having a particular attribute.
        /// In the case of multiple methods, the first one found is returned.
        /// </summary>
        /// <param name="fixtureType">The type to examine</param>
        /// <param name="attributeName">The FullName of the attribute to look for</param>
        /// <param name="bindingFlags">BindingFlags to use in looking for method</param>
        /// <returns>A MethodInfo or null</returns>
        public static MethodInfo GetMethodWithAttribute( Type fixtureType, string attributeName, BindingFlags bindingFlags, bool inherit )
        {
            foreach(MethodInfo method in fixtureType.GetMethods( bindingFlags ) )
            {
                if( HasAttribute( method, attributeName, inherit ) ) 
                    return method;
            }

            return null;
        }

        /// <summary>
        /// Get attribute of a given type on a member. If multiple attributes
        /// of a type are present, the first one found is returned.
        /// </summary>
        /// <param name="member">The member to examine</param>
        /// <param name="attrName">The FullName of the attribute type to look for</param>
        /// <param name="inherit">True to include inherited attributes</param>
        /// <returns>The attribute or null</returns>
        public static System.Attribute GetAttribute( MemberInfo member, string attrName, bool inherit )
        {
            object[] attributes = member.GetCustomAttributes( inherit );
            foreach( Attribute attribute in attributes )
                if ( attribute.GetType().FullName == attrName )
                    return attribute;
            return null;
        }

        public static IEnumerable<Attribute> GetAttributes(MemberInfo member, string attrName, bool inherit)
        {
            object[] attributes = member.GetCustomAttributes(inherit);
            foreach (Attribute attribute in attributes)
            {
                if (attribute.GetType().FullName == attrName)
                {
                    yield return attribute;
                }
            }
            yield break;
        }

        /// <summary>
        /// Get the value of a named property on an object
        /// </summary>
        /// <param name="obj">The object for which the property value is needed</param>
        /// <param name="name">The name of a non-indexed property of the object</param>
        /// <param name="bindingFlags">BindingFlags for use in determining which properties are needed</param>param>
        /// <returns></returns>
        public static object GetPropertyValue( object obj, string name, BindingFlags bindingFlags )
        {
            PropertyInfo property = GetNamedProperty( obj.GetType(), name, bindingFlags );
            if ( property != null )
                return property.GetValue( obj, null );
            return null;
        }

        /// <summary>
        /// Examine a type and get a property with a particular name.
        /// In the case of overloads, the first one found is returned.
        /// </summary>
        /// <param name="type">The type to examine</param>
        /// <param name="bindingFlags">BindingFlags to use</param>
        /// <returns>A PropertyInfo or null</returns>
        public static PropertyInfo GetNamedProperty( Type type, string name, BindingFlags bindingFlags )
        {
            return type.GetProperty( name, bindingFlags );
        }

        public static bool IsMockTest(MemberInfo type, string attrName, bool inherit)
        {
            var categories = ReflectTools.GetAttributes(type, attrName, inherit);
            foreach (var item in categories)
            {
                var category = (NUnit.Framework.CategoryAttribute)item;
                if (IsMockAttribute(category.Name))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsMockAttribute(string attributeName)
        {
            return string.Compare(attributeName, "LQB.Mocks", true) == 0;
        }
    }


}
