﻿namespace LUnitEngine
{
    using LUnitEngine.Attributes;

    /// <summary>
    /// This is the base class for distinct attributes in LUnit.
    /// </summary>
    internal abstract class Attribute
    {
        /// <summary>
        /// Name of the attribute.
        /// </summary>
        private AttributeName name;

        /// <summary>
        /// Initializes a new instance of the <see cref="Attribute"/> class.
        /// </summary>
        /// <param name="name">Name of the attribute.</param>
        protected Attribute(AttributeName name)
        {
            this.name = name;
        }

        /// <summary>
        /// Gets the name of the attribute.
        /// </summary>
        /// <value>The name of the attribute.</value>
        public string Name
        {
            get
            {
                return this.name.ToString();
            }
        }

        /// <summary>
        /// Factory method for creating attribute classes.
        /// </summary>
        /// <param name="attribute">An attribute marking a code construct.</param>
        /// <returns>An instance of an attribute class.</returns>
        public static Attribute Create(System.Attribute attribute)
        {
            switch (attribute.GetType().FullName)
            {
                case NUnit.Framework.Category:
                    return new CategoryAttribute(attribute);
                case NUnit.Framework.ExpectedException:
                    return new ExpectedExceptionAttribute(attribute);
                case NUnit.Framework.Explicit:
                    return new ExplicitAttribute(attribute);
                case NUnit.Framework.Ignore:
                    return new IgnoreAttribute(attribute);
                case NUnit.Framework.SetUp:
                    return new SetUpAttribute(attribute);
                case NUnit.Framework.SetUpFixture:
                    return new SetUpFixtureAttribute(attribute);
                case NUnit.Framework.TearDown:
                    return new TearDownAttribute(attribute);
                case NUnit.Framework.Test:
                    return new TestAttribute(attribute);
                case NUnit.Framework.TestFixture:
                    return new TestFixtureAttribute(attribute);
                case NUnit.Framework.TestFixtureSetUp:
                    return new TestFixtureSetUpAttribute(attribute);
                case NUnit.Framework.TestFixtureTearDown:
                    return new TestFixtureTearDownAttribute(attribute);
                case LUnit.LoTestAttribute:
                    return new LoTestAttribute(attribute);
                case LUnit.LoTestFixtureAttribute:
                    return new LoTestFixtureAttribute(attribute);
                default:
                    throw new System.Exception("Will never get here.");
            }
        }
    }
}
