﻿namespace LUnitEngine
{
    using System;
    using System.IO;
    using System.Reflection;

    /// <summary>
    /// Report object that does nothing.
    /// </summary>
    internal sealed class NullReport : IReportRunResults
    {
        /// <summary>
        /// Called prior to processing an assembly containing unit tests.
        /// </summary>
        public void BeginAssembly()
        {
        }

        /// <summary>
        /// Called prior to execution of any unit tests within a test fixture.
        /// </summary>
        public void BeginClass()
        {
        }

        /// <summary>
        /// Called prior to execution of a test method.
        /// </summary>
        public void BeginMethod()
        {
        }

        /// <summary>
        /// Called prior to execution of any unit tests within a namespace.
        /// </summary>
        /// <param name="name">The parameter is not used.</param>
        public void BeginNamespace(string name)
        {
        }

        /// <summary>
        /// Called after processing an assembly containing unit tests.
        /// </summary>
        /// <param name="state">The parameter is not used.</param>
        public void EndAssembly(RunState state)
        {
        }

        /// <summary>
        /// Called after all unit tests within a test fixture have been executed.
        /// </summary>
        /// <param name="type">The parameter is not used.</param>
        /// <param name="state">The parameter is not used.</param>
        public void EndClass(Type type, RunState state)
        {
        }

        /// <summary>
        /// Called after execution of a test method.
        /// </summary>
        /// <param name="info">The parameter is not used.</param>
        /// <param name="description">The parameter is not used.</param>
        /// <param name="state">The parameter is not used.</param>
        /// <param name="result">The parameter is not used.</param>
        public void EndMethod(MethodInfo info, string description, RunState state, ResultType result)
        {
        }

        /// <summary>
        /// Called after all unit tests within a namespace have been executed.
        /// </summary>
        /// <param name="state">The parameter is not used.</param>
        public void EndNamespace(RunState state)
        {
        }

        /// <summary>
        /// Called when there is an error executing a method.
        /// </summary>
        /// <param name="methodType">The parameter is not used.</param>
        /// <param name="message">The parameter is not used.</param>
        /// <param name="stackTrace">The parameter is not used.</param>
        public void ReportError(MethodType methodType, string message, string stackTrace)
        {
        }

        /// <summary>
        /// Write the report.  While other methods are called by a test runner,
        /// this method should be called by the code that creates the report object.
        /// </summary>
        /// <param name="writer">The parameter is not used.</param>
        public void Write(TextWriter writer)
        {
        }
    }
}
