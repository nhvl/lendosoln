﻿namespace LUnitEngine
{
    using System.Reflection;

    /// <summary>
    /// This is the base class for version specific parsers and is the entry point for client code.
    /// </summary>
    public abstract class Parser
    {
        /// <summary>
        /// Factory method to create a parser specific to an NUnit version.
        /// </summary>
        /// <param name="version">The NUnit version.</param>
        /// <returns>A parser specific to an NUnit version.</returns>
        public static Parser GetParser(Version version)
        {
            if (version == Version.Supported.NUnitx2x6x3)
            {
                return new Parsers.Parser_2_6_3();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Parse the assembly into a tree of test runners.
        /// </summary>
        /// <param name="assembly">An assembly containing unit tests.</param>
        /// <returns>A tree of test runners.</returns>
        public abstract Runner ParseAssembly(Assembly assembly);
    }
}
