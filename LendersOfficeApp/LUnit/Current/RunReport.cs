﻿namespace LUnitEngine
{
    using System;
    using System.IO;
    using System.Reflection;

    /// <summary>
    /// This interface is used by LUnit to report the results of an 
    /// execution.  It may also be used prior to execution to discover
    /// the tests and whether or not they will be executed.
    /// </summary>
    public interface IReportRunResults
    {
        /// <summary>
        /// Called prior to processing an assembly containing unit tests.
        /// </summary>
        void BeginAssembly();

        /// <summary>
        /// Called after processing an assembly containing unit tests.
        /// </summary>
        /// <param name="state">The run state for the full assembly.</param>
        void EndAssembly(RunState state);

        /// <summary>
        /// Called prior to execution of any unit tests within a namespace.
        /// </summary>
        /// <param name="name">The namespace.</param>
        void BeginNamespace(string name);

        /// <summary>
        /// Called after all unit tests within a namespace have been executed.
        /// </summary>
        /// <param name="state">The run state for the full namespace.</param>
        void EndNamespace(RunState state);

        /// <summary>
        /// Called prior to execution of any unit tests within a test fixture.
        /// </summary>
        void BeginClass();

        /// <summary>
        /// Called after all unit tests within a test fixture have been executed.
        /// </summary>
        /// <param name="type">The type of the test fixture class.</param>
        /// <param name="state">The aggregate run state for all unit tests within the test fixture.</param>
        void EndClass(Type type, RunState state);

        /// <summary>
        /// Called prior to execution of a test method.
        /// </summary>
        void BeginMethod();

        /// <summary>
        /// Called when there is an error executing a method.
        /// </summary>
        /// <param name="methodType">The method type; test, setup and tear down methods may report errors.</param>
        /// <param name="message">The error message.</param>
        /// <param name="stackTrace">The stack trace where the error occurred.</param>
        void ReportError(MethodType methodType, string message, string stackTrace);

        /// <summary>
        /// Called after execution of a test method.
        /// </summary>
        /// <param name="info">The method information.</param>
        /// <param name="description">Optional description for the method.</param>
        /// <param name="state">The run state for the method (calculated prior to execution).</param>
        /// <param name="result">The result of the execution (or non-execution, depending on the run state).</param>
        void EndMethod(MethodInfo info, string description, RunState state, ResultType result);

        /// <summary>
        /// Write the report.  While other methods are called by a test runner,
        /// this method should be called by the code that creates the report object.
        /// </summary>
        /// <param name="writer">Text writer used to write the results.</param>
        void Write(TextWriter writer);
    }

    /// <summary>
    /// This class is used to retrieve a report instance that generates a specific format.
    /// </summary>
    public static class ReportRunResults
    {
        /// <summary>
        /// Gets or sets a value indicating whether to suppress some of the report output.  Report classes should check this value.
        /// </summary>
        /// <value>A value indicating whether to suppress some of the report output.</value>
        public static bool DEBUGONLY { get; set; }

        /// <summary>
        /// Retrieve a report object that generates the report in the NUnit.Engine.api XML grammar.
        /// </summary>
        /// <param name="assembly">An assembly containing unit tests.</param>
        /// <param name="version">The targeted version of NUnit.</param>
        /// <returns>A report object that generates XML.</returns>
        public static IReportRunResults GetNUnitXML(Assembly assembly, Version version)
        {
            return new NUnitReport(assembly, version);
        }
    }
}
