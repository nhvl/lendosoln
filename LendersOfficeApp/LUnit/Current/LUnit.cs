﻿namespace LUnitEngine
{
    /// <summary>
    /// The LUnit framework defines a set of custom attributes that are attached to
    /// classes and methods.  The names of these attributes are defined here.  The
    /// class structure mimics the fullname of the attribute classes.
    /// </summary>
    public static class LUnit
    {
        /// <summary>
        /// Fullname of the LUnit LoTestAttribute attribute class.
        /// </summary>
        public const string LoTestAttribute = "LUnit.LoTestAttribute";

        /// <summary>
        /// Fullname of the LUnit LoTestFixtureAttribute attribute class.
        /// </summary>
        public const string LoTestFixtureAttribute = "LUnit.LoTestFixtureAttribute";
    }
}