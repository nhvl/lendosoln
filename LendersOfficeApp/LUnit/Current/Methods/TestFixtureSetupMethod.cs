﻿namespace LUnitEngine.Methods
{
    using System.Reflection;
    using LUnitEngine.Attributes;

    /// <summary>
    /// Encapsulate a fixture setup method.
    /// </summary>
    internal sealed class TestFixtureSetUpMethod : Method
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestFixtureSetUpMethod"/> class.
        /// </summary>
        /// <param name="info">The method information.</param>
        /// <param name="attribute">The fixture setup attribute.</param>
        public TestFixtureSetUpMethod(MethodInfo info, TestFixtureSetUpAttribute attribute)
            : base(info)
        {
            this.Attribute = attribute;
        }

        /// <summary>
        /// Gets or sets the fixture setup attribute.
        /// </summary>
        private TestFixtureSetUpAttribute Attribute { get; set; }
    }
}
