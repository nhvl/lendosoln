﻿namespace LUnitEngine.Methods
{
    using System.Collections.Generic;
    using System.Reflection;
    using LUnitEngine.Attributes;

    /// <summary>
    /// Encapsulate a unit test method.
    /// </summary>
    internal sealed class TestMethod : Method
    {
        /// <summary>
        /// The attribute that marks this as a test method.
        /// </summary>
        private TestAttribute attribute;

        /// <summary>
        /// Ignore attribute.
        /// </summary>
        private IgnoreAttribute ignore;

        /// <summary>
        /// Explicit attribute.
        /// </summary>
        private ExplicitAttribute eksplicit;

        /// <summary>
        /// Expected exception attribute.
        /// </summary>
        private ExpectedExceptionAttribute expected;

        /// <summary>
        /// LoTest attribute.
        /// </summary>
        private LoTestAttribute testLo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestMethod"/> class.
        /// </summary>
        /// <param name="info">The method information.</param>
        /// <param name="attribute">The test attribute.</param>
        /// <param name="ignore">Optional ignore attribute.</param>
        /// <param name="eksplicit">Optional explicit attribute.</param>
        /// <param name="expected">Optional expected exception attribute.</param>
        /// <param name="categories">Optional list of category attributes.</param>
        /// <param name="testLo">Optional LoTest attribute.</param>
        public TestMethod(MethodInfo info, TestAttribute attribute, IgnoreAttribute ignore, ExplicitAttribute eksplicit, ExpectedExceptionAttribute expected, List<CategoryAttribute> categories, LoTestAttribute testLo)
            : base(info)
        {
            this.attribute = attribute;
            this.ignore = ignore;
            this.eksplicit = eksplicit;
            this.expected = expected;
            this.Categories = categories;
            this.testLo = testLo;
        }

        /// <summary>
        /// Gets the name of the method.
        /// </summary>
        /// <value>The name of the method.</value>
        public string MethodName
        {
            get
            {
                return this.MethodInfo.Name;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the method should be ignored.
        /// </summary>
        public bool Ignore
        {
            get
            {
                return this.ignore != null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether a method category name must appear in the include list, else the method is ignored.
        /// </summary>
        /// <value>A value whether a method category name must appear in the include list, else the method is ignored.</value>
        public bool Explicit
        {
            get
            {
                return this.eksplicit != null;
            }
        }

        /// <summary>
        /// Gets the name of the type for the expected exception.
        /// </summary>
        /// <value>The name of the type for the expected exception.</value>
        public string ExpectedExceptionType
        {
            get
            {
                return (this.expected == null) ? string.Empty : this.expected.TypeName;
            }
        }

        /// <summary>
        /// Gets the expected message in the expected exception.
        /// </summary>
        /// <value>The expected message in the expected exception.</value>
        public string ExpectedExceptionMessage
        {
            get
            {
                return (this.expected == null) ? string.Empty : this.expected.ExpectedMessage;
            }
        }

        /// <summary>
        /// Gets the list of category names.
        /// </summary>
        /// <value>The list of category names.</value>
        public List<CategoryAttribute> Categories { get; private set; }

        /// <summary>
        /// Gets the description of this method.
        /// </summary>
        /// <value>The description of this method.</value>
        public string Description
        {
            get
            {
                return (this.testLo != null) ? this.testLo.Description : this.attribute.Description;
            }
        }

        /// <summary>
        /// Gets the timeout for execution of this method.
        /// </summary>
        /// <value>The timeout for execution of this method.</value>
        public int TimeoutInSeconds
        {
            get
            {
                return (this.testLo != null) ? this.testLo.TimeoutInSeconds : 0;
            }
        }

        /// <summary>
        /// Calculate the run state based on the attached attributes and the include or exclude list of category names.
        /// </summary>
        /// <param name="includeCategories">The include list of category names.</param>
        /// <param name="excludeCategories">The exclude list of category names.</param>
        /// <returns>The run state.</returns>
        public RunState RunBehavior(IEnumerable<string> includeCategories, IEnumerable<string> excludeCategories)
        {
            if (this.Ignore)
            {
                return RunState.Ignored;
            }

            return CategoryAttribute.RunBehavior(this.Categories, this.eksplicit, includeCategories, excludeCategories);
        }
    }
}
