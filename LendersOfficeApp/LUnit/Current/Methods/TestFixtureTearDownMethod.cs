﻿namespace LUnitEngine.Methods
{
    using System.Reflection;
    using LUnitEngine.Attributes;

    /// <summary>
    /// Encapsulate a fixture tear down method.
    /// </summary>
    internal sealed class TestFixtureTearDownMethod : Method
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestFixtureTearDownMethod"/> class.
        /// </summary>
        /// <param name="info">The method information.</param>
        /// <param name="attribute">The fixture tear down attribute.</param>
        public TestFixtureTearDownMethod(MethodInfo info, TestFixtureTearDownAttribute attribute)
            : base(info)
        {
            this.Attribute = attribute;
        }

        /// <summary>
        /// Gets or sets the fixture tear down attribute.
        /// </summary>
        private TestFixtureTearDownAttribute Attribute { get; set; }
    }
}
