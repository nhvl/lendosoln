﻿namespace LUnitEngine.Methods
{
    using System.Reflection;
    using LUnitEngine.Attributes;

    /// <summary>
    /// Encapsulate a setup method.
    /// </summary>
    internal sealed class SetUpMethod : Method
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetUpMethod"/> class.
        /// </summary>
        /// <param name="info">The method information.</param>
        /// <param name="attribute">The setup attribute.</param>
        public SetUpMethod(MethodInfo info, SetUpAttribute attribute)
            : base(info)
        {
            this.Attribute = attribute;
        }

        /// <summary>
        /// Gets or sets the setup attribute.
        /// </summary>
        private SetUpAttribute Attribute { get; set; }
    }
}
