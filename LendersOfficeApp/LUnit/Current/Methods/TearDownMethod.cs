﻿namespace LUnitEngine.Methods
{
    using System.Reflection;
    using LUnitEngine.Attributes;

    /// <summary>
    /// Encapsulate a tear down method.
    /// </summary>
    internal sealed class TearDownMethod : Method
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TearDownMethod"/> class.
        /// </summary>
        /// <param name="info">The method information.</param>
        /// <param name="attribute">The tear down attribute.</param>
        public TearDownMethod(MethodInfo info, TearDownAttribute attribute)
            : base(info)
        {
            this.Attribute = attribute;
        }

        /// <summary>
        /// Gets or sets the tear down attribute.
        /// </summary>
        private TearDownAttribute Attribute { get; set; }
    }
}
