﻿namespace LUnitEngine
{
    using System;

    /// <summary>
    /// This class defines all the names of the NUnit attributes, and provides
    /// methods for comparing the names for equality.
    /// </summary>
    internal sealed class AttributeName : IEquatable<AttributeName>
    {
        /// <summary>
        /// The name of a category attribute.
        /// </summary>
        public static readonly AttributeName Category = new AttributeName(NUnit.Framework.Category);

        /// <summary>
        /// The name of a description attribute.
        /// </summary>
        public static readonly AttributeName Description = new AttributeName(NUnit.Framework.Description);

        /// <summary>
        /// The name of an expected exception attribute.
        /// </summary>
        public static readonly AttributeName ExpectedException = new AttributeName(NUnit.Framework.ExpectedException);

        /// <summary>
        /// The name of an explicit attribute.
        /// </summary>
        public static readonly AttributeName Explicit = new AttributeName(NUnit.Framework.Explicit);

        /// <summary>
        /// The name of the ignore attribute.
        /// </summary>
        public static readonly AttributeName Ignore = new AttributeName(NUnit.Framework.Ignore);

        /// <summary>
        /// The name of the setup attribute.
        /// </summary>
        public static readonly AttributeName SetUp = new AttributeName(NUnit.Framework.SetUp);

        /// <summary>
        /// The name of a fixture setup attribute.
        /// </summary>
        public static readonly AttributeName SetUpFixture = new AttributeName(NUnit.Framework.SetUpFixture);

        /// <summary>
        /// The name of a tear down attribute.
        /// </summary>
        public static readonly AttributeName TearDown = new AttributeName(NUnit.Framework.TearDown);

        /// <summary>
        /// The name of a test attribute.
        /// </summary>
        public static readonly AttributeName Test = new AttributeName(NUnit.Framework.Test);

        /// <summary>
        /// The name of a test fixture attribute.
        /// </summary>
        public static readonly AttributeName TestFixture = new AttributeName(NUnit.Framework.TestFixture);

        /// <summary>
        /// The name of a test fixture setup attribute.
        /// </summary>
        public static readonly AttributeName TestFixtureSetUp = new AttributeName(NUnit.Framework.TestFixtureSetUp);

        /// <summary>
        /// The name of a test fixture tear down attribute.
        /// </summary>
        public static readonly AttributeName TestFixtureTearDown = new AttributeName(NUnit.Framework.TestFixtureTearDown);

        /// <summary>
        /// The name of a Lenders Office test custom attribute.
        /// </summary>
        public static readonly AttributeName LoTestAttribute = new AttributeName(LUnit.LoTestAttribute);

        /// <summary>
        /// The name of a Lenders Office test fixture custom attribute.
        /// </summary>
        public static readonly AttributeName LoTestFixtureAttribute = new AttributeName(LUnit.LoTestFixtureAttribute);

        /// <summary>
        /// The name value of the attribute.
        /// </summary>
        private string name;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributeName"/> class.
        /// </summary>
        /// <param name="name">The attribute name.</param>
        private AttributeName(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Equality operator.
        /// </summary>
        /// <param name="att">Attribute name.</param>
        /// <param name="value">String that is an attribute name.</param>
        /// <returns>True if the two names match, false otherwise.</returns>
        public static bool operator ==(AttributeName att, string value)
        {
            if (object.ReferenceEquals(att, null) && value == null)
            {
                return true;
            }

            if (object.ReferenceEquals(att, null) || value == null)
            {
                return false;
            }

            return att.name == value;
        }

        /// <summary>
        /// Inequality operator.
        /// </summary>
        /// <param name="att">Attribute name.</param>
        /// <param name="value">String that is an attribute name.</param>
        /// <returns>True if the two names do not match, false if they do match.</returns>
        public static bool operator !=(AttributeName att, string value)
        {
            return !(att == value);
        }

        /// <summary>
        /// Determine whether a given attribute name is supported for an NUnit version.
        /// </summary>
        /// <param name="name">The attribute name.</param>
        /// <param name="version">The NUnit version.</param>
        /// <returns>True if the attribute name is supported by the NUnit version, false otherwise.</returns>
        public static bool IsSupported(string name, Version version)
        {
            if (version == Version.Supported.NUnitx2x6x3)
            {
                return IsSupported_2_6_3(name);
            }

            return false;
        }

        /// <summary>
        /// Override of Object.Equals.
        /// </summary>
        /// <param name="obj">Comparison object.</param>
        /// <returns>True if are equals, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (obj is string)
            {
                return (string)obj == this.name;
            }
            else if (obj is AttributeName)
            {
                return (obj as AttributeName).name == this.name;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Equality operator when both are attribute name instances.
        /// </summary>
        /// <param name="att">The object being compared.</param>
        /// <returns>True if the two objects are equal, false otherwise.</returns>
        public bool Equals(AttributeName att)
        {
            if (att == null)
            {
                return false;
            }

            return att.name == this.name;
        }

        /// <summary>
        /// Retrieve the name of the attribute.
        /// </summary>
        /// <returns>The name of the attribute.</returns>
        public override string ToString()
        {
            return this.name;
        }

        /// <summary>
        /// Retrieve a hash code for the attribute name.
        /// </summary>
        /// <returns>A hash code for the attribute name.</returns>
        public override int GetHashCode()
        {
            return this.name.GetHashCode();
        }

        /// <summary>
        /// This returns true for NUnit v2.6.3 attributes that LUnit supports and makes use of.
        /// </summary>
        /// <param name="name">The name of the attribute.</param>
        /// <returns>True if the attribute is supported, false otherwise.</returns>
        private static bool IsSupported_2_6_3(string name)
        {
            // Description is not supported on purpose.
            return AttributeName.Category == name
                    || AttributeName.ExpectedException == name
                    || AttributeName.Explicit == name
                    || AttributeName.Ignore == name
                    || AttributeName.SetUp == name
                    || AttributeName.SetUpFixture == name
                    || AttributeName.TearDown == name
                    || AttributeName.Test == name
                    || AttributeName.TestFixture == name
                    || AttributeName.TestFixtureSetUp == name
                    || AttributeName.TestFixtureTearDown == name
                    || AttributeName.LoTestAttribute == name;
        }
    }
}
