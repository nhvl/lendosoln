﻿namespace LUnitEngine
{
    using System;

    /// <summary>
    /// This class encapsulates the concept of a version of the NUnit framework.
    /// </summary>
    public sealed class Version : IEquatable<Version>
    {
        /// <summary>
        /// The version number string, e.g., 2.6.3.
        /// </summary>
        private string version;

        /// <summary>
        /// Initializes a new instance of the <see cref="Version"/> class.
        /// </summary>
        /// <param name="version">The version number string.</param>
        private Version(string version)
        {
            this.version = version;
        }

        /// <summary>
        /// Equality method.
        /// </summary>
        /// <param name="other">Other version instance.</param>
        /// <returns>True if the two versions are equal, false otherwise.</returns>
        public bool Equals(Version other)
        {
            return this.version == other.version;
        }

        /// <summary>
        /// Retrieve the version number string.
        /// </summary>
        /// <returns>The version number string.</returns>
        public override string ToString()
        {
            return this.version;
        }

        /// <summary>
        /// This class defines all NUnit versions supported by LUnit.
        /// </summary>
        public static class Supported
        {
            /// <summary>
            /// NUnit v2.6.3 is supported.
            /// </summary>
            public static readonly Version NUnitx2x6x3 = new Version("2.6.3");
        }
    }
}
