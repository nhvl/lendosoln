﻿namespace LUnitEngine.Fixtures
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LUnitEngine.Attributes;
    using LUnitEngine.Methods;

    /// <summary>
    /// Encapsulate a class that contains unit test methods.
    /// </summary>
    internal sealed class TestFixture : Fixture
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestFixture"/> class.
        /// </summary>
        /// <param name="classType">The type of the class.</param>
        /// <param name="ignore">Optional ignore attribute.</param>
        /// <param name="eksplicit">Optional explicit attribute.</param>
        /// <param name="categories">Optional set of category attributes.</param>
        /// <param name="testFixtureLo">Optional LoTestFixture attribute.</param>
        /// <param name="fixSetUp">Optional fixture setup method.</param>
        /// <param name="fixTearDown">Optional fixture tear down method.</param>
        /// <param name="setup">Optional setup method.</param>
        /// <param name="teardown">Optional tear down method.</param>
        /// <param name="tests">The contained unit test methods.</param>
        public TestFixture(
            Type classType,
            IgnoreAttribute ignore,
            ExplicitAttribute eksplicit,
            List<CategoryAttribute> categories,
            LoTestFixtureAttribute testFixtureLo,
            TestFixtureSetUpMethod fixSetUp,
            TestFixtureTearDownMethod fixTearDown,
            SetUpMethod setup,
            TearDownMethod teardown,
            List<TestMethod> tests)
            : base(classType)
        {
            this.Ignore = ignore;
            this.Explicit = eksplicit;
            this.Categories = categories;
            this.LoTestFixture = testFixtureLo;
            this.FixtureSetUp = fixSetUp;
            this.FixtureTearDown = fixTearDown;
            this.SetUp = setup;
            this.TearDown = teardown;

            tests.OrderBy(p => p.MethodName);
            this.Tests = tests;
        }

        /// <summary>
        /// Gets the optional fixture setup method.
        /// </summary>
        /// <value>The optional fixture setup method.</value>
        public TestFixtureSetUpMethod FixtureSetUp { get; private set; }

        /// <summary>
        /// Gets the optional fixture tear down method.
        /// </summary>
        /// <value>The optional fixture tear down method.</value>
        public TestFixtureTearDownMethod FixtureTearDown { get; private set; }

        /// <summary>
        /// Gets the optional setup method.
        /// </summary>
        /// <value>The optional setup method.</value>
        public SetUpMethod SetUp { get; private set; }

        /// <summary>
        /// Gets the optional tear down method.
        /// </summary>
        /// <value>The optional tear down method.</value>
        public TearDownMethod TearDown { get; private set; }

        /// <summary>
        /// Gets the test methods.
        /// </summary>
        /// <value>The test methods.</value>
        public List<TestMethod> Tests { get; private set; }

        /// <summary>
        /// Gets the optional LoTestFixture attribute.
        /// </summary>
        /// <value>The optional LoTestFixture attribute.</value>
        public LoTestFixtureAttribute LoTestFixture { get; private set; }

        /// <summary>
        /// Gets or sets the optional ignore attribute.
        /// </summary>
        /// <value>The optional ignore attribute.</value>
        private IgnoreAttribute Ignore { get; set; }

        /// <summary>
        /// Gets or sets the optional explicit attribute.
        /// </summary>
        /// <value>The optional explicit attribute.</value>
        private ExplicitAttribute Explicit { get; set; }

        /// <summary>
        /// Gets or sets the optional category attributes.
        /// </summary>
        /// <value>The optional category attributes.</value>
        private List<CategoryAttribute> Categories { get; set; }

        /// <summary>
        /// Utility method to calculate the run state for the fixture based upon the NUnit
        /// attributes and the input include or exclude category names.
        /// </summary>
        /// <param name="includeCategories">List of category include names.</param>
        /// <param name="excludeCategories">List of category exclude names.</param>
        /// <returns>The calculated run state.</returns>
        public RunState ShouldRunFixture(IEnumerable<string> includeCategories, IEnumerable<string> excludeCategories)
        {
            if (this.Ignore != null)
            {
                return RunState.Ignored;
            }

            var state = CategoryAttribute.RunBehavior(this.Categories, this.Explicit, includeCategories, excludeCategories);
            if (state != RunState.Run)
            {
                return state;
            }

            if (this.Tests.Count == 1)
            {
                return this.Tests[0].RunBehavior(includeCategories, excludeCategories);
            }

            bool hasIgnore = false;
            bool allNotIncluded = true;
            bool allExcluded = true;
            bool allNotExplicit = true;
            foreach (var test in this.Tests)
            {
                state = test.RunBehavior(includeCategories, excludeCategories);
                switch (state)
                {
                    case RunState.Run:
                        return RunState.Run;
                    case RunState.Ignored:
                        hasIgnore = true;
                        allNotIncluded = false;
                        allExcluded = false;
                        allNotExplicit = false;
                        break;
                    case RunState.NotIncluded:
                        allExcluded = false;
                        allNotExplicit = false;
                        break;
                    case RunState.Excluded:
                        allNotIncluded = false;
                        allNotExplicit = false;
                        break;
                    case RunState.NotExplicit:
                        allNotIncluded = false;
                        allExcluded = false;
                        break;
                    default:
                        throw new System.Exception("Will never get here.");
                }
            }

            if (allNotIncluded)
            {
                return RunState.NotIncluded;
            }

            if (allExcluded)
            {
                return RunState.Excluded;
            }

            if (allNotExplicit)
            {
                return RunState.NotExplicit;
            }

            // included and excluded are mutually exlusive sets.
            // explicit has meaning only when there is an included list
            return hasIgnore ? RunState.Ignored : RunState.NotIncluded;
        }
    }
}
