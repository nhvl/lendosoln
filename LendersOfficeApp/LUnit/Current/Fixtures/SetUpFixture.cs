﻿namespace LUnitEngine.Fixtures
{
    using System;
    using System.Collections.Generic;
    using LUnitEngine.Methods;

    /// <summary>
    /// Encapsulate a class that is used to contain setup and tear down methods for a namespace.
    /// </summary>
    internal sealed class SetUpFixture : Fixture
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetUpFixture"/> class.
        /// </summary>
        /// <param name="classType">The type of the class that is this setup fixture.</param>
        /// <param name="setup">The setup method for the setup fixture.</param>
        /// <param name="teardown">The tear down method for the setup fixture.</param>
        public SetUpFixture(Type classType, SetUpMethod setup, TearDownMethod teardown)
            : base(classType)
        {
            this.SetUp = setup;
            this.TearDown = teardown;
        }

        /// <summary>
        /// Gets the setup method.
        /// </summary>
        /// <value>The setup method.</value>
        public SetUpMethod SetUp { get; private set; }

        /// <summary>
        /// Gets the tear down method.
        /// </summary>
        /// <value>The tear down method.</value>
        public TearDownMethod TearDown { get; private set; }
    }
}
