﻿namespace LUnitEngine
{
    using System;
    using System.Collections.Generic;
    using LUnitEngine.Methods;

    /// <summary>
    /// This is the base class for distinct fixture types in LUnit.
    /// </summary>
    internal abstract class Fixture
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Fixture"/> class.
        /// </summary>
        /// <param name="classType">The type of the fixture class.</param>
        protected Fixture(Type classType)
        {
            this.ClassType = classType;
        }

        /// <summary>
        /// Gets the name of the fixture class.
        /// </summary>
        /// <value>The name of the fixture class.</value>
        public string ClassName
        {
            get
            {
                return this.ClassType.Name;
            }
        }

        /// <summary>
        /// Gets the type of the fixture class.
        /// </summary>
        /// <value>The type of the fixture class.</value>
        public Type ClassType { get; private set; }

        /// <summary>
        /// Calculate an identifier for a fixture class.
        /// </summary>
        /// <param name="classType">The type of the fixture class.</param>
        /// <returns>The identifier for the fixture class.</returns>
        public static string GetIdentifier(Type classType)
        {
            return classType.FullName.GetHashCode().ToString();
        }
    }
}
