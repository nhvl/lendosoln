﻿namespace LUnitEngine
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Xml;

    // Documentation can be found here : https://github.com/nunit/docs/wiki/Test-Result-XML-Format

    /// <summary>
    /// This is a reporting class that generates an XML report conforming to the grammar
    /// defined by the NUnit team.  This grammar is the output of the in-process runner
    /// NUnit.Engine.api, which only works for NUnit versions >= 3.0.0.  However, a subset of
    /// the grammar can be used for earlier versions so it is adopted for LUnit.  
    /// </summary>
    public sealed class NUnitReport : IReportRunResults
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NUnitReport"/> class.
        /// </summary>
        /// <param name="assembly">An assembly containing unit tests.</param>
        /// <param name="version">The targeted NUnit version.</param>
        public NUnitReport(Assembly assembly, Version version)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<test-run/>");
            this.Report = doc;

            this.Initialize(assembly, version);

            this.NamespaceElements = new Stack<XmlElement>();
            this.ClassElements = new Stack<XmlElement>();

            this.NamespaceStarts = new Stack<DateTime>();
            this.ClassStarts = new Stack<DateTime>();
        }

        /// <summary>
        /// Gets the XML report.
        /// </summary>
        /// <value>The XML report.</value>
        public XmlDocument Report { get; private set; }

        /// <summary>
        /// Gets or sets the XML element that represents an assembly.
        /// </summary>
        /// <value>The XML element that represents an assembly.</value>
        private XmlElement AssemblyElement { get; set; }

        /// <summary>
        /// Gets or sets the XML elements that represent namespaces.
        /// </summary>
        /// <value>The XML elements that represent namespaces.</value>
        private Stack<XmlElement> NamespaceElements { get; set; }

        /// <summary>
        /// Gets or sets the XML elements that represent classes.
        /// </summary>
        /// <value>The XML elements that represent classes.</value>
        private Stack<XmlElement> ClassElements { get; set; }

        /// <summary>
        /// Gets or sets the XML element that represents a test method.
        /// </summary>
        /// <value>The XML element that represents a test method.</value>
        private XmlElement MethodElement { get; set; }

        /// <summary>
        /// Gets or sets the timestamp for the start of processing an assembly.
        /// </summary>
        /// <value>The timestamp for the start of processing an assembly.</value>
        private DateTime AssemblyStart { get; set; }

        /// <summary>
        /// Gets or sets the timestamps for the start of processing assemblies.
        /// </summary>
        /// <value>The timestamps for the start of processing assemblies.</value>
        private Stack<DateTime> NamespaceStarts { get; set; }

        /// <summary>
        /// Gets or sets the timestamps for the start of processing classes.
        /// </summary>
        /// <value>The timestamps for the start of processing classes.</value>
        private Stack<DateTime> ClassStarts { get; set; }

        /// <summary>
        /// Gets or sets the timestamp for the start of processing a test method.
        /// </summary>
        /// <value>The timestamp for the start of processing a test method.</value>
        private DateTime MethodStart { get; set; }

        /// <summary>
        /// Called prior to processing an assembly containing unit tests.
        /// </summary>
        public void BeginAssembly()
        {
            XmlElement elem = (XmlElement)this.Report.DocumentElement.SelectSingleNode("test-suite");
            this.AssemblyElement = elem;

            this.AssemblyStart = DateTime.UtcNow;
        }

        /// <summary>
        /// Called after processing an assembly containing unit tests.
        /// </summary>
        /// <param name="state">The run state for the full assembly.</param>
        public void EndAssembly(RunState state)
        {
            var end = DateTime.UtcNow;

            XmlElement elem = this.AssemblyElement;
            DateTime start = this.AssemblyStart;
            this.SetTimestamps(elem, start, end);

            this.SetCounts(elem);
            this.SetState(elem, state);
        }

        /// <summary>
        /// Called prior to execution of any unit tests within a namespace.
        /// </summary>
        /// <param name="name">The namespace.</param>
        public void BeginNamespace(string name)
        {
            XmlElement elem = this.Report.CreateElement("test-suite");
            elem.SetAttribute("name", name);
            elem.SetAttribute("type", "TestSuite");

            if (this.NamespaceElements.Count == 0)
            {
                this.AssemblyElement.AppendChild(elem);
            }
            else
            {
                this.NamespaceElements.Peek().AppendChild(elem);
            }

            this.NamespaceElements.Push(elem);
            this.NamespaceStarts.Push(DateTime.UtcNow);
        }

        /// <summary>
        /// Called after all unit tests within a namespace have been executed.
        /// </summary>
        /// <param name="state">The run state for the full namespace.</param>
        public void EndNamespace(RunState state)
        {
            var end = DateTime.UtcNow;

            XmlElement elem = this.NamespaceElements.Pop();
            DateTime start = this.NamespaceStarts.Pop();
            this.SetTimestamps(elem, start, end);

            this.SetCounts(elem);
            this.SetState(elem, state);
        }

        /// <summary>
        /// Called prior to execution of any unit tests within a test fixture.
        /// </summary>
        public void BeginClass()
        {
            XmlElement elem = this.Report.CreateElement("test-suite");
            elem.SetAttribute("type", "TestFixture");

            this.NamespaceElements.Peek().AppendChild(elem);

            this.ClassElements.Push(elem);
            this.ClassStarts.Push(DateTime.UtcNow);
        }

        /// <summary>
        /// Called after all unit tests within a test fixture have been executed.
        /// </summary>
        /// <param name="type">The type of the test fixture class.</param>
        /// <param name="state">The aggregate run state for all unit tests within the test fixture.</param>
        public void EndClass(Type type, RunState state)
        {
            var end = DateTime.UtcNow;

            XmlElement elem = this.ClassElements.Pop();
            elem.SetAttribute("id", Fixture.GetIdentifier(type));
            elem.SetAttribute("classname", type.Name);

            DateTime start = this.ClassStarts.Pop();
            this.SetTimestamps(elem, start, end);

            this.SetState(elem, state);
            this.SetClassCounts(elem);
        }

        /// <summary>
        /// Called prior to execution of a test method.
        /// </summary>
        public void BeginMethod()
        {
            XmlElement elem = this.Report.CreateElement("test-case");
            this.ClassElements.Peek().AppendChild(elem);

            this.MethodElement = elem;
            this.MethodStart = DateTime.UtcNow;
        }

        /// <summary>
        /// Called when there is an error executing a method.
        /// </summary>
        /// <param name="methodType">The method type; test, setup and tear down methods may report errors.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="stackTrace">The stack trace where the error occurred.</param>
        public void ReportError(MethodType methodType, string errorMessage, string stackTrace)
        {
            XmlElement elem = null;
            switch (methodType)
            {
                case MethodType.NamespaceSetUp:
                    elem = this.NamespaceElements.Peek();
                    elem.SetAttribute("site", "SetUp");
                    break;

                case MethodType.FixtureSetUp:
                    elem = this.ClassElements.Peek();
                    elem.SetAttribute("site", "SetUp");
                    break;

                case MethodType.SetUp:
                    elem = this.MethodElement;
                    elem.SetAttribute("site", "SetUp");
                    break;

                case MethodType.Test:
                    elem = this.MethodElement;
                    this.ClassElements.Peek().AppendChild(elem);
                    break;

                case MethodType.TearDown:
                    elem = this.MethodElement;
                    elem.SetAttribute("site", "TearDown");
                    break;

                case MethodType.FixtureTearDown:
                    elem = this.ClassElements.Peek();
                    elem.SetAttribute("site", "TearDown");
                    break;

                case MethodType.NamespaceTearDown:
                    elem = this.NamespaceElements.Peek();
                    elem.SetAttribute("site", "TearDown");
                    break;
                default:
                    throw new Exception("Will never get here");
            }

            if (elem == null)
            {
                return;
            }

            XmlElement error = this.Report.CreateElement("failure");
            elem.AppendChild(error);

            XmlElement message = this.Report.CreateElement("message");
            XmlCDataSection cdata = this.Report.CreateCDataSection(errorMessage);
            message.AppendChild(cdata);
            error.AppendChild(message);

            if (!string.IsNullOrEmpty(stackTrace))
            {
                XmlElement trace = this.Report.CreateElement("stack-trace");
                cdata = this.Report.CreateCDataSection(stackTrace);
                trace.AppendChild(cdata);
                error.AppendChild(trace);
            }
        }

        /// <summary>
        /// Called after execution of a test method.
        /// </summary>
        /// <param name="info"> The method information.</param>
        /// <param name="description">Optional description for the method.</param>
        /// <param name="state">The run state for the method (calculated prior to execution).</param>
        /// <param name="result">The result of the execution (or non-execution, depending on the run state).</param>
        public void EndMethod(MethodInfo info, string description, RunState state, ResultType result)
        {
            var end = DateTime.UtcNow;

            XmlElement elem = this.MethodElement;
            elem.SetAttribute("id", Method.GetIdentifier(info));
            elem.SetAttribute("methodname", info.Name);
            elem.SetAttribute("result", result.ToString());

            DateTime start = this.MethodStart;
            this.SetTimestamps(elem, start, end);

            this.SetState(elem, state);

            if (!string.IsNullOrEmpty(description))
            {
                var props = elem.OwnerDocument.CreateElement("properties");
                elem.AppendChild(props);

                var prop = elem.OwnerDocument.CreateElement("propery");
                prop.SetAttribute("name", "description");
                prop.SetAttribute("value", description);
                props.AppendChild(prop);
            }

            this.MethodElement = null;
            this.MethodStart = default(DateTime);
        }

        /// <summary>
        /// Write the report.  While other methods are called by a test runner,
        /// this method should be called by the code that creates the report object.
        /// </summary>
        /// <param name="writer">Text writer used to write the results.</param>
        public void Write(TextWriter writer)
        {
            this.Report.Save(writer);
        }

        /// <summary>
        /// Initialize the XML report.
        /// </summary>
        /// <param name="assembly">An assembly containing unit tests.</param>
        /// <param name="version">The targeted NUnit version.</param>
        private void Initialize(Assembly assembly, Version version)
        {
            XmlElement root = this.Report.DocumentElement;
            root.SetAttribute("id", "2"); // hard-coded per NUnit documentation

            XmlElement suite = this.Report.CreateElement("test-suite");
            suite.SetAttribute("type", "Assembly");
            suite.SetAttribute("name", assembly.Location);
            root.AppendChild(suite);

            if (!ReportRunResults.DEBUGONLY)
            {
                XmlElement env = this.Report.CreateElement("environment");
                env.SetAttribute("framework-version", version.ToString());
                env.SetAttribute("clr-version", Environment.Version.ToString());
                env.SetAttribute("os-version", Environment.OSVersion.ToString());
                env.SetAttribute("platform", Environment.OSVersion.Platform.ToString());
                env.SetAttribute("cwd", Environment.CurrentDirectory);
                env.SetAttribute("machine-name", Environment.MachineName);
                env.SetAttribute("user", Environment.UserName);
                env.SetAttribute("user-domain", Environment.UserDomainName);
                env.SetAttribute("culture", CultureInfo.CurrentCulture.ToString());
                env.SetAttribute("uiculture", CultureInfo.CurrentUICulture.ToString());
                env.SetAttribute("os-architecture", assembly.GetName().ProcessorArchitecture.ToString());
                suite.AppendChild(env);
            }
        }

        /// <summary>
        /// Add timestamps to an XML element.
        /// </summary>
        /// <param name="elem">The XML element.</param>
        /// <param name="start">The start timestamp.</param>
        /// <param name="end">The end timestamp.</param>
        private void SetTimestamps(XmlElement elem, DateTime start, DateTime end)
        {
            if (!ReportRunResults.DEBUGONLY)
            {
                elem.SetAttribute("start-time", this.Timestamp(start));
                elem.SetAttribute("end-time", this.Timestamp(end));
                elem.SetAttribute("duration", (end - start).TotalSeconds.ToString("F3"));
            }
        }

        /// <summary>
        /// Add counts to an XML element.
        /// </summary>
        /// <param name="elem">The XML element.</param>
        private void SetCounts(XmlElement elem)
        {
            if (ReportRunResults.DEBUGONLY)
            {
                return;
            }

            int testcaseCount = 0;
            int totalCount = 0;
            int passedCount = 0;
            int failedCount = 0;
            int inconclusiveCount = 0;
            int skippedCount = 0;
            foreach (XmlElement suite in elem.SelectNodes("test-suite"))
            {
                string count = suite.GetAttribute("testcasecount");
                if (!string.IsNullOrEmpty(count))
                {
                    testcaseCount += int.Parse(count);
                }

                string total = suite.GetAttribute("total");
                if (!string.IsNullOrEmpty(total))
                {
                    totalCount += int.Parse(total);

                    string passed = suite.GetAttribute("passed");
                    passedCount += int.Parse(passed);

                    string failed = suite.GetAttribute("failed");
                    failedCount += int.Parse(failed);

                    string inconclusive = suite.GetAttribute("inconclusive");
                    inconclusiveCount += int.Parse(inconclusive);

                    string skipped = suite.GetAttribute("skipped");
                    skippedCount += int.Parse(skipped);
                }
            }

            elem.SetAttribute("testcasecount", testcaseCount.ToString());
            elem.SetAttribute("total", totalCount.ToString());
            elem.SetAttribute("passed", passedCount.ToString());
            elem.SetAttribute("failed", failedCount.ToString());
            elem.SetAttribute("inconclusive", inconclusiveCount.ToString());
            elem.SetAttribute("skipped", skippedCount.ToString());
        }

        /// <summary>
        /// Set counts for a test fixture element.
        /// </summary>
        /// <param name="elem">The XML element representing a test fixture.</param>
        private void SetClassCounts(XmlElement elem)
        {
            var tests = elem.SelectNodes("test-case");
            elem.SetAttribute("testcasecount", tests.Count.ToString());

            int total = 0;
            int passed = 0;
            int failed = 0;
            int inconclusive = 0;
            int skipped = 0;
            foreach (XmlElement test in tests)
            {
                string runstate = test.GetAttribute("runstate");
                if (runstate == "Runnable")
                {
                    total++;
                }

                string result = test.GetAttribute("result");
                switch (result)
                {
                    case "Passed":
                        passed++;
                        break;
                    case "Failed":
                        failed++;
                        break;
                    case "Inconclusive":
                        inconclusive++;
                        break;
                    case "Skipped":
                        skipped++;
                        break;
                    default:
                        throw new Exception("Should not get here");
                }

                elem.SetAttribute("total", total.ToString());
                elem.SetAttribute("passed", passed.ToString());
                elem.SetAttribute("failed", failed.ToString());
                elem.SetAttribute("inconclusive", inconclusive.ToString());
                elem.SetAttribute("skipped", skipped.ToString());
            }
        }

        /// <summary>
        /// Set the run state for an XML element.
        /// </summary>
        /// <param name="elem">The XML element.</param>
        /// <param name="state">The run state.</param>
        private void SetState(XmlElement elem, RunState state)
        {
            switch (state)
            {
                case RunState.Excluded:
                    elem.SetAttribute("runstate", "Skipped");
                    break;

                case RunState.Ignored:
                    elem.SetAttribute("runstate", "Ignored");
                    break;

                case RunState.NotExplicit:
                    elem.SetAttribute("runstate", "Explicit");
                    break;

                case RunState.NotIncluded:
                    elem.SetAttribute("runstate", "Skipped");
                    break;

                case RunState.Run:
                    elem.SetAttribute("runstate", "Runnable");
                    break;

                case RunState.Undefined:
                    elem.SetAttribute("runstate", "NotRunnable");
                    break;
                default:
                    throw new System.Exception("Will never get here.");
            }
        }

        /// <summary>
        /// Convert a timestamp to a string representation.
        /// </summary>
        /// <param name="utc">The timestamp, which is in the UTC representation.</param>
        /// <returns>String representation of the UTC timestamp.</returns>
        private string Timestamp(DateTime utc)
        {
            return utc.ToString(string.Format("yyyy-MM-ddTHH:mm:ss.fffZ", utc));
        }
    }
}
