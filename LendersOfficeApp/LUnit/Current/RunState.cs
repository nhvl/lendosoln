﻿namespace LUnitEngine
{
    /// <summary>
    /// Defines whether or not a test method or test suite will run,
    /// and if not the reason why.
    /// </summary>
    public enum RunState
    {
        /// <summary>
        /// If there is anything to be executed.
        /// </summary>
        Run,

        /// <summary>
        /// If the suite/fixture/method should be ignored.
        /// </summary>
        Ignored,

        /// <summary>
        /// If the suite/fixture/method is not in a list of included category names.
        /// </summary>
        NotIncluded,

        /// <summary>
        /// If the suite/fixture/method is in a list of excluded category names.
        /// </summary>
        Excluded,

        /// <summary>
        /// If the suite/fixture/method is marked as explicit and there were no included category names.
        /// </summary>
        NotExplicit,

        /// <summary>
        /// Preliminary value upon initialization.
        /// </summary>
        Undefined
    }

    /// <summary>
    /// Defines the semantic nature of a test method.
    /// </summary>
    public enum MethodType
    {
        /// <summary>
        /// The method is a unit test.
        /// </summary>
        Test,

        /// <summary>
        /// The method should be executed prior to each unit test in a test fixture.
        /// </summary>
        SetUp,

        /// <summary>
        /// The method should be executed after each unit test in a test fixture.
        /// </summary>
        TearDown,

        /// <summary>
        /// The method should be executed once, prior to any other methods in a test fixture.
        /// </summary>
        FixtureSetUp,

        /// <summary>
        /// The method should be executed once, after all other methods in a test fixture.
        /// </summary>
        FixtureTearDown,

        /// <summary>
        /// The method should be executed once, prior to any other methods within a namespace.
        /// </summary>
        NamespaceSetUp,

        /// <summary>
        /// The method should be executed once, after all other methods within a namespace.
        /// </summary>
        NamespaceTearDown
    }

    /// <summary>
    /// Defines what happens for a test method or test suite when executed.
    /// </summary>
    public enum ResultType
    {
        /// <summary>
        /// The unit test(s) passed.
        /// </summary>
        Passed,

        /// <summary>
        /// One or more unit tests failed.
        /// </summary>
        Failed,

        /// <summary>
        /// One or more unit tests were skipped, the remaining passed or were inconclusive.
        /// </summary>
        Skipped,

        /// <summary>
        /// One of more unit tests were inconclusive, the remaining passed.
        /// </summary>
        Inconclusive
    }
}
