﻿namespace LUnitEngine
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This is the base class for execution as seen by client code.
    /// </summary>
    public abstract class Runner
    {
        /// <summary>
        /// Calculate the run behavior base on various attributes and the input include or exclude category names.
        /// </summary>
        /// <param name="report">Report object where the calculations are reported.</param>
        /// <param name="include">Optional list of include category names.</param>
        /// <param name="exclude">Optional list of exclude category names.</param>
        public abstract void RunBehavior(IReportRunResults report, IEnumerable<string> include, IEnumerable<string> exclude);

        /// <summary>
        /// Execute the unit tests; RunBehavior must be called prior to calling this method.
        /// </summary>
        /// <param name="report">Report object where the results are reported, should be different from that passed into RunBehavior.</param>
        /// <param name="identifiers">Optional list of identifiers that limit the test to be executed.</param>
        public abstract void Run(IReportRunResults report, IEnumerable<string> identifiers);

        /// <summary>
        /// This is the root executable class that client code will be given.
        /// </summary>
        internal sealed class Starter : Runner
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Starter"/> class.
            /// </summary>
            /// <param name="next">An assembly runner.</param>
            public Starter(ConcreteRunner next)
            {
                this.Next = next;
            }

            /// <summary>
            /// Gets or sets the next test runner.
            /// </summary>
            /// <value>The next test runner.</value>
            private ConcreteRunner Next { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the RunBehavior method has been executed.
            /// </summary>
            /// <value>A value indicating whether the RunBehavior method has been executed.</value>
            private bool BehaviorHasBeenRun { get; set; }

            /// <summary>
            /// Calculate the run behavior base on various attributes and the input include or exclude category names.
            /// </summary>
            /// <param name="report">Report object where the calculations are reported, or null if no report is necessary.</param>
            /// <param name="include">Optional list of include category names.</param>
            /// <param name="exclude">Optional list of exclude category names.</param>
            public override void RunBehavior(IReportRunResults report, IEnumerable<string> include, IEnumerable<string> exclude)
            {
                var safeReport = (report == null) ? new NullReport() : report;
                this.Next.RunBehavior(safeReport, RunState.Run, include, exclude);
                this.BehaviorHasBeenRun = true;
            }

            /// <summary>
            /// Execute the unit tests.  If there is an include or exclude list, call RunBehavior prior to this method.
            /// </summary>
            /// <param name="report">Report object where the results are reported, should be different from that passed into RunBehavior.</param>
            /// <param name="identifiers">Optional list of identifiers that limit the test to be executed.</param>
            public override void Run(IReportRunResults report, IEnumerable<string> identifiers)
            {
                if (report == null)
                {
                    throw new ApplicationException("LUnit will not run without a valid IReportRunResults report.");
                }

                HashSet<string> set = null;

                if (!this.BehaviorHasBeenRun)
                {
                    this.RunBehavior(null, null, null);
                }
                else if (identifiers != null)
                {
                    set = new HashSet<string>();
                    foreach (var id in identifiers)
                    {
                        set.Add(id);
                    }
                }

                this.Next.PreRunSetInstance(null);
                this.Next.Run(report, set);
            }
        }

        /// <summary>
        /// This is the base class for LUnit objects that are executable but not exposed to client code.
        /// </summary>
        internal abstract class ConcreteRunner
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ConcreteRunner"/> class.
            /// </summary>
            /// <param name="name">The name of the test runner.</param>
            /// <param name="next">The child test runners.</param>
            protected ConcreteRunner(string name, IEnumerable<ConcreteRunner> next)
            {
                this.Name = name;
                this.Next = next;
                this.State = RunState.Run;
            }

            /// <summary>
            /// Gets the name of the test runner.
            /// </summary>
            /// <value>The name of the test runner.</value>
            public string Name { get; private set; }

            /// <summary>
            /// Gets or sets the child test runners.
            /// </summary>
            /// <value>The child test runners.</value>
            public IEnumerable<ConcreteRunner> Next { get; set; }

            /// <summary>
            /// Gets or sets the run state for this test runner.
            /// </summary>
            /// <value>The run state for this test runner.</value>
            protected RunState State { get; set; }

            /// <summary>
            /// Calculate the run behavior base on various attributes and the input include or exclude category names.
            /// </summary>
            /// <param name="report">Report object where the calculations are reported, or null if no report is necessary.</param>
            /// <param name="containerState">The run state of the containing test runner.</param>
            /// <param name="include">Optional list of include category names.</param>
            /// <param name="exclude">Optional list of exclude category names.</param>
            /// <returns>The calculated run state.</returns>
            public abstract RunState RunBehavior(IReportRunResults report, RunState containerState, IEnumerable<string> include, IEnumerable<string> exclude);

            /// <summary>
            /// Execute the unit tests.
            /// </summary>
            /// <param name="report">Report object where the results are reported, should be different from that passed into RunBehavior.</param>
            /// <param name="identifiers">Optional list of identifiers that limit the test to be executed.</param>
            public abstract void Run(IReportRunResults report, HashSet<string> identifiers);

            /// <summary>
            /// Set the fixture class instance prior to execution of the fixture's methods.
            /// </summary>
            /// <param name="instance">The fixture class instance.</param>
            internal abstract void PreRunSetInstance(object instance);

            /// <summary>
            /// Determine whether this or child test runners contain runnable unit tests.
            /// </summary>
            /// <param name="identifiers">Optional set of identifiers that limit which test fixtures/methods should be run.</param>
            /// <returns>True if there are runnable unit tests, false otherwise.</returns>
            internal abstract bool ContainsRunnable(HashSet<string> identifiers);
        }
    }
}
