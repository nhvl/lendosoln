﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit test fixture attribute.
    /// </summary>
    internal sealed class TestFixtureAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestFixtureAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit test fixture attribute.</param>
        public TestFixtureAttribute(System.Attribute reflected)
            : base(AttributeName.TestFixture)
        {
            this.Description = (string)reflected.GetType().GetProperty("Description").GetValue(reflected);
        }

        /// <summary>
        /// Gets the description of the test fixture.
        /// </summary>
        /// <value>The description of the test fixture.</value>
        public string Description { get; private set; }
    }
}
