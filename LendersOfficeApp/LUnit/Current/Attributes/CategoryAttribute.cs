﻿namespace LUnitEngine.Attributes
{
    using System.Collections.Generic;

    /// <summary>
    /// Encapsulate an NUnit category attribute.
    /// </summary>
    internal sealed class CategoryAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit category attribute.</param>
        public CategoryAttribute(System.Attribute reflected)
            : base(AttributeName.Category)
        {
            this.Value = (string)reflected.GetType().GetProperty("Name").GetValue(reflected);
        }

        /// <summary>
        /// Gets the category value, aka category name.
        /// </summary>
        /// <value>The category value, aka category name.</value>
        public string Value { get; private set; }

        /// <summary>
        /// Utility method that calculates a RunState based upon relevant attributes (usually category attributes)
        /// and an include or exclude list.
        /// </summary>
        /// <param name="held">The category attributes.</param>
        /// <param name="eksplicit">Optional explicit attribute.</param>
        /// <param name="include">List of include category names.</param>
        /// <param name="exclude">List of exclude category names.</param>
        /// <returns>The calculated run state.</returns>
        public static RunState RunBehavior(List<CategoryAttribute> held, ExplicitAttribute eksplicit, IEnumerable<string> include, IEnumerable<string> exclude)
        {
            if (held == null || held.Count == 0)
            {
                return RunState.Run;
            }

            bool hasIncludedItems = false;
            bool hasExcludedItems = false;
            bool hasIncluded = false;
            bool hasExcluded = false;
            if (include != null)
            {
                foreach (var listCat in include)
                {
                    hasIncludedItems = true;
                    if (hasIncluded)
                    {
                        break;
                    }

                    foreach (var heldCat in held)
                    {
                        if (string.Compare(heldCat.Value, listCat, true) == 0)
                        {
                            hasIncluded = true;
                            break;
                        }
                    }
                }
            }
            else if (exclude != null)
            {
                foreach (var listCat in exclude)
                {
                    hasExcludedItems = true;
                    if (hasExcluded)
                    {
                        break;
                    }

                    foreach (var heldCat in held)
                    {
                        if (string.Compare(heldCat.Value, listCat, true) == 0)
                        {
                            hasExcluded = true;
                            break;
                        }
                    }
                }
            }

            if (eksplicit != null && !hasIncluded)
            {
                return RunState.NotExplicit;
            }

            if (!hasIncludedItems && !hasExcludedItems)
            {
                return RunState.Run;
            }

            if (hasExcluded)
            {
                return RunState.Excluded;
            }

            if (hasIncludedItems && !hasIncluded)
            {
                return RunState.NotIncluded;
            }

            return RunState.Run;
        }
    }
}
