﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate the LUnit LoTestFixture attribute.
    /// </summary>
    internal sealed class LoTestFixtureAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoTestFixtureAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An LUnit LoTestFixture attribute.</param>
        public LoTestFixtureAttribute(System.Attribute reflected)
            : base(AttributeName.Category)
        {
            this.Description = (string)reflected.GetType().GetProperty("Description").GetValue(reflected);
        }

        /// <summary>
        /// Gets the description for the attribute.
        /// </summary>
        /// <value>The description for the attribute.</value>
        public string Description { get; private set; }
    }
}