﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit tear down attribute.
    /// </summary>
    internal sealed class TearDownAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TearDownAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit tear down attribute.</param>
        public TearDownAttribute(System.Attribute reflected)
            : base(AttributeName.TearDown)
        {
        }
    }
}
