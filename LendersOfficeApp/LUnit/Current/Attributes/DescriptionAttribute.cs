﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit description attribute.
    /// </summary>
    internal sealed class DescriptionAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DescriptionAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit description attribute.</param>
        public DescriptionAttribute(System.Attribute reflected)
            : base(AttributeName.Description)
        {
            this.Value = (string)reflected.GetType().GetProperty("Description").GetValue(reflected);
        }

        /// <summary>
        /// Gets the description value.
        /// </summary>
        /// <value>The description value.</value>
        public string Value { get; private set; }
    }
}
