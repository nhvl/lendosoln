﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit setup fixture attribute.
    /// </summary>
    internal sealed class SetUpFixtureAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetUpFixtureAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit setup fixture attribute.</param>
        public SetUpFixtureAttribute(System.Attribute reflected)
            : base(AttributeName.SetUpFixture)
        {
        }
    }
}
