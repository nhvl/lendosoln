﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit test fixture setup attribute.
    /// </summary>
    internal sealed class TestFixtureSetUpAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestFixtureSetUpAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit text fixture setup attribute.</param>
        public TestFixtureSetUpAttribute(System.Attribute reflected)
            : base(AttributeName.TestFixtureSetUp)
        {
        }
    }
}
