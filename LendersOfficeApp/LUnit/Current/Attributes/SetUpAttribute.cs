﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit setup attribute.
    /// </summary>
    internal sealed class SetUpAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetUpAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit setup attribute.</param>
        public SetUpAttribute(System.Attribute reflected)
            : base(AttributeName.SetUp)
        {
        }
    }
}
