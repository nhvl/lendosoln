﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit explicit attribute.
    /// </summary>
    internal sealed class ExplicitAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExplicitAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit explicit attribute.</param>
        public ExplicitAttribute(System.Attribute reflected)
            : base(AttributeName.Explicit)
        {
            this.Reason = (string)reflected.GetType().GetProperty("Reason").GetValue(reflected);
        }

        /// <summary>
        /// Gets the reason why the test is marked as requiring an explicit category reference in the include list.
        /// </summary>
        /// <value>The reason why the test is marked as requiring an explicit category reference in the include list.</value>
        public string Reason { get; private set; }
    }
}
