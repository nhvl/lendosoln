﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit test attribute.
    /// </summary>
    internal sealed class TestAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit test attribute.</param>
        public TestAttribute(System.Attribute reflected)
            : base(AttributeName.Test)
        {
            this.Description = (string)reflected.GetType().GetProperty("Description").GetValue(reflected);
        }

        /// <summary>
        /// Gets the description for the marked unit test.
        /// </summary>
        /// <value>The description for the marked unit test.</value>
        public string Description { get; private set; }
    }
}
