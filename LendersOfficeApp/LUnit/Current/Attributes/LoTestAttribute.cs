﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate the LUnit LoTest attribute.
    /// </summary>
    internal sealed class LoTestAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoTestAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An LUnit LoTest attribute.</param>
        public LoTestAttribute(System.Attribute reflected)
            : base(AttributeName.Category)
        {
            this.Description = (string)reflected.GetType().GetProperty("Description").GetValue(reflected);
            this.TimeoutInSeconds = (int)reflected.GetType().GetProperty("Timeout").GetValue(reflected);
        }

        /// <summary>
        /// Gets the description for the attribute.
        /// </summary>
        /// <value>The description for the attribute.</value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the timeout in seconds for the attribute.
        /// </summary>
        /// <value>The timeout in seconds for the attribute.</value>
        public int TimeoutInSeconds { get; private set; }
    }
}