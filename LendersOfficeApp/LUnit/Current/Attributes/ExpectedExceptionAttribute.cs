﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit expected exception attribute.
    /// </summary>
    internal sealed class ExpectedExceptionAttribute : Attribute
    {
        /// <summary>
        /// Magic value to detect when expected exceptions don't specify the exception type.
        /// </summary>
        public const string NOTSPECIFIED = "ADE7C5B1-9E57-44C7-AFD1-3F970121CF5A";

        /// <summary>
        /// Initializes a new instance of the <see cref="ExpectedExceptionAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit expected attribute.</param>
        public ExpectedExceptionAttribute(System.Attribute reflected)
            : base(AttributeName.ExpectedException)
        {
            string typeName = (string)reflected.GetType().GetProperty("ExpectedExceptionName").GetValue(reflected);
            if (!string.IsNullOrEmpty(typeName))
            {
                this.TypeName = typeName;
            }
            else
            {
                System.Type type = (System.Type)reflected.GetType().GetProperty("ExpectedException").GetValue(reflected);
                if (type != null)
                {
                    this.TypeName = type.FullName;
                }
                else
                {
                    this.TypeName = NOTSPECIFIED;
                }
            }

            string message = (string)reflected.GetType().GetProperty("ExpectedMessage").GetValue(reflected);
            if (message != null)
            {
                this.ExpectedMessage = message;
            }
        }

        /// <summary>
        /// Gets the name of the type for the expected exception.
        /// </summary>
        /// <value>The name of the type for the expected exception.</value>
        public string TypeName { get; private set; }

        /// <summary>
        /// Gets the message for the expected exception.
        /// </summary>
        /// <value>The message for the expected exception.</value>
        public string ExpectedMessage { get; private set; }
    }
}
