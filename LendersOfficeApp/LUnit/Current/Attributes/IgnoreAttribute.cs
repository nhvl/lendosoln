﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit ignore attribute.
    /// </summary>
    internal sealed class IgnoreAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IgnoreAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit ignore attribute.</param>
        public IgnoreAttribute(System.Attribute reflected)
            : base(AttributeName.Ignore)
        {
            this.Reason = (string)reflected.GetType().GetProperty("Reason").GetValue(reflected);
        }

        /// <summary>
        /// Gets the reason why the marked unit test/fixture is ignored.
        /// </summary>
        /// <value>The reason why the marked unit test/fixture is ignored.</value>
        public string Reason { get; private set; }
    }
}
