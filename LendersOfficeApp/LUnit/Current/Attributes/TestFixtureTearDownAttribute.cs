﻿namespace LUnitEngine.Attributes
{
    /// <summary>
    /// Encapsulate an NUnit test fixture tear down attribute.
    /// </summary>
    internal sealed class TestFixtureTearDownAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestFixtureTearDownAttribute"/> class.
        /// </summary>
        /// <param name="reflected">An NUnit test fixture tear down attribute.</param>
        public TestFixtureTearDownAttribute(System.Attribute reflected)
            : base(AttributeName.TestFixtureTearDown)
        {
        }
    }
}
