﻿namespace LUnitEngine.Parsers
{
    using System.Collections.Generic;

    /// <summary>
    /// Namespaces are organized into a hierarchical structure.  That structure
    /// is represented here where a single root is enforced for simplicity.
    /// </summary>
    internal sealed class NamespaceTree
    {
        /// <summary>
        /// Keep a map of namespace to associated tree node.
        /// </summary>
        private Dictionary<string, Node> allnodes;

        /// <summary>
        /// Keep a list of all the root namespaces.
        /// </summary>
        private List<string> roots;

        /// <summary>
        /// Initializes a new instance of the <see cref="NamespaceTree"/> class.
        /// </summary>
        public NamespaceTree()
        {
            this.allnodes = new Dictionary<string, Node>();
            this.roots = new List<string>();
        }

        /// <summary>
        /// Ensure that no nulls pollute the tree.
        /// </summary>
        /// <param name="ns">The namespace, which may be null.</param>
        /// <returns>The namespace, with null replaced with an empty string.</returns>
        public static string ResolveNamespace(string ns)
        {
            return ns ?? string.Empty;
        }

        /// <summary>
        /// Retrieve the roots of a namespace hierarchy.
        /// </summary>
        /// <returns>The roots of a namespace hierarchy.</returns>
        public List<string> GetRoots()
        {
            return this.roots;
        }

        /// <summary>
        /// Retrieve the tree node associated with the namespace.
        /// </summary>
        /// <param name="namespace">The namespace.</param>
        /// <returns>The tree node associated with the namespace.</returns>
        public Node FindNode(string @namespace)
        {
            return this.allnodes.ContainsKey(@namespace) ? this.allnodes[@namespace] : null;
        }

        /// <summary>
        /// Add a namespace to the tree.
        /// </summary>
        /// <param name="namespace">The namespace.</param>
        public void AddNamespace(string @namespace)
        {
            string ns = ResolveNamespace(@namespace);
            if (this.allnodes.ContainsKey(ns))
            {
                return;
            }

            if (string.IsNullOrEmpty(ns))
            {
                this.CreateRootNode(string.Empty);
                return;
            }

            int index = ns.Length;
            string working = ns;
            while (index > 0)
            {
                index = working.LastIndexOf('.');
                working = this.HandleNamespace(working);
                if (this.allnodes.ContainsKey(working))
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Recursive method that deals with each component of a namespace.
        /// </summary>
        /// <param name="ns">The namespace.</param>
        /// <returns>The undigested remnant of the namespace.</returns>
        private string HandleNamespace(string ns)
        {
            string working = ns;
            int index = ns.LastIndexOf('.');
            if (index > 0)
            {
                working = ns.Substring(0, index);
                if (this.allnodes.ContainsKey(working))
                {
                    this.AddChildNode(ns, working);
                }
                else
                {
                    this.HandleNamespace(working);
                    this.AddChildNode(ns, working);
                }
            }
            else if (this.allnodes.ContainsKey(string.Empty))
            {
                this.AddChildNode(ns, string.Empty);
            }
            else
            {
                this.CreateRootNode(ns);
            }

            return working;
        }

        /// <summary>
        /// Create a node that is a root in the namespace tree.
        /// </summary>
        /// <param name="namespace">The namespace.</param>
        private void CreateRootNode(string @namespace)
        {
            var node = new Node(@namespace);
            this.allnodes[@namespace] = node;
            this.roots.Add(@namespace);
        }

        /// <summary>
        /// Add a namespace as a child to a pre-existing namespace.
        /// </summary>
        /// <param name="namespace">The child namespace.</param>
        /// <param name="parentNS">The parent namespace.</param>
        private void AddChildNode(string @namespace, string parentNS)
        {
            var parent = this.allnodes[parentNS];
            var node = new Node(@namespace);
            parent.Children.Add(node);
            this.allnodes[@namespace] = node;
        }

        /// <summary>
        /// Node in a namespace tree.
        /// </summary>
        public class Node
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Node"/> class.
            /// </summary>
            /// <param name="namespace">The namespace.</param>
            public Node(string @namespace)
            {
                this.Namespace = @namespace;
                this.Children = new List<Node>();
            }

            /// <summary>
            /// Gets the namespace name.
            /// </summary>
            /// <value>The namespace name.</value>
            public string Namespace { get; private set; }

            /// <summary>
            /// Gets the list of child namespaces.
            /// </summary>
            /// <value>The list of child namespaces.</value>
            public List<Node> Children { get; private set; }
        }
    }
}
