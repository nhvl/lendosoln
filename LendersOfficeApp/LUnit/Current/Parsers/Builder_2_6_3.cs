﻿namespace LUnitEngine.Parsers
{
    using System.Collections.Generic;
    using System.Linq;
    using LUnitEngine.Attributes;
    using LUnitEngine.Fixtures;
    using LUnitEngine.Methods;
    using LUnitEngine.Runners;

    /// <summary>
    /// Assemble a tree of test runners for NUnit v2.6.3.
    /// </summary>
    internal static class Builder_2_6_3
    {
        /// <summary>
        /// Create the tree of test runners.
        /// </summary>
        /// <param name="assemblyName">The name of the assembly that contains the unit tests.</param>
        /// <param name="list">A list of classes that have been marked with NUnit attributes.</param>
        /// <param name="tree">A tree that contains the namespace hierarchy.</param>
        /// <returns>An assembly runner.</returns>
        public static AssemblyRunner CreateRunners(string assemblyName, List<DecoratedClass> list, NamespaceTree tree)
        {
            var dictionary = new Dictionary<string, List<DecoratedClass>>();
            foreach (var item in list)
            {
                if (!dictionary.ContainsKey(item.Namespace))
                {
                    var dictList = new List<DecoratedClass>();
                    dictionary[item.Namespace] = dictList;
                }

                dictionary[item.Namespace].Add(item);
            }

            var runners = new List<Runner.ConcreteRunner>();
            var roots = tree.GetRoots();
            foreach (string ns in roots)
            {
                var node = tree.FindNode(ns);
                var runner = CreateRunner(node, dictionary);
                runners.Add(runner);
            }

            return new AssemblyRunner(assemblyName, runners);
        }

        /// <summary>
        /// Create a test runner for a given namespace.
        /// </summary>
        /// <param name="node">A structure that represents the namespace.</param>
        /// <param name="dictionary">Map from namespace name to test fixtures that are in that namespace.</param>
        /// <returns>A namespace runner.</returns>
        private static NamespaceRunner CreateRunner(NamespaceTree.Node node, Dictionary<string, List<DecoratedClass>> dictionary)
        {
            SetUpFixture setup = null;
            var next = new List<Runner.ConcreteRunner>();

            if (dictionary.ContainsKey(node.Namespace))
            {
                var classes = dictionary[node.Namespace];
                var tuple = Process(classes);
                setup = tuple.Item2;

                foreach (var fixture in tuple.Item1)
                {
                    var methods = new List<MethodRunner>();
                    foreach (var test in fixture.Tests)
                    {
                        methods.Add(new MethodRunner(fixture.SetUp, test, fixture.TearDown));
                    }

                    next.Add(new ClassRunner(methods, fixture.FixtureSetUp, fixture, fixture.FixtureTearDown));
                }
            }

            foreach (var child in node.Children)
            {
                var runner = CreateRunner(child, dictionary);
                next.Add(runner);
            }

            int index = node.Namespace.LastIndexOf('.');
            string name = (index > 0) ? node.Namespace.Substring(index + 1) : node.Namespace;

            return new NamespaceRunner(name, next.OrderBy(p => p.Name), setup);
        }

        /// <summary>
        /// Separate the set of marked fixtures into a single setup fixture and a list of test fixture.
        /// </summary>
        /// <param name="classes">The set of marked fixture.</param>
        /// <returns>A tuple with the list of test fixtures and the single setup fixture.</returns>
        private static System.Tuple<List<TestFixture>, SetUpFixture> Process(List<DecoratedClass> classes)
        {
            SetUpFixture setup = null;
            var tests = new List<TestFixture>();

            // NOTE : NUnit can run classes with test methods that aren't marked with TestFixture
            //        and the input classes already only contains classes with test methods
            foreach (var type in classes)
            {
                bool isSetUp = false;
                var attributes = new List<Attribute>();
                foreach (var decoration in type.Decorations)
                {
                    var attribute = Attribute.Create(decoration);
                    attributes.Add(attribute);

                    if (attribute is SetUpFixtureAttribute)
                    {
                        isSetUp = true;
                    }
                }

                if (isSetUp)
                {
                    setup = ProcessSetUp(type);
                }
                else
                {
                    var fixture = ProcessTest(type, attributes);
                    tests.Add(fixture);
                }
            }

            return new System.Tuple<List<TestFixture>, SetUpFixture>(tests, setup);
        }

        /// <summary>
        /// Construct a setup fixture from the marked items within the class.
        /// </summary>
        /// <param name="type">The class consisting of marked items.</param>
        /// <returns>A setup fixture.</returns>
        private static SetUpFixture ProcessSetUp(DecoratedClass type)
        {
            SetUpMethod setup = null;
            TearDownMethod teardown = null;
            foreach (var method in type.Methods)
            {
                foreach (var decoration in method.Decorations)
                {
                    var attribute = Attribute.Create(decoration);

                    if (attribute is SetUpAttribute)
                    {
                        setup = new SetUpMethod(method.MethodInfo, attribute as SetUpAttribute);
                        break;
                    }
                    else if (attribute is TearDownAttribute)
                    {
                        teardown = new TearDownMethod(method.MethodInfo, attribute as TearDownAttribute);
                        break;
                    }
                }
            }

            return new SetUpFixture(type.ClassType, setup, teardown);
        }

        /// <summary>
        /// Construct a test fixture from the marked items within a class.
        /// </summary>
        /// <param name="type">The class consisting of marked items.</param>
        /// <param name="typeAttributes">The set of attributes that mark the class.</param>
        /// <returns>A test fixture.</returns>
        private static TestFixture ProcessTest(DecoratedClass type, List<Attribute> typeAttributes)
        {
            IgnoreAttribute typeIgnore = null;
            ExplicitAttribute typeExplicit = null;
            LoTestFixtureAttribute testFixtureLo = null;
            var typeCategories = new List<CategoryAttribute>();

            foreach (var attribute in typeAttributes)
            {
                if (attribute is IgnoreAttribute)
                {
                    typeIgnore = attribute as IgnoreAttribute;
                }
                else if (attribute is ExplicitAttribute)
                {
                    typeExplicit = attribute as ExplicitAttribute;
                }
                else if (attribute is CategoryAttribute)
                {
                    typeCategories.Add(attribute as CategoryAttribute);
                }
                else if (attribute is LoTestFixtureAttribute)
                {
                    testFixtureLo = attribute as LoTestFixtureAttribute;
                }
            }

            SetUpMethod setup = null;
            TearDownMethod teardown = null;
            TestFixtureSetUpMethod fixtureSetUp = null;
            TestFixtureTearDownMethod fixtureTearDown = null;
            var tests = new List<TestMethod>();

            foreach (var method in type.Methods)
            {
                TestAttribute test = null;
                IgnoreAttribute ignore = null;
                ExpectedExceptionAttribute expected = null;
                ExplicitAttribute eksplicit = null;
                LoTestAttribute testLo = null;
                var categories = new List<CategoryAttribute>();

                foreach (var decoration in method.Decorations)
                {
                    var attribute = Attribute.Create(decoration);

                    if (attribute is SetUpAttribute)
                    {
                        setup = new SetUpMethod(method.MethodInfo, attribute as SetUpAttribute);
                        break;
                    }
                    else if (attribute is TearDownAttribute)
                    {
                        teardown = new TearDownMethod(method.MethodInfo, attribute as TearDownAttribute);
                        break;
                    }
                    else if (attribute is TestFixtureSetUpAttribute)
                    {
                        fixtureSetUp = new TestFixtureSetUpMethod(method.MethodInfo, attribute as TestFixtureSetUpAttribute);
                        break;
                    }
                    else if (attribute is TestFixtureTearDownAttribute)
                    {
                        fixtureTearDown = new TestFixtureTearDownMethod(method.MethodInfo, attribute as TestFixtureTearDownAttribute);
                        break;
                    }
                    else if (attribute is IgnoreAttribute)
                    {
                        ignore = attribute as IgnoreAttribute;
                    }
                    else if (attribute is ExpectedExceptionAttribute)
                    {
                        expected = attribute as ExpectedExceptionAttribute;
                    }
                    else if (attribute is ExplicitAttribute)
                    {
                        eksplicit = attribute as ExplicitAttribute;
                    }
                    else if (attribute is TestAttribute)
                    {
                        test = attribute as TestAttribute;
                    }
                    else if (attribute is CategoryAttribute)
                    {
                        categories.Add(attribute as CategoryAttribute);
                    }
                    else if (attribute is LoTestAttribute)
                    {
                        testLo = attribute as LoTestAttribute;
                    }
                }

                // LUnit does not currently support parameter value injection via the TestCaseAttribute or the Values
                // parameter attribute.  As a result, we'll simply exclude any tests with parameters.
                if (test != null && method.MethodInfo.GetParameters().Length == 0)
                {
                    var testMethod = new TestMethod(method.MethodInfo, test, ignore, eksplicit, expected, categories, testLo);
                    tests.Add(testMethod);
                }
            }

            return new TestFixture(type.ClassType, typeIgnore, typeExplicit, typeCategories, testFixtureLo, fixtureSetUp, fixtureTearDown, setup, teardown, tests);
        }
    }
}
