﻿namespace LUnitEngine.Parsers
{
    using System.Reflection;

    /// <summary>
    /// Parse an assembly containing unit tests for NUnit v2.6.3.
    /// </summary>
    internal sealed class Parser_2_6_3 : Parser
    {
        /// <summary>
        /// Parse an assembly with unit tests.
        /// </summary>
        /// <param name="assembly">The assembly containing unit tests.</param>
        /// <returns>A test runner.</returns>
        public override Runner ParseAssembly(Assembly assembly)
        {
            var items = Reader.ReadAssembly(assembly, Version.Supported.NUnitx2x6x3);
            var tree = Distiller.ConstructNamepaceTree(items);
            var assemRunner = Builder_2_6_3.CreateRunners(assembly.Location, items, tree);
            return new Runner.Starter(assemRunner);
        }
    }
}
