﻿namespace LUnitEngine.Parsers
{
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// Extract all code constructs marked with NUnit attributes from an assembly.
    /// </summary>
    internal static class Reader
    {
        /// <summary>
        /// Extract all code constructs marked with NUnit attributes from an assembly.
        /// </summary>
        /// <param name="assembly">An assembly containing unit tests.</param>
        /// <param name="version">The version of NUnit that the unit tests target.</param>
        /// <returns>A list of marked code constructs.</returns>
        public static List<DecoratedClass> ReadAssembly(Assembly assembly, Version version)
        {
            var classes = new List<DecoratedClass>();

            // NOTE: NUnit can run classes with test methods that aren't marked with TestFixture
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                string className = type.Name;
                var classDecorations = ReadAttributes(type, version);

                var methods = new List<DecoratedMethod>();
                foreach (var info in type.GetMethods(BindingFlags.Public | BindingFlags.Instance))
                {
                    var methodDecorations = ReadAttributes(info, version);
                    if (methodDecorations.Count > 0)
                    {
                        methods.Add(new DecoratedMethod(info, methodDecorations));
                    }
                }

                if (methods.Count > 0)
                {
                    classes.Add(new DecoratedClass(type, methods, classDecorations));
                }
            }

            return classes;
        }

        /// <summary>
        /// Read supported attributes from a code construct.
        /// </summary>
        /// <param name="member">The code construct.</param>
        /// <param name="version">The targeted version of NUnit.</param>
        /// <returns>A list of supported attributes.</returns>
        private static List<System.Attribute> ReadAttributes(MemberInfo member, Version version)
        {
            var attributes = new List<System.Attribute>();
            foreach (var attribute in member.GetCustomAttributes())
            {
                if (AttributeName.IsSupported(attribute.GetType().FullName, version))
                {
                    attributes.Add(attribute);
                }
            }

            return attributes;
        }
    }
}
