﻿namespace LUnitEngine.Parsers
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Encapsulate the notion of a class that is marked with NUnit attributes.
    /// </summary>
    internal sealed class DecoratedClass : DecoratedItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DecoratedClass"/> class.
        /// </summary>
        /// <param name="type">The type of the class.</param>
        /// <param name="methods">The marked methods within the class.</param>
        /// <param name="decorations">The attributes that mark the class.</param>
        public DecoratedClass(Type type, List<DecoratedMethod> methods, List<System.Attribute> decorations)
            : base(decorations)
        {
            this.ClassType = type;
            this.Methods = methods;
        }

        /// <summary>
        /// Gets the namespace for the class.
        /// </summary>
        /// <value>The namespace for the class.</value>
        public string Namespace
        {
            get
            {
                return this.ClassType.Namespace ?? string.Empty;
            }
        }

        /// <summary>
        /// Gets the type of the class.
        /// </summary>
        /// <value>The type of the class.</value>
        public Type ClassType { get; private set; }

        /// <summary>
        /// Gets the set of marked methods contained within the class.
        /// </summary>
        /// <value>The set of marked methods contained within the class.</value>
        public List<DecoratedMethod> Methods { get; private set; }
    }
}
