﻿namespace LUnitEngine.Parsers
{
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// Base class for code constructs that can be marked with attributes.
    /// </summary>
    internal abstract class DecoratedItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DecoratedItem"/> class.
        /// </summary>
        /// <param name="decorations">The set of attributes marking a code construct.</param>
        protected DecoratedItem(List<System.Attribute> decorations)
        {
            this.Decorations = decorations;
        }

        /// <summary>
        /// Gets the set of attributes marking a code construct.
        /// </summary>
        /// <value>The set of attributes marking a code construct.</value>
        public List<System.Attribute> Decorations { get; private set; }
    }
}
