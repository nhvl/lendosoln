﻿namespace LUnitEngine.Parsers
{
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// Encapsulate the notion of a method that is marked with NUnit attributes.
    /// </summary>
    internal sealed class DecoratedMethod : DecoratedItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DecoratedMethod"/> class.
        /// </summary>
        /// <param name="info">The method information.</param>
        /// <param name="decorations">The list of marking attributes.</param>
        public DecoratedMethod(MethodInfo info, List<System.Attribute> decorations)
            : base(decorations)
        {
            this.MethodInfo = info;
        }

        /// <summary>
        /// Gets the method information.
        /// </summary>
        /// <value>The method information.</value>
        public MethodInfo MethodInfo { get; private set; }
    }
}
