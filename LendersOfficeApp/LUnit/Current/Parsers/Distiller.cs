﻿namespace LUnitEngine.Parsers
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Construct a tree that represents the hierarchy of namespace relationships.
    /// </summary>
    internal static class Distiller
    {
        /// <summary>
        /// Construct a tree that represents the hierarchy of namespace relationships.
        /// </summary>
        /// <param name="list">List of all marked classes for an assembly.</param>
        /// <returns>A tree that represents the hierarchy of namespace relationships.</returns>
        public static NamespaceTree ConstructNamepaceTree(List<DecoratedClass> list)
        {
            var ordered = list.OrderBy(p => p.Namespace);
            NamespaceTree tree = new NamespaceTree();
            foreach (var item in list)
            {
                tree.AddNamespace(item.ClassType.Namespace);
            }

            return tree;
        }
    }
}
