﻿namespace LUnitEngine.Runners
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Reflection;
    using LUnitEngine.Fixtures;
    using LUnitEngine.Methods;

    /// <summary>
    /// Test runner that runs all tests within a test fixture.
    /// </summary>
    internal sealed class ClassRunner : Runner.ConcreteRunner
    {
        /// <summary>
        /// Fixture setup method for the class.
        /// </summary>
        private TestFixtureSetUpMethod setup;

        /// <summary>
        /// The test fixture represented by the class.
        /// </summary>
        private TestFixture fixture;

        /// <summary>
        /// Fixture tear down method for the class.
        /// </summary>
        private TestFixtureTearDownMethod teardown;

        /// <summary>
        /// Instance of the class.
        /// </summary>
        private object instance;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassRunner"/> class.
        /// </summary>
        /// <param name="next">The set of runners contained by this runner.</param>
        /// <param name="setup">Fixture setup method for the class.</param>
        /// <param name="fixture">The test fixture represented by the class.</param>
        /// <param name="teardown">Fixture tear down method for the class.</param>
        public ClassRunner(IEnumerable<Runner.ConcreteRunner> next, TestFixtureSetUpMethod setup, TestFixture fixture, TestFixtureTearDownMethod teardown)
            : base(fixture.ClassName, next)
        {
            this.setup = setup;
            this.fixture = fixture;
            this.teardown = teardown;
        }

        /// <summary>
        /// Determine the run state aggregated from all contained tests.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        /// <param name="containerState">The run state of the containing runner.</param>
        /// <param name="include">Optional list of included category names.</param>
        /// <param name="exclude">Optional list of excluded category names.</param>
        /// <returns>The calculated run state.</returns>
        public override RunState RunBehavior(IReportRunResults report, RunState containerState, IEnumerable<string> include, IEnumerable<string> exclude)
        {
            report.BeginClass();
            this.State = this.fixture.ShouldRunFixture(include, exclude);
            Utility.CalculateRunStage(this.Next, report, this.State, include, exclude);
            report.EndClass(this.fixture.ClassType, this.State);
            return this.State;
        }

        /// <summary>
        /// Execute the contained tests; RunBehavior must be called prior to this method, with a different report object.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        /// <param name="identifiers">Optional list of fixture/method identifiers to limit which tests are executed.</param>
        public override void Run(IReportRunResults report, HashSet<string> identifiers)
        {
            bool runnable = this.ContainsRunnable(identifiers);

            report.BeginClass();
            if (runnable && this.State == RunState.Run)
            {
                if (this.instance == null)
                {
                    report.ReportError(MethodType.FixtureSetUp, "Constructor failed", string.Empty);
                    this.SetState(RunState.Undefined);
                }
                else if (this.setup != null)
                {
                    bool ok = Utility.RunSetUpOrTearDownMethod(report, this.instance, this.setup, MethodType.FixtureSetUp, this.State);
                    if (!ok)
                    {
                        this.SetState(RunState.Undefined);
                    }
                }
            }

            foreach (var method in this.Next)
            {
                method.Run(report, identifiers);
            }

            if (runnable && this.teardown != null && this.State == RunState.Run)
            {
                Utility.RunSetUpOrTearDownMethod(report, this.instance, this.teardown, MethodType.FixtureTearDown, this.State);
            }

            report.EndClass(this.fixture.ClassType, this.State);
        }

        /// <summary>
        /// Tests are executed as member methods of an instance of a fixture class.
        /// The instance is set using this method.
        /// </summary>
        /// <param name="instance">The parameter is not used.</param>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Unit test runners much catch and report on errors, rather than propagate them.")]
        internal override void PreRunSetInstance(object instance)
        {
            try
            {
                ConstructorInfo ctor = this.fixture.ClassType.GetConstructor(Type.EmptyTypes);
                this.instance = ctor.Invoke(Type.EmptyTypes);
            }
            catch
            {
                // allow null instance to represent the failure of the constructor
            }

            foreach (var item in this.Next)
            {
                item.PreRunSetInstance(this.instance);
            }
        }

        /// <summary>
        /// Set the run state for this and contained runners.
        /// </summary>
        /// <param name="state">The run state.</param>
        internal void SetState(RunState state)
        {
            this.State = state;

            foreach (MethodRunner method in this.Next)
            {
                method.SetState(state);
            }
        }

        /// <summary>
        /// Determines whether or not there are contained items that will be executed.
        /// </summary>
        /// <param name="identifiers">Optional list of fixture/method identifiers to limit which tests are executed.</param>
        /// <returns>True if there are contained items that will be executed, false otherwise.</returns>
        internal override bool ContainsRunnable(HashSet<string> identifiers)
        {
            if (identifiers == null || identifiers.Count == 0)
            {
                return true;
            }

            string id = Fixture.GetIdentifier(this.fixture.ClassType);
            if (identifiers.Contains(id))
            {
                foreach (MethodRunner method in this.Next)
                {
                    string methodId = method.GetIdentifier();
                    if (!identifiers.Contains(methodId))
                    {
                        identifiers.Add(methodId);
                    }
                }

                return true;
            }
            else
            {
                foreach (var method in this.Next)
                {
                    if (method.ContainsRunnable(identifiers))
                    {
                        return true;
                    }
                }

                return false;
            }
        }
    }
}
