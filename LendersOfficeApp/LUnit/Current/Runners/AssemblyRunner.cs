﻿namespace LUnitEngine.Runners
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Test runner that runs all tests within an assembly.
    /// </summary>
    internal sealed class AssemblyRunner : Runner.ConcreteRunner
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssemblyRunner"/> class.
        /// </summary>
        /// <param name="name">The assembly's name.</param>
        /// <param name="next">A list of runners that are children of this runner.</param>
        internal AssemblyRunner(string name, IEnumerable<Runner.ConcreteRunner> next)
            : base(name, next)
        {
        }

        /// <summary>
        /// Determine the run state aggregated from all contained tests.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        /// <param name="containerState">The run state of the containing runner.</param>
        /// <param name="include">Optional list of included category names.</param>
        /// <param name="exclude">Optional list of excluded category names.</param>
        /// <returns>The calculated run state.</returns>
        public override RunState RunBehavior(IReportRunResults report, RunState containerState, IEnumerable<string> include, IEnumerable<string> exclude)
        {
            report.BeginAssembly();
            this.State = Utility.CalculateRunStage(this.Next, report, containerState, include, exclude);
            report.EndAssembly(this.State);
            return this.State;
        }

        /// <summary>
        /// Execute the contained tests; RunBehavior must be called prior to this method, with a different report object.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        /// <param name="identifiers">Optional list of fixture/method identifiers to limit which tests are executed.</param>
        public override void Run(IReportRunResults report, HashSet<string> identifiers)
        {
            report.BeginAssembly();
            foreach (var ns in this.Next)
            {
                ns.Run(report, identifiers);
            }

            report.EndAssembly(this.State);
        }

        /// <summary>
        /// Tests are executed as member methods of an instance of a fixture class.
        /// The instance is set using this method.
        /// </summary>
        /// <param name="instance">The parameter is not used.</param>
        internal override void PreRunSetInstance(object instance)
        {
            foreach (var ns in this.Next)
            {
                ns.PreRunSetInstance(null);
            }
        }

        /// <summary>
        /// Determines whether or not there are contained items that will be executed.
        /// </summary>
        /// <param name="identifiers">Optional list of fixture/method identifiers to limit which tests are executed.</param>
        /// <returns>True if there are contained items that will be executed, false otherwise.</returns>
        internal override bool ContainsRunnable(HashSet<string> identifiers)
        {
            foreach (var ns in this.Next)
            {
                if (ns.ContainsRunnable(identifiers))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
