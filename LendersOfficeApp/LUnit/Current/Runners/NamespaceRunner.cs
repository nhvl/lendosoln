﻿namespace LUnitEngine.Runners
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using LUnitEngine.Fixtures;
    using LUnitEngine.Methods;

    /// <summary>
    /// Test runner that runs all test within a namespace.
    /// </summary>
    internal sealed class NamespaceRunner : Runner.ConcreteRunner
    {
        /// <summary>
        /// The namespace.
        /// </summary>
        private string nameSpace;

        /// <summary>
        /// The setup method.
        /// </summary>
        private SetUpMethod setup;

        /// <summary>
        /// The tear down method.
        /// </summary>
        private TearDownMethod teardown;

        /// <summary>
        /// Instance of the setup fixture associated with this namespace.
        /// </summary>
        private object instance;

        /// <summary>
        /// Initializes a new instance of the <see cref="NamespaceRunner"/> class.
        /// </summary>
        /// <param name="namespace">The namespace.</param>
        /// <param name="next">List of contained runners.</param>
        /// <param name="fixture">Optional setup fixture associated with this namespace.</param>
        public NamespaceRunner(string @namespace, IEnumerable<Runner.ConcreteRunner> next, SetUpFixture fixture)
            : base(@namespace, next)
        {
            this.nameSpace = @namespace;

            if (fixture != null)
            {
                this.setup = fixture.SetUp;
                this.teardown = fixture.TearDown;
            }
        }

        /// <summary>
        /// Determine the run state aggregated from all contained tests.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        /// <param name="containerState">The run state of the containing runner.</param>
        /// <param name="include">Optional list of included category names.</param>
        /// <param name="exclude">Optional list of excluded category names.</param>
        /// <returns>The calculated run state.</returns>
        public override RunState RunBehavior(IReportRunResults report, RunState containerState, IEnumerable<string> include, IEnumerable<string> exclude)
        {
            report.BeginNamespace(this.nameSpace);
            this.State = Utility.CalculateRunStage(this.Next, report, containerState, include, exclude);
            report.EndNamespace(this.State);
            return this.State;
        }

        /// <summary>
        /// Execute the contained tests; RunBehavior must be called prior to this method, with a different report object.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        /// <param name="identifiers">Optional list of fixture/method identifiers to limit which tests are executed.</param>
        public override void Run(IReportRunResults report, HashSet<string> identifiers)
        {
            bool runnable = this.ContainsRunnable(identifiers);

            report.BeginNamespace(this.nameSpace);
            if (runnable && this.setup != null && this.State == RunState.Run)
            {
                bool ok = Utility.RunSetUpOrTearDownMethod(report, this.instance, this.setup, MethodType.NamespaceSetUp, this.State);
                if (!ok)
                {
                    this.SetState(RunState.Undefined);
                }
            }

            foreach (var type in this.Next)
            {
                type.Run(report, identifiers);
            }

            if (runnable && this.teardown != null && this.State == RunState.Run)
            {
                Utility.RunSetUpOrTearDownMethod(report, this.instance, this.teardown, MethodType.NamespaceTearDown, this.State);
            }

            report.EndNamespace(this.State);
        }

        /// <summary>
        /// Set the instance of the setup fixture, and propagate call to contained runners.
        /// </summary>
        /// <param name="instance">The parameter is not used.</param>
        internal override void PreRunSetInstance(object instance)
        {
            if (this.setup != null)
            {
                ConstructorInfo ctor = this.setup.MethodInfo.DeclaringType.GetConstructor(Type.EmptyTypes);
                this.instance = ctor.Invoke(Type.EmptyTypes);
            }
            else if (this.teardown != null)
            {
                ConstructorInfo ctor = this.teardown.MethodInfo.DeclaringType.GetConstructor(Type.EmptyTypes);
                this.instance = ctor.Invoke(Type.EmptyTypes);
            }

            foreach (var type in this.Next)
            {
                type.PreRunSetInstance(null);
            }
        }

        /// <summary>
        /// Determines whether or not there are contained items that will be executed.
        /// </summary>
        /// <param name="identifiers">Optional list of fixture/method identifiers to limit which tests are executed.</param>
        /// <returns>True if there are contained items that will be executed, false otherwise.</returns>
        internal override bool ContainsRunnable(HashSet<string> identifiers)
        {
            foreach (var type in this.Next)
            {
                if (type.ContainsRunnable(identifiers))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Set the run state and propagate it to contained runners.
        /// </summary>
        /// <param name="state">The run state.</param>
        private void SetState(RunState state)
        {
            this.State = state;

            foreach (var runner in this.Next)
            {
                if (runner is NamespaceRunner)
                {
                    var ns = runner as NamespaceRunner;
                    ns.SetState(state);
                }
                else if (runner is ClassRunner)
                {
                    var classRunner = runner as ClassRunner;
                    classRunner.SetState(state);
                }
            }
        }
    }
}
