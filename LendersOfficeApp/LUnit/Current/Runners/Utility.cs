﻿namespace LUnitEngine.Runners
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Utility methods for test runners.
    /// </summary>
    internal static class Utility
    {
        /// <summary>
        /// Calculate the run state for a set of test runners.
        /// </summary>
        /// <param name="runners">A set of test runners.</param>
        /// <param name="report">Object to which the results will be reported.</param>
        /// <param name="containerState">The run state of a containing test runner.</param>
        /// <param name="include">Optional list of include category names.</param>
        /// <param name="exclude">Optional list of exclude category names.</param>
        /// <returns>The calculated run state.</returns>
        public static RunState CalculateRunStage(IEnumerable<Runner.ConcreteRunner> runners, IReportRunResults report, RunState containerState, IEnumerable<string> include, IEnumerable<string> exclude)
        {
            bool hasRun = false;
            bool hasIgnore = false;
            bool hasNotIncluded = false;
            bool hasExcluded = false;
            bool hasNotExplicit = false;
            foreach (var runner in runners)
            {
                var state = runner.RunBehavior(report, containerState, include, exclude);
                switch (state)
                {
                    case RunState.Run:
                        hasRun = true;
                        break;
                    case RunState.Ignored:
                        hasIgnore = true;
                        break;
                    case RunState.NotIncluded:
                        hasNotIncluded = true;
                        break;
                    case RunState.Excluded:
                        hasExcluded = true;
                        break;
                    case RunState.NotExplicit:
                        hasNotExplicit = true;
                        break;
                    default:
                        throw new System.Exception("Will never get here.");
                }
            }

            if (hasRun)
            {
                return RunState.Run;
            }

            if (hasIgnore)
            {
                return RunState.Ignored;
            }

            if (hasNotIncluded)
            {
                return RunState.NotIncluded;
            }

            if (hasExcluded)
            {
                return RunState.Excluded;
            }

            if (hasNotExplicit)
            {
                return RunState.NotExplicit;
            }

            return RunState.Undefined;
        }

        /// <summary>
        /// Execute a setup or tear down method.
        /// </summary>
        /// <param name="report">Object to which the results will be reported.</param>
        /// <param name="instance">Class instance against which the methods will be executed.</param>
        /// <param name="method">The method to be executed.</param>
        /// <param name="type">The type of the method to be executed, setup or tear down.</param>
        /// <param name="state">The pre-calculated run state, used to determine whether or not the setup/tear-down method should be executed.</param>
        /// <returns>True if there were no errors, false if an error occurred.</returns>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Unit test runners much catch and report on errors, rather than propagate them.")]
        public static bool RunSetUpOrTearDownMethod(IReportRunResults report, object instance, Method method, MethodType type, RunState state)
        {
            var result = ResultType.Inconclusive;
            try
            {
                if (state == RunState.Run)
                {
                    method.MethodInfo.Invoke(instance, null);
                }

                result = ResultType.Passed;
            }
            catch (System.Reflection.TargetInvocationException tex)
            {
                result = ResultType.Failed;
                if (report != null)
                {
                    report.ReportError(type, tex.InnerException.Message, tex.InnerException.StackTrace);
                }
                else
                {
                    throw;
                }
            }
            catch (Exception ex)
            {
                result = ResultType.Failed;
                if (report != null)
                {
                    report.ReportError(type, ex.Message, ex.StackTrace);
                }
                else
                {
                    throw;
                }
            }

            return result == ResultType.Passed;
        }

        /// <summary>
        /// Determine whether a namespace is the child of another namespace.
        /// </summary>
        /// <param name="thisNS">The namespace under examination.</param>
        /// <param name="potentialParentNS">The namespace that is being checked as the parent.</param>
        /// <returns>True if the namespace is a child of the potential parent namespace.</returns>
        public static bool IsSubnamespace(string thisNS, string potentialParentNS)
        {
            string prefix = (potentialParentNS.Length == 0) ? string.Empty : potentialParentNS + ".";
            return potentialParentNS.Length == 0 || thisNS.StartsWith(potentialParentNS);
        }
    }
}
