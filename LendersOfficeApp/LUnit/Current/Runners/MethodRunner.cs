﻿namespace LUnitEngine.Runners
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Threading.Tasks;
    using CommonProjectLib.Common;
    using DataAccess;
    using LendersOffice.Common;
    using LUnitEngine.Attributes;
    using LUnitEngine.Methods;

    /// <summary>
    /// Test runner for single unit test method.
    /// </summary>
    internal sealed class MethodRunner : Runner.ConcreteRunner
    {
        /// <summary>
        /// The setup method.
        /// </summary>
        private SetUpMethod setup;

        /// <summary>
        /// The unit test method.
        /// </summary>
        private TestMethod method;

        /// <summary>
        /// The tear down method.
        /// </summary>
        private TearDownMethod teardown;

        /// <summary>
        /// Instance of the containing class.
        /// </summary>
        private object instance;

        /// <summary>
        /// Flag set just prior to calling the test method.
        /// </summary>
        private bool setupDone;

        /// <summary>
        /// Flag set just after the test method completes.
        /// </summary>
        private bool methodDone;

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodRunner"/> class.
        /// </summary>
        /// <param name="setup">The setup method.</param>
        /// <param name="method">The unit test method.</param>
        /// <param name="teardown">The tear down method.</param>
        public MethodRunner(SetUpMethod setup, TestMethod method, TearDownMethod teardown)
            : base(method.MethodName, null)
        {
            this.setup = setup;
            this.method = method;
            this.teardown = teardown;
        }

        /// <summary>
        /// Determine the run state for this test.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        /// <param name="containerState">The run state of the containing runner.</param>
        /// <param name="include">Optional list of included category names.</param>
        /// <param name="exclude">Optional list of excluded category names.</param>
        /// <returns>The calculated run state.</returns>
        public override RunState RunBehavior(IReportRunResults report, RunState containerState, IEnumerable<string> include, IEnumerable<string> exclude)
        {
            report.BeginMethod();
            var state = this.method.RunBehavior(include, exclude);
            this.State = (state == RunState.Run) ? containerState : state;
            report.EndMethod(this.method.MethodInfo, this.method.Description, this.State, ResultType.Inconclusive);
            return this.State;
        }

        /// <summary>
        /// Execute the contained tests; RunBehavior must be called prior to this method, with a different report object.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        /// <param name="identifiers">Optional list of fixture/method identifiers to limit which tests are executed.</param>
        public override void Run(IReportRunResults report, HashSet<string> identifiers)
        {
            this.setupDone = this.methodDone = false;

            bool runnable = this.ContainsRunnable(identifiers);
            if (!runnable || this.State != RunState.Run)
            {
                this.HandleNotRunnable(report);
            }
            else if (this.method.TimeoutInSeconds > 0)
            {
                this.HandleTimeout(report, identifiers);
            }
            else
            {
                this.HandleRun(report, identifiers);
            }
        }

        /// <summary>
        /// Set the instance of the containing class.
        /// </summary>
        /// <param name="instance">The instance of the containing class.</param>
        internal override void PreRunSetInstance(object instance)
        {
            this.instance = instance;
        }

        /// <summary>
        /// Set the run state.
        /// </summary>
        /// <param name="state">The run state.</param>
        internal void SetState(RunState state)
        {
            this.State = state;
        }

        /// <summary>
        /// Retrieve the identifier for this method.
        /// </summary>
        /// <returns>The identifier for this method.</returns>
        internal string GetIdentifier()
        {
            return Method.GetIdentifier(this.method.MethodInfo);
        }

        /// <summary>
        /// Determine whether this method will be executed.
        /// </summary>
        /// <param name="identifiers">Optional list of identifiers for tests that should be executed.</param>
        /// <returns>True if the test method will be executed, false otherwise.</returns>
        internal override bool ContainsRunnable(HashSet<string> identifiers)
        {
            if (identifiers == null || identifiers.Count == 0)
            {
                return true;
            }

            string id = this.GetIdentifier();
            return identifiers.Contains(id);
        }

        /// <summary>
        /// Report the method has been skipped.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        private void HandleNotRunnable(IReportRunResults report)
        {
            report.BeginMethod();

            this.methodDone = this.setupDone = true;

            report.EndMethod(this.method.MethodInfo, this.method.Description, this.State, ResultType.Skipped);
        }

        /// <summary>
        /// Run the method on a worker thread so the timeout can be enforced.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        /// <param name="identifiers">Optional list of fixture/method identifiers to limit which tests are executed.</param>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Unit test runners much catch and report on errors, rather than propagate them.")]
        private void HandleTimeout(IReportRunResults report, HashSet<string> identifiers)
        {
            report.BeginMethod();

            var result = ResultType.Inconclusive;
            var localMethodType = MethodType.SetUp;
            var task = new Task(() => { this.HandleRunOnThread(identifiers); });
            try
            {
                task.Start();
                bool done = task.Wait(1000 * this.method.TimeoutInSeconds);
                localMethodType = this.GetMethodType();
                if (done)
                {
                    result = ResultType.Passed;
                }
                else
                {
                    report.ReportError(localMethodType, "Timeout before completion", Environment.StackTrace);
                }
            }
            catch (AggregateException ex)
            {
                localMethodType = this.GetMethodType();
                result = this.ProcessException(report, localMethodType, ex.InnerExceptions[0]);
            }
            catch (Exception ex)
            {
                localMethodType = this.GetMethodType();
                result = this.ProcessException(report, localMethodType, ex);
            }
            finally
            {
                report.EndMethod(this.method.MethodInfo, this.method.Description, this.State, result);
            }
        }

        /// <summary>
        /// Execute the (setup, test, tear down) methods on this thread.
        /// </summary>
        /// <param name="report">The results are reported to this object.</param>
        /// <param name="identifiers">Optional list of fixture/method identifiers to limit which tests are executed.</param>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Unit test runners much catch and report on errors, rather than propagate them.")]
        private void HandleRun(IReportRunResults report, HashSet<string> identifiers)
        {
            report.BeginMethod();

            var result = ResultType.Inconclusive;
            try
            {
                this.HandleRunOnThread(identifiers);
                result = ResultType.Passed;
            }
            catch (Exception ex)
            {
                result = this.ProcessException(report, this.GetMethodType(), ex);
            }
            finally
            {
                report.EndMethod(this.method.MethodInfo, this.method.Description, this.State, result);
            }
        }

        /// <summary>
        /// Execute the contained tests.
        /// </summary>
        /// <param name="identifiers">Optional list of fixture/method identifiers to limit which tests are executed.</param>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Unit test runners much catch and report on errors, rather than propagate them.")]
        private void HandleRunOnThread(HashSet<string> identifiers)
        {
            // NOTE: IF this method is executing on a separate thread in the context of a task, it may continue running after the
            //       original thread has reported a timeout.  In that case this method will run to completion and set some
            //       member state fields.  However, these fields are only used when reporting and the calling thread has
            //       already captured the current state and finished the reporting.
            this.RunSetUp();
            this.setupDone = true;

            try
            {
                this.method.MethodInfo.Invoke(this.instance, null);
                this.methodDone = true;
            }
            finally
            {
                var tuple = this.RunTearDown();
                if (this.methodDone && tuple != null)
                {
                    // this.methodDone == true means there isn't alread an exception in process.
                    // tuple != null  means an exception occurred in the tear down method.
                    throw new Exception(string.Concat(tuple.Item1, Environment.NewLine, tuple.Item2));
                }
            }
        }

        /// <summary>
        /// Execute the setup method.
        /// </summary>
        private void RunSetUp()
        {
            if (this.setup != null)
            {
                Utility.RunSetUpOrTearDownMethod(null, this.instance, this.setup, MethodType.SetUp, this.State);
            }
        }

        /// <summary>
        /// Execute the tear down method.
        /// </summary>
        /// <returns>Exception information, or null.</returns>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Unit test runners much catch and report on errors, rather than propagate them.")]
        private Tuple<string, string> RunTearDown()
        {
            // Since this method is called from a finally clause, it is possible that an exception is already
            // winding up the call stack.  For this reason, we don't want to interfere with that by allowing
            // an exception to leave this method.  Instead we'll capture the details of the exception and 
            // allow the calling code to use it as appropriate.
            Tuple<string, string> tuple = null;
            if (this.teardown != null)
            {
                try
                {
                    Utility.RunSetUpOrTearDownMethod(null, this.instance, this.teardown, MethodType.TearDown, this.State);
                }
                catch (System.Reflection.TargetInvocationException ex)
                {
                    tuple = new Tuple<string, string>(ex.InnerException.Message, ex.InnerException.StackTrace);
                }
                catch (Exception ex)
                {
                    tuple = new Tuple<string, string>(ex.Message, ex.StackTrace);
                }
            }

            return tuple;
        }

        /// <summary>
        /// Retrieve the method type for reporting, based on method progress flags.
        /// </summary>
        /// <returns>The method type for reporting.</returns>
        private MethodType GetMethodType()
        {
            var type = MethodType.SetUp;
            if (this.setupDone)
            {
                type = MethodType.Test;
            }

            if (this.methodDone)
            {
                type = MethodType.TearDown;
            }

            return type;
        }

        /// <summary>
        /// Process an exception based on the NUnit expected exception attribute.
        /// </summary>
        /// <param name="report">The report where the exception may get written, if unexpected.</param>
        /// <param name="method">The method where the exception occurred.</param>
        /// <param name="ex">The exception.</param>
        /// <returns>Calculate the result of the method execution depending on the expected exception attribute.</returns>
        private ResultType ProcessException(IReportRunResults report, MethodType method, Exception ex)
        {
            if (ex is System.Reflection.TargetInvocationException)
            {
                return this.ProcessException(report, method, ex.InnerException);
            }

            ResultType result = ResultType.Inconclusive;
            bool hasExpected = this.method.ExpectedExceptionType.Length > 0;
            bool hasExpectedType = hasExpected && this.method.ExpectedExceptionType != ExpectedExceptionAttribute.NOTSPECIFIED;
            bool hasExpectedMessage = hasExpected && !string.IsNullOrEmpty(this.method.ExpectedExceptionMessage);
            if (hasExpectedType && ex.GetType().FullName != this.method.ExpectedExceptionType)
            {
                result = ResultType.Failed;
            }
            else if (hasExpectedMessage && ex.Message != this.method.ExpectedExceptionMessage)
            {
                result = ResultType.Failed;
            }
            else if (hasExpected)
            {
                result = ResultType.Passed;
            }
            else
            {
                result = ResultType.Failed;
            }

            if (result == ResultType.Failed)
            {
                CPmlFIdGenerator generator = new CPmlFIdGenerator();
                string code = generator.GenerateNewFriendlyId();

                string userMessage = ErrorMessages.Generic;
                string developerMessage = ex.Message;
                string stackTrace = ex.StackTrace;

                string message = string.Format("Reference #: {1}{0}{0}UserMessage: {2}{0}{0}DeveloperMessage: {3}", Environment.NewLine, code, userMessage, developerMessage);
                report.ReportError(MethodType.Test, message, stackTrace);
            }

            return result;
        }
    }
}
