﻿namespace LUnitEngine
{
    /// <summary>
    /// The NUnit framework consists of a set of custom attributes that are attached to
    /// classes and methods.  The names of these attributes are defined here.  The
    /// class structure mimics the fullname of the attribute classes.
    /// </summary>
    public static class NUnit
    {
        /// <summary>
        /// Inner class represents the Framework part of the NUnit.Framework namespace.
        /// </summary>
        public static class Framework
        {
            /// <summary>
            /// Fullname of the NUnit Category attribute class.
            /// </summary>
            public const string Category = "NUnit.Framework.CategoryAttribute";

            /// <summary>
            /// Fullname of the NUnit Description attribute class.
            /// </summary>
            public const string Description = "NUnit.Framework.DescriptionAttribute";

            /// <summary>
            /// Fullname of the NUnit ExpectedException attribute class.
            /// </summary>
            public const string ExpectedException = "NUnit.Framework.ExpectedExceptionAttribute";

            /// <summary>
            /// Fullname of the NUnit Explicit attribute class.
            /// </summary>
            public const string Explicit = "NUnit.Framework.ExplicitAttribute";

            /// <summary>
            /// Fullname of the NUnit Ignore attribute class.
            /// </summary>
            public const string Ignore = "NUnit.Framework.IgnoreAttribute";

            /// <summary>
            /// Fullname of the NUnit SetUp attribute class.
            /// </summary>
            public const string SetUp = "NUnit.Framework.SetUpAttribute";

            /// <summary>
            /// Fullname of the NUnit SetUpFixture attribute class.
            /// </summary>
            public const string SetUpFixture = "NUnit.Framework.SetUpFixtureAttribute";

            /// <summary>
            /// Fullname of the NUnit TearDown attribute class.
            /// </summary>
            public const string TearDown = "NUnit.Framework.TearDownAttribute";

            /// <summary>
            /// Fullname of the NUnit Test attribute class.
            /// </summary>
            public const string Test = "NUnit.Framework.TestAttribute";

            /// <summary>
            /// Fullname of the NUnit TestFixture attribute class.
            /// </summary>
            public const string TestFixture = "NUnit.Framework.TestFixtureAttribute";

            /// <summary>
            /// Fullname of the NUnit TestFixtureSetUp attribute class.
            /// </summary>
            public const string TestFixtureSetUp = "NUnit.Framework.TestFixtureSetUpAttribute";

            /// <summary>
            /// Fullname of the NUnit TestFixtureTearDown attribute class.
            /// </summary>
            public const string TestFixtureTearDown = "NUnit.Framework.TestFixtureTearDownAttribute";
        }
    }
}
