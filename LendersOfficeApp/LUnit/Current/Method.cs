﻿namespace LUnitEngine
{
    using System.Reflection;

    /// <summary>
    /// This is the base class for distinct method types within LUnit.
    /// </summary>
    internal abstract class Method
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Method"/> class.
        /// </summary>
        /// <param name="info">The method information.</param>
        protected Method(MethodInfo info)
        {
            this.MethodInfo = info;
        }

        /// <summary>
        /// Gets the method information.
        /// </summary>
        /// <value>The method information.</value>
        public MethodInfo MethodInfo { get; private set; }

        /// <summary>
        /// Calculate an identifier for a test method.
        /// </summary>
        /// <param name="info">The method information.</param>
        /// <returns>The identifier for the test method.</returns>
        public static string GetIdentifier(MethodInfo info)
        {
            return (info.DeclaringType.FullName + "." + info.Name).GetHashCode().ToString();
        }
    }
}
