﻿namespace LUnit.Current
{
    using System.Collections.Generic;
    using System.Reflection;
    using System.Text;
    using System.Xml;
    using Interface;
    using LUnitEngine;

    /// <summary>
    /// Entry point into the LUnitEngine feature.
    /// </summary>
    public sealed class Shim : IRunner
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Shim"/> class.
        /// </summary>
        /// <param name="assembly">The assembly containing unit tests.</param>
        public Shim(Assembly assembly)
        {
            var parser = Parser.GetParser(Version.Supported.NUnitx2x6x3);
            this.Runner = parser.ParseAssembly(assembly);
            this.TestAssembly = assembly;
        }

        /// <summary>
        /// Gets or sets the test runner.
        /// </summary>
        /// <value>The test runner.</value>
        private Runner Runner { get; set; }

        /// <summary>
        /// Gets or sets the assembly containing unit tests.
        /// </summary>
        /// <value>The assembly containing unit tests.</value>
        private Assembly TestAssembly { get; set; }

        /// <summary>
        /// Retrieve all the test fixtures that will be run.
        /// </summary>
        /// <returns>All the test fixtures that will be run.</returns>
        public TestClassData[] GetTestFixtures()
        {
            var report = ReportRunResults.GetNUnitXML(this.TestAssembly, Version.Supported.NUnitx2x6x3);
            this.Runner.RunBehavior(report, null, this.GetExcluded());

            var nunitXML = (NUnitReport)report;
            return this.ReadTestData(nunitXML.Report);
        }

        /// <summary>
        /// Run the test methods contained within the specified test fixture,
        /// or all test methods if fixtureId is null.
        /// </summary>
        /// <param name="fixtureId">The identifier for a test fixture, or null.</param>
        /// <returns>The results of running the test methods.</returns>
        public TestClassResult RunFixture(string fixtureId)
        {
            this.Runner.RunBehavior(null, null, this.GetExcluded());

            var report = ReportRunResults.GetNUnitXML(this.TestAssembly, Version.Supported.NUnitx2x6x3);
            string[] identifiers = string.IsNullOrEmpty(fixtureId) ? null : new string[] { fixtureId };
            this.Runner.Run(report, identifiers);

            var nunitXML = (NUnitReport)report;
            var elem = (XmlElement)nunitXML.Report.SelectSingleNode(string.Format("//test-suite[@type='TestFixture'][@id='{0}']", fixtureId));
            return this.ReadTestFixture(elem);
        }

        /// <summary>
        /// Run a single test method, as indicated with the specified fixture and test identifiers.
        /// </summary>
        /// <param name="fixtureId">The identifier for the test fixture containine the test method.</param>
        /// <param name="testId">The identifier for the test method.</param>
        /// <returns>The result of running the test method.</returns>
        public TestClassResult RunSingleTest(string fixtureId, string testId)
        {
            string[] identifiers = new string[] { testId };

            this.Runner.RunBehavior(null, null, this.GetExcluded());

            var report = ReportRunResults.GetNUnitXML(this.TestAssembly, Version.Supported.NUnitx2x6x3);
            this.Runner.Run(report, identifiers);

            var nunitXML = (NUnitReport)report;
            var elem = (XmlElement)nunitXML.Report.SelectSingleNode(string.Format("//test-suite[@type='TestFixture'][@id='{0}']", fixtureId));
            return this.ReadTestFixture(elem);
        }

        /// <summary>
        /// Retrieve a list of category names that should be skipped.
        /// </summary>
        /// <returns>List of category names that should be excluded.</returns>
        private string[] GetExcluded()
        {
            return new string[] { "Connection Dependent", "LQB.Mocks" };
        }

        /// <summary>
        /// Parse the XML into a set of data class instances.
        /// </summary>
        /// <param name="doc">The XML document.</param>
        /// <returns>A set of data class instances.</returns>
        private TestClassData[] ReadTestData(XmlDocument doc)
        {
            var results = this.ReadTestResults(doc);

            var datas = new TestClassData[results.Length];
            for (int i = 0; i < results.Length; ++i)
            {
                datas[i] = results[i].Data;
            }

            return datas;
        }

        /// <summary>
        /// Parse the XML into a set of result class instances.
        /// </summary>
        /// <param name="doc">The XML document.</param>
        /// <returns>A set of result class instances.</returns>
        private TestClassResult[] ReadTestResults(XmlDocument doc)
        {
            var list = new List<TestClassResult>();
            foreach (XmlElement elem in doc.DocumentElement.SelectNodes("//test-suite[@type='TestFixture']"))
            {
                var result = this.ReadTestFixture(elem);
                list.Add(result);
            }

            return list.ToArray();
        }

        /// <summary>
        /// Parse a test fixture's result XML into a class result instance.
        /// </summary>
        /// <param name="elem">A test fixture's result XML.</param>
        /// <returns>A class result instance.</returns>
        private TestClassResult ReadTestFixture(XmlElement elem)
        {
            var methodDatas = new List<TestMethodData>();
            var methodResults = new List<TestMethodResult>();

            foreach (XmlElement methElem in elem.SelectNodes("test-case"))
            {
                var result = this.ReadTestMethod(methElem);
                if (result.Data.IsRunnable)
                {
                    methodDatas.Add(result.Data);
                    methodResults.Add(result);
                }
            }

            string id = elem.GetAttribute("id");
            string name = elem.GetAttribute("classname");
            string runnable = elem.GetAttribute("runstate");
            string ns = this.GetNamespace(elem);
            string fullname = string.Concat(ns, ".", name);

            var classData = new TestClassData(id, name, ns, fullname, runnable == "Runnable", methodDatas.ToArray());
            return new TestClassResult(classData, this.GetStatus(elem), this.GetMessage(elem), methodResults.ToArray());
        }

        /// <summary>
        /// Parse a test-case element into a method result.
        /// </summary>
        /// <param name="elem">The test-case element.</param>
        /// <returns>A method result instance.</returns>
        private TestMethodResult ReadTestMethod(XmlElement elem)
        {
            string id = elem.GetAttribute("id");
            string name = elem.GetAttribute("methodname");
            string runnable = elem.GetAttribute("runstate");
            string description = this.ReadDescription(elem);
            string friendlyName = string.IsNullOrEmpty(description) ? name : description;
            var data = new TestMethodData(id, name, friendlyName, runnable == "Runnable");
            return new TestMethodResult(data, this.GetStatus(elem), this.GetMessage(elem));
        }

        /// <summary>
        /// Read the description for a containing element.
        /// </summary>
        /// <param name="container">The containing element.</param>
        /// <returns>The description.</returns>
        private string ReadDescription(XmlElement container)
        {
            return this.ReadProperty(container, "description");
        }

        /// <summary>
        /// Read a property value for a containing element.
        /// </summary>
        /// <param name="container">The containing element.</param>
        /// <param name="propertyName">The name of the property.</param>
        /// <returns>The value of the property.</returns>
        private string ReadProperty(XmlElement container, string propertyName)
        {
            var props = (XmlElement)container.SelectSingleNode("properties");
            if (props == null)
            {
                return null;
            }

            foreach (XmlElement prop in props.SelectNodes("property"))
            {
                string name = prop.GetAttribute("name");
                if (name == propertyName)
                {
                    return prop.GetAttribute("value");
                }
            }

            return null;
        }

        /// <summary>
        /// Parse an XML element that has an @result into a ResultStatus value.
        /// </summary>
        /// <param name="elem">An XML element that has an @result.</param>
        /// <returns>A ResultStatus value.</returns>
        private ResultStatus GetStatus(XmlElement elem)
        {
            switch (elem.GetAttribute("result"))
            {
                case "Passed":
                    return ResultStatus.Success;
                case "Failed":
                    return ResultStatus.Fail;
                case "Skipped":
                case "Inconclusive":
                    return ResultStatus.Ignore;
                case "":
                    return this.GetStatusFromChildren(elem);
                default:
                    throw new System.Exception("Will never get here.");
            }
        }

        /// <summary>
        /// Parse the child elements for @result and present pass or fail.
        /// </summary>
        /// <param name="elem">An XML element that has an @result.</param>
        /// <returns>A ResultStatus value.</returns>
        private ResultStatus GetStatusFromChildren(XmlElement elem)
        {
            bool hasFailure = false;
            bool hasSuccess = false;
            foreach (var node in elem.ChildNodes)
            {
                if (node is XmlElement)
                {
                    var child = (XmlElement)node;
                    string resvalue = child.GetAttribute("result");
                    if (resvalue == "Failed")
                    {
                        hasFailure = true;
                    }
                    else if (resvalue == "Passed")
                    {
                        hasSuccess = true;
                    }
                }
            }

            if (hasFailure)
            {
                return ResultStatus.Fail;
            }
            else if (hasSuccess)
            {
                return ResultStatus.Success;
            }
            else
            {
                return ResultStatus.Ignore;
            }
        }

        /// <summary>
        /// Extract a failure message from a containing XML element.
        /// </summary>
        /// <param name="elem">The containing XML element.</param>
        /// <returns>The failure message, or null.</returns>
        private string GetMessage(XmlElement elem)
        {
            var failure = (XmlElement)elem.SelectSingleNode("failure");
            if (failure == null)
            {
                return null;
            }

            var message = (XmlElement)failure.SelectSingleNode("message");
            if (message == null)
            {
                return "NO FAILURE MESSAGE RETURNED";
            }

            string text = message.InnerText;
            if (string.IsNullOrEmpty(text))
            {
                return "NO FAILURE MESSAGE RETURNED";
            }

            var stack = (XmlElement)failure.SelectSingleNode("stack-trace");
            if (stack != null)
            {
                text = string.Concat(text, System.Environment.NewLine, stack.InnerText);
            }

            return text;
        }

        /// <summary>
        /// Navigate the XML structure to extract the full namespace for a test fixture.
        /// </summary>
        /// <param name="fixtureElem">An XML element that represents the test fixture.</param>
        /// <returns>The full namespace for the test fixture.</returns>
        private string GetNamespace(XmlElement fixtureElem)
        {
            StringBuilder sb = new StringBuilder();
            XmlElement elem = (XmlElement)fixtureElem.ParentNode;
            while (elem.GetAttribute("type") == "TestSuite")
            {
                string name = elem.GetAttribute("name");
                if (!string.IsNullOrEmpty(name))
                {
                    if (sb.Length > 0)
                    {
                        name += ".";
                    }

                    sb.Insert(0, name);
                }

                elem = (XmlElement)elem.ParentNode;
            }

            return sb.ToString();
        }
    }
}