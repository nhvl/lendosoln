using System;
using System.Reflection;
using DataAccess;
using LendersOffice.Common;

namespace LUnit
{
    public class TestMethod
    {
        string     m_fullName = "";
        string     m_name;
        MethodInfo m_method;

        string m_expectedExceptionName;
        bool m_expectingException; 
        string m_expectedMessage;
        string m_description = null;
        int m_timeout = 0;

 
        public string FriendlyName
        {
            get 
            {   if( m_description == null || m_description == "" )
                    return m_name;

                return m_description; 
            }
        }


        public string FullName
        {
            get { return m_fullName; }
        }

        public string Name
        {
            get { return m_name; }
        }

        public MethodInfo Method
        {
            get { return m_method; }
        }

        private bool ExpectingException
        {
            get
            {
                return m_expectingException;
            }
        }


        public int Timeout
        {
            get { return m_timeout; }
        }


        public TestMethod( MethodInfo method )
        {
            m_name     = method.DeclaringType == method.ReflectedType 
                ? method.Name : method.DeclaringType.Name + "." + method.Name;
            m_fullName = method.ReflectedType.FullName + "." + method.Name;
            m_method   = method; 
            m_description = ReflectionReader.GetDescription( method, out m_timeout );
            

        }

        public TestMethod( MethodInfo method, string expectedExceptionName, string expectedMessage, bool expectingException )
            : this( method)
        {
            m_expectedExceptionName = expectedExceptionName;
            m_expectedMessage       = expectedMessage;
            m_expectingException = expectingException;

        }

        private bool IsExpectedExceptionType(Exception exception)
        {
            if (ExpectingException)

                if (false == string.IsNullOrEmpty(m_expectedExceptionName))
                {
                    return m_expectedExceptionName.Equals(exception.GetType().FullName);
                }
                else
                {
                    return true;
                }
            return false;
        }

        private string GetStackTrace(Exception exception)
        {
            try
            {
                return exception.StackTrace;
            }
            catch( Exception )
            {
                return "No stack trace available";
            }
        }

		
        private void ProcessException(Exception exception)
        {
            // no exception happened
            if( exception == null )
            {
                if ( ExpectingException == false)
                    return;
                string expectedType = m_expectedExceptionName == null ? "An Exception" : m_expectedExceptionName;
                throw new CBaseException(ErrorMessages.Generic, expectedType + " was expected");
            }


            // exception happened

            if( ExpectingException == false)
                throw new LUnitException( "Don't expect any exception", exception );

            if( IsExpectedExceptionType( exception ) )
            {
                if (m_expectedMessage != null && !m_expectedMessage.Equals(exception.Message))
                {
                    string message = string.Format("Expected exception to have message: \"{0}\" but received message \"{1}\"", 
                        m_expectedMessage, exception.Message);
                    throw new CBaseException(ErrorMessages.Generic, message + " " + exception.ToString());
                } 
                else 
                    return;
            }
            else
            {
                string message = "Expected: " + m_expectedExceptionName + " but was " + exception.GetType().FullName;
                throw new CBaseException(ErrorMessages.Generic, message + " " + exception.ToString());
            }
        }

        public void Run( object fixtureObj )
        {
            Exception retException = null;
            try
            {
                Method.Invoke( fixtureObj, null );
            }
            catch( Exception ex )
            {
                retException = RemoveSomeException( ex );
            }
            ProcessException( retException );

        }

        public static Exception RemoveSomeException(Exception ex)
        {
            if( ex == null )
                return null;
            if( ex is  TargetInvocationException )
                return ex.InnerException;

            while( ex.GetType().FullName == "NUnit.Core.NunitException" )
            {

                if( ex.InnerException != null )
                    ex = ex.InnerException;
                else
                    break;
            }

            while( ex is  LUnitException )
            {

                if( ex.InnerException != null )
                    ex = ex.InnerException;
                else
                    break;
            }

            return  ex;
        }

        class LUnitException : CBaseException 
        {
            public LUnitException(string message, Exception inner) :
                base(ErrorMessages.Generic, message + ".\r\n" + inner) 
            {}

        }





    }

}
