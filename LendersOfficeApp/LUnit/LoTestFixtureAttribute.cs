using System;

namespace LUnit
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
    public class LoTestFixtureAttribute  : Attribute
    {
        private string m_description;

        /// <summary>
        /// Descriptive text for this test
        /// </summary>
        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }

        public LoTestFixtureAttribute( string description )
        {
            m_description = description;
        }

        public LoTestFixtureAttribute()
        {
        }
    }

}
