﻿LUnit is an NUnit runner that can be executed in-process.  LendingQB uses
NUnit version 2.6.3, for which there is no pre-existing in-process runner.
Ideally we would move to the latest version of NUnit and use the 
NUnit.Engine.api library.  However, there are apparently some security
errors with some of the unit tests that prevent us from moving to a more
recent version.

There are currently two problems with LUnit.  The first is that it only works
with a now very outdated version of the NUnit framework, per the discussion
above.  The second is that the code itself is difficult to maintain and very few
of the SDE team members have worked on the code.

Ultimately the goal is to move to NUnit.Engine.api, but that will need to be
done in a series of steps.  The first step is to make the eventual adoption
of NUnit.Engine.api as painless as possible.  Accordingly, we would like
our current code to be based upon the output of NUnit.Engine.api.  That
output takes the form of an XML document.  A skeleton schema for the XML
grammar is in the file LUnit/Current/NUnitTestResult.ss.xml.

There are two main hurdles to achieving the goal.  The first is that
the current LUnit is difficult to maintain.  In order to bypass this
problem, the code in the 'Current' folder is a complete re-write of
LUnit that should be easier to understand.  This new code is implemented
to an interface defined in the 'Interface' folder.  The main entry
point is the IRunner interface, which is designed to conform to the
current LendingQB GUI's usage of LUnit.  The new code also generates
NUnit XML and then parses that to present the output of the IRunner
methods.  This implementation is designed to get over the second
hurdle.  When we move to NUnit.Engine.api we should be able to
keep the parsing code with only minor modifications.  Since this
is all kept behind the IRunner interface, the application code will
be left unchanged.  The class LUnit.Current.Shim has the implementation
of the IRunner interface.  It is given the name 'Shim' because the
IRunner interface is designed around the current LendingQB GUI and not part
of the native design of the code.  The native interface design is given in
the class LUnit.Current.Runner.

In addition to the LUnit re-write, the existing code has been
copied to the 'Legacy' folder and made to conform with the StyleCop rules.
It has also been modified slightly to conform to the IRunner interface, the
implementation given by the LUnit.Legacy.Shim class.  There is a factory
class, LUnit.Interface.LUnitFactory which creates either the Legacy or
Current implementation of IRunner based upon the input flag useNewLUnit.
The idea is that we can easily switch back to the Legacy implementation
while we fix problems discovered with the new code.

While testing the new code, it was discovered that the GUI code is reliant
on the precise details of the FixtureId and MethodId parameters.  They must
be integers and they must be interpretable as zero-based indices into an
array.  This is a tight coupling between the GUI and the details of the LUnit
implementation.  This forced some undesirable changes on the LUnit code.  The
folder 'WebShim' was created and defines the IWebRunner interface, which
precisely matches the data structures and method signatures that the GUI
requires.  The class LUnit.WebRunner implements this interface and calls the
original code.  The web pages were modified to work with the IWebRunner
interface.  The class LUnit.WebRunnerFactory was also added to permit the
creation of one of three implementations of the IWebRunner interface, bases on
the LUnitType enumeration.  The value LUnitType.Original gives back the
original LUnit implementation, only behind the IWebRunner interface.  For the
other two implementations that are accessed via the IRunner interface, the
class LUnit.Interface.WebRunner was created.  This class translates between
the IWebRunner and IRunner interfaces.  The values LUnitType.Legacy and
LUnitType.Current are mapped to the useNewLUnit argument taken by the
LUnit.Interface.LUnitFactory.

In summary, here is the architecture:
- Application code uses the LUnit.WebShim.IWebRunner interface.
- LUnit.WebRunnerFactory uses LUnit.LUnitType to create an implementation of
  the LUnit.WebShim.IWebRunner interface.
  - LUnit.LUnitType.Original gives the original code.
  - LUnit.LUnitType.Legacy gives the code in the 'Legacy' folder.
  - LUnit.LUnitType.Current gives the code in the 'Current' folder.
- Both Legacy and Current code is accessed using via LUnit.Interface.IRunner
  - The code in the 'Legacy' folder is the same as the original code, only
    conforming to StyleCop (e.g., well documented) so it should be somewhat
	easier to maintain.
  - The code in the 'Current' folder is a complete re-write of LUnit that 
    works with the LUnit.Engine.api XML grammar, and should be much easier
	to maintain than the other implementations.

Here are the anticipated migration steps:
- Run with the original code for awhile to ensure the IWebRunner mechanism
  is reliable.
- Run with the 'Legacy' code for awhile, with the ability to switch back to the
  original code if necessary.  This will allow us to ensure that the
  translation between the IWebRunner and IRunner interfaces is working
  correctly.
- Drop the original code and switch to the 'Current' code for awhile, with the
  ability to switch back to the Legacy code if necessary.  This will allow
  us to ensure that the LUnit re-write is working.
- Drop the Legacy code, rewrite the GUI to work with arbitrary identifiers and
  drop the WebShim code.  The architecture will then be based on IRunner, with
  IWebRunner eliminated.
- Switch to the most recent version of NUnit and resolve all the security
  issues.  This is done outside the context of LUnit because LUnit doesn't
  actually call into NUnit.
- Write a new version of LUnit that uses NUnit.Engine.api, still safely behind
  the IRunner interface.  Run this for awhile, with the ability to switch back
  to the Current code.
- Drop the Current code.  From now on we can freely adopt new versions of NUnit
  and have zero maintenance to do on LUnit.
