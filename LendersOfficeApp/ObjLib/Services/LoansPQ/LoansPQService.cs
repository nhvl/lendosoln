﻿namespace LendersOfficeApp.ObjLib.Services.LoansPQ
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.ObjLib.Conversions.LoansPQ.Exporter;
    using LendersOffice.ObjLib.Conversions.LoansPQ.Importer;
    using LendersOffice.Security;
    using LendersOfficeApp.WebService;

    public class LoansPQService : ILoansPQService 
    {
        private const string DEFAULT_LOAN_TEMPLATE_NAME = "Default Template";
        private ILoanFileCreator _creator;
        private ILoanFileExporter _exporter;
        private CPmlFIdGenerator fid = new CPmlFIdGenerator();

        private ILoanFileCreator Creator
        {
            get
            {
                if (_creator == null)
                {
                    _creator = new BasicLoanFileCreator();
                }
                return _creator;
            }
        }

        private ILoanFileExporter Exporter
        {
            get
            {
                if (_exporter == null)
                {
                    _exporter = new CLFExporter();
                }
                return _exporter;
            }
        }
        
        public LoansPQService()
        {
        }

        #region ILoansPQService Members
        /// <summary>
        /// Updates the loan file with the give loanspqfile id.  If the file does not exist it creates it.
        /// </summary>
        /// <param name="loanPQLoanNumber"></param>
        /// <param name="data"></param>
        /// <returns>The lendingqb loanid  </returns>
        public Guid UpdateLoanFile( long loanPQLoanNumber, string data)
        {
            string loanName; 
            Guid loanID = GetLoanIDFor(BrokerUserPrincipal.CurrentPrincipal.BrokerId, loanPQLoanNumber, out loanName);
            Log("LPQ Update Call Received. LoanID = " + loanID + " Loan Name= " + loanName);
            Log("CLF Data " + data);
            {
                try
                {
                    if (loanID == Guid.Empty)
                    {
                        loanID = Creator.CreateLoan(BrokerUserPrincipal.CurrentPrincipal,
                            LendersOffice.Audit.E_LoanCreationSource.ImportFromLoansPQ,
                            new Dictionary<string, object>() { { "sLpqLoanNumber", loanPQLoanNumber } }, DEFAULT_LOAN_TEMPLATE_NAME);
                    }

                    CLFImporter importer = new CLFImporter(loanID, data);
                    importer.Import();
                }
                catch (CBaseException e)
                {
                    if (e.UserMessage.Contains("Duplicate contact"))
                    {
                        Tools.LogWarning(e);
                    }
                    else
                    {
                        Tools.LogError(e);
                    }
                    throw new InvalidOperationException("Error ID = " + e.ErrorReferenceNumber + " " + e.UserMessage);

                }                                        
                catch (NullReferenceException e)
                {
                    string id = fid.GenerateNewFriendlyId();
                    Tools.LogError("Error ID = " + id + " LPQ Programming Error", e);
                    throw new InvalidOperationException("There was a error with your request. Id " + id);
                }
                catch (InvalidOperationException e)
                {
                    string id = fid.GenerateNewFriendlyId();
                    Tools.LogError("Error ID = " + id + " LPQ There was an error updating loan file with Id " + loanID + " " + e.Message);
                    Tools.LogError(e);

                    if (e.Message != null && e.Message.Contains("There is an error in XML document") && e.InnerException != null)
                    {
                        throw new InvalidOperationException(e.InnerException.Message + " id " + id);
                    }
                    else
                    {
                        throw new InvalidOperationException(e.Message + " id " + id);
                    }
                }
                catch (Exception e)
                {
                    string id = fid.GenerateNewFriendlyId();
                    Tools.LogError("Error ID = " + id + " LPQ There was an error updating loan file with Id " + loanID + " " + e.Message);
                    Tools.LogError(e);
                    throw new InvalidOperationException(e.Message + " id " + id);
                }
            }

            return loanID;
        }

        /// <summary>
        /// Exports the loan file to the given CLF document described by the string. 
        /// </summary>
        /// <param name="loansPQFileID"></param>
        /// <param name="clfData"></param>
        /// <returns>A new clf document with the lendingqb loan file merged in. If the file doest exist it will throw an exception. </returns>
        public string ExportLoanFile(long loansPQFileID, string clfData)
        {
            string loanName; 
            Guid loanID = GetLoanIDFor(BrokerUserPrincipal.CurrentPrincipal.BrokerId, loansPQFileID, out loanName);
            if (loanID == Guid.Empty)
            {
                throw new ArgumentException("The loan file with the given loan number does not exist. " + loansPQFileID);
            }
            string data;
            Log("LPQ Export Call Received. LoanID = " + loanID + " Loan Nm  = " + loanName);
            Log("CLF Data " + clfData);
            try
            {
                data = Exporter.ExportTo(loanID, clfData);
                Log("LPQ ID " + loansPQFileID + data);
            }
            catch (CBaseException e)
            {
                if (e.UserMessage.Contains("Duplicate contact"))
                {
                    Tools.LogWarning(e);
                }
                else
                {
                    Tools.LogError(e);
                }
                throw new InvalidOperationException("Error ID = " + e.ErrorReferenceNumber + " " + e.UserMessage);
            }
            catch (NullReferenceException e)
            {
                string id = fid.GenerateNewFriendlyId();
                Tools.LogError("Error ID = " + id + " LPQ Programming Error ", e);
                throw new InvalidOperationException("There was a error with your request. id " + id);
            }
            catch (Exception e)
            {
                string id = fid.GenerateNewFriendlyId();
                Tools.LogError("Error ID = " + id + " Errors encountered while exporting loan file to CLF format.", e);
                throw new InvalidOperationException(e.Message + " Id " + id);
            }
            return data;
        }
        /// <summary>
        /// Returns a url that can be used to log the user into  loan editor automatically and open the loan with the given loan id.
        /// </summary>
        /// <param name="lpqFileID"></param>
        /// <returns></returns>
        public string GenerateUrlForEditingLoan(long lpqFileID)
        {
            string loanName; 
            Guid loanID = GetLoanIDFor(BrokerUserPrincipal.CurrentPrincipal.BrokerId, lpqFileID, out loanName);
            if (loanID == Guid.Empty)
            {
                throw new ArgumentException("The loan file with the given loan number does not exist.");
            }
            return GenerateUrlForEditingLoan(loanID, loanName);
        }

        private string GenerateUrlForEditingLoan(Guid loanID, string loanName)
        {
            if (loanID == Guid.Empty)
            {
                throw new ArgumentException("The loan file with the given loan id does not exist.");
            }
            return GenerateURL(AppView.GetBypassTicket(), loanID, loanName);
        }

        #endregion
        /// <summary>
        /// Returns a safe url with the given auth id loan id and loan name. In order to use it the first part of the url has to be appended ( https://secure.lendersoffice.com OR https://secure.lendingqb.com )
        /// </summary>
        /// <param name="authID"></param>
        /// <param name="loanID"></param>
        /// <param name="loanNm"></param>
        /// <returns></returns>
        private static string GenerateURL(Guid authID, Guid loanID, string loanNm)
        {
            return LendersOffice.AntiXss.AspxTools.SafeUrl("/newlos/loanapp.aspx?auth_id=" + authID.ToString() + "&loanid=" + loanID.ToString()+"&loanname="+ loanNm ).Replace("\"", "");
        }
        /// <summary>
        /// Looks up a loan by loans pq file id. If no such loan exist the empty guid is returned. IT also sets the loan name. 
        /// </summary>
        /// <param name="brokerID"></param>
        /// <param name="lpqFileID"></param>
        /// <param name="loanName"></param>
        /// <returns></returns>
        private static Guid GetLoanIDFor(Guid brokerID, long lpqFileID, out string loanName)
        {
            Guid loanId = Guid.Empty;
            loanName = string.Empty;
            //perform loan lookup (loan as to belong to current broker and have the given loans pq field.) 
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerUserPrincipal.CurrentPrincipal.BrokerId), 
                                            new SqlParameter("@LoansPQFileID", lpqFileID) 
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "GetLoansPQLinkedLoanID", parameters))
            {
                if (reader.Read())
                {
                    loanName = reader["LoanName"].ToString();
                    loanId = new Guid(reader["LoanId"].ToString());
                }
            }

            return loanId;
        }

        private static void Log(string msg)
        {
            Tools.LogInfo(msg);
        }


    }
}
