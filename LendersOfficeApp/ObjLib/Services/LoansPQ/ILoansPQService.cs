﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LendersOfficeApp.ObjLib.Services.LoansPQ
{
    public interface  ILoansPQService
    {
        Guid UpdateLoanFile(long lpqFileID,string data );
        string ExportLoanFile(long lpqFileID, string clfData );
        string GenerateUrlForEditingLoan(long lpqFileID);
    }
}
