namespace LendersOfficeApp 
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using Adapter.MethodInvoke;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.Queries.MethodInvoke;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.Adapters;
    using LqbGrammar.Exceptions;
    using LendersOffice.ObjLib.Resource;

    public class Global : System.Web.HttpApplication
    {

        class FirstRequestInitialize
        {
            private static bool s_isInitialized = false;
            private static object s_lock = new object();

            public static void Initialize(HttpContext context)
            {
                if (s_isInitialized)
                {
                    return;
                }

                lock (s_lock)
                {
                    if (s_isInitialized)
                    {
                        // 4/30/2015 dd - Check again.
                        return;
                    }

                    if (WebsiteUtilities.IsDatabaseOffline)
                    {
                        // 2/29/2012 dd - Do not initialize if database is offline. Need to perform iisreset when database
                        // is back online.
                        return;
                    }
                    Stopwatch sw = Stopwatch.StartNew();
                    StringBuilder sb = new StringBuilder();
                    int step = 0;

                    try
                    {
                        AppendLog(sb, sw, "Initialize started", step++);

                        Utilities.DetectTimeDifferentWithDB();

                        AppendLog(sb, sw, "Utilities.DetectTimeDifferentWithDB()", step++);

                        BrokerDB.InitializeBrokerIdLock();

                        // Ensure that the temporary folder exists.
                        string sPath = ConstApp.TempFolder;
                        if (!Directory.Exists(sPath))
                        {
                            Directory.CreateDirectory(sPath);
                        }


                        if (!ResourceManager.Instance.AreGenericResourcesAvailable())
                        {
                            EmailUtilities.SendToCritical($"{ConstAppDavid.ServerName} LQB Is Not Configured Correctly", "PmlShared or PmlCommon cannot be resolved.");
                        }

                        #region LpeTasksReceiver
                        AppendLog(sb, sw, "Start LpeTaskReceiver thread is necessary.", step++);

                        LpeTaskReceiver.Start();

                        #endregion

                        Tools.SetupServicePointCallback();

                        AppendLog(sb, sw, "Initialize end", step);
                    }
                    catch (Exception exc)
                    {
                        Tools.LogErrorWithCriticalTracking(new SystemStartupException(exc));
                        throw;
                    }
                    finally
                    {
                        sw.Stop();

                        Tools.LogInfo("LendersOfficeApp_Global", "LendersOfficeApp_Global::Initialize executed in " + sw.ElapsedMilliseconds + "ms." + Environment.NewLine + sb.ToString());
                    }


                    s_isInitialized = true;
                }
            }

            private static void AppendLog(StringBuilder sb, Stopwatch sw, string description, int step)
            {
                string displayFormat = "    [{0}] Step {1} - {2}{3}";

                sb.AppendFormat(displayFormat, sw.ElapsedMilliseconds, step, description, Environment.NewLine);
            }
        }

        public Global()
        {
            InitializeComponent();
        }

        private void AppendLog(StringBuilder sb, Stopwatch sw, string description, int step)
        {
            string displayFormat = "    [{0}] Step {1} - {2}{3}";

            sb.AppendFormat(displayFormat, sw.ElapsedMilliseconds, step, description, Environment.NewLine);
        }
        protected void Application_Start( Object sender , EventArgs a )
        {
            // 4/30/2015 dd - For IIS 7.0 Integrated mode, it is better to perform initialize in BeginRequest. The reason is
            // only in BeginRequest that HttpRequest will be available.

            // The FOOL architecture must be initialized immediately at startup and cannot be attempted a second time
            // or a null reference exception will get thrown.  Accordingly, this must be done here and not along side 
            // the other initialization code that is run in the BeginRequest event.
            InitializeApplicationFramework();
        }

        private static void InitializeApplicationFramework()
        {
            IExceptionHandlerFactory[] arrHandlers = ExceptionHandlerUtil.GetCoreExceptionHandlerFactories();
            using (IApplicationInitialize iAppInit = LqbApplication.CreateInitializer(arrHandlers))
            {
                const string AppName = "LendersOfficeApp";
                iAppInit.SetName(AppName);

                Adapter.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
                LendersOffice.ApplicationInitializer.RegisterFactories(AppName, iAppInit);
                // Register method invocation calls.
                iAppInit.Register<ICustomReportRunner>(new CustomReportRunner());
            }
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
        }

        protected void Application_PostMapRequestHandler(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (context.Handler is System.Web.UI.Page && !string.IsNullOrEmpty(context.Request.PathInfo))
            {
                string contentType = context.Request.ContentType.Split(';')[0];
                if (contentType.Equals("application/json", StringComparison.OrdinalIgnoreCase))
                {
                    context.Response.Filter = new LendersOffice.HttpModule.WebMethodExceptionLogger(context.Response);
                }
            }
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            FirstRequestInitialize.Initialize(((HttpApplication)sender).Context);

            if (WebsiteUtilities.IsDatabaseOffline)
            {
                HttpContext context = HttpContext.Current;
                if (context != null)
                {
                    if (Request.Url.AbsolutePath.EndsWith("Website/index.aspx", StringComparison.OrdinalIgnoreCase) == false)
                    {
                        Response.Redirect(ConstSite.LoginUrl);
                    }
                }
            }

            Tools.SetupServicePointManager();

            if (ConstStage.EnableAntiXssOnUIControls)
            {
                HttpContext.Current.Items["SanitizeOutput"] = "Yes";
            }

            //OPM 
            if (Response.ContentType != "application/pdf")
            {
                HttpContext.Current.Response.AppendHeader("P3P", "CP=\"CAO PSA OUR\"");
            }
        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

			if (null != HttpContext.Current) 
			{
				if (Response.ContentType == "text/html")
				{
					// 01/30/08 mf. OPM 19673 - We do not want these headers when response
					// is pdf or fannie file.  This code sets the headers:
					// Cache-control: No-cache, no-store
					// Pragma: No-cache

					Response.Cache.SetCacheability(HttpCacheability.NoCache);
					Response.Cache.SetNoStore();
				}

			}
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs a)
        {
            // Options support is added for cors between edocs and secure. 
            // Options request dont do anything but request headers.
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.SkipAuthorization = true;
            }

            if (HttpContext.Current.Request.Url.AbsolutePath.ToLower().EndsWith("loadmin/test/dumprequest.aspx"))
            {
                HttpContext.Current.SkipAuthorization = true;
            }

            if (WebsiteUtilities.IsDatabaseOffline)
            {
                return;
            }

            RequestHelper.AuthenticateRequest();
		}

        protected void Application_Error(Object sender, EventArgs e)
        {
            if (WebsiteUtilities.IsDatabaseOffline)
            {   
                Server.ClearError();
                return;
            }
            Exception ex = Server.GetLastError() ;

			if( ex is System.OutOfMemoryException )
			{
				 // A desparate call, assuming that we can get out-of-memory exception before garbage collection is invoked to do a full (time-consuming) job to collect all collectable memory.
				GC.GetTotalMemory( true );
			}

            if (ex.ToString().IndexOf("Hosting.IIS7WorkerRequest.ExplicitFlush()") >= 0)
            {
                Server.ClearError();
                return;
            }

            if (ex is HttpException)
            {
                if (((HttpException)ex).GetHttpCode() == 404)
                {
                    // 6/18/2013 dd - Ignore 404 page not found.
                    return;
                }

                //If you are adding more error codes to the ignore list - do unchecked((int)0x800703E3);
                int[] ignoreErrorCodeList = { -2147023901, -2147014842, -2147023901, -2147024832};

                if (ignoreErrorCodeList.Contains(((HttpException)ex).ErrorCode))
                {
                    Server.ClearError();
                    return;
                }


                HttpException innerException = ex.InnerException as HttpException;

                if (innerException != null)
                {

                    // Ignore "The remote host closed the connection" exception.
                    if (ignoreErrorCodeList.Contains(innerException.ErrorCode))
                    {
                        Server.ClearError();
                        return;
                    }

                }
            }

            // If the user entered a filename that failed the HTTPRequestValidation
            // because &# was in it, display a custom error message
            bool isInvalidFilename = false;
            if (ex is System.Reflection.TargetInvocationException)
            {
                HttpRequestValidationException innerException = ex.InnerException as HttpRequestValidationException;
                string invalidPattern = ".*filename=\\\".*&#.*\\\"";              
                if (innerException != null && Regex.IsMatch(ex.InnerException.Message, invalidPattern, RegexOptions.Singleline))
                {
                    isInvalidFilename = true;                    
                }
            }
            
            // Transfer to custom error page for everyone except developer
            // testing on localhost.
            
            if (!Request.Url.IsLoopback && !ConstSite.IsDevMachine) 
            {
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
                bool autofillErrorForm = (principal != null);
                if (autofillErrorForm && !isInvalidFilename)
                {
                    ErrorUtilities.DisplayErrorPage(ex, true, principal.BrokerId, principal.EmployeeId);
                }
                else if (autofillErrorForm && isInvalidFilename)
                {                    
                    ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.EDocs.InvalidUploadFilename, ex.ToString()),
                        false, principal.BrokerId, principal.EmployeeId);
                }
                else if (!autofillErrorForm && !isInvalidFilename)
                {
                    ErrorUtilities.DisplayErrorPage(ex, true, Guid.Empty, Guid.Empty);
                }
                else // !autofillErrorForm && isInvalidFilename
                {
                    ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.EDocs.InvalidUploadFilename, ex.ToString()),
                        false, Guid.Empty, Guid.Empty);
                }
            } 
            else 
            {
                string sErrorMessage ;
                if (null != ex.InnerException)
                    sErrorMessage = ex.InnerException.ToString() ;
                else
                    sErrorMessage = ex.ToString() ;

                Tools.LogError(sErrorMessage);
            }
        }

        protected void Session_End(Object sender, EventArgs e)
        {
        }

        protected void Application_End( Object sender , EventArgs a )
        {
            if (WebsiteUtilities.IsDatabaseOffline)
            {
                return;
            }

            if (LpeTaskReceiver.IsRunning)
            {
                LpeTaskReceiver.Stop();
            }

            Tools.LogInfo("LendersOfficeApp_Global", "LendersOfficeApp_Global::Application_End");
        }

		#region Web Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    

        }
		#endregion

    }

}

