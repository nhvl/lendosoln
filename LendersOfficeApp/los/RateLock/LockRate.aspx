﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LockRate.aspx.cs" Inherits="LendersOfficeApp.los.RateLock.LockRate" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>RateLock</title>
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet" />
</head>
<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto; BACKGROUND-COLOR: #003366" >
    <form id="LockRate" runat="server">
    
    			<table cellspacing="0" cellpadding="8" border="0" width="100%" height="100%">
			<tr>
			<td height="100%">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
				<tr>
				<td height="100%" style="BORDER: 2px outset; BACKGROUND-COLOR: gainsboro;">
    

            <table height="100%" cellspacing="2" cellpadding="3" width="100%" border="0">
			<tr>
				<td class="FormTableHeader" noWrap height="0%"><ml:EncodedLiteral id="m_Title" runat="server" /></td>
			</tr>
			<tbody id="MainBody">
			<tr>
			    <td>
			        <table id="LoanInfoTable" height="100%" cellspacing="2" cellpadding="3" width="100%" border="0">
			        <tr>
			            <td>
			                <span class="FieldLabel" >Loan Number:</span>
			                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_LoanNumber" runat="server" /></span>
			            </td>
			            <td>
			                <span class="FieldLabel" >Primary Applicant:</span>
			                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_Applicant" runat="server" /></span>
			            </td>
			        </tr>
			        <tr id="m_relockRow" runat="server">
			            <td>
			                <span class="FieldLabel" >Lock Expiration Date:</span>
			                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_lockExpirationDate" runat="server" /></span>
			            </td>
			            <td>
			                <span class="FieldLabel" >Re-Lock Period:</span>
			                &nbsp;<select id="m_lockDays" runat="server" onchange="onChangeLockPeriod()" />&nbsp;days
			                <input type="button" id="priceBtn" value= " Price " onclick="priceLoan()" />
			            </td>			        
			        </tr>
			        <tr id="m_lockDatesRow" runat="server">
			            <td>
			                <span class="FieldLabel" >Original Lock Expiration Date:</span>
			                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_OriginalLockExpirationDate" runat="server" /></span>
			            </td>
			            <td>
			                <span class="FieldLabel" >Current Lock Expiration Date:</span>
			                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_CurrentLockExpirationDate" runat="server" /></span>
			            </td>			        
			        </tr>
			        <tr id="m_lockedRatesRow" runat="server">
			            <td>
			                <span class="FieldLabel" >Currently Locked Rate:</span>
			                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_rowCurrentlyLockedRate" runat="server" /></span>
			            </td>
			            <td>
			                <span class="FieldLabel" >Currently Locked <%= AspxTools.HtmlString( m_pointsPrice ) %>:</span>
			                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_rowCurrentlyLockedPoints" runat="server" /></span>
			            </td>			        
			        </tr>			        
			        <tr>
			            <td colspan="2">
			                <span class="FieldLabel" >Number of times this file has&nbsp;<ml:EncodedLiteral id="m_timesEventDesc" runat="server" />:</span>
			                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_timesEventOccured" runat="server" /></span>
			            </td>
			        </tr>
			        <tr id="m_extentionDaysRow" runat="server">
			            <td colspan="2">
			                <span class="FieldLabel" >Total extension previously applied to this loan file: &nbsp;<ml:EncodedLiteral id="Literal1" runat="server" /></span>
			                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_extensionDays" runat="server" /></span>
			            </td>
			        </tr>
			        
			        </table>
			    </td>
			</tr>
			
	        <tr id="m_reasonRow" runat="server">
	            <td>
	                <span class="FieldLabel" >Reason for&nbsp;<ml:EncodedLiteral id="m_reasonDesc" runat="server" />:</span>
	                <span style="padding-left: 3px"><input id="reason" style="width:370px; padding-left:2px" type="text" onchange="requiredTextChange(this)"/></span>
                    <img alt="Required" src="../../images/require_icon.gif" id="requiredGif" />
	            </td>
	        </tr>
	        
	        <tr>
	        <td>
	            <table cellspacing="0" cellpadding="0" border="0" width="100%" height="178px">
	            <tr valign="top">
	            <td>
	                <div id="m_PricingTableDiv" runat="server">
	                <span class="FieldLabel">Currently Locked Rate:</span>
	                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_currentlyLockedRate" runat="server" /></span><br />
	                <span class="FieldLabel">Currently Locked <%= AspxTools.HtmlString( m_pointsPrice ) %>:</span>
	                <span style="padding-left: 3px"><ml:EncodedLiteral id="m_currentlyLockedPoints" runat="server" /></span>
	                </div>
	            </td>
	            <td>
	                <span id='PricingTableMsg'></span>
	                <img id='WaitImg' src="../../images/status.gif" style="display:none" />
	                <span style="color:Red" id='PricingTableError'></span>
	                <div id="PricingTable"></div>
	            </td>
	            </tr>
	            </table>
	            
	        </td>
	        </tr>
			<tr id="m_floatDownMsg" runat="server">
			    <td>
			        <span id="FloatDownMsgSpan" style="color:Red; display:none" >Note: A rate lock float down will not change the current lock expiration date for this loan.</span>
			    </td>
		    </tr>
		    
		    
		    <tr>
		    <td>
		    
		    <table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
		    <tr>
		    <td>
		        <span id="m_expirationDateSpan" runat="server" ><span class="FieldLabel">Expiration Date:</span>&nbsp;<ml:EncodedLiteral id="m_expirationDate" runat="server"></ml:EncodedLiteral></span>
		        <span class="FieldLabel" id="ExpirationDateRelockLabel"></span>&nbsp;<span id="ExpirationDateRelock"  ></span>
		    </td>
		    <td align="right"><input type="button" id="btn" value=" Cancel " onclick="onClosePopup()" /> </td>
		    </tr>
		    </table>
		    
		    </td>
		    </tr>
		    </tbody>
		    
		    
		    
		    <tbody id="ConfirmationBody" style="display:none">
		    <tr><td><div id="ConfirmationTableDiv"></div></td></tr>
		    </tbody>
		    
			
			</table>




				</td>
				</tr>
				</table>
			</td>
			</tr>
			</table>

			
			
			
    </form>
        <!-- very similar code exists in PML/main/LockRate.aspx -->
        <asp:PlaceHolder runat="server" id="m_extendScript">
            <script type="text/javascript">
                var gRunPricingOnInit = true;
                var gWaitMsg = "Please wait while your lock extension options are generated.";
                
                function getActionNode(tableRow, showCalendarDays)
                {
                    var actionStr = showCalendarDays ? tableRow[5] : tableRow[4];

                    var node = document.createElement('a');
                    if (showCalendarDays && tableRow[4] === "0 days") {
                        node.title = "This option does not extend the Lock Expiration Date";
                        node.className += ' disabled';
                    }
                    else if (showCalendarDays && actionStr == "disabled") {
                        node.title = "This option is unavailable because there is a better option which results in the same Lock Expiration Date.";
                        node.className += ' disabled';
                    }
                    else {
                        node.href = "javascript:extend('" + actionStr + "')";
                    }                    
                    node.appendChild(document.createTextNode('extend'));

                    return node;
                }

                function extend(extention) {
                
                    if ( document.getElementById('reason').value == '' )
                    {
                        alert('Please enter an explanation for why a lock extension is needed.' );
                        return;
                    }
                    
                    var arguments = extention.split(":");
                    
                    var args = new Object();
                    args['lockAction'] = arguments[0];
                    args['lockArguments'] = extention.replace(arguments[0] + ":", "") + ":" + document.getElementById('reason').value;
                    
                    priceLoan( args );
                    
                }
                function buildConfirmationTable( result )
                {
                    var confirmDiv = document.getElementById('ConfirmationTableDiv');
                    var confirmBody = document.getElementById('ConfirmationBody');
                    var confirmTable = document.createElement('table');
                    confirmTable.setAttribute('width','100%');
                    var body = document.createElement('tbody');
                    confirmDiv.appendChild(confirmTable);
                    confirmTable.appendChild(body);
                    
                    var tr = document.createElement('tr');
                    var td = document.createElement('td');
                    td.style.color='green';
                    td.colSpan = 2;
                    td.appendChild(document.createTextNode('Your rate lock extension has been approved.'));
                    tr.appendChild(td);
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('Loan Number:',<%= AspxTools.JsString( m_LoanNumber.Text ) %>));
                    tr.appendChild(buildCell('Primary Applicant:',<%= AspxTools.JsString( m_Applicant.Text ) %>));
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('Prior Lock Expiration Date:',<%= AspxTools.JsString( m_CurrentLockExpirationDate.Text ) %>));
                    tr.appendChild(buildCell('New Lock Expiration Date:', result['newexpiration']));
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('Locked Rate:',<%= AspxTools.JsString(  m_currentlyLockedRate.Text ) %>));
                    tr.appendChild(buildCell('New Locked <%= AspxTools.HtmlString( m_pointsPrice ) %>:',result['newprice']));
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr');
                    tr.setAttribute('height', '250px');
                    tr.setAttribute('valign', 'top');
                    tr.vAlign = 'top';
                    td = buildCell('Number of times this file has had its rate lock expiration date extended:', result['totalevents']);
                    td.colSpan = 2;
                    tr.appendChild(td);
                    var button = document.createElement('input');
                    button.style.marginLeft ='40px';
                    button.type ='button';
                    button.value = ' Close ';
                    button.onclick = function() { onClosePopup();};
                    td.appendChild(button);
                    tr.appendChild(td);
                    body.appendChild(tr);
                    
                    confirmBody.style.display = 'block';
                }
                function tableBuiltCallback( bIsVisible )
                {
                }
            </script>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" id="m_floatDownScript">
            <script type="text/javascript">
                var gRunPricingOnInit = true;
                var gWaitMsg = "Please wait while your Float Down options are generated.";

                function getActionNode(tableRow, showCalendarDays)
                {
                    var actionStr = tableRow[6];
                    var node = document.createElement('a');
                    node.href = "javascript:floatDown('" + actionStr + "')";                    
                    node.appendChild(document.createTextNode(actionStr.split(":")[0]));

                    return node;
                }

                function floatDown(args) {
                    var arguments = args.split(":");
                    if (arguments[0] == 'unavailable') {
                        var reasons = '';
                        for (var i = 1; i < arguments.length; i++) {
                            reasons += " " + arguments[i];
                        }
                        alert(reasons);
                    }
                    else {
                        floatDownImpl( args );
                    }
                }

                function floatDownImpl( arguments ) {
                
                    var floatDownArgs = arguments.split(":");

                    if (floatDownArgs.length == 0) {
                        return;
                    }
                    
                    var args = new Object();
                    args['lockAction'] = floatDownArgs[0];
                    args['lockArguments'] = arguments.replace(floatDownArgs[0] + ":", "");

                    priceLoan(args);
                }
                
                function buildConfirmationTable( result )
                {
                    var confirmDiv = document.getElementById('ConfirmationTableDiv');
                    var confirmBody = document.getElementById('ConfirmationBody');
                    var confirmTable = document.createElement('table');
                    confirmTable.setAttribute('width','100%');
                    var body = document.createElement('tbody');
                    confirmDiv.appendChild(confirmTable);
                    confirmTable.appendChild(body);
                    
                    var tr = document.createElement('tr');
                    var td = document.createElement('td');
                    td.style.color='green';
                    td.colSpan = 2;
                    td.appendChild(document.createTextNode('Your rate lock float down for this loan has been approved.'));
                    tr.appendChild(td);
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('Loan Number:',<%= AspxTools.JsString( m_LoanNumber.Text ) %>));
                    tr.appendChild(buildCell('Primary Applicant',<%= AspxTools.JsString( m_Applicant.Text ) %>));
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('Prior Locked Rate:',<%= AspxTools.JsString(  m_rowCurrentlyLockedRate.Text ) %>));
                    tr.appendChild(buildCell('Prior Locked <%= AspxTools.HtmlString( m_pointsPrice ) %>:', <%= AspxTools.JsString(  m_rowCurrentlyLockedPoints.Text ) %>));
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('New Locked Rate:', result['newrate']));
                    tr.appendChild(buildCell('New Locked <%= AspxTools.HtmlString( m_pointsPrice ) %>:',result['newprice']));
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr');
                    td = buildCell('Number of times this file has had a rate lock float down:', result['totalevents']);
                    td.colSpan = 2;
                    tr.appendChild(td);
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr'); 
                    tr.setAttribute('height', '220px');
                    tr.setAttribute('valign', 'top');
                    tr.vAlign = 'top';
                    td = buildCell('Lock Expiration Date:', result['newexpiration']);
                    td.colSpan = 2;
                    var button = document.createElement('input');
                    button.style.marginLeft ='210px';
                    button.type ='button';
                    button.value = ' Close ';
                    button.onclick = function() { onClosePopup();};
                    td.appendChild(button);
                    tr.appendChild(td);
                    body.appendChild(tr);
                    
                
                    confirmBody.style.display = 'block';
                }

                function tableBuiltCallback( bIsVisible )
                {
                    var tableMsg = document.getElementById('FloatDownMsgSpan');
                    if (tableMsg != null)
                        tableMsg.style.display = bIsVisible ? 'inline': 'none';
                }
            </script>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" id="m_relockScript">
            <script type="text/javascript">
                var gRunPricingOnInit = false;
                var gWaitMsg = "Please wait while your re-lock options are generated.";

                function getActionNode(tableRow, showCalendarDays)
                {
                    var actionStr = tableRow[5];
                    var node = document.createElement('a');
                    node.href = "javascript:relock('" + actionStr + "')";
                    node.appendChild(document.createTextNode('re-lock'));

                    return node;
                }

                function relock(argStr) {
                
                    if (hasDisabledAttr(document.getElementById('PricingTable'))) return;

                    if (document.getElementById('reason').value == '') {
                        alert('Please enter an explanation for why a relock is needed.');
                        return;
                    }

                    var arguments = argStr.split(":");


                    var args = new Object();
                    args['lockAction'] = arguments[0];
                    args['lockArguments'] = argStr.replace(arguments[0] + ":", "") + ":" + document.getElementById('reason').value;

                    priceLoan(args);
                }
                
                
                function buildConfirmationTable( result )
                {
                    var confirmDiv = document.getElementById('ConfirmationTableDiv');
                    var confirmBody = document.getElementById('ConfirmationBody');
                    var confirmTable = document.createElement('table');
                    confirmTable.setAttribute('width','100%');
                    var body = document.createElement('tbody');
                    confirmDiv.appendChild(confirmTable);
                    confirmTable.appendChild(body);
                    
                    var tr = document.createElement('tr');
                    var td = document.createElement('td');
                    td.style.color='green';
                    td.colSpan = 2;
                    td.appendChild(document.createTextNode('Your relock for this loan has been approved.'));
                    tr.appendChild(td);
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('Loan Number:',<%= AspxTools.JsString( m_LoanNumber.Text ) %>));
                    body.appendChild(tr);
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('Primary Applicant',<%= AspxTools.JsString( m_Applicant.Text ) %>));
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('Re-Locked Rate:',result['newrate']));
                    body.appendChild(tr);
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('Re-Locked <%= AspxTools.HtmlString( m_pointsPrice ) %>:', result['newprice']));
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr'); 
                    tr.appendChild(buildCell('Re-Locked Expiration Date:', result['newexpiration'] ));
                    tr.appendChild(buildCell('',''));
                    body.appendChild(tr);
                    
                    tr = document.createElement('tr');
                    tr.setAttribute('height', '250px');
                    tr.setAttribute('valign', 'top');
                    tr.vAlign = 'top';
                    td = buildCell('Number of times this file has been Re-Locked:', result['totalevents']);
                    td.colSpan = 2;
                    tr.appendChild(td);
                    var button = document.createElement('input');
                    button.style.marginLeft ='40px';
                    button.type ='button';
                    button.value = ' Close ';
                    button.onclick = function() { onClosePopup();};
                    td.appendChild(button);
                    tr.appendChild(td);
                    body.appendChild(tr);
                    
                
                    confirmBody.style.display = 'block';
                }
                
                function tableBuiltCallback( bIsVisible )
                {
                    var lockDates = <%=AspxTools.JsArray(m_newExpirationDates) %>;
                    if ( document.getElementById('m_lockDays') != null )
                    {
                        document.getElementById('ExpirationDateRelockLabel').innerText = bIsVisible ?  'New Lock Expiration Date:' : '';
                        document.getElementById('ExpirationDateRelock').innerText = bIsVisible ? lockDates[document.getElementById('m_lockDays').selectedIndex] : '';
                    }
                    
                    disableTable( bIsVisible );
                    
                    var priceBtn = document.getElementById('priceBtn');
                    if ( priceBtn != null )
                    {
                        priceBtn.disabled =  bIsVisible ? '' : 'disabled';
                    }
                    
                }
                
                function onChangeLockPeriod()
                {
                    disableTable( false );
                }

            </script>
        </asp:PlaceHolder>
    
    
        <script type="text/javascript">
        function _init() {
            if( gRunPricingOnInit )
            {
                setWaitMsg(gWaitMsg);
                
                setTimeout(function() { priceLoan(null); }, 800);
            }
        }
        
        var pollInterval;
        var pollCount = 0;

        function priceLoan( actionArgs ) {
            var isForAction = actionArgs != null;
            var days = null;
            var args = new Object();
            if ( document.getElementById('m_lockDays') != null )
            {
                days = document.getElementById('m_lockDays').value;
                if ( days == '')
                {
                    alert('Please choose a lock days value');
                    return;
                }
                
                args['lockdays'] = days;
            }
            
            setWaitMsg(isForAction ? 'Please Wait.' : gWaitMsg);

            document.getElementById('PricingTable').innerHTML = '';
            tableBuiltCallback( false );
            
            if ( isForAction )
                document.getElementById('PricingTable').style.display = 'none';
         
            
            var result = executeService("CreatePriceRequest", args);
            if (isError) return;
            
            pollCount = 0; 
            if ( result && result.value && result.value['requestid'])
            {
                var requestId = result.value['requestid'];
                
                args = (actionArgs == null ) ? new Object(): actionArgs;
                
                args['requestid'] = requestId;
                if ( days != null)
                    args['lockdays'] = days;
                
                pollInterval = setInterval(function() { getResult( args, isForAction ); }, 3000);
                
            }
        }
        
        
        function getResult( args, bIsForAction )
        {
            pollCount++;
            
            var result = executeService( 'GetResultIfReady', args );
            if (isError) return;
            
            if ( result && result.value && result.value['isdone'] === 'True')
            {
                clearInterval(pollInterval);
                setWaitMsg('');
                
                if ( bIsForAction )
                    {
                        buildConfirmationTable( result.value );
                        document.getElementById('MainBody').style.display = 'none';
                        document.getElementById('ConfirmationTableDiv').style.display = 'block';
                        refreshPipeline();
                    } 
                else
                {
                    buildTable(result.value['TableJSON']);
                    document.getElementById('PricingTable').style.display = 'block';
                    document.getElementById('ConfirmationTableDiv').style.display = 'none';
                    
                    tableBuiltCallback( true );
                }
            }
            
         
            if ( pollCount >=  20)
            {
                clearInterval(pollInterval);
                alert ('Unable to get results');
            }
        }
        
        
        function executeService( service, args )
        {
            args['loanid'] = <%= AspxTools.JsString(m_loanId.ToString()) %>;
            args['mode'] = <%= AspxTools.JsString( m_mode.ToString("D")) %>;
            
            var result = gService.lockrate.call(service, args);
            if ( !result.error )
            {
                if (result.value['displayerror'])
                {
                    displayError( result.value['displayerror']);
                }
                
                return result;
            }
            else
            {
                if ( result.UserMessage != null )
                    displayError(result.UserMessage);
                else
                    if ( result.value['UserMessage'] )
                        displayError(result.value['UserMessage']);
                return null;
            }
            
        }
        
        var isError = false;
        function displayError( error )
        {
            isError = true;
            setWaitMsg('');
            document.getElementById('PricingTableError').innerText = error;
            document.getElementById('PricingTable').style.display = 'none';
            tableBuiltCallback( false );
            
            if (pollInterval)
                clearInterval(pollInterval);
        }        
        
        function buildTable(jsonContent) {
            var tableContent = $.parseJSON(jsonContent);
            var parentNode = document.getElementById('PricingTable');
            var table = document.createElement('table');
            var body = document.createElement('tbody');
            var distinctCalendarDays = {};

            var tr, td;
            for (var i = 0; i < tableContent.length; i++) {
                tr = document.createElement('tr');
                if (i == 0) tr.className = 'GridHeader';
                else tr.className = (i % 2 == 0) ? 'GridItem' : 'GridAlternatingItem';
                for (var j = 0; j < tableContent[i].length; j++) {
                    td = document.createElement('td');                    
                    if ( j == tableContent[i].length -1 && i != 0 ) {
                        td.appendChild(getActionNode(tableContent[i], ML.showCalendarDays));
                    }
                    else {
                        var spanElement = document.createElement("span");                        
                        var textContent = tableContent[i][j];
                        var brIndex = textContent.indexOf("<br/>");
                        if (brIndex != -1) {
                            td.appendChild(document.createTextNode(textContent.slice(0, brIndex)));
                            td.appendChild(document.createElement("br"));
                            td.appendChild(document.createTextNode(textContent.slice(brIndex + 5, textContent.length)));
                        }
                        else {
                            td.appendChild(document.createTextNode(tableContent[i][j]));
                        }
                    }

                    tr.appendChild(td);
                }
                body.appendChild(tr);
            }         

            table.appendChild(body);
            parentNode.appendChild(table);
        }
        
        function buildDisabledNode(toolTip){
            var tdNode = document.createElement('td');
            var node = document.createElement('a');
            node.title = toolTip;
            node.className += ' disabled';
            node.appendChild(document.createTextNode('extend'));
            tdNode.appendChild(node);

            return tdNode;
        }

        function buildCell(label, content)
        {
            var td = document.createElement('td');
            var labelSpan = document.createElement('span');
            labelSpan.className = 'FieldLabel';
            labelSpan.style.paddingRight = '3px';
            labelSpan.appendChild(document.createTextNode(label));
            td.appendChild(labelSpan);
            td.appendChild(document.createTextNode(content));
            return td;

        }
        function disableTable( bToggle )
        {
            setDisabledAttr(document.getElementById('PricingTable'), !bToggle);
        }
        
        function setWaitMsg ( msg )
        {
            bShowWait = msg != '';
            document.getElementById('PricingTableMsg').innerText = msg;
            
            document.getElementById('WaitImg').style.display = bShowWait ? 'block' : 'none';
        }
        
        function refreshPipeline()
        {
            if ( window && window.opener )
            {
                if ( typeof(window.opener.refreshPipeline) != 'undefined' )
                {
                    window.opener.refreshPipeline();
                }
            }
        }
        
        function requiredTextChange(textBox) {
            if (textBox.value != "") {
                document.getElementById("requiredGif").style.display = "none";
            }
            else {
                document.getElementById("requiredGif").style.display = "";
            }
        }

    </script>
</body>
</html>
