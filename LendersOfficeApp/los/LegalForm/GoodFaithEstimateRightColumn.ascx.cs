using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOfficeApp.los.LegalForm
{

	public partial  class GoodFaithEstimateRightColumn : System.Web.UI.UserControl
	{

		/*
        public string PaidBy 
        {
            get 
            {
                return PdByT_dd.SelectedItem.Value;
            }
            set 
            {
                ListItem item = PdByT_dd.Items.FindByValue(value);
                if (item != null) 
                {
                    item.Selected = true;
                }
            }
        }
		*/
		public int PdByT
		{
			get { return PdByT_dd.SelectedIndex; }
			set { PdByT_dd.SelectedIndex = value; }
		}

        public bool ToBrok 
        {
            get { return ToBrok_chk.Checked; }
            set { ToBrok_chk.Checked = value; }
        }

        public bool Apr 
        {
            get { return Apr_chk.Checked; }
            set { Apr_chk.Checked = value; }
        }
        public bool Poc 
        {
            get { return Poc_chk.Checked; }
            set { Poc_chk.Checked = value; }
        }

		public bool Fha
		{
			get { return Fha_chk.Checked; }
			set { Fha_chk.Checked = value; }
		}
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
