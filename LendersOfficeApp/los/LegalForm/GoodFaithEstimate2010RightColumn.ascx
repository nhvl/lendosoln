<%@ Control Language="c#" AutoEventWireup="false" Codebehind="GoodFaithEstimate2010RightColumn.ascx.cs" Inherits="LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<td noWrap class="GFE2010RightColumn">
    <input type="radio" name="Page2" ID="Page2A_rb" runat="server" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" data-fsid="Page2" data-index="1" />
    <ml:EncodedLabel ID="Page2A_lbl" runat="server" />
    
    <input type="radio" name="Page2" ID="Page2B_rb" runat="server" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" visible="false" data-fsid="Page2" data-index="2" />
    <ml:EncodedLabel ID="Page2B_lbl" runat="server" />
    
    <input type="radio" name="Page2" ID="Page2C_rb" runat="server" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" visible="false" data-fsid="Page2" data-index="3" />
    <ml:EncodedLabel ID="Page2C_lbl" runat="server" />
    &nbsp;
    
    <span id="Page2Placeholder" visible="false" runat="server">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </span>
    
    <input type="checkbox" name="BF" id="BF_chk" runat="server" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" visible="false" data-fsid="Bf"/>
    <label for="BF_chk" ID="BF_lbl" runat="server" visible="false">BF</label>
    
    <input type="checkbox" name="GBF" id="GBF_chk" value="GBF" runat="server" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" visible="false"/>
    <label for="GBF_chk" ID="GBF_lbl" runat="server" visible="false">GBF</label>
</td>
<td class="GFE2010RightColumn">
    <asp:dropdownlist id="PdByT_dd" runat="server" onchange="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" TabIndex=200 data-fsid="PdByT">
        <asp:ListItem Value="0">borr pd</asp:ListItem>
        <asp:ListItem Value="1">borr fin</asp:ListItem>
        <asp:ListItem Value="2">seller</asp:ListItem>
        <asp:ListItem Value="3">lender</asp:ListItem>
        <asp:ListItem Value="4">broker</asp:ListItem>
    </asp:dropdownlist>
</td>
<% if (!m_settlementMode) { %>
<td class="GFE2010RightColumn"><INPUT id="Apr_chk" runat="server" type="checkbox" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" title="APR" TabIndex=200 data-fsid="Apr">A</td>
<td class="GFE2010RightColumn"><INPUT id="Fha_chk" onblur="unhighlightRow(this);" title="FHA Allowable" onfocus="highlightRow(this);" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" type="checkbox" name="Checkbox1" runat="server" TabIndex=200 data-fsid="Fha">F</td>
<% } %>
<td class="GFE2010RightColumn"><INPUT id="Poc_chk" onblur="unhighlightRow(this);" title="Paid Outside of Closing" onfocus="highlightRow(this);" type="checkbox" name="Checkbox1" runat="server" tabindex=200 data-fsid="Poc"><ml:EncodedLiteral id="Poc_chkText" Text="POC" runat="server"></ml:EncodedLiteral></td>
<%if (!m_ccTemplateMode) { %>
<td class="GFE2010RightColumn"><INPUT id="Borr_chk" onblur="unhighlightRow(this);" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" visible="false" title="Borrower" onfocus="highlightRow(this);" type="checkbox"  runat="server" tabindex=200 data-fsid="Borr"><ml:EncodedLiteral id="Borr_chkText" Text="BORR" visible="false" runat="server"></ml:EncodedLiteral></td>
<%} %>
<td class="GFE2010RightColumn"><INPUT id="Paid_chk" runat="server" type="checkbox" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" title="Paid" visible="false" TabIndex=200 data-fsid="Paid"><ml:EncodedLiteral id="Paid_chkText" Text="PD" visible="false" runat="server"></ml:EncodedLiteral></td>
<% if (m_settlementMode || m_ccTemplateMode) { %>
<td class="GFE2010RightColumn"><input id="Dflp_chk" runat="server" type="checkbox" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" title="Deducted From Loan Proceeds" TabIndex=200 data-fsid="Dflp"><ml:EncodedLiteral id="Dflp_chkText" Text="DFLP" runat="server"></ml:EncodedLiteral></td>
<% } if (m_ccTemplateMode) { %>
<td class="GFE2010RightColumn"><input id="TrdPty_chk" runat="server" type="checkbox" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" title="Paid to Third Party" TabIndex=200 data-fsid="TrdPty"><ml:EncodedLiteral id="TrdPty_chkText" Text="TP" runat="server"></ml:EncodedLiteral></td>
<td class="GFE2010RightColumn"><input id="Aff_chk" runat="server" type="checkbox" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" title="Third Party is Affiliate" TabIndex=200 data-fsid="Aff"><ml:EncodedLiteral id="Aff_chkText" Text="AFF" runat="server"></ml:EncodedLiteral></td>
<td class="GFE2010RightColumn" runat="server" id="EscrowCell" visible="false" ><input id="Escrow_chk" visible="false" runat="server" type="checkbox" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" title="Escrowed?"  tabindex=200 data-fsid="Escrow" /><ml:EncodedLiteral runat="server" Visible="false" ID="Escrow_chkText" Text="E" > </ml:EncodedLiteral> </td>
<% } %>
<td class="GFE2010RightColumn"><INPUT id="ToBrok_chk" runat="server" type="checkbox" onclick="if(typeof(unlockPaidTo) == 'function') unlockPaidTo(this.id); if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" title="Paid to Broker" TabIndex=200 data-fsid="ToBrok"><ml:EncodedLiteral id="ToBrok_chkText" Text="B" runat="server"></ml:EncodedLiteral></td>
<td class="GFE2010RightColumn">&nbsp;<INPUT id="PaidTo_tb" class="PaidToTextBox" runat="server" type="text" onchange="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" title="Paid to" TabIndex=200 data-fsid="PaidTo"/>&nbsp;<img id="AgentPicker" class="AgentPicker" runat="server" alt="pick from agents" src="~/images/contacts.png" /></td>
<td style="display:none" class="GFE2010RightColumn">
<% if (m_settlementMode) { %>
    <INPUT id="Apr_hdn" runat="server" type="hidden"></td>
<% } if (!m_ccTemplateMode) { %>
    <INPUT id="TrdPty_hdn" runat="server" type="hidden">
    <INPUT id="Aff_hdn" runat="server" type="hidden">
    <INPUT id="QmWarn_hdn" runat="server" type="hidden">
</td>
<% } %>