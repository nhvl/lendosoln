using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using System.Linq.Expressions;
using LendersOffice.Common;

namespace LendersOfficeApp.los.LegalForm
{

	public partial  class GoodFaithEstimate2010RightColumn : System.Web.UI.UserControl
	{
        protected bool m_settlementMode = false;
        protected bool m_ccTemplateMode = false;

        protected bool m_showAgentPicker = false;

        protected E_LegacyGfeFieldT m_legacyGfeFieldT;

        public bool ShowAgentPicker
        {
            get { return m_showAgentPicker; }
            set { m_showAgentPicker = value; }
        }

        public E_LegacyGfeFieldT LegacyGfeFieldT
        {
            get { return m_legacyGfeFieldT; }
            set { m_legacyGfeFieldT = value; }
        }

        protected bool m_Enabled = true;
        public bool Enabled
        {
            get
            {
                return m_Enabled;
            }
            set
            {
                m_Enabled = value;
                SetEnabled(m_Enabled);
            }
        }

        public void InitItemProps(int props)
        {
            this.Apr = LosConvert.GfeItemProps_Apr(props);
            this.ToBrok = LosConvert.GfeItemProps_ToBr(props);
            this.PdByT = LosConvert.GfeItemProps_Payer(props);
            this.Fha = LosConvert.GfeItemProps_FhaAllow(props);
            this.Borr = LosConvert.GfeItemProps_Borr(props);
            this.Poc = LosConvert.GfeItemProps_Poc(props);
            this.BF = LosConvert.GfeItemProps_BF(props);
            // this.GBF = LosConvert.GfeItemProps_GBF(props);
            this.ThirdParty_Hidden = LosConvert.GfeItemProps_PaidToThirdParty(props);
            this.Affiliate_Hidden = LosConvert.GfeItemProps_ThisPartyIsAffiliate(props);
            this.QmWarning_Hidden = LosConvert.GfeItemProps_ShowQmWarning(props);
        }

        private void SetEnabled(bool enable)
        {
            var childControls = this.GetType().GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            foreach(var control in childControls)
            {
                if(control.FieldType.BaseType == typeof(HtmlInputControl))
                {
                    ((HtmlControl)control.GetValue(this)).Disabled = !enable;
                }
                else if (control.FieldType.BaseType == typeof(WebControl))
                {
                    ((WebControl)control.GetValue(this)).Enabled = enable;
                }
            }
        }

        private bool m_Page2A_rbVisible;
        private bool m_Page2B_rbVisible;
        private bool m_Page2C_rbVisible;

        public bool Page2A_rbVisible
        {
            get { return m_Page2A_rbVisible; }
            set {
                Page2A_rb.Style.Add("display", value ? "" : "none");
                m_Page2A_rbVisible = value;
                Page2A_rb.Visible = true;
            }
        }
        public bool Page2B_rbVisible
        {
            get { return m_Page2B_rbVisible; }
            set {
                Page2B_rb.Style.Add("display", value ? "" : "none");
                m_Page2B_rbVisible = value;
                Page2B_rb.Visible = true;
            }
        }

        public bool Page2C_rbVisible
        {
            get { return m_Page2C_rbVisible; }
            set {
                Page2C_rb.Style.Add("display", value ? "" : "none");
                m_Page2C_rbVisible = value;
                Page2C_rb.Visible = true;
            }
        }

        public string Page2Value
        {
            set
            {
                Page2A_lbl.Text = value;
                Page2A_rb.Checked = true;
                Page2A_rb.Disabled = true;
            }
        }

        public int getIndexOfRadioButton(String label)
        {
            if (Page2A_lbl.Text == label)
                return 1;
            else if (Page2B_lbl.Text == label)
                return 2;
            else if (Page2C_lbl.Text == label)
                return 3;
            else
                return -1;
            
        }

        public string Page2Option1Text
        {
            set { Page2A_lbl.Text = value; }
        }

        public string Page2Option1Value
        {
            set { Page2A_rb.Value = parseValue(value); }
        }

        public string Page2Option2Text
        {
            set
            {
                Page2B_rbVisible = true;
                Page2B_lbl.Text = value;
            }
        }

        public string Page2Option2Value
        {
            set { Page2B_rb.Value = parseValue(value); }
        }

        public string Page2Option3Text
        {
            set
            {
                Page2C_rbVisible = true;
                Page2C_lbl.Text = value;
            }
        }

        public string Page2Option3Value
        {
            set { Page2C_rb.Value = parseValue(value); }
        }

        public E_GfeSectionT Page2Selection
        {
            set
            {
                Page2A_rb.Checked = value.ToString("D") == Page2A_rb.Value;
                Page2B_rb.Checked = value.ToString("D") == Page2B_rb.Value;
                Page2C_rb.Checked = value.ToString("D") == Page2C_rb.Value;
            }
            get
            {
                if (Page2A_rb.Checked)
                    return (E_GfeSectionT)Convert.ToInt32(Page2A_rb.Value);
                else if (Page2B_rb.Checked)
                    return (E_GfeSectionT)Convert.ToInt32(Page2B_rb.Value);
                else
                    return (E_GfeSectionT)Convert.ToInt32(Page2C_rb.Value);
            }
        }

		public int PdByT
		{
			get { return Convert.ToInt32(PdByT_dd.SelectedValue); }
			set { PdByT_dd.SelectedValue = value.ToString(); }
		}

        public DropDownList PdByT_DDL
        {
            get { return PdByT_dd; }
        }

        public bool ToBrok
        {
            get { return ToBrok_chk.Checked; }
            set { ToBrok_chk.Checked = value; }
        }

        public bool Apr 
        {
            get { return Apr_chk.Checked; }
            set { Apr_chk.Checked = value; }
        }

        public bool Poc 
        {
            get { return Poc_chk.Checked; }
            set { Poc_chk.Checked = value; }
        }

        public bool Fha
        {
            get { return Fha_chk.Checked; }
            set { Fha_chk.Checked = value; }
        }

        public bool Paid
        {
            get { return Paid_chk.Checked; }
            set { Paid_chk.Checked = value; }
        }

        public string PaidTo
        {
            get { return PaidTo_tb.Value; }
           // set { PaidTo_tb.Value = value; }
        }

        public void SetPaidTo(Expression<Func<string>> property)
        {
            PaidTo_tb.Value = property.Compile()();

            var propertyInfo = ((MemberExpression)property.Body).Member as System.Reflection.PropertyInfo; 
            if (propertyInfo  != null)
            {
                PaidTo_tb.Attributes.Add("data-field-id", propertyInfo.Name);
                var fieldTable = CFieldInfoTable.GetInstance();
                var field = fieldTable[propertyInfo.Name];

                if (null != field && null != field.DbInfo && field.DbInfo.m_charMaxLen > 0)
                {
                    PaidTo_tb.MaxLength = field.DbInfo.m_charMaxLen;
                }
            }
        }

        public bool Dflp
        {
            get { return Dflp_chk.Checked; }
            set { Dflp_chk.Checked = value; }
        }

        public bool ThirdParty
        {
            get { return TrdPty_chk.Checked; }
            set { TrdPty_chk.Checked = value; }
        }

        public bool Affiliate
        {
            get { return Aff_chk.Checked; }
            set { Aff_chk.Checked = value; }
        }

        public bool Apr_Hidden
        {
            get { return Apr_hdn.Value == "Y"; }
            set { Apr_hdn.Value = value ? "Y" : "N"; }
        }

        public bool ThirdParty_Hidden
        {
            get { return TrdPty_hdn.Value == "Y"; }
            set { TrdPty_hdn.Value = value ? "Y" : "N"; }
        }

        public bool Affiliate_Hidden
        {
            get { return Aff_hdn.Value == "Y"; }
            set { Aff_hdn.Value = value ? "Y" : "N"; }
        }

        public bool QmWarning_Hidden
        {
            get { return QmWarn_hdn.Value == "Y"; }
            set { QmWarn_hdn.Value = value ? "Y" : "N"; }
        }

        public bool BF
        {
            get { return BF_chk.Checked; }
            set { BF_chk.Checked = value; }
        }

        public bool Borr
        {
            get { return Borr_chk.Checked; }
            set { Borr_chk.Checked = value; }
        }

        public bool GBF
        {
            get { return GBF_chk.Checked; }
            set { GBF_chk.Checked = value; }
        }

        public bool Page2PlaceholderVisible
        {
            get { return Page2Placeholder.Visible; }
            set { Page2Placeholder.Visible = value; }
        }

        public bool BFVisible
        {
            get { return BF_chk.Visible && BF_lbl.Visible; }
            set { BF_chk.Visible = value; BF_lbl.Visible = value; }
        }

        public bool GBFVisible
        {
            get { return GBF_chk.Visible && GBF_lbl.Visible; }
            set { GBF_chk.Visible = value; GBF_lbl.Visible = value; }
        }

        public bool BorrVisible
        {
            get { return Borr_chk.Visible && Borr_chkText.Visible; }
            set { Borr_chk.Visible = value; Borr_chkText.Visible = value; }
        }

        public bool PaidCBVisible
        {
            get { return Paid_chk.Visible; }
            set 
            { 
                Paid_chk.Visible = value; 
                Paid_chkText.Visible = value; 
            }
        }

        public bool PaidToTBVisible
        {
            get { return PaidTo_tb.Visible; }
            set 
            { 
                PaidTo_tb.Visible = value;
                AgentPicker.Visible = value;
                PaidToBCBVisible = value;
            }
        }

        public bool PaidToBCBVisible
        {
            get { return ToBrok_chk.Visible; }
            set 
            { 
                ToBrok_chk.Visible = value;
                ToBrok_chkText.Visible = value;
            }
        }

        public bool PocCBVisible
        {
            get { return Poc_chk.Visible; }
            set 
            { 
                Poc_chk.Visible = value;
                Poc_chkText.Visible = value;
            }
        }

        public bool DflpCBVisible
        {
            get { return Dflp_chk.Visible; }
            set
            {
                Dflp_chk.Visible = value;
                Dflp_chkText.Visible = value;
            }
        }

        public bool PdByTDDVisible
        {
            get { return PdByT_dd.Visible; }
            set { PdByT_dd.Visible = value; }
        }

        public bool TpCBVisible
        {
            get { return TrdPty_chk.Visible; }
            set
            {
                TrdPty_chk.Visible = value;
                TrdPty_chkText.Visible = value;
                AffCBVisible = value;
            }
        }

        public bool AffCBVisible
        {
            get { return Aff_chk.Visible; }
            set
            {
                Aff_chk.Visible = value;
                Aff_chkText.Visible = value;
            }
        }

        public bool EscrowedVisible
        {
            get { return Escrow_chk.Visible; }
            set
            {
                Escrow_chk.Visible = value;
                Escrow_chkText.Visible = value;
            }
        }

        public bool EscrowCellVisible
        {
            get { return EscrowCell.Visible; }
            set { EscrowCell.Visible = value; }
        }

        public bool SettlementMode
        {
            set { m_settlementMode = value; }
        }

        public bool CCTemplateMode
        {
            set { m_ccTemplateMode = value; }
        }

        private string parseValue(string value)
        {
            return ((E_GfeSectionT)Enum.Parse(typeof(E_GfeSectionT), value)).ToString("D");
        }

        protected void PageLoad(object sender, System.EventArgs e)
		{
            string id = this.ClientID;
            Poc_chk.Attributes.Add("onclick", "OnPocCheck('" + id + "'); if(typeof(refreshCalculation) == 'function') refreshCalculation();");
            Paid_chk.Attributes.Add("onclick", "OnPaidChecking('" + id + "'); if(typeof(refreshCalculation) == 'function') refreshCalculation();");
            TrdPty_chk.Attributes.Add("onclick", "OnTpCheck('" + id + "'); if(typeof(refreshCalculation) == 'function') refreshCalculation();");
            Aff_chk.Attributes.Add("onclick", "OnAffCheck('" + id + "'); if(typeof(refreshCalculation) == 'function') refreshCalculation();");
            PaidTo_tb.Attributes.Add("onchange", "SetWarning('" + id + "'); if(typeof(refreshCalculation) == 'function') refreshCalculation();");

            AgentPicker.Attributes.Add("onclick", "PickAgent('" + id + "','" + LegacyGfeFieldT.ToString("d") + "'); if(typeof(refreshCalculation) == 'function') refreshCalculation();");
            AgentPicker.Attributes.Add("onmouseover", "OnAgentHover('" + id + "');");
            AgentPicker.Attributes.Add("onmouseout", "OnAgentHoverOut('" + id + "');");

            ((BasePage)Page).RegisterJsScript("GoodFaithEstimate2010RightColumn.js");

            if (PaidToTBVisible && !ShowAgentPicker)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "HideAgentPicker" + id, "HideAgentPicker('" + id + "');", true);
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
