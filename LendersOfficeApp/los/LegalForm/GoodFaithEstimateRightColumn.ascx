<%@ Control Language="c#" AutoEventWireup="false" Codebehind="GoodFaithEstimateRightColumn.ascx.cs" Inherits="LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<asp:dropdownlist id="PdByT_dd" runat="server" onchange="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" TabIndex=200>
	<asp:ListItem Value="0">borr pd</asp:ListItem>
	<asp:ListItem Value="1">borr fin</asp:ListItem>
	<asp:ListItem Value="2">seller</asp:ListItem>
	<asp:ListItem Value="3">lender</asp:ListItem>
	<asp:ListItem Value="4">broker</asp:ListItem>
</asp:dropdownlist>
<INPUT id="ToBrok_chk" runat="server" type="checkbox" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" title="Paid to Broker" TabIndex=200>B
<INPUT id="Apr_chk" runat="server" type="checkbox" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" onfocus="highlightRow(this);" onblur="unhighlightRow(this);" title="APR" TabIndex=200>A
<INPUT id="Fha_chk" onblur="unhighlightRow(this);" title="FHA Allowable" onfocus="highlightRow(this);" onclick="if(typeof(refreshCalculation) == 'function') refreshCalculation();" type="checkbox" name="Checkbox1" runat="server" TabIndex=200>F 
<input id="Poc_chk" onblur="unhighlightRow(this);" title="Paid Outside of Closing" onfocus="highlightRow(this);" onclick="if(typeof(OnPocCheck) == 'function') OnPocCheck(this);  if(typeof(refreshCalculation) == 'function') refreshCalculation();" type="checkbox" name="Checkbox1" runat="server" tabindex=200>POC

