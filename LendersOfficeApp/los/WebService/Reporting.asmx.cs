﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Security;
    using System.Web.Services;
    using System.Xml.Linq;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LendersOffice.XsltExportReport;
    using LqbGrammar.Drivers.SecurityEventLogging;

    /// <summary>
    /// Container for methods used to access reports or batch exports
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Reporting : System.Web.Services.WebService
    {
        // Secret key allows LQB to manage the load of the synchronous batch export service.
        private Guid synchronousBatchExportKey = new Guid("36921e48-8c73-4501-b5b8-ef8d20eb7e9e");

        [WebMethod(Description = @"This method adds a batch export to the batch export queue. 
            [sTicket] Ticket returned by AuthService.asmx. 
            [sCustomReportName] The name of the LendingQB custom report to use to determine which loans to include in the batch export. 
              Data contents of the export will be determined by the batch export format. 
            [sBatchExportType] The type of batch export to run. The type is included in the corresponding link within the Batch Export Report Formats list in LendingQB. 
              Output is a string in LOXML format. The result includes a status attribute [ERROR | OK]. 
              ERROR responses include an error message. 
              OK responses include a batchExportID.")]
        public string AddBatchExportToQueue(string sTicket, string sCustomReportName, string sBatchExportType)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            Tuple<bool, string> hasPerm_resultStr = VerifyBEServicePermissions(principal);
            if (!hasPerm_resultStr.Item1)
            {
                return hasPerm_resultStr.Item2;
            }

            #region Use Custom Report to generate the list of loans to export
            if (!HasCustomReportPermission(principal))
            {
                return GenerateErrorResponseXml(ErrorMessages.CustomReport.CustomReportFeaturePermissionRequired);
            }

            CustomReportResult result = new CustomReportResult();
            result.RunCustomReport(sCustomReportName, principal, true);

            if (result.HasError)
            {
                return GenerateErrorResponseXml(result.ErrorMessage);
            }

            if (result.CustomReport == null)
            {
                return GenerateErrorResponseXml(ErrorMessages.BatchExport.NullResult);
            }

            List<Row> loanList = result.CustomReport.Flatten();
            List<Guid> loanIDList = new List<Guid>(loanList.Count);
            foreach (Row loan in loanList)
            {
                if (loan.Key != Guid.Empty)
                {
                    loanIDList.Add(loan.Key);
                }
            }
            #endregion

            string sBatchExportReportID = string.Empty;

            try
            {
                hasPerm_resultStr = AddBatchExportToQueue_impl(principal, loanIDList, sBatchExportType.TrimWhitespaceAndBOM());
                if(!hasPerm_resultStr.Item1)
                {
                    return GenerateErrorResponseXml(hasPerm_resultStr.Item2);
                }

                sBatchExportReportID = hasPerm_resultStr.Item2;
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml(ErrorMessages.BatchExport.BEFormatNotFound);
            }

            return GenerateBEQueueSuccessMessage(sBatchExportReportID);
        }

        [WebMethod(Description = @"This method adds a batch export to the batch export queue. 
            [sTicket] Ticket returned by AuthService.asmx. 
            [sLoanList] An LOXML formatted list of LendingQB loan files to include in the batch export.
            [sBatchExportType] The type of batch export to run. The type is included in the corresponding link within the Batch Export Report Formats list in LendingQB. 
              Output is a string in LOXML format. The result includes a status attribute [ERROR | OK]. 
              ERROR responses include an error message. 
              OK responses include a batchExportID.")]
        public string AddBatchExportToQueueWithLoanList(string sTicket, string sLoanList, string sBatchExportType)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            Tuple<bool, string> hasPerm_resultStr = VerifyBEServicePermissions(principal);
            if (!hasPerm_resultStr.Item1)
            {
                return hasPerm_resultStr.Item2;
            }

            List<Guid> loanIDList = new List<Guid>();
            try
            {
                loanIDList = GetLoanIDsFromLoanBatch(principal, sLoanList);
            }
            catch (AccessDenied)
            {
                return GenerateErrorResponseXml(ErrorMessages.WebServices.LoanBatchReadAccessDenied);
            }
            catch (CBaseException cbExc)
            {
                return GenerateErrorResponseXml(cbExc.UserMessage);
            }

            string sBatchExportReportID = string.Empty;

            try
            {
                hasPerm_resultStr = AddBatchExportToQueue_impl(principal, loanIDList, sBatchExportType.TrimWhitespaceAndBOM());
                if (!hasPerm_resultStr.Item1)
                {
                    return GenerateErrorResponseXml(hasPerm_resultStr.Item2);
                }

                sBatchExportReportID = hasPerm_resultStr.Item2;

            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml(ErrorMessages.BatchExport.BEFormatNotFound);
            }

            return GenerateBEQueueSuccessMessage(sBatchExportReportID);
        }

        [WebMethod(Description = @"This method retrieves the status and contents of a batch export that was previously queued. 
            [sTicket] Ticket returned by AuthService.asmx. 
            [sBatchExportID] A batchExportID returned by AddBatchExportToQueue, or an ID belonging to a batch export that was manually queued.
              The authenticated user is required to be the same user who requested the batch export. 
              Output is a string in LOXML format. The result includes a status attribute [ERROR | PENDING | COMPLETE].
              ERROR responses include an error message.
              PENDING means that the batch export is waiting to be processed.
              COMPLETE responses include the output file contents in base64-encoded format. The contents element also includes an outputtype attribute to indicate the actual file type (XML | CSV | XLSX | TXT).")]
        public string RetrieveBatchExport(string sTicket, string sBatchExportID)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            Tuple<bool, string> hasPerm_errorStr = VerifyBEServicePermissions(principal);
            if (!hasPerm_errorStr.Item1)
            {
                return hasPerm_errorStr.Item2;
            }

            try
            {
                XsltExportResultStatusItem beResult = XsltExportResultStatus.GetResult(principal.BrokerId, principal.UserId, sBatchExportID);

                switch (beResult.Status)
                {
                    case E_XsltExportResultStatusT.Complete:
                        string sBEFileContents = GetBatchExportFileContentsBase64Encoded(beResult.ReportId);

                        if (String.IsNullOrEmpty(sBEFileContents))
                        {
                            return GenerateErrorResponseXml(ErrorMessages.BatchExport.BERetrievalFailure);
                        }

                        string sBEFileType = "UNKNOWN";
                        try
                        {
                            sBEFileType = beResult.OutputFileName.Substring(beResult.OutputFileName.LastIndexOf('.'));
                            sBEFileType = sBEFileType.Replace(".", "").ToUpper();
                        }
                        catch (ArgumentNullException argnullExc)
                        {
                            Tools.LogWarning(string.Format("RetrieveBatchExport::Batch export with report ID {0} has no outputfilename.", beResult.ReportId), argnullExc);
                        }
                        catch (ArgumentOutOfRangeException aoorExc)
                        {
                            Tools.LogWarning(string.Format("RetrieveBatchExport::Batch export with report ID {0} has no outputfilename extension.", beResult.ReportId), aoorExc);
                        }

                        return GenerateBECompleteResponseXml(sBEFileContents, sBEFileType, beResult.ReportId);
                    case E_XsltExportResultStatusT.Error: return GetBEErrorResponseXml(beResult);
                    case E_XsltExportResultStatusT.Pending: return GenerateBEPendingResponseXml(beResult.ReportId);
                    default:
                        throw new UnhandledEnumException(beResult.Status);
                }
            }
            catch (ArgumentNullException anExc)
            {
                return GenerateErrorResponseXml(anExc.Message);
            }
            catch (NotFoundException nfExc)
            {
                return GenerateErrorResponseXml(nfExc.Message);
            }
            catch (UnhandledEnumException ueExc)
            {
                Tools.LogErrorWithCriticalTracking(ueExc);
                return GenerateErrorResponseXml(ueExc.Message);
            }
        }

        [WebMethod(Description = @"This method returns a CSV string containing the requested custom report data. 
            [sTicket] Ticket returned by AuthService.asmx. 
            [sQueryNm] The LendingQB custom report name. 
            [includeAllWithAccess] If true then the report will include all loans up to the loan access level of the authenticated user (Corporate | Branch | Individual). 
              Otherwise the report will only include loans that the user is explicitly assigned to (Individual level).")]
        public string RetrieveCustomReport(string sTicket, string sQueryNm, bool includeAllWithAccess)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (principal == null)
            {
                return GenerateErrorResponseXml(ErrorMessages.WebServices.InvalidAuthTicket);
            }

            // OPM 272825 - Allow PML users to run custom reports.
            if (principal.Type != "P" && !HasCustomReportPermission(principal))
            {
                return GenerateErrorResponseXml(ErrorMessages.CustomReport.CustomReportFeaturePermissionRequired);
            }
            // Field-level checks are made by the custom report engine itself

            Tools.LogInfo("Custom Report: " + sQueryNm + Environment.NewLine + "IncludeAllWithAccess: " + includeAllWithAccess);

            CustomReportResult result = new CustomReportResult();
            result.RunCustomReport(sQueryNm, principal, includeAllWithAccess);

            if (result.HasError)
            {
                return GenerateErrorResponseXml(result.ErrorMessage);
            }

            if (result.CustomReport == null)
            {
                return "";
            }
            else
            {
                return result.CustomReport.ConvertToCsv(principal);
            }
        }

        [WebMethod(Description = @"LENDINGQB INTERNAL USE ONLY. This method processes a batch export immediately, rather than queueing.
            [sTicket] Ticket returned by AuthService.asmx.
            [sCustomReportName] The name of the LendingQB custom report to use to determine which loans to include in the batch export. 
              Data contents of the export will be determined by the batch export format.
            [sBatchExportType] The type of batch export to run. The type is included in the corresponding link within the Batch Export Report Formats list in LendingQB. 
            [sKey] An application key that must be input to run this function.
              Output is a string in LOXML format. The result includes a status attribute [ERROR | COMPLETE].
              ERROR responses include an error message.
              COMPLETE responses include the output file contents in base64-encoded format. The contents element also includes an outputtype attribute to indicate the actual file type (XML | CSV | XLSX | TXT).")]
        public string RunSynchronousBatchExport(string sTicket, string sCustomReportName, string sBatchExportType, string sKey)
        {
            // Guid key is validated against synchronousBatchExportKey regardless of hyphens
            if (!sKey.Replace("-", "").Equals(synchronousBatchExportKey.ToString("N")))
            {
                return GenerateErrorResponseXml("Invalid application code.");
            }
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            Tuple<bool, string> hasPerm_resultStr = VerifyBEServicePermissions(principal);
            if (!hasPerm_resultStr.Item1)
            {
                return hasPerm_resultStr.Item2;
            }

            #region Use Custom Report to generate the list of loans to export
            if (!HasCustomReportPermission(principal))
            {
                return GenerateErrorResponseXml(ErrorMessages.CustomReport.CustomReportFeaturePermissionRequired);
            }

            CustomReportResult result = new CustomReportResult();
            result.RunCustomReport(sCustomReportName, principal, true);

            if (result.HasError)
            {
                return GenerateErrorResponseXml(result.ErrorMessage);
            }

            if (result.CustomReport == null)
            {
                return GenerateErrorResponseXml(ErrorMessages.BatchExport.NullResult);
            }

            List<Row> loanList = result.CustomReport.Flatten();
            List<Guid> loanIDList = new List<Guid>(loanList.Count);
            foreach (Row loan in loanList)
            {
                if (loan.Key != Guid.Empty)
                {
                    loanIDList.Add(loan.Key);
                }
            }
            #endregion

            #region Create request object and run the batch export
            string sBatchExportReportID = string.Empty;
            string sBEFileType = "UNKNOWN";
            XsltExportResult sBatchExportResult = null;

            try
            {
                List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
                XsltExportRequest request = new XsltExportRequest(principal.UserId, loanIDList, sBatchExportType.TrimWhitespaceAndBOM(), null, parameters);
                if (!UserHasFieldLevelPermissionToBatchExport(request, principal))
                {
                    return GenerateErrorResponseXml(ErrorMessages.BatchExport.BEFieldPermissionRequired);
                }
                sBatchExportResult = RunSynchronousBatchExport_impl(request, principal);
                sBatchExportReportID = sBatchExportResult.ReportId;

                // Pull the file type out of the request object
                try
                {
                    sBEFileType = request.OutputFileName.Substring(request.OutputFileName.LastIndexOf('.'));
                    sBEFileType = sBEFileType.Replace(".", "").ToUpper();
                }
                catch (ArgumentNullException argnullExc)
                {
                    Tools.LogWarning(string.Format("RetrieveBatchExport::Batch export with report ID {0} has no outputfilename.", sBatchExportReportID), argnullExc);
                }
                catch (ArgumentOutOfRangeException aoorExc)
                {
                    Tools.LogWarning(string.Format("RetrieveBatchExport::Batch export with report ID {0} has no outputfilename extension.", sBatchExportReportID), aoorExc);
                }
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml(ErrorMessages.BatchExport.BEFormatNotFound);
            }
            #endregion

            #region Package the result contents into a LOXML string
            try
            {
                XsltExportResultStatus.Update(sBatchExportResult);
                string sBEFileContents = GetBatchExportFileContentsBase64Encoded(sBatchExportReportID);

                if (String.IsNullOrEmpty(sBEFileContents))
                {
                    return GenerateErrorResponseXml(ErrorMessages.BatchExport.BERetrievalFailure);
                }

                return GenerateBECompleteResponseXml(sBEFileContents, sBEFileType, sBatchExportReportID);

            }
            catch (ArgumentNullException anExc)
            {
                return GenerateErrorResponseXml(anExc.Message);
            }
            catch (NotFoundException nfExc)
            {
                return GenerateErrorResponseXml(nfExc.Message);
            }
            catch (UnhandledEnumException ueExc)
            {
                Tools.LogErrorWithCriticalTracking(ueExc);
                return GenerateErrorResponseXml(ueExc.Message);
            }
            #endregion
        }

        [WebMethod(Description = @"LENDINGQB INTERNAL USE ONLY. This method processes a batch export immediately, rather than queueing.
            [sTicket] Ticket returned by AuthService.asmx.
            [sLoanList] An LOXML formatted list of LendingQB loan files to include in the batch export.
            [sBatchExportType] The type of batch export to run. The type is included in the corresponding link within the Batch Export Report Formats list in LendingQB. 
            [sKey] An application key that must be input to run this function.
              Output is a string in LOXML format. The result includes a status attribute [ERROR | COMPLETE].
              ERROR responses include an error message.
              COMPLETE responses include the output file contents in base64-encoded format. The contents element also includes an outputtype attribute to indicate the actual file type (XML | CSV | XLSX | TXT).")]
        public string RunSynchronousBatchExportWithLoanList(string sTicket, string sLoanList, string sBatchExportType, string sKey)
        {
            // Guid key is validated against synchronousBatchExportKey regardless of hyphens
            if (!sKey.Replace("-", "").Equals(synchronousBatchExportKey.ToString("N")))
            {
                return GenerateErrorResponseXml("Invalid application code.");
            }
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            Tuple<bool, string> hasPerm_resultStr = VerifyBEServicePermissions(principal);
            if (!hasPerm_resultStr.Item1)
            {
                return hasPerm_resultStr.Item2;
            }

            List<Guid> loanIDList = new List<Guid>();
            try
            {
                loanIDList = this.ExtractLoanIdsFromLOXml(principal, sLoanList);
            }
            catch (CBaseException cbExc)
            {
                return GenerateErrorResponseXml(cbExc.UserMessage);
            }

            #region Create request object and run the batch export
            string sBatchExportReportID = string.Empty;
            string sBEFileType = "UNKNOWN";
            XsltExportResult sBatchExportResult = null;

            try
            {
                List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
                XsltExportRequest request = new XsltExportRequest(principal.UserId, loanIDList, sBatchExportType.TrimWhitespaceAndBOM(), null, parameters);
                if (!UserHasFieldLevelPermissionToBatchExport(request, principal))
                {
                    return GenerateErrorResponseXml(ErrorMessages.BatchExport.BEFieldPermissionRequired);
                }
                sBatchExportResult = RunSynchronousBatchExport_impl(request, principal);
                sBatchExportReportID = sBatchExportResult.ReportId;

                // Pull the file type out of the request object
                try
                {
                    sBEFileType = request.OutputFileName.Substring(request.OutputFileName.LastIndexOf('.'));
                    sBEFileType = sBEFileType.Replace(".", "").ToUpper();
                }
                catch (ArgumentNullException argnullExc)
                {
                    Tools.LogWarning(string.Format("RetrieveBatchExport::Batch export with report ID {0} has no outputfilename.", sBatchExportReportID), argnullExc);
                }
                catch (ArgumentOutOfRangeException aoorExc)
                {
                    Tools.LogWarning(string.Format("RetrieveBatchExport::Batch export with report ID {0} has no outputfilename extension.", sBatchExportReportID), aoorExc);
                }
            }
            catch (LoanNotFoundException exc)
            {
                return GenerateErrorResponseXml(exc.UserMessage);
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml(ErrorMessages.BatchExport.BEFormatNotFound);
            }
            catch (PageDataAccessDenied)
            {
                return GenerateErrorResponseXml(ErrorMessages.WebServices.LoanBatchReadAccessDenied);
            }
            #endregion

            #region Package the result contents into a LOXML string
            try
            {
                XsltExportResultStatus.Update(sBatchExportResult);
                string sBEFileContents = GetBatchExportFileContentsBase64Encoded(sBatchExportReportID);

                if (string.IsNullOrEmpty(sBEFileContents))
                {
                    return GenerateErrorResponseXml(ErrorMessages.BatchExport.BERetrievalFailure);
                }

                return GenerateBECompleteResponseXml(sBEFileContents, sBEFileType, sBatchExportReportID);

            }
            catch (ArgumentNullException anExc)
            {
                return GenerateErrorResponseXml(anExc.Message);
            }
            catch (NotFoundException nfExc)
            {
                return GenerateErrorResponseXml(nfExc.Message);
            }
            catch (UnhandledEnumException ueExc)
            {
                Tools.LogErrorWithCriticalTracking(ueExc);
                return GenerateErrorResponseXml(ueExc.Message);
            }
            #endregion
        }

        private Tuple<bool, string> AddBatchExportToQueue_impl(AbstractUserPrincipal principal, List<Guid> loanIDList, string sBatchExportType)
        {
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();
            // to-do: allow the request to indicate that the field associated with the batch export should be marked
            // 3/11/14 - None of the existing batch exports have been implemented to use this, so it isn't critical
            /*
            try
            {
                XsltMapItem item = XsltMap.Get(sBatchExportFormatName);
              
                if (bSetCustomField)
                {
                    if (!string.IsNullOrEmpty(item.CustomFieldId))
                    {
                        parameters.Add(new KeyValuePair<string, string>("UpdatedField", sCustomFieldId));
                        parameters.Add(new KeyValuePair<string, string>("UpdatedData", sCustomFieldValue));
                    }
                }
            }
            catch (NotFoundException)
            {
                throw;
            }
            */

            try
            {
                XsltExportRequest request = new XsltExportRequest(principal.UserId, loanIDList, sBatchExportType, null, parameters);
                if (!UserHasFieldLevelPermissionToBatchExport(request, principal))
                {
                    return new Tuple<bool, string>(false, ErrorMessages.BatchExport.BEFieldPermissionRequired);
                }

                XsltExport.SubmitRequest(principal.BrokerId, request);
                return new Tuple<bool, string>(true, request.ReportId);
            }
            catch (NotFoundException)
            {
                throw;
            }
        }

        private XsltExportResult RunSynchronousBatchExport_impl(XsltExportRequest request, AbstractUserPrincipal principal)
        {
            try
            {
                XsltExportResult result = XsltExport.ExecuteSynchronously(request, principal);
                return result;
            }
            catch (NotFoundException)
            {
                throw;
            }
        }

        private string GenerateErrorResponseXml(string sMessage)
        {
            XElement xResult = new XElement("result", new XAttribute("status", "ERROR"));
            XElement xMessage = new XElement("message", SecurityElement.Escape(sMessage));
            xResult.Add(xMessage);
            return GenerateResponseXmlDoc(xResult);
        }
        private string GenerateBEQueueSuccessMessage(string sReportID)
        {
            XElement xResult = new XElement("result", new XAttribute("status", "OK"));
            XElement xMessage = new XElement("batchExportID", SecurityElement.Escape(sReportID));
            xResult.Add(xMessage);
            return GenerateResponseXmlDoc(xResult);
        }

        private string GenerateBECompleteResponseXml(string sBEFileContents, string sBEFileType, string sReportID)
        {
            XElement xResult = GenerateBEResultXml("COMPLETE", "Batch export finished.", sReportID);
            XElement xContents = new XElement("contents", new XAttribute("outputtype", SecurityElement.Escape(sBEFileType)));
            xContents.Add(new XAttribute("encoding", "base64"));
            xContents.SetValue(sBEFileContents);
            xResult.Add(xContents);

            return GenerateResponseXmlDoc(xResult);
        }
        private string GetBEErrorResponseXml(XsltExportResultStatusItem beResult)
        {
            // If at some point in the future the worker saves the error to the DB, or to some easily-accessible log, then this can return the actual error.
            XElement xResult = GenerateBEResultXml("ERROR", ErrorMessages.BatchExport.BatchExportFailure, beResult.ReportId);
            return GenerateResponseXmlDoc(xResult);
        }
        private string GenerateBEPendingResponseXml(string sReportID)
        {
            XElement xResult = GenerateBEResultXml("PENDING", "The batch export is in the queue.", sReportID);
            return GenerateResponseXmlDoc(xResult);
        }
        private string GenerateResponseXmlDoc(XElement xResult)
        {
            XDocument xDoc = new XDocument();
            XElement xRoot = new XElement("LOXmlFormat", new XAttribute("version", LendersOffice.Constants.ConstAppDavid.LOXmlFormatVersion));
            xDoc.Add(xRoot);
            xRoot.Add(xResult);
            return xDoc.ToString();
        }
        private XElement GenerateBEResultXml(string sStatus, string sMessage, string sReportID)
        {
            XElement xResult = new XElement("result", new XAttribute("status", SecurityElement.Escape(sStatus)));
            XElement xBatchExportID = new XElement("batchExportID", SecurityElement.Escape(sReportID));
            xResult.Add(xBatchExportID);
            XElement xMessage = new XElement("message", SecurityElement.Escape(sMessage));
            xResult.Add(xMessage);
            return xResult;
        }

        private string GetBatchExportFileContentsBase64Encoded(string sReportID)
        {
            byte[] contents = FileDBTools.ReadData(E_FileDB.Normal, sReportID);

            return (contents != null) ? Convert.ToBase64String(contents) : string.Empty;
        }

        /// <summary>
        /// Retrieves the loan identifiers from the LOXML list of loans and verifies the user has
        /// read permission for each loan.
        /// </summary>
        /// <param name="principal">The user requesting a batch export.</param>
        /// <param name="loanList">An LOXML list of loans where each loan holds an sLId, sLNm, or both.</param>
        /// <returns>A list of loan identifiers the user has permission to read.</returns>
        private List<Guid> GetLoanIDsFromLoanBatch(AbstractUserPrincipal principal, string loanList)
        {
            List<Guid> loanIDList = LOFormatExporter.GetLoanIdentifiersFromBatch(loanList, principal);

            if (loanIDList.Count == 0)
            {
                throw new CBaseException(ErrorMessages.WebServices.EmptyLoanBatch, ErrorMessages.WebServices.EmptyLoanBatch);
            }

            // Validate read permission on all loans before returning
            foreach (Guid sLId in loanIDList)
            {
                AuthServiceHelper.CheckLoanAuthorization(sLId, WorkflowOperations.ReadLoan);
            }

            return loanIDList;
        }

        /// <summary>
        /// Retrieves the loan identifiers from the LOXML list of loans.
        /// </summary>
        /// <param name="principal">The user requesting a batch export.</param>
        /// <param name="loanList">An LOXML list of loans where each loan holds an sLId, sLNm, or both.</param>
        /// <returns>A list of loan identifiers from the LoXml.</returns>
        private List<Guid> ExtractLoanIdsFromLOXml(AbstractUserPrincipal principal, string loanList)
        {
            List<Guid> loanIDList = LOFormatExporter.GetLoanIdentifiersFromBatch(loanList, principal);

            if (loanIDList.Count == 0)
            {
                throw new CBaseException(ErrorMessages.WebServices.EmptyLoanBatch, ErrorMessages.WebServices.EmptyLoanBatch);
            }

            return loanIDList;
        }

        private bool HasCustomReportPermission(AbstractUserPrincipal principal)
        {
            return (principal.ApplicationType == E_ApplicationT.LendersOffice) && principal.HasPermission(Permission.CanRunCustomReports);
        }
        private bool HasBatchExportPermission(AbstractUserPrincipal principal)
        {
            return (principal.ApplicationType == E_ApplicationT.LendersOffice) && principal.HasPermission(Permission.CanRunCustomReports);
        }

        private bool UserHasFieldLevelPermissionToBatchExport(XsltExportRequest request, AbstractUserPrincipal principal)
        {
            XsltExportWorker worker = new XsltExportWorker(request, principal);
            if (principal.ApplicationType != E_ApplicationT.LendersOffice)
            {
                return false; // Even though the web service is blocked for non-B user, the extra check here is meant to guarantee the principal type conversion 
            }

            return worker.UserHasFieldLevelAccess(principal as BrokerUserPrincipal);
        }

        private Tuple<bool, string> VerifyBEServicePermissions(AbstractUserPrincipal principal)
        {
            if (principal == null)
            {
                return new Tuple<bool, string>(false, GenerateErrorResponseXml(ErrorMessages.WebServices.InvalidAuthTicket));
            }

            if (!HasBatchExportPermission(principal))
            {
                return new Tuple<bool, string>(false, GenerateErrorResponseXml(ErrorMessages.BatchExport.BEFrameworkPermissionRequired));
            }

            return new Tuple<bool, string>(true, string.Empty);
        }
    }

    public class CustomReportResult
    {
        #region Variables
        private Report m_report;
        private string m_sErrorMessage;

        public Report CustomReport
        {
            get { return m_report; }
        }

        public bool HasError { get; private set; }

        public string ErrorMessage
        {
            get { return m_sErrorMessage; }
            private set
            {
                m_sErrorMessage = value;
                HasError = true;
            }
        }
        #endregion

        public CustomReportResult()
        {
            m_report = null;
            HasError = false;
            m_sErrorMessage = string.Empty;
        }

        public void RunCustomReport(string sCustomReportName, AbstractUserPrincipal principal, bool bIncludeAllLoansAtUserAccessLevel)
        {
            LoanReporting lreporting = new LoanReporting();
            if (principal.Type == "P")
            {
                lreporting.MaxViewableRowCount = ConstStage.RetrieveCustomReportPmlUserMaxRowsReturned;
            }

            Guid queryID = GetCustomReportQueryID(principal, sCustomReportName);

            if (HasError)
            {
                return;
            }

            // Either honor the user's loan access level [Corporate | Branch | Assigned] or confine the result to loans explicitly assigned to the user
            E_ReportExtentScopeT userLoanAccessScope = (bIncludeAllLoansAtUserAccessLevel) ? E_ReportExtentScopeT.Default : E_ReportExtentScopeT.Assign;

            try
            {
                m_report = lreporting.RunReport(principal.BrokerId, queryID, principal, userLoanAccessScope);
            }
            catch (ArgumentException argExc)
            {
                ErrorMessage = argExc.Message;
            }
            catch (PermissionException permExc)
            {
                ErrorMessage = permExc.Message;
            }
            catch (CBaseException baseExc)
            {
                ErrorMessage = baseExc.Message;
            }
        }

        private Guid GetCustomReportQueryID(AbstractUserPrincipal principal, string sQueryNm)
        {
            Guid queryID = Guid.Empty;
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            parameters.Add(new SqlParameter("@QueryName", sQueryNm));

            // Search the user's reports for a match
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "RetrieveQueryIdByQueryNameAndEmployeeId", parameters))
            {
                if (reader.Read())
                {
                    queryID = new Guid(reader["QueryId"].ToString());
                }
            }

            // Try the lender's reports
            if (queryID == Guid.Empty)
            {
                parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@BrokerId", principal.BrokerId));
                parameters.Add(new SqlParameter("@QueryName", sQueryNm));

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "RetrieveQueryIdByQueryNameAndBrokerId", parameters))
                {
                    if (reader.Read())
                    {
                        queryID = new Guid(reader["QueryId"].ToString());
                    }
                }
            }

            // Try LQB's default report library
            if (queryID == Guid.Empty)
            {

                parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@BrokerId", LendersOffice.Admin.BrokerDB.AdminBr));
                parameters.Add(new SqlParameter("@QueryName", sQueryNm));

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(LendersOffice.Admin.BrokerDB.AdminBr, "RetrieveQueryIdByQueryNameAndBrokerId", parameters))
                {
                    if (reader.Read())
                    {
                        queryID = new Guid(reader["QueryId"].ToString());
                    }
                    else
                    {
                        ErrorMessage = ErrorMessages.CannotFindCustomReportQuery;
                    }
                }
            }

            return queryID;
        }
    }
}
