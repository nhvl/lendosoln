﻿// <copyright file="EDocsService.asmx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Justin Lara
//    Date:   11/21/2014
// </summary>
namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Security;
    using System.Text;
    using System.Web.Services;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using EDocs;
    using EDocs.Contents;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Integration.DocumentCapture;
    using LendersOffice.Integration.GenericFramework;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.Security;

    /// <summary>
    /// Container for methods used to access EDocs methods that are not loan-specific.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class EDocsService : System.Web.Services.WebService
    {
        /// <summary>
        /// The maximum number of records to retrieve from the database at a time.
        /// </summary>
        private static readonly int ListEDocsWithAuditHistoryMaxRecords = 2000;

        /// <summary>
        /// Secret key used for authenticating the <see cref="ListEDocsWithAuditHistory(string, string, EDocsFieldId[], int)"/> caller as coming from LendingQB.
        /// </summary>
        private static readonly string ListEDocsWithAuditHistorySecretKey = "peqOIaCzVkpcQkPqHyzT";

        /// <summary>
        /// The list of columns in the EDocs Audit History table to export with <see cref="ListEDocsAuditHistory(string, string, int)"/>.
        /// </summary>
        private static List<string> edocsAuditHistoryColumnIds = new List<string>
        {
            "DocumentId",
            "ModifiedByUserId",
            "Version",
            "ModifiedDate",
            "Description",
            "Details"
        };

        /// <summary>
        /// Enumerates the column names that can be retrieved by the <see cref="ListEDocsByDocIds(string, string, Guid[], EDocsFieldId[])"/> method.
        /// The enum values must have exactly the same names as columns returned by the EDOCS_FetchBrokerDocsByVersion stored procedure.
        /// </summary>
        public enum EDocsFieldId
        {
            /// <summary>
            /// Document ID.
            /// </summary>
            DocumentId = 1,

            /// <summary>
            /// The associated Loan ID.
            /// </summary>
            sLId = 2,

            /// <summary>
            /// The associated App ID.
            /// </summary>
            aAppId = 3,

            /// <summary>
            /// Document validity.
            /// </summary>
            IsValid = 4,

            /// <summary>
            /// Document's Folder ID.
            /// </summary>
            FolderId = 5,

            /// <summary>
            /// Folder name.
            /// </summary>
            FolderName = 6,

            /// <summary>
            /// Document Type ID.
            /// </summary>
            DocTypeId = 7,

            /// <summary>
            /// Document Type name.
            /// </summary>
            DocTypeName = 8,

            /// <summary>
            /// Document Type validity.
            /// </summary>
            DocTypeIsValid = 9,

            /// <summary>
            /// Public Description.
            /// </summary>
            PublicDescription = 10,

            /// <summary>
            /// Internal Description.
            /// </summary>
            InternalDescription = 11,

            /// <summary>
            /// Number of pages.
            /// </summary>
            NumPages = 12,

            /// <summary>
            /// Creation date.
            /// </summary>
            CreatedDate = 13,

            /// <summary>
            /// Last modified date.
            /// </summary>
            LastModifiedDate = 14,

            /// <summary>
            /// EDoc Status.
            /// </summary>
            Status = 15,

            /// <summary>
            /// Status Description.
            /// </summary>
            StatusDescription = 16
        }

        /// <summary>
        /// Returns a PDF document containing EDocs on the given loan file. EDocs with Protected status are included by default. Obsolete/Rejected docs are omitted.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="sShippingTemplateName">The shipping template determining which EDocs to include.</param>
        /// <param name="sLNm">The LendingQB Loan Number for the loan file from which to download the EDocs.</param>
        /// <param name="includeScreenedDocs">Whether to include EDocs with a status of "Screened".</param>
        /// <param name="includeDocsWithoutStatus">Whether to include EDocs with no status.</param>
        /// <returns>A PDF document containing the processed EDocs, encoded in base 64.</returns>
        /// <remarks>The loan number will be used as a unique id between LendingQB and the 3rd party system. Therefore, the user should proceed with extra care when changing the loan number in LendingQB.</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod(Description = "This method returns a PDF containing EDocs on the given loan file [sLNm] that are included in the shipping template [sShippingTemplateName]. EDocs with Protected status are included by default. Obsolete/Rejected docs are omitted. Does not preserve electronic signatures.")]
        public byte[] DownloadEdocsByShippingTemplate(string sTicket, string sShippingTemplateName, string sLNm, bool includeScreenedDocs, bool includeDocsWithoutStatus)
        {
            AbstractUserPrincipal principal = this.GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            if (principal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) == false)
            {
                throw new WarningOnlyWebserviceException(new AccessDenied(ErrorMessages.EDocs.InsufficientReadPermission));
            }

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                throw new WarningOnlyWebserviceException(new LoanNotFoundException());
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                throw new WarningOnlyWebserviceException(new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan));
            }

            AuthServiceHelper.CheckLoanAuthorization(sLId, WorkflowOperations.ReadLoan);

            EDocumentRepository repository = EDocumentRepository.GetUserRepository();

            List<EDocument> eDocList = repository.GetSortedDocumentsByShippingTemplate(sShippingTemplateName, sLId);

            if (eDocList.Count == 0)
            {
                throw new WarningOnlyWebserviceException(new CBaseException(ErrorMessages.EDocs.EmptyShippingTemplateOrEDocList, ErrorMessages.EDocs.EmptyShippingTemplateOrEDocList));
            }

            List<Guid> eDocIDList = new List<Guid>(eDocList.Count);
            foreach (EDocument eDoc in eDocList)
            {
                bool bIncludeEDoc = false;
                switch (eDoc.DocStatus)
                {
                    case E_EDocStatus.Approved:
                        bIncludeEDoc = true;
                        break;

                    case E_EDocStatus.Blank:
                        bIncludeEDoc = includeDocsWithoutStatus;
                        break;

                    case E_EDocStatus.Obsolete:
                    case E_EDocStatus.Rejected:
                        bIncludeEDoc = false;
                        break;

                    case E_EDocStatus.Screened:
                        bIncludeEDoc = includeScreenedDocs;
                        break;

                    default:
                        throw new UnhandledEnumException(eDoc.DocStatus);
                }

                if (bIncludeEDoc)
                {
                    eDocIDList.Add(eDoc.DocumentId);
                }
            }

            using (MemoryStream stream = new MemoryStream())
            {
                EDocumentViewer.WriteSinglePdf(eDocIDList, stream);

                return stream.ToArray();
            }
        }

        /// <summary>
        /// Downloads a requested GenericEDocument as a byte array, specified by its GUID.
        /// </summary>
        /// <param name="ticket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="docId">The ID of the document to download.</param>
        /// <returns>The string content of the requested "Generic" EDoc.</returns>
        [WebMethod(Description = "Downloads the non-PDF byte content related to the document with the given [docId]. This method will not return PDF EDocs, use DownloadEdocsPdfById() for that instead. Some EDocs (e.g. UCD) may have both PDF and text content.")]
        public byte[] DownloadDocumentNonPdfBytesById(string ticket, Guid docId)
        {
            return BinaryFileHelper.ReadAllBytes(ValidateWebServiceAndGetGenericDocPath(ticket, docId));
        }

        /// <summary>
        /// Downloads a requested GenericEDocument as a string, specified by its GUID.
        /// </summary>
        /// <param name="ticket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="docId">The ID of the document to download.</param>
        /// <returns>The string content of the requested "Generic" EDoc.</returns>
        [WebMethod(Description = "Downloads the non-PDF text content related to the document with the given [docId]. This method will not return EDocs PDF content, use DownloadEdocsPdfById() for that instead. Some EDocs (e.g. UCD) may have both PDF and non-PDF content.")]
        public string DownloadTextDocumentById(string ticket, Guid docId)
        {
            return TextFileHelper.ReadFile(ValidateWebServiceAndGetGenericDocPath(ticket, docId));
        }

        /// <summary>
        /// Downloads a requested EDoc in PDF, specified by its GUID.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="docId">The ID of the document to download.</param>
        /// <returns>A PDF document of the requested EDoc, encoded in base 64.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod]
        public byte[] DownloadEdocsPdfById(string sTicket, Guid docId)
        {
            string ticketLoanReferenceNumber;
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalAndsLIdFromTicket(sTicket, out ticketLoanReferenceNumber);
            CallVolume.IncreaseAndCheck(principal);

            if (principal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) == false)
            {
                throw new WarningOnlyWebserviceException(new AccessDenied(ErrorMessages.EDocs.InsufficientReadPermission));
            }

            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            EDocument eDocs = null;
            if (string.IsNullOrEmpty(ticketLoanReferenceNumber))
            {
                eDocs = repository.GetDocumentById(docId);
            }
            else
            {
                Guid genericFrameworksLId = Tools.GetLoanIdByLoanReferenceNumber(principal.BrokerId, ticketLoanReferenceNumber);
                eDocs = repository.GetDocumentsByLoanId(genericFrameworksLId).FirstOrDefault(eDoc => eDoc.DocumentId == docId);
                if (eDocs == null)
                {
                    throw new WarningOnlyWebserviceException(new NotFoundException(ErrorMessages.EDocs.DocumentNotFoundException));
                }
            }

            if (eDocs.ImageStatus == E_ImageStatus.NoCachedImagesButInQueue)
            {
                // OPM 472992.  Our doc has not been rasterized.
                // Throw rather than return a corrupt PDF.
                // Similar message to what the UI gives when the doc is not ready.
                var msg = ErrorMessages.EDocs.DocIsNotPrepared + " Prepping editor -est.time remaining: " + eDocs.GetImageReadyETA();
                throw new WarningOnlyWebserviceException(new CBaseException(msg, msg));
            }

            string tmpFile = eDocs.GetPDFTempFile_Current();
            byte[] bytes = BinaryFileHelper.ReadAllBytes(tmpFile);
            try
            {
                FileOperationHelper.Delete(tmpFile); // dd 6/1/2018 - Delete temp file right away to avoid multiple calls to this webservice cause out of disk.
            }
            catch (IOException)
            {
                // NO-OP
            }

            return bytes;
        }

        /// <summary>
        /// This method retrieves a list of EDocs DocTypes the user has access to.
        /// </summary>
        /// <param name="ticket">Ticket returned by the authentication service.</param>
        /// <returns>A string in LOXML format. The result includes a status attribute [ERROR | COMPLETE].
        /// ERROR responses include an error message.
        /// COMPLETE responses include an LOXML list of DocTypes.</returns>
        [WebMethod(Description = @"This method retrieves a list of EDocs DocTypes the user has access to.
            [ticket] Ticket returned by AuthService.asmx.
              Output is a string in LOXML format. The result includes a status attribute [ERROR | COMPLETE].
              ERROR responses include an error message.
              COMPLETE responses include an LOXML list of DocTypes.")]
        public string ListDocTypes(string ticket)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null, true);
            CallVolume.IncreaseAndCheck(principal);
            Tools.LogInfo("ListDocTypes request by " + principal.DisplayName + " (" + principal.LoginNm + "). Broker ID: " + principal.BrokerId);

            string errorMessage;
            if (!this.VerifyEdocsServicePermissions(principal, out errorMessage))
            {
                string errorXml = this.GenerateErrorResponseXml(errorMessage);
                Tools.LogInfo("ListDocTypes Error:" + Environment.NewLine + errorXml);
                return errorXml;
            }

            // Build the result XElement from any DocTypes not in system folders
            var docTypes = from docType in EDocumentDocType.GetDocTypesByBroker(principal.BrokerId, E_EnforceFolderPermissions.True)
                           where !docType.Folder.IsSystemFolder
                           select new XElement(
                               "docType",
                               new XElement("id", docType.Id),
                               new XElement("docTypeName", docType.DocTypeName),
                               new XElement("folderName", docType.Folder.FolderNm));

            XElement docTypeResult = new XElement("result", new XAttribute("status", "COMPLETE"));
            foreach (XElement docType in docTypes)
            {
                docTypeResult.Add(docType);
            }

            string resultXml = WebServiceUtilities.GenerateResponseXml(docTypeResult);
            Tools.LogInfo("ListDocTypes Response:" + Environment.NewLine + resultXml);
            return resultXml;
        }

        /// <summary>
        /// Retrieves an XML list of EDocs audit history records with a version number later than the provided version number, ordered by version number.
        /// Retrieves a maximum number of records equal to <see cref="ListEDocsWithAuditHistoryMaxRecords"/>.
        /// </summary>
        /// <param name="authTicket">The authentication ticket returned from <see cref="AuthService"/>.</param>
        /// <param name="secretKey">A secret key to make sure that the method is only called by LendingQB.</param>
        /// <param name="version">
        /// The EDocs records in the return XML will have version numbers greater than this parameter's value.
        /// Negative values are not valid and will return an error. 0 is valid and will return all EDocs records ordered by version number.</param>
        /// <returns>An XML-formatted string containing EDocs records which satisfy the conditions of this method.</returns>
        [WebMethod(Description = "LENDINGQB INTERNAL USE ONLY.")]
        public string ListEDocsAuditHistory(string authTicket, string secretKey, int version)
        {
            ServiceResult serviceResult = new ServiceResult();
            AbstractUserPrincipal principal = this.GetPrincipalFromTicket(authTicket);
            CallVolume.IncreaseAndCheck(principal);

            if (principal == null)
            {
                return GenerateErrorResponse(serviceResult, "Invalid Credentials.");
            }

            if (!secretKey.Equals(ListEDocsWithAuditHistorySecretKey))
            {
                return GenerateErrorResponse(serviceResult, "Invalid Credentials.");
            }

            if (!(principal.IsInRole(ConstApp.ROLE_ADMINISTRATOR) && principal.HasPermission(Permission.CanViewEDocs)))
            {
                return GenerateErrorResponse(serviceResult, "You lack permission to export this edocs information.");
            }

            if (version < 0)
            {
                return GenerateErrorResponse(serviceResult, "\"" + version + "\" is not a valid value for \"version\". Please provide a valid version number.");
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", principal.BrokerId),
                new SqlParameter("@Version", version),
                new SqlParameter("@Count", ListEDocsWithAuditHistoryMaxRecords)
            };

            XElement lendingQBLoanData;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "EDOCS_FetchBrokerAuditHistoryByVersion", parameters))
            {
                lendingQBLoanData = CreateSqlServiceEDocsAuditHistoryResponse(reader);
            }

            serviceResult.Status = ServiceResultStatus.OK;
            XElement xmlResponse = new XElement("response", XElement.Parse(serviceResult.ToResponse()), lendingQBLoanData);

            return xmlResponse.ToString();
        }

        /// <summary>
        /// Gets data representing the given EDocs formatted as an XML list. XML format is suitable for use in the SQL Transmission service.
        /// The SQL Service needs to be able to load deleted EDocs data and no permission currently exists for that.
        /// Otherwise, this could be public access and use an <see cref="EDocumentRepository"/> to enforce permissions.
        /// </summary>
        /// <param name="authTicket">The authentication ticket returned from <see cref="AuthService"/>.</param>
        /// <param name="secretKey">A secret key to make sure that the method is only called by LendingQB.</param>
        /// <param name="documentIds">A list of Document IDs to get the data for.</param>
        /// <param name="fieldsToRetrieve">A list of EDocs field names to retrieve for each document.</param>
        /// <returns>An XML-formatted string containing data about the documents for the given list of IDs.</returns>
        [WebMethod(Description = "LENDINGQB INTERNAL USE ONLY.")]
        public string ListEDocsByDocIds(string authTicket, string secretKey, Guid[] documentIds, EDocsFieldId[] fieldsToRetrieve)
        {
            ServiceResult serviceResult = new ServiceResult();
            AbstractUserPrincipal principal = this.GetPrincipalFromTicket(authTicket);
            CallVolume.IncreaseAndCheck(principal);

            if (principal == null)
            {
                return GenerateErrorResponse(serviceResult, "Invalid Credentials.");
            }

            if (!secretKey.Equals(ListEDocsWithAuditHistorySecretKey))
            {
                return GenerateErrorResponse(serviceResult, "Invalid Credentials.");
            }

            if (!(principal.IsInRole(ConstApp.ROLE_ADMINISTRATOR) && principal.HasPermission(Permission.CanViewEDocs)))
            {
                return GenerateErrorResponse(serviceResult, "You lack permission to export this edocs information.");
            }

            if (documentIds == null || !documentIds.Any())
            {
                return GenerateErrorResponse(serviceResult, "Please provide a list of Document IDs for export.");
            }

            XElement lendingQBLoanData = new XElement("LendingQBLoanData");
            
            foreach (Guid docId in documentIds)
            {
                lendingQBLoanData.Add(CreateSqlServiceEDocumentXml(EDocument.GetDocumentById(docId, principal.BrokerId), fieldsToRetrieve.Distinct()));
            }

            serviceResult.Status = ServiceResultStatus.OK;
            XElement xmlResponse = new XElement("response", XElement.Parse(serviceResult.ToResponse()), lendingQBLoanData);

            return xmlResponse.ToString();
        }

        /// <summary>
        /// Lists the EDocs on a loan file.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="sLNm">The LendingQB Loan Number for the loan file from which to get the list of EDocs.</param>
        /// <returns>An XML string representing the EDocs on the loan.</returns>
        /// <remarks>The loan number will be used as a unique id between LendingQB and the 3rd party system. Therefore, the user should proceed with extra care when changing the loan number in LendingQB.</remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod]
        public string ListEdocsByLoanNumber(string sTicket, string sLNm)
        {
            AbstractUserPrincipal principal = this.GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            if (principal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) == false)
            {
                return "<result status=\"Error\"><error>User does not have permission to view EDocs.</error></result>";
            }

            Guid loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (loanId == Guid.Empty)
            {
                return "<result status=\"Error\"><error>Could not locate loan number " + SecurityElement.Escape(sLNm) + "</error></result>";
            }

            return this.ListEdocsByLoanId(loanId, principal);
        }

        /// <summary>
        /// Lists the EDocs on a loan file.
        /// </summary>
        /// <param name="authTicket">The authorization ticket.</param>
        /// <param name="loanRefNum">The loan reference number.</param>
        /// <returns>The EDocs associated with the loan, or an error if the EDocs could not be retrieved.</returns>
        [WebMethod]
        public string ListEdocsByReferenceNumber(string authTicket, string loanRefNum)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicketAndValidateByRefNum(authTicket, loanRefNum, false);
            CallVolume.IncreaseAndCheck(principal);

            if (principal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) == false)
            {
                return "<result status=\"Error\"><error>User does not have permission to view EDocs.</error></result>";
            }

            Guid loanId = Tools.GetLoanIdByLoanReferenceNumber(principal.BrokerId, loanRefNum);

            if (loanId == Guid.Empty)
            {
                return "<result status=\"Error\"><error>Could not locate loan reference number " + SecurityElement.Escape(loanRefNum) + "</error></result>";
            }

            return this.ListEdocsByLoanId(loanId, principal);
        }

        /// <summary>
        /// Gives a list of recently modified EDocs.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="appCode">App code for the Edocs.</param>
        /// <param name="modifiedDate">This method returns EDocs modified after this date.</param>
        /// <returns>An XML string containing a list of recently modified EDocs.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod(Description = "Returns an XML document containing a list of recently modified EDocs. " +
        "This method is used if the client wants to poll the LO/PML system on a fixed interval (try to keep this interval big) " +
        "for recently modified files so that they can be imported into a 3rd-party LOS. " +
        "Contact support for valid appCode. " +
        "THIS METHOD ONLY RETURN EDOCS MODIFIED SINCE THE LAST MODIFIED DATE. THE METHOD WILL NOT RETURN DOCUMENTS MODIFIED PASSED 3 MONTHS.")]
        public string ListModifiedEDocsByAppCode(string sTicket, string appCode, string modifiedDate)
        {
            string errorMessage = string.Empty;
            if (string.IsNullOrEmpty(sTicket))
            {
                errorMessage += "[sTicket] cannot be empty. ";
            }

            if (string.IsNullOrEmpty(appCode))
            {
                errorMessage += "[appCode] cannot be empty. ";
            }

            if (string.IsNullOrEmpty(modifiedDate))
            {
                errorMessage += "[lastModifiedDate] cannot be empty. ";
            }

            if (errorMessage != string.Empty)
            {
                throw new WarningOnlyWebserviceException(new CBaseException(errorMessage, "Loan.asmx::ListModifiedEDocsByAppCode::" + errorMessage));
            }

            AbstractUserPrincipal principal = this.GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            Tools.LogInfo("Loan.asmx::ListModifiedEDocsByAppCode. Principal=" + principal.LoginNm + ". AppCode=[" + appCode + "]");

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", principal.BrokerId),
                new SqlParameter("@AppCode", appCode)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "VerifyAppCode", parameters))
            {
                if (!reader.HasRows)
                {
                    throw new AccessDenied(ErrorMessages.WebServices.InvalidAppCode);
                }
            }

            return this.ListModifiedEDocsImpl(new Guid(appCode), principal, modifiedDate);
        }

        /// <summary>
        /// Updates the metadata of a batch of EDocs with the supplied data.
        /// </summary>
        /// <param name="authTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="edocLoXml">The edoc LOXML representing the updates that the user wants to make to one or more EDocs.</param>
        /// <returns>A status response to let the user know if the update was successful and any error or warning messages.</returns>
        [WebMethod(Description = "Update the EDocs metadata of the documents described in the edocLoXml.")]
        public string UpdateEdocs(string authTicket, string edocLoXml)
        {
            return this.UpdateEdocsImpl(authTicket, edocLoXml, UpdateMetadataFromLoXml);
        }

        /// <summary>
        /// Deletes a batch of EDocs in the given LOXML list.
        /// </summary>
        /// <param name="authTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="edocLoXml">The edoc LOXML listing the ids of documents to delete.</param>
        /// <returns>A status response to let the user know if the deletion was successful and any error or warning messages.</returns>
        [WebMethod(Description = "Delete a list of EDocs, specified by docid.")]
        public string DeleteEdocs(string authTicket, string edocLoXml)
        {
            return this.UpdateEdocsImpl(
                authTicket, 
                edocLoXml, 
                (document, docUpdateXml, serviceResult, user) =>
                {
                    if (!DeleteEdoc(document))
                    {
                        serviceResult.AppendWarning($"You do not have permission to delete docid {document.DocumentId}.");
                    }
                });
        }

        /// <summary>
        /// Uploads Document content to EDocs, as if it were uploaded through the "Upload Documents" page.
        /// </summary>
        /// <param name="authTicket">Ticket returned by <see cref="AuthService"/>.</param>
        /// <param name="loanRefNum">The LendingQB Loan Reference Number.</param>
        /// <param name="appId">The application ID to associate the new document with.</param>
        /// <param name="fileName">The file name of the uploaded file. File extension will be used to determine what format the file should be understood as.</param>
        /// <param name="docTypeId">The document type identifier, which can be retrieved from the ListDocTypes method.</param>
        /// <param name="description">Public information related to the PDF document.</param>
        /// <param name="dataContent">The byte content of the document.</param>
        /// <returns>A webservice result string of the upload result.</returns>
        [WebMethod(Description = "Uploads a document to a loan file.  "
            + "[authTicket] Ticket returned by AuthService.asmx.  "
            + "[loanRefNum] The LendingQB Loan Reference Number.  "
            + "[appId] The application ID to associate the new document with.  "
            + "[fileName] The file name of the uploaded file. File extension will be used to determine what format the file should be understood as.  "
            + "[docTypeId] The document type identifier, which can be retrieved from the ListDocTypes method.  "
            + "[description] Public information related to the PDF document.  "
            + "[dataContent] The byte content of the document. Text files should be UTF-8 encoded before SOAP encoding.")]
        public string UploadDocumentContent(string authTicket, string loanRefNum, Guid appId, string fileName, int docTypeId, string description, byte[] dataContent)
        {
            BillingInfo genericVendorBilling;
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicketByRefNum(authTicket, loanRefNum, out genericVendorBilling);
            ServiceResult result = new ServiceResult();
            var loanId = Tools.GetLoanIdByLoanReferenceNumber(principal.BrokerId, loanRefNum);
            if (genericVendorBilling?.NeedsServiceType == true)
            {
                string developerMessage = "Invalid serviceType for Generic Framework Billing."
                    + $" ProviderID = {genericVendorBilling.ProviderID}, ServiceType = {genericVendorBilling.ServiceType}.";
                throw new WarningOnlyWebserviceException(new AccessDenied("Generic Documents aren't supported yet for Generic Vendors with Multiple services.", developerMessage));
            }

            var fileData = new FileData(fileName, dataContent);
            Guid docId;
            try
            {
                bool bypassWorkflow = genericVendorBilling != null;
                docId = EDocsUpload.UploadDoc(principal, fileData, loanId, appId, docTypeId, string.Empty, description, null, bypassWorkflow);
            }
            catch (CBaseException cbe)
            {
                result.AppendError(cbe.UserMessage);
                return result.ToResponse();
            }
            catch (LqbGrammar.Exceptions.DeveloperException de)
            {
                result.AppendError(de.Message);
                return result.ToResponse();
            }

            result.Status = ServiceResultStatus.OK;
            result.AdditionalProperties.Add("documentid", docId.ToString());
            return result.ToResponse();
        }

        /// <summary>
        /// Adds the PDF document to the specified loan file's EDocs repository.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="sLNm">LendingQB Loan Number.</param>
        /// <param name="documentType">The PDF file's document type name.</param>
        /// <param name="notes">Notes to attach to the PDF document.</param>
        /// <param name="sDataContent">The PDF content encoded in base 64.</param>
        /// <returns>Status string.
        /// If the operation saves without an error then following string will be returned: &lt;result status="OK" />
        /// If the save failed, the following string will be returned: &lt;result status="Error" >&lt;error>{ERROR MESSAGE HERE}&lt;/error>&lt;/result> .
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod(Description = "Add a PDF to a loan file. [sTicket] Ticket returned by AuthService.asmx.  [sLNm] The LendingQB Loan Name.  [documentType] The PDF document type name.  [notes] Notes related to the PDF document.  [sDataContent] The base 64 encoded PDF document.")]
        public string UploadPDFDocument(string sTicket, string sLNm, string documentType, string notes, string sDataContent)
        {
            this.IncreaseAndCheckCallVolume(sTicket, sLNm);
            return this.UploadPDFDocumentToAppInternal(sTicket, sLNm, Guid.Empty /*aAppId*/, documentType, notes, sDataContent, null);
        }

        /// <summary>
        /// Add a PDF to a loan file as a service.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="sLNm">LendingQB Loan Number.</param>
        /// <param name="documentType">The PDF file's document type name.</param>
        /// <param name="notes">Notes to attach to the PDF document.</param>
        /// <param name="sDataContent">The PDF content encoded in base 64.</param>
        /// <param name="serviceType">The type of service currently in use.</param>
        /// <returns>Status string.
        /// If the operation saves without an error then following string will be returned:
        /// &lt;result status="OK" />
        /// If the save failed, the following string will be returned:
        /// &lt;result status="Error" >&lt;error>{ERROR MESSAGE HERE}&lt;/error>&lt;/result> .</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod(Description = "Add a PDF to a loan file. [sTicket] Ticket returned by AuthService.asmx.  [sLNm] The LendingQB Loan Name.  [documentType] The PDF document type name.  [notes] Notes related to the PDF document.  [sDataContent] The base 64 encoded PDF document. [serviceType] The type of service currently in use.")]
        public string UploadPDFDocumentAsService(string sTicket, string sLNm, string documentType, string notes, string sDataContent, TypeOfService serviceType)
        {
            this.IncreaseAndCheckCallVolume(sTicket, sLNm);
            return this.UploadPDFDocumentToAppInternal(sTicket, sLNm, Guid.Empty /*aAppId*/, documentType, notes, sDataContent, serviceType);
        }

        /// <summary>
        /// Add a PDF to a loan file as a service.
        /// </summary>
        /// <param name="authTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="loanRefNum">LendingQB Loan Reference Number.</param>
        /// <param name="documentType">The PDF file's document type name.</param>
        /// <param name="notes">Notes to attach to the PDF document.</param>
        /// <param name="dataContent">The PDF content encoded in base 64.</param>
        /// <param name="serviceType">The type of service currently in use.</param>
        /// <returns>Status string.
        /// If the operation saves without an error then following string will be returned:
        /// &lt;result status="OK" />
        /// If the save failed, the following string will be returned:
        /// &lt;result status="Error" >&lt;error>{ERROR MESSAGE HERE}&lt;/error>&lt;/result> .</returns>
        [WebMethod(Description = "Add a PDF to a loan file. [authTicket] Ticket returned by AuthService.asmx.  [loanRefNum] The LendingQB Loan Reference Number.  [documentType] The PDF document type name.  [notes] Notes related to the PDF document.  [dataContent] The base 64 encoded PDF document. [serviceType] The type of service currently in use.")]
        public string UploadPDFDocumentByReferenceNumberAsService(string authTicket, string loanRefNum, string documentType, string notes, string dataContent, TypeOfService serviceType)
        {
            CallVolume.IncreaseAndCheck(AuthServiceHelper.GetPrincipalFromTicketAndValidateByRefNum(authTicket, loanRefNum, false));
            return this.UploadPDFDocumentToAppByReferenceNumber(authTicket, loanRefNum, appId: Guid.Empty, documentType: documentType, notes: notes, dataContent: dataContent, serviceType: serviceType);
        }

        /// <summary>
        /// Adds the PDF document to the EDocs repository of a specified loan on the specified app.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="sLNm">LendingQB Loan Number.</param>
        /// <param name="aAppId">The Application Id to contain the file.</param>
        /// <param name="documentType">The PDF file's document type name.</param>
        /// <param name="notes">Notes to attach to the PDF document.</param>
        /// <param name="sDataContent">The PDF content encoded in base 64.</param>
        /// <returns>Status string.
        /// If the operation saves without an error then following string will be returned:
        /// &lt;result status="OK" />
        /// If the save failed, the following string will be returned:
        /// &lt;result status="Error" >&lt;error>{ERROR MESSAGE HERE}&lt;/error>&lt;/result> .</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod(Description = "Add a PDF to a loan file. [sTicket] Ticket returned by AuthService.asmx.  [sLNm] The LendingQB Loan Name.  [aAppId] The Application Id to contain the file.  [documentType] The PDF document type name.  [notes] Notes related to the PDF document.  [sDataContent] The base 64 encoded PDF document.")]
        public string UploadPDFDocumentToApp(string sTicket, string sLNm, Guid aAppId, string documentType, string notes, string sDataContent)
        {
            this.IncreaseAndCheckCallVolume(sTicket, sLNm);
            return this.UploadPDFDocumentToAppInternal(sTicket, sLNm, aAppId, documentType, notes, sDataContent, null);
        }

        /// <summary>
        /// Adds the PDF document to the specified loan file's EDocs repository on the specified app as a service.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="sLNm">LendingQB Loan Number.</param>
        /// <param name="aAppId">The Application Id to contain the file.</param>
        /// <param name="documentType">The PDF file's document type name.</param>
        /// <param name="notes">Notes to attach to the PDF document.</param>
        /// <param name="sDataContent">The PDF content encoded in base 64.</param>
        /// <param name="serviceType">The type of service currently in use.</param>
        /// <returns>Status string.
        /// If the operation saves without an error then following string will be returned:
        /// &lt;result status="OK" />
        /// If the save failed, the following string will be returned:
        /// &lt;result status="Error" >&lt;error>{ERROR MESSAGE HERE}&lt;/error>&lt;/result> .</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Reviewed. Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod(Description = "Add a PDF to a loan file. [sTicket] Ticket returned by AuthService.asmx.  [sLNm] The LendingQB Loan Name.  [aAppId] The Application Id to contain the file.  [documentType] The PDF document type name.  [notes] Notes related to the PDF document.  [sDataContent] The base 64 encoded PDF document. [serviceType] The type of service currently in use.")]
        public string UploadPDFDocumentToAppAsService(string sTicket, string sLNm, Guid aAppId, string documentType, string notes, string sDataContent, TypeOfService serviceType)
        {
            this.IncreaseAndCheckCallVolume(sTicket, sLNm);
            return this.UploadPDFDocumentToAppInternal(sTicket, sLNm, aAppId, documentType, notes, sDataContent, serviceType);
        }

        /// <summary>
        /// Uploads a PDF into the document capture system.
        /// </summary>
        /// <param name="authTicket">
        /// Ticket returned by AuthService.asmx.
        /// </param>
        /// <param name="loanName">
        /// The LendingQB loan name.
        /// </param>
        /// <param name="applicationId">
        /// The ID of the application to associate with the document.
        /// </param>
        /// <param name="pdfContent">
        /// The base 64 encoded PDF document.
        /// </param>
        /// <param name="captureUsername">
        /// Optional. The username for the document capture system.
        /// </param>
        /// <param name="capturePassword">
        /// Optional. The password for the document capture system.
        /// </param>
        /// <returns>
        /// Status string.
        /// If the operation saves without an error then following string will be returned:
        /// &lt;result status="OK" />
        /// If the save failed, the following string will be returned:
        /// &lt;result status="Error" >&lt;error>{ERROR MESSAGE HERE}&lt;/error>&lt;/result> .
        /// </returns>
        [WebMethod(Description = "Uploads a PDF into the document capture system. [authTicket] Ticket returned by AuthService.asmx. [loanName] The LendingQB Loan Name. [applicationId] The ID of the application to associate with the document. [pdfContent] The base 64 encoded PDF document. [captureUsername] Optional. The username for the document capture system. [capturePassword] Optional. The password for the document capture system.")]
        public string UploadPDFtoDocumentCapture(string authTicket, string loanName, Guid applicationId, string pdfContent, string captureUsername, string capturePassword)
        {
            this.IncreaseAndCheckCallVolume(authTicket, loanName);

            Tools.LogInfo($"EDocsService.asmx::UploadPDFtoDocumentCapture loanName={loanName} applicationId={applicationId}");
            BillingInfo genericFrameworkBillingInfo;
            var principal = AuthServiceHelper.GetPrincipalFromTicket(authTicket, loanName, out genericFrameworkBillingInfo);

            if (principal == null)
            {
                return WebServiceUtilities.GenerateErrorResponseXml(ErrorMessages.WebServices.InvalidAuthTicket);
            }

            var loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, loanName);
            if (loanId == Guid.Empty)
            {
                return WebServiceUtilities.GenerateErrorResponseXml(ErrorMessages.WebServices.InvalidLoanNumber);
            }

            var workflowCheck = Tools.IsWorkflowOperationAuthorized(principal, loanId, WorkflowOperations.UseDocumentCapture);
            if (!workflowCheck.Item1)
            {
                var errorMessage = workflowCheck.Item2;
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "User does not have permission to use Document Capture.";
                }

                return WebServiceUtilities.GenerateErrorResponseXml(errorMessage);
            }

            if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                return WebServiceUtilities.GenerateErrorResponseXml(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            if (!principal.HasPermission(Permission.CanViewEDocs))
            {
                return WebServiceUtilities.GenerateErrorResponseXml("User does not have permission to view EDocs.");
            }

            Tools.ScanForVirus(pdfContent);

            var response = new ServiceResult();

            var parameters = KtaDocumentUploadParameters.FromUserPrincipal(principal);
            parameters.LoanId = loanId;
            parameters.ApplicationId = applicationId;
            parameters.DocumentSource = E_EDocumentSource.UserUpload;
            parameters.PdfBytes = Convert.FromBase64String(pdfContent);

            var submissionResult = KtaUtilities.UploadDocument(parameters, captureUsername, capturePassword);

            if (!submissionResult.Success)
            {
                response.AppendError(submissionResult.ErrorMessage);
            }

            return response.ToResponse();
        }

        /// <summary>
        /// Creates "LendingQBLoanData" XML response for returning from the <see cref="ListEDocsAuditHistory(string, string, int)"/> method.
        /// </summary>
        /// <param name="sqlReader">The SQL reader from which to load EDocs data.</param>
        /// <returns>A response XElement for serializing into a response.</returns>
        private static XElement CreateSqlServiceEDocsAuditHistoryResponse(DbDataReader sqlReader)
        {
            XElement lendingQBLoanData = new XElement("LendingQBLoanData");
            while (sqlReader.Read())
            {
                XElement edoc = new XElement("eDoc");
                foreach (string fieldId in edocsAuditHistoryColumnIds)
                {
                    edoc.Add(CreateXmlField(fieldId.ToString(), sqlReader[fieldId]));
                }

                lendingQBLoanData.Add(edoc);
            }

            return lendingQBLoanData;
        }

        /// <summary>
        /// Creates an "dDoc" XML element for transmitting with SQL Transmission service.
        /// </summary>
        /// <param name="document">The <see cref="EDocument"/> for which the XML will be built.</param>
        /// <param name="fieldsToRetrieve">A list of fields to retrieve from the EDocument.</param>
        /// <returns>An XElement representing the EDoc.</returns>
        private static XElement CreateSqlServiceEDocumentXml(EDocument document, IEnumerable<EDocsFieldId> fieldsToRetrieve)
        {
            XElement documentElement = new XElement("eDoc");

            foreach (EDocsFieldId fieldId in fieldsToRetrieve)
            {
                switch (fieldId)
                {
                    case EDocsFieldId.DocumentId:
                        documentElement.Add(CreateXmlField(EDocsFieldId.DocumentId.ToString(), document.DocumentId));
                        break;

                    case EDocsFieldId.sLId:
                        documentElement.Add(CreateXmlField(EDocsFieldId.sLId.ToString(), document.LoanId));
                        break;

                    case EDocsFieldId.aAppId:
                        documentElement.Add(CreateXmlField(EDocsFieldId.aAppId.ToString(), document.AppId));
                        break;

                    case EDocsFieldId.IsValid:
                        documentElement.Add(CreateXmlField(EDocsFieldId.IsValid.ToString(), document.IsValid));
                        break;

                    case EDocsFieldId.FolderId:
                        documentElement.Add(CreateXmlField(EDocsFieldId.FolderId.ToString(), document.FolderId));
                        break;

                    case EDocsFieldId.FolderName:
                        documentElement.Add(CreateXmlField(EDocsFieldId.FolderName.ToString(), document.Folder.FolderNm));
                        break;

                    case EDocsFieldId.DocTypeId:
                        documentElement.Add(CreateXmlField(EDocsFieldId.DocTypeId.ToString(), document.DocumentTypeId));
                        break;

                    case EDocsFieldId.DocTypeName:
                        documentElement.Add(CreateXmlField(EDocsFieldId.DocTypeName.ToString(), document.DocTypeName));
                        break;

                    case EDocsFieldId.DocTypeIsValid:
                        documentElement.Add(CreateXmlField(EDocsFieldId.DocTypeIsValid.ToString(), !document.RequiresDocTypeChange));
                        break;

                    case EDocsFieldId.PublicDescription:
                        documentElement.Add(CreateXmlField(EDocsFieldId.PublicDescription.ToString(), document.PublicDescription));
                        break;

                    case EDocsFieldId.InternalDescription:
                        documentElement.Add(CreateXmlField(EDocsFieldId.InternalDescription.ToString(), document.InternalDescription));
                        break;

                    case EDocsFieldId.NumPages:
                        documentElement.Add(CreateXmlField(EDocsFieldId.NumPages.ToString(), document.PageCount));
                        break;

                    case EDocsFieldId.CreatedDate:
                        documentElement.Add(CreateXmlField(EDocsFieldId.CreatedDate.ToString(), document.CreatedDate));
                        break;

                    case EDocsFieldId.LastModifiedDate:
                        documentElement.Add(CreateXmlField(EDocsFieldId.LastModifiedDate.ToString(), document.LastModifiedDate));
                        break;

                    case EDocsFieldId.Status:
                        documentElement.Add(CreateXmlField(EDocsFieldId.Status.ToString(), document.DocStatus.ToString("D")));
                        break;

                    case EDocsFieldId.StatusDescription:
                        documentElement.Add(CreateXmlField(EDocsFieldId.StatusDescription.ToString(), document.StatusDescription));
                        break;

                    default:
                        throw new UnhandledEnumException(fieldId);
                }
            }

            return documentElement;
        }

        /// <summary>
        /// Creates a field element for string values, removing invalid characters from <paramref name="fieldValue"/>.
        /// </summary>
        /// <param name="fieldId">Identifier (name) for the field.</param>
        /// <param name="fieldValue">The value of the field.</param>
        /// <returns>A "field" XElement containing the field ID and value.</returns>
        private static XElement CreateXmlField(string fieldId, string fieldValue)
        {
            return CreateXmlField(fieldId, (object)Tools.SantizeXmlString(fieldValue));
        }

        /// <summary>
        /// Creates a "field" element for the xml response to the SQL Transmission service.
        /// </summary>
        /// <param name="fieldId">Identifier (name) for the field.</param>
        /// <param name="fieldValue">The value of the field.</param>
        /// <returns>A "field" XElement containing the field ID and value.</returns>
        private static XElement CreateXmlField(string fieldId, object fieldValue)
        {
            return new XElement("field", new XAttribute("id", fieldId), fieldValue);
        }

        /// <summary>
        /// Generates an error response using a ServiceResult object.
        /// </summary>
        /// <param name="serviceResult">The ServiceResult object to use for generating the error.</param>
        /// <param name="errorMessage">An error message to append to the ServiceResult.</param>
        /// <returns>An xml-formatted string with the error response from the ServiceResult.</returns>
        private static string GenerateErrorResponse(ServiceResult serviceResult, string errorMessage)
        {
            serviceResult.AppendError(errorMessage);
            serviceResult.Status = ServiceResultStatus.Error;
            return serviceResult.ToResponse();
        }

        /// <summary>
        /// Applies the <paramref name="serviceType"/> to the <paramref name="genericFrameworkBillingInfo"/>, if needed.
        /// </summary>
        /// <param name="genericFrameworkBillingInfo">The Generic Framework billing information.</param>
        /// <param name="serviceType">The service type, or null if unspecified.</param>
        /// <exception cref="AccessDenied"><paramref name="genericFrameworkBillingInfo"/> does not have a valid service type, and <paramref name="serviceType"/> could not be applied as a replacement.</exception>
        private static void ResolveGenericFrameworkServiceType(BillingInfo genericFrameworkBillingInfo, TypeOfService? serviceType)
        {
            if (genericFrameworkBillingInfo != null && genericFrameworkBillingInfo.NeedsServiceType)
            {
                string userMessage = null;
                if (serviceType.HasValue)
                {
                    genericFrameworkBillingInfo.ServiceType = serviceType.Value;
                    if (genericFrameworkBillingInfo.NeedsServiceType)
                    {
                        userMessage = "Invalid serviceType.";
                    }
                }
                else
                {
                    userMessage = "Please use UploadPDFDocumentAsService or UploadPDFDocumentToAppAsService to specify the service in use.";
                }

                if (userMessage != null)
                {
                    string developerMessage = "Invalid serviceType for Generic Framework Billing."
                    + " ProviderID = " + genericFrameworkBillingInfo.ProviderID
                    + ", ServiceType " + (serviceType.HasValue ? "= " + serviceType.Value.ToString() : "was null");
                    throw new WarningOnlyWebserviceException(new AccessDenied(userMessage, developerMessage));
                }
            }
        }

        /// <summary>
        /// Validates common Web Service requirements and gets the content path for the given generic document.
        /// </summary>
        /// <param name="ticket">The authentication ticket.</param>
        /// <param name="docId">The document to get generic content path for.</param>
        /// <returns>The content path of the doc contents.</returns>
        private static string ValidateWebServiceAndGetGenericDocPath(string ticket, Guid docId)
        {
            string ticketLoanReferenceNumber;
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalAndsLIdFromTicket(ticket, out ticketLoanReferenceNumber);
            CallVolume.IncreaseAndCheck(principal);

            if (!principal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs))
            {
                throw new WarningOnlyWebserviceException(new AccessDenied(ErrorMessages.EDocs.InsufficientReadPermission));
            }

            EDocumentRepository repository = EDocumentRepository.GetUserRepository(principal);
            GenericEDocument eDocs = null;
            if (string.IsNullOrEmpty(ticketLoanReferenceNumber))
            {
                eDocs = repository.GetGenericDocumentById(docId);
            }
            else
            {
                Guid genericFrameworksLId = Tools.GetLoanIdByLoanReferenceNumber(principal.BrokerId, ticketLoanReferenceNumber);
                eDocs = repository.GetGenericDocumentsByLoanId(genericFrameworksLId).FirstOrDefault(eDoc => eDoc.DocumentId == docId);
                if (eDocs == null)
                {
                    throw new WarningOnlyWebserviceException(new NotFoundException(ErrorMessages.EDocs.DocumentNotFoundException));
                }
            }

            return eDocs.GetContentPath();
        }

        /// <summary>
        /// Deletes the specified EDoc.
        /// </summary>
        /// <param name="document">The document to delete.</param>
        /// <returns>A boolean value representing whether the deletion was successful.</returns>
        private static bool DeleteEdoc(EDocument document)
        {
            var repository = EDocumentRepository.GetUserRepository();
            try
            {
                repository.DeleteDocument(document.DocumentId, $"Deleted by user via Web Services.");
                return true;
            }
            catch (AccessDenied)
            {
                return false;
            }
        }

        /// <summary>
        /// Updates this document with information from the provided updating LOXml, checking whether the given user has permission to update each field.
        /// Uses <see cref="EDocumentRepository"/>, which gets permissions from <see cref="System.Threading.Thread.CurrentPrincipal"/>
        /// </summary>
        /// <param name="documentToUpdate">The document to update from the input xml.</param>
        /// <param name="docUpdateXml">The XML element representing the update request for the doc.</param>
        /// <param name="serviceResult">The serviceResult object to use for recording warnings and errors to return to the caller.</param>
        /// <param name="user">
        /// The user requesting the update. This user's permissions will be checked for updates that require additional permissions. 
        /// It is assumed at this point that the user has permission to view and edit eDocs in general. Request-specific permissions will be checked.
        /// </param>
        private static void UpdateMetadataFromLoXml(EDocument documentToUpdate, XElement docUpdateXml, ServiceResult serviceResult, AbstractUserPrincipal user)
        {
            int? newFolderId = null;
            IEnumerable<EDocumentFolder> validDocFolders =
                EDocumentFolder.GetFoldersInBroker(user.BrokerId);
            string folderName = docUpdateXml.Attribute("folder_name")?.Value;
            if (!string.IsNullOrEmpty(folderName))
            {
                EDocumentFolder namedFolder = validDocFolders
                    .Where(folder => folder.CanRead() && folderName.Equals(
                        folder.FolderNm,
                        StringComparison.OrdinalIgnoreCase))
                    .FirstOrDefault();
                if (namedFolder != null)
                {
                    // FolderId can't be set directly, so instead use it to filter the new doc type
                    newFolderId = namedFolder.FolderId;
                }
            }

            IEnumerable<DocType> validDocTypes =
                EDocumentDocType.GetDocTypesByBroker(user.BrokerId, true);
            string docTypeName = docUpdateXml.Attribute("doc_type")?.Value;
            if (!string.IsNullOrEmpty(docTypeName))
            {
                DocType namedDocType = validDocTypes
                    .Where(
                        docType =>
                            docType.DocTypeName.Equals(
                                docTypeName,
                                StringComparison.OrdinalIgnoreCase)
                            && (newFolderId == null || docType.Folder.FolderId == newFolderId))
                    .FirstOrDefault();
                if (namedDocType != null)
                {
                    documentToUpdate.DocumentTypeId = namedDocType.Id;
                }
                else
                {
                    string folderNameMessage = string.IsNullOrEmpty(folderName) ? string.Empty : $" in folder {folderName}";
                    serviceResult.AppendWarning($"Could not find doc type \"{docTypeName}\"{folderNameMessage}.");
                }
            }

            // Specified ID should override specified document type name.
            var docTypeIdAttribute = docUpdateXml.Attribute("doc_type_id");
            int docTypeId;
            if (docTypeIdAttribute != null 
                && int.TryParse(docTypeIdAttribute.Value, out docTypeId)
                && validDocTypes.Any(type => docTypeId == type.Id))
            {
                documentToUpdate.DocumentTypeId = docTypeId;
            }
            else if (docTypeIdAttribute != null)
            {
                serviceResult.AppendWarning($"Could not find document type id \"{docTypeIdAttribute.Value}\".");
            }

            string docStatusString = docUpdateXml.Attribute("doc_status")?.Value;
            E_EDocStatus docStatus;
            if (Enum.TryParse(docStatusString, ignoreCase: true, result: out docStatus) && Enum.IsDefined(typeof(E_EDocStatus), docStatus))
            {
                if (UserHasPermissionToSetStatus(user, docStatus))
                {
                    documentToUpdate.DocStatus = docStatus;
                }
                else
                {
                    serviceResult.AppendWarning($"You do not have permission to set the \"{docStatus:D}\" status.");
                }
            }
            else if (docUpdateXml.Attribute("doc_status") != null)
            {
                serviceResult.AppendWarning($"Could not parse document status \"{docStatusString}\".");
            }

            string statusDescription = docUpdateXml.Attribute("doc_status_description")?.Value;
            if (!string.IsNullOrEmpty(statusDescription))
            {
                // DB field is varchar(200). Will be truncated by SqlParameter.
                WarnTruncation(statusDescription, "doc_status_description", 200, serviceResult);

                documentToUpdate.StatusDescription = statusDescription;
            }

            string description = docUpdateXml.Attribute("description")?.Value;
            if (!string.IsNullOrEmpty(description))
            {
                WarnTruncation(description, "description", 200, serviceResult);

                documentToUpdate.PublicDescription = description;
            }

            string internalDescription = docUpdateXml.Attribute("internal_description")?.Value;
            if (!string.IsNullOrEmpty(internalDescription)
                && user.HasPermission(Permission.CanViewEDocsInternalNotes) && user.HasPermission(Permission.CanEditEDocsInternalNotes))
            {
                WarnTruncation(internalDescription, "internal_description", 200, serviceResult);

                documentToUpdate.InternalDescription = internalDescription;
            }

            // Has a public setter, but not sure if this is viewable anywhere in the UI.
            string comments = docUpdateXml.Attribute("comments")?.Value;
            if (!string.IsNullOrEmpty(comments))
            {
                WarnTruncation(comments, "comments", 500, serviceResult);

                documentToUpdate.Comments = comments;
            }

            string hideFromPmlString = docUpdateXml.Attribute("hide_from_pml_users")?.Value;
            bool hideFromPml;
            if (bool.TryParse(hideFromPmlString, out hideFromPml) && user.HasPermission(Permission.AllowHideEDocsFromPML))
            {
                documentToUpdate.HideFromPMLUsers = hideFromPml;
            }

            EDocumentRepository.GetUserRepository().Save(documentToUpdate);
        }

        /// <summary>
        /// Warn the user that a field will be truncated.
        /// </summary>
        /// <param name="value">The value that might be truncated.</param>
        /// <param name="name">The field name to display to the user.</param>
        /// <param name="maxLength">The maximum length of the field.</param>
        /// <param name="serviceResult">The service result object to add warnings to.</param>
        private static void WarnTruncation(string value, string name, int maxLength, ServiceResult serviceResult)
        {
            if (value.Length > maxLength)
            {
                serviceResult.AppendWarning($"Value \"{name}\" is longer than max of {maxLength} characters. Will be truncated to first {maxLength} characters.");
            }
        }

        /// <summary>
        /// Determines whether the given user principal has permission to set the given status.
        /// </summary>
        /// <param name="user">The user to be checked for permission.</param>
        /// <param name="docStatus">The doc status for which permission is being checked.</param>
        /// <returns>Whether the user has permission to set the status.</returns>
        private static bool UserHasPermissionToSetStatus(AbstractUserPrincipal user, E_EDocStatus docStatus)
        {
            if (!(user.HasPermission(Permission.AllowScreeningEDocs)
                || user.HasPermission(Permission.AllowApprovingRejectingEDocs)))
            {
                return false;
            }

            switch (docStatus)
            {
                case E_EDocStatus.Obsolete:
                case E_EDocStatus.Screened:
                    return user.HasPermission(Permission.AllowScreeningEDocs);
                case E_EDocStatus.Approved:
                case E_EDocStatus.Rejected:
                    return user.HasPermission(Permission.AllowApprovingRejectingEDocs);
                case E_EDocStatus.Blank:
                    return true;
                default:
                    throw new UnhandledEnumException(docStatus);
            }
        }

        /// <summary>
        /// Updates each edoc in the LOXML list with the given update action.
        /// </summary>
        /// <param name="authTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="edocLoXml">The edoc LOXML listing the ids of documents to update.</param>
        /// <param name="updateAction">The action to execute for each EDoc, once the document has been loaded and permission-checked.</param>
        /// <returns>A status response to let the user know if the operation was successful and any error or warning messages.</returns>
        private string UpdateEdocsImpl(string authTicket, string edocLoXml, Action<EDocument, XElement, ServiceResult, AbstractUserPrincipal> updateAction)
        {
            var serviceResult = new ServiceResult();
            XElement document = XElement.Parse(edocLoXml);
            XElement list = document.Element("list");
            IEnumerable<XElement> docs = list?.Elements("edoc") ?? document.Elements("edoc");
            if (!docs.Any(doc => doc != null))
            {
                serviceResult.AppendWarning("No 'edoc' elements found in the LOXML.");
                serviceResult.Status = ServiceResultStatus.OKWithWarning;
            }

            AbstractUserPrincipal principal;
            try
            {
                principal = this.GetPrincipalFromTicket(authTicket);
            }
            catch (RestrictedLoanAccessDenied)
            {
                // For Generic Framework vendors, we need to verify that all the doc IDs match their restricted loan file.
                principal = AuthServiceHelper.GetPrincipalFromTicket(authTicket, null, true);
                IEnumerable<Guid> docIds = docs.Select(doc => doc.Attribute("docid")?.Value.ToNullable<Guid>(Guid.TryParse)).Where(id => id.HasValue).Select(id => id.Value);
                Guid loanId;
                var loanDocIds = EDocument.GetDocumentIdsOnSameLoan(docIds.First(), principal.BrokerId, out loanId);

                foreach (Guid documentId in docIds)
                {
                    if (!loanDocIds.Contains(documentId))
                    {
                        throw new WarningOnlyWebserviceException(new AccessDenied("Document access denied."));
                    }
                }

                principal = AuthServiceHelper.GetPrincipalFromTicketAndValidateByRefNum(authTicket, Tools.GetLoanRefNmByLoanId(principal.BrokerId, loanId), ignoreTicketLoanRestriction: false);
            }

            CallVolume.IncreaseAndCheck(principal);
            if (!principal.HasPermission(Permission.CanViewEDocs) || !principal.HasPermission(Permission.CanEditEDocs))
            {
                serviceResult.AppendError("Permission denied.");
                serviceResult.Status = ServiceResultStatus.Error;
                return serviceResult.ToResponse();
            }

            // EDocumentRepository checks permissions automatically.
            var documentRepository = EDocumentRepository.GetUserRepository();

            foreach (XElement docUpdateXml in docs)
            {
                if (docUpdateXml == null)
                {
                    continue;
                }

                var docIdAttribute = docUpdateXml.Attribute("docid");
                Guid docId;
                if (docIdAttribute == null)
                {
                    serviceResult.AppendWarning($"Did not find \"docid\" attribute on \"edoc\" element.");
                    continue;
                }

                if (!Guid.TryParse(docIdAttribute.Value, out docId))
                {
                    serviceResult.AppendWarning($"Could not parse docid \"{docIdAttribute.Value}\".");
                    continue;
                }

                EDocument documentToUpdate = null;
                try
                {
                    documentToUpdate = documentRepository.GetDocumentById(docId);
                }
                catch (NotFoundException)
                {
                    serviceResult.AppendWarning($"EDocument with docid \"{docId}\" was not found.");
                    continue;
                }
                catch (AccessDenied)
                {
                    serviceResult.AppendWarning($"Access to docid \"{docId}\" was denied.");
                    continue;
                }

                if (!documentRepository.CanUpdateDocument(documentToUpdate))
                {
                    serviceResult.AppendWarning($"You do not have permission to edit docid \"{docId}\".");
                    continue;
                }

                updateAction(documentToUpdate, docUpdateXml, serviceResult, principal);
            }

            return serviceResult.ToResponse();
        }

        /// <summary>
        /// Returns the EDocs associated with a loan ID.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="principal">The requesting principal.</param>
        /// <returns>The EDocs associated with the loan, or an error if the EDocs could not be retrieved.</returns>
        private string ListEdocsByLoanId(Guid loanId, AbstractUserPrincipal principal)
        {
            try
            {
                AuthServiceHelper.CheckLoanAuthorization(loanId, WorkflowOperations.ReadLoan);
            }
            catch (CBaseException)
            {
                return "<result status=\"Error\"><error>User does not have permission to read loan.</error></result>";
            }

            if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                return $"<result status=\"Error\"><error>{ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan}</error></result>";
            }

            try
            {
                Dictionary<int, string> foldersDictionary = new Dictionary<int, string>();

                EDocumentRepository repository = EDocumentRepository.GetUserRepository();

                var list = repository.GetDocumentsByLoanId(loanId);
                XDocument xmlRoot = new XDocument();
                XElement root = new XElement("list");
                xmlRoot.Add(root);
                foreach (var document in list)
                {
                    string folderName;
                    if (foldersDictionary.TryGetValue(document.FolderId, out folderName) == false)
                    {
                        folderName = document.Folder.FolderNm;
                        foldersDictionary.Add(document.FolderId, folderName);
                    }

                    var edocElement = new XElement(
                            "edoc",
                            new XAttribute("docid", document.DocumentId.ToString()),
                            new XAttribute("doc_type", document.DocTypeName),
                            new XAttribute("folder_name", folderName),
                            new XAttribute("application", document.AppName),
                            new XAttribute("description", document.PublicDescription),
                            new XAttribute("doc_type_id", document.DocumentTypeId.ToString()),
                            new XAttribute("doc_status", document.DocStatus.ToString("D")),
                            new XAttribute("created_date", document.CreatedDate));

                    if (document.GenericFileType != null)
                    {
                        edocElement.SetAttributeValue("non_pdf_content_type", document.GenericFileType.Value.ToString("F"));
                    }

                    if (principal.HasPermission(Permission.CanViewEDocsInternalNotes))
                    {
                        edocElement.SetAttributeValue("internal_comments", document.InternalDescription);
                    }

                    root.Add(edocElement);
                }

                return "<result status=\"OK\">" + xmlRoot.ToString() + "</result>";
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                return "<result status=\"Error\"><error>" + SecurityElement.Escape(exc.UserMessage) + "</error></result>";
            }
        }

        /// <summary>
        /// Generates an ERROR XML response.
        /// </summary>
        /// <param name="message">The error message to include in the XML.</param>
        /// <returns>A full LOXML response containing an error message.</returns>
        private string GenerateErrorResponseXml(string message)
        {
            XElement resultElement = new XElement("result", new XAttribute("status", "ERROR"));
            XElement messageElement = new XElement("message", SecurityElement.Escape(message));
            resultElement.Add(message);
            return WebServiceUtilities.GenerateResponseXml(resultElement);
        }

        /// <summary>
        /// Gets the principal from the ticket, checking if the ticket has access to the specified loan.
        /// </summary>
        /// <param name="ticket">The ticket of the user.</param>
        /// <param name="loanNumber">The loan number being accessed; used for tickets with a restriction to a loan.</param>
        /// <returns>The principal represented by the ticket.</returns>
        private AbstractUserPrincipal GetPrincipalFromTicket(string ticket, string loanNumber = null)
        {
            return AuthServiceHelper.GetPrincipalFromTicket(ticket, loanNumber);
        }

        /// <summary>
        /// Increases and checks the call volume for the broker associated with the user principal.
        /// Throws an exception if the call volume is too high today.
        /// </summary>
        /// <param name="authTicket">An authentication ticket from <see cref="AuthService"/>.</param>
        /// <param name="loanNumber">LendingQB loan number.</param>
        private void IncreaseAndCheckCallVolume(string authTicket, string loanNumber)
        {
            AbstractUserPrincipal principal = this.GetPrincipalFromTicket(authTicket, loanNumber);
            CallVolume.IncreaseAndCheck(principal);
        }

        /// <summary>
        /// Gives a list of recently modified EDocs.
        /// </summary>
        /// <param name="appCode">App code for the Edocs.</param>
        /// <param name="principal">The user principal who made the request.</param>
        /// <param name="modifiedDateString">This method returns EDocs modified after this date.</param>
        /// <returns>An XML string containing a list of recently modified EDocs.</returns>
        private string ListModifiedEDocsImpl(Guid appCode, AbstractUserPrincipal principal, string modifiedDateString)
        {
            StringBuilder debugString = new StringBuilder();
            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                DateTime modifiedDate;

                if (!DateTime.TryParse(modifiedDateString, out modifiedDate))  
                {
                    return new ErrorServiceResult("Invalid modifiedDateString.").ToResponse();
                }

                if (modifiedDate.AddMonths(3) < DateTime.Today)
                {
                    modifiedDate = DateTime.Today.AddMonths(-3);
                }

                XmlDocument doc = new XmlDocument();
                XmlElement rootElement = doc.CreateElement("modified_edocs");
                doc.AppendChild(rootElement);

                EDocumentRepository repo = EDocumentRepository.GetUserRepository();

                foreach (var modifiedDoc in repo.GetRecentlyModifiedDocs(modifiedDate))
                {
                    XmlElement el = doc.CreateElement("eDoc");
                    el.SetAttribute("LoanNumber", modifiedDoc.LoanName);
                    el.SetAttribute("FolderName", modifiedDoc.FolderName);
                    el.SetAttribute("DocTypeName", modifiedDoc.DocTypeName);
                    el.SetAttribute("BorrowerName", modifiedDoc.BorrowerFullName);
                    el.SetAttribute("Description", modifiedDoc.Description);
                    el.SetAttribute("Comment", modifiedDoc.Comments);
                    el.SetAttribute("NumPages", modifiedDoc.Pages.ToString());
                    el.SetAttribute("LastModifiedDate", modifiedDoc.LastModifiedDate.ToString("MM/dd/yyyy HH:mm:ss"));
                    el.SetAttribute("DocumentId", modifiedDoc.DocumentId.ToString());

                    rootElement.AppendChild(el);
                }

                rootElement.SetAttribute("last_modified_date", modifiedDate.ToString("MM/dd/yyyy HH:mm:ss"));
                return doc.InnerXml;
            }
            finally
            {
                sw.Stop();
                debugString.AppendLine("Executed in " + sw.ElapsedMilliseconds + "ms.");
                Tools.LogInfo(debugString.ToString());
            }
        }

        /// <summary>
        /// Adds the PDF document to the specified loan file's EDocs repository on the specified app as a service.
        /// </summary>
        /// <param name="authTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="loanNumber">LendingQB Loan Number.</param>
        /// <param name="appId">The Application Id to contain the file.</param>
        /// <param name="documentType">The PDF file's document type name.</param>
        /// <param name="notes">Notes to attach to the PDF document.</param>
        /// <param name="dataContent">The PDF content encoded in base 64.</param>
        /// <param name="serviceType">The type of service currently in use.</param>
        /// <returns>Status string.
        /// If the operation saves without an error then following string will be returned:
        /// &lt;result status="OK" />
        /// If the save failed, the following string will be returned:
        /// &lt;result status="Error" >&lt;error>{ERROR MESSAGE HERE}&lt;/error>&lt;/result> .</returns>
        private string UploadPDFDocumentToAppInternal(string authTicket, string loanNumber, Guid appId, string documentType, string notes, string dataContent, TypeOfService? serviceType)
        {
            Tools.LogInfo("Loan.asmx::UploadPDFDocument sLNm=" + loanNumber + Environment.NewLine + dataContent);
            BillingInfo genericFrameworkBillingInfo;
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(authTicket, loanNumber, out genericFrameworkBillingInfo);

            Guid loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, loanNumber);
            if (loanId == Guid.Empty)
            {
                throw new WarningOnlyWebserviceException(new IntegrationInvalidLoanNumberException(loanNumber));
            }

            if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                throw new WarningOnlyWebserviceException(new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan));
            }

            if (principal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) == false)
            {
                throw new WarningOnlyWebserviceException(new AccessDenied("User does not have permission to view EDocs."));
            }

            return this.UploadPDFDocumentToAppInternal(principal, loanId, genericFrameworkBillingInfo, appId, documentType, notes, dataContent, serviceType);
        }

        /// <summary>
        /// Adds the PDF document to the specified loan file's EDocs repository on the specified app as a service.
        /// </summary>
        /// <param name="authTicket">Authentication string returned by <see cref="AuthService.asmx"/>.</param>
        /// <param name="loanRefNum">LendingQB Loan Reference Number.</param>
        /// <param name="appId">The Application Id to contain the file.</param>
        /// <param name="documentType">The PDF file's document type name.</param>
        /// <param name="notes">Notes to attach to the PDF document.</param>
        /// <param name="dataContent">The PDF content encoded in base 64.</param>
        /// <param name="serviceType">The type of service currently in use.</param>
        /// <returns>Status string.
        /// If the operation saves without an error then following string will be returned:
        /// &lt;result status="OK" />
        /// If the save failed, the following string will be returned:
        /// &lt;result status="Error" >&lt;error>{ERROR MESSAGE HERE}&lt;/error>&lt;/result> .</returns>
        private string UploadPDFDocumentToAppByReferenceNumber(string authTicket, string loanRefNum, Guid appId, string documentType, string notes, string dataContent, TypeOfService? serviceType)
        {
            Tools.LogInfo("Loan.asmx::UploadPDFDocument refNum=" + loanRefNum + Environment.NewLine + dataContent);

            BillingInfo genericFrameworkBillingInfo;
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicketAndValidateByRefNum(authTicket, loanRefNum, false, out genericFrameworkBillingInfo);

            Guid loanId = Tools.GetLoanIdByLoanReferenceNumber(principal.BrokerId, loanRefNum);
            if (loanId == Guid.Empty)
            {
                throw new WarningOnlyWebserviceException(new IntegrationInvalidLoanReferenceNumberException(loanRefNum));
            }

            if (principal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) == false)
            {
                throw new WarningOnlyWebserviceException(new AccessDenied("User does not have permission to view EDocs."));
            }

            return this.UploadPDFDocumentToAppInternal(principal, loanId, genericFrameworkBillingInfo, appId, documentType, notes, dataContent, serviceType);
        }

        /// <summary>
        /// Adds the PDF document to the specified loan file's EDocs repository on the specified app as a service.
        /// </summary>
        /// <param name="principal">The UserPrincipal logging in to the service.</param>
        /// <param name="loanId">LendingQB Loan ID (sLId).</param>
        /// <param name="genericFrameworkBillingInfo">Billing info for generic framework vendors, if applicable.</param>
        /// <param name="appId">The Application Id to contain the file.</param>
        /// <param name="documentType">The PDF file's document type name.</param>
        /// <param name="notes">Notes to attach to the PDF document.</param>
        /// <param name="dataContent">The PDF content encoded in base 64.</param>
        /// <param name="serviceType">The type of service currently in use.</param>
        /// <returns>Status string.
        /// If the operation saves without an error then following string will be returned:
        /// &lt;result status="OK" />
        /// If the save failed, the following string will be returned:
        /// &lt;result status="Error" >&lt;error>{ERROR MESSAGE HERE}&lt;/error>&lt;/result> .</returns>
        private string UploadPDFDocumentToAppInternal(AbstractUserPrincipal principal, Guid loanId, BillingInfo genericFrameworkBillingInfo, Guid appId, string documentType, string notes, string dataContent, TypeOfService? serviceType)
        {
            try
            {
                ResolveGenericFrameworkServiceType(genericFrameworkBillingInfo, serviceType);
                bool didFindDocType;
                Guid docId = PdfImporter.Import(principal, loanId, appId, dataContent, documentType, notes, genericFrameworkBillingInfo, useUnclassifiedFallback: true, didFindDocType: out didFindDocType);

                if (!didFindDocType)
                {
                    LoXmlServiceResult result = new LoXmlServiceResult();
                    result.Status = ServiceResultStatus.OKWithWarning;

                    result.AppendWarning($"Unable to determine DocType {documentType}. Defaulted to UNCLASSIFIED DocType.");
                    result.AddResultElement("DocumentId", docId.ToString("N"));

                    return result.ToResponse();

                    /*var warning = $"Unable to determine DocType {documentType}. Defaulted to UNCLASSIFIED DocType.";
                    return $"<result status=\"OKWithWarning\" documentid=\"{docId.ToString("N")}\"><warning>{warning}</warning></result>";*/
                }

                return $"<result status=\"OK\" documentid=\"{docId:N}\" />";
            }
            catch (IntegrationInvalidLoanNumberException exc)
            {
                return "<result status=\"Error\"><error>" + SecurityElement.Escape(exc.UserMessage) + "</error></result>";
            }
            catch (BlankLoanNumberException exc)
            {
                return "<result status=\"Error\"><error>" + SecurityElement.Escape(exc.UserMessage) + "</error></result>";
            }
            catch (AccessDenied exc)
            {
                return "<result status=\"Error\"><error>" + SecurityElement.Escape(exc.UserMessage) + "</error></result>";
            }
            catch (PageDataAccessDenied exc)
            {
                return "<result status=\"Error\"><error>" + SecurityElement.Escape(exc.UserMessage) + "</error></result>";
            }
            catch (InvalidPDFFileException exc)
            {
                return "<result status=\"Error\"><error>" + SecurityElement.Escape(exc.UserMessage) + "</error></result>";
            }
            catch (InsufficientPdfPermissionException exc)
            {
                return "<result status=\"Error\"><error>" + SecurityElement.Escape(exc.UserMessage) + "</error></result>";
            }
            catch (CBaseException exc)
            {
                if (!exc.UserMessage.StartsWith("Cannot find document type"))
                {
                    Tools.LogErrorWithCriticalTracking(exc); // db - Being conservative here by e-mailing this exc - if it gets annoying, we can remove this
                }

                return "<result status=\"Error\"><error>" + SecurityElement.Escape(exc.UserMessage) + "</error></result>";
            }
        }

        /// <summary>
        /// Verifies that the principal exists and has appropriate permissions to use this service.
        /// </summary>
        /// <param name="principal">The user accessing the service.</param>
        /// <param name="errorMessage">An out parameter that will be populated with an error message, if applicable.</param>
        /// <returns>A boolean expressing whether the principal has appropriate permissions.</returns>
        private bool VerifyEdocsServicePermissions(AbstractUserPrincipal principal, out string errorMessage)
        {
            if (principal == null)
            {
                errorMessage = ErrorMessages.WebServices.InvalidAuthTicket;
                return false;
            }

            if (!principal.HasPermission(Permission.CanViewEDocs))
            {
                errorMessage = ErrorMessages.EDocs.InsufficientReadPermission;
                return false;
            }

            errorMessage = string.Empty;
            return true;
        }
    }
}