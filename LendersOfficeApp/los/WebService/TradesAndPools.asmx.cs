﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Web.Services;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.CapitalMarkets;
    using LendersOffice.ObjLib.MortgagePool;
    using LendersOffice.Security;

    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class TradesAndPools : System.Web.Services.WebService
    {
        private static string GenerateErrorResponseXml(string errMsg)
        {
            return String.Format("<result status=\"Error\"><error>{0}</error></result>", errMsg);
        }

        private static string GenerateOKResponseXml()
        {
            return "<result status=\"OK\" />";
        }

        private static string GenerateOKWithWarningXml(string warningMsg)
        {
            return String.Format("<result status=\"OKWithWarning\">{0}</result>", warningMsg);
        }

        private static string GenerateWarningXml(string warningMsg)
        {
            return String.Format("<warning>{0}</warning>", warningMsg);
        }

        private static object GetFieldValue(string fieldId, object source, Type sourceType)
        {
            PropertyInfo PropInfo = sourceType.GetProperty(fieldId);
            PropertyInfo RepPropInfo = sourceType.GetProperty(fieldId + "_rep");

            if (PropInfo != null && (PropInfo.PropertyType.IsEnum || RepPropInfo == null))
                return PropInfo.GetValue(source, null);
            else if (RepPropInfo != null)
                return RepPropInfo.GetValue(source, null);

            return null;
        }

        private static string SetFieldValue(string value, string fieldId, object dest, Type destType)
        {
            PropertyInfo PropInfo = destType.GetProperty(fieldId);
            PropertyInfo RepPropInfo = destType.GetProperty(fieldId + "_rep");

            // Check if FieldId is valid
            if (PropInfo == null && RepPropInfo == null)
            {
                return String.Format("Field not set. No field exists with FieldId = {0}.", fieldId);
            }

            // Attempt to write to _rep first (setter should parse string for us)
            if (RepPropInfo != null && RepPropInfo.GetSetMethod() != null)
            {
                RepPropInfo.SetValue(dest, value, null);
                return "";
            }

            ////////////////////////////////////////
            // If here, no rep or rep not writable//
            ////////////////////////////////////////

            // Check if non-rep field exists and is writable
            if (PropInfo == null || PropInfo.GetSetMethod() == null)
            {
                return String.Format("Field not set. Field cannot be written to. FieldId = {0}.", fieldId);
            }

            // Parse value string to property type
            Object val;
            Type propType = PropInfo.PropertyType;

            // If field is nullable and value blank, set field to null
            if (string.IsNullOrEmpty(value) && propType.IsGenericType && propType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                PropInfo.SetValue(dest, null, null);
                return "";
            }

            if (propType == typeof(bool) || propType == typeof(bool?))
                val = value.Equals("True", StringComparison.OrdinalIgnoreCase);
            else if (propType == typeof(DateTime) || propType == typeof(DateTime?) || propType == typeof(CDateTime))
            {
                // Parse Date
                DateTime dt;
                if (!DateTime.TryParse(value, out dt))
                {
                    return String.Format("Field not set. Invalid value. FieldId = {0}. Value = {1}.", fieldId, value);
                }

                // Convert to CDateTime if necessary
                if (propType == typeof(CDateTime))
                    val = CDateTime.Create(dt);
                else
                    val = dt;
            }
            else if (propType.IsEnum || propType == typeof(int) || propType == typeof(int?))
            {
                int i;
                if (!int.TryParse(value, out i))
                {
                    return String.Format("Field not set. Invalid value. FieldId = {0}. Value = {1}.", fieldId, value);
                }

                val = i;
            }
            else if (propType == typeof(decimal) || propType == typeof(decimal?))
            {
                decimal d;
                LosConvert convert = new LosConvert();
                if (!decimal.TryParse(convert.SanitizedDecimalString(value), out d))
                {
                    return String.Format("Field not set. Invalid value. FieldId = {0}. Value = {1}.", fieldId, value);
                }

                val = d;
            }
            else // string
                val = value;

            // Set Property
            PropInfo.SetValue(dest, val, null);

            return "";
        }

        [WebMethod(Description = "Retrieve requested Mortgage Pool Fields and return pool data in LO XML format. [sTicket] - The authentication ticket. [poolXmlQuery] - xml that defines the list of fields to be included in the response.")]
        public string LoadAllMortgagePools(string authTicket, string poolXmlQuery)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(authTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            XmlDocument queryXml = null;
            try
            {
                queryXml = Tools.CreateXmlDoc(poolXmlQuery);
            }
            catch (XmlException x)
            {
                Tools.LogError("Error parsing XML document in LoadMortgagePools.", x);
                return GenerateErrorResponseXml("Invalid XML Document: " + x.Message);
            }

            if (null == queryXml)
            {
                return GenerateErrorResponseXml("Invalid XML Document");
            }

            List<MortgagePool> pools = MortgagePool.FindMortgagePools(principal.BrokerId);

            // Create Output Doc
            XDocument xdoc = WritePoolXml(queryXml, pools);

            return xdoc.ToString();
        }

        /// <summary>
        /// Retrieve requested Mortgage Pool fields
        /// </summary>
        [WebMethod(Description = "Retrieve requested Mortgage Pool Fields and return pool data in LO XML format. [sTicket] - The authentication ticket. [poolId] - The LendingQB pool ID.  [poolXmlQuery] - xml that defines the list of fields to be included in the response.")]
        public string LoadMortgagePool(string sTicket, string poolId, string poolXmlQuery)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Check query doc
            XmlDocument queryXml = null;
            try
            {
                queryXml = Tools.CreateXmlDoc(poolXmlQuery);
            }
            catch (System.Xml.XmlException x)
            {
                Tools.LogError("Error creating XML document in LoadMortgagePool: " + x);
                return GenerateErrorResponseXml("Invalid XML Document: " + x.Message);
            }

            if (null == queryXml)
            {
                return GenerateErrorResponseXml("Invalid XML Document");
            }

            // Find Pool
            DataTable dt = MortgagePool.FindMortgagePoolsByInternalID(poolId, false);
            if (dt.Rows.Count == 0)
                return GenerateErrorResponseXml("Pool not found. PoolId = " + poolId);

            MortgagePool pool = new MortgagePool((long)dt.Rows[0]["PoolId"]);

            // Create Output Doc
            XDocument xdoc = WritePoolXml(queryXml, new[] { pool });

            return xdoc.ToString();
        }

        private static XDocument WritePoolXml(XmlDocument queryXml, IEnumerable<MortgagePool> pools)
        {
            XmlNodeList queryFields = queryXml.SelectNodes("//pool/field");
            var queryContainsLoans = queryXml.SelectSingleNode("//pool/loans") != null;
            var queryContainsTrades = queryXml.SelectSingleNode("//pool/trades") != null;
            XDocument xdoc = new XDocument();
            XElement xroot = new XElement("LOXmlFormat", new XAttribute("version", LendersOffice.Constants.ConstAppDavid.LOXmlFormatVersion));
            xdoc.Add(xroot);

            foreach (MortgagePool pool in pools)
            {
                pool.SetFormatTarget(FormatTarget.XsltExport);
                XElement xpool = new XElement("pool");

                // Output Fields
                bool listLoans = false;
                bool listTrades = false;
                foreach (XmlElement el in queryFields)
                {
                    object val = null;
                    string fieldId = el.GetAttribute("id");
                    switch (fieldId)
                    {
                        case "PoolId":
                            val = pool.InternalId;
                            break;
                        case "Loans":
                            listLoans = true;
                            continue;
                        case "Trades":
                            listTrades = true;
                            continue;
                        case "UniqueId":
                            val = pool.PublicId;
                            break;
                        case nameof(MortgagePool.BasePricingTable):
                            val = pool.BasePricingTableJSON;
                            break;
                        case nameof(MortgagePool.FeatureCodes):
                            val = pool.FeatureCodesJSON;
                            break;
                        default:
                            val = GetFieldValue(fieldId, pool, typeof(MortgagePool));
                            break;
                    }

                    // Add to result xml
                    XElement xfield = new XElement("field", new XAttribute("id", fieldId));
                    if (val == null)
                        xfield.Value = "";
                    else if (val is DateTime)
                        xfield.Value = ((DateTime)val).ToShortDateString();
                    else if (val is Enum)
                        xfield.Value = ((Enum)val).ToString("D");
                    else
                        xfield.Value = val.ToString();

                    ////if (!string.IsNullOrEmpty(xfield.Value))
                    ////{
                        xpool.Add(xfield);
                    ////}

                    // Write loanIds if requested
                    if (listLoans || queryContainsLoans)
                    {
                        List<Guid> loanIds = pool.FetchLoansInPool();
                        foreach (Guid loanId in loanIds)
                        {
                            string sLNm = "";
                            SqlParameter[] parameters = {
                                                    new SqlParameter("@LoanId", loanId)
                                                };

                            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(pool.BrokerId, "GetLoanNumberByLoanId", parameters))
                            {
                                if (reader.Read())
                                {
                                    sLNm = (string)reader["sLNm"];
                                }
                            }

                            XElement xloan = new XElement("loanName");
                            xloan.Value = sLNm;
                            xpool.Add(xloan);
                        }
                    }

                    // Write tradeNums if requested
                    if (listTrades || queryContainsTrades)
                    {
                        List<Trade> trades = pool.GetTrades();
                        foreach (Trade trade in trades)
                        {
                            XElement xtrade = new XElement("tradeNum");
                            xtrade.Value = trade.TradeNum_rep;
                            xpool.Add(xtrade);
                        }
                    }
                }
                xroot.Add(xpool);
            }

            return xdoc;
        }

        /// <summary>
        /// Retrieve requested Trade fields
        /// </summary>
        [WebMethod(Description = "Retrieve requested Trade Fields and return trade data in LO XML format. [sTicket] - The authentication ticket. [tradeNum] - The LendingQB trade number.  [tradeXmlQuery] - xml that defines the list of fields to be included in the response.")]
        public string LoadTrade(string sTicket, string tradeNum, string tradeXmlQuery)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Check query doc
            XmlDocument queryXml = null;
            try
            {
                queryXml = Tools.CreateXmlDoc(tradeXmlQuery);
            }
            catch (System.Xml.XmlException x)
            {
                Tools.LogError("Error creating XML document in LoadTrade: " + x);
                return GenerateErrorResponseXml("Invalid XML Document: " + x.Message);
            }

            if (null == queryXml)
            {
                return GenerateErrorResponseXml("Invalid XML Document");
            }

            // Find Trade
            int num;
            if (!int.TryParse(tradeNum, out num))
                return GenerateErrorResponseXml("Invalid TradeNum. TradeNum = " + tradeNum);

            Trade trade;
            try
            {
                trade = Trade.RetrieveByTradeNum(num, principal.BrokerId);
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml("Trade not found. TradeNum = " + tradeNum);
            }

            // Create Output Doc
            XDocument xdoc = new XDocument();
            XElement xroot = new XElement("LOXmlFormat", new XAttribute("version", LendersOffice.Constants.ConstAppDavid.LOXmlFormatVersion));
            XElement xtrade = new XElement("trade");
            xdoc.Add(xroot);
            xroot.Add(xtrade);

            // Output Fields
            bool listTransactions = false;
            XmlNodeList xTradeFieldList = queryXml.SelectNodes("//trade/field");
            foreach (XmlElement el in xTradeFieldList)
            {
                object val = null;
                string fieldId = el.GetAttribute("id");
                switch(fieldId)
                {
                    case "AssociatedPoolId":
                        if (trade.AssociatedPoolId != null)
                        {
                            MortgagePool pool = new MortgagePool((long)trade.AssociatedPoolId);
                            val = pool.InternalId;
                        }
                        break;
                    case "Month":
                        // Special Case. Want to return int value for month, not string _rep value.
                        val = trade.Month;
                        break;
                    case "Description": case "DescriptionObj":
                        val = trade.Description_rep;
                        break;
                    case "Transactions":
                        listTransactions = true;
                        continue;
                    default:
                        val = GetFieldValue(fieldId, trade, typeof(Trade));
                        break;
                }

                // Add to result xml
                XElement xfield = new XElement("field", new XAttribute("id", fieldId));
                if (val == null)
                    xfield.Value = "";
                else if (val is DateTime)
                    xfield.Value = ((DateTime)val).ToShortDateString();
                else if (val is Enum)
                    xfield.Value = ((Enum)val).ToString("D");
                else
                    xfield.Value = val.ToString();

                xtrade.Add(xfield);
            }

            // Write transactionNums if requested
            if (listTransactions || queryXml.SelectSingleNode("//trade/transactions") != null)
            {
                foreach (Transaction trans in trade.Transactions)
                {
                    XElement xtrans = new XElement("transactionNum");
                    xtrans.Value = trans.TransactionNum_rep;
                    xtrade.Add(xtrans);
                }
            }

            return xdoc.ToString();
        }

        /// <summary>
        /// Retrieve requested Transaction fields
        /// </summary>
        [WebMethod(Description = "Retrieve requested Transaction Fields and return transaction data in LO XML format. [sTicket] - The authentication ticket. [transactionNum] - The LendingQB transaction number.  [transactionXmlQuery] - xml that defines the list of fields to be included in the response.")]
        public string LoadTransaction(string sTicket, string transactionNum, string transactionXmlQuery)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Check query doc
            XmlDocument queryXml = null;
            try
            {
                queryXml = Tools.CreateXmlDoc(transactionXmlQuery);
            }
            catch (System.Xml.XmlException x)
            {
                Tools.LogError("Error creating XML document in LoadTrade: " + x);
                return GenerateErrorResponseXml("Invalid XML Document: " + x.Message);
            }

            if (null == queryXml)
            {
                return GenerateErrorResponseXml("Invalid XML Document");
            }

            // Find Transaction
            int num;
            if (!int.TryParse(transactionNum, out num))
                return GenerateErrorResponseXml("Invalid TransactionNum. TransactionNum = " + transactionNum);

            Transaction trans;
            try
            {
                trans = Transaction.RetrieveByTransactionNum(principal.BrokerId, num);
            }
            catch (CBaseException)
            {
                return GenerateErrorResponseXml("Transaction not found. TransactionNum = " + transactionNum);
            }
            
            // Create Output Doc
            XDocument xdoc = new XDocument();
            XElement xroot = new XElement("LOXmlFormat", new XAttribute("version", LendersOffice.Constants.ConstAppDavid.LOXmlFormatVersion));
            XElement xtrans = new XElement("transaction");
            xdoc.Add(xroot);
            xroot.Add(xtrans);

            // Output Fields
            XmlNodeList xFieldList = queryXml.SelectNodes("//transaction/field");
            foreach (XmlElement el in xFieldList)
            {
                object val = null;
                string fieldId = el.GetAttribute("id");
                switch (fieldId)
                {
                    case "LinkedTrade":
                        if (trans.LinkedTrade != null)
                            val = trans.LinkedTrade.TradeNum_rep;
                        break;
                    case "LinkedTransaction": case "LinkedTransactionId":
                        if (trans.LinkedTransaction != null)
                            val = trans.LinkedTransaction.TransactionNum_rep;
                        break;
                    case "TradeId": case "TradeNum":
                        if (trans.TradeId != Guid.Empty)
                        {
                            Trade trade = Trade.RetrieveById(trans.TradeId, principal.BrokerId);
                            val = trade.TradeNum_rep;
                        }
                        break;
                    case "TransactionId":
                        val = trans.TransactionNum_rep;
                        break;
                    default:
                        val = GetFieldValue(fieldId, trans, typeof(Transaction));
                        break;
                }

                // Add to result xml
                XElement xfield = new XElement("field", new XAttribute("id", fieldId));
                if (val == null)
                    xfield.Value = "";
                else if (val is DateTime)
                    xfield.Value = ((DateTime)val).ToShortDateString();
                else if (val is Enum)
                    xfield.Value = ((Enum)val).ToString("D");
                else
                    xfield.Value = val.ToString();

                xtrans.Add(xfield);
            }

            return xdoc.ToString();
        }

        /// <summary>
        /// Create New Mortgage Pool
        /// </summary>
        [WebMethod(Description = "Create new Mortgage Pool and return the Pool ID of the newly created pool. [sTicket] - The authentication ticket.")]
        public string CreateMortgagePool(string sTicket)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            string internalId = MortgagePool.GetPoolAutoId();
            var newId = MortgagePool.CreatePool(internalId);
            if (newId == -1)
                return GenerateErrorResponseXml("Error Creating New Mortgage Pool");

            return internalId;
        }

        /// <summary>
        /// Create New Trade
        /// </summary>
        [WebMethod(Description = "Create new Trade and return the Trade Number of the newly created trade. [sTicket] - The authentication ticket.")]
        public string CreateTrade(string sTicket)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            Trade trade = new Trade(principal.BrokerId);
            trade.TradeNum_rep = Trade.GetTradeAutoNum(principal.BrokerId).ToString();
            trade.Save();

            return trade.TradeNum_rep;
        }

        /// <summary>
        /// Create New Transaction
        /// </summary>
        [WebMethod(Description = "Create new Transaction and return the Transaction Number of the newly created transaction. [sTicket] - The authentication ticket. [tradeNum] - The LendingQB trade number this transaction belongs to")]
        public string CreateTransaction(string sTicket, string tradeNum)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Find Trade
            int num;
            if (!int.TryParse(tradeNum, out num))
                return GenerateErrorResponseXml("Invalid TradeNum. TradeNum = " + tradeNum);

            Trade trade;
            try
            {
                trade = Trade.RetrieveByTradeNum(num, principal.BrokerId);
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml("Trade not found. TradeNum = " + tradeNum);
            }

            // Create Transaction
            Transaction trans = new Transaction(trade);
            trans.Date_rep = DateTime.Today.ToShortDateString();
            trans.Save();

            return trans.TransactionNum_rep;
        }

        /// <summary>
        /// Update a Mortgage Pool
        /// </summary>
        [WebMethod(Description = "Update a Mortgage Pool and return an XML response indicating the success of the operation. [sTicket] - The authentication ticket. [poolId] - The LendingQB pool ID. [poolDataContent] - LO XML format. Only fields that need updating should be sent over.")]
        public string SaveMortgagePool(string sTicket, string poolId, string poolDataContent)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Check data content
            XmlDocument xmlDoc = null;
            try
            {
                xmlDoc = Tools.CreateXmlDoc(poolDataContent);
            }
            catch (System.Xml.XmlException x)
            {
                Tools.LogError("Error creating XML document in SaveMortgagePool: " + x);
                return GenerateErrorResponseXml("Invalid XML Document: " + x.Message);
            }

            if (null == xmlDoc)
            {
                return GenerateErrorResponseXml("Invalid XML Document");
            }

            // Find Pool
            DataTable dt = MortgagePool.FindMortgagePoolsByInternalID(poolId, false);
            if (dt.Rows.Count == 0)
                return GenerateErrorResponseXml("Pool not found. PoolId = " + poolId);

            MortgagePool pool = new MortgagePool((long)dt.Rows[0]["PoolId"]);

            StringBuilder warning = new StringBuilder();

            // Set Fields
            XmlNodeList xfieldList = xmlDoc.SelectNodes("//pool/field");
            foreach (XmlElement el in xfieldList)
            {
                string fieldId = el.GetAttribute("id");
                string value = el.InnerXml.TrimWhitespaceAndBOM();

                switch (fieldId)
                {
                    case "BasePoolNumber":
                        string oldPoolNumber = pool.BasePoolNumber;
                        pool.BasePoolNumber = value;
                        if (!MortgagePool.ValidPoolNumber(pool))
                        {
                            pool.BasePoolNumber = oldPoolNumber;
                            warning.Append(GenerateWarningXml(String.Format("BasePoolNumber not set. A pool with that BasePoolNumber already exists. BasePoolNumber must be unique. BasePoolNumber = {0}.", value)));
                            continue;
                        }
                        break;
                    case "PoolId": case "InternalId":
                        // Keep going, without warning, if PoolId is not actually changed (prevents uniqueness warning)
                        if (pool.InternalId == value)
                            continue;

                        // Check that PoolId is int
                        int newNum;
                        if (!int.TryParse(value, out newNum))
                        {
                            warning.Append(GenerateWarningXml(String.Format("Field not set. Invalid value. FieldId = {0}. Value = {1}.", fieldId, value)));
                            continue;
                        }

                        // Test for uniqueness
                        if (MortgagePool.FindMortgagePoolsByInternalID(value, false).Rows.Count > 0)
                        {
                            warning.Append(GenerateWarningXml(String.Format("PoolId not set. A pool with that PoolId already exists. PoolId must be unique. PoolId = {0}.", newNum)));
                            continue;
                        }

                        pool.InternalId = value;
                        break;
                    default:
                        string res = SetFieldValue(value, fieldId, pool, typeof(MortgagePool));
                        if (!string.IsNullOrEmpty(res.TrimWhitespaceAndBOM()))
                            warning.Append(GenerateWarningXml(res));

                        break;
                }
            }

            // Save Changes
            // Note: Assign Loans reloads data. Must save before assigning loans.
            if (!pool.SaveData())
                return GenerateErrorResponseXml("Pool failed to save.");

            // Assign Loans to Pool
            List<Guid> loansInPool = pool.FetchLoansInPool();
            List<Guid> newLoanIds = new List<Guid>();
            XmlNodeList xLoanNameList = xmlDoc.SelectNodes("//pool/loanName");
            foreach (XmlElement el in xLoanNameList)
            {
                // Get list of loan IDs from names
                Guid id = Tools.GetLoanIdByLoanName(principal.BrokerId, el.InnerXml.TrimWhitespaceAndBOM());
                if (id == Guid.Empty)
                {
                    warning.Append(GenerateWarningXml(String.Format("Loan with Name={0} was not assigned to pool; check loan name.", el.InnerXml.TrimWhitespaceAndBOM())));
                    continue;
                }

                // Only add loans not currently in pool
                if (!loansInPool.Contains(id))
                {
                    newLoanIds.Add(id);
                }
            }

            pool.AssignLoans(newLoanIds);

            // Assign Trades to Pool
            List<Trade> tradesInPool = pool.GetTrades();
            List<Trade> newTradeNums = new List<Trade>();
            XmlNodeList xTradeNumList = xmlDoc.SelectNodes("//pool/tradeNum");
            foreach (XmlElement el in xTradeNumList)
            {
                // parse trade num
                int num;
                if (!int.TryParse(el.InnerXml.TrimWhitespaceAndBOM(), out num))
                 {
                     warning.Append(GenerateWarningXml(String.Format("Trade not assigned to pool: TradeNum = {0}.", el.InnerXml.TrimWhitespaceAndBOM())));
                    continue;
                }

                // Only add trades not currently in pool
                if (!tradesInPool.Any(a => a.TradeNum == num))
                {
                    Trade trade;
                    try
                    {
                        trade = Trade.RetrieveByTradeNum(num, principal.BrokerId);
                    }
                    catch (NotFoundException)
                    {
                        warning.Append(GenerateWarningXml(String.Format("Trade not assigned to pool: TradeNum = {0}.", el.InnerXml.TrimWhitespaceAndBOM())));
                        continue;
                    }

                    // Add Trade
                    trade.AssociatedPoolId = pool.PoolId;
                    trade.Save();
                }
            }

            // Write Output
            string s = warning.ToString();
            if (!String.IsNullOrEmpty(s))
                return GenerateOKWithWarningXml(s);

            return GenerateOKResponseXml();
        }

        /// <summary>
        /// Update a Trade
        /// </summary>
        [WebMethod(Description = "Update a Trade and return an XML response indicating the success of the operation. [sTicket] - The authentication ticket. [tradeNum] - The LendingQB trade number. [tradeXmlQuery] - LO XML format. Only fields that need updating should be sent over.")]
        public string SaveTrade(string sTicket, string tradeNum, string tradeXmlContent)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Check data content
            XmlDocument xmlDoc = null;
            try
            {
                xmlDoc = Tools.CreateXmlDoc(tradeXmlContent);
            }
            catch (System.Xml.XmlException x)
            {
                Tools.LogError("Error creating XML document in SaveMortgagePool: " + x);
                return GenerateErrorResponseXml("Invalid XML Document: " + x.Message);
            }

            if (null == xmlDoc)
            {
                return GenerateErrorResponseXml("Invalid XML Document");
            }

            // Find Trade
            int num;
            if (!int.TryParse(tradeNum, out num))
                return GenerateErrorResponseXml("Invalid TradeNum. TradeNum = " + tradeNum);

            Trade trade;
            try
            {
                trade = Trade.RetrieveByTradeNum(num, principal.BrokerId);
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml("Trade not found. TradeNum = " + tradeNum);
            }

            StringBuilder warning = new StringBuilder();

            // Set Fields
            XmlNodeList xfieldList = xmlDoc.SelectNodes("//trade/field");
            foreach (XmlElement el in xfieldList)
            {
                string fieldId = el.GetAttribute("id");
                string value = el.InnerXml.TrimWhitespaceAndBOM();
                switch (fieldId)
                {
                    case "AssociatedPoolId":
                        // If value is blank, dissacociate trade from pool
                        if (string.IsNullOrEmpty(value.TrimWhitespaceAndBOM()))
                        {
                            trade.AssociatedPoolId = null;
                            continue;
                        }

                        // Find Associated Pool
                        DataTable dt = MortgagePool.FindMortgagePoolsByInternalID(value, false);
                        if (dt.Rows.Count == 0)
                        {
                            warning.Append(GenerateWarningXml(String.Format("AssociatedPoolId not set. Pool not found. PoolId = {0}.", value)));
                            continue;
                        }

                        MortgagePool pool = new MortgagePool((long)dt.Rows[0]["PoolId"]);
                        trade.AssociatedPoolId = pool.PoolId;
                        break;
                    case "Description": case "DescriptionObj":
                        Description desc;
                        try
                        {
                            desc = Description.GetDescriptionByName(value);
                        }
                        catch (NotFoundException)
                        {
                            warning.Append(GenerateWarningXml(String.Format("Field not set. Invalid value. FieldId = {0}. Value = {1}.", fieldId, value)));
                            continue;
                        }

                        trade.DescriptionObj = desc;
                        break;
                    case "TradeNum":
                        // Keep going, without warning, if tradeNum is not actually changed (prevents uniqueness warning)
                        if (trade.TradeNum_rep == value)
                            continue;

                        // Check that TradeNum is int
                        int newNum;
                        if (!int.TryParse(value, out newNum))
                        {
                            warning.Append(GenerateWarningXml(String.Format("Field not set. Invalid value. FieldId = {0}. Value = {1}.", fieldId, value)));
                            continue;
                        }

                        // Test for uniqueness
                        if (Trade.GetTradesByBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId).Any(a => a.TradeNum == newNum))
                        {
                            warning.Append(GenerateWarningXml(String.Format("TradeNum not set. A trade with that TradeNum already exists. TradeNum must be unique. TradeNum = {0}.", newNum)));
                            continue;
                        }

                        trade.TradeNum_rep = value;
                        break;
                    default:
                        string res = SetFieldValue(value, fieldId, trade, typeof(Trade));
                        if (!string.IsNullOrEmpty(res.TrimWhitespaceAndBOM()))
                            warning.Append(GenerateWarningXml(res));

                        break;
                }
            }

            // Save Changes
            trade.Save();

            // Write Output
            string s = warning.ToString();
            if (!String.IsNullOrEmpty(s))
                return GenerateOKWithWarningXml(s);

            return GenerateOKResponseXml();
        }

        /// <summary>
        /// Update a Transaction
        /// </summary>
        [WebMethod(Description = "Update a Trade and return an XML response indicating the success of the operation. [sTicket] - The authentication ticket. [transactionNum] - The LendingQB transaction number. [transactionXmlContent] - LO XML format. Only fields that need updating should be sent over.")]
        public string SaveTransaction(string sTicket, string transactionNum, string transactionXmlContent)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Check data content
            XmlDocument xmlDoc = null;
            try
            {
                xmlDoc = Tools.CreateXmlDoc(transactionXmlContent);
            }
            catch (System.Xml.XmlException x)
            {
                Tools.LogError("Error creating XML document in SaveMortgagePool: " + x);
                return GenerateErrorResponseXml("Invalid XML Document: " + x.Message);
            }

            if (null == xmlDoc)
            {
                return GenerateErrorResponseXml("Invalid XML Document");
            }

            // Find Transaction
            int num;
            if (!int.TryParse(transactionNum, out num))
                return GenerateErrorResponseXml("Invalid TransactionNum. TransactionNum = " + transactionNum);

            Transaction trans;
            try
            {
                trans = Transaction.RetrieveByTransactionNum(principal.BrokerId, num);
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml("Transaction not found. TransactionNum = " + transactionNum);
            }

            StringBuilder warning = new StringBuilder();

            // Set Fields
            XmlNodeList xfieldList = xmlDoc.SelectNodes("//transaction/field");
            foreach (XmlElement el in xfieldList)
            {
                string fieldId = el.GetAttribute("id");
                string value = el.InnerXml.TrimWhitespaceAndBOM();
                switch (fieldId)
                {
                    case "Date":
                        if(string.IsNullOrEmpty(value.TrimWhitespaceAndBOM()))
                            warning.Append(GenerateWarningXml("Date not set. Date cannot be blank."));
                        else
                            trans.Date_rep = value;

                        break;
                    default:
                        string res = SetFieldValue(value, fieldId, trans, typeof(Transaction));
                        if (!string.IsNullOrEmpty(res.TrimWhitespaceAndBOM()))
                            warning.Append(GenerateWarningXml(res));

                        break;
                }
            }

            // Save Changes
            trans.Save();

            // Write Output
            string s = warning.ToString();
            if (!String.IsNullOrEmpty(s))
                return GenerateOKWithWarningXml(s);

            return GenerateOKResponseXml();
        }

        /// <summary>
        /// List pool IDs that meet the search criteria
        /// </summary>
        [WebMethod(Description = @"Search for Mortgage Pool that meet the criteria and return a list of pool IDs. 
                                    [sTicket] - The authentication ticket. 
                                    [poolStatus] - 0 (Any) | 1 (Open) | 2 (Closed). 
                                    [poolAgency] - 0 (Any) | 1 (Fannie Mae) | 2 (Freddie Mac) | 3 (Ginnie Mae). 
                                    [poolAmortizationType] - 0 (Any) | 1 (Fixed) | 2 (ARM). 
                                    [poolNumber] - The full pool number, including any prefixes or suffixes. Leave blank to match all. 
                                    [poolId] - The pool id. Leave blank to match all.")]
        public string ListPools(string sTicket, int poolStatus, int poolAgency, int poolAmortizationType, string poolNumber, string poolId)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            bool? isOpen = null;
            E_MortgagePoolAgencyT? agencyT = null;
            E_sFinMethT? amortT = null;
            DataTable dt;

            if (poolStatus == 2 /* Closed */)
                isOpen = false;
            else if (poolStatus == 1 /* Open */)
                isOpen = true;

            switch (poolAgency)
            {
                //case 0: break; // Any - Leave NULL
                case 1: agencyT = E_MortgagePoolAgencyT.FannieMae; break;
                case 2: agencyT = E_MortgagePoolAgencyT.FreddieMac; break;
                case 3: agencyT = E_MortgagePoolAgencyT.GinnieMae; break;
            }

            switch (poolAmortizationType)
            {
                //case 0: break; // Any - Leave NULL
                case 1: amortT = E_sFinMethT.Fixed; break;
                case 2: amortT = E_sFinMethT.ARM; break;
            }

            if (poolAmortizationType != 0)
                amortT = (E_sFinMethT) (poolAmortizationType - 1);

            dt = MortgagePool.FindMortgagePools(
                isOpen, 
                agencyT, 
                amortT, 
                poolNumber, 
                false, 
                poolId, 
                false,
                null,
                false);

            XDocument xdoc = new XDocument();
            XElement xroot = new XElement("list");
            xdoc.Add(xroot);
            foreach (DataRow dr in dt.Rows)
            {
                XElement xe = new XElement("poolId");
                xe.Value = dr["InternalId"].ToString();
                xroot.Add(xe);
            }
            return xdoc.ToString();
        }

        /// <summary>
        /// List trade numbers that meet the search criteria
        /// </summary>
        [WebMethod(Description = @"Search for Trades that meet the criteria and return a list of trade numbers. [sTicket] - The authentication ticket. 
                                    [tradeStatus] - 0 (Any) | 1 (Open) | 2 (Closed). 
                                    [tradeSecurity] - Trade secutiry description. 
                                    [tradeSecurityType] - 0 (Any) | 1 (TBA) | 2 (Call) | 3 (Put). 
                                    [tradeContractType] - 0 (Any) | 1 (Buy) | 2 (Sell). 
                                    [tradeSettlementMonth] - 0 (Any) | 1 (Jan) | 2 (Feb) | ... | 12 (Dec).
                                    [tradeCoupon] - The coupon rate of the trade. 
                                    [tradeNum] - The trade number. Leave blank to match all numbers.")]
        public string ListTrades(string sTicket, int tradeStatus, string tradeSecurity, int tradeSecurityType, int tradeContractType, int tradeSettlementMonth, string tradeCoupon, string tradeNum)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            ListTradeFilter f = new ListTradeFilter(principal.BrokerId, tradeStatus, tradeSecurity, tradeSecurityType, tradeContractType, tradeSettlementMonth, tradeCoupon, tradeNum);

            IEnumerable<Trade> trades;
            try
            {
                trades = Trade.GetTradesByBroker(principal.BrokerId).Filter(f);
            }
            catch (ArgumentException e)
            {
                return GenerateErrorResponseXml(e.Message);
            }

            XDocument xdoc = new XDocument();
            XElement xroot = new XElement("list");
            xdoc.Add(xroot);
            foreach (Trade trade in trades)
            {
                XElement xe = new XElement("tradeNum");
                xe.Value = trade.TradeNum_rep;
                xroot.Add(xe);
            }
            return xdoc.ToString();
        }

        /// <summary>
        /// Remove assigned loan from pool
        /// </summary>
        [WebMethod(Description = "Removes loan from pool. [sTicket] - The authentication ticket. [loanName] - The LendingQB loan name. [poolId] - The LendingQB pool ID")]
        public string RemoveLoanFromPool(string sTicket, string loanName, string poolId)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, loanName);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            //Get LoanId
            Guid id = Tools.GetLoanIdByLoanName(principal.BrokerId, loanName);
            if (id == Guid.Empty)
            {
                return GenerateErrorResponseXml("Loan not found. LoanName = " + loanName);
            }

            // Find Pool
            DataTable dt = MortgagePool.FindMortgagePoolsByInternalID(poolId, false);
            if (dt.Rows.Count == 0)
                return GenerateErrorResponseXml("Pool not found. PoolId = " + poolId);

            MortgagePool pool = new MortgagePool((long)dt.Rows[0]["PoolId"]);

            // Remove loan from pool

            // Note: RemoveLoans requires a list in order to work correctly
            List<Guid> ids = new List<Guid>();
            ids.Add(id);

            pool.RemoveLoans(ids);

            return GenerateOKResponseXml();
        }

        /// <summary>
        /// Remove pool association from trade
        /// </summary>
        [WebMethod(Description = "Detaches a trade from its associated pool. [sTicket] - The authentication ticket. [tradeNum] - The LendingQB trade number.")]
        public string DisassociateTrade(string sTicket, string tradeNum)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Find Trade
            int num;
            if (!int.TryParse(tradeNum, out num))
                return GenerateErrorResponseXml("Invalid TradeNum. TradeNum = " + tradeNum);

            Trade trade;
            try
            {
                trade = Trade.RetrieveByTradeNum(num, principal.BrokerId);
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml("Trade not found. TradeNum = " + tradeNum);
            }

            if (trade.AssociatedPoolId == null)
                return GenerateOKWithWarningXml(GenerateWarningXml("Trade was not associated with any pool. TradeNum = " + tradeNum));

            trade.AssociatedPoolId = null;
            trade.Save();

            return GenerateOKResponseXml();
        }

        /// <summary>
        /// Delete Transaction and remove from trade
        /// </summary>
        [WebMethod(Description = "Deletes a Transaction. [sTicket] - The authentication ticket. [transactionNum] - The LendingQB transaction number.")]
        public string DeleteTransaction(string sTicket, string transactionNum)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Find Transaction
            int num;
            if (!int.TryParse(transactionNum, out num))
                return GenerateErrorResponseXml("Invalid TransactionNum. TransactionNum = " + transactionNum);

            Transaction trans;
            try
            {
                trans = Transaction.RetrieveByTransactionNum(principal.BrokerId, num);
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml("Transaction not found. TransactionNum = " + transactionNum);
            }

            trans.Delete();

            return GenerateOKResponseXml();
        }

        /// <summary>
        /// Link Trasnaction to Trade
        /// </summary>
        [WebMethod(Description = "Links a Transaction to Trade. [sTicket] - The authentication ticket. [transactionNum] - The LendingQB transaction number. [tradeNum] - The LendingQB trade number.")]
        public string LinkTransactionToTrade(string sTicket, string transactionNum, string tradeNum)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Find Transaction
            int num;
            if (!int.TryParse(transactionNum, out num))
                return GenerateErrorResponseXml("Invalid TransactionNum. TransactionNum = " + transactionNum);

            Transaction trans;
            try
            {
                trans = Transaction.RetrieveByTransactionNum(principal.BrokerId, num);
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml("Transaction not found. TransactionNum = " + transactionNum);
            }

            // Find Trade
            if (!int.TryParse(tradeNum, out num))
                return GenerateErrorResponseXml("Invalid TradeNum. TradeNum = " + tradeNum);

            Trade trade;
            try
            {
                trade = Trade.RetrieveByTradeNum(num, principal.BrokerId);
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml("Trade not found. TradeNum = " + tradeNum);
            }

            trans.LinkToTrade(trade.TradeId);

            return GenerateOKResponseXml();
        }

        /// <summary>
        /// Unlinks transaction.
        /// </summary>
        [WebMethod(Description = "Unlinks a Transaction. [sTicket] - The authentication ticket. [transactionNum] - The LendingQB transaction number.")]
        public string UnlinkTransaction(string sTicket, string transactionNum)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            // Find Transaction
            int num;
            if (!int.TryParse(transactionNum, out num))
                return GenerateErrorResponseXml("Invalid TransactionNum. TransactionNum = " + transactionNum);

            Transaction trans;
            try
            {
                trans = Transaction.RetrieveByTransactionNum(principal.BrokerId, num);
            }
            catch (NotFoundException)
            {
                return GenerateErrorResponseXml("Transaction not found. TransactionNum = " + transactionNum);
            }

            // No need to do anything if transaction not linked
            if (!trans.HasLink)
            {
                return GenerateOKWithWarningXml(GenerateWarningXml("Transaction not modified. Transaction was not linked."));
            }

            Transaction.DeleteAndUnlink(principal.BrokerId, trans.LinkedTransactionId.Value, trans.TransactionId);

            return GenerateOKResponseXml();
        }

        protected class ListTradeFilter : TradeFilter
        {
            private Guid m_BrokerId;
            private int m_Status;
            private string m_Security;
            private int m_Type;
            private int m_Direction;
            private int m_Month;
            private string m_Coupon;
            private string m_TradeNum;

            public ListTradeFilter(Guid BrokerId, int tradeStatus, string tradeSecurity, int tradeSecurityType, int tradeContractType, int tradeSettlementMonth, string tradeCoupon, string tradeNum)
            {
                m_BrokerId = BrokerId;
                m_Status = tradeStatus;
                m_Security = tradeSecurity;
                m_Type = tradeSecurityType;
                m_Direction = tradeContractType;
                m_Month = tradeSettlementMonth;
                m_Coupon = tradeCoupon;
                m_TradeNum = tradeNum;
            }

            public override Guid BrokerId
            {
                get { return m_BrokerId; }
            }

            public override TradeStatus? Status
            {
                get
                {
                    switch(m_Status)
                    {
                        case 0: return null;
                        case 1: return TradeStatus.Open;
                        case 2: return TradeStatus.Closed;
                        default: throw new ArgumentException("ListTrades: Invalid Trade Status. Status=" + m_Status);
                    }
                }
            }

            public override string TradeNum
            {
                get { return m_TradeNum; }
            }

            public override bool TradeNumPartialMatch
            {
                get { return false; }
            }

            public override E_TradeType? Type
            {
                get
                {
                    switch (m_Type)
                    {
                        case 0: return null;
                        case 1: return E_TradeType.TBA;
                        case 2: return E_TradeType.CallOption;
                        case 3: return E_TradeType.PutOption;
                        default: throw new ArgumentException("ListTrades: Invalid Trade Type. Type=" + m_Type);
                    }
                }
            }

            public override E_TradeDirection? Direction
            {
                get
                {
                    switch (m_Direction)
                    {
                        case 0: return null;
                        case 1: return E_TradeDirection.Buy;
                        case 2: return E_TradeDirection.Sell;
                        default: throw new ArgumentException("ListTrades: Invalid Trade Direction. Direction=" + m_Direction);
                    }
                }
            }

            public override int? Month
            {
                get
                {
                    if (m_Month == 0)
                        return null;

                    if (m_Month < 0 && m_Month > 12)
                        throw new ArgumentException("ListTrades: Invalid Trade Month. Month=" + m_Month);

                    return m_Month;
                }
            }

            public override Guid? DescriptionId
            {
                get
                {
                    if (String.IsNullOrEmpty(m_Security.TrimWhitespaceAndBOM()))
                        return null;

                    Description desc;
                    try
                    {
                        desc = Description.GetDescriptionByName(m_Security);
                    }
                    catch (NotFoundException)
                    {
                        throw new ArgumentException("ListTrades: Invalid Trade Security. Security=" + m_Security);
                    }

                    return desc.DescriptionId;
                }
            }

            public override string Coupon
            {
                get { return m_Coupon; }
            }

            public override bool FilterTradesInPools
            {
                get { return false; }
            }

            public override DateTime? SettlementDateLowerBound
            {
                get { return null; }
            }

            public override DateTime? SettlementDateUpperBound
            {
                get { return null; }
            }
        }
    }
}
