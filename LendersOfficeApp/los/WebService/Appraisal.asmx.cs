﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Linq;
    using System.Web.Services;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.Security;
    using LendersOfficeApp.WebService;

    /// <summary>
    /// Appraisal related webservices.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class Appraisal : System.Web.Services.WebService
    {
        /// <summary>
        /// Gets a list of vendors available to the given ticket.
        /// </summary>
        /// <param name="ticket">The ticket of the user requested the list of vendors.</param>
        /// <returns>The list of vendors associated with the given user.</returns>
        [WebMethod]
        public string GetAvailableVendors(string ticket)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);
            CallVolume.IncreaseAndCheck(principal);
            var items = AppraisalVendorFactory.GetAvailableVendorsForBroker(principal.BrokerId).Where(p => !p.UsesGlobalDMS);
            return new VendorListingServiceResult(items).ToResponse();
        }

        /// <summary>
        /// Gets the vendor settings for the given vendor id. Accepts custom vendor credentials. 
        /// </summary>
        /// <param name="ticket">The auth ticket.</param>
        /// <param name="loanName">The loan name.</param>
        /// <param name="vendorId">The vendor id.</param>
        /// <param name="optionalAccountId">The vendor account id.</param>
        /// <param name="optionalUserName">The vendor username.</param>
        /// <param name="optionalPassword">The vendor password.</param>
        /// <returns>The vendor settings.</returns>
        [WebMethod]
        public string GetVendorSettings(string ticket, string loanName, Guid vendorId, string optionalAccountId, string optionalUserName, string optionalPassword)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);
            CallVolume.IncreaseAndCheck(principal);

            ServiceResult sr;

            string[] items = new string[]
            {
                optionalAccountId,
                optionalUserName,
                optionalPassword
            };

            Credential cred = null;
            if (items.Any(p => !string.IsNullOrWhiteSpace(p)))
            {
                if (items.All(p => !string.IsNullOrWhiteSpace(p)))
                {
                    cred = new Credential()
                    {
                        AccountId = optionalAccountId,
                        Username = optionalUserName,
                        Password = optionalPassword
                    };
                }
                else
                {
                    sr = new ServiceResult();
                    sr.Status = ServiceResultStatus.Error;
                    sr.AppendError("If any is provided all are required. (optionalVendorAccountId, optionalUsername, optionalUsername)");
                    return sr.ToResponse();
                }
            }

            Guid loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, loanName);

            var response = AppraisalVendorFactory.GetVendorSettings(principal, loanId, vendorId, cred);

            switch (response.ResponseType)
            {
                case ResponseType.Error:
                    sr = new ServiceResult();
                    sr.Status = ServiceResultStatus.Error;
                    sr.AppendError(((ErrorVendorSettingsResponse)response).Message);
                    break;
                case ResponseType.LQB:
                    LqbVendorSettingsResponse lqbResponse = (LqbVendorSettingsResponse)response;
                    sr = new LqbVendorSettingsResult(lqbResponse);
                    break;
                case ResponseType.GDMS:
                    GdmsVendorSettingsResponse gdmsResponse = (GdmsVendorSettingsResponse)response;
                    sr = new GdmsVendorSettingsResult(gdmsResponse);
                    break;
                default:
                    throw new UnhandledEnumException(response.ResponseType);
            }

            return sr.ToResponse();
        }

        /// <summary>
        /// Submits the order under the given ticket to the vendor specified using the 
        /// given xml for the order details.
        /// </summary>
        /// <param name="ticket">The ticket of the logged in user.</param>
        /// <param name="vendorId">The vendor id.</param>
        /// <param name="xml">The order details.</param>
        /// <returns>Xml with okay or failure.</returns>
        [WebMethod]
        public string SubmitOrder(string ticket, Guid vendorId, string xml)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);
            CallVolume.IncreaseAndCheck(principal);

            ServiceResult r = new ServiceResult();

            var vendor = AppraisalVendorFactory.GetAvailableVendorsForBroker(principal.BrokerId).Where(p => p.VendorId == vendorId).FirstOrDefault();

            if (vendor == null || vendor.UsesGlobalDMS)
            {
                r.Status = ServiceResultStatus.Error;
                r.AppendError("Vendor not found.");
                return r.ToResponse();
            }

            LqbOrderXmlRequestParser parser = new LqbOrderXmlRequestParser(xml);
            parser.Generate();

            if (parser.HasErrors)
            {
                r.Status = ServiceResultStatus.Error;
                foreach (var error in parser.GetErrors())
                {
                    r.AppendError(error);
                }

                return r.ToResponse();
            }

            LqbOrderRequestDetails details = parser.GetDetails();
            Guid loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, details.LoanName);

            if (loanId == Guid.Empty)
            {
                r.Status = ServiceResultStatus.Error;
                r.AppendError("Loan not found.");
                return r.ToResponse();
            }

            var response = AppraisalVendorFactory.SubmitOrder(principal, loanId, details, vendor);
            return response.ToResponse();
        }
    }
}
