namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Web.Services;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.LockPolicies;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.ObjLib.RatesheetExpiration;
    using LendersOffice.RatePrice;
    using LendersOffice.RatePrice.Helpers;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using System.Globalization;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.Common;
    using LqbGrammar.Drivers.SecurityEventLogging;

    [WebService(Namespace="http://www.lendersoffice.com/los/webservices/")]
	public class Broker : System.Web.Services.WebService
	{
        public struct BrokerRecord
		{
			public Guid BrokerId ;
			public string BrokerName ;
			public string BrokerAddress ;
			public string BrokerCity ;
			public string BrokerState ;
			public string BrokerZip ;
			public string BrokerPhone ;
			public string BrokerFax ;
		}

        public struct ProductRecord
        {
            public Guid lLpTemplateId;
            public bool isExpired;
            public decimal LenderProfitMargin;
        }

		public Broker()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

        [WebMethod]
        public string GetSnapshotVersion(string sTicket)
        {
            // 10/17/2012 dd - This method return the current snapshot version. It will use with the ExportBaseRates to avoid third party (currently only PML0213 CMG utilize this) calling ExportBaseRates too many time
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null); 

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            if (principal.ApplicationType != E_ApplicationT.LendersOffice)
            {
                return GenerateErrorResponseXml("You do not have permission.");
            }

            if (principal.IsInRole(ConstApp.ROLE_ADMINISTRATOR) == false || principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
            {
                return GenerateErrorResponseXml("You do not have permission");
            }

            LendersOfficeApp.los.RatePrice.CLpeReleaseInfo info = principal.BrokerDB.GetLatestReleaseInfo(); ;
            return info.ReleaseVer.ToString("yyyy-MM-dd_HH_mm_ss");

        }

        [WebMethod]
        public string ExportBaseRatesIncludeExpired(string sTicket)
        {
            return ExportBaseRatesByPriceGroupImpl(sTicket, string.Empty, true);

        }

        [WebMethod]
        public string ExportBaseRates(string sTicket)
        {
            return ExportBaseRatesByPriceGroup(sTicket, string.Empty);
        }

        [WebMethod]
        public string ExportBaseRatesByPriceGroup(string sTicket, string priceGroup)
        {
            return ExportBaseRatesByPriceGroupImpl(sTicket, priceGroup, false);
        }

        [WebMethod]
        public string DownloadIISReport(string sTicket, string date)
        {
            var result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Error;
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);
            var broker = BrokerDB.RetrieveById(principal.BrokerId);

            if (!principal.HasRole(E_RoleT.Administrator) || !broker.IsGenerateDailyIISReport)
            {
                result.AppendError(ErrorMessages.GenericAccessDenied);
                return result.ToResponse();
            }

            DateTime reportedDate = DateTime.ParseExact(date, "MM/dd/yyyy", CultureInfo.CurrentCulture);

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", principal.BrokerId),
                new SqlParameter("@ReportedDate", reportedDate),
            };

            string keyStr = string.Empty;
            using (var reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "BROKER_IIS_LOG_DATA_Retrieve_FileDbKey", parameters))
            {
                if (reader.Read())
                {
                    keyStr = (string)reader[0];
                }
                else
                {
                    result.AppendError("Unable to locate iis log file.");
                    return result.ToResponse();
                }
            }

            byte[] bytes = FileDBTools.ReadData(E_FileDB.Normal, keyStr);
            var content = System.Text.Encoding.UTF8.GetString(bytes);

            return content;
        }

        private string ExportBaseRatesByPriceGroupImpl(string sTicket, string priceGroup, bool isIncludeExpiredProgram)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            if (principal.ApplicationType != E_ApplicationT.LendersOffice)
            {
                return GenerateErrorResponseXml("You do not have permission to export rate data");
            }

            if (principal.IsInRole(ConstApp.ROLE_ADMINISTRATOR) == false || principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
            {
                return GenerateErrorResponseXml("You do not have permission to export rate data");
            }

            try
            {
                LendersOfficeApp.los.RatePrice.CLpeReleaseInfo info = principal.BrokerDB.GetLatestReleaseInfo();
                DataSrc dataSrc = info.DataSrcForSnapshot;
                using (MemoryStream outputStream = new MemoryStream(100000))
                {
                    XmlTextWriter xmlWriter = new XmlTextWriter(outputStream, System.Text.Encoding.ASCII);
                    xmlWriter.WriteStartElement("BaseRates");
                    xmlWriter.WriteAttributeString("version", info.ReleaseVer.ToString("yyyy-MM-dd_HH_mm_ss"));

                    List<ProductRecord> productIds = GetProductIds(dataSrc, principal.BrokerId, priceGroup); // Gets all products that are Lpe, Non-masters and are enabled
                    foreach(ProductRecord productRecord in productIds)
                    {
                        // If the product is expired, do not include it in the xml list returned to the user
                        if (isIncludeExpiredProgram == false)
                        {
                            if (productRecord.isExpired)
                            {
                                continue;
                            }
                        }

                        Guid productId = productRecord.lLpTemplateId;
                        
                        CLoanProductData product = new CLoanProductData(productId);
                        if (product == null)
                        {
                            continue;
                        }
                        
                        product.DataSourceForLoad = dataSrc;
                        product.InitLoad();

                        decimal currentIOStart = -1;
                        decimal currentPPStart = -1;
                        decimal currentLKStart = -1;

                        bool hasIO = false;
                        bool hasPP = false;
                        bool hasLK = false;

                        xmlWriter.WriteStartElement("Program");
                        xmlWriter.WriteAttributeString("lLpTemplateId", product.lLpTemplateId.ToString());
                        xmlWriter.WriteAttributeString("Name", product.lLpTemplateNm);
                        xmlWriter.WriteAttributeString("Investor", product.lLpInvestorNm);
                        xmlWriter.WriteAttributeString("LenderProfitMargin", productRecord.LenderProfitMargin.ToString("N3"));
                        xmlWriter.WriteAttributeString("IsExpired", productRecord.isExpired.ToString());
                        xmlWriter.WriteAttributeString("lRateSheetDownloadStartD", product.lRateSheetDownloadStartD.ToString("yyyy/MM/dd HH:mm:ss"));
                        xmlWriter.WriteAttributeString("lRateSheetDownloadEndD", product.lRateSheetDownloadEndD.ToString("yyyy/MM/dd HH:mm:ss"));
                        foreach (CRateSheetFields rate in product.GetRawRateSheetWithDeltas())
                        {
                            if (IsRateIOType(rate))
                            {
                                if (currentIOStart != rate.Point) // We must start a new IO segment
                                {
                                    if (hasLK == true) // We must close off the previous LK segment
                                    {
                                        xmlWriter.WriteEndElement(); // </LK>
                                    }
                                    if (hasPP == true) // We must close off the previous PP segment
                                    {
                                        xmlWriter.WriteEndElement(); // </PP>
                                    }
                                    if (hasIO == true) // We must close off the previous IO segment
                                    {
                                        xmlWriter.WriteEndElement(); // </IO>
                                    }

                                    currentIOStart = rate.Point;
                                    xmlWriter.WriteStartElement("IO");
                                    xmlWriter.WriteAttributeString("Start", rate.Point_rep);
                                    xmlWriter.WriteAttributeString("End", rate.Margin_rep);

                                    //Reset the flags, we have a new IO segment
                                    hasPP = false;
                                    hasLK = false;
                                    currentPPStart = -1;
                                    currentLKStart = -1;
                                }
                                
                                hasIO = true;
                            }
                            else if (IsRatePPType(rate))
                            {
                                if (currentPPStart != rate.Point) // We must start a new PP segment
                                {
                                    if (hasLK == true) // We must close off the previous LK segment
                                    {
                                        xmlWriter.WriteEndElement(); // </LK>
                                    }
                                    if (hasPP == true) // We must close off the previous PP segment
                                    {
                                        xmlWriter.WriteEndElement(); // </PP>
                                    }
                                    
                                    xmlWriter.WriteStartElement("PP");
                                    currentPPStart = rate.Point;
                                    xmlWriter.WriteAttributeString("Start", rate.Point_rep);
                                    xmlWriter.WriteAttributeString("End", rate.Margin_rep);

                                    //Reset the flags, we have a new PP segment
                                    hasLK = false;
                                    currentLKStart = -1;
                                }
                                
                                hasPP = true;
                            }
                            else if (IsRateLKType(rate))
                            {

                                if (currentLKStart != rate.Point) // We must start a new LK segment
                                {
                                    if (hasLK == true) // We must close off the previous LK segment
                                    {
                                        xmlWriter.WriteEndElement(); // </LK>
                                    }

                                    currentLKStart = rate.Point;
                                    xmlWriter.WriteStartElement("LK");
                                    xmlWriter.WriteAttributeString("Start", rate.Point_rep);
                                    xmlWriter.WriteAttributeString("End", rate.Margin_rep);
                                }
                                
                                hasLK = true;
                            }
                            else // This is a normal Rate Option segment
                            {
                                xmlWriter.WriteStartElement("RateOption");
                                xmlWriter.WriteAttributeString("NR", rate.Rate_rep.Trim('%'));
                                xmlWriter.WriteAttributeString("P", rate.Point_rep.Trim('%'));
                                xmlWriter.WriteAttributeString("M", rate.Margin_rep.Trim('%'));
                                xmlWriter.WriteAttributeString("QR", rate.QRateBase_rep.Trim('%'));
                                xmlWriter.WriteAttributeString("TR", rate.TeaserRate_rep.Trim('%'));
                                xmlWriter.WriteEndElement(); // </RateOption>
                            }
                        }

                        if (hasLK == true) // We must close off the previous PP segment
                        {
                            xmlWriter.WriteEndElement(); // </LK>
                        }
                        
                        if (hasPP == true) // We must close off the previous PP segment
                        {
                            xmlWriter.WriteEndElement(); // </PP>
                        }
                        if (hasIO == true) // We must close off the previous IO segment
                        {
                            xmlWriter.WriteEndElement(); // </IO>
                        }

                        xmlWriter.WriteEndElement(); // </Program>
                    }
                    xmlWriter.WriteEndElement(); // </BaseRates>
                    xmlWriter.Flush();
                    string xml = System.Text.ASCIIEncoding.ASCII.GetString(outputStream.GetBuffer(), 0, (int)outputStream.Position);
                    // 12/28/2011 dd - Since Bills.com will use this webservice to get our base rate. We need
                    // to put in PB so debug any issue or question from client.
                    Tools.LogInfo("ExportBaseRatesByPriceGroup:: " + principal.LoginNm + Environment.NewLine + xml);
                    return xml;
                }
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking("Unable to export base rates", exc);
                throw;
            }
        }

        // Retrieve the list of enabled products for this lender and determine if they are expired
        // If they are expired, they will not be included in the xml list returned to the user.
        // 10/26/2011 dd - OPM 72933 - Add option to pass priceGroup. When priceGroup is not null or empty then only
        // return valid product in the price group. If price group name does not existed then no product will return.
        private static List<ProductRecord> GetProductIds(DataSrc dataSrc, Guid brokerId, string priceGroup)
        {
            BrokerDB broker = BrokerDB.RetrieveById(brokerId);
            bool? useRateSheetExpirationFeature = null; // this will load from either the price group or the broker default lock policy
            LockPolicy lockPolicyToUse = null;

            HashSet<Guid> onlyContainTheseProducts = null;
            Dictionary<Guid, decimal> lenderProfitMarginDictionary = new Dictionary<Guid, decimal>();

            if (string.IsNullOrEmpty(priceGroup) == false)
            {
                onlyContainTheseProducts = new HashSet<Guid>();

                SqlParameter[] parameters = new SqlParameter[] {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@LpePriceGroupName", priceGroup)
                };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListValidProductIdsByPriceGroupName", parameters))
                {
                    while (reader.Read())
                    {
                        onlyContainTheseProducts.Add((Guid)reader["lLpTemplateId"]);
                        lenderProfitMarginDictionary[(Guid)reader["lLpTemplateId"]] = (decimal)reader["LenderProfitMargin"];
                    }
                }

                var pg = PriceGroup.RetrieveByName(priceGroup, brokerId);
                if (pg != null)
                {
                    lockPolicyToUse = LockPolicy.Retrieve(brokerId, pg.LockPolicyID.Value);
                    useRateSheetExpirationFeature = lockPolicyToUse.IsUsingRateSheetExpirationFeature;
                }
                else
                {
                    throw new CBaseException
                        (
                              "Unable to find price group with name: " + priceGroup
                            , "Unable to find price group with name: " + priceGroup
                        );
                }
            }
            else
            {
                lockPolicyToUse = LockPolicy.Retrieve(broker.BrokerID, broker.DefaultLockPolicyID.Value);
                useRateSheetExpirationFeature = lockPolicyToUse.IsUsingRateSheetExpirationFeature;
            }
            List<ProductRecord> productRecords = new List<ProductRecord>();

            if ( useRateSheetExpirationFeature.Value == false)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerId)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(dataSrc, "GetProductIdsByBrokerId", parameters))
                {
                    while (reader.Read())
                    {
                        Guid productId = new Guid(reader["lLpTemplateId"].ToString());
                        ProductRecord productRecord = new ProductRecord();
                        productRecord.lLpTemplateId = productId;
                        productRecord.isExpired = false; // Since the RS expiration feature isn't enabled, none of the programs are considered expired

                        decimal lenderProfitMargin = 0;
                        if (lenderProfitMarginDictionary.TryGetValue(productId, out lenderProfitMargin) == true)
                        {
                            productRecord.LenderProfitMargin = lenderProfitMargin;
                        }

                        if (onlyContainTheseProducts == null || onlyContainTheseProducts.Contains(productId))
                        {
                            productRecords.Add(productRecord);
                        }


                    }
                }
            }
            else
            {
                DataSet ds = new DataSet();
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerId)
                                            };
                DataSetHelper.Fill(ds, dataSrc, "GetProductInfoByBrokerId", parameters);
                DataTable programData = ds.Tables[0];
                LendersOfficeApp.los.RatePrice.RateOptionExpirationResult result;

                List<string> investorNamesForRateLockCutOff = new List<string>();
                List<string> rsFileIdForFileInfo = new List<string>();
                List<Tuple<string, string>> investorProductCodePair = new List<Tuple<string, string>>();
                foreach (DataRow program in programData.Rows)
                {
                    string investor = program["lLpInvestorNm"].ToString();
                    investorNamesForRateLockCutOff.Add(investor);
                    rsFileIdForFileInfo.Add(program["LpeAcceptableRsFileId"].ToString());
                    investorProductCodePair.Add(new Tuple<string, string>(investor, program["ProductCode"].ToString()));
                }

                RateOptionExpirationDataLoader dataLoader = new RateOptionExpirationDataLoader(investorNamesForRateLockCutOff, rsFileIdForFileInfo, investorProductCodePair, broker);
                foreach (DataRow program in programData.Rows)
                {
                    string investorName = program["lLpInvestorNm"].ToString();
                    result = LendersOfficeApp.los.RatePrice.CApplicantPriceOLD.DetermineRateOptionExpiration(
                    lockPolicyToUse
                    , E_LpeRsExpirationByPassPermissionT.NoByPass
                    , program["LpeAcceptableRsFileId"].ToString()
                    , long.Parse(program["LpeAcceptableRsFileVersionNumber"].ToString())
                    , investorName
                    , program["ProductCode"].ToString()
                    , false /* byPassManualDisabled */
                    , dataLoader);

                    ProductRecord productRecord = new ProductRecord();
                    productRecord.lLpTemplateId = new Guid(program["lLpTemplateId"].ToString());
                    productRecord.isExpired = result.IsBlockedRateLockSubmission;
                    decimal lenderProfitMargin = 0;
                    if (lenderProfitMarginDictionary.TryGetValue(productRecord.lLpTemplateId, out lenderProfitMargin) == true)
                    {
                        productRecord.LenderProfitMargin = lenderProfitMargin;
                    }

                    if (onlyContainTheseProducts == null || onlyContainTheseProducts.Contains(productRecord.lLpTemplateId))
                    {
                        productRecords.Add(productRecord);
                    }
                }
            }

            return productRecords;
        }

        private static bool IsRateIOType(CRateSheetFields rate)
        {
            return rate.IsSpecialKeyword && (GetSpecialKeywordGroupType(rate) == SpecialKeywordGroupType.IO);
        }

        private static bool IsRatePPType(CRateSheetFields rate)
        {
            return rate.IsSpecialKeyword && (GetSpecialKeywordGroupType(rate) == SpecialKeywordGroupType.PP);
        }

        private static bool IsRateLKType(CRateSheetFields rate)
        {
            return rate.IsSpecialKeyword && (GetSpecialKeywordGroupType(rate) == SpecialKeywordGroupType.LK);
        }

        private static SpecialKeywordGroupType GetSpecialKeywordGroupType(CRateSheetFields rate)
        {
            if (rate.IsSpecialKeyword == false)
            {
                return SpecialKeywordGroupType.NA;
            }

            switch(rate.Rate_rep.ToUpper())
            {
                case "LOCK":
                    return SpecialKeywordGroupType.LK;
                case "PP":
                    return SpecialKeywordGroupType.PP;
                case "IO":
                    return SpecialKeywordGroupType.IO;
                default:
                    throw new CBaseException("Unable to generate base rates", "Unhandled segment type: " + rate.Rate_rep.ToUpper());
            }
        }

        private enum SpecialKeywordGroupType
        {
            IO = 0,
            PP = 1,
            LK = 2,
            NA = 3
        }

        private static string GenerateErrorResponseXml(string errMsg)
        {
            return String.Format("<result status=\"Error\"><error>{0}</error></result>", errMsg);
        }

	}
}
