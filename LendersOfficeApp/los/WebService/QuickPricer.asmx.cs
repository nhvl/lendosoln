﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using System.Web.Services;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Conversions;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.ObjLib.LoXml;
    using LendersOffice.ObjLib.QuickPricer;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.QuickPricer;
    using LendersOffice.Security;
    using los.RatePrice;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Container for web services that interact with QuickPricer.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class QuickPricer : WebService
    {
        /// <summary>
        /// Runs QuickPricer on a file and returns the results.
        /// </summary>
        /// <param name="authorizationTicket">An authentication ticket.</param>
        /// <param name="xmlInput">The LOXml to import into the quickpricer file prior to running pricing.</param>
        /// <param name="quickPricerLoanId">The loan id of the Quick Pricer file. If blank, a new loan will be grabbed from the pool.</param>
        /// <returns>An LoXml formatted PML 2.0 result.</returns>
        [WebMethod(Description = "Runs QuickPricer on a file and returns the results.")]
        [SuppressMessage(
            category: "LendingQBStyleCop.LendingQBCustomRules",
            checkId: "LB1003:CannotSwallowGeneralException",
            Justification = "Return generic error response instead of throwing out of service.")]
        public string RunQuickPricerV2(string authorizationTicket, string xmlInput, string quickPricerLoanId)
        {
            AbstractUserPrincipal principal;
        
            try
            {
                principal = AuthServiceHelper.GetPrincipalFromTicket(authorizationTicket, null);
            }
            catch (AccessDenied)
            {
                return ErrorResponse(ErrorMessages.WebServices.InvalidAuthTicket);
            }

            if (principal == null)
            {
                return ErrorResponse(ErrorMessages.WebServices.InvalidAuthTicket);
            }

            CallVolume.IncreaseAndCheck(principal);

            if (!principal.IsActuallyUsePml2AsQuickPricer)
            {
                return ErrorResponse(ErrorMessages.WebServices.QuickPricer2NotEnabled);
            }
            else if (string.IsNullOrWhiteSpace(xmlInput))
            {
                return ErrorResponse(ErrorMessages.WebServices.InvalidInputLOXml);
            }

            Guid loanId;
            if (string.IsNullOrWhiteSpace(quickPricerLoanId))
            {
                loanId = QuickPricer2LoanPoolManager.GetSandboxedUnusedLoanAndMarkAsInUse();
            }
            else if (!Guid.TryParse(quickPricerLoanId, out loanId))
            {
                return ErrorResponse(ErrorMessages.WebServices.InvalidQuickPricer2LoanIdFormat);
            }
            else if (!Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                return ErrorResponse(ErrorMessages.WebServices.QuickPricer2FileNotFound(quickPricerLoanId));
            }

            XmlDocument pricingXml = null;
            LoFormatImporterResult importResult = null;
            try
            {
                importResult = LOFormatImporter.Import(loanId, principal, xmlInput, new string[] { "sSelectedProductCodeFilter", "sAvailableProductCodeFilter" });
                pricingXml = DistributeUnderwritingEngine.GetPricingResultInXml(
                    principal,
                    loanId,
                    alwaysUseRegularPricing: false, // Passing false to use the lender settings.
                    isIncludeOriginatorCompensation: true,
                    isUsePml2: true,
                    allowQp2File: true);
            }
            catch (InvalidLoXmlException e)
            {
                return ErrorResponse(e.Errors);
            }
            catch (Exception e)
            {
                Tools.LogError("Error encountered in RunQuickPricerV2.", e);
                var errorMessage = (e as CBaseException)?.UserMessage ?? ErrorMessages.Generic;
                return ErrorResponse(errorMessage);
            }

            var serviceResult = new LoXmlServiceResult();
            XElement loanXml = CreateQuickPricerResponseXml(loanId, pricingXml, importResult);
            serviceResult.AddResultElement(loanXml);
            return serviceResult.ToResponse();
        }

        /// <summary>
        /// Runs QuickPricer 1.0.
        /// </summary>
        /// <param name="authorizationTicket">The user's authentication ticket.</param>
        /// <param name="xmlInput">The XML input for pricing.</param>
        /// <returns>The QuickPricer results.</returns>
        [WebMethod(Description = "Runs QuickPricer 1.0")]
        public string RunQuickPricerV1(string authorizationTicket, string xmlInput)
        {
            string errMsg = string.Empty;
            if (string.IsNullOrEmpty(authorizationTicket))
            {
                errMsg += $"[{nameof(authorizationTicket)}] cannot be empty.";
            }

            if (string.IsNullOrEmpty(xmlInput))
            {
                errMsg += $"[{nameof(xmlInput)}] cannot be empty.";
            }

            if (errMsg != string.Empty)
            {
                throw new CBaseException(errMsg, "QuickPricer.asmx::RunQuickPricerV1 " + errMsg);
            }

            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(authorizationTicket, null);
            CallVolume.IncreaseAndCheck(principal);

            return this.RunQuickPricerV1(
                principal, 
                xmlInput,
                useBestPricing: true,
                includeInvestorName: false,
                fromConsumerPortal: false);
        }

        /// <summary>
        /// Runs QuickPricer 1.0 for lock desk.
        /// </summary>
        /// <param name="authorizationTicket">The user's authentication ticket.</param>
        /// <param name="xmlInput">The XML input for pricing.</param>
        /// <returns>An LOXML response or error.</returns>
        [WebMethod(Description = "Runs QuickPricer 1.0 for lock desk")]
        public string RunQuickPricerV1ForLockDesk(string authorizationTicket, string xmlInput)
        {
            string errMsg = string.Empty;
            if (string.IsNullOrEmpty(authorizationTicket))
            {
                errMsg += $"[{nameof(authorizationTicket)}] cannot be empty.";
            }

            if (string.IsNullOrEmpty(xmlInput))
            {
                errMsg += $"[{nameof(xmlInput)}] cannot be empty.";
            }

            if (errMsg != string.Empty)
            {
                throw new CBaseException(errMsg, "QuickPricer.asmx::RunQuickPricerV1 " + errMsg);
            }

            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(authorizationTicket, null);
            CallVolume.IncreaseAndCheck(principal);

            return this.RunQuickPricerV1(
                principal,
                xmlInput,
                useBestPricing: false,
                includeInvestorName: true,
                fromConsumerPortal: false);
        }

        /// <summary>
        /// Converts a QuickPricer 2.0 file to a lead.
        /// </summary>
        /// <param name="authorizationTicket">The user authentication ticket.</param>
        /// <param name="quickPricerLoanId">The QuickPricer loan ID.</param>
        /// <returns>An LOXML string containing the converted file's loan number or an error.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Parameters kept with hungarian notation to maintain public interface.")]
        [WebMethod(Description = "Creates a lead.")]
        public string CreateLead(string authorizationTicket, string quickPricerLoanId)
        {
            return ConvertQuickPricerFile(authorizationTicket, quickPricerLoanId, templateName: string.Empty, convertingQpFileToLead: true);
        }

        /// <summary>
        /// Converts a QuickPricer 2.0 file to a loan.
        /// </summary>
        /// <param name="authorizationTicket">The user authentication ticket.</param>
        /// <param name="quickPricerLoanId">The QuickPricer loan ID.</param>
        /// <param name="templateName">The template to create the loan file from, if applicable.</param>
        /// <returns>An LOXML string containing the converted file's loan number or an error.</returns>
        [WebMethod(Description = "Creates a loan.")]
        public string CreateLoan(string authorizationTicket, string quickPricerLoanId, string templateName)
        {
            return ConvertQuickPricerFile(authorizationTicket, quickPricerLoanId, templateName, convertingQpFileToLead: false);
        }

        /// <summary>
        /// Runs QuickPricer 1.0.
        /// </summary>
        /// <param name="principal">The user running QuickPricer.</param>
        /// <param name="xmlData">The XML input for the pricing engine.</param>
        /// <param name="useBestPricing">Indicates whether to use best pricing.</param>
        /// <param name="includeInvestorName">Indicates whether to include investor name.</param>
        /// <param name="fromConsumerPortal">Indicates whether this method was called from the consumer portal web services.</param>
        /// <returns>The QuickPricer results.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Legacy implementation variables kept with hungarian notation to remain consistent with the loan file")]
        internal string RunQuickPricerV1(AbstractUserPrincipal principal, string xmlData, bool useBestPricing, bool includeInvestorName, bool fromConsumerPortal)
        {
            return this.RunQuickPricerV1(principal, Tools.CreateXmlDoc(xmlData), useBestPricing, includeInvestorName, fromConsumerPortal);
        }

        /// <summary>
        /// Runs QuickPricer 1.0.
        /// </summary>
        /// <param name="principal">The user running QuickPricer.</param>
        /// <param name="xmlDocument">The XML input for the pricing engine.</param>
        /// <param name="useBestPricing">Indicates whether to use best pricing.</param>
        /// <param name="includeInvestorName">Indicates whether to include investor name.</param>
        /// <param name="fromConsumerPortal">Indicates whether this method was called from the consumer portal web services.</param>
        /// <returns>The QuickPricer results.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Legacy implementation variables kept with hungarian notation to remain consistent with the loan file")]
        internal string RunQuickPricerV1(AbstractUserPrincipal principal, XmlDocument xmlDocument, bool useBestPricing, bool includeInvestorName, bool fromConsumerPortal)
        {
            StringBuilder debugMessage = new StringBuilder();
            Stopwatch sw = Stopwatch.StartNew();
            BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);

            debugMessage.AppendLine("QuickPricer.asmx::RunQuickPricerV1 [" + principal.LoginNm + "]");
            debugMessage.AppendLine("=== REQUEST XML ===");
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.NewLineOnAttributes = true;
                settings.Indent = true;
                using (XmlWriter writer = XmlWriter.Create(debugMessage, settings))
                {
                    xmlDocument.WriteTo(writer);
                }
            }
            catch (FormatException)
            {
                debugMessage.Append(xmlDocument.ToString());
            }

            XmlElement loanElement = (XmlElement)xmlDocument.SelectSingleNode("//loan");
            bool isDebugMode = loanElement.GetAttribute("debug") == "true"; // 10/22/2010 dd - Secret attribute that will return more debug information.
            bool isIncludeIneligible = loanElement.GetAttribute("IncludeIneligible") == "True";

            CPageData templateLoan = new CQuickPricerLoanData(brokerDB.QuickPricerTemplateId);
            templateLoan.InitLoad();
            templateLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
            templateLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            QuickPricerLoanItem quickPricerLoanItem = new QuickPricerLoanItem();

            string sHouseValPe = loanElement.GetAttribute("sHouseValPe");
            string sLAmtCalcPe = loanElement.GetAttribute("sLAmtCalcPe");
            string sProOFinBalPe = loanElement.GetAttribute("sProOFinBalPe");

            quickPricerLoanItem.SetLoanInformation(sHouseValPe, sLAmtCalcPe, sProOFinBalPe);

            string sLPurposeTPe = loanElement.GetAttribute("sLPurposeTPe");
            if (string.IsNullOrEmpty(sLPurposeTPe))
            {
                quickPricerLoanItem.sLPurposeTPe = E_sLPurposeT.Purchase; // Default value.
            }
            else
            {
                quickPricerLoanItem.sLPurposeTPe = (E_sLPurposeT)Enum.Parse(typeof(E_sLPurposeT), sLPurposeTPe);
            }

            quickPricerLoanItem.sApprValPe_rep = loanElement.GetAttribute("sApprValPe");

            // 2/23/2015 ir - Add support of non-credit qualifying FHA Streamline / VA IRRRL, iOPM 202865
            bool? sIsCreditQualifying = loanElement.GetAttribute("sIsCreditQualifying")?.ToNullableBool();
            if (sIsCreditQualifying.HasValue)
            {
                quickPricerLoanItem.sIsCreditQualifying = sIsCreditQualifying.Value;
            }

            // 9/30/2011 dd - Base on 65859, sProdFilterTerm40Yrs is obsolete. This is for backward compatibility.
            bool isProdFilterTerm40Yrs = loanElement.GetAttribute("sProdFilterTerm40Yrs")?.ToNullableBool() ?? templateLoan.sProdFilterDue40Yrs;
            quickPricerLoanItem.sProdFilterDueOther = isProdFilterTerm40Yrs || (loanElement.GetAttribute("sProdFilterTermOther")?.ToNullableBool() ?? templateLoan.sProdFilterDueOther);

            quickPricerLoanItem.sProdFilterDue10Yrs = loanElement.GetAttribute("sProdFilterTerm10Yrs")?.ToNullableBool() ?? templateLoan.sProdFilterDue10Yrs;
            quickPricerLoanItem.sProdFilterDue15Yrs = loanElement.GetAttribute("sProdFilterTerm15Yrs")?.ToNullableBool() ?? templateLoan.sProdFilterDue15Yrs;
            quickPricerLoanItem.sProdFilterDue20Yrs = loanElement.GetAttribute("sProdFilterTerm20Yrs")?.ToNullableBool() ?? templateLoan.sProdFilterDue20Yrs;
            quickPricerLoanItem.sProdFilterDue25Yrs = loanElement.GetAttribute("sProdFilterTerm25Yrs")?.ToNullableBool() ?? templateLoan.sProdFilterDue25Yrs;
            quickPricerLoanItem.sProdFilterDue30Yrs = loanElement.GetAttribute("sProdFilterTerm30Yrs")?.ToNullableBool() ?? templateLoan.sProdFilterDue30Yrs;

            quickPricerLoanItem.sProdFilterFinMethFixed = loanElement.GetAttribute("sProdFilterFinMethFixed")?.ToNullableBool() ?? templateLoan.sProdFilterFinMethFixed;

            bool isProdFilterFinMethLess1YrArm = loanElement.GetAttribute("sProdFilterFinMethLess1YrArm")?.ToNullableBool() ?? templateLoan.sProdFilterFinMethLess1YrArm;
            bool isProdFilterFinMeth2YrsArm = loanElement.GetAttribute("sProdFilterFinMeth2YrsArm")?.ToNullableBool() ?? templateLoan.sProdFilterFinMeth2YrsArm;

            quickPricerLoanItem.sProdFilterFinMethOther = isProdFilterFinMethLess1YrArm || isProdFilterFinMeth2YrsArm || (loanElement.GetAttribute("sProdFilterFinMethOther")?.ToNullableBool() ?? templateLoan.sProdFilterFinMethOther);

            quickPricerLoanItem.sProdFilterFinMeth3YrsArm = loanElement.GetAttribute("sProdFilterFinMeth3YrsArm")?.ToNullableBool() ?? templateLoan.sProdFilterFinMeth3YrsArm;
            quickPricerLoanItem.sProdFilterFinMeth5YrsArm = loanElement.GetAttribute("sProdFilterFinMeth5YrsArm")?.ToNullableBool() ?? templateLoan.sProdFilterFinMeth5YrsArm;
            quickPricerLoanItem.sProdFilterFinMeth7YrsArm = loanElement.GetAttribute("sProdFilterFinMeth7YrsArm")?.ToNullableBool() ?? templateLoan.sProdFilterFinMeth7YrsArm;
            quickPricerLoanItem.sProdFilterFinMeth10YrsArm = loanElement.GetAttribute("sProdFilterFinMeth10YrsArm")?.ToNullableBool() ?? templateLoan.sProdFilterFinMeth10YrsArm;

            quickPricerLoanItem.sProdFilterPmtTPI = loanElement.GetAttribute("sProdFilterPmtTPI")?.ToNullableBool() ?? templateLoan.sProdFilterPmtTPI;
            quickPricerLoanItem.sProdFilterPmtTIOnly = loanElement.GetAttribute("sProdFilterPmtTIOnly")?.ToNullableBool() ?? templateLoan.sProdFilterPmtTIOnly;

            quickPricerLoanItem.sProdIncludeNormalProc = loanElement.GetAttribute("sProdIncludeNormalProc")?.ToNullableBool() ?? templateLoan.sProdIncludeNormalProc;

            // OPM 48947
            quickPricerLoanItem.sProdIsDuRefiPlus = loanElement.GetAttribute("sProdIsDuRefiPlus")?.ToNullableBool() ?? templateLoan.sProdIsDuRefiPlus;
            string isNormalProc = loanElement.GetAttribute("sProdIncludeNormalProc");
            if (quickPricerLoanItem.sProdIsDuRefiPlus)
            {
                if (string.IsNullOrEmpty(isNormalProc) == false && isNormalProc.ToLower().Equals("false"))
                {
                    string msg = "Error parsing Quick Pricer request XML data: sProdIncludeNormalProc cannot be false if sProdIsDuRefiPlus is true.";
                    throw new CBaseException(msg, msg);
                }
                else
                {
                    quickPricerLoanItem.sProdIncludeNormalProc = true;
                }
            }

            quickPricerLoanItem.sProdIncludeMyCommunityProc = loanElement.GetAttribute("sProdIncludeMyCommunityProc")?.ToNullableBool() ?? templateLoan.sProdIncludeMyCommunityProc;
            quickPricerLoanItem.sProdIncludeHomePossibleProc = loanElement.GetAttribute("sProdIncludeHomePossibleProc")?.ToNullableBool() ?? templateLoan.sProdIncludeHomePossibleProc;
            quickPricerLoanItem.sProdIncludeFHATotalProc = loanElement.GetAttribute("sProdIncludeFHATotalProc")?.ToNullableBool() ?? templateLoan.sProdIncludeFHATotalProc;
            quickPricerLoanItem.sProdIncludeVAProc = loanElement.GetAttribute("sProdIncludeVAProc")?.ToNullableBool() ?? templateLoan.sProdIncludeVAProc;
            quickPricerLoanItem.sProdIncludeUSDARuralProc = loanElement.GetAttribute("sProdIncludeUSDARuralProc")?.ToNullableBool() ?? templateLoan.sProdIncludeUSDARuralProc;
            quickPricerLoanItem.sProdRLckdDays_rep = loanElement.GetAttribute("sProdRLckdDays");
            quickPricerLoanItem.sCreditScoreType1_rep = loanElement.GetAttribute("sCreditScoreType1");
            quickPricerLoanItem.sProdImpound = loanElement.GetAttribute("sProdImpound")?.ToNullableBool() ?? templateLoan.sProdImpound;

            string sProdMIOptionT = loanElement.GetAttribute("sProdMIOptionT");
            if (!string.IsNullOrEmpty(sProdMIOptionT))
            {
                quickPricerLoanItem.sProdMIOptionT = (E_sProdMIOptionT)Enum.Parse(typeof(E_sProdMIOptionT), sProdMIOptionT);
            }
            else
            {
                if (quickPricerLoanItem.sLtv80TestResultT == E_sLtv80TestResultT.Over80)
                {
                    // 9/11/2013 dd - Assume Borrower Paid PMI when it is not set explicitly.
                    quickPricerLoanItem.sProdMIOptionT = E_sProdMIOptionT.BorrowerPdPmi;
                }
            }

            string sProdConvMIOptionT = loanElement.GetAttribute("sProdConvMIOptionT");
            if (string.IsNullOrEmpty(sProdConvMIOptionT) == false)
            {
                quickPricerLoanItem.sProdConvMIOptionT = (E_sProdConvMIOptionT)Enum.Parse(typeof(E_sProdConvMIOptionT), sProdConvMIOptionT);
            }
            else
            {
                if (quickPricerLoanItem.sLtv80TestResultT == E_sLtv80TestResultT.Over80)
                {
                    // 9/11/2013 dd - Assume Borrower Paid PMI when it is not set explicitly.
                    quickPricerLoanItem.sProdConvMIOptionT = E_sProdConvMIOptionT.BorrPaidMonPrem;
                }
            }

            string sConvSplitMIRT = loanElement.GetAttribute("sConvSplitMIRT");
            if (string.IsNullOrEmpty(sConvSplitMIRT) == false)
            {
                quickPricerLoanItem.sConvSplitMIRT = (E_sConvSplitMIRT)Enum.Parse(typeof(E_sConvSplitMIRT), sConvSplitMIRT);
            }

            string sOccTPe = loanElement.GetAttribute("sOccTPe");
            if (string.IsNullOrEmpty(sOccTPe))
            {
                quickPricerLoanItem.sOccTPe = E_sOccT.PrimaryResidence;
            }
            else
            {
                quickPricerLoanItem.sOccTPe = (E_sOccT)Enum.Parse(typeof(E_sOccT), loanElement.GetAttribute("sOccTPe"));
            }

            string sProdSpT = loanElement.GetAttribute("sProdSpT");
            if (string.IsNullOrEmpty(sProdSpT))
            {
                quickPricerLoanItem.sProdSpT = E_sProdSpT.SFR;
            }
            else
            {
                quickPricerLoanItem.sProdSpT = (E_sProdSpT)Enum.Parse(typeof(E_sProdSpT), loanElement.GetAttribute("sProdSpT"));
            }

            quickPricerLoanItem.sSpStatePe = loanElement.GetAttribute("sSpStatePe");
            quickPricerLoanItem.sSpCounty = loanElement.GetAttribute("sSpCounty");
            quickPricerLoanItem.sSpZip = loanElement.GetAttribute("sSpZip");

            string sProd3rdPartyUwResultT = loanElement.GetAttribute("sProd3rdPartyUwResultT");
            if (string.IsNullOrEmpty(sProd3rdPartyUwResultT) == false)
            {
                quickPricerLoanItem.sProd3rdPartyUwResultT = (E_sProd3rdPartyUwResultT)Enum.Parse(typeof(E_sProd3rdPartyUwResultT), sProd3rdPartyUwResultT);
            }

            string useLoanValueForsProd3rdPartyUwResultT = loanElement.GetAttribute("UseLoanValueForsProd3rdPartyUwResultT");
            if (!string.IsNullOrEmpty(useLoanValueForsProd3rdPartyUwResultT))
            {
                quickPricerLoanItem.UseLoanValueForsProd3rdPartyUwResultT = bool.Parse(useLoanValueForsProd3rdPartyUwResultT);
            }

            // 10/28/2011 dd - sPrimAppTotNonspIPe and sTransmOMonPmtPe are add to quick pricer API to accommodate
            // Pacific Union request.
            quickPricerLoanItem.sPrimAppTotNonspIPe_rep = loanElement.GetAttribute("sPrimAppTotNonspIPe");
            quickPricerLoanItem.sTransmOMonPmtPe_rep = loanElement.GetAttribute("sTransmOMonPmtPe");

            // 10/17/2013 dd - OPM 136671 - Exposed Number of Financed Properties for quick pricer.
            quickPricerLoanItem.sNumFinancedProperties = loanElement.GetAttribute("sNumFinancedProperties");

            quickPricerLoanItem.sProOHExpPe_rep = loanElement.GetAttribute("sProOHExpPe");
            quickPricerLoanItem.sProdCondoStories_rep = loanElement.GetAttribute("sProdCondoStories");
            string sFhaCondoApprovalStatusT = loanElement.GetAttribute("sFhaCondoApprovalStatusT");
            if (string.IsNullOrEmpty(sFhaCondoApprovalStatusT) == false)
            {
                quickPricerLoanItem.sFhaCondoApprovalStatusT = (E_sFhaCondoApprovalStatusT)Enum.Parse(typeof(E_sFhaCondoApprovalStatusT), sFhaCondoApprovalStatusT);
            }

            string aProdBCitizenT = loanElement.GetAttribute("aProdBCitizenT");
            if (string.IsNullOrEmpty(aProdBCitizenT) == false)
            {
                quickPricerLoanItem.aProdBCitizenT = (E_aProdCitizenT)Enum.Parse(typeof(E_aProdCitizenT), aProdBCitizenT);
            }

            quickPricerLoanItem.sProdIsSpInRuralArea = loanElement.GetAttribute("sProdIsSpInRuralArea")?.ToNullableBool();
            quickPricerLoanItem.sProdIsCondotel = loanElement.GetAttribute("sProdIsCondotel")?.ToNullableBool();
            quickPricerLoanItem.sProdIsNonwarrantableProj = loanElement.GetAttribute("sProdIsNonwarrantableProj")?.ToNullableBool();
            quickPricerLoanItem.sProdIsFhaMipFinanced = loanElement.GetAttribute("sProdIsFhaMipFinanced")?.ToNullableBool();
            quickPricerLoanItem.sProdIsVaFundingFinanced = loanElement.GetAttribute("sProdIsVaFundingFinanced")?.ToNullableBool();
            quickPricerLoanItem.sProdIsUsdaRuralHousingFeeFinanced = loanElement.GetAttribute("sProdIsUsdaRuralHousingFeeFinanced")?.ToNullableBool();
            quickPricerLoanItem.sIsSelfEmployed = loanElement.GetAttribute("sIsSelfEmployed")?.ToNullableBool();

            // 8/27/2012 dd - The sGfeIsTPOTransaction is hijack to use as "Is HomeFixer" for REMN PML0200
            quickPricerLoanItem.sGfeIsTPOTransaction = loanElement.GetAttribute("sGfeIsTPOTransaction")?.ToNullableBool();

            // 4/2/2014 ir - sLenderFeeBuyoutRequested Required for REMN PML0200, OPM 175882
            // Kept for legacy support
            bool? sLenderFeeBuyoutRequested = loanElement.GetAttribute("sLenderFeeBuyoutRequested").ToNullableBool();
            if (sLenderFeeBuyoutRequested.HasValue)
            {
                quickPricerLoanItem.sLenderFeeBuyoutRequestedT = sLenderFeeBuyoutRequested.Value ? E_sLenderFeeBuyoutRequestedT.Yes : E_sLenderFeeBuyoutRequestedT.No;
            }

            // 8/13/2014 je - sLenderFeeBuyoutRequested changed to sLenderFeeBuyoutRequestedT. OPM 186233
            string sLenderFeeBuyoutRequestedT = loanElement.GetAttribute("sLenderFeeBuyoutRequestedT");
            if (string.IsNullOrEmpty(sLenderFeeBuyoutRequestedT) == false)
            {
                quickPricerLoanItem.sLenderFeeBuyoutRequestedT = (E_sLenderFeeBuyoutRequestedT)Enum.Parse(typeof(E_sLenderFeeBuyoutRequestedT), sLenderFeeBuyoutRequestedT);
            }

            // 12/12/2012 BB - Stockton QP; will be used by the lock desk and needs to be non-rate merge w/investor name (opm 107321)
            quickPricerLoanItem.bUseBestPricing = useBestPricing;

            // 7/10/2013 dd - eOPM 441230 & e442278 - Add support for standalone second in Quick Pricer.
            string sLienPosTPe = loanElement.GetAttribute("sLienPosTPe");
            if (string.IsNullOrEmpty(sLienPosTPe) == false)
            {
                quickPricerLoanItem.sLienPosTPe = (E_sLienPosT)Enum.Parse(typeof(E_sLienPosT), sLienPosTPe);
            }

            string sSubFinT = loanElement.GetAttribute("sSubFinT");
            if (string.IsNullOrEmpty(sSubFinT) == false)
            {
                quickPricerLoanItem.sSubFinT = (E_sSubFinT)Enum.Parse(typeof(E_sSubFinT), sSubFinT);
            }

            string sSubFinPe = loanElement.GetAttribute("sSubFinPe");
            if (string.IsNullOrEmpty(sSubFinPe) == false)
            {
                quickPricerLoanItem.sSubFinPe_rep = sSubFinPe;
            }

            bool? sIsIOnlyPe = loanElement.GetAttribute("sIsIOnlyPe")?.ToNullableBool();
            if (sIsIOnlyPe.HasValue)
            {
                quickPricerLoanItem.sIsIOnlyPe = sIsIOnlyPe.Value;
            }

            if (loanElement.HasAttribute("sLeadSrcDesc"))
            {
                quickPricerLoanItem.sLeadSrcDesc = loanElement.GetAttribute("sLeadSrcDesc");
            }

            if (loanElement.HasAttribute("sCaseAssignmentD"))
            {
                quickPricerLoanItem.sCaseAssignmentD = loanElement.GetAttribute("sCaseAssignmentD");
            }

            if (loanElement.HasAttribute("sCustomPMLField1"))
            {
                quickPricerLoanItem.sCustomPMLField1 = loanElement.GetAttribute("sCustomPMLField1");
            }

            if (loanElement.HasAttribute("sCustomPMLField2"))
            {
                quickPricerLoanItem.sCustomPMLField2 = loanElement.GetAttribute("sCustomPMLField2");
            }

            if (loanElement.HasAttribute("sCustomPMLField3"))
            {
                quickPricerLoanItem.sCustomPMLField3 = loanElement.GetAttribute("sCustomPMLField3");
            }

            if (loanElement.HasAttribute("sCustomPMLField4"))
            {
                quickPricerLoanItem.sCustomPMLField4 = loanElement.GetAttribute("sCustomPMLField4");
            }

            if (loanElement.HasAttribute("sCustomPMLField5"))
            {
                quickPricerLoanItem.sCustomPMLField5 = loanElement.GetAttribute("sCustomPMLField5");
            }

            if (loanElement.HasAttribute("sCustomPMLField6"))
            {
                quickPricerLoanItem.sCustomPMLField6 = loanElement.GetAttribute("sCustomPMLField6");
            }

            if (loanElement.HasAttribute("sCustomPMLField7"))
            {
                quickPricerLoanItem.sCustomPMLField7 = loanElement.GetAttribute("sCustomPMLField7");
            }

            if (loanElement.HasAttribute("sCustomPMLField8"))
            {
                quickPricerLoanItem.sCustomPMLField8 = loanElement.GetAttribute("sCustomPMLField8");
            }

            if (loanElement.HasAttribute("sCustomPMLField9"))
            {
                quickPricerLoanItem.sCustomPMLField9 = loanElement.GetAttribute("sCustomPMLField9");
            }

            if (loanElement.HasAttribute("sCustomPMLField10"))
            {
                quickPricerLoanItem.sCustomPMLField10 = loanElement.GetAttribute("sCustomPMLField10");
            }

            if (loanElement.HasAttribute("sCustomPMLField11"))
            {
                quickPricerLoanItem.sCustomPMLField11 = loanElement.GetAttribute("sCustomPMLField11");
            }

            if (loanElement.HasAttribute("sCustomPMLField12"))
            {
                quickPricerLoanItem.sCustomPMLField12 = loanElement.GetAttribute("sCustomPMLField12");
            }

            if (loanElement.HasAttribute("sCustomPMLField13"))
            {
                quickPricerLoanItem.sCustomPMLField13 = loanElement.GetAttribute("sCustomPMLField13");
            }

            if (loanElement.HasAttribute("sCustomPMLField14"))
            {
                quickPricerLoanItem.sCustomPMLField14 = loanElement.GetAttribute("sCustomPMLField14");
            }

            if (loanElement.HasAttribute("sCustomPMLField15"))
            {
                quickPricerLoanItem.sCustomPMLField15 = loanElement.GetAttribute("sCustomPMLField15");
            }

            if (loanElement.HasAttribute("sCustomPMLField16"))
            {
                quickPricerLoanItem.sCustomPMLField16 = loanElement.GetAttribute("sCustomPMLField16");
            }

            if (loanElement.HasAttribute("sCustomPMLField17"))
            {
                quickPricerLoanItem.sCustomPMLField17 = loanElement.GetAttribute("sCustomPMLField17");
            }

            if (loanElement.HasAttribute("sCustomPMLField18"))
            {
                quickPricerLoanItem.sCustomPMLField18 = loanElement.GetAttribute("sCustomPMLField18");
            }

            if (loanElement.HasAttribute("sCustomPMLField19"))
            {
                quickPricerLoanItem.sCustomPMLField19 = loanElement.GetAttribute("sCustomPMLField19");
            }

            if (loanElement.HasAttribute("sCustomPMLField20"))
            {
                quickPricerLoanItem.sCustomPMLField20 = loanElement.GetAttribute("sCustomPMLField20");
            }

            // OPM 187119
            if (loanElement.HasAttribute("sEstCloseD"))
            {
                quickPricerLoanItem.sEstCloseD_rep = loanElement.GetAttribute("sEstCloseD");
            }
            else if (!string.IsNullOrEmpty(templateLoan.sEstCloseD_rep))
            {
                quickPricerLoanItem.sEstCloseD_rep = templateLoan.sEstCloseD_rep;
            }

            // OPM 220009
            if (loanElement.HasAttribute("sAppTotLiqAsset"))
            {
                quickPricerLoanItem.sAppTotLiqAsset_rep = loanElement.GetAttribute("sAppTotLiqAsset");
            }

            // 2/3/2016 BS - Case 237125. Set QuickPricerLoanItem FromConsumerPortal
            quickPricerLoanItem.FromConsumerPortal = fromConsumerPortal;

            // OPM 236392 - Allow caller to suppress fee service.
            quickPricerLoanItem.DisableFeeService = loanElement.GetAttribute("disableFeeService")?.ToNullableBool() ?? false;

            QuickPricerResult quickPricerResult;
            try
            {
                quickPricerResult = QuickPricerEngine.SubmitSynchronous(principal, quickPricerLoanItem, isDebugMode);
            }
            catch (TimeoutException e)
            {
                var exception = new CBaseException(ErrorMessages.WebServices.QuickPricerTimeout, e);
                exception.IsEmailDeveloper = false;
                throw exception;
            }

            XmlDocument resultDocument = new XmlDocument();

            XmlElement pricingResultsElement = resultDocument.CreateElement("PricingResults");
            resultDocument.AppendChild(pricingResultsElement);

            if (isDebugMode)
            {
                XmlElement debugElement = resultDocument.CreateElement("Debug");
                pricingResultsElement.AppendChild(debugElement);
                debugElement.AppendChild(resultDocument.CreateCDataSection(quickPricerResult.DebugInfo));
            }

            if (quickPricerResult.HasError)
            {
                pricingResultsElement.SetAttribute("Status", "Error");
                XmlElement errorElement = resultDocument.CreateElement("Error");
                pricingResultsElement.AppendChild(errorElement);
                errorElement.SetAttribute("Message", quickPricerResult.ErrorMessage);
            }
            else
            {
                Dictionary<Guid, CApplicantPriceXml> uniqueApplicantPriceInResult = new Dictionary<Guid, CApplicantPriceXml>();
                foreach (var o in quickPricerResult.LoanPrograms)
                {
                    XmlElement programElement = resultDocument.CreateElement("Program");
                    pricingResultsElement.AppendChild(programElement);

                    programElement.SetAttribute("Name", o.Name);
                    programElement.SetAttribute("RateExpired", o.IsBlockedRateLockSubmission ? "True" : "False");
                    programElement.SetAttribute("Term", o.Term.ToString());
                    programElement.SetAttribute("ProductType", o.ProductType);
                    programElement.SetAttribute("FinMethT", o.FinMethT.ToString());
                    programElement.SetAttribute("Due", o.Due.ToString());
                    programElement.SetAttribute("RAdj1stCapMon", o.RAdj1stCapMon.ToString());
                    if (includeInvestorName == true && !string.IsNullOrEmpty(o.InvestorName))
                    {
                        programElement.SetAttribute("Investor", o.InvestorName);
                    }

                    foreach (var rateOption in o.RateOptions)
                    {
                        XmlElement rateOptionElement = resultDocument.CreateElement("RateOption");
                        programElement.AppendChild(rateOptionElement);

                        rateOptionElement.SetAttribute("Rate", rateOption.Rate);
                        rateOptionElement.SetAttribute("Point", rateOption.Point);
                        rateOptionElement.SetAttribute("Payment", rateOption.Payment);
                        rateOptionElement.SetAttribute("Margin", rateOption.Margin);
                        rateOptionElement.SetAttribute("lLpTemplateId", rateOption.lLpTemplateId.ToString());

                        // OPM 122257. If in PML2.0 mode, show PML2.0 fields in XML.
                        if (brokerDB.IsNewPmlUIEnabled)
                        {
                            rateOptionElement.SetAttribute("APR", rateOption.Apr);

                            rateOptionElement.SetAttribute("TotalClosingCost", rateOption.ClosingCost);
                            rateOptionElement.SetAttribute("PrepaidClosingCost", rateOption.PrepaidCost);
                            rateOptionElement.SetAttribute("NonPrepaidClosingCost", rateOption.NonPrepaidCost);
                            rateOptionElement.SetAttribute("BreakEven", rateOption.BreakEven);
                            rateOptionElement.SetAttribute("Reserves", rateOption.Reserves);
                            rateOptionElement.SetAttribute("DTI", rateOption.Dti);

                            if (rateOption.ClosingCostBreakDown != null && rateOption.ClosingCostBreakDown.Count() != 0)
                            {
                                XmlElement feeList = resultDocument.CreateElement("ClosingCostList");
                                rateOptionElement.AppendChild(feeList);
                                foreach (var fee in rateOption.ClosingCostBreakDown.OrderBy(p => int.Parse(p.HudLine)))
                                {
                                    XmlElement feeEl = resultDocument.CreateElement("ClosingCost");
                                    feeEl.SetAttribute("Line", fee.HudLine);
                                    feeEl.SetAttribute("Description", fee.Description);
                                    feeEl.SetAttribute("Amount", fee.Fee.ToString("C"));
                                    feeList.AppendChild(feeEl);
                                }
                            }
                        }

                        if (uniqueApplicantPriceInResult.ContainsKey(rateOption.lLpTemplateId) == false)
                        {
                            uniqueApplicantPriceInResult.Add(rateOption.lLpTemplateId, quickPricerResult.GetApplicantPrice(rateOption.lLpTemplateId));
                        }
                    }
                }

                if (isIncludeIneligible)
                {
                    XmlElement ineligibleProgramListElement = resultDocument.CreateElement("IneligibleProgramList");
                    pricingResultsElement.AppendChild(ineligibleProgramListElement);

                    foreach (var o in quickPricerResult.IneligibleLoanProgramList)
                    {
                        XmlElement programElement = resultDocument.CreateElement("IneligibleProgram");
                        ineligibleProgramListElement.AppendChild(programElement);
                        programElement.SetAttribute("Name", o.lLpTemplateNm);
                        programElement.SetAttribute("lLpTemplateId", o.lLpTemplateId.ToString());
                        foreach (var denialReason in o.DisqualifiedRuleList)
                        {
                            XmlElement denialReasonElement = resultDocument.CreateElement("DenialReason");
                            programElement.AppendChild(denialReasonElement);
                            denialReasonElement.SetAttribute("Description", denialReason);
                        }
                    }
                }

                // 9/7/2011 dd - Add adjustment.
                XmlElement adjustmentListElement = resultDocument.CreateElement("AdjustmentsTable");
                pricingResultsElement.AppendChild(adjustmentListElement);
                foreach (CApplicantPriceXml applicantPrice in uniqueApplicantPriceInResult.Values)
                {
                    if (applicantPrice == null)
                    {
                        continue;
                    }

                    XmlElement adjustment = resultDocument.CreateElement("Adjustment");
                    adjustmentListElement.AppendChild(adjustment);
                    adjustment.SetAttribute("lLpTemplateId", applicantPrice.lLpTemplateId.ToString());
                    adjustment.SetAttribute("lLpTemplateNm", applicantPrice.lLpTemplateNm);
                    foreach (CAdjustItem item in applicantPrice.AdjustDescs)
                    {
                        XmlElement adjustmentItem = resultDocument.CreateElement("AdjustmentItem");
                        adjustment.AppendChild(adjustmentItem);
                        adjustmentItem.SetAttribute("Rate", item.Rate);
                        adjustmentItem.SetAttribute("Point", item.Fee);
                        adjustmentItem.SetAttribute("Margin", item.Margin);
                        adjustmentItem.SetAttribute("Description", item.Description);
                    }
                }
            }

            if (quickPricerResult != null)
            {
                sw.Stop();
                DistributeUnderwritingEngine.RecordPricingTotalTime(quickPricerResult.RequestId, "API_RunQuickPricer", (int)sw.ElapsedMilliseconds);
            }

            try
            {
                XDocument xdoc = XDocument.Parse(resultDocument.OuterXml);
                debugMessage.AppendLine();
                debugMessage.AppendLine("=== RESPONSE XML ===");
                debugMessage.AppendLine(xdoc.ToString(SaveOptions.None));
            }
            catch (FormatException)
            {
                debugMessage.Append(resultDocument.OuterXml);
            }

            debugMessage.AppendLine();
            debugMessage.AppendLine("Execution in " + sw.ElapsedMilliseconds + "ms.");

            Tools.LogInfo(debugMessage.ToString());
            return resultDocument.OuterXml;
        }

        /// <summary>
        /// Creates the pricing result element for use in the response.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="pricingXml">The pricing results XML.</param>
        /// <param name="importResult">The result fromt he LOFormatImporter call.</param>
        /// <returns>The XElement to include in the response.</returns>
        private static XElement CreateQuickPricerResponseXml(Guid loanId, XmlDocument pricingXml, LoFormatImporterResult importResult)
        {
            var idElement = new XElement("field");
            idElement.SetAttributeValue("id", "sLId");
            idElement.SetValue(loanId);

            var pricingResults = new XElement("field");
            pricingResults.SetAttributeValue("id", "PricingResults");
            pricingResults.SetValue(pricingXml.OuterXml);

            var loan = new XElement("loan", idElement, pricingResults);
            loan.SetAttributeValue("isQuickPricer", "True");
            foreach (var outputField in importResult.FieldsToOutput)
            {
                loan.Add(outputField.Value);
            }

            return loan;
        }

        /// <summary>
        /// Creates a lead or loan from a QuickPricer file.
        /// </summary>
        /// <param name="authorizationTicket">The authentication ticket.</param>
        /// <param name="quickPricerLoanId">The QuickPricer file ID.</param>
        /// <param name="templateName">The name of the template to create from.</param>
        /// <param name="convertingQpFileToLead">
        /// True if the QP file is being converted to a lead, false if it is being converted to a loan.
        /// </param>
        /// <returns>An LOXML response holding either the loan number of the converted file or an error message.</returns>
        private static string ConvertQuickPricerFile(string authorizationTicket, string quickPricerLoanId, string templateName, bool convertingQpFileToLead)
        {
            AbstractUserPrincipal principal;

            try
            {
                principal = AuthServiceHelper.GetPrincipalFromTicket(authorizationTicket, null);
            }
            catch (AccessDenied)
            {
                return ErrorResponse(ErrorMessages.WebServices.InvalidAuthTicket);
            }

            if (principal == null)
            {
                return ErrorResponse(ErrorMessages.WebServices.InvalidAuthTicket);
            }

            CallVolume.IncreaseAndCheck(principal);

            if (!CanUserCreateFiles(principal, convertingQpFileToLead))
            {
                return ErrorResponse(ErrorMessages.WebServices.CannotCreateFiles(convertingQpFileToLead));
            }

            if (string.IsNullOrEmpty(templateName) && IsTemplateRequired(principal, convertingQpFileToLead))
            {
                return ErrorResponse(ErrorMessages.WebServices.TemplateRequiredToCreateLoan);
            }

            CPageData dataLoan;
            if (!TryGetLoanFile(quickPricerLoanId, out dataLoan) || dataLoan.sLoanFileT != E_sLoanFileT.QuickPricer2Sandboxed)
            {
                return ErrorResponse(ErrorMessages.WebServices.InvalidQuickPricerLoan(quickPricerLoanId));
            }

            CPageData templateData = null;
            if (!string.IsNullOrEmpty(templateName) && !TryGetTemplate(templateName, principal, out templateData))
            {
                return ErrorResponse(ErrorMessages.WebServices.CannotFindLoanTemplate(templateName));
            }

            if (templateData != null && !Tools.PurposeMatchesTransactionType(dataLoan.sIsRefinancing, templateData.sLPurposeT))
            {
                return ErrorResponse(ErrorMessages.WebServices.TemplatePurposeDoesNotMatchQpLoan(templateName));
            }

            var parameters = new LeadConversionParameters();
            parameters.LeadId = dataLoan.sLId;
            parameters.FileVersion = dataLoan.sFileVersion;
            parameters.CreateFromTemplate = templateData != null && templateData.sLId != Guid.Empty;
            parameters.TemplateId = templateData?.sLId ?? Guid.Empty;
            parameters.RemoveLeadPrefix = false;
            parameters.ConvertFromQp2File = true;
            parameters.ConvertToLead = convertingQpFileToLead;
            parameters.IsEmbeddedPML = true;
            parameters.IsNonQm = false;

            var results = new LeadConversionResults();
            try
            {
                results = LeadConversion.ConvertLeadToLoan_Pml(principal, parameters);
            }
            catch (SystemException)
            {
                return ErrorResponse(ErrorMessages.UnableToConvertLead);
            }
            catch (CBaseException exception)
            {
                return ErrorResponse(exception.UserMessage);
            }

            if (string.IsNullOrEmpty(results.ConvertedLoanNumber))
            {
                return ErrorResponse(ErrorMessages.UnableToConvertLead);
            }

            return GenerateCreateFileResponse(results.ConvertedLoanNumber);
        }

        /// <summary>
        /// Generates an LOXML response notifying the user of the loan number for
        /// the newly created lead or loan.
        /// </summary>
        /// <param name="loanNumber">The loan number for the new file.</param>
        /// <returns>A serialized LOXML response string.</returns>
        private static string GenerateCreateFileResponse(string loanNumber)
        {  
            var loanNumberElement = new XElement("field");
            loanNumberElement.SetAttributeValue("id", "sLNm");
            loanNumberElement.SetValue(loanNumber);

            var loanElement = new XElement("loan");
            loanElement.Add(loanNumberElement);

            var serviceResult = new LoXmlServiceResult();
            serviceResult.AddResultElement(loanElement);
            return serviceResult.ToResponse();
        }

        /// <summary>
        /// Determines whether a template is required for creating a file.
        /// </summary>
        /// <param name="principal">The user attempting to create a file.</param>
        /// <param name="convertingQpFileToLead">Indicates whether a QP file is being converted to a lead.</param>
        /// <returns>A boolean indicating whether a template is required to create a lead or loan.</returns>
        private static bool IsTemplateRequired(AbstractUserPrincipal principal, bool convertingQpFileToLead)
        {
            return !convertingQpFileToLead && 
                principal.Type.Equals("B") && 
                principal.BrokerDB.IsForceLeadToLoanTemplate;
        }

        /// <summary>
        /// Determines whether the given template name is valid for the user.
        /// </summary>
        /// <param name="templateName">The template name passed into the web service.</param>
        /// <param name="principal">The user calling the web service.</param>
        /// <param name="templateData">The template loan object to be assigned.</param>
        /// <returns>A boolean indicating whether the template name is valid for the user.</returns>
        private static bool TryGetTemplate(string templateName, AbstractUserPrincipal principal, out CPageData templateData)
        {
            Guid templateId = Tools.GetLoanIdByLoanName(principal.BrokerId, templateName);
            templateData = null;

            return templateId != Guid.Empty
                && TryGetLoanFile(templateId.ToString(), out templateData)
                && (principal.BranchId == templateData.sBranchId
                || principal.HasPermission(Permission.AllowAccessToTemplatesOfAnyBranch));
        }

        /// <summary>
        /// Determines whether a file ID corresponds to a valid QuickPricer file
        /// that can be accessed by the current principal.
        /// </summary>
        /// <param name="fileId">The file ID submitted via web service.</param>
        /// <param name="dataLoan">The QuickPricer loan object.</param>
        /// <returns>A boolean indicating whether the ID corresponds to a valid QP file for the user.</returns>
        private static bool TryGetLoanFile(string fileId, out CPageData dataLoan)
        {
            try
            {
                Guid parsedId = Guid.Parse(fileId);
                dataLoan = CPageData.CreateUsingSmartDependency(parsedId, typeof(QuickPricer));
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();

                return true;
            }
            catch (SystemException)
            {
                dataLoan = null;
                return false;
            }
            catch (CBaseException)
            {
                dataLoan = null;
                return false;
            }
        }

        /// <summary>
        /// Determines whether the user has permission to create new files.
        /// </summary>
        /// <param name="principal">The user attempting to create a file.</param>
        /// <param name="convertingQpFileToLead">
        /// True if the user is creating a lead from a QP file, false if the user is creating a loan.
        /// </param>
        /// <returns>A boolean indicating whether the user has permission to create a file.</returns>
        private static bool CanUserCreateFiles(AbstractUserPrincipal principal, bool convertingQpFileToLead)
        {
            if (convertingQpFileToLead)
            {
                return principal.HasPermission(Permission.AllowCreatingNewLeadFiles);
            }

            if (!principal.Type.Equals("P", StringComparison.OrdinalIgnoreCase))
            {
                return principal.HasPermission(Permission.AllowCreatingNewLoanFiles);
            }

            bool brokerUserCanCreateLoans = principal.PortalMode == E_PortalMode.Broker
                && principal.HasPermission(Permission.AllowCreatingWholesaleChannelLoans);

            bool correspondentUserCanCreateLoans = principal.PortalMode == E_PortalMode.Correspondent
                && principal.HasPermission(Permission.AllowCreatingCorrChannelLoans);

            bool miniCorrespondentUserCanCreateLoans = principal.PortalMode == E_PortalMode.MiniCorrespondent
                && principal.HasPermission(Permission.AllowCreatingMiniCorrChannelLoans);

            return principal.BrokerDB.IsAllowExternalUserToCreateNewLoan
                && (brokerUserCanCreateLoans || correspondentUserCanCreateLoans || miniCorrespondentUserCanCreateLoans);
        }

        /// <summary>
        /// Serializes an LOXML error response with the given message.
        /// </summary>
        /// <param name="errorMessage">The error message to wrap in LOXML.</param>
        /// <returns>A serialized error response.</returns>
        private static string ErrorResponse(string errorMessage)
        {
            var result = new ErrorServiceResult(errorMessage, includeLoXmlFormatRootTag: true);
            return result.ToResponse();
        }

        /// <summary>
        /// Serializes an LOXML error response with the given error messages.
        /// </summary>
        /// <param name="errorMessages">The error messages.</param>
        /// <returns>A serialized error response.</returns>
        private static string ErrorResponse(IEnumerable<string> errorMessages)
        {
            var result = new ErrorServiceResult(errorMessages, includeLoXmlFormatRootTag: true);
            return result.ToResponse();
        }
    }
}
