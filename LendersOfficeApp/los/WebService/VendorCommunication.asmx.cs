﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.Web.Services;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.Integration.VendorCommunication;
    using LendersOffice.Integration.VendorCommunication.Appraisal;
    using LendersOffice.ObjLib.Conversions.LQBAppraisal;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.Security;
    using LqbCommunication;

    /// <summary>
    /// Container for methods used to access the Bi-Directional Communication plugin for integration frameworks.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class VendorCommunication : WebService
    {
        /// <summary>
        /// Retrieves all vendor messages associated with a given loan.
        /// </summary>
        /// <param name="authTicket">An authentication ticket.</param>
        /// <param name="loanNumber">A loan number.</param>
        /// <param name="framework">A value indicating which integration framework to work with.</param>
        /// <param name="includeUnreadIndicators">Indicates whether to include unread indicators for the current user.</param>
        /// <returns>A serialized XML list of vendor messages associated with the loan.</returns>
        [WebMethod(Description = "Retrieves all vendor messages available to the user for the given integration framework")]
        public string GetMessagesForLoan(string authTicket, string loanNumber, string framework, bool includeUnreadIndicators)
        {
            try
            {
                var result = new ServiceResult();
                var principal = AuthServiceHelper.GetPrincipalFromTicket(authTicket, null);

                if (principal == null)
                {
                    result.AppendError(ErrorMessages.WebServices.InvalidAuthTicket);
                    return result.ToResponse();
                }

                CallVolume.IncreaseAndCheck(principal);

                Guid loanId;
                RetrieveLoanIdFromLoanNumber(loanNumber, principal, result, out loanId);

                CommunicationFrameworkType frameworkType;
                ParseFrameworkType(framework, result, out frameworkType);

                ValidateAgainstCooldownThreshold(loanId, principal, frameworkType, result);

                if (result.HasErrors)
                {
                    return result.ToResponse();
                }

                List<VendorMessage> messages = null;
                List<int> unreadIndicators = null;

                if (frameworkType == CommunicationFrameworkType.Appraisal)
                {
                    var handler = new AppraisalMessageHandler(loanId, principal);
                    messages = handler.GetAllMessages();
                    unreadIndicators = includeUnreadIndicators ? handler.GetUnreadMessages() : null;
                    return SerializeVendorMessageList(messages, unreadIndicators);
                }
                else
                {
                    result.AppendError($"The value {framework} is not a valid framework type.");
                    return result.ToResponse();
                }
            }
            catch (CBaseException exc)
            {
                return WebServiceUtilities.GenerateErrorResponseXml(exc);
            }
        }

        /// <summary>
        /// Sets a message as read for the current user.
        /// </summary>
        /// <param name="authTicket">An authentication ticket.</param>
        /// <param name="loanNumber">A loan number.</param>
        /// <param name="framework">A value indicating which integration framework to work with.</param>
        /// <param name="messageId">The ID of the message to set as read.</param>
        /// <returns>A simple XML OK indicator.</returns>
        [WebMethod(Description = "Sets a vendor message as read for the current user")]
        public string SetMessageRead(string authTicket, string loanNumber, string framework, string messageId)
        {
            try
            {
                var result = new ServiceResult();
                var principal = AuthServiceHelper.GetPrincipalFromTicket(authTicket, null);

                if (principal == null)
                {
                    result.AppendError(ErrorMessages.WebServices.InvalidAuthTicket);
                    return result.ToResponse();
                }

                CallVolume.IncreaseAndCheck(principal);

                Guid loanId;
                RetrieveLoanIdFromLoanNumber(loanNumber, principal, result, out loanId);

                CommunicationFrameworkType frameworkType;
                ParseFrameworkType(framework, result, out frameworkType);

                int messageIdValue;
                ParseMessageId(messageId, result, out messageIdValue);

                if (result.HasErrors)
                {
                    return result.ToResponse();
                }

                if (frameworkType == CommunicationFrameworkType.Appraisal)
                {
                    var handler = new AppraisalMessageHandler(loanId, principal);
                    handler.SetMessageRead(messageIdValue);
                    return result.ToResponse();
                }
                else
                {
                    result.AppendError($"The value {framework} is not a valid framework type.");
                    return result.ToResponse();
                }
            }
            catch (CBaseException exc)
            {
                return WebServiceUtilities.GenerateErrorResponseXml(exc);
            }
        }

        /// <summary>
        /// Sends a message to a vendor on behalf of the current user.
        /// </summary>
        /// <param name="authTicket">An authentication ticket.</param>
        /// <param name="loanNumber">A loan number.</param>
        /// <param name="framework">A value indicating which integration framework to work with.</param>
        /// <param name="orderNumber">The order number associated with the message.</param>
        /// <param name="recipient">The recipient of the message.</param>
        /// <param name="subject">The subject of the message.</param>
        /// <param name="body">The body of the message.</param>
        /// <returns>The response from the vendor.</returns>
        [WebMethod(Description = "Transmits a message to a vendor on behalf of the current user")]
        public string SendMessage(string authTicket, string loanNumber, string framework, string orderNumber, string recipient, string subject, string body)
        {
            try
            {
                var result = new ServiceResult();
                var principal = AuthServiceHelper.GetPrincipalFromTicket(authTicket, null);

                if (principal == null)
                {
                    result.AppendError(ErrorMessages.WebServices.InvalidAuthTicket);
                    return result.ToResponse();
                }

                CallVolume.IncreaseAndCheck(principal);

                Guid loanId;
                RetrieveLoanIdFromLoanNumber(loanNumber, principal, result, out loanId);

                CommunicationFrameworkType frameworkType;
                ParseFrameworkType(framework, result, out frameworkType);

                if (result.HasErrors)
                {
                    return result.ToResponse();
                }

                if (frameworkType == CommunicationFrameworkType.Appraisal)
                {
                    AppraisalMessageRecipient receivingParty;
                    TryParseAppraisalRecipient(recipient, result, out receivingParty);

                    if (result.HasErrors)
                    {
                        return result.ToResponse();
                    }

                    var handler = new AppraisalMessageHandler(loanId, principal);
                    var vendor = GetAppraisalVendorFromOrderNumber(loanId, orderNumber);
                    var response = handler.GenerateAndSendMessage(loanId, vendor, principal, orderNumber, receivingParty, subject, body);
                    return GenerateResponseFromVendor(response, result);
                }
                else
                {
                    result.AppendError($"The value {framework} is not a valid framework type.");
                    return result.ToResponse();
                }
            }
            catch (CBaseException exc)
            {
                return WebServiceUtilities.GenerateErrorResponseXml(exc);
            }
        }

        /// <summary>
        /// Retrieves the loan ID corresponding to the given loan number and runs validation checks.
        /// </summary>
        /// <param name="loanNumber">The loan number.</param>
        /// <param name="principal">The user principal.</param>
        /// <param name="result">The web service result.</param>
        /// <param name="loanId">The loan ID to populate.</param>
        private static void RetrieveLoanIdFromLoanNumber(string loanNumber, AbstractUserPrincipal principal, ServiceResult result, out Guid loanId)
        {
            loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, loanNumber);
            if (loanId == Guid.Empty)
            {
                result.AppendError(ErrorMessages.WebServices.InvalidLoanNumber);
                return;
            }

            if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                result.AppendError(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                return;
            }

            AuthServiceHelper.CheckLoanAuthorization(loanId, WorkflowOperations.ReadLoan);
        }

        /// <summary>
        /// Attempts to parse a framework type provided by the user.
        /// </summary>
        /// <param name="framework">A value representing the framework.</param>
        /// <param name="result">The web service result.</param>
        /// <param name="type">The framework enum to populate.</param>
        private static void ParseFrameworkType(string framework, ServiceResult result, out CommunicationFrameworkType type)
        {
            if (framework.TryParseDefine(out type, ignoreCase: true))
            {
                return;
            }

            result.AppendError($"Framework value {framework} is not valid for the bi-directional communication feature.");
        }

        /// <summary>
        /// Attempts to parse an appraisal message recipient.
        /// </summary>
        /// <param name="recipient">The appraisal message recipient.</param>
        /// <param name="result">The web service result.</param>
        /// <param name="receivingParty">The receiving party enum to populate.</param>
        /// <returns>A boolean indicating whether the parse was successful.</returns>
        private static bool TryParseAppraisalRecipient(string recipient, ServiceResult result, out AppraisalMessageRecipient receivingParty)
        {
            if (recipient.TryParseDefine(out receivingParty, ignoreCase: true))
            {
                return true;
            }

            result.AppendError($"Recipient value {recipient} is not valid.");
            return false;
        }

        /// <summary>
        /// Attempts to parse a message ID provided by the user.
        /// </summary>
        /// <param name="messageIdString">The message ID string.</param>
        /// <param name="result">The web service result.</param>
        /// <param name="messageId">The message ID to populate.</param>
        /// <exception cref="WarningOnlyWebserviceException">If the message ID cannot be parsed.</exception>
        private static void ParseMessageId(string messageIdString, ServiceResult result, out int messageId)
        {
            if (int.TryParse(messageIdString, out messageId))
            {
                return;
            }

            result.AppendError($"Message ID {messageId} must be an integer.");
        }

        /// <summary>
        /// Serializes a list of vendor messages to XML.
        /// </summary>
        /// <param name="messageList">A list of vendor messages.</param>
        /// <param name="unreadIndicators">A collection of unread indicators for the current user.</param>
        /// <returns>A serialized XML string containing the list of vendor messages.</returns>
        private static string SerializeVendorMessageList(List<VendorMessage> messageList, List<int> unreadIndicators)
        {
            var vendorMessageElements = new List<XElement>();
            foreach (var message in messageList)
            {
                bool? isUnreadByCurrentUser = unreadIndicators?.Contains(message.MessageId) ?? null;
                vendorMessageElements.Add(GenerateVendorMessageXml(message, isUnreadByCurrentUser));
            }

            var result = new LoXmlServiceResult();
            result.AddResultElement(LoXmlServiceResult.CreateCollectionElement("VendorMessages", vendorMessageElements));
            result.Status = ServiceResultStatus.OK;
            return result.ToResponse();
        }

        /// <summary>
        /// Generates a vendor message XML element.
        /// </summary>
        /// <param name="message">A vendor message.</param>
        /// <param name="isUnreadByCurrentUser">
        /// A boolean indicating whether the current user has read the message, or null if no read indicator should be included.
        /// </param>
        /// <returns>A vendor message XML element.</returns>
        /// <remarks>
        /// The Vendor and Broker IDs are excluded as users don't operate on those directly.
        /// </remarks>
        private static XElement GenerateVendorMessageXml(VendorMessage message, bool? isUnreadByCurrentUser = null)
        {
            var messageXml = new XElement("record");

            messageXml.Add(new XElement("MessageId", message.MessageId.ToString()));
            messageXml.Add(new XElement("LoanId", message.LoanId.ToString()));
            messageXml.Add(new XElement("OrderNumber", message.OrderNumber));
            messageXml.Add(new XElement("SendingParty", message.SendingParty));
            messageXml.Add(new XElement("ReceivingParty", message.ReceivingParty));
            messageXml.Add(new XElement("Subject", message.Subject));
            messageXml.Add(new XElement("Message", message.Message));
            messageXml.Add(new XElement("DateTime", message.Date.ToString()));

            if (isUnreadByCurrentUser.HasValue)
            {
                messageXml.Add(new XElement("IsUnread", isUnreadByCurrentUser.Value.ToString()));
            }

            return messageXml;
        }

        /// <summary>
        /// Gets the appraisal vendor associated with a given order number.
        /// </summary>
        /// <param name="loanId">A loan ID containing the specified order.</param>
        /// <param name="orderNumber">An appraisal order number.</param>
        /// <returns>The vendor associated with the order, or the empty GUID if the order could not be found.</returns>
        private static AppraisalVendorConfig GetAppraisalVendorFromOrderNumber(Guid loanId, string orderNumber)
        {
            var order = LQBAppraisalOrder.Load(loanId.ToString(), orderNumber);
            var vendorId = order?.VendorId ?? Guid.Empty;

            if (vendorId == Guid.Empty)
            {
                var errorMessage = $"Could not find appraisal order \"{orderNumber}\".";
                throw new WarningOnlyWebserviceException(new CBaseException(errorMessage, errorMessage));
            }

            var vendor = AppraisalVendorConfig.Retrieve(vendorId);
            return vendor;
        }

        /// <summary>
        /// Wraps a vendor communication response to return to the user.
        /// </summary>
        /// <param name="response">The vendor communication response.</param>
        /// <param name="result">The web service result.</param>
        /// <returns>A serialized XML response to return to the user.</returns>
        private static string GenerateResponseFromVendor(LqbCommunicationResponse response, ServiceResult result)
        {
            if (response.Status == Status.Error)
            {
                result.AppendError(response.Message);
            }

            return result.ToResponse();
        }

        /// <summary>
        /// Certain DB-intensive services are subject to a cooldown beyond the usual call volume checks.
        /// This method enforces the cooldown via the auto-expire text cache.
        /// </summary>
        /// <param name="loanId">A loan identifier.</param>
        /// <param name="principal">The current user.</param>
        /// <param name="framework">The framework for which messages are being accessed.</param>
        /// <param name="result">The web service result.</param>
        private static void ValidateAgainstCooldownThreshold(Guid loanId, AbstractUserPrincipal principal, CommunicationFrameworkType framework, ServiceResult result)
        {
            string key = $"{loanId.ToString("N")}_{principal.UserId.ToString("N")}_{framework.ToString("D")}_VendorCommunication";

            var duration = ConstStage.VendorCommunicationWebServiceCooldownMinutes;
            var value = AutoExpiredTextCache.GetFromCache(key);

            if (string.IsNullOrEmpty(value))
            {
                StartCooldown(key, duration);
            }
            else
            {
                // Double check just in case the duration has passed but the auto-expire hasn't taken effect yet
                var lastCall = DateTime.Parse(value);
                var lastCallPlusDuration = lastCall.AddMinutes(duration);
                if (lastCallPlusDuration.CompareTo(DateTime.Now) < 0)
                {
                    AutoExpiredTextCache.RemoveFromCacheImmediately(key);
                    StartCooldown(key, duration);
                }
                else
                {
                    result.AppendError($"Please wait {duration} minutes between calls to this web service. The last call for this user and loan occurred at {lastCall.ToString()}.");
                }
            }
        }

        /// <summary>
        /// Begins a cooldown for a DB-intensive method. This restricts the service for a certain duration
        /// for a specified user/loan combination.
        /// </summary>
        /// <param name="key">The key to use as the cache ID.</param>
        /// <param name="duration">The cooldown duration.</param>
        private static void StartCooldown(string key, int duration)
        {
            // The cache value contains the instant the call was made.
            var content = DateTime.Now.ToString();
            var timeToLive = new TimeSpan(hours: 0, minutes: duration, seconds: 0);
            AutoExpiredTextCache.AddToCache(content, timeToLive, key);
        }
    }
}
