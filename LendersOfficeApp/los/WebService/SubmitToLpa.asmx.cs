﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Services;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.LoanProductAdvisor;
    using LendersOffice.ObjLib.Conversions.Aus;
    using LendersOffice.ObjLib.Conversions.LoanProspector;
    using LendersOffice.ObjLib.Conversions.LoanProspector.Seamless;
    using LendersOffice.ObjLib.Conversions.Templates;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.Security;

    /// <summary>
    /// Webservices to submit to LPA.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class SubmitToLpa : WebService
    {
        /// <summary>
        /// Gets the list of Credit Reporting Companies that can be used for submission to LPA.
        /// </summary>
        /// <param name="authTicket">The auth ticket.</param>
        /// <returns>LoXml containg the results of the webservice call. Example given below.</returns>
        /// <remarks>
        /// Sample output XML:
        /// <![CDATA[
        /// <LOXmlFormat version="1.0">
        ///   <result status="OK">
        ///     <collection id="CreditProviders">
        ///       <record index="1" type="CreditProvider">
        ///         <field id="Name">CBCInnovis, Inc</field>
        ///         <field id="CreditProviderId">1</field>
        ///       </record>
        ///       <record index="2" type="CreditProvider">
        ///         <field id="Name">Equifax Mortgage Solutions</field>
        ///         <field id="CreditProviderId">5</field>
        ///       </record>
        ///     </collection>
        ///   </result>
        /// </LOXmlFormat>
        /// ]]>
        /// </remarks>
        [WebMethod(Description = "Gets the Credit Providers available for submission to LPA.")]
        public string GetCreditProviders(string authTicket)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Auto;

            AbstractUserPrincipal principal;
            if (!this.RunBasicChecks(authTicket, result, out principal))
            {
                return result.ToResponse();
            }

            int index = 1;
            var craElements = new List<XElement>();
            foreach (var cra in Tools.FreddieMacCreditReportingCompanyMapping.Concat(Tools.FreddieMacTechnicalAffiliateMapping))
            {
                if (string.IsNullOrEmpty(cra.Value) || cra.Key == "invalid")
                {
                    continue;
                }

                var record = LoXmlServiceResult.CreateCollectionRecordElement(index++, "CreditProvider");
                record.Add(LoXmlServiceResult.CreateFieldElement("Name", cra.Value));
                record.Add(LoXmlServiceResult.CreateFieldElement("CreditProviderId", cra.Key));

                craElements.Add(record);
            }

            var craCollection = LoXmlServiceResult.CreateCollectionElement("CreditProviders", craElements);

            result.AddResultElement(craCollection);

            return result.ToResponse();
        }

        /// <summary>
        /// Submits a request to seamless LPA.
        /// </summary>
        /// <param name="authTicket">The auth ticket.</param>
        /// <param name="optionsXml">The options xml. Sample in remarks below.</param>
        /// <returns>
        /// The xml results.
        /// </returns>
        /// <remarks>
        /// Below is the XML used for this file.
        /// --Sample Input XML--
        /// <![CDATA[
        /// <LOXmlFormat version="1.0">
        ///     <field id="sLId">Id</field>
        ///     <field id="sLNm">LoanNumber</field>
        ///     <field id="sLRefNm">ReferenceNumber</field>
        ///     <field id="LpaUserId">UserId</field>
        ///     <field id="LpaUserPassword">Password</field>
        ///     <field id="LpaLoanId">LoanId</field>
        ///     <field id="LpaAusKey">AusKey</field>
        ///     <field id="CaseStateType"></field>
        ///     <field id="LpaSellerNumber"></field>
        ///     <field id="LpaSellerPassword"></field>
        ///     <field id="LpaTpoNumber"></field>
        ///     <field id="LpaNotpNumber"></field>
        ///     <field id="LenderBranchId"></field>
        ///     <field id="CreditReportOption"></field>
        ///     <field id="CreditProviderId"></field>
        ///     <collection id="ReorderCreditApplications">
        ///         <record>
        ///             <field id="AppId">SomeSsnHere</field>
        ///         </record>
        ///     </collection>
        /// </LOXmlFormat>
        /// ]]>
        /// The Loan can be identified using sLId, sLNm, sLRefNm, in that order.
        /// --CaseStateType--
        /// Application/1,
        /// FinalDisposition/2,
        /// PostClosingQualityControl/3,
        /// Prequalification/4,
        /// Underwriting/5
        /// --CreditReportOption--
        /// OrderNew/1,
        /// Reissue/2,
        /// UsePrevious/3
        /// --Output XML--
        /// Error 
        /// Audit Error
        /// Partial/Polling response.
        /// </remarks>
        [WebMethod(Description = "Makes a submission to Seamless Freddie LPA.")]
        public string SubmitRequest(string authTicket, string optionsXml)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Auto;

            AbstractUserPrincipal principal;
            if (!this.RunBasicChecks(authTicket, result, out principal))
            {
                return result.ToResponse();
            }

            var optionsParseResult = ServiceRequestOptions.ParseRequestOptions(optionsXml);
            Tools.LogInfo("SubmitToLpa.SubmitRequest:" + Environment.NewLine + optionsParseResult.ValueOrDefault.GenerateLogLOXml(fieldsToMask: new string[] { "LpaUserPassword", "LpaSellerPassword", "AppId" }));
            if (!optionsParseResult.IsSuccessful)
            {
                result.AppendError(optionsParseResult.ErrorMessage);
                return result.ToResponse();
            }

            var options = optionsParseResult.ValueOrDefault;
            Guid loanId;
            if (!this.RunInitialLpaChecks(options, principal, result, out loanId))
            {
                return result.ToResponse();
            }

            E_sFredProcPointT? caseStateType;
            CreditReportType? creditReportOption;
            HashSet<Guid> appsForReorderService;
            var dataLoan = new Lazy<CPageData>(() =>
            {
                var loan = CPageData.CreateUsingSmartDependency(loanId, typeof(SubmitToLpa));
                loan.InitLoad();
                return loan;
            });

            string creditProviderId;
            if (this.ValidateSubmitOptions(options, dataLoan, out creditReportOption, out caseStateType, out creditProviderId, out appsForReorderService, result))
            {
                return result.ToResponse();
            }

            LpaSeamlessLoginModel submissionInfo = new LpaSeamlessLoginModel(dataLoan.Value, principal);

            // If the user did not add the field, don't use it. If the user entered an empty field, still override it.
            submissionInfo.LpaCredentials.LpaUserId = options.Fields.GetValueOrNull("LpaUserId") ?? submissionInfo.LpaCredentials.LpaUserId;
            submissionInfo.LpaCredentials.LpaPassword = options.Fields.GetValueOrNull("LpaUserPassword") ?? submissionInfo.LpaCredentials.LpaPassword;
            submissionInfo.LpaCredentials.LpaSellerNumber = options.Fields.GetValueOrNull("LpaSellerNumber") ?? submissionInfo.LpaCredentials.LpaSellerNumber;
            submissionInfo.LpaCredentials.LpaSellerPassword = options.Fields.GetValueOrNull("LpaSellerPassword") ?? submissionInfo.LpaCredentials.LpaSellerPassword;
            submissionInfo.LoanInformation.LenderBranchId = options.Fields.GetValueOrNull("LenderBranchId") ?? submissionInfo.LoanInformation.LenderBranchId;
            submissionInfo.LoanInformation.LpaAusKey = options.Fields.GetValueOrNull("LpaAusKey") ?? submissionInfo.LoanInformation.LpaAusKey;
            submissionInfo.LoanInformation.LpaLoanId = options.Fields.GetValueOrNull("LpaLoanId") ?? submissionInfo.LoanInformation.LpaLoanId;
            submissionInfo.LoanInformation.LpaNotpNumber = options.Fields.GetValueOrNull("LpaNotpNumber") ?? submissionInfo.LoanInformation.LpaNotpNumber;
            submissionInfo.LoanInformation.LpaTpoNumber = options.Fields.GetValueOrNull("LpaTpoNumber") ?? submissionInfo.LoanInformation.LpaTpoNumber;
            submissionInfo.LoanInformation.CaseStateType = caseStateType ?? submissionInfo.LoanInformation.CaseStateType; 
            submissionInfo.CreditInfo.CreditReportOption = creditReportOption ?? submissionInfo.CreditInfo.CreditReportOption;
            submissionInfo.CreditInfo.CraProviderId = creditProviderId ?? submissionInfo.CreditInfo.CraProviderId;

            foreach (var appInfo in submissionInfo.CreditInfo.Applications)
            {
                appInfo.ReorderApplicationCredit = appsForReorderService.Contains(appInfo.ApplicationId);
            }

            LoanAuditResult auditErrorResult;
            LpaResponseViewModel responseModel = LoanSubmitRequest.SaveOptionsAndSubmit(submissionInfo, loanId, principal, isPml: false, prioritizeUiCredentials: true, auditErrorResult: out auditErrorResult);
            if (auditErrorResult != null)
            {
                // Audit didn't pass after loan modification.
                this.CreateAuditResponse(loanId, auditErrorResult, result);
                return result.ToResponse();
            }

            this.CreateResponse(responseModel, result);
            return result.ToResponse();
        }

        /// <summary>
        /// Polls the system to see if an LPA response has been received.
        /// </summary>
        /// <param name="authTicket">The user's auth ticket.</param>
        /// <param name="optionsXml">Options xml. Sample in remarks below.</param>
        /// <returns>The xml results.</returns>
        /// <remarks>
        /// Below is the XML used for this file.
        /// --Sample Input XML--
        /// <![CDATA[
        /// <LOXmlFormat version="1.0">
        ///     <field id="sLId">Id</field>
        ///     <field id="sLNm">LoanNumber</field>
        ///     <field id="sLRefNm">ReferenceNumber</field>
        ///     <field id="JobId">JobId</field>
        /// </LOXmlFormat>
        /// ]]>
        /// The Loan can be identified using sLId, sLNm, sLRefNm, in that order.
        /// --Sample Output XML--
        /// Error
        /// Partial/Poll response
        /// Completed/Summary Data response.
        /// </remarks>
        [WebMethod(Description = "Polls for LPA submission results.")]
        public string PollLpaSubmissionResults(string authTicket, string optionsXml)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Auto;

            AbstractUserPrincipal principal;
            if (!this.RunBasicChecks(authTicket, result, out principal))
            {
                return result.ToResponse();
            }

            var optionsParseResult = ServiceRequestOptions.ParseRequestOptions(optionsXml);
            Tools.LogInfo("SubmitToLpa.PollLpaSubmissionResults:" + Environment.NewLine + optionsParseResult.ValueOrDefault.GenerateLogLOXml(fieldsToMask: null));
            if (!optionsParseResult.IsSuccessful)
            {
                result.AppendError(optionsParseResult.ErrorMessage);
                return result.ToResponse();
            }

            var options = optionsParseResult.ValueOrDefault;
            Guid loanId;
            if (!this.RunInitialLpaChecks(options, principal, result, out loanId))
            {
                return result.ToResponse();
            }

            Guid publicId;
            string jobId = options.Fields.GetValueOrNull("JobId") ?? string.Empty;
            if (!Guid.TryParse(options.Fields.GetValueOrNull("JobId"), out publicId))
            {
                result.AppendError(WebServiceUtilities.CreateInvalidFieldString("jobId", jobId));
                return result.ToResponse();
            }

            var responseViewmodel = LoanSubmitRequest.Poll(publicId, loanId, isPml: false, principal: principal);
            this.CreateResponse(responseViewmodel, result);

            return result.ToResponse();
        }

        /// <summary>
        /// Imports the LPA results last recieved on the loan.
        /// </summary>
        /// <param name="authTicket">The auth ticket of the user.</param>
        /// <param name="optionsXml">The options xml. Sample in remarks.</param>
        /// <returns>The result xml.</returns>
        /// <remarks>
        /// Below is the XML used for this file.
        /// --Input XML--
        /// <![CDATA[
        /// <LOXmlFormat version="1.0">
        ///     <field id="ImportLpaFindings">true</field>
        ///     <field id="ImportCreditReport">true</field>
        ///     <field id="AutopopulateLiabilitiesTo1003">true</field>
        ///     <field id="sLId">Id</field>
        ///     <field id="sLNm">LoanNumber</field>
        ///     <field id="sLRefNm">ReferenceNumber</field>
        /// </LOXmlFormat>
        /// ]]>
        /// The Loan can be identified using sLId, sLNm, sLRefNm, in that order.
        /// --Output XML--
        /// Error response XML
        /// OR
        /// <![CDATA[
        /// <LOXmlFormat version="1.0">
        ///     <result status="OK">
        ///         <LpaFeedbackEdocId>GuidId</LpaFeedbackEdocId>
        ///     </result>
        /// </LOXmlFormat>
        /// ]]>
        /// </remarks>
        [WebMethod(Description = "Imports the latest LPA results for the loan file.")]
        public string ImportLpaResults(string authTicket, string optionsXml)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Auto;

            AbstractUserPrincipal principal;
            if (!this.RunBasicChecks(authTicket, result, out principal))
            {
                return result.ToResponse();
            }

            var optionsParseResult = ServiceRequestOptions.ParseRequestOptions(optionsXml);
            Tools.LogInfo("SubmitToLpa.ImportLpaResults:" + Environment.NewLine + optionsParseResult.ValueOrDefault.GenerateLogLOXml(null));
            if (!optionsParseResult.IsSuccessful)
            {
                result.AppendError(optionsParseResult.ErrorMessage);
                return result.ToResponse();
            }

            var options = optionsParseResult.ValueOrDefault;
            Guid loanId;
            if (!this.RunInitialLpaChecks(options, principal, result, out loanId))
            {
                return result.ToResponse();
            }

            bool hasError = false;

            bool importFindings;
            if (!bool.TryParse(options.Fields.GetValueOrNull("ImportLpaFindings"), out importFindings))
            {
                result.AppendError(WebServiceUtilities.CreateInvalidFieldString("ImportLpaFindings", options.Fields.GetValueOrNull("ImportLpaFindings")));
                hasError = true;
            }

            bool importCreditReport;
            if (!bool.TryParse(options.Fields.GetValueOrNull("ImportCreditReport"), out importCreditReport))
            {
                result.AppendError(WebServiceUtilities.CreateInvalidFieldString("ImportCreditReport", options.Fields.GetValueOrNull("ImportCreditReport")));
                hasError = true;
            }

            bool importLiaiblities;
            if (!bool.TryParse(options.Fields.GetValueOrNull("AutopopulateLiabilitiesTo1003"), out importLiaiblities))
            {
                result.AppendError(WebServiceUtilities.CreateInvalidFieldString("AutopopulateLiabilitiesTo1003", options.Fields.GetValueOrNull("AutopopulateLiabilitiesTo1003")));
                hasError = true;
            }

            if (hasError)
            {
                return result.ToResponse();
            }

            try
            {
                Guid? edocId = LoanSubmitRequest.ImportSubmissionResponse(loanId, importFindings, importCreditReport, importLiaiblities);
                if (edocId.HasValue)
                {
                    result.AddResultElement(LoXmlServiceResult.CreateFieldElement("LpaFeedbackEdocId", edocId.Value.ToString()));
                }
                else
                {
                    result.AppendError($"Unable to import LPA results.");
                }
            }
            catch (SystemException exc)
            {
                Tools.LogError(exc);
                result.AppendError(ErrorMessages.Generic);
            }

            return result.ToResponse();
        }

        /// <summary>
        /// Grabs special values from the options and validates them.
        /// </summary>
        /// <param name="options">The options to pull from.</param>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="creditReportOption">The credit report option chosen.</param>
        /// <param name="caseStateType">The case state type chosen.</param>
        /// <param name="creditProviderId">The credit provider id chosen.</param>
        /// <param name="appsForReorderService">The apps for reorder services.</param>
        /// <param name="result">The results to modify.</param>
        /// <returns>True if validated false otherwise.</returns>
        private bool ValidateSubmitOptions(
            ServiceRequestOptions options, 
            Lazy<CPageData> dataLoan,
            out CreditReportType? creditReportOption, 
            out E_sFredProcPointT? caseStateType, 
            out string creditProviderId,
            out HashSet<Guid> appsForReorderService,
            LoXmlServiceResult result)
        {
            bool hasError = false;
            var caseStateTypeString = options.Fields.GetValueOrNull("CaseStateType");
            if (caseStateTypeString == null)
            {
                caseStateType = null;
            }
            else
            {
                E_sFredProcPointT tempType = E_sFredProcPointT.LeaveBlank;
                if (caseStateTypeString != string.Empty &&
                     (!Enum.TryParse(options.Fields.GetValueOrNull("CaseStateType"), out tempType) || !Enum.IsDefined(typeof(E_sFredProcPointT), tempType)))
                {
                    result.AppendError(WebServiceUtilities.CreateInvalidFieldString("CaseStateType", caseStateTypeString));
                    hasError = true;
                }

                caseStateType = tempType;
            }

            var creditReportOptionString = options.Fields.GetValueOrNull("CreditReportOption");
            if (creditReportOptionString == null)
            {
                creditReportOption = null;
            }
            else
            {
                CreditReportType tempType;
                if (!Enum.TryParse(creditReportOptionString, out tempType) || !Enum.IsDefined(typeof(CreditReportType), tempType))
                {
                    result.AppendError(WebServiceUtilities.CreateInvalidFieldString("CreditReportOption", creditReportOptionString));
                    hasError = true;
                }

                creditReportOption = tempType;
            }

            creditProviderId = options.Fields.GetValueOrNull("CreditProviderId");
            if (!string.IsNullOrEmpty(creditProviderId) && !Tools.FreddieMacCreditReportingCompanyMapping.ContainsKey(creditProviderId) && !Tools.FreddieMacTechnicalAffiliateMapping.ContainsKey(creditProviderId))
            {
                result.AppendError(WebServiceUtilities.CreateInvalidFieldString("CreditProviderId", creditProviderId));
                hasError = true;
            }

            appsForReorderService = new HashSet<Guid>();
            var ssnRecords = options.Collections.GetValueOrNull("ReorderCreditApplications");
            if (ssnRecords != null)
            {
                foreach (var ssnRecord in ssnRecords)
                {
                    var ssn = ssnRecord.Fields.GetValueOrNull("AppId")?.Replace("-", string.Empty);
                    var appFound = false;
                    for (int i = 0; i < dataLoan.Value.nApps; i++)
                    {
                        var currentApp = dataLoan.Value.GetAppData(i);
                        if (currentApp.aBSsn.Replace("-", string.Empty) == ssn ||
                            currentApp.aCSsn.Replace("-", string.Empty) == ssn)
                        {
                            appsForReorderService.Add(currentApp.aAppId);
                            appFound = true;
                            break;
                        }
                    }

                    if (!appFound)
                    {
                        result.AppendError(WebServiceUtilities.CreateInvalidFieldString("AppId", ssn));
                        hasError = true;
                    }
                }
            }

            return hasError;
        }

        /// <summary>
        /// Runs the audit before submission.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="auditErrorResult">The audit results to use to generate the response xml.</param>
        /// <param name="result">The result to populate.</param>
        private void CreateAuditResponse(Guid loanId, LoanAuditResult auditErrorResult, LoXmlServiceResult result)
        {
            result.Status = ServiceResultStatus.Error;
            result.AddResultElement("LpaResponseStatus", AusResponseStatus.AuditFailure.ToString());

            List<XElement> errors = new List<XElement>();
            int errorsCount = 1;
            foreach (var loanError in auditErrorResult.LoanErrors)
            {
                var loanErrorRecord = LoXmlServiceResult.CreateCollectionRecordElement(errorsCount++, "AuditError");
                loanErrorRecord.Add(LoXmlServiceResult.CreateFieldElement("Field", loanError.FieldName));
                loanErrorRecord.Add(LoXmlServiceResult.CreateFieldElement("Value", loanError.Value));
                loanErrorRecord.Add(LoXmlServiceResult.CreateFieldElement("Message", loanError.Message));

                errors.Add(loanErrorRecord);
            }

            foreach (var borrower in auditErrorResult.BorrowerAudits)
            {
                foreach (var borrError in borrower.Errors)
                {
                    var borrowerErrorRecord = LoXmlServiceResult.CreateCollectionRecordElement(errorsCount++, "AuditError");
                    borrowerErrorRecord.Add(LoXmlServiceResult.CreateFieldElement("Field", borrError.FieldName));
                    borrowerErrorRecord.Add(LoXmlServiceResult.CreateFieldElement("Value", borrError.Value));
                    borrowerErrorRecord.Add(LoXmlServiceResult.CreateFieldElement("Message", borrError.Message));
                    borrowerErrorRecord.Add(LoXmlServiceResult.CreateFieldElement("BorrowerName", borrower.FullName));

                    errors.Add(borrowerErrorRecord);
                }
            }

            result.AddResultElement(LoXmlServiceResult.CreateCollectionElement("AuditErrors", errors));
        }

        /// <summary>
        /// Creates an XML response from the viewmodel.
        /// </summary>
        /// <param name="viewModelToTransform">The viewmodel to transform from.</param>
        /// <param name="result">The results to populate.</param>
        private void CreateResponse(LpaResponseViewModel viewModelToTransform, LoXmlServiceResult result)
        {
            result.AddResultElement("LpaResponseStatus", viewModelToTransform.Status.ToString());
            switch (viewModelToTransform.Status)
            {
                case AusResponseStatus.Error:
                    result.AppendError(viewModelToTransform.ErrorMessage);
                    break;
                case AusResponseStatus.Processing:
                    result.AddResultElement(LoXmlServiceResult.CreateFieldElement("JobId", viewModelToTransform.PublicJobId.ToString()));
                    result.AddResultElement(LoXmlServiceResult.CreateFieldElement("PollingIntervalInMs", viewModelToTransform.PollingPeriodMs.ToString()));
                    break;
                case AusResponseStatus.Done:
                    List<XElement> summaryItems = new List<XElement>();
                    int index = 1;
                    foreach (var lpaResult in viewModelToTransform.LpaResults)
                    {
                        var summaryItem = LoXmlServiceResult.CreateCollectionRecordElement(index++, "SummaryValue");
                        summaryItem.Add(LoXmlServiceResult.CreateFieldElement("Name", lpaResult.Key));
                        summaryItem.Add(LoXmlServiceResult.CreateFieldElement("Value", lpaResult.Value));

                        summaryItems.Add(summaryItem);
                    }

                    result.AddResultElement(LoXmlServiceResult.CreateCollectionElement("Summary", summaryItems));
                    break;
                default:
                    throw new UnhandledEnumException(viewModelToTransform.Status);
            }
        }

        /// <summary>
        /// Runs basic checks before running the webservice.
        /// </summary>
        /// <param name="authTicket">The auth ticket of the user.</param>
        /// <param name="result">The results to populate.</param>
        /// <param name="principal">The principal determined from the auth ticket.</param>
        /// <returns>True if passed. False otherwise.</returns>
        private bool RunBasicChecks(string authTicket, LoXmlServiceResult result, out AbstractUserPrincipal principal)
        {
            string errors = WebServiceUtilities.TryToGetUserPrincipal(authTicket, null, out principal);
            if (!string.IsNullOrEmpty(errors))
            {
                result.AppendError(errors);
                return false;
            }

            CallVolume.IncreaseAndCheck(principal);
            return true;
        }

        /// <summary>
        /// Runs some initial checks for the LPA webmethods.
        /// </summary>
        /// <param name="requestOptions">The options xml passed into the method.</param>
        /// <param name="principal">The principal.</param>
        /// <param name="result">The result to modify.</param>
        /// <param name="loanId">The loan id found, if found.</param>
        /// <returns>True if valid, false otherwise.</returns>
        private bool RunInitialLpaChecks(ServiceRequestOptions requestOptions, AbstractUserPrincipal principal, LoXmlServiceResult result, out Guid loanId)
        {
            loanId = Guid.Empty;
            if (!principal.BrokerDB.IsSeamlessLpaEnabled)
            {
                result.AppendError("This feature is not enabled.");
                return false;
            }

            string errorMsg;
            bool loanFound = false;

            string loanIdString = requestOptions.Fields.GetValueOrNull(nameof(CPageData.sLId));
            loanId = loanIdString?.ToNullable<Guid>(Guid.TryParse) ?? Guid.Empty;
            errorMsg = $"Invalid loan id {loanIdString}.";

            if (loanId == Guid.Empty)
            {
                // Invalid loan id, try the others.
                string loanName = requestOptions.Fields.GetValueOrNull(nameof(CPageData.sLNm));
                string loanReferenceNumber = requestOptions.Fields.GetValueOrNull(nameof(CPageData.sLRefNm));

                if (!string.IsNullOrEmpty(loanName))
                {
                    errorMsg = $"Invalid loan name {loanName}.";
                    loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, loanName);
                    if (loanId != Guid.Empty)
                    {
                       loanFound = true;
                    }
                }
                else if (!string.IsNullOrEmpty(loanReferenceNumber))
                {
                    errorMsg = $"Invalid loan reference number {loanReferenceNumber}.";
                    loanId = Tools.GetLoanIdByLoanReferenceNumber(principal.BrokerId, loanReferenceNumber);
                    if (loanId != Guid.Empty)
                    {
                        loanFound = true;
                    }
                }
            }
            else if (!string.IsNullOrEmpty(Tools.GetLoanNameByLoanId(principal.BrokerId, loanId)))
            {
                loanFound = true;
            }

            if (!loanFound)
            {
                result.AppendError(errorMsg);
                return false;
            }

            return true;
        }
    }
}
