namespace LendersOfficeApp.los.common
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.ObjLib.Appraisal;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;

    public partial class GlobalDMSHandler : IHttpHandler
    {
        #region IHttpHandler Members
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            GDMSAppraisalOrderInfo orderInfo = null;
            GDMSPushStatusUpdateMessage updateMessage = null;
            var updateResponse = new GDMSPushStatusUpdateResponse() { Status = GDMSPushStatusUpdateResponse.StatusType.ERROR }; // Error until success
            try
            {
                updateMessage = ExtractPushStatusUpdateMessage(context.Request);
                if (null == updateMessage)
                {
                    throw new GlobalDMSHandlerException("Could not parse xml.");
                }

                int fileNumber = updateMessage.FileNumber;

                // Use a stored procedure with a FileNumber -> Loan GUID mapping
                // FileNumber -> sLId, brokerId, employeeId, userId
                orderInfo = GDMSAppraisalOrderInfo.Load(fileNumber);
                if (orderInfo == null)
                {
                    throw new GlobalDMSHandlerException("Could not find file number (" + fileNumber + ").");
                }

                updateResponse.Status = GDMSPushStatusUpdateResponse.StatusType.OK;
            }
            catch (GlobalDMSHandlerException exc)
            {
                updateResponse.Message = exc.UserMessage;
            }
            finally
            {
                SendResponse(context.Response, updateResponse.ToXml());
            }

            if (updateResponse.Status == GDMSPushStatusUpdateResponse.StatusType.OK)
            {
                LoadDataFromGlobalDMS(orderInfo, updateMessage);
                orderInfo.Save();
            }
        }
        #endregion
    }

    public partial class GlobalDMSHandler
    {
        public static void LoadDataFromGlobalDMS(GDMSAppraisalOrderInfo orderInfo, GDMSPushStatusUpdateMessage updateMessage)
        {
            string retrievalErrorMessage;
            var appraisalVendor = AppraisalVendorConfig.Retrieve(orderInfo.VendorId);
            GDMSCredentials credentials = appraisalVendor.GetGDMSCredentials(orderInfo.BrokerId, orderInfo.EmployeeId, out retrievalErrorMessage);
            if (credentials == null)
            {
                Tools.LogError(string.Format("Unable to load GlobalDMS credentials for BrokerId {0}, EmployeeId {1}. Error message was {2}", orderInfo.BrokerId, orderInfo.EmployeeId, retrievalErrorMessage));
            }
            else
            {
                // Get order related to filenumber
                GdmsLoadedOrder gdmsOrderData = LoadOrderAndFilesFromGDMS(credentials, orderInfo);
                if (gdmsOrderData != null)
                {
                    CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(orderInfo.LoanId, typeof(GlobalDMSHandler));
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    // Ensure there are files and the broker has auto-save set up for appraisals
                    if (gdmsOrderData.UploadedFiles.Any() && null != AutoSaveDocTypeFactory.GetDocTypeForPageId(E_AutoSavePage.AppraisalDocs, orderInfo.BrokerId, E_EnforceFolderPermissions.True))
                    {
                        foreach (LendersOffice.GDMS.LookupMethods.OrderFileObject uploadedFile in gdmsOrderData.UploadedFiles)
                        {
                            var file = new AppraisalFileDocUploadInfo(orderInfo.FileNumber, uploadedFile);
                            file.UploadFileToEdocs(orderInfo, dataLoan);
                        }
                    }

                    BindData(dataLoan, orderInfo, gdmsOrderData, updateMessage);
                    dataLoan.Save();
                }
            }
        }

        private static GdmsLoadedOrder LoadOrderAndFilesFromGDMS(GDMSCredentials credentials, GDMSAppraisalOrderInfo orderInfo)
        {
            return LoadDataFromGDMS(
                () => new OrdersClient(credentials),
                ordersClient =>
                {
                    LendersOffice.GDMS.LookupMethods.ClientOrder order = ordersClient.GetOrder(orderInfo.FileNumber);
                    LendersOffice.GDMS.LookupMethods.OrderFileObject[] uploadedFiles = ordersClient.GetOrderUploadedFilesList(orderInfo.FileNumber);
                    LendersOffice.GDMS.LookupMethods.clsCUDetail collateralUnderwriterDetail = ConstStage.EnableGdmsCollateralUnderwriterData ? ordersClient.GetUcdpCuSummaryData(orderInfo.FileNumber) : null;
                    return new GdmsLoadedOrder(order, uploadedFiles, collateralUnderwriterDetail);
                });
        }

        private static LendersOffice.GDMS.LookupMethods.clsAppraiserForListing LoadAppraiserFromGDMS(GDMSCredentials credentials, int appraiserId)
        {
            return LoadDataFromGDMS(
                () => new LookupMethodsClient(credentials),
                lookupClient => lookupClient.GetAppraiserById(appraiserId));
        }

        private static T LoadDataFromGDMS<T, TGDMSClient>(Func<TGDMSClient> createClient, Func<TGDMSClient, T> loadData) where TGDMSClient : GDMSClient
        {
            using (var client = createClient())
            {
                try
                {
                    return loadData(client);
                }
                catch (GDMSErrorResponseException exc)
                {
                    Tools.LogError(exc);
                    return default(T);
                }
            }
        }

        private static IRealEstateOwned GetLinkedReo(CPageData dataLoan, GDMSAppraisalOrderInfo orderInfo)
        {
            if (!orderInfo.HasLinkedReo || !orderInfo.AppIdAssociatedWithProperty.HasValue)
            {
                return null;
            }

            CAppData associatedApp = dataLoan.GetAppData(orderInfo.AppIdAssociatedWithProperty.Value);
            if (associatedApp == null)
            {
                return null;
            }

            return associatedApp.aReCollection.GetRecordOf(orderInfo.LinkedReoId.Value) as IRealEstateOwned;
        }

        private static void SendResponse(HttpResponse response, byte[] responseXml)
        {
            response.ContentType = "text/xml";
            response.ContentEncoding = System.Text.Encoding.UTF8;
            response.Output.Write(Encoding.UTF8.GetChars(responseXml, 0, responseXml.Length));
            response.Flush();
            Tools.LogInfo("GlobalDMSHandler: Sent " + Encoding.UTF8.GetString(responseXml));
        }

        private static void BindData(CPageData dataLoan, GDMSAppraisalOrderInfo orderInfo, GdmsLoadedOrder gdmsOrderData, GDMSPushStatusUpdateMessage updateMessage)
        {
            bool completed = "completed".Equals(gdmsOrderData.Order.StatusName, StringComparison.CurrentCultureIgnoreCase) || "complete".Equals(gdmsOrderData.Order.StatusName, StringComparison.CurrentCultureIgnoreCase);
            string dateString = DateTime.Now.ToShortDateString();

            if (completed)
            {
                orderInfo.ReceivedDate_rep = dateString;
            }

            orderInfo.FhaDocFileId = updateMessage.EAD_DocID;
            orderInfo.UcdpAppraisalId = updateMessage.UCDP_DocID;

            if (gdmsOrderData.CollateralUnderwriterDetail != null)
            {
                orderInfo.CuRiskScore = gdmsOrderData.CollateralUnderwriterDetail.CURiskScore;
                orderInfo.AppraisalQualityRiskT = gdmsOrderData.CollateralUnderwriterDetail.FNM1002 ? E_CuRiskT.HeightenedRisk : E_CuRiskT.NotHeightenedRisk;
                orderInfo.OvervaluationRiskT = gdmsOrderData.CollateralUnderwriterDetail.FNM1004 ? E_CuRiskT.HeightenedRisk : E_CuRiskT.NotHeightenedRisk;
                orderInfo.PropertyEligibilityRiskT = gdmsOrderData.CollateralUnderwriterDetail.FNM1006 ? E_CuRiskT.HeightenedRisk : E_CuRiskT.NotHeightenedRisk;
            }

            IRealEstateOwned linkedReo = orderInfo.GetLinkedReo(dataLoan);
            if (orderInfo.HasLinkedReo)
            {
                var estimatedValue = gdmsOrderData.Order?.EstimatedValue;
                if (estimatedValue.HasValue && linkedReo?.Val == 0)
                {
                    linkedReo.Val = estimatedValue.Value;
                }

                return;
            }

            if (completed)
            {
                dataLoan.sApprRprtRd_rep = dateString;
            }

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Til, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            preparer.PrepareDate_rep = SanitizeDate(gdmsOrderData.Order.TILDate_String);

            dataLoan.sSpAppraisalId = updateMessage.UCDP_DocID;
            dataLoan.sSpFhaDocId = updateMessage.EAD_DocID;
            if (gdmsOrderData.CollateralUnderwriterDetail != null)
            {
                dataLoan.sSpCuScore = orderInfo.CuRiskScore;
                dataLoan.sSpOvervaluationRiskT = orderInfo.OvervaluationRiskT;
                dataLoan.sSpPropertyEligibilityRiskT = orderInfo.PropertyEligibilityRiskT;
                dataLoan.sSpAppraisalQualityRiskT = orderInfo.AppraisalQualityRiskT;
            }
        }

        private static void ImportAppraiser(CPageData dataLoan, LendersOffice.GDMS.LookupMethods.clsAppraiserForListing appraiser)
        {
            if (appraiser == null)
            {
                return;
            }

            CAgentFields agent = dataLoan.GetAgentFields(-1);
            agent.AgentRoleT = E_AgentRoleT.Appraiser;
            agent.AgentName = appraiser.ApprFirstName+" "+(string.IsNullOrEmpty(appraiser.ApprMiddleInit)?"":appraiser.ApprMiddleInit+" ")+appraiser.ApprLastName;
            agent.CompanyName = appraiser.ApprCompanyName;
            agent.StreetAddr = appraiser.ApprAddress1;
            agent.City = appraiser.ApprCity;
            agent.State = appraiser.ApprState;
            agent.Zip = appraiser.ApprZip;
            agent.Phone = appraiser.ApprPhone1;
            agent.EmailAddr = appraiser.ApprEmail;
            agent.FaxNum = appraiser.ApprFax;
            agent.LicenseNumOfAgent = appraiser.License1;
            agent.LicenseInfoList.Add(new LicenseInfo(appraiser.LicenseState1, SanitizeDate(appraiser.License1Exp_String), appraiser.License1));
            agent.Update();
        }

        public static string SanitizeDate(string date)
        {
            if (string.IsNullOrEmpty(date) || date.Equals("1/1/1900", StringComparison.OrdinalIgnoreCase))
            {
                return string.Empty;
            }

            return date;
        }

        private static GDMSPushStatusUpdateMessage ExtractPushStatusUpdateMessage(HttpRequest request)
        {
            string responseXml;
            using (var reader = new StreamReader(request.InputStream, request.ContentEncoding))
            {
                responseXml = reader.ReadToEnd();
            }

            Tools.LogInfo("GlobalDMSHandler: Received PushStatusUpdateMessage:" + Environment.NewLine + responseXml);

            GDMSPushStatusUpdateMessage gdmsPushStatusUpdateMessage;

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.XmlResolver = null;
#if LQB_NET45
            xmlReaderSettings.DtdProcessing = DtdProcessing.Parse;
#else
            xmlReaderSettings.ProhibitDtd = false;
#endif
            try
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(responseXml), xmlReaderSettings))
                {
                    // Deserialize to object
                    gdmsPushStatusUpdateMessage = GDMSPushStatusUpdateMessage.FromXml(reader);
                }
            }
            catch (InvalidOperationException) // Could not read the XML
            {
                Tools.LogErrorWithCriticalTracking(
                    string.Format(
                        @"Invalid GDMS push request from {0}:{1}. Got {2}",
                        request.UserHostName,
                        request.UserHostAddress,
                        responseXml
                    )
                );
                gdmsPushStatusUpdateMessage = null;
            }

            return gdmsPushStatusUpdateMessage;
        }

        /// <summary>
        /// Because Tuples get ridiculous.
        /// </summary>
        private class GdmsLoadedOrder
        {
            public GdmsLoadedOrder(
                LendersOffice.GDMS.LookupMethods.ClientOrder order,
                LendersOffice.GDMS.LookupMethods.OrderFileObject[] uploadedFiles,
                LendersOffice.GDMS.LookupMethods.clsCUDetail collateralUnderwriterDetail)
            {
                this.Order = order;
                this.UploadedFiles = uploadedFiles;
                this.CollateralUnderwriterDetail = collateralUnderwriterDetail;
            }

            public LendersOffice.GDMS.LookupMethods.ClientOrder Order { get; }

            public LendersOffice.GDMS.LookupMethods.OrderFileObject[] UploadedFiles { get; }

            public LendersOffice.GDMS.LookupMethods.clsCUDetail CollateralUnderwriterDetail { get; }
        }
    }

    public class GlobalDMSHandlerException : CBaseException
    {
        public GlobalDMSHandlerException(string userMessage)
            : base(userMessage, userMessage)
        {
        }
    }
}
