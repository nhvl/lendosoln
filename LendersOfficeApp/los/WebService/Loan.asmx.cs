﻿/// Author: David Dao
namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Security;
    using System.Text;
    using System.Web.Services;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using System.Xml.Xsl;
    using DataAccess;
    using EDocs;
    using global::Integration.Encompass;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.Conversions.LoanProspector;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.DU;
    using LendersOffice.Integration.GenericFramework;
    using LendersOffice.Integration.LoanProductAdvisor;
    using LendersOffice.Integration.TotalScorecard;
    using LendersOffice.LoanSearch;
    using LendersOffice.ObjLib.Conversions.Aus;
    using LendersOffice.ObjLib.Loan.Security;
    using LendersOffice.ObjLib.LoanSearch.Basic;
    using LendersOffice.ObjLib.Resource;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.QueryProcessor;
    using LendersOffice.QuickPricer;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LendersOfficeApp.LegacyLink.CalyxPoint;
    using LoanProspectorResponse;
    using LendersOffice.ObjLib.Security;
    using LqbGrammar.DataTypes;
    using LendersOffice.ObjLib.DU.Seamless;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using LendersOffice.Conversions.Mismo3.Version4;

    /// <summary>
    /// Container for methods used to load and save loan files.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class Loan : WebService
    {
        /// <summary>
        /// A reference to <see cref="edocsService"/> for web services that have been migrated to that file.
        /// </summary>
        private EDocsService edocsService = new EDocsService();

        /// <summary>
        /// A reference to <see cref="quickPricer"/> for web services that have been migrated to that file.
        /// </summary>
        private QuickPricer quickPricer = new QuickPricer();

        /// <summary>
        /// A generic description for EDocs methods that have been migrated to EDocsService.asmx.
        /// </summary>
        private const string useEDocs = "[DEPRECATED] Please use the EDocsService version of this method instead. ";

        /// <summary>
        /// Category for logging LQB Document Framework requests in PaulBunyan.
        /// </summary>
        private const string DocumentFrameworkCategory = "Loan.asmx::DocumentFramework";

        public Loan()
        {
            //CODEGEN: This call is required by the ASP.NET Web Services Designer
            InitializeComponent();
        }

        /// <summary>
        /// Gets the principal from the ticket, but this will assume that the method is not specific to a loan.
        /// </summary>
        /// <param name="ticket">The ticket of the user.</param>
        /// <returns>The principal represented by the ticket.</returns>
        private AbstractUserPrincipal GetPrincipalFromTicket(string ticket)
        {
            return AuthServiceHelper.GetPrincipalFromTicket(ticket, null);
        }

        /// <summary>
        /// Gets the principal from the ticket, checking if the ticket has access to the specified loan.
        /// </summary>
        /// <param name="ticket">The ticket of the user.</param>
        /// <param name="loanRefNm">The loan reference number being accessed; used for tickets with a restriction to a loan.</param>
        /// <returns>The principal represented by the ticket.</returns>
        private AbstractUserPrincipal GetPrincipalFromTicketAndValidateByRefNum(string ticket, string loanRefNm)
        {
            return AuthServiceHelper.GetPrincipalFromTicketAndValidateByRefNum(ticket, loanRefNm, false);
        }

        /// <summary>
        /// Gets the principal from the ticket, checking if the ticket has access to the specified loan.
        /// </summary>
        /// <param name="ticket">The ticket of the user.</param>
        /// <param name="loanNumber">The loan number being accessed; used for tickets with a restriction to a loan.</param>
        /// <returns>The principal represented by the ticket.</returns>
        private AbstractUserPrincipal GetPrincipalFromTicket(string ticket, string loanNumber)
        {
            return AuthServiceHelper.GetPrincipalFromTicket(ticket, loanNumber);
        }

        private void IncreaseAndCheckCallVolume(string sTicket, string sLNm)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);
        }

        private void IncreaseAndCheckCallVolume(string sTicket)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
        }

        /// In this class we only need two main methods. Load and Save for all of
        /// our 3rd party integration.
        /// Look at sample-request.xml for prototype of request xml.

        /// <summary>
        /// This is a synchronous method. Return value is loan document in MISMO format.
        /// </summary>
        [WebMethod(Description = "Query data from a loan file. [sTicket] Ticket returned by AuthService.asmx. [sLNm] The LendingQB Loan Name. [sXmlQuery] The Xml defines the list of fields to be included in the response. Refer to lo_xmlquery_blank.xml for an example. [format] Identifies the output format to use. 0 - LO XML Query. 3 - Calyx Point (This is returned as an encoded base-64 Xml encased string. This is because the Calyx Point file is a binary file.) 4 - Fannie Mae 3.2. 9 - MISMO 2.6, 10 - MISMO 3.3, 12 - MISMO 3.4")]
        public string Load(string sTicket, string sLNm, string sXmlQuery, int format)
        {
            IntegrationExportFormat exportFormat = (IntegrationExportFormat)format;
            if (string.IsNullOrWhiteSpace(sTicket) || string.IsNullOrWhiteSpace(sLNm)
                || (string.IsNullOrWhiteSpace(sXmlQuery) && exportFormat == IntegrationExportFormat.LOFile))
            {
                // 9/19/2015 - dd - Invalid Arguments fail early.
                throw new ArgumentException("sTicket and sLNm cannot be empty. sXmlQuery can only be empty for non-LOXML formats.");
            }

            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            return LoadImpl(principal, sLId, sXmlQuery, exportFormat);
        }

        /// <summary>
        /// This is a synchronous method. Return value is loan document in MISMO format.
        /// </summary>
        [WebMethod(Description = "Query data from a loan file. [sTicket] Ticket returned by AuthService.asmx. [sLRefNm] The LendingQB Loan Reference Number. [sXmlQuery] The Xml defines the list of fields to be included in the response. Refer to lo_xmlquery_blank.xml for an example. [format] Identifies the output format to use. 0 - LO XML Query. 3 - Calyx Point (This is returned as an encoded base-64 Xml encased string. This is because the Calyx Point file is a binary file.) 4 - Fannie Mae 3.2. 9 - MISMO 2.6, 10 - MISMO 3.3, 12 - MISMO 3.4")]
        public string LoadByRefNumber(string sTicket, string sLRefNm, string sXmlQuery, int format)
        {
            IntegrationExportFormat exportFormat = (IntegrationExportFormat)format;
            if (string.IsNullOrWhiteSpace(sTicket) || string.IsNullOrWhiteSpace(sLRefNm)
                || (string.IsNullOrWhiteSpace(sXmlQuery) && exportFormat == IntegrationExportFormat.LOFile))
            {
                // 9/19/2015 - dd - Invalid Arguments fail early.
                throw new ArgumentException("sTicket and sLRefNm cannot be empty. sXmlQuery can only be empty for non-LOXML formats.");
            }

            AbstractUserPrincipal principal = GetPrincipalFromTicketAndValidateByRefNum(sTicket, sLRefNm);
            CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanReferenceNumber(principal.BrokerId, sLRefNm);
            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanReferenceNumberException(sLRefNm);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            return LoadImpl(principal, sLId, sXmlQuery, exportFormat);
        }

        private string LoadImpl(AbstractUserPrincipal principal, Guid sLId, string sXmlQuery, IntegrationExportFormat format)
        {
            Stopwatch sw = Stopwatch.StartNew();
            Tools.LogInfo("Loan.asmx::LoadImpl", "Load sLId=[" + sLId + "], format=" + format);

            string ret = "";

            try
            {
                if (format == IntegrationExportFormat.LOFile 
                    || ((format == IntegrationExportFormat.Mismo33 || format == IntegrationExportFormat.Mismo34) && !string.IsNullOrEmpty(sXmlQuery)))
                {
                    if (Tools.IsLogLargeRequest(sXmlQuery.Length))
                    {
                        try
                        {
                            XDocument xdoc = XDocument.Parse(sXmlQuery);
                            Tools.LogInfo("Loan.asmx::LoadImpl", xdoc.ToString(SaveOptions.None));
                        }
                        catch (FormatException)
                        {
                            Tools.LogInfo("Loan.asmx::LoadImpl", sXmlQuery);
                        }
                    }
                    else
                    {
                        Tools.LogInfo("Loan.asmx::LoadImpl", sXmlQuery.Length + " bytes exceed threshold for logging.");
                    }
                }

                AuthServiceHelper.CheckLoanAuthorization(sLId, WorkflowOperations.ReadLoan);

                switch (format)
                {
                    case IntegrationExportFormat.LOFile:
                        bool isClosingDisclosure = XElement.Parse(sXmlQuery).XPathSelectElements("//collection[@id=\"DocumentPackage\"]/record[1]/field").FirstOrDefault(element => element.Attribute("id").Value.Equals("hasclosingdisclosure", StringComparison.OrdinalIgnoreCase))?.Value.ToNullableBool() ?? false;
                        ret = LOFormatExporter.Export(sLId, principal, sXmlQuery, isClosingDisclosure);
                        break;
                    case IntegrationExportFormat.CalyxPoint:
                        ret = ExportToPoint(sLId, principal);
                        break;
                    case IntegrationExportFormat.FannieMae32:
                        ret = ExportToFannieMae32(sLId);
                        break;
                    case IntegrationExportFormat.Mismo26:
                        ret = LendersOffice.Conversions.GenericMismoClosing26.GenericMismoClosing26Exporter.Export(sLId, principal);
                        break;
                    case IntegrationExportFormat.Mismo33:
                        var parsedPackage = LOFormatExporter.GetDocumentPackageFromLoXml(sXmlQuery, exportIsLoxmlFormat: false);
                        if (!parsedPackage.IsSuccessful)
                        {
                            Tools.LogError($"Loan.asmx::Load - Parsing Error{Environment.NewLine}{parsedPackage.ErrorMessage}{Environment.NewLine}Original XML:{Environment.NewLine}{parsedPackage.Input}");
                            var result = new LoXmlServiceResult() { Status = ServiceResultStatus.Error };
                            result.AppendError(parsedPackage.ErrorMessage);
                            ret = result.ToResponse();
                        }
                        else
                        {
                            ret = LendersOffice.Conversions.Mismo3.Version3.Mismo33RequestProvider.SerializeMessage(
                                loanIdentifier: sLId,
                                vendorAccountID: null,
                                transactionID: Guid.NewGuid().ToString(),
                                docPackage: parsedPackage.ValueOrDefault,
                                includeIntegratedDisclosureExtension: true,
                                principal: principal,
                                vendor: DocFrameworkMethodContainer.ParseVendor(sXmlQuery),
                                useMismo3DefaultNamespace: true);
                        }

                        ret = ret.TrimWhitespaceAndBOM();
                        break;
                    case IntegrationExportFormat.Mismo34:
                        var docPackage = LOFormatExporter.GetDocumentPackageFromLoXml(sXmlQuery, exportIsLoxmlFormat: false);
                        if (!docPackage.IsSuccessful)
                        {
                            Tools.LogError($"Loan.asmx::Load - Parsing Error{Environment.NewLine}{docPackage.ErrorMessage}{Environment.NewLine}Original XML:{Environment.NewLine}{docPackage.Input}");
                            var result = new LoXmlServiceResult() { Status = ServiceResultStatus.Error };
                            result.AppendError(docPackage.ErrorMessage);
                            ret = result.ToResponse();
                        }
                        else
                        {
                            Mismo34ExporterOptions options = new Mismo34ExporterOptions()
                            {
                                LoanId = sLId,
                                TransactionId = Guid.NewGuid().ToString(),
                                DocumentPackage = docPackage.ValueOrDefault,
                                IncludeIntegratedDisclosureExtension = true,
                                Principal = PrincipalFactory.CurrentPrincipal,
                                DocumentVendor = DocFrameworkMethodContainer.ParseVendor(sXmlQuery),
                                UseMismo3DefaultNamespace = true
                            };

                            var exporter = new Mismo34Exporter(options);
                            ret = exporter.SerializePayload();
                        }

                        ret = ret.TrimWhitespaceAndBOM();
                        break;
                    case IntegrationExportFormat.LqbDocumentRequest:
                        ret = DocFrameworkMethodContainer.CreateLqbDocumentRequest(principal, sLId, sXmlQuery);
                        break;
                    default:
                        throw new CBaseException(ErrorMessages.InvalidIntegrationFormat, format + " is not valid integration format");
                }
                // 5/29/2012 dd - I added this to debug return output. May want to disable if it get too big
                if (Tools.IsLogLargeRequest(ret.Length))
                {
                    Tools.LogInfo("Loan.asmx::LoadImpl", ret);
                }
                else
                {
                    Tools.LogInfo("Loan.asmx::LoadImpl", ret.Length + " bytes exceed threshold for logging.");
                }

            }
            catch (IntegrationInvalidLoanNumberException)
            {
                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(1000); // Delay an invalid loan number request by 1 second to avoid potential abuse.
                throw;
            }
            catch (AccessDenied)
            {
                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(1000); // Delay an access denied request by 1 second to avoid potential abuse.
                throw;
            }
            catch (PageDataAccessDenied)
            {
                throw;
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
                throw;
            }
            finally
            {
                sw.Stop();
                Tools.LogInfo("Loan.asmx::LoadImpl", "Duration: " + sw.ElapsedMilliseconds + "ms. ");
            }
            return ret;
        }

        /// <summary>
        /// Deletes the application with the specified applicant from the specified loan.  Result is ok if deletion works, otherwise returns error.
        /// If there is only one applicant on an application, it deletes the application, unless there is only one application.
        /// If deleting the borrower, the coborrower becomes the new borrower.
        /// </summary>
        [WebMethod(Description = "Delete an applicant from an application. [sTicket] Ticket returned by AuthServices.asmx.  [sLNm] The LendingQB Loan Name. [applicantSSN] The SSN of the borrower/coborrower we'll delete.")]
        public string DeleteApplicant(string sTicket, string sLNm, string applicantSSN)
        {

            // opm 76095
            try
            {
                AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm); // this also sets the principal, then used for CPageData.
                CallVolume.IncreaseAndCheck(principal);
                return ApplicantDeleter.DeleteApplicant(sLNm, applicantSSN, principal);
            }
            catch (DeleteApplicationPermissionDenied exc)
            {
                Tools.LogError(exc);
                return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc); // db - Being conservative here by e-mailing this exc - if it gets annoying, we can remove this
                return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
            }
            catch (Exception exc)
            {
                CBaseException e = new CBaseException(ErrorMessages.Generic, exc);
                Tools.LogErrorWithCriticalTracking(e);
                return WebServiceUtilities.GenerateErrorResponseXml_Old(e.UserMessage);
            }
        }

        private class ApplicantDeleter  // the whole point of this class is to make it easy to call Delete App and Swap Borrower / Coborrower.
        {
            public static string DeleteApplicant(string sLNm, string applicantSSN, AbstractUserPrincipal principal)
            {
                // opm 76095
                var applicantSSNWODashes = applicantSSN.Replace("-", ""); // make dashes not matter.

                StringBuilder debugMessage = new StringBuilder("Loan.asmx::DeleteApplication principal.UserId='" + principal.UserId
                        + "', sLNm='" + sLNm + "', applicantSSN=" + applicantSSN + Environment.NewLine);

                Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

                if (sLId == Guid.Empty)
                {
                    return WebServiceUtilities.GenerateErrorResponseXml_Old((new IntegrationInvalidLoanNumberException(sLNm)).UserMessage);
                }
                AuthServiceHelper.CheckLoanAuthorization(sLId, WorkflowOperations.WriteLoan);

                if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
                {
                    return WebServiceUtilities.GenerateErrorResponseXml_Old(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                }

                var dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(ApplicantDeleter)); //CSwapCoborrowerData(sLId);  // do this since it should have the info for deleting either borrower / coborrower.
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                bool foundApplicant = false;
                bool needToDeleteApplication = false;
                bool deletedApplication = false;
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    var appData = dataLoan.GetAppData(i);
                    if (applicantSSNWODashes == appData.aSsn.Replace("-", ""))
                    {
                        if (string.IsNullOrEmpty(appData.aCSsn)
                            && string.IsNullOrEmpty(appData.aCFirstNm)
                            && string.IsNullOrEmpty(appData.aCLastNm))
                        {
                            // just delete the whole application if deleting the borrower and the coborrower is empty.
                            needToDeleteApplication = true;
                            deletedApplication = dataLoan.DelApp(appData.aAppId);
                        }
                        else
                        {
                            // to delete the borrower, we'll swap the two first, then delete. (calling existing functions)
                            appData.SwapMarriedBorAndCobor();
                            appData.DelMarriedCobor();
                            dataLoan.Save();
                        }
                        foundApplicant = true;
                        break;
                    }
                    else if (applicantSSNWODashes == appData.aCSsn.Replace("-", ""))
                    {
                        if (string.IsNullOrEmpty(appData.aBSsn)
                            && string.IsNullOrEmpty(appData.aBFirstNm)
                            && string.IsNullOrEmpty(appData.aBLastNm))
                        {
                            // just delete the whole application if deleting the coborrower and the borrower is empty.
                            needToDeleteApplication = true;
                            deletedApplication = dataLoan.DelApp(appData.aAppId);
                        }
                        else
                        {
                            appData.DelMarriedCobor();
                            dataLoan.Save();
                        }
                        foundApplicant = true;
                        break;
                    }
                }
                if (foundApplicant)
                {
                    if (needToDeleteApplication && false == deletedApplication)
                    {
                        debugMessage.Append(" fail.");
                        Tools.LogInfo(debugMessage.ToString());
                        return WebServiceUtilities.GenerateErrorResponseXml_Old("Borrower/Coborrower with SSN: '" + applicantSSN + "' was found for loan '" + sLNm + " but was not deleted as there was only one applicant and application.");
                    }
                    else
                    {
                        // don't call Save after delete apps, it will error.
                        debugMessage.Append(" success");
                        Tools.LogInfo(debugMessage.ToString());
                        return "<result status=\"OK\"/>";
                    }
                }
                else
                {
                    debugMessage.Append(" fail.");
                    Tools.LogInfo(debugMessage.ToString());
                    return WebServiceUtilities.GenerateErrorResponseXml_Old("Borrower/Coborrower with SSN: '" + applicantSSN + "' was not found for loan '" + sLNm);
                }

            }

        }

        [WebMethod]
        public string ParseFnma32ToLoXml(string sTicket, string fnma32Content, string sXmlQuery)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
			CallVolume.IncreaseAndCheck(principal);
			
            StringBuilder debugMessage = new StringBuilder("Loan.asmx::ParseFnma32ToLoXml");
            debugMessage.AppendLine();
            string ret;
            Guid sLId = Guid.Empty;
            try
            {
                sLId = QuickPricerLoanPoolManager.GetUnusedLoan(principal);
                debugMessage.AppendLine("QuickPricerLoanPoolManager.GetUnusedLoan=" + sLId);

                CPageData dataLoan = new CQuickPricerLoanData(sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                dataLoan.sEmployeeManagerId = principal.EmployeeId;
                dataLoan.sEmployeeLenderAccExecId = principal.EmployeeId;
                dataLoan.sEmployeeCallCenterAgentId = principal.EmployeeId;
                dataLoan.sEmployeeLoanOpenerId = principal.EmployeeId;
                dataLoan.sEmployeeUnderwriterId = principal.EmployeeId;
                dataLoan.sEmployeeLockDeskId = principal.EmployeeId;
                dataLoan.sEmployeeLoanRepId = principal.EmployeeId;
                dataLoan.sEmployeeProcessorId = principal.EmployeeId;
                dataLoan.sEmployeeBrokerProcessorId = principal.EmployeeId;

                dataLoan.Save();

                FannieMae32Importer importer = new FannieMae32Importer();
                importer.IsByPassPermission = true;
                importer.ImportIntoExisting(fnma32Content, sLId);

                ret = LOFormatExporter.Export(sLId, principal, sXmlQuery);

            }
            catch (Exception exc)
            {
                debugMessage.AppendLine("Error:" + exc.ToString());
                throw;
            }
            finally
            {
                if (sLId != Guid.Empty)
                {
                    QuickPricerLoanPoolManager.ReleaseToPool(principal, sLId);
                    debugMessage.AppendLine("QuickPricerLoanPoolManager.ReleaseToPool sLId=" + sLId);
                }
                Tools.LogInfo(debugMessage.ToString());
            }
            return ret;

        }
        [WebMethod(Description = useEDocs + "Add a PDF to a loan file. [sTicket] Ticket returned by AuthService.asmx.  [sLNm] The LendingQB Loan Name.  [documentType] The PDF document type name.  [notes] Notes related to the PDF document.  [sDataContent] The base 64 encoded PDF document.")]
        public string UploadPDFDocument(string sTicket, string sLNm, string documentType, string notes, string sDataContent)
        {
            return edocsService.UploadPDFDocument(sTicket, sLNm, documentType, notes, sDataContent);
        }

        [WebMethod(Description = useEDocs + "Add a PDF to a loan file. [sTicket] Ticket returned by AuthService.asmx.  [sLNm] The LendingQB Loan Name.  [aAppId] The Application Id to contain the file.  [documentType] The PDF document type name.  [notes] Notes related to the PDF document.  [sDataContent] The base 64 encoded PDF document.")]
        public string UploadPDFDocumentToApp(string sTicket, string sLNm, Guid aAppId, string documentType, string notes, string sDataContent)
        {
            return edocsService.UploadPDFDocumentToApp(sTicket, sLNm, aAppId, documentType, notes, sDataContent);
        }

        [WebMethod(Description = useEDocs + "Add a PDF to a loan file. [sTicket] Ticket returned by AuthService.asmx.  [sLNm] The LendingQB Loan Name.  [documentType] The PDF document type name.  [notes] Notes related to the PDF document.  [sDataContent] The base 64 encoded PDF document. [serviceType] The type of service currently in use.")]
        public string UploadPDFDocumentAsService(string sTicket, string sLNm, string documentType, string notes, string sDataContent, TypeOfService serviceType)
        {
            return edocsService.UploadPDFDocumentAsService(sTicket, sLNm, documentType, notes, sDataContent, serviceType);
        }

        [WebMethod(Description = useEDocs + "Add a PDF to a loan file. [sTicket] Ticket returned by AuthService.asmx.  [sLNm] The LendingQB Loan Name.  [aAppId] The Application Id to contain the file.  [documentType] The PDF document type name.  [notes] Notes related to the PDF document.  [sDataContent] The base 64 encoded PDF document. [serviceType] The type of service currently in use.")]
        public string UploadPDFDocumentToAppAsService(string sTicket, string sLNm, Guid aAppId, string documentType, string notes, string sDataContent, TypeOfService serviceType)
        {
            return edocsService.UploadPDFDocumentToAppAsService(sTicket, sLNm, aAppId, documentType, notes, sDataContent, serviceType);
        }

        private string ExportToPoint(Guid sLId, AbstractUserPrincipal principal)
        {
            CPointData dataLoan = new CPointData(sLId);
            dataLoan.SetFormatTarget(FormatTarget.PointNative);
            dataLoan.InitLoad();

            NameValueCollectionWrapper rg = new NameValueCollectionWrapper(new NameValueCollection());
            PointInteropLib.DataFile dataFile = new PointInteropLib.DataFile();
            PointDataConversion pointConversion = new PointDataConversion(principal.BrokerId, dataLoan.sLId);
            using (MemoryStream outputStream = new MemoryStream(100000))
            {
                XmlTextWriter xmlWriter = new XmlTextWriter(outputStream, System.Text.Encoding.ASCII);
                xmlWriter.WriteStartElement("PointFormat");
                for (int nApps = 0; nApps < dataLoan.nApps; nApps++)
                {
                    using (MemoryStream stream = new MemoryStream(100000))
                    {
                        if (nApps == 0)
                            xmlWriter.WriteStartElement("Borrower");
                        else
                            xmlWriter.WriteStartElement("Coborrower");

                        pointConversion.TransferDataToPoint(dataLoan, rg, nApps);
                        dataFile.WriteSortedStream(stream, rg.GetCollection());

                        xmlWriter.WriteCData(Convert.ToBase64String(stream.GetBuffer(), 0, (int)stream.Position));

                        xmlWriter.WriteEndElement();
                    }
                }
                xmlWriter.WriteEndElement(); // </PointFormat>
                xmlWriter.Flush();
                return System.Text.ASCIIEncoding.ASCII.GetString(outputStream.GetBuffer(), 0, (int)outputStream.Position);
            }

        }

        private string ExportToFannieMae32(Guid sLId)
        {
            FannieMae32Exporter exporter = new FannieMae32Exporter(sLId);

            byte[] buffer = exporter.Export();
            return System.Text.ASCIIEncoding.ASCII.GetString(buffer);
        }

        [WebMethod(Description = "Update the loan file with new information. [sDataContent] The information to update. Only fields that needs updating should be sent over. [format] Identifies the output format to use. 0 - LO XML Query. <RETURN> - XML result string indicating if the Save operation succeeded or not.")]
        public string Save(string sTicket, string sLNm, string sDataContent, int format)
        {
            IntegrationImportFormat importFormat = (IntegrationImportFormat)format;
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }

            return Save(principal, sLId, sDataContent, importFormat);
        }

        [WebMethod(Description = "Update the loan file with new information. [sDataContent] The information to update. Only fields that needs updating should be sent over. [format] Identifies the output format to use. 0 - LO XML Query. <RETURN> - XML result string indicating if the Save operation succeeded or not.")]
        public string SaveByRefNumber(string sTicket, string sLRefNm, string sDataContent, int format)
        {
            IntegrationImportFormat importFormat = (IntegrationImportFormat)format;
            AbstractUserPrincipal principal = GetPrincipalFromTicketAndValidateByRefNum(sTicket, sLRefNm);

            Guid sLId = Tools.GetLoanIdByLoanReferenceNumber(principal.BrokerId, sLRefNm);

            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanReferenceNumberException(sLRefNm);
            }

            return Save(principal, sLId, sDataContent, importFormat);
        }

        private string Save(AbstractUserPrincipal principal, Guid sLId, string sDataContent, IntegrationImportFormat format)
        {
            StringBuilder debugMessage = new StringBuilder();
            Stopwatch sw = Stopwatch.StartNew();
            string responseXml = string.Empty;

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                responseXml = WebServiceUtilities.GenerateErrorResponseXml_Old(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                return responseXml;
            }
            
			CallVolume.IncreaseAndCheck(principal);
            try
            {
                debugMessage.AppendLine("Loan.asmx::SaveImpl sLId=" + sLId + ", format=" + format);
                debugMessage.AppendLine();
                if (format == IntegrationImportFormat.LOFile)
                {
                    if (string.IsNullOrEmpty(sDataContent) == false)
                    {
                        debugMessage.AppendLine("=== REQUEST XML ===");
                        try
                        {
                            debugMessage.AppendLine(Tools.RedactXmlUsingXPath(sDataContent, "//@password", "//creditcard/@*"));
                        }
                        catch (XmlException)
                        {
                            var dataContentForLogging = Tools.RedactXmlUsingRegex(sDataContent, "password");
                            dataContentForLogging = System.Text.RegularExpressions.Regex.Replace(dataContentForLogging, "<creditcard[^>]*(/>|>[^<]*<creditcard\\s*>)", "<creditcard redacted=\"true\" />");
                            debugMessage.Append(dataContentForLogging);
                        }
                    }
                }
                else
                {
                    debugMessage.AppendLine("=== REQUEST DATA CONTENT ===");
                    debugMessage.AppendLine(sDataContent);
                }
                debugMessage.AppendLine();
                debugMessage.AppendLine("=== RESPONSE ===");

                List<WorkflowOperation> operations = new List<WorkflowOperation>()
                {
                    WorkflowOperations.WriteLoan,
                    WorkflowOperations.RunPmlForAllLoans,
                    WorkflowOperations.RunPmlForRegisteredLoans,
                    WorkflowOperations.RunPmlToStep3
                };

                Dictionary<WorkflowOperation, bool> operationAuthorizedDict = AuthServiceHelper.AreOperationsAuthorized(sLId, operations);
                if (operationAuthorizedDict.Values.All((result) => !result))
                {
                    throw new PageDataAccessDenied(ErrorMessages.GenericAccessDenied);
                }

                // 10/29/2010 dd - OPM 47751 - Base on specs, if there is a pending changes for loan file then do not 
                // allow a Save through web service.
                List<string> modifiedUserList = CPageData.GetRecentModificationUserName(sLId, principal);
                if (modifiedUserList.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Loan file is being editted by other users and could not be saved. Users editing this file: ");
                    bool isFirst = true;
                    foreach (var o in modifiedUserList)
                    {
                        if (isFirst == false)
                        {
                            sb.Append(", ");
                        }
                        isFirst = false;
                        sb.Append(o);
                    }
                    responseXml = WebServiceUtilities.GenerateErrorResponseXml_Old(sb.ToString());
                    debugMessage.AppendLine(responseXml);
                    return responseXml;
                }

                string ret = "";
                LoFormatImporterResult importResult = null;
                switch (format)
                {
                    case IntegrationImportFormat.LOFile:
                        importResult = LOFormatImporter.Import(sLId, principal, sDataContent);
                        ret = importResult.ErrorMessages;
                        break;
                    case IntegrationImportFormat.FannieMae32:
                        try
                        {
                            FannieMae32Importer importer = new FannieMae32Importer();
                            importer.ImportIntoExisting(sDataContent, sLId);
                        }
                        catch (Exception exc)
                        {
                            Tools.LogErrorWithCriticalTracking(exc);
                            ret = "Unable to import FNMA file.";
                        }
                        break;
                    case IntegrationImportFormat.Encompass_Mismo23:
                        EncompassMismoImporter.Import(sLId, sDataContent, false);
                        break;
                    case IntegrationImportFormat.ULDD:
                        UlddImporter ulddImporter = new UlddImporter();
                        ulddImporter.ImportIntoExisting(sLId, sDataContent);
                        break;
                    default:
                        throw new CBaseException(ErrorMessages.InvalidIntegrationFormat, format + " is not valid integration format");
                }

                if (importResult != null && importResult.ResultStatus == LoFormatImporterResultStatus.Error)
                {
                    responseXml = WebServiceUtilities.GenerateErrorResponseXml_Old(ret);
                    debugMessage.AppendLine(responseXml);
                    return responseXml;
                }

                if (ret == "")
                {
                    responseXml = "<result status=\"OK\"/>";
                    debugMessage.AppendLine(responseXml);
                    return responseXml;
                }
                else
                {
                    responseXml = WebServiceUtilities.GenerateWarningResponseXml_Old(ret);
                    debugMessage.AppendLine(responseXml);
                    return responseXml;
                }
            }
            catch (IntegrationInvalidLoanNumberException exc)
            {
                responseXml = WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
                debugMessage.AppendLine(responseXml);
                return responseXml;
            }
            catch (BlankLoanNumberException exc)
            {
                responseXml = WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
                debugMessage.AppendLine(responseXml);
                return responseXml;
            }
            catch (PageDataAccessDenied exc) // OPM 20377
            {
                responseXml = WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
                debugMessage.AppendLine(responseXml);
                return responseXml;
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                responseXml = WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
                debugMessage.AppendLine(responseXml);
                return responseXml;
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                responseXml = WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
                debugMessage.AppendLine(responseXml);
                return responseXml;
            }
            catch (XmlException exc)
            {
                responseXml = WebServiceUtilities.GenerateErrorResponseXml_Old(exc.Message);
                debugMessage.AppendLine(responseXml);
                return responseXml;
            }
            catch (Exception exc)
            {
                CBaseException e = new CBaseException(ErrorMessages.Generic, exc);
                Tools.LogErrorWithCriticalTracking(e);
                responseXml = WebServiceUtilities.GenerateErrorResponseXml_Old(e.UserMessage);
                debugMessage.AppendLine(responseXml);
                return responseXml;
            }
            finally
            {
                sw.Stop();

                debugMessage.AppendLine();
                debugMessage.AppendLine("Execution in " + sw.ElapsedMilliseconds + "ms.");

                Tools.LogInfo(debugMessage.ToString());
            }
        }

        [WebMethod(Description = "Returns a TRUE if the loan # exists.")]
        public bool IsLoanExisted(string sTicket, string sLNm)
        {
            StringBuilder debugMessage = new StringBuilder();
            debugMessage.AppendLine("Loan.asmx::IsLoanExisted sLNm=[" + sLNm + "]");

            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
            bool found = false;
            if (sLId == Guid.Empty)
            {
                debugMessage.AppendLine("Not Found");
            }
            else
            {
                try
                {
                    AuthServiceHelper.CheckLoanAuthorization(sLId, WorkflowOperations.ReadLoan);
                    found = true;
                }
                catch (AccessDenied e)
                {
                    debugMessage.AppendLine("Found but with access denied "  + sLId + " "  + e.ToString());
                    throw;
                }
            }

            if (found)
            {
                debugMessage.Append("  Result: Found");
            }
            else
            {
                debugMessage.Append("  Result: Not Found");
            }

            Tools.LogInfo(debugMessage.ToString());
            return found;
        }

        [WebMethod(Description = "This method is deprecated. Please use the RetrieveCustomReport method in Reporting.asmx instead.")]
        public string RetrieveCustomReport(string sTicket, string sQueryNm, bool includeAllWithAccess)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            // 8/10/2010 vm - Denying non LO users access to RetrieveCustomReport.
            if (principal.ApplicationType != E_ApplicationT.LendersOffice)
            {
                return WebServiceUtilities.GenerateErrorResponseXml_Old(ErrorMessages.GenericAccessDenied);
            }

            Tools.LogInfo("Custom Report: " + sQueryNm + Environment.NewLine + "IncludeAllWithAccess: " + includeAllWithAccess);
            Guid queryId = Guid.Empty;
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            parameters.Add(new SqlParameter("@QueryName", sQueryNm));

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "RetrieveQueryIdByQueryNameAndEmployeeId", parameters))
            {
                if (reader.Read())
                {
                    queryId = new Guid(reader["QueryId"].ToString());
                }
            }

            if (queryId == Guid.Empty)
            {
                parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@BrokerId", principal.BrokerId));
                parameters.Add(new SqlParameter("@QueryName", sQueryNm));

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "RetrieveQueryIdByQueryNameAndBrokerId", parameters))
                {
                    if (reader.Read())
                    {
                        queryId = new Guid(reader["QueryId"].ToString());
                    }

                }
            }

            if (queryId == Guid.Empty)
            {

                parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@BrokerId", BrokerDB.AdminBr));
                parameters.Add(new SqlParameter("@QueryName", sQueryNm));

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerDB.AdminBr, "RetrieveQueryIdByQueryNameAndBrokerId", parameters))
                {
                    if (reader.Read())
                    {
                        queryId = new Guid(reader["QueryId"].ToString());
                    }
                    else
                    {
                        // Return error because the user passed an invalid query name.
                        return WebServiceUtilities.GenerateErrorResponseXml_Old(ErrorMessages.CannotFindCustomReportQuery);
                    }
                }
            }

            LoanReporting lr = new LoanReporting();
            Report ra = null;

            if (includeAllWithAccess) // Include all loans the user has access to
            {
                ra = lr.RunReport(principal.BrokerId, queryId, principal.UserId, E_ReportExtentScopeT.Default);
            }
            else // Include only loans assigned to the user
            {
                ra = lr.RunReport(principal.BrokerId, queryId, principal.UserId, E_ReportExtentScopeT.Assign);
            }

            if (ra == null)
            {
                return "";
            }
            else
            {
                return ra.ConvertToCsv(principal);
            }
        }

        [WebMethod]
        public string DeleteLoan(string sTicket, string sLNm)
        {
            StringBuilder debugMessage = new StringBuilder();

            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
			CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            debugMessage.AppendLine("Loan.asmx::DeleteLoan(" + principal.LoginNm + ", sLNm=" + sLNm);

            string result = string.Empty;
            if (sLId == Guid.Empty)
            {
                result = WebServiceUtilities.GenerateErrorResponseXml_Old("Invalid loan number");
            }
            else if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                result = WebServiceUtilities.GenerateErrorResponseXml_Old(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }
            else
            {
                try
                {
                    Tools.DeclareLoanFileInvalid(principal, sLId, false, false);
                    result = "<result status=\"OK\"/>";
                }
                catch (CBaseException exc)
                {
                    result = WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
                }
            }


            Tools.LogInfo(debugMessage.ToString());
            return result;
        }

        [WebMethod(Description = "Create a new loan file and return a service result for the newly created loan.")]
        public string CreateWithOptions(string sTicket, string optionsXml)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            LoanLoXmlServiceResult result = new LoanLoXmlServiceResult();

            ParseResult<LoanCreationOptions> optionsResult = LoanCreationOptions.ParseLoanCreationOptionsXml(optionsXml, principal);
            if (!optionsResult.IsSuccessful)
            {
                string errorMessage = optionsResult.ErrorMessage ?? ErrorMessages.Generic;
                Tools.LogWarning($"Unable to parse {nameof(LoanCreationOptions)} from [{optionsXml}]. Sending error message: \"{errorMessage}\"");
                result.AppendError(errorMessage);
            }
            else
            {
                LoanCreationResult loanCreationResult = LoanCreation.CreateWithOptions(principal, optionsResult.ValueOrDefault);
                if (loanCreationResult.IsSuccessful)
                {
                    result.AddLoanElement(LoXmlServiceResult.CreateFieldElement(nameof(CPageData.sLNm), loanCreationResult.LoanName));
                }
                else
                {
                    result.AppendError(loanCreationResult.ErrorMessage);
                }
            }

            return result.ToResponse();
        }

        /// <summary>
        /// Pass empty sTemplateNm to create from blank template
        /// </summary>
        [WebMethod(Description = "Create a new loan file and return a randomly generated loan name. [sTemplateNm] - The loan template number used to initialized the loan file. This value can be left blank.")]
        public string Create(string sTicket, string sTemplateNm)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            Guid templateId = Guid.Empty;
            if (!string.IsNullOrEmpty(sTemplateNm))
            {
                templateId = Tools.GetLoanIdByLoanName(principal.BrokerId, sTemplateNm);
                if (templateId == Guid.Empty)
                {
                    // Throw error because user pass invalid template name.
                    throw new CBaseException(ErrorMessages.CannotFindLoanTemplate, ErrorMessages.CannotFindLoanTemplate);
                }
            }

            LoanCreationResult result = LoanCreation.CreateWithOptions(principal, new LoanCreationOptions(templateId: templateId), new LoanCreationPermissionResolver.AlwaysPassResolver(principal));
            if (!result.IsSuccessful)
            {
                throw new CBaseException(result.ErrorMessage, result.ErrorMessage);
            }

            if (principal.BrokerId == new Guid("719521A1-CA41-4841-A15A-7C219A6090E2"))
            {
                // 1/24/2014 dd - eOPM 500105 - PML0180 Nations Direct Mortgage is a PML 1.0 client,
                // however they are using one of the PML custom keyword in our engine. Since
                // Encompass does not pass over the default value custom keyword when create a new loan
                // therefore we must set it ourself.
                if (!result.SourceLoanIdentifier.HasValue || result.SourceLoanIdentifier == Guid.Empty)
                {
                    // 1/24/2014 dd - Only set if create from blank loan.
                    CPageData dataLoan = new CPageData(result.LoanIdentifier, new string[] { "sCustomPMLField1" });
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    dataLoan.sCustomPMLField1_rep = "0"; // Default value
                    dataLoan.Save();
                }
            }

            return result.LoanName;
        }

        [WebMethod(Description="Create new lead and return a lead name.[sTemplateNm] - The loan template number used to initialized the lead file. This value can be left blank.")]
        public string CreateLead(string sTicket, string sTemplateNm)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            Guid templateId = Guid.Empty;
            if (!string.IsNullOrEmpty(sTemplateNm))
            {
                templateId = Tools.GetLoanIdByLoanName(principal.BrokerId, sTemplateNm);
                if (templateId == Guid.Empty)
                {
                    // Throw error because user pass invalid template name.
                    throw new CBaseException(ErrorMessages.CannotFindLoanTemplate, ErrorMessages.CannotFindLoanTemplate);
                }
            }

            LoanCreationResult result = LoanCreation.CreateWithOptions(principal, new LoanCreationOptions(isLead: true, templateId: templateId), new LoanCreationPermissionResolver.AlwaysPassResolver(principal));
            if (result.IsSuccessful)
            {
                return result.LoanName;
            }

            throw new CBaseException(result.ErrorMessage, result.ErrorMessage);
        }

        private string RegisterLoanProgramWithRateOptionIdInternal(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee, string rateOptionId, bool skipDuplicateCheck, List<E_AgentRoleT> rolesToEmailPmlCertTo, List<EmailAddress> emailsToEmailPmlCertTo)
        {
            IncreaseAndCheckCallVolume(sTicket, sLNm);
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            List<WorkflowOperation> operations = new List<WorkflowOperation>()
            {
                WorkflowOperations.UseOldSecurityModelForPml,
                WorkflowOperations.RegisterLoan
            };

            Dictionary<WorkflowOperation, bool> operationAuthorizedDict = AuthServiceHelper.AreOperationsAuthorized(sLId, operations);
            if (operationAuthorizedDict.Values.All((result) => !result))
            {
                string message = string.Empty;
                foreach (var kvp in operationAuthorizedDict)
                {
                    message += $"{kvp.Key.Id}: {kvp.Value}";
                }

                throw new PageDataAccessDenied(message);
            }

            try
            {
                DataAccess.LoanComparison.DuplicateFinder.SetSkipLoanSubmissionCheck(sLId, skipDuplicateCheck);
                Tools.LogInfo("Loan.asmx::RegisterLoanProgram(" + principal.LoginNm + ", sLNm=" + sLNm + ", lLpTemplateId=" + lLpTemplateId + ", requestRate=" + requestedRate + ", requestedFee=" + requestedFee + ", rateOptionId=" + rateOptionId + ", skipDuplicateCheck=" + skipDuplicateCheck);
                DistributeUnderwritingEngine.RegisterLoanProgram(principal, sLId, new Guid(lLpTemplateId), requestedRate, requestedFee, rateOptionId, false /* isRequestingRateLock */);

                SendPmlCertificate(principal, sLId, false /* isRateLockRequest */, rolesToEmailPmlCertTo, emailsToEmailPmlCertTo);
            }
            catch (CBaseException exc)
            {
                if (exc.UserMessage.StartsWith("RATE_CHANGE_ERR:") == true)
                {
                    // Error Message in format RATE_CHANGE_ERR:{oldRate}|{oldPoint}|{newRate}|{newPoint}|{rateId}
                    string[] parts = exc.UserMessage.Substring("RATE_CHANGE_ERR:".Length).Split('|');
                    if (parts.Length != 5)
                    {
                        return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
                    }
                    else
                    {
                        return string.Format("<result status=\"Error\"><error code=\"RateChange\" oldRate=\"{0}\" oldPoint=\"{1}\" newRate=\"{2}\" newPoint=\"{3}\" rateOptionId=\"{4}\">Rate change due to DTI adjustment</error></result>",
                            parts[0], parts[1], parts[2], parts[3], parts[4]);
                    }
                }
                else if(string.Equals(exc.DeveloperMessage, ErrorMessages.PML.InsufficientPermissionToSubmitWithoutCredit, StringComparison.OrdinalIgnoreCase))
                {
                    return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.DeveloperMessage);
                }
                else
                {
                    return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
                }
            }
            return "<result status=\"OK\"/>";
        }

        [WebMethod]
        public string RegisterLoanProgramWithRateOptionId(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee, string rateOptionId)
        {
            return RegisterLoanProgramWithRateOptionIdInternal(sTicket, sLNm, lLpTemplateId, requestedRate, requestedFee, rateOptionId, false, rolesToEmailPmlCertTo: null, emailsToEmailPmlCertTo: null);
        }

        [WebMethod]
        public string RegisterLoanProgram(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee)
        {
            return RegisterLoanProgramWithRateOptionIdInternal(sTicket, sLNm, lLpTemplateId, requestedRate, requestedFee, "0.000,0.000" /*rateOptionId*/, false, rolesToEmailPmlCertTo: null, emailsToEmailPmlCertTo: null);
        }

        [WebMethod]
        public string RegisterLoanProgramAndSkipDuplicateCheck(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee, bool skipDuplicateCheck)
        {
            return RegisterLoanProgramWithRateOptionIdInternal(sTicket, sLNm, lLpTemplateId, requestedRate, requestedFee, "0.000,0.000", skipDuplicateCheck, rolesToEmailPmlCertTo: null, emailsToEmailPmlCertTo: null);
        }

        [WebMethod]
        public string RegisterLoanProgramWithRateOptionIdAndSkipDuplicateCheck(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee, string rateOptionId, bool skipDuplicateCheck)
        {
            return RegisterLoanProgramWithRateOptionIdInternal(sTicket, sLNm, lLpTemplateId, requestedRate, requestedFee, rateOptionId, skipDuplicateCheck, rolesToEmailPmlCertTo: null, emailsToEmailPmlCertTo: null);
        }

        /// <summary>Registers a loan program. All the options are now passed in the LOXml XML blob rather than as parameters.</summary>
        /// <param name="sTicket">The authentication ticket.</param>
        /// <param name="sLNm">The loan number of the target loan.</param>
        /// <param name="optionsXml">The options XML. Uses LOXml format.</param>
        /// <returns>The result XML.</returns>
        /// <optionsXml>
        /// <![CDATA[ 
        /// <LOXmlFormat>
        /// <field id="LoanProgramTemplateId"></field>
        ///	<field id="RequestedRate"></field>
        ///	<field id="RequestedFee"></field>
        ///	<field id="RateOptionId"></field>
        ///	<field id="SkipDuplicateCheck"></field>
        ///	<collection id="PmlCertificateRoleRecipients">
        ///		<record>
        ///			<field id="Role"></field>
        ///		</record>
        ///	</collection>
        ///	<collection id="PmlCertificateEmailRecipients">
        ///		<record>
        ///			<field id="Email"></field>
        ///		</record>
        ///	</collection>
        ///</LOXmlFormat>
        /// ]]>
        /// </optionsXml>
        [WebMethod(Description = "Registers a loan program. Options can be passed in using an LOXmlFormat payload.")]
        public string RegisterLoanProgramWithOptions(string sTicket, string sLNm, string optionsXml)
        {
            List<string> errors;
            LockRegisterLoanProgramOptions options = LockRegisterLoanProgramOptions.Parse(optionsXml, out errors);
            if (options == null)
            {
                ErrorServiceResult errorResult = new ErrorServiceResult(errors);
                return errorResult.ToResponse();
            }

            return RegisterLoanProgramWithRateOptionIdInternal(sTicket, sLNm, options.LoanProgramTemplateId, options.RequestedRate, options.RequestedFee, options.RateOptionId, options.SkipDuplicateCheck, options.PmlCertificateRoleRecipients, options.PmlCertificateEmailRecipients);
        }

        private string LockLoanProgramWithRateOptionIdInternal(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee, string rateOptionId, bool skipDuplicateCheck, List<E_AgentRoleT> rolesToEmailPmlCertTo, List<EmailAddress> emailsToEmailPmlCertTo)
        {
            IncreaseAndCheckCallVolume(sTicket, sLNm);
            StringBuilder debugMessage = new StringBuilder();
            string result = string.Empty;
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                return WebServiceUtilities.GenerateErrorResponseXml_Old(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            try
            {
                // 4/17/2014 ir - OPM 178892: User requires ability to lock rate
                List<WorkflowOperation> operations = new List<WorkflowOperation>()
                {
                    WorkflowOperations.RequestRateLockCausesLock,
                    WorkflowOperations.RequestRateLockRequiresManualLock
                };

                Dictionary<WorkflowOperation, bool> operationsAuthorizedDict = AuthServiceHelper.AreOperationsAuthorized(sLId, operations);
                if (operationsAuthorizedDict.Values.All((authorizedResult) => !authorizedResult))
                {
                    throw new PageDataAccessDenied(ErrorMessages.GenericAccessDenied);
                }

                DataAccess.LoanComparison.DuplicateFinder.SetSkipLoanSubmissionCheck(sLId, skipDuplicateCheck);
                debugMessage.AppendLine("Loan.asmx::LockLoanProgramWithRateOptionId(" + principal.LoginNm + ", sLNm=" + sLNm + ", lLpTemplateId=" + lLpTemplateId + ", requestRate=" + requestedRate + ", requestedFee=" + requestedFee + ", rateOptionId=" + rateOptionId + ", skipDuplicateCheck=" + skipDuplicateCheck);

                DistributeUnderwritingEngine.RegisterLoanProgram(principal, sLId, new Guid(lLpTemplateId), requestedRate, requestedFee, rateOptionId, true /* isRequestingRateLock */);

                SendPmlCertificate(principal, sLId, true /* isRateLockRequest */, rolesToEmailPmlCertTo, emailsToEmailPmlCertTo);

                bool isRateLocked = false;
                if (operationsAuthorizedDict[WorkflowOperations.RequestRateLockCausesLock] == true)
                {
                    Utilities.LockRate(sLId, E_ApplicationT.PriceMyLoan);
                    isRateLocked = true;
                }

                return $"<result status=\"OK\" rateLocked=\"{isRateLocked}\" />";
            }
            catch (CBaseException exc)
            {
                if (exc.UserMessage.StartsWith("RATE_CHANGE_ERR:") == true)
                {
                    // Error Message in format RATE_CHANGE_ERR:{oldRate}|{oldPoint}|{newRate}|{newPoint}|{rateId}
                    string[] parts = exc.UserMessage.Substring("RATE_CHANGE_ERR:".Length).Split('|');
                    if (parts.Length != 5)
                    {
                        result = WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
                    }
                    else
                    {
                        result = string.Format("<result status=\"Error\"><error code=\"RateChange\" oldRate=\"{0}\" oldPoint=\"{1}\" newRate=\"{2}\" newPoint=\"{3}\" rateOptionId=\"{4}\">Rate change due to DTI adjustment</error></result>",
                            parts[0], parts[1], parts[2], parts[3], parts[4]);
                    }
                }
                else if (string.Equals(exc.DeveloperMessage, ErrorMessages.PML.InsufficientPermissionToSubmitWithoutCredit, StringComparison.OrdinalIgnoreCase))
                {
                    return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.DeveloperMessage);
                }
                else
                {
                    result = WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
                }
            }
            debugMessage.AppendLine();
            debugMessage.AppendLine(result);
            Tools.LogInfo(debugMessage.ToString());
            return result;
        }

        [WebMethod]
        public string LockLoanProgramWithRateOptionId(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee, string rateOptionId)
        {
            return LockLoanProgramWithRateOptionIdInternal(sTicket, sLNm, lLpTemplateId, requestedRate, requestedFee, rateOptionId, false, rolesToEmailPmlCertTo: null, emailsToEmailPmlCertTo: null);
        }

        [WebMethod]
        public string LockLoanProgram(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee)
        {
            return LockLoanProgramWithRateOptionIdInternal(sTicket, sLNm, lLpTemplateId, requestedRate, requestedFee, "0.000,0.000" /*rateOptionId*/, false, rolesToEmailPmlCertTo: null, emailsToEmailPmlCertTo: null);
        }

        [WebMethod]
        public string LockLoanProgramAndSkipDuplicateCheck(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee, bool skipDuplicateCheck)
        {
            return LockLoanProgramWithRateOptionIdInternal(sTicket, sLNm, lLpTemplateId, requestedRate, requestedFee, "0.000,0.000", skipDuplicateCheck, rolesToEmailPmlCertTo: null, emailsToEmailPmlCertTo: null);
        }

        [WebMethod]
        public string LockLoanProgramWithRateOptionIdAndSkipDuplicateCheck(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee, string rateOptionId, bool skipDuplicateCheck)
        {
            return LockLoanProgramWithRateOptionIdInternal(sTicket, sLNm, lLpTemplateId, requestedRate, requestedFee, rateOptionId, skipDuplicateCheck, rolesToEmailPmlCertTo: null, emailsToEmailPmlCertTo: null);
        }


        /// <summary>Locks a loan program. All the options are now passed in the LOXml XML blob rather than as parameters.</summary>
        /// <param name="sTicket">The authentication ticket.</param>
        /// <param name="sLNm">The loan number of the target loan.</param>
        /// <param name="optionsXml">The options XML. Uses LOXml format.</param>
        /// <returns>The result XML.</returns>
        /// <optionsXml>
        /// <![CDATA[ 
        /// <LOXmlFormat>
        /// <field id="LoanProgramTemplateId"></field>
        ///	<field id="RequestedRate"></field>
        ///	<field id="RequestedFee"></field>
        ///	<field id="RateOptionId"></field>
        ///	<field id="SkipDuplicateCheck"></field>
        ///	<collection id="PmlCertificateRoleRecipients">
        ///		<record>
        ///			<field id="Role"></field>
        ///		</record>
        ///	</collection>
        ///	<collection id="PmlCertificateEmailRecipients">
        ///		<record>
        ///			<field id="Email"></field>
        ///		</record>
        ///	</collection>
        ///</LOXmlFormat>
        /// ]]>
        /// </optionsXml>
        [WebMethod(Description = "Locks a loan program. Options can be passed in using an LOXmlFormat payload.")]
        public string LockLoanProgramWithOptions(string sTicket, string sLNm, string optionsXml)
        {
            List<string> errors;
            LockRegisterLoanProgramOptions options = LockRegisterLoanProgramOptions.Parse(optionsXml, out errors);
            if (options == null)
            {
                ErrorServiceResult errorResult = new ErrorServiceResult(errors);
                return errorResult.ToResponse();
            }

            return LockLoanProgramWithRateOptionIdInternal(sTicket, sLNm, options.LoanProgramTemplateId, options.RequestedRate, options.RequestedFee, options.RateOptionId, options.SkipDuplicateCheck, options.PmlCertificateRoleRecipients, options.PmlCertificateEmailRecipients);
        }

        private class LockRegisterLoanProgramOptions
        {
            public string LoanProgramTemplateId { get; private set; }

            public decimal RequestedRate { get; private set; }

            public decimal RequestedFee { get; private set; }

            public string RateOptionId { get; private set; }

            public bool SkipDuplicateCheck { get; private set; }

            public List<E_AgentRoleT> PmlCertificateRoleRecipients { get; private set; }

            public List<EmailAddress> PmlCertificateEmailRecipients { get; private set; }

            public static LockRegisterLoanProgramOptions Parse(string optionsXml, out List<string> errors)
            {
                errors = new List<string>();

                if (string.IsNullOrEmpty(optionsXml))
                {
                    errors.Add("Empty XML provided");
                    return null;
                }

                var parseResults = ServiceRequestOptions.ParseRequestOptions(optionsXml);
                if (!parseResults.IsSuccessful)
                {
                    errors.Add(parseResults.ErrorMessage ?? "Invalid XML");
                    return null;
                }

                LockRegisterLoanProgramOptions options = new LockRegisterLoanProgramOptions();
                var topLevelFields = new[]
                {
                    new { Key = "LoanProgramTemplateId",
                          Valid = new Func<string, bool>((field) => field.ToNullable<Guid>(Guid.TryParse).HasValue),
                          Setter = new Action<string>((field) => options.LoanProgramTemplateId = field) },
                    new { Key = "RequestedRate",
                          Valid = new Func<string, bool>((field) => field.ToNullable<decimal>(decimal.TryParse).HasValue),
                          Setter = new Action<string>((field) => options.RequestedRate = field.ToNullable<decimal>(decimal.TryParse).Value) },
                    new { Key = "RequestedFee",
                          Valid = new Func<string, bool>((field) => field.ToNullable<decimal>(decimal.TryParse).HasValue),
                          Setter = new Action<string>((field) => options.RequestedFee = field.ToNullable<decimal>(decimal.TryParse).Value) },
                    new { Key = "RateOptionId",
                          Valid = new Func<string, bool>((field) => true),
                          Setter = new Action<string>((field) => options.RateOptionId = field ?? "0.000,0.000") },
                    new { Key = "SkipDuplicateCheck",
                          Valid = new Func<string, bool>((field) => field == null || field.ToNullableBool().HasValue),
                          Setter = new Action<string>((field) => options.SkipDuplicateCheck = field.ToNullableBool() ?? false )}
                };

                foreach (var fieldInfo in topLevelFields)
                {
                    string field = parseResults.ValueOrDefault.Fields.GetValueOrNull(fieldInfo.Key);
                    if (!fieldInfo.Valid(field))
                    {
                        errors.Add($"Invalid value [{field}] for field {fieldInfo.Key}");
                        continue;
                    }

                    fieldInfo.Setter(field);
                }

                var pmlCertRolerecipients = parseResults.ValueOrDefault.Collections.GetValueOrNull("PmlCertificateRoleRecipients");
                if (pmlCertRolerecipients != null)
                {
                    options.PmlCertificateRoleRecipients = new List<E_AgentRoleT>();
                    foreach (var roleRecord in pmlCertRolerecipients)
                    {
                        string roleString;
                        E_AgentRoleT role;
                        if (!roleRecord.Fields.TryGetValue("Role", out roleString))
                        {
                            errors.Add("Empty record in PmlCertificateRoleRecipients");
                            continue;
                        }

                        if (!roleString.TryParseDefine<E_AgentRoleT>(out role, ignoreCase: true))
                        {
                            errors.Add($"Invalid value [{roleString}] for Role");
                            continue;
                        }

                        options.PmlCertificateRoleRecipients.Add(role);
                    }
                }

                var pmlCertEmailRecipients = parseResults.ValueOrDefault.Collections.GetValueOrNull("PmlCertificateEmailRecipients");
                if (pmlCertEmailRecipients != null)
                {
                    options.PmlCertificateEmailRecipients = new List<EmailAddress>();
                    foreach (var emailRecord in pmlCertEmailRecipients)
                    {
                        string emailString;
                        if (!emailRecord.Fields.TryGetValue("Email", out emailString))
                        {
                            errors.Add("Empty record in PmlCertificateEmailRecipients");
                            continue;
                        }

                        var email = EmailAddress.Create(emailString);
                        if (!email.HasValue)
                        {
                            errors.Add($"Invalid value [{emailString}] for Email");
                            continue;
                        }

                        options.PmlCertificateEmailRecipients.Add(email.Value);
                    }
                }

                if (errors.Any())
                {
                    return null;
                }

                return options;
            }
        }

        [WebMethod]
        public string BreakBrokerRateLock(string sTicket, string sLNm, string reason)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }
            else if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                return WebServiceUtilities.GenerateErrorResponseXml_Old(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }
            else if (string.IsNullOrEmpty(reason))
            {
                return WebServiceUtilities.GenerateErrorResponseXml_Old("Reason for breaking the rate lock was missing.");
            }

            try
            {
                CPageData dataLoan = new CPerformBreakRateLockData(sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.BreakRateLock(principal.DisplayName, reason);
                dataLoan.Save();
            }
            catch (CBaseException exc)
            {
                return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
            }
            return "<result status=\"OK\"/>";
        }

        [WebMethod]
        public string RemoveRequestedRate(string sTicket, string sLNm)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                return WebServiceUtilities.GenerateErrorResponseXml_Old(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            try
            {

                CPageData dataLoan = new CPerformBreakRateLockData(sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                //opm 19109 fs 06/16/08
                AbstractAuditItem auditItem = new RemoveRequestedRateAuditItem(principal, dataLoan.sNoteIRSubmitted_rep, dataLoan.sLOrigFPcSubmitted_rep, dataLoan.sRAdjMarginRSubmitted_rep, dataLoan.sQualIRSubmitted_rep, dataLoan.sOptionArmTeaserRSubmitted_rep, dataLoan.sIsOptionArmSubmitted, dataLoan.sTermSubmitted_rep, dataLoan.sDueSubmitted_rep, dataLoan.sFinMethTSubmitted.ToString());

                dataLoan.RemoveRequestedRate();

                dataLoan.Save();
                AuditManager.RecordAudit(dataLoan.sLId, auditItem);
            }
            catch (CBaseException exc)
            {
                return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
            }
            return "<result status=\"OK\"/>";
        }

        [WebMethod (Description = useEDocs)]
        public string ListEdocsByLoanNumber(string sTicket, string sLNm)
        {
            return edocsService.ListEdocsByLoanNumber(sTicket, sLNm);
        }

        [WebMethod(Description = useEDocs)]
        public byte[] DownloadEdocsPdfById(string sTicket, Guid docId)
        {
            return edocsService.DownloadEdocsPdfById(sTicket, docId);
        }

        [WebMethod(Description = useEDocs + "This method returns a PDF containing EDocs on the given loan file [sLNm] that are included in the shipping template [sShippingTemplateName]. EDocs with Protected status are included by default. Obsolete/Rejected docs are omitted. Does not preserve electronic signatures.")]
        public byte[] DownloadEdocsByShippingTemplate(string sTicket, string sShippingTemplateName, string sLNm, bool includeScreenedDocs, bool includeDocsWithoutStatus)
        {
            return edocsService.DownloadEdocsByShippingTemplate(sTicket, sShippingTemplateName, sLNm, includeScreenedDocs, includeDocsWithoutStatus);
        }

        private XsltArgumentList GetXsltParams(Guid brokerId)
        {
            string cssContents = ResourceManager.Instance.GetResourceContents(LendersOffice.ObjLib.Resource.ResourceType.CertificateCss, brokerId);
            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("VirtualRoot", "", Tools.VRoot);
            args.AddParam("GenericStyle", "", cssContents);
            args.AddParam("HideJs", "", "True");
            args.AddParam("Email", "", "True");
            return args;
        }

        private void SendPmlCertificate(AbstractUserPrincipal principal, Guid sLId, bool isRateLockRequest, List<E_AgentRoleT> rolesToEmailTo, List<EmailAddress> emailsToEmailTo)
        {
            CPageData dataLoan = new CSendEmailCertificateData(sLId);
            dataLoan.InitLoad();

            if (string.IsNullOrEmpty(dataLoan.sPmlCertXmlContent.Value))
            {
                return;
            }
            #region Generate HTML Content
            BranchDB branch = new BranchDB(dataLoan.sBranchId, dataLoan.sBrokerId);
            branch.Retrieve();

            bool isAttachLogo = branch.IsDisplayNmModified == false;

            string pmlLenderSiteId = Tools.GetFileDBKeyForPmlLogoWithLoanId(dataLoan.BrokerDB.PmlSiteID, dataLoan.sLId);

            string logoId = $"_logo_{DateTime.Now.Ticks}";
            byte[] logo = null;
            if (isAttachLogo)
            {
                try
                {
                    logo = FileDBTools.ReadData(E_FileDB.Normal, pmlLenderSiteId.ToString().ToLower() + ".logo.gif");
                }
                catch (FileNotFoundException)
                {
                }
            }

            var xsltParams = GetXsltParams(principal.BrokerId);
            xsltParams.AddParam("LenderPmlSiteId", "", pmlLenderSiteId);
            if (logo != null)
            {
                xsltParams.AddParam("LogoSource", "", $"cid:{logoId}");
            }
            string bodyContent = XslTransformHelper.Transform(GetXsltFileLocation(dataLoan.sBrokerId), dataLoan.sPmlCertXmlContent.Value, xsltParams);
            #endregion

            #region Gather Recipient email
            // OPM #1792 & #1850
            // Email to everyone assign to this loan include AE, Lock Desk, Underwriter etc.. and Loan Officer in official agent list.
            HashSet<string> uniqueEmails = new HashSet<string>();

            CAppData dataApp = dataLoan.GetAppData(0);
            string aBFirstNm = dataApp.aBFirstNm;
            string aBLastNm = dataApp.aBLastNm;
            string loanNm = dataLoan.sLNm;

            if (rolesToEmailTo == null && emailsToEmailTo == null)
            {
                // If roles/emails lists aren't initialized, we'll fall back to original behavior.
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
                CAgentFields agent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (string.IsNullOrEmpty(agent.EmailAddr) == false && !brokerDB.IsEmailPMLUserCertificateDisabled)
                {
                    uniqueEmails.Add(agent.EmailAddr.ToLower());
                }

                agent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.BrokerProcessor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (string.IsNullOrEmpty(agent.EmailAddr) == false && !brokerDB.IsEmailPMLUserCertificateDisabled)
                {
                    uniqueEmails.Add(agent.EmailAddr.ToLower());
                }

                if (string.IsNullOrEmpty(dataLoan.sEmployeeLenderAccExecEmail) == false)
                {

                    uniqueEmails.Add(dataLoan.sEmployeeLenderAccExecEmail.ToLower());
                }

                // 8/19/2005 dd - Part of OPM 2245. If loan is submit manually for underwrite do not email Lock Desk.
                if (isRateLockRequest)
                {
                    if (string.IsNullOrEmpty(dataLoan.sEmployeeLockDeskEmail) == false)
                    {
                        // 9/10/2009 vm - OPM 33691 Require LO users to always email the assigned lock desk when LOCKING a loan
                        uniqueEmails.Add(dataLoan.sEmployeeLockDeskEmail.ToLower());

                    }
                }
                if (string.IsNullOrEmpty(dataLoan.sEmployeeUnderwriterEmail) == false)
                {
                    uniqueEmails.Add(dataLoan.sEmployeeUnderwriterEmail.ToLower());
                }

                if (string.IsNullOrEmpty(dataLoan.sEmployeeProcessorEmail) == false)
                {
                    uniqueEmails.Add(dataLoan.sEmployeeProcessorEmail.ToLower());
                }
            }
            else
            {
                // At least one of the email lists is populated. We'll only send the PML cert to those emails.
                foreach (var email in emailsToEmailTo.CoalesceWithEmpty())
                {
                    uniqueEmails.Add(email.ToString());
                }

                foreach (var role in rolesToEmailTo.CoalesceWithEmpty())
                {
                    CAgentFields agent = dataLoan.sAgentCollection.GetAgentOfRole(role, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    if (!string.IsNullOrEmpty(agent.EmailAddr))
                    {
                        uniqueEmails.Add(agent.EmailAddr.ToLower());
                    }
                }
            }

            if (uniqueEmails.Count == 0)
            {
                return; // No Email address
            }    

            #endregion

            #region Send Certificates
            // 12/20/06 mf - OPM 8833. We can now get the "From" address based on
            // the rule settings of the broker and the assignments of the loan.
            EventNotificationRulesView rView = new EventNotificationRulesView(dataLoan.sBrokerId);
            string fromAddress = rView.GetFromAddressString(dataLoan.sLId);

            foreach (string recipient in uniqueEmails)
            {
                string subject = loanNm + " - " + aBFirstNm + " " + aBLastNm + " - Loan Certificate";

                if (principal.IsRateLockedAtSubmission)
                {
                    if (isRateLockRequest)
                        subject = loanNm + " - " + aBFirstNm + " " + aBLastNm + " - Rate Lock Request Certificate";
                    else
                        subject = loanNm + " - " + aBFirstNm + " " + aBLastNm + " - Loan Registration Certificate";
                }

                var cbe = new LendersOffice.Email.CBaseEmail(dataLoan.sBrokerId)
                {
                    DisclaimerType = E_DisclaimerType.NORMAL,
                    From = fromAddress,
                    To = recipient,
                    Subject = subject,
                    Message = bodyContent,
                    IsHtmlEmail = true
                };
                if (logo != null && logo.Length > 0)
                {
                    cbe.AddAttachment(logoId, logo);
                }
                cbe.Send();
            }
            #endregion

        }

        private string GetXsltFileLocation(Guid brokerId)
        {
            return ResourceManager.Instance.GetResourcePath(LendersOffice.ObjLib.Resource.ResourceType.PmlCertificateXslt, brokerId);
        }

        [WebMethod]
        public string PrepareLoanForResubmission(string sTicket, string sLNm)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }

            List<WorkflowOperation> operations = new List<WorkflowOperation>()
            {
                WorkflowOperations.WriteLoan,
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlToStep3
            };

            //If an LO/PML user does not have write access for a loan, If they do not have Run PML privilege then return an “Access denied” error if the user attempts to use web services to save the loan,Loan::PrepareLoanForResubmission and Loan::Save methods
            var operationsAuthorizedDict = AuthServiceHelper.AreOperationsAuthorized(sLId, operations);
            if (operationsAuthorizedDict.Values.All((result) => !result))
            {
                throw new PageDataAccessDenied(ErrorMessages.GenericAccessDenied);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                return WebServiceUtilities.GenerateErrorResponseXml_Old(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            try
            {
                // 10/29/2010 dd - OPM 47751 - Base on specs, if there is a pending changes for loan file then do not 
                // allow a Save through web service.
                List<string> modifiedUserList = CPageData.GetRecentModificationUserName(sLId, principal);
                if (modifiedUserList.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Loan file is being editted by other users and could not be saved. Users editing this file: ");
                    bool isFirst = true;
                    foreach (var o in modifiedUserList)
                    {
                        if (isFirst == false)
                        {
                            sb.Append(", ");
                        }
                        isFirst = false;
                        sb.Append(o);
                    }
                    return WebServiceUtilities.GenerateErrorResponseXml_Old(sb.ToString());
                }

                Tools.PrepareLoanForResubmission(sLId);
                return "<result status=\"OK\"/>";
            }
            catch (PageDataAccessDenied exc)
            {
                return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
            }
            catch (Exception exc)
            {
                CBaseException e = new CBaseException(ErrorMessages.Generic, exc);
                Tools.LogErrorWithCriticalTracking(e);
                return WebServiceUtilities.GenerateErrorResponseXml_Old(e.UserMessage);
            }
        }

        [WebMethod]
        public string CreateWithULDD(string sTicket, string xml)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            UlddImporter importer = new UlddImporter();
            return importer.ImportAsNewLoan(xml);
        }

        [WebMethod]
        public string CreateWithFannieMaeFile(string sTicket, string content)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            try
            {
                StringReader reader = new StringReader(content);

                string[] lines = null;
                ArrayList list = new ArrayList();
                string line = null;
                while ((line = reader.ReadLine()) != null)
                    list.Add(line);

                lines = (string[])list.ToArray(typeof(string));

                Guid templateId = Guid.Empty;

                if (principal.Type == "P")
                {
                    // 3/14/2007 dd - For "P" user always create loan using PmlLoanTemplateId
                    templateId = GetPmlLoanTemplateId(principal.BrokerId);
                }

                CPageData dataLoan = FannieMaeImporter.Import(principal, lines, FannieMaeImportSource.LendersOffice, true, templateId, true);
                return dataLoan.sLNm;

            }
            catch (InvalidFannieFileFormatException)
            {
                throw;
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                throw;
            }
        }

        private Guid GetPmlLoanTemplateId(Guid brokerId)
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
            return brokerDB.PmlLoanTemplateID;
        }

        [WebMethod]
        public string CreateWithCalyxPointFile(string sTicket, string base64content)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            try
            {
                byte[] buffer = Convert.FromBase64String(base64content);
                MemoryStream stream = new MemoryStream(buffer);

                Guid templateId = Guid.Empty;

                if (principal.Type == "P")
                {
                    // 3/14/2007 dd - For "P" user always create loan using PmlLoanTemplateId
                    templateId = GetPmlLoanTemplateId(principal.BrokerId);
                }

                CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.PointImport);
                Guid sLId = creator.BeginCreateImportBaseLoanFile(
                    sourceFileId: templateId,
                    setInitialEmployeeRoles: true,
                    addEmployeeOfficialAgent: true,
                    assignEmployeesFromRelationships: false,
                    branchIdToUse: Guid.Empty);

                CPointData dataLoan = new CImportPointData(sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                PointImportHelper importHelper = new PointImportHelper();
                importHelper.Import(dataLoan, principal.BrokerId, stream, dataLoan.sEmployeeLoanRepId, dataLoan.sEmployeeProcessorId, true, true);
                dataLoan.SetsLNmWithPermissionBypass(creator.LoanName); // 5/16/2005 dd - Always use the new name generate by LendingQB
                dataLoan.Save();

                creator.CommitFileCreation(false);

                return creator.LoanName;

            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                return WebServiceUtilities.GenerateErrorResponseXml_Old(exc.UserMessage);
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                throw;
            }
        }

        [WebMethod]
        public string ListClosingCostTemplateName(string sTicket)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            List<string> list = CCcTemplateData.ListClosingCostTemplateName(principal);

            XmlDocument doc = new XmlDocument();
            XmlElement rootElement = doc.CreateElement("closing_cost_template_list");
            doc.AppendChild(rootElement);

            foreach (var name in list)
            {
                XmlElement element = doc.CreateElement("closing_cost_template");
                rootElement.AppendChild(element);
                element.SetAttribute("name", name);
            }
            return doc.InnerXml;
        }

        [WebMethod]
        public string RetrieveClosingCostTemplateFeeList(string sTicket, string closingCostName)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            XmlDocument doc = new XmlDocument();
            XmlElement rootElement = doc.CreateElement("closing_cost_template_fee_list");

            doc.AppendChild(rootElement);

            CCcTemplateData closingCost = CCcTemplateData.RetrieveClosingCostTemplateByName(principal, closingCostName);

            rootElement.SetAttribute("gfe_version", closingCost.GfeVersion.ToString());
            foreach (var hudFee in closingCost.HudFeeItemList)
            {
                // 12/15/2010 dd - Only include fee that has at least non-zero fee.
                if (hudFee.PercentFee == 0 && hudFee.BaseAmountFee == 0)
                {
                    continue;
                }
                XmlElement feeElement = doc.CreateElement("fee");
                rootElement.AppendChild(feeElement);

                feeElement.SetAttribute("line_number", hudFee.HudLineNumber);
                feeElement.SetAttribute("description", hudFee.Description);
                feeElement.SetAttribute("percent_fee", hudFee.PercentFee.ToString());
                feeElement.SetAttribute("percent_base", hudFee.PercentBaseT.ToString());
                feeElement.SetAttribute("base_dollar_fee", hudFee.BaseAmountFee.ToString());
                feeElement.SetAttribute("is_apr", hudFee.IsApr.ToString());
                feeElement.SetAttribute("is_fha", hudFee.IsFhaAllowable.ToString());
                feeElement.SetAttribute("is_poc", hudFee.IsPoc.ToString());
                feeElement.SetAttribute("is_paid_to_broker", hudFee.IsPaidToBroker.ToString());
                feeElement.SetAttribute("gfe2010_section", hudFee.GfeSectionT.ToString());
            }
            return doc.InnerXml;
        }

        [WebMethod(Description = "[OBSOLETE] Use ListModifiedLoansByAppCode.")]
        public string ListModifiedLoans(string sTicket)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            Tools.LogInfo("Loan.asmx::ListModifiedLoans. Principal=" + principal.LoginNm);

            Guid appCode = GetDefaultAppCode(principal);

            return ListModifiedLoansImpl(appCode, principal, ConstStage.UseBatchWorkflowReadChecker);
        }

        [WebMethod(Description = "Returns an XML string containing a list of recently modified loans. " +
        "This method is used if the client wants to poll the LO/PML system on a fixed interval (try to keep this interval big) " +
        "for recently modified files so that they can be imported into a 3rd-party LOS. " +
        "Contact support for valid appCode. " +
        "THIS METHOD ONLY RETURN LOANS MODIFIED IN LAST 90 DAYS." +
        "The tracking of modified loan is not enabled by default. To have it enabled you need to contact our integration team, and they will turn on the tracking feature.")]
        public string ListModifiedLoansByAppCode(string sTicket, string appCode)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            Tools.LogInfo("Loan.asmx::ListModifiedLoansByAppCode. Principal=" + principal.LoginNm + ". AppCode=[" + appCode + "]");

            return ListModifiedLoansImpl(new Guid(appCode), principal, ConstStage.UseBatchWorkflowReadChecker);

        }

        private static string ListModifiedLoansImpl(Guid appCode, AbstractUserPrincipal principal, bool useBatchWorkflowReadChecker)
        {
            StringBuilder debugString = new StringBuilder();
            Stopwatch sw = Stopwatch.StartNew();
            Guid brokerId = principal.BrokerId;

            try
            {
                DateTime latestModifiedDate = DateTime.MinValue;

                XmlDocument doc = new XmlDocument();

                XmlElement rootElement = doc.CreateElement("modified_loans");
                doc.AppendChild(rootElement);

                // 10/8/07 db - OPM 18372 Restrict the list of files to those loans that the user has permission to see,
                // according to the permission required in the LO and PML loan find pages.
                string procedureName;
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@AppCode", appCode));

                if (principal.Type == "P")
                {
                    procedureName = "ListIntegrationModifiedFilesByBrokerId_PmlUser";
                    parameters.Add(new SqlParameter("@BrokerId", principal.BrokerId));
                    parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));

                    SqlParameter[] employeeIdParameter = {
                                                             new SqlParameter("@EmployeeId", principal.EmployeeId)
                                                         };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "IsPriceMyLoanManagerByEmployeeId", employeeIdParameter))
                    {
                        if (reader.Read() && (bool)reader["IsPmlManager"])
                            parameters.Add(new SqlParameter("@IsPmlManager", true));
                        else
                            parameters.Add(new SqlParameter("@IsPmlManager", false));
                    }
                }
                else if (principal.Type == "B")
                {
                    procedureName = "ListIntegrationModifiedFilesByBrokerId_BrokerUser";
                    parameters.Add(new SqlParameter("@BrokerId", principal.BrokerId));
                    parameters.Add(new SqlParameter("@BranchId", principal.BranchId));
                    parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
                    if (principal.HasPermission(Permission.BrokerLevelAccess))
                        parameters.Add(new SqlParameter("@AccessLevel", "broker"));
                    else if (principal.HasPermission(Permission.BranchLevelAccess))
                        parameters.Add(new SqlParameter("@AccessLevel", "branch"));
                    else
                        parameters.Add(new SqlParameter("@AccessLevel", "individual"));
                }
                else //In case an Internal user calls this
                {
                    procedureName = "ListIntegrationModifiedFilesByBrokerId";
                    parameters.Add(new SqlParameter("@BrokerId", principal.BrokerId));
                }
                debugString.AppendLine("exec " + procedureName);
                foreach (SqlParameter p in parameters)
                {
                    debugString.AppendLine("     " + p.ParameterName + "=" + p.Value);
                }

                List<KeyValuePair<Guid, XmlElement>> elementList = new List<KeyValuePair<Guid, XmlElement>>();
                List<Guid> workflowCheckLoans = new List<Guid>();

                List<IReadOnlyDictionary<string, object>> records = new List<IReadOnlyDictionary<string, object>>();

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, procedureName, parameters))
                {
                    while (reader.Read())
                    {
                        records.Add(reader.ToDictionary());
                    }
                }

                foreach (IReadOnlyDictionary<string, object> record in records)
                {
                    Guid sLId = (Guid)record["sLId"];
                    string sLNm = (string)record["sLNm"];
                    string sOldLNm = (string)record["sOldLNm"];
                    string aBNm = (string)record["aBNm"];
                    string aBSsn = string.Empty;
                    string sSpAddr = (string)record["sSpAddr"];
                    string sStatusT = (record["sStatusT"] is DBNull) ? string.Empty : ((int)record["sStatusT"]).ToString();
                    string sLRefNm = (record["sLRefNm"] is DBNull) ? string.Empty : (string)record["sLRefNm"];
                    bool isValid = (bool)record["IsValid"];

                    int sEncryptionMigrationVersion = record["sEncryptionMigrationVersion"] is DBNull ? (int)EncryptionMigrationVersion.Unmigrated : Convert.ToInt32(record["sEncryptionMigrationVersion"]);
                    Guid sEncryptionKey = record["sEncryptionKey"] is DBNull ? Guid.Empty : (Guid)record["sEncryptionKey"];
                    byte[] aBSsnEncrypted = record["aBSsnEncrypted"] is DBNull ? null : (byte[])record["aBSsnEncrypted"];

                    EncryptionKeyIdentifier? encKey = EncryptionKeyIdentifier.Create(sEncryptionKey);

                    string aBSsnDecrypted = encKey.HasValue ? LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(encKey.Value, aBSsnEncrypted) : string.Empty;

                    if (sEncryptionMigrationVersion != (int)EncryptionMigrationVersion.Unmigrated)
                    {
                        aBSsn = aBSsnDecrypted;
                    }

                    DateTime modifiedD = (DateTime)record["LastModifiedD"];
                    if (latestModifiedDate.CompareTo(modifiedD) < 0)
                        latestModifiedDate = modifiedD;

                    XmlElement el = doc.CreateElement("loan");
                    el.SetAttribute("name", sLNm);
                    el.SetAttribute("old_name", sOldLNm);
                    el.SetAttribute("valid", isValid.ToString());
                    el.SetAttribute("aBNm", aBNm);
                    el.SetAttribute("aBSsn", aBSsn);
                    el.SetAttribute("sSpAddr", sSpAddr);
                    el.SetAttribute("sLRefNm", sLRefNm);
                    el.SetAttribute("LastModifiedD", modifiedD.ToString("MM/dd/yyyy HH:mm:ss"));
                    el.SetAttribute("sStatusT", sStatusT);

                    elementList.Add(new KeyValuePair<Guid, XmlElement>(sLId, el));

                    if (isValid && useBatchWorkflowReadChecker)
                    {
                        workflowCheckLoans.Add(sLId);
                    }
                }                

                int count = 0;

                HashSet<Guid> loansWithReadAccess = null;

                if (useBatchWorkflowReadChecker)
                {
                    LoanReadPermissionResolver r = new LoanReadPermissionResolver(brokerId);
                    loansWithReadAccess = r.GetReadableLoans(principal, workflowCheckLoans, includeTemplates: false);
                }

                foreach (var o in elementList)
                {
                    Guid sLId = o.Key;
                    bool isValid = o.Value.GetAttribute("valid").Equals("True", StringComparison.OrdinalIgnoreCase);

                    if (useBatchWorkflowReadChecker)
                    {
                        if (isValid && !loansWithReadAccess.Contains(sLId))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        // 10/3/2013 dd - Always include invalid loan (delete loan) to the modification list.
                        if (isValid && !AuthServiceHelper.IsOperationAuthorized(sLId, WorkflowOperations.ReadLoan))
                        {
                            continue;
                        }
                    }

                    rootElement.AppendChild(o.Value);
                    count++;
                }

                rootElement.SetAttribute("last_modified_date", latestModifiedDate.ToString("MM/dd/yyyy HH:mm:ss"));
                debugString.AppendLine("Total Modified Loans=" + count);

                return doc.InnerXml;
            }
            finally
            {
                sw.Stop();
                debugString.AppendLine("Executed in " + sw.ElapsedMilliseconds + "ms.");
                Tools.LogInfo(debugString.ToString());
            }
        }

        [WebMethod(Description = useEDocs + "Returns an XML string containing a list of recently modified eDocs. " +
        "This method is used if the client wants to poll the LO/PML system on a fixed interval (try to keep this interval big) " +
        "for recently modified files so that they can be imported into a 3rd-party LOS. " +
        "Contact support for valid appCode. " +
        "THIS METHOD ONLY RETURN EDOCS MODIFIED SINCE THE LAST MODIFIED DATE.")]
        public string ListModifiedEDocsByAppCode(string sTicket, string appCode, string modifiedDate)
        {
            return edocsService.ListModifiedEDocsByAppCode(sTicket, appCode, modifiedDate);
        }

        [WebMethod(Description = "[OBSOLETE]. Use ClearModifiedLoansListByAppCode")]
        public void ClearModifiedLoansList(string sTicket, DateTime lastModifiedDate)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            Tools.LogInfo("Loan.asmx::ClearModifiedLoansList  Principal=" + principal.LoginNm + " - Last Modified Date=" + lastModifiedDate);

            Guid appCode = GetDefaultAppCode(principal);

            ClearModifiedLoansListImpl(appCode, principal, lastModifiedDate);

        }

        [WebMethod(Description = "Clear the list of recently modified loans with a Modified Date older than the [lastModifiedDate]")]
        public void ClearModifiedLoansListByAppCode(string sTicket, DateTime lastModifiedDate, string appCode)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            Tools.LogInfo("Loan.asmx::ClearModifiedLoansListByAppCode  Principal=[" + principal.LoginNm + "] - Last Modified Date=" + lastModifiedDate + ", AppCode=[" + appCode + "]");

            ClearModifiedLoansListImpl(new Guid(appCode), principal, lastModifiedDate);
        }

        private void ClearModifiedLoansListImpl(Guid appCode, AbstractUserPrincipal principal, DateTime lastModifiedDate)
        {
            // 3/16/2006 dd - Need to round up to 1 second to delete the last entry.
            lastModifiedDate = lastModifiedDate.AddSeconds(1);

            SqlParameter[] parameters = { 
                                                new SqlParameter("@AppCode", appCode),
                                                new SqlParameter("@LastModifiedD", lastModifiedDate),
                                                new SqlParameter("@BrokerId", principal.BrokerId)
                                            };
            StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "ClearIntegrationModifiedFilesByBrokerId", 5, parameters);

        }

        [WebMethod(Description = "[OBSOLETE] Use ClearModifiedLoanByNameByAppCode.")]
        public void ClearModifiedLoanByName(string sTicket, string loanName)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, loanName);
            CallVolume.IncreaseAndCheck(principal);
            Tools.LogInfo("Loan.asmx::ClearModifiedLoanByName  Principal=" + principal.LoginNm + ", loanName=" + loanName);

            Guid appCode = GetDefaultAppCode(principal);

            ClearModifiedLoanByNameImpl(appCode, principal, loanName);
        }

        [WebMethod(Description = "Remove a particular loan name [loanName] from the list of recently modified loans.")]
        public void ClearModifiedLoanByNameByAppCode(string sTicket, string loanName, string appCode)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, loanName);
            CallVolume.IncreaseAndCheck(principal);
            Tools.LogInfo("Loan.asmx::ClearModifiedLoanByNameByAppCode Principal=[" + principal.LoginNm + "], loanName=[" + loanName + "], appCode=[" + appCode + "]");

            ClearModifiedLoanByNameImpl(new Guid(appCode), principal, loanName);
        }

        private void ClearModifiedLoanByNameImpl(Guid appCode, AbstractUserPrincipal principal, string loanName)
        {
            var loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, loanName);
            if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            SqlParameter[] parameters = {
                                                new SqlParameter("@AppCode", appCode),
                                                new SqlParameter("@BrokerId", principal.BrokerId),
                                                new SqlParameter("@sLNm", loanName)
                                            };
            StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "ClearIntegrationModifiedFileByName", 5, parameters);
        }

        /// <summary>
        /// OPM 171897 - For all the ModifiedLoans api, we are now REQUIRE an AppCode id provide from us. To allow current lenders to continue using the current API,
        /// we will automatically populate the default app code. However once we contact lender and told them to use new API, they will no longer be able to go back.
        /// 
        /// Hopefully in 1 years this method is no longer necessary. // 6/10/2014 dd - 
        /// </summary>
        /// <param name="principal">The user principal.</param>
        /// <returns>The default app code associated with the user.</returns>
        private Guid GetDefaultAppCode(AbstractUserPrincipal principal)
        {
            Guid[] allowableBrokerIdList = {
                                               new Guid("E9F006C9-CB83-4531-8925-08FCCF4ADD63"), // PML0223	First Technology Federal Credit Union

                                               // 6/16/2014 dd - Will remove these 3 in next release because it use SendDataToDBClient executable.
                                               new Guid("30DFD692-3393-46DA-B4C0-1A382E78E10E"), // PML0214	Wisconsin Housing and Economic Development Authority
                                               new Guid("855B4AC6-00CF-4E47-8B43-2076C5C0D1DC"), // PML0229	JMAC Lending, Inc.
                                               new Guid("B3D89260-B97D-4E9D-A11D-7E560F4A6C93"), // PML0247	Fidelity Bank d/b/a Fidelity Bank Mortgage

                                               new Guid("B94B7017-9E70-4DE7-A316-B5CD7D1E34BB"), // PML0177	THE LENDING COMPANY, INC.

                                                // These lenders are restrict by user.
                                               //new Guid("C943629F-CB38-4D3B-890E-D4B01E8A38A3"), // PML0184	Montage Mortgage, LLC
                                                //new Guid("F85912A1-F167-4D2B-A9D6-39944BF119AB"), // PMLTEST8	SHORELINE FUNDING GROUP 8
                                               //new Guid("C0CCA87A-4D51-4DAE-8572-418BB0C7B88A"), // PML0165	SI MORTGAGE COMPANY
                                               //new Guid("984BB55E-489B-4DB0-8837-6A9E83971805"), // PML0127	FFC MORTGAGE CORP
                                               //new Guid("B987FF40-139A-4474-B528-A796D75E1130"), // PML0159	Venta Financial Group, Inc.
                                               //new Guid("C58F41FF-9302-4240-94E6-DB578CBB9348"), // PML0168	ATLANTIC PACIFIC MORTGAGE CORPORATION
                                               //new Guid("73ECFD86-BC26-4041-B6D9-DEF7245D5C5E"), // PML0243	THE LENDING PARTNERS, LLC

                                           };

            // 6/16/2014 dd - Explicit list of allowable users from OPM 182300.
            HashSet<string> allowableUserSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
            {
                // PML0127	FFC MORTGAGE CORP
                "srobinson", 

                // PML0159	Venta Financial Group, Inc.
                "bntouch",
                "motivity",
                "ron@defrates.com",

                // PML0165 SI MORTGAGE COMPANY
                "sibackup",

                // PML0168	ATLANTIC PACIFIC MORTGAGE CORPORATION
                "AHumphries",

                // PML0243	THE LENDING PARTNERS, LLC
                "LQB_PML0243",

                // PMLTEST8	SHORELINE FUNDING GROUP 8
                "TLCTEST8",

                // PML0184 Montage Mortgage, LLC
                "PML0184_LM"
            };

            if (allowableUserSet.Contains(principal.LoginNm) == false)
            {

                if (allowableBrokerIdList.Contains(principal.BrokerId) == false)
                {
                    throw new CBaseException("Default App Code is not allow. Contact support for valid app code.",
                        "Default App Code is not allow. Contact support for valid app code.");
                }
            }

            Guid? appCode = GetAppCodeForBrokerId(principal.BrokerId);
            if (appCode != null) return appCode.Value;

            throw new CBaseException("Default App Code is not found.", "Default App Code is not found.");
        }

        public static Guid? GetAppCodeForBrokerId(Guid brokerId)
        {
            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

            string sql = "SELECT TOP 1 AppCode FROM BROKER_WEBSERVICE_APP_CODE WHERE BrokerId=@BrokerId AND Enabled=1";

            Guid? appCode = null;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    appCode = (Guid)reader["AppCode"];
                }
            };

            DBSelectUtility.ProcessDBData(brokerId, sql, null, parameters, readHandler);
            return appCode;
        }

        [WebMethod]
        public string ListMyActiveTaskList(string sTicket)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            var list = TaskUtilities.GetTasksInPipeline(principal, principal.UserId, E_TaskStatus.Active, "TaskDueDate", SortOrder.Ascending);

            XDocument xdoc = new XDocument();
            XElement xroot = new XElement("list");
            xdoc.Add(xroot);
            foreach (var item in list)
            {
                XElement xe = new XElement("item",
                    new XAttribute("sLNm", item.LoanNumCached),
                new XAttribute("TaskDueDate", item.TaskDueDate.HasValue ? item.TaskDueDate.Value.ToString("MM/dd/yyy") : ""),
                new XAttribute("TaskSubject", item.TaskSubject),
                new XAttribute("TaskId", item.TaskId),
                new XAttribute("aBLastNm", item.BorrowerLastNmCached));

                xroot.Add(xe);
            }
            return xdoc.ToString();
        }

        [WebMethod]
        public string RetrieveTaskById(string sTicket, string taskId)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            try
            {
                XDocument xdoc = new XDocument();
                XElement taskElement = new XElement("task");
                xdoc.Add(taskElement);

                var task = Task.Retrieve(principal.BrokerId, taskId);

                taskElement.Add(new XAttribute("TaskDueDate", task.TaskDueDate.HasValue ? task.TaskDueDate.Value.ToString("MM/dd/yyyy") : ""),
                    new XAttribute("TaskSubject", task.TaskSubject),
                    new XAttribute("TaskStatus", task.TaskStatus_rep),
                    new XAttribute("TaskAssignedUserId", task.TaskAssignedUserId.ToString()),
                    new XAttribute("TaskAssignedUserName", task.AssignedUserFullName),
                    new XAttribute("UserPermissionLevel", task.UserPermissionLevel.GetFriendlyDescription())
                    );


                XElement historyElement = new XElement("histories");
                taskElement.Add(historyElement);
                foreach (var historyItem in task.GetTaskHistoryItems())
                {
                    historyElement.Add(new XElement("history",
                        new XAttribute("Action", historyItem.Action),
                        new XAttribute("ActionByUser", historyItem.ActionByUser),
                        new XAttribute("Date", historyItem.Date),
                        new XAttribute("Description", historyItem.Description)));
                }
                return xdoc.ToString();
            }
            catch (TaskNotFoundException)
            {
                throw new Exception("Task " + taskId + " is not found.");
            }
            catch (TaskPermissionException)
            {
                throw new Exception("Task access denied.");
            }
        }

        [WebMethod]
        public string RetrieveUsersAssignedToLoan(string sTicket, string sLNm)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);
            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            var loanAssignments = new LoanAssignmentContactTable(principal.BrokerId, sLId);

            XDocument xdoc = new XDocument();
            XElement rootElement = new XElement("list");
            xdoc.Add(rootElement);
            foreach (EmployeeLoanAssignment assignment in loanAssignments.Items)
            {
                rootElement.Add(new XElement("user",
                    new XAttribute("name", assignment.FullName),
                    new XAttribute("role", assignment.RoleModifiableDesc),
                    new XAttribute("userid", assignment.UserId)));
            }
            return xdoc.ToString();

        }

        [WebMethod]
        public string TaskEdit(string sTicket, string taskId, string subject, string comment, string dueDate, string assignedTo)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            ServiceResult result = new ServiceResult();

            try
            {
                var task = Task.Retrieve(principal.BrokerId, taskId);
                task.TaskSubject = subject;
                task.Comments = comment;
                DateTime? dt = null;

                if (string.IsNullOrEmpty(dueDate) == false)
                {
                    DateTime v;
                    if (DateTime.TryParse(dueDate, out v) == true)
                    {
                        dt = v;
                    }
                }
                task.TaskDueDate = dt;
                task.TaskAssignedUserId = new Guid(assignedTo);
                task.Save(true);
            }
            catch (CBaseException exc)
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError(exc.UserMessage);

            }
            catch (Exception exc)
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError(exc.Message);
            }
            return result.ToResponse();
        }

        [WebMethod]
        public string TaskResolve(string sTicket, string taskId, string subject, string comment, string dueDate, string assignedTo)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            ServiceResult result = new ServiceResult();

            try
            {
                var task = Task.Retrieve(principal.BrokerId, taskId);
                task.TaskSubject = subject;
                task.Comments = comment;
                DateTime? dt = null;

                if (string.IsNullOrEmpty(dueDate) == false)
                {
                    DateTime v;
                    if (DateTime.TryParse(dueDate, out v) == true)
                    {
                        dt = v;
                    }
                }
                task.TaskDueDate = dt;
                task.TaskAssignedUserId = new Guid(assignedTo);
                task.Resolve();
            }
            catch (CBaseException exc)
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError(exc.UserMessage);

            }

            catch (Exception exc)
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError(exc.Message);
            }
            return result.ToResponse();

        }
        #region Component Designer generated code

        //Required by the Web Services Designer 
        private IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion

        /// <summary>
        /// Save the loan data to LendingQB. If the loanId is empty 
        /// (i.e., 00000000-000...etc) or identifies a loan that is not in
        /// the system, a new loan will be created. This function returns
        /// the loanId of the saved (or created) loan.
        /// </summary>
        [WebMethod(Description = "Do not use this method")]
        public Guid SaveMismo23(string sTicket, Guid loanId, string sXmlData)
        {
            // 6/20/2007 dd - LON still use it.
            //            Tools.LogBug("OBSOLETE Loan.asmx::LoadMismo23ByLoanName. sTicket=" + sTicket);
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            try
            {
                // todo: I wonder what kind of security/authentication is done here
                // to prevent broker A from saving over broker B's loan.
                // load loan data

                // 08/08/03-Binh-Added to check for invalid loan Id. If the loan is 
                // invalid or does not exists, clear out the Guid so that a new loan 
                // can be generated properly.
                loanId = ClearLoanIdIfInvalid(loanId);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sXmlData);

                LendersOffice.Conversions.Mismo23.LoanMismo23Importer importer = new LendersOffice.Conversions.Mismo23.LoanMismo23Importer();
                CPageData dataLoan = importer.Import(loanId, doc, principal, E_IntegrationT.NHC);

                loanId = dataLoan.sLId;

            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking(e);
                throw;
            }
            return loanId;
        }

        public static Guid ClearLoanIdIfInvalid(Guid loanId)
        {
            if (loanId != Guid.Empty)
            {
                string sSQL = @"SELECT lc.sLId, lc.IsValid FROM Loan_File_Cache lc WITH(nolock) JOIN BROKER b ON lc.sBrokerId = b.BrokerId WHERE sLId = @id AND b.status = 1";

                bool found = false;
                Action<IDataReader> readHandler = delegate (IDataReader sdr)
                {
                    if (sdr.Read())
                    {
                        found = true;
                        if (0 == Convert.ToInt32(sdr["IsValid"]))
                        {
                            loanId = Guid.Empty;
                        }
                    }
                };

                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    var listParams = new SqlParameter[] { new SqlParameter("@id", loanId) };
                    DBSelectUtility.ProcessDBData(connInfo, sSQL, null, listParams, readHandler);
                    if (found) break;
                }

                if (!found)
                {
                    loanId = Guid.Empty;
                }
            }

            return loanId;
        }

        /// <summary>
        /// Load the loan identified by the sLNm in Mismo 2.3.1 Format. The 
        /// sXmlQuery parameter provides a hint as to what data is "required"
        /// by the caller. LoadMismo23 may return more data than asked, but
        /// it will never return less data than asked unless that data isn't
        /// available.
        /// </summary>
        [WebMethod(Description = "Load the loan identified by the sLNm in Mismo 2.3.1 Format. [sTicket] Ticket returned by AuthService.asmx. [sLNm] The LendingQB Loan Name. [sXmlQuery] The Xml defines the list of fields to be included in the response. Refer to lo_xmlquery_blank.xml for an example.")]
        public string LoadMismo23ByLoanName(string sTicket, string sLNm, string sXmlQuery)
        {
            // 11/8/2006 dd - Still in use
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            Tools.LogBug("Using OBSOLETE Webservice method  Loan.asmx::LoadMismo23ByLoanName. LoginName=" + principal.LoginNm);
            Guid loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (Guid.Empty != loanId)
            {
                AuthServiceHelper.CheckLoanAuthorization(loanId, WorkflowOperations.ReadLoan);
                return LoadMismo23(loanId, sXmlQuery, principal);
            }
            else
            {
                return "";
            }
        }

        private string LoadMismo23(Guid loanId, string sXmlQuery, AbstractUserPrincipal principal)
        {
            try
            {
                if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
                {
                    throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                }

                // todo: I wonder what kind of authentication is done here to
                // prevent broker A from loading broker B's loan.
                // load loan data

                // create the xmlReader & writer
                XmlTextReader xmlReader = new XmlTextReader(new System.IO.StringReader(sXmlQuery));
                using (System.IO.MemoryStream memStream = new System.IO.MemoryStream())
                {
                    XmlTextWriter xmlWriter = new XmlTextWriter(memStream, System.Text.Encoding.ASCII);

                    // convert to MISMO format
                    LendersOffice.Conversions.Mismo23.LoanMismo23Exporter exporter = new LendersOffice.Conversions.Mismo23.LoanMismo23Exporter(loanId);
                    exporter.Export(xmlReader, xmlWriter);

                    xmlWriter.Flush();

                    // return requested data
                    return System.Text.ASCIIEncoding.ASCII.GetString(memStream.GetBuffer(), 0, (int)memStream.Position);
                }
            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking(e);
                throw;
            }
        }

        [WebMethod(Description = "Retrieve a PDF document for a particular loan file. [Result] - The PDF content in base64 format.")]
        public string GetPdfByLoanName(string sTicket, string sLNm, string sPdfName)
        {
            #region Validate user input
            string errorMessage = string.Empty;
            if (string.IsNullOrEmpty(sTicket))
            {
                errorMessage += "[sTicket] cannot be empty. ";
            }
            if (string.IsNullOrEmpty(sLNm))
            {
                errorMessage += "[sLNm] cannot be empty. ";
            }
            if (string.IsNullOrEmpty(sPdfName))
            {
                errorMessage += "[sPdfName] cannot be empty. ";
            }
            if (errorMessage != string.Empty)
            {
                throw new CBaseException(errorMessage, "Loan.asmx::GetPdfByLoanName::" + errorMessage);
            }
            #endregion

            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);
            Guid loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (Guid.Empty != loanId)
            {
                AuthServiceHelper.CheckLoanAuthorization(loanId, WorkflowOperations.ReadLoan);

                if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
                {
                    throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                }

                return GetPdf(loanId, sPdfName);
            }
            else
            {
                throw new LoanNotFoundException(loanId);
            }
        }

        /// <summary>
        /// Return PDF file in Base64 Encode. Return "" if sPdfName is invalid or no pdf generated.
        /// </summary>
        private string GetPdf(Guid sLId, string sPdfName)
        {
            string ret = "";
            try
            {
                AbstractUserPrincipal principal = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;

                NameValueCollection arguments = new NameValueCollection();
                arguments["loanid"] = sLId.ToString();
                arguments["brokerid"] = principal.BrokerId.ToString();

                var repository = new LendersOffice.Pdf.PdfGeneratorRepository();
                var pdf = repository.CreatePdfInstance(sPdfName);

                if (null == pdf) //OPM 171220: Attempt to retrieve Custom PDF prior to returning null response
                {
                    var cPDFFormList = LendersOffice.PdfForm.PdfForm.RetrieveCustomFormsOfCurrentBroker();
                    foreach (LendersOffice.PdfForm.PdfForm cPDF in cPDFFormList)
                    {
                        if (cPDF.Description.Equals(sPdfName, StringComparison.OrdinalIgnoreCase))
                        {
                            arguments["custompdfid"] = cPDF.FormId.ToString();
                            pdf = (LendersOffice.PdfGenerator.IPDFGenerator)new LendersOffice.Pdf.CCustomPDF();
                            break;
                        }
                    }
                }
                if (null != pdf)
                {
                    ((LendersOffice.Pdf.IPDFPrintItem)pdf).Arguments = arguments;
                    MemoryStream stream = new MemoryStream();
                    pdf.GeneratePDF(stream, "", "");
                    byte[] buffer = stream.ToArray(); // pdf.GeneratePDF("", "");
                    if (null != buffer)
                        ret = Convert.ToBase64String(buffer);
                }
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
            }
            return ret;
        }


        [WebMethod]
        public Guid UploadLONXml(string sTicket, Guid loanId, string sXmlData)
        {
            // 11/8/2006 dd - Still in use.
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            try
            {
                if (Guid.Empty == loanId)
                {
                    return loanId; // Only accept update from existing loan file.
                }

                if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
                {
                    throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                }

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(sXmlData);

                LendersOffice.Conversions.LON.LoanLONXmlReader reader = new LendersOffice.Conversions.LON.LoanLONXmlReader();
                reader.Read(loanId, doc);
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                throw;
            }

            return loanId;
        }

        private string SearchV1(XmlElement loanElement, AbstractUserPrincipal principal)
        {
            LendingQBSearchFilter searchFilter = new LendingQBSearchFilter();

            if (string.IsNullOrEmpty(loanElement.GetAttribute("sLNm")) == false)
            {
                searchFilter.sLNm = loanElement.GetAttribute("sLNm");
            }
            if (string.IsNullOrEmpty(loanElement.GetAttribute("aBLastNm")) == false)
            {
                searchFilter.aBLastNm = loanElement.GetAttribute("aBLastNm");
            }
            if (string.IsNullOrEmpty(loanElement.GetAttribute("aBFirstNm")) == false)
            {
                searchFilter.aBFirstNm = loanElement.GetAttribute("aBFirstNm");
            }
            if (string.IsNullOrEmpty(loanElement.GetAttribute("sSpAddr")) == false)
            {
                searchFilter.sSpAddr = loanElement.GetAttribute("sSpAddr");
            }
            if (string.IsNullOrEmpty(loanElement.GetAttribute("sStatusT")) == false)
            {
                int sStatusT = -1;
                if (int.TryParse(loanElement.GetAttribute("sStatusT"), out sStatusT) == false)
                {
                    sStatusT = -1;
                }
                searchFilter.sStatusT = sStatusT;
            }
            else
            {
                searchFilter.sStatusT = -1;
            }
            if (string.IsNullOrEmpty(loanElement.GetAttribute("sLT")) == false)
            {
                int sLT = -1;
                if (int.TryParse(loanElement.GetAttribute("sLT"), out sLT) == false)
                {
                    sLT = -1;
                }
                searchFilter.sLT = sLT;
            }
            else
            {
                searchFilter.sLT = -1;
            }

            int totalNumberOfLoans;
            DataSet ds = LendingQBSearch.Search(principal, searchFilter, "aBLastNm", 0, 50, out totalNumberOfLoans);

            XmlDocument resultXmlDocument = new XmlDocument();
            XmlElement root = resultXmlDocument.CreateElement("result");
            resultXmlDocument.AppendChild(root);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                XmlElement item = resultXmlDocument.CreateElement("loan");
                root.AppendChild(item);
                item.SetAttribute("sLNm", row.GetSafeValue("sLNm", string.Empty));
                item.SetAttribute("aBFirstNm", row.GetSafeValue("aBFirstNm", string.Empty));
                item.SetAttribute("aBLastNm", row.GetSafeValue("aBLastNm", string.Empty));
                item.SetAttribute("sSpAddr", row.GetSafeValue("sSpAddr", string.Empty));
                item.SetAttribute("sLAmtCalc", row.GetSafeValue("sLAmtCalc", 0M).ToString());
            }
            return resultXmlDocument.OuterXml;
        }
        
        private string SearchV2(XmlElement loanElement, AbstractUserPrincipal principal)
        {
            SearchV2ServiceResult result = new SearchV2ServiceResult();
            LoanCriteria criteria = new LoanCriteria();

            foreach (XmlElement loanField in loanElement.SelectNodes("field"))
            {
                string id = loanField.GetAttribute("id");

                if (id == null)
                {
                    result.AppendWarning("Invalid format: Missing ID attribute.");
                    continue;
                }

                switch (id)
                {
                    case "sSpAddr":
                        criteria.PropertyAddress = loanField.InnerText;
                        break;
                    case "sSpCity":
                        criteria.PropertyCity = loanField.InnerText;
                        break;
                    case "sSpState":
                        criteria.PropertyState = loanField.InnerText;
                        break;
                    case "sSpZip":
                        criteria.PropertyZip = loanField.InnerText;
                        break;
                    case "sStatusD":
                        var date = loanField.InnerText.ToNullable<DateTime>(DateTime.TryParse);
                        if (date == null)
                        {
                            result.AppendWarning($"Invalid date {loanField.InnerText} for sStatusD.");
                            continue;
                        }

                        criteria.StatusModifiedByDate = date;
                        break;
                    default:
                        result.AppendWarning("Invalid format: Invalid field id " + id + ".");
                        break;
                }
            }

            foreach (XmlElement applicant in loanElement.SelectNodes("applicant"))
            {
                ApplicantCriteria appCriteria = new ApplicantCriteria();

                foreach (XmlElement appField in applicant.SelectNodes("field"))
                {
                    string id = appField.GetAttribute("id");

                    if (id == null)
                    {
                        result.AppendWarning("Invalid format: Missing ID attribute.");
                        continue;
                    }

                    switch (id)
                    {
                        case "aBFirstNm":
                            appCriteria.FirstName = appField.InnerText;
                            break;
                        case "aBLastNm":
                            appCriteria.LastName = appField.InnerText;
                            break;
                        case "aBSsn":
                            appCriteria.SSN = appField.InnerText;
                            break;
                        case "aBPhone":
                            appCriteria.Phone = appField.InnerText;
                            break;
                        case "aBEmail":
                            appCriteria.Email = appField.InnerText;
                            break;
                        case "aBSsnLastFour":
                            appCriteria.Last4SSN = appField.InnerText;
                            break;
                        default:
                            result.AppendWarning("Invalid format: Invalid field id " + id + ".");
                            break;
                    }
                }

                if (appCriteria.HasData())
                { 
                    criteria.ApplicantCriterias.Add(appCriteria);
                }
            }

            LoanSearch search = new LoanSearch(principal.BrokerId);

            if (criteria.IsLoanDataSpecified() || criteria.ApplicantCriterias.Count > 0)
            {
                try
                {
                    IEnumerable<LoanDetails> loans = search.Search(criteria);
                    result.Status = ServiceResultStatus.OK;
                    LoanReadPermissionResolver resolver = new LoanReadPermissionResolver(principal.BrokerId);

                    var ids = from p in loans
                              select p.sLId;

                    HashSet<Guid> loanIdsWithRead = resolver.GetReadableLoans(principal, ids);
                    result.SetResults(loans.Where(p => loanIdsWithRead.Contains(p.sLId)));
                }

                catch (CBaseException e)
                {
                    var errorResult = new ErrorServiceResult(e, true);
                    return errorResult.ToResponse();
                }

            }
            else
            {
                result.AppendWarning("No filtering data specified.");
                result.Status = ServiceResultStatus.OKWithWarning;
            }

            return result.ToResponse();
        }

        [WebMethod]
        public string Search(string sTicket, string sSearchFilterXml)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            Tools.LogInfo("Loan.asmx::Search [" + principal.LoginNm + "]: " + sSearchFilterXml);

            XmlDocument xmlDocument = Tools.CreateXmlDoc(sSearchFilterXml);

            XmlElement loanElement = (XmlElement)xmlDocument.SelectSingleNode("//filter");

            if (loanElement != null)
            {
                return SearchV1(loanElement, principal);
            }

            loanElement = (XmlElement)xmlDocument.SelectSingleNode("//loan");

            if (loanElement != null)
            {
                return this.SearchV2(loanElement, principal);
            }

            ServiceResult result = new ErrorServiceResult("sSearchFilterXml is Invalid");
            return result.ToResponse();
        }

        [WebMethod]
        public string RunInternalPricing(string sTicket, string sLNm, string sXmlData)
        {
            #region Validate User Inputs
            string errMsg = string.Empty;
            if (string.IsNullOrEmpty(sTicket))
            {
                errMsg += "[sTicket] cannot be empty.";
            }
            if (string.IsNullOrEmpty(sXmlData))
            {
                errMsg += "[sXmlData] cannot be empty.";
            }
            if (errMsg != string.Empty)
            {
                throw new CBaseException(errMsg, "Loan.asmx::RunQuickPricer " + errMsg);
            }
            #endregion
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);
            Tools.LogInfo("Loan.asmx::RunInternalPricing [" + principal.LoginNm + "]: sLNm=[" + sLNm + "] " + sXmlData);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                throw new CBaseException("Invalid loan number " + sLNm, "Invalid loan number " + sLNm);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            XmlDocument xmlDocument = Tools.CreateXmlDoc(sXmlData);
            XmlElement loanElement = (XmlElement)xmlDocument.SelectSingleNode("//loan");

            CPageData dataLoan = new CQuickPricerLoanData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            E_sPricingModeT sPricingModeT = (E_sPricingModeT)Enum.Parse(typeof(E_sPricingModeT), loanElement.GetAttribute("sPricingModeT"));

            bool b = false;
            bool? isBestPriceEnabled = null;

            if (HasBoolAttr(loanElement, "sProdFilterDisplayrateMerge", out b))
            {
                dataLoan.sProdFilterDisplayrateMerge = b;
                isBestPriceEnabled = b;
            }
            if (HasBoolAttr(loanElement, "sProdFilterDisplayUsingCurrentNoteRate", out b))
            {
                dataLoan.sProdFilterDisplayUsingCurrentNoteRate = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterRestrictResultToCurrentRegistered", out b))
            {
                dataLoan.sProdFilterRestrictResultToCurrentRegistered = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterMatchCurrentTerm", out b))
            {
                dataLoan.sProdFilterMatchCurrentTerm = b;
            }


            if (HasBoolAttr(loanElement, "sProdFilterDue15Yrs", out b))
            {
                dataLoan.sProdFilterDue15Yrs = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterDue20Yrs", out b))
            {
                dataLoan.sProdFilterDue20Yrs = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterDue30Yrs", out b))
            {
                dataLoan.sProdFilterDue30Yrs = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterDueOther", out b))
            {
                dataLoan.sProdFilterDueOther = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterFinMethFixed", out b))
            {
                dataLoan.sProdFilterFinMethFixed = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterFinMeth3YrsArm", out b))
            {
                dataLoan.sProdFilterFinMeth3YrsArm = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterFinMeth5YrsArm", out b))
            {
                dataLoan.sProdFilterFinMeth5YrsArm = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterFinMeth7YrsArm", out b))
            {
                dataLoan.sProdFilterFinMeth7YrsArm = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterFinMeth10YrsArm", out b))
            {
                dataLoan.sProdFilterFinMeth10YrsArm = b;
            }

            if (HasBoolAttr(loanElement, "sProdFilterFinMethOther", out b))
            {
                dataLoan.sProdFilterFinMethOther = b;
            }

            dataLoan.Save();
            return DistributeUnderwritingEngine.RequestInternalPricingResultInXml(principal, sLId, sPricingModeT, isBestPriceEnabled).OuterXml;
        }

        private bool HasBoolAttr(XmlElement el, string attrName, out bool b)
        {
            string s = el.GetAttribute(attrName);
            b = false;
            if (string.IsNullOrEmpty(s))
            {
                return false;
            }
            else
            {
                b = s.Equals("True", StringComparison.OrdinalIgnoreCase);
                return true;
            }
        }

        [WebMethod(Description = "[Deprecated] Please use the RunQuickPricerV1 method in QuickPricer.asmx instead, or upgrade to RunQuickPricerV2")]
        public string RunQuickPricer(string sTicket, string sXmlData)
        {
            return this.quickPricer.RunQuickPricerV1(sTicket, sXmlData);
        }

        [WebMethod(Description = "[Deprecated] Please use the RunQuickPricerV1ForLockDesk method in QuickPricer.asmx instead, or upgrade to RunQuickPricerV2")]
        public string RunQuickPricerForLockDesk(string sTicket, string sXmlData)
        {
            return this.quickPricer.RunQuickPricerV1ForLockDesk(sTicket, sXmlData);
        }

        [WebMethod]
        public string SubmitTotalScorecard(string sTicket, string sLNm)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Loan.asmx::SubmitTotalScorecard( {0} )", sLNm);
            sb.AppendLine();
            ServiceResult result = new ServiceResult();

            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            try
            {

                Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
                sb.AppendLine("Found loan : " + sLId);
                if (sLId == Guid.Empty)
                {
                    result.Status = ServiceResultStatus.Error;
                    result.AppendError("Invalid loan number: " + sLNm);
                }
                else
                {
                    // Step 1 - Need to run the same validation as when we click FHA Total submission in our app
                    CPageData dataLoan = new CFHATotalAuditData(sLId);
                    dataLoan.InitLoad();

                    if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
                    {
                        result.Status = ServiceResultStatus.Error;
                        result.AppendError(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                    }

                    if (dataLoan.sHasTotalScorecardWarning)
                    {
                        result.Status = ServiceResultStatus.Error;
                        foreach (var error in dataLoan.GetTotalScorecardWarningList())
                        {
                            result.AppendError(error);
                        }
                    }

                    // Step 2 - Run Total
                    if (result.Status != ServiceResultStatus.Error)
                    {
                        string serverCacheKey = string.Empty;
                        CreditRiskEvaluation creditRisk = new CreditRiskEvaluation();
                        bool isOk = creditRisk.Evaluate(sLId, E_CalcModeT.PriceMyLoan, E_DataSourcePage.Form_1003, out serverCacheKey);

                        if (isOk == false)
                        {
                            // Step 3 - Check for error
                            result.Status = ServiceResultStatus.Error;

                            string errorList = AutoExpiredTextCache.GetFromCache(serverCacheKey);
                            if (string.IsNullOrEmpty(errorList))
                            {
                                result.AppendError("Unexpected errors");
                            }
                            else
                            {
                                string[] errorCodes = errorList.Split(',');
                                string[] borrNameList = new string[5];


                                for (int nApps = 0; nApps < dataLoan.nApps; nApps++)
                                {
                                    CAppData dataApp = dataLoan.GetAppData(nApps);
                                    if (nApps < borrNameList.Length)
                                    {
                                        if (dataApp.aBIsValidNameSsn)
                                        {
                                            borrNameList[nApps] = dataApp.aBNm;
                                        }
                                        else if (dataApp.aCIsValidNameSsn)
                                        {
                                            borrNameList[nApps] = dataApp.aCNm;
                                        }
                                        else
                                        {
                                            borrNameList[nApps] = string.Empty;
                                        }
                                    }
                                }
                                string fieldType = dataLoan.sLPurposeT == E_sLPurposeT.Purchase ? "sales price" : "appraised value";
                                foreach (var code in errorCodes)
                                {
                                    string error;
                                    if (code.TrimWhitespaceAndBOM() == "475")
                                    {
                                        error = string.Format("Total Closing Cost must be at least $0 and cannot exceed 20% of {0}. Submitted total closing cost was {1}.", fieldType, dataLoan.sTotEstCcNoDiscnt1003_rep);
                                    }
                                    else if (code.TrimWhitespaceAndBOM() == "485")
                                    {
                                        error = string.Format("Borrower Closing Costs must be at least $0 and cannot exceed 20% of {0}. Submitted borrower closing cost was {1}", fieldType, dataLoan.sTotalScoreBorrClosingCosts_rep);
                                    }
                                    else
                                    {
                                        error = string.Format(ErrorCodeHandler.GetMessage(code), borrNameList);
                                    }
                                    result.AppendError(error);
                                }
                            }
                        }
                        else
                        {
                            result.Status = ServiceResultStatus.OK;
                            // av opm 96990 
                            if (principal.BrokerId != new Guid("baf07293-f626-44c6-9750-706199a7c0b6")) //dont import for remn.
                            {
                                TotalCertUtilities.ImportConditionsFromCert(sLId);
                            }

                        }
                    }
                }
            }
            catch (PageDataAccessDenied exc) // OPM 20377
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError(exc.ToString());
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                throw;
            }
            finally
            {
                Tools.LogInfo(sb.ToString() + " " + result.ToResponse());
            }
            return result.ToResponse();
        }

        [WebMethod]
        public string SubmitToLoanProspectorSeamless(string sTicket, string sLNm,
                                                     string lpUserId, string lpPassword,
                                                     string creditMethod, string craProvider,
                                                     string borrowerCreditInfoXml)
        {
            StringBuilder debugMessage = new StringBuilder();
            try
            {
                AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
                CallVolume.IncreaseAndCheck(principal);

                debugMessage.AppendLine("Loan.asmx::SubmitToLoanProspectorSeamless(" + principal.LoginNm + ", sLNm=" + sLNm + ", lpUserId=" + lpUserId + ")");

                Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
                string rawResponse = string.Empty;

                string status = string.Empty;
                string errorMessage = string.Empty;

                if (sLId == Guid.Empty)
                {
                    status = "Error";
                    errorMessage = "Invalid Loan Number";
                }
                else
                {
                    // 7/30/2013 dd - LP credit information & cra are not store in loan file.
                    // Therefore we need to save it in memory. Using the same format as the actual Freddie Export page.
                    AusCreditOrderingInfo creditInfo = new AusCreditOrderingInfo();
                    creditInfo.CraProviderId = craProvider;
                    if (creditMethod.Equals("Merge", StringComparison.OrdinalIgnoreCase))
                    {
                        creditInfo.CreditReportOption = CreditReportType.OrderNew;
                    }
                    else if (creditMethod.Equals("Infile", StringComparison.OrdinalIgnoreCase))
                    {
                        creditInfo.CreditReportOption = CreditReportType.UsePrevious;
                    }

                    // 7/31/2013 dd - BorrowerCreditInfoXml will have this format.
                    // <credit>
                    //    <merged_credit_reference>
                    //        <field aBSsn="" value="referenceid"/>
                    //    </merged_credit_reference>
                    //    <request_reorder_credit>
                    //        <field aBSsn=""/>
                    //    </request_reorder_credit>
                    // </credit>

                    if (string.IsNullOrEmpty(borrowerCreditInfoXml) == false)
                    {
                        XDocument borrowerCreditDoc = XDocument.Parse(borrowerCreditInfoXml);

                        // 7/31/2013 dd - I am just piggy back the CDuSubmissionImportaData class because it contains aBSsn.
                        // I cannot use the smart dependency here because it will throw exception .
                        CPageData dataLoan = new CDuSubmissionImportData(sLId);
                        dataLoan.InitLoad();

                        if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
                        {
                            status = "Error";
                            errorMessage = ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan;
                        }

                        XElement mergedCreditElement = borrowerCreditDoc.Root.Element("merged_credit_reference");
                        XElement requestReorderCreditElement = borrowerCreditDoc.Root.Element("request_reorder_credit");
                        if (null != mergedCreditElement && status != "Error")
                        {
                            List<AusCreditOrderingInfo.AusCreditAppData> appCreditList = new List<AusCreditOrderingInfo.AusCreditAppData>();
                            creditInfo.Applications = appCreditList;
                            foreach (XElement fieldElement in mergedCreditElement.Elements("field"))
                            {
                                XAttribute attr = fieldElement.Attribute("aBSsn");
                                if (attr == null) 
                                {
                                    status = "Error";
                                    errorMessage = "SSN in merged credit reference is required.";
                                    break;
                                }
                                string aBSsn = attr.Value.Replace("-", "");

                                if (!string.IsNullOrEmpty(aBSsn))
                                {
                                    CAppData app = dataLoan.Apps.FirstOrDefault(dataApp => aBSsn == dataApp.aBSsn.Replace("-", string.Empty));
                                    var appCreditData = new AusCreditOrderingInfo.AusCreditAppData(app.aBNm, app.aCNm, null, app.aAppId);

                                    // If the current app appears in the list of requested reorders, add the reorder ID to the object
                                    if (requestReorderCreditElement != null && requestReorderCreditElement.Elements("field").Any(field => field?.Attribute("aBSsn") != null && field.Attribute("aBSsn").Value.Replace("-", "") == app.aBSsn))
                                    {
                                        appCreditData.CraResubmitId = fieldElement.Attribute("value").Value;
                                    }

                                    appCreditList.Add(appCreditData);
                                }
                            }
                        }
                    }

                    if (status != "Error")
                    {
                        LoanSubmitRequest.SaveTemporaryCRAInformation(sLId, creditInfo);

                        MismoServiceOrderResponse response = LoanProspectorServer.SubmitSynchronous(lpUserId, lpPassword, principal, sLId, out rawResponse);
                        if (response.Status.Processing == E_MismoStatusProcessing.Error)
                        {
                            status = "Error";
                            ////errorMessage = LoanProspectorHelper.BuildErrorMessage(response, rawResponse);
                        }
                        else
                        {
                            status = "OK";
                        }
                    }
                }

                XDocument xdoc = new XDocument();
                XElement resultElement = new XElement("result");
                xdoc.Add(resultElement);

                resultElement.SetAttributeValue("status", status);

                if (status == "Error")
                {
                    XElement errorElement = new XElement("error");
                    errorElement.Value = errorMessage;
                    resultElement.Add(errorElement);
                }

                string result = xdoc.ToString(SaveOptions.None);
                return result;
            }
            catch (AccessDenied)
            {
                XDocument xdoc = new XDocument();
                XElement resultElement = new XElement("result");
                xdoc.Add(resultElement);

                resultElement.SetAttributeValue("status", "Error");

                XElement errorElement = new XElement("error");
                errorElement.Value = "Access Denied. Invalid LP UserId / Password.";
                resultElement.Add(errorElement);

                return xdoc.ToString(SaveOptions.None);
            }
            finally
            {
                Tools.LogInfo(debugMessage.ToString());
            }
        }

        [WebMethod]
        public string SubmitToFannieMaeEarlyCheck(string sTicket, string sLNm, string duUserName, string duPassword, string institutionId)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            ServiceResult result = new ServiceResult();

            if (sLId == Guid.Empty)
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError("Invalid Loan Number.");
            }
            else
            {
                CPageData dataLoan = new CPageData(sLId, new string[] { "sLenderCaseNum" });
                dataLoan.InitLoad();

                if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
                {
                    result.Status = ServiceResultStatus.Error;
                    result.AppendError(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                }
                else
                {
                    FnmaXisEarlyCheckRequest request = new FnmaXisEarlyCheckRequest(sLId);
                    request.FannieMaeMORNETUserID = duUserName;
                    request.FannieMaeMORNETPassword = duPassword;
                    request.LenderInstitutionId = institutionId;
                    request.LenderCaseIdentifier = dataLoan.sLenderCaseNum; // Require for MISMO 2.3 or 1003 format.

                    AbstractFnmaXisResponse response = DUServer.SubmitSynchronous(request);

                    if (response.HasAnyError)
                    {
                        result.Status = ServiceResultStatus.Error;
                        result.AppendError(response.ErrorInHtml);
                    }
                    else
                    {
                        result.Status = ServiceResultStatus.OK;
                        ((FnmaXisEarlyCheckResponse)response).ImportToLoanFile(sLId, PrincipalFactory.CurrentPrincipal); // Save to FileDB on successful response.
                    }
                }
            }

            return result.ToResponse();
        }


        [WebMethod(Description = "Performs a seamless submission to Desktop Underwriter. Imports the DU credit report.")]
        public string SubmitToDesktopUnderwriterSeamless(string sTicket, string sLNm, string duUserName, string duPassword, string institutionId, string craProvider, string craLoginName, string craPassword, bool isCopyLiabilities)
        {
            return this.SubmitToDesktopUnderwriterSeamless_OptionalCredit(sTicket, sLNm, duUserName, duPassword, institutionId, craProvider, craLoginName, craPassword, isCopyLiabilities, importCreditReport: true);
        }

        [WebMethod(Description = "Performs a seamless submission to Desktop Underwriter. Provides an option to not import the credit report.")]
        public string SubmitToDesktopUnderwriterSeamless_OptionalCredit(string sTicket, string sLNm, string duUserName, string duPassword, string institutionId, string craProvider, string craLoginName, string craPassword, bool isCopyLiabilities, bool importCreditReport)
        {
            IncreaseAndCheckCallVolume(sTicket, sLNm);
            return this.SubmitToDesktopUnderwriterSeamlessImpl(sTicket, sLNm, duUserName, duPassword, institutionId, craProvider, craLoginName, craPassword, isCopyLiabilities, string.Empty, importCreditReport);
        }

        [WebMethod(Description = "Performs a seamless submission to Desktop Underwriter. Credit is re-issued according to the creditXml and imported.")]
        public string SubmitToDesktopUnderwriterSeamlessReissueCredit(string sTicket, string sLNm, string duUserName, string duPassword, string institutionId, string craProvider, string craLoginName, string craPassword, bool isCopyLiabilities, string creditXml)
        {
            return this.SubmitToDesktopUnderwriterSeamlessReissueCredit_OptionalCreditImport(sTicket, sLNm, duUserName, duPassword, institutionId, craProvider, craLoginName, craPassword, isCopyLiabilities, creditXml, importCreditReport: true);
        }

        [WebMethod(Description = "Performs a seamless submission to Desktop Underwriter. Credit is re-issued according to the creditXml.")]
        public string SubmitToDesktopUnderwriterSeamlessReissueCredit_OptionalCreditImport(string sTicket, string sLNm, string duUserName, string duPassword, string institutionId, string craProvider, string craLoginName, string craPassword, bool isCopyLiabilities, string creditXml, bool importCreditReport)
        {
            if (string.IsNullOrEmpty(creditXml) || string.IsNullOrEmpty(creditXml.TrimWhitespaceAndBOM()))
            {
                return WebServiceUtilities.GenerateErrorResponseXml_Old("creditXml may not be blank");
            }

            IncreaseAndCheckCallVolume(sTicket, sLNm);
            return this.SubmitToDesktopUnderwriterSeamlessImpl(sTicket, sLNm, duUserName, duPassword, institutionId, craProvider, craLoginName, craPassword, isCopyLiabilities, creditXml, importCreditReport);
        }

        private string SubmitToDesktopUnderwriterSeamlessImpl(string sTicket, string sLNm, string duUserName, string duPassword, string institutionId, string craProvider, string craLoginName, string craPassword, bool isCopyLiabilities, string creditXml, bool importCreditReport = true)
        {
            StringBuilder debugMessage = new StringBuilder();

            try
            {
                AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);

                Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

                debugMessage.AppendLine("Loan.asmx::SubmitToDesktopUnderwriterSeamlessImpl(" + principal.LoginNm + ", sLNm=" + sLNm + ", duUserName=" + duUserName + ", institutionId=" + institutionId + ", CraProvider=" + craProvider + ", craLoginName=" + craLoginName + ", isCopyLiabilities=" + isCopyLiabilities + ", creditXml=" + creditXml);

                string status = string.Empty;
                string errorMessage = string.Empty;
                string sDuCaseId = string.Empty;
                string html = string.Empty;
                string recommendationDescription = string.Empty;
                if (sLId == Guid.Empty)
                {
                    status = "Error";
                    errorMessage = "Invalid Loan Number";
                }
                else if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
                {
                    status = "Error";
                    errorMessage = ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan;
                }
                else
                {

                    #region Create Request


                    string globallyUniqueIdentifier = string.Empty;
                    FnmaXisDuUnderwriteRequest request = new FnmaXisDuUnderwriteRequest(sLId);
                    request.FannieMaeMORNETUserID = duUserName;
                    request.FannieMaeMORNETPassword = duPassword;
                    request.LenderInstitutionId = institutionId;
                    request.GloballyUniqueIdentifier = globallyUniqueIdentifier;
                    request.CraProviderNumber = craProvider;
                    request.CraLoginName = craLoginName;
                    request.CraPassword = craPassword;
                    request.CopyLiabilitiesIndicator = isCopyLiabilities;

                    CDuSubmissionImportData dataLoan = new CDuSubmissionImportData(sLId);
                    dataLoan.InitLoad();

                    request.UseLegacyFindingsFormat = dataLoan.sLT == E_sLT.FHA || dataLoan.sLT == E_sLT.VA || principal.BrokerDB.UsingLegacyFindingsForSeamlessDU;
                    if (string.IsNullOrEmpty(creditXml) == false)
                    {
                        string duSeamlessCreditInformationError;
                        ConfigureDUSeamlessCreditInformationList(dataLoan, creditXml, request, out duSeamlessCreditInformationError);
                        if (string.IsNullOrEmpty(duSeamlessCreditInformationError) == false)
                        {
                            debugMessage.AppendLine(duSeamlessCreditInformationError);
                            Tools.LogInfo(debugMessage.ToString());
                            return WebServiceUtilities.GenerateErrorResponseXml_Old(duSeamlessCreditInformationError);
                        }
                    }

                    #endregion

                    var submissionResult = DuRequestSubmission.SubmitWithTimeout(principal, sLId, request);

                    if (submissionResult.HasError)
                    {
                        var exception = submissionResult.Error as CBaseException;
                        status = "Error";
                        errorMessage = exception != null ? exception.UserMessage : "Unable to submit to DU.";
                    }
                    else
                    {
                        var response = submissionResult.Value;
                        if (response.HasAnyError)
                        {
                            status = "Error";
                            errorMessage = response.ErrorInHtml;
                        }
                        else
                        {
                            status = "OK";
                            sDuCaseId = response.MornetPlusCasefileIdentifier;
                            html = ((FnmaXisDuUnderwriteResponse)response).FnmaFindingHtmlNew;
                            if (string.IsNullOrEmpty(html))
                            {
                                html = ((FnmaXisDuUnderwriteResponse)response).FnmaFindingHtml;
                            }

                            recommendationDescription = ((FnmaXisDuUnderwriteResponse)response).UnderwritingRecommendationDescription;
                        }
                        response.ImportToLoanFile(
                            sLId,
                            principal,
                            isImportDuFindings: true,
                            isImportCreditReport: importCreditReport,
                            isCopyLiabilitiesFromCreditReport: isCopyLiabilities);
                    }
                }

                XDocument xdoc = new XDocument();
                XElement resultElement = new XElement("result");
                xdoc.Add(resultElement);

                resultElement.SetAttributeValue("status", status);

                if (status == "Error")
                {
                    XElement errorElement = new XElement("error");
                    errorElement.Value = errorMessage;
                    resultElement.Add(errorElement);
                }
                else
                {
                    resultElement.SetAttributeValue("sDuCaseId", sDuCaseId);
                    XElement findingElement = new XElement("FindingsHtml");
                    findingElement.SetAttributeValue("sDuCaseId", sDuCaseId);
                    findingElement.SetAttributeValue("RecommendationDescription", recommendationDescription.TrimWhitespaceAndBOM());
                    findingElement.Value = html;

                    resultElement.Add(findingElement);
                }

                string result = xdoc.ToString(SaveOptions.None);
                debugMessage.AppendLine();
                debugMessage.AppendLine(result);
                return result;
            }
            finally
            {
                Tools.LogInfo(debugMessage.ToString());
            }
        }

        /// <summary>
        /// Configures DU Seamless credit information for proper credit re-issue.
        /// </summary>
        /// <param name="dataLoan">The loan data, prepared for data loading.</param>
        /// <param name="creditXml">The credit xml to be parsed.</param>
        /// <param name="request">The DU seamless request object.</param>
        /// <param name="errorMessage">Error message to be populated to user if credit information cannot be configured.</param>
        private static void ConfigureDUSeamlessCreditInformationList(CDuSubmissionImportData dataLoan, string creditXml, FnmaXisDuUnderwriteRequest request, out string errorMessage)
        {
            errorMessage = null;
            dataLoan.SetFormatTarget(FormatTarget.FannieMaeMornetPlus);

            bool useCreditReportOnFileForAllApplicants; //Instruction to use existing credit information on file for all applicants.
            IEnumerable<string> useExistingCreditForApplicantCollection; //Instruction to use existing credit information on file for specific applicants.
            IEnumerable<XisCreditInformation> creditInformationCollectionFromCreditXml; //XisCreditInformation objects parsed from xml;
            
            ////Parse xml and verify data completeness.
            LOFormatImporter.ParseCreditLOXmlForDUSeamless(creditXml, out useCreditReportOnFileForAllApplicants, out useExistingCreditForApplicantCollection, out creditInformationCollectionFromCreditXml);
            foreach (XisCreditInformation creditInformation in creditInformationCollectionFromCreditXml)
            {
                if (string.IsNullOrEmpty(creditInformation.Ssn))
                {
                    errorMessage = "One or more applicants in creditXml has blank ssn.";
                }
                else if (string.IsNullOrEmpty(creditInformation.CreditReportIdentifier))
                {
                    errorMessage = "One or more applicants in creditXml has blank reportId.";
                }

                if (string.IsNullOrEmpty(errorMessage) == false)
                {
                    return;
                }
            }

            if (useCreditReportOnFileForAllApplicants)
            {
                
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData dataApp = dataLoan.GetAppData(i);
                    XisCreditInformation creditInformation = CreateDUSeamlessCreditInformationFromApp(dataApp, out errorMessage);
                    if (string.IsNullOrEmpty(errorMessage) == false)
                    {
                        return;
                    }
                    else
                    {
                        request.AddCreditInformation(creditInformation);
                    }
                }
            }
            else
            {
                if (dataLoan.nApps != creditInformationCollectionFromCreditXml.Count() + useExistingCreditForApplicantCollection.Count())
                {
                    errorMessage = "Mismatch in the number of applicants between the loan and creditXml.";
                    return;
                }

                List<CAppData> dataAppList = new List<CAppData>();
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData dataApp = dataLoan.GetAppData(i);
                    XisCreditInformation creditInformation = creditInformationCollectionFromCreditXml.FirstOrDefault(c => c.Ssn.Equals(dataApp.aBSsn));
                    if (creditInformation == null)
                    {
                        bool useExistingCreditForApplicant = useExistingCreditForApplicantCollection.Any(ssn => ssn.Equals(dataApp.aBSsn));
                        if (useExistingCreditForApplicant == true)
                        {
                            creditInformation = CreateDUSeamlessCreditInformationFromApp(dataApp, out errorMessage);
                        }
                        else
                        {
                            errorMessage = "SSN on one or more applicants in creditXml cannot be matched against loan.";
                        }
                    }

                    if (string.IsNullOrEmpty(errorMessage) == false)
                    {
                        return;
                    }
                    else
                    {
                        request.AddCreditInformation(creditInformation);
                    }
                }
            }
        }

        /// <summary>
        /// Configures DU Seamless credit information from 1003 application.
        /// </summary>
        /// <param name="dataApp">The 1003 application object.</param>
        /// <param name="errorMessage">Error message to be populated to user if credit information cannot be configured.</param>
        /// <returns>Credit information object populated from 1003 application.</returns>
        private static XisCreditInformation CreateDUSeamlessCreditInformationFromApp(CAppData dataApp, out string errorMessage)
        {
            errorMessage = null;

            string ssn = dataApp.aBSsn;
            string reportId = dataApp.aCreditReportId;
            E_XisCreditInformationRequestType creditRequestType = dataApp.aHasSpouse ? E_XisCreditInformationRequestType.Joint : E_XisCreditInformationRequestType.Individual;
            
            if (string.IsNullOrEmpty(ssn))
            {
                errorMessage = "One or more applications in the loan are missing a primary borrower ssn.";
                return null;
            }
            else if (string.IsNullOrEmpty(reportId))
            {
                errorMessage = "One or more applications in the loan do not have credit on file.";
                return null;
            }
            else
            {
                return new XisCreditInformation(ssn, reportId, creditRequestType);
            }
        }


        [WebMethod]
        public string SubmitToDesktopUnderwriterWithInstituteId(string sTicket, string sLNm, string duUserName, string duPassword, string institutionId)
        {
            IncreaseAndCheckCallVolume(sTicket);
            return SubmitToDesktopUnderwriterWithInstituteIdInternal(sTicket, sLNm, duUserName, duPassword, institutionId);
        }

        private string SubmitToDesktopUnderwriterWithInstituteIdInternal(string sTicket, string sLNm, string duUserName, string duPassword, string institutionId)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);


            if (sLId == Guid.Empty)
            {
                return WebServiceUtilities.GenerateErrorResponseXml("Invalid loan number.");
            }
            else if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                return WebServiceUtilities.GenerateErrorResponseXml(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }
            else
            {
                return SubmitToFnmaImpl(sLId, false, duUserName, duPassword, institutionId);
            }

        }

        [WebMethod]
        public string SubmitToDesktopUnderwriter(string sTicket, string sLNm, string duUserName, string duPassword)
        {
            IncreaseAndCheckCallVolume(sTicket);
            return SubmitToDesktopUnderwriterWithInstituteIdInternal(sTicket, sLNm, duUserName, duPassword, string.Empty /*institutionId*/);
        }


        [WebMethod(Description = "Import loan file data from Freddie Mac into an existing or new loan.")]
        public string ImportFromFreddieMac(string sTicket, string optionsXml)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            LoanLoXmlServiceResult result = new LoanLoXmlServiceResult();

            ParseResult<LoanTransferRequestInput> optionsResult = ParseLoanTransferRequestInputOptionsXml(optionsXml, principal);
            if (!optionsResult.IsSuccessful)
            {
                result.AppendError(optionsResult.ErrorMessage ?? ErrorMessages.Generic);
            }
            else
            {
                LoanTransferRequestInput input = optionsResult.ValueOrDefault;
                LoanTransferRequestOutput output = LoanTransferRequest.RunLoanTransferRequest(input);

                if (!output.Success)
                {
                    foreach (string error in output.Errors)
                    {
                        result.AppendError(error);
                    }
                }
                else
                {
                    CPageData loan = new NotEnforceAccessControlPageData(output.LoanId, new[] { nameof(CPageData.sLNm), nameof(CPageData.sLRefNm) });
                    loan.InitLoad();
                    result.AddLoanElement(LoXmlServiceResult.CreateFieldElement(nameof(CPageData.sLNm), loan.sLNm));
                    result.AddLoanElement(LoXmlServiceResult.CreateFieldElement(nameof(CPageData.sLRefNm), loan.sLRefNm));
                }
            }

            string response = result.ToResponse();
            Tools.LogInfo("ImportFromFreddieMac - Sending Response" + Environment.NewLine + response);
            return response;
        }

        /// <summary>
        /// Parses <param name="optionsXml"/> into an input for the loan transfer request process of making a new loan or amending an existing loan.
        /// </summary>
        private static ParseResult<LoanTransferRequestInput> ParseLoanTransferRequestInputOptionsXml(string optionsXml, AbstractUserPrincipal principal)
        {
            ParseResult<ServiceRequestOptions> requestOptionsParseResult = ServiceRequestOptions.ParseRequestOptions(optionsXml);
            if (!requestOptionsParseResult.IsSuccessful)
            {
                return ParseResult<LoanTransferRequestInput>.Failure(optionsXml, requestOptionsParseResult.ErrorMessage);
            }

            ServiceRequestOptions requestOptions = requestOptionsParseResult.ValueOrDefault;
            Tools.LogInfo("LoanTransferRequestInputOptions:" + Environment.NewLine + requestOptions.GenerateLogLOXml(fieldsToMask: new string[] { "LpaUserPassword", "LpaSellerPassword" }));

            var result = new LoanTransferRequestInput();
            result.Principal = principal;
            result.LpaUserId = requestOptions.Fields.GetValueOrNull("LpaUserId");
            result.LpaUserPassword = requestOptions.Fields.GetValueOrNull("LpaUserPassword");
            result.LpaSellerNumber = requestOptions.Fields.GetValueOrNull("LpaSellerNumber");
            result.LpaSellerPassword = requestOptions.Fields.GetValueOrNull("LpaSellerPassword");
            result.LpaLoanId = requestOptions.Fields.GetValueOrNull("LpaLoanId");
            result.LpaTransactionId = requestOptions.Fields.GetValueOrNull("LpaTransactionId");
            result.LpaAusKey = requestOptions.Fields.GetValueOrNull("LpaAusKey");
            result.LpaNotpNumber = requestOptions.Fields.GetValueOrNull("LpaNotpNumber");
            result.ShouldImportCredit = requestOptions.Fields.GetValueOrNull("ShouldImportCredit")?.ToNullable<bool>(bool.TryParse) ?? false;

            Guid loanId = requestOptions.Fields.GetValueOrNull(nameof(CPageData.sLId))?.ToNullable<Guid>(Guid.TryParse) ?? Guid.Empty;
            bool lookedUpLoanId = false;
            if (loanId == Guid.Empty)
            {
                string loanName = requestOptions.Fields.GetValueOrNull(nameof(CPageData.sLNm));
                string loanReferenceNumber = requestOptions.Fields.GetValueOrNull(nameof(CPageData.sLRefNm));
                if (!string.IsNullOrEmpty(loanName))
                {
                    lookedUpLoanId = true;
                    loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, loanName);
                }
                else if (!string.IsNullOrEmpty(loanReferenceNumber))
                {
                    lookedUpLoanId = true;
                    loanId = Tools.GetLoanIdByLoanReferenceNumber(principal.BrokerId, loanReferenceNumber);
                }
            }

            if (loanId != Guid.Empty)
            {
                result.LoanResolution = LoanTransferLoanResolver.ExistingLoan(loanId);
                result.LoanBranchId = Tools.GetLoanBranchIdByLoanId(principal.BrokerId, loanId);
            }
            else if (lookedUpLoanId)
            {
                return ParseResult<LoanTransferRequestInput>.Failure(optionsXml, ErrorMessages.LoanNotFound);
            }
            else
            {
                bool isLead = requestOptions.Fields.GetValueOrNull("IsLead")?.ToNullable<bool>(bool.TryParse) ?? false;
                Guid templateId = requestOptions.Fields.GetValueOrNull("TemplateId")?.ToNullable<Guid>(Guid.TryParse) ?? Guid.Empty;
                string templateName = requestOptions.Fields.GetValueOrNull("TemplateNm");
                if (templateId == Guid.Empty && !string.IsNullOrEmpty(templateName))
                {
                    templateId = Tools.GetLoanIdByLoanName(principal.BrokerId, templateName);
                    if (templateId == Guid.Empty)
                    {
                        return ParseResult<LoanTransferRequestInput>.Failure(optionsXml, ErrorMessages.CannotFindLoanTemplate);
                    }
                }

                result.LoanResolution = LoanTransferLoanResolver.NewLoan(createLead: isLead, templateId: templateId);
                result.LoanBranchId = requestOptions.Fields.GetValueOrNull(nameof(CPageData.sBranchId))?.ToNullable<Guid>(Guid.TryParse) ?? Guid.Empty;
            }

            return ParseResult<LoanTransferRequestInput>.Success(
                optionsXml,
                result);
        }

        [WebMethod]
        public string CreateWithDesktopUnderwriter(string sTicket, string doUserName, string doPassword, string sDuCaseId, bool isImportCredit)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            StringBuilder debugMessage = new StringBuilder();
            debugMessage.AppendLine("Loan.asmx::CreateWithDesktopUnderwriter. " + principal.LoginNm + ", DO / DU UserName=" + doUserName + ", sDuCaseId=[" + sDuCaseId + "], IsImportCredit=" + isImportCredit);
             
            try
            {
                string globallyUniqueIdentifier = "";
                string errorMessage = "";
                bool isDone = false;
                bool isRemn = new Guid("baf07293-f626-44c6-9750-706199a7c0b6") == principal.BrokerId;

                while (!isDone)
                {
                    FnmaXisCasefileExportRequest request = new FnmaXisCasefileExportRequest(Guid.Empty);
                    request.FannieMaeMORNETUserID = doUserName;
                    request.FannieMaeMORNETPassword = doPassword;
                    request.GloballyUniqueIdentifier = globallyUniqueIdentifier;
                    request.MornetPlusCasefileIdentifier = sDuCaseId;
                    //if (doUserName == "
                    //request.LenderInstitutionIdentifier = lenderInstitutionIdentifer;
                    AbstractFnmaXisResponse response = DUServer.Submit(request);
                    if (response.IsReady)
                    {
                        isDone = true;
                        if (response.HasError)
                        {
                            errorMessage = response.ErrorMessage;

                        }
                        else if (response.HasBusinessError)
                        {
                            errorMessage = response.BusinessErrorMessage + Environment.NewLine + response.FnmaStatusLog;
                        }
                        else
                        {
                            CLoanFileCreator creator = CLoanFileCreator.GetCreator(BrokerUserPrincipal.CurrentPrincipal, LendersOffice.Audit.E_LoanCreationSource.ImportFromDoDu);
                            Guid sLId = creator.CreateBlankLoanFile();

                            FnmaXisCasefileExportResponse xisResponse = ((FnmaXisCasefileExportResponse)response);
                            xisResponse.ParseHtmlAsFallBack = isRemn;
                            xisResponse.ImportToLoanFile(sLId, BrokerUserPrincipal.CurrentPrincipal, true /* isImport1003 */, true /* isImportDuFindings */, isImportCredit);

                            // 5/14/2012 dd - Save the AUS response to loan file.
                            CPageData dataLoan = new CDuSubmissionImportData(sLId);
                            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                            E_sProd3rdPartyUwResultT duResult = xisResponse.DuResult;
                            if (duResult != E_sProd3rdPartyUwResultT.NA)
                            {
                                dataLoan.sProd3rdPartyUwResultT = duResult;
                            }
                            dataLoan.sAusFindingsPull = true; // OPM 90052
                            dataLoan.Save();

                            debugMessage.AppendLine("sLNm=" + dataLoan.sLNm);

                            return dataLoan.sLNm;
                        }
                    }
                    globallyUniqueIdentifier = response.GloballyUniqueIdentifier;
                }

                debugMessage.AppendLine("Error: " + errorMessage);

                ServiceResult result = new ServiceResult();
                result.Status = ServiceResultStatus.Error;
                result.AppendError(errorMessage);


                return result.ToResponse();
            }

            finally
            {
                Tools.LogInfo(debugMessage.ToString());
            }
        }

        [WebMethod]
        public string ImportFromFannieMaeWithInstitutionId(string sTicket, string sLNm, string doUserName, string doPassword, string lenderInstitutionIdentifer, bool isImportCredit)
        {
            /*
             * In order to use this method the loan must existed and have sDuCaseId set in loan file.
             */
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                return WebServiceUtilities.GenerateErrorResponseXml("Invalid loan number.");
            }
            else
            {
                CPageData dataLoan = new CDuSubmissionImportData(sLId);
                dataLoan.InitLoad();

                string sDuCaseId = dataLoan.sDuCaseId;

                if (string.IsNullOrEmpty(sDuCaseId) == true)
                {
                    return WebServiceUtilities.GenerateErrorResponseXml("Case file id is missing from loan file.");
                }
                else if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
                {
                    return WebServiceUtilities.GenerateErrorResponseXml(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                }

                // 5/14/2012 dd - Import Finding Status

                string globallyUniqueIdentifier = "";
                string errorMessage = "";
                bool isDone = false;


                while (!isDone)
                {
                    FnmaXisCasefileExportRequest request = new FnmaXisCasefileExportRequest(Guid.Empty);
                    request.FannieMaeMORNETUserID = doUserName;
                    request.FannieMaeMORNETPassword = doPassword;
                    request.GloballyUniqueIdentifier = globallyUniqueIdentifier;
                    request.MornetPlusCasefileIdentifier = sDuCaseId;
                    request.LenderInstitutionIdentifier = lenderInstitutionIdentifer;
                    AbstractFnmaXisResponse response = DUServer.Submit(request);
                    if (response.IsReady)
                    {
                        isDone = true;
                        if (response.HasError)
                        {
                            errorMessage = response.ErrorMessage;

                        }
                        else if (response.HasBusinessError)
                        {
                            errorMessage = response.BusinessErrorMessage + Environment.NewLine + response.FnmaStatusLog;
                        }
                        else
                        {
                            FnmaXisCasefileExportResponse xisResponse = ((FnmaXisCasefileExportResponse)response);

                            xisResponse.ImportToLoanFile(sLId, BrokerUserPrincipal.CurrentPrincipal, true /* isImport1003 */, true /* isImportDuFindings */, isImportCredit);

                            // 5/14/2012 dd - Save the AUS response to loan file.
                            dataLoan = new CDuSubmissionImportData(sLId);
                            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                            E_sProd3rdPartyUwResultT duResult = xisResponse.DuResult;
                            if (duResult != E_sProd3rdPartyUwResultT.NA)
                            {
                                dataLoan.sProd3rdPartyUwResultT = duResult;
                            }
                            dataLoan.sAusFindingsPull = true; // OPM 90052
                            dataLoan.Save();
                        }
                    }
                    globallyUniqueIdentifier = response.GloballyUniqueIdentifier;
                }


                if (string.IsNullOrEmpty(errorMessage))
                {
                    return "<result status=\"OK\"></result>";
                }
                else
                {
                    XmlDocument document = new XmlDocument();
                    XmlElement resultElement = document.CreateElement("result");
                    document.AppendChild(resultElement);

                    resultElement.SetAttribute("status", "Error");
                    XmlElement errorListElement = document.CreateElement("Errors");
                    resultElement.AppendChild(errorListElement);

                    XmlElement errorElement = document.CreateElement("Error");
                    errorElement.InnerText = errorMessage;

                    errorListElement.AppendChild(errorElement);

                    return document.OuterXml;
                }
            }
        }

        [WebMethod]
        public string ImportFromFannieMae(string sTicket, string sLNm, string doUserName, string doPassword, bool isImportCredit)
        {
            /*
             * In order to use this method the loan must existed and have sDuCaseId set in loan file.
             */
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);


            if (sLId == Guid.Empty)
            {
                return WebServiceUtilities.GenerateErrorResponseXml("Invalid loan number.");
            }
            else
            {
                CPageData dataLoan = new CDuSubmissionImportData(sLId);
                dataLoan.InitLoad();

                string sDuCaseId = dataLoan.sDuCaseId;

                if (string.IsNullOrEmpty(sDuCaseId) == true)
                {
                    return WebServiceUtilities.GenerateErrorResponseXml("Case file id is missing from loan file");
                }
                else if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
                {
                    return WebServiceUtilities.GenerateErrorResponseXml(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                }

                // 5/14/2012 dd - Import Finding Status

                string globallyUniqueIdentifier = "";
                string errorMessage = "";
                bool isDone = false;
                bool isremn = false;

                string lenderInstitutionIdentifer = string.Empty;

                if (doUserName.Equals("c1x04r6f", StringComparison.CurrentCultureIgnoreCase) ||
                    doUserName.Equals("w2n7emnt", StringComparison.CurrentCultureIgnoreCase) ||
                    doUserName.Equals("c1x04rbd", StringComparison.CurrentCultureIgnoreCase) ||
                    doUserName.Equals("c1x04hnd", StringComparison.CurrentCultureIgnoreCase) ||
                    doUserName.Equals("c1x04p3s", StringComparison.CurrentCultureIgnoreCase) ||
                    doUserName.Equals("c1x04rcw", StringComparison.CurrentCultureIgnoreCase)
                    )
                {
                    // 6/5/2012 dd - For REMN account and our test account run another query to get Lender Institution Id.
                    lenderInstitutionIdentifer = GetDesktopUnderwriterLenderInstitutionIdentifier(sDuCaseId, doUserName, doPassword, out errorMessage);
                    isremn = true;

                    if (string.IsNullOrEmpty(errorMessage) == false)
                    {
                        isDone = true;
                    }
                }


                while (!isDone)
                {
                    FnmaXisCasefileExportRequest request = new FnmaXisCasefileExportRequest(Guid.Empty);
                    request.FannieMaeMORNETUserID = doUserName;
                    request.FannieMaeMORNETPassword = doPassword;
                    request.GloballyUniqueIdentifier = globallyUniqueIdentifier;
                    request.MornetPlusCasefileIdentifier = sDuCaseId;
                    request.LenderInstitutionIdentifier = lenderInstitutionIdentifer;
                    AbstractFnmaXisResponse response = DUServer.Submit(request);
                    if (response.IsReady)
                    {
                        isDone = true;
                        if (response.HasError)
                        {
                            errorMessage = response.ErrorMessage;

                        }
                        else if (response.HasBusinessError)
                        {
                            errorMessage = response.BusinessErrorMessage + Environment.NewLine + response.FnmaStatusLog;
                        }
                        else
                        {
                            FnmaXisCasefileExportResponse xisResponse = ((FnmaXisCasefileExportResponse)response);
                            xisResponse.ParseHtmlAsFallBack = isremn;
                            xisResponse.ImportToLoanFile(sLId, BrokerUserPrincipal.CurrentPrincipal, true /* isImport1003 */, true /* isImportDuFindings */, isImportCredit);

                            // 5/14/2012 dd - Save the AUS response to loan file.
                            dataLoan = new CDuSubmissionImportData(sLId);
                            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                            E_sProd3rdPartyUwResultT duResult = xisResponse.DuResult;
                            if (duResult != E_sProd3rdPartyUwResultT.NA)
                            {
                                dataLoan.sProd3rdPartyUwResultT = duResult;
                            }
                            dataLoan.sAusFindingsPull = true; // OPM 90052
                            dataLoan.Save();
                        }
                    }
                    globallyUniqueIdentifier = response.GloballyUniqueIdentifier;
                }


                if (string.IsNullOrEmpty(errorMessage))
                {
                    return "<result status=\"OK\"></result>";
                }
                else
                {
                    XmlDocument document = new XmlDocument();
                    XmlElement resultElement = document.CreateElement("result");
                    document.AppendChild(resultElement);

                    resultElement.SetAttribute("status", "Error");
                    XmlElement errorListElement = document.CreateElement("Errors");
                    resultElement.AppendChild(errorListElement);

                    XmlElement errorElement = document.CreateElement("Error");
                    errorElement.InnerText = errorMessage;

                    errorListElement.AppendChild(errorElement);

                    return document.OuterXml;
                }
            }
        }

        private string GetDesktopUnderwriterLenderInstitutionIdentifier(string duCaseId, string duUserName, string duPassword, out string errorMessage)
        {
            FnmaDwebCasefileStatusRequest request = new FnmaDwebCasefileStatusRequest(false /* isDo*/, Guid.Empty);

            request.FannieMaeMORNETUserID = duUserName;
            request.FannieMaeMORNETPassword = duPassword;
            request.MornetPlusCasefileIdentifier = duCaseId;

            FnmaDwebCasefileStatusResponse response = (FnmaDwebCasefileStatusResponse)DUServer.Submit(request);
            errorMessage = string.Empty;
            string lenderInstitutionIdentifer = string.Empty;
            if (response.IsReady)
            {
                if (response.HasError)
                {
                    errorMessage = response.ErrorMessage;
                }
                else if (response.HasBusinessError)
                {
                    errorMessage = response.BusinessErrorMessage + Environment.NewLine + response.FnmaStatusLog;
                }
                else
                {
                    lenderInstitutionIdentifer = response.LenderInstitutionIdentifier;
                }
            }
            return lenderInstitutionIdentifer;
        }

        [WebMethod(Description = "This method returns casefile status from DU/DO. isDo determines whether the user credentials are for Desktop Originator. This method obtains CasefileID from the specified loan file.")]
        public string GetDesktopUnderwriterCasefileStatusByLoanFile(string sTicket, string sLNm, string duUserName, string duPassword, bool isDo)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);
            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            CPageData dataLoan = new CDuSubmissionImportData(sLId);
            dataLoan.InitLoad();

            string sDuCaseId = dataLoan.sDuCaseId;

            if (string.IsNullOrEmpty(sDuCaseId) == true)
            {
                return WebServiceUtilities.GenerateErrorResponseXml("Case file id is missing from loan file");
            }
            else if (dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed)
            {
                return WebServiceUtilities.GenerateErrorResponseXml(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }
            else
            {
                return GetDesktopUnderwriterCasefileStatus(sDuCaseId, duUserName, duPassword, isDo);
            }
        }

        [WebMethod(Description = "This method returns casefile status from DU/DO. isDo determines whether the user credentials are for Desktop Originator.")]
        public string GetDesktopUnderwriterCasefileStatus(string sTicket, string sDuCaseId, string duUserName, string duPassword, bool isDo)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            return GetDesktopUnderwriterCasefileStatus(sDuCaseId, duUserName, duPassword, isDo);
        }

        private string GetDesktopUnderwriterCasefileStatus(string sDuCaseId, string duUserName, string duPassword, bool isDo)
        {

            FnmaDwebCasefileStatusRequest request = new FnmaDwebCasefileStatusRequest(isDo /* isDo*/, Guid.Empty);

            request.FannieMaeMORNETUserID = duUserName;
            request.FannieMaeMORNETPassword = duPassword;
            request.MornetPlusCasefileIdentifier = sDuCaseId;

            FnmaDwebCasefileStatusResponse response = (FnmaDwebCasefileStatusResponse)DUServer.Submit(request);
            string lenderInstitutionIdentifer = string.Empty;
            if (response.IsReady)
            {
                if (response.HasError)
                    return WebServiceUtilities.GenerateErrorResponseXml(response.ErrorMessage);
                else if (response.HasBusinessError)
                    return WebServiceUtilities.GenerateErrorResponseXml(response.BusinessErrorMessage);
                else
                    return "<CasefileStatusResponse>"
                            + "<LastUnderwritingDate>" + response.LastUnderwritingDate + "</LastUnderwritingDate>"
                            + "<UnderwritingRecommendation>" + response.UnderwritingRecommendationDescription + "</UnderwritingRecommendation>"
                            + "<UnderwritingStatus>" + response.UnderwritingStatusDescription + "</UnderwritingStatus>"
                            + "<UnderwritingSubmissionType>" + response.UnderwritingSubmissionType + "</UnderwritingSubmissionType>"
                            + "<OriginatorInstitutionName>" + response.OriginatorInstitutionName + "</OriginatorInstitutionName>"
                            + "<OriginatorInstitutionIdentifier>" + response.OriginatorInstitutionIdentifier + "</OriginatorInstitutionIdentifier>"
                            + "<LenderInstitutionName>" + response.LenderInstitutionName + "</LenderInstitutionName>"
                            + "<LenderInstitutionIdentifier>" + response.LenderInstitutionIdentifier + "</LenderInstitutionIdentifier>"
                            + "</CasefileStatusResponse>";
            }
            else
            {
                return string.Empty;
            }
        }

        [WebMethod]
        public string SubmitToDesktopOriginator(string sTicket, string sLNm, string doUserName, string doPassword)
        {
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                return WebServiceUtilities.GenerateErrorResponseXml("Invalid loan number.");
            }
            else if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                return WebServiceUtilities.GenerateErrorResponseXml(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }
            else
            {
                return SubmitToFnmaImpl(sLId, true, doUserName, doPassword, string.Empty);
            }

        }

        private string SubmitToFnmaImpl(Guid sLId, bool isDo, string doUserName, string doPassword, string institutionId)
        {
            CPageData dataLoan = new CDuSubmissionImportData(sLId);
            dataLoan.InitLoad();
            
            string sDuCaseId = dataLoan.sDuCaseId;

            FnmaDwebCasefileImportRequest request = new FnmaDwebCasefileImportRequest(isDo, sLId);
            request.FannieMaeMORNETUserID = doUserName;
            request.FannieMaeMORNETPassword = doPassword;
            request.MornetPlusCasefileIdentifier = sDuCaseId;
            request.XisCasefileInstitution.InstitutionIdentifier = institutionId;
            //request.FnmaContent = fnmaContent;
            string status = "Error";
            string errorMessage = string.Empty;
            string url = string.Empty;
            try
            {
                AbstractFnmaXisResponse response = DUServer.Submit(request);
                if (response.IsReady)
                {
                    if (response.HasError)
                    {
                        status = "Error";
                        errorMessage = response.ErrorMessage;
                    }
                    else if (response.HasBusinessError)
                    {
                        status = "Error";
                        errorMessage = response.BusinessErrorMessage + Environment.NewLine + response.FnmaStatusLog;
                    }
                    else
                    {
                        status = "OK";
                        if (sDuCaseId != response.MornetPlusCasefileIdentifier)
                        {
                            dataLoan = new CDuSubmissionImportData(sLId);
                            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                            dataLoan.sDuCaseId = response.MornetPlusCasefileIdentifier;
                            dataLoan.Save();
                            sDuCaseId = response.MornetPlusCasefileIdentifier;
                        }
                        string cacheAuth = string.Format("{0}\n{1}\n{2}\n{3}", request.Url, request.FannieMaeMORNETUserID, request.FannieMaeMORNETPassword, response.MornetPlusCasefileIdentifier);
                        string id = AutoExpiredTextCache.AddToCache(cacheAuth, new TimeSpan(0, 15, 0)); // Cache for 15 minute.
                        url = "https://webservices.lendingqb.com/los/webservice/LaunchDoDu.aspx?id=" + id;
                    }
                }
            }
            catch (CBaseException exc)
            {
                status = "Error";
                errorMessage = exc.UserMessage;
            }

            XmlDocument document = new XmlDocument();
            XmlElement resultElement = document.CreateElement("result");
            document.AppendChild(resultElement);

            resultElement.SetAttribute("status", status);
            if (status == "Error")
            {
                XmlElement errorListElement = document.CreateElement("Errors");
                resultElement.AppendChild(errorListElement);

                XmlElement errorElement = document.CreateElement("Error");
                errorElement.InnerText = errorMessage;

                errorListElement.AppendChild(errorElement);
            }
            else
            {
                XmlElement urlElement = document.CreateElement("Url");
                resultElement.AppendChild(urlElement);

                urlElement.InnerText = url;
            }
            return document.OuterXml;
        }

        //4/28/2014 bs - For mobile app only, it will be retired on 8/15/2014. The new one is available in MobileWebServices.Mobile.asmx.cs
        //The reason is so the old mobile won't crash until user upgrades to new version
        [WebMethod]
        public string RetrieveRecentActiveLoanList(string sTicket)
        {
            // 11/8/2011 dd - This method is only design to use in mobile app. 
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            return LendersOffice.LoanSearch.LendingQBSearch.ListRecentLoansForMobileInXmlFormat(principal);
        }

        //4/28/2014 bs - For mobile app only, it will be retired on 8/15/2014. The new one is available in MobileWebServices.Mobile.asmx.cs
        //The reason is so the old mobile won't crash until user upgrades to new version
        [WebMethod]
        public string RetrieveLoanActivityList(string sTicket, string sLNm, int version)
        {
            // 11/8/2011 dd - This method is only design to use in mobile app.
            if (version != 1)
            {
                // 11/8/2011 dd - For now we only support version 1.
                throw new CBaseException("Expect version=1", "Expect version=1");
            }
            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);
            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            AuthServiceHelper.CheckLoanAuthorization(sLId, WorkflowOperations.ReadLoan);

            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            return LendersOffice.MobileApp.LatestActivity.GenerateLoanActivityXml(sLId, principal);
        }

        // OPM 69104
        [WebMethod(Description = "This method returns an XML containing the list of loan files accessible to a PML user.")]
        public string RetrievePmlLoanList(string sTicket)
        {
            LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(2000); //delay 2 seconds to decrease the rate of traffic

            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket);
            CallVolume.IncreaseAndCheck(principal);

            var searchFilter = new LendersOffice.LoanSearch.LendingQBSearchFilter();
            searchFilter.LoanAssignedToT = 0;   // Assigned to Anyone
            searchFilter.sStatusT = -1;         // Any status
            searchFilter.sStatusDateT = -1;     // All dates

            XmlDocument outXml = LendersOffice.LoanSearch.LendingQBPmlSearch.SearchReturnXml(principal, searchFilter, 200);

            return outXml.OuterXml;
        }

        [WebMethod]
        public string CheckValidAppStatus(string appName, string appVersion)
        {
            // 8/20/2012 dd - This method is for other standalone app to check to see if
            // the newer version is available.

            // Right now we just always return app has latest version.
            string status = "Current"; // Current, NewVersionAvailable, NewVersionMustUpdate, Error
            string errorMessage = string.Empty;

            if (string.IsNullOrEmpty(appName) || string.IsNullOrEmpty(appVersion))
            {
                status = "Error";
                errorMessage = "AppName and AppVersion cannot be empty.";
            }
            if (appVersion == "-1.0")
            {
                // 8/20/2012 dd - Test Warning.
                status = "NewVersionAvailable";
                errorMessage = "There is a newer version of " + appName + " available.";
            }
            else if (appVersion == "-2.0")
            {
                // 8/20/2012 dd - Test NewVersionMustUpdate.
                status = "NewVersionMustUpdate";
                errorMessage = "This version of " + appName + " is no longer supported.";
            }


            XElement el = new XElement("result");
            el.SetAttributeValue("status", status);
            if (string.IsNullOrEmpty(errorMessage) == false)
            {
                el.Value = errorMessage;
            }
            return el.ToString();
        }

        /// <summary>
        /// Recieves a PDF file from an integration partner, and adds it to a loan. 
        /// </summary>
        /// <param name="AuthTicket">The authorization ticket from the call to AuthService.GetUserAuthTicket</param>
        /// <param name="sLNm">An identifier for the loan in which these docs should wind up, will be provided in the customer's request.</param>
        /// <param name="DocumentClassification">A string that classifies the submitted document, per that spreadsheet in case 83810</param>
        /// <param name="base64PdfContent">An array of the bytes that compose the PDF.</param>
        /// <returns>An xml string with a Result element and a Status attribute. On success, the Status attribute will be "Success". 
        /// On error, the Status attribute will be "Error" and the Result element will contain an ErrorMessage element, whose text will be a description of the error.</returns>
        [WebMethod(Description = "PDF upload for integrations")]
        public string RecievePdf(string AuthTicket, string sLNm, string DocumentClassification, string base64PdfContent)
        {
            ServiceResult result = new ServiceResult();
            result.Status = ServiceResultStatus.Error;
            try
            {
                if (string.IsNullOrEmpty(AuthTicket.TrimWhitespaceAndBOM()) ||
                    string.IsNullOrEmpty(sLNm.TrimWhitespaceAndBOM()) ||
                    string.IsNullOrEmpty(DocumentClassification.TrimWhitespaceAndBOM()) ||
                    string.IsNullOrEmpty(base64PdfContent.TrimWhitespaceAndBOM()))
                {
                    result.AppendError("All parameters are required.");
                    return result.ToResponse();
                }

                try
                {
                    BillingInfo genericFrameworkBillingInfo;
                    AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(AuthTicket, sLNm, out genericFrameworkBillingInfo);
                    CallVolume.IncreaseAndCheck(principal);
                    if (genericFrameworkBillingInfo != null)
                    {
                        result.AppendError(
                            "Generic Framework tickets do not have access to RecievePdf. Please use the equivalent methods "
                            + (genericFrameworkBillingInfo.ServiceType == TypeOfService.Multiple
                                ? "UploadToPdfDocumentAsService or UploadToPdfDocumentToAppAsService, with a valid service type,"
                                : "UploadToPdfDocument or UploadToPdfDocumentToApp")
                            + " to upload files to the loan.");
                        return result.ToResponse();
                    }
                }
                catch (CBaseException)
                {
                    result.AppendError("Could not authenticate your auth ticket. Make sure it's from AuthService.");
                    return result.ToResponse();
                }

                //Only service users are allowed
                var lastName = BrokerUserPrincipal.CurrentPrincipal.LastName;
                if (!lastName.Equals("Service Account", StringComparison.CurrentCultureIgnoreCase))
                {
                    result.AppendError("Account with login [" + BrokerUserPrincipal.CurrentPrincipal.LoginNm + "] is not a service account.");
                    return result.ToResponse();
                }

                var BrokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
                Guid sLId = Tools.GetLoanIdByLoanName(BrokerId, sLNm);
                if (sLId == Guid.Empty)
                {
                    result.AppendError("Loan [" + sLNm + "] was not found");
                    return result.ToResponse();
                }
                else if (Tools.IsQP2LoanFile(sLId, BrokerId))
                {
                    result.AppendError(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                    return result.ToResponse();
                }

                int DestinationDocTypeId;
                DocType DestinationDocType;
                bool unclassified = false;
                var ClassifiedDocTypes = EDocumentDocType.GetDocTypesByClassificationName(BrokerId);
                if (!ClassifiedDocTypes.TryGetValue(DocumentClassification, out DestinationDocType))
                {
                    DestinationDocTypeId = EDocumentDocType.GetOrCreatePartnerUpload(BrokerId);
                    unclassified = true;
                }
                else
                {
                    DestinationDocTypeId = int.Parse(DestinationDocType.DocTypeId);
                }

                byte[] documentBytes;
                try
                {
                    documentBytes = Convert.FromBase64String(base64PdfContent);
                }
                catch (FormatException)
                {
                    result.Status = ServiceResultStatus.Error;
                    result.AppendError("Could not parse base64PdfContent, please make sure that it is Base 64 encoded.");
                    return result.ToResponse();
                }

                //save
                //This user will be limited, so we need to bypass checks
                EDocumentRepository repo = EDocumentRepository.GetSystemRepository(BrokerId);

                //All we need is the id of the first app so we can put the documents in it
                var dataLoan = new CFullAccessPageData(sLId, new[] { "sLNm" });
                dataLoan.InitLoad();
                var AppId = dataLoan.GetAppData(0).aAppId;

                var doc = repo.CreateDocument(E_EDocumentSource.FromIntegration);
                doc.DocumentTypeId = DestinationDocTypeId;
                doc.PublicDescription = "Uploaded by integration with " + BrokerUserPrincipal.CurrentPrincipal.FirstName + " on " + DateTime.Now.ToShortDateString();

                if (unclassified)
                {
                    doc.PublicDescription = doc.PublicDescription + " as " + DocumentClassification;
                }

                doc.LoanId = sLId;
                doc.AppId = AppId;
                doc.IsUploadedByPmlUser = false;

                string filePath = TempFileUtils.NewTempFilePath();
                BinaryFileHelper.WriteAllBytes(filePath, documentBytes);
                try
                {
                    doc.UpdatePDFContentOnSave(filePath);
                }
                catch (InvalidPDFFileException)
                {
                    result.Status = ServiceResultStatus.Error;
                    result.AppendError("base64PdfContent was decoded, but it doesn't look like a PDF.");
                    return result.ToResponse();
                }

                repo.Save(doc);

                if (result.Status == ServiceResultStatus.Error) result.Status = ServiceResultStatus.OK;
                return result.ToResponse();
            }
            finally
            {
                if (result.Status == ServiceResultStatus.Error)
                {
                    Tools.LogWarning("Error receiving pdf via integration. Response is: " + result.ToResponse());
                }
                else if (result.Status == ServiceResultStatus.OKWithWarning)
                {
                    Tools.LogWarning("Warnings issued when receiving pdf via integration. Response is: " + result.ToResponse());
                }
            }
        }

        /// <summary>
        /// Retrieves the audit logs in CSV format.
        /// </summary>
        /// <param name="authTicket">The user's authentication ticket.</param>
        /// <param name="loanNumber">The loan number.</param>
        /// <exception cref="ArgumentException">Throws if an invalid authTicket or loanNumber are passed in.</exception>
        [WebMethod(Description = "Retrieves the loan's audit log")]
        public string GetAuditLog(string authTicket, string loanNumber)
        {

            if (string.IsNullOrWhiteSpace(loanNumber))
            {
                throw new ArgumentException(ErrorMessages.WebServices.InvalidLoanNumber);
            }

            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(authTicket, null);

            if (principal == null)
            {
                throw new ArgumentException(ErrorMessages.WebServices.InvalidAuthTicket);
            }

            bool hasAccountantReadPermission = principal.HasPermission(Permission.AllowAccountantRead);
            bool hasInvestorViewPermission = principal.HasPermission(Permission.AllowViewingInvestorInformation);
            bool hasQualityControlAccess = principal.HasPermission(Permission.AllowQualityControlAccess);

            if (!((hasAccountantReadPermission && hasInvestorViewPermission) || hasQualityControlAccess))
            {
                throw new ArgumentException(ErrorMessages.GenericAccessDenied);
            }

            Guid loanIdentifier = Tools.GetLoanIdByLoanName(principal.BrokerId, loanNumber);
            if (loanIdentifier == Guid.Empty)
            {
                throw new ArgumentException(ErrorMessages.WebServices.InvalidLoanNumber);
            }

            if (!AuthServiceHelper.IsOperationAuthorized(loanIdentifier, WorkflowOperations.ReadLoan))
            {
                throw new ArgumentException(ErrorMessages.GenericAccessDenied);
            }

            CallVolume.IncreaseAndCheck(principal);
            if (Tools.IsQP2LoanFile(loanIdentifier, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            return AuditManager.CreateAuditListCSV(loanIdentifier, principal);
        }

        /// <summary>
        /// Loads available document packages and plan codes for given loan through the LQB Document Framework. Logs request and response to PB.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by AuthService.asmx.</param>
        /// <param name="sLNm">LendingQB Loan Number.</param>
        /// <param name="vendorId">Unique identifier of LQB Document Framework vendor.</param>
        /// <param name="userName">Username for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="password">Password for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="accountId">Account ID for accessing LQB Document Framework vendor. Not all vendors require this.</param>
        /// <returns>Returns an LOXml formatted response with either available doc packages with permissions and plan codes or reason for error.</returns>
        [WebMethod(Description = "Loads available document packages and plan codes for given loan through the LQB Document Framework. AccountId may be empty if not required by the document vendor.")]
        public string DocumentFrameworkGetAvailableDocPackagesAndPlanCodes(string sTicket, string sLNm, string vendorId, string userName, string password, string accountId)
        {
            string methodName = "DocumentFrameworkGetAvailableDocPackagesAndPlanCodes";
            LogDocumentFrameworkRequest(methodName, sLNm, vendorId, userName, accountId);

            // Validate principal
            AbstractUserPrincipal principal;
            string obtainPrincipalFailureMessage = WebServiceUtilities.TryToGetUserPrincipal(sTicket, sLNm, out principal);
            if (string.IsNullOrEmpty(obtainPrincipalFailureMessage) == false)
            {
                LoXmlServiceResult badPrincipalResult = new LoXmlServiceResult();
                badPrincipalResult.Status = ServiceResultStatus.Error;
                badPrincipalResult.AppendError(obtainPrincipalFailureMessage);
                return badPrincipalResult.ToResponse();
            }

            var loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
            if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                LoXmlServiceResult cannotRunOnQp2FileResult = new LoXmlServiceResult();
                cannotRunOnQp2FileResult.Status = ServiceResultStatus.Error;
                cannotRunOnQp2FileResult.AppendError(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                return cannotRunOnQp2FileResult.ToResponse();
            }

            string response = DocFrameworkMethodContainer.GeneratePackageTypeAndPlanCodeResponse(principal, sLNm, vendorId, userName, password, accountId);

            LogDocumentFrameworkResponse(methodName, response);
            return response;
        }

        /// <summary>
        /// Performs audit through LQB Document Framework.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by AuthService.asmx.</param>
        /// <param name="sLNm">LendingQB loan number.</param>
        /// <param name="vendorId">Unique identifier of LQB Document Framework vendor.</param>
        /// <param name="userName">Username for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="password">Password for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="accountId">Account ID for accessing LQB Document Framework vendor. Not all vendors require this.</param>
        /// <param name="packageId">Document package to be requested from vendor.</param>
        /// <param name="planCodeId">Plan code id, representing loan program requested from the vendor.</param>
        /// <returns>Returns the results of LQB Document Framework audit. If successful, a token will be issued to allow requesting of documents.</returns>
        [WebMethod(Description = "Requests an audit through the LQB Document Framework. AccountId may be empty if not required by the document vendor.")]
        public string DocumentFrameworkPerformAudit(string sTicket, string sLNm, string vendorId, string userName, string password, string accountId, string packageId, string planCodeId)
        {
            string methodName = "DocumentFrameworkPerformAudit";
            LogDocumentFrameworkRequest(methodName, sLNm, vendorId, userName, accountId, "packageId: " + packageId, "planCodeId: " + planCodeId);

            // Validate principal
            AbstractUserPrincipal principal;
            string obtainPrincipalFailureMessage = WebServiceUtilities.TryToGetUserPrincipal(sTicket, sLNm, out principal);
            if (string.IsNullOrEmpty(obtainPrincipalFailureMessage) == false)
            {
                LoXmlServiceResult badPrincipalResult = new LoXmlServiceResult();
                badPrincipalResult.Status = ServiceResultStatus.Error;
                badPrincipalResult.AppendError(obtainPrincipalFailureMessage);
                return badPrincipalResult.ToResponse();
            }

            var loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
            if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                LoXmlServiceResult cannotRunOnQp2FileResult = new LoXmlServiceResult();
                cannotRunOnQp2FileResult.Status = ServiceResultStatus.Error;
                cannotRunOnQp2FileResult.AppendError(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                return cannotRunOnQp2FileResult.ToResponse();
            }

            string response = DocFrameworkMethodContainer.GenerateAuditResponse(principal, sLNm, vendorId, userName, password, accountId, packageId, planCodeId);

            LogDocumentFrameworkResponse(methodName, response);
            return response;
        }

        /// <summary>
        /// Requests a list of individual forms that can be generated at document request via LQB Document Framework.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by AuthService.asmx.</param>
        /// <param name="sLNm">LendingQb loan number.</param>
        /// <param name="vendorId">Unique identifier of LQB Document Framework vendor.</param>
        /// <param name="userName">Username for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="password">Password for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="accountId">Account ID for accessing LQB Document Framework vendor. Not all vendors require this.</param>
        /// <param name="packageId">Document package to be requested from vendor.</param>
        /// <param name="planCodeId">Plan code id, representing loan program requested from the vendor.</param>
        /// <returns>Returns a list of individual forms that can be generated at document request via LQB Document Framework.</returns>
        [WebMethod(Description = "Requests a list of individual forms that can be generated at document request via LQB Document Framework.")]
        public string DocumentFrameworkGetFormsList(string sTicket, string sLNm, string vendorId, string userName, string password, string accountId, string packageId, string planCodeId)
        {
            string methodName = "DocumentFrameworkGetFormsList";
            LogDocumentFrameworkRequest(methodName, sLNm, vendorId, userName, accountId, "packageId: " + packageId, "planCodeId: " + planCodeId);

            // Validate principal
            AbstractUserPrincipal principal;
            string obtainPrincipalFailureMessage = WebServiceUtilities.TryToGetUserPrincipal(sTicket, sLNm, out principal);
            if (string.IsNullOrEmpty(obtainPrincipalFailureMessage) == false)
            {
                LoXmlServiceResult badPrincipalResult = new LoXmlServiceResult();
                badPrincipalResult.Status = ServiceResultStatus.Error;
                badPrincipalResult.AppendError(obtainPrincipalFailureMessage);
                return badPrincipalResult.ToResponse();
            }

            var loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
            if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                LoXmlServiceResult cannotRunOnQp2FileResult = new LoXmlServiceResult();
                cannotRunOnQp2FileResult.Status = ServiceResultStatus.Error;
                cannotRunOnQp2FileResult.AppendError(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                return cannotRunOnQp2FileResult.ToResponse();
            }

            string response = DocFrameworkMethodContainer.GenerateFormListResponse(principal, sLNm, vendorId, userName, password, accountId, packageId, planCodeId);

            LogDocumentFrameworkResponse(methodName, response);
            return response;
        }

        /// <summary>
        /// Generates Documents via LQB Document Framework.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by AuthService.asmx.</param>
        /// <param name="sLNm">LendingQb loan number.</param>
        /// <param name="token">The token obtained from DocumentFrameworkPerformAudit.</param>
        /// <param name="optionsXml">The xml containing the document generation options.</param>
        /// <returns>If successful, returns xml containing a key to download the documents. Will return error xml if there is an error.</returns>
        [WebMethod(Description = "Generates Documents via LQB Document Framework.")]
        public string DocumentFrameworkGenerateDocs(string sTicket, string sLNm, string token, string optionsXml)
        {
            string methodName = "DocumentFrameworkGenerateDocs";
            LogDocumentFrameworkRequest(methodName, sLNm, string.Empty, string.Empty, string.Empty, "token: " + token, "optionsXml: " + optionsXml);

            // Validate principal
            AbstractUserPrincipal principal;
            string obtainPrincipalFailureMessage = WebServiceUtilities.TryToGetUserPrincipal(sTicket, sLNm, out principal);
            if (string.IsNullOrEmpty(obtainPrincipalFailureMessage) == false)
            {
                LoXmlServiceResult badPrincipalResult = new LoXmlServiceResult();
                badPrincipalResult.Status = ServiceResultStatus.Error;
                badPrincipalResult.AppendError(obtainPrincipalFailureMessage);
                return badPrincipalResult.ToResponse();
            }

            var loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
            if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                LoXmlServiceResult cannotRunOnQp2FileResult = new LoXmlServiceResult();
                cannotRunOnQp2FileResult.Status = ServiceResultStatus.Error;
                cannotRunOnQp2FileResult.AppendError(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
                return cannotRunOnQp2FileResult.ToResponse();
            }

            string response = DocFrameworkMethodContainer.CreateDocumentGenerationResponse(principal, sLNm, token, optionsXml);

            LogDocumentFrameworkResponse(methodName, response);
            return response;
        }

        /// <summary>
        /// Downloads the generated document using the download key provided.
        /// </summary>
        /// <param name="sTicket">Authentication string returned by AuthService.asmx.</param>
        /// <param name="sLNm">The loan number of the loan that this document was generated for.</param>
        /// <param name="downloadKey">Key to download the generated documents.</param>
        /// <returns>The file.</returns>
        [WebMethod(Description = "Downloads the generated documents using the download key provided in DocumentFrameworkGenerateDocs.")]
        public byte[] DocumentFrameworkDownloadGeneratedDocument(string sTicket, string sLNm, string downloadKey)
        {
            string methodName = "DocumentFrameworkDownloadGeneratedDocument";
            LogDocumentFrameworkRequest(methodName, sLNm, string.Empty, string.Empty, string.Empty, "downloadKey: " + downloadKey);

            AbstractUserPrincipal principal;
            string obtainPrincipalFailureMessage = WebServiceUtilities.TryToGetUserPrincipal(sTicket, sLNm, out principal);
            if (string.IsNullOrEmpty(obtainPrincipalFailureMessage) == false)
            {
                throw new CBaseException("Unable to validate.", "Unable to get principal using sTicket.");
            }

            var loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
            if (Tools.IsQP2LoanFile(loanId, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            byte[] file = DocFrameworkMethodContainer.DownloadGeneratedDocs(downloadKey);
            LogDocumentFrameworkResponse(methodName, "File successfully retrieved.");
            return file;
        }

        [WebMethod(Description = "Generates the 10 closest counseling orgniazations and populates them to the loan.")]
        public string GenerateClosestCounselingOrganization(string sTicket, string sLNm)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Error;

            if (string.IsNullOrEmpty(sTicket))
            {
                result.AppendError("sTicket cannot be empty.");
                return result.ToResponse();
            }

            if (string.IsNullOrEmpty(sLNm))
            {
                result.AppendError("sLNm cannot be empty.");
                return result.ToResponse();
            }
            AbstractUserPrincipal principal;
            string obtainPrincipalFailureMessage = WebServiceUtilities.TryToGetUserPrincipal(sTicket, null, out principal);
            if (!string.IsNullOrEmpty(obtainPrincipalFailureMessage))
            {
                result.AppendError(obtainPrincipalFailureMessage);
                return result.ToResponse();
            }

            CallVolume.IncreaseAndCheck(principal);
            return CounselingOrganizationMethods.GenerateClosestCounselingOrganization(sLNm, principal);
        }

        [WebMethod(Description = "Allows users to record an initial disclosure in the system.")]
        public string RecordInitialDisclosureEventWithPartnerKey(string sTicket, string loanNumber, string dataXml, string secretKey)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Error;

            if (string.IsNullOrEmpty(sTicket))
            {
                result.AppendError("sTicket cannot be empty.");
                return result.ToResponse();
            }
            else if (string.IsNullOrEmpty(loanNumber))
            {
                result.AppendError("loanNumber cannot be empty.");
                return result.ToResponse();
            }
            else if (string.IsNullOrEmpty(secretKey))
            {
                result.AppendError("secretKey cannot be empty.");
                return result.ToResponse();
            }

            Guid? neededKey = ConstStage.WebserviceRecordInitialDisclosureSecretKey;
            Guid key;
            if (!neededKey.HasValue || !Guid.TryParse(secretKey, out key) || neededKey.Value != key)
            {
                result.AppendError("Access denied. Invalid key.");
                return result.ToResponse();
            }
            AbstractUserPrincipal principal;
            string principalFailureMessage = WebServiceUtilities.TryToGetUserPrincipal(sTicket, null, out principal);
            if (!string.IsNullOrEmpty(principalFailureMessage))
            {
                result.AppendError(principalFailureMessage);
                return result.ToResponse();
            }

            CallVolume.IncreaseAndCheck(principal);

            DocFrameworkMethodContainer.RecordInitialDisclosureEvent(principal, loanNumber, dataXml, result);
            return result.ToResponse();
        }

        [WebMethod(Description = "Triggers the creation of a Closing Disclosure Archive for the loan file with the specified reference number. Returns XML containing the created archive's ID and status.")]
        public string CreateClosingDisclosureArchiveByReferenceNumber(string authTicket, string loanRefNumber, string dataXml, string secretKey)
        {
            return CreateClosingDisclosureArchive(authTicket, loanRefNumber, secretKey, dataXml, loanIdIsRefNum: true);
        }

        [WebMethod(Description = "Triggers the creation of a Closing Disclosure Archive for the specified loan file. Returns XML containing the created archive's ID and status.")]
        public string CreateClosingDisclosureArchive(string authTicket, string loanNumber, string dataXml, string secretKey)
        {
            return CreateClosingDisclosureArchive(authTicket, loanNumber, secretKey, dataXml, loanIdIsRefNum: false);
        }

        /// <summary>
        /// Creates a closing disclosure archive as if the "Create Closing Disclosure" button was pressed on the "Internal Support" page.
        /// </summary>
        /// <param name="authTicket">Webservice authentication ticket.</param>
        /// <param name="loanIdentifier">Loan Name (sLNm) or Loan Reference Number (sLRefNm). NOT Loan ID (sLId).</param>
        /// <param name="secretKey">The secret key associated with disclosure archive web services. Checked against ConstStage "WebserviceRecordInitialDisclosureSecretKey".</param>
        /// <param name="dataXml">The data xml provided as parameters for creating the archive.</param>
        /// <param name="loanIdIsRefNum">Whether the <paramref name="loanIdentifier"/> is loan name (false) or reference number (true).</param>
        /// <returns>The web service response string to return to the caller.</returns>
        private static string CreateClosingDisclosureArchive(string authTicket, string loanIdentifier, string secretKey, string dataXml, bool loanIdIsRefNum)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Error;
            AbstractUserPrincipal principal;
            string principalFailureMessage = WebServiceUtilities.TryToGetUserPrincipal(authTicket, null, out principal);
            if (!string.IsNullOrEmpty(principalFailureMessage))
            {
                result.AppendError(principalFailureMessage);
                return result.ToResponse();
            }
            CallVolume.IncreaseAndCheck(principal);

            if (string.IsNullOrEmpty(authTicket))
            {
                result.AppendError("sTicket cannot be empty.");
                return result.ToResponse();
            }
            else if (string.IsNullOrEmpty(loanIdentifier))
            {
                result.AppendError($"{(loanIdIsRefNum ? "loanRefNumber" : "loanNumber")} cannot be empty.");
                return result.ToResponse();
            }
            else if (string.IsNullOrEmpty(secretKey))
            {
                result.AppendError("secretKey cannot be empty.");
                return result.ToResponse();
            }

            Guid? neededKey = ConstStage.WebserviceRecordInitialDisclosureSecretKey;
            Guid key;
            if (!neededKey.HasValue || !Guid.TryParse(secretKey, out key) || neededKey.Value != key)
            {
                result.AppendError("Access denied. Invalid key.");
                return result.ToResponse();
            }

            ClosingCostArchive.E_ClosingCostArchiveStatus statusToSet;
            if (string.IsNullOrEmpty(dataXml))
            {
                statusToSet = ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed;
            }
            else
            {
                XElement parameterXml;
                try
                {
                    parameterXml = XElement.Parse(dataXml);
                }
                catch (XmlException)
                {
                    result.AppendError("dataXml is invalid XML.");
                    return result.ToResponse();
                }
                string archiveStatus = parameterXml.XPathSelectElement("loan/collection[@id=\"DisclosureArchiveMetadata\"][1]/record[1]/field[@id=\"Status\"][1]")?.Value;

                if (!Enum.TryParse(archiveStatus, out statusToSet) || !Enum.IsDefined(typeof(ClosingCostArchive.E_ClosingCostArchiveStatus), statusToSet))
                {
                    statusToSet = ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed;
                }
            }

            Guid loanId = loanIdIsRefNum ? Tools.GetLoanIdByLoanReferenceNumber(principal.BrokerId, loanIdentifier) : Tools.GetLoanIdByLoanName(principal.BrokerId, loanIdentifier);
            result = DocFrameworkMethodContainer.CreateClosingDisclosureArchive(loanId, statusToSet);
            return result.ToResponse();
        }

        /// <summary>
        /// Logs LQB Document Framework request to PaulBunyan.
        /// </summary>
        /// <param name="methodName">Name of Document Framework webmethod to be logged.</param>
        /// <param name="sLNm">LendingQB Loan Number.</param>
        /// <param name="vendorIdString">Unique identifier of LQB Document Framework vendor.</param>
        /// <param name="userName">Username for accessing LQB Document Framework vendor. Generally required by vendor.</param>
        /// <param name="accountId">Account ID for accessing LQB Document Framework vendor. Not all vendors require this.</param>
        /// <param name="additionalParameters">Additoinal parameters to be logged.</param>
        private static void LogDocumentFrameworkRequest(string methodName, string sLNm, string vendorId, string userName, string accountId, params string[] additionalParameters)
        {
            string messageBody = string.Format("{0} - Request{5}LNm: {1}{5}vendorId: {2}{5}userName: {3}{5}accountId: {4}", methodName, sLNm, vendorId, userName, accountId, Environment.NewLine);
            if (additionalParameters != null && additionalParameters.Length > 0)
            {
                messageBody += string.Join(Environment.NewLine, additionalParameters);
            }

            Tools.LogInfo(DocumentFrameworkCategory, messageBody);
        }

        /// <summary>
        /// Logs LQB Document Framework reponse to PaulBunyan.
        /// </summary>
        /// <param name="methodName">Name of Document Framework webmethod to be logged.</param>
        /// <param name="message">Message body containing API user inputs.</param>
        private static void LogDocumentFrameworkResponse(string methodName, string message)
        {
            Tools.LogInfo(DocumentFrameworkCategory, methodName + " - Response" + Environment.NewLine + message);
        }

        [WebMethod(Description = "Convert lead to loan and return a loan number. [sTicket] Ticket returned by AuthService.asmx. [sTemplateNm] - The loan template number used to initialized the lead file. [sLNm] The LendingQB Loan Name.")]
        public string ConvertLeadToLoan(string sTicket, string sLNm, string sXmlData)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();

            if (string.IsNullOrEmpty(sTicket))
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError("[sTicket] cannot be empty.");
            }
            if (string.IsNullOrEmpty(sXmlData))
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError("[sXmlData] cannot be empty.");
            }
            if (string.IsNullOrEmpty(sLNm))
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError("[sLNm] cannot be empty.");
            }
            if (result.Status == ServiceResultStatus.Error)
            {
                return result.ToResponse();
            }

            AbstractUserPrincipal principal = GetPrincipalFromTicket(sTicket, sLNm);
            CallVolume.IncreaseAndCheck(principal);
            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

            if (sLId == Guid.Empty)
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError("Invalid loan.");
                return result.ToResponse();
            }

            BrokerDB broker = principal.BrokerDB;
            bool? removeLeadPrefix = null;
            string sTemplateNm = string.Empty;
            var parameters = new LeadConversionParameters();

            XmlDocument xmlDocument = Tools.CreateXmlDoc(sXmlData); 
                       
            if (xmlDocument == null)
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError("Invalid XML Document");
                return result.ToResponse();
            }

            XmlNodeList list = null;

            list = xmlDocument.SelectNodes("//field");           

            if (list != null && list.Count != 0)
            {
                foreach (XmlElement el in list)
                {
                    string fieldId = el.GetAttribute("id");
                    if (fieldId.Equals("RemoveLeadPrefix", StringComparison.OrdinalIgnoreCase))
                    {
                        bool tmpRemoveLeadPrefix;
                        if (bool.TryParse(el.InnerXml, out tmpRemoveLeadPrefix))
                        {
                            removeLeadPrefix = tmpRemoveLeadPrefix;
                        }
                        else
                        {
                            result.Status = ServiceResultStatus.Error;
                            result.AppendError($"{el.InnerXml} is not a valid input for RemoveLeadPrefix");
                        }
                    }
                    else if (fieldId.Equals("sTemplateNm", StringComparison.OrdinalIgnoreCase))
                    {
                        sTemplateNm = el.InnerXml;
                    }
                    else
                    {
                        result.Status = ServiceResultStatus.Error;
                        result.AppendError($"Field ID {el.InnerXml} is not valid.");
                    }
                }
            }
            else
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError("No fields selected - please refer to the LOXmlFormat schema documentation regarding Loan WebService.");
            }

            if (result.Status == ServiceResultStatus.Error)
            {
                return result.ToResponse();
            }
    
            CPageData dataLoan;

            dataLoan = new CPageData(sLId, new[] { "sSrcsLId", "sLoanFileT", "sStatusT" });
            dataLoan.InitLoad();

            if (broker.IsForceLeadToLoanNewLoanNumber)
            {
                removeLeadPrefix = false;
            }
            else if (broker.IsForceLeadToUseLoanCounter || broker.IsForceLeadToLoanRemoveLeadPrefix)
            {
                removeLeadPrefix = true;
            }

            if (removeLeadPrefix == null)
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError("Field id [RemoveLeadPrefix] is required within [sXmlData].");
                return result.ToResponse();
            }

            if (!Tools.IsStatusLead(dataLoan.sStatusT))
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError($"This lead has already been converted into a loan, with name: {dataLoan.sLNm}.");
                return result.ToResponse();
            }

            bool ConvertFromQp2File = dataLoan.sLoanFileT == E_sLoanFileT.QuickPricer2Sandboxed ? true : false;            

            Guid templateId = Tools.GetLoanIdByLoanName(principal.BrokerId, sTemplateNm);
            bool createFromTemplate;

            if (!broker.IsForceLeadToLoanTemplate && (dataLoan.sSrcsLId == Guid.Empty || broker.QuickPricerTemplateId == Guid.Empty))
            {
                createFromTemplate = false;
            }
            else
            {
                if (templateId == Guid.Empty)
                {
                    result.Status = ServiceResultStatus.Error;
                    result.AppendError("Field id [sTemplateNm] is required within [sXmlData].");
                    return result.ToResponse();
                }
                else
                {
                    createFromTemplate = true;
                }
            }                                  

            parameters.LeadId = sLId;
            parameters.FileVersion = ConstAppDavid.SkipVersionCheck;
            parameters.TemplateId = templateId;
            parameters.CreateFromTemplate = createFromTemplate;
            parameters.RemoveLeadPrefix = removeLeadPrefix.Value;
            parameters.ConvertFromQp2File = ConvertFromQp2File;
            parameters.ConvertToLead = false; 
            parameters.IsEmbeddedPML = true;
            parameters.IsNonQm = false;

            var conversionResults = new LeadConversionResults();

            try
            {
                conversionResults = LeadConversion.ConvertLeadToLoan_Pml(principal, parameters);
            }
            catch (SystemException e)
            {
                Tools.LogError("Error while converting lead to loan through the webservice", e);
                result.Status = ServiceResultStatus.Error;
                result.AppendError(ErrorMessages.UnableToConvertLead);
                return result.ToResponse();
            }
           
            if (!string.IsNullOrEmpty(conversionResults.MersErrors))
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError(conversionResults.MersErrors);
                return result.ToResponse();
            }

            if (conversionResults.ConvertedLoanId == Guid.Empty)
            {
                result.Status = ServiceResultStatus.Error;
                result.AppendError(conversionResults.Message);
                return result.ToResponse();
            }

            if (conversionResults.Message != null)
            {
                result.Status = ServiceResultStatus.OKWithWarning;
                result.AppendWarning(conversionResults.Message);                
            }
            else
            {
                result.Status = ServiceResultStatus.OK;
            }

            result.AddResultElement("LoanNumber", conversionResults.ConvertedLoanNumber);
            return result.ToResponse();            
        }

        /// <summary>
        /// Enumerates the available export formats for the <see cref="Loan.Load(string, string, string, int)"/> web service method.
        /// </summary>
        private enum IntegrationExportFormat
        {
            /// <summary>
            /// LendingQB's own "LOXML" format from <see cref="LOFormatExporter"/>.
            /// </summary>
            LOFile = 0,
            CalyxPoint = 3,
            FannieMae32 = 4,
            Mismo26 = 9,
            Mismo33 = 10,
            LqbDocumentRequest = 11,
            Mismo34 = 12
        }

        /// <summary>
        /// Enumerates the available import formats for the <see cref="Loan.Save(string, string, string, int)"/> web service method.
        /// </summary>
        private enum IntegrationImportFormat
        {
            /// <summary>
            /// LendingQB-proprietary "LOXML" format.
            /// </summary>
            LOFile = 0,
            FannieMae32 = 4,
            Encompass_Mismo23 = 7,
            ULDD = 8
        }
    }

    class CSendEmailCertificateData : CPmlPageData
    {
        private static CSelectStatementProvider s_selectProvider;

        static CSendEmailCertificateData()
        {
            StringList list = new StringList();

            #region Target Fields
            DataAccessUtils.AddCertificateFields(list);

            list.Add("sEmployeeLockDesk");
            list.Add("sEmployeeUnderwriter");
            list.Add("sPmlCertXmlContent");
            list.Add("sLNm");
            list.Add("aBNm_acNm");
            list.Add("sfApplyLoanProductTemplate");
            list.Add("sNoteIR");
            list.Add("sBrokComp1Lckd");
            list.Add("sBrokComp1MldsLckd");
            list.Add("sBrokComp1Desc");
            list.Add("sBrokComp1Pc");
            list.Add("sRAdjMarginR");
            list.Add("sBranchId");

            #endregion

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);

        }

        protected override bool m_enforceAccessControl
        {
            get { return false; }
        }

        public CSendEmailCertificateData(Guid fileId)
            : base(fileId, "CSendEmailCertificateData", s_selectProvider)
        {
        }
    }
}
