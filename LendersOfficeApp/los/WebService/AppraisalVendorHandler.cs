namespace LendersOfficeApp.los.common
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Xml;
    using System.Xml.Serialization;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Conversions.LQBAppraisal;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LQBAppraisal.AppraisalStatusUpdate;

    public class AppraisalVendorHandler : System.Web.IHttpHandler
    {
        private static XmlSerializer appraisalStatusUpdateSerializerWithRoot = new XmlSerializer(typeof(AppraisalStatusUpdate), new XmlRootAttribute() { ElementName = "AppraisalStatusUpdate", IsNullable = true });

        private const string DateFormatString = "dd-MM-yyyy";

        private static readonly Func<AppraiserLicense, LicenseInfo, bool> MatchingLicense = (payloadLicense, appraiserLicense) =>
            string.Equals(payloadLicense.ApprLicenseState, appraiserLicense.State, StringComparison.OrdinalIgnoreCase) &&
            string.Equals(payloadLicense.ApprLicenseNum, appraiserLicense.License, StringComparison.OrdinalIgnoreCase);

        public bool IsReusable
        {
            get { return true; }
        }

        private void SendResponse(HttpResponse response, byte[] responseXml)
        {
            response.ContentType = "text/xml";
            response.ContentEncoding = System.Text.Encoding.UTF8;
            response.Output.Write(Encoding.UTF8.GetChars(responseXml, 0, responseXml.Length));
            response.Flush();
        }

        private void UpdateLoan(CPageData dataLoan, decimal? estimatedValue, DateTime? updateCompletedDate, AppraisalStatusUpdate update, LQBAppraisalOrder orderInfo, bool importAppraiser)
        {
            if (dataLoan.sApprVal == 0 && estimatedValue.HasValue)
            {
                dataLoan.sApprVal = estimatedValue.Value;
            }

            if (updateCompletedDate.HasValue && !dataLoan.sApprRprtRd.IsValid)
            {
                dataLoan.sApprRprtRd_rep = updateCompletedDate.Value.ToShortDateString();
            }

            DateTime submittedToUCDPD;
            if (!dataLoan.sSpSubmittedToUcdpD.IsValid
                && DateTime.TryParseExact(
                update.OrderDataUpdate.OrderFields.DateSubmittedToUCDP
                , DateFormatString
                , null
                , System.Globalization.DateTimeStyles.None
                , out submittedToUCDPD))
            {
                dataLoan.sSpSubmittedToUcdpD_rep = submittedToUCDPD.ToShortDateString();
            }

            DateTime submittedToFhaD;
            if (!dataLoan.sSpSubmittedToFhaD.IsValid
                && DateTime.TryParseExact(
                update.OrderDataUpdate.OrderFields.DateSubmittedToFHA
                , DateFormatString
                , null
                , System.Globalization.DateTimeStyles.None
                , out submittedToFhaD))
            {
                dataLoan.sSpSubmittedToFhaD_rep = submittedToFhaD.ToShortDateString();
            }

            if (importAppraiser)
            {
                ImportAppraiser(dataLoan, update.OrderDataUpdate.AssignedAppraiser);
            }

            if (!string.IsNullOrEmpty(update.OrderDataUpdate.OrderFields.FHADocID))
            {
                dataLoan.sSpFhaDocId = update.OrderDataUpdate.OrderFields.FHADocID;
            }

            if (!string.IsNullOrEmpty(update.OrderDataUpdate.OrderFields.CURiskScore))
            {
                dataLoan.sSpCuScore = orderInfo.CuRiskScore; // The value has already been updated via the previous call to orderInfo.PushUpdate
            }

            if (!string.IsNullOrEmpty(update.OrderDataUpdate.OrderFields.OvervaluationRiskIndicator))
            {
                dataLoan.sSpOvervaluationRiskT = orderInfo.OvervaluationRiskT;
            }

            if (!string.IsNullOrEmpty(update.OrderDataUpdate.OrderFields.PropertyEligibilitiyRiskIndicator))
            {
                dataLoan.sSpPropertyEligibilityRiskT = orderInfo.PropertyEligibilityRiskT;
            }

            if (!string.IsNullOrEmpty(update.OrderDataUpdate.OrderFields.AppraisalQualityRiskIndicator))
            {
                dataLoan.sSpAppraisalQualityRiskT = orderInfo.AppraisalQualityRiskT;
            }

            if (string.IsNullOrEmpty(dataLoan.sSpAppraisalId) && !string.IsNullOrEmpty(update.OrderDataUpdate.OrderFields.UCDPDocID))
            {
                dataLoan.sSpAppraisalId = update.OrderDataUpdate.OrderFields.UCDPDocID;
            }

            if (orderInfo.VendorId != dataLoan.sAppraisalVendorId && !string.IsNullOrEmpty(update.OrderDataUpdate.OrderFields.Status))
            {
                dataLoan.sAppraisalVendorId = orderInfo.VendorId;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            var update = extractAppraisalStatusUpdateMessage(request);

            var response = new LQBAppraisalStatusUpdateResponse();
            byte[] responseXml;

            response.Status = LQBAppraisalStatusUpdateResponse.StatusType.OK;
            if (null == update)
            {
                response.Status = LQBAppraisalStatusUpdateResponse.StatusType.ERROR;
                response.Message = "Could not parse xml.";
                responseXml = response.ToXml();
                SendResponse(context.Response, responseXml);
                return;
            }
            string orderNumber = update.TransactionInfo.OrderNumber;
            
            // Use a stored procedure with a FileNumber -> Loan GUID mapping
            // FileNumber -> sLId, brokerId, employeeId, userId
            Guid sLId = Guid.Empty;
            try
            {
                sLId = new Guid(update.TransactionInfo.LoanID);
            }
            catch (FormatException)
            {
                response.Status = LQBAppraisalStatusUpdateResponse.StatusType.ERROR;
                response.Message = "Invalid LoanID (" + update.TransactionInfo.LoanID + ").";
                responseXml = response.ToXml();
                SendResponse(context.Response, responseXml);
                LogStatusUpdateInfo(update, responseXml);
                return;
            }

            LQBAppraisalOrder orderInfo = null;

            try
            {
                orderInfo = LQBAppraisalOrder.Load(sLId.ToString(), orderNumber);
            }
            catch (LoanNotFoundException)
            {
                // dd 11/9/2018 - Temporary forward the not found loan id to the secondary site.
                if (!string.IsNullOrEmpty(ConstStage.AsyncPostbackForwardingHostname))
                {
                    RequestHelper.Forwarding(ConstStage.AsyncPostbackForwardingHostname, context);
                    return;
                }
            }

            if (orderInfo == null)
            {
                response.Status = LQBAppraisalStatusUpdateResponse.StatusType.ERROR;
                response.Message = "Could not find order number (" + orderNumber + ").";
                responseXml = response.ToXml();
                SendResponse(context.Response, responseXml);
                LogStatusUpdateInfo(update, responseXml);
                return;
            }

            try
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(AppraisalVendorHandler));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                bool importAppraiser = false;
                if (string.IsNullOrEmpty(orderInfo.OrderDataUpdate.OrderFields.DateCompleted))
                {
                    // If this is the first "completed" update, import the appraiser agent so long
                    // as at least one valid license was supplied.
                    importAppraiser = !string.IsNullOrEmpty(update.OrderDataUpdate.OrderFields.DateCompleted) &&
                        update.OrderDataUpdate.AssignedAppraiser.AppraiserLicenseList.Count > 0;
                }

                DateTime completeD;
                bool completedDValid = DateTime.TryParseExact(update.OrderDataUpdate.OrderFields.DateCompleted, DateFormatString, null, System.Globalization.DateTimeStyles.None, out completeD);
                if (completedDValid)
                {
                    orderInfo.ReceivedDate_rep = completeD.ToShortDateString();
                }

                orderInfo.PushUpdate(update.OrderDataUpdate); //Merge with existing OrderDataUpdate
                foreach (var doc in update.UploadedDocumentList)
                {
                    var appraisalDoc = orderInfo.AddDocument(doc);
                    if (!string.IsNullOrEmpty(appraisalDoc.FileDBKey))
                    {
                        Guid? eDocId = null;
                        if (appraisalDoc.DocumentFormat == E_UploadedDocumentDocumentFormat.PDF)
                        {
                            eDocId = AutoSaveDocTypeFactory.SavePdfDocAsSystem(E_AutoSavePage.AppraisalDocs, appraisalDoc.FileDBKey,
                                E_FileDB.Normal, dataLoan.sBrokerId, dataLoan.sLId, dataLoan.GetAppData(0).aAppId, appraisalDoc.DocumentName);
                        }
                        else if (appraisalDoc.DocumentFormat == E_UploadedDocumentDocumentFormat.XML)
                        {
                            eDocId = AutoSaveDocTypeFactory.SaveXmlDocAsSystem(E_AutoSavePage.AppraisalDocs, appraisalDoc.FileDBKey,
                                E_FileDB.Normal, dataLoan.sBrokerId, dataLoan.sLId, dataLoan.GetAppData(0).aAppId, appraisalDoc.DocumentName);

                            PopulateFromXml(appraisalDoc.FileDBKey, orderInfo, dataLoan);
                        }

                        if (eDocId == Guid.Empty)
                        {
                            eDocId = null;
                        }

                        appraisalDoc.SetEdocId(eDocId);
                    }
                }

                orderInfo.Save();

                var estimatedValue = update.OrderDataUpdate.OrderFields.EstimatedValue.Replace("$", "").ToNullable<decimal>(decimal.TryParse);
                var linkedReo = this.GetLinkedReo(orderInfo, dataLoan);
                if (orderInfo.HasLinkedReo)
                {
                    // Appraisals for REOs don't update anything except for the order and the linked REO.
                    if (estimatedValue.HasValue && linkedReo?.Val == 0)
                    {
                        linkedReo.Val = estimatedValue.Value;
                        dataLoan.Save();
                    }

                    return;
                }

                this.UpdateLoan(dataLoan, estimatedValue, completedDValid ? completeD : (DateTime?)null, update, orderInfo, importAppraiser);
                dataLoan.Save();
            }
            catch (SqlException e)
            {
                response.Status = LQBAppraisalStatusUpdateResponse.StatusType.ERROR;
                response.Message = "Error processing update data.";
                Tools.LogError(e);
            }
            catch (CBaseException e)
            {
                response.Status = LQBAppraisalStatusUpdateResponse.StatusType.ERROR;
                response.Message = "Error processing update data.";
                Tools.LogError(e);
            }
            catch (XmlException e)
            {
                response.Status = LQBAppraisalStatusUpdateResponse.StatusType.ERROR;
                response.Message = "Error parsing UploadedDocument";
                Tools.LogError(e);
            }
            catch (Exception e)
            {
                response.Status = LQBAppraisalStatusUpdateResponse.StatusType.ERROR;
                response.Message = e.Message;
                Tools.LogError(e);
                throw;
            }
            finally
            {
                responseXml = response.ToXml();
                SendResponse(context.Response, responseXml);
                LogStatusUpdateInfo(update, responseXml);
            }
        }

        private IRealEstateOwned GetLinkedReo(LQBAppraisalOrder orderInfo, CPageData dataLoan)
        {
            if (!orderInfo.HasLinkedReo || !orderInfo.AppIdAssociatedWithProperty.HasValue)
            {
                return null;
            }

            CAppData associatedApp = dataLoan.GetAppData(orderInfo.AppIdAssociatedWithProperty.Value);
            if (associatedApp == null)
            {
                return null;
            }

            return associatedApp.aReCollection.GetRecordOf(orderInfo.LinkedReoId.Value) as IRealEstateOwned;
        }
        private void PopulateFromXml(string fileDbKey, LQBAppraisalOrder orderInfo, CPageData dataLoan)
        {
            byte[] fileContent = FileDBTools.ReadData(E_FileDB.Normal, fileDbKey);
            string xml = Encoding.UTF8.GetString(fileContent).TrimWhitespaceAndBOM();// 3/11/14 tj - Eliminate byte order mark (BOM)
            dataLoan.PopulateDataFromAppraisalXml(xml, orderInfo);

            // 10/7/2013 gf - opm 115878 populate more fields from UAD Appraisal XML,
            // moved to PopulateDataFromAppraisalXml called above. Needed same 
            // functionality for GDMS.

            /*
            XDocument root = XDocument.Parse(xml);

            IEnumerable eval;
            XAttribute attr;
            eval = (IEnumerable)root.XPathEvaluate("//VALUATION_RESPONSE/VALUATION/@AppraisalEffectiveDate");
            attr = eval.Cast<XAttribute>().FirstOrDefault();
            if (attr != null && string.IsNullOrEmpty(dataLoan.sSpValuationEffectiveD_rep))
            {
                dataLoan.sSpValuationEffectiveD_rep = attr.Value;
            }

            eval = (IEnumerable)root.XPathEvaluate("//VALUATION_RESPONSE/PROPERTY/STRUCTURE/@LivingUnitCount");
            attr = eval.Cast<XAttribute>().FirstOrDefault();
            if (attr != null && dataLoan.sUnitsNum_rep == "0")
            {
                dataLoan.sUnitsNum_rep = attr.Value;
            }
            
            eval = (IEnumerable)root.XPathEvaluate("//VALUATION_RESPONSE/PROPERTY/STRUCTURE/_UNIT_GROUP[@UnitType=\"UnitOne\"]/@TotalBedroomCount");
            attr = eval.Cast<XAttribute>().FirstOrDefault();
            if (attr != null && dataLoan.sSpUnit1BedroomsCount_rep == "0")
            {
                dataLoan.sSpUnit1BedroomsCount_rep = attr.Value;
            }
            
            eval = (IEnumerable)root.XPathEvaluate("//VALUATION_RESPONSE/PROPERTY/STRUCTURE/_UNIT_GROUP[@UnitType=\"UnitTwo\"]/@TotalBedroomCount");
            attr = eval.Cast<XAttribute>().FirstOrDefault();
            if (attr != null && dataLoan.sSpUnit2BedroomsCount_rep == "0")
            {
                dataLoan.sSpUnit2BedroomsCount_rep = attr.Value;
            }
            
            eval = (IEnumerable)root.XPathEvaluate("//VALUATION_RESPONSE/PROPERTY/STRUCTURE/_UNIT_GROUP[@UnitType=\"UnitThree\"]/@TotalBedroomCount");
            attr = eval.Cast<XAttribute>().FirstOrDefault();
            if (attr != null && dataLoan.sSpUnit3BedroomsCount_rep == "0")
            {
                dataLoan.sSpUnit3BedroomsCount_rep = attr.Value;
            }
            
            eval = (IEnumerable)root.XPathEvaluate("//VALUATION_RESPONSE/PROPERTY/STRUCTURE/_UNIT_GROUP[@UnitType=\"UnitFour\"]/@TotalBedroomCount");
            attr = eval.Cast<XAttribute>().FirstOrDefault();
            if (attr != null && dataLoan.sSpUnit4BedroomsCount_rep == "0")
            {
                dataLoan.sSpUnit4BedroomsCount_rep = attr.Value;
            }
            */
    }

    private AppraisalStatusUpdate extractAppraisalStatusUpdateMessage(HttpRequest request)
        {
            string requestXml;

            // dd 10-21-2018 - Need to make a copy of InputStream.
            // We may need InputStream to be available if we need to forward to another server.
            using (MemoryStream stream = new MemoryStream())
            {
                request.InputStream.CopyTo(stream);
                stream.Seek(0, SeekOrigin.Begin);

                using (var reader = new StreamReader(stream, request.ContentEncoding))
                {
                    requestXml = reader.ReadToEnd();
                }

            }

            AppraisalStatusUpdate update = new AppraisalStatusUpdate();

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.XmlResolver = null;
#if LQB_NET45
            xmlReaderSettings.DtdProcessing = DtdProcessing.Parse;
#else
            xmlReaderSettings.ProhibitDtd = false;
#endif
            try
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(requestXml), xmlReaderSettings))
                {
                    update = FromAppraisalStatusUpdateXml(reader);
                }
            }
            catch (InvalidOperationException) // Could not read the XML
            {
                Tools.LogErrorWithCriticalTracking(
                    string.Format(
                        @"Invalid AppraisalStatusUpdate from {0}:{1}. Got {2}",
                        request.UserHostName,
                        request.UserHostAddress,
                        requestXml
                    )
                );
                update = null;
            }
            string parsingMessage = (update == null ? Environment.NewLine + "Parsing failed for this xml" : string.Empty);
            try
            {
                requestXml = System.Text.RegularExpressions.Regex.Replace(requestXml, @"<UploadedDocument( [^>]*)>[^<]+</UploadedDocument>", @"<UploadedDocument$1>[Base64FileData]</UploadedDocument>");
                Tools.LogInfo("AppraisalVendorHandler - RawXML received:"+ Environment.NewLine + requestXml + parsingMessage );
            }
            catch (OutOfMemoryException exc)
            {
                Tools.LogError("AppraisalVendorHandler - unable to log RawXML:" + parsingMessage, exc);
            }
            return update;
        }
        private AppraisalStatusUpdate FromAppraisalStatusUpdateXml(XmlReader reader)
        {
            XmlSerializer deserializer;

            if (ConstStage.UseXmlSerializerWithNoXmlRoot)
            {
                deserializer = new XmlSerializer(typeof(AppraisalStatusUpdate));
            }
            else
            {
                deserializer = appraisalStatusUpdateSerializerWithRoot;
            }

            var response = (AppraisalStatusUpdate)deserializer.Deserialize(reader);

            return response;
        }

        private void ImportAppraiser(CPageData dataLoan, AssignedAppraiser appr)
        {
            var existingAppraisers = dataLoan.GetAgentsOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (dataLoan.sDocsD.IsValid && existingAppraisers.Any(existingAppraiser => existingAppraiser.IsValid))
            {
                // To maintain consistency between generated docs and the
                // appraiser agent(s) on file, do not add or update an agent
                // when docs have been sent out.
                return;
            }

            // Get the latest license from the payload so that
            // if multiple licenses are supplied, we use the 
            // most current information.
            var latestLicense = appr.AppraiserLicenseList.First();
            var latestLicenseExpirationDate = DateTime.ParseExact(latestLicense.ApprLicenseExpDate, DateFormatString, null); // Converts 16-4-2017 into 4/16/2017, which is valid to parse.

            foreach (var license in appr.AppraiserLicenseList)
            {
                var expirationDate = DateTime.ParseExact(license.ApprLicenseExpDate, DateFormatString, null);

                if (expirationDate > latestLicenseExpirationDate)
                {
                    latestLicense = license;
                    latestLicenseExpirationDate = expirationDate;
                }
            }

            CAgentFields appraiserAgentForUpdate = this.GetAppraiserAgentFromLicense(
                existingAppraisers, 
                latestLicense, 
                latestLicenseExpirationDate);

            // If we're unable to get the appraiser agent from the 
            // latest license, we may be in a situation where an appraiser
            // agent does exist on the loan with one of the older licenses
            // provided in the payload. In that case, we'll want to add the
            // new license to the agent. 
            if (appraiserAgentForUpdate == null)
            {
                appraiserAgentForUpdate = this.GetAppraiserAgentFromLicenseList(
                    existingAppraisers,
                    appr.AppraiserLicenseList);
            }

            // If we are still unable to obtain the appraiser agent, we'll
            // create a new appraiser contact. Otherwise, we will update the
            // agent's license information based on what was sent in the 
            // payload.
            if (appraiserAgentForUpdate == null)
            {
                appraiserAgentForUpdate = dataLoan.GetAgentFields(-1);

                appraiserAgentForUpdate.AgentRoleT = E_AgentRoleT.Appraiser;

                appraiserAgentForUpdate.LicenseNumOfAgent = latestLicense.ApprLicenseNum;

                foreach (var lic in appr.AppraiserLicenseList)
                {
                    string apprLicenseExpDate = DateTime.ParseExact(lic.ApprLicenseExpDate, DateFormatString, null).ToShortDateString();

                    appraiserAgentForUpdate.LicenseInfoList.Add(new LicenseInfo(lic.ApprLicenseState, apprLicenseExpDate, lic.ApprLicenseNum));
                }
            }
            else
            {
                foreach (var payloadLicense in appr.AppraiserLicenseList)
                {
                    var payloadLicenseDate = DateTime.ParseExact(payloadLicense.ApprLicenseExpDate, DateFormatString, null).ToShortDateString();

                    var licenseMatch = (from LicenseInfo appraiserLicense in appraiserAgentForUpdate.LicenseInfoList.List
                                       where MatchingLicense(payloadLicense, appraiserLicense)
                                       select appraiserLicense).FirstOrDefault();

                    if (licenseMatch == null)
                    {
                        appraiserAgentForUpdate.LicenseInfoList.Add(new LicenseInfo(
                            payloadLicense.ApprLicenseState,
                            payloadLicenseDate,
                            payloadLicense.ApprLicenseNum));
                    }
                    else
                    {
                        licenseMatch.ExpD = payloadLicenseDate;
                    }
                }
            }
            
            appraiserAgentForUpdate.AgentName = appr.AppraiserInfo.ApprName;
            appraiserAgentForUpdate.CompanyName = appr.AppraiserCompanyInfo.ApprCompanyName;
            appraiserAgentForUpdate.StreetAddr = appr.AppraiserCompanyInfo.ApprAddress;
            appraiserAgentForUpdate.City = appr.AppraiserCompanyInfo.ApprCity;
            appraiserAgentForUpdate.State = appr.AppraiserCompanyInfo.ApprState;
            appraiserAgentForUpdate.Zip = appr.AppraiserCompanyInfo.ApprZIP;
            appraiserAgentForUpdate.Phone = appr.AppraiserInfo.ApprPhone;
            appraiserAgentForUpdate.PhoneOfCompany = appr.AppraiserCompanyInfo.ApprCompanyPhone;
            appraiserAgentForUpdate.FaxNum = appr.AppraiserInfo.ApprFax;
            appraiserAgentForUpdate.FaxOfCompany = appr.AppraiserCompanyInfo.ApprCompanyFax;
            appraiserAgentForUpdate.EmailAddr = appr.AppraiserInfo.ApprEmail;
            
            appraiserAgentForUpdate.Update();
        }

        private CAgentFields GetAppraiserAgentFromLicense(
            List<CAgentFields> existingAppraisers, 
            AppraiserLicense license,
            DateTime licenseExpirationDate)
        {
            foreach (var existingAppraiser in existingAppraisers)
            {
                if (!existingAppraiser.IsValid)
                {
                    continue;
                }

                foreach (LicenseInfo licenseInfo in existingAppraiser.LicenseInfoList)
                {
                    if (MatchingLicense(license, licenseInfo))
                    {
                        return existingAppraiser;
                    }
                }
            }

            return null;
        }

        private CAgentFields GetAppraiserAgentFromLicenseList(
            List<CAgentFields> existingAppraisers, 
            List<AppraiserLicense> appraiserLicenseList)
        {
            foreach (var license in appraiserLicenseList)
            {
                var licenseExpirationDate = DateTime.ParseExact(license.ApprLicenseExpDate, DateFormatString, null);

                var foundAppraiser = this.GetAppraiserAgentFromLicense(
                    existingAppraisers,
                    license,
                    licenseExpirationDate);

                if (foundAppraiser != null)
                {
                    return foundAppraiser;
                }
            }

            return null;
        }

        private void LogStatusUpdateInfo(AppraisalStatusUpdate update, byte[] responseXml)
        {
            StringBuilder log = new StringBuilder();
            log.AppendLine("AppraisalVendorHandler - Received AppraisalStatusUpdate:");
            using (StringWriter textWriter = new StringWriter())
            using (XmlTextWriter xmlWriter = new XmlTextWriter(textWriter))
            {
                update.WriteXml(xmlWriter);
                log.AppendLine(textWriter.ToString());
            }
            log.AppendLine();
            log.AppendLine("Sent LQBAppraisalStatusUpdateResponse:");
            log.AppendLine(Encoding.Default.GetString(responseXml));
            Tools.LogInfo(log.ToString());
        }
    }
}