namespace LendersOfficeApp.WebService
{
    using System;
    using System.ComponentModel;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Web.Services;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using System.Collections.Generic;

    [WebService(Namespace="http://www.lendersoffice.com/los/webservices/")]
	public class PmlUser : System.Web.Services.WebService
	{
		public PmlUser()
		{
            InitializeComponent(); //CODEGEN: This call is required by the ASP.NET Web Services Designer
		}

        public static string GenerateErrorResponseXml(string errMsg)
        {
            return (new XElement(
                "result",
                new XAttribute("status", "Error"),
                new XElement("error", errMsg))).ToString(SaveOptions.DisableFormatting);
        }

		/// <summary>
		/// Update a PML User
		/// </summary>
		[WebMethod(Description="Update a PriceMyLoan user and return an XML response indicating the success of the operation. [sTicket] - The authentication ticket. [xmlDoc] - The XML document containing the user data.  [userName] - The login name of the PriceMyLoan user to update.")]
		public string UpdateUser(string sTicket, string xmlDoc, string userName) 
		{
			LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(2000); //delay 2 seconds to decrease the rate of traffic
            AbstractUserPrincipal principal = null;
			try
			{
				principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);
			}
			catch(CBaseException a)
			{
                return GenerateErrorResponseXml(a.UserMessage);
			}	

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            if (WebServiceUtilities.CanAdministratePmlUsers(principal) == false)
            {
                GenerateErrorResponseXml("You do not have permission to update PML users");
            }

			XmlDocument userXml = null;
			try
			{
				userXml = Tools.CreateXmlDoc(xmlDoc);
			}
            catch (System.Xml.XmlException x)
            {
                Tools.LogError("Error creating XML document in UpdateUser: " + x);
                return GenerateErrorResponseXml("Invalid XML Document: " + x.Message);
            }
            catch (Exception e)
            {
                Tools.LogError("Error creating XML document in UpdateUser: " + e);
                return GenerateErrorResponseXml("Invalid XML Document: " + e.Message);
            }

            if (null == userXml)
            {
                return GenerateErrorResponseXml("Invalid XML Document");
            }

			XmlNodeList list = null;
			try
			{
				list = userXml.SelectNodes("//field") ;
			}
            catch (Exception e)
            {
                Tools.LogError("Error while selecting fields from xml document in UpdateUser: " + e);
                return GenerateErrorResponseXml("XML Document does not have the correct structure - please refer to the data dictionary for updating PriceMyLoan users");
            }
			PmlUserRecord pmlUser = new PmlUserRecord();
			Type myType = typeof(PmlUserRecord);

            if (null != list && list.Count != 0)
            {
                foreach (XmlElement el in list)
                {
                    string fieldId = el.GetAttribute("id");
                    if (fieldId.Equals("PasswordExpirationOption"))
                    {
                        if (el.GetAttribute("date") != null)
                        {
                            pmlUser.PasswordExpirationValue = el.GetAttribute("date").Replace("'", "").Replace("\"", "");
                        }
                    }
                    else if (fieldId.Equals("LendingLicense"))
                    {
                        //we'll parse these out and handle them later, for now just save them
                        pmlUser.lendingLicenseXmlElements.Add(el);
                        continue;
                    }
                    else if (fieldId.Equals("PMLNavigation"))
                    {
                        //we'll parse these out and handle them later, for now just save them
                        pmlUser.pmlNavigationXmlElements.Add(el);
                        continue;
                    }

                    FieldInfo myFieldInfo = myType.GetField(fieldId);
                    if (myFieldInfo != null)
                    {
                        string v = el.InnerText.TrimWhitespaceAndBOM();

                        if (fieldId.Equals("LoginName", StringComparison.OrdinalIgnoreCase))
                        {
                            // 9/24/2013 dd - Don't Allow single quote or double quote in login name.
                            v = v.Replace("'", "").Replace("\"", "");
                        }
                        myFieldInfo.SetValue(pmlUser, v);
                    }
                }
            }
            else
            {
                return GenerateErrorResponseXml("No fields selected for updating - please refer to the LOXmlFormat schema and the data dictionary for updating PriceMyLoan users");
            }

            if (principal.Type == "P")
            {
                return pmlUser.UpdateByPMLSupervisor(userName);
            }
            else
            {
                return pmlUser.Update(userName);
            }
		}

		/// <summary>
		/// Create new PML User
		/// </summary>
		[WebMethod(Description="Create a new PriceMyLoan user and return an XML response indicating the success of the operation. [sTicket] - The authentication ticket. [xmlDoc] - The XML document containing the user data.")]
		public string CreateUser(string sTicket, string xmlDoc) 
		{
			LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(2000); //delay 2 seconds to decrease the rate of traffic

            AbstractUserPrincipal principal = null;
			try
			{
				principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);
			}
			catch(CBaseException a)
			{
                return GenerateErrorResponseXml(a.UserMessage);
			}

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            if(WebServiceUtilities.CanAdministratePmlUsers(principal) == false)
            {
                return GenerateErrorResponseXml("You do not have permission to create PML users");
            }

			XmlDocument userXml = null;
			try
			{
				userXml = Tools.CreateXmlDoc(xmlDoc);
			}
			catch(System.Xml.XmlException x)
			{
				Tools.LogError("Error creating XML document in CreateUser: " + x);
                return GenerateErrorResponseXml("Invalid XML Document: " + x.Message);
			}
			catch(Exception e)
			{
				Tools.LogError("Error creating XML document in CreateUser: " + e);
                return GenerateErrorResponseXml("Invalid XML Document: " + e.Message);
			}

            if (null == userXml)
            {
                return GenerateErrorResponseXml("Invalid XML Document");
            }
			
			XmlNodeList list = null;
			try
			{
				list = userXml.SelectNodes("//field") ;
			}
			catch(Exception e)
			{
				Tools.LogError("Error while selecting fields from xml document in UpdateUser: " + e);
                return GenerateErrorResponseXml("XML Document does not have the correct structure - please refer to the data dictionary for creating PriceMyLoan users");
			}
				
			PmlUserRecord pmlUser = new PmlUserRecord();
			Type myType = typeof(PmlUserRecord);
		
			if(null != list)
			{
				foreach (XmlElement el in list) 
				{
					string fieldId = el.GetAttribute("id");
					if(fieldId.Equals("PasswordExpirationOption"))
					{
                        if (el.GetAttribute("date") != null)
                        {
                            pmlUser.PasswordExpirationValue = el.GetAttribute("date").TrimWhitespaceAndBOM().Replace("'", "").Replace("\"", "");
                        }
					}
					else if (fieldId.Equals("LendingLicense"))
					{
						//we'll parse these out and handle them later, for now just save them
						pmlUser.lendingLicenseXmlElements.Add(el);
						continue;
					}
                    else if (fieldId.Equals("PMLNavigation"))
                    {
                        //we'll parse these out and handle them later, for now just save them
                        pmlUser.pmlNavigationXmlElements.Add(el);
                        continue;
                    }
					
					FieldInfo myFieldInfo = myType.GetField(fieldId);
                    if (myFieldInfo != null)
                    {
                        myFieldInfo.SetValue(pmlUser, el.InnerText.TrimWhitespaceAndBOM().Replace("'", "").Replace("\"", ""));
                    }
				}
			}

            if (principal.Type == "P")
            {
                return pmlUser.CreateByPMLSupervisor();
            }
            else
            {
                return pmlUser.Create();
            }
		}

        // OPM 32445
        [WebMethod(Description = "This method returns a CSV string containing the contact information for PML users.")]
        public string ExportRelationshipInformationOfPmlUser(string sTicket, string userName)
        {
            LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(2000); //delay 2 seconds to decrease the rate of traffic
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            if (WebServiceUtilities.CanAdministratePmlUsers(principal) == false)
            {
                return GenerateErrorResponseXml("You do not have permission to export information of PML users");
            }

            LendersOfficeApp.los.BrokerAdmin.ContactInfoExport contactInfo = new LendersOfficeApp.los.BrokerAdmin.ContactInfoExport();

            try
            {
                return PmlBroker.CreatePMLRelationshipInfoCSV(userName);
            }
            catch (Exception e)
            {
                Tools.LogError(ErrorMessages.GenericFailureToExportDataToCSV + " (Error occurred while exporting PML user relationship info in PmlUser webservice)", e);
                return GenerateErrorResponseXml(ErrorMessages.GenericFailureToExportDataToCSV);
            }
        }

        // OPM 48063
        [WebMethod(Description = "This method returns a CSV string containing the contact information for the requested PML user.")]
        public string ExportPMLUserInformation(string sTicket, string pmlUserLoginName)
        {
            LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(2000); //delay 2 seconds to decrease the rate of traffic
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            if (WebServiceUtilities.CanAdministratePmlUsers(principal) == false)
            {
                return GenerateErrorResponseXml("You do not have permission to export information of PML users");
            }

            LendersOfficeApp.los.BrokerAdmin.ContactInfoExport contactInfo = new LendersOfficeApp.los.BrokerAdmin.ContactInfoExport();

            try
            {
                string csv = "";
                if (principal.Type == "P")
                {
                    csv = PmlBroker.CreateContactInfoCSVForPmlSupervisor(principal.BrokerId, principal.EmployeeId, pmlUserLoginName, Guid.Empty);
                }
                else
                {
                    csv = PmlBroker.CreateContactInfoCSV(principal, "P", pmlUserLoginName, Guid.Empty);
                }

                Regex r = new Regex("\n", RegexOptions.Multiline);
                MatchCollection mc = r.Matches(csv);

                if (mc.Count <= 1)
                {
                    throw new CBaseException(ErrorMessages.GenericFailureToExportDataToCSV + ": Login name does not exist.", ErrorMessages.GenericFailureToExportDataToCSV + ": Login name does not exist.");
                }

                return csv;
            }
            catch (CBaseException cb)
            {
                return GenerateErrorResponseXml(cb.UserMessage);
            }
            catch (Exception e)
            {
                Tools.LogError(ErrorMessages.GenericFailureToExportDataToCSV + " (Error occurred while exporting PML user info in PmlUser webservice)", e);
                return GenerateErrorResponseXml(ErrorMessages.GenericFailureToExportDataToCSV);
            }
        }
		
        // OPM 28858
        [WebMethod(Description = "This method returns a CSV string containing the contact information for PML users.")]
        public string ExportContactInformationOfAllPmlUsers(string sTicket)
        {
            LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(2000); //delay 2 seconds to decrease the rate of traffic
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (null == principal)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            if (WebServiceUtilities.CanAdministratePmlUsers(principal) == false)
            {
                return GenerateErrorResponseXml("You do not have permission to export information of PML users");
            }

            LendersOfficeApp.los.BrokerAdmin.ContactInfoExport contactInfo = new LendersOfficeApp.los.BrokerAdmin.ContactInfoExport();

            try
            {
                if (principal.Type == "P")
                {
                    return PmlBroker.CreateContactInfoCSVForPmlSupervisor(principal.BrokerId, principal.EmployeeId);
                }
                else
                {
                    return PmlBroker.CreateContactInfoCSV(principal, "P");
                }
            }
            catch (Exception e)
            {
                Tools.LogError(ErrorMessages.GenericFailureToExportDataToCSV + " (Error occurred while exporting PML users relationship info in PmlUser webservice)", e);
                return GenerateErrorResponseXml(ErrorMessages.GenericFailureToExportDataToCSV);
            }
        }

        [WebMethod(Description = "This method allows users to change their personal password without first creating an auth ticket (for example, if your password has expired)")]
        public string ChangePasswordWithoutAuthTicket(string customerCode, string userName, string oldPassword, string newPassword)
        {
            try
            {
                AuthServiceHelper.AuthPmlUser(userName, oldPassword, customerCode, false);
                return ChangePasswordImpl(PrincipalFactory.CurrentPrincipal, newPassword);
            }
            catch(CBaseException a)
            {
                return GenerateErrorResponseXml(a.UserMessage);
            }

        }

        // 10-7-08 OPM 22908 jk - Allow PML users to change their password even if they are not admin or have permission to change
        // passwords. It also works when the password is expired. It only checks if the password is strong if the user is Citi.
        // In other cases the check for a strong password is bypassed and it just changes the password.
        // Returns an xml response designating success or indicating what the error was.
        [WebMethod(Description="This method allows users to change their personal password")]
        public string ChangePassword(string sTicket, string password)
        {
            BrokerUserPrincipal principal = null;

            try
            {
                principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null) as BrokerUserPrincipal;
            }
            catch(PasswordExpiredException exc)
            {
                principal = exc.Principal as BrokerUserPrincipal; //This should work even if the password is expired.
            }
            catch(CBaseException a)
            {
                return GenerateErrorResponseXml(a.UserMessage);
            }

            return this.ChangePasswordImpl(principal, password);
        }

        private string ChangePasswordImpl(AbstractUserPrincipal principal, string password)
        {
            if (principal == null)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            //Load current employee with all their information
            EmployeeDB employee = new EmployeeDB(principal.EmployeeId, principal.BrokerId);

            if (!employee.Retrieve())
            {
                return GenerateErrorResponseXml("Unable to retrieve user");
            }

            if (password == "")
            {
                return GenerateErrorResponseXml("Password cannot be empty");
            }

            StrongPasswordStatus ret = employee.IsStrongPassword(password);

            //If the password isn't strong, return with a message describing why it isn't strong
            switch (ret)
            {
                case StrongPasswordStatus.PasswordContainsLogin:
                    return GenerateErrorResponseXml(ErrorMessages.PwCannotContainLoginOrName);

                case StrongPasswordStatus.PasswordMustBeAlphaNumeric:
                    return GenerateErrorResponseXml(ErrorMessages.PasswordMustBeAlphaNumeric);

                case StrongPasswordStatus.PasswordTooShort:
                    return GenerateErrorResponseXml(ErrorMessages.PasswordTooShort);

                case StrongPasswordStatus.PasswordRecycle:
                    return GenerateErrorResponseXml(ErrorMessages.PasswordsShouldNotBeRecycled);

                case StrongPasswordStatus.PasswordContainsConsecutiveIdenticalChars:
                    return GenerateErrorResponseXml(ErrorMessages.PasswordContainsConsecIdenticalChars);

                case StrongPasswordStatus.PasswordContainsConsecutiveAlphanumericChars:
                    return GenerateErrorResponseXml(ErrorMessages.PasswordContainsConsecCharsInAlphaOrNumericOrder);

                case StrongPasswordStatus.PasswordHasUnverifiedCharacters:
                    return GenerateErrorResponseXml(ErrorMessages.PasswordHasUnverifiedCharacters);
            }

            if (ret != StrongPasswordStatus.OK)
            {
                Tools.LogBug("In PmlUser webservice ChangePassword function, StrongPasswordStatus is not 'OK', but all known invalid password categories have been checked.  It's possible that a new one was added and we are not handling it here yet.");
                return GenerateErrorResponseXml("Failed to change password");
            }

            employee.Password = password;
            employee.ResetPasswordExpirationD();
            employee.Save(principal);

            return "<result status=\"OK\"/>";
        }


        #region Component Designer generated code

        //Required by the Web Services Designer 
        private IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion
	}
}
