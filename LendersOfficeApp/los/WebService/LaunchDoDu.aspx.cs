﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.DU;

    // // 5/15/2012 dd - Inherit from System.Web.UI.Page because this page can access through
    // other browser beside IE.
    public partial class LaunchDoDu : MinimalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = RequestHelper.GetSafeQueryString("id");

            string content = AutoExpiredTextCache.GetFromCache(id);

            if (string.IsNullOrWhiteSpace(content))
            {
                throw this.GenerateInvalidContentException(id, content);
            }

            string[] parts = content.Split('\n');

            if (parts.Length != 4)
            {
                throw this.GenerateInvalidContentException(id, content);
            }

            string url = parts[0];
            string userName = parts[1];
            string password = parts[2];
            string sDuCaseId = parts[3];

            Form.Action = url;
            Form.Enctype = "multipart/form-data";

            XisRequestGroup requestGroup = new XisRequestGroup();
            requestGroup.Request.LoginAccountIdentifier = userName;
            requestGroup.Request.LoginAccountPassword = password;
            requestGroup.Request.RequestDateTime = DateTime.Now.ToString();
            requestGroup.Request.RequestData = new XisRequestData();
            requestGroup.Request.RequestData.FnmProduct = new XisFnmProduct();
            requestGroup.Request.RequestData.FnmProduct.FunctionName = "Submit";
            requestGroup.Request.RequestData.FnmProduct.Name = "DWEB";
            requestGroup.Request.RequestData.FnmProduct.VersionNumber = "2.0";
            requestGroup.Request.RequestData.FnmProduct.Connection = null;

            XisDwebSubmitRequest dwebSubmitRequest = new XisDwebSubmitRequest();
            dwebSubmitRequest.MornetPlusCasefileIdentifier = sDuCaseId;
            dwebSubmitRequest.SoftwareProvider = new XisSoftwareProvider();
            dwebSubmitRequest.SoftwareProvider.AccountNumber = ConstAppDavid.FannieMae_SoftwareProviderCode;
            dwebSubmitRequest.Options = new XisOptions();
            dwebSubmitRequest.Options.DownloadEnable = "Yes";
            XisCasefileInstitution casefileInstitution = new XisCasefileInstitution();
            dwebSubmitRequest.AddCasefileInstitution(casefileInstitution);


            ClientScript.RegisterHiddenField("RI", GenerateFormValue(requestGroup));
            ClientScript.RegisterHiddenField("CI", GenerateFormValue(dwebSubmitRequest));

        }

        private CBaseException GenerateInvalidContentException(
            string id, 
            string content)
        {
            var devMsg = string.Format(
                "Invalid entry received from cache using ID '{0}' during DO/DU page launch: '{1}'",
                id,
                content);

            var exc = new CBaseException(
                ErrorMessages.WebServices.DoDuSessionInvalidExpired,
                devMsg);

            exc.IsEmailDeveloper = false;

            return exc;
        }

        private string GenerateFormValue(AbstractXisXmlNode node)
        {
            StringBuilder sb = new StringBuilder(500);
            using (StringWriter stream = new StringWriter(sb))
            {
                XmlWriter writer = new XmlTextWriter(stream);
                node.GenerateXml(writer);
                writer.Flush();
                return sb.ToString();

            }

        }
    }
}
