namespace LendersOfficeApp.los.common
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.Integration.DocumentCapture;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.DocumentVendor.LQBESignUpdate;

    public class ESignUpdateHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return true; }
        }

        private void SendResponse(HttpResponse response)
        {
            response.ContentEncoding = System.Text.Encoding.UTF8;
            response.Flush();
            response.End();
        }

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;

            string requestXml;

            // dd 01-19-2019 - Need to make a copy of InputStream.
            // We may need InputStream to be available if we need to forward to another server.
            using (MemoryStream stream = new MemoryStream())
            {
                request.InputStream.CopyTo(stream);
                stream.Seek(0, SeekOrigin.Begin);

                using (var reader = new StreamReader(stream, request.ContentEncoding))
                {
                    requestXml = reader.ReadToEnd();
                }

            }

            string replacedStr = requestXml;
            int documentStartIndex = requestXml.IndexOf("<documents>", StringComparison.OrdinalIgnoreCase);
            if (documentStartIndex != -1)
            {
                int endIndex = requestXml.IndexOf("</documents>", StringComparison.OrdinalIgnoreCase);
                if (endIndex == -1)
                {
                    // Handle bad XML formatting
                    endIndex = requestXml.Length;
                }

                int replacedStrLen = endIndex - documentStartIndex - 11;

                replacedStr = requestXml.Substring(0, documentStartIndex)
                    + $"<Documents>For security reasons, document has been removed from the logs. Document size: {replacedStrLen}" + requestXml.Substring(endIndex);
            }            

            Tools.LogInfo("ESignUpdateHandler: Received LQBESignUpdate:" + Environment.NewLine + replacedStr);

            // SG OPM 213551: Add logging for Docutech Esign error
            const int numOfLastChars = 100;
            string additionalInfo = 
                "ESignUpdateHandler: Additional info about request:" + Environment.NewLine +
                "   HTTP Request Content Length (request.ContentLength) : " + request.ContentLength + Environment.NewLine +
                "   Request XML Length from Reader (requestXml.Length)  : " + requestXml.Length + Environment.NewLine;
            if (requestXml.Length > numOfLastChars)
            {
                additionalInfo +=
                    "   Last " + numOfLastChars + " characters of Request XML (should contain proper closing tags) : " + Environment.NewLine +
                    '[' + requestXml.Substring(requestXml.Length - numOfLastChars) + ']';
            }

            Tools.LogInfo(additionalInfo);

            var eSignNotification = extractESignNotification(request, requestXml);

            if (eSignNotification == null) // Could not read XML
            {
                context.Response.StatusCode = 500;
                SendResponse(context.Response);
                return;
            }

            Guid loanId = new Guid(eSignNotification.LoanID);

            Guid brokerId = Guid.Empty;

            try
            {
                DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);
            }
            catch (LoanNotFoundException)
            {
                if (!string.IsNullOrEmpty(ConstStage.AsyncPostbackForwardingHostname))
                {
                    RequestHelper.Forwarding(ConstStage.AsyncPostbackForwardingHostname, context);
                    return;
                }
                else
                {
                    throw;
                }
            }

            //eSignNotification.TransactionID; // We'd use this if we were sending a response
            //eSignNotification.ResponseSource; // Could be used later

            // When there are 0 messages, just log it to PB
            if (!string.IsNullOrEmpty(eSignNotification.Documents))
            {
                bool success = uploadDocumentToEDocs(brokerId, eSignNotification);
                if (!success)
                {
                    context.Response.StatusCode = 500;
                    SendResponse(context.Response);
                    return;
                }
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                new Guid(eSignNotification.LoanID), typeof(ESignUpdateHandler));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var notification = eSignNotification.ESignNotification;

            E_EDisclosureDisclosureEventT disclosureEvent = E_EDisclosureDisclosureEventT.ReceivedLQBESignUpdate;
            if (notification.ToUpper().Contains("E-CONSENT RECEIVED FOR ALL PARTIES"))
            {
                disclosureEvent = E_EDisclosureDisclosureEventT.EConsentReceivedForAllParties;
            }
            else if (notification.ToUpper().Contains("E-CONSENT RECEIVED"))
            {
                disclosureEvent = E_EDisclosureDisclosureEventT.EConsentReceived;
            }
            else if (notification.ToUpper().Contains("E-CONSENT DECLINED"))
            {
                disclosureEvent = E_EDisclosureDisclosureEventT.EConsentDeclined;
            }
            else if (notification.ToUpper().Contains("DOCUMENTS REVIEWED"))
            {
                disclosureEvent = E_EDisclosureDisclosureEventT.DocumentsReviewed;
            }
            else if (notification.ToUpper().Contains("E-SIGN COMPLETED"))
            {
                disclosureEvent = E_EDisclosureDisclosureEventT.ESignCompleted;
                dataLoan.sHasESignedDocumentsT = E_sHasESignedDocumentsT.Yes;
            }
            else if (notification.ToUpper().Contains("MANUAL FULFILLMENT INITIATED"))
            {
                disclosureEvent = E_EDisclosureDisclosureEventT.ManualFulfillmentInitiated;
            }
            else if (notification.ToUpper().Contains("DOCUMENTS SIGNED"))
            {
                disclosureEvent = E_EDisclosureDisclosureEventT.DocumentsSigned;
            }

            dataLoan.ProcessEDisclosureDisclosureTrigger(disclosureEvent, eSignNotification);
            dataLoan.Save();

            // Send a blank response
            context.Response.StatusCode = 200;
            SendResponse(context.Response);
        }

        private bool uploadDocumentToEDocs(Guid brokerId, LQBESignUpdate eSignNotification)
        {
            Guid loanId = new Guid(eSignNotification.LoanID); // FormatException: LoanID is not a valid GUID

            BrokerDB broker = BrokerDB.RetrieveById(brokerId);

            // Does this broker have EDOCS enabled?
            if (!broker.IsEDocsEnabled)
            {
                Tools.LogError("The broker \"" + broker.Name + "\" does not have EDocs enabled.");
                return false;
            }

            // Do they have the generic doc framework enabled?
            if (broker.AllActiveDocumentVendors.TrueForAll(v => v.VendorId == Guid.Empty))
            {
                Tools.LogError("The broker \"" + broker.Name + "\" does not have LQBESignUpdate enabled.");
                return false;
            }

            // Match the given borrower last name to a loan app
            CPageData dataLoan = new CESignUpdateHandlerData(loanId);
            dataLoan.InitLoad();

            Guid appId = Guid.Empty;
            if (!string.IsNullOrEmpty(eSignNotification.BorrLastName))
            {
                string key = eSignNotification.BorrLastName.TrimWhitespaceAndBOM().ToLower();
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    var app = dataLoan.GetAppData(i);
                    if (app.aBLastNm.ToLower() == key)
                    {
                        appId = app.aAppId;
                        break;
                    }
                }
            }

            // If we didn't find a matching last name, match the doc to the first application
            if (appId == Guid.Empty)
            {
                appId = dataLoan.GetAppData(0).aAppId;
            }

            string path = Utilities.Base64StringToFile(eSignNotification.Documents, "LQBESignUpdate.pdf"); // FormatException: not a valid base-64 string

            var useCaptureMethod = broker.EnableKtaIntegration
                && broker.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.DocumentCapture;
            var useCaptureFallbackMethod = false;

            if (useCaptureMethod)
            {
                if (OcrEnabledBranchGroupManager.IsLoanBranchGroupEnabledForOcr(broker, loanId))
                {
                    var parameters = new KtaDocumentUploadParameters()
                    {
                        EmployeeId = Guid.Empty,
                        BranchId = dataLoan.sBranchId,
                        BrokerId = brokerId,
                        LoanId = loanId,
                        ApplicationId = appId,
                        UserType = "B",
                        UserLoginName = "Esign Upload " + eSignNotification.ResponseSource,
                        CustomerCode = broker.CustomerCode,
                        PdfPath = path
                    };

                    var result = KtaUtilities.UploadDocument(parameters);
                    if (result.Success)
                    {
                        return true;
                    }

                    var messageData = $"for ESign Update Handler loan {loanId}, app {appId} ";
                    if (result.MissingCredentials)
                    {
                        // Allow falling back to existing upload behavior only when service credentials are missing.
                        Tools.LogError("Missing service credentials " + messageData);
                        useCaptureFallbackMethod = true;
                    }
                    else
                    {
                        Tools.LogError("Error encountered " + messageData + result.ErrorMessage);
                        return false;
                    }
                }
                else
                {
                    useCaptureFallbackMethod = true;
                }
            }

            bool canUseSplitDocsForThisSigningType = broker.DocMagicSplitESignedDocs;
            bool useSplitDocsAsPrimaryMethod = broker.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.Split;
            bool useSplitDocsAsFallbackMethod = useCaptureFallbackMethod && broker.BackupDocumentSavingOption == E_DocMagicDocumentSavingOption.Split;

            var useSplitDocsMethod = canUseSplitDocsForThisSigningType && (useSplitDocsAsPrimaryMethod || useSplitDocsAsFallbackMethod);

            if (useSplitDocsMethod)
            {
                DocMagicPDFManager.UploadDocument(Guid.Empty, brokerId, loanId, appId, "", "Autosaved: " + DateTime.Now.ToShortTimeString(), path);
                return true;
            }
            else
            {
                // At this point we haven't saved the documents using Capture or Split Docs. Save as Single Document
                // regardless of the save method settings to ensure the documents are not lost.
                int docTypeId;
                if (broker.DocMagicDefaultDocTypeID.HasValue)
                {
                    docTypeId = broker.DocMagicDefaultDocTypeID.Value;
                }
                else
                {
                    docTypeId = EDocumentDocType.GetOrCreatePackageDocType(brokerId, "VENDOR UPLOAD");
                }

                return CreateEdoc(path, docTypeId, eSignNotification.TransactionID, brokerId, loanId, appId);
            }
        }

        private bool CreateEdoc(string path, int docTypeId, string transactionId, Guid brokerId, Guid loanId, Guid appId)
        {
            var repo = EDocumentRepository.GetSystemRepository(brokerId);
            var doc = repo.CreateDocument(E_EDocumentSource.ESignPOST);
            doc.LoanId = loanId;
            doc.AppId = appId;
            doc.DocumentTypeId = docTypeId;
            //doc.InternalDescription // leave blank
            //doc.PublicDescription // leave blank
            doc.IsUploadedByPmlUser = false;
            
            try
            {
                doc.UpdatePDFContentOnSave(path);
            }
            catch (InvalidPDFFileException e)
            {
                Tools.LogError(transactionId + " does not contain a valid pdf.", e);
                return false;
            }
            catch (InsufficientPdfPermissionException e)
            {
                Tools.LogError(transactionId + " contains a password protected pdf.", e);
                return false;
            }
            catch (CBaseException e)
            {
                Tools.LogError("Unexpected exception from pdf reader", e);
                return false;
            }

            doc.EDocOrigin = E_EDocOrigin.LO;
            repo.Save(doc);
            return true;
        }

        private LQBESignUpdate extractESignNotification(HttpRequest request, string requestXml)
        {
            LQBESignUpdate eSignNotification;

            // 10/14/2013 gf - improve parsing of request, mimic generic doc framework
            var responseWrapperXml = XDocument.Parse(requestXml);
            var lqbRootNode = from n in responseWrapperXml.Root.DescendantNodesAndSelf().OfType<XElement>()
                              where n.Name.LocalName.Equals("LQBESignUpdate")
                              select n;
            string esignXml;
            if (lqbRootNode.Any())
            {
                esignXml = lqbRootNode.First().ToString(SaveOptions.DisableFormatting);
            }
            else
            {
                var textNodes = from node in responseWrapperXml.DescendantNodes()
                                where node.NodeType == XmlNodeType.Text
                                select node;
                esignXml = HttpUtility.HtmlDecode(textNodes.First().ToString());
            }

            var deserializer = new XmlSerializer(typeof(LQBESignUpdate));
            try
            {
                using (var reader = new ESignXmlTextReader(new StringReader(esignXml)))
                {
                    eSignNotification = (LQBESignUpdate)deserializer.Deserialize(reader);
                }
            }
            catch (InvalidOperationException exc) // Could not read the XML
            {
                Tools.LogErrorWithCriticalTracking(
                    string.Format(
                        @"Invalid ESignUpdateHandler push request from {0}:{1}. Got {2}",
                        request.UserHostName,
                        request.UserHostAddress,
                        requestXml
                    ),
                    exc
                );
                eSignNotification = null;
            }

            return eSignNotification;
        }
    }

    public class CESignUpdateHandlerData : CPageData
    {
        private static CSelectStatementProvider s_selectProvider;
        static CESignUpdateHandlerData()
        {
            StringList list = new StringList();

            list.Add("aBLastNm");
            list.Add(nameof(CPageBase.sBranchId));

            s_selectProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, list.Collection);
        }
        public CESignUpdateHandlerData(Guid fileId)
            : base(fileId, "CESignUpdateHandlerData", s_selectProvider)
        {
        }

        override protected bool m_enforceAccessControl
        {
            get { return false; }
        }
    }

    // Ignore namespaces when deserializing
    // So that it doesn't matter if they send us xml with a namespace or without
    // 10/14/2013 gf - instead of forcing blank namespace, force namespace
    // from the generated code.
    public class ESignXmlTextReader : XmlTextReader
    {
        public ESignXmlTextReader(TextReader reader) : base(reader) { }

        public override string NamespaceURI
        {
            get
            {
                if (NodeType == XmlNodeType.Element)
                {
                    return "http://tempuri.org/LQBESignUpdate";
                }
                return base.NamespaceURI;
            }
        }
    }
}
