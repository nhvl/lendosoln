﻿namespace LendersOfficeApp.WebService
{
    using System.Web.Services;
    using DataAccess;
    using LendersOffice.ObjLib.Conversions.LoanFileImportExport;
    using LendersOffice.Security;

    /// <summary>
    /// Provides ability to export (from anywhere except production (but loauth export is allowed)) and import (when not on production).<para></para>
    /// Note the webmethods do not have descriptions and are not bumping the call volume because only a limited set of users can call them. <para></para>
    /// To download/upload the export/import, please see BrokerReplicateService's create file, upload file, and download file methods.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class LoanReplicateService : System.Web.Services.WebService
    {
        /// <summary>
        /// Exports the loan with the user specified options.
        /// </summary>
        /// <param name="ticket">The ticket to authenticate the user.</param>
        /// <param name="options">The options to use during the export.</param>
        /// <returns>A result including the name of the file for download.</returns>
        [WebMethod]
        public LoanExportResult ExportLoan(string ticket, LoanExportOptions options)
        {
            var principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);

            return Tools.LogAndThrowIfErrors(() => LoanReplicator.ExportLoan(principal, options));
        }

        /// <summary>
        /// Imports the xml to the database, overwriting the specified loan, or creating a new one.
        /// </summary>
        /// <param name="ticket">The ticket to authenticate the user.</param>
        /// <param name="options">The options to use during the import.</param>
        /// <returns>A result indicating how the import went.</returns>
        [WebMethod]
        public LoanImportResult ImportLoan(string ticket, LoanImportOptions options)
        {
            var principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);

            return Tools.LogAndThrowIfErrors(() => LoanReplicator.ImportLoan(principal, options));            
        }
    }
}