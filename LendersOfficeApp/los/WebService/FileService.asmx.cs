﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Services;

    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Integration.OCR;
    using LendersOffice.ObjLib.UserDropbox;
    using LendersOffice.Security;

    /// <summary>
    /// Summary description for FileService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FileService : System.Web.Services.WebService
    {

        /// <summary>
        /// Upload a document with a given name.
        /// </summary>
        [WebMethod(Description = "Upload a document with a given name.")]
        public bool SaveDoc(string authTicket, string name, byte[] documentBytes)
        {

            try
            {
                AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(authTicket, null);

                DateTime uploaded = DateTime.Now;

                BrokerDB db = BrokerDB.RetrieveById(principal.BrokerId);

                // OPM 453291 - Reject documents that have a virus.
                Tools.ScanForVirus(documentBytes);

                if (OCRService.AddFileFromDropbox(principal, name, uploaded, documentBytes))
                {
                    return true;
                }
                else if (db.EnableDropboxBarcodeScanning)
                {
                    Dropbox.UploadFile(principal.UserId, principal.BrokerId, name, uploaded, documentBytes);
                }
                else
                {
                    Dropbox.AddFile(principal.UserId, principal.BrokerId, name, uploaded, documentBytes);
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Error writing file in the FileService webservice", e);
                throw;
            }

            return true;
        }

        public class DropboxFileDTO
        {
            public Guid FileId;
            public string FileName;

            //Files really shouldn't be more than 4 GB, which is the most an int can hold
            public int SizeBytes;

            public DateTime UploadedOn;

            public DropboxFileDTO(Dropbox.DropboxFile fi)
            {
                FileId = fi.FileId;
                FileName = fi.FileName;
                SizeBytes = fi.SizeBytes;
                UploadedOn = fi.UploadedOn;
            }

            //For serialization
            public DropboxFileDTO() { }
        }

        [WebMethod(Description = "List files for user")]
        public List<DropboxFileDTO> ListFiles(string authTicket)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(authTicket, null);

            var files = Dropbox.GetFilesForUser(principal.ConnectionInfo, principal.UserId);

            return files.Select(a => new DropboxFileDTO(a)).ToList();
        }
    }
}
