﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Reflection;
using LendersOffice.Pdf;
using LendersOffice.Common;

namespace LendersOfficeApp.WebService.doc
{
    public partial class PdfList : MinimalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            m_grid.DataSource = RetrievePdfList().OrderBy(o=>o.Key);
            m_grid.DataBind();
        }

        private List<KeyValuePair<string, string>> RetrievePdfList()
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            
            foreach (var pdf in LendersOffice.Pdf.PdfPrintList.RetrieveOnlyPrintItem())
            {
                if (pdf.IsExcludePrintGroup)
                {
                    continue;
                }
                string value = pdf.Type;
                ConstructorInfo c = PDFClassHashTable.GetConstructor(value);
                IPDFPrintItem o = (IPDFPrintItem)c.Invoke(new object[0]);

                list.Add(new KeyValuePair<string, string>(o.Name, o.Description));
            }

            return list;
        }
    }
}
