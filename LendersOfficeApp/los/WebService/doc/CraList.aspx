﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CraList.aspx.cs" Inherits="LendersOfficeApp.WebService.doc.CraList" EnableViewState="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>CRA List</title>
    <link href="style.css" type="text/css" rel="Stylesheet" />
    <style>
      .Header {
        background-color:#003366;
        color:White;
      }
    </style>
</head>
<body>
  <div class="header">CRA List</div>
  <div class="breadcrumb">
    <a href="index.htm">Main</a> > CRA List
  </div>
    <form id="form1" runat="server">
    <div style="padding-left:10px">
    <asp:GridView ID="m_grid" AutoGenerateColumns="false" runat="server" AlternatingRowStyle-BackColor="gainsboro" Width="600px" CellPadding="3">
      <Columns>
        <asp:BoundField HeaderText="CRA ID" DataField="ID" ItemStyle-CssClass="fieldlabel" HeaderStyle-CssClass="header fieldlabel" ItemStyle-Wrap="false" />
        <asp:BoundField HeaderText="Name" DataField="VendorName" ItemStyle-CssClass="fielddata" HeaderStyle-CssClass="header fieldlabel" ItemStyle-Wrap="false" ItemStyle-Width="100%"/>
      </Columns>
    </asp:GridView>
    </div>
    </form>
</body>
</html>
