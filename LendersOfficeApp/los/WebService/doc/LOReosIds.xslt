﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-16" indent="yes"/>
  <xsl:template match="/">
    <xsl:apply-templates select="LOXml/Collection/aReCollection"/>
  </xsl:template>
  <xsl:template match="LOXml/Collection/aReCollection">
    <html>
      <head>
        <title>LO XML Format</title>
        <link href="style.css" type="text/css" rel="stylesheet"></link>
      </head>
      <body>
        <div class="header">REO Field Ids</div>
        <div class="breadcrumb">
          <a href="index.htm">Main</a> > REO Field Ids
        </div>
        <hr />
        <table border="0" cellspacing="0" cellpadding="3" style="margin-left:10px;border:1px solid black;empty-cells:show;border-collapse:collapse" rules="all">
          <tr>
            <td class="fieldLabel" style="background-color:#003366;color:white;">Field Id</td>
            <td class="fieldLabel"  style="background-color:#003366;color:white">Field Description</td>
          </tr>
          <xsl:for-each select="field">
            <tr>
              <xsl:if test="position() mod 2 = 0">
                <xsl:attribute name="bgcolor">gainsboro</xsl:attribute>
              </xsl:if>
              <td class="fieldLabel" valign="top">
                <xsl:value-of select="@id" />
              </td>
              <td class="fieldData" valign="top">
                <pre>
                  <xsl:value-of select="."/>
                </pre>
              </td>
            </tr>

          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>


