﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <xsl:apply-templates select="LOXml"/>
  </xsl:template>
  <xsl:template match="LOXml">
    <html>
      <head>
        <title>LO XML Format</title>
        <link href="./style.css" type="text/css" rel="stylesheet"></link>
        <style>
          table {
            width: 100%;
            border:1px solid black;
            empty-cells:show;
            border-collapse:collapse;
          }
          table thead td {
            background-color:#003366;
            color:white;
            border: 1px solid black;
          }
          table td:nth-child(1) {
            width:13%;
          }
          table td:nth-child(2) {
            width:10%;
          }
          table td:nth-child(3) {
            width:7%;
          }
          table td:nth-child(4) {
            width:70%;
          }
          table tr:nth-child(even) {
            background: gainsboro;
          }
          .fieldValues {
            margin-left: 10px;
            font-family: Courier New, monospace;
          }
          #content-container {
            margin: 0 10px 0 10px;
          }
        </style>
      </head>
      <body>
          <div class="header">Field Ids for Creating/Updating Housing Expenses</div>
          <div class="breadcrumb">
            <a href="index.htm">Main</a> > Field Ids for Creating/Updating Housing Expenses
          </div>
          <hr />
        <div id="content-container">
          <table border="0" cellspacing="0" cellpadding="3" rules="all">
            <thead>
              <tr>
                <td class="fieldLabel">Field Id</td>
                <td class="fieldLabel" style="padding-right:20px;">Notes</td>
                <td class="fieldLabel" style="padding-right:20px;">Default Value</td>
                <td class="fieldLabel">Field Description</td>
              </tr>
            </thead>
            <xsl:apply-templates select="*[self::field or self::collection or self::attribute]" />
          </table>
        </div>
      </body>
    </html>
  </xsl:template>
  <xsl:template match="*[self::field or self::attribute]">
    <tr>
      <td class="fieldLabel" style="padding-right:20px" valign="top">
        <xsl:if test="local-name() = 'attribute'">@</xsl:if><xsl:value-of select="@id" />
      </td>
      <td class="fieldData" valign="top">
        <xsl:value-of select="@notes" />
      </td>
      <td class="fieldData" valign="top" style="padding-right:20px">
        <xsl:value-of select="value[@default='true']/@baseValue" />
        <xsl:if test="value[@default='true']/@name != '' and value[@default='true']/@name != value[@default='true']/@baseValue">
          - <xsl:value-of select="value[@default='true']/@name" />
        </xsl:if>
        <xsl:value-of select="@default" />
      </td>
      <td class="fieldData" valign="top">
        <div>
          <xsl:value-of select="./text()"/>
        </div>
        <xsl:for-each select="value">
          <div class="fieldValues">
            <xsl:if test="@description != @name">
              <xsl:attribute name="title"><xsl:value-of select="@description" /></xsl:attribute>
            </xsl:if>
            <xsl:value-of select="@baseValue" /><xsl:if test="@name != ''"> - <xsl:value-of select="@name" /></xsl:if>
          </div>
        </xsl:for-each>
      </td>
    </tr>
  </xsl:template>
  <xsl:template match="collection">
    <tr>
      <td class="fieldLabel" style="font-style:italic" valign="top">
        <xsl:value-of select="@id" />
      </td >
      <td class="fieldData" valign="top">
        <xsl:value-of select="@notes" />
      </td>
      <td>
      </td>
      <td class="fieldData" valign="top">
        <div class="fieldData" style="font-style:italic">
        Collection. </div><xsl:value-of select="./text()" /> <br />
        <xsl:for-each select="record">
          Record:
          <table border="0" cellspacing="0" cellpadding="3" rules="all">
            <thead>
              <tr>
                <td class="fieldLabel">Field Id</td>
                <td class="fieldLabel" style="padding-right:20px;">Notes</td>
                <td class="fieldLabel" style="padding-right:20px;">Default Value</td>
                <td class="fieldLabel">Field Description</td>
              </tr>
            </thead>
              <xsl:apply-templates select="*[self::field or self::attribute]" />
          </table>
        </xsl:for-each>
      </td>
    </tr>
</xsl:template>
</xsl:stylesheet>