﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.CreditReport;

namespace LendersOfficeApp.WebService.doc
{
    public partial class CraList : MinimalPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            m_grid.DataSource = MasterCRAList.RetrieveAvailableCras(Guid.Empty);
            m_grid.DataBind();
        }
    }
}
