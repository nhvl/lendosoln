<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<xsl:apply-templates select="LOXml"/>
	</xsl:template>
	<xsl:template match="LOXml">
	  <html>
	    <head>
	      <title>LO XML Format</title>
	      <link href="style.css" type="text/css" rel="stylesheet"></link>
	    </head>
	    <body>
		<div class="header">Field Ids for Creating/Updating PML Originating Companies</div>
		<div class="breadcrumb">
			<a href="index.htm">Main</a> > Field Ids for Creating/Updating PML Originating Companies
		</div>
		<hr />	    
	      <table border="0" cellspacing="0" cellpadding="3" style="margin-left:10px;border:1px solid black;empty-cells:show;border-collapse:collapse" rules="all">
	        <tr><td class="fieldLabel" style="background-color:#003366;color:white;">Field Id</td>
				<td class="fieldLabel" style="padding-right:20px; background-color:#003366;color:white">Required?</td>
				<td class="fieldLabel" style="padding-right:20px; background-color:#003366;color:white">Default Value</td>
				<td class="fieldLabel" style="background-color:#003366;color:white">Field Description</td>
			</tr>
	        <xsl:for-each select="field">
	        <tr>
	          <xsl:if test="position() mod 2 = 0">
	            <xsl:attribute name="bgcolor">gainsboro</xsl:attribute>
	          </xsl:if>
	          <td class="fieldLabel" style="padding-right:20px" valign="top"><xsl:value-of select="@id" /></td>
			  <td class="fieldData" valign="top"><xsl:value-of select="@required" /></td>
			  <td valign="top" style="padding-right:20px">
				  <pre><xsl:value-of select="default" />
								</pre>
							</td>
	          <td class="fieldData" valign="top"><pre><xsl:value-of select="description"/></pre></td>
	        </tr>
	        </xsl:for-each>
	      </table>
	    </body>
	  </html>
	</xsl:template>
</xsl:stylesheet>