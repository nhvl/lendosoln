﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <xsl:apply-templates select="LOXml"/>
  </xsl:template>
  <xsl:template match="LOXml">
    <html>
      <head>
        <title>
          LO XML Format = <xsl:value-of select="@page-title" />
        </title>
        <style>
          table {
            border:1px solid black;
            empty-cells:show;
            border-collapse:collapse;
            width: 100%;
          }
          td {
            padding: 3px;
            border: 1px solid black;
          }
          .fieldIdTable {
            margin: 0px 10px;
          }
          .fieldValues {
            margin-left: 10px;
            font-family: Courier New, monospace;
          }
          div.header {
            width: auto;
          }
        </style>
        <link href="style.css" type="text/css" rel="stylesheet"></link>
      </head>
      <body>
        <div class="header">
          <xsl:value-of select="@page-title" />
        </div>
        <div class="breadcrumb">
          <a href="index.htm">Main</a> > <xsl:value-of select="@page-title" />
        </div>
        <hr />
        <div class="fieldIdTable">
          <table>
            <tr>
              <td class="fieldLabel" style="background-color:#003366;color:white;">Field Id</td>
              <xsl:if test="@info-name">
                <td class="fieldLabel" style="background-color:#003366;color:white;">
                  <xsl:value-of select="@info-name" />
                </td>
              </xsl:if>
              <td class="fieldLabel"  style="background-color:#003366;color:white">Field Description</td>
            </tr>
            <xsl:for-each select="field">
              <tr>
                <xsl:if test="position() mod 2 = 0">
                  <xsl:attribute name="bgcolor">gainsboro</xsl:attribute>
                </xsl:if>
                <td class="fieldLabel" valign="top">
                  <xsl:value-of select="@id" />
                </td>
                <xsl:if test="/LOXml/@info-name">
                  <td class="fieldData" valign="top">
                    <xsl:value-of select="@info" />
                  </td>
                </xsl:if>
                <td class="fieldData" valign="top">
                    <xsl:value-of select="."/>
                    <xsl:for-each select="value">
                      <div class="fieldValues">
                        <xsl:if test="@description != @name">
                          <xsl:attribute name="title">
                            <xsl:value-of select="@description" />
                          </xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="@baseValue" />
                        <xsl:if test="@name != ''">
                          - <xsl:value-of select="@name" />
                        </xsl:if>
                      </div>
                    </xsl:for-each>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>