﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Services;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.Security;

    /// <summary>
    /// Methods for using the title framework.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class OrderTitle : WebService
    {
        /// <summary>
        /// Will get the available providers for ordering title quotes.
        /// </summary>
        /// <param name="authTicket">The auth ticket from AuthService.asmx.</param>
        /// <param name="loanNumber">The looan number of the target loan.</param>
        /// <param name="optionsXml">XML containing options for choosing the lender services.</param>
        /// <returns>
        /// LOXml containing the available quote providers for the loan. Example given below.
        /// </returns>
        /// <remarks>
        /// Below contains sample input and output XML.
        /// --Sample Options XML--
        /// <LOXmlFormat version="1.0">
        ///   <field id="ForPolicy">false</field>
        ///   <field id="ForInteractive">true</field>
        ///   <field id="ForNonInteractive">false</field>
        /// </LOXmlFormat>
        /// --Sample Result--
        /// <LOXmlFormat version="1.0">
        ///  <result status="OK">
        ///    <collection id="ServiceProviders">
        ///      <record index="0" type="ServiceProvider">
        ///        <field id="Id">SomeGuid</field>
        ///        <field id="Name">Name</field>
        ///        <field id="UsesProviderCodes">True</field>
        ///      </record>
        ///      <record index="1" type="Quote Provider">
        ///        <field id="Id">SomeGuid</field>
        ///        <field id="Name">Name</field>
        ///        <field id="UsesProviderCodes">False</field>
        ///      </record>
        ///    </collection>
        ///  </result>
        /// </LOXmlFormat>
        /// </remarks>
        [WebMethod(Description = "Gets the Service Providers for ordering title quotes.")]
        public string GetServiceProviders(string authTicket, string loanNumber, string optionsXml)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Error;
            string errors;

            AbstractUserPrincipal principal;
            Guid loanId;
            var valid = this.RunBasicValidation(authTicket, loanNumber, result, out principal, out loanId);
            if (!valid)
            {
                result.Status = ServiceResultStatus.Error;
                return result.ToResponse();
            }

            if (string.IsNullOrEmpty(optionsXml))
            {
                result.AppendError("No input xml provided.");
                return result.ToResponse();
            }

            XmlDocument options = WebServiceUtilities.TryParseXmlDoc(optionsXml, out errors);
            if (!string.IsNullOrEmpty(errors))
            {
                result.AppendError(errors);
                return result.ToResponse();
            }

            string isForPolicyString = options.SelectSingleNode("/LOXmlFormat/field[@id='ForPolicy']")?.InnerText;
            string isForInteractiveString = options.SelectSingleNode("/LOXmlFormat/field[@id='ForInteractive']")?.InnerText;
            string isForNonInteractiveString = options.SelectSingleNode("/LOXmlFormat/field[@id='ForNonInteractive']")?.InnerText;

            bool isValidXml = true;

            bool isForPolicy = false;
            if (isForPolicyString != null && !bool.TryParse(isForPolicyString, out isForPolicy))
            {
                result.AppendError(WebServiceUtilities.CreateInvalidFieldString("ForPolicy", isForPolicyString));
                isValidXml = false;
            }

            bool isForInteractive = false;
            if (isForInteractiveString != null && !bool.TryParse(isForInteractiveString, out isForInteractive))
            {
                result.AppendError(WebServiceUtilities.CreateInvalidFieldString("ForInteractive", isForInteractiveString));
                isValidXml = false;
            }

            bool isForNonInteractive = false;
            if (isForNonInteractiveString != null && !bool.TryParse(isForNonInteractiveString, out isForNonInteractive))
            {
                result.AppendError(WebServiceUtilities.CreateInvalidFieldString("ForNonInteractive", isForNonInteractiveString));
                isValidXml = false;
            }

            if (!isValidXml)
            {
                return result.ToResponse();
            }

            var lenderServices = TitleLenderService.GetLenderServicesByBrokerId(principal, forPolicy: isForPolicy, forInteractive: isForInteractive, forNonInteractive: isForNonInteractive);
            List<XElement> collectionItems = new List<XElement>();
            int count = 1;
            foreach (var lenderService in lenderServices)
            {
                var record = LoXmlServiceResult.CreateCollectionRecordElement(count++, "ServiceProvider");
                record.Add(LoXmlServiceResult.CreateFieldElement("Id", lenderService.PublicId.ToString()));
                record.Add(LoXmlServiceResult.CreateFieldElement("Name", lenderService.DisplayName));
                record.Add(LoXmlServiceResult.CreateFieldElement("UsesProviderCodes", lenderService.UsesProviderCodes.ToString()));
                collectionItems.Add(record);
            }

            result.AddResultElement(LoXmlServiceResult.CreateCollectionElement("ServiceProviders", collectionItems));
            result.Status = ServiceResultStatus.OK;
            return result.ToResponse();
        }

        /// <summary>
        /// Will order a title quote for the given loan number using the given configuration options.
        /// </summary>
        /// <param name="authTicket">The auth key from AuthService.asmx.</param>
        /// <param name="loanNumber">The loan number of the target loan.</param>
        /// <param name="orderOptionsXml">LoXml that specifies quote order options. Sample below.</param>
        /// <returns>LOXml containing information about the placed order.</returns>
        /// <remarks>
        /// Below contains sample request and response XML.
        /// --Sample Order Options XML--
        /// <LOXmlFormat version="1.0">
        ///   <field id="QuoteProviderId">SomeQuoteProviderId</field>
        ///   <field id="TitleProviderCode">TitleProviderCode</field>
        ///   <field id="AccountId">accountid</field>
        ///   <field id="Username">username</field>
        ///   <field id="Password">password</field>
        ///   <collection id="IncludedFees">
        ///      <field id="Title">true</field>
        ///      <field id="Inspection">true</field>
        ///      <field id="Government">false</field>
        ///      <field id="PropertyTax">false</field>
        ///   </collection>
        /// </LOXmlFormat>
        /// --Sample Success Response XML--
        /// <LOXmlFormat version="1.0">
        ///  <result status="OK">
        ///    <field id="OrderId">OrderId</field>
        ///    <field id="OrderNumber">OrderNumber</field> 
        ///    <collection id="Fees">
        ///      <record index="0" type="Fee">
        ///         <field id="Hudline">800</field>
        ///         <field id="Description">Description</field>
        ///         <field id="Amount">$123.45</field>
        ///         <field id="WasDeleted">false</field>
        ///      </record>
        ///    </collection>
        ///  </result>
        /// </LOXmlFormat>
        /// --Sample Error Response XML--
        /// <LOXmlFormat version="1.0">
        ///   <result status="Error">
        ///     <Errors>
        ///       <Error>Error1</Error>
        ///       <Error>Error2</Error>
        ///     </Errors>
        ///   </result>
        /// </LOXmlFormat>
        /// </remarks>
        [WebMethod(Description = "Orders a Title Quote.")]
        public string OrderTitleQuote(string authTicket, string loanNumber, string orderOptionsXml)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();
            string error;

            AbstractUserPrincipal principal;
            Guid loanId;
            var valid = this.RunBasicValidation(authTicket, loanNumber, result, out principal, out loanId);
            if (!valid)
            {
                result.Status = ServiceResultStatus.Error;
                return result.ToResponse();
            }

            var authorized = Tools.IsWorkflowOperationAuthorized(principal, loanId, WorkflowOperations.OrderTitleQuote);
            if (!authorized.Item1)
            {
                result.AppendError(authorized.Item2);
                return result.ToResponse();
            }

            if (string.IsNullOrEmpty(orderOptionsXml))
            {
                result.AppendError("No input xml provided.");
                return result.ToResponse();
            }

            XmlDocument orderOptions = WebServiceUtilities.TryParseXmlDoc(orderOptionsXml, out error);
            if (!string.IsNullOrEmpty(error))
            {
                result.AppendError(error);
                return result.ToResponse();
            }

            bool isValidXml = true;
            string publicIdString = orderOptions.SelectSingleNode("/LOXmlFormat/field[@id='QuoteProviderId']")?.InnerText;
            string titleProviderCode = orderOptions.SelectSingleNode("/LOXmlFormat/field[@id='TitleProviderCode']")?.InnerText;
            string accountId = orderOptions.SelectSingleNode("/LOXmlFormat/field[@id='AccountId']")?.InnerText;
            string username = orderOptions.SelectSingleNode("/LOXmlFormat/field[@id='Username']")?.InnerText;
            string password = orderOptions.SelectSingleNode("/LOXmlFormat/field[@id='Password']")?.InnerText;

            Guid publicId = Guid.Empty;
            if (publicIdString != null && !Guid.TryParse(publicIdString, out publicId))
            {
                result.AppendError(WebServiceUtilities.CreateInvalidFieldString("QuoteProviderId", publicIdString));
                isValidXml = false;
            }

            var includedFees = orderOptions.SelectNodes("/LOXmlFormat/collection[@id='IncludedFees']/field");
            ErnstQuoteFeeCategoryFlags feeFlags = ErnstQuoteFeeCategoryFlags.None;
            foreach (XmlElement includedFee in includedFees)
            {
                var fee = includedFee.GetAttribute("id");
                var value = includedFee.InnerText;

                ErnstQuoteFeeCategoryFlags feeFlag;
                bool isIncluded = false;
                if (!Enum.TryParse(fee, out feeFlag) || !Enum.IsDefined(typeof(ErnstQuoteFeeCategoryFlags), feeFlag) ||
                    !bool.TryParse(value, out isIncluded))
                {
                    result.AppendError(WebServiceUtilities.CreateInvalidFieldString(fee, value));
                    isValidXml = false;
                    continue;
                }

                if (!isIncluded)
                {
                    continue;
                }

                feeFlags |= feeFlag;
            }

            if ((string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password)) ||
                (!string.IsNullOrEmpty(username) && string.IsNullOrEmpty(password)))
            {
                result.AppendError("A username or password was provided but not the other. Please either provide none or both.");
                isValidXml = false;
            }

            if (!isValidXml)
            {
                return result.ToResponse();
            }

            int? lenderServiceId = TitleLenderService.GetLenderServiceIdByPublicId(principal.BrokerId, publicId);
            if (!lenderServiceId.HasValue)
            {
                result.AppendError($"Unable to find service provider with id {publicId}");
                return result.ToResponse();
            }

            CPageData loanData = null;
            if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(password))
            {
                loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderTitle));
                loanData.InitLoad();

                var credential = ServiceCredential.ListAvailableServiceCredentials(principal, loanData.sBranchId, ServiceCredentialService.TitleQuotes)
                    .FirstOrDefault(cred => cred.TitleQuoteLenderServiceId.HasValue && cred.TitleQuoteLenderServiceId == lenderServiceId.Value);

                if (credential != null)
                {
                    username = credential.UserName;
                    password = credential.UserPassword.Value;
                    accountId = string.IsNullOrEmpty(credential.AccountId) ? accountId : credential.AccountId;
                }
                else
                {
                    result.AppendError("Unable to find valid service credentials.");
                    return result.ToResponse();
                }
            }

            TitleQuoteOrderViewModel viewModel = new TitleQuoteOrderViewModel(loanId, lenderServiceId.Value, titleProviderCode, feeFlags, accountId, username, password);

            ErnstQuoteRequestData requestData = ErnstQuoteRequestData.CreateFromViewModel(viewModel, principal, out error);
            if (requestData == null)
            {
                result.AppendError(error);
                return result.ToResponse();
            }

            var requestHandler = AbstractTitleRequestHandler.CreateRequestHandler(requestData);
            List<string> errors;
            var titleOrder = requestHandler.SubmitRequest(out errors);
            if (titleOrder == null)
            {
                errors.ForEach(e => result.AppendError(e));
                return result.ToResponse();
            }

            if (loanData == null)
            {
                loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(OrderTitle));
                loanData.InitLoad();
            }

            IEnumerable<ModifiedFeeData> modifiedFees = titleOrder.ApplyQuoteToLoanForCalculation(loanData);

            result.AddResultElement(LoXmlServiceResult.CreateFieldElement("OrderId", titleOrder.PublicId.ToString()));
            result.AddResultElement(LoXmlServiceResult.CreateFieldElement("OrderNumber", titleOrder.OrderNumber));
            List<XElement> feeCollection = new List<XElement>();
            LosConvert converter = new LosConvert();
            int count = 1;
            foreach (var fee in modifiedFees)
            {
                var feeItem = this.CreateFeeForExport(count++, fee.Hudline, fee.Description, converter.ToMoneyString(fee.Amount, FormatDirection.ToRep), fee.ResponsibleParty, fee.WasDeleted, null, null);
                feeCollection.Add(feeItem);
            }

            result.AddResultElement(LoXmlServiceResult.CreateCollectionElement("Fees", feeCollection));
            foreach (string warningMessage in titleOrder.QuotedFees.WarningMessages)
            {
                result.AppendWarning(warningMessage);
            }

            return result.ToResponse();
        }

        /// <summary>
        /// Applies the title quote from the selected title order to the loan file.
        /// </summary>
        /// <param name="authTicket">The auth key from AuthService.asmx.</param>
        /// <param name="loanNumber">The loan number.</param>
        /// <param name="orderId">The order id of the order.</param>
        /// <returns>
        /// Success or error XML.
        /// </returns>
        /// <remarks>
        /// Below contains sample response XML.
        /// --Sample Success Response XML--
        /// <LOXmlFormat version="1.0">
        ///  <result status="OK">
        ///     <collection id="Fees">
        ///      <record index="0" type="Fee">
        ///         <field id="Hudline">800</field>
        ///         <field id="Description">Description</field>
        ///         <field id="Amount">$123.45</field>
        ///         <field id="WasDeleted">false</field>
        ///         <field id="Id">SomeGuid</field>
        ///         <field id="ClosingCostFeeType">SomeGuid</field>
        ///      </record>
        ///     </collection>
        ///  </result>
        /// </LOXmlFormat>
        /// --Sample Error Response XML--
        /// <LOXmlFormat version="1.0">
        ///   <result status="Error">
        ///     <Errors>
        ///       <Error>Error1</Error>
        ///       <Error>Error2</Error>
        ///     </Errors>
        ///   </result>
        /// </LOXmlFormat>
        /// </remarks>
        [WebMethod(Description = "Applies a quote received from a title order.")]
        public string ApplyTitleQuote(string authTicket, string loanNumber, Guid orderId)
        {
            LoXmlServiceResult result = new LoXmlServiceResult();
            result.Status = ServiceResultStatus.Error;

            AbstractUserPrincipal principal;
            Guid loanId;
            var valid = this.RunBasicValidation(authTicket, loanNumber, result, out principal, out loanId);
            if (!valid)
            {
                result.Status = ServiceResultStatus.Error;
                return result.ToResponse();
            }

            var authorized = Tools.IsWorkflowOperationAuthorized(principal, loanId, WorkflowOperations.ApplyTitleQuote);
            if (!authorized.Item1)
            {
                result.AppendError(authorized.Item2);
                return result.ToResponse();
            }

            var titleOrder = TitleOrder.LoadOrderByPublicId(orderId, loanId, principal.BrokerId);
            if (titleOrder == null)
            {
                result.AppendError($"Unable to find order with id {orderId}");
                return result.ToResponse();
            }

            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(OrderTitle));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            IEnumerable<ModifiedFeeData> modifiedFees = titleOrder.ApplyAndSaveQuoteToLoan(dataLoan, principal, null, authorized);

            List<XElement> feeCollection = new List<XElement>();
            int count = 1;
            LosConvert converter = new LosConvert();
            foreach (var fee in modifiedFees)
            {
                var feeItem = this.CreateFeeForExport(count++, fee.Hudline, fee.Description, converter.ToMoneyString(fee.Amount, FormatDirection.ToRep), fee.ResponsibleParty, fee.WasDeleted, fee.UniqueId, fee.ClosingCostFeeTypeId, fee.SourceFeeTypeId);
                feeCollection.Add(feeItem);
            }

            result.AddResultElement(LoXmlServiceResult.CreateCollectionElement("Fees", feeCollection));
            result.Status = ServiceResultStatus.OK;
            return result.ToResponse();
        }

        /// <summary>
        /// Performs basic validation.
        /// </summary>
        /// <param name="authTicket">The auth ticket.</param>
        /// <param name="loanNumber">The loan number.</param>
        /// <param name="result">The results object to modify.</param>
        /// <param name="principal">The principal created.</param>
        /// <param name="loanId">The loan id found.</param>
        /// <returns>True if validated. False otherwise.</returns>
        private bool RunBasicValidation(string authTicket, string loanNumber, LoXmlServiceResult result, out AbstractUserPrincipal principal, out Guid loanId)
        {
            principal = null;
            loanId = Guid.Empty;
            string errors = WebServiceUtilities.TryToGetUserPrincipal(authTicket, null, out principal);
            if (!string.IsNullOrEmpty(errors))
            {
                result.AppendError(errors);
                return false;
            }

            CallVolume.IncreaseAndCheck(principal);

            if (string.IsNullOrEmpty(loanNumber))
            {
                result.AppendError("No loan number provided.");
                return false;
            }

            if (principal.BrokerDB.DisableNewTitleFramework)
            {
                result.AppendError("This feature is not enabled.");
                return false;
            }

            loanId = Tools.GetLoanIdByLoanName(principal.BrokerId, loanNumber);
            if (loanId == Guid.Empty)
            {
                result.AppendError($"Invalid loan number: {loanNumber}.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Creates xml representing a fee that's been changed.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="hudline">The fee's hudline.</param>
        /// <param name="description">The fee's description.</param>
        /// <param name="amount">The fee amount.</param>
        /// <param name="responsibleParty">The party responsible for the fee.</param>
        /// <param name="wasDeleted">Whether the fee was deleted or not.</param>
        /// <param name="id">The fee's id.</param>
        /// <param name="feeTypeId">The fee's fee type id.</param>
        /// <param name="sourceFeeTypeId">The fee's <see cref="BaseClosingCostFee.SourceFeeTypeId"/>.</param>
        /// <returns>XElement with all fee values.</returns>
        private XElement CreateFeeForExport(int index, int hudline, string description, string amount, ModifiedFeeResponsibleParty responsibleParty, bool wasDeleted, Guid? id = null, Guid? feeTypeId = null, Guid? sourceFeeTypeId = null)
        {
            var feeItem = LoXmlServiceResult.CreateCollectionRecordElement(index, "Fee");
            feeItem.Add(LoXmlServiceResult.CreateFieldElement("Hudline", hudline.ToString()));
            feeItem.Add(LoXmlServiceResult.CreateFieldElement("Description", description));
            feeItem.Add(LoXmlServiceResult.CreateFieldElement("Amount", amount));
            feeItem.Add(LoXmlServiceResult.CreateFieldElement("ResponsibleParty", responsibleParty.ToString("G")));
            feeItem.Add(LoXmlServiceResult.CreateFieldElement("WasDeleted", wasDeleted.ToString()));

            if (id.HasValue)
            {
                feeItem.Add(LoXmlServiceResult.CreateFieldElement("Id", id.ToString()));
            }

            if (feeTypeId.HasValue)
            {
                feeItem.Add(LoXmlServiceResult.CreateFieldElement("ClosingCostFeeType", feeTypeId.ToString()));
            }

            if (sourceFeeTypeId.HasValue)
            {
                feeItem.Add(LoXmlServiceResult.CreateFieldElement("SourceFeeType", sourceFeeTypeId.ToString()));
            }

            return feeItem;
        }
    }
}
