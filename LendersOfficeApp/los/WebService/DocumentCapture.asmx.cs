﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Linq;
    using System.Web.Services;
    using System.Xml.Linq;
    using DataAccess;
    using EDocs;
    using LendersOffice.Integration.DocumentCapture;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Container for methods used to send and receive document capture related data.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class DocumentCapture : WebService
    {
        /// <summary>
        /// Return a list of EDoc folders and doc types enabled for the lender associated with the OCR request ID.
        /// </summary>
        /// <param name="partnerKey">
        /// The key provided to the document capture partner.
        /// </param>
        /// <param name="ocrRequestId">
        /// The ID of the OCR request.
        /// </param>
        /// <returns>
        /// The list of EDoc folders and doc types.
        /// </returns>
        [WebMethod(Description = "Return a list of EDoc folders and doctypes enabled for the lender associated with the OCR request ID.")]
        public string GetDocumentTypes(string partnerKey, string ocrRequestId)
        {
            AuthServiceHelper.AuthDocumentCapture(partnerKey);

            Tools.LogInfo("DocumentCapture.asmx::GetDocumentTypes", "Request ID = " + ocrRequestId);

            var requestId = OcrRequestId.Create(ocrRequestId);
            if (!requestId.HasValue)
            {
                var result = new ServiceResult();
                result.AppendError("Invalid OCR request ID specified.");
                return result.ToResponse();
            }

            var brokerId = OcrRequest.GetBrokerIdFromRequestId(requestId.Value);
            if (!brokerId.HasValue)
            {
                var result = new ServiceResult();
                result.AppendError("Unable to locate lender associated with OCR request ID.");
                return result.ToResponse();
            }

            var docTypes = from docType in EDocumentDocType.GetDocTypesByBroker(brokerId.Value, E_EnforceFolderPermissions.False)
                           where !docType.Folder.IsSystemFolder && docType.EDocClassification.Id != EdocClassification.NoClassification.Id
                           select new XElement(
                               "docType",
                               new XElement("id", docType.Id),
                               new XElement("lqbDocTypeName", new XCData(docType.DocTypeName)),
                               new XElement("ktaDocTypeName", new XCData(docType.EDocClassification.Name)),
                               new XElement("folderName", new XCData(docType.Folder.FolderNm)));

            var docTypeRoot = new XElement("docTypes");
            foreach (XElement docType in docTypes)
            {
                docTypeRoot.Add(docType);
            }

            var docTypeResult = new XElement("result", new XAttribute("status", "COMPLETE"));
            docTypeResult.Add(docTypeRoot);

            var resultXml = WebServiceUtilities.GenerateResponseXml(docTypeResult);
            Tools.LogInfo("DocumentCapture.asmx::GetDocumentTypes", $"Broker {brokerId} request {requestId.Value} response:" + Environment.NewLine + resultXml);

            return resultXml;
        }

        /// <summary>
        /// Saves information pertaining to the document transaction corresponding to the OCR request ID.
        /// </summary>
        /// <param name="partnerKey">
        /// The key provided to the document capture partner.
        /// </param>
        /// <param name="ocrRequestId">
        /// The ID of the OCR request.
        /// </param>
        /// <param name="xmlMetaData">
        /// All of the information pertaining to the document transaction corresponding to the OCR request ID 
        /// that LQB requires in order to present the associated EDocs to the user as classified by KTA, 
        /// e.g. LQB doc type, page order, etc.
        /// </param>
        /// <returns>
        /// The status for the save.
        /// </returns>
        [WebMethod(Description = "Saves information pertaining to the document transaction corresponding to the OCR request ID.")]
        public string UploadMetaData(string partnerKey, string ocrRequestId, string xmlMetaData)
        {
            AuthServiceHelper.AuthDocumentCapture(partnerKey);

            Tools.LogInfo("DocumentCapture.asmx::UploadMetaData", $"Request ID = {ocrRequestId}, xmlMetaData = {xmlMetaData}");

            var result = new ServiceResult();

            var metadata = xmlMetaData?.Trim();
            if (string.IsNullOrEmpty(metadata))
            {
                result.AppendError("XML metadata must be supplied.");
                return result.ToResponse();
            }

            var requestId = OcrRequestId.Create(ocrRequestId);
            if (!requestId.HasValue)
            {
                result.AppendError("Invalid OCR request ID specified.");
                return result.ToResponse();
            }

            var brokerId = OcrRequest.GetBrokerIdFromRequestId(requestId.Value);
            if (!brokerId.HasValue)
            {
                result.AppendError("Unable to locate lender associated with OCR request ID.");
                return result.ToResponse();
            }

            KtaUtilities.ProcessDocumentCaptureMetadata(brokerId.Value, requestId.Value, metadata);

            return result.ToResponse();
        }

        /// <summary>
        /// Updates the status of the document transaction associated with the OCR request ID.
        /// </summary>
        /// <param name="partnerKey">
        /// The key provided to the document capture partner.
        /// </param>
        /// <param name="ocrRequestId">
        /// The ID of the OCR request.
        /// </param>
        /// <param name="status">
        /// The updated status for the document transaction.
        /// </param>
        /// <param name="supplementaryData">
        /// Supplementary data for the status update.
        /// </param>
        /// <returns>
        /// The status for the update.
        /// </returns>
        [WebMethod(Description = "Updates the status of the document transaction associated with the OCR request ID.")]
        public string UpdateStatus(string partnerKey, string ocrRequestId, int status, string supplementaryData)
        {
            AuthServiceHelper.AuthDocumentCapture(partnerKey);

            Tools.LogInfo("DocumentCapture.asmx::UpdateStatus", $"Request ID = {ocrRequestId}, status = {status}, supplementaryData = {supplementaryData}");

            var result = new ServiceResult();

            var requestId = OcrRequestId.Create(ocrRequestId);
            if (!requestId.HasValue)
            {
                result.AppendError("Invalid OCR request ID specified.");
                return result.ToResponse();
            }

            if (!Enum.IsDefined(typeof(OcrRequestStatus), status))
            {
                result.AppendError("Invalid status specified.");
                return result.ToResponse();
            }

            var brokerId = OcrRequest.GetBrokerIdFromRequestId(requestId.Value);
            if (!brokerId.HasValue)
            {
                result.AppendError("Unable to locate lender associated with OCR request ID.");
                return result.ToResponse();
            }

            var newStatus = new OcrStatus(requestId.Value, brokerId.Value)
            {
                Status = (OcrRequestStatus)status,
                Date = DateTime.Now
            };

            var statusData = supplementaryData?.Trim();
            if (!string.IsNullOrEmpty(statusData))
            {
                newStatus.SupplementalData = KtaUtilities.CreateSupplementaryData(statusData);
            }

            newStatus.Save();
            return result.ToResponse();
        }
    }
}