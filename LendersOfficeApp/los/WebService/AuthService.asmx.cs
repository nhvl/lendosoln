namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Web.Services;
    using System.Xml;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.ObjLib.Webservices;
	/// <summary>
	/// Container for methods to convert user identification data into 
	/// authentication tickets (auth-tickets).
	/// </summary>
	[WebService(Namespace="http://www.lendersoffice.com/los/webservices/")]
	public class AuthService : System.Web.Services.WebService
	{

		public AuthService()
		{
			InitializeComponent();
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

		/// <summary>
		/// Convert a user's login and password into an auth-ticket.
		/// </summary>
        [WebMethod(Description = "Convert a LendingQB user's login and password into an auth-ticket.")]
		public string GetUserAuthTicket(string userName, string passWord)
		{
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(passWord))
            {
                throw new AccessDenied(ErrorMessages.InvalidLoginPassword);
            }

            AbstractUserPrincipal principal = AuthServiceHelper.AuthenticateUser(userName, passWord);
            RequestHelper.EnforceForWebServices(principal);
            return AuthServiceHelper.CreateAuthTicket("USER_TICKET", principal);

        }

   

        /// <summary>
        /// Convert a user's login and password into an auth-ticket and change the password of the user.
        /// </summary>
        /// <param name="userName">The login name of the user.</param>
        /// <param name="passWord">The current password of the user.</param>
        /// <param name="newPassword">The new password for the user.</param>
        /// <returns>The auth-ticket for the user.</returns>
        /// <exception cref="CBaseException">Thrown when some part of the process fails.</exception>
        [WebMethod(Description = "Convert a LendingQB user's login and password into an auth-ticket and change the password of the user.")]
        public string GetUserAuthTicketAndChangePassword(string userName, string passWord, string newPassword)
        {
            // We're using an older model here of throwing exceptions on user errors to mimic the other web services in the AuthService.
            // Mike M. and I discussed this and decided that for webservices 1.0, we'll try to keep this consistent, and won't worry about
            // making it better.  Webservices 2.0 will do something nicer and return wrapped responses.
            AbstractUserPrincipal principal;
            bool resetPasswordExpiration = false;
            try
            {
                principal = AuthServiceHelper.AuthenticateUser(userName, passWord);
            }
            catch (PasswordExpiredException exc)
            {
                principal = exc.Principal; // If the user's password has expired, this method is the API's way to update it.
                resetPasswordExpiration = true;
            }

            if (principal == null)
            {
                throw CBaseException.GenericException("Missing Principal"); // Shouldn't happen, but handling explicitly to prevent null reference
            }

            RequestHelper.EnforceForWebServices(principal);
            var employee = LendersOffice.Admin.EmployeeDB.RetrieveByIdOrNull(principal.BrokerId, principal.EmployeeId);
            if (employee == null)
            {
                throw CBaseException.GenericException("Missing Employee"); // Shouldn't happen, but handling explicitly to prevent null reference
            }

            LendersOffice.Admin.StrongPasswordStatus passwordStatus = employee.IsStrongPassword(newPassword);
            if (passwordStatus != LendersOffice.Admin.StrongPasswordStatus.OK)
            {
                throw new CBaseException(LendersOffice.Admin.EmployeeDB.GetStrongPasswordUserMessage(passwordStatus), ErrorMessages.GenericAccessDenied);
            }

            employee.Password = newPassword;
            if (resetPasswordExpiration)
            {
                employee.ResetPasswordExpirationD();
            }

            employee.Save(principal);
            return AuthServiceHelper.CreateAuthTicket("USER_TICKET", principal);
        }

        /// <summary>
        /// Convert an Active Directory (LQB) user's login and password into an auth-ticket.
        /// </summary>
        [WebMethod(Description = @"Authenticate a LendingQB user against an Active Directory database. Returns an auth-ticket. 
                        [customerCode] Contact your account administrator to request your company's customer code. 
                        This method should only be used to authenticate LendingQB users that have been configured for Active Directory authentication.")]
        public string GetActiveDirectoryUserAuthTicket(string userName, string passWord, string customerCode)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(passWord) || string.IsNullOrEmpty(customerCode))
            {
                throw new AccessDenied(ErrorMessages.InvalidLoginPassword);
            }

            AbstractUserPrincipal principal = AuthServiceHelper.AuthenticateActiveDirectoryUser(userName, passWord, customerCode);
            RequestHelper.EnforceForWebServices(principal);
            return AuthServiceHelper.CreateAuthTicket("AD_USER_TICKET", principal);
        }

		/// <summary>
		/// Convert a PML user's login, password and customer code into an auth-ticket.
		/// </summary>
		[WebMethod(Description="Convert a PriceMyLoan.com user's login and password into an auth-ticket. [customerCode] Your PML Support Engineer can provide you with your company's customer code if you do not know what it is. The format for this customer code is PML#### (i.e., PML0445)")]
		public string GetPmlUserAuthTicket(string userName, string passWord, string customerCode)
		{
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(passWord) || string.IsNullOrEmpty(customerCode))
            {
                throw new AccessDenied(ErrorMessages.InvalidLoginPassword);
            }

            AuthServiceHelper.AuthPmlUser(userName, passWord, customerCode);
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            RequestHelper.EnforceForWebServices(principal);

            return AuthServiceHelper.CreateAuthTicket("PML_USER_TICKET", principal);
		}

        /// <summary>
        /// This method authenticates a Mobile PML user based on the provided parameters.
        /// </summary>
        /// <param name="userName">The user's username.</param>
        /// <param name="passWord">The user's login password.</param>
        /// <param name="customerCode">The user's broker's customer code.</param>
        /// <param name="deviceUniqueId">The device's id that is being used to log in to the system.</param>
        /// <param name="devicePassword">The password of the user's device.</param>
        /// <returns>An xml response containing the user's encrypted ticket for authentication.</returns>
        [WebMethod(Description = "Convert a PML user's login and password into an auth-ticket for mobile users.")]
        public string GetMobilePmlUserAuthTicket(string userName, string passWord, string customerCode, string deviceUniqueId, string devicePassword)
        {

            string trimmedDevicePassword = devicePassword.Replace(" ", "");
            string trimmedDeviceUniqueId = deviceUniqueId.Replace(" ", "");

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(passWord) || string.IsNullOrEmpty(trimmedDevicePassword) || string.IsNullOrEmpty(trimmedDeviceUniqueId))
            {
                throw new AccessDenied(ErrorMessages.InvalidLoginPassword);
            }

            AuthServiceHelper.AuthPmlUser(userName, passWord, customerCode);

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            AuthServiceHelper.VerifyMobileAccess(principal, trimmedDevicePassword, trimmedDeviceUniqueId);

            return AuthServiceHelper.CreateAuthTicketServiceResult("PML_USER_TICKET", principal);
        }

        /// <summary>
        /// Convert the partner's secret key, the broker ID and the user's login
        /// into an auth-ticket. Note that the password is not required. Anybody
        /// with the partner's secret key and the broker ID can impersonate a user.
        /// That is why the partner's secret key is required. It's assumed that 
        /// the partner can be trusted to not abuse this access to the system.
        /// </summary>
        [WebMethod(Description="This method is deprecated. Please use GetUserAuthTicket instead.")]
		public string GetBrokerPartnerAuthTicket(string sSecretKey, Guid brokerId, string userName)
		{

            AuthServiceHelper.AuthPartner(sSecretKey, brokerId, userName);
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            string currentIP = RequestHelper.ClientIP;

            if (ConstStage.AllowableIPsForAuthenticationPartnerWebservice.Contains(currentIP) == false && RequestHelper.IsLOAdminIP(currentIP) == false)
            {
                // 4/28/2015 dd - Honor client IP restriction if the request is not come from one of our trusted server.
                RequestHelper.EnforceForWebServices(principal);
            }

            return AuthServiceHelper.CreateAuthTicket("PARTNER_TICKET", principal);

        }

        [WebMethod(Description = "Convert a LendingQB user's login and password into an auth-ticket wich checking mobile restriction.")]
        public string GetMobileUserAuthTicket(string userName, string passWord, string deviceUniqueId, string devicePassword)
        {
            string trimmedDevicePassword = devicePassword.Replace(" ", ""); ;
            string trimmedDeviceUniqueId = deviceUniqueId.Replace(" ", "");
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(passWord) || string.IsNullOrEmpty(trimmedDevicePassword)||string.IsNullOrEmpty(trimmedDeviceUniqueId))
            {
                throw new AccessDenied(ErrorMessages.InvalidLoginPassword);
            }

            //authenticate with login in info   
            AbstractUserPrincipal principal = AuthServiceHelper.AuthenticateUser(userName, passWord);

            //verify mobile id 
            AuthServiceHelper.VerifyMobileAccess(principal, trimmedDevicePassword, trimmedDeviceUniqueId);
            return AuthServiceHelper.CreateAuthTicket("USER_TICKET", principal);
        }

        [WebMethod(Description = "Authenticate a LendingQB user against an Active Directory database with checking mobile restriction.")]
        public string GetMobileADUseAuthTicket(string userName, string passWord, string customerCode, string deviceUniqueId, string devicePassword)
        {
            //verify input
            string trimmedDevicePassword = devicePassword.Replace(" ", ""); ;
            string trimmedDeviceUniqueId = deviceUniqueId.Replace(" ", "");
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(passWord) || string.IsNullOrEmpty(customerCode) || string.IsNullOrEmpty(trimmedDevicePassword) || string.IsNullOrEmpty(trimmedDeviceUniqueId))
            {
                throw new AccessDenied(ErrorMessages.InvalidLoginPassword);
            }

            //authenticate with login in info
            AbstractUserPrincipal principal = AuthServiceHelper.AuthenticateActiveDirectoryUser(userName, passWord, customerCode);

            //verify mobile id 
            AuthServiceHelper.VerifyMobileAccess(principal, trimmedDevicePassword, trimmedDeviceUniqueId);
            return AuthServiceHelper.CreateAuthTicket("AD_USER_TICKET", principal);
        }
	}
}
