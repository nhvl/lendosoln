﻿// db - NOTE: This class really belongs in the LoLib Conversions folder along with the other imports (Fannie Mae, Point, LoXML, etc),
// but it needs access to the EDocs project.  The EDocs project cannot be added as a reference to the LoLib project
// because of a circular dependency, so for now, this code will be put into LendersOfficeApp, which has an EDocs reference
// It would be better to move the necessary EDocs functionality into LoLib when there is more time for regression testing.
namespace LendersOfficeApp.WebService
{
    using System;
    using DataAccess;
    using EDocs;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Security;

    public class PdfImporter
    {
        /// <summary>
        /// Imports a PDF into EDocs with the specified data.
        /// </summary>
        /// <param name="principal">The user principal.</param>
        /// <param name="sLId">The loan id of the loan file to which the document belongs.</param>
        /// <param name="aAppId">The app id of the loan application to which the document belongs.</param>
        /// <param name="fileContent">The file content, encoded in base-64.</param>
        /// <param name="docTypeDesc">The <see cref="EDocs.DocType.DocTypeName"/> to contain the EDoc.</param>
        /// <param name="publicDescription">The <see cref="EDocs.EDocument.PublicDescription"/> of the EDoc.</param>
        /// <param name="billingInfo">The Generic Framework billing information to use for billing; ignored if null.</param>
        /// <returns>The document id of the document that was just uploaded.</returns>
        public static Guid Import(AbstractUserPrincipal principal, Guid sLId, Guid aAppId, string fileContent, string docTypeDesc, string publicDescription, 
            LendersOffice.Integration.GenericFramework.BillingInfo genericFrameworkBillingInfo, bool useUnclassifiedFallback, out bool didFindDocType)
        {
            // OPM 475715 - Check workflow for regular (non-generic framework) users.
            if (genericFrameworkBillingInfo == null)
            {
                EDocsUpload.AssertUploadEDocsWorkflowPrivileges(principal, sLId);
            }

            CPageData dataLoan = new CLOPmlLoanViewData(sLId); // TODO - determine if we really need any more data from this
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(aAppId);
            if (dataApp == null)
            {
                dataApp = dataLoan.GetAppData(0);
            }

            aAppId = dataApp.aAppId; // In case aAppId == Guid.Empty or GetAppData returned null for aAppId
            PdfDocument document = new PdfDocument(sLId, aAppId);
            Guid docId = document.Load(principal, fileContent, docTypeDesc, publicDescription, useUnclassifiedFallback, out didFindDocType);
            if (genericFrameworkBillingInfo != null)
            {
                genericFrameworkBillingInfo.RecordDocumentUpload(sLId, document.DocumentId);
            }

            return docId;
        }
    }

    public class PdfDocument
    {
        private Guid m_sLId = Guid.Empty;
        private Guid m_aAppId = Guid.Empty;
        private Guid? documentId = null;

        public PdfDocument(Guid sLId, Guid aAppId)
        {
            m_sLId = sLId;
            m_aAppId = aAppId;
        }

        public Guid DocumentId
        {
            get { return this.documentId.Value; }
        }

        public Guid Load(AbstractUserPrincipal principal, string sDataContent, string docTypeDesc, string publicDescription, bool useUnclassifiedFallback, out bool didFindDocType)
        {
            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            EDocument eDoc = repository.CreateDocument(E_EDocumentSource.UserUpload);
            string tempFile = TempFileUtils.NewTempFilePath();
            BinaryFileHelper.WriteAllBytes(tempFile, Convert.FromBase64String(sDataContent));

            eDoc.IsUploadedByPmlUser = principal.Type.Equals("p", StringComparison.OrdinalIgnoreCase);
            eDoc.FaxNumber = "";
            eDoc.LoanId = m_sLId;
            eDoc.AppId = m_aAppId;
            eDoc.DocumentTypeId = RetrieveDocTypeByDesc(docTypeDesc, useUnclassifiedFallback, principal, out didFindDocType);
            eDoc.PublicDescription = publicDescription;
            eDoc.UpdatePDFContentOnSave(tempFile);
            eDoc.EDocOrigin = E_EDocOrigin.LO;

            if (!didFindDocType)
            {
                eDoc.AddAuditEntry(E_PdfUserAction.UnableToDetermineDocType, DateTime.Now, principal.UserId, $"Unable to determine DocType {docTypeDesc}. Defaulted to UNCLASSIFIED DocType.");
            }

            repository.Save(eDoc);
            FileOperationHelper.Delete(tempFile);

            this.documentId = eDoc.DocumentId; // Only defined when saved.

            return this.documentId.GetValueOrDefault();
        }

        private int RetrieveDocTypeByDesc(string docTypeDesc, bool useUnclassifiedFallback, AbstractUserPrincipal uploader, out bool didFindDocType)
        {
            AbstractUserPrincipal principal = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;

            foreach (DocType documentType in EDocumentDocType.GetDocTypesByBroker(principal.BrokerId, E_EnforceFolderPermissions.True))
            {
                if (documentType.DocTypeName.Equals(docTypeDesc, StringComparison.OrdinalIgnoreCase))
                {
                    didFindDocType = true;
                    return int.Parse(documentType.DocTypeId);
                }
            }

            if (useUnclassifiedFallback)
            {
                didFindDocType = false;
                var unclassifiedDocTypeInfo = LendersOffice.ObjLib.Edocs.AutoSaveDocType.AutoSaveDocTypeFactory.GetUnclassifiedDocTypeInfo(E_AutoSavePage.None, uploader.BrokerId, string.Empty);
                return unclassifiedDocTypeInfo.DocType.Id;
            }

            throw new CBaseException("Cannot find document type: " + docTypeDesc, "Cannot find document type: " + docTypeDesc + " while importing from the Loan.asmx webservice.");
        }
    }
}