﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Web.Services;
    using LendersOffice.Security;
    using LendersOfficeApp.ObjLib.Services.LoansPQ;

    /// <summary>
    /// Allows the caller to pass in LoansPQ common loan format to create LendingQB Loans. The IDs returned can be used to redirect the user 
    /// to the page for the given action. Guids returned from the create and edit calls can be used in a  custom url to redirect users to the loan 
    /// without requiring them to login. The IDs are only good for 2 minutes. 
    /// </summary>
    [WebService(Namespace = "https://www.lendersoffice.com/los/webservices/", Name = "LoansPQ Web Service",
        Description = "")]
    [System.ComponentModel.ToolboxItem(false)]
    public class LoansPQ : System.Web.Services.WebService
    {
        private ILoansPQService _serv = new LoansPQService(); 
        /// <summary>
        /// Creates a LendingQB loan file using the given credentials, give id and import data. The id returned
        /// can be used for 2 minutes to redirect the user to a specified url. 
        /// </summary>
        /// <param name="username">LendingQB Username</param>
        /// <param name="password"></param>
        /// <param name="LoansPQFileID"></param>
        /// <param name="commonLoanFormat"></param>
        /// <returns>Url and unique guid that can be used for 2 minutes to redirect to the loan editor for the created loan</returns>
        [WebMethod(Description = "Creates/Updates a LendingQB loan file using the given credentials, id and import data. The id returned can be used  to redirect the user to the loan editor.")]
        public string TransferDataToLendersOffice(string username, string password, long loansPQFileID, string clfData)
        {
            AuthServiceHelper.AuthenticateUser(username, password);
            Guid loanID = _serv.UpdateLoanFile(loansPQFileID, clfData);
            return _serv.GenerateUrlForEditingLoan(loansPQFileID);

        }
        /// <summary>
        /// Retrieves an id that can be used in a url to redirect the user to the loan editor for the specified loan.  
        /// </summary>
        /// <param name="username">LendingQB Username</param>
        /// <param name="password"></param>
        /// <param name="LoansPQFileID"></param>
        /// <returns>unique guid that can be used for 2 minutes to redirect to the loan editor for the created loan</returns>
        [WebMethod(Description = "Retrieves an id that can be used to redirect the user to the loan editor for the specified loan.")]
        public string EditLoanFile(string username, string password, long loansPQFileID)
        {
            AuthServiceHelper.AuthenticateUser(username, password);
            return _serv.GenerateUrlForEditingLoan(loansPQFileID);
        }

        /// <summary>
        /// Serializes and  the LendingQB Loan file with the given file id accessible by the specified user to the common loan format. 
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <param name="LoansPQFileID"></param>
        /// <returns></returns>
        [WebMethod(Description = "Retrieves the specified LendingQB loan file and updates the given CLF data file with the new changes.")]
        public string RetrieveLoanFile(string username, string password, long loansPQFileID, string clfData)
        {
            AuthServiceHelper.AuthenticateUser(username, password);
            return _serv.ExportLoanFile(loansPQFileID, clfData);
        }
    }



}
