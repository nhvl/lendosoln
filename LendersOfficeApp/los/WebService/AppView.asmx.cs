namespace LendersOfficeApp.WebService
{
    using System;
    using System.ComponentModel;
    using System.Web.Services;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Container for methods to retrieve URLs that allows a client to
    /// log in directly into LendingQB without having to type in
    /// their login and password.
    /// </summary>
    [WebService(Namespace="http://www.lendersoffice.com/los/webservices/")]
	public class AppView : WebService
	{
		public AppView()
		{
			InitializeComponent();
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

		/// <summary>
		/// Returns the URL that would allow a user connect directly into the
		/// page that they see when they open a loan file.
		/// </summary>
		[WebMethod]
		public string GetViewLoanUrl(string sTicket, string sLoanName)
		{
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, sLoanName);
            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLoanName);

            if (sLId == Guid.Empty) 
            {
                throw new IntegrationInvalidLoanNumberException(sLoanName);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }

            return "/newlos/loanapp.aspx?auth_id=" + GetBypassTicket().ToString() + "&loanid=" + sLId.ToString() ;
		}
		
		/// <summary>
		/// Returns the URL allowing the user to connect directly into the
		/// LendingQB desktop view. This is the first screen they see
		/// if they were to log into LendingQB manually via the login
		/// screen.
		/// </summary>
        [WebMethod(Description = "Returns a partial URL to redirect the client directly to the LendingQB Desktop (Pipeline View). The partial URL should be prefixed with https://secure.lendingqb.com")]
		public string GetViewDesktopUrl(string sTicket)
		{
            AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);
			return "/los/main.aspx?auth_id=" + GetBypassTicket().ToString() ;
		}
		private string MapUrl(string sSubPath)
		{
			string sURL = Context.Request.Url.ToString() ;
			sURL = sURL.Substring(0, sURL.IndexOf("/los/")) ;
			return sURL + sSubPath ;
		}
		internal static Guid GetBypassTicket()
		{
			AbstractUserPrincipal principal = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal ;
            return principal.GenerateByPassTicket();
		}

        private Guid GetMcl2PmlByPassTicket() 
        {
            AbstractUserPrincipal principal = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal ;
            Guid ticket = Guid.NewGuid() ;


            AutoExpiredTextCache.AddToCache(principal.UserId.ToString(), new TimeSpan(0, 1, 0), "Mcl2Pml_" + ticket.ToString());
            
            return ticket;
        }

		[WebMethod(Description="Returns a partial URL to redirect the client directly to the PriceMyLoan.com Pipeline View. The partial URL should be prefixed with https://secure.pricemyloan.com")]
		public string GetPmlPipelineUrl(string sTicket) 
        {
            AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);
            string url = "/main/baseframe.aspx?authid=" + GetBypassTicket() + "&islogin=" + LendersOffice.Constants.ConstAppDavid.PmlStrangeKey;
            return url;

        }

		[WebMethod(Description="Returns a partial URL to redirect the client directly into the PriceMyLoan.com Loan File View (Step #1). The partial URL should be prefixed with https://secure.pricemyloan.com.")]
		public string GetPmlLoanUrl(string sTicket, string sLoanName) 
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, sLoanName);
            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLoanName);

            if (sLId == Guid.Empty) 
            {
                throw new IntegrationInvalidLoanNumberException(sLoanName);
            }

            if (Tools.IsQP2LoanFile(sLId, principal.BrokerId))
            {
                throw new ArgumentException(ErrorMessages.WebServices.CannotRunServiceForQuickPricerLoan);
            }
            
            string url = "";

            if (principal.LoginNm.ToUpper() == ConstAppDavid.PmlMortgageLeagueLogin.ToUpper()) 
            {
                CPageData dataLoan = new CPmlTransformData(sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
                dataLoan.Save();

                string tabKey = "APPINFO";

                if (!dataLoan.GetAppData(0).aIsCreditReportOnFile) 
                {
                    // 11/3/2008 dd - OPM 25810 - If there is no credit report on file then redirect user to credit page.
                    tabKey = "CREDIT";
                }

                
                url = "/main/main.aspx?gmlid=" + GetMcl2PmlByPassTicket() + "&loanid=" + sLId + "&islogin=" + LendersOffice.Constants.ConstAppDavid.PmlStrangeKey + "&tabkey=" + tabKey;
            } 
            else 
            {
                url = "/main/agents.aspx?authid=" + GetBypassTicket() + "&loanid=" + sLId + "&islogin=" + LendersOffice.Constants.ConstAppDavid.PmlStrangeKey;
            }
            
            return url;
        }
	}
}

