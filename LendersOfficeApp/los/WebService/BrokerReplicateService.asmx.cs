﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Web.Services;
    using DataAccess;
    using DataAccess.TempFile;
    using LendersOffice.ObjLib.Conversions.LoanFileImportExport;
    using LendersOffice.Security;

    /// <summary>
    /// Provides ability to export (from anywhere except production (but loauth export is allowed)) and import (when not on production),
    /// as well as the ability to download/upload files from the temporary folder in a memory efficient way, as well as a way that supports
    /// parallelism. <para></para>
    /// Note the webmethods do not have descriptions and are not bumping the call volume because only a limited set of users can call them. <para></para>
    /// Also provided are the ability to upload and download temporary files, which should not be used generally.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]

    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BrokerReplicateService : System.Web.Services.WebService
    {
        /// <summary>
        /// Exports the broker with the user specified options.
        /// </summary>
        /// <param name="ticket">The ticket to authenticate the user.</param>
        /// <param name="options">The options to use during the export.</param>
        /// <returns>A result including the name of the file for download.</returns>
        [WebMethod]
        public BrokerExportResult ExportBrokerFromOptions(string ticket, BrokerExportOptions options)
        {
            var principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);

            return Tools.LogAndThrowIfErrors(() => BrokerReplicator.ExportBrokerXml(principal, options));
        }

        /// <summary>
        /// Exports the broker with default options. (compression, include files, no users.).
        /// </summary>
        /// <param name="ticket">The ticket to authenticate the user.</param>
        /// <param name="brokerId">The id of the broker to export from.</param>
        /// <returns>A result including the name of the file for download.</returns>
        [WebMethod]
        public BrokerExportResult ExportBrokerFromBrokerId(string ticket, Guid brokerId)
        {
            var options = new BrokerExportOptions()
            {
                BrokerId = brokerId,
                IncludeFileDbEntries = true,
                Compress = true,
                ExportType = ReplicationExportType.Broker,
                SpecificExportType = "NO_USER",
                LogDebugInfo = false
            };

            return this.ExportBrokerFromOptions(ticket, options);
        }

        /// <summary>
        /// Imports the xml to the database.
        /// </summary>
        /// <param name="ticket">The ticket to authenticate the user.</param>
        /// <param name="options">The options to use during the import.</param>
        /// <returns>A result indicating how the import went.</returns>
        [WebMethod]
        public BrokerImportResult ImportXml(string ticket, BrokerImportOptions options)
        {
            var principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);

            return Tools.LogAndThrowIfErrors<BrokerImportResult>(() => BrokerReplicator.ImportBrokerXml(principal, options));
        }

        /// <summary>
        /// Creates a file in the temp directory with the specified number of bytes so a user then use UploadFileChunk to upload a file.
        /// </summary>
        /// <param name="ticket">The ticket that authenticates the user.</param>
        /// <param name="numBytes">The number of bytes to allocate for the file.</param>
        /// <returns>A result containing any errors as well as the file name.</returns>
        [WebMethod]
        public TempFileCreationResult CreateFile(string ticket, long numBytes)
        {
            var principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);

            return Tools.LogAndThrowIfErrors(() => TempFileManager.CreateFile(principal, numBytes));
        }

        /// <summary>
        /// Uploads a chunk of bytes to a file that the user has access to in the temp directory.
        /// </summary>
        /// <param name="ticket">The authentication ticket of the user.</param>
        /// <param name="fileName">The filename relative to the temporary directory.</param>
        /// <param name="offset">The offset in bytes from the beginning of the file.</param>
        /// <param name="bytes">The byte array to write to the file starting at offset.</param>
        /// <returns>A result containing any errors.</returns>
        [WebMethod]
        public TempFileUploadResult UploadFileChunk(string ticket, string fileName, long offset, byte[] bytes)
        {
            var principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);

            return Tools.LogAndThrowIfErrors(() => TempFileManager.WriteChunkToFile(principal, fileName, offset, bytes));
        }

        /// <summary>
        /// Sends the bytes from the file which must be in c:\LoTemp <para></para>
        /// Only people with permission (from conststage) can download this information. <para></para>
        /// </summary>
        /// <param name="ticket">The authentication ticket of the user.</param>
        /// <param name="fileName">The filename relative to the temporary directory.</param>
        /// <param name="offset">The offset from the beginning of the file.</param>
        /// <param name="maxNumberOfBytes">The maximum number of bytes to download.</param>
        /// <returns>A result where the bytes are from the file, not to exceed maxNumberOfBytes.</returns>
        [WebMethod]
        public TempFileDownloadResult DownloadFileChunk(string ticket, string fileName, long offset, int maxNumberOfBytes)
        {
            var principal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);

            return Tools.LogAndThrowIfErrors(() => TempFileManager.GetChunkFromFile(principal, fileName, offset, maxNumberOfBytes));
        }
    }
}