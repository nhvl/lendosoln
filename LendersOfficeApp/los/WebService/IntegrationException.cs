///
/// Author: David Dao
///
namespace LendersOfficeApp.WebService
{
    using System;
    using DataAccess;

    public class IntegrationInvalidLoanNumberException: CBaseException 
    {
        public IntegrationInvalidLoanNumberException(string sLNm) : base("[" + sLNm + "] is an invalid loan number.", "Integration Error: [" + sLNm + "] is an invalid loan number.") 
        {
        }
        public override string EmailSubjectCode 
        {
            get { return "INTG_INVALID_LOAN"; }
        }
    }

}
