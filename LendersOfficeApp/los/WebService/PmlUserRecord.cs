﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;

    public class PmlUserRecord
    {
        private BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
        protected bool IsCreating = false;
        protected EmployeeImportSet.Desc employee;
        protected EmployeeDB employeeEditing;
        private BrokerUserPermissions buP;
        public ArrayList lendingLicenseXmlElements = new ArrayList();
        // ir - 8/11/2014 pmlNavigationXmlElements controls TPO Custom Portal Page Visibility
        public ArrayList pmlNavigationXmlElements = new ArrayList();
        private SortedDictionary<string, Guid> x_pmlNavigationOptions = null;
        private SortedDictionary<string, Guid> pmlNavigationOptions
        {
            get
            {
                if (x_pmlNavigationOptions == null)
                {
                    x_pmlNavigationOptions = new SortedDictionary<string, Guid>(StringComparer.InvariantCultureIgnoreCase);
                    IEnumerable<Tuple<Guid, string>> TpoNavigationPermissions = principal.BrokerDB.GetPmlNavigationConfiguration(principal).GetPageNodeNames();
                    foreach (var entry in TpoNavigationPermissions)
                    {
                        x_pmlNavigationOptions.Add(entry.Item2, entry.Item1); //Tranposition is intentional
                    }
                }
                return x_pmlNavigationOptions;
            }
        }
        private SortedDictionary<string, List<PmlBroker>> x_PmlCompanies = null;
        private SortedDictionary<string, List<PmlBroker>> CurrentPmlBrokers
        {
            get
            {
                if (x_PmlCompanies == null)
                {
                    var pmlBrokers = PmlBroker.ListAllPmlBrokerByBrokerId(principal.BrokerId);
                    x_PmlCompanies = new SortedDictionary<string, List<PmlBroker>>(StringComparer.OrdinalIgnoreCase);
                    foreach (var pmlBroker in pmlBrokers)
                    {
                        string pmlBrokerName = pmlBroker.Name.TrimWhitespaceAndBOM();

                        if (x_PmlCompanies.ContainsKey(pmlBrokerName))
                        {
                            x_PmlCompanies[pmlBrokerName].Add(pmlBroker);
                        }
                        else
                        {   
                            List<PmlBroker> newList = new List<PmlBroker>();
                            newList.Add(pmlBroker);
                            x_PmlCompanies.Add(pmlBrokerName,newList);
                        }

                    }
                }
                return x_PmlCompanies;
            }
        }


        private Dictionary<string, Guid> cachedBranchIDByName = null;
        protected virtual Dictionary<string, Guid> branchIDByName
        {
            get
            {
                if (this.cachedBranchIDByName == null)
                {
                    this.cachedBranchIDByName = new Dictionary<string, Guid>();

                    SqlParameter[] parameters = 
                    {
                        new SqlParameter("@BrokerId", principal.BrokerId)
                    };

                    using (IDataReader sR = StoredProcedureHelper.ExecuteReader(
                        principal.BrokerId,
                        "ListBranchByBrokerId",
                        parameters))
                    {
                        while (sR.Read())
                        {
                            var lowerCaseBranchName = sR["Name"].ToString().TrimWhitespaceAndBOM().ToLower();

                            if (!this.cachedBranchIDByName.ContainsKey(lowerCaseBranchName))
                            {
                                this.cachedBranchIDByName.Add(lowerCaseBranchName, (Guid)sR["BranchId"]);
                            }
                        }
                    }
                }

                return cachedBranchIDByName;
            }
        }

        private Dictionary<string, Guid> cachedPriceGroupIDByName = null;
        protected virtual Dictionary<string, Guid> priceGroupIDByName
        {
            get
            {
                if (this.cachedPriceGroupIDByName == null)
                {
                    this.cachedPriceGroupIDByName = new Dictionary<string, Guid>();

                    SqlParameter[] parameters = 
                    {
                        new SqlParameter("@BrokerId", principal.BrokerId)
                    };

                    using (IDataReader sR = StoredProcedureHelper.ExecuteReader(
                        principal.BrokerId,
                        "ListPricingGroupByBrokerId",
                        parameters))
                    {
                        while (sR.Read())
                        {
                            var lowerCasePriceGroupName = sR["LpePriceGroupName"].ToString().TrimWhitespaceAndBOM().ToLower();

                            if (!this.cachedPriceGroupIDByName.ContainsKey(lowerCasePriceGroupName))
                            {
                                this.cachedPriceGroupIDByName.Add(lowerCasePriceGroupName, (Guid)sR["LpePriceGroupId"]);
                            }
                        }
                    }
                }

                return this.cachedPriceGroupIDByName;
            }
        }

        private Dictionary<string, Guid> cachedLandingPages = null;
        protected virtual Dictionary<string, Guid> landingPages
        {
            get
            {
                if (this.cachedLandingPages == null)
                {
                    this.cachedLandingPages = new Dictionary<string, Guid>(StringComparer.OrdinalIgnoreCase);

                    foreach (var kvp in BrokerDB.RetrieveById(this.principal.BrokerId).GetLandingPages())
                    {
                        if (!this.cachedLandingPages.ContainsKey(kvp.Value))
                        {
                            this.cachedLandingPages.Add(kvp.Value, kvp.Key);
                        }
                    }
                }

                return this.cachedLandingPages;
            }
        }

        private BrokerLoanAssignmentTable cachedBrokerLoanAssignmentTable;
        protected virtual BrokerLoanAssignmentTable brokerLoanAssignmentTable
        {
            get
            {
                if (this.cachedBrokerLoanAssignmentTable == null)
                {
                    this.cachedBrokerLoanAssignmentTable = new BrokerLoanAssignmentTable();
                }

                return this.cachedBrokerLoanAssignmentTable;
            }
        }

        private Guid m_pmlBrokerId = Guid.Empty;

        public string NmlsIdentifier = null; // 3/2/2011 dd - Request for LON integration.
        public string OriginatorCompensationSetLevelT = null; //OPM 172995 - ir: Allow setting of compensation via Web Services
        public string OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = null;
        public string OriginatorCompensationPercent = null;
        public string OriginatorCompensationBaseT = null;
        public string OriginatorCompensationMinAmount = null;
        public string OriginatorCompensationMaxAmount = null;
        public string OriginatorCompensationFixedAmount = null;
        public string OriginatorCompensationNotes = null;
        //OPM 172995 - ir: Allow setting of custom pricing policy via Web Services
        public string CustomPricingPolicyField1Fixed = null;
        public string CustomPricingPolicyField2Fixed = null;
        public string CustomPricingPolicyField3Fixed = null;
        public string CustomPricingPolicyField4Fixed = null;
        public string CustomPricingPolicyField5Fixed = null;
        public string CustomPricingPolicyFieldValueSource = null;

        #region Variables - First tab of PML editor
        public string FirstName = null;
        public string LastName = null;
        public string Company = null;
        public string CompanyId = string.Empty; //OPM XXXXXX - ir: Broker may have duplicate Company names. Use CompanyID to resolve.
        public string BranchName = null;
        public string Address = null;
        public string City = null;
        public string State = null;
        public string Zipcode = null;
        public string Phone = null;
        public string Fax = null;
        public string CellPhone = null;
        public string IsCellphoneForMultiFactorOnly = null;
        public string Pager = null;
        public string Email = null;
        public string Notes = null;
        #endregion

        #region Variables - Second tab of PML editor
        public string AllowCreatingWholesaleChannelLoans = null;
        public string AllowCreatingMiniCorrespondentChannelLoans = null;
        public string AllowCreatingCorrespondentChannelLoans = null;
        public string AllowViewingCorrChannelLoans = null;
        public string AllowViewingMiniCorrChannelLoans = null;
        public string AllowViewingWholesaleChannelLoans = null;

        // OPM 242494, 4/19/2016, ML
        // We want to default to the OC's values to mirror the
        // PML user editor UI.
        public string PopulatePMLPermissionsT = null;
        public string CanApplyForIneligibleLoanPrograms = null;
        public string CanRunPricingWithoutCreditReport = null;
        public string CanSubmitLoansWithoutCreditReport = null;

        public string IsLoanOfficer = null;
        public string IsBrokerProcessor = null;
        public string IsSupervisor = null;
        public string SupervisedBy = null;
        public string IsActive = null;
        public string LoginName = null;
        public string Password = null;
        public string DOLoginName = null;
        public string DOPassword = null;
        public string DULoginName = null;
        public string DUPassword = null;
        public string LoanAccessLevel = null;
        public string IsExternalSecondary = null;
        public string IsExternalPostCloser = null;

        // 0 = Must change at next login
        // 1 = Never expires
        // 2 = Expires on [date]
        public string PasswordExpirationOption = null;
        public string PasswordExpirationValue = null;
        public string ExpirePasswordsEveryXDays = null;
        #endregion

        #region Variables - Relationships
        public string PopulateBrokerRelationshipsT = null;
        public string Manager = null;
        public string LockDesk = null;
        public string AccountExecutive = null;
        public string Underwriter = null;
        public string JuniorUnderwriter = null;
        public string Processor = null;
        public string JuniorProcessor = null;
        public string PriceGroup = null;
        public string BrokerProcessor = null;
        public string MiniCorrespondentPriceGroup = null;
        public string CorrespondentPriceGroup = null;

        public string TPOLandingPage = null;
        public string MiniCorrespondentLandingPage = null;
        public string CorrespondentLandingPage = null;

        public string PopulateMiniCorrRelationshipsT = null;
        public string MiniCorrespondentManager = null;
        public string MiniCorrespondentProcessor = null;
        public string MiniCorrespondentJuniorProcessor = null;
        public string MiniCorrespondentLenderAccExec = null;
        public string MiniCorrespondentExternalPostCloser = null;
        public string MiniCorrespondentUnderwriter = null;
        public string MiniCorrespondentJuniorUnderwriter = null;
        public string MiniCorrespondentCreditAuditor = null;
        public string MiniCorrespondentLegalAuditor = null;
        public string MiniCorrespondentLockDesk = null;
        public string MiniCorrespondentPurchaser = null;
        public string MiniCorrespondentSecondary = null;
        public string MiniCorrespondentBranchName = null;

        public string PopulateCorrRelationshipsT = null;
        public string CorrespondentManager = null;
        public string CorrespondentProcessor = null;
        public string CorrespondentJuniorProcessor = null;
        public string CorrespondentLenderAccExec = null;
        public string CorrespondentExternalPostCloser = null;
        public string CorrespondentUnderwriter = null;
        public string CorrespondentJuniorUnderwriter = null;
        public string CorrespondentCreditAuditor = null;
        public string CorrespondentLegalAuditor = null;
        public string CorrespondentLockDesk = null;
        public string CorrespondentPurchaser = null;
        public string CorrespondentSecondary = null;
        public string CorrespondentBranchName = null;
        #endregion

        public string PortalMode = null;

        //These next 4 functions are just here in case we decide to change how we handle ignored vs deleted
        //null = ignore, empty = delete
        private bool IsValueBeingDeleted(string val)
        {
            if ((val != null) && (val.TrimWhitespaceAndBOM().Equals("")))
            {
                return true;
            }

            return false;
        }

        private bool IsValueSetToEmpty(string val)
        {
            if (IsCreating)
            {
                return !IsValueSet(val);
            }
            else
            {
                return IsValueBeingDeleted(val);
            }
        }

        private bool IsValueBeingIgnored(string val)
        {
            if (val == null)
            {
                return true;
            }

            return false;
        }

        //null = ignore, empty = delete, but either counts for "not set"
        private bool IsValueSet(string val)
        {
            if ((val == null) || (val.TrimWhitespaceAndBOM().Equals("")))
            {
                return false;
            }

            return true;
        }

        private bool IsValueTrue(string val)
        {
            if (val == null)
            {
                return false;
            }

            string localVal = val.TrimWhitespaceAndBOM().ToLower();
            try
            {
                if (localVal.Equals("y") || localVal.Equals("yes") || localVal.Equals("true") || localVal.Equals("t"))
                {
                    return true;
                }
                else if (Int32.Parse(localVal) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the portal mode. Check's the PML Users permissions before doing so.
        /// </summary>
        /// <returns>The result of this operation as a string.</returns>
        private string SetPortalMode()
        {
            if (this.PortalMode == null)
            {
                return "";
            }

            string genericPortalModeError = "Unable to set user's portal mode.";

            if (this.employeeEditing == null)
            {
                return genericPortalModeError;
            }

            if (this.buP == null)
            {
                try
                {
                    buP = new BrokerUserPermissions(employeeEditing.BrokerID, employeeEditing.ID);
                }
                catch (SqlException e)
                {
                    Tools.LogError("Unable to get user permissions.", e);
                    return genericPortalModeError;
                }
                catch (CBaseException e)
                {
                    Tools.LogError("Unable to get user permissions.", e);
                    return genericPortalModeError;
                }
            }

            E_PortalMode newMode;
            if (!Enum.TryParse<E_PortalMode>(this.PortalMode, true, out newMode))
            {
                Tools.LogError("Invalid portal mode: " + this.PortalMode);
                return "Unknown portal mode specified.";
            }

            Permission permissionToCheck;
            string portalAsString;
            switch (newMode)
            {
                case E_PortalMode.Broker:
                    portalAsString = "Broker";
                    permissionToCheck = Permission.AllowViewingWholesaleChannelLoans;
                    break;
                case E_PortalMode.Correspondent:
                    portalAsString = "Correspondent";
                    permissionToCheck = Permission.AllowViewingCorrChannelLoans;
                    break;
                case E_PortalMode.MiniCorrespondent:
                    portalAsString = "Mini-Correspondent";
                    permissionToCheck = Permission.AllowViewingMiniCorrChannelLoans;
                    break;
                case E_PortalMode.Blank:
                default:
                    Tools.LogError("Invalid portal mode: " + this.PortalMode);
                    return "Unknown portal mode specified.";
            }

            if (!this.buP.HasPermission(permissionToCheck))
            {
                return "PML User does not have permissions for the " + portalAsString + " portal mode.";
            }

            employeeEditing.PortalMode = newMode;
            return string.Empty;
        }

        private string HasAllRequiredFields()
        {
            StringBuilder missingFields = new StringBuilder();

            if (IsValueSetToEmpty(FirstName))
            {
                missingFields.Append((missingFields.Length != 0) ? ", First Name" : "First Name");
            }

            if (IsValueSetToEmpty(LastName))
            {
                missingFields.Append((missingFields.Length != 0) ? ", Last Name" : "Last Name");
            }

            if (principal.Type == "B" && IsValueSetToEmpty(Company))
            {
                missingFields.Append((missingFields.Length != 0) ? ", Company Name" : "Company Name");
            }

            if (IsValueSetToEmpty(Phone))
            {
                missingFields.Append((missingFields.Length != 0) ? ", Phone" : "Phone");
            }

            if (IsValueSetToEmpty(Email))
            {
                missingFields.Append((missingFields.Length != 0) ? ", Email" : "Email");
            }

            if (IsValueSetToEmpty(LoginName))
            {
                missingFields.Append((missingFields.Length != 0) ? ", Login Name" : "Login Name");
            }

            if (IsValueSetToEmpty(Password))
            {
                missingFields.Append((missingFields.Length != 0) ? ", Password" : "Password");
            }

            if (IsValueSetToEmpty(LoanAccessLevel))
            {
                missingFields.Append((missingFields.Length != 0) ? ", Loan Access Level" : "Loan Access Level");
            }

            if (this.IsCreating)
            {
                if (!this.IsValueSet(this.PopulateBrokerRelationshipsT) ||
                    (this.PopulateBrokerRelationshipsT != "OriginatingCompany" && this.PopulateBrokerRelationshipsT != "0" &&
                    this.PopulateBrokerRelationshipsT != "IndividualUser" && this.PopulateBrokerRelationshipsT != "1"))
                {
                    missingFields.Append((missingFields.Length != 0) ? ", PopulateBrokerRelationshipsT" : "PopulateBrokerRelationshipsT");
                }

                if (!this.IsValueSet(this.PopulateCorrRelationshipsT) ||
                    (this.PopulateCorrRelationshipsT != "OriginatingCompany" && this.PopulateCorrRelationshipsT != "0" &&
                    this.PopulateCorrRelationshipsT != "IndividualUser" && this.PopulateCorrRelationshipsT != "1"))
                {
                    missingFields.Append((missingFields.Length != 0) ? ", PopulateCorrRelationshipsT" : "PopulateCorrRelationshipsT");
                }

                if (!this.IsValueSet(this.PopulateMiniCorrRelationshipsT) ||
                    (this.PopulateMiniCorrRelationshipsT != "OriginatingCompany" && this.PopulateMiniCorrRelationshipsT != "0" &&
                    this.PopulateMiniCorrRelationshipsT != "IndividualUser" && this.PopulateMiniCorrRelationshipsT != "1"))
                {
                    missingFields.Append((missingFields.Length != 0) ? ", PopulateMiniCorrRelationshipsT" : "PopulateMiniCorrRelationshipsT");
                }

                if (!this.IsValueSet(this.PopulatePMLPermissionsT) ||
                    (this.PopulatePMLPermissionsT != "OriginatingCompany" && this.PopulatePMLPermissionsT != "0" &&
                    this.PopulatePMLPermissionsT != "IndividualUser" && this.PopulatePMLPermissionsT != "1"))
                {
                    missingFields.Append((missingFields.Length != 0) ? ", PopulatePMLPermissionsT" : "PopulatePMLPermissionsT");
                }
            }

            string missingRole = principal.Type == "P" ? 
                "At least one role (IsLoanOfficer, IsBrokerProcessor, IsExternalSecondary, IsExternalPostCloser) must be true." : 
                "At least one role (IsLoanOfficer, IsBrokerProcessor, IsExternalSecondary, IsExternalPostCloser, IsSupervisor) must be true.";

            if (IsCreating)
            {
                if ((principal.Type == "P" ? true : !IsValueTrue(IsSupervisor)) && !IsValueTrue(IsLoanOfficer) && !IsValueTrue(IsBrokerProcessor) && !IsValueTrue(IsExternalSecondary) && IsValueTrue(IsExternalPostCloser))
                {
                    missingFields.Append((missingFields.Length != 0) ? ", " + missingRole : missingRole);
                }
            }
            else
            {
                EmployeeRoles roles = new EmployeeRoles(principal.BrokerId, employeeEditing.ID);
                bool userWillBeSupervisor = IsValueSet(IsSupervisor) ? IsValueTrue(IsSupervisor) : employeeEditing.IsPmlManager;
                bool userWillBeLO = IsValueSet(IsLoanOfficer) ? IsValueTrue(IsLoanOfficer) : roles.IsInRole(CEmployeeFields.s_LoanRepRoleId);
                bool userWillBeBP = IsValueSet(IsBrokerProcessor) ? IsValueTrue(IsBrokerProcessor) : roles.IsInRole(CEmployeeFields.s_BrokerProcessorId);
                bool userWillBeExternalSecondary = IsValueSet(IsExternalSecondary) ? IsValueTrue(IsExternalSecondary) : roles.IsInRole(CEmployeeFields.s_ExternalSecondaryId);
                bool userWillBeExternalPostCloser = IsValueSet(IsExternalPostCloser) ? IsValueTrue(IsExternalPostCloser) : roles.IsInRole(CEmployeeFields.s_ExternalPostCloserId);
                if (!userWillBeSupervisor && !userWillBeLO && !userWillBeBP &&
                    !userWillBeExternalSecondary && !userWillBeExternalPostCloser)
                {
                    missingFields.Append((missingFields.Length != 0) ? ", " + missingRole : missingRole);
                }
            }

            if (missingFields.Length != 0)
            {
                missingFields.Insert(0, "Missing required fields: ");
            }

            return missingFields.ToString();
        }

        private string SetLoginName()
        {
            if (!IsValueSet(LoginName))
            {
                return "";
            }

            //If the user is "setting" the login name to the previously existing login name, we don't need to do anything
            if (!IsCreating && (employeeEditing.LoginName.Equals(LoginName)))
            {
                return "";
            }

            BrokerEmployeeLoginSet logIns = new BrokerEmployeeLoginSet();
            try
            {
                logIns.Retrieve(principal.BrokerId, "P");
            }
            catch { }
           
            if (logIns.IsValid(LoginName, "P") == true)
            {
                return "Username already taken";
            }

            if (IsCreating)
            {
                employee.Login = LoginName;
            }
            else
            {
                employeeEditing.LoginName = LoginName;
            }
            return "";
        }

        private string SetOptions()
        {
            var useOcSettings = this.PopulatePMLPermissionsT == "OriginatingCompany" ||
                this.PopulatePMLPermissionsT == "0";

            if (useOcSettings)
            {
                var errorMessage = new StringBuilder();

                if (this.IsValueSet(this.CanApplyForIneligibleLoanPrograms))
                {
                    errorMessage.Append("CanApplyForIneligibleLoanPrograms ");
                }

                if(this.IsValueSet(this.CanRunPricingWithoutCreditReport))
                {
                    errorMessage.Append("CanRunPricingWithoutCreditReport ");
                }

                if (this.IsValueSet(this.CanSubmitLoansWithoutCreditReport))
                {
                    errorMessage.Append("CanSubmitLoansWithoutCreditReport ");
                }

                if (errorMessage.Length != 0)
                {
                    return "PML permissions are determined by originating company. Please exclude these properties: " + errorMessage.ToString();
                }
            }

            if (IsCreating)
            {
                employee.CreateWholesaleChannelLoans = IsValueTrue(AllowCreatingWholesaleChannelLoans) ? "y" : "n";
                employee.CreateMiniCorrespondentChannelLoans = IsValueTrue(AllowCreatingMiniCorrespondentChannelLoans) ? "y" : "n";
                employee.CreateCorrespondentChannelLoans = IsValueTrue(AllowCreatingCorrespondentChannelLoans) ? "y" : "n";
                employee.AllowViewingWholesaleChannelLoans = IsValueTrue(AllowViewingWholesaleChannelLoans) ? "y" : "n";
                employee.AllowViewingCorrChannelLoans = IsValueTrue(AllowViewingCorrChannelLoans) ? "y" : "n";
                employee.AllowViewingMiniCorrChannelLoans = IsValueTrue(AllowViewingMiniCorrChannelLoans) ? "y" : "n";

                if (useOcSettings)
                {
                    return "";
                }

                employee.ApplyForIneligibleLoanPrograms = IsValueTrue(CanApplyForIneligibleLoanPrograms) ? "y" : "n";
                employee.RunWithoutReport = IsValueTrue(CanRunPricingWithoutCreditReport) ? "y" : "n";

                //must have the can read permission in order to set the can submit permission to true
                if (!IsValueTrue(CanRunPricingWithoutCreditReport) && IsValueTrue(CanSubmitLoansWithoutCreditReport))
                {
                    return "The permission CanRunPricingWithoutCreditReport must be true in order to set the permission CanSubmitLoansWithoutCreditReport to true";
                }

                if (IsValueTrue(CanRunPricingWithoutCreditReport) && IsValueTrue(CanSubmitLoansWithoutCreditReport))
                {
                    employee.SubmitWithoutReport = "y";
                }
                else
                {
                    employee.SubmitWithoutReport = "n";
                }
                return "";
            }
            else
            {
                if (buP == null)
                {
                    return "";
                }

                if (!IsValueBeingIgnored(AllowCreatingWholesaleChannelLoans))
                {
                    buP.SetPermission(Permission.AllowCreatingWholesaleChannelLoans, IsValueTrue(AllowCreatingWholesaleChannelLoans));
                }

                if (!IsValueBeingIgnored(AllowCreatingMiniCorrespondentChannelLoans))
                {
                    buP.SetPermission(Permission.AllowCreatingMiniCorrChannelLoans, IsValueTrue(AllowCreatingMiniCorrespondentChannelLoans));
                }

                if (!IsValueBeingIgnored(AllowCreatingCorrespondentChannelLoans))
                {
                    buP.SetPermission(Permission.AllowCreatingCorrChannelLoans, IsValueTrue(AllowCreatingCorrespondentChannelLoans));
                }

                if (!IsValueBeingIgnored(AllowViewingWholesaleChannelLoans))
                {
                    buP.SetPermission(Permission.AllowViewingWholesaleChannelLoans, IsValueTrue(AllowViewingWholesaleChannelLoans));
                }

                if (!IsValueBeingIgnored(AllowViewingCorrChannelLoans))
                {
                    buP.SetPermission(Permission.AllowViewingCorrChannelLoans, IsValueTrue(AllowViewingCorrChannelLoans));
                }

                if (!IsValueBeingIgnored(AllowViewingMiniCorrChannelLoans))
                {
                    buP.SetPermission(Permission.AllowViewingMiniCorrChannelLoans, IsValueTrue(AllowViewingMiniCorrChannelLoans));
                }

                if (useOcSettings)
                {
                    return "";
                }

                if (!IsValueBeingIgnored(CanApplyForIneligibleLoanPrograms))
                {
                    buP.SetPermission(Permission.CanApplyForIneligibleLoanPrograms, IsValueTrue(CanApplyForIneligibleLoanPrograms));
                }

                if (!IsValueBeingIgnored(CanRunPricingWithoutCreditReport))
                {
                    buP.SetPermission(Permission.CanRunPricingEngineWithoutCreditReport, IsValueTrue(CanRunPricingWithoutCreditReport));
                }

                //If the user does not have the CanRunPricingEngineWithoutCreditReport permission,
                //they cannot have the CanSubmitWithoutCreditReport permission
                if (buP.HasPermission(Permission.CanRunPricingEngineWithoutCreditReport))
                {
                    if (IsValueBeingIgnored(CanSubmitLoansWithoutCreditReport))
                    {
                        return "";
                    }

                    buP.SetPermission(Permission.CanSubmitWithoutCreditReport, IsValueTrue(CanSubmitLoansWithoutCreditReport));
                }
                else
                {
                    buP.SetPermission(Permission.CanSubmitWithoutCreditReport, false);
                    if (IsValueTrue(CanSubmitLoansWithoutCreditReport) || (buP.HasPermission(Permission.CanRunPricingEngineWithoutCreditReport) && IsValueBeingIgnored(CanSubmitLoansWithoutCreditReport)))
                    {
                        return "The permission CanRunPricingWithoutCreditReport must be true in order to set the permission CanSubmitLoansWithoutCreditReport to true";
                    }
                }

                return "";
            }
        }

        private string SetPassword()
        {
            //If the value is being set to blank, it will get flagged in HasAllRequiredFields
            if (!IsValueSet(Password))
            {
                return "";
            }

            //8/20/07 - removed password restrictions since other systems might not have the same restrictions
            if (IsCreating)
                employee.Password = Password;
            else
                employeeEditing.Password = Password;
            return "";
        }

        private string SetPasswordExpiresEveryXDays()
        {
            if (IsValueSetToEmpty(ExpirePasswordsEveryXDays))
            {
                if (!IsCreating)
                {
                    employeeEditing.PasswordExpirationPeriod = 0;
                }
                else
                {
                    employee.PasswordExpiresEvery = 0;
                }

                return "";
            }
            
            if (!IsCreating && IsValueBeingIgnored(ExpirePasswordsEveryXDays))
            {
                return "";
            }
            
            try
            {
                int expiresEvery = Int32.Parse(ExpirePasswordsEveryXDays.TrimWhitespaceAndBOM());
                if ((expiresEvery == 0) || (expiresEvery == 15) || (expiresEvery == 30) || (expiresEvery == 45) || (expiresEvery == 60))
                {
                    if (IsCreating)
                    {
                        //Only set this value if it's not set to password never expires
                        if (!employee.PasswordExpirationDate.Equals(SmallDateTime.MaxValue))
                        {
                            employee.PasswordExpiresEvery = expiresEvery;
                        }
                    }
                    else
                    {
                        //Only set this value if it's not set to password never expires
                        if (!employeeEditing.PasswordExpirationD.Equals(SmallDateTime.MaxValue))
                        {
                            employeeEditing.PasswordExpirationPeriod = expiresEvery;
                        }
                    }
                }
                else
                {
                    return "ExpirePasswordsEveryXDays must have a value of 0, 15, 30, 45, or 60";
                }
            }
            catch (System.FormatException)
            {
                return "Invalid integer format for ExpirePasswordsEveryXDays";
            }

            return "";
        }

        private string SetPasswordOptions()
        {
            if (IsValueBeingIgnored(PasswordExpirationOption))
            {
                if (IsCreating)
                {
                    employee.PasswordExpirationDate = SmallDateTime.MinValue;
                }
                return SetPasswordExpiresEveryXDays();
            }

            if (IsValueSetToEmpty(PasswordExpirationOption))
            {
                return "PasswordExpirationOption must have a value of 0, 1, or 2";
            }

            try
            {
                int pwOption = Int32.Parse(PasswordExpirationOption.TrimWhitespaceAndBOM());

                // 0 = Must change at next login
                // 1 = Never expires
                // 2 = Expires on [date]
                switch (pwOption)
                {
                    case 0:
                        if (IsCreating)
                        {
                            employee.PasswordExpirationDate = SmallDateTime.MinValue;
                        }
                        else
                        {
                            employeeEditing.PasswordExpirationD = SmallDateTime.MinValue;
                        }
                        return SetPasswordExpiresEveryXDays();
                    case 1:
                        if (IsCreating)
                        {
                            employee.PasswordExpirationDate = SmallDateTime.MaxValue;
                        }
                        else
                        {
                            employeeEditing.PasswordExpirationD = SmallDateTime.MaxValue;
                        }
                        return "";
                    case 2:
                        if (!IsValueSet(PasswordExpirationValue))
                        {
                            return "Missing the date attribute for PasswordExpirationOption";
                        }
                        DateTime expDate;
                        try
                        {
                            expDate = DateTime.Parse(PasswordExpirationValue);
                            if (IsCreating)
                            {
                                employee.PasswordExpirationDate = expDate;
                            }
                            else
                            {
                                employeeEditing.PasswordExpirationD = expDate;
                            }
                            return SetPasswordExpiresEveryXDays();
                        }
                        catch (System.FormatException)
                        {
                            return "Invalid date format for PasswordExpirationOption value attribute";
                        }
                        catch (System.ArgumentOutOfRangeException)
                        {
                            return "Invalid date for PasswordExpirationOption value attribute";
                        }
                    default:
                        return "Invalid option for PasswordExpirationOption";
                }
            }
            catch (System.FormatException)
            {
                return "Invalid integer format for PasswordExpiration";
            }
        }

        private string SetEmail()
        {
            if (IsValueSet(Email))
            {
                string strRegex = ConstApp.EmailValidationExpression;
                Regex re = new Regex(strRegex);
                if (re.IsMatch(Email))
                {
                    if (IsCreating)
                    {
                        employee.Email = Email;
                    }
                    else
                    {
                        employeeEditing.Email = Email;
                    }
                }
                else
                {
                    return "Invalid Email format";
                }
            }
            return "";
        }

        private string SetSupervisorInfo()
        {
            bool isNowSupervisor;

            //First set IsSupervisor
            if (IsCreating)
            {
                isNowSupervisor = IsValueTrue(IsSupervisor);
                employee.IsSupervisor = isNowSupervisor;
            }
            else
            {
                if (IsValueBeingIgnored(IsSupervisor))
                {
                    isNowSupervisor = employeeEditing.IsPmlManager;
                }
                else
                {
                    isNowSupervisor = IsValueTrue(IsSupervisor);
                    employeeEditing.IsPmlManager = isNowSupervisor;
                }
            }

            //If the user is not a supervisor, set who they are supervised by, if anyone
            if (!isNowSupervisor)
            {
                if (IsValueSetToEmpty(SupervisedBy))
                {
                    if (IsCreating)
                    {
                        employee.SupervisorId = Guid.Empty;
                    }
                    else
                    {
                        employeeEditing.PmlExternalManagerEmployeeId = Guid.Empty;
                    }
                    return "";
                }

                if (!IsCreating && IsValueBeingIgnored(SupervisedBy))
                {
                    return "";
                }

                Hashtable m_SupSet = new Hashtable();
                int nCountMatches = 0;

                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", principal.BrokerId)
                                            };

                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "ListPriceMyLoanManagerByBrokerId", parameters ))
                {
                    while (sR.Read())
                    {
                        try
                        {
                            if (sR["UserFullName"].ToString().TrimWhitespaceAndBOM().ToLower().Equals(SupervisedBy.TrimWhitespaceAndBOM().ToLower()))
                            {
                                ++nCountMatches;
                            }
                            m_SupSet.Add(sR["UserFullName"].ToString().TrimWhitespaceAndBOM().ToLower(), sR["EmployeeId"]);
                        }
                        catch { }
                    }
                }

                if (nCountMatches > 1)
                {
                    return "Multiple matches for Supervisor";
                }
                else if (m_SupSet.Contains(SupervisedBy.TrimWhitespaceAndBOM().ToLower()) == true)
                {
                    if (IsCreating)
                    {
                        employee.SupervisorId = (Guid)m_SupSet[SupervisedBy.TrimWhitespaceAndBOM().ToLower()];
                    }
                    else
                    {
                        employeeEditing.PmlExternalManagerEmployeeId = (Guid)m_SupSet[SupervisedBy.TrimWhitespaceAndBOM().ToLower()];
                    }
                }
                else
                {
                    return "Supervisor not found";
                }
            }
            else
            {
                if (!IsCreating)
                {
                    employeeEditing.PmlExternalManagerEmployeeId = Guid.Empty;
                }
            }
            return "";
        }

        private string SetLendingLicenseInfo()
        {
            foreach (XmlElement el in lendingLicenseXmlElements)
            {
                string licenseNum = el.GetAttribute("LicenseNumber");

                if (!IsValueSet(licenseNum))
                {
                    return "The LicenseNumber attribute must be set for all Lending Licenses";
                }

                if (licenseNum.Length > 30)
                {
                    return String.Format("Lending License {0} has exceeded the maximum length (30) allowed for a license number", licenseNum);
                }

                if (!el.HasAttribute("Action"))
                {
                    return String.Format("The Action attribute is missing for Lending License {0}", licenseNum);
                }

                string licenseAction = el.GetAttribute("Action");
                int action;

                try
                {
                    action = Int32.Parse(licenseAction);
                }
                catch (FormatException)
                {
                    return String.Format("Invalid integer format for the Action attribute of Lending License {0}", licenseNum);
                }

                string licenseState = null;
                if (el.HasAttribute("State"))
                {
                    licenseState = el.GetAttribute("State").Replace("'", "").Replace("\"", "");
                }

                string expDate = null;
                if (el.HasAttribute("ExpirationDate"))
                {
                    expDate = el.GetAttribute("ExpirationDate").Replace("'", "").Replace("\"", "");
                }

                if (IsValueSet(expDate))
                {
                    try
                    {
                        DateTime.Parse(expDate);
                    }
                    catch (FormatException)
                    {
                        return String.Format("Invalid date format for the ExpirationDate attribute of lending license {0}", licenseNum);
                    }
                    catch (System.ArgumentOutOfRangeException)
                    {
                        return String.Format("Invalid date for the ExpirationDate attribute of lending license {0}", licenseNum);
                    }
                }

                //0 - Add
                //1 - Update
                //2 - Delete
                switch (action)
                {
                    case 0:
                        if (IsCreating)
                        {
                            if (employee.LicenseInformationList.IsFull)
                            {
                                return String.Format("Cannot add lending license {0} - You have entered the maximum (25) number of licenses", licenseNum);
                            }
                            employee.LicenseInformationList.Add(new LicenseInfo(licenseState, expDate, licenseNum));
                        }
                        else
                        {
                            if (employeeEditing.LicenseInformationList.IsFull)
                            {
                                return String.Format("Cannot add lending license {0} - You have entered the maximum (25) number of licenses", licenseNum);
                            }
                            employeeEditing.LicenseInformationList.Add(new LicenseInfo(licenseState, expDate, licenseNum));
                        }
                        break;
                    case 1:
                        if (IsCreating)
                        {
                            return "Lending licenses can only be added, not updated or deleted, when creating new users";
                        }

                        if (employeeEditing.LicenseInformationList.IsDuplicateLicenseNum(licenseNum))
                        {
                            return String.Format("Cannot update lending license {0} because there are multiple licenses with the same license number", licenseNum);
                        }
                        else if (!employeeEditing.LicenseInformationList.Exists(licenseNum))
                        {
                            return String.Format("Cannot update lending license {0} - License number not found", licenseNum);
                        }
                        else
                        {
                            employeeEditing.LicenseInformationList.UpdateLicense(licenseNum, licenseState, expDate);
                        }
                        break;
                    case 2:
                        if (IsCreating)
                        {
                            return "Lending Licenses can only be added, not updated or deleted, when creating new users";
                        }

                        if (employeeEditing.LicenseInformationList.IsDuplicateLicenseNum(licenseNum))
                        {
                            return String.Format("Cannot delete lending license {0} because there are multiple licenses with the same license number", licenseNum);
                        }
                        else
                        {
                            employeeEditing.LicenseInformationList.DeleteLicense(licenseNum);
                        }
                        break;
                    default:
                        return String.Format("The action attribute LendingLicense {0} must contain a value of 0, 1, or 2", licenseNum);
                }
            }
            return "";
        }

        private bool IsSpecifyingOrginatingCompanyForRelationships(string relationshipTXml)
        {
            return relationshipTXml.Equals("OriginatingCompany", StringComparison.OrdinalIgnoreCase) || relationshipTXml == "0";
        }

        private bool IsSpecifyingIndividualForRelationships(string relationshipTXml)
        {
            return relationshipTXml.Equals("IndividualUser", StringComparison.OrdinalIgnoreCase) || relationshipTXml == "1";
        }

        private string SetPmlBrokerInformation()
        {
            string pmlBroker = string.Empty;
            PmlBroker selectedPmlBroker = null;
            if (this.IsCreating)
            {
                if (string.IsNullOrEmpty(this.Company))
                {
                    return "Company Name cannot be blank.";
                }

                pmlBroker = this.Company.TrimWhitespaceAndBOM();
            }
            else
            {
                if (string.IsNullOrEmpty(this.Company))
                {
                    selectedPmlBroker = this.CurrentPmlBrokers.SelectMany((pair) => pair.Value).FirstOrDefault((pb) => pb.PmlBrokerId == this.employeeEditing.PmlBrokerId);
                    if (selectedPmlBroker == null)
                    {
                        return "Unable to obtain user's Company.";
                    }
                }
                else
                {
                    pmlBroker = this.Company.TrimWhitespaceAndBOM();
                }
            }

            if (selectedPmlBroker == null)
            {
                List<PmlBroker> pbList;
                if (this.CurrentPmlBrokers.TryGetValue(pmlBroker, out pbList))
                {
                    if (pbList.Count != 1)
                    {
                        bool pbFound = false;
                        foreach (PmlBroker pb in pbList)
                        {
                            // Need to fallback to company id if company name isn't unique enough.
                            if (this.CompanyId.Equals(pb.CompanyId))
                            {
                                if (pbFound)
                                {
                                    return $"There are multiple entries of Company Name - ID: {pmlBroker} - {this.CompanyId}";
                                }

                                m_pmlBrokerId = pb.PmlBrokerId;
                                selectedPmlBroker = pb;
                                pbFound = true;
                            }
                        }

                        if (!pbFound)
                        {
                            return $"Cannot find Company - CompanyID: {pmlBroker} - {this.CompanyId}";
                        }
                    }
                    else
                    {
                        selectedPmlBroker = pbList[0];
                    }
                }
                else
                {
                    return $"Invalid Company Name: {pmlBroker}";
                }
            }

            this.m_pmlBrokerId = selectedPmlBroker.PmlBrokerId;
            if (!string.IsNullOrEmpty(this.PopulateBrokerRelationshipsT))
            {
                if (this.IsSpecifyingOrginatingCompanyForRelationships(this.PopulateBrokerRelationshipsT))
                {
                    if (this.IsCreating)
                    {
                        this.employee.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.OriginatingCompany;
                        this.employee.ManagerId = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.Manager].Id;
                        this.employee.ProcessorId = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.Processor].Id;
                        this.employee.JuniorProcessorId = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.JuniorProcessor].Id;
                        this.employee.LenderAccountExecId = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.LenderAccountExecutive].Id;
                        this.employee.UnderwriterId = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.Underwriter].Id;
                        this.employee.JuniorUnderwriterId = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.JuniorUnderwriter].Id;
                        this.employee.LockDeskId = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.LockDesk].Id;
                    }
                    else
                    {
                        this.employeeEditing.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.OriginatingCompany;
                        this.employeeEditing.ManagerEmployeeID = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.Manager].Id;
                        this.employeeEditing.ProcessorEmployeeID = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.Processor].Id;
                        this.employeeEditing.JuniorProcessorEmployeeID = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.JuniorProcessor].Id;
                        this.employeeEditing.LenderAcctExecEmployeeID = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.LenderAccountExecutive].Id;
                        this.employeeEditing.UnderwriterEmployeeID = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.Underwriter].Id;
                        this.employeeEditing.JuniorUnderwriterEmployeeID = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.JuniorUnderwriter].Id;
                        this.employeeEditing.LockDeskEmployeeID = selectedPmlBroker.BrokerRoleRelationships[E_RoleT.LockDesk].Id;
                    }
                }
                else if (this.IsSpecifyingIndividualForRelationships(this.PopulateBrokerRelationshipsT))
                {
                    if (this.IsCreating)
                    {
                        this.employee.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser;
                    }
                    else
                    {
                        this.employeeEditing.PopulateBrokerRelationshipsT = EmployeePopulationMethodT.IndividualUser;
                    }
                }
                else
                {
                    return $"Invalid value for PopulateBrokerRelationshipsT: {this.PopulateBrokerRelationshipsT}";
                }
            }

            if (!string.IsNullOrEmpty(this.PopulateMiniCorrRelationshipsT))
            {
                if (this.IsSpecifyingOrginatingCompanyForRelationships(this.PopulateMiniCorrRelationshipsT))
                {
                    if (this.IsCreating)
                    {
                        this.employee.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.OriginatingCompany;
                        this.employee.MiniCorrespondentManagerId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Manager].Id;
                        this.employee.MiniCorrespondentProcessorId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Processor].Id;
                        this.employee.MiniCorrespondentJuniorProcessorId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.JuniorProcessor].Id;
                        this.employee.MiniCorrespondentLenderAccExecId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.LenderAccountExecutive].Id;
                        this.employee.MiniCorrespondentUnderwriterId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Underwriter].Id;
                        this.employee.MiniCorrespondentJuniorUnderwriterId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.JuniorUnderwriter].Id;
                        this.employee.MiniCorrespondentCreditAuditorId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.CreditAuditor].Id;
                        this.employee.MiniCorrespondentLegalAuditorId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.LegalAuditor].Id;
                        this.employee.MiniCorrespondentLockDeskId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.LockDesk].Id;
                        this.employee.MiniCorrespondentPurchaserId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Purchaser].Id;
                        this.employee.MiniCorrespondentSecondaryId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Secondary].Id;
                    }
                    else
                    {
                        this.employeeEditing.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.OriginatingCompany;
                        this.employeeEditing.MiniCorrManagerEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Manager].Id;
                        this.employeeEditing.MiniCorrProcessorEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Processor].Id;
                        this.employeeEditing.MiniCorrJuniorProcessorEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.JuniorProcessor].Id;
                        this.employeeEditing.MiniCorrLenderAccExecEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.LenderAccountExecutive].Id;
                        this.employeeEditing.MiniCorrUnderwriterEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Underwriter].Id;
                        this.employeeEditing.MiniCorrJuniorUnderwriterEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.JuniorUnderwriter].Id;
                        this.employeeEditing.MiniCorrCreditAuditorEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.CreditAuditor].Id;
                        this.employeeEditing.MiniCorrLegalAuditorEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.LegalAuditor].Id;
                        this.employeeEditing.MiniCorrLockDeskEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.LockDesk].Id;
                        this.employeeEditing.MiniCorrPurchaserEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Purchaser].Id;
                        this.employeeEditing.MiniCorrSecondaryEmployeeId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Secondary].Id;
                    }
                }
                else if (this.IsSpecifyingIndividualForRelationships(this.PopulateMiniCorrRelationshipsT))
                {
                    if (this.IsCreating)
                    {
                        this.employee.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;
                    }
                    else
                    {
                        this.employeeEditing.PopulateMiniCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;
                    }
                }
                else
                {
                    return $"Invalid value for PopulateMiniCorrRelationshipsT: {this.PopulateMiniCorrRelationshipsT}";
                }
            }

            if (!string.IsNullOrEmpty(this.PopulateCorrRelationshipsT))
            {
                if (this.IsSpecifyingOrginatingCompanyForRelationships(this.PopulateCorrRelationshipsT))
                {
                    if (this.IsCreating)
                    {
                        this.employee.PopulateCorrRelationshipsT = EmployeePopulationMethodT.OriginatingCompany;
                        this.employee.CorrespondentManagerId = selectedPmlBroker.MiniCorrRoleRelationships[E_RoleT.Manager].Id;
                        this.employee.CorrespondentProcessorId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.Processor].Id;
                        this.employee.CorrespondentJuniorProcessorId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.JuniorProcessor].Id;
                        this.employee.CorrespondentLenderAccExecId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.LenderAccountExecutive].Id;
                        this.employee.CorrespondentUnderwriterId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.Underwriter].Id;
                        this.employee.CorrespondentJuniorUnderwriterId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.JuniorUnderwriter].Id;
                        this.employee.CorrespondentCreditAuditorId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.CreditAuditor].Id;
                        this.employee.CorrespondentLegalAuditorId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.LegalAuditor].Id;
                        this.employee.CorrespondentLockDeskId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.LockDesk].Id;
                        this.employee.CorrespondentPurchaserId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.Purchaser].Id;
                        this.employee.CorrespondentSecondaryId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.Secondary].Id;
                    }
                    else
                    {
                        this.employeeEditing.PopulateCorrRelationshipsT = EmployeePopulationMethodT.OriginatingCompany;
                        this.employeeEditing.CorrManagerEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.Manager].Id;
                        this.employeeEditing.CorrProcessorEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.Processor].Id;
                        this.employeeEditing.CorrJuniorProcessorEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.JuniorProcessor].Id;
                        this.employeeEditing.CorrLenderAccExecEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.LenderAccountExecutive].Id;
                        this.employeeEditing.CorrUnderwriterEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.Underwriter].Id;
                        this.employeeEditing.CorrJuniorUnderwriterEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.JuniorUnderwriter].Id;
                        this.employeeEditing.CorrCreditAuditorEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.CreditAuditor].Id;
                        this.employeeEditing.CorrLegalAuditorEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.LegalAuditor].Id;
                        this.employeeEditing.CorrLockDeskEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.LockDesk].Id;
                        this.employeeEditing.CorrPurchaserEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.Purchaser].Id;
                        this.employeeEditing.CorrSecondaryEmployeeId = selectedPmlBroker.CorrRoleRelationships[E_RoleT.Secondary].Id;
                    }
                }
                else if (this.IsSpecifyingIndividualForRelationships(this.PopulateCorrRelationshipsT))
                {
                    if (this.IsCreating)
                    {
                        this.employee.PopulateCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;
                    }
                    else
                    {
                        this.employeeEditing.PopulateCorrRelationshipsT = EmployeePopulationMethodT.IndividualUser;
                    }
                }
                else
                {
                    return $"Invalid value for PopulateCorrRelationshipsT: {this.PopulateCorrRelationshipsT}";
                }
            }

            if (!string.IsNullOrEmpty(this.PopulatePMLPermissionsT))
            {
                if (this.IsSpecifyingOrginatingCompanyForRelationships(this.PopulatePMLPermissionsT))
                {
                    if (this.IsCreating)
                    {
                        this.employee.PopulatePMLPermissionsT = EmployeePopulationMethodT.OriginatingCompany;
                        this.employee.ApplyForIneligibleLoanPrograms = selectedPmlBroker.Permissions[Permission.CanApplyForIneligibleLoanPrograms] ? "y" : "n";
                        this.employee.RunWithoutReport = selectedPmlBroker.Permissions[Permission.CanRunPricingEngineWithoutCreditReport] ? "y" : "n";
                        this.employee.SubmitWithoutReport = selectedPmlBroker.Permissions[Permission.CanSubmitWithoutCreditReport] ? "y" : "n";
                    }
                    else
                    {
                        this.employeeEditing.PopulatePMLPermissionsT = EmployeePopulationMethodT.OriginatingCompany;

                        if (buP == null)
                        {
                            return "";
                        }

                        buP.SetPermission(Permission.CanApplyForIneligibleLoanPrograms, selectedPmlBroker.Permissions[Permission.CanApplyForIneligibleLoanPrograms]);
                        buP.SetPermission(Permission.CanRunPricingEngineWithoutCreditReport, selectedPmlBroker.Permissions[Permission.CanRunPricingEngineWithoutCreditReport]);
                        buP.SetPermission(Permission.CanSubmitWithoutCreditReport, selectedPmlBroker.Permissions[Permission.CanSubmitWithoutCreditReport]);
                    }
                }
                else if (this.IsSpecifyingIndividualForRelationships(this.PopulatePMLPermissionsT))
                {
                    if (this.IsCreating)
                    {
                        this.employee.PopulatePMLPermissionsT = EmployeePopulationMethodT.IndividualUser;
                    }
                    else
                    {
                        this.employeeEditing.PopulatePMLPermissionsT = EmployeePopulationMethodT.IndividualUser;
                    }
                }
                else
                {
                    return $"Invalid value for PopulatePMLPermissionsT: {this.PopulatePMLPermissionsT}";
                }
            }

            return "";
        }

        private string SetAccessiblePmlNavigationLinksId()
        {
            if (pmlNavigationXmlElements.Count == 0)
            {
                return string.Empty;
            }

            List<Guid> AccessiblePmlNavigationLinksIdList = null;
            if (IsCreating)
            {
                AccessiblePmlNavigationLinksIdList = new List<Guid>();
            }
            else
            {
                AccessiblePmlNavigationLinksIdList = new List<Guid>(employeeEditing.AccessiblePmlNavigationLinksId);
            }

            foreach (XmlElement el in pmlNavigationXmlElements)
            {
                string page = el.GetAttribute("Page");
                if (!IsValueSet(page))
                {
                    return "Page attribute must be defined for all PMLNavigation elements.";
                }

                if (!el.HasAttribute("Action"))
                {
                    return "Action attribute must be defined for all PMLNavigation elements.";
                }

                int action;
                if (int.TryParse(el.GetAttribute("Action"), out action) == false)
                {
                    return "Unable to parse Action from PMLNavigation element with page element: " + page;
                }

                Guid target;
                if (pmlNavigationOptions.TryGetValue(page,out target) == false)
                {
                    return string.Format("PMLNavigation Page {0} Not Found", page);
                }

                switch (action)
                {
                    case 0:
                        AccessiblePmlNavigationLinksIdList.Add(target);
                        break;
                    case 1:
                        if (IsCreating)
                        {
                            return "Cannot remove PML TPO Navigation pages at user creation";
                        }
                        else
                        {
                            AccessiblePmlNavigationLinksIdList.Remove(target);
                        }
                        break;
                    default:
                        return "PMLNavigation Action attribute must contain value of 0 (Add) or 1 (Remove)";
                }
            }

            if (IsCreating)
            {
                employee.AccessiblePmlNavigationLinksId = AccessiblePmlNavigationLinksIdList.ToArray();
            }
            else
            {
                employeeEditing.AccessiblePmlNavigationLinksId = AccessiblePmlNavigationLinksIdList.ToArray();
            }

            return string.Empty;
        }

        private string SetLoanAccessLevel()
        {
            if (string.IsNullOrEmpty(LoanAccessLevel)) // Just for safety - should already be handled in HasAllRequiredFields
            {
                return "Loan Access Level cannot be blank.";
            }

            if (!Enum.IsDefined(typeof(E_PmlLoanLevelAccess), LoanAccessLevel))
            {
                return "Invalid Loan Access Level.";
            }
            
            E_PmlLoanLevelAccess access = (E_PmlLoanLevelAccess)Enum.Parse(typeof(E_PmlLoanLevelAccess), LoanAccessLevel, true);
            if (principal.Type == "P" && access == E_PmlLoanLevelAccess.Supervisor)
            {
                return "PML Supervisors may only set Corporate or Individual Loan Access levels.";
            }

            if (IsCreating)
            {
                if (!IsValueTrue(IsSupervisor) && access == E_PmlLoanLevelAccess.Supervisor)
                {
                    return "Only PML Supervisors may have Supervisor Loan Access level.";
                }
                employee.PmlLoanLevelAccess = access;
            }
            else
            {
                if (!(IsValueSet(IsSupervisor) ? IsValueTrue(IsSupervisor) : employeeEditing.IsPmlManager)
                    && access == E_PmlLoanLevelAccess.Supervisor)
                {
                    return "Only PML Supervisors may have Supervisor Loan Access level.";
                }
                employeeEditing.PmlLevelAccess = access;
            }

            return "";
        }

        protected string SetRoles()
        {
            if (IsCreating)
            {
                if (principal.Type == "P" && IsValueTrue(IsSupervisor))
                    return "A PML Supervisor may not create another PML Supervisor.";

                string newRoles = "";
                if (IsValueTrue(IsLoanOfficer))
                    newRoles += CEmployeeFields.s_LoanRepRoleId.ToString();
                if (IsValueTrue(IsBrokerProcessor))
                {
                    if (false == string.IsNullOrEmpty(newRoles))
                        newRoles += ",";

                    newRoles += CEmployeeFields.s_BrokerProcessorId.ToString();
                }
                if (IsValueTrue(IsExternalSecondary))
                {
                    if (!string.IsNullOrEmpty(newRoles))
                    {
                        newRoles += ",";
                    }

                    newRoles += CEmployeeFields.s_ExternalSecondaryId;
                }
                if (IsValueTrue(IsExternalPostCloser))
                {
                    if (!string.IsNullOrEmpty(newRoles))
                    {
                        newRoles += ",";
                    }

                    newRoles += CEmployeeFields.s_ExternalPostCloserId;
                }
                employee.RoleId = newRoles;
            }
            else
            {
                string newRoles = "";
                EmployeeRoles roles = new EmployeeRoles(principal.BrokerId, employeeEditing.ID);
                bool userWillBeLO = IsValueSet(IsLoanOfficer) ? IsValueTrue(IsLoanOfficer) : roles.IsInRole(CEmployeeFields.s_LoanRepRoleId);
                bool userWillBeBP = IsValueSet(IsBrokerProcessor) ? IsValueTrue(IsBrokerProcessor) : roles.IsInRole(CEmployeeFields.s_BrokerProcessorId);
                bool userWillBeExternalSecondary = IsValueSet(IsExternalSecondary) ? IsValueTrue(IsExternalSecondary) : roles.IsInRole(CEmployeeFields.s_ExternalSecondaryId);
                bool userWillBeExternalPostCloser = IsValueSet(IsExternalPostCloser) ? IsValueTrue(IsExternalPostCloser) : roles.IsInRole(CEmployeeFields.s_ExternalPostCloserId);
                if (userWillBeLO)
                    newRoles += CEmployeeFields.s_LoanRepRoleId.ToString();
                if (userWillBeBP)
                {
                    if (false == string.IsNullOrEmpty(newRoles))
                        newRoles += ",";

                    newRoles += CEmployeeFields.s_BrokerProcessorId.ToString();
                }
                if (userWillBeExternalSecondary)
                {
                    if (!string.IsNullOrEmpty(newRoles))
                    {
                        newRoles += ",";
                    }

                    newRoles += CEmployeeFields.s_ExternalSecondaryId;
                }
                if (userWillBeExternalPostCloser)
                {
                    if (!string.IsNullOrEmpty(newRoles))
                    {
                        newRoles += ",";
                    }

                    newRoles += CEmployeeFields.s_ExternalPostCloserId;
                }
                employeeEditing.Roles = newRoles;
            }

            return "";
        }

        private string SaveAndBuildReturnMessage(ArrayList result)
        {
            if (null == result)
            {
                return "<result status=\"OK\"/>";
            }

            bool success = true;
            StringBuilder resultMessage = new StringBuilder();
            resultMessage.Append("<result status=\"Error\">");
            foreach (string error in result)
            {
                if (!error.TrimWhitespaceAndBOM().Equals(""))
                {
                    success = false;
                    resultMessage.Append("<error>" + error + "</error>");
                }
            }
            resultMessage.Append("</result>");

            if (success)
            {
                try
                {
                    if (IsCreating)
                    {
                        EmployeeImportSet impSet = new EmployeeImportSet();
                        impSet.Add(employee);
                        impSet.Save(principal.BrokerId, principal.UserId, principal, isWebserviceCall: true);
                    }
                    else
                    {
                        employeeEditing.WhoDoneIt = principal.UserId;
                        employeeEditing.Save(principal);
                        if (null != buP)
                        {
                            buP.Update();
                        }
                    }
                    return "<result status=\"OK\"/>";
                }
                catch (Exception e)
                {
                    Tools.LogError("Unable to save user in SaveAndBuildReturnMessage " + e);
                    return "<result status=\"Error\"><error>Unable to save user</error></result>";
                }
            }
            else
            {
                return resultMessage.ToString();
            }
        }

        public virtual string Update(string userName)
        {
            if (userName.ToLower().TrimWhitespaceAndBOM().Equals("") || userName.ToLower().TrimWhitespaceAndBOM().Equals("none"))
            {
                return "<result status=\"Error\"><error>User name cannot be blank or 'none'</error></result>";
            }

            IsCreating = false;
            ArrayList result = new ArrayList();
            Guid employeeID = Guid.Empty;
            SqlParameter[] parameters = {
												new SqlParameter("@LoginName", userName),
												new SqlParameter("@BrokerId", principal.BrokerId)
											};

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "RetrieveEmployeeIdByPmlLoginName", parameters))
            {
                if (reader.Read())
                {
                    employeeID = (Guid)reader["EmployeeId"];
                }
                else
                {
                    return "<result status=\"Error\"><error>User name not found</error></result>";
                }
            }

            if (employeeID.Equals(Guid.Empty))
            {
                return "<result status=\"Error\"><error>User name not found</error></result>";
            }

            employeeEditing = new EmployeeDB(employeeID, principal.BrokerId);

            if (!employeeEditing.Retrieve())
            {
                return "<result status=\"Error\"><error>User name not found</error></result>";
            }

            try
            {
                buP = new BrokerUserPermissions(employeeEditing.BrokerID, employeeEditing.ID);
            }
            catch (SqlException e)
            {
                Tools.LogError("Unable to set user permissions", e);
                result.Add("Unable to set user permissions");
            }
            catch (CBaseException e)
            {
                Tools.LogError("Unable to set user permissions", e);
                result.Add("Unable to set user permissions");
            }

            result.Add(HasAllRequiredFields());

            if (this.PopulateBrokerRelationshipsT == "OriginatingCompany" || this.PopulateBrokerRelationshipsT == "0" ||
                this.PopulateCorrRelationshipsT == "OriginatingCompany" || this.PopulateCorrRelationshipsT == "0" ||
                this.PopulateMiniCorrRelationshipsT == "OriginatingCompany" || this.PopulateMiniCorrRelationshipsT == "0" ||
                this.PopulatePMLPermissionsT == "OriginatingCompany" || this.PopulatePMLPermissionsT == "0" ||
                !IsValueBeingIgnored(this.Company))
            {
                result.Add(SetPmlBrokerInformation());
            }

            if (m_pmlBrokerId != Guid.Empty)
            {
                employeeEditing.PmlBrokerId = m_pmlBrokerId;
            }

            result.AddRange(this.SetBranches());
            result.Add(SetEmail());
            result.Add(SetLoginName());
            result.Add(SetPasswordOptions());
            result.Add(SetPassword());
            result.AddRange(this.SetPriceGroups());
            result.Add(SetSupervisorInfo());
            result.AddRange(SetRelationships());
            result.Add(SetOptions());
            result.Add(SetLendingLicenseInfo());
            result.Add(SetAccessiblePmlNavigationLinksId());
            result.Add(SetRoles());
            result.Add(this.SetPortalMode());

            if (IsValueSetToEmpty(this.IsActive))
            {
                result.Add("IsActive must have a value of True or False");
            }
            else if (!IsValueBeingIgnored(this.IsActive))
            {
                employeeEditing.IsActive = IsValueTrue(this.IsActive);
                employeeEditing.AllowLogin = IsValueTrue(this.IsActive);
            }

            if (IsValueSet(this.FirstName)) //required field
            {
                employeeEditing.FirstName = this.FirstName;
            }

            if (IsValueSet(this.LastName)) //required field
            {
                employeeEditing.LastName = this.LastName;
            }

            if (!IsValueBeingIgnored(this.Address))
            {
                employeeEditing.Address.StreetAddress = this.Address;
            }

            if (!IsValueBeingIgnored(this.City))
            {
                employeeEditing.Address.City = this.City;
            }

            if (!IsValueBeingIgnored(this.State))
            {
                employeeEditing.Address.State = this.State.ToUpper();
            }

            if (!IsValueBeingIgnored(this.Zipcode))
            {
                employeeEditing.Address.Zipcode = this.Zipcode;
            }

            if (!IsValueBeingIgnored(this.Fax))
            {
                employeeEditing.Fax = this.Fax;
            }

            if (IsValueSet(this.Phone))  //required field
            {
                employeeEditing.Phone = this.Phone;
            }

            if (!IsValueBeingIgnored(this.CellPhone))
            {
                employeeEditing.CellPhone = this.CellPhone;
            }

            if (!IsValueBeingIgnored(this.IsCellphoneForMultiFactorOnly))
            {
                employeeEditing.IsCellphoneForMultiFactorOnly = this.IsValueTrue(this.IsCellphoneForMultiFactorOnly);
            }

            if (!IsValueBeingIgnored(this.Pager))
            {
                employeeEditing.Pager = this.Pager;
            }

            if (!IsValueBeingIgnored(this.Notes))
            {
                employeeEditing.NotesByEmployer = this.Notes;
            }

            if (IsValueSet(this.LoanAccessLevel))  //required field
            {
                result.Add(SetLoanAccessLevel());
            }

            if (!IsValueBeingIgnored(this.NmlsIdentifier))
            {
                employeeEditing.LosIdentifier = this.NmlsIdentifier;
            }

            if (this.OriginatorCompensationSetLevelT == "1" || this.OriginatorCompensationSetLevelT == "IndividualUser")
            {
                employeeEditing.OriginatorCompensationSetLevelT = E_OrigiantorCompenationLevelT.IndividualUser;
            }
            else if (this.OriginatorCompensationSetLevelT == "0" || this.OriginatorCompensationSetLevelT == "OriginatingCompany"
                || employeeEditing.OriginatorCompensationSetLevelT == E_OrigiantorCompenationLevelT.OriginatingCompany)
            {
                employeeEditing.OriginatorCompensationSetLevelT = E_OrigiantorCompenationLevelT.OriginatingCompany;
                string errorMessage = "";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo) ? "" : " OriginatorCompensationIsOnlyPaidForFirstLienOfCombo";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationPercent) ? "" : " OriginatorCompensationPercent";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationBaseT) ? "" : " OriginatorCompensationBaseT";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationMinAmount) ? "" : " OriginatorCompensationMinAmount";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationMaxAmount) ? "" : " OriginatorCompensationMaxAmount";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationFixedAmount) ? "" : " OriginatorCompensationFixedAmount";
                if (errorMessage != "")
                {
                    result.Add("Compensation is determined by originating company. Please exclude these properties:" + errorMessage);
                }
            }

            if (string.IsNullOrEmpty(this.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo) == false)
            {
                employeeEditing.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = IsValueTrue(OriginatorCompensationIsOnlyPaidForFirstLienOfCombo);
            }
            decimal v = 0;
            if (string.IsNullOrEmpty(this.OriginatorCompensationPercent) == false)
            {
                if (decimal.TryParse(this.OriginatorCompensationPercent, out v))
                {
                    employeeEditing.OriginatorCompensationPercent = v;
                }
            }
            if (this.OriginatorCompensationBaseT == "0")
            {
                employeeEditing.OriginatorCompensationBaseT = E_PercentBaseT.LoanAmount;
            }
            else if (this.OriginatorCompensationBaseT == "4")
            {
                employeeEditing.OriginatorCompensationBaseT = E_PercentBaseT.TotalLoanAmount;
            }

            if (string.IsNullOrEmpty(this.OriginatorCompensationMinAmount) == false)
            {
                if (decimal.TryParse(this.OriginatorCompensationMinAmount, out v))
                {
                    employeeEditing.OriginatorCompensationMinAmount = v;
                }
            }
            if (string.IsNullOrEmpty(this.OriginatorCompensationMaxAmount) == false)
            {
                if (decimal.TryParse(this.OriginatorCompensationMaxAmount, out v))
                {
                    employeeEditing.OriginatorCompensationMaxAmount = v;
                }
            }
            if (string.IsNullOrEmpty(this.OriginatorCompensationFixedAmount) == false)
            {
                if (decimal.TryParse(this.OriginatorCompensationFixedAmount, out v))
                {
                    employeeEditing.OriginatorCompensationFixedAmount = v;
                }
            }
            if (this.OriginatorCompensationNotes != null)
            {
                employeeEditing.OriginatorCompensationNotes = this.OriginatorCompensationNotes;
            }

            if (this.CustomPricingPolicyFieldValueSource == "1" || this.CustomPricingPolicyFieldValueSource == "IndividualUser")
            {
                employeeEditing.CustomPricingPolicyFieldValueSource = E_PricingPolicyFieldValueSource.IndividualUser;
            }
            else if (this.CustomPricingPolicyFieldValueSource == "0" || this.CustomPricingPolicyFieldValueSource == "OriginatingCompany"
    || employeeEditing.CustomPricingPolicyFieldValueSource == E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch)
            {
                employeeEditing.CustomPricingPolicyFieldValueSource = E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
                string errorMessage = "";
                errorMessage += string.IsNullOrEmpty(this.CustomPricingPolicyField1Fixed) ? "" : " CustomPricingPolicyField1Fixed";
                errorMessage += string.IsNullOrEmpty(this.CustomPricingPolicyField2Fixed) ? "" : " CustomPricingPolicyField2Fixed";
                errorMessage += string.IsNullOrEmpty(this.CustomPricingPolicyField3Fixed) ? "" : " CustomPricingPolicyField3Fixed";
                errorMessage += string.IsNullOrEmpty(this.CustomPricingPolicyField4Fixed) ? "" : " CustomPricingPolicyField4Fixed";
                errorMessage += string.IsNullOrEmpty(this.CustomPricingPolicyField5Fixed) ? "" : " CustomPricingPolicyField5Fixed";
                if (errorMessage != "")
                {
                    result.Add("Loan Officer Pricing Policy is determined by originating company. Please exclude these properties:" + errorMessage);
                }
            }
            if (this.CustomPricingPolicyField1Fixed != null)
            {
                employeeEditing.CustomPricingPolicyField1Fixed = this.CustomPricingPolicyField1Fixed;
            }
            if (this.CustomPricingPolicyField2Fixed != null)
            {
                employeeEditing.CustomPricingPolicyField2Fixed = this.CustomPricingPolicyField2Fixed;
            }
            if (this.CustomPricingPolicyField3Fixed != null)
            {
                employeeEditing.CustomPricingPolicyField3Fixed = this.CustomPricingPolicyField3Fixed;
            }
            if (this.CustomPricingPolicyField4Fixed != null)
            {
                employeeEditing.CustomPricingPolicyField4Fixed = this.CustomPricingPolicyField4Fixed;
            }
            if (this.CustomPricingPolicyField5Fixed != null)
            {
                employeeEditing.CustomPricingPolicyField5Fixed = this.CustomPricingPolicyField5Fixed;
            }

            if (this.DOLoginName != null)
            {
                employeeEditing.DOAutoLoginName = this.DOLoginName;
            }

            if (this.DOPassword != null)
            {
                employeeEditing.DOAutoPassword = this.DOPassword;
            }

            if (this.DULoginName != null)
            {
                employeeEditing.DUAutoLoginName = this.DULoginName;
            }

            if (this.DUPassword != null)
            {
                employeeEditing.DUAutoPassword = this.DUPassword;
            }

            return SaveAndBuildReturnMessage(result);
        }

        public virtual string UpdateByPMLSupervisor(string userName)
        {
            if (userName.ToLower().TrimWhitespaceAndBOM().Equals("") || userName.ToLower().TrimWhitespaceAndBOM().Equals("none"))
            {
                return "<result status=\"Error\"><error>User name cannot be blank or 'none'</error></result>";
            }

            IsCreating = false;
            IsSupervisor = "false"; // We currently don't allow supervisors to create other supervisors

            ArrayList result = new ArrayList();

            Guid employeeID = Guid.Empty;
            SqlParameter[] parameters = {
												new SqlParameter("@LoginName", userName),
												new SqlParameter("@BrokerId", principal.BrokerId)
											};

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "RetrieveEmployeeIdByPmlLoginName", parameters))
            {
                if (reader.Read())
                {
                    employeeID = (Guid)reader["EmployeeId"];
                }
                else
                {
                    return "<result status=\"Error\"><error>User name not found</error></result>";
                }
            }

            if (employeeID.Equals(Guid.Empty))
            {
                return "<result status=\"Error\"><error>User name not found</error></result>";
            }

            employeeEditing = new EmployeeDB(employeeID, principal.BrokerId);

            if (!employeeEditing.Retrieve())
            {
                return "<result status=\"Error\"><error>User name not found</error></result>";
            }

            if (employeeEditing.PmlExternalManagerEmployeeId != principal.EmployeeId)
            {
                return "<result status=\"Error\"><error>Cannot update users you do not supervise</error></result>";
            }

            result.Add(HasAllRequiredFields());
            result.Add(SetEmail());
            result.Add(SetLoginName());
            result.Add(SetPasswordOptions());
            result.Add(SetPassword());
            result.Add(this.SetPortalMode());
            result.AddRange(this.SetRelationshipsForPmlSupervisor());

            result.Add(SetRoles());

            if (IsValueSetToEmpty(this.IsActive))
            {
                result.Add("IsActive must have a value of True or False");
            }
            else if (!IsValueBeingIgnored(this.IsActive))
            {
                employeeEditing.IsActive = IsValueTrue(this.IsActive);
                employeeEditing.AllowLogin = IsValueTrue(this.IsActive);
            }

            if (IsValueSet(this.FirstName)) //required field
            {
                employeeEditing.FirstName = this.FirstName;
            }

            if (IsValueSet(this.LastName)) //required field
            {
                employeeEditing.LastName = this.LastName;
            }

            if (!IsValueBeingIgnored(this.Fax))
            {
                employeeEditing.Fax = this.Fax;
            }

            if (IsValueSet(this.Phone))  //required field
            {
                employeeEditing.Phone = this.Phone;
            }

            if (!IsValueBeingIgnored(this.CellPhone))
            {
                employeeEditing.CellPhone = this.CellPhone;
            }

            if (!IsValueBeingIgnored(this.IsCellphoneForMultiFactorOnly))
            {
                employeeEditing.IsCellphoneForMultiFactorOnly = this.IsValueTrue(this.IsCellphoneForMultiFactorOnly);
            }

            if (!IsValueBeingIgnored(this.Pager))
            {
                employeeEditing.Pager = this.Pager;
            }

            if (IsValueSet(this.LoanAccessLevel))  //required field
            {
                result.Add(SetLoanAccessLevel());
            }
            if (!IsValueBeingIgnored(this.NmlsIdentifier))
            {
                employeeEditing.LosIdentifier = this.NmlsIdentifier;
            }
            return SaveAndBuildReturnMessage(result);
        }

        // This create method is specifically for user by LO Users.
        public virtual string Create()
        {
            IsCreating = true;
            employee = new EmployeeImportSet.Desc();
            ArrayList result = new ArrayList();

            result.Add(HasAllRequiredFields());
            result.Add(SetPmlBrokerInformation());
            result.AddRange(this.SetBranches());
            result.Add(SetEmail());
            result.Add(SetLoginName());
            result.Add(SetPasswordOptions());
            result.Add(SetPassword());
            result.AddRange(this.SetPriceGroups());
            result.Add(SetSupervisorInfo());
            result.AddRange(SetRelationships());
            result.Add(SetOptions());
            result.Add(SetLendingLicenseInfo());
            result.Add(SetAccessiblePmlNavigationLinksId());
            result.Add(SetLoanAccessLevel());
            result.Add(SetRoles());
            result.AddRange(this.SetLandingPages());

            employee.UserType = "P";
            employee.FirstName = (this.FirstName == null) ? "" : this.FirstName;
            employee.LastName = (this.LastName == null) ? "" : this.LastName;
            employee.CompanyName = (this.Company == null) ? "" : this.Company;
            employee.PmlBrokerId = m_pmlBrokerId;
            employee.Street = (this.Address == null) ? "" : this.Address;
            employee.City = (this.City == null) ? "" : this.City;
            employee.State = (this.State == null) ? "" : this.State.ToUpper();
            employee.Zipcode = (this.Zipcode == null) ? "" : this.Zipcode;
            employee.Phone = (this.Phone == null) ? "" : this.Phone;
            employee.Fax = (this.Fax == null) ? "" : this.Fax;
            employee.CellPhone = (this.CellPhone == null) ? "" : this.CellPhone;
            if (!this.IsValueBeingIgnored(this.IsCellphoneForMultiFactorOnly))
            {
                employee.IsCellphoneForMultiFactorOnly = this.IsValueTrue(this.IsCellphoneForMultiFactorOnly);
            }
            employee.Pager = (this.Pager == null) ? "" : this.Pager;
            employee.Notes = (this.Notes == null) ? "" : this.Notes;
            employee.NmlsIdentifier = (this.NmlsIdentifier == null) ? "" : this.NmlsIdentifier;

            if (this.OriginatorCompensationSetLevelT == "1" || this.OriginatorCompensationSetLevelT == "IndividualUser")
            {
                employee.OriginatorCompensationSetLevelT = E_OrigiantorCompenationLevelT.IndividualUser;
            }
            else
            {
                employee.OriginatorCompensationSetLevelT = E_OrigiantorCompenationLevelT.OriginatingCompany;
                string errorMessage = "";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo) ? "" : " OriginatorCompensationIsOnlyPaidForFirstLienOfCombo";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationPercent) ? "" : " OriginatorCompensationPercent";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationBaseT) ? "" : " OriginatorCompensationBaseT";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationMinAmount) ? "" : " OriginatorCompensationMinAmount";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationMaxAmount) ? "" : " OriginatorCompensationMaxAmount";
                errorMessage += string.IsNullOrEmpty(this.OriginatorCompensationFixedAmount) ? "" : " OriginatorCompensationFixedAmount";
                if (errorMessage != "")
                {
                    result.Add("Compensation is determined by originating company. Please exclude these properties:" + errorMessage);
                }
            }

            if (string.IsNullOrEmpty(this.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo) == false)
            {
                employeeEditing.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = IsValueTrue(OriginatorCompensationIsOnlyPaidForFirstLienOfCombo);
            }
            decimal v = 0;
            if (string.IsNullOrEmpty(this.OriginatorCompensationPercent) == false)
            {
                if (decimal.TryParse(this.OriginatorCompensationPercent, out v))
                {
                    employee.OriginatorCompensationPercent = v;
                }
            }
            if (this.OriginatorCompensationBaseT == "0")
            {
                employee.OriginatorCompensationBaseT = E_PercentBaseT.LoanAmount;
            }
            else if (this.OriginatorCompensationBaseT == "4")
            {
                employee.OriginatorCompensationBaseT = E_PercentBaseT.TotalLoanAmount;
            }

            if (string.IsNullOrEmpty(this.OriginatorCompensationMinAmount) == false)
            {
                if (decimal.TryParse(this.OriginatorCompensationMinAmount, out v))
                {
                    employee.OriginatorCompensationMinAmount = v;
                }
            }
            if (string.IsNullOrEmpty(this.OriginatorCompensationMaxAmount) == false)
            {
                if (decimal.TryParse(this.OriginatorCompensationMaxAmount, out v))
                {
                    employee.OriginatorCompensationMaxAmount = v;
                }
            }
            if (string.IsNullOrEmpty(this.OriginatorCompensationFixedAmount) == false)
            {
                if (decimal.TryParse(this.OriginatorCompensationFixedAmount, out v))
                {
                    employee.OriginatorCompensationFixedAmount = v;
                }
            }
            employee.OriginatorCompensationNotes = this.OriginatorCompensationNotes;

            if (this.CustomPricingPolicyFieldValueSource == "1" || this.CustomPricingPolicyFieldValueSource == "IndividualUser")
            {
                employee.CustomPricingPolicyFieldValueSource = E_PricingPolicyFieldValueSource.IndividualUser;
            }
            else
            {
                employee.CustomPricingPolicyFieldValueSource = E_PricingPolicyFieldValueSource.OriginatingCompanyOrBranch;
                string errorMessage = "";
                errorMessage += string.IsNullOrEmpty(this.CustomPricingPolicyField1Fixed) ? "" : " CustomPricingPolicyField1Fixed";
                errorMessage += string.IsNullOrEmpty(this.CustomPricingPolicyField2Fixed) ? "" : " CustomPricingPolicyField2Fixed";
                errorMessage += string.IsNullOrEmpty(this.CustomPricingPolicyField3Fixed) ? "" : " CustomPricingPolicyField3Fixed";
                errorMessage += string.IsNullOrEmpty(this.CustomPricingPolicyField4Fixed) ? "" : " CustomPricingPolicyField4Fixed";
                errorMessage += string.IsNullOrEmpty(this.CustomPricingPolicyField5Fixed) ? "" : " CustomPricingPolicyField5Fixed";
                if (errorMessage != "")
                {
                    result.Add("Loan Officer Pricing Policy is determined by originating company. Please exclude these properties:" + errorMessage);
                }
            }
           employee.CustomPricingPolicyField1Fixed = this.CustomPricingPolicyField1Fixed;
           employee.CustomPricingPolicyField2Fixed = this.CustomPricingPolicyField2Fixed;
           employee.CustomPricingPolicyField3Fixed = this.CustomPricingPolicyField3Fixed;
           employee.CustomPricingPolicyField4Fixed = this.CustomPricingPolicyField4Fixed;
           employee.CustomPricingPolicyField5Fixed = this.CustomPricingPolicyField5Fixed;
           employee.DOAutoLoginName = this.DOLoginName;
           employee.DOAutoPassword = this.DOPassword;
           employee.DUAutoLoginName = this.DULoginName;
           employee.DUAutoPassword = this.DUPassword;

           return SaveAndBuildReturnMessage(result);
        }

        public virtual string CreateByPMLSupervisor()
        {
            EmployeeDB supervisor = new EmployeeDB(principal.EmployeeId, principal.BrokerId);
            supervisor.Retrieve();
            employee = new EmployeeImportSet.Desc();

            IsCreating = true;
            
            ArrayList result = new ArrayList();
            result.Add(HasAllRequiredFields());
            result.Add(SetEmail());
            result.Add(SetLoginName());
            result.Add(SetPasswordOptions());
            result.Add(SetPassword());

            result.AddRange(this.SetRelationshipsForPmlSupervisor());

            result.Add(SetLoanAccessLevel());
            result.Add(SetRoles());

            employee.UserType = "P";
            employee.FirstName = (this.FirstName == null) ? "" : this.FirstName;
            employee.LastName = (this.LastName == null) ? "" : this.LastName;
            employee.Phone = (this.Phone == null) ? "" : this.Phone;
            employee.CellPhone = (this.CellPhone == null) ? "" : this.CellPhone;
            if (!this.IsValueBeingIgnored(this.IsCellphoneForMultiFactorOnly))
            {
                employee.IsCellphoneForMultiFactorOnly = this.IsValueTrue(this.IsCellphoneForMultiFactorOnly);
            }
            employee.Pager = (this.Pager == null) ? "" : this.Pager;
            employee.Fax = (this.Fax == null) ? "" : this.Fax;

            employee.SupervisorId = principal.EmployeeId;
            employee.PmlBrokerId = supervisor.PmlBrokerId;
            employee.LenderAccountExecId = supervisor.LenderAcctExecEmployeeID;
            employee.UnderwriterId = supervisor.UnderwriterEmployeeID;
            employee.JuniorUnderwriterId = supervisor.JuniorUnderwriterEmployeeID;
            employee.LockDeskId = supervisor.LockDeskEmployeeID;
            employee.ManagerId = supervisor.ManagerEmployeeID;
            employee.ProcessorId = supervisor.ProcessorEmployeeID;
            employee.JuniorProcessorId = supervisor.JuniorProcessorEmployeeID;
            employee.PriceGroup = supervisor.LpePriceGroupID;
            employee.BranchId = supervisor.BranchID;

            this.SetMiniCorrespondentRelationships(employee, supervisor);

            this.SetCorrespondentRelationships(employee, supervisor);

            if (!IsValueSet(this.BrokerProcessor))
            {
                employee.BrokerProcessorId = supervisor.BrokerProcessorEmployeeId;
            }
            if (!IsValueSet(this.MiniCorrespondentExternalPostCloser))
            {
                employee.MiniCorrespondentExternalPostCloserId = supervisor.MiniCorrExternalPostCloserEmployeeId;
            }
            if (!IsValueSet(this.CorrespondentExternalPostCloser))
            {
                employee.CorrespondentExternalPostCloserId = supervisor.CorrExternalPostCloserEmployeeId;
            }
            return SaveAndBuildReturnMessage(result);
        }

        protected void SetMiniCorrespondentRelationships(EmployeeImportSet.Desc toEmployee, EmployeeDB fromEmployee)
        {
            toEmployee.MiniCorrespondentManagerId = fromEmployee.MiniCorrManagerEmployeeId;
            toEmployee.MiniCorrespondentProcessorId = fromEmployee.MiniCorrProcessorEmployeeId;
            toEmployee.MiniCorrespondentJuniorProcessorId = fromEmployee.MiniCorrJuniorProcessorEmployeeId;
            toEmployee.MiniCorrespondentLenderAccExecId = fromEmployee.MiniCorrLenderAccExecEmployeeId;
            toEmployee.MiniCorrespondentUnderwriterId = fromEmployee.MiniCorrUnderwriterEmployeeId;
            toEmployee.MiniCorrespondentJuniorUnderwriterId = fromEmployee.MiniCorrJuniorUnderwriterEmployeeId;
            toEmployee.MiniCorrespondentCreditAuditorId = fromEmployee.MiniCorrCreditAuditorEmployeeId;
            toEmployee.MiniCorrespondentLegalAuditorId = fromEmployee.MiniCorrLegalAuditorEmployeeId;
            toEmployee.MiniCorrespondentLockDeskId = fromEmployee.MiniCorrLockDeskEmployeeId;
            toEmployee.MiniCorrespondentPurchaserId = fromEmployee.MiniCorrPurchaserEmployeeId;
            toEmployee.MiniCorrespondentSecondaryId = fromEmployee.MiniCorrSecondaryEmployeeId;

            toEmployee.MiniCorrespondentBranchId = fromEmployee.MiniCorrespondentBranchID;
            toEmployee.UseOriginatingCompanyMiniCorrBranchId = fromEmployee.UseOriginatingCompanyMiniCorrBranchId;
            toEmployee.MiniCorrespondentPriceGroupId = fromEmployee.MiniCorrespondentPriceGroupID;
            toEmployee.UseOriginatingCompanyMiniCorrPriceGroupId = fromEmployee.UseOriginatingCompanyMiniCorrPriceGroupId;
        }

        protected void SetCorrespondentRelationships(EmployeeImportSet.Desc toEmployee, EmployeeDB fromEmployee)
        {
            toEmployee.CorrespondentManagerId = fromEmployee.CorrManagerEmployeeId;
            toEmployee.CorrespondentProcessorId = fromEmployee.CorrProcessorEmployeeId;
            toEmployee.CorrespondentJuniorProcessorId = fromEmployee.CorrJuniorProcessorEmployeeId;
            toEmployee.CorrespondentLenderAccExecId = fromEmployee.CorrLenderAccExecEmployeeId;
            toEmployee.CorrespondentUnderwriterId = fromEmployee.CorrUnderwriterEmployeeId;
            toEmployee.CorrespondentJuniorUnderwriterId = fromEmployee.CorrJuniorUnderwriterEmployeeId;
            toEmployee.CorrespondentCreditAuditorId = fromEmployee.CorrCreditAuditorEmployeeId;
            toEmployee.CorrespondentLegalAuditorId = fromEmployee.CorrLegalAuditorEmployeeId;
            toEmployee.CorrespondentLockDeskId = fromEmployee.CorrLockDeskEmployeeId;
            toEmployee.CorrespondentPurchaserId = fromEmployee.CorrPurchaserEmployeeId;
            toEmployee.CorrespondentSecondaryId = fromEmployee.CorrSecondaryEmployeeId;

            toEmployee.CorrespondentBranchId = fromEmployee.CorrespondentBranchID;
            toEmployee.UseOriginatingCompanyCorrBranchId = fromEmployee.UseOriginatingCompanyCorrBranchId;
            toEmployee.CorrespondentPriceGroupId = fromEmployee.CorrespondentPriceGroupID;
            toEmployee.UseOriginatingCompanyCorrPriceGroupId = fromEmployee.UseOriginatingCompanyCorrPriceGroupId;
        }

        private string SetRelationship(RelationshipImportSetting relationshipSetting)
        {
            if (this.IsValueSet(relationshipSetting.EmployeeName) && 
                this.brokerLoanAssignmentTable[relationshipSetting.RoleT] != null)
            {
                Guid? matchingEmployeeID = null;

                foreach (BrokerLoanAssignmentTable.Spec eSpec in this.brokerLoanAssignmentTable[relationshipSetting.RoleT])
                {
                    bool namesMatch = eSpec.FullName.TrimWhitespaceAndBOM().ToLower()
                        .Equals(relationshipSetting.EmployeeName.TrimWhitespaceAndBOM().ToLower());

                    if (namesMatch && matchingEmployeeID == null)
                    {
                        matchingEmployeeID = eSpec.EmployeeId;
                    }
                    else if (namesMatch)
                    {
                        return "Multiple matches found for " + Role.Get(relationshipSetting.RoleT).ModifiableDesc;
                    }
                }

                if (matchingEmployeeID == null)
                {
                    return Role.Get(relationshipSetting.RoleT).ModifiableDesc + " not found";
                }
                else
                {
                    if (this.IsCreating)
                    {
                        relationshipSetting.SetForNewEmployee(matchingEmployeeID.Value);
                    }
                    else
                    {
                        relationshipSetting.SetForExistingEmployee(matchingEmployeeID.Value);
                    }
                }
            }
            else if (this.IsValueSetToEmpty(relationshipSetting.EmployeeName))
            {
                if (this.IsCreating)
                {
                    relationshipSetting.ClearForNewEmployee();
                }
                else
                {
                    relationshipSetting.ClearForExistingEmployee();
                }
            }

            return "";
        }

        protected ArrayList SetRelationships()
        {
            ArrayList returnList = new ArrayList();

            try
            {
                var availableBrokerRelationships = new List<RelationshipImportSetting>()
                {
                    new RelationshipImportSetting(
                        this.Manager,
                        E_RoleT.Manager,
                        (id) => this.employee.ManagerId = id,
                        (id) => this.employeeEditing.ManagerEmployeeID = id,
                        () => this.employee.ManagerId = Guid.Empty,
                        () => this.employeeEditing.ManagerEmployeeID = Guid.Empty),
                    new RelationshipImportSetting(
                        this.Processor,
                        E_RoleT.Processor,
                        (id) => this.employee.ProcessorId = id,
                        (id) => this.employeeEditing.ProcessorEmployeeID = id,
                        () => this.employee.ProcessorId = Guid.Empty,
                        () => this.employeeEditing.ProcessorEmployeeID = Guid.Empty),
                    new RelationshipImportSetting(
                        this.JuniorProcessor,
                        E_RoleT.JuniorProcessor,
                        (id) => this.employee.JuniorProcessorId = id,
                        (id) => this.employeeEditing.JuniorProcessorEmployeeID = id,
                        () => this.employee.JuniorProcessorId = Guid.Empty,
                        () => this.employeeEditing.JuniorProcessorEmployeeID = Guid.Empty),
                    new RelationshipImportSetting(
                        this.AccountExecutive,
                        E_RoleT.LenderAccountExecutive,
                        (id) => this.employee.LenderAccountExecId  = id,
                        (id) => this.employeeEditing.LenderAcctExecEmployeeID = id,
                        () => this.employee.LenderAccountExecId = Guid.Empty,
                        () => this.employeeEditing.LenderAcctExecEmployeeID = Guid.Empty),
                    new RelationshipImportSetting(
                        this.Underwriter,
                        E_RoleT.Underwriter,
                        (id) => this.employee.UnderwriterId = id,
                        (id) => this.employeeEditing.UnderwriterEmployeeID = id,
                        () => this.employee.UnderwriterId = Guid.Empty,
                        () => this.employeeEditing.UnderwriterEmployeeID = Guid.Empty),
                    new RelationshipImportSetting(
                        this.JuniorUnderwriter,
                        E_RoleT.JuniorUnderwriter,
                        (id) => this.employee.JuniorUnderwriterId = id,
                        (id) => this.employeeEditing.JuniorUnderwriterEmployeeID = id,
                        () => this.employee.JuniorUnderwriterId = Guid.Empty,
                        () => this.employeeEditing.JuniorUnderwriterEmployeeID = Guid.Empty),
                    new RelationshipImportSetting(
                        this.LockDesk,
                        E_RoleT.LockDesk,
                        (id) => this.employee.LockDeskId = id,
                        (id) => this.employeeEditing.LockDeskEmployeeID = id,
                        () => this.employee.LockDeskId = Guid.Empty,
                        () => this.employeeEditing.LockDeskEmployeeID = Guid.Empty)
                };

                var availableCorrespondentRelationships = new List<RelationshipImportSetting>()
                {
                    new RelationshipImportSetting(
                        this.CorrespondentManager,
                        E_RoleT.Manager,
                        (id) => this.employee.CorrespondentManagerId = id,
                        (id) => this.employeeEditing.CorrManagerEmployeeId = id,
                        () => this.employee.CorrespondentManagerId = Guid.Empty,
                        () => this.employeeEditing.CorrManagerEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentProcessor,
                        E_RoleT.Processor,
                        (id) => this.employee.CorrespondentProcessorId = id,
                        (id) => this.employeeEditing.CorrProcessorEmployeeId = id,
                        () => this.employee.CorrespondentProcessorId = Guid.Empty,
                        () => this.employeeEditing.CorrProcessorEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentJuniorProcessor,
                        E_RoleT.JuniorProcessor,
                        (id) => this.employee.CorrespondentJuniorProcessorId = id,
                        (id) => this.employeeEditing.CorrJuniorProcessorEmployeeId = id,
                        () => this.employee.CorrespondentJuniorProcessorId = Guid.Empty,
                        () => this.employeeEditing.CorrJuniorProcessorEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentLenderAccExec,
                        E_RoleT.LenderAccountExecutive,
                        (id) => this.employee.CorrespondentLenderAccExecId = id,
                        (id) => this.employeeEditing.CorrLenderAccExecEmployeeId = id,
                        () => this.employee.CorrespondentLenderAccExecId = Guid.Empty,
                        () => this.employeeEditing.CorrLenderAccExecEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentUnderwriter,
                        E_RoleT.Underwriter,
                        (id) => this.employee.CorrespondentUnderwriterId = id,
                        (id) => this.employeeEditing.CorrUnderwriterEmployeeId = id,
                        () => this.employee.CorrespondentUnderwriterId = Guid.Empty,
                        () => this.employeeEditing.CorrUnderwriterEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentJuniorUnderwriter,
                        E_RoleT.JuniorUnderwriter,
                        (id) => this.employee.CorrespondentJuniorUnderwriterId = id,
                        (id) => this.employeeEditing.CorrJuniorUnderwriterEmployeeId = id,
                        () => this.employee.CorrespondentJuniorUnderwriterId = Guid.Empty,
                        () => this.employeeEditing.CorrJuniorUnderwriterEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentCreditAuditor,
                        E_RoleT.CreditAuditor,
                        (id) => this.employee.CorrespondentCreditAuditorId = id,
                        (id) => this.employeeEditing.CorrCreditAuditorEmployeeId = id,
                        () => this.employee.CorrespondentCreditAuditorId = Guid.Empty,
                        () => this.employeeEditing.CorrCreditAuditorEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentLegalAuditor,
                        E_RoleT.LegalAuditor,
                        (id) => this.employee.CorrespondentLegalAuditorId = id,
                        (id) => this.employeeEditing.CorrLegalAuditorEmployeeId = id,
                        () => this.employee.CorrespondentLegalAuditorId = Guid.Empty,
                        () => this.employeeEditing.CorrLegalAuditorEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentLockDesk,
                        E_RoleT.LockDesk,
                        (id) => this.employee.CorrespondentLockDeskId = id,
                        (id) => this.employeeEditing.CorrLockDeskEmployeeId = id,
                        () => this.employee.CorrespondentLockDeskId = Guid.Empty,
                        () => this.employeeEditing.CorrLockDeskEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentPurchaser,
                        E_RoleT.Purchaser,
                        (id) => this.employee.CorrespondentPurchaserId = id,
                        (id) => this.employeeEditing.CorrPurchaserEmployeeId = id,
                        () => this.employee.CorrespondentPurchaserId = Guid.Empty,
                        () => this.employeeEditing.CorrPurchaserEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentSecondary,
                        E_RoleT.Secondary,
                        (id) => this.employee.CorrespondentSecondaryId = id,
                        (id) => this.employeeEditing.CorrSecondaryEmployeeId = id,
                        () => this.employee.CorrespondentSecondaryId = Guid.Empty,
                        () => this.employeeEditing.CorrSecondaryEmployeeId = Guid.Empty)
                };

                var availableMiniCorrespondentRelationships = new List<RelationshipImportSetting>()
                {
                    new RelationshipImportSetting(
                        this.MiniCorrespondentManager,
                        E_RoleT.Manager,
                        (id) => this.employee.MiniCorrespondentManagerId = id,
                        (id) => this.employeeEditing.MiniCorrManagerEmployeeId = id,
                        () => this.employee.MiniCorrespondentManagerId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrManagerEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentProcessor,
                        E_RoleT.Processor,
                        (id) => this.employee.MiniCorrespondentProcessorId = id,
                        (id) => this.employeeEditing.MiniCorrProcessorEmployeeId = id,
                        () => this.employee.MiniCorrespondentProcessorId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrProcessorEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentJuniorProcessor,
                        E_RoleT.JuniorProcessor,
                        (id) => this.employee.MiniCorrespondentJuniorProcessorId = id,
                        (id) => this.employeeEditing.MiniCorrJuniorProcessorEmployeeId = id,
                        () => this.employee.MiniCorrespondentJuniorProcessorId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrJuniorProcessorEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentLenderAccExec,
                        E_RoleT.LenderAccountExecutive,
                        (id) => this.employee.MiniCorrespondentLenderAccExecId = id,
                        (id) => this.employeeEditing.MiniCorrLenderAccExecEmployeeId = id,
                        () => this.employee.MiniCorrespondentLenderAccExecId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrLenderAccExecEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentUnderwriter,
                        E_RoleT.Underwriter,
                        (id) => this.employee.MiniCorrespondentUnderwriterId = id,
                        (id) => this.employeeEditing.MiniCorrUnderwriterEmployeeId = id,
                        () => this.employee.MiniCorrespondentUnderwriterId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrUnderwriterEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentJuniorUnderwriter,
                        E_RoleT.JuniorUnderwriter,
                        (id) => this.employee.MiniCorrespondentJuniorUnderwriterId = id,
                        (id) => this.employeeEditing.MiniCorrJuniorUnderwriterEmployeeId = id,
                        () => this.employee.MiniCorrespondentJuniorUnderwriterId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrJuniorUnderwriterEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentCreditAuditor,
                        E_RoleT.CreditAuditor,
                        (id) => this.employee.MiniCorrespondentCreditAuditorId = id,
                        (id) => this.employeeEditing.MiniCorrCreditAuditorEmployeeId = id,
                        () => this.employee.MiniCorrespondentCreditAuditorId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrCreditAuditorEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentLegalAuditor,
                        E_RoleT.LegalAuditor,
                        (id) => this.employee.MiniCorrespondentLegalAuditorId = id,
                        (id) => this.employeeEditing.MiniCorrLegalAuditorEmployeeId = id,
                        () => this.employee.MiniCorrespondentLegalAuditorId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrLegalAuditorEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentLockDesk,
                        E_RoleT.LockDesk,
                        (id) => this.employee.MiniCorrespondentLockDeskId = id,
                        (id) => this.employeeEditing.MiniCorrLockDeskEmployeeId = id,
                        () => this.employee.MiniCorrespondentLockDeskId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrLockDeskEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentPurchaser,
                        E_RoleT.Purchaser,
                        (id) => this.employee.MiniCorrespondentPurchaserId = id,
                        (id) => this.employeeEditing.MiniCorrPurchaserEmployeeId = id,
                        () => this.employee.MiniCorrespondentPurchaserId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrPurchaserEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentSecondary,
                        E_RoleT.Secondary,
                        (id) => this.employee.MiniCorrespondentSecondaryId = id,
                        (id) => this.employeeEditing.MiniCorrSecondaryEmployeeId = id,
                        () => this.employee.MiniCorrespondentSecondaryId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrSecondaryEmployeeId = Guid.Empty),
                };

                var validRelationships = new List<RelationshipImportSetting>()
                {
                    new RelationshipImportSetting(
                        this.BrokerProcessor,
                        E_RoleT.Pml_BrokerProcessor,
                        (id) => this.employee.BrokerProcessorId = id,
                        (id) => this.employeeEditing.BrokerProcessorEmployeeId = id,
                        () => this.employee.BrokerProcessorId = Guid.Empty,
                        () => this.employeeEditing.BrokerProcessorEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.CorrespondentExternalPostCloser,
                        E_RoleT.Pml_PostCloser,
                        (id) => this.employee.CorrespondentExternalPostCloserId = id,
                        (id) => this.employeeEditing.CorrExternalPostCloserEmployeeId = id,
                        () => this.employee.CorrespondentExternalPostCloserId = Guid.Empty,
                        () => this.employeeEditing.CorrExternalPostCloserEmployeeId = Guid.Empty),
                    new RelationshipImportSetting(
                        this.MiniCorrespondentExternalPostCloser,
                        E_RoleT.Pml_PostCloser,
                        (id) => this.employee.MiniCorrespondentExternalPostCloserId = id,
                        (id) => this.employeeEditing.MiniCorrExternalPostCloserEmployeeId = id,
                        () => this.employee.MiniCorrespondentExternalPostCloserId = Guid.Empty,
                        () => this.employeeEditing.MiniCorrExternalPostCloserEmployeeId = Guid.Empty)
                };

                if (this.PopulateBrokerRelationshipsT != "OriginatingCompany" && this.PopulateBrokerRelationshipsT != "0")
                {
                    validRelationships.AddRange(availableBrokerRelationships);
                }
                else if (availableBrokerRelationships.Any(relationship => this.IsValueSet(relationship.EmployeeName)))
                {
                    var message = "Broker relationships are determined by originating company. Please exclude " +
                        "all broker relationship fields except broker processor, if applicable.";

                    returnList.Add(message);
                }

                if (this.PopulateMiniCorrRelationshipsT != "OriginatingCompany" && this.PopulateMiniCorrRelationshipsT != "0")
                {
                    validRelationships.AddRange(availableMiniCorrespondentRelationships);
                }
                else if (availableMiniCorrespondentRelationships.Any(relationship => this.IsValueSet(relationship.EmployeeName)))
                {
                    var message = "Mini-Correspondent relationships are determined by originating company. Please exclude " +
                           "all mini-correspondent relationship fields except external post closer, if applicable.";

                    returnList.Add(message);
                }

                if (this.PopulateCorrRelationshipsT != "OriginatingCompany" && this.PopulateCorrRelationshipsT != "0")
                {
                    validRelationships.AddRange(availableCorrespondentRelationships);
                }
                else if (availableCorrespondentRelationships.Any(relationship => this.IsValueSet(relationship.EmployeeName)))
                {
                    var message = "Correspondent relationships are determined by originating company. Please exclude " +
                        "all correspondent relationship fields except external post closer, if applicable.";

                    returnList.Add(message);
                }

                foreach (var relationship in validRelationships)
                {
                    if (this.IsValueSet(relationship.EmployeeName) &&
                        this.brokerLoanAssignmentTable[relationship.RoleT] == null)
                    {
                        this.brokerLoanAssignmentTable.Retrieve(principal.BrokerId, relationship.RoleT);
                    }

                    var errorMessage = this.SetRelationship(relationship);

                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        returnList.Add(errorMessage);
                    }
                }
            }
            catch (Exception e)
            {
                Tools.LogError("Unable to load role table in PmlUserRecord.", e);
                returnList.Add("Unable to set relationships");
            }

            return returnList;
        }

        protected List<string> SetRelationshipsForPmlSupervisor()
        {
            var result = new List<string>();

            var relationshipsToCheck = new List<RelationshipImportSetting>()
            {
                new RelationshipImportSetting(
                    this.BrokerProcessor,
                    E_RoleT.Pml_BrokerProcessor,
                    (id) => this.employee.BrokerProcessorId = id,
                    (id) => this.employeeEditing.BrokerProcessorEmployeeId = id,
                    () => this.employee.BrokerProcessorId = Guid.Empty,
                    () => this.employeeEditing.BrokerProcessorEmployeeId = Guid.Empty),
                new RelationshipImportSetting(
                    this.MiniCorrespondentExternalPostCloser,
                    E_RoleT.Pml_PostCloser,
                    (id) => this.employee.MiniCorrespondentExternalPostCloserId = id,
                    (id) => this.employeeEditing.MiniCorrExternalPostCloserEmployeeId = id,
                    () => this.employee.MiniCorrespondentExternalPostCloserId = Guid.Empty,
                    () => this.employeeEditing.MiniCorrExternalPostCloserEmployeeId = Guid.Empty),
                new RelationshipImportSetting(
                    this.CorrespondentExternalPostCloser,
                    E_RoleT.Pml_PostCloser,
                    (id) => this.employee.CorrespondentExternalPostCloserId = id,
                    (id) => this.employeeEditing.CorrExternalPostCloserEmployeeId = id,
                    () => this.employee.CorrespondentExternalPostCloserId = Guid.Empty,
                    () => this.employeeEditing.CorrExternalPostCloserEmployeeId = Guid.Empty)
            };

            foreach (var relationship in relationshipsToCheck)
            {
                if (this.IsValueSet(relationship.EmployeeName) &&
                    this.brokerLoanAssignmentTable[relationship.RoleT] == null)
                {
                    this.brokerLoanAssignmentTable.Retrieve(principal.BrokerId, relationship.RoleT);
                }

                string assignmentResult = this.SetRelationship(relationship);

                result.Add(assignmentResult);
            }

            return result;
        }

        protected List<string> SetPriceGroups()
        {
            var priceGroupsToCheck = new List<ImportSetting>()
            {
                new ImportSetting(
                    this.PriceGroup,
                    (id) => this.employee.PriceGroup = id,
                    (id) => this.employeeEditing.LpePriceGroupID = id,
                    () => this.employee.PriceGroup = Guid.Empty,
                    () => this.employeeEditing.LpePriceGroupID = Guid.Empty,
                    "Price Group not found",
                    true),
                new ImportSetting(
                    this.MiniCorrespondentPriceGroup,
                    (id) => this.employee.MiniCorrespondentPriceGroupId = id,
                    (id) => this.employeeEditing.MiniCorrespondentPriceGroupID = id,
                    () => this.employee.UseOriginatingCompanyMiniCorrPriceGroupId = true,
                    () => this.employeeEditing.UseOriginatingCompanyMiniCorrPriceGroupId = true,
                    "Mini-Correspondent Price Group not found",
                    false),
                new ImportSetting(
                    this.CorrespondentPriceGroup,
                    (id) => this.employee.CorrespondentPriceGroupId = id,
                    (id) => this.employeeEditing.CorrespondentPriceGroupID = id,
                    () => this.employee.UseOriginatingCompanyCorrPriceGroupId = true,
                    () => this.employeeEditing.UseOriginatingCompanyCorrPriceGroupId = true,
                    "Correspondent Price Group not found",
                    false)
            };

            var result = new List<string>();

            foreach (var priceGroupSetting in priceGroupsToCheck)
            {
                result.Add(this.SetPriceGroup(priceGroupSetting));
            }

            return result;
        }

        private string SetPriceGroup(ImportSetting priceGroupSetting)
        {
            if (this.IsValueSetToEmpty(priceGroupSetting.Name))
            {
                if (this.IsCreating)
                {
                    priceGroupSetting.SetDefaultForNewEmployee();
                }
                else
                {
                    priceGroupSetting.SetDefaultForExistingEmployee();
                }

                return "";
            }

            if ((!this.IsCreating || !priceGroupSetting.IsRequiredForCreation) && this.IsValueBeingIgnored(priceGroupSetting.Name))
            {
                return "";
            }

            if (this.priceGroupIDByName.ContainsKey(priceGroupSetting.Name.TrimWhitespaceAndBOM().ToLower()))
            {
                Guid priceGroupID = this.priceGroupIDByName[priceGroupSetting.Name.TrimWhitespaceAndBOM().ToLower()];

                if (this.IsCreating)
                {
                    priceGroupSetting.SetForNewEmployee(priceGroupID);
                }
                else
                {
                    priceGroupSetting.SetForExistingEmployee(priceGroupID);
                }
            }
            else
            {
                return priceGroupSetting.NotFoundMessage;
            }

            return "";
        }

        private string SetBranch(ImportSetting branchSetting)
        {
            if (this.IsValueSetToEmpty(branchSetting.Name))
            {
                if (this.IsCreating)
                {
                    branchSetting.SetDefaultForNewEmployee();
                }
                else
                {
                    branchSetting.SetDefaultForExistingEmployee();
                }

                return "";
            }

            if ((!this.IsCreating || !branchSetting.IsRequiredForCreation) && this.IsValueBeingIgnored(branchSetting.Name))
            {
                return "";
            }

            if (this.branchIDByName.ContainsKey(branchSetting.Name.TrimWhitespaceAndBOM().ToLower()))
            {
                Guid branchID = this.branchIDByName[branchSetting.Name.TrimWhitespaceAndBOM().ToLower()];

                if (this.IsCreating)
                {
                    branchSetting.SetForNewEmployee(branchID);
                }
                else
                {
                    branchSetting.SetForExistingEmployee(branchID);
                }
            }
            else
            {
                return branchSetting.NotFoundMessage;
            }

            return "";
        }

        protected List<string> SetBranches()
        {
            var branchesToCheck = new List<ImportSetting>()
            {
                new ImportSetting(
                    this.BranchName,
                    (id) => this.employee.BranchId = id,
                    (id) => this.employeeEditing.BranchID = id,
                    () => this.employee.BranchId = principal.BranchId,
                    () => this.employeeEditing.BranchID = principal.BranchId,
                    "Branch Name not found",
                    true),
                new ImportSetting(
                    this.MiniCorrespondentBranchName,
                    (id) => this.employee.MiniCorrespondentBranchId = id,
                    (id) => this.employeeEditing.MiniCorrespondentBranchID = id,
                    () => this.employee.UseOriginatingCompanyMiniCorrBranchId = true,
                    () => this.employeeEditing.UseOriginatingCompanyMiniCorrBranchId = true,
                    "Mini-Correspondent Branch Name not found",
                    false),
                new ImportSetting(
                    this.CorrespondentBranchName,
                    (id) => this.employee.CorrespondentBranchId = id,
                    (id) => this.employeeEditing.CorrespondentBranchID = id,
                    () => this.employee.UseOriginatingCompanyCorrBranchId = true,
                    () => this.employeeEditing.UseOriginatingCompanyCorrBranchId = true,
                    "Correspondent Branch Name not found",
                    false),
            };

            var result = new List<string>();

            foreach (var branch in branchesToCheck)
            {
                result.Add(this.SetBranch(branch));
            }

            return result;
        }

        private string SetLandingPage(ImportSetting landingPageSetting)
        {
            if (this.IsValueSetToEmpty(landingPageSetting.Name))
            {
                if (this.IsCreating)
                {
                    landingPageSetting.SetDefaultForNewEmployee();
                }
                else
                {
                    landingPageSetting.SetDefaultForExistingEmployee();
                }

                return "";
            }
            else if (this.IsValueBeingIgnored(landingPageSetting.Name))
            {
                return "";
            }
            else if (this.landingPages.ContainsKey(landingPageSetting.Name))
            {
                Guid landingPageID = this.landingPages[landingPageSetting.Name];

                if (this.IsCreating)
                {
                    landingPageSetting.SetForNewEmployee(landingPageID);
                }
                else
                {
                    landingPageSetting.SetForExistingEmployee(landingPageID);
                }
            }
            else
            {
                return landingPageSetting.NotFoundMessage;
            }

            return "";
        }

        protected List<string> SetLandingPages()
        {
            var landingPagesToCheck = new List<ImportSetting>()
            {
                new ImportSetting(
                    this.TPOLandingPage,
                    (id) => this.employee.TPOLandingPageID = id,
                    (id) => this.employeeEditing.TPOLandingPageID = id,
                    () => this.employee.TPOLandingPageID = null,
                    () => this.employeeEditing.TPOLandingPageID = null,
                    "Broker Landing Page not found",
                    false),
                new ImportSetting(
                    this.MiniCorrespondentLandingPage,
                    (id) => this.employee.MiniCorrespondentLandingPageID = id,
                    (id) => this.employeeEditing.MiniCorrespondentLandingPageID = id,
                    () => this.employee.MiniCorrespondentLandingPageID = null,
                    () => this.employeeEditing.MiniCorrespondentLandingPageID = null,
                    "Mini-Correspondent Landing Page not found",
                    false),
                new ImportSetting(
                    this.CorrespondentLandingPage,
                    (id) => this.employee.CorrespondentLandingPageID = id,
                    (id) => this.employeeEditing.CorrespondentLandingPageID = id,
                    () => this.employee.CorrespondentLandingPageID = null,
                    () => this.employeeEditing.CorrespondentLandingPageID = null,
                    "Correspondent Landing Page not found",
                    false)
            };

            var result = new List<string>();

            foreach (var landingPageSetting in landingPagesToCheck)
            {
                result.Add(this.SetLandingPage(landingPageSetting));
            }
            
            return result;
        }

        private struct RelationshipImportSetting
        {
            public readonly string EmployeeName;
            public readonly E_RoleT RoleT;
            public readonly Action<Guid> SetForNewEmployee;
            public readonly Action<Guid> SetForExistingEmployee;
            public readonly Action ClearForNewEmployee;
            public readonly Action ClearForExistingEmployee;

            public RelationshipImportSetting(
                string employeeName,
                E_RoleT roleT,
                Action<Guid> setForNewEmployee,
                Action<Guid> setForExistingEmployee,
                Action clearForNewEmployee,
                Action clearForExistingEmployee)
            {
                this.EmployeeName = employeeName;
                this.RoleT = roleT;
                this.SetForNewEmployee = setForNewEmployee;
                this.SetForExistingEmployee = setForExistingEmployee;
                this.ClearForNewEmployee = clearForNewEmployee;
                this.ClearForExistingEmployee = clearForExistingEmployee;
            }
        }

        private struct ImportSetting
        {
            public readonly string Name;
            public readonly Action<Guid> SetForNewEmployee;
            public readonly Action<Guid> SetForExistingEmployee;
            public readonly Action SetDefaultForNewEmployee;
            public readonly Action SetDefaultForExistingEmployee;
            public readonly string NotFoundMessage;
            public readonly bool IsRequiredForCreation;

            public ImportSetting(
                string name,
                Action<Guid> setForNewEmployee,
                Action<Guid> setForExistingEmployee,
                Action setDefaultForNewEmployee,
                Action setDefaultForExistingEmployee,
                string notFoundMessage,
                bool isRequiredForCreation)
            {
                this.Name = name;
                this.SetForNewEmployee = setForNewEmployee;
                this.SetForExistingEmployee = setForExistingEmployee;
                this.SetDefaultForNewEmployee = setDefaultForNewEmployee;
                this.SetDefaultForExistingEmployee = setDefaultForExistingEmployee;
                this.NotFoundMessage = notFoundMessage;
                this.IsRequiredForCreation = isRequiredForCreation;
            }
        }
    }
}