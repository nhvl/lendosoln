// <copyright file="IntegrationInvalidLoanReferenceNumberException.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   09/01/2016
// </summary>
namespace LendersOfficeApp.WebService
{
    using System;
    using DataAccess;

    /// <summary>
    /// Should be thrown when loan.asmx runs into an invalid reference number.
    /// </summary>
    public class IntegrationInvalidLoanReferenceNumberException : CBaseException 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrationInvalidLoanReferenceNumberException" /> class.
        /// </summary>
        /// <param name="loanRefNm">Loan Reference Number.</param>
        public IntegrationInvalidLoanReferenceNumberException(string loanRefNm) : base("[" + loanRefNm + "] is an invalid loan reference number.", "Integration Error: [" + loanRefNm + "] is an invalid loan reference number.") 
        {
        }

        /// <summary>
        /// Gets the email subject code.
        /// </summary>
        /// <value>The email subject code.</value>
        public override string EmailSubjectCode 
        {
            get { return "INTG_INVALID_LOAN"; }
        }
    }
}
