namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Web;
    using System.Web.Services;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using EDocs;
    using iTextSharp.text.exceptions;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.CustomPmlFields;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.DU;
    using LendersOffice.Email;
    using LendersOffice.ObjLib.ConsumerPortal;
    using LendersOffice.Pdf;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;

    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class ConsumerPortal : System.Web.Services.WebService
    {
        /// <summary>
        /// Convert the XML with the following format to dictionary
        /// 
        ///     &lt;RootElement&gt;
        ///         &lt;field id="key"&gt;value&lt;/field&gt;
        ///     &lt;/RootElement&gt;
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private Dictionary<string, string> ConvertToDictionary(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            }

            XDocument doc = XDocument.Parse(xml);

            return ConvertToDictionary(doc);
        }

        /// <summary>
        /// Convert the XML with the following format to dictionary
        /// 
        ///     &lt;RootElement&gt;
        ///         &lt;field id="key"&gt;value&lt;/field&gt;
        ///     &lt;/RootElement&gt;
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        private Dictionary<string, string> ConvertToDictionary(XDocument doc)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            if (doc == null)
            {
                return dictionary;
            }

            foreach (XElement item in doc.Descendants("field"))
            {
                dictionary.Add(item.Attribute("id").Value, item.Value);
            }
            return dictionary;
        }

        /// <summary>
        /// Converts string into a guid and returns true if succesful else false 
        /// and empty guid. 
        /// </summary>
        /// <param name="value">The string containing the guid</param>
        /// <param name="id">The guid to populate</param>
        /// <returns>Success</returns>
        private bool TryParseGuid(string value, out Guid id)
        {
            try
            {
                id = new Guid(value);
                return true;
            }
            catch (FormatException)
            {
                id = Guid.Empty;
                return false;
            }
        }

        private ConsumerPortalUser GetPortalUser(ConsumerPortalConfig config, string email)
        {
            ConsumerPortalUser user = ConsumerPortalUser.Retrieve(config.BrokerId, email);

            if (user == null)
            {
                throw new NotFoundException(email + " is not found.", email + " is not found");
            }
            return user;
        }

        /// <summary>
        /// Use this method to verify the partner key.
        /// 
        /// The hash of partner key must match with value in StageConstants.
        /// This method will throw exception if partner key does not match.
        /// TODO: We will implement an IP check.
        /// </summary>
        /// <param name="sPartnerKey"></param>
        private void AuthenticatePartnerKey(string sPartnerKey)
        {
            AuthServiceHelper.CheckKey(sPartnerKey);

            string currentIP = RequestHelper.ClientIP;

            if (!ConstStage.AllowableIPsForAuthenticationPartnerWebservice.Contains(currentIP) && !RequestHelper.IsLOAdminIP(currentIP))
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "Invalid IP address");
            }
        }

        private ConsumerPortalConfig GetConsumerPortalConfig(string sConsumerPortalId)
        {
            Guid portalId = new Guid(sConsumerPortalId);

            ConsumerPortalConfig config = ConsumerPortalConfig.RetrieveWithoutBrokerIdSlowSlow(portalId);
            if (config == null)
            {
                throw CBaseException.GenericException("Could not find portal.");
            }
            return config;
        }

        private string GenerateErrorMessage(CBaseException exc)
        {
            if (exc is AccessDenied)
            {
                //no op - dont need to log these.
            }
            else
            {
                Tools.LogErrorWithCriticalTracking(exc);
            }

            return this.GenerateErrorMessage(exc.UserMessage);
        }

        private string GenerateErrorMessage(string msg)
        {
            return "ERR_MSG: " + msg;
        }

        [WebMethod(Description = @"Return settings for the consumer site")]
        public string GetSettings(string sPartnerKey, string sConsumerPortalId, string sEmployeeLoanRepLogin)
        {
            Tools.LogInfo("ConsumerPortal.asmx::GetSettings. sConsumerPortalId=[" + sConsumerPortalId + "], sEmployeeLoanRepLogin=[" + sEmployeeLoanRepLogin + "]");
            
            var passedInParametersMessage = string.Format(
                    "Parameters to GetSettings were: sPartnerKey [NOT LOGGED DUE TO SECURITY], sConsumerPortalId '{0}', sEmployeeLoanRepLogin '{1}'",
                    sConsumerPortalId,
                    sEmployeeLoanRepLogin);

            try
            {
                AuthenticatePartnerKey(sPartnerKey);
                ConsumerPortalConfig config = GetConsumerPortalConfig(sConsumerPortalId);
                BrokerDB brokerInfo = BrokerDB.RetrieveById(config.BrokerId);
                XElement element = new XElement("settings");

                string availableLockPeriods = string.Empty;
                foreach (int lockPeriod in config.AvailableLockPeriods)
                {
                    if (string.IsNullOrEmpty(availableLockPeriods) == false)
                    {
                        availableLockPeriods += ";";
                    }
                    availableLockPeriods += lockPeriod.ToString();
                }

                string displayedReferralSources = string.Empty;
                foreach (var o in config.DisplayedReferralSources)
                {
                    if (string.IsNullOrEmpty(displayedReferralSources) == false)
                    {
                        displayedReferralSources += ";";
                    }
                    displayedReferralSources += o.Name;

                }

                string availableStates = string.Empty;
                ///Summary
                ///5/5/2014 bs - Opm 180374. If bit "UseLegacyBehvaiorForSubjPropStateInNewCPORTAL" is true, then use the legacy behavior to determine valid state options in the subj property dropdown 
                ///instead of using the LO's licensed state list
                ///
                if (brokerInfo.UseLegacyBehaviorForSubjPropStateInNewCPORTAL == true)
                {
                    foreach (var o in config.AvailableStates)
                    {
                        if (string.IsNullOrEmpty(availableStates) == false)
                        {
                            availableStates += ";";
                        }
                        availableStates += o.Key + ":" + o.Value;
                    }
                }
                else
                {
                    ///Summary
                    ///5/5/2014 bs - Opm 180374. If LO is being passed through query, it will pull LO's licensed state
                    ///
                    EmployeeDB employeeDB = null;
                    if (!string.IsNullOrEmpty(sEmployeeLoanRepLogin))
                    {
                        Guid employeeId = Guid.Empty;
                        SqlParameter[] parameters = {
                                                    new SqlParameter("@LoginName", sEmployeeLoanRepLogin),
                                                    new SqlParameter("@BrokerId", config.BrokerId)
                                                    };
                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(config.BrokerId, "GetEmployeeIdByLoginName", parameters))
                        {
                            if (reader.Read())
                            {
                                bool isActive = (bool)reader["IsActive"];
                                if (isActive)
                                {
                                    employeeId = (Guid)reader["EmployeeId"];
                                }
                            }
                        }

                        if (employeeId != Guid.Empty)
                        {
                            employeeDB = new EmployeeDB(employeeId, config.BrokerId);
                            employeeDB.Retrieve();
                            Tools.LogInfo("ConsumerPortal.asmx::GetSettings for sConsumerPortalId=[" + sConsumerPortalId + "], use LO=[" + sEmployeeLoanRepLogin + "] setting");
                        }
                    }
                    
                    // 12/12/2015 BS - OPM 233781. For any reason if LO is not valid, then it will use default LO/Lead
                    if (employeeDB == null)
                    {
                        ///Summary
                        ///5/5/2014 bs - Opm 180374. If LO is not being passed through query, it will use default LO in Consumer portal config 
                        ///
                        if (config.LoanOfficerEmployeeDB != null)
                        {
                            employeeDB = config.LoanOfficerEmployeeDB;
                            Tools.LogInfo("ConsumerPortal.asmx::GetSettings for sConsumerPortalId=[" + sConsumerPortalId + "], use default LO=[" + config.LoanOfficerEmployeeDB.FullName + "] setting");
                        }
                        ///Summary
                        ///5/5/2014 bs - Opm 180374. If LO is not being passed through query and there's no default loan officer, it will use default Lead Manager in Consumer portal config 
                        ///
                        else
                        {
                            employeeDB = config.LeadManagerEmployeeDB;
                            Tools.LogInfo("ConsumerPortal.asmx::GetSettings. sConsumerPortalId=[" + sConsumerPortalId + "], use default Lead=[" + config.LeadManagerEmployeeDB.FullName + "] setting");
                        }
                    }

                    ArrayList licenseInfoList = employeeDB.LicenseInformationList.List;
                    foreach (LicenseInfo licenseInfo in licenseInfoList)
                    {
                        if (licenseInfo.ExpirationDate.Date < DateTime.Now.Date)
                            continue;
                        foreach (var o in config.AvailableStates)
                        {
                            if (licenseInfo.State.Equals(o.Key))
                            {
                                availableStates += o.Key + ":" + o.Value + ";";
                            }
                        }
                    }
                }

                element.SetAttributeValue("Name", config.Name);
                element.SetAttributeValue("ReferralUrl", config.ReferralUrl);
                element.SetAttributeValue("StyleSheetUrl", config.StyleSheetUrl);
                element.SetAttributeValue("IsAskForReferralSource", config.IsAskForReferralSource);
                element.SetAttributeValue("DisplayedReferralSources", displayedReferralSources);
                element.SetAttributeValue("IsAllowLockPeriodSelection", config.IsAllowLockPeriodSelection);
                element.SetAttributeValue("AvailableLockPeriods", availableLockPeriods);
                element.SetAttributeValue("AvailableStates", availableStates);
                element.SetAttributeValue("IsHideLoanOfficer", config.IsHideLoanOfficer);
                element.SetAttributeValue("GetRatesPageDisclaimer", config.GetRatesPageDisclaimer);
                element.SetAttributeValue("OnlineApplicationTerms", config.OnlineApplicationTerms);
                element.SetAttributeValue("OnlineApplicationCreditCheck", config.OnlineApplicationCreditCheck);
                element.SetAttributeValue("EligibleProgramDisclaimer", config.EligibleProgramDisclaimer);
                element.SetAttributeValue("NoEligibleProgramMessage", config.NoEligibleProgramMessage);
                element.SetAttributeValue("F1003SubmissionReceiptT", config.F1003SubmissionReceiptT);
                element.SetAttributeValue("ShortApplicationConfirmation", config.ShortApplicationConfirmation);
                element.SetAttributeValue("FullApplicationConfirmation", config.FullApplicationConfirmation);
                element.SetAttributeValue("ContactInfoMessage", config.ContactInfoMessage);
                element.SetAttributeValue("HelpLinkPagesHostURL", config.HelpLinkPagesHostURL);
                element.SetAttributeValue("IsHelpForGetRatesEnabled", config.IsHelpForGetRatesEnabled);
                element.SetAttributeValue("IsHelpForApplicantLoginEnabled", config.IsHelpForApplicantLoginEnabled);
                element.SetAttributeValue("IsHelpForApplicantLoanPipelineEnabled", config.IsHelpForApplicantLoanPipelineEnabled);
                element.SetAttributeValue("IsHelpForPersonalInformationEnabled", config.IsHelpForPersonalInformationEnabled);
                element.SetAttributeValue("IsHelpForLoanInformationEnabled", config.IsHelpForLoanInformationEnabled);
                element.SetAttributeValue("IsHelpForPropertyInformationEnabled", config.IsHelpForPropertyInformationEnabled);
                element.SetAttributeValue("IsHelpForLoanOfficerEnabled", config.IsHelpForLoanOfficerEnabled);
                element.SetAttributeValue("IsHelpForAssetInformationEnabled", config.IsHelpForAssetInformationEnabled);
                element.SetAttributeValue("IsHelpForLiabilityInformationEnabled", config.IsHelpForLiabilityInformationEnabled);
                element.SetAttributeValue("IsHelpForExpenseInformationEnabled", config.IsHelpForExpenseInformationEnabled);
                element.SetAttributeValue("IsHelpForIncomeInformationEnabled", config.IsHelpForIncomeInformationEnabled);
                element.SetAttributeValue("IsHelpForDeclarationsEnabled", config.IsHelpForDeclarationsEnabled);
                element.SetAttributeValue("IsHelpForSelectLoanEnabled", config.IsHelpForSelectLoanEnabled);
                element.SetAttributeValue("IsHelpForSubmitApplicationEnabled", config.IsHelpForSubmitApplicationEnabled);
                element.SetAttributeValue("IsHelpForLoanStatusAndDocExchangeEnabled", config.IsHelpForLoanStatusAndDocExchangeEnabled);
                element.SetAttributeValue("IsAutomaticPullCredit", config.IsAutomaticPullCredit);
                element.SetAttributeValue("CompanyName", brokerInfo.Name);
                element.SetAttributeValue("CompanyAddress", brokerInfo.Address.StreetAddress + " " + brokerInfo.Address.CityStateZipcode );
                element.SetAttributeValue("CompanyPhone", brokerInfo.Phone);
                element.SetAttributeValue("SessionTimeoutInMinutes", config.SessionTimeoutInMinutes);
                element.SetAttributeValue("IsAskForMemberId", config.IsAskForMemberId);
                element.SetAttributeValue("IsEnableHomeEquityInLoanPurpose", config.IsEnableHomeEquityInLoanPurpose);
                element.SetAttributeValue("ARMProgramDisclosuresURL", config.ARMProgramDisclosuresURL);

      element.SetAttributeValue("SelectsLoanProgramInFullSubmission", (int)config.SelectsLoanProgramInFullSubmission);
element.SetAttributeValue("IsEnableHELOC", config.IsEnableHELOC);
                element.SetAttributeValue("DefaultLoanTemplateNm", config.DefaultLoanTemplateNm);
                // 5/30/2014 BS - OPM 182025 - Include Automatically Run DU field
                element.SetAttributeValue("IsAutomaticRunDU", config.IsAutomaticRunDU);
                element.SetAttributeValue("IsGenerateLeadOnShortAppSubmission", config.IsGenerateLeadOnShortAppSubmission);
                element.SetAttributeValue("AskConsumerForSubjProperty", (int)config.AskConsumerForSubjProperty);
                element.SetAttributeValue("AskConsumerForSubjValue", (int)config.AskConsumerForSubjValue);
                // 10/21/2014 EJM - OPM 184659 - Include PML 2.0 Enabled
                element.SetAttributeValue("IsNewPmlUIEnabled", brokerInfo.IsNewPmlUIEnabled);
                // 11/20/2014 JE - OPM 194062 - Add Release Notification Warning
                element.SetAttributeValue("UserNotification", ConstStage.CPortalUserNotification.TrimWhitespaceAndBOM());

                element.SetAttributeValue("IsAllowRespaOnly_Purchase", config.IsAllowRespaOnly_Purchase);
                element.SetAttributeValue("IsAllowRespaOnly_Refinance", config.IsAllowRespaOnly_Refinance);
                element.SetAttributeValue("IsAllowRespaOnly_HomeEquity", config.IsAllowRespaOnly_HomeEquity);
                element.SetAttributeValue("EnableShortApplications", config.EnableShortApplications);
                element.SetAttributeValue("EnableFullApplications", config.EnableFullApplications);
                element.SetAttributeValue("AllowRegisterNewAccounts", config.AllowRegisterNewAccounts);

                // 8/29/2013 GF - OPM 136980 - Export the custom PML fields sorted by rank.
                var customPmlFieldsXml = new XElement("CustomPmlFields");
                var customPmlFields = new List<CustomPmlField>();
                foreach (var keywordNum in config.EnabledCustomPmlFields_FullApp.Union(config.EnabledCustomPmlFields_QuickPricer))
                {
                    customPmlFields.Add(brokerInfo.CustomPmlFieldList.Get(keywordNum));
                }
                foreach (var customPmlField in customPmlFields.OrderBy(a => a.Rank))
                {
                    if (!customPmlField.IsValid) continue; // Shouldn't happen. Regardless, don't want to add.

                    var customPmlFieldXml = new XElement("CustomPmlField",
                        new XAttribute("Id", string.Format("sCustomPMLField{0}", customPmlField.KeywordNum)),
                        new XAttribute("Type", customPmlField.Type.ToString("d")),
                        new XAttribute("Description", customPmlField.Description),
                        new XAttribute("Visibility", customPmlField.VisibilityType.ToString("d")),
                        new XAttribute("AskInFullApp", config.EnabledCustomPmlFields_FullApp.Contains(customPmlField.KeywordNum)),
                        new XAttribute("AskInQuickPricer", config.EnabledCustomPmlFields_QuickPricer.Contains(customPmlField.KeywordNum)));
                    if (customPmlField.Type == E_CustomPmlFieldType.Dropdown)
                    {
                        var select = new XElement("SelectMap");
                        foreach (var kvp in customPmlField.EnumMapKeyValuePairs)
                        {
                            var option = new XElement("Option",
                                new XAttribute("Value", kvp.Key),
                                new XAttribute("Description", kvp.Value));
                            select.Add(option);
                        }
                        customPmlFieldXml.Add(select);
                    }
                    customPmlFieldsXml.Add(customPmlFieldXml);
                }
                element.Add(customPmlFieldsXml);

                return element.ToString();
            }
            catch (CBaseException exc)
            {
                Tools.LogError(passedInParametersMessage, exc);

                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(passedInParametersMessage, e);

                throw;
            }
        }

        [WebMethod(Description = @"Call this service when user login using email and password. 
        If the return string start with ERR_MSG then email/password are invalid")]
        public string GetConsumerAuthTicket(string sPartnerKey, string sConsumerPortalId,
            string sEmail, string sPassword)
        {

            try
            {
                AuthenticatePartnerKey(sPartnerKey);

                ConsumerPortalConfig config = GetConsumerPortalConfig(sConsumerPortalId);

                PrincipalFactory.E_LoginProblem loginProblem = PrincipalFactory.E_LoginProblem.None;


                ConsumerPortalUserPrincipal principal = PrincipalFactory.CreateConsumerPrincipalWithFailureType(
                    config.BrokerId, config.Id, sEmail, sPassword, out loginProblem);

                if (loginProblem == PrincipalFactory.E_LoginProblem.None)
                {
                    ConsumerPortalUserTicket ticket = ConsumerPortalUserTicket.Create(config.Id, principal);

                    return CreateAuthTicket(ticket);
                }
                else
                {
                    return "ERR_MSG: Invalid email/password.";
                }

            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string GetConsumerAuthTicketForSubmittedLoan(string sPartnerKey, string sTicket, string sLNm, string partialSsn)
        {

            try
            {
                AuthenticatePartnerKey(sPartnerKey);

                XElement el = XElement.Parse(sTicket);

                ConsumerPortalUserTicket ticket = ConsumerPortalUserTicket.Decrypt(el.Attribute("EncryptedTicket").Value);
                ticket.ValidateLoanAndPartialSsn(sLNm, partialSsn);

                return CreateAuthTicket(ticket);
            }
            catch (CBaseException exc)
            {

                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod(Description = @"Call this method before Load/Save api. This method will extend 
                                 the timeout of ticket without require user email/password")]
        public string RenewConsumerAuthTicket(string sPartnerKey, string sTicket)
        {

            try
            {
                AuthenticatePartnerKey(sPartnerKey);

                XElement el = XElement.Parse(sTicket);

                ConsumerPortalUserTicket ticket = ConsumerPortalUserTicket.Decrypt(el.Attribute("EncryptedTicket").Value);

                ticket.ExtendExpiration();

                return CreateAuthTicket(ticket);
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        private string CreateAuthTicket(ConsumerPortalUserTicket ticket)
        {
            XElement ticketElement = new XElement("CONSUMER_USER_TICKET");
            ticketElement.SetAttributeValue("EncryptedTicket", ticket.Encrypt());
            ticketElement.SetAttributeValue("IsTemporaryPassword", ticket.Principal.IsTemporaryPassword ? "True" : "False");
            ticketElement.SetAttributeValue("Site", ConstStage.SiteCode);
            return ticketElement.ToString();

        }

        [WebMethod]
        public string CreateConsumer(string sPartnerKey, string sConsumerPortalId, string sConsumerInfoXml)
        {

            try
            {
                // <Consumer>
                //      <field id="FirstName"></field>
                //      <field id="LastName"></field>
                //      <field id="Phone"></field>
                //      <field id="Email"></field>
                //      <field id="SecurityPin"></field>
                //      <field id="ReferralSource"></field>
                //      <field id="PropertyState">IN</field>
                //      <field id="sEmployeeLoanRepLogin">{LO name}</field>
                // </Consumer>

                // Log XML
                string logXml = string.Empty;
                XDocument xmldoc = null;
                if (!string.IsNullOrWhiteSpace(sConsumerInfoXml))
                {
                    xmldoc = XDocument.Parse(sConsumerInfoXml);
                    XElement pin = xmldoc.Descendants("field").FirstOrDefault(e => e.Attribute("id").Value == "SecurityPin");
                    if (pin != null)
                    {
                        string oldPinValue = pin.Value;
                        pin.Value = ConstAppDavid.FakePasswordDisplay;
                        logXml = xmldoc.ToString(SaveOptions.None);
                        pin.Value = oldPinValue;
                    }
                }

                Tools.LogInfo("ConsumerPortal.asmx::CreateConsumer. sConsumerInfoXml=[" + logXml + "]");

                AuthenticatePartnerKey(sPartnerKey);
                
                ConsumerPortalConfig config = GetConsumerPortalConfig(sConsumerPortalId);

                Dictionary<string, string> dictionary = ConvertToDictionary(xmldoc);
                string emailAddr = SafeString(dictionary, "Email");

                ConsumerPortalUser user = ConsumerPortalUser.Retrieve(config.BrokerId, emailAddr);

                if (user != null)
                {
                    var msg = "Account already exists.";
                    Tools.LogInfo(msg);
                    return this.GenerateErrorMessage(msg);
                }

                user = ConsumerPortalUser.Create(config.BrokerId, emailAddr);
                user.FirstName = SafeString(dictionary, "FirstName");
                user.LastName = SafeString(dictionary, "LastName");
                user.Phone = SafeString(dictionary, "Phone");
                user.SecurityPin = SafeString(dictionary, "SecurityPin");

                string referralSource = SafeString(dictionary, "ReferralSource");

                if (string.IsNullOrEmpty(referralSource) == false)
                {
                    // 9/13/2013 dd - In eOPM 449435 - Referral Source can be id or description.
                    // But when save in Consumer User profile, always store the description.
                    foreach (var o in config.DisplayedReferralSources)
                    {
                        if (o.Id.HasValue && o.Id.Value.ToString() == referralSource)
                        {
                            user.ReferralSource = o.Name;
                            break;
                        }
                        else if (o.Name.Equals(referralSource, StringComparison.OrdinalIgnoreCase))
                        {
                            user.ReferralSource = o.Name;
                            break;

                        }
                    }
                }
                string tempPassword = user.GenerateTempPassword();
                user.Save();

                ConsumerPortalUserTicket ticket = ConsumerPortalUserTicket.Create(config.Id, user);


                CBaseEmail email = new CBaseEmail(config.BrokerId);
                email.From = config.NotificationFromEmailAddress;
                email.To = user.Email;
                email.Subject = "Temporary Password";
                email.Message = "Temporary password is: " + tempPassword;

                email.SendImmediately();

                // OPM 169699 - Send notification email to LO or lead manager
                EmployeeDB employeeDB = null;
                string sEmployeeLoanRepLogin = SafeString(dictionary, "sEmployeeLoanRepLogin");

                if (!string.IsNullOrEmpty(sEmployeeLoanRepLogin))
                {
                    Guid employeeId = Guid.Empty;
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@LoginName", sEmployeeLoanRepLogin),
                                                    new SqlParameter("@BrokerId", config.BrokerId)
                                                };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(config.BrokerId, "GetEmployeeIdByLoginName", parameters))
                    {
                        if (reader.Read())
                        {
                            bool isActive = (bool)reader["IsActive"];
                            if (isActive)
                            {
                                employeeId = (Guid)reader["EmployeeId"];
                            }
                            else
                            {

                                Tools.LogInfo("[CPNewUser] User Not active: " + sEmployeeLoanRepLogin);
                            }

                        }
                        else
                        {
                            Tools.LogInfo("[CPNewUser]  Did not find user " + sEmployeeLoanRepLogin);
                        }
                    }

                    if (employeeId != Guid.Empty)
                    {
                        employeeDB = new EmployeeDB(employeeId, config.BrokerId);
                        employeeDB.Retrieve();
                    }
                }

                if (employeeDB == null)
                {
                    employeeDB = config.LeadManagerEmployeeDB;
                }

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Property Location: " + SafeString(dictionary, "PropertyState"));
                sb.AppendLine("First Name: " + user.FirstName);
                sb.AppendLine("Last Name: " + user.LastName);
                sb.AppendLine("Phone Number: " + user.Phone);
                sb.AppendLine("Email Address: " + user.Email);

                email = new CBaseEmail(config.BrokerId);
                email.From = config.NotificationFromEmailAddress;
                email.To = employeeDB.Email;
                email.Subject = "A New Consumer Has Registered to Apply Online";
                email.Message = sb.ToString();
                email.SendImmediately();

                Tools.LogInfo("CreateConsumer notification email sent to " + email.To);

                return CreateAuthTicket(ticket);
            }
            catch (System.Xml.XmlException xmlEx)
            {
                string logXml = Tools.RedactXmlUsingRegex(sConsumerInfoXml, "field id=\"SecurityPin\"");
                Tools.LogError("ConsumerPortal.asmx::CreateConsumer. sConsumerInfoXml=[" + logXml + "]", xmlEx);
                throw;
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod(Description = "Will send an email to consumer with instructions on how to login.")]
        public string SendResetPasswordEmail(string sPartnerKey, string sConsumerPortalId, string sEmail, string sSecurityPin, string sEmployeeLoanRepLogin)
        {
            Tools.LogInfo("ConsumerPortal.asmx::SendResetPasswordEmail sConsumerPortalId=[" + sConsumerPortalId + "], sEmail=[" + sEmail + "]");
            try
            {
                AuthenticatePartnerKey(sPartnerKey);
                ConsumerPortalConfig config = GetConsumerPortalConfig(sConsumerPortalId);
                ConsumerPortalUser user = GetPortalUser(config, sEmail);

                // 4/7/17 BS - Use Loan officer from query string if there is. 
                // If not, fallback to the default LO in the cportal config for notification phone.
                string phone = config.LoanOfficerEmployeeDB.Phone;
                if (!string.IsNullOrEmpty(sEmployeeLoanRepLogin))
                {

                    var employeeDB = this.RetrieveEmployeeDBForLogin(sEmployeeLoanRepLogin, config.BrokerId);

                    if (employeeDB != null)
                    {
                        phone = employeeDB.Phone;
                    }
                }

                string message;
                if (!user.IsLockedOut && (!user.IsSecurityPinSet || sSecurityPin == user.SecurityPin))
                {
                    user.InitializePasswordReset();
                    message =  "Temporary password is: " + user.PasswordResetCode;
                }
                else if (user.IsLockedOut)
                {
                    message = "Your account has been locked due to too many incorrect login attempts. "
                        + "Please call us at " + phone + " to unlock your account.";
                }
                else
                {
                    user.LoginAttempts += 1;
                    message = "The last four digits of your SSN provided at password reset do not "
                        + "match the last four digits of your SSN provided at account creation. "
                        + "Please request the password reset once again using the correct last four "
                        + "digits of your SSN. If you have correctly entered the last four digits "
                        + "of your SSN and still cannot reset your password, please call us at " + phone + ".";
                }

                user.Save();

                // 8/21/2014 AV - 178471 Re: Password Reset Request - Use Loan officer from query string if there, if not
                // use loan officer for default application email.  If not I fallback to the default notification from address.
                string fromAddress = null;
                if (!string.IsNullOrEmpty(sEmployeeLoanRepLogin))
                {
                   
                    var employeeDB = this.RetrieveEmployeeDBForLogin(sEmployeeLoanRepLogin, config.BrokerId);

                    if (employeeDB != null)
                    {
                        fromAddress = employeeDB.Email;
                    }
                }

                if (string.IsNullOrEmpty(fromAddress) && config.LoanOfficerEmployeeDB != null)
                {
                    fromAddress = config.LoanOfficerEmployeeDB.Email;
                }

                if (string.IsNullOrEmpty(fromAddress))
                {
                    fromAddress = config.NotificationFromEmailAddress;
                }

                CBaseEmail email = new CBaseEmail(config.BrokerId);
                email.From = fromAddress;
                email.To = user.Email;
                email.Subject = "Password Reset Request";
                email.Message = message;
                email.SendImmediately();

                return "OK";

            }
            catch (NotFoundException)
            {
                // 6/6/2013 dd - Return OK even if an email address is not found.
                // The reason is we do not want to give any indication that an email address is valid or not for security reason.
                return "OK";
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
        }

        private string SafeString(Dictionary<string, string> dictionary, string key)
        {
            string value = string.Empty;
            if (dictionary.TryGetValue(key, out value) == false)
            {
                value = string.Empty;
            }
            return value;
        }

        [WebMethod(Description="Creates a lead using the specified template impersonating the specified user.")]
        public string CreateLead(string sPartnerKey, string sTicket, string sTemplateNm, string sLoginNm)
        {
            return Tools.LogAndThrowIfErrors<string>(() => CreateLeadImpl(sPartnerKey, sTicket, sTemplateNm, sLoginNm));
        }
        
        private string CreateLeadImpl(string sPartnerKey, string sTicket, string sTemplateNm, string sLoginNm)
        {
            AuthenticatePartnerKey(sPartnerKey);
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            ConsumerPortalUserPrincipal consumerPrincipal = null;
            if (principal is ConsumerPortalUserPrincipal)
            {
                consumerPrincipal = (ConsumerPortalUserPrincipal)principal;

                principal = consumerPrincipal.GetImpersonatePrincipal(sLoginNm);
            }
            Guid templateId = Guid.Empty;

            if (sTemplateNm != "")
            {

                templateId = Tools.GetLoanIdByLoanName(principal.BrokerId, sTemplateNm);
                if (templateId == Guid.Empty)
                {
                    // Throw error because user pass invalid template name.
                    throw new CBaseException(ErrorMessages.CannotFindLoanTemplate, ErrorMessages.CannotFindLoanTemplate);

                }
            }
            else if (consumerPrincipal != null)
            {
                // 6/20/2013 dd - For consumer portal principal create lead using Quick Pricer template.
                BrokerDB brokerDB = BrokerDB.RetrieveById(consumerPrincipal.BrokerId);

                // 7/1/2013 dd - eOPM 438678 - When consumer portal create a lead use the PML Loan template.
                templateId = brokerDB.PmlLoanTemplateID;
            }

            LendersOffice.Audit.E_LoanCreationSource source = LendersOffice.Audit.E_LoanCreationSource.CreateLeadUsingWebservice;

            if (Guid.Empty != templateId)
                source = LendersOffice.Audit.E_LoanCreationSource.CreateFromTemplateUsingWebservice;

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, source);

            Guid leadId = creator.BeginCreateLead(templateId);

            //OPM 190201 - Set consumer portal creation date and type
            if (leadId != Guid.Empty)
            {
                var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                    leadId,
                    typeof(ConsumerPortal));

                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                dataLoan.sConsumerPortalCreationD_rep = DateTime.Now.ToShortDateString();
                dataLoan.sConsumerPortalCreationT = E_sConsumerPortalCreationT.FullApplication;

                dataLoan.Save();
            }

            creator.CommitFileCreation(false);

            if (consumerPrincipal != null)
            {
                ConsumerPortalUser.LinkLoan(consumerPrincipal.BrokerId, consumerPrincipal.Id, creator.sLId);
            }
            return creator.LoanName;

        }

        /// <summary>
        /// Gets the EmployeeDB for the given login and broker.
        /// </summary>
        /// <param name="login">The login name of the employee.</param>
        /// <param name="brokerID">The id of the employee's broker.</param>
        /// <returns>
        /// An EmployeeDB instance for the given login and broker. If the 
        /// employee is no longer active, cannot be found, or login is null or 
        /// empty, returns null.
        /// </returns>
        private EmployeeDB RetrieveEmployeeDBForLogin(string login, Guid brokerID)
        {
            if (string.IsNullOrEmpty(login))
            {
                return null;
            }

            EmployeeDB employeeDB = null;
            Guid employeeId = Guid.Empty;

            SqlParameter[] parameters = 
            {
                new SqlParameter("@LoginName", login),
                new SqlParameter("@BrokerId", brokerID)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "GetEmployeeIdByLoginName", parameters))
            {
                if (reader.Read())
                {
                    bool isActive = (bool)reader["IsActive"];
                    if (isActive)
                    {
                        employeeId = (Guid)reader["EmployeeId"];
                    }

                }
            }

            if (employeeId != Guid.Empty)
            {
                employeeDB = new EmployeeDB(employeeId, brokerID);
                employeeDB.Retrieve();
            }

            return employeeDB;
        }

        /// <summary>
        /// Creates a lead from a short application.
        /// </summary>
        /// <param name="fileCreatorEmployeeDB">The employee to use when creating the lead.</param>
        /// <param name="consumerPortalConfig">The consumer portal from which the lead is being created.</param>
        /// <param name="applicationInfoXml">The application information xml.</param>
        /// <returns>The loan number of the newly created lead.</returns>
        /// <exception cref="CBaseException">
        /// Thrown when any of the parameters is null or when the principal for 
        /// fileCreatorEmployeeDB cannot be created.
        /// </exception>
        private string CreateLeadFromShortApplication(EmployeeDB fileCreatorEmployeeDB, ConsumerPortalConfig consumerPortalConfig, string applicationInfoXml)
        {
            if (fileCreatorEmployeeDB == null)
            {
                throw CBaseException.GenericException("The fileCreatorEmployeeDB parameter was null.");
            }
            if (consumerPortalConfig == null)
            {
                throw CBaseException.GenericException("The consumerPortalConfig parameter was null.");
            }
            else if (applicationInfoXml == null)
            {
                throw CBaseException.GenericException("The applicationInfoXml parameter was null.");
            }

            var applicationInfo = this.ConvertToDictionary(applicationInfoXml);

            var principal = PrincipalFactory.Create(
                fileCreatorEmployeeDB.BrokerID,
                fileCreatorEmployeeDB.UserID,
                "B",
                true,
                false) as AbstractUserPrincipal;

            if (principal == null)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Could not get creating user's principal because create returned null UserId=" + 
                    fileCreatorEmployeeDB.UserID);
            }

            var loanFileCreator = CLoanFileCreator.GetCreator(
                principal,
                LendersOffice.Audit.E_LoanCreationSource.CreateFromTemplateUsingWebservice);

            Guid leadID = loanFileCreator.BeginCreateLead(consumerPortalConfig.DefaultLoanTemplateId);

            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                leadID,
                typeof(ConsumerPortal));

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var dataApp = dataLoan.GetAppData(0);

            // Assign application fields.
            dataApp.aBEmail = this.SafeString(applicationInfo, "aBEmail");
            dataApp.aBFirstNm = this.SafeString(applicationInfo, "aBFirstNm");
            dataApp.aBHPhone = this.SafeString(applicationInfo, "aBHPhone");
            dataApp.aBLastNm = this.SafeString(applicationInfo, "aBLastNm");
            var aOccT = this.SafeString(applicationInfo, "aOccT");
            dataApp.aOccT = (E_aOccT)Enum.Parse(typeof(E_aOccT), aOccT);

            // Assign loan fields.
            // Can't use Enum.Parse because the app xml may pass values that 
            // aren't found in the enum for Home Equity loans.
            var sLPurposeT = this.SafeString(applicationInfo, "sLPurposeT");
            var parsedLoanPurpose = E_sLPurposeT.Purchase;
            switch (sLPurposeT)
            {
                case "0":
                    parsedLoanPurpose = E_sLPurposeT.Purchase;
                    break;
                case "1":
                    parsedLoanPurpose = E_sLPurposeT.Refin;
                    break;
                case "2":
                    parsedLoanPurpose = E_sLPurposeT.RefinCashout;
                    break;
                case "80":
                case "81":
                    parsedLoanPurpose = E_sLPurposeT.HomeEquity;
                    break;
                default:
                    throw CBaseException.GenericException("Unsupported value for sLPurposeT");
            }

            dataLoan.sLPurposeT = parsedLoanPurpose;
            dataLoan.sIsLineOfCredit = sLPurposeT == "80";

            dataLoan.sLAmtLckd = true;
            dataLoan.sLAmtCalc_rep = this.SafeString(applicationInfo, "sLAmtCalc");
            dataLoan.sSpState = this.SafeString(applicationInfo, "sSpState");
            dataLoan.sSpZip = this.SafeString(applicationInfo, "sSpZip");

            string county, city, state;
            Tools.GetCountyCityStateByZip(dataLoan.sSpZip, out county, out city, out state);
            dataLoan.sSpCounty = county;
            dataLoan.sSpCity = city;

            var losConvert = new LosConvert();
            decimal sProOFinBalPe = 0;

            if (!string.IsNullOrEmpty(this.SafeString(applicationInfo, "sProOFinBalPe")))
            {
                sProOFinBalPe = losConvert.ToMoney(this.SafeString(applicationInfo, "sProOFinBalPe"));
            }

            // The lien position needs to be set BEFORE sProOFinBalPe and sSubFinPe.
            if (dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity && sProOFinBalPe > 0)
            {
                dataLoan.sLienPosT = E_sLienPosT.Second;
                dataLoan.sIsOFinNew = false;
            }

            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
            var oldCalcMode = dataLoan.CalcModeT;
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;

            dataLoan.sHouseValPe_rep = this.SafeString(applicationInfo, "sPurchPrice");
            dataLoan.sProOFinBalPe = sProOFinBalPe;
            dataLoan.sSubFinPe_rep = this.SafeString(applicationInfo, "sSubFinPe");

            var sProdSpT = this.SafeString(applicationInfo, "sProdSpT");
            if (!string.IsNullOrEmpty(sProdSpT))
            {
                dataLoan.sProdSpT = (E_sProdSpT)Enum.Parse(typeof(E_sProdSpT), sProdSpT);
            }

            dataLoan.CalcModeT = oldCalcMode;

            var sRLckdDays = this.SafeString(applicationInfo, "sRLckdDays");
            if (!string.IsNullOrEmpty(sRLckdDays))
            {
                dataLoan.sRLckdDays_rep = sRLckdDays;
            }

            var sProdImpound = this.SafeString(applicationInfo, "sProdImpound");
            if (!string.IsNullOrEmpty(sProdImpound))
            {
                dataLoan.sProdImpound = sProdImpound.ToUpper() == "TRUE";
            }

            // OPM 189813 - Write Custom PML Fields to new lead file.
            var sCustomPMLField1 = this.SafeString(applicationInfo, "sCustomPMLField1");
            if (!string.IsNullOrEmpty(sCustomPMLField1))
            {
                dataLoan.sCustomPMLField1_rep = sCustomPMLField1;
            }

            var sCustomPMLField2 = this.SafeString(applicationInfo, "sCustomPMLField2");
            if (!string.IsNullOrEmpty(sCustomPMLField2))
            {
                dataLoan.sCustomPMLField2_rep = sCustomPMLField2;
            }

            var sCustomPMLField3 = this.SafeString(applicationInfo, "sCustomPMLField3");
            if (!string.IsNullOrEmpty(sCustomPMLField3))
            {
                dataLoan.sCustomPMLField3_rep = sCustomPMLField3;
            }

            var sCustomPMLField4 = this.SafeString(applicationInfo, "sCustomPMLField4");
            if (!string.IsNullOrEmpty(sCustomPMLField4))
            {
                dataLoan.sCustomPMLField4_rep = sCustomPMLField4;
            }

            var sCustomPMLField5 = this.SafeString(applicationInfo, "sCustomPMLField5");
            if (!string.IsNullOrEmpty(sCustomPMLField5))
            {
                dataLoan.sCustomPMLField5_rep = sCustomPMLField5;
            }

            var sCustomPMLField6 = this.SafeString(applicationInfo, "sCustomPMLField6");
            if (!string.IsNullOrEmpty(sCustomPMLField6))
            {
                dataLoan.sCustomPMLField6_rep = sCustomPMLField6;
            }

            var sCustomPMLField7 = this.SafeString(applicationInfo, "sCustomPMLField7");
            if (!string.IsNullOrEmpty(sCustomPMLField7))
            {
                dataLoan.sCustomPMLField7_rep = sCustomPMLField7;
            }

            var sCustomPMLField8 = this.SafeString(applicationInfo, "sCustomPMLField8");
            if (!string.IsNullOrEmpty(sCustomPMLField8))
            {
                dataLoan.sCustomPMLField8_rep = sCustomPMLField8;
            }

            var sCustomPMLField9 = this.SafeString(applicationInfo, "sCustomPMLField9");
            if (!string.IsNullOrEmpty(sCustomPMLField9))
            {
                dataLoan.sCustomPMLField9_rep = sCustomPMLField9;
            }

            var sCustomPMLField10 = this.SafeString(applicationInfo, "sCustomPMLField10");
            if (!string.IsNullOrEmpty(sCustomPMLField10))
            {
                dataLoan.sCustomPMLField10_rep = sCustomPMLField10;
            }

            var sCustomPMLField11 = this.SafeString(applicationInfo, "sCustomPMLField11");
            if (!string.IsNullOrEmpty(sCustomPMLField11))
            {
                dataLoan.sCustomPMLField11_rep = sCustomPMLField11;
            }

            var sCustomPMLField12 = this.SafeString(applicationInfo, "sCustomPMLField12");
            if (!string.IsNullOrEmpty(sCustomPMLField12))
            {
                dataLoan.sCustomPMLField12_rep = sCustomPMLField12;
            }

            var sCustomPMLField13 = this.SafeString(applicationInfo, "sCustomPMLField13");
            if (!string.IsNullOrEmpty(sCustomPMLField13))
            {
                dataLoan.sCustomPMLField13_rep = sCustomPMLField13;
            }

            var sCustomPMLField14 = this.SafeString(applicationInfo, "sCustomPMLField14");
            if (!string.IsNullOrEmpty(sCustomPMLField14))
            {
                dataLoan.sCustomPMLField14_rep = sCustomPMLField14;
            }

            var sCustomPMLField15 = this.SafeString(applicationInfo, "sCustomPMLField15");
            if (!string.IsNullOrEmpty(sCustomPMLField15))
            {
                dataLoan.sCustomPMLField15_rep = sCustomPMLField15;
            }

            var sCustomPMLField16 = this.SafeString(applicationInfo, "sCustomPMLField16");
            if (!string.IsNullOrEmpty(sCustomPMLField16))
            {
                dataLoan.sCustomPMLField16_rep = sCustomPMLField16;
            }

            var sCustomPMLField17 = this.SafeString(applicationInfo, "sCustomPMLField17");
            if (!string.IsNullOrEmpty(sCustomPMLField17))
            {
                dataLoan.sCustomPMLField17_rep = sCustomPMLField17;
            }

            var sCustomPMLField18 = this.SafeString(applicationInfo, "sCustomPMLField18");
            if (!string.IsNullOrEmpty(sCustomPMLField18))
            {
                dataLoan.sCustomPMLField18_rep = sCustomPMLField18;
            }

            var sCustomPMLField19 = this.SafeString(applicationInfo, "sCustomPMLField19");
            if (!string.IsNullOrEmpty(sCustomPMLField19))
            {
                dataLoan.sCustomPMLField19_rep = sCustomPMLField19;
            }

            var sCustomPMLField20 = this.SafeString(applicationInfo, "sCustomPMLField20");
            if (!string.IsNullOrEmpty(sCustomPMLField20))
            {
                dataLoan.sCustomPMLField20_rep = sCustomPMLField20;
            }

            //OPM 190201 - Set consumer portal creation date and type
            dataLoan.sConsumerPortalCreationD_rep = DateTime.Now.ToShortDateString();
            dataLoan.sConsumerPortalCreationT = E_sConsumerPortalCreationT.ShortApplication;

            dataLoan.Save();

            loanFileCreator.CommitFileCreation(false);

            return dataLoan.sLNm;
        }

        [WebMethod]
        public string SubmitShortApplication(string sPartnerKey, string sConsumerPortalId, string applicationInfoXml)
        {
            Tools.LogInfo("ConsumerPortal.asmx::SubmitShortApplication. applicationInfoXml=[" + applicationInfoXml + "]");

            try
            {
                AuthenticatePartnerKey(sPartnerKey);

                // <loan>
                //    <field id="sLPurposeT">0 - Purchase, 1 - Refinance existing 1st lien, 9 - refinance existing 1st & 2nd, 2 - Refinance Cash-out</field>
                //    <field id="aOccT">0 - Primary, 1 - Vacation home, 2 - Investment Property</field>
                //    <field id="sProdSpT">0 - Single Family, 8 - 2 units, 9 - 3 units, 10 - 4 units, 2 - Condo, 1 - Pud</field>
                //    <field id="sLAmtCalc">{Loan Amount}</field>
                //    <field id="sPurchPrice">Purchase Price</field>
                //    <field id="sSpZip">
                //    <field id="sSpState">{2 character code, i.e: CA, NV ..)</field>
                //    <field id="sProdImpound">{True | False}</field>
                //    <field id="sRLckdDays">{Lock Period}</field>
                //    <field id="sCreditScoreType1">{Credit Score}</field>
                //    <field id="aBFirstNm">{Borrower First Name}</field>
                //    <field id="aBLastNm">{Borrower Last Name}</field>
                //    <field id="aBEmail">{Email Address}</field>
                //    <field id="aBHPhone">{Phone Number}</field>
                //    <field id="selectedProgram">{Selected Program Name}</field>
                //    <field id="selectedRate">{selected Rate}</field>
                //    <field id="selectedPoint>{selected point}</field>
                //    <field id="sEmployeeLoanRepLogin">{LO name}</field>
                //</loan>
                var consumerPortalConfig = GetConsumerPortalConfig(sConsumerPortalId);
                var dictionary = ConvertToDictionary(applicationInfoXml);
                string createdLeadNumber = null;

                string sEmployeeLoanRepLogin = SafeString(dictionary, "sEmployeeLoanRepLogin");
                var employeeDB = this.RetrieveEmployeeDBForLogin(sEmployeeLoanRepLogin, consumerPortalConfig.BrokerId);

                if (employeeDB == null)
                {
                    employeeDB = consumerPortalConfig.LeadManagerEmployeeDB;
                }

                if (consumerPortalConfig.IsGenerateLeadOnShortAppSubmission)
                {
                    createdLeadNumber = this.CreateLeadFromShortApplication(employeeDB, consumerPortalConfig, applicationInfoXml);
                }

                #region Convert to nice description string.
                string purpose = string.Empty;
                string sLPurposeT = SafeString(dictionary, "sLPurposeT");
                switch (SafeString(dictionary, "sLPurposeT"))
                {
                    case "0":
                        purpose = "Purchase";
                        break;
                    case "1":
                        purpose = "Refinance without Cash Out";
                        break;
                    case "2":
                        purpose = "Refinance with Cash Out";
                        break;
                    case "80":
                        purpose = "Home Equity Line Of Credit";
                        break;
                    case "81":
                        purpose = "Home Equity Loan";
                        break;
                }

                string occ = string.Empty;
                switch (SafeString(dictionary, "aOccT"))
                {
                    case "0":
                        occ = "Primary";
                        break;
                    case "1":
                        occ = "Vacation Home";
                        break;
                    case "2":
                        occ = "Investment Property";
                        break;
                }

                string propType = string.Empty;
                switch (SafeString(dictionary, "sProdSpT"))
                {
                    case "0":
                        propType = "SFR";
                        break;
                    case "2":
                        propType = "Condo";
                        break;
                    case "1":
                        propType = "PUD";
                        break;
                    case "8":
                        propType = "2 units";
                        break;
                    case "9":
                        propType = "3 units";
                        break;
                    case "10":
                        propType = "4 units";
                        break;

                }
                #endregion
                string subject = "NEW LEAD " + SafeString(dictionary, "aBLastNm") + ", " + SafeString(dictionary, "aBFirstNm");

                string loanAmountLabel = "Loan Amount: ";

                if (sLPurposeT == "80")
                {
                    loanAmountLabel = "Desired Initial Cash Draw From Line: ";
                }
                else if (sLPurposeT == "81")
                {
                    loanAmountLabel = "Equity Loan Amount: ";
                }
                StringBuilder body = new StringBuilder();
                body.AppendLine("NEW LEAD INFORMATION");
                if (!string.IsNullOrEmpty(createdLeadNumber))
                {
                    body.AppendLine();
                    body.AppendLine("Lead Number: " + createdLeadNumber);
                }
                body.AppendLine();
                body.AppendLine("First Name: " + SafeString(dictionary, "aBFirstNm"));
                body.AppendLine("Last Name: " + SafeString(dictionary, "aBLastNm"));
                body.AppendLine("Email: " + SafeString(dictionary, "aBEmail"));
                body.AppendLine("Phone: " + SafeString(dictionary, "aBHPhone"));
                body.AppendLine();
                if (sLPurposeT == "80")
                {
                    // Home Equity Line Of Credit
                    body.AppendLine("Existing Loan Balance: " + SafeString(dictionary, "sProOFinBalPe"));
                    body.AppendLine("Desired Credit Line: " + SafeString(dictionary, "sSubFinPe"));

                }
                else if (sLPurposeT == "81")
                {
                    // Home Equity Loan
                    body.AppendLine("Existing Loan Balance: " + SafeString(dictionary, "sProOFinBalPe"));
                }
                body.AppendLine(loanAmountLabel + SafeString(dictionary, "sLAmtCalc"));
                body.AppendLine((sLPurposeT == "0" ? "Purchase Price: " : "Home Value: ") + SafeString(dictionary, "sPurchPrice"));
                body.AppendLine("Purpose: " + purpose);
                body.AppendLine("Occupancy: " + occ);
                body.AppendLine("Property Type: " + propType);
                body.AppendLine("State: " + SafeString(dictionary, "sSpState"));
                body.AppendLine("Zip: " + SafeString(dictionary, "sSpZip"));
                body.AppendLine("Impound: " + SafeString(dictionary, "sProdImpound"));
                body.AppendLine("FICO: " + SafeString(dictionary, "sCreditScoreType1"));
                body.AppendLine();
                body.AppendLine("Program Name: " + SafeString(dictionary, "selectedProgram"));
                body.AppendLine("Rate: " + SafeString(dictionary, "selectedRate"));
                body.AppendLine("Point: " + SafeString(dictionary, "selectedPoint"));

                CBaseEmail email = new CBaseEmail(consumerPortalConfig.BrokerId);
                email.From = consumerPortalConfig.NotificationFromEmailAddress;

                email.Subject = subject;
                email.Message = body.ToString();
                email.To = employeeDB.Email;

                email.Send();

                return consumerPortalConfig.ShortApplicationConfirmation;
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string SubmitShortApplicationWithoutRunningQP(string sPartnerKey, string sConsumerPortalId, string applicationInfoXml)
        {
            Tools.LogInfo("ConsumerPortal.asmx::SubmitShortApplicationWithoutRunningQP. applicationInfoXml=[" + applicationInfoXml + "]");

            try
            {
                AuthenticatePartnerKey(sPartnerKey);

                // <loan>
                //    <field id="sLPurposeT">0 - Purchase, 1 - Refinance existing 1st lien, 9 - refinance existing 1st & 2nd, 2 - Refinance Cash-out</field>
                //    <field id="aOccT">0 - Primary, 1 - Vacation home, 2 - Investment Property</field>
                //    <field id="sLAmtCalc">{Loan Amount}</field>
                //    <field id="sPurchPrice">Purchase Price</field>
                //    <field id="sSpZip">
                //    <field id="aBFirstNm">{Borrower First Name}</field>
                //    <field id="aBLastNm">{Borrower Last Name}</field>
                //    <field id="aBEmail">{Email Address}</field>
                //    <field id="aBHPhone">{Phone Number}</field>
                //    <field id="selectedProgram">{Selected Program Name}</field>
                //    <field id="selectedRate">{selected Rate}</field>
                //    <field id="sEmployeeLoanRepLogin">{LO name}</field>
                //</loan>
                var consumerPortalConfig = GetConsumerPortalConfig(sConsumerPortalId);
                var dictionary = ConvertToDictionary(applicationInfoXml);
                string createdLeadNumber = null;

                string sEmployeeLoanRepLogin = this.SafeString(dictionary, "sEmployeeLoanRepLogin");
                var employeeDB = this.RetrieveEmployeeDBForLogin(sEmployeeLoanRepLogin, consumerPortalConfig.BrokerId);

                if (employeeDB == null)
                {
                    employeeDB = consumerPortalConfig.LeadManagerEmployeeDB;
                }

                if (consumerPortalConfig.IsGenerateLeadOnShortAppSubmission)
                {
                    createdLeadNumber = this.CreateLeadFromShortApplication(employeeDB, consumerPortalConfig, applicationInfoXml);
                }

                #region Convert to nice description string.
                string purpose = string.Empty;
                string sLPurposeT = SafeString(dictionary, "sLPurposeT");
                switch (SafeString(dictionary, "sLPurposeT"))
                {
                    case "0":
                        purpose = "Purchase";
                        break;
                    case "1":
                        purpose = "Refinance without Cash Out";
                        break;
                    case "2":
                        purpose = "Refinance with Cash Out";
                        break;
                    case "80":
                        purpose = "Home Equity Line Of Credit";
                        break;
                    case "81":
                        purpose = "Home Equity Loan";
                        break;
                }

                string occ = string.Empty;
                switch (SafeString(dictionary, "aOccT"))
                {
                    case "0":
                        occ = "Primary";
                        break;
                    case "1":
                        occ = "Vacation Home";
                        break;
                    case "2":
                        occ = "Investment Property";
                        break;
                }
                #endregion
                string subject = "NEW LEAD " + SafeString(dictionary, "aBLastNm") + ", " + SafeString(dictionary, "aBFirstNm");

                string loanAmountLabel = "Loan Amount: ";

                if (sLPurposeT == "80")
                {
                    loanAmountLabel = "Desired Initial Cash Draw From Line: ";
                }
                else if (sLPurposeT == "81")
                {
                    loanAmountLabel = "Equity Loan Amount: ";
                }
                StringBuilder body = new StringBuilder();
                body.AppendLine("NEW LEAD INFORMATION");
                if (!string.IsNullOrEmpty(createdLeadNumber))
                {
                    body.AppendLine();
                    body.AppendLine("Lead Number: " + createdLeadNumber);
                }
                body.AppendLine();
                body.AppendLine("First Name: " + SafeString(dictionary, "aBFirstNm"));
                body.AppendLine("Last Name: " + SafeString(dictionary, "aBLastNm"));
                body.AppendLine("Email: " + SafeString(dictionary, "aBEmail"));
                body.AppendLine("Phone: " + SafeString(dictionary, "aBHPhone"));
                body.AppendLine();
                if (sLPurposeT == "80")
                {
                    // Home Equity Line Of Credit
                    body.AppendLine("Existing Loan Balance: " + SafeString(dictionary, "sProOFinBalPe"));
                    body.AppendLine("Desired Credit Line: " + SafeString(dictionary, "sSubFinPe"));

                }
                else if (sLPurposeT == "81")
                {
                    // Home Equity Loan
                    body.AppendLine("Existing Loan Balance: " + SafeString(dictionary, "sProOFinBalPe"));
                }
                else if ((sLPurposeT == "1") || (sLPurposeT == "2"))
                {
                    body.AppendLine("Current Interest Rate: " + SafeString(dictionary, "selectedRate"));
                }
                body.AppendLine(loanAmountLabel + SafeString(dictionary, "sLAmtCalc"));
                body.AppendLine((sLPurposeT == "0" ? "Purchase Price: " : "Home Value: ") + SafeString(dictionary, "sPurchPrice"));
                body.AppendLine("Purpose: " + purpose);
                body.AppendLine("Occupancy: " + occ);
                body.AppendLine("Zip: " + SafeString(dictionary, "sSpZip"));
                body.AppendLine();
                body.AppendLine("Rate/Terms Borrower is looking for: " + SafeString(dictionary, "selectedProgram"));


                CBaseEmail email = new CBaseEmail(consumerPortalConfig.BrokerId);
                email.From = consumerPortalConfig.NotificationFromEmailAddress;

                email.Subject = subject;
                email.Message = body.ToString();
                email.To = employeeDB.Email;

                email.Send();

                return consumerPortalConfig.ShortApplicationConfirmation;
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string UpdateConsumerInfo(string sTicket, string sConsumerInfoXml)
        {
            //Tools.LogInfo("UpdateConsumerInfo:: sConsumerInfoXml=" + sConsumerInfoXml);

            try
            {
                ConsumerPortalUserPrincipal principal = Authenticate(sTicket);
                ConsumerPortalUser user = ConsumerPortalUser.Retrieve(principal.BrokerId, principal.Id);

                // <Consumer>
                //      <field id="Password"></field>
                // </Consumer>

                // 6/6/2013 dd - Only allow to change password right now.
                Dictionary<string, string> dictionary = ConvertToDictionary(sConsumerInfoXml);

                if (dictionary.ContainsKey("Password"))
                {
                    user.SetPassword(dictionary["Password"]);
                }
                if (dictionary.ContainsKey("FirstName"))
                {
                    user.FirstName = dictionary["FirstName"];
                }
                if (dictionary.ContainsKey("LastName"))
                {
                    user.LastName = dictionary["LastName"];
                }
                if (dictionary.ContainsKey("Phone"))
                {
                    user.Phone = dictionary["Phone"];
                }
                if (dictionary.ContainsKey("IsNotifyOnNewDocRequests"))
                {
                    user.IsNotifyOnNewDocRequests = bool.Parse(dictionary["IsNotifyOnNewDocRequests"]);
                }
                if (dictionary.ContainsKey("IsNotifyOnStatusChanges"))
                {
                    user.IsNotifyOnStatusChanges = bool.Parse(dictionary["IsNotifyOnStatusChanges"]);
                }
                if (dictionary.ContainsKey("NotificationEmail"))
                {
                    user.NotificationEmail = dictionary["NotificationEmail"];
                }

                if (dictionary.ContainsKey("SignatureName"))
                {
                    user.SignatureName = dictionary["SignatureName"];
                }

                if (dictionary.ContainsKey("ReferralSource"))
                {
                    user.ReferralSource = dictionary["ReferralSource"];
                }

                if (dictionary.ContainsKey("AcceptDisclosure"))
                {
                    if (dictionary["AcceptDisclosure"].Equals(bool.TrueString, StringComparison.OrdinalIgnoreCase))
                    {
                        user.DisclosureAcceptedD = DateTime.Now;
                    }
                }

                if (dictionary.ContainsKey("SecurityPin") && !user.IsSecurityPinSet)
                {
                    user.SecurityPin = dictionary["SecurityPin"];
                }

                user.Save();

                return "OK";
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string GetConsumerInfo(string sTicket)
        {

            try
            {
                ConsumerPortalUserPrincipal principal = Authenticate(sTicket);
                ConsumerPortalUser user = ConsumerPortalUser.Retrieve(principal.BrokerId, principal.Id);
                XElement el = new XElement("Consumer",
                    new XElement("field", new XAttribute("id", "FirstName"), new XText(user.FirstName)),
                    new XElement("field", new XAttribute("id", "LastName"), new XText(user.LastName)),
                    new XElement("field", new XAttribute("id", "Phone"), new XText(user.Phone)),
                    new XElement("field", new XAttribute("id", "IsNotifyOnNewDocRequests"), new XText(user.IsNotifyOnNewDocRequests.ToString())),
                    new XElement("field", new XAttribute("id", "IsNotifyOnStatusChanges"), new XText(user.IsNotifyOnStatusChanges.ToString())),
                    new XElement("field", new XAttribute("id", "ReferralSource"), new XText(user.ReferralSource)),
                    new XElement("field", new XAttribute("id", "NotificationEmail"), new XText(user.NotificationEmail)),
                    new XElement("field", new XAttribute("id", "SignatureName"), new XText(user.SignatureName)),
                    new XElement("field", new XAttribute("id", "IsSecurityPinSet"), new XText(user.IsSecurityPinSet.ToString())));
                    
                return el.ToString();
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string GetPipeline(string sTicket)
        {

            try
            {
                XElement el = new XElement("Pipeline");

                //el.Add(new XElement("Loan",
                //    new XAttribute("sLNm", "0001"),
                //    new XAttribute("Status", "Submitted"),
                //    new XAttribute("PropertyDescription", "111 Any Street, Anaheim, CA 92805"),
                //    new XAttribute("LoanAmount", "$300,000"),
                //    new XAttribute("LoanPurpose", "Purchase"),
                //    new XAttribute("DateOpened", "06/04/2013"),
                //    new XAttribute("DateSubmitted", "06/05/2013"),
                //    new XAttribute("LastActivityDate", "06/05/2013")
                //    new XAttribute("IsAppSubmitted", "Yes")
                //    ));

                var principal = Authenticate(sTicket);


                ConsumerPortalConfig config = principal.ConsumerPortalConfig;

                foreach (var loanApp in ConsumerPortalUser.GetLinkedLoanApps(principal.BrokerId, principal.Id))
                {
                    var dateSubmitted = loanApp.sConsumerPortalSubmittedD.HasValue ? loanApp.sConsumerPortalSubmittedD.Value :
                        (loanApp.sConsumerPortalVerbalSubmissionD.HasValue ? loanApp.sConsumerPortalVerbalSubmissionD.Value : loanApp.sAppSubmittedD);
                    var dateSubmittedRep = dateSubmitted.HasValue ? dateSubmitted.Value.ToString("MM/dd/yyyy") : string.Empty;
                    var dateVerballySubmitted = loanApp.sConsumerPortalVerbalSubmissionD;
                    var dateVerballySubmittedRep = dateVerballySubmitted.HasValue ? dateVerballySubmitted.Value.ToString("MM/dd/yyyy") : string.Empty;
                    el.Add(new XElement("Loan",
                        new XAttribute("sLNm", loanApp.sLNm),
                        new XAttribute("Status", config.GetStatusDescription(loanApp.sStatusT)),
                        new XAttribute("PropertyDescription", GetPropertyDescription(loanApp)),
                        new XAttribute("LoanAmount", loanApp.sFinalLAmt.ToString("C")),
                        new XAttribute("LoanPurpose", GetLoanPurposeDescription(loanApp)),
                        new XAttribute("DateOpened", loanApp.sLeadD.HasValue ? loanApp.sLeadD.Value.ToString("MM/dd/yyyy") : string.Empty),
                        new XAttribute("DateSubmitted", dateSubmittedRep),
                        new XAttribute("DateVerballySubmitted", dateVerballySubmittedRep),
                        new XAttribute("LastActivityDate", "NEED TO REMOVE"),
                        new XAttribute("IsAppSubmitted", IsAppSubmitted(loanApp).ToString())
                        ));
                }


                return el.ToString();
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        /// <summary>
        /// Determines if the app is submitted. See opm 242509
        /// </summary>
        /// <param name="loanApp"></param>
        /// <returns>Returns true if the app is considered submitted. Otherwise, false</returns>
        private bool IsAppSubmitted(LinkedLoanApp loanApp)
        {
            if (loanApp.sInitialLoanEstimateCreatedD.HasValue)
            {
                return true;
            }
            if (loanApp.sStatusT != E_sStatusT.Lead_New)
            {
                return true;
            }
            if (loanApp.sGfeInitialDisclosureD.HasValue)
            {
                return true;
            }
            if (!loanApp.sConsumerPortalCreationD.HasValue && loanApp.sAppSubmittedD.HasValue)
            {
                return true;
            }
            if (loanApp.sConsumerPortalSubmittedD.HasValue || loanApp.sConsumerPortalVerbalSubmissionD.HasValue)
            {
                return true;
            }
            return false;
        }

        private string GetLoanPurposeDescription(LinkedLoanApp loanApp)
        {
            // 6/13/2013 dd - Per specs
            // First line is loan purpose
            // Second line is property occupancy.
            // i.e:
            //     Purchase a home
            //     Investment property.

            string desc = string.Empty;
            if (loanApp.sLienPosT == E_sLienPosT.First)
            {
                switch (loanApp.sLPurposeT)
                {
                    case E_sLPurposeT.Purchase:
                        desc = "Purchase a home";
                        break;
                    case E_sLPurposeT.Refin:
                        desc = "Refinance without Cash Out";
                        break;
                    case E_sLPurposeT.RefinCashout:
                        desc = "Refinance with Cash Out";
                        break;
                    case E_sLPurposeT.Construct:
                    case E_sLPurposeT.ConstructPerm:
                        desc = "Construction loan";
                        break;
                    case E_sLPurposeT.Other:
                        desc = "Other";
                        break;
                    case E_sLPurposeT.FhaStreamlinedRefinance:
                        desc = "FHA Streamline";
                        break;
                    case E_sLPurposeT.VaIrrrl:
                        desc = "VA IRRRL";
                        break;
                    case E_sLPurposeT.HomeEquity:
                        if (loanApp.sSubFinT == E_sSubFinT.CloseEnd)
                        {
                            desc = "Home Equity Loan";
                        }
                        else
                        {
                            desc = "Home Equity Line Of Credit";
                        }
                        break;
                    default:
                        throw new UnhandledEnumException(loanApp.sLPurposeT);
                }
            }
            else if (loanApp.sLienPosT == E_sLienPosT.Second)
            {
                switch (loanApp.sSubFinT)
                {
                    case E_sSubFinT.CloseEnd:
                        desc = "Home Equity Loan";
                        break;
                    case E_sSubFinT.Heloc:
                        desc = "Home Equity Line Of Credit";
                        break;
                    default:
                        throw new UnhandledEnumException(loanApp.sSubFinT);
                }
            }
            desc += Environment.NewLine;

            switch (loanApp.aOccT)
            {
                case E_aOccT.PrimaryResidence:
                    desc += "Primary Residence";
                    break;
                case E_aOccT.SecondaryResidence:
                    desc += "Secondary Residence";
                    break;
                case E_aOccT.Investment:
                    desc += "Investment Property";
                    break;
                default:
                    throw new UnhandledEnumException(loanApp.aOccT);
            }
            return desc;
        }

        private string GetPropertyDescription(LinkedLoanApp loanApp)
        {
            // 6/13/2013 dd - Per specs
            // First line is property type
            // Second line is city, state, zip
            // i.e:
            //      Single Family Residence
            //      San Diego, CA 92117
            // 9/10/2013 dd - eOPM 454965 - Now include property address.

            string desc = string.Empty;
            switch (loanApp.sProdSpT)
            {
                case E_sProdSpT.SFR:
                    desc = "Single Family Residence";
                    break;
                case E_sProdSpT.PUD:
                    desc = "PUD";
                    break;
                case E_sProdSpT.Condo:
                    desc = "Condominium";
                    break;
                case E_sProdSpT.CoOp:
                    desc = "Co-Op";
                    break;
                case E_sProdSpT.Manufactured:
                    desc = "Manufactured";
                    break;
                case E_sProdSpT.Townhouse:
                    desc = "Townhouse";
                    break;
                case E_sProdSpT.Commercial:
                    desc = "Commercial";
                    break;
                case E_sProdSpT.MixedUse:
                    desc = "Mixed Use";
                    break;
                case E_sProdSpT.TwoUnits:
                    desc = "2-Units";
                    break;
                case E_sProdSpT.ThreeUnits:
                    desc = "3-Units";
                    break;
                case E_sProdSpT.FourUnits:
                    desc = "4-Units";
                    break;
                case E_sProdSpT.Modular:
                    desc = "Modular";
                    break;
                case E_sProdSpT.Rowhouse:
                    desc = "Rowhouse";
                    break;
                default:
                    throw new UnhandledEnumException(loanApp.sProdSpT);
            }
            desc += Environment.NewLine;
            if (string.IsNullOrEmpty(loanApp.sSpAddr) == false)
            {
                desc += loanApp.sSpAddr + ", ";
            }
            if (string.IsNullOrEmpty(loanApp.sSpCity) == false) 
            {
                desc += loanApp.sSpCity + ", ";
            }
            desc += loanApp.sSpState + " " + loanApp.sSpZip;

            return desc;
        }

        [WebMethod]
        public string RunQuickPricer(string sPartnerKey, string sConsumerPortalId, string sXmlData)
        {
            Tools.LogInfo("ConsumerPortal.asmx::RunQuickPricer sConsumerPortalId=[" + sConsumerPortalId + "], sXmlData=[" + sXmlData + "]");

            try
            {
                AuthenticatePartnerKey(sPartnerKey);
                ConsumerPortalConfig config = GetConsumerPortalConfig(sConsumerPortalId);

                XmlDocument xdoc = Tools.CreateXmlDoc(sXmlData);
                bool bUseBestPricing = true;
                bUseBestPricing = Convert.ToBoolean(xdoc.DocumentElement.GetAttribute("bUseBestPricing"));// 11/13/2014 bs - To determine whether QuickPricer will use best pricing or not
                QuickPricer quickPricerService = new QuickPricer();

                AbstractUserPrincipal principal = null;

                if (!string.IsNullOrEmpty(xdoc.DocumentElement.GetAttribute("sEmployeeLoanRepLogin")))
                {
                    // 9/21/2013 dd - eOPM 440712 - If lo is pass in user query then run quick pricer as the user.
                    Guid userId = Guid.Empty;
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@LoginNm", xdoc.DocumentElement.GetAttribute("sEmployeeLoanRepLogin")),
                                                    new SqlParameter("@BrokerId", config.BrokerId)
                                                };

                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(config.BrokerId, "GetActiveUserIdByLoginName", parameters))
                    {
                        if (reader.Read())
                        {
                            userId = (Guid)reader["UserId"];
                        }

                    }

                    if (userId != Guid.Empty)
                    {
                        principal = (AbstractUserPrincipal)PrincipalFactory.Create(config.BrokerId, userId, "B", true/* allowDuplicateLogin */, true /* isStoreToCookie */);
                    }
                }

                if (principal == null)
                {
                    // 9/21/2013 dd - Run pricing as Lead Manager of consumer portal.
                    principal = (AbstractUserPrincipal)PrincipalFactory.Create(config.BrokerId, config.LeadManagerUserId, "B", true/* allowDuplicateLogin */, true /* isStoreToCookie */);
                }

                principal.IsOriginalPrincipalAConsumer = true; // 9/11/2013 dd - OPM 137164 - So the RunPmlRequestSourceType is set correctly.
                xdoc.DocumentElement.SetAttribute("UseLoanValueForsProd3rdPartyUwResultT", bool.TrueString);
                return quickPricerService.RunQuickPricerV1(principal, xdoc, bUseBestPricing, false, true);
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string GetDocuments(string sTicket, string sLNm)
        {
            Tools.LogInfo("ConsumerPortal.asmx:: GetDocuments sLNm=[" + sLNm + "]");

            try
            {
                ConsumerPortalUserPrincipal principal = Authenticate(sTicket);


                Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);

                if (sLId != principal.AllowViewSubmittedLoanId)
                {
                    throw CBaseException.GenericException("Access to loan denied.");
                }

                if (!ConsumerPortalUser.IsLinkedToLoan(principal.BrokerId, principal.Id, sLId))
                {
                    throw CBaseException.GenericException("Access to loan denied.");
                }
                ConsumerPortalUser user = ConsumerPortalUser.Retrieve(principal.BrokerId, principal.Id);
                bool requestDisclosure; 
                IEnumerable<DocumentItem> items = user.GetAccessibleDocuments(sLId, out requestDisclosure);


                XElement doclistXml = new XElement(new XElement("DocumentList"));
             
                    doclistXml.Add(new XAttribute("NeedDisclosureConsent", requestDisclosure.ToString()));
                    foreach (DocumentItem doc in items)
                    {
                        doclistXml.Add(new XElement("Document",
                             new XAttribute("CanDownload", doc.CanDownload),
                             new XAttribute("CanFaxIn", doc.CanFaxIn),
                             new XAttribute("CanSignOnline", doc.CanSignOnline),
                             new XAttribute("CanUpload", doc.CanUpload),
                             new XAttribute("Description", doc.Description),
                             new XAttribute("DocumentType", doc.DocumentType),
                             new XAttribute("RequiredSignatureMessage", doc.RequiredSignatureMessage),
                             new XAttribute("Status", doc.Status),
                             new XAttribute("IsNew", doc.IsNew.ToString()),
                             new XAttribute("Id", doc.DocumentId)));
                    }

                return doclistXml.ToString();
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string GetSignLink(string sTicket, string sDocumentId)
        {
            Tools.LogInfo("ConsumerPortal.asmx:: GetSignLink sDocumentId=[" + sDocumentId + "]");

            try
            {
                ConsumerPortalUserPrincipal p = Authenticate(sTicket);

                long requestId;

                if (!long.TryParse(sDocumentId, out requestId))
                {
                    throw CBaseException.GenericException("Invalid document id");
                }
                string key = EDocPortalTicket.CreateSignatureTicket(p, requestId);
                return ConstSite.EDocSignerAuthUrl + "?k=" + key;
            }
            catch (CBaseException exception)
            {
                return GenerateErrorMessage(exception);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string GetFaxDownloadLink(string sTicket, string sDocumentId)
        {
            Tools.LogInfo("ConsumerPortal.asmx:: GetFaxDownloadLink=[" + sDocumentId + "]");

            try
            {
                ConsumerPortalUserPrincipal p = Authenticate(sTicket);
                long requestId;
                if (!long.TryParse(sDocumentId, out requestId))
                {
                    throw CBaseException.GenericException("Invalid document id");
                }

                string key = EDocPortalTicket.CreateFaxPackageTicket(p, requestId);
                return MakeUrl("ConsumerPDF.aspx?k=" + key);
            }
            catch (CBaseException e)
            {
                return GenerateErrorMessage(e);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string GetDocumentDownloadLink(string sTicket, string sDocumentId)
        {
            Tools.LogInfo("ConsumerPortal.asmx:: GetDocumentDownloadLink=[" + sDocumentId + "]");

            try
            {
                ConsumerPortalUserPrincipal p = Authenticate(sTicket);
                string key;
                long requestId;

                if (sDocumentId.StartsWith("s") && long.TryParse(sDocumentId.Substring(1), out requestId))
                {
                    key = EDocPortalTicket.CreateDownlodShareRequestTicket(p, requestId);
                }
                else
                {
                    Guid documentId;
                    if (long.TryParse(sDocumentId, out requestId))
                    {
                        key = EDocPortalTicket.CreateDownlodRequestTicket(p, requestId);
                    }
                    else
                    {
                        try
                        {
                            documentId = new Guid(sDocumentId);
                            key = EDocPortalTicket.CreateDownloadEDocTicket(p, documentId);
                        }
                        catch (FormatException)
                        {
                            throw CBaseException.GenericException("Invalid document id");
                        }
                    }
                }
                return MakeUrl("ConsumerPDF.aspx?k=" + key);
            }

            catch (CBaseException e)
            {
                return GenerateErrorMessage(e);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string SubmitNewDocument(string sTicket, string sLNm, string sDescription, string sBase64Pdf)
        {
            return Tools.LogAndThrowIfErrors<string>(() => SubmitNewDocumentImpl(sTicket, sLNm, sDescription, sBase64Pdf));
        }

        private string SubmitNewDocumentImpl(string sTicket, string sLNm, string sDescription, string sBase64Pdf)
        {
            Tools.LogInfo("SubmitNewDocument:: sDescription=[" + sDescription + "] sLNm=[" + sLNm + "]");

            ConsumerPortalUserPrincipal p = Authenticate(sTicket);
            Guid sLId = Tools.GetLoanIdByLoanName(p.BrokerId, sLNm);
            
            if (!ConsumerPortalUser.IsLinkedToLoan(p.BrokerId, p.Id, sLId))
            {
                throw CBaseException.GenericException("Access to loan denied.");
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(ConsumerPortal));
            dataLoan.InitLoad();

            Guid appId = dataLoan.GetAppData(0).aAppId; 

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData app = dataLoan.GetAppData(i);
                if (app.aBEmail.TrimWhitespaceAndBOM().Equals(p.Email.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase) ||
                    app.aCEmail.TrimWhitespaceAndBOM().Equals(p.Email.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase))
                {
                    appId = app.aAppId;
                    break;
                }
            }

            try
            {
                var unclassifiedId = EDocumentDocType.GetOrCreateBorrowerUpload(p.BrokerId);

                var repo = EDocumentRepository.GetSystemRepository(p.BrokerId);
                var doc = repo.CreateDocument(E_EDocumentSource.AcceptedFromConsumer);
                doc.LoanId = sLId;

                doc.AppId = appId;
                doc.IsUploadedByPmlUser = false;
                
                doc.DocumentTypeId = unclassifiedId;
                doc.PublicDescription = sDescription;

                string filePath = Utilities.Base64StringToFile(sBase64Pdf, ".pdf");

                doc.UpdatePDFContentOnSave(filePath);
                doc.EDocOrigin = E_EDocOrigin.ConsumerPortal;
                repo.Save(doc);

                return "OK";

            }
            catch (CBaseException e)
            {
                return GenerateErrorMessage(e);
            }
        }

        [WebMethod] 
        public string SubmitDocument(string sTicket, string sDocumentId, string sBase64Pdf)
        {
            Tools.LogInfo("SubmitDocument:: sDocumentId=[" + sDocumentId + "]");

            try
            {
                long requestId;

                if (!long.TryParse(sDocumentId, out requestId))
                {
                    throw CBaseException.GenericException("Invalid request id");
                }
                byte[] pdfBytes = System.Convert.FromBase64String(sBase64Pdf);
                ConsumerPortalUserPrincipal p = Authenticate(sTicket);
                ConsumerActionItem item = new ConsumerActionItem(requestId);
                try
                {
                    item.AcceptConsumerUpload(pdfBytes);
                    item.Save();
                }
                catch (InvalidPdfException e)
                {
                    return GenerateErrorMessage(new CBaseException("Invalid PDF", e));
                }
                return "OK";
            }
            catch (CBaseException e)
            {
                return GenerateErrorMessage(e);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        [WebMethod]
        public string SubmitLoan(string sTicket, string sLNm)
        {
            Tools.LogInfo("SubmitLoan:: sLNm=[" + sLNm + "]");

            try
            {
                ConsumerPortalUserPrincipal principal = Authenticate(sTicket);

                ConsumerPortalConfig config = principal.ConsumerPortalConfig;

                Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
                if (sLId == Guid.Empty)
                {
                    throw new IntegrationInvalidLoanNumberException(sLNm);
                }

                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(ConsumerPortal));

                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                if (principal.BrokerId == new Guid("E9F006C9-CB83-4531-8925-08FCCF4ADD63")) // First Tech Fed (PML0223)
                {
                    // 7/8/2013 dd - OPM 124658 - When consumer submit the loan for FirstTech,
                    // then we convert loan to status open and pick a next reserve loan number.
                    dataLoan.SetsLNmWithPermissionBypass(HardcodeIdentitySequence.GetNextAvailableIdentity(principal.BrokerId, "OSI"));
                    sLNm = dataLoan.sLNm; // 10/14/2013 dd - Need to update loan number to run the seamless.
                }
                // Change Status to Open
                // Set Opened Date to Today
                // if allow to run DU then run DU


                // 6/21/2013 dd - eOPM 438124 - Set Interviewer method to internet and 
                // interviewer date to date consumer submit loan.
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    dataLoan.GetAppData(i).aIntrvwrMethodT = E_aIntrvwrMethodT.Internet;
                    if (principal.BrokerDB.IsEnableSettingInterviewDateForConsumerPortalSubmission)
                    {
                        dataLoan.GetAppData(i).a1003InterviewD_rep = DateTime.Today.ToString("MM/dd/yyyy");
                        dataLoan.GetAppData(i).a1003InterviewDLckd = true;
                    }
                }

                dataLoan.sConsumerPortalSubmittedD_rep = DateTime.Now.ToShortDateString();
                dataLoan.Save();

                // OPM 169699 - Send notification email to LO or lead manager
                string toEmail = "";
                if (!string.IsNullOrEmpty(dataLoan.sEmployeeLoanRepEmail))
                    toEmail = dataLoan.sEmployeeLoanRepEmail;
                else
                    toEmail = config.LeadManagerEmployeeDB.Email;

                bool hasValidApproveEligible = false;
                if (config.IsAutomaticRunDU && dataLoan.sLpTemplateId != Guid.Empty
                    && dataLoan.sLPurposeT != E_sLPurposeT.HomeEquity)
                {
                    // 6/20/2013 dd - Only run this loan, if the setting is turn on and
                    // loan has a valid program template id. We do not want to run
                    // DU if there are no eligible program.

                    // 9/30/2013 dd - Only run if it is not home equity.


                    //if (dataLoan.GetAppData(0).aIsFannieTestBorrower)
                    //{
                    try
                    {
                        Loan loan = new Loan();

                        loan.SubmitToDesktopUnderwriterSeamless(sTicket, sLNm,
                            config.AutomaticDULogin, config.AutomaticDUPassword, "",
                            config.AutomaticDUCraId, config.AutomaticDUCraLogin, config.AutomaticDUCraPassword, true);

                        FnmaXisDuUnderwriteResponse response = new FnmaXisDuUnderwriteResponse(sLId);
                        if (response.IsValid && response.HasError == false)
                        {
                            hasValidApproveEligible = response.UnderwritingRecommendationDescription.TrimWhitespaceAndBOM().Equals("Approve/Eligible", StringComparison.OrdinalIgnoreCase);
                        }
                    }
                    catch (CBaseException)
                    {
                        // Don't display any error message to user when du fail.
                    }
                    //}
                }

                // Send Pre-Approval or Pre-Qualification letter
                if (config.F1003SubmissionReceiptT == E_F1003SubmissionReceiptT.PreApprovalLetter)
                {
                    // Depend on AUS status send out pre-approval letter.
                    if (hasValidApproveEligible)
                    {
                        if (config.PreQualificationLetterdId != Guid.Empty)
                        {
                            byte[] pdf = GetCustomPdfForm(sLId, principal.BrokerId, config.PreApprovalLetterId);
                            SendAttachmentEmail(config.BrokerId, config.NotificationFromEmailAddress,
                                principal.Email, "Pre-Approved", "Pre-Approved", "Pre-Approved.pdf",
                                pdf);
                        }
                    }

                }
                else if (config.F1003SubmissionReceiptT == E_F1003SubmissionReceiptT.PreQualificationLetter)
                {
                    // Send out pre-qualification letter.
                    if (config.PreQualificationLetterdId != Guid.Empty)
                    {
                        byte[] pdf = GetCustomPdfForm(sLId, principal.BrokerId, config.PreQualificationLetterdId);
                        SendAttachmentEmail(config.BrokerId, config.NotificationFromEmailAddress,
                            principal.Email, "Pre-Qualification", "Pre-Qualification", "Pre-Qualification.pdf",
                            pdf);
                    }

                }

                // 9/30/2013 dd - Switch loan status to Open last. The reason is once the loan in "Loan Open"
                // Consumer Portal will not have write acecss. The loan is currently a lead.
                if (config.SubmissionsShouldBecomeLoans == true)
                {
                    CLoanFileNamer namer = new CLoanFileNamer();
                    string mers = null;
                    string name = null;
                    string refNm = null;

                    dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(ConsumerPortal));
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    //dont do this for first tech as the code above already renames their loans for OSI 
                    if (config.BrokerId != new Guid("E9F006C9-CB83-4531-8925-08FCCF4ADD63")) // First Tech Fed (PML0223)
                    {

                        for (int x = 0; x < 5; x++)
                        {
                            name = namer.GetLoanAutoName(config.BrokerId, dataLoan.sBranchId, false, false, true/* enableMersGeneration*/, out mers, out refNm);
                            if (!IsLoanNumberInUse(config.BrokerId, name))
                            {
                                break; //name found
                            }
                            name = null;
                        }

                        if (string.IsNullOrEmpty(name))
                        {
                            Tools.LogError("Could not find a name after 50 tries for " + dataLoan.sLNm + " " + dataLoan.sLId);
                        }
                        else
                        {
                            dataLoan.SetsLNmWithPermissionBypass(name);
                            sLNm = name;

                            // OPM 197010 - Generate MERS MIN for loans submitted via consumer portal
                            if (dataLoan.BrokerDB.IsAutoGenerateMersMin && !string.IsNullOrEmpty(mers))
                            {
                                dataLoan.DisableMersMinCheck = true;
                                dataLoan.sMersMin = mers;
                                dataLoan.DisableMersMinCheck = false;
                            }
                        }
                    }


                    dataLoan.sStatusT = E_sStatusT.Loan_Open;
                    dataLoan.sOpenedD_rep = DateTime.Today.ToString("MM/dd/yyyy");
                    dataLoan.Save();
                }

                XElement root = new XElement("result");
                root.SetAttributeValue("sLNm", sLNm);
                root.SetAttributeValue("FullApplicationConfirmation", config.FullApplicationConfirmation);

                CBaseEmail email = new CBaseEmail(config.BrokerId);
                email.From = config.NotificationFromEmailAddress;
                email.To = toEmail;
                email.Subject = String.Format("{0} - {1} {2} has submitted their application online", sLNm, dataLoan.GetAppData(0).aBFirstNm, dataLoan.GetAppData(0).aBLastNm);
                email.Message = "Please review their application and follow up with them regarding next steps as soon as possible.";
                email.Send();

                Tools.LogInfo("SubmitLoan notification email sent to " + email.To);


                return root.ToString();
                //return config.FullApplicationConfirmation;
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        private byte[] GetCustomPdfForm(Guid sLId, Guid sBrokerId, Guid formId)
        {
            NameValueCollection arguments = new NameValueCollection();
            arguments["loanid"] = sLId.ToString();
            arguments["brokerid"] = sBrokerId.ToString();
            arguments["custompdfid"] = formId.ToString();

            var repository = new PdfGeneratorRepository();
            var pdf = repository.CreatePdfInstance("Custom");

            if (null != pdf)
            {
                ((LendersOffice.Pdf.IPDFPrintItem)pdf).Arguments = arguments;
                MemoryStream stream = new MemoryStream();
                pdf.GeneratePDF(stream, "", "");
                byte[] buffer = stream.ToArray(); // pdf.GeneratePDF("", "");
                return buffer;

            }
            throw new CBaseException("No Custom PDF Class", "No Custom PDF Class");
        }

        private bool IsLoanNumberInUse(Guid brokerId, string sLNm)
        {

            SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerId", brokerId),
                                                    new SqlParameter("@LoanNumber", sLNm)
                                                };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "IsLoanNumberExisting", parameters))
            {
                return reader.Read();
            }
        }

        private void SendAttachmentEmail(Guid brokerId, string from, string to, string subject, string message, string fileName,
            byte[] data)
        {

            CBaseEmail email = new CBaseEmail(brokerId);
            email.From = from;
            email.To = to;
            email.AddAttachment(fileName, data);
            email.Subject = subject;
            email.Message = message;
            email.Send();
        }

        [WebMethod]
        public string SelectLoan(string sTicket, string sLNm, string lLpTemplateId, decimal requestedRate, decimal requestedFee)
        {
            Tools.LogInfo("SelectLoan:: sLNm=[" + sLNm + "] lLpTemplateId=[" + lLpTemplateId + ", requestedRate=[" + requestedRate + "], requestedFee=[" + requestedFee + "]");

            try
            {
                ConsumerPortalUserPrincipal principal = Authenticate(sTicket);

                ConsumerPortalConfig config = principal.ConsumerPortalConfig;

                Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
                if (sLId == Guid.Empty)
                {
                    throw new IntegrationInvalidLoanNumberException(sLNm);
                }

                // 6/19/2013 dd - After Register a loan
                DistributeUnderwritingEngine.RegisterLoanProgram(principal.GetImpersonatePrincipal(), sLId,
                    new Guid(lLpTemplateId), requestedRate, requestedFee, "0.000,0.000", false /* isRequestingRateLock */);

                // Change loan status back to lead because consumer may still working on the loan.
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(ConsumerPortal));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sStatusT = E_sStatusT.Lead_New;
                dataLoan.sStatusLckd = true;
                dataLoan.Save();
                return "OK";
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                
                throw;
            }
        }

        [WebMethod]
        public string GetLoanStatuses(string sTicket, string sLNm)
        {
            Tools.LogInfo("GetLoanStatuses:: sLNm=" + sLNm);

            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            try
            {
                ConsumerPortalUserPrincipal principal = Authenticate(sTicket);

                ConsumerPortalConfig config = principal.ConsumerPortalConfig;

                Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
                if (sLId == Guid.Empty)
                {
                    throw new IntegrationInvalidLoanNumberException(sLNm);
                }

                var statusList = config.StatusList;
                var dependencyList = statusList.Select(c => c.FieldId);

                // Change loan status back to lead because consumer may still working on the loan.
                CPageData dataLoan = new NotEnforceAccessControlPageData(sLId, dependencyList);
                dataLoan.InitLoad();

                foreach (var dateConfig in statusList)
                {
                    var fieldValue = PageDataUtilities.GetValue(
                        dataLoan,
                        dataLoan.GetAppData(0),
                        dateConfig.FieldId);

                    var kvp = new KeyValuePair<string, string>(
                        dateConfig.Description,
                        fieldValue);

                    list.Add(kvp);
                }


                XElement el = new XElement("statuses");
                foreach (var o in list)
                {
                    el.Add(new XElement("status", new XAttribute("name", o.Key), new XAttribute("value", o.Value)));
                }
                return el.ToString();
            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        /// <summary>
        /// Lists the loan officers available for a loan. 
        /// 
        /// Rodrigo, need to retrieve loan officers in all branches assign to consumer portal.
        /// </summary>
        /// <param name="sPartnerKey"></param>
        /// <param name="sLNm">The loan to check for</param>
        /// <returns></returns>
        [WebMethod]
        public string GetAvailableLoanOfficers(string sTicket, string sLNm)
        {
            Tools.LogInfo("GetAvailableLoanOfficers:: sLNm=[" + sLNm + "]");

            try
            {
                ConsumerPortalUserPrincipal principal = Authenticate(sTicket);

                ConsumerPortalConfig config = principal.ConsumerPortalConfig;

                XDocument xdoc = new XDocument(new XElement("Agents"));
                XElement root = xdoc.Root;
                string baseImageUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + "/UserProfileImage.aspx";

                if (config.IsHideLoanOfficer)
                {

                    // Return only loan officer assign to loan file or in settings.
                    Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
                    if (sLId == Guid.Empty)
                    {
                        throw new IntegrationInvalidLoanNumberException(sLNm);
                    }

                    CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(ConsumerPortal));
                    dataLoan.InitLoad();

                    if (dataLoan.sEmployeeLoanRepId != Guid.Empty)
                    {
                        root.Add(MakeAgentXmlByEmployeeId(baseImageUrl, principal.BrokerId, dataLoan.sEmployeeLoanRepId, E_AgentRoleT.LoanOfficer));
                    }
                    else
                    {
                        root.Add(MakeAgentXml(baseImageUrl, config.LoanOfficerEmployeeDB, E_AgentRoleT.LoanOfficer));
                    }
                }
                else
                {
                    // Return all active loan officers.

                    foreach (var employee in GetOfficers(principal.BrokerId))
                    {
                        if (employee != null)
                        {
                            root.Add(MakeAgentXml(baseImageUrl, employee));
                        }
                    }
                }

                return xdoc.ToString();

            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        private static IEnumerable<BrokerLoanAssignmentTable.Spec> GetOfficers(Guid brokerId)
        {
            var ret = new BrokerLoanAssignmentTable();

            var allBranches = Guid.Empty;
            ret.Retrieve(brokerId, allBranches, null, E_RoleT.LoanOfficer, LendersOffice.Admin.E_EmployeeStatusFilterT.ActiveOnly);
            return ret[E_RoleT.LoanOfficer];
        }

        private static XElement MakeAgentXmlByUserId(string baseImageUrl, Guid brokerId, Guid userId, E_AgentRoleT role)
        {
            EmployeeDB employeeDB = EmployeeDB.RetrieveByUserId(brokerId, userId);

            return MakeAgentXml(baseImageUrl, employeeDB, role);
        }

        private static XElement MakeAgentXmlByEmployeeId(string baseImageUrl, Guid brokerId, Guid employeeId, E_AgentRoleT role)
        {
            EmployeeDB employeeDB = EmployeeDB.RetrieveById(brokerId, employeeId);

            return MakeAgentXml(baseImageUrl, employeeDB, role);
        }

        private static XElement MakeAgentXml(string baseImageUrl, EmployeeDB employeeDB, E_AgentRoleT role)
        {

            return new XElement("Agent",
                new XAttribute("Role", role.ToString()),
                new XAttribute("FirstName", employeeDB.FirstName),
                new XAttribute("MiddleName", employeeDB.MiddleName),
                new XAttribute("LastName", employeeDB.LastName),
                new XAttribute("Suffix", employeeDB.Suffix),
                new XAttribute("Email", employeeDB.Email),
                new XAttribute("Addr", employeeDB.Address.StreetAddress),
                new XAttribute("City", employeeDB.Address.City),
                new XAttribute("State", employeeDB.Address.State),
                new XAttribute("Zip", employeeDB.Address.Zipcode),
                new XAttribute("Phone", employeeDB.Phone),
                new XAttribute("Fax", employeeDB.Fax),
                new XAttribute("LoginName", employeeDB.LoginName),
                new XAttribute("UserType", employeeDB.UserType),
                new XAttribute("LosIdentifier", employeeDB.LosIdentifier), // NMLS ID
                new XAttribute("ProfileUrl", baseImageUrl + "?imageid=" + employeeDB.ID.ToString())
                );
        }

        private static XElement MakeAgentXml(string baseImageUrl, LendersOffice.Admin.BrokerLoanAssignmentTable.Spec employee)
        {
            if (employee == null)
                return null;

            return new XElement("Agent",
                new XAttribute("FirstName", employee.FirstName),
                new XAttribute("MiddleName", employee.MiddleName),
                new XAttribute("LastName", employee.LastName),
                new XAttribute("Suffix", employee.Suffix),
                new XAttribute("Email", employee.Email),
                new XAttribute("Addr", employee.Addr),
                new XAttribute("City", employee.City),
                new XAttribute("State", employee.State),
                new XAttribute("Zip", employee.Zip),
                new XAttribute("Phone", employee.Phone),
                new XAttribute("Fax", employee.Fax),
                new XAttribute("LoginName", employee.LoginName),
                new XAttribute("ProfileUrl", baseImageUrl + "?imageid=" + employee.EmployeeId)
            );
        }

        [WebMethod]
        public string GetAssignedOfficer(string sTicket, string sLNm)
        {
            Tools.LogInfo("GetAssignedOfficer:: sLNm=[" + sLNm + "]");

            try
            {
                ConsumerPortalUserPrincipal principal = Authenticate(sTicket);

                ConsumerPortalConfig config = principal.ConsumerPortalConfig;

                XDocument xdoc = new XDocument(new XElement("Agents"));
                XElement root = xdoc.Root;
                string baseImageUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + "/UserProfileImage.aspx";

                Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
                if (sLId == Guid.Empty)
                {
                    throw new IntegrationInvalidLoanNumberException(sLNm);
                }

                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(ConsumerPortal));
                dataLoan.InitLoad();

                if (dataLoan.sEmployeeLoanRepId != Guid.Empty)
                {
                    root.Add(MakeAgentXmlByEmployeeId(baseImageUrl, principal.BrokerId, dataLoan.sEmployeeLoanRepId, E_AgentRoleT.LoanOfficer));
                }

                if (dataLoan.sEmployeeProcessorId != Guid.Empty)
                {
                    root.Add(MakeAgentXmlByEmployeeId(baseImageUrl, principal.BrokerId, dataLoan.sEmployeeProcessorId, E_AgentRoleT.Processor));
                }


                return xdoc.ToString();

            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        [WebMethod]
        public string OrderCredit(string sTicket, string sLNm)
        {
            Tools.LogInfo("OrderCredit:: sLNm=[" + sLNm + "]");

            try
            {
                ConsumerPortalUserPrincipal principal = Authenticate(sTicket);

                ConsumerPortalConfig config = principal.ConsumerPortalConfig;

                if (config.IsAutomaticPullCredit == false)
                {
                    return "OK";
                }

                Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
                if (sLId == Guid.Empty)
                {
                    throw new IntegrationInvalidLoanNumberException(sLNm);
                }

                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(ConsumerPortal));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                //bool isUseFakeLiability = true;

                //if (dataLoan.GetAppData(0).aIsFannieTestBorrower)
                //{
                //    isUseFakeLiability = false;
                //}

                //if (isUseFakeLiability == false)
                //{
                    // 6/18/2013 dd - I am going to reuse the credit code from our Load API.

                    XDocument xdoc = new XDocument();
                    XElement root = new XElement("LOXmlFormat", new XAttribute("version", "1.0"));
                    xdoc.Add(root);

                    XElement loanElement = new XElement("loan");
                    root.Add(loanElement);

                    for (int i = 0; i < dataLoan.nApps; i++)
                    {
                        CAppData dataApp = dataLoan.GetAppData(i);
                        if (dataApp.aIsCreditReportOnFile == true)
                        {
                            // 9/16/2013 dd - If credit report is already on file then do not order through this web service.
                            // If actual reissue is need then loan officer must perform in LendingQB
                            continue;
                        }
                        XElement applicantElement = new XElement("applicant");
                        loanElement.Add(applicantElement);
                        applicantElement.Add(new XAttribute("id", dataApp.aBSsn.Replace("-", "")));

                        XElement creditElement = new XElement("credit");
                        applicantElement.Add(creditElement);

                        creditElement.Add(new XAttribute("craid", config.AutomaticCRAId.ToString()));
                        creditElement.Add(new XAttribute("requestType", "NEW"));
                        creditElement.Add(new XAttribute("experian", "Y"));
                        if (dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity && dataLoan.BrokerDB.CustomerCode.Equals("PML0223"))
                        {
                            // 10/15/2013 dd - OPM 140999 - For HomeEquity, FirstTech does not want to pull all three bureau.
                            creditElement.Add(new XAttribute("equifax", "N"));
                            creditElement.Add(new XAttribute("transunion", "N"));
                        }
                        else
                        {
                            creditElement.Add(new XAttribute("equifax", "Y"));
                            creditElement.Add(new XAttribute("transunion", "Y"));

                        }
                        
                        creditElement.Add(new XAttribute("loginName", config.AutomaticCRALogin));
                        creditElement.Add(new XAttribute("password", config.AutomaticCRAPassword));
                        creditElement.Add(new XAttribute("accountId", config.AutomaticCRAAccountId));
                        creditElement.Add(new XAttribute("fannieMaeMORNETUserID", config.AutomaticCRADULogin));
                        creditElement.Add(new XAttribute("fannieMaeMORNETPassword", config.AutomaticCRADUPassword));
                        creditElement.Add(new XAttribute("isImportLiabilitiesFromCreditReport", "Y"));
                    }

                    Loan loanService = new Loan();
                    loanService.Save(sTicket, sLNm, xdoc.ToString(), 0);
                //}


                //if (isUseFakeLiability)
                //{
                //    // TODO: Enter Fake Liability now.
                //    for (int i = 0; i < dataLoan.nApps; i++)
                //    {
                //        CAppData da = dataLoan.GetAppData(i);

                //        da.aBExperianScore_rep = "800";
                //        da.aBEquifaxScore_rep = "800";
                //        da.aBTransUnionScore_rep = "800";

                //        if (da.aCIsValidNameSsn)
                //        {
                //            da.aCExperianScore_rep = "800";
                //            da.aCEquifaxScore_rep = "800";
                //            da.aCTransUnionScore_rep = "800";

                //        }
                //        CLiaCollection liaCollection = da.aLiaCollection;

                //        if (liaCollection.CountRegular == 0)
                //        {
                //            var record = liaCollection.AddRegularRecord();
                //            record.DebtT = E_DebtRegularT.Installment;
                //            record.OwnerT = E_LiaOwnerT.Borrower;
                //            record.ComNm = "USAA Federal Savings Bank";
                //            record.Bal_rep = "$28,626";
                //            record.Pmt_rep = "$533";
                //            record.Update();

                //            record = liaCollection.AddRegularRecord();
                //            record.DebtT = E_DebtRegularT.Revolving;
                //            record.OwnerT = E_LiaOwnerT.Borrower;
                //            record.ComNm = "Chase";
                //            record.Bal_rep = "$228";
                //            record.Pmt_rep = "$10";
                //            record.Update();

                //            record = liaCollection.AddRegularRecord();
                //            record.DebtT = E_DebtRegularT.Mortgage;
                //            record.OwnerT = E_LiaOwnerT.Borrower;
                //            record.ComNm = "Wells Fargo";
                //            record.Bal_rep = "$89,579.45";
                //            record.Pmt_rep = "$623.74";
                //            record.Update();
                //            liaCollection.Flush();
                //        }

                //    }
                //    dataLoan.Save();
                //}
                return "OK";

            }
            catch (CBaseException exc)
            {
                return GenerateErrorMessage(exc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);

                throw;
            }
        }

        private XElement CreateLoXmlField(string id, string value)
        {
            XElement field = new XElement("field",
                new XAttribute("id", id), value);

            return field;
        }

        private ConsumerPortalUserPrincipal Authenticate(string sTicketXml)
        {
            XElement el = XElement.Parse(sTicketXml);

            ConsumerPortalUserPrincipal principal = ConsumerPortalUserTicket.Decrypt(el.Attribute("EncryptedTicket").Value).Principal;
            HttpContext.Current.User = principal;
            Thread.CurrentPrincipal = principal;
            return principal;
        }

        private string MakeUrl(string pageAndQuery)
        {
            return String.Format("{0}://{1}/{2}/{3}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Host, Tools.VRoot, pageAndQuery);
        }

        [WebMethod]
        public void CreateLogoutLog(string sTicket)
        {
            ConsumerPortalUserPrincipal principal = Authenticate(sTicket);
            SecurityEventLogHelper.CreateLogoutLog(principal);
        }
    }
}