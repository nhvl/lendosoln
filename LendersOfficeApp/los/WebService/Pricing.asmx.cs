﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Web.Services;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.HttpModule;
    using LendersOffice.Security;
    using los.RatePrice;
    using LendersOffice;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// Summary description for Pricing
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Pricing : System.Web.Services.WebService
    {
        private void SetEnabledPerformancePageSizeWarning(bool enabled)
        {
            PerformanceMonitorItem item = PerformanceMonitorItem.Current;
            if (null == item)
            {
                return;
            }
            item.IsEnabledPageSizeWarning = enabled;
        }
        private void SetEnabledPerformancePageRenderTimeWarning(bool enabled)
        {
            PerformanceMonitorItem item = PerformanceMonitorItem.Current;
            if (null == item)
            {
                return;
            }
            item.IsEnabledPageRenderTimeWarning = enabled;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sTicket"></param>
        /// <param name="sLNm"></param>
        /// <param name="priceGroupName">Pass empty string to use the default price group.</param>
        /// <returns></returns>
        [WebMethod]
        public string RunPricing(string sTicket, string sLNm, string priceGroupName)
        {
            return RunPricingImpl(sTicket, sLNm, priceGroupName, E_TriState.Blank);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sTicket"></param>
        /// <param name="sLNm"></param>
        /// <param name="priceGroupName">Pass empty string to use the default price group.</param>
        /// <returns></returns>
        [WebMethod]
        public string RunPricingPerRateOverride(string sTicket, string sLNm, string priceGroupName, bool overrideValue)
        {
            // VERY TEMP Method.  For deployment of the per-rate pricing, we 
            // are using a temp lender bit, but Budi needs a way for Dualtest 
            // to choose to override the bit and run perrate directly.
            return RunPricingImpl(sTicket, sLNm, priceGroupName, overrideValue ? E_TriState.Yes : E_TriState.No);
        }

        /// <summary>
        /// Return current pricing release info.
        /// </summary>
        /// <param name="key">Authentication key.</param>
        /// <returns></returns>
        [WebMethod]
        public string GetCurrentReleaseInfo(string key)
        {
            // This method will only be use by AWS to pull webservices.lendingqb.com for the latest LPE release info.

            if (string.IsNullOrEmpty(key) || key != ConstStage.SecretKeyForLpeReleaseInfo)
            {
                return "Access Denied";
            }

            var releaseInfo = CLpeReleaseInfo.GetLatestReleaseInfo();

            var result = new { DataLastModifiedD = releaseInfo.DataLastModifiedD, FileKey = releaseInfo.FileKey.Value, ReleaseVer = releaseInfo.ReleaseVer };

            string json = SerializationHelper.JsonNetAnonymousSerialize(result);

            return json;
        }

        private string RunPricingImpl(string sTicket, string sLNm, string priceGroupName, E_TriState perRateOverride)
        {
            SetEnabledPerformancePageRenderTimeWarning(false);
            SetEnabledPerformancePageSizeWarning(false);
            // 3/8/2011 dd - This method design to use in PML Dual Test.
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, sLNm);

            Guid sLId = Tools.GetLoanIdByLoanName(principal.BrokerId, sLNm);
            if (sLId == Guid.Empty)
            {
                throw new IntegrationInvalidLoanNumberException(sLNm);
            }

            Guid sProdLpePriceGroupId = Guid.Empty;

            if (string.IsNullOrEmpty(priceGroupName) == false)
            {
                // Look for price group name. If name does not exist then throw exception.
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", principal.BrokerId),
                                                new SqlParameter("@ExternalPriceGroupEnabled", true)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "ListPricingGroupByBrokerId", parameters))
                {
                    while (reader.Read())
                    {
                        string lpePriceGroupName = (string)reader["LpePriceGroupName"];
                        if (priceGroupName.Equals(lpePriceGroupName, StringComparison.OrdinalIgnoreCase))
                        {
                            sProdLpePriceGroupId = (Guid)reader["LpePriceGroupId"];
                            break;
                        }
                    }
                }
                if (sProdLpePriceGroupId == Guid.Empty)
                {
                    // 3/8/2011 dd - Name not found throw exception.
                    throw new CBaseException(priceGroupName + " not found", priceGroupName + " not found");
                }
            }
            XmlDocument doc = DistributeUnderwritingEngine.GetRegularPricingResultInXml(principal, sLId, sProdLpePriceGroupId);

            return doc.OuterXml;

        }

        /// <summary>
        /// Return a current index value for the standard ARM index.
        /// </summary>
        /// <param name="sTicket"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetArmIndexes(string sTicket)
        {
            AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            var armList = SystemArmIndex.ListSystemArmIndexes();

            XDocument xdoc = new XDocument();

            XElement root = new XElement("arm_indexes");
            xdoc.Add(root);

            foreach (var arm in armList)
            {
                root.Add(new XElement("arm_index",
                    new XAttribute("name", arm.IndexNameVstr),
                    new XAttribute("value", arm.IndexCurrentValueDecimal.ToString("F3")),
                    new XAttribute("effective_date", arm.EffectiveD.ToString("MM/dd/yyyy"))));
            }

            return xdoc.ToString();
        }
    }
}
