// <copyright file="WebServiceUtilities.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Isaac Ribakoff
//    Date:   7/23/2014 5:15:34 PM 
// </summary>

namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Web;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.Security;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Assorted utilities for LendersOfficeApp.WebService namespace.
    /// </summary>
    internal static class WebServiceUtilities
    {
        /// <summary>
        /// Error format string for fields.
        /// </summary>
        private static string InvalidFieldFormatString = "Invalid value in {0} Field: {1}.";

        /// <summary>
        /// Creates the error string for an invalid field.
        /// </summary>
        /// <param name="fieldName">The field name.</param>
        /// <param name="fieldValue">The field value.</param>
        /// <returns>The formatted string.</returns>
        internal static string CreateInvalidFieldString(string fieldName, string fieldValue)
        {
            return string.Format(InvalidFieldFormatString, fieldName, fieldValue);
        }

        /// <summary>
        /// Wraps response string in xml format.
        /// </summary>
        /// <param name="resultElement">Content element.</param>
        /// <returns>Xml representation.</returns>
        internal static string GenerateResponseXml(XElement resultElement)
        {
            XDocument docElement = new XDocument();
            XElement rootElement = new XElement("LOXmlFormat", new XAttribute("version", ConstAppDavid.LOXmlFormatVersion));
            docElement.Add(rootElement);
            rootElement.Add(resultElement);
            return docElement.ToString();
        }

        /// <summary>
        /// DEPRECATED! Wraps error response string in xml format.
        /// </summary>
        /// <param name="errMsg">Error message string.</param>
        /// <returns>Xml representation of error message.</returns>
        internal static string GenerateErrorResponseXml_Old(string errMsg)
        {
            XElement result = new XElement("result", new XAttribute("status", "Error"), new XElement("error", errMsg));
            return result.ToString(SaveOptions.DisableFormatting);
        }

        /// <summary>
        /// DEPRECATED! Creates a warning response xml.
        /// </summary>
        /// <param name="warningMsg">The warning message.</param>
        /// <returns>The warning response xml.</returns>
        internal static string GenerateWarningResponseXml_Old(string warningMsg)
        {
            XElement result = new XElement("result", new XAttribute("status", "OKWithWarning"), new XElement("warning", warningMsg));
            return result.ToString(SaveOptions.DisableFormatting);
        }

        /// <summary>
        /// Creates a warning response xml.
        /// </summary>
        /// <param name="warnings">The warnings.</param>
        /// <returns>The warning response xml.</returns>
        internal static string GenerateWarningResponseXml(params string[] warnings)
        {
            ServiceResult result = new ServiceResult();
            result.Status = ServiceResultStatus.OKWithWarning;
            foreach (var warning in warnings)
            {
                result.AppendWarning(warning);
            }

            return result.ToResponse();
        }

        /// <summary>
        /// Creates an error response xml.
        /// </summary>
        /// <param name="errors">The errors.</param>
        /// <returns>The error response xml.</returns>
        internal static string GenerateErrorResponseXml(params string[] errors)
        {
            ErrorServiceResult errorResult = new ErrorServiceResult(errors);
            return errorResult.ToResponse();
        }

        /// <summary>
        /// Wraps a <see cref="CBaseException"/> error message in XML format.
        /// </summary>
        /// <param name="exc">An exception that occurred during a web service.</param>
        /// <returns>An XML-wrapped error message.</returns>
        internal static string GenerateErrorResponseXml(CBaseException exc)
        {
            if (exc is WarningOnlyWebserviceException)
            {
                // This type of exception always has a generic user message, and the InnerException
                // is likely to contain a more informative explanation of what went wrong.
                if (exc.InnerException is CBaseException)
                {
                    var cbaseExc = (CBaseException)exc.InnerException;
                    return GenerateErrorResponseXml(cbaseExc.UserMessage);
                }

                if (exc.InnerException is DeveloperException)
                {
                    var devExc = (DeveloperException)exc.InnerException;
                    return GenerateErrorResponseXml(devExc.Message);
                }
            }

            return GenerateErrorResponseXml(exc.UserMessage);
        }

        /// <summary>
        /// Returns object property name in string form.
        /// </summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="expression">Use () => someObject.someProperty.</param>
        /// <returns>String representation of property.</returns>
        internal static string GetPropertyName<T>(Expression<Func<T>> expression)
        {
            MemberExpression body = (MemberExpression)expression.Body;
            return body.Member.Name;
        }

        /// <summary>
        /// Parses user-provided xml and updates licenseInfoList accordingly.
        /// </summary>
        /// <param name="lendingLicenseXml">XmlElement containing licensing information.</param>
        /// <param name="licenseInfoList">LicenseInfoList object to be updated.</param>
        /// <returns>Empty string if setting is successful, error message if unsuccessful.</returns>
        internal static string SetLendingLicenseInfo(XmlElement lendingLicenseXml, LendersOffice.Admin.LicenseInfoList licenseInfoList)
        {
            string licenseNum = lendingLicenseXml.GetAttribute("LicenseNumber");

            if (string.IsNullOrEmpty(licenseNum) || string.IsNullOrEmpty(licenseNum.TrimWhitespaceAndBOM()))
            {
                return "The LicenseNumber attribute must be set for all Lending Licenses";
            }

            if (licenseNum.Length > 30)
            {
                return string.Format("Lending License {0} has exceeded the maximum length (30) allowed for a license number", licenseNum);
            }

            if (lendingLicenseXml.HasAttribute("Action") == false)
            {
                return string.Format("The Action attribute is missing for Lending License {0}", licenseNum);
            }

            string licenseAction = lendingLicenseXml.GetAttribute("Action");
            int action;

            try
            {
                action = int.Parse(licenseAction);
            }
            catch (FormatException)
            {
                return string.Format("Invalid integer format for the Action attribute of Lending License {0}", licenseNum);
            }

            string licenseState = null;
            if (lendingLicenseXml.HasAttribute("State"))
            {
                licenseState = lendingLicenseXml.GetAttribute("State").Replace("'", string.Empty).Replace("\"", string.Empty);
            }

            string expDate = null;
            if (lendingLicenseXml.HasAttribute("ExpirationDate"))
            {
                expDate = lendingLicenseXml.GetAttribute("ExpirationDate").Replace("'", string.Empty).Replace("\"", string.Empty);
            }

            if (string.IsNullOrEmpty(expDate) || string.IsNullOrEmpty(expDate.TrimWhitespaceAndBOM()))
            {
                try
                {
                    DateTime.Parse(expDate);
                }
                catch (FormatException)
                {
                    return string.Format("Invalid date format for the ExpirationDate attribute of lending license {0}", licenseNum);
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    return string.Format("Invalid date for the ExpirationDate attribute of lending license {0}", licenseNum);
                }
            }

            ////0 - Add
            ////1 - Update
            ////2 - Delete
            switch (action)
            {
                case 0:
                    if (licenseInfoList.IsFull)
                    {
                        return $"Cannot add lending license number {licenseNum} - You have entered the maximum number of licenses ({LicenseInfoList.MAX_LICENSES}).";
                    }
                    else if (licenseInfoList.Exists(licenseNum, licenseState) != null)
                    {
                        return $"Cannot add lending license number {licenseNum} for state {licenseState} because it already exists in this object.";
                    }

                    licenseInfoList.Add(new LicenseInfo(licenseState, expDate, licenseNum));
                    break;
                case 1:
                    if (licenseInfoList.Count == 0)
                    {
                        return "There are currently no lending licenses in this object.";
                    }
                    else if (licenseInfoList.IsDuplicateLicenseNum(licenseNum))
                    {
                        return string.Format("Cannot update lending license {0} because there are multiple licenses with the same license number", licenseNum);
                    }
                    else if (licenseInfoList.Exists(licenseNum) == false)
                    {
                        return string.Format("Cannot update lending license {0} - License number not found", licenseNum);
                    }
                    else
                    {
                        licenseInfoList.UpdateLicense(licenseNum, licenseState, expDate);
                    }

                    break;
                case 2:
                    if (licenseInfoList.Count == 0)
                    {
                        return "There are currently no lending licenses in this object.";
                    }
                    else if (licenseInfoList.IsDuplicateLicenseNum(licenseNum))
                    {
                        return string.Format("Cannot delete lending license {0} because there are multiple licenses with the same license number", licenseNum);
                    }
                    else
                    {
                        licenseInfoList.DeleteLicense(licenseNum);
                    }

                    break;
                default:
                    return string.Format("The action attribute LendingLicense {0} must contain a value of 0, 1, or 2", licenseNum);
            }

            return string.Empty;
        }

        /// <summary>
        /// Tries to obtain AbstractUserPrincipal from authorizationTicket.
        /// </summary>
        /// <param name="authorizationTicket">Authorization ticket string generated by Authorization Service.</param>
        /// <param name="loanNumber">The loan number (<see cref="CPageData.sLNm" />) being accessed in the call; used for tickets that are restricted to particular loans. Pass null or empty if loan is not specified.</param>
        /// <param name="principal">AbstractUserPrincipal object to be set.</param>
        /// <returns>String containing error messages. Empty if successful.</returns>
        internal static string TryToGetUserPrincipal(string authorizationTicket, string loanNumber, out AbstractUserPrincipal principal)
        {
            principal = null;
            try
            {
                principal = AuthServiceHelper.GetPrincipalFromTicket(authorizationTicket, loanNumber);
            }
            catch (CBaseException a)
            {
                return a.Message;
            }

            if (null == principal)
            {
                return ErrorMessages.WebServices.InvalidAuthTicket;
            }

            return string.Empty;
        }

        /// <summary>
        /// Tries to parse xmlDoc into XmlNodeList object.
        /// </summary>
        /// <param name="xmlDoc">Xml string to be parsed.</param>
        /// <param name="list">XmlNodeList object to be created.</param>
        /// <returns>String containing error messages. Empty if successful.</returns>
        internal static string TryToParseXmlString(string xmlDoc, out XmlNodeList list)
        {
            list = null;
            string error;
            XmlDocument userXml = TryParseXmlDoc(xmlDoc, out error);
            if (userXml == null || !string.IsNullOrEmpty(error))
            {
                return error;
            }

            try
            {
                list = userXml.SelectNodes("//field");
            }
            catch (System.Xml.XPath.XPathException e)
            {
                Tools.LogError("Error while selecting fields from xml document in TryToParseXmlString: " + e);
                return "XML Document does not have the correct structure - please refer to the data dictionary";
            }

            return string.Empty;
        }

        /// <summary>
        /// Tries to parse the xml string into an XmlDocument.
        /// </summary>
        /// <param name="xmlString">The xml string to parse.</param>
        /// <param name="errors">Any errors encountered while parsing.</param>
        /// <returns>The XmlDocument if successful. Null otherwise.</returns>
        internal static XmlDocument TryParseXmlDoc(string xmlString, out string errors)
        {
            XmlDocument userXml = null;
            try
            {
                userXml = Tools.CreateXmlDoc(xmlString);
            }
            catch (System.Xml.XmlException x)
            {
                Tools.LogError("Error creating XML document in TryToParseXmlString: " + x);
                errors = "Invalid XML Document: " + x.Message;
                return null;
            }

            if (null == userXml)
            {
                errors = "Invalid XML Document";
                return null;
            }

            errors = string.Empty;
            return userXml;
        }

        internal static bool CanAdministratePmlUsers(AbstractUserPrincipal principal)
        {
            if (principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
            {
                return false;
            }
            else if (principal.HasPermission(Permission.CanAdministrateExternalUsers))
            {
                return true;
            }
            else if (principal.IsInRole(ConstApp.ROLE_ADMINISTRATOR))
            {
                return true;
            }
            else if (principal.Type == "P")
            {
                try
                {
                    EmployeeDB empDB = new EmployeeDB(principal.EmployeeId, principal.BrokerId);
                    empDB.Retrieve();
                    if (empDB.IsPmlManager)
                    {
                        return true;
                    }
                }
                catch (CBaseException e)
                {
                    Tools.LogError("Unable to retrieve PML user employeedb object in PMLUser webservice", e);
                }

                return false;
            }
            else
            {
                return false;
            }
        }
    }
}