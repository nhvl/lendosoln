﻿namespace LendersOfficeApp.WebService
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web.Services;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Webservices;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using LendersOffice.Common;
    using LqbGrammar.Drivers.SecurityEventLogging;

    /// <summary>
    /// Container for methods to access Employee data.
    /// </summary>
    [WebService(Namespace = "http://www.lendersoffice.com/los/webservices/")]
    public class Employee : System.Web.Services.WebService
    {
        /// <summary>
        /// A secret key used to ensure that the <see cref="ExportEmployees(string, string, string[], DateTime)"/> method is only used by LendingQB processes.
        /// </summary>
        private const string ExportEmployeesSecretKey = "5EJgr8sACCgbJKes00hm";

        /// <summary>
        /// Represents the different field IDs that can be exported by <see cref="ExportEmployees(string, string, EmployeeFieldId[], DateTime)"/>.
        /// </summary>
        public enum EmployeeFieldId
        {
            EmployeeId = 1,
            BranchId = 2,
            UserFirstNm = 3,
            UserLastNm = 4,
            IsActive = 5,
            Addr = 6,
            City = 7,
            State = 8,
            Zip = 9,
            Phone = 10,
            Fax = 11,
            StartD = 12,
            NewOnlineLoanEventNotifOptionT = 13,
            Email = 14,
            NotesByEmployer = 15,
            EmployeeUserId = 16,
            EmployeeLicenseNumber = 17,
            CommissionPointOfLoanAmount = 18,
            CommissionPointOfGrossProfit = 19,
            CommissionMinBase = 20,
            LicenseXmlContent = 21,
            CellPhone = 22,
            Pager = 23,
            NmlsIdentifier = 24,
            OriginatorCompensationAuditXml = 26,
            OriginatorCompensationPercent = 27,
            OriginatorCompensationBaseT = 28,
            OriginatorCompensationMinAmount = 29,
            OriginatorCompensationMaxAmount = 30,
            OriginatorCompensationFixedAmount = 31,
            OriginatorCompensationNotes = 32,
            OriginatorCompensationLastModifiedDate = 33,
            TaskRelatedEmailOptionT = 34,
            OriginatorCompensationSetLevelT = 35,
            EmployeeIDInCompany = 36,
            EmployeeStartD = 37,
            EmployeeTerminationD = 38,
            CustomPricingPolicyField1Fixed = 39,
            CustomPricingPolicyField2Fixed = 40,
            CustomPricingPolicyField3Fixed = 41,
            CustomPricingPolicyField4Fixed = 42,
            CustomPricingPolicyField5Fixed = 43,
            CustomPricingPolicyFieldSource = 44,
            ChumsId = 45,
            CanBeMemberOfMultipleTeams = 46,
            UserMiddleNm = 47,
            UserSuffix = 48,
            UserNmOnLoanDocs = 49,
            UserNmOnLoanDocsLckd = 50,
            FavoritePageIdsJSON = 51,
            CanContactSupport = 52,
            LoginNm = 53,
            NeedToAcceptLatestAgreement = 54,
            PasswordExpirationD = 55,
            PasswordExpirationPeriod = 56,
            RecentLoginD = 57,
            IsActiveDirectoryUser = 58,
            UserId = 59,
            Permissions = 60,
            IsAccountOwner = 61,
            ByPassBgCalcForGfeAsDefault = 62,
            LenderAccExecEmployeeId = 63,
            LastUsedCreditProtocolId = 64,
            LockDeskEmployeeId = 65,
            SelectedPipelineCustomReportId = 66,
            ManagerEmployeeId = 67,
            LpePriceGroupId = 68,
            UnderwriterEmployeeId = 69,
            ProcessorEmployeeId = 70,
            LpeRsExpirationByPassPermission = 71,
            MustLogInFromTheseIpAddresses = 72,
            IsQuickPricerEnabled = 73,
            LastModifiedDate = 74,
            AssignedPrintGroups = 75,
            JuniorUnderwriterEmployeeID = 76,
            JuniorProcessorEmployeeID = 77,
            ShowQMStatusInPml2 = 78,
            LoanOfficerAssistantEmployeeID = 79,
            EnabledIpRestriction = 80,
            EnabledMultiFactorAuthentication = 81,
            EnabledClientDigitalCertificateInstall = 82,
            UsePml2AsQuickPricer = 83,
            SupportEmail = 84,
            SupportEmailVerified = 85,
            OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = 86
        }

        /// <summary>
        /// Returns a set format of CSV file containing information for all employees of the broker belonging to the passed-in authentication ticket.
        /// </summary>
        /// <param name="sTicket">An authentication ticket from <see cref="AuthService"/>.</param>
        /// <returns>A CSV-formatted string of employee information.</returns>
        [WebMethod(Description = "This method returns a CSV file containing information for all employees.")]
        public string ExportEmployeeInformation(string sTicket)
        {
            AbstractUserPrincipal principal = AuthServiceHelper.GetPrincipalFromTicket(sTicket, null);

            if (principal == null)
            {
                return GenerateErrorResponseXml("Invalid authorization ticket");
            }

            if (!principal.IsInRole(ConstApp.ROLE_ADMINISTRATOR))
            {
                return GenerateErrorResponseXml("You lack permission to export employee information.");
            }

            var exporter = new EmployeeExporter(principal.BrokerId);
            string employeeListCsv = exporter.ExportCsv();

            return employeeListCsv;
        }

        /// <summary>
        /// Exports employees that have been modified since a certain time from the LendingQB database. Created for use with the database replication service.
        /// </summary>
        /// <param name="ticket">Authentication ticket returned by <see cref="AuthService"/>. The user must be in the <see cref="ConstApp.ROLE_ADMINISTRATOR"/> role, or an error will be returned.</param>
        /// <param name="key">A secret authentication key to be used only by LendingQB. This is checked against <see cref="ExportEmployeesSecretKey"/> to verify that the web service method is being called from an LQB source.</param>
        /// <param name="fieldsToExport">A non-null, non-empty list of field names to export.</param>
        /// <param name="lastModifiedDateTime">The <see cref="DateTime"/> when the client-side service was last modified. <see cref="DateTime.MinValue"/> and <see cref="DateTime.MaxValue"/> are considered invalid.</param>
        /// <returns>An xml-formatted string containing data points for each employee belonging to the broker.</returns>
        [WebMethod(Description = "LENDINGQB INTERNAL USE ONLY.")]
        public string ExportEmployees(string ticket, string key, EmployeeFieldId[] fieldsToExport, DateTime? lastModifiedDateTime)
        {
            ServiceResult serviceResult = new ServiceResult();
            fieldsToExport = fieldsToExport.Distinct().ToArray();
            if (!key.Equals(ExportEmployeesSecretKey))
            {
                return GenerateErrorResponse(serviceResult, "Invalid credentials.");
            }

            AbstractUserPrincipal callingUserPrincipal = null;
            try
            {
                callingUserPrincipal = AuthServiceHelper.GetPrincipalFromTicket(ticket, null);
                if (callingUserPrincipal == null)
                {
                    return GenerateErrorResponse(serviceResult, "Invalid credentials.");
                }
            }
            catch (AccessDenied)
            {
                return GenerateErrorResponse(serviceResult, "Invalid credentials.");
            }

            if (!callingUserPrincipal.IsInRole(ConstApp.ROLE_ADMINISTRATOR))
            {
                return GenerateErrorResponse(serviceResult, "You lack permission to export employee information.");
            }

            if (fieldsToExport == null || fieldsToExport.Count() == 0)
            {
                return GenerateErrorResponse(serviceResult, "Please provide a list of fields to include in the export.");
            }

            if (lastModifiedDateTime == DateTime.MinValue || lastModifiedDateTime == DateTime.MaxValue)
            {
                return GenerateErrorResponse(serviceResult, "Please provide a valid lastModifiedDateTime.");
            }

            XElement lendingQBLoanData = new XElement("LendingQBLoanData");

            IEnumerable<Tuple<BrokerUserPrincipal, EmployeeDB>> employeeObjectsList = RetrieveEmployeeUserData(callingUserPrincipal.BrokerId, lastModifiedDateTime);
            foreach (Tuple<BrokerUserPrincipal, EmployeeDB> employeeObjects in employeeObjectsList)
            {
                BrokerUserPrincipal brokerUser = employeeObjects.Item1;
                EmployeeDB employeeData = employeeObjects.Item2;

                // Don't include if the user can't be loaded, or is a special "B" user with the "Can Modify Loan Programs" permission (Internal users).
                if (brokerUser == null || brokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    continue;
                }

                XElement employeeElement = new XElement("employee");
                foreach (EmployeeFieldId fieldName in fieldsToExport)
                {
                    employeeElement.Add(CreateXmlField(fieldName, GetEmployeeDataPoint(fieldName, employeeData, brokerUser)));
                }

                lendingQBLoanData.Add(employeeElement);
            }

            serviceResult.Status = ServiceResultStatus.OK;
            XElement xmlResponse = new XElement("response", XElement.Parse(serviceResult.ToResponse()), lendingQBLoanData);

            return xmlResponse.ToString(SaveOptions.DisableFormatting);
        }

        /// <summary>
        /// Retrieves the <see cref="EmployeeDB"/> and <see cref="BrokerUserPrincipal"/> data to be sent to the client with a single query to the database.
        /// </summary>
        /// <param name="connInfo">The connection info object to be used for connecting to the database.</param>
        /// <param name="brokerId">The brokerId for which the employee records should be loaded.</param>
        /// <param name="lastModifiedDate">The earliest "Last Modified Date" among the <see cref="EmployeeDB"/> records that should be returned from the query.</param>
        /// <returns>An IEnumerable containing pairs of corresponding <see cref="BrokerUserPrincipal"/> and <see cref="EmployeeDB"/> objects.</returns>
        private IEnumerable<Tuple<BrokerUserPrincipal, EmployeeDB>> RetrieveEmployeeUserData(Guid brokerId, DateTime? lastModifiedDate)
        {
            var employeeRecords = new List<Tuple<Dictionary<string, object>, EmployeeDB>>();
            using (var dataReader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveEmployeeUserListForBroker", new SqlParameter[] { new SqlParameter("@BrokerId", brokerId), new SqlParameter("@LastModifiedDate", lastModifiedDate) }))
            {
                while (dataReader.Read())
                {
                    EmployeeDB employeeDB = new EmployeeDB((Guid)dataReader["EmployeeId"], brokerId);
                    employeeDB.Retrieve(dataReader);
                    employeeRecords.Add(Tuple.Create(dataReader.ToDictionary(), employeeDB));
                }
            }

            foreach (var employeeRecord in employeeRecords)
            {
                BrokerUserPrincipal principal = (BrokerUserPrincipal)BrokerUserPrincipal.CreatePrincipal(
                    employeeRecord.Item1,
                    expectedLoginSessionId: Guid.Empty,
                    allowDuplicateLogin: true,
                    applicationT: E_ApplicationT.ToBeDefineFromUserType,
                    isStoreToCookie: false,
                    updateCurrentPrincipal: false);

                yield return new Tuple<BrokerUserPrincipal, EmployeeDB>(principal, employeeRecord.Item2);
            }
        }

        /// <summary>
        /// Creates a "field" XML element to represent a data point about an an employee. Returns null if passed a null or empty value.
        /// </summary>
        /// <param name="fieldId">The name / identifier for the field.</param>
        /// <param name="value">The value of the field in string form.</param>
        /// <returns>An <see cref="XElement"/> representing the field in the form: &lt;field id = "fieldName"&gt;fieldValue&lt;/field&gt;.</returns>
        private static XElement CreateXmlField(EmployeeFieldId fieldId, string value)
        {
            if (!Enum.IsDefined(typeof(EmployeeFieldId), fieldId) || string.IsNullOrEmpty(value))
            {
                return null;
            }

            XElement fieldAsElement = new XElement("field");
            fieldAsElement.Add(new XAttribute("id", fieldId.ToString("G")));
            fieldAsElement.Value = value;

            return fieldAsElement;
        }

        /// <summary>
        /// Generates an error message to send.
        /// </summary>
        /// <param name="serviceResult">A <see cref="ServiceResult"/> object which already contains any errors or other messages to include.</param>
        /// <param name="message">An error message to add to the <see cref="serviceResult"/> before sending.</param>
        /// <returns>An error message xml "result" element.</returns>
        private static string GenerateErrorResponse(ServiceResult serviceResult, string message)
        {
            serviceResult.Status = ServiceResultStatus.Error;
            serviceResult.AppendError(message);
            return serviceResult.ToResponse();
        }

        /// <summary>
        /// Generates a simple error response with the given error message.
        /// </summary>
        /// <param name="errMsg">An error message to include.</param>
        /// <returns>The error response string.</returns>
        private static string GenerateErrorResponseXml(string errMsg)
        {
            return string.Format("<result status=\"Error\"><error>{0}</error></result>", errMsg);
        }

        /// <summary>
        /// Uses a value from one of 3 different employee objects to populate an XML representation of the field/value information.
        /// </summary>
        /// <param name="fieldName">The field name enumeration value to look up the field value for.</param>
        /// <param name="employeeData">The <see cref="EmployeeDB"/> object which may be used to retrieve the field value.</param>
        /// <param name="employeeDetails">The <see cref="EmployeeDetails"/> object which may be used to retrieve the field value.</param>
        /// <param name="brokerUser">The <see cref="BrokerUserPrincipal"/> object which may be used to retrieve the field value.</param>
        /// <returns>An xml field value with the proper LQB system value for the field Id.</returns>
        private static string GetEmployeeDataPoint(EmployeeFieldId fieldName, EmployeeDB employeeData, BrokerUserPrincipal brokerUser)
        {
            switch (fieldName)
            {
                // Data points from dbo.EMPLOYEE
                case EmployeeFieldId.EmployeeId:
                    return employeeData.ID.ToString();

                case EmployeeFieldId.BranchId:
                    return employeeData.BranchID.ToString();

                case EmployeeFieldId.UserFirstNm:
                    return employeeData.FirstName;

                case EmployeeFieldId.UserLastNm:
                    return employeeData.LastName;

                case EmployeeFieldId.IsActive:
                    return employeeData.IsActive.ToString();

                case EmployeeFieldId.Addr:
                    return employeeData.Address.StreetAddress;

                case EmployeeFieldId.City:
                    return employeeData.Address.City;

                case EmployeeFieldId.State:
                    return employeeData.Address.State;

                case EmployeeFieldId.Zip:
                    return employeeData.Address.Zipcode;

                case EmployeeFieldId.Phone:
                    return employeeData.Phone;

                case EmployeeFieldId.Fax:
                    return employeeData.Fax;

                case EmployeeFieldId.StartD:
                    return employeeData.StartD.ToString();

                case EmployeeFieldId.NewOnlineLoanEventNotifOptionT:
                    return employeeData.NewOnlineLoanEventNotifOptionT.ToString("D");

                case EmployeeFieldId.Email:
                    return employeeData.Email;

                case EmployeeFieldId.NotesByEmployer:
                    return employeeData.NotesByEmployer;

                case EmployeeFieldId.EmployeeUserId:
                case EmployeeFieldId.UserId:
                    return employeeData.UserID.ToString();

                case EmployeeFieldId.EmployeeLicenseNumber:
                    return employeeData.AgentLicenseNumber;

                case EmployeeFieldId.CommissionPointOfLoanAmount:
                    return employeeData.CommissionPointOfLoanAmount.ToString();

                case EmployeeFieldId.CommissionPointOfGrossProfit:
                    return employeeData.CommissionPointOfGrossProfit.ToString();

                case EmployeeFieldId.CommissionMinBase:
                    return employeeData.CommissionMinBase.ToString();

                case EmployeeFieldId.LicenseXmlContent:
                    return employeeData.LicenseXmlContent;

                case EmployeeFieldId.CellPhone:
                    return employeeData.CellPhone;

                case EmployeeFieldId.Pager:
                    return employeeData.Pager;

                case EmployeeFieldId.NmlsIdentifier:
                    return employeeData.LosIdentifier;

                case EmployeeFieldId.OriginatorCompensationAuditXml:
                    return employeeData.OriginatorCompensationAuditData.ToString();

                case EmployeeFieldId.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo:
                    return employeeData.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo.ToString();

                case EmployeeFieldId.OriginatorCompensationPercent:
                    return employeeData.OriginatorCompensationPercent.ToString();

                case EmployeeFieldId.OriginatorCompensationBaseT:
                    return employeeData.OriginatorCompensationBaseT.ToString("D");

                case EmployeeFieldId.OriginatorCompensationMinAmount:
                    return employeeData.OriginatorCompensationMinAmount.ToString();

                case EmployeeFieldId.OriginatorCompensationMaxAmount:
                    return employeeData.OriginatorCompensationMaxAmount.ToString();

                case EmployeeFieldId.OriginatorCompensationFixedAmount:
                    return employeeData.OriginatorCompensationFixedAmount.ToString();

                case EmployeeFieldId.OriginatorCompensationNotes:
                    return employeeData.OriginatorCompensationNotes.ToString();

                case EmployeeFieldId.OriginatorCompensationLastModifiedDate:
                    return employeeData.OriginatorCompensationLastModifiedDate.ToString();

                case EmployeeFieldId.TaskRelatedEmailOptionT:
                    return employeeData.TaskRelatedEmailOptionT.ToString("D");

                case EmployeeFieldId.OriginatorCompensationSetLevelT:
                    return employeeData.OriginatorCompensationSetLevelT.ToString("D");

                case EmployeeFieldId.EmployeeIDInCompany:
                    return employeeData.EmployeeIDInCompany;

                case EmployeeFieldId.EmployeeStartD:
                    return employeeData.EmployeeStartD.ToString();

                case EmployeeFieldId.EmployeeTerminationD:
                    return employeeData.EmployeeTerminationD.ToString();

                case EmployeeFieldId.CustomPricingPolicyField1Fixed:
                    return employeeData.CustomPricingPolicyField1Fixed;

                case EmployeeFieldId.CustomPricingPolicyField2Fixed:
                    return employeeData.CustomPricingPolicyField2Fixed;

                case EmployeeFieldId.CustomPricingPolicyField3Fixed:
                    return employeeData.CustomPricingPolicyField3Fixed;

                case EmployeeFieldId.CustomPricingPolicyField4Fixed:
                    return employeeData.CustomPricingPolicyField4Fixed;

                case EmployeeFieldId.CustomPricingPolicyField5Fixed:
                    return employeeData.CustomPricingPolicyField5Fixed;

                case EmployeeFieldId.CustomPricingPolicyFieldSource:
                    return employeeData.CustomPricingPolicyFieldValueSource.ToString("D");

                case EmployeeFieldId.ChumsId:
                    return employeeData.ChumsId;

                case EmployeeFieldId.CanBeMemberOfMultipleTeams:
                    return employeeData.CanBeMemberOfMultipleTeams.ToString();

                case EmployeeFieldId.UserMiddleNm:
                    return employeeData.MiddleName;

                case EmployeeFieldId.UserSuffix:
                    return employeeData.Suffix;

                case EmployeeFieldId.UserNmOnLoanDocs:
                    return employeeData.NmOnLoanDocs;

                case EmployeeFieldId.UserNmOnLoanDocsLckd:
                    return employeeData.NmOnLoanDocsLckd.ToString();

                case EmployeeFieldId.FavoritePageIdsJSON:
                    return employeeData.FavoritePageIdsJSON;

                case EmployeeFieldId.CanContactSupport:
                    return employeeData.CanContactSupport.ToString();

                // Data points from dbo.ALL_USER
                case EmployeeFieldId.LoginNm:
                    return employeeData.LoginName;

                case EmployeeFieldId.NeedToAcceptLatestAgreement:
                    return brokerUser.NeedToAcceptLatestAgreement.ToString();

                case EmployeeFieldId.PasswordExpirationD:
                    return employeeData.PasswordExpirationD.ToString();

                case EmployeeFieldId.PasswordExpirationPeriod:
                    return employeeData.PasswordExpirationPeriod.ToString();

                case EmployeeFieldId.RecentLoginD:
                    return employeeData.RecentLoginD.ToString();

                case EmployeeFieldId.IsActiveDirectoryUser:
                    return employeeData.IsActiveDirectoryUser.ToString();

                /// Data points from dbo.BROKER_USER (primary key is <see cref="EmployeeFieldId.UserId"/>)
                case EmployeeFieldId.Permissions:
                    return brokerUser.Permissions;

                case EmployeeFieldId.IsAccountOwner:
                    return employeeData.IsAccountOwner.ToString();

                case EmployeeFieldId.ByPassBgCalcForGfeAsDefault:
                    return brokerUser.ByPassBgCalcForGfeAsDefault.ToString();

                case EmployeeFieldId.LenderAccExecEmployeeId:
                    return employeeData.LenderAcctExecEmployeeID.ToString();

                case EmployeeFieldId.LastUsedCreditProtocolId:
                    return brokerUser.LastUsedCreditProtocolId.ToString();

                case EmployeeFieldId.LockDeskEmployeeId:
                    return employeeData.LockDeskEmployeeID.ToString();

                case EmployeeFieldId.SelectedPipelineCustomReportId:
                    return brokerUser.SelectedPipelineCustomReportId.ToString();

                case EmployeeFieldId.ManagerEmployeeId:
                    return employeeData.ManagerEmployeeID.ToString();

                case EmployeeFieldId.LpePriceGroupId:
                    return employeeData.LpePriceGroupID.ToString();

                case EmployeeFieldId.UnderwriterEmployeeId:
                    return employeeData.UnderwriterEmployeeID.ToString();

                case EmployeeFieldId.ProcessorEmployeeId:
                    return employeeData.ProcessorEmployeeID.ToString();

                case EmployeeFieldId.LpeRsExpirationByPassPermission:
                    return brokerUser.LpeRsExpirationByPassPermission.ToString();

                case EmployeeFieldId.MustLogInFromTheseIpAddresses:
                    return employeeData.MustLogInFromTheseIpAddresses;

                case EmployeeFieldId.IsQuickPricerEnabled:
                    return employeeData.IsQuickPricerEnabled.ToString();

                case EmployeeFieldId.LastModifiedDate:
                    return employeeData.LastModifiedDate.ToString();

                case EmployeeFieldId.AssignedPrintGroups:
                    return ObsoleteSerializationHelper.JavascriptJsonSerialize(employeeData.AvailablePrintGroups);

                case EmployeeFieldId.JuniorUnderwriterEmployeeID:
                    return employeeData.JuniorUnderwriterEmployeeID.ToString();

                case EmployeeFieldId.JuniorProcessorEmployeeID:
                    return employeeData.JuniorProcessorEmployeeID.ToString();

                case EmployeeFieldId.ShowQMStatusInPml2:
                    return employeeData.ShowQMStatusInPml2.ToString();

                case EmployeeFieldId.LoanOfficerAssistantEmployeeID:
                    return employeeData.LoanOfficerAssistantEmployeeID.ToString();

                case EmployeeFieldId.EnabledIpRestriction:
                    return employeeData.EnabledIpRestriction.ToString();

                case EmployeeFieldId.EnabledMultiFactorAuthentication:
                    return employeeData.EnabledMultiFactorAuthentication.ToString();

                case EmployeeFieldId.EnabledClientDigitalCertificateInstall:
                    return employeeData.EnabledClientDigitalCertificateInstall.ToString();

                case EmployeeFieldId.UsePml2AsQuickPricer:
                    return brokerUser.IsActuallyUsePml2AsQuickPricer.ToString();

                case EmployeeFieldId.SupportEmail:
                    return employeeData.SupportEmail;

                case EmployeeFieldId.SupportEmailVerified:
                    return employeeData.SupportEmailVerified.ToString();

                default:
                    throw new UnhandledEnumException(fieldName);
            }
        }
    }
}