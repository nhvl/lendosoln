using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOfficeApp.los
{
	/// <summary>
	/// Summary description for main2.
	/// </summary>
	public partial class main : BasePage
	{
        private string m_pageTitle = "LendingQB";
        protected string m_mainSrc = "";
		protected void PageLoad(object sender, System.EventArgs e)
		{
            string serverID = "";

            ServerLocation server = ConstAppDavid.CurrentServerLocation;
            switch (server) 
            {
                case ServerLocation.LocalHost:
                    serverID = " (LOCALHOST)  (LOCALHOST)";
                    break;
                case ServerLocation.Demo:
                    serverID = " (DEMO)  (DEMO)";
                    break;
                case ServerLocation.Development:
                    serverID = " (DEVELOPMENT)  (DEVELOPMENT)";
                    break;
                case ServerLocation.DevCopy:
                    serverID = " (DEVCOPY)  (DEVCOPY)";
                    break;
            }
            m_pageTitle += serverID;

            this.Page.Title = m_pageTitle;
            m_mainSrc = "pipeline.aspx?showAlert=" + RequestHelper.GetSafeQueryString("showAlert");


		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
