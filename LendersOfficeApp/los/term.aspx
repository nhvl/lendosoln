<%@ Page language="c#" Codebehind="term.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.term" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>LendingQB Terms & Conditions</title>
  </head>
  <body scroll="no">
	<script type="text/javascript">
  <!--
  function enableBtn(cb) {
    document.getElementById("m_okBtn").disabled = !cb.checked;
  }
  function printTerm() {
      lqbPrintByFrame(termframe);
  }
  //-->
</script>

    <form id="term" method="post" runat="server">
      <iframe src=<%= AspxTools.SafeUrl(VirtualRoot + "/website/legal/term.htm") %> style="width:100%" height="500" scrolling=yes id="termframe"></iframe>
<table>
  <tr>
    <td><input type=button value="I DO NOT accept terms &amp; conditions" onclick="location.replace(<%= AspxTools.JsString(VirtualRoot)%> + '/logout.aspx');"></td></tr>
  <tr>
    <td><asp:CheckBox id=m_acceptCB runat="server" Text="I accept the terms and conditions" onclick="enableBtn(this);"></asp:CheckBox>&nbsp;&nbsp;<asp:Button id=m_okBtn runat="server" Text="Accept Terms &amp; Conditions" enabled="False" onclick="Button1_Click"></asp:Button><input type=button value="Print" onclick="printTerm();"></td></tr>
</table>
     </form>
	
  </body>
</html>
