using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Reminders;
using LendersOffice.Security;
using LendersOffice.DataLock;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using MeridianLink.CommonControls;

namespace LendersOffice.Reminders
{
	public partial class TaskEditor : LendersOffice.Common.BasePage
	{
		private ArrayList m_Cache = new ArrayList();

		private CDiscNotif  m_Task = null;
		private LockAdapter m_Lock = null;

		private BrokerUserPrincipal BrokerUser
		{
			// Access security principal.
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		public String ErrorMessage
		{
			// Write out error message to client.

			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value );
			}
		}

		protected void SubmitClick( object sender , System.EventArgs a )
		{
			// Handle submitting current values and then close.

			try
			{
				// Save back the current values.  We process the simple
				// fields here.  Participant updates (including tracking
				// activation and removal) follow.

				if( m_Task.IsRetrieved == false && m_Task.IsNew == false )
				{
					throw new CBaseException(ErrorMessages.InvalidTaskSubmitted, "Submitting invalid task -- not new and not retrieved." );
				}

				m_Task.DiscSubject = m_Subject.Text.TrimWhitespaceAndBOM();

				m_Task.NotifIsRead = !m_MarkAsNotRead.Checked;

				switch( m_Status.SelectedItem.Value )
				{
					case "A": m_Task.DiscStatus = E_DiscStatus.Active;     break;
					case "D": m_Task.DiscStatus = E_DiscStatus.Done;       break;
					case "S": m_Task.DiscStatus = E_DiscStatus.Suspended;  break;
					case "C": m_Task.DiscStatus = E_DiscStatus.Cancelled;  break;
				}

				switch( m_Priority.SelectedItem.Value )
				{
					case "L": m_Task.DiscPriority = E_DiscPriority.Low;    break;
					case "N": m_Task.DiscPriority = E_DiscPriority.Normal; break;
					case "H": m_Task.DiscPriority = E_DiscPriority.High;   break;
				}

				if( m_LoanNumber.Text.TrimWhitespaceAndBOM() != "" )
				{
					// A loan is referenced, so let's save whatever is placed
					// as the associated loan file.

                    Guid sLId = Tools.GetLoanIdByLoanName(BrokerUser.BrokerId, m_LoanNumber.Text.TrimWhitespaceAndBOM());

                    if (Guid.Empty != sLId)
					{
						m_Task.DiscRefObjNm1 = m_LoanNumber.Text.TrimWhitespaceAndBOM();
						m_Task.DiscRefObjNm2 = m_Borrower.Text.TrimWhitespaceAndBOM();

						m_Task.DiscRefObjId = sLId;

						m_Task.DiscRefObjType = "Loan_File";
					}
				}

				if( m_SetDue.Checked == true && m_DueDate.Text.TrimWhitespaceAndBOM() != "" )
				{
					m_Task.DiscDueDate = DateTime.Parse( m_DueDate.Text.TrimWhitespaceAndBOM() + " " + m_DueTime.Text.TrimWhitespaceAndBOM() );
				}
				else
				{
					m_Task.DiscDueDate = DateTime.MinValue;
				}

				if( m_SetWarn.Checked == true && m_WarnDate.Text.TrimWhitespaceAndBOM() != "" )
				{
					m_Task.NotifAlertDate = DateTime.Parse( m_WarnDate.Text.TrimWhitespaceAndBOM() + " " + m_WarnTime.Text.TrimWhitespaceAndBOM() );
				}
				else
				{
					m_Task.NotifAlertDate = DateTime.MinValue;
				}

				if( m_Add.Text.TrimWhitespaceAndBOM() != "" )
				{
					m_Task.NewMsg = m_Add.Text.TrimWhitespaceAndBOM();
				}

				// 4/8/2005 kb - Save these changes with a shared lock
				// on the cached version record.  We are trying to prevent
				// simultaneous saves and overwriting saves that cause
				// one to blow away the changes saved by another.

				try
				{
					lock( m_Lock.Record )
					{
						// Verify that the version we are saving is consistent
						// with what was last saved.  This is where we punt if
						// someone else saved changes while we were looking and
						// editing ours.

						m_Lock.Check();

						// Write out the changes to the db.  We take the values
						// of these elements as-is.  Submit handling will close
						// once finished.

						if( m_Task.Save( BrokerUser.DisplayName , m_Acknowledge.Checked , m_KeepMonitor.Checked , true , DateTime.Now ) == false )
						{
                            throw new CBaseException(ErrorMessages.FailedToSaveTask, ErrorMessages.FailedToSaveTask);
						}

						// Now that we've saved the submitted task -- and it could
						// be a new one -- we need to update the notifications that
						// are associated with this log (tracking included).  This
						// is tricky business that should be encapsulated by a db-
						// side sproc, but the sproc language is too limited to
						// deal with this kind of merge.  So, we wrap this merge
						// in a transaction and remove those who are not in the
						// active list now, but were, and we add the participants
						// that weren't in the list, but now are in the list.  Note
						// that previously invalidated notifications are brought
						// back from the dead.
						//
						// 9/2/2005 kb - Before joining the final active list to this
						// task using notification rows, we check if the user has
						// asked to be removed/quit on submit.  If so, yank them as
						// removed from the active list.

						if( m_QuitOnSubmit.Checked == true )
						{
							foreach( ParticipantEntry entry in m_Cache )
							{
								if( entry.UserId == BrokerUser.UserId )
								{
									m_Cache.Remove( entry );

									break;
								}
							}
						}

						if( m_Task.Join( BrokerUser.UserId , m_Cache ) == false )
						{
                            throw new CBaseException(ErrorMessages.FailedToJoinTask, ErrorMessages.FailedToJoinTask);
						}

						// Now increase the cached version number so subsequent
						// loads and saves can be maintained as serial.

						m_Lock.Bump();
					}
				}
				catch( VersionOutOfSync e )
				{
					// Someone has saved while we were editing ours.

					ErrorMessage = "Failed to submit task changes: version is out of sync with latest.";

					Tools.LogError( e );
				}

				// We have successfully saved, so notify all listeners.

				m_Task.Signal
					( BrokerUser.DisplayName
					, DateTime.Now
					);
			}
			catch( Exception e )
			{
				// Be vulnerable...  Tell her all your faults.

				ErrorMessage = "Failed to submit task changes.";

				Tools.LogError( e );
			}
			finally
			{
				// Always add the hidden variable to trigger a close
				// of this window on return from this postback.

				ClientScript.RegisterHiddenField( "m_commandToDo" , "Close" );
			}
		}

        protected void EditParticipantsClick(object sender, System.EventArgs a)
        {
            LoadParticipantChoices();
            if (m_Task.DiscRefObjType.ToLower().IndexOf("loan") != -1)
            {
                m_LoanNumber.Text = m_Task.DiscRefObjNm1;
                m_Borrower.Text = m_Task.DiscRefObjNm2;
            }
            m_Log.Text = m_Task.DiscHistory;
            ClientScript.RegisterHiddenField("IsDisplayParticipantsList", "1");
        }

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Initialize this task editor.

			try
			{
				// Bind the active notification list.  We pull
				// these users straight from their cached fields
				// of the notification table.  Hopefully, these
				// fields are kept in sync with name changes.

				if( BrokerUser == null )
				{
					throw new CBaseException(ErrorMessages.InvalidAuthentication, "Unable to authenticate user.");
				}

				// Load the specified task from the database.  If no
				// notification id was specified, then we assume we
				// need a new task and the current user is the author.
				// Note that we may run through here more than once.
				// If this becomes normal operation, then we need to
				// protect against handling the new discussion or new
				// notification case more than once -- need to stash
				// the ids in the view state after the first load and
				// subsequent save.

                LoadTaskFromDB();

                LoadLoanInfoFromDB();

				// Get the details (shared and private) of this task.
				// Once loaded, we pull the content.  After events
				// come in, we will update their state.  For now, we
				// hit the database and reload with each postback.

				if( IsPostBack == false )
				{
					// We load these values from the task once.  All
					// subsequent postbacks rely on the user's state.

                    LoadTaskDetails();
				}

				// Get current participant set and lookup whether we
				// are tracking them.  Note that we don't care if the
				// user is tracked by someone, rather if we are doing
				// the tracking.  I'm keeping this open with two lookups
				// in the database so we can be fluid, using app server
				// processing to do what the db could do.

				if( IsPostBack == false )
				{
                    // We only load the active participant list on the first
					// loading of this page.  All subsequent returns rely on
					// the cached list stored in the hidden variable.
                    LoadActiveParticipantListFromDB();
				}
				else
				{
					// We are processing a postback, so bind the grid
					// to reflect the state of the client-side cache,
					// which is updated by script events on the client.

                    LoadActiveParticipantListFromMemory();
				}

				// Now, build the user's desired state list from the
				// cached, client-edited state.  Even if this is the
				// first load, it's okay -- we just have the database
				// state in a different form.

                LoadClientState();
			}
			catch( Exception e )
			{
				// Be vulnerable...  Tell her all your faults.

				ErrorMessage = "Failed to load task for editing.";

				m_Submit.Enabled = false;

				Tools.LogError( e );
			}
        }

        private void LoadTaskFromDB()
        {
            Guid nid = RequestHelper.GetGuid("notifId", Guid.Empty);
            Guid did = RequestHelper.GetGuid("discLogId", Guid.Empty);
            Guid lid = RequestHelper.GetGuid("loanId", Guid.Empty);

            if (nid != Guid.Empty || did != Guid.Empty)
            {
                // Load this task.  If the task is not able to be
                // retrieved, then we will throw.
                //
                // 3/8/2005 kb - We're hitting too much by way of
                // collisions and failing to load (APB sees it about
                // once a day).  Need to retry loading.

                Boolean found = false;

                try
                {
                    m_Task = new CDiscNotif(BrokerUser.BrokerId, nid, did);

                    for (int t = 1; t < 3 && found == false; ++t)
                    {
                        try
                        {
                            if (m_Task.Retrieve(false, true) == true)
                            {
                                found = true;
                            }
                        }
                        catch (Exception e)
                        {
                            Tools.LogError("Failed to retrieve task " + did + ".", e);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new CBaseException(ErrorMessages.UnableToRetrieveTask, ErrorMessages.UnableToRetrieveTask + " " + e.Message);
                }

                if (found == false)
                {
                    throw new CBaseException(ErrorMessages.UnableToRetrieveTask, ErrorMessages.UnableToRetrieveTask);
                }
            }
            else
            {
                m_Task = new CDiscNotif(BrokerUser.BrokerId, Guid.Empty, Guid.Empty);
            }

            m_Lock = new CDiscNotif.Locker(m_Task.DiscLogId);

            if (IsPostBack == false)
            {
                ViewState.Add("Lock", m_Lock.Retrieve().State);
            }
            else
            {
                m_Lock.State = ViewState["Lock"].ToString();
            }

            if (m_Task.NotifId == Guid.Empty)
            {
                m_Task.NotifReceiverLoginNm = BrokerUser.LoginNm;
                m_Task.NotifReceiverUserId = BrokerUser.UserId;
            }

            if (m_Task.DiscLogId == Guid.Empty)
            {
                m_Task.DiscCreatorLoginNm = BrokerUser.LoginNm;
                m_Task.DiscCreatorUserId = BrokerUser.UserId;

            }

            if (m_Task.DiscRefObjId == Guid.Empty && lid != Guid.Empty)
            {
                // 10/4/2004 kb - We previously depended on the query string
                // to determine the loan that we were associated with.  Some
                // calls to load this ui didn't include the loan in the qs.
                // So, when the loan is not specified in the query string,
                // then we assume the task's member is valid.  When we don't
                // have a loan associated with the task, we assume that the
                // query string is telling us what loan is associated.

                m_Task.DiscRefObjId = lid;
            }
        }

        private void LoadLoanInfoFromDB()
        {
            if (m_Task.DiscRefObjId != Guid.Empty)
            {
                // Pull the specified loan and force and update to
                // the object ref for this task.
                SqlParameter[] parameters = {
                                                new SqlParameter("@LoanId", m_Task.DiscRefObjId)
                                            };
                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "GetLoanNumberByLoanID", parameters ))
                {
                    if (sR.Read() == true)
                    {
                        m_Task.DiscRefObjType = "Loan_File";

                        m_Task.DiscRefObjNm1 = sR["sLNm"].ToString();
                        m_Task.DiscRefObjNm2 = sR["sPrimBorrowerFullNm"].ToString();
                    }
                }
            }
        }

        private void LoadTaskDetails()
        {
            m_Subject.Text = m_Task.DiscSubject;

            #region Status
            switch (m_Task.DiscStatus)
            {
                case E_DiscStatus.Active:
                    {
                        m_Status.SelectedIndex = m_Status.Items.IndexOf(m_Status.Items.FindByValue("A"));
                    }
                    break;

                case E_DiscStatus.Done:
                    {
                        m_Status.SelectedIndex = m_Status.Items.IndexOf(m_Status.Items.FindByValue("D"));
                    }
                    break;

                case E_DiscStatus.Suspended:
                    {
                        m_Status.SelectedIndex = m_Status.Items.IndexOf(m_Status.Items.FindByValue("S"));
                    }
                    break;

                case E_DiscStatus.Cancelled:
                    {
                        m_Status.SelectedIndex = m_Status.Items.IndexOf(m_Status.Items.FindByValue("C"));
                    }
                    break;
            }
            #endregion

            #region Priority
            switch (m_Task.DiscPriority)
            {
                case E_DiscPriority.Low:
                    {
                        m_Priority.SelectedIndex = m_Priority.Items.IndexOf(m_Priority.Items.FindByValue("L"));
                    }
                    break;

                case E_DiscPriority.Normal:
                    {
                        m_Priority.SelectedIndex = m_Priority.Items.IndexOf(m_Priority.Items.FindByValue("N"));
                    }
                    break;

                case E_DiscPriority.High:
                    {
                        m_Priority.SelectedIndex = m_Priority.Items.IndexOf(m_Priority.Items.FindByValue("H"));
                    }
                    break;
            }
            #endregion

            if (m_Task.DiscRefObjType.ToLower().IndexOf("loan") != -1)
            {
                m_LoanNumber.Text = m_Task.DiscRefObjNm1;
                m_Borrower.Text = m_Task.DiscRefObjNm2;
            }

            m_Log.Text = m_Task.DiscHistory;

            #region Due Date
            if (m_Task.DiscDueDate.Date != DateTime.MinValue.Date)
            {
                m_DueDate.Text = m_Task.DiscDueDate.ToShortDateString();
                m_DueTime.Text = m_Task.DiscDueDate.ToShortTimeString();

                if (m_Task.DiscDueDate.CompareTo(DateTime.Now) < 0)
                {
                    // By default, hilight the alert condition and make
                    // the default of a save keep the event in the task
                    // monitor.

                    m_DueDate.ForeColor = Color.Red;

                    m_KeepMonitor.Checked = true;
                }

                m_SetDue.Checked = true;
            }
            else
            {
                m_SetDue.Checked = false;
            }
            #endregion

            #region Alert Date
            if (m_Task.NotifAlertDate != DateTime.MinValue.Date)
            {
                m_WarnDate.Text = m_Task.NotifAlertDate.ToShortDateString();
                m_WarnTime.Text = m_Task.NotifAlertDate.ToShortTimeString();

                if (m_Task.NotifAlertDate.CompareTo(DateTime.Now) < 0)
                {
                    // By default, hilight the alert condition and make
                    // the default of a save keep the event in the task
                    // monitor.

                    m_WarnDate.ForeColor = Color.Red;

                    m_KeepMonitor.Checked = true;
                }

                m_SetWarn.Checked = true;
            }
            else
            {
                m_SetWarn.Checked = false;
            }
            #endregion
        }

        private void LoadActiveParticipantListFromDB()
        {
            Boolean found;
            DataSet ts = new DataSet();
            DataSet ps = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@DiscLogId", m_Task.DiscLogId)
                                            , new SqlParameter("@IsValid", true)
                                        };

            DataSetHelper.Fill(ps, this.BrokerUser.BrokerId, "ListDiscParticipantsByDiscId", parameters);

            parameters = new SqlParameter[] {
                                                new SqlParameter("@DiscLogId", m_Task.DiscLogId)
                                             , new SqlParameter("@CreatorUserId", BrokerUser.UserId)
                                             , new SqlParameter("@IsValid", true)
                                            };
            DataSetHelper.Fill(ts, this.BrokerUser.BrokerId, "ListDiscTracks", parameters);
            m_ActiveInfo.Value = "";

            found = false;

            if (ps != null && ts != null)
            {
                // Merge these results to generate a composit list
                // of active participants.  If the user is found in
                // the tracking set, then we flip the bit true.  If
                // the viewing/editing user is not found in the
                // active set, then we add them here, at the start.
                // This might seem presumptuous, but when we consider
                // that the user can only edit or join (rejoin is the
                // same thing) tasks, then they should always be in
                // the active list.  Can we edit a task that we are
                // not actively involved in?  If this is ever so,
                // then remove the implicit addition of the editing
                // user from this page load code.
                //
                // 12/8/2004 kb - I think I found out what that pesky
                // submit failure was...  We depend on login names
                // as immutable.  This is not the case.  It seems a
                // user can be added to the notify list with one
                // login name, change it, then come back in and add
                // him/herself as unique.  The submit consideres the
                // added user as unique because noone with that login
                // is in the active notify list.  But, someone with
                // the same user id is already in the list.  Oops.

                ArrayList looky = new ArrayList();
                ArrayList array = new ArrayList();

                foreach (DataRow row in ts.Tables[0].Rows)
                {
                    looky.Add(row["NotifReceiverUserId"]);
                }

                foreach (DataRow row in ps.Tables[0].Rows)
                {
                    ParticipantEntry entry = new ParticipantEntry();

                    entry.FirstName = row["UserFirstNm"].ToString();
                    entry.LastName = row["UserLastNm"].ToString();

                    entry.UserId = (Guid)row["NotifReceiverUserId"];

                    if (looky.Contains(entry.UserId) != false)
                    {
                        entry.IsTracked = true;
                    }
                    else
                    {
                        entry.IsTracked = false;
                    }

                    if (entry.UserId == BrokerUser.UserId)
                    {
                        found = true;
                    }

                    array.Add(entry);
                }

                if (found == false)
                {
                    ParticipantEntry entry = new ParticipantEntry();

                    entry.FirstName = BrokerUser.DisplayName;
                    entry.UserId = BrokerUser.UserId;

                    entry.IsTracked = false;

                    array.Add(entry);
                }

                foreach (ParticipantEntry entry in array)
                {
                    if (m_ActiveInfo.Value != "")
                    {
                        m_ActiveInfo.Value += ",";
                    }

                    m_ActiveInfo.Value += entry.UserId + ":" + entry.FirstName + ":" + entry.LastName + ":";

                    if (entry.IsTracked == true)
                    {
                        m_ActiveInfo.Value += "Y";
                    }
                    else
                    {
                        m_ActiveInfo.Value += "N";
                    }
                }

                m_ActiveList.DataSource = array;
                m_ActiveList.DataBind();

                m_ActiveGrid.DataSource = array;
                m_ActiveGrid.DataBind();
            }
        }

        private void LoadActiveParticipantListFromMemory()
        {
            ArrayList array = new ArrayList();

            foreach (String item in m_ActiveInfo.Value.Split(','))
            {
                // Decode the cached list items.  Each item contains
                // the 4 entry elements for an active participant.

                String[] user = item.Split(':');

                if (user.Length != 4)
                {
                    continue;
                }

                // Pull the parts of each entry and add the
                // entry to the list for binding.

                ParticipantEntry entry = new ParticipantEntry();

                entry.UserId = new Guid(user[0]);

                entry.FirstName = user[1];
                entry.LastName = user[2];

                if (user[3] == "Y")
                {
                    entry.IsTracked = true;
                }
                else
                {
                    entry.IsTracked = false;
                }

                array.Add(entry);
            }

            m_ActiveGrid.DataSource = array;
            m_ActiveGrid.DataBind();
        }

        private void LoadParticipantChoices()
        {
            // Initialize the list of employees to include in
            // the participant editor list.  We may need to get
            // their status as a current or previous participant
            // as well.  We maintain separate lists based on roles,
            // so employees typically show up in more than one
            // list.  All the broker's active employees will go
            // into the everyone list.
            //
            // 3/24/2005 kb - Adding the loan's currently assigned
            // employees to the option selector too.
            //
            // 8/15/2005 kb - Revised how we load loan assignments
            // and employees and their roles.  We use the views as
            // much as possible.

            LoanAssignmentContactTable loanRole = new LoanAssignmentContactTable(BrokerUser.BrokerId, m_Task.DiscRefObjId);
            BrokerLoanAssignmentTable userRole = new BrokerLoanAssignmentTable();
            BrokerEmployeeMapping userMaps = new BrokerEmployeeMapping();

            DataSet os = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@DiscLogId", m_Task.DiscLogId)
                                            , new SqlParameter("@IsValid", false)
                                        };
            DataSetHelper.Fill(os, BrokerUser.BrokerId, "ListDiscParticipantsByDiscId", parameters);


            try
            {
                userRole.Retrieve(BrokerUser.BrokerId);
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to load broker's role table of employee roles for " + BrokerUser.BrokerId + ".", e);
            }

            try
            {
                userMaps.Retrieve(BrokerUser.BrokerId);
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to load broker's mapping of employee status for " + BrokerUser.BrokerId + ".", e);
            }


            // Get each row and craft an entry for our choice
            // list box.  We bind our entries to the list.

            Hashtable table = new Hashtable();
            Hashtable track = new Hashtable();
            ArrayList glist = new ArrayList();
            ArrayList alist = new ArrayList();
            ArrayList olist = new ArrayList();

            foreach (String sRole in userRole)
            {
                foreach (BrokerLoanAssignmentTable.Spec eS in userRole[sRole])
                {
                    // Create individual entry for organization.  Note that
                    // we make an entry for each role an employee maintains.
                    //
                    // 8/15/2005 kb - Because we load the roles using the
                    // revised accessor, we need to filter out all non-
                    // internal employees.

                    EmployeeEntry entry = new EmployeeEntry();

                    entry.FirstName = eS.FirstName;
                    entry.LastName = eS.LastName;

                    entry.Role = Role.Get(sRole).ModifiableDesc;
                    entry.Rank = Role.Get(sRole).ImportanceRank;

                    if (userMaps[eS.EmployeeId] != null)
                    {
                        BrokerEmployeeMapping.Spec bS = userMaps[eS.EmployeeId];

                        if (bS.Type != "B")
                        {
                            continue;
                        }

                        entry.UserId = bS.UserId;
                    }
                    else
                    {
                        entry.UserId = Guid.Empty;
                    }

                    // Get the containing short list.  Create a new one if
                    // this is the first employee in this role.

                    ArrayList array = table[sRole] as ArrayList;

                    if (array == null)
                    {
                        table.Add(sRole, array = new ArrayList());
                    }

                    if (track.Contains(entry.UserId) == true)
                    {
                        // Each employee is listed once for every role they
                        // fulfill.  We want to get the highest ranking
                        // role and display that.

                        EmployeeEntry current = track[entry.UserId] as EmployeeEntry;

                        if (current.Rank > entry.Rank)
                        {
                            track[entry.UserId] = entry;
                        }
                    }
                    else
                    {
                        track.Add(entry.UserId, entry);
                    }

                    array.Add(entry);
                }
            }

            // Get all the assigned employees and initialize a list
            // for binding to our section grid.

            foreach (EmployeeLoanAssignment eA in loanRole.Items)
            {
                EmployeeEntry entry = new EmployeeEntry();

                entry.FirstName = eA.FirstName;
                entry.LastName = eA.LastName;
                entry.Role = eA.RoleModifiableDesc;
                entry.Rank = eA.RoleImportanceRank;

                entry.UserId = eA.UserId;

                glist.Add(entry);
            }

            // Walk the list of inactive notifications and add these
            // entries to the previously joined list.

            if (os != null)
            {
                foreach (DataRow old in os.Tables[0].Rows)
                {
                    EmployeeEntry entry = track[old["NotifReceiverUserId"]] as EmployeeEntry;

                    if (entry != null)
                    {
                        olist.Add(entry);
                    }
                }
            }

            foreach (EmployeeEntry entry in track.Values)
            {
                alist.Add(entry);
            }

            foreach (ArrayList array in table.Values)
            {
                array.Sort();
            }

            alist.Sort();

            olist.Sort();

            // Bind each employee set to the corresponding list.
            // Each list is shown as visible when the respective
            // radio button is selected.
            m_EmployeesMgr.DataSource = table["Manager"];
            m_EmployeesMgr.DataBind();

            m_EmployeesPro.DataSource = table["Processor"];
            m_EmployeesPro.DataBind();

            m_EmployeesOff.DataSource = table["Agent"];
            m_EmployeesOff.DataBind();

            m_EmployeesAsg.DataSource = glist;
            m_EmployeesAsg.DataBind();

            m_EmployeesPre.DataSource = olist;
            m_EmployeesPre.DataBind();

            m_EmployeesAll.DataSource = alist;
            m_EmployeesAll.DataBind();
        }

        private void LoadClientState()
        {
            m_Cache.Clear();

            foreach (String item in m_ActiveInfo.Value.Split(','))
            {
                String[] user = item.Split(':');

                if (user.Length == 4)
                {
                    ParticipantEntry entry = new ParticipantEntry();

                    entry.UserId = new Guid(user[0]);

                    entry.FirstName = user[1];
                    entry.LastName = user[2];

                    if (user[3] == "Y")
                    {
                        entry.IsTracked = true;
                    }
                    else
                    {
                        entry.IsTracked = false;
                    }

                    m_Cache.Add(entry);
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
        #endregion
    }
}