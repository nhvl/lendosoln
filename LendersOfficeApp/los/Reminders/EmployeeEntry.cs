﻿using System;

namespace LendersOffice.Reminders
{
    /// <summary>
    /// Trap simple employee details.  Login must
    /// be unique.
    /// </summary>

    public class EmployeeEntry : IComparable
    {
        /// <summary>
        /// Trap simple employee details.  Login must
        /// be unique.
        /// </summary>

        private String m_FirstName;
        private String m_LastName;
        private Guid m_UserId;
        private String m_Role;
        private Int32 m_Rank;
        private String m_Label;

        public Int32 CompareTo(Object oThat)
        {
            Int32 r; EmployeeEntry that = oThat as EmployeeEntry;

            if (that == null)
            {
                return +1;
            }

            r = m_FirstName.CompareTo(that.m_FirstName);

            if (r != 0)
            {
                return r;
            }

            r = m_LastName.CompareTo(that.m_LastName);

            if (r != 0)
            {
                return r;
            }

            r = m_Rank.CompareTo(that.m_Rank);

            if (r != 0)
            {
                return r;
            }

            return 0;
        }

        public String FirstName
        {
            // Access member.

            set
            {
                m_FirstName = value;
            }
            get
            {
                return m_FirstName;
            }
        }

        public String LastName
        {
            // Access member.

            set
            {
                m_LastName = value;
            }
            get
            {
                return m_LastName;
            }
        }

        public Guid UserId
        {
            // Access member.

            set
            {
                m_UserId = value;
            }
            get
            {
                return m_UserId;
            }
        }

        public String Role
        {
            // Access member.

            set
            {
                m_Role = value;
            }
            get
            {
                return m_Role;
            }
        }

        public Int32 Rank
        {
            // Access member.

            set
            {
                m_Rank = value;
            }
            get
            {
                return m_Rank;
            }
        }

        public String Label
        {
            // Access member.

            set
            {
                m_Label = value;
            }
            get
            {
                return m_Label;
            }
        }

        #region ( Constructors )

        /// <summary>
        /// Construct employee descriptor.
        /// </summary>
        /// <param name="sFirstName">
        /// Name of user.
        /// </param>
        /// <param name="sLastName">
        /// Name of user.
        /// </param>
        /// <param name="userId">
        /// Unique user id.
        /// </param>
        /// <param name="sRole">
        /// Employee's role.
        /// </param>
        /// <param name="iRank">
        /// Role's rank.
        /// </param>
        /// <param name="sLabel">
        /// Label for the notification command.
        /// </param>

        public EmployeeEntry(String sFirstName, String sLastName, Guid userId, String sRole, Int32 iRank, String sLabel)
        {
            // Initialize members.

            m_FirstName = sFirstName;
            m_LastName = sLastName;
            m_UserId = userId;
            m_Role = sRole;
            m_Rank = iRank;
            m_Label = sLabel;
        }

        /// <summary>
        /// Construct employee descriptor.
        /// </summary>
        /// <param name="sFirstName">
        /// Name of user.
        /// </param>
        /// <param name="sLastName">
        /// Name of user.
        /// </param>
        /// <param name="userId">
        /// Unique user id.
        /// </param>
        /// <param name="sRole">
        /// Employee's role.
        /// </param>

        public EmployeeEntry(String sFirstName, String sLastName, Guid userId, String sRole)
        {
            // Initialize members.

            m_FirstName = sFirstName;
            m_LastName = sLastName;
            m_UserId = userId;
            m_Role = sRole;

            m_Rank = 100;

            m_Label = "";
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public EmployeeEntry()
        {
            // Initialize members.

            m_FirstName = "";
            m_LastName = "";
            m_Role = "";
            m_Label = "";

            m_UserId = Guid.Empty;

            m_Rank = 100;
        }

        #endregion

    }
}