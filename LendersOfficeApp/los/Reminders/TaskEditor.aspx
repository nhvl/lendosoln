<%@ Page language="c#" Codebehind="TaskEditor.aspx.cs" AutoEventWireup="false" Inherits="LendersOffice.Reminders.TaskEditor" validateRequest=true %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
	<TITLE>TaskEditor</TITLE>
	<LINK href="../../css/stylesheetnew.css" type="text/css" rel="stylesheet">
	<style type="text/css">
		.groupTable {
			width: 100%;
			padding: 2px;
			border-spacing: 2px;
			*padding: 0px; /* use for IE in compatibility mode */
			*border-spacing: 0px; /* use for IE in compatibility mode */
    		border-collapse: collapse;
		}
	</style>
</HEAD>
<BODY MS_POSITIONING="FlowLayout" style="BACKGROUND-COLOR: gainsboro; DISPLAY: none; OVERFLOW-Y: hidden;" onload="onInit();">
	<script>
		var isdirty = false;

		function onInit() { try
		{
			<% if( IsPostBack == false ) { %>
			{
				resize( 640 , 614 );
			}
			<% } %>

			/* OPM 12431 - dl
			if( document.getElementById("m_SetWarn").checked )
			{
				lightupNode( document.getElementById("m_SetWarn").parentElement.children.tags( "div" )[ 0 ] );
			}
			else
			{
				disableNode( document.getElementById("m_SetWarn").parentElement.getElementsByTagName( "div" )[ 0 ] );
			}*/

			var m_SetDue = document.getElementById("m_SetDue");

			if( m_SetDue.checked )
			{
				lightupNode( m_SetDue.parentElement.getElementsByTagName( "div" )[ 0 ] );
			}
			else
			{
				disableNode( m_SetDue.parentElement.getElementsByTagName( "div" )[ 0 ] );
			}

			var i , tags;

			for( i = 0 , tags = document.getElementById("m_ParticipantGroups").getElementsByTagName( "input" ) ; i < tags.length ; ++i )
			{
				if( tags[ i ].checked == null )
				{
					continue;
				}

				if( tags[ i ].checked == true )
				{
					onClickParticipantGroup( tags[ i ] );

					break;
				}
			}

			if( document.getElementById("m_errorMessage") != null )
			{
				alert( document.getElementById("m_errorMessage").value );
			}
			else
			if( document.getElementById("m_commandToDo") != null )
			{
				if( document.getElementById("m_commandToDo").value == "Close" )
				{
					var args = {};
					args.OK = true;

					onClose(args);
				}
			}

			document.body.style.display = "block";

			if( document.getElementById("m_Subject").value == "" )
			{
				document.getElementById("m_Subject").focus();
			}
			else
			{
				document.getElementById("m_Add").focus();
			}

			if(document.getElementById('IsDisplayParticipantsList') && document.getElementById('IsDisplayParticipantsList').value == '1')
			{
	            onDisplayEditParticipants();
	        }
		}
		catch( e )
		{
			alert( e.message );
		}}

		function onDisplayEditParticipants()
		{
		    document.getElementById("m_History").style.display = "none";
			document.getElementById("m_Options").style.display = "none";
			document.getElementById("m_Editor").style.display = "";
		}

		function onEditParticipants()
		{
		    try
		    {
			    if(!document.getElementById('IsDisplayParticipantsList') || document.getElementById('IsDisplayParticipantsList').value == '0')
			    {
			        document.getElementById(<%= AspxTools.JsGetClientIdString(m_editParticipants) %>).click();
			    }
			    else
			    {
			        onDisplayEditParticipants();
			    }
		    }
		    catch( e )
		    {
			    alert( e.message );
		    }
		}

		function onEditHistoryLog() { try
		{
			document.getElementById("m_Editor").style.display  =  "none";
			document.getElementById("m_History").style.display = "";
			document.getElementById("m_Options").style.display = "";
		}
		catch( e )
		{
			alert( e.message );
		}}

		function disableNode( oControl ) { try
		{
			if( oControl == null )
			{
				return;
			}

			var i = 0;
			var all = oControl.getElementsByTagName("*");
			while( i < all.length )
			{
				var c = all[i];

				c.style.backgroundColor = "gainsboro";

				if( c.disabled != null )
				{
				    setDisabledAttr(c, true);
				}

				++i;
			}
		}
		catch( e )
		{
		}}

		function lightupNode( oControl ) { try
		{
			if( oControl == null )
			{
				return;
			}

			var i = 0;

			var all = oControl.getElementsByTagName("*");
			while( i < all.length )
			{
				var c = all[i];

				c.style.backgroundColor = "";

				if( c.disabled != null )
				{
				    setDisabledAttr(c, false);
				}

				++i;
			}
		}
		catch( e )
		{
		}}

		function markTaskDirty() { try
		{
			isdirty = true;
		}
		catch( e )
		{
		}}

		function onClose(args) { try
		{
			if( isdirty == true )
			{
				if( confirm( "Changes will be lost.  Close without submitting?" ) == false )
				{
					return;
				}
			}

			onClosePopup(args);
		}
		catch( e )
		{
		}}
	</script>
	<h4 class="page-header">Edit Task</h4>
	<FORM id="TaskEditor" method="post" runat="server">
		<TABLE cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
		<TR valign="top" id="m_TaskDetails">
		<TD height="1px">
			<TABLE cellpadding="0" cellspacing="6" border="0" width="100%">
			<TR valign="top">
			<TD>
				<TABLE cellpadding="4" cellspacing="0" border="0" width="100%">
				<TR class="FormTableHeader">
				<TD colspan="2" style="PADDING: 4px;" nowrap>
					Task Details
				</TD>
				</TR>
				<TR style="PADDING-TOP: 4px;">
				<TD class="FieldLabel" nowrap>
					Subject
				</TD>
				<TD align="left" nowrap>
					<ASP:TextBox id="m_Subject" runat="server" style="PADDING-LEFT: 4px; WIDTH: 500px;" EnableViewState="False" onchange="markTaskDirty();">
					</ASP:TextBox>
				</TD>
				</TR>
				<TR>
				<TD class="FieldLabel" nowrap>
					Status / Priority
				</TD>
				<TD align="left" nowrap>
					<ASP:DropDownList id="m_Status" runat="server" style="PADDING-LEFT: 4px;" Width="140px" EnableViewState="False" onchange="markTaskDirty();">
						<ASP:ListItem Value="A" Selected="True">Active</ASP:ListItem>
						<ASP:ListItem Value="D">Done</ASP:ListItem>
						<ASP:ListItem Value="S">Suspended</ASP:ListItem>
						<ASP:ListItem Value="C">Cancelled</ASP:ListItem>
					</ASP:DropDownList>
					/
					<ASP:DropDownList id="m_Priority" runat="server" style="PADDING-LEFT: 4px;" Width="140px" EnableViewState="False" onchange="markTaskDirty();">
						<ASP:ListItem Value="L">Low</ASP:ListItem>
						<ASP:ListItem Value="N" Selected="True">Normal</ASP:ListItem>
						<ASP:ListItem Value="H">High</ASP:ListItem>
					</ASP:DropDownList>
				</TD>
				</TR>
				<TR>
				<TD class="FieldLabel" nowrap>
					Due Date
				</TD>
				<TD align="left" nowrap>
					<ASP:CheckBox id="m_SetDue" runat="server" onclick="if( checked == true ) lightupNode( parentElement.getElementsByTagName( 'div' )[ 0 ] ); else disableNode( parentElement.getElementsByTagName( 'div' )[ 0 ] ); markTaskDirty();">
					</ASP:CheckBox>
					<DIV style="DISPLAY: inline;">
						<ML:DateTextBox id="m_DueDate" runat="server" style="PADDING-LEFT: 4px;" preset="date" Width="80px" HelperAlign="AbsMiddle" onchange="style.color = 'black'; markTaskDirty();" />
						@
						<ML:TimeTextBox id="m_DueTime" runat="server" />
					</DIV>
				</TD>
				<TR>
				<TD class="FieldLabel" nowrap>
					Borrower
				</TD>
				<TD align="left" nowrap>
					<ML:EncodedLabel id="m_Borrower" runat="server" EnableViewState="False" />
				</TD>
				</TR>
				<TR>
				<TD class="FieldLabel" nowrap>
					Loan Number
				</TD>
				<TD align="left" nowrap>
					<ML:EncodedLabel id="m_LoanNumber" runat="server" EnableViewState="False" />
				</TD>
				</TR>
				</TABLE>
			</TD>
			</TR>
			</TABLE>
		</TD>
		</TR>
		<TR valign="top" id="m_History">
		<TD height="100%">
			<TABLE cellpadding="0" cellspacing="6" border="0" width="100%" height="100%">
			<TR valign="top">
			<TD style="width:66%">
				<TABLE cellpadding="4" cellspacing="0" border="0" width="100%" height="100%">
				<TR class="FormTableHeader">
				<TD colspan="2" nowrap>
					History
				</TD>
				</TR>
				<TR>
				<TD align="right" height="100%" nowrap style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px;">
					<ASP:TextBox id="m_Log" runat="server" style="PADDING: 12px; FONT: 11px arial; TEXT-ALIGN: left; BACKGROUND-COLOR: gainsboro; width:100%;resize: none;overflow-y: scroll;" Height="100%" TextMode="MultiLine" ReadOnly="True" EnableViewState="False" />
				</TD>
				</TR>
				</TABLE>
			</TD>
			<TD style="width:33%">
				<TABLE cellpadding="4" cellspacing="0" border="0" width="100%" height="100%">
				<TR class="FormTableHeader">
				<TD nowrap>
					Active Participants
				</TD>
				<TD nowrap align="right">
					(
					    <A href="#" style="COLOR: tomato;" onclick="onEditParticipants(); return false;">edit list</A>
						<ASP:BUTTON id="m_editParticipants" onclick="EditParticipantsClick" runat="server" style ="display:none" />
					)
				</TD>
				</TR>
				<TR>
				<TD colspan="2" align="right" height="100%" style="PADDING-TOP: 5px; PADDING-RIGHT: 0px; PADDING-LEFT: 0px;">
					<DIV style="*PADDING: 8px; BORDER: 2px inset; BACKGROUND-COLOR: whitesmoke; OVERFLOW-X: hidden; OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 100%;">
						<ASP:DataGrid id="m_ActiveList" runat="server" CellPadding="2" CellSpacing="2" GridLines="None" Width="100%" ShowHeader="False" AutoGenerateColumns="False" EnableViewState="True">
						    <HeaderStyle BackColor="Gainsboro" Font-Bold="True" Font-Name="Arial" Font-Size="11px" />
						    <Columns>
							    <ASP:TemplateColumn HeaderText="Name">
								    <ItemTemplate>
									    <div id="Active" key='<%# AspxTools.HtmlString(Eval("UserId")) %>'>
											<%# AspxTools.HtmlString(Eval( "FirstName" ) + " " + Eval( "LastName"  )) %>
									    </div>
								    </ItemTemplate>
							    </ASP:TemplateColumn>
						    </Columns>
						</ASP:DataGrid>
					</DIV>
				</TD>
				</TR>
				</TABLE>
			</TD>
			</TR>
			</TABLE>
		</TD>
		</TR>
		<TR valign="top" id="m_Editor" style="DISPLAY: none;">
		<TD height="100%">
			<TABLE cellpadding="0" cellspacing="6" border="0" width="100%" height="100%">
			<TR valign="top">
			<TD>
				<TABLE cellpadding="4" cellspacing="0" border="0" width="100%" height="100%">
				<TR class="FormTableHeader" height="1px">
				<TD nowrap>
					Editor
				</TD>
				<TD nowrap align="right">
					(
					<A href="#" style="COLOR: tomato;" onclick="onEditHistoryLog(); return false;">
						return to history</A>
					)
				</TD>
				</TR>
				<TR>
				<TD class="FieldLabel" nowrap>
					Add
				</TD>
				<TD align="right" style="PADDING-RIGHT: 0px;" nowrap>
					<TABLE cellpadding="0" cellspacing="0" border="0" style="PADDING-TOP: 1px;">
					<TR>
					<TD>
						<DIV style="PADDING: 2px; PADDING-RIGHT: 0px; BORDER: 2px inset; BORDER-RIGHT: none; BACKGROUND-COLOR: whitesmoke; WIDTH: 110px; HEIGHT: 165px;">
							<TABLE class="groupTable">
							<TBODY>
								<tr style="FONT: bold 11px arial; BACKGROUND: gainsboro;padding:2px">
									<td>Group</td>
								</tr>
							<TR>
							<TD>
								<ASP:RadioButtonList id="m_ParticipantGroups" runat="server" CellPadding="1" CellSpacing="0" RepeatDirection="Vertical" onclick="if( retrieveEventTarget(event).value != null ) onClickParticipantGroup( retrieveEventTarget(event) );">
									<ASP:ListItem Value="*" Selected="True">Everyone</ASP:ListItem>
									<ASP:ListItem Value="m">Managers</ASP:ListItem>
									<ASP:ListItem Value="p">Processors</ASP:ListItem>
									<ASP:ListItem Value="o">Officers</ASP:ListItem>
									<ASP:ListItem Value="a">Assigned</ASP:ListItem>
									<ASP:ListItem Value="-">Quitted</ASP:ListItem>
								</ASP:RadioButtonList>
							</TD>
							</TR>
							</TBODY>
							</TABLE>
						</DIV>
					</TD>
					<TD>
						<DIV style="PADDING: 2px; PADDING-LEFT: 0px; BORDER: 2px inset; BORDER-LEFT: none; BACKGROUND-COLOR: white; OVERFLOW-X: hidden; OVERFLOW-Y: scroll; WIDTH: 440px; HEIGHT: 165px;">
							<TABLE cellpadding="0" cellspacing="0" width="100%">
							<TR group="*">
							<TD>
								<ASP:DataGrid id="m_EmployeesAll" runat="server" CellPadding="2" CellSpacing="2" GridLines="None" Width="100%" AutoGenerateColumns="False" EnableViewState="True">
									<HeaderStyle BackColor="Gainsboro" Font-Bold="True" Font-Name="Arial" Font-Size="11px" />
									<Columns>
										<ASP:TemplateColumn HeaderText="Name">
											<ItemTemplate>
												<%# AspxTools.HtmlString(Eval( "FirstName" ) + " " + Eval( "LastName"  )) %>
											</ItemTemplate>
										</ASP:TemplateColumn>
										<ASP:BoundColumn HeaderText="Role" DataField="Role" />
										<ASP:TemplateColumn HeaderText="<a href='#' onclick='onAppendEntireGroup( this );'>add group</a>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
											<ItemTemplate>
												<A href="#"
													userid='<%# AspxTools.HtmlString(Eval( "UserId" )) %>'
													firstname='<%# AspxTools.HtmlString(Eval( "FirstName" )) %>'
													lastname='<%# AspxTools.HtmlString(Eval( "LastName" )) %>'
													onclick="addParticipantToActiveGrid(
														<%# AspxTools.ClientId(m_ActiveBase) %>,
														<%= AspxTools.ClientId(m_ActiveList) %>,
														<%# AspxTools.ClientId(m_ActiveInfo) %>,
														this.getAttribute('userid'),
														this.getAttribute('firstname'),
														this.getAttribute('lastname')); return false;">add</A>
											</ItemTemplate>
										</ASP:TemplateColumn>
									</Columns>
								</ASP:DataGrid>
							</TD>
							</TR>
							<TR group="m" style="DISPLAY: none;">
							<TD>
								<ASP:DataGrid id="m_EmployeesMgr" runat="server" CellPadding="2" CellSpacing="2" GridLines="None" Width="100%" AutoGenerateColumns="False" EnableViewState="True">
									<HeaderStyle BackColor="Gainsboro" Font-Bold="True" Font-Name="Arial" Font-Size="11px" />
									<Columns>
										<ASP:TemplateColumn HeaderText="Name">
											<ItemTemplate>
												<%# AspxTools.HtmlString(Eval( "FirstName" ) + " " + Eval( "LastName"  )) %>
											</ItemTemplate>
										</ASP:TemplateColumn>
										<ASP:TemplateColumn HeaderText="<a href='#' onclick='onAppendEntireGroup( this );'>add group</a>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
											<ItemTemplate>
												<A href="#"
													userid='<%# AspxTools.HtmlString(Eval( "UserId" )) %>'
													firstname='<%# AspxTools.HtmlString(Eval( "FirstName" )) %>'
													lastname='<%# AspxTools.HtmlString(Eval( "LastName" )) %>'
													onclick="addParticipantToActiveGrid(
														<%# AspxTools.ClientId(m_ActiveBase) %>,
														<%= AspxTools.ClientId(m_ActiveList) %>,
														<%# AspxTools.ClientId(m_ActiveInfo) %>,
														this.getAttribute('userid'),
														this.getAttribute('firstname'),
														this.getAttribute('lastname')); return false;">add</A>
											</ItemTemplate>
										</ASP:TemplateColumn>
									</Columns>
								</ASP:DataGrid>
							</TD>
							</TR>
							<TR group="p" style="DISPLAY: none;">
							<TD>
								<ASP:DataGrid id="m_EmployeesPro" runat="server" CellPadding="2" CellSpacing="2" GridLines="None" Width="100%" AutoGenerateColumns="False" EnableViewState="True">
									<HeaderStyle BackColor="Gainsboro" Font-Bold="True" Font-Name="Arial" Font-Size="11px" />
									<Columns>
										<ASP:TemplateColumn HeaderText="Name">
											<ItemTemplate>
												<%# AspxTools.HtmlString(Eval( "FirstName" ) + " " + Eval( "LastName"  )) %>
											</ItemTemplate>
										</ASP:TemplateColumn>
										<ASP:TemplateColumn HeaderText="<a href='#' onclick='onAppendEntireGroup( this );'>add group</a>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
											<ItemTemplate>
												<A href="#"
													userid='<%# AspxTools.HtmlString(Eval( "UserId" )) %>'
													firstname='<%# AspxTools.HtmlString(Eval( "FirstName" )) %>'
													lastname='<%# AspxTools.HtmlString(Eval( "LastName" )) %>'
													onclick="addParticipantToActiveGrid(
														<%# AspxTools.ClientId(m_ActiveBase) %>,
														<%= AspxTools.ClientId(m_ActiveList) %>,
														<%# AspxTools.ClientId(m_ActiveInfo) %>,
														this.getAttribute('userid'),
														this.getAttribute('firstname'),
														this.getAttribute('lastname')); return false;">add</A>
											</ItemTemplate>
										</ASP:TemplateColumn>
									</Columns>
								</ASP:DataGrid>
							</TD>
							</TR>
							<TR group="o" style="DISPLAY: none;">
							<TD>
								<ASP:DataGrid id="m_EmployeesOff" runat="server" CellPadding="2" CellSpacing="2" GridLines="None" Width="100%" AutoGenerateColumns="False" EnableViewState="True">
									<HeaderStyle BackColor="Gainsboro" Font-Bold="True" Font-Name="Arial" Font-Size="11px" />
									<Columns>
										<ASP:TemplateColumn HeaderText="Name">
											<ItemTemplate>
												<%# AspxTools.HtmlString(Eval( "FirstName" ) + " " + Eval( "LastName"  )) %>
											</ItemTemplate>
										</ASP:TemplateColumn>
										<ASP:TemplateColumn HeaderText="<a href='#' onclick='onAppendEntireGroup( this );'>add group</a>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
											<ItemTemplate>
												<A href="#"
													userid='<%# AspxTools.HtmlString(Eval( "UserId" )) %>'
													firstname='<%# AspxTools.HtmlString(Eval( "FirstName" )) %>'
													lastname='<%# AspxTools.HtmlString(Eval( "LastName" )) %>'
													onclick="addParticipantToActiveGrid(
														<%# AspxTools.ClientId(m_ActiveBase) %>,
														<%= AspxTools.ClientId(m_ActiveList) %>,
														<%# AspxTools.ClientId(m_ActiveInfo) %>,
														this.getAttribute('userid'),
														this.getAttribute('firstname'),
														this.getAttribute('lastname')); return false;">add</A>
											</ItemTemplate>
										</ASP:TemplateColumn>
									</Columns>
								</ASP:DataGrid>
							</TD>
							</TR>
							<TR group="a" style="DISPLAY: none;">
							<TD>
								<ASP:DataGrid id="m_EmployeesAsg" runat="server" CellPadding="2" CellSpacing="2" GridLines="None" Width="100%" AutoGenerateColumns="False" EnableViewState="True">
									<HeaderStyle BackColor="Gainsboro" Font-Bold="True" Font-Name="Arial" Font-Size="11px" />
									<Columns>
										<ASP:TemplateColumn HeaderText="Name">
											<ItemTemplate>
												<%# AspxTools.HtmlString(Eval( "FirstName" ) + " " + Eval( "LastName"  )) %>
											</ItemTemplate>
										</ASP:TemplateColumn>
										<ASP:BoundColumn HeaderText="Role" DataField="Role" />
										<ASP:TemplateColumn HeaderText="<a href='#' onclick='onAppendEntireGroup( this );'>add group</a>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
											<ItemTemplate>
												<A href="#"
													userid='<%# AspxTools.HtmlString(Eval( "UserId" )) %>'
													firstname='<%# AspxTools.HtmlString(Eval( "FirstName" )) %>'
													lastname='<%# AspxTools.HtmlString(Eval( "LastName" )) %>'
													onclick="addParticipantToActiveGrid(
														<%# AspxTools.ClientId(m_ActiveBase) %>,
														<%= AspxTools.ClientId(m_ActiveList) %>,
														<%# AspxTools.ClientId(m_ActiveInfo) %>,
														this.getAttribute('userid'),
														this.getAttribute('firstname'),
														this.getAttribute('lastname')); return false;">add</A>
											</ItemTemplate>
										</ASP:TemplateColumn>
									</Columns>
								</ASP:DataGrid>
							</TD>
							</TR>
							<TR group="-">
							<TD>
								<ASP:DataGrid id="m_EmployeesPre" runat="server" CellPadding="2" CellSpacing="2" GridLines="None" Width="100%" AutoGenerateColumns="False" EnableViewState="True">
									<HeaderStyle BackColor="Gainsboro" Font-Bold="True" Font-Name="Arial" Font-Size="11px"/>
									<Columns>
										<ASP:TemplateColumn HeaderText="Name">
											<ItemTemplate>
												<%# AspxTools.HtmlString(Eval( "FirstName" ) + " " + Eval( "LastName"  )) %>
											</ItemTemplate>
										</ASP:TemplateColumn>
										<ASP:BoundColumn HeaderText="Role" DataField="Role" />
										<ASP:TemplateColumn HeaderText="<a href='#' onclick='onAppendEntireGroup( this );'>add group</a>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
											<ItemTemplate>
												<A href="#"
													userid='<%# AspxTools.HtmlString(Eval( "UserId" )) %>'
													firstname='<%# AspxTools.HtmlString(Eval( "FirstName" )) %>'
													lastname='<%# AspxTools.HtmlString(Eval( "LastName" )) %>'
													onclick="addParticipantToActiveGrid(
														<%# AspxTools.ClientId(m_ActiveBase) %>,
														<%= AspxTools.ClientId(m_ActiveList) %>,
														<%# AspxTools.ClientId(m_ActiveInfo) %>,
														this.getAttribute('userid'),
														this.getAttribute('firstname'),
														this.getAttribute('lastname')); return false;">add</A>
											</ItemTemplate>
										</ASP:TemplateColumn>
									</Columns>
								</ASP:DataGrid>
							</TD>
							</TR>
							</TBODY>
							</TABLE>
						</DIV>
					</TD>
					</TR>
					</TABLE>
					<SCRIPT>
						function onClickParticipantGroup( oOption ) { try
						{
							if( oOption == null || oOption.value == null )
							{
								return;
							}

							var i , objs , root = oOption;

							while( root.tagName.toLowerCase() != "table" )
							{
								root = root.parentElement;
							}

							root = root.parentElement;

							while( root.tagName.toLowerCase() != "table" )
							{
								root = root.parentElement;
							}

							root = root.parentElement;

							while( root.tagName.toLowerCase() != "table" )
							{
								root = root.parentElement;
							}

							for( i = 0 , objs = root.getElementsByTagName( "tr" ) ; i < objs.length ; ++i )
							{
								if( objs[ i ].group == null )
								{
									continue;
								}

								if( objs[ i ].group == oOption.value )
								{
									objs[ i ].style.display = "";
								}
								else
								{
									objs[ i ].style.display = "none";
								}
							}
						}
						catch( e )
						{
							alert( e.message );
						}}

						function onAppendEntireGroup( oGroup ) { try
						{
							while( oGroup != null && oGroup.tagName.toLowerCase() != "table" )
							{
								oGroup = oGroup.parentElement;
							}

							if( oGroup == null )
							{
								return;
							}

							var i;

							for( i = 0 ; i < oGroup.rows.length ; ++i )
							{
								if( oGroup.rows[ i ].cells.length == 0 )
								{
									continue;
								}

								var c = oGroup.rows[ i ].cells[ oGroup.rows[ i ].cells.length - 1 ];

								if( c.tagName.toLowerCase() == "td" )
								{
									if( c.children.length > 0 && c.children[ 0 ].tagName.toLowerCase() == "a" )
									{
										c.children[ 0 ].click();
									}
								}
							}
						}
						catch( e )
						{
							alert( e.message );
						}}
					</SCRIPT>
				</TD>
				</TR>
				<TR height="100%">
				<TD class="FieldLabel" nowrap>
					Active
				</TD>
				<TD align="right" style="PADDING-RIGHT: 0px;" nowrap>
					<INPUT type="hidden" id="m_ActiveInfo" runat="server" value="">
					<ASP:Panel id="m_ActiveBase" runat="server" style="PADDING: 2px; BORDER: 2px inset; BACKGROUND-COLOR: white; OVERFLOW-X: hidden; OVERFLOW-Y: scroll; WIDTH: 550px; HEIGHT: 100%;">
						<ASP:DataGrid id="m_ActiveGrid" runat="server" CellPadding="2" CellSpacing="2" GridLines="None" Width="100%" AutoGenerateColumns="False" EnableViewState="True">
						<HeaderStyle BackColor="Gainsboro" Font-Bold="True" Font-Name="Arial" Font-Size="11px">
						</HeaderStyle>
						<Columns>
							<ASP:TemplateColumn HeaderText="Track?">
								<ItemTemplate>
									<INPUT type="checkbox"
										userid='<%# AspxTools.HtmlString(Eval( "UserId" )) %>'
										onclick="checkParticipantTrackingOption(
											<%= AspxTools.ClientId(m_ActiveBase) %>,
											<%= AspxTools.ClientId(m_ActiveInfo) %>,
											this.getAttribute('userid'), this.checked);"
										<%# AspxTools.HtmlString((bool)DataBinder.Eval( Container.DataItem , "IsTracked" ) ? "checked" : "") %>
										>
								</ItemTemplate>
							</ASP:TemplateColumn>
							<ASP:TemplateColumn HeaderText="Name">
								<ItemTemplate>
									<SPAN id="Participant" key='<%# AspxTools.HtmlString(Eval( "UserId" )) %>'>
											<%# AspxTools.HtmlString(Eval( "FirstName" ) + " " + Eval( "LastName"  )) %>
									</SPAN>
								</ItemTemplate>
							</ASP:TemplateColumn>
							<ASP:TemplateColumn HeaderText="<a href='#' onclick='onRemoveEntireGroup( this );'>remove all</a>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
								<ItemTemplate>
									<A href="#"
                                        userid='<%# AspxTools.HtmlString(Eval( "UserId" )) %>'
                                        onclick="removeParticipantFromActiveGrid(
											<%= AspxTools.ClientId(m_ActiveBase) %>,
											<%= AspxTools.ClientId(m_ActiveList) %>,
											<%= AspxTools.ClientId(m_ActiveInfo) %>,
											this.getAttribute('userid'));">remove</A>
								</ItemTemplate>
							</ASP:TemplateColumn>
						</Columns>
						</ASP:DataGrid>
					</ASP:Panel>
					<SCRIPT>
						function addParticipantToActiveGrid( oBase , oList , oStore , sUserId , sFirstName , sLastName ) { try
						{
							if( oBase == null || oStore == null || sUserId == null || sFirstName == null || sLastName == null )
							{
								return;
							}

							var i , list = oStore.value.split( "," );

							for( i = 0 ; i < list.length ; ++i )
							{
								var userid , item = list[ i ].split( ":" );

								userid = item[ 0 ];

								if( userid.toLowerCase() == sUserId.toLowerCase() )
								{
									return;
								}
							}

							var grid = oBase.getElementsByTagName( 'table' )[ 0 ];

							if( grid == null )
							{
								return;
							}

							var user = grid.insertRow();

							if( user == null )
							{
								return;
							}

							user.insertCell();
							var h = hypescriptDom;
							user.cells[ 0 ].innerHTML = "";
							user.cells[ 0 ].appendChild(h("input", {type:"checkbox", attrs:{userid:sUserId}, 
								onchange:function(){ 
									checkParticipantTrackingOption( <%= AspxTools.ClientId(m_ActiveBase) %>, <%= AspxTools.ClientId(m_ActiveInfo) %>, sUserId, this.checked ); 
								}}));
							user.cells[ 0 ].align     = "left";

							user.insertCell();

							user.cells[ 1 ].innerHTML = "";
							user.cells[ 1 ].appendChild(h("span", {id:"Participant", attrs:{key:sUserId}}, sFirstName + " " + sLastName));
							user.cells[ 1 ].align     = "left";

							if( grid.rows[ 0 ].cells.length > 2 )
							{
								user.insertCell();

								user.cells[ 2 ].innerHTML = "";
								user.cells[ 2 ].appendChild(h("a", {href:"#", attrs:{userid:sUserId}, 
									onclick:function(){
										removeParticipantFromActiveGrid(<%= AspxTools.ClientId(m_ActiveBase) %>, <%= AspxTools.ClientId(m_ActiveList) %>, <%= AspxTools.ClientId(m_ActiveInfo) %>, sUserId)
									}}, "remove"));
								user.cells[ 2 ].align     = "right";
							}

							user.scrollIntoView();

							var actv = oList.insertRow();

							if( actv != null )
							{
								actv.insertCell();

								actv.cells[ 0 ].innerHTML = "<DIV id=\"Active\" key=\"" + sUserId + "\">" + sFirstName + " " + sLastName + "</DIV>";
								actv.cells[ 0 ].align     = "left";
							}

							actv.scrollIntoView();

							if( oStore.value != "" )
							{
								oStore.value += ","
							}

							oStore.value += sUserId + ":" + sFirstName + ":" + sLastName + ":N";

							markTaskDirty();
						}
						catch( e )
						{
							alert( e.message );
						}}

						function checkParticipantTrackingOption( oBase , oStore , sUserId , bChecked ) { try
						{
							if( oBase == null || oStore == null || sUserId == null || bChecked == null )
							{
								return;
							}

							var i , list = oStore.value.split( "," );

							for( i = 0 ; i < list.length ; ++i )
							{
								var user , fsnm , lsnm , item = list[ i ].split( ":" );

								user = item[ 0 ];
								fsnm = item[ 1 ];
								lsnm = item[ 2 ];

								if( user.toLowerCase() == sUserId.toLowerCase() )
								{
									if( bChecked == true )
									{
										oStore.value = oStore.value.replace( list[ i ] , user + ":" + fsnm + ":" + lsnm + ":Y" );
									}
									else
									{
										oStore.value = oStore.value.replace( list[ i ] , user + ":" + fsnm + ":" + lsnm + ":N" );
									}

									break;
								}
							}

							markTaskDirty();
						}
						catch( e )
						{
							alert( e.message );
						}}

						function removeParticipantFromActiveGrid( oBase , oList , oStore , sUserId ) { try
						{
							if( oBase == null || oList == null || oStore == null || sUserId == null )
							{
								return;
							}

							var i , list = oStore.value.split( "," );

							for( i = 0 ; i < list.length ; ++i )
							{
								var user , trkd , item = list[ i ].split( ":" );

								user = item[ 0 ];
								trkd = item[ 3 ];

								if( user.toLowerCase() == sUserId.toLowerCase() )
								{
									if( i > 0 )
									{
										oStore.value = oStore.value.replace( "," + list[ i ] , "" );
									}
									else
									{
										oStore.value = oStore.value.replace( list[ i ] , "" );
									}

									break;
								}
							}

							var grid = oBase.getElementsByTagName( "table" )[ 0 ];

							if( grid == null )
							{
								return;
							}

							for( i = 0 ; i < grid.rows.length ; ++i )
							{
								var row = grid.rows[ i ];

								var Participant = getElementByIdFromParent(row, "Participant" );

								if( Participant == null || Participant.getAttribute('key').toLowerCase() != sUserId.toLowerCase() )
								{
									continue;
								}

								row.remove ? row.remove() : row.removeNode( true );

								break;
							}

							for( i = 0 ; i < oList.rows.length ; ++i )
							{
								var row = oList.rows[ i ];

								var Active = getElementByIdFromParent(row, "Active");
								if( Active == null || Active.getAttribute('key').toLowerCase() != sUserId.toLowerCase() )
								{
									continue;
								}

								row.remove ? row.remove() : row.removeNode( true );

								break;
							}

							markTaskDirty();
						}
						catch( e )
						{
							alert( e.message );
						}}

						function onRemoveEntireGroup( oGroup ) { try
						{
							while( oGroup != null && oGroup.tagName.toLowerCase() != "table" )
							{
								oGroup = oGroup.parentElement;
							}

							if( oGroup == null )
							{
								return;
							}

							var i = oGroup.rows.length;

							while( --i >= 0 )
							{
								if( oGroup.rows[ i ].cells.length == 0 )
								{
									continue;
								}

								var c = oGroup.rows[ i ].cells[ oGroup.rows[ i ].cells.length - 1 ];

								if( c.tagName.toLowerCase() == "td" )
								{
									if( c.children.length > 0 && c.children[ 0 ].tagName.toLowerCase() == "a" )
									{
										c.children[ 0 ].click();
									}
								}
							}
						}
						catch( e )
						{
							alert( e.message );
						}}
					</SCRIPT>
				</TD>
				</TR>
				</TABLE>
			</TD>
			</TR>
			</TABLE>
		</TD>
		</TR>
		<TR valign="top" id="m_Options">
		<TD height="1px">
			<TABLE cellpadding="0" cellspacing="6" border="0" width="100%">
			<TR valign="top">
			<TD style="width:66%;">
				<TABLE cellpadding="4" cellspacing="0" border="0" width="100%">
				<TR class="FormTableHeader">
				<TD colspan="2" style="PADDING: 4px;" nowrap>
					Add to the Log
				</TD>
				</TR>
				<TR style="PADDING-TOP: 4px;">
				<TD class="FieldLabel" valign=top style="width:80px;">
					New Notes
				</TD>
				<TD align=right nowrap>
					<ASP:TextBox id="m_Add" runat="server" style="PADDING: 4px; FONT: 11px arial; width:100%;resize: none;overflow-y: scroll;" Height="90px" TextMode="MultiLine" EnableViewState="False" onchange="markTaskDirty();">
					</ASP:TextBox>
				</TD>
				</TR>
				</TABLE>
			</TD>
			<TD style="width:33%;">
				<TABLE cellpadding="4" cellspacing="0" border="0" width="100%">
				<TR class="FormTableHeader">
				<TD colspan="2" nowrap>
					Your Private Options
				</TD>
				</TR>
				<!--TR>
				<TD class="FieldLabel" nowrap>
					Warn
				</TD>
				<TD align="left" nowrap>
					<ASP:CheckBox id="m_SetWarn" runat="server" onclick="if( checked == true ) lightupNode( parentElement.children[ 1 ] ); else disableNode( parentElement.children[ 1 ] ); markTaskDirty();">
					</ASP:CheckBox>
					<DIV style="DISPLAY: inline;">
						<ML:DateTextBox id="m_WarnDate" runat="server" style="PADDING-LEFT: 4px;" preset="date" Width="82px" HelperAlign="AbsMiddle" onchange="style.color = 'black'; markTaskDirty();">
						</ML:DateTextBox>
						@
						<ML:TimeTextBox id="m_WarnTime" runat="server">
						</ML:TimeTextBox>
					</DIV>
				</TD>
				</TR-->
				<TR>
				<TD class="FieldLabel" nowrap>
					Options
				</TD>
				<TD align="left" nowrap>
					<DIV>
						<ASP:CheckBox id="m_MarkAsNotRead" runat="server" onchange="markTaskDirty();">
						</ASP:CheckBox>
						Mark as unread
					</DIV>
					<!--DIV>
						<ASP:RadioButton id="m_KeepMonitor" runat="server" GroupName="Monitor" onchange="markTaskDirty();">
						</ASP:RadioButton>
						Keep alerting me in the monitor
					</DIV>
					<DIV>
						<ASP:RadioButton id="m_Acknowledge" runat="server" GroupName="Monitor" onchange="markTaskDirty();">
						</ASP:RadioButton>
						Acknowledge this task
					</DIV-->
				</TD>
				</TR>
				</TABLE>
			</TD>
			</TR>
			</TABLE>
		</TD>
		</TR>
		<TR>
		<TD align="center" height="1px" style="PADDING: 8px; PADDING-TOP: 0px;">
			<HR>
			<table cellspacing="0" cellpadding="0">
			<tr>
			<td class="FieldLabel">
				Quit this task?
			</td>
			<td>
				<asp:CheckBox id="m_QuitOnSubmit" runat="server">
				</asp:CheckBox>
			</td>
			<td>
				<asp:Button id="m_Submit" runat="server" style="DISPLAY: none;" Text="OK" Width="80px" OnClick="SubmitClick" />
				<INPUT type="button" value="OK" style="WIDTH: 80px;" onclick="this.disabled = true; parentElement.children[ 0 ].click();">
				<INPUT type="button" value="Cancel" style="WIDTH: 80px;" onclick="onClose();">
			</td>
			</tr>
			</table>
		</TD>
		</TR>
		</TABLE>
	</FORM>
	<UC:CModalDlg runat="server" />
</BODY>
</HTML>
