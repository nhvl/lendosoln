using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for BlockTreeUserControl.
	/// </summary>

	public partial  class BlockTreeUserControl : System.Web.UI.UserControl , INamingContainer
	{
        private LendersOffice.QueryProcessor.BlockTreeNode       m_Tree;

        private String m_Indent = "";

        public BlockTreeNode DataSource
        {
            // Access member.

            set
            {
                m_Tree = value;
            }
        }

        public String Indent
        {
            // Access member.

            set
            {
                if( value != null )
                {
                    m_Indent = value.TrimWhitespaceAndBOM();
                }
                else
                {
                    m_Indent = null;
                }
            }
            get
            {
                return m_Indent;
            }
        }

        /// <summary>
        /// Bind the current tree to our placeholder as
        /// a set of nested divs.
        /// </summary>

        public override void DataBind()
        {
            // Recursively bind the tree.

            m_Holder.Controls.Clear();

            if( m_Tree != null )
            {
                Bind( m_Holder , m_Tree , false , true );
            }
        }

        private void Bind( Control cControl , BlockTreeNode tNode , Boolean bIsTail , Boolean bIsRoot )
        {
            // Start by validating the input.

            if( cControl == null || tNode == null )
            {
                return;
            }

            // Add a new div child to the parent control
            // and populate it with the node's text and
            // specified data (as an attribute).

            HtmlContainerControl item = new HtmlGenericControl( "DIV" );

            if( bIsRoot == false )
            {
                if( m_Indent != null && m_Indent.Length > 0 )
                {
                    item.Attributes.Add( "style" , "WIDTH: 100%; MARGIN-LEFT: " + m_Indent + ";" );
                }
                else
                {
                    item.Attributes.Add( "style" , "WIDTH: 100%; MARGIN-LEFT: 16px;" );
                }
            }

            item.Attributes.Add( "nowrap" , "" );

            // Add some tree decoration.  We should provide a
            // designer supplied text mechanism to specify this
            // decoration.

            HtmlContainerControl mark = new HtmlGenericControl( "span" );

            mark.Attributes.Add( "style" , "TEXT-ALIGN: right; VERTICAL-ALIGN: top; FONT: 8pt arial;" );

            if( bIsRoot == true )
            {
                mark.InnerHtml = "&#x250c;&#x25a0;";
            }
            else
            if( bIsTail == true )
            {
                mark.InnerHtml = "&#x2514;&#x25a0;";
            }
            else
            {
                mark.InnerHtml = "&#x251c;&#x25a0;";
            }

            item.Controls.Add( mark );

            // We place the template items in their own
            // container to keep things sane.

            HtmlContainerControl span = new HtmlGenericControl( "span" );

            span.Attributes.Add( "style" , "DISPLAY: inline;" );

            if( tNode.Key != null )
            {
                span.Attributes.Add( "key" , tNode.Key );
                span.Attributes.Add( "tid" , "nodeBox" );
                span.Attributes.Add("class", "node-element");
            }

            if( tNode.Selectable == true )
            {
                span.Attributes.Add("class", "node-element");
                span.Attributes.Add("onclick", "treeNodeClick(event);");
            }

            if( tNode.Text != null && tNode.Text.Length > 0 )
            {
                span.InnerHtml = tNode.Text;
            }

            item.Controls.Add( span );

            // Now add the children after the custom.

            int b = 1;

            foreach( BlockTreeNode block in tNode )
            {
                if( b < tNode.Count )
                {
                    Bind( item , block , false , false );
                }
                else
                {
                    Bind( item , block , true , false );
                }

                ++b;
            }

            // Add the child control to the parent.

            cControl.Controls.Add( item );
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion

        /// <summary>
        /// Construct default.
        /// </summary>

        public BlockTreeUserControl()
        {
            // Initialize members.

            m_Tree = null;
        }

	}

    /// <summary>
    /// Every entry in the tree is a tree node.
    /// </summary>

    public class BlockTreeNode
    {
        /// <summary>
        /// Every entry in the tree is a tree node.
        /// </summary>

        private String         m_Key; // inner data to associate with div
        private String        m_Text; // inner text to associate with div
        private Boolean m_Selectable; // does the node accept selections
        private ArrayList m_Children; // child nodes to nest within this

        public Boolean Selectable
        {
            // Access member.

            set
            {
                m_Selectable = value;
            }
            get
            {
                return m_Selectable;
            }
        }

        public String Key
        {
            // Access member.

            set
            {
                m_Key = value;
            }
            get
            {
                return m_Key;
            }
        }

        public String Text
        {
            // Access member.

            set
            {
                m_Text = value;
            }
            get
            {
                return m_Text;
            }
        }

        public int Count
        {
            // Access member.

            get
            {
                return m_Children.Count;
            }
        }

        /// <summary>
        /// Add tree node to our children set.
        /// </summary>
        /// <param name="tNode">
        /// Child to nest within this node.
        /// </param>

        public void Add( BlockTreeNode tNode )
        {
            // Add node to our children set.

            if( m_Children.Contains( tNode ) == false && tNode != null )
            {
                m_Children.Add( tNode );
            }
        }

        /// <summary>
        /// Expose enumeration interface.
        /// </summary>
        /// <returns>
        /// Enumerator to walk with.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Expose enumeration interface.

            return m_Children.GetEnumerator();
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public BlockTreeNode()
        {
            // Initialize members.

            m_Children = new ArrayList();

            m_Selectable = false;

            m_Text = "";

            m_Key = "";
        }

    }

}
