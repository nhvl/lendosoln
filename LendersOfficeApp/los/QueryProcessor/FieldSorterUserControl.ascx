<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FieldSorterUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.FieldSorterUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ASP:Panel id="m_Base" runat="server" style="PADDING-BOTTOM: 0px; PADDING-TOP: 0px">
<script>
    function chooseSortItem( oContainer , sFieldId ) { try
    {
        // Validate object set.

        if( oContainer == null || sFieldId == null )
        {
            return;
        }

		// Delegate to all listening handlers.

        if( fireReportEvent != null )
        {
            fireReportEvent( "select" , sFieldId , oContainer );
        }
	}
	catch( e )
	{
		window.status = e.message;
	}}

    function selectSortItem( oContainer , oStore , sFieldId ) { try
    {
        // Validate object set.

        if( oContainer == null || oStore == null || sFieldId == null )
        {
            return;
        }

        // Update the current selection identifier
        // and then color the background.

        var i;
        var all = oContainer.getElementsByTagName("*");
        for( i = 0 ; i < all.length ; ++i )
        {
            var curr = all[i];

            if( curr == null || curr.getAttribute('kind') == null || curr.getAttribute('kind') != "item" )
            {
                continue;
            }

            if( curr.getAttribute('key') == sFieldId )
            {
                curr.className = "FieldSorterMarkItem";
                oStore.value   = sFieldId;
            }
            else
            {
                curr.className = "FieldSorterListItem";
            }
        }

        // Raise the event for other controls.

        if( fireReportEvent != null )
        {
            fireReportEvent( "select" , sFieldId , oContainer );
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function shiftSortItemUp( oContainer , oStore , sFieldId ) { try
    {
        // Validate the container and field.
        var all = oContainer.getElementsByTagName("*");
        if(  oStore.value == null || sFieldId == null )
        {
            return;
        }

        // Walk the children and swap the previous
        // entry with the one that matches.

        var i , prev = null;

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a sort list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                if( prev != null && curr.getAttribute('key') == sFieldId )
                {
                    // Found a match.  Swap with
                    // the previous child.

                    swapNodePoly(curr, prev);

                    break;
                }

                prev = curr;
            }
        }

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a groupby list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                // Push the entry onto the end.

				var s , f , item = "";

				s = oStore.value.indexOf( "-" + curr.getAttribute('key') + "+" );

				f = oStore.value.indexOf( "-" , s + 1 );

				if( s != -1 && f > s )
				{
					item = oStore.value.substr( s , f - s + 1 );
				}

                oStore.value = oStore.value.replace( item , "" );
                oStore.value = oStore.value + item;
            }
        }

        // Raise the event for other controls.

        if( fireReportEvent != null )
        {
            fireReportEvent( "sortup" , sFieldId , oContainer );
        }

        // We now have a dirty report.

        if( markReportAsDirty != null )
        {
            markReportAsDirty();
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function shiftSortItemDn( oContainer , oStore , sFieldId ) { try
    {
        // Validate the container and field.
        var all = oContainer.getElementsByTagName("*");
        if( all.length == 0 || oStore.value == null || sFieldId == null )
        {
            return;
        }

        // Walk the children and swap the current
        // entry with the previous that matches.

        var i , prev = null;

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a sort list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                if( prev != null && prev.getAttribute('key') == sFieldId )
                {
                    // Found a match.  Swap with
                    // the current child.

                    swapNodePoly(prev, curr);

                    break;
                }

                prev = curr;
            }
        }

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a sort list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                // Push the entry onto the end.

				var s , f , item = "";

				s = oStore.value.indexOf( "-" + curr.getAttribute('key') + "+" );

				f = oStore.value.indexOf( "-" , s + 1 );

				if( s != -1 && f > s )
				{
					item = oStore.value.substr( s , f - s + 1 );
				}

                oStore.value = oStore.value.replace( item , "" );
                oStore.value = oStore.value + item;
            }
        }

        // Raise the event for other controls.

        if( fireReportEvent != null )
        {
            fireReportEvent( "sortdn" , sFieldId , oContainer );
        }

        // We now have a dirty report.

        if( markReportAsDirty() != null )
        {
            markReportAsDirty();
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function removeSortItem( oContainer , oStore , sFieldId ) { try
    {
        // Validate the container and field.
        var all = oContainer.getElementsByTagName("*");
        if( oStore.value == null || sFieldId == null )
        {
            return;
        }

        // Walk the children and swap the current
        // entry with the previous that matches.

        var i;

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a sort list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                if( curr.getAttribute('key') == sFieldId )
                {
					var s , f , item = "";

					s = oStore.value.indexOf( "-" + curr.getAttribute('key') + "+" );

					f = oStore.value.indexOf( "-" , s + 1 );

					if( s != -1 && f > s )
					{
						item = oStore.value.substr( s , f - s + 1 );
					}

					oStore.value = oStore.value.replace( item , "" );

                    curr.remove ? curr.remove() : curr.removeNode( true );

                    break;
                }
            }
        }

        // We now have a dirty report.

        if( markReportAsDirty() != null )
        {
            markReportAsDirty();
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function clickSortItemOption( oContainer , oStore , sFieldId ) { try
    {
		// Validate the container and field.
        var all = oContainer.getElementsByTagName("*");
        if( oStore.value == null || sFieldId == null )
        {
            return;
        }

        // Walk the children and update the status
        // of the checkbox based on our current
        // state -- we toggle.

        var i;

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a sort list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                if( curr.getAttribute('key') == sFieldId )
                {
					var s , f , item = "";

					s = oStore.value.indexOf( "-" + curr.getAttribute('key') + "+" );

					f = oStore.value.indexOf( "-" , s + 1 );

					if( s != -1 && f > s )
					{
						item = oStore.value.substr( s , f - s + 1 );
					}

					if( item == ( "-" + curr.getAttribute('key') + "+0-" ) )
					{
						oStore.value = oStore.value.replace( item , "-" + curr.getAttribute('key') + "+1-" );

						curr.children[ 3 ].className = "FieldSorterCommand FieldSorterChecked";
					}
					else
					if( item == ( "-" + curr.getAttribute('key') + "+1-" ) )
					{
						oStore.value = oStore.value.replace( item , "-" + curr.getAttribute('key') + "+0-" );

						curr.children[ 3 ].className = "FieldSorterCommand";
					}

                    break;
                }
            }
        }

        // We now have a dirty report.

        if( markReportAsDirty != null )
        {
            markReportAsDirty();
        }
	}
	catch( e )
	{
        window.status = e.message;
    }}

    function updateSortItem( oContainer , oStore , sFieldArg ) { try
    {
        // Validate the container and field.
        var all = oContainer.getElementsByTagName("*");
        if( oStore.value == null || sFieldArg == null )
        {
            return;
        }

        // Break the function into parts and
        // see if we're adding or removing.
        var parts;
        if (typeof (stringSplitRemainder) != 'undefined')
        {
            parts = stringSplitRemainder(sFieldArg, ":", 2);
        }
        else
        {
            parts = sFieldArg.split( ":" );
        }

        if( parts.length != 2 )
        {
			return;
		}

        if( oStore.value.indexOf( "-" + parts[ 0 ] + "+" ) == -1 )
        {
            // We need to add a new sorting item.  This is
            // usually a postback event.  We handle it here
            // to experiment and get us one step closer to
            // a postback-less ui.

			var name , id;

			id   = parts[ 0 ];
			name = parts[ 1 ];
            
            var h = hypescriptDom;
            oContainer.appendChild(h("div", {attrs:{kind:"item", key:id}, className:"FieldSorterListItem"}, [
                h("a", {className:"FieldSorterLinkCommand", attrs:{key:id}, onclick:function(){
                    this.className = 'FieldSorterLinkCommand';
                    shiftSortItemUp(oContainer, oStore, id);
                }, innerHTML:"&#x25b2;"}),
                h("a", {className:"FieldSorterLinkCommand", attrs:{key:id}, onclick:function(){
                    this.className = 'FieldSorterLinkCommand';
                    shiftSortItemDn(oContainer, oStore, id);
                }, innerHTML:"&#x25bc;"}),
                h("div", {className:"FieldSorterLabel", attrs:{key:id}, onclick:function(){
                    chooseSortItem(oContainer, id);
                }}, name),
                h("div", {className:"FieldSorterCommand", attrs:{key:id}, onclick:function(){
                    clickSortItemOption(oContainer, oStore, id);
                    blur();
                }}),
            ]));
            oStore.value += "-" + parts[ 0 ] + "+0-";
        }
        else
        {
            // Remove the item from our grouping.  We don't
            // need to propagate an event because interested
            // users can key off of the click event.  If we
            // need to know when grouped columns are dropped
            // from this control, then add the firing of the
            // event here.

			var i , s , f;

			for( i = 0 ; i < all.length ; ++i )
			{
				// Get current child and check if it is
				// a sort list item.

				var curr = all[i];

				if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
				{
					if( curr.getAttribute('key') == parts[ 0 ] )
					{
						curr.remove ? curr.remove() : curr.removeNode( true );

						break;
					}
				}
			}

			s = oStore.value.indexOf( "-" + parts[ 0 ] + "+" );

			f = oStore.value.indexOf( "-" , s + 1 );

			if( s != -1 && f > s )
			{
				oStore.value = oStore.value.replace( oStore.value.substr( s , f - s + 1 ) , "" );
			}
        }

        // We now have a dirty report.

        if( markReportAsDirty() != null )
        {
            markReportAsDirty();
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}
</script>
<INPUT id=m_Store type=hidden name=m_Store runat="server"> <INPUT id=m_CurrF
type=hidden name=m_CurrF runat="server"> <ASP:Panel id=m_Header style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 2px; MARGIN: 0px; PADDING-TOP: 4px;" runat="server" EnableViewState="False">
<TABLE cellSpacing=0 cellPadding=0 width="100%">
  <TR class=FieldLabel>
    <TD align=left>Fields names </TD>
    <TD align=right>Descending? </TD></TR></TABLE></ASP:Panel><ASP:Panel id=m_Root style="OVERFLOW-Y: scroll;OVERFLOW-X: hidden; MARGIN: 0px;width:100%" runat="server" EnableViewState="False"><ASP:Repeater id=m_List runat="server" EnableViewState="False">
            <ItemTemplate>
                <DIV kind="item" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' class='<%# AspxTools.HtmlString(( Selection == DataBinder.Eval( Container.DataItem , "Id" ).ToString() ) ? "FieldSorterMarkItem" : "FieldSorterListItem") %>'>
                    <A class="FieldSorterLinkCommand" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' onclick="this.className = 'FieldSorterLinkCommand'; shiftSortItemUp( <%# AspxTools.JsStringUnquoted(m_Root.ClientID) %> , document.getElementById(<%# AspxTools.JsString(m_Store.ClientID) %>) , this.getAttribute('key') );">
                        &#x25b2;</A>
                    <A class="FieldSorterLinkCommand" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' onclick="this.className = 'FieldSorterLinkCommand'; shiftSortItemDn( <%# AspxTools.JsStringUnquoted(m_Root.ClientID) %> , document.getElementById(<%# AspxTools.JsString(m_Store.ClientID) %>) , this.getAttribute('key') );">
                        &#x25bc;</A>
					<div key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' class="FieldSorterLabel" onclick="chooseSortItem( <%# AspxTools.JsStringUnquoted(m_Root.ClientID) %> , this.getAttribute('key') );">
						<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Name" )) %>
					</div>
					<div key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' class='FieldSorterCommand<%# AspxTools.HtmlString(Convert.ToInt32( DataBinder.Eval( Container.DataItem , "Option" ) ) != 0 ? " FieldSorterChecked" : "") %>' onclick="clickSortItemOption( document.getElementById('<%# AspxTools.JsStringUnquoted(m_Root.ClientID) %>') ,document.getElementById(<%# AspxTools.JsString(m_Store.ClientID) %>) , this.getAttribute('key') ); blur();"> </div>
                </DIV>
            </ItemTemplate>
        </ASP:Repeater></ASP:Panel>
<script>
    // If the event handling table is available, we
    // need to link up our event processing functions.

    if( registerForReportEvent != null )
    {
        var base = document.getElementById("<%= AspxTools.ClientId(m_Base) %>" );
        var root = document.getElementById("<%= AspxTools.ClientId(m_Root) %>" );
        var curr = document.getElementById("<%= AspxTools.ClientId(m_CurrF) %>" );
        var stor = document.getElementById("<%= AspxTools.ClientId(m_Store) %>" );

        registerForReportEvent( base , curr ,  selectSortItem , "select" );
        registerForReportEvent( root , stor , shiftSortItemUp , "sortup" );
        registerForReportEvent( root , stor , shiftSortItemDn , "sortdn" );
        registerForReportEvent( root , stor ,  updateSortItem , "sorted" );
        registerForReportEvent( root , stor ,  removeSortItem , "remove" );
    }
</script>
</ASP:Panel>
