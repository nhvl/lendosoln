using System;
using System.Data;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for OperatorSetUserControl.
	/// </summary>

    [ ControlBuilderAttribute( typeof( OperatorSetItemControlBuilder ) )
    ]
    [ ParseChildrenAttribute( false )
    ]
	public partial  class OperatorSetUserControl : System.Web.UI.UserControl , INamingContainer
	{
        private System.Collections.ArrayList                 m_Items;
        private System.String                       m_SelectionLabel;
        private System.String                         m_SelectionCmd;

        public String CssClass
        {
            // Access member.

            set
            {
                m_Base.CssClass = value;
            }
        }

        public String SelectionLabel
        {
            // Access member

            set
            {
                m_SelectionLabel = value;
            }
            get
            {
                return m_SelectionLabel;
            }
        }

        public String SelectionCmd
        {
            // Access member

            set
            {
                m_SelectionCmd = value;
            }
            get
            {
                if( m_SelectionCmd == null || m_SelectionCmd.Length == 0 )
                {
                    return m_SelectionLabel;
                }

                return m_SelectionCmd;
            }
        }

        public String Height
        {
            // Access member.

            set
            {
                m_Root.Height = Unit.Parse( value );

                if( m_Root.Height.Type == UnitType.Pixel )
                {
                    m_Root.Height = Unit.Pixel( Convert.ToInt32( m_Root.Height.Value ) - 12 );
                }
            }
        }

        public String Width
        {
            // Access member.

            set
            {
                m_Base.Width = Unit.Parse( value );
            }
        }

        /// <summary>
        /// Add parsed item elements to this control.
        /// </summary>

        protected override void AddParsedSubObject( object obj )
        {
            // Only add child items.

            OperatorSetItem item = obj as OperatorSetItem;

            if( item != null )
            {
                m_Items.Add
                 ( item
                );
            }
            else

            base.AddParsedSubObject( obj );
        }

        /// <summary>
        /// Take what is specified and add it to our list.
        /// </summary>

        public void BindView()
        {
            // Populate list of operators from embedded
            // tags and bind.

            m_Grid.DataSource = m_Items;
            m_Grid.DataBind();
        }

        /// <summary>
        /// Render the web control.
        /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Bind the current data state to the ui.

            BindView();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PreRender += new System.EventHandler(this.ControlPreRender);
		}
		#endregion

        public OperatorSetUserControl()
        {
            // Initialize members.

            m_Items = new ArrayList();

            m_SelectionLabel = "";
            m_SelectionCmd   = "";
        }
	}

    /// <summary>
    /// Provide item listing template for predefining operators.
    /// </summary>

    public class OperatorSetItemControlBuilder : ControlBuilder
    {
        /// <summary>
        /// Override child control filter and provide implementation
        /// when items are built.
        /// </summary>

        public override Type GetChildControlType( string tagName , IDictionary attributes )
        {
            if( tagName == "Item" || tagName == "item" )
            {
                return typeof( OperatorSetItem );
            }

            return null;
        }

    }

    /// <summary>
    /// Provide container of custom children controls.
    /// </summary>

    public class OperatorSetItem : Control , INamingContainer
    {
        /// <summary>
        /// Data item
        /// 
        /// We reference the bound content for templating.
        /// </summary>

        private String  m_Label; // displayed description
        private String m_Symbol; // symbolic representation
        private String   m_Code; // code-based identifier

        public string Label
        {
            // Access member.

            set
            {
                m_Label = value;
            }
            get
            {
                return m_Label;
            }
        }

        public string Symbol
        {
            // Access member.

            set
            {
                m_Symbol = value;
            }
            get
            {
                return m_Symbol;
            }
        }

        public string Code
        {
            // Access member.

            set
            {
                m_Code = value;
            }
            get
            {
                return m_Code;
            }
        }

        public string Key
        {
            // Access member.

            get
            {
                return m_Code + ":" + m_Symbol;
            }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public OperatorSetItem()
        {
            // Initialize members.

            m_Label  = "";
            m_Symbol = "";
            m_Code   = "";
        }

    }

}
