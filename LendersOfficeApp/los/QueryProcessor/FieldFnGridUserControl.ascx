<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="FieldFnGridUserControl.ascx.cs"
    Inherits="LendersOffice.QueryProcessor.FieldFnGridUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Panel ID="m_Head" runat="server">
<div class="FieldFnGridListHeader">
    <span style="width: 230px;">Field names and functions </span>
    <span class="TxtCenter30">Avg</span>
    <span class="TxtCenter30">Min</span>
    <span class="TxtCenter30">Max</span>
    <span class="TxtCenter30">Sum</span>
    <span class="TxtCenter30">Sort</span>
    <span class="TxtCenter30">Group</span>
</div>
</asp:Panel>
<asp:Panel ID="m_Base" runat="server" Style="overflow-y: scroll;overflow-x: hidden;">

    <script type="text/javascript">

        function chooseGridItem(oContainer, sFieldId) {
            try {
                // Validate object set.

                if (oContainer == null || sFieldId == null) {
                    return;
                }

                // Delegate to all listening handlers.

                if (fireReportEvent != null) {
                    fireReportEvent("select", sFieldId, oContainer);
                }
            }
            catch (e) {
                window.status = e.message;
            }
        }

        function includeGridItem(oContainer, oStore, sFieldId) {
            try {
                var cmds = ["Av", "Mn", "Mx", "Sm", "St", "Gb"];

                // Validate the container and field.

                if (oContainer.getElementsByTagName("*").length == 0 || oStore.value == null || sFieldId == null) {
                    return;
                }

                // Update the container by adding a row item.

                var id, name;
                var parts;
                if (typeof (stringSplitRemainder) != 'undefined')
                {
                    parts = stringSplitRemainder(sFieldId, ":", 2);
                }
                else
                {
                    parts = parts = sFieldId.split(":");
                }
                //var parts = sFieldId.split(":");

                if (parts.length != 2) {
                    return;
                }

                if (oStore.value.indexOf("-" + parts[0] + ":") != -1) {
                    return;
                }

                name = parts[1];
                id = parts[0];
                
                var h = hypescriptDom;
                $(oContainer).find('.FieldFnList').append(
                    h("li", {attrs:{kind:"item", key:id}, className:("FieldFnGridListItem "+id)}, 
                        [
                            h("input", {type:"hidden", className:"FieldId", value:id}),
                            h("input", {type:"hidden", className:"FieldName", value:name}),
                            h("span", {className:"Clickable FieldFnGridLinkCommand Up  ", innerHTML:"&#x25b2;"}),
                            " ",
                            h("span", {className:"Clickable FieldFnGridLinkCommand Down", innerHTML:"&#x25bc;"}),
                            " ",
                            h("span", {className:"Label"}, name),
                        ].concat(cmds.map(function(cmd){
                            return h("span", {className:"TxtCenter30"}, h("div", {className:(cmd+" Command FldFnGridOptCmd")}))
                        })))
                );

                oStore.value += "-" + id + ":-";

                // We now have a dirty report.

                if (markReportAsDirty != null) {
                    markReportAsDirty();
                }
            }
            catch (e) {
                window.status = e.message;
            }
        }

        jQuery(function($) {
            var container = $('#<%= AspxTools.ClientId(m_Base) %>');

            container.find('.FieldFnList').sortable({
                stop: function(event, ui) {
                    SaveOrderAndCommands();
                },
                axis: 'y',
                distance: 5
            });

            var commandEvents =
            {
                'click': function() {
                    $(this).toggleClass('FieldFnGridOptionChecked');
                    var cmd = GetCommand(this);
                    var itemName = $(this).parent().siblings('.FieldName').val();
                    var itemId = $(this).parent().siblings('.FieldId').val();

                    SaveOrderAndCommands();

                    FireCommandEvents(cmd, itemName, itemId);
                    $(this).blur();
                },

                'mouseenter mouseleave': function() {
                    $(this).toggleClass('FieldFnGridOptionHilight');
                }

            };
            container.find('.FieldFnList').on(commandEvents, '.Command');

            var listEvents =
            {
                'mousedown': function() {
                    $(this).parents('li').addClass('FieldFnGridMarkItem').siblings().removeClass('FieldFnGridMarkItem');
                    var itemId = $(this).siblings('.FieldId').val();
                    $("#<%= AspxTools.ClientId(m_CurrF) %>").val(itemId);
                    fireEvent("select", itemId);
                }
            };
            container.find('.FieldFnList').on(listEvents, 'li span.Label');

            var upEvents =
            {
                'click': function() {
                    var elem = $(this).parent();
                    elem.insertBefore(elem.prev());

                    SaveOrderAndCommands();
                }
            }
            container.find('.FieldFnList').on(upEvents, 'li span.FieldFnGridLinkCommand.Up');

            var downEvents =
            {
                'click': function() {
                    var elem = $(this).parent();
                    elem.insertAfter(elem.next());

                    SaveOrderAndCommands();
                }
            }
            container.find('.FieldFnList').on(downEvents, 'li span.FieldFnGridLinkCommand.Down');

            function GetOrderAndCommands() {
                var results = {};
                container.find('.FieldFnList li').each(function() {
                    var commands = [];
                    $(this).find('.FieldFnGridOptionChecked').each(function() {
                        commands.push(GetCommand(this));
                    });

                    var id = $(this).find('.FieldId').val();
                    results[id] = commands;
                });

                return results;
            }

            function SerializeOrderAndCommands(toSerialize) {
                var results = [];

                $.each(toSerialize, function(entry, commands) {
                    results.push('-');
                    results.push(entry);
                    results.push(':');
                    results.push(commands.join(','));
                    results.push('-');

                });

                return results.join('');
            }

            function SaveOrderAndCommands() {
                var current = GetOrderAndCommands();
                var serialized = SerializeOrderAndCommands(current);
                $('#<%= AspxTools.ClientId(m_Store) %>').val(serialized);
                if (markReportAsDirty() != null) {
                    markReportAsDirty();
                }

                var temp = [];
                $.each(current, function(id, cmds) {
                    temp.push(id);
                });

                fireEvent("listupdated", temp);

                return current;
            }

            function UpdateListOrder(newOrder) {
                var sorted = [];
                var elems = container.find('.FieldFnList');

                $.each(newOrder, function(idx, fieldId) {
                    sorted.push(elems.find('li.' + fieldId)[0]);
                });

                $(sorted).detach();
                container.find('.FieldFnList').append(sorted);
            }

            //Bind reporting events
            if (registerForReportEvent != null) {
                var root = $("#<%= AspxTools.ClientId(m_Root) %>")[0];
                var curr = $("#<%= AspxTools.ClientId(m_CurrF) %>")[0];
                var stor = $("#<%= AspxTools.ClientId(m_Store) %>")[0];

                registerForReportEvent(root, stor, includeGridItem, "include");

                registerForReportEvent(container[0], curr,
                function(base, currentFieldInput, fieldid) {
                    $(base).find('.' + fieldid + ' span.Label').trigger('mousedown');
                }, "select");

                registerForReportEvent(container[0], stor,
                function(base, storage, fieldid) {
                    $(base).find('.' + fieldid).remove();
                    SaveOrderAndCommands();
                }, "remove");

                registerForReportEvent(container[0], stor,
                function(base, storage, newOrder) {
                    UpdateListOrder(newOrder);
                }, "listupdated");
            }

            //Returns the command (Av, Mn, Mx, etc) of a given element (it's the first class)
            function GetCommand(elem) {
                return $(elem).attr('class').split(' ')[0];
            }

            var cmdToEvent = {
                'St': 'sorted',
                'Gb': 'groupd'
            };
            function FireCommandEvents(cmd, itemName, itemId) {
                if (fireReportEvent == null) return;

                var eventName = cmdToEvent[cmd];
                if (eventName) fireEvent(eventName, itemId + ":" + itemName);

                fireEvent("clickf", itemId + ':' + cmd + ':' + itemName);
            }


            function fireEvent(eventName, eventParam) {
                if (!fireReportEvent) return;
                fireReportEvent(eventName, eventParam, container[0]);
            }
        });

    </script>

    <input type="hidden" id="m_CurrF" name="m_CurrF" runat="server" value="" />
    <input type="hidden" id="m_Store" name="m_Store" runat="server" value="" />
    <asp:Panel ID="m_Root" runat="server" Style="width: 100%;">
        <ol class="FieldFnList OrderList">
            <asp:Repeater ID="m_List" runat="server" EnableViewState="False" OnItemDataBound="BindCommands">
                <ItemTemplate>
                    <li kind="item" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' class='FieldFnGridListItem <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %> <%# AspxTools.HtmlString( ( Selection == DataBinder.Eval( Container.DataItem , "Id" ).ToString() ) ? "FieldFnGridMarkItem" : "" ) %>'>
                        <input type="hidden" class="FieldId" value="<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>" />
                        <input type="hidden" class="FieldName" value="<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "NamePerm" )) %>" />
                        <span class="Clickable FieldFnGridLinkCommand Up">&#x25b2;</span>
                        <span class="Clickable FieldFnGridLinkCommand Down">&#x25bc;</span>
                        <span class="Label"><%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "NamePerm" )) %></span>
                        <asp:Repeater ID="m_Cmds" runat="server" EnableViewState="false">
                            <ItemTemplate>
                                <span class="TxtCenter30">
                                    <div class="<%# AspxTools.HtmlString(Container.DataItem.ToString()) %> Command FldFnGridOptCmd
                                    <%# AspxTools.HtmlString( IsActive( Container.DataItem.ToString(), DataBinder.Eval(Container.Parent.Parent, "DataItem.Id") )?" FieldFnGridOptionChecked":"") %>"></div>
                                </span>
                            </ItemTemplate>
                        </asp:Repeater>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ol>
    </asp:Panel>
</asp:Panel>
