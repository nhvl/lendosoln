<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FieldBrowseUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.FieldBrowseUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<STYLE>

    .fieldbrowselistitem
    { PADDING-LEFT: 4px
    ; PADDING-RIGHT: 4px
    ; PADDING-BOTTOM: 1px
    ; PADDING-TOP: 1px
    ; FONT: 11px arial
    ; COLOR: black
    ; WIDTH: 100%
    }

</STYLE>
<ASP:Panel id="m_Base" runat="server" style="PADDING-TOP: 0px; PADDING-BOTTOM: 0px; PADDING-RIGHT: 0px; OVERFLOW: auto;">
    <INPUT type="hidden" id="m_CurrF" name="m_CurrF" runat="server" value="">
    <INPUT type="hidden" id="m_Store" name="m_Store" runat="server" value="">
    <ASP:DataGrid id="m_Grid" runat="server" CellPadding="2" ShowHeader="False" ShowFooter="False" GridLines="None" AutoGenerateColumns="False" AllowSorting="False" Width="100%" EnableViewState="False">
        <HeaderStyle CssClass="GridHeader">
        </HeaderStyle>
        <FooterStyle CssClass="GridFooter">
        </FooterStyle>
        <AlternatingItemStyle>
        </AlternatingItemStyle>
        <ItemStyle VerticalAlign="Top">
        </ItemStyle>
        <SelectedItemStyle CssClass="GridSelectedItem">
        </SelectedItemStyle>
        <Columns>
            <ASP:TemplateColumn ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <SPAN class="FieldBrowseListItem">
                        <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Name" )) %>
                    </SPAN>
                </ItemTemplate>
            </ASP:TemplateColumn>
        </Columns>
    </ASP:DataGrid>
</ASP:Panel>
