using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for ChooseEntryUserControl.
	/// </summary>

	public partial  class ChooseEntryUserControl : System.Web.UI.UserControl , INamingContainer
	{
        protected LendersOffice.QueryProcessor.SearchListUserControl m_List;

        private LendersOffice.QueryProcessor.FieldLookup m_Look;

        public String CssClass
        {
            // Access member.

            set
            {
                m_Base.CssClass = value;
            }
        }

        public String SelectionLabel
        {
            // Access member

            set
            {
                m_List.SelectionLabel = value;
            }
            get
            {
                return m_List.SelectionLabel;
            }
        }

        public String SelectionCmd
        {
            // Access member

            set
            {
                m_List.SelectionCmd = value;
            }
            get
            {
                if( m_List.SelectionCmd == null || m_List.SelectionCmd.Length == 0 )
                {
                    return m_List.SelectionLabel;
                }

                return m_List.SelectionCmd;
            }
        }

        public String Height
        {
            // Access member.

            set
            {
                m_Base.Height = Unit.Parse( value );

                if( m_Base.Height.Type == UnitType.Pixel )
                {
                    m_List.Height = Unit.Pixel( Convert.ToInt32( m_Base.Height.Value ) - 75 ).ToString();
                }
                else
                {
                    m_List.Height = value;
                }
            }
        }

        public String Width
        {
            // Access member.

            set
            {
                m_Base.Width = Unit.Parse( value );

                if( m_Base.Width.Type == UnitType.Pixel )
                {
                    m_Valu.Width = Unit.Pixel( Convert.ToInt32( m_Base.Width.Value ) - 132 );
                    m_Item.Style.Add(HtmlTextWriterStyle.Width, Unit.Pixel( Convert.ToInt32( m_Base.Width.Value ) - 132 ).ToString());
                }
                else
                {
                    m_Valu.Width = Unit.Parse( value );
                    m_Item.Style.Add(HtmlTextWriterStyle.Width, Unit.Parse( value ).ToString());
                }
            }
        }

        /// <summary>
        /// Expose repeater list data-binding interface.
        /// </summary>

        public FieldLookup DataSource
        {
            // Access member.

            set
            {
	            m_Look = m_List.DataLookup = value;
            }
        }

        /// <summary>
        /// Bind the specified option entry boxes to the schema.
        /// </summary>

        public void BindView()
        {
            // We need to initialize the mapping list so that
            // our client script will have a choice lookup
            // table for each field with multiple options.

            System.Text.StringBuilder mapping = new System.Text.StringBuilder();

            foreach( FieldGroup group in m_Look.Fields )
            {
                foreach( FieldLabel label in group )
                {
                    // Gather the mapping pairs for the ui's
                    // mapping lookup table (stored as a str).

                    Field     field = m_Look.LookupById( label.Id );
                    ArrayList items = new ArrayList();

                    if( field.Mapping.Count != 0 )
                    {
                        foreach( string option in field.Mapping )
                        {
                            ListItem li = new ListItem();

                            li.Value = option;
                            li.Text  = option;

                            items.Add( li );
                        }
                    }

                    // Gather all the relevant macros and add
                    // them to the set too.

                    foreach (FieldGroup mcgrp in m_Look.Macros)
                    {
                        foreach (FieldLabel mclab in mcgrp)
                        {
                            Field macro = m_Look.LookupById(mclab.Id);

                            if (macro.Kind == field.Kind)
                            {
                                ListItem li = new ListItem();

                                li.Text = macro.Name;
                                li.Value = macro.Id;

                                items.Add(li);
                            }
                        }
                    }

                    if( items.Count > 0 )
                    {
                        // We have at least one item for the drop
                        // down when the current field is selected
                        // and it's value entry focused.  Cache
                        // this mapping choice for later lookup
                        // by the client side script.  We place each
                        // option in with its text description and
                        // a query processor specific value key.
                        // With constants, the key is the same as
                        // the text.  With macros, we place the field
                        // id of the macro in the key.  The ui will
                        // pass this key around to other user controls
                        // when you specify a value using the drop
                        // down items list.  This macro key is not
                        // for display, but for telling the condition
                        // tree code to add the condition so that
                        // it compares against a macro field and not
                        // a simple constant value.

                        mapping.AppendFormat( "*{0}:", field.Id);

                        foreach( ListItem item in items )
                        {
                            mapping.AppendFormat("{0}^{1};", item.Text, item.Value);
                        }

                        mapping.Append("*");
                    }
                }
            }

            m_Maps.Value = mapping.ToString();
        }
        
        /// <summary>
        /// Render the web control.
        /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Bind the current data state to the ui.

            BindView();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.PreRender += new System.EventHandler(this.ControlPreRender);

        }
		#endregion

	}

}
