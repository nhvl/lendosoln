<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SearchListUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.SearchListUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript">

    //
    // function:
    //
    // addCurrentColumn
    //
    // description:
    //
    // Handle removing current field. Has to be here instead of in the script file because of all those embedded variables.
    //

    function addCurrentColumn(oControl, oRoot, sArg) {
        try {
            // Validate controls and base.

            if (oControl == null || oRoot == null || sArg == null) {
                return;
            }

            // Add a new entry.

            var parts;
            if(typeof(stringSplitRemainder) != 'undefined')
            {
                parts = stringSplitRemainder(sArg, ":", 2);
            }
            else
            {
                parts = sArg.split(":");
            }

            var i;

            if (parts.length < 2) {
                return;
            }

            for (i = 0; i < oRoot.children.length; ++i) {
                var o = oRoot.children[i];

                if (o.getAttribute("key") == null || o.getAttribute("type") == null) {
                    continue;
                }

                if (o.getAttribute("type") == "Header" && o.getAttribute("column") != null) {
                    o.insertAdjacentHTML("afterEnd"
					, "<DIV class=\"SearchListItem\" column key=\"" + parts[0] + "\" label=\"" + parts[1] + "\" type=\"Item\" cid=\"<%# AspxTools.HtmlString(ClientID) %>\" uid=\"<%= AspxTools.HtmlString(UniqueID) %>\" onclick=\"onSearchListItemPicked( <%= AspxTools.ClientId(m_Root) %> , <%= AspxTools.ClientId(m_Store) %> , this );\" ondblclick=\"onPickCurrentListItem( <%= AspxTools.ClientId(m_Root) %>, <%= AspxTools.ClientId(m_Store) %> , this.getAttribute('key'), this.getAttribute('label'), '<%= AspxTools.HtmlString(UniqueID) %>' , '<%= AspxTools.HtmlString(SelectionCmd) %>' , '<%= AspxTools.HtmlString(PostBackOnPick) %>' );\">"
					+ "    <DIV class=\"SearchListLabel\">"
					+ "        <A href=\"#\" class=\"SearchListLinkCommand\" cid=\"<%= AspxTools.HtmlString(ClientID) %>\" uid=\"<%= AspxTools.HtmlString(UniqueID) %>\" key=\"" + parts[0] + "\" label=\"" + parts[1] + "\" onclick=\"onPickCurrentListItem( <%= AspxTools.ClientId(m_Root) %> , <%= AspxTools.ClientId(m_Store) %>, this.getAttribute('key'), this.getAttribute('label'), '<%= AspxTools.HtmlString(UniqueID) %>' , '<%= AspxTools.HtmlString(SelectionCmd) %>', '<%= AspxTools.HtmlString(PostBackOnPick) %>' );\">"
					+ "            <%= AspxTools.HtmlString(SelectionLabel) %></A>"
					+ "        <SPAN>"
					+ "            " + parts[1]
					+ "        </SPAN>"
					+ "    </DIV>"
					+ "</DIV>"
					);
                }
            }
        }
        catch (e) {
            window.status = e.message;
        }
    }
</script>
<ASP:Panel id="m_Base" runat="server">
	<INPUT id="m_Store" name="m_Store" runat="server" type="hidden">
	<DIV style="MARGIN: 4px; TEXT-ALIGN: right;">
		<DIV style="WIDTH: 100%;" ondblclick="onSelectDomain( <%= AspxTools.ClientId(m_Root) %> , <%= AspxTools.ClientId(m_Root) %> , document.getElementById('<%= AspxTools.ClientId(m_Group) %>').value );">
			<TABLE cellpadding="0" cellspacing="0" width="100%" border="0">
				<TR>
					<TD align="left" style="WIDTH: 70px; FONT: 11px arial;">
						Show group:
					</TD>
					<TD align="right">
						<ASP:DropDownList id="m_Group" runat="server" style="PADDING-LEFT: 4px; BORDER: 1px groove; FONT: 11px arial;" onchange="triggerEvent(this,'dblclick' );"></ASP:DropDownList>
					</TD>
				</TR>
			</TABLE>
		</DIV>
	</DIV>
	<ASP:Panel id="m_Root" runat="server" class="m-root" EnableViewState="False">
		<ASP:Repeater id="m_Curr" runat="server" EnableViewState="False">
			<ItemTemplate>
				<DIV class=<%# AspxTools.HtmlAttribute(RenderItemClass( Container.DataItem )) %> column cid=<%# AspxTools.HtmlAttribute(ClientID) %> uid=<%= AspxTools.HtmlAttribute(UniqueID) %> onclick="onSearchListItemPicked( <%# AspxTools.ClientId(m_Root) %> , <%# AspxTools.ClientId(m_Store) %> , this );" ondblclick="onPickCurrentListItem( <%= AspxTools.ClientId(m_Root) %> , <%= AspxTools.ClientId(m_Store) %> , this.getAttribute('key') , this.getAttribute('label') , <%= AspxTools.JsString(UniqueID) %> , <%= AspxTools.JsString(SelectionCmd) %> , <%= AspxTools.JsString(PostBackOnPick.ToString()) %>, <%# AspxTools.JsString(DataBinder.Eval( Container.DataItem , "Type"  ).ToString()) %>, this.access );"
					key   = <%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Key").ToString()) %>
					label = <%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Label" ).ToString()) %>
					type  = <%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Type"  ).ToString()) %>
					access = <%# AspxTools.HtmlAttribute(CheckPermission( Container.DataItem )) %>
				>
					<DIV class="SearchListLabel">
						<A href="#" class=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Type" ).ToString() != "Header" ? "SearchListLinkCommand" : "SearchListHidden") %> cid=<%= AspxTools.HtmlAttribute(ClientID) %> uid=<%= AspxTools.HtmlAttribute(UniqueID) %> key=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Key" ).ToString()) %> label=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Label" ).ToString()) %> access=<%# AspxTools.HtmlAttribute(CheckPermission( Container.DataItem ).ToString()) %> onclick="onPickCurrentListItem(<%= AspxTools.ClientId(m_Root) %>, <%= AspxTools.ClientId(m_Store) %>, this.getAttribute('key') , this.getAttribute('label'), <%= AspxTools.JsString(UniqueID) %> , <%= AspxTools.JsString(SelectionCmd) %> , <%= AspxTools.JsString(PostBackOnPick.ToString()) %>, null, this.access );">
							<%# AspxTools.HtmlString(SelectionLabel) %></A>
						<SPAN class=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Type" ).ToString() != "Header" ? "" : "SearchListHidden") %>>
						</SPAN>
						<SPAN>
							<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
							<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Type"  ).ToString() == "Header" ? "..." : "") %>
						</SPAN>
					</DIV>
					<DIV class=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Description" ).ToString() != "" ? "SearchListDescription" : "SearchListHidden") %>>
						<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Description" ).ToString()) %>
					</DIV>
				</DIV>
			</ItemTemplate>
		</ASP:Repeater>
		<ASP:Repeater id="m_List" runat="server" EnableViewState="False">
			<ItemTemplate>
				<DIV class = <%# AspxTools.HtmlAttribute(RenderItemClass( Container.DataItem )) %> cid=<%# AspxTools.HtmlAttribute(ClientID) %> uid=<%= AspxTools.HtmlAttribute(UniqueID) %> onclick="onSearchListItemPicked( <%# AspxTools.ClientId(m_Root) %> , <%# AspxTools.ClientId(m_Store) %> , this );" ondblclick="onPickCurrentListItem( <%= AspxTools.ClientId(m_Root) %> , <%= AspxTools.ClientId(m_Store) %> , this.getAttribute('key') , this.getAttribute('label') , <%= AspxTools.JsString(UniqueID) %> , <%= AspxTools.JsString(SelectionCmd) %> , <%= AspxTools.JsString(PostBackOnPick.ToString()) %>, <%# AspxTools.JsString(DataBinder.Eval( Container.DataItem , "Type"  ).ToString()) %>, this.access );"
					key   = <%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Key"   ).ToString()) %>
					label = <%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Label" ).ToString()) %>
					type  = <%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Type"  ).ToString()) %>
					access = <%# AspxTools.HtmlAttribute(CheckPermission( Container.DataItem )) %>
                >
					<DIV class="SearchListLabel">
						<A href="#" class=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Type" ).ToString() != "Header" ? "SearchListLinkCommand" : "SearchListHidden") %> cid=<%= AspxTools.HtmlAttribute(ClientID) %> uid=<%= AspxTools.HtmlAttribute(UniqueID) %> key=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Key" ).ToString()) %> label=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Label" ).ToString()) %> access=<%# AspxTools.HtmlAttribute(CheckPermission( Container.DataItem )) %> onclick="onPickCurrentListItem( <%= AspxTools.ClientId(m_Root) %> , <%= AspxTools.ClientId(m_Store) %> , this.getAttribute('key') , this.getAttribute('label') , <%= AspxTools.JsString(UniqueID) %> , <%= AspxTools.JsString(SelectionCmd) %> , <%= AspxTools.JsString(PostBackOnPick.ToString()) %>, null, this.access );">
							<%# AspxTools.HtmlString(SelectionLabel) %></A>
						<SPAN class=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Type" ).ToString() != "Header" ? "" : "SearchListHidden") %>>
						</SPAN>
						<SPAN>
							<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" ).ToString()) %>
							<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Type"  ).ToString() == "Header" ? "..." : "") %>
						</SPAN>
					</DIV>
					<DIV class=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Description" ).ToString() != "" ? "SearchListDescription" : "SearchListHidden") %>>
						<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Description" ).ToString()) %>
					</DIV>
				</DIV>
			</ItemTemplate>
		</ASP:Repeater>
	</ASP:Panel>
	<DIV style="MARGIN: 4px; TEXT-ALIGN: right;" onkeypress="if( event.keyCode == 13 ) { event.cancelBubble = true; return false; }">
		<DIV style="WIDTH: 100%;" cid=<%= AspxTools.HtmlAttribute(ClientID) %> onkeypress="if( event.keyCode == 13 ) getElementByIdFromParent(this, 'm_Next').click();">
			<TABLE cellpadding="0" cellspacing="0" width="100%" border="0">
				<TR>
					<TD align="left" style="WIDTH: 60px; FONT: 11px arial;">
						Search for:
					</TD>
					<TD align="left">
						<ASP:TextBox id="m_Query" runat="server" style="BORDER-WIDTH: 2px groove; FONT: 11px arial; MARGIN-BOTTOM: 0px; PADDING-LEFT: 4px; width:100%; box-sizing: border-box;" TabIndex="2" />
					</TD>
					<TD align="right" width="116">
						<INPUT id="m_Prev" type="button" style="WIDTH: 54px; FONT: 11px arial; PADDING: 1px; BORDER-WIDTH: 1px;" value="Prev" tabindex="2" onclick="onSearchListPrevMatchClick( <%= AspxTools.ClientId(m_Root) %> , <%= AspxTools.ClientId(m_Store) %> , <%= AspxTools.ClientId(m_Query) %> );">
						<INPUT id="m_Next" type="button" style="WIDTH: 54px; FONT: 11px arial; PADDING: 1px; BORDER-WIDTH: 1px;" value="Next" tabindex="2" onclick="onSearchListNextMatchClick( <%= AspxTools.ClientId(m_Root) %> , <%= AspxTools.ClientId(m_Store) %> , <%= AspxTools.ClientId(m_Query) %> );">
					</TD>
				</TR>
			</TABLE>
		</DIV>
	</DIV>
	<script>

        // If the event handling table is available, we
        // need to link up our event processing functions.

        if( registerForReportEvent != null )
        {
            var base = document.getElementById("<%= AspxTools.ClientId(m_Base) %>" );
            var root = document.getElementById("<%= AspxTools.ClientId(m_Root) %>" );
            var text = document.getElementById("<%= AspxTools.ClientId(m_Query) %>" );
            var drop = document.getElementById("<%= AspxTools.ClientId(m_Group) %>" );
            var stor = document.getElementById("<%= AspxTools.ClientId(m_Store) %>" );
			window.<%= AspxTools.ClientId(m_Store) %> = stor;

            registerForReportEvent( root , stor ,  findSelectedItem , "onload" );
            registerForReportEvent( base , root , enableSearchPanel , "enable" );
            registerForReportEvent( base , drop , enableSearchExtra , "enable" );
            registerForReportEvent( base , text , enableSearchExtra , "enable" );
            registerForReportEvent( base , root , dsableSearchPanel , "dsable" );
            registerForReportEvent( base , drop , dsableSearchExtra , "dsable" );
            registerForReportEvent( base , text , dsableSearchExtra , "dsable" );

    		if( <%= AspxTools.JsBool(HideReportFields) %> )
    		{
    			registerForReportEvent( base , root , hidePickedFields , "include" );
    			registerForReportEvent( base , root , showHiddenFields , "remove" );
    		}

    		if( <%= AspxTools.JsBool(ShowReportFields) %> )
    		{
    			registerForReportEvent( base , root , addCurrentColumn , "include" );
    			registerForReportEvent( base , root , dropColumnFields , "remove" );
    		}
        }
	</script>
</ASP:Panel>
