using System;
using System.Data;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for FieldBrowseControl.
	/// </summary>

	public partial  class FieldBrowseUserControl : System.Web.UI.UserControl , INamingContainer
	{
        protected LendersOffice.QueryProcessor.Query          m_View;
        private LendersOffice.QueryProcessor.FieldLookup      m_Look;

        public String Selection
        {
            // Access member.

            set
            {
                m_CurrF.Value = value;
            }
            get
            {
                return m_CurrF.Value;
            }
        }

        public Query DataSource
        {
            // Access member.

            set
            {
                m_View = value;
            }
        }

        public FieldLookup DataLookup
        {
            // Access member.

            set
            {
                m_Look = value;
            }
        }

        public String CssClass
        {
            // Access member.

            set
            {
                m_Base.CssClass = value;
            }
        }

        public String Height
        {
            // Access member.

            set
            {
                m_Base.Height = Unit.Parse( value );
            }
        }

        public String Width
        {
            // Access member.

            set
            {
                m_Base.Width = Unit.Parse( value );
            }
        }

        /// <summary>
        /// Bind the data to the current ui status.
        /// </summary>

        public void BindData()
        {
            // Reorder the layout list to match that of
            // the data store, but only if the store is
            // valid (i.e., only on postback).

            if( IsPostBack == true )
            {
                foreach( string id in m_Store.Value.Split( '-' ) )
                {
                    // Throw out invalid entries.

                    if( id.Length == 0 )
                    {
                        continue;
                    }

                    // Push the column to the end.

                    Column column = m_View.Columns.Find( id );

                    m_View.Columns.Nix( column );
                    m_View.Columns.Add( column );
                }

                for( int i = 0 ; i < m_View.Columns.Count ; ++i )
                {
                    // If not in our cached list, then it
                    // shouldn't be in the data object.
                    // The client-side updates now drive
                    // the changes to the underlying
                    // content.  If multiple controls try
                    // to do this same operation, we want
                    // the first to succeed, and the others
                    // to not need to.

                    Column column = m_View.Columns[ i ];

                    if( column.Show == true && m_Store.Value.IndexOf( "-" + column.Id + "-" ) == -1 )
                    {
                        m_View.Columns.Nix( column );

                        --i;
                    }
                }
            }
        }

        /// <summary>
        /// Bind the report layout to our list.
        /// </summary>

        public void BindView()
        {
            // Use the current layout and list the ordered
            // entries.

            ArrayList shown = new ArrayList();

            if( m_View != null && m_Look != null )
            {
                if( IsPostBack == false )
                {
                    // Transfer the data to the store this
                    // first time.

                    m_Store.Value = "";

                    foreach( Column column in m_View.Columns )
                    {
                        if( column.Show == false )
                        {
                            continue;
                        }

                        m_Store.Value += "-" + column.Id + "-";
                    }
                }

                foreach( Column column in m_View.Columns )
                {
                    // Add listing fields in order.

                    if( column.Show == false )
                    {
                        continue;
                    }

                    shown.Add( new FieldBrowseItemWrapper( column ) );
                }
            }

            m_Grid.DataSource = shown;
            m_Grid.DataBind();
        }


        /// <summary>
        /// Bind the report layout to our list.
        /// </summary>

        public void HoldView()
        {
            // Use the current sorting and list the ordered
            // entries.

            ArrayList shown = new ArrayList();

            m_Store.Value = "";

            if( m_View != null && m_Look != null )
            {
                foreach( Column column in m_View.Columns )
                {
                    // Add listing fields in order.

                    if( column.Show == false )
                    {
                        continue;
                    }

                    m_Store.Value += "-" + column.Id + "-";

                    shown.Add( new FieldBrowseItemWrapper( column ) );
                }
            }

            m_Grid.DataSource = shown;
            m_Grid.DataBind();
        }

	    /// <summary>
	    ///	Bind data and render lists.
	    /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Use current columns to list report.

            HoldView();
		}

	    /// <summary>
	    ///	Initialize this control.
	    /// </summary>

		protected void ControlLoad( object sender , System.EventArgs e )
		{
            // Update the data elements according
            // to the state of the event store.
            // When we bind the view, the changes
            // that we make here should already be
            // in effect.

            BindData();
		}

	    /// <summary>
	    ///	Initialize this control.
	    /// </summary>

		protected void ControlInit( object sender , System.EventArgs e )
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.ControlInit);
            this.Load += new System.EventHandler(this.ControlLoad);
            this.PreRender += new System.EventHandler(this.ControlPreRender);

        }
		#endregion

	}

    /// <summary>
    /// Wrapping class for ui display.
    /// </summary>

    public class FieldBrowseItemWrapper
    {
        /// <summary>
        /// Column to interrogate.
        /// </summary>

        private Column m_Column; // column to interrogate

        public string Name
        {
            // Access member.

            get
            {
                return m_Column.Name;
            }
        }

        public string Kind
        {
            // Access member.

            get
            {
                string kind = "Text";

                switch( m_Column.Kind )
                {
                    case Field.E_ClassType.Cal: kind = "Date";
                    break;

                    case Field.E_ClassType.Cnt: kind = "Count";
                    break;

                    case Field.E_ClassType.Csh: kind = "Money";
                    break;

                    case Field.E_ClassType.Pct: kind = "Percentage";
                    break;

                    case Field.E_ClassType.LongText:
                    case Field.E_ClassType.Txt: kind = "Text";
                    break;

                    default:
                        DataAccess.Tools.LogBug("Unhandle Field.E_ClassType::" + m_Column.Kind);
                        break;
                }

                return kind;
            }
        }

        public string Functions
        {
            // Access member.

            get
            {
                string functions = "";

                foreach( Function function in m_Column.Functions )
                {
                    if( functions.Length > 0 )
                    {
                        functions += ", ";
                    }

                    switch( function.Type  )
                    {
                        case E_FunctionType.Av: functions += "Avg";
                        break;

                        case E_FunctionType.Mn: functions += "Min";
                        break;

                        case E_FunctionType.Mx: functions += "Max";
                        break;

                        case E_FunctionType.Sm: functions += "Tot";
                        break;

                        case E_FunctionType.Ct: functions += "Num";
                        break;
                    }
                }

                if( functions.Length == 0 )
                {
                    return "None";
                }

                return functions;
            }
        }

        public string Id
        {
            // Access member.

            get
            {
                return m_Column.Id;
            }
        }

        public FieldBrowseItemWrapper( Column cColumn )
        {
            // Initialize members.

            m_Column = cColumn;
        }

    }

    /// <summary>
    /// Event handling arguments.
    /// </summary>

    public class FieldBrowseEventArgs : EventArgs
    {
        /// <summary>
        /// Event args
        /// 
        /// Action is defined by this control's ui.  The
        /// target is always a field id.
        /// </summary>

        public string Action; // requested action
        public string Target; // field target id

        /// <summary>
        /// Construct specific event args.
        /// </summary>
        /// <param name="sAction">
        /// Action requested.
        /// </param>
        /// <param name="fTarget">
        /// Target of action.
        /// </param>

        public FieldBrowseEventArgs( string sAction , string sTarget )
        {
            // Initialize members.

            Action = sAction;
            Target = sTarget;
        }

        /// <summary>
        /// Construct default event args.
        /// </summary>

        public FieldBrowseEventArgs()
        {
            // Initialize members.

            Action = "";
            Target = "";
        }

    }

    /// <summary>
    /// Delegate declaration for firing events.
    /// </summary>

    public delegate void FieldBrowseEventHandler( object sender , FieldBrowseEventArgs e );

}
