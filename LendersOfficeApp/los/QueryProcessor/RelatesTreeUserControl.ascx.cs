using System;
using System.Collections;
using System.IO;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for RelatesTreeUserControl.
	/// </summary>

    [ ParseChildren( true )
    ]
    public partial  class RelatesTreeUserControl : System.Web.UI.UserControl , IPostBackEventHandler , INamingContainer
	{
        protected LendersOffice.QueryProcessor.BlockTreeUserControl m_Tree;
        protected LendersOffice.QueryProcessor.TemplatedUserControl m_Node;
        protected LendersOffice.QueryProcessor.TemplatedUserControl m_Read;
		protected LendersOffice.QueryProcessor.TemplatedUserControl m_Item;
		protected LendersOffice.QueryProcessor.TemplatedUserControl m_List;
		private System.Boolean                                  m_ReadOnly;
        private System.Collections.Hashtable                 m_LookupTable;
        private System.Int32                                    m_LookupId;
        private LendersOffice.QueryProcessor.FieldLookup            m_Look;
        private LendersOffice.QueryProcessor.Query                  m_View;

        public event RelatesTreeEventHandler Update;
        public event QueryUpdateEventHandler  Dirty;

        public void RaisePostBackEvent( string eventArgument )
        {
            // Process the tree's request.

            try
            {
                // Parse command and dispatch event.

                string[] parameters = eventArgument.Split( ':' );

                if( m_View == null || parameters.Length <= 1 )
                {
                    return;
                }

                switch( parameters[ 0 ].ToLower() )
                {
                    case "addall":
                    case "addany":
                    case "remove":
                    {
                        // We only forward conditions to our containers.  If any
                        // postback event occurs, then process the request and
                        // pass the referenced conditions to our owner.  We only
                        // handle these requests on postback.  This call will not
                        // be effective after control load when the conditions
                        // can be manipulated without the conditions tree being
                        // aware of it and with no opportunity to update its
                        // lookup table.

                        Conditions conditions = m_LookupTable[ parameters[ 1 ] ] as Conditions;

                        if( conditions != null )
                        {
                            if( Update != null )
                            {
                                // Delegate to container.

                                Update( this , new RelatesTreeEventArgs( parameters[ 0 ] , conditions ) );
                            }
                            else
                            {
                                // Handle this ourself.

                                switch( parameters[ 0 ].ToLower() )
                                {
                                    case "addall":
                                    {
                                        // We have received a request to add a condition group
                                        // under the existing group.

                                        conditions.Add( new Conditions( E_ConditionsType.An ) );

                                        if( Dirty != null )
                                        {
                                            Dirty( this , "AddAll" );
                                        }
                                    }
                                    break;

                                    case "addany":
                                    {
                                        // We have received a request to add a condition group
                                        // under the existing group.

                                        conditions.Add( new Conditions( E_ConditionsType.Or ) );

                                        if( Dirty != null )
                                        {
                                            Dirty( this , "AddAny" );
                                        }
                                    }
                                    break;

                                    case "remove":
                                    {
                                        // We have received a request to remove a condition group
                                        // from its parent.  This will cause all children to get
                                        // dropped.  We walk the composite to find the containing
                                        // group.  If the selected group is the root, we only
                                        // cleanup the children.  We attempt to remove it from
                                        // all groups -- we must have the same instance that's
                                        // in the relates composite or addresses won't matchup.

                                        foreach( object item in m_View.Relates.Walk )
                                        {
                                            Conditions group = item as Conditions;

                                            if( group == null )
                                            {
                                                continue;
                                            }

                                            group.Nix( conditions );
                                        }

                                        if( conditions != m_View.Relates )
                                        {
                                            m_View.Relates.Nix( conditions );
                                        }
                                        else
                                        {
                                            m_View.Relates.Clear();
                                        }

                                        if( Dirty != null )
                                        {
                                            Dirty( this , "Remove" );
                                        }

                                        // Clear the current selection postback text.

                                        m_Info.Value = "";
                                    }
                                    break;
                                }
                            }
                        }

                        // We may have targeted an individual condition.  If this
                        // is so, we re-lookup the entry and handle the appropriate
                        // commands.

                        Condition condition = m_LookupTable[ parameters[ 1 ] ] as Condition;

                        if( condition != null )
                        {
                            // Handle this ourself.

                            switch( parameters[ 0 ].ToLower() )
                            {
                                case "remove":
                                {
                                    // We nix the referenced condition.  This condition was
                                    // added -- should have been, anyway -- to the lookup
                                    // table during binding.  Thus, the address should be
                                    // the same as in the query conditions.

                                    foreach( object item in m_View.Relates.Walk )
                                    {
                                        Conditions group = item as Conditions;

                                        if( group == null )
                                        {
                                            continue;
                                        }

                                        group.Nix( condition );
                                    }

                                    m_View.Relates.Nix( condition );

                                    if( Dirty != null )
                                    {
                                        Dirty( this , "Remove" );
                                    }

                                    // Clear the current selection postback text.

                                    m_Info.Value = "";
                                }
                                break;
                            }
                        }
                    }
                    break;

					case "addcon":
					{
						// Add the condition specified in the argument
						// to the currently specified group.

                        string[] parts = parameters[1].Split(new char[] {'*'}, 4);

						if( parts.Length < 4 )
						{
							break;
						}

						if( parts[ 0 ] == "" )
						{
							break;
						}

						if( parts[ 1 ] == "" )
						{
							break;
						}

						// Get the containing conditions group.  If not
						// found, just punt (though, this shouldn't happen).

						Conditions conditions = m_LookupTable[ parts[ 0 ] ] as Conditions;

						if( conditions != null )
						{
							// Get the primary field for this condition.

							Field field = m_Look.LookupById( parts[ 1 ] );

							if( field == null )
							{
								m_ExceptionDetails = "Unable to add new condition.  Field is invalid";

								break;
							}

							// Create a new condition based on what is
							// specified within the parameters.

							Condition condition = new Condition();

							condition.Id = field.Id;

							switch( parts[ 2 ] )
							{
								case "Eq": condition.Type = E_ConditionType.Eq;
								break;

								case "Ne": condition.Type = E_ConditionType.Ne;
								break;

								case "Bw": condition.Type = E_ConditionType.Bw;
								break;

								case "Nw": condition.Type = E_ConditionType.Nw;
								break;

								case "Ge": condition.Type = E_ConditionType.Ge;
								break;

								case "Gt": condition.Type = E_ConditionType.Gt;
								break;

								case "Le": condition.Type = E_ConditionType.Le;
								break;

								case "Lt": condition.Type = E_ConditionType.Lt;
								break;
							}

							if( condition.Type == E_ConditionType.Invalid )
							{
								m_ExceptionDetails = "Unable to add new condition.  Invalid comparison operator";

								break;
							}

							// Dice up the argument portion and initialize
							// the condition.
							//
							// 10/26/2004 kb - Caroline commented that she needed
							// to use a slash to search for selling agent company
							// names with a slash (example: RE/MAX).  Really, any
							// symbol we use will cause trouble, and much client
							// script assumes that this slash is the delimiter,
							// so we switch tactics and assume that the first slash
							// does delimit, and all subsequent are part of the
							// argument.

							string[] predicate = parts[ 3 ].Split( '/' );

							for( int x = 2 ; x < predicate.Length ; ++x )
							{
								predicate[ 1 ] += "/" + predicate[ x ];
							}

							if( predicate.Length < 2 )
							{
								m_ExceptionDetails = "Unable to add new condition.  Invalid request format";

								break;
							}

							// Create and add a new argument.

							Argument argument = new Argument();

							if( predicate[ 0 ].ToLower().TrimWhitespaceAndBOM() == "field" )
							{
								argument.Type = E_ArgumentType.Field;
							}
							else
							{
								argument.Type = E_ArgumentType.Const;
							}

							if( argument.Type == E_ArgumentType.Const )
							{
								argument.Value = field.Mapping.ByVal[ predicate[ 1 ] ];
							}
							else
							{
								argument.Value = predicate[ 1 ];
							}

							condition.Argument = argument;

							// Validate this new condition.  If the field
							// type doesn't match the argument, we punt.
							// If the field type doesn't match the operator
							// chosen, then we punt.
							//
							// 12/13/2004 kb - We now support blank field arguments.
							// For string fields, we compare against the empty string
							// and null.  For all other field types, we compare against
							// null to simulate looking for not-defined entries.

							if( condition.Argument.Type == E_ArgumentType.Const )
							{
								if( condition.Argument.Value != "" )
								{
									try
									{
										switch( field.Type )
										{
											case Field.E_FieldType.Boo:
											{
												Convert.ToInt32( condition.Argument.Value );
											}
											break;

											case Field.E_FieldType.Int:
											{
												Convert.ToInt32( condition.Argument.Value );
											}
											break;

											case Field.E_FieldType.Enu:
											{
												Convert.ToInt32( condition.Argument.Value );
											}
											break;

											case Field.E_FieldType.Dbl:
											{
												Convert.ToDouble( condition.Argument.Value );
											}
											break;

											case Field.E_FieldType.Dec:
											{
												Convert.ToDecimal( condition.Argument.Value );
											}
											break;

											case Field.E_FieldType.Dtm:
											{
												Convert.ToDateTime( condition.Argument.Value );
											}
											break;

											default:
											{
												if( field.Kind == Field.E_ClassType.Cal )
												{
													Convert.ToDateTime( condition.Argument.Value );
												}
											}
											break;
										}
									}
									catch
									{
										m_ExceptionDetails = "Unable to update condition.  Invalid argument format";

										break;
									}
								}
								else
								{
									if( condition.Type != E_ConditionType.Eq && condition.Type != E_ConditionType.Ne )
									{
										m_ExceptionDetails = "Unable to update condition.  Invalid argument value";

										break;
									}
								}
							}
							else
							if( condition.Argument.Type == E_ArgumentType.Field )
							{
								Field other = m_Look.LookupById( condition.Argument.Value );

								if( other == null )
								{
									m_ExceptionDetails = "Unable to add new condition.  Invalid field as argument";

									break;
								}

								if( field.Type != other.Type && field.Kind != other.Kind )
								{
									m_ExceptionDetails = "Unable to add new condition.  Field types mismatch";

									break;
								}
							}
							else
							{
								m_ExceptionDetails = "Unable to add new condition.  Invalid argument type";

								break;
							}

							if( field.Type != Field.E_FieldType.Str && ( condition.Type == E_ConditionType.Bw || condition.Type == E_ConditionType.Nw ) )
							{
								m_ExceptionDetails = "Unable to add new condition.  Operator not valid";

								break;
							}

							// Add the condition to the specified group.  We
							// mark this tree as dirty and propagate the event
							// up to the containing page.

							conditions.Add( condition );

							if( Dirty != null )
							{
								Dirty( this , "AddCon" );
							}
						}
					}
					break;

					case "updcon":
					{
						// Add the condition specified in the argument
						// to the currently specified group.

                        string[] parts = parameters[1].Split(new char[]{'*'}, 4);

						if( parts.Length < 4 )
						{
							break;
						}

						if( parts[ 0 ] == "" )
						{
							break;
						}

						if( parts[ 1 ] == "" )
						{
							break;
						}

						// Get the containing conditions group.  If not
						// found, just punt (though, this shouldn't happen).

						Condition condition = new Condition( m_LookupTable[ parts[ 0 ] ] as Condition );

						if( condition == null )
						{
							m_ExceptionDetails = "Unable to update condition.  Operation failed";

							break;
						}

						// Get the primary field for this condition.

						Field field = m_Look.LookupById( parts[ 1 ] );

						if( field == null )
						{
							m_ExceptionDetails = "Unable to update condition.  Field is invalid";

							break;
						}

						// Create a new condition based on what is
						// specified within the parameters.

						condition.Id = field.Id;

						switch( parts[ 2 ] )
						{
							case "Eq": condition.Type = E_ConditionType.Eq;
							break;

							case "Ne": condition.Type = E_ConditionType.Ne;
							break;

							case "Bw": condition.Type = E_ConditionType.Bw;
							break;

							case "Nw": condition.Type = E_ConditionType.Nw;
							break;

							case "Ge": condition.Type = E_ConditionType.Ge;
							break;

							case "Gt": condition.Type = E_ConditionType.Gt;
							break;

							case "Le": condition.Type = E_ConditionType.Le;
							break;

							case "Lt": condition.Type = E_ConditionType.Lt;
							break;
						}

						if( condition.Type == E_ConditionType.Invalid )
						{
							m_ExceptionDetails = "Unable to update condition.  Invalid comparison operator";

							break;
						}

						// Dice up the argument portion and initialize
						// the condition.
						//
						// 10/26/2004 kb - Caroline commented that she needed
						// to use a slash to search for selling agent company
						// names with a slash (example: RE/MAX).  Really, any
						// symbol we use will cause trouble, and much client
						// script assumes that this slash is the delimiter,
						// so we switch tactics and assume that the first slash
						// does delimit, and all subsequent are part of the
						// argument.

						string[] predicate = parts[ 3 ].Split( '/' );

						for( int x = 2 ; x < predicate.Length ; ++x )
						{
							predicate[ 1 ] += "/" + predicate[ x ];
						}

						if( predicate.Length < 2 )
						{
							m_ExceptionDetails = "Unable to update condition.  Invalid request format";

							break;
						}

						// Create and add a new argument.

						Argument argument = new Argument();

						if( predicate[ 0 ].ToLower().TrimWhitespaceAndBOM() == "field" )
						{
							argument.Type = E_ArgumentType.Field;
						}
						else
						{
							argument.Type = E_ArgumentType.Const;
						}

						if( argument.Type == E_ArgumentType.Const )
						{
							argument.Value = field.Mapping.ByVal[ predicate[ 1 ] ];
						}
						else
						{
							argument.Value = predicate[ 1 ];
						}

						condition.Argument = argument;

						// Validate this new condition.  If the field type doesn't
						// match the argument, we punt.  If the field type doesn't
						// match the operator chosen, then we punt.
						//
						// 12/13/2004 kb - We now support blank field arguments.
						// For string fields, we compare against the empty string
						// and null.  For all other field types, we compare against
						// null to simulate looking for not-defined entries.

						if( condition.Argument.Type == E_ArgumentType.Const )
						{
							if( condition.Argument.Value != "" )
							{
								try
								{
									switch( field.Type )
									{
										case Field.E_FieldType.Boo:
										{
											Convert.ToInt32( condition.Argument.Value );
										}
										break;

										case Field.E_FieldType.Int:
										{
											Convert.ToInt32( condition.Argument.Value );
										}
										break;

										case Field.E_FieldType.Enu:
										{
											Convert.ToInt32( condition.Argument.Value );
										}
										break;

										case Field.E_FieldType.Dbl:
										{
											Convert.ToDouble( condition.Argument.Value );
										}
										break;

										case Field.E_FieldType.Dec:
										{
											Convert.ToDecimal( condition.Argument.Value );
										}
										break;

										case Field.E_FieldType.Dtm:
										{
											Convert.ToDateTime( condition.Argument.Value );
										}
										break;

										default:
										{
											if( field.Kind == Field.E_ClassType.Cal )
											{
												Convert.ToDateTime( condition.Argument.Value );
											}
										}
										break;
									}
								}
								catch
								{
									m_ExceptionDetails = "Unable to update condition.  Invalid argument format";

									return;
								}
							}
							else
							{
								if( condition.Type != E_ConditionType.Eq && condition.Type != E_ConditionType.Ne )
								{
									m_ExceptionDetails = "Unable to update condition.  Invalid argument value";

									break;
								}
							}
						}
						else
						if( condition.Argument.Type == E_ArgumentType.Field )
						{
							Field other = m_Look.LookupById( condition.Argument.Value );

							if( other == null )
							{
								m_ExceptionDetails = "Unable to update condition.  Invalid field as argument";

								break;
							}

							if( field.Type != other.Type && field.Kind != other.Kind )
							{
								m_ExceptionDetails = "Unable to update condition.  Field types mismatch";

								break;
							}
						}
						else
						{
							m_ExceptionDetails = "Unable to update condition.  Invalid argument type";

							break;
						}

						if( field.Type != Field.E_FieldType.Str && ( condition.Type == E_ConditionType.Bw || condition.Type == E_ConditionType.Nw ) )
						{
							m_ExceptionDetails = "Unable to update condition.  Operator not valid";

							break;
						}

						// 12/13/2004 kb - Moved writeback of the updated condition to
						// then end because we were saving changes that weren't final
						// (and that's not a good thing).  Now, if we're here, then
						// all checks passed.  Update requires this because we could
						// fail all over the place.
						//
						// We have overwritten an existing condition, so it should
						// get persisted as the new model state.  Mark this tree
						// as dirty.

						Condition keepCond = m_LookupTable[ parts[ 0 ] ] as Condition;
						
						keepCond.Id       = condition.Id;
						keepCond.Type     = condition.Type;
						keepCond.Argument = condition.Argument;

						if( Dirty != null )
						{
							Dirty( this , "UpdCon" );
						}
					}
					break;
				}
            }
            catch( Exception e )
            {
                // We got whacked with an error.  This needs to be
                // cleaned up.

                m_ExceptionDetails = "Failed to complete request (" + e.Message + ")";
            }
        }

        private String m_ExceptionDetails
        {
            // Access member.

            set
            {
                // Generate dynamic error not for the containing
                // page to process.

                if( value.EndsWith( "." ) == false )
                {
                    Page.ClientScript.RegisterHiddenField( "m_ErrorMessage" , value + "." );
                }
                else
                {
                    Page.ClientScript.RegisterHiddenField( "m_ErrorMessage" , value );
                }
            }
        }

        /// <summary>
        /// Node descriptions are used to contain conditions nodes.
        /// </summary>

        public class NodeDescription : TemplatedUserControl.Container
        {
            /// <summary>
            /// We reference the bound content for templating.
            /// </summary>

            private String m_Type; // node type
            private String  m_Uid; // owner id
            private String  m_Qid; // lookup id

			#region ( Data item elements )

            public string Type
            {
                // Access member.

                set
                {
                    m_Type = value;
                }
                get
                {
                    return m_Type;
                }
            }

            public string Qid
            {
                // Access member.

                set
                {
                    m_Qid = value;
                }
                get
                {
                    return m_Qid;
                }
            }

            public string Uid
            {
                // Access member.

                set
                {
                    m_Uid = value;
                }
                get
                {
                    return m_Uid;
                }
            }

			#endregion

        }

        /// <summary>
        /// Item descriptions are used to contain condition nodes.
        /// </summary>

        public class ItemDescription : TemplatedUserControl.Container
        {
            /// <summary>
            /// Data item
            /// 
            /// We reference the bound content for templating.
            /// </summary>

            private String m_Name;
            private String m_Type;
            private String m_Kind;
            private String m_ArgV;
			private String m_Data;
            private String  m_Uid;
            private String  m_Qid;

			#region ( Data item elements )

            public string Name
            {
                // Access member.

                set
                {
                    m_Name = value;
                }
                get
                {
                    return m_Name;
                }
            }

            public string Type
            {
                // Access member.

                set
                {
                    m_Type = value;
                }
                get
                {
                    return m_Type;
                }
            }

            public string Kind
            {
                // Access member.

                set
                {
                    m_Kind = value;
                }
                get
                {
                    return m_Kind;
                }
            }

			public string Valu
			{
				// Access member.

				get
				{
					return m_ArgV;
				}
			}

			public string ArgV
			{
				// Access member.

				set
				{
					m_ArgV = value;
				}
				get
				{
					if( m_Kind == "field" )
					{
						return "[" + m_ArgV + "]";
					}
					else
						if( m_Kind == "value" )
					{
						return "'" + m_ArgV + "'";
					}

					return m_ArgV;
				}
			}

			public string Data
			{
				// Access member.

				set
				{
					m_Data = value;
				}
				get
				{
					return m_Data;
				}
			}

            public string Uid
            {
                // Access member.

                set
                {
                    m_Uid = value;
                }
                get
                {
                    return m_Uid;
                }
            }

            public string Qid
            {
                // Access member.

                set
                {
                    m_Qid = value;
                }
                get
                {
                    return m_Qid;
                }
            }

			#endregion

        }

        public Query DataSource
        {
            // Access member.

            set
            {
                m_View = value;
            }
        }

        public FieldLookup DataLookup
        {
            // Access member.

            set
            {
                m_Look = value;
            }
        }

        public String CssClass
        {
            // Access member.

            set
            {
                m_Base.CssClass = value;
            }
        }

        public String Height
        {
            // Access member.

            set
            {
                m_Base.Height = Unit.Parse( value );
            }
        }

        public String Width
        {
            // Access member.

            set
            {
                m_Base.Width = Unit.Parse( value );
            }
        }

        public String ReadOnly
        {
            // Access member.

            set
            {
                if( Convert.ToBoolean( value ) != true )
                {
                    m_ReadOnly = false;
                }
                else
                {
                    m_ReadOnly = true;
                }
            }
        }

        /// <summary>
        /// Bind the specified conditions to this tree.  We use
        /// recursive helpers to walk the composite structure.
        /// </summary>

        public void BindView()
        {
            // Walk the conditions tree and add nodes as we find
            // nodes and leaves in our bound composite.

            m_LookupId = 0;

            if( m_LookupTable.Count > 0 )
            {
                m_LookupTable.Clear();
            }

            // Create a block tree node, initialize it, and
            // bind it to our tree.

            BlockTreeNode node;

            node = Bind( m_View.Relates );

            m_Tree.DataSource = node;
            m_Tree.DataBind();
        }

        private BlockTreeNode Bind( Conditions cConditions )
        {
            // Create and initialize conditions tree node.

            BlockTreeNode tn = new BlockTreeNode();

            if( cConditions == null )
            {
                return tn;
            }

            tn.Text = RenderNode( cConditions , tn.Key = Convert.ToString( ++m_LookupId ) );

            tn.Selectable = true;

            foreach( object child in cConditions )
            {
                // Recursively add the children.

                if( child is Conditions )
                {
                    tn.Add( Bind( child as Conditions ) );
                }
                else
                if( child is Condition )
                {
                    tn.Add( Bind( child as Condition ) );
                }
            }

            m_LookupTable.Add( tn.Key , cConditions );

            return tn;
        }

        private BlockTreeNode Bind( Condition cCondition )
        {
            // Create and initialize condition tree item.

            BlockTreeNode tn = new BlockTreeNode();

            if( cCondition == null )
            {
                return tn;
            }

            tn.Text = RenderItem( cCondition , tn.Key = Convert.ToString( ++m_LookupId ) );

            m_LookupTable.Add( tn.Key , cCondition );

            return tn;
        }

        /// <summary>
        /// Using the given conditions, we render an inner html string
        /// for a node in our tree.
        /// </summary>
        /// <param name="cConditions">
        /// Conditions to render.
        /// </param>
        /// <returns>
        /// String representation of templated, bound controls.
        /// </returns>

        private string RenderNode( Conditions cConditions , string luId )
        {
            // Using the given conditions, we render an inner html string
            // for a node in our tree.

            try
            {
                // Validate input conditions

                if( cConditions == null )
                {
                    return "Invalid node object";
                }

                // Initialize a new node description and bind it to
                // our conditions.

                NodeDescription nd = new NodeDescription();
                StringWriter    sw = new StringWriter();

                switch( cConditions.Type )
                {
                    case E_ConditionsType.An: nd.Type = "All must be true";
                    break;

                    case E_ConditionsType.Or: nd.Type = "Only one needs to be true";
                    break;

                    default:
                    {
                        nd.Type = "?!";
                    }
                    break;
                }

                nd.Uid = UniqueID;
                nd.Qid = luId;

                if( m_Node != null )
                {
                    if( m_ReadOnly == true && m_Read != null )
                    {
                        m_Read.Template.InstantiateIn( nd );
                    }
                    else
                    {
                        m_Node.Template.InstantiateIn( nd );
                    }
                }
                else
                if( m_Read != null )
                {
                    m_Read.Template.InstantiateIn( nd );
                }

                nd.DataBind();

                nd.RenderControl( new HtmlTextWriter( sw , " " ) );

                return sw.ToString();
            }
            catch( Exception e )
            {
                if( e is InvalidProgramException )
                {
                    throw e;
                }

                return "Failed to construct node";
            }
        }

        private string RenderHtmlString(HtmlContainerControl htmlControl)
        {
            string result = string.Empty;
            using (StringWriter sw = new StringWriter())
            {
                var writer = new System.Web.UI.HtmlTextWriter(sw);
                htmlControl.RenderControl(writer);
                result = sw.ToString();
                writer.Close();
            }
            return result;
        }

        /// <summary>
        /// Using the given condition, we render an inner html string
        /// for a node in our tree.
        /// </summary>
        /// <param name="cCondition">
        /// Condition to render.
        /// </param>
        /// <returns>
        /// String representation of templated, bound controls.
        /// </returns>

        private string RenderItem( Condition cCondition , string luId )
        {
            // Using the given condition, we render an inner html string
            // for a node in our tree.

            try
            {
                // Validate input conditions

                if( cCondition == null )
                {
                    return "Invalid item object";
                }

                // Initialize a new node description and bind it to
                // our conditions.

                ItemDescription id = new ItemDescription();
                StringWriter    sw = new StringWriter();

                id.Name = m_Look.LookupById( cCondition.Id ).Name;

                if (LoanFieldSecurityManager.IsSecureField(cCondition.Id))
                {
                    cCondition.Permissions = LoanFieldSecurityManager.SecureFieldPermissions(cCondition.Id);
                    id.Name += cCondition.Perm.ToString();
                    if (!LoanFieldSecurityManager.CanReadField(cCondition.Id))
                    {
                        HtmlContainerControl span = new HtmlGenericControl();
                        span.Attributes.Add("class", "DisabledConditionField");
                        span.InnerText = id.Name;
                        id.Name = RenderHtmlString(span);
                    }
                }

                switch( cCondition.Type )
                {
                    case E_ConditionType.Eq: id.Type = "  =";
                    break;

                    case E_ConditionType.Bw: id.Type = " =*";
                    break;

                    case E_ConditionType.Ne: id.Type = " !=";
                    break;

                    case E_ConditionType.Nw: id.Type = "!=*";
                    break;

                    case E_ConditionType.Ge: id.Type = " >=";
                    break;

                    case E_ConditionType.Gt: id.Type = "  >";
                    break;

                    case E_ConditionType.Le: id.Type = " <=";
                    break;

                    case E_ConditionType.Lt: id.Type = "  <";
                    break;

                    default:
                    {
                        id.Type = "?!";
                    }
                    break;
                }

				id.ArgV = cCondition.Argument.Value;

                if( cCondition.Argument.Type != E_ArgumentType.Field )
                {
                    id.ArgV = m_Look.LookupById( cCondition.Id ).Mapping.ByKey[ id.ArgV ];
                }
                else
                {
                    id.ArgV = m_Look.LookupById(id.ArgV).Name;
                    if (LoanFieldSecurityManager.IsSecureField(cCondition.Argument.Value))
                    {
                        string perm = FieldPermissions.GetPermissionString(LoanFieldSecurityManager.SecureFieldPermissions(cCondition.Argument.Value));
                        id.ArgV += (perm.Length == 0) ? "" : " " + perm;
                        if (!LoanFieldSecurityManager.CanReadField(cCondition.Argument.Value))
                        {
                            HtmlContainerControl span = new HtmlGenericControl();
                            span.Attributes.Add("class", "DisabledConditionField");
                            span.InnerText = id.ArgV;
                            id.ArgV = RenderHtmlString(span);
                        }
                    }
                }

                switch( cCondition.Argument.Type )
                {
                    case E_ArgumentType.Field: id.Kind = "field";
                    break;

                    case E_ArgumentType.Const: id.Kind = "value";
                    break;

                    default:
                    {
                        id.Kind = "?!";
                    }
                    break;
                }

				id.Data = String.Format( "{0}:{1}:field;{2}:{3}:match;{4}:{5}:{6}"
					, id.Name
					, cCondition.Id
					, id.Type
					, cCondition.Type
					, id.Valu
					, cCondition.Argument.Value
					, id.Kind
					);

                id.Uid = UniqueID;
                id.Qid = luId;

                if( m_Item != null )
                {
					if( m_ReadOnly == true && m_List != null )
					{
						m_List.Template.InstantiateIn( id );
					}
					else
					{
						m_Item.Template.InstantiateIn( id );
					}
				}
				else
				if( m_List != null )
				{
					m_List.Template.InstantiateIn( id );
				}

                id.DataBind();

                id.RenderControl( new HtmlTextWriter( sw , " " ) );

                return sw.ToString();
            }
            catch
            {
                return "Failed to construct item";
            }
        }

        /// <summary>
        /// Render the web control.
        /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Write out a new tree view using current conditions.

            BindView();
		}

        /// <summary>
        /// Initialize web control.
        /// </summary>

		protected void ControlInit( object sender , System.EventArgs e )
		{
            // Add our script handling code to this tree.

            m_Tree.Attributes[ "onselectedindexchange" ] = "document.all." + m_Info.ClientID + ".value = selectedNodeIndex;";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.ControlInit);
            this.PreRender += new System.EventHandler(this.ControlPreRender);

        }
		#endregion

        /// <summary>
        /// Construct default.
        /// </summary>

        public RelatesTreeUserControl()
        {
            // Initialize members.

            m_LookupTable = new Hashtable();
            m_LookupId    = 0;
        }

	}

    /// <summary>
    /// Event handling arguments.
    /// </summary>

    public class RelatesTreeEventArgs : EventArgs
    {
        /// <summary>
        /// Event args
        /// 
        /// Action is defined by this control's ui.  The
        /// target is always a conditions.
        /// </summary>

        public string     Action; // requested action
        public Conditions Target; // conditions target

        /// <summary>
        /// Construct specific event args.
        /// </summary>
        /// <param name="sAction">
        /// Action requested.
        /// </param>
        /// <param name="cTarget">
        /// Target of action.
        /// </param>

        public RelatesTreeEventArgs( string sAction , Conditions cTarget )
        {
            // Initialize members.

            Action = sAction;
            Target = cTarget;
        }

        /// <summary>
        /// Construct default event args.
        /// </summary>

        public RelatesTreeEventArgs()
        {
            // Initialize members.

            Action = "";
            Target = null;
        }

    }

    /// <summary>
    /// Delegate declaration for firing events.
    /// </summary>

    public delegate void RelatesTreeEventHandler( object sender , RelatesTreeEventArgs e );

}
