using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for FilterRangeUserControl.
	/// </summary>

    [ ControlBuilder( typeof( FilterRangeControlBuilder ) )
    ]
    [ ParseChildren( false )
    ]
	public partial  class FilterRangeUserControl : System.Web.UI.UserControl , INamingContainer
	{

        private FilterRangeStringPairArray m_Fields;
        private Query                        m_View;

        public Query DataSource
        {
            set { m_View = value; }
        }

        public string CssClass
        {
            set { m_Base.CssClass = value; }
        }

        public string Height
        {
            set
            {
                m_Base.Height = Unit.Parse( value );

                if( m_Base.Height.Type == UnitType.Pixel )
                {
                    m_Root.Height = Unit.Pixel( Convert.ToInt32( m_Base.Height.Value ) - 90 );
                }
            }
        }

        public string Entry
        {
            set
            {
                m_Root.Width = Unit.Parse( value );
            }
        }

        public string Width
        {
            set
            {
                m_Base.Width = Unit.Parse( value );
            }
        }

        public string StartDate
        {
            get
            {
                return getStartValue(a => a.SelectedItem.Text);
            }
        }

        public string FinalDate
        {
            get
            {
                return getFinalValue(a => a.SelectedItem.Text);
            }
        }

        /// <summary>
        /// Add parsed item elements to this control.
        /// </summary>

        protected override void AddParsedSubObject( object obj )
        {
            // Stash the field set for list binding.

            FilterRangeFields fields = obj as FilterRangeFields;

            if( fields != null )
            {
                foreach( object item in fields.Controls )
                {
                    FilterRangeItem field = item as FilterRangeItem;

                    if( field != null )
                    {
                        m_Fields.Add( field.Label , field.Key );
                    }
                }

                return;
            }

            // Only add macro groups.

            FilterRangeMacros macros = obj as FilterRangeMacros;

            if( macros != null )
            {
                foreach( object item in macros.Controls )
                {
                    FilterRangeItem macro = item as FilterRangeItem;

                    if( macro == null )
                    {
                        continue;
                    }

                    if( macros.For == "Start" )
                    {
                        m_StartList.Items.Add( new ListItem( macro.Label , macro.Key ) );
                    }
                    else
                    if( macros.For == "End" )
                    {
                        m_FinalList.Items.Add( new ListItem( macro.Label , macro.Key ) );
                    }
                }

                return;
            }

            // Unknown, add default.

            base.AddParsedSubObject( obj );
        }

        private string getStartValue(Func<DropDownList, string> GetVal)
        {
            string start = null;
            if (m_StartUseL.Checked == true)
            {
                start = GetVal(m_StartList);
            }
            else if (m_StartUseD.Checked == true)
            {
                start = m_StartDate.Text;

                if (start == "")
                {
                    start = null;
                }
            }

            return start;
        }

        private string getFinalValue(Func<DropDownList, string> GetVal)
        {
            string final = null;
            if (m_FinalUseL.Checked == true)
            {
                final = GetVal(m_FinalList);
            }
            else if (m_FinalUseD.Checked == true)
            {
                final = m_FinalDate.Text;

                if (final == "")
                {
                    final = null;
                }
            }
            return final;
        }

        /// <summary>
        /// Bind the data to the current ui status.
        /// </summary>

        public void BindData()
        {
            // Search for our condition group that contains
            // a date range (possibly).  If not found, we
            // will add one if we need to.  If no range is
            // currently set, then we remove date pieces.

            ArrayList groups = new ArrayList();
            String     start = null;
            String     final = null;


            start = getStartValue(a => a.SelectedItem.Value);
            final = getFinalValue(a => a.SelectedItem.Value);

			groups.Add( m_View.Filters );

			foreach( Object item in m_View.Filters.Walk )
			{
				Conditions conditions = item as Conditions;

				if( conditions == null )
				{
					continue;
				}

				groups.Add( item );
			}

			// Start by cleaning up any existing conditions from
            // this group.  We re-add the stored range in the
            // next step.  As we clean up, we want to find the
			// condition group that will house these date ranges
			// and the best way to do that is to find an empty
			// or-group or a group that currently contains one
			// of our fields in a date range comparison.

			Conditions host = null;

			foreach( Conditions conditions in groups )
			{
				// Before we update the data model with latest
				// client state, we make sure we clear out any
				// conditions that pertain to old ranges.

				for( int i = 0 , n = conditions.Count ; i < n ; ++i )
				{
					// Get next item and validate it as condition.
					// If it is one of ours by field id, then nix
					// it from our set.

					Condition c = conditions[ i ] as Condition;

					if( c == null )
					{
						continue;
					}

					if( c.Type == E_ConditionType.Ge || c.Type == E_ConditionType.Le )
					{
						foreach( FilterRangeStringPair pair in  m_Fields )
						{
							if( c.Id == pair.Key )
							{
								conditions.Nix( c );

								--i;
								--n;
							}
						}
					}
				}
			}

			foreach( Object item in m_View.Filters )
			{
				// We now search for the collection that shall hold
				// our date ranges.  We want the conditions collection
				// that only has empty and-conditions as children.

				Conditions conditions = item as Conditions;

				if( conditions != null && conditions.Type == E_ConditionsType.Or )
				{
					host = conditions;

					foreach( Object child in conditions )
					{
						Conditions c = child as Conditions;

						if( c == null || c.Count != 0 || c.Type != E_ConditionsType.An )
						{
							host = null;

							break;
						}
					}

					if( host != null )
					{
						while( host.Count > 0 )
						{
							Conditions c = host[ 0 ] as Conditions;

							host.Nix( c );
						}

						break;
					}

					host = null;
				}
			}

            // If we have a complete specification, let's add a
            // new range, even if only one edge is given.

            if( m_Active.Checked == true && ( start != null || final != null ) )
            {
                // If the condition group needs to be added, do
                // it here as an or-type group.

                if( host == null )
                {
                    m_View.Filters.Add( host = new Conditions( E_ConditionsType.Or ) );
                }

                // Define the starting end of the range if specified.

				Argument sArg = new Argument( E_ArgumentType.Const , start );

				foreach( ListItem item in m_StartList.Items )
				{
					if( sArg.Value == item.Value )
					{
						sArg.Type = E_ArgumentType.Field;

						break;
					}
				}

                // Define the final end of the range if specified.

				Argument fArg = new Argument( E_ArgumentType.Const , final );

                foreach( ListItem item in m_FinalList.Items )
                {
                    if( fArg.Value == item.Value )
                    {
                        fArg.Type = E_ArgumentType.Field;

                        break;
                    }
                }

				// Now, add the range for each field specified
				// in the store list of selected fields.

				foreach( String sField in m_Store.Value.Split( '-' ) )
				{
					if( sField != "" )
					{
						Conditions range = new Conditions( E_ConditionsType.An );

						if( sArg.Value != "" )
						{
							range.Add( E_ConditionType.Ge , sField , sArg );
						}

						if( fArg.Value != "" )
						{
							range.Add( E_ConditionType.Le , sField , fArg );
						}

						if( range.Count > 0 )
						{
							host.Add( range );
						}
					}
				}
            }
        }


        /// <summary>
        /// Bind associated data elements to our ui.
        /// </summary>

        public void BindView()
        {
            // We search for filter conditions that show a range.
            // We use the current state within the filter to
            // determine our ui state.
            
            if( IsPostBack == true )
            {
                BindData();
            }

            m_List.DataSource = m_Fields;
            m_List.DataBind();
        }

        /// <summary>
        /// Bind the range parameters to our filter.
        /// </summary>

        public void HoldView()
        {
            // The embedded client state needs to be updated to
            // reflect latest and greatest (remember, the data
            // model may have changed outside of this component
            // during overall event processing).  We find the
            // current range parameters from the filters tree.

            ArrayList groups = new ArrayList();
            String     start = null;
            String     final = null;

            m_List.DataSource = m_Fields;
            m_List.DataBind();

			m_Store.Value = "";

			// Gather all the children buckets so we can look
			// for the one that contains our date ranges.  It
			// is important to minimize our assumptions about
			// ordering of the condition lists in this set.

			foreach( Object item in m_View.Filters )
			{
				Conditions c = item as Conditions;

				if( c == null )
				{
					continue;
				}

				if( c.Type == E_ConditionsType.Or )
				{
					groups.Add( c );
				}
			}

			foreach( Conditions conditions in groups )
			{
				// Walk the conditions and find both ends of a valid
				// date range.  We take either, and the range must
				// must use a field from the current field items
				// set -- both endpoints must use the same field.
				//
				// 3/23/2005 kb - I need to comment that we've made
				// a lot of changes to support multi-selecting date
				// ranges and or-ing them together.  Well, to do it
				// right, you need each range and-ed together, then
				// gather all these groups and or them together.
				// We just lookup whatever we find at this point.

                foreach( Object item in conditions.Walk )
                {
                    // Get next item and validate it as condition.
                    // If it is one of ours by field id, then pull
                    // out the information.

                    Condition c = item as Condition;

                    if( c == null )
                    {
                        continue;
                    }

                    if( c.Type == E_ConditionType.Ge || c.Type == E_ConditionType.Le )
                    {
                        foreach( FilterRangeStringPair pair in m_Fields )
                        {
                            if( c.Id == pair.Key )
                            {
                                if( c.Type == E_ConditionType.Ge )
                                {
                                    start = c.Argument.Value;
                                }
                                else
                                {
                                    final = c.Argument.Value;
                                }

								if( m_Store.Value.IndexOf( "-" + c.Id + "-" ) == -1 )
								{
									m_Store.Value += "-" + c.Id + "-";
								}

                                break;
                            }
                        }
                    }
                }
            }

            // If we have all the pieces to visually set the
            // range, then update the ui.  Otherwise, skip it
            // and deactivate the ui.

            if( start != null || final != null )
            {
                int i , n;

                if( start == null )
                {
                    start = "";
                }

                if( final == null )
                {
                    final = "";
                }

                for( i = 0 , n = m_StartList.Items.Count ; i < n ; ++i )
                {
                    if( m_StartList.Items[ i ].Value == start )
                    {
                        m_StartList.SelectedIndex = i;
                        m_StartUseL.Checked = true;

						break;
                    }
                }

                if( i == n )
                {
                    m_StartDate.Text    = start;
                    m_StartUseD.Checked = true;
                }

                for( i = 0 , n = m_FinalList.Items.Count ; i < n ; ++i )
                {
                    if( m_FinalList.Items[ i ].Value == final )
                    {
                        m_FinalList.SelectedIndex = i;
                        m_FinalUseL.Checked = true;

						break;
                    }
                }

                if( i == n )
                {
                    m_FinalDate.Text    = final;
                    m_FinalUseD.Checked = true;
                }

                m_Active.Checked = true;
            }
            else
            {
                // Turn off the panel.

                m_StartList.SelectedIndex = 0;
                m_StartDate.Text = "";

                m_FinalList.SelectedIndex = 0;
                m_FinalDate.Text = "";

                m_StartUseL.Checked = true;
                m_FinalUseL.Checked = true;

                m_Active.Checked = false;
            }
        }

        /// <summary>
        /// Render this control's state.
        /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Render bound ui elements.

            HoldView();
		}

        /// <summary>
        /// Initialize this control.
        /// </summary>

		protected void ControlLoad( object sender , System.EventArgs e )
		{
            // Make sure our data model matches the embedded
            // state according to the client just before
            // postback.  If this is the first load, we'll
            // have already bound the embedded view with
            // the starting data model.

            if( IsPostBack == true )
            {
                BindData();
            }
		}

        /// <summary>
        /// Initialize this control.
        /// </summary>

		protected void ControlInit( object sender , System.EventArgs e )
        {
            // Tell the server renderer that we postback function.

            Page.ClientScript.GetPostBackEventReference( this, "" );
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.ControlInit);
            this.Load += new System.EventHandler(this.ControlLoad);
            this.PreRender += new System.EventHandler(this.ControlPreRender);

        }
		#endregion

        /// <summary>
        /// Construct default.
        /// </summary>

        public FilterRangeUserControl()
        {
            // Initialize members.

            m_Fields = new FilterRangeStringPairArray();
        }

	}

    /// <summary>
    /// Provide item listing template for predefining combo
    /// box drop down set.
    /// </summary>

    public class FilterRangeControlBuilder : ControlBuilder
    {
        /// <summary>
        /// Override child control filter and provide implementation
        /// when items are built.
        /// </summary>

        public override Type GetChildControlType( string tagName , IDictionary attributes )
        {
            if( tagName == "FIELDS" || tagName == "Fields" || tagName == "fields" )
            {
                return typeof( FilterRangeFields );
            }
            else
            if( tagName == "MACROS" || tagName == "Macros" || tagName == "macros" )
            {
                return typeof( FilterRangeMacros );
            }

            return null;
        }

    }

    /// <summary>
    /// Provide item listing template for predefining combo
    /// box drop down set.
    /// </summary>

    public class FilterRangeItemControlBuilder : ControlBuilder
    {
        /// <summary>
        /// Override child control filter and provide implementation
        /// when items are built.
        /// </summary>

        public override Type GetChildControlType( string tagName , IDictionary attributes )
        {
            if( tagName == "ITEM" || tagName == "Item" || tagName == "item" )
            {
                return typeof( FilterRangeItem );
            }

            return null;
        }

    }

    /// <summary>
    /// We provide a collection node within the sub tags.
    /// </summary>

    [ ControlBuilder( typeof( FilterRangeItemControlBuilder ) )
    ]
    [ ParseChildren( false )
    ]
    public class FilterRangeFields : Control , INamingContainer
    {
        /// <summary>
        /// Construct default.
        /// </summary>

        public FilterRangeFields()
        {
        }

    }

    /// <summary>
    /// We provide a collection node within the sub tags.
    /// </summary>

    [ ControlBuilder( typeof( FilterRangeItemControlBuilder ) )
    ]
    [ ParseChildren( false )
    ]
    public class FilterRangeMacros : Control , INamingContainer
    {
        private String m_For; // range end designator

        public string For
        {
            set { m_For = value; }
            get { return m_For; }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public FilterRangeMacros()
        {
            // Initialize members.

            m_For = "";
        }

    }

    /// <summary>
    /// Provide container of custom children controls.
    /// </summary>

    public class FilterRangeItem : Control , INamingContainer
    {
        /// <summary>
        /// Data item
        /// 
        /// We reference the bound content for templating.
        /// </summary>

        private String m_Label; // displayed description
        private String   m_Key; // clicked identifier

        public string Label
        {
            set { m_Label = value; }
            get { return m_Label; }
        }

        public string Key
        {
            set { m_Key = value; }
            get { return m_Key; }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public FilterRangeItem()
        {
            // Initialize members.

            m_Label = "";
            m_Key   = "";
        }

    }

    /// <summary>
    /// Maintain drop down bindings.
    /// </summary>

    public class FilterRangeStringPair
    {
        /// <summary>
        /// Simple association
        /// 
        /// We maintain a labeled key binding for each entry
        /// in the drop down list.
        /// </summary>

        public string m_Label = ""; // entry label
        public string   m_Key = ""; // entry id

        public string Label
        {
            set { m_Label = value; }
            get { return m_Label; }
        }

        public string Key
        {
            set { m_Key = value; }
            get { return m_Key; }
        }

        /// <summary>
        /// Construct specified pair.
        /// </summary>
        /// <param name="sLabel">
        /// Label to display.
        /// </param>
        /// <param name="sKey">
        /// Key to track.
        /// </param>

        public FilterRangeStringPair( string sLabel , string sKey )
        {
            // Initialize members.

            m_Label = sLabel;
            m_Key   = sKey;
        }

        /// <summary>
        /// Construct specified pair.
        /// </summary>
        /// <param name="sLabel">
        /// Label to display.
        /// </param>

        public FilterRangeStringPair( string sLabel )
        {
            // Initialize members.

            m_Label = sLabel;
        }

    }

    /// <summary>
    /// Maintain drop down bindings.
    /// </summary>

    public class FilterRangeStringPairArray : IEnumerable
    {
        private ArrayList m_Set = new ArrayList(); // set of pairs

        public FilterRangeStringPair this[ int iPair ]
        {
            get { return m_Set[ iPair ] as FilterRangeStringPair; }
        }

        public int Count
        {
            get { return m_Set.Count; }
        }

        /// <summary>
        /// Append to this set.
        /// </summary>
        /// <param name="sLabel">
        /// Label to display.
        /// </param>
        /// <param name="sKey">
        /// Key to track.
        /// </param>

        public void Add( string sLabel , string sKey )
        {
            // Append to this set.

			m_Set.Add( new FilterRangeStringPair( sLabel , sKey ) );
        }

        /// <summary>
        /// Append to this set.
        /// </summary>
        /// <param name="sLabel">
        /// Label to display.
        /// </param>

        public void Add( string sLabel )
        {
            // Append to this set.

            m_Set.Add( new FilterRangeStringPair( sLabel ) );
        }

        /// <summary>
        /// Nix all entries.
        /// </summary>

        public void Clear()
        {
            // Nix all entries.

            m_Set.Clear();
        }

        /// <summary>
        /// Expose iteration interface.
        /// </summary>
        /// <returns>
        /// Returns enumerator.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Access iterator.

            return m_Set.GetEnumerator();
        }

    }

}
