<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="FieldLayoutUserControl.ascx.cs"
    Inherits="LendersOffice.QueryProcessor.FieldLayoutUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Panel ID="m_Base" runat="server">
    <script>
        function chooseListItem(oContainer, sFieldId) {
            try {
                // Validate object set.

                if (oContainer == null || sFieldId == null) {
                    return;
                }

                // Delegate to all listening handlers.

                if (fireReportEvent != null) {
                    fireReportEvent("select", sFieldId, oContainer);
                }
            }
            catch (e) {
                window.status = e.message;
            }
        }

        function includeListItem(oContainer, oStore, sFieldId) {
            try {
                // Validate the container and field.

                if (oContainer.getElementsByTagName("*").length == 0 || oStore.value == null || sFieldId == null) {
					return;
                }

                // Parse out id parts.  We construct a line item from
                // scratch using the template as our guide.

                var id, name;
                var parts;
                if (typeof (stringSplitRemainder) != 'undefined')
                {
                    parts = stringSplitRemainder(sFieldId, ":", 2);
                }
                else
                {
                    parts = sFieldId.split(":");
                }

                if (parts.length != 2) {
                    return;
                }

                if (oStore.value.indexOf("-" + parts[0] + "-") != -1) {
                    return;
                }
                
                var h = hypescriptDom;
                name = parts[1];
                id = parts[0];
                
                $(oContainer).find('.FieldLayoutList').append(h("li", {attrs:{kind:"item", key:id}, className:(id+" FieldLayoutListItem")}, [
                    h("input", {type:"hidden", className:"FieldId", value:id}),
                    h("span", {className:"Clickable FieldLayoutLinkCommand Up"  , attrs:{key:id, href:"#"}, innerHTML:"&#x25b2;"}),
                    " ",
                    h("span", {className:"Clickable FieldLayoutLinkCommand Down", attrs:{key:id, href:"#"}, innerHTML:"&#x25bc;"}),
                    " ",
                    h("a", {href:"#", className:"FieldLayoutLinkCommand", attrs:{key:id}, onclick:function(){
                        this.className = 'FieldLayoutLinkCommand';
                        removeListItem(oContainer, oStore, id);
                    }}, "remove"),
                    " ",
                    h("span", {className:"Label", attrs:{key:id}}, name),
                ]));

                oStore.value += "-" + id + "-";

                // Delegate to all listening handlers.

                if (fireReportEvent != null) {
                    fireReportEvent("select", id, oContainer);
                }

                // We now have a dirty report.

                if (markReportAsDirty() != null) {
                    markReportAsDirty();
                }
            }
            catch (e) {
                alert(window.status = e.message);
            }
        }

        function removeListItem(oContainer, oStore, sFieldId) {
            try {
                // Validate the container and field.

                if (oContainer.getElementsByTagName("*").length == 0 || oStore.value == null || sFieldId == null) {
                    return;
                }

                // Walk the children and swap the current
                // entry with the previous that matches.

                var i;
                var nRestricted = 0;
                var all = oContainer.getElementsByTagName("*");
                for (i = 0; i < all.length; ++i) {
                    // Get current child and check if it is
                    // a sort list item.

                    var curr = all[i];
                    var kind = curr.getAttribute("kind");
                    var key = curr.getAttribute("key");
                    var access = curr.getAttribute("access");

                    if (key != null && kind == "item") {
                        if (access == "false" && key != sFieldId) {
                            nRestricted++;
                        }
                        if (key == sFieldId) {
                            curr.remove ? curr.remove() : curr.removeNode(true);
                            oStore.value = oStore.value.replace("-" + sFieldId + "-", "");
                        }
                    }
                }

                if (nRestricted > 0) {
                    g_SearchRestrictedFields = nRestricted;
                }

                // Raise the event for other controls.

                if (fireReportEvent != null) {
                    fireReportEvent("remove", sFieldId, oContainer);
                }

                // We now have a dirty report.

                if (markReportAsDirty() != null) {
                    markReportAsDirty();
                }
            }
            catch (e) {
                window.status = e.message;
            }
        }

        jQuery(function($) {
            var container = $('#<%= AspxTools.ClientId(m_Root) %>');
            container.find('.FieldLayoutList').sortable({
                stop: function(event, ui) {
                    SaveOrder();
                },
                axis: 'y',
                distance: 5
            });

            var listEvents =
            {
                'mousedown': function() {
                    $(this).parents('li').addClass('FieldLayoutMarkItem').siblings().removeClass('FieldLayoutMarkItem');
                    var itemId = $(this).siblings('.FieldId').val();
                    $("#<%= AspxTools.ClientId(m_CurrF) %>").val(itemId);
                    fireEvent("select", itemId);
                }
            };
            container.find('.FieldLayoutList').on(listEvents, 'li span.Label');

            var upEvents =
            {
                'click': function() {
                    var elem = $(this).parent();
                    elem.insertBefore(elem.prev());

                    SaveOrder();
                }
            }
            container.find('.FieldLayoutList').on(upEvents, 'li span.FieldLayoutLinkCommand.Up');

            var downEvents =
            {
                'click': function() {
                    var elem = $(this).parent();
                    elem.insertAfter(elem.next());

                    SaveOrder();
                }
            }
            container.find('.FieldLayoutList').on(downEvents, 'li span.FieldLayoutLinkCommand.Down');

            function GetOrder() {
                var results = [];
                container.find('.FieldLayoutList li').each(function() {
                    var id = $(this).find('.FieldId').val();
                    results.push(id);
                });

                return results;
            }

            function SerializeOrder(toSerialize) {
                var results = [];

                $.each(toSerialize, function(idx, id) {
                    results.push('-');
                    results.push(id);
                    results.push('-');
                });

                return results.join('');
            }

            function SaveOrder() {
                var current = GetOrder();
                var serialized = SerializeOrder(current);
                $('#<%= AspxTools.ClientId(m_Store) %>').val(serialized);
                if (markReportAsDirty() != null) {
                    markReportAsDirty();
                }
                fireEvent("listupdated", current);

                return current;
            }

            function UpdateListOrder(newOrder) {
                var sorted = [];
                var elems = container.find('.FieldLayoutList');
                $.each(newOrder, function(idx, fieldId) {
                    sorted.push(elems.find('li.' + fieldId)[0]);
                });

                $(sorted).detach();
                container.find('.FieldLayoutList').append(sorted);
                SaveOrder();
            }

            function fireEvent(eventName, eventParam) {
                if (!fireReportEvent) return;

                fireReportEvent(eventName, eventParam, container[0]);
            }

            if (registerForReportEvent != null) {
                var root = $("#<%= AspxTools.ClientId(m_Root) %>")[0];
                var curr = $("#<%= AspxTools.ClientId(m_CurrF) %>")[0];
                var stor = $("#<%= AspxTools.ClientId(m_Store) %>")[0];

                registerForReportEvent(root, stor, includeListItem, "include");
                registerForReportEvent(container[0], stor, removeListItem, "remove");

                registerForReportEvent(container[0], curr,
                function(base, currentFieldInput, fieldid) {
                    $(base).find('.' + fieldid + ' span.Label').trigger('mousedown');
                }, "select");

                registerForReportEvent(container[0], stor,
                function(base, storage, newOrder) {
                    UpdateListOrder(newOrder);
                }, "listupdated");

            }
        });

    </script>

    <input type="hidden" id="m_CurrF" name="m_CurrF" runat="server" value="">
    <input type="hidden" id="m_Store" name="m_Store" runat="server" value="">
    <asp:Panel ID="m_Root" runat="server" Style="width: 386; border: 1px groove; margin: 4px;
        padding-top: 0px; padding-bottom: 0px; background-color: white; overflow-y: auto;">
        <ol class="FieldLayoutList OrderList">
            <asp:Repeater ID="m_List" runat="server" EnableViewState="False">
                <ItemTemplate>
                    <li kind="item" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' class='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %> <%# AspxTools.HtmlString(RenderClassName( Container.DataItem )) %>'
                        access=<%# AspxTools.HtmlAttribute(RenderPermission( Container.DataItem )) %> >
                        <input type="hidden" class="FieldId" value='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' />
                        <span class="Clickable FieldLayoutLinkCommand Up" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>'>&#x25b2;</span>
                        <span class="Clickable FieldLayoutLinkCommand Down" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>'>&#x25bc;</span>

                        <a href="#" class="FieldLayoutLinkCommand" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>'
                            onclick="this.className = 'FieldLayoutLinkCommand';  removeListItem( <%# AspxTools.JsStringUnquoted(m_Root.ClientID) %> , <%# AspxTools.JsStringUnquoted(m_Store.ClientID) %> , this.getAttribute('key') );">
                            remove</a>

                        <span key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' class="Label">
                                <%#AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "NamePerm" ))%>
                            </span>
                      </li>
                </ItemTemplate>
            </asp:Repeater>
        </ol>
    </asp:Panel>

    <script>
        window.<%= AspxTools.ClientId(m_Store) %> = document.getElementById("<%= AspxTools.ClientId(m_Store) %>");
    </script>
</asp:Panel>
