using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for OptionEntryUserControl.
	/// </summary>

	public partial  class OptionEntryUserControl : System.Web.UI.UserControl , INamingContainer
	{
        protected LendersOffice.QueryProcessor.OperatorSetUserControl m_OSet;
        protected LendersOffice.QueryProcessor.ChooseEntryUserControl m_Valu;
        protected LendersOffice.QueryProcessor.SearchListUserControl  m_List;

        public String Height
        {
            // Access member.

            set
            {
                m_Wall.Height = Unit.Parse( value );

                m_List.Height = value;
                m_OSet.Height = value;
                m_Valu.Height = value;
            }
        }

        public String Width
        {
            // Access member.

            set
            {
                m_Wall.Width = Unit.Parse( value );

                m_List.Width = value;
                m_OSet.Width = value;
                m_Valu.Width = value;
            }
        }

        /// <summary>
        /// Expose repeater list data-binding interface.
        /// </summary>

        public FieldLookup DataLookup
        {
            // Access member.

            set
            {
                m_List.DataLookup = value;
                m_Valu.DataSource = value;
            }
        }

        /// <summary>
        /// Expose repeater list data-binding interface.
        /// </summary>

        public Query DataSource
        {
            // Access member.

            set
            {
				m_List.DataSource = value;
            }
        }

        /// <summary>
        /// Bind the specified option entry boxes to the schema.
        /// </summary>

        public void BindView()
        {
        }

        /// <summary>
        /// Render the web control.
        /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Bind the current data state to the ui.

            BindView();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PreRender += new System.EventHandler(this.ControlPreRender);
		}
		#endregion

	}

}
