using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using LendersOffice.Admin;
using LendersOffice.Common;

using LendersOffice.Security;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for FieldFnGridControl.
	/// </summary>

	public partial  class FieldFnGridUserControl : System.Web.UI.UserControl , INamingContainer
	{
        private LendersOffice.QueryProcessor.Query            m_View;
        private LendersOffice.QueryProcessor.FieldLookup      m_Look;

        public Boolean IsActive( string sCode , object sId )
        {
            // Check query state.

            Column column = m_View.Columns.Find( sId.ToString() );

            if( column == null )
            {
                return false;
            }

            // Lookup up functions and ordering membership.

            bool res = false;

            switch( sCode )
            {
                case "Av": res = column.Functions.Has( E_FunctionType.Av );
                break;

                case "Mn": res = column.Functions.Has( E_FunctionType.Mn );
                break;

                case "Mx": res = column.Functions.Has( E_FunctionType.Mx );
                break;

                case "Sm": res = column.Functions.Has( E_FunctionType.Sm );
                break;

				case "Ct": res = column.Functions.Has( E_FunctionType.Ct );
				break;
			
				case "St":
                {
                    if( m_View.Sorting.Has( column.Id ) )
                    {
                        res = true;
                    }
                }
                break;

                case "Gb":
                {
                    if( m_View.GroupBy.Has( column.Id ) )
                    {
                        res = true;
                    }
                }
                break;
            }

            return res;
        }

        public String Selection
        {
            // Access member.

            set
            {
                m_CurrF.Value = value;
            }
            get
            {
                return m_CurrF.Value;
            }
        }

        public Query DataSource
        {
            // Access member.

            set
            {
                m_View = value;
            }
        }

        public FieldLookup DataLookup
        {
            // Access member.

            set
            {
                m_Look = value;
            }
        }

        public String CssClass
        {
            // Access member.

            set
            {
                m_Base.CssClass = value;
                m_Head.CssClass = value;
            }
        }

        public String Height
        {
            // Access member.

            set
            {
                m_Base.Height = Unit.Parse( value );
            }
        }

        public String Width
        {
            // Access member.

            set
            {
                m_Base.Width = Unit.Parse( value );
                m_Head.Width = Unit.Parse( value );
            }
        }

        /// <summary>
        /// Bind the data to the current ui status.
        /// </summary>

        public void BindData()
        {
            // Reorder the layout list to match that of
            // the data store, but only if the store is
            // valid (i.e., only on postback).  The list
			// now contains new fields that may not exist
			// in the persisted view instance.  We may
			// need to add them.

            if( IsPostBack == true )
            {
				ArrayList included = new ArrayList();

                m_View.Sorting.Clear();
                m_View.GroupBy.Clear();

                foreach( string field in m_Store.Value.Split( new[] {'-'}, StringSplitOptions.RemoveEmptyEntries ) )
                {
                    // Throw out invalid entries.

					string[] parts = field.Split( ':' );

                    if( parts.Length != 2 )
                    {
                        continue;
                    }

                    // Push the column to the end.  Create a new
					// one and add it if this is the first time
					// we've heard about it.

                    Column column = m_View.Columns.Find( parts[ 0 ] );

					if( column == null )
					{
						try
						{
							column = new Column( m_Look.LookupById( parts[ 0 ] ) , true );
						}
						catch( Exception e )
						{
							throw new ArgumentException( "Unable to add new column to grid." , parts[ 0 ] , e );
						}

						if( column.Id != parts[ 0 ] )
						{
							// Big bug.  This should never happen, unless invalid
							// fields were added to the ui's list, or the ids became
							// corrupted in the event processing.

							continue;
						}

						m_View.Columns.Add( column );
					}
					else
					{
						m_View.Columns.Nix( column );
						m_View.Columns.Add( column );
					}

					// Build up the function list.  Each field has a set of
					// function options associated with it.  We clear the
					// column's list and then reconstruct it.  One thing to
					// watch out for is that we don't mangle the order of
					// the sorting or group by lists.  Because they filter
					// out duplicates, any additions that already exist have
					// no side effect.  If the list is empty, or needs to be
					// rearranged by another user control that maintains the
					// order, then it will be accomplished after we add to it,
					// so the order will still be okay for rendering.

					column.Functions.Clear();

					foreach( string func in parts[ 1 ].Split( ',' ) )
					{
						switch( func )
						{
							case "Av": column.Functions.Add( E_FunctionType.Av );
							break;

							case "Mn": column.Functions.Add( E_FunctionType.Mn );
							break;

							case "Mx": column.Functions.Add( E_FunctionType.Mx );
							break;

							case "Sm": column.Functions.Add( E_FunctionType.Sm );
							break;

							case "Ct": column.Functions.Add( E_FunctionType.Ct );
							break;
						
							case "St":
							{
								m_View.Sorting.Add( column.Id );
							}
							break;

							case "Gb":
							{
								m_View.GroupBy.Add( column.Id );
							}
							break;
						}
					}

					// Remember the listed fields so we can quickly filter
					// out those that were removed.

					included.Add( column.Id );
                }

                for( int i = 0 ; i < m_View.Columns.Count ; ++i )
                {
                    // If not in our included list, then it shouldn't be in
					// the data object.  The client-side updates now drive
                    // the changes to the underlying content.  If multiple
					// controls try to do this same operation, we want the
					// first to succeed, and the others to not need to.

                    Column column = m_View.Columns[ i ];

                    if( column.Show == true && included.Contains( column.Id ) == false )
                    {
                        m_View.Columns.Nix( column );

                        --i;
                    }
                }
            }
        }

        public void BindCommands(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item;
            if (item.ItemType == ListItemType.Item ||
                item.ItemType == ListItemType.AlternatingItem ||
                item.ItemType == ListItemType.SelectedItem)
            {
                var Commands = (Repeater)item.FindControl("m_Cmds");
                Commands.DataSource = new[] { "Av", "Mn", "Mx", "Sm", "St", "Gb"};
                Commands.DataBind();
            }
        }

        /// <summary>
        /// Bind the report layout to our list.
        /// </summary>

        public void BindView(BrokerDB brokerDB)
        {
            // Use the current layout and list the ordered
            // entries.

			if( IsPostBack == false )
			{
				HoldView(brokerDB);
			}
        }

        /// <summary>
        /// Bind the report layout to our list.
        /// </summary>

		public void HoldView(BrokerDB brokerDB)
		{
			// Use the current sorting and list the ordered
			// entries.  We now map the model's state back onto
			// the cache store, as if for the first time, to
			// accommodate any server-side event handling that
			// may have changed the model's state.  I know this
			// is redundant, but it is the cleanest way to get
			// separation of concerns so that all these independent
			// user controls that edit the same query can work
			// together.

            var shown = new List<Column>();

			if( m_View != null && m_Look != null )
			{
				m_Store.Value = "";

				foreach( Column column in m_View.Columns )
				{
					string functions = "";

					if( column.Show == false )
					{
						continue;
					}

					foreach( Function func in column.Functions )
					{
						if( functions.Length > 0 )
						{
							functions += ",";
						}

						switch( func.Type )
						{
							case E_FunctionType.Av: functions += "Av";
							break;

							case E_FunctionType.Mn: functions += "Mn";
							break;

							case E_FunctionType.Mx: functions += "Mx";
							break;

							case E_FunctionType.Sm: functions += "Sm";
							break;

							case E_FunctionType.Ct: functions += "Ct";
							break;
						}
					}

					if( m_View.Sorting.Has( column.Id ) == true )
					{
						if( functions.Length > 0 )
						{
							functions += ",";
						}

						functions += "St";
					}

					if( m_View.GroupBy.Has( column.Id ) == true )
					{
						if( functions.Length > 0 )
						{
							functions += ",";
						}

						functions += "Gb";
					}

					m_Store.Value += ""
						+ "-"
						+ column.Id
						+ ":"
						+ functions
						+ "-"
						;
				}

				foreach( Column column in m_View.Columns )
				{
					// Add listing fields in order.

                    if (column.Show == false || brokerDB.IsCustomFieldGloballyDefined(column.Id))
					{
						continue;
					}

					shown.Add( column );
				}
			}

			m_List.DataSource = shown;
            m_List.DataBind();
        }

	    /// <summary>
	    ///	Bind data and render lists.
	    /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Use current columns to list report.
            BrokerDB brokerDB = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;
            HoldView(brokerDB);
		}

	    /// <summary>
	    ///	Initialize this control.
	    /// </summary>

		protected void ControlLoad( object sender , System.EventArgs e )
		{
            // Update the data elements according
            // to the state of the event store.
            // When we bind the view, the changes
            // that we make here should already be
            // in effect.

            BindData();
		}

	    /// <summary>
	    ///	Initialize this control.
	    /// </summary>

		protected void ControlInit( object sender , System.EventArgs e )
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();

		    var Page = this.Page as BasePage;

            Page.EnableJqueryMigrate = false;
            Page.RegisterJsScript("jquery-ui-1.11.4.min.js");
            Page.RegisterCSS("jquery-ui-1.11.custom.css");
            
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.ControlInit);
            this.Load += new System.EventHandler(this.ControlLoad);
            this.PreRender += new System.EventHandler(this.ControlPreRender);
        }
		#endregion

	}

    /// <summary>
    /// Event handling arguments.
    /// </summary>

    public class FieldFnGridEventArgs : EventArgs
    {
        /// <summary>
        /// Event args
        /// 
        /// Action is defined by this control's ui.  The
        /// target is always a field id.
        /// </summary>

        public string Action; // requested action
        public string Target; // field target id

        /// <summary>
        /// Construct specific event args.
        /// </summary>
        /// <param name="sAction">
        /// Action requested.
        /// </param>
        /// <param name="fTarget">
        /// Target of action.
        /// </param>

        public FieldFnGridEventArgs( string sAction , string sTarget )
        {
            // Initialize members.

            Action = sAction;
            Target = sTarget;
        }

        /// <summary>
        /// Construct default event args.
        /// </summary>

        public FieldFnGridEventArgs()
        {
            // Initialize members.

            Action = "";
            Target = "";
        }

    }

    /// <summary>
    /// Delegate declaration for firing events.
    /// </summary>

    public delegate void FieldFnGridEventHandler( object sender , FieldFnGridEventArgs e );

}
