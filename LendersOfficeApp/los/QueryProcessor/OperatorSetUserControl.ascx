<%@ Control Language="c#" AutoEventWireup="false" Codebehind="OperatorSetUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.OperatorSetUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<STYLE>

    .operatorheader
    { PADDING: 4px
    ; PADDING-LEFT: 4px
    ; MARGIN-BOTTOM: 0px
    ; MARGIN-RIGHT: 12px
    ; MARGIN-TOP: 1px
    ; BORDER: none
    ; FONT: bold 11px arial
    ; WIDTH: 100%
    ; COLOR: black
    }

	.operatorcommandlink
	{ COLOR: tomato
	}

    .operatorlabel
    { PADDING: 1px
    ; PADDING-LEFT: 4px
    ; MARGIN-BOTTOM: 1px
    ; MARGIN-TOP: 1px
    ; FONT: 11px arial
    ; COLOR: black
    }

    .operatorsymbol
    { PADDING: 1px
    ; PADDING-LEFT: 4px
    ; MARGIN-BOTTOM: 1px
    ; MARGIN-TOP: 1px
    ; FONT: 11px arial
    ; COLOR: black
    }

</STYLE>
<ASP:Panel id="m_Base" runat="server" EnableViewState="False">
<script>
    function onPickOperatorItem( containerId , storeId , sOperatorKey , sSelectionCmd ) { try
    {
        // Validate controls.

        var oContainer = document.getElementById(containerId);
        var oStore = document.getElementById(storeId);
        if( oContainer == null || oStore == null || sOperatorKey == null )
        {
            return;
        }

        if( sOperatorKey == "" )
        {
            return;
        }

        // Store the selection and fire the event.

        oStore.value = sOperatorKey;

        if( fireReportEvent != null )
        {
            fireReportEvent( sSelectionCmd , sOperatorKey , oContainer );
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}
</script>
    <INPUT type="hidden" id="m_Store" name="m_Store" runat="server" value="">
    <ASP:Panel id="m_Root" runat="server" style="MARGIN: 4px; BORDER: 1px groove; BACKGROUND-COLOR: white; OVERFLOW-Y: auto;" EnableViewState="False">
        <DIV class="OperatorHeader">
            Comparison Operators...
        </DIV>
        <ASP:DataGrid id="m_Grid" runat="server" Width="100%" AutoGenerateColumns="False" AllowSorting="False" ShowHeader="False" ShowFooter="False" GridLines="None" BackColor="Transparent" EnableViewState="False">
            <Columns>
                <ASP:TemplateColumn ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <A href="#" class="OperatorCommandLink" style="PADDING-LEFT: 4px;" 
                            key=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Key" ).ToString()) %> 
                            onclick="onPickOperatorItem( '<%# AspxTools.ClientId(m_Base) %>' , '<%# AspxTools.ClientId(m_Store) %>' , this.getAttribute('key') , <%# AspxTools.JsString(SelectionCmd) %> );" >
                            select
                        </A>
                    </ItemTemplate>
                </ASP:TemplateColumn>
                <ASP:TemplateColumn ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <SPAN class="OperatorSymbol">
                            <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Symbol" ).ToString()) %>
                        </SPAN>
                    </ItemTemplate>
                </ASP:TemplateColumn>
                <ASP:TemplateColumn ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <SPAN class="OperatorLabel">
                            <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" ).ToString()) %>
                        </SPAN>
                    </ItemTemplate>
                </ASP:TemplateColumn>
            </Columns>
        </ASP:DataGrid>
    </ASP:Panel>
</ASP:Panel>
