<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FilterClassUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.FilterClassUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ASP:Panel id="m_Base" runat="server" style="OVERFLOW-Y: auto; PADDING: 4px;">
    <script>
        function clickOptionBox( iArg , storeId , containerId) { try
        {
            // Valdate the list variable.
            var oStore = document.getElementById(storeId);
            var oContainer = document.getElementById(containerId);
           if( oStore.value == null || oContainer.getElementsByTagName("*").length == 0 )
            {
                return false;
            }

            // If already checked, we remove it and return
            // false to show it's now not checked.  Otherwise,
            // we add it to the list and return that it is
            // now checked.

            if( markReportAsDirty != null )
            {
                markReportAsDirty();
            }

            if( oStore.value.indexOf( "-" + iArg + "-" ) != -1 )
            {
                // Remove the entry from the store.

                oStore.value = oStore.value.replace( "-" + iArg + "-" , "" );

                // Uncheck the label box.

                var i;
                var all = oContainer.getElementsByTagName("*");
                for( i = 0 ; i < all.length ; ++i )
                {
                    var child = all[i];

                    if( child.kind == null || child.kind != "label" )
                    {
                        continue;
                    }

                    child.checked = false;
                }

                return false;
            }

            oStore.value += "-" + iArg + "-";

            return true;
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function clickAllOptions( oContainer , checkIt ) { try
        {
            // Validate the container.

            if( oContainer.getElementsByTagName("*").length == 0 )
            {
                return;
            }

            // Make every child checkbox conform.

            var i;
            var all = oContainer.getElementsByTagName("*");
            for( i = 0 ; i < all.length ; ++i )
            {
                // Examine this child's properties and if it is
                // an option checkbox, click it if it differs.

                var child = all[i];

                if( child.getAttribute('kind') != null && child.getAttribute('kind') == "option" )
                {
                    if( child.checked != checkIt )
                    {
                        child.click();
                    }
                }
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}
    </script>
    <style type="text/css">
        .w-33 { width: 33%; }
        .no-wrap { white-space: nowrap; }
        .d-inline-block { display: inline-block; }
    </style>
    <DIV style="FONT-WEIGHT: bold; WIDTH: 100%;">
        <INPUT kind="label" type="checkbox" onclick="clickAllOptions( <%= AspxTools.ClientId(m_Base) %> , this.checked );">
        <%= AspxTools.HtmlString(this.GroupLabel) %>
    </DIV>
    <INPUT id="m_Store" runat="server" type="hidden" name="m_Store">
    <DIV style="PADDING-LEFT: 16px;">
        <ASP:Repeater id="m_List" runat="server">
            <ItemTemplate>
                <SPAN class="w-33 no-wrap d-inline-block" nowrap>
                    <INPUT kind="option" type="checkbox" value=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Argument" ).ToString()) %> <%# AspxTools.HtmlString(IsBoundClassChecked( Container.DataItem ) ? "checked" : "") %> onclick="this.checked = clickOptionBox( this.value , '<%# AspxTools.ClientId(m_Store) %>' , '<%# AspxTools.ClientId(m_Base) %>');">
                    <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" ).ToString()) %>
                </SPAN>
            </ItemTemplate>
        </ASP:Repeater>
    </DIV>
</ASP:Panel>
