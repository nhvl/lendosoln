<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CommentEditUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.CommentEditUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ASP:Panel id="m_Base" runat="server">
    <script>
        function changeCommentText( oContainer , oStore , sComments ) { try
        {
            // Validate control objects.

            if( oContainer == null || oStore == null || sComments == null )
            {
                return;
            }

            // Update the storage container with the text.

            oStore.value = sComments;

            // Raise the event for other controls.

            if( fireReportEvent != null )
            {
                fireReportEvent( "detail" , sComments , oContainer );
            }

            // We now have a dirty report.

            if( markReportAsDirty != null )
            {
                markReportAsDirty();
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}
    </script>
    <INPUT type="hidden" id="m_Store" name="m_Store" runat="server" value="">
    <DIV style="WIDTH: 288px; TEXT-ALIGN: center; VERTICAL-ALIGN: middle;">
        <ASP:TextBox id="m_Text" runat="server" style="BORDER: 1px groove; MARGIN: 4px; MARGIN-TOP: 3px; MARGIN-BOTTOM: 3px; FONT: 11px arial; PADDING-LEFT: 4px; OVERFLOW-Y: auto;" TextMode="MultiLine" EnableViewState="False">
        </ASP:TextBox>
    </DIV>
    <script>
        // If the event handling table is available, we
        // need to link up our event processing functions.

        if( registerForReportEvent != null )
        {
            var base = document.getElementById("<%= AspxTools.ClientId(m_Base) %>" );
            var stor = document.getElementById("<%= AspxTools.ClientId(m_Store) %>" );
            // ...
        }
    </script>
</ASP:Panel>
