using System;
using System.Data;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using System.Linq;
using LendersOffice.Security;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for FieldLayoutControl.
	/// </summary>

	public partial  class FieldLayoutUserControl : System.Web.UI.UserControl , INamingContainer
	{
        private LendersOffice.QueryProcessor.FieldLookup      m_Look;
		private LendersOffice.QueryProcessor.Query            m_View;

        public String Selection
        {
            // Access member.

            set
            {
                m_CurrF.Value = value;
            }
            get
            {
                return m_CurrF.Value;
            }
        }

        public Query DataSource
        {
            // Access member.

            set
            {
                m_View = value;
            }
        }

        public FieldLookup DataLookup
        {
            // Access member.

            set
            {
                m_Look = value;
            }
        }

        public String CssClass
        {
            // Access member.

            set
            {
                m_Base.CssClass = value;
            }
        }

        public String Height
        {
            // Access member.

            set
            {
                m_Base.Height = Unit.Parse( value );

                if( m_Base.Height.Type == UnitType.Pixel )
                {
                    m_Root.Height = Unit.Pixel( Convert.ToInt32( m_Base.Height.Value ) - 12 );
                }
                else
                {
                    m_Root.Height = Unit.Percentage( 100 );
                }
            }
        }

        public String Width
        {
            // Access member.

            set
            {
                m_Base.Width = Unit.Parse( value );
            }
        }

        /// <summary>
        /// Bind the data to the current ui status.
        /// </summary>

        public void BindData(BrokerDB brokerDB)
        {
            // Reorder the layout list to match that of
            // the data store, but only if the store is
            // valid (i.e., only on postback).

            if (IsPostBack == true)
            {
                foreach (string id in m_Store.Value.Split('-'))
                {
                    // Throw out invalid entries.

                    if (id.Length == 0)
                    {
                        continue;
                    }

                    // Push the column to the end.

                    Column column = m_View.Columns.Find(id);
                    string customName;
                    if (brokerDB.GetCustomFieldName(id, m_Look ,out customName))
                        column.Name = customName;

                    m_View.Columns.Nix(column);
                    m_View.Columns.Add(column);
                }

                for (int i = 0; i < m_View.Columns.Count; ++i)
                {
                    // If not in our cached list, then it
                    // shouldn't be in the data object.
                    // The client-side updates now drive
                    // the changes to the underlying
                    // content.  If multiple controls try
                    // to do this same operation, we want
                    // the first to succeed, and the others
                    // to not need to.

                    Column column = m_View.Columns[i];

                    if (column.Show == true && m_Store.Value.IndexOf("-" + column.Id + "-") == -1 && !brokerDB.customFieldPrefixes.Keys.Contains(column.Id))
                    {
                        m_View.Columns.Nix(column);

                        --i;
                    }
                }
            }
        }

        protected string RenderClassName(object container)
        {
            string key = DataBinder.Eval(container, "Id").ToString();
            string className = "FieldLayoutListItem " + ((Selection == key) ? "FieldLayoutMarkItem" : "");

            if (!LoanFieldSecurityManager.CanReadField(key))
            {
                className += " disabledFieldLayoutListItem";
            }

            return className;
        }

        protected string RenderPermission(object container)
        {
            string key = DataBinder.Eval(container, "Id").ToString();
            return LoanFieldSecurityManager.CanReadField(key) ? "true" : "false";
        }

        /// <summary>
        /// Bind the report layout to our list.
        /// </summary>

        public void BindView(BrokerDB brokerDB)
        {
            // Use the current layout and list the ordered
            // entries.

            ArrayList shown = new ArrayList();

            if( m_View != null && m_Look != null )
            {
                if( IsPostBack == false )
                {
                    // Transfer the data to the store this
                    // first time.

                    m_Store.Value = "";

                    foreach( Column column in m_View.Columns )
                    {
                        if( column.Show == false )
                        {
                            continue;
                        }

                        m_Store.Value += "-" + column.Id + "-";
                    }
                }

                foreach( Column column in m_View.Columns )
                {
                    // Add listing fields in order.

                    if( column.Show == false || brokerDB.IsCustomFieldGloballyDefined(column.Id))
                    {
                        continue;
                    }

                    //Entire fields are saved, which includes the name.  Check if a report is using a custom field with a lender wide
                    //value now and vice versa, and update it with the current value. 
                    string customName;
                    //field now has a lender wide value
                    if (brokerDB.GetCustomFieldName(column.Id, m_Look, out customName))
                        column.Name = customName;
                    else
                    {
                        //field is no longer a lenderwide value, so use its original name
                        Field originalCol = m_Look.LookupById(column.Id);
                        if(originalCol != null)
                            column.Name = m_Look.LookupById(column.Id).Name;
                    }
                    column.Perm = LoanFieldSecurityManager.SecureFieldPermissions(column.Id);
                    shown.Add( column );
                }
            }

            m_List.DataSource = shown;
            m_List.DataBind();
        }


        /// <summary>
        /// Bind the report layout to our list.
        /// </summary>

        public void HoldView(BrokerDB brokerDB)
        {
            // Use the current sorting and list the ordered
            // entries.

            ArrayList shown = new ArrayList();

            m_Store.Value = "";

            if( m_View != null && m_Look != null )
            {
                foreach( Column column in m_View.Columns )
                {
                    // Add listing fields in order.

                    if (column.Show == false || brokerDB.IsCustomFieldGloballyDefined(column.Id))
                    {
                        continue;
                    }

                    m_Store.Value += "-" + column.Id + "-";

                    shown.Add( column );
                }
            }

            m_List.DataSource = shown;
            m_List.DataBind();
        }

	    /// <summary>
	    ///	Bind data and render lists.
	    /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Use current columns to list report.
            BrokerDB brokerDB = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;
            HoldView(brokerDB);
		}

	    /// <summary>
	    ///	Initialize this control.
	    /// </summary>

		protected void ControlLoad( object sender , System.EventArgs e )
		{
            // Update the data elements according
            // to the state of the event store.
            // When we bind the view, the changes
            // that we make here should already be
            // in effect.
            BrokerDB brokerDB = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;
            BindData(brokerDB);
		}

	    /// <summary>
	    ///	Initialize this control.
	    /// </summary>

		protected void ControlInit( object sender , System.EventArgs e )
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();

            var Page = this.Page as BasePage;

            Page.EnableJqueryMigrate = false;
            Page.RegisterJsScript("jquery-ui-1.11.4.min.js");
            Page.RegisterCSS("jquery-ui-1.11.custom.css");
            
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new System.EventHandler(this.ControlInit);
            this.Load += new System.EventHandler(this.ControlLoad);
            this.PreRender += new System.EventHandler(this.ControlPreRender);

        }
		#endregion

	}

    /// <summary>
    /// Event handling arguments.
    /// </summary>

    public class FieldLayoutEventArgs : EventArgs
    {
        /// <summary>
        /// Event args
        /// 
        /// Action is defined by this control's ui.  The
        /// target is always a field id.
        /// </summary>

        public string Action; // requested action
        public string Target; // field target id

        /// <summary>
        /// Construct specific event args.
        /// </summary>
        /// <param name="sAction">
        /// Action requested.
        /// </param>
        /// <param name="fTarget">
        /// Target of action.
        /// </param>

        public FieldLayoutEventArgs( string sAction , string sTarget )
        {
            // Initialize members.

            Action = sAction;
            Target = sTarget;
        }

        /// <summary>
        /// Construct default event args.
        /// </summary>

        public FieldLayoutEventArgs()
        {
            // Initialize members.

            Action = "";
            Target = "";
        }

    }

    /// <summary>
    /// Delegate declaration for firing events.
    /// </summary>

    public delegate void FieldLayoutEventHandler( object sender , FieldLayoutEventArgs e );

}
