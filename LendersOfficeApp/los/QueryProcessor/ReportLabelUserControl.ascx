<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ReportLabelUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.ReportLabelUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ASP:Panel id="m_Base" runat="server">
    <script>
        function changeLabelText( oContainer , oStore , sComments ) { try
        {
            // Validate control objects.

            if( oContainer == null || oStore == null || sComments == null )
            {
                return;
            }

            // Update the storage container with the text.

            oStore.value = sComments;

            // Raise the event for other controls.

            if( fireReportEvent != null )
            {
                fireReportEvent( "labels" , sComments , oContainer );
            }

            // We now have a dirty report.

            if( markReportAsDirty != null )
            {
                markReportAsDirty();
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}
    </script>
    <INPUT type="hidden" id="m_Store" name="m_Store" runat="server" value="">
    <DIV style="PADDING: 4px; TEXT-ALIGN: center; VERTICAL-ALIGN: middle;">
		<TABLE style="WIDTH: 100%; PADDING: 0px;" cellpadding="0" cellspacing="0" border="0">
		<TR>
		<TD align="left">
			<SPAN style="FONT: bold 11px arial;">
				Name
			</SPAN>
		</TD>
		<TD align="right">
			<ASP:TextBox id="m_Text" runat="server" style="BORDER: 1px inset; FONT: 11px arial; PADDING-LEFT: 4px;">
			</ASP:TextBox>
		</TD>
		</TR>
		</TABLE>
    </DIV>
    <script>
        // If the event handling table is available, we
        // need to link up our event processing functions.

        if( registerForReportEvent != null )
        {
            var base = document.getElementById("<%= AspxTools.ClientId(m_Base) %>" );
            var stor = document.getElementById("<%= AspxTools.ClientId(m_Store) %>" );

            // ...
        }
    </script>
</ASP:Panel>
