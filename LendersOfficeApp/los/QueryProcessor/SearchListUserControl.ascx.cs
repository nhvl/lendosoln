namespace LendersOffice.QueryProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Reports;
    using LendersOffice.Security;

    /// <summary>
    /// Summary description for SearchListUserControl.
    /// </summary>

    public partial  class SearchListUserControl : System.Web.UI.UserControl , IPostBackEventHandler , INamingContainer
	{
        protected System.Web.UI.WebControls.Panel             m_Wrap;

        private Dictionary<string, SearchListItem> m_LookupTable;
        private List<SearchListItem> m_Data;
        private   String    m_SelectionLabel;
        private   String      m_SelectionCmd;
		private   Boolean m_ShowReportFields;
		private   Boolean m_HideReportFields;
		private   FieldLookup         m_Look;
		private   Query               m_View;

        public event SearchListEventHandler Pick;

        public override String ClientID
        {
            get
            {
                return m_Base.ClientID;
            }
        }

		public Boolean HideReportFields
		{
			set
			{
				m_HideReportFields = value;
			}
			get
			{
				return m_HideReportFields;
			}
		}

		public Boolean ShowReportFields
		{
			set
			{
				m_ShowReportFields = value;
			}
			get
			{
				return m_ShowReportFields;
			}
		}
        public Boolean PostBackOnPick
        {
            get
            {
                if( Pick != null )
                {
                    return true;
                }

                return false;
            }
        }

        public bool HideSpecialFields { get; set; }

        public void RaisePostBackEvent( string eventArgument )
        {
            // Parse command and dispatch event.

            string[] parameters = eventArgument.Split( ':' );

            if( parameters.Length > 1 )
            {
                // Dispatch selection events according to
                // the search item id.

                switch( parameters[ 0 ].ToLower() )
                {
                    case "pick":
                        // Find the referenced item and forward it via
                        // delegation.  If the lookup table is empty,
                        // we probably should data bind this control
                        // to a valid search list first.

                        if (Pick == null) return;
                        if (!m_LookupTable.ContainsKey(parameters[1])) return;

                        SearchListItem item = m_LookupTable[parameters[1]];
                                Pick( this , new SearchListEventArgs( "Pick" , item ) );
                    break;
                }
            }
        }

        public String CssClass
        {
            set
            {
                m_Base.CssClass = value;
            }
        }

        public String SelectionLabel
        {
            set
            {
                m_SelectionLabel = value;
            }
            get
            {
                return m_SelectionLabel;
            }
        }

        public String SelectionCmd
        {
            set
            {
                m_SelectionCmd = value;
            }
            get
            {
                if( m_SelectionCmd == null || m_SelectionCmd.Length == 0 )
                {
                    return m_SelectionLabel;
                }

                return m_SelectionCmd;
            }
        }

        public String Height
        {
            set
            {
                m_Root.Height = Unit.Parse( value );

                if( m_Root.Height.Type == UnitType.Pixel )
                {
                    m_Root.Height = Unit.Pixel( Convert.ToInt32( m_Root.Height.Value ) - 62 );
                }
            }
        }

        public String Width
        {
            set
            {
                m_Base.Width = Unit.Parse( value );

                if( m_Base.Width.Type == UnitType.Pixel && m_Base.Width.Value > 90 )
                {
                    m_Group.Width = Unit.Pixel( Convert.ToInt32( m_Base.Width.Value ) - 90 );
                }
            }
        }

		public Query DataSource
		{
			set
			{
				m_View = value;
			}
		}

        /// <summary>
        /// Expose repeater list data-binding interface.
        /// </summary>
        public FieldLookup DataLookup
        {
            set
            {
				m_Look = value;
            }
        }

        protected string RenderItemClass(object container)
        {
            string key   = DataBinder.Eval(container, "Key").ToString();
            string className = "SearchList" + DataBinder.Eval(container, "Type").ToString() + " ";
            if (!LoanFieldSecurityManager.CanReadField(key))
            {
                className += "disabledSearchlistitem";
            }
            
            return className;
        }

        protected string CheckPermission(object container)
        {
            string key   = DataBinder.Eval(container, "Key").ToString();
            return LoanFieldSecurityManager.CanReadField(key) ? "true" : "false";
        }

		/// <summary>
		/// Map fields and columns into search list.
		/// </summary>

		public void BindView(BrokerDB broker)
        {
			// Load the given lookup list into our set.

            string first = "" , domain = "";

            if( m_Group.SelectedItem != null )
            {
                domain = m_Group.SelectedItem.Text;
            }

            m_Group.Items.Clear();

			// Assign in-use columns to the list if the option
			// has been assigned.

			m_LookupTable.Clear();
			m_Data.Clear();

			if( m_ShowReportFields == true )
			{
				// Put the columns of the report up first, but only
				// if the containing page has elected for it.

                var aCurr = new List<SearchListItem>();

				aCurr.Add(new SearchListItem("Current Report Fields" , "Current Report Fields" , E_SearchListItemT.Header ));

				m_Group.Items.Add( "Current Report Fields" );

				foreach( Column column in m_View.Columns )
				{
                    if (this.HideSpecialFields && LoanReporting.IsSpecialField(column.Id))
                    {
                        continue;
                    }

                    if (!broker.IsCustomFieldGloballyDefined(column.Id))
                    {
                        column.Perm = LoanFieldSecurityManager.SecureFieldPermissions(column.Id);
                        aCurr.Add(new SearchListItem(column.Id, column.NamePerm, E_SearchListItemT.Item));
                    }
				}

				m_Curr.DataSource = aCurr;
				m_Curr.DataBind();
			}

			// Now add the reportable fields to the list for
			// selection.  If the field is in use, and these
			// fields are displayed, then we gray out the
			// item in its normal position.  Note: For now,
			// we exclude the grayed out ones with invalid.

			foreach( FieldGroup group in m_Look.Fields )
			{
                m_Data.Add(new SearchListItem(group.Domain, group.Domain, E_SearchListItemT.Header));

				m_Group.Items.Add( group.Domain );


				foreach( FieldLabel label in group )
				{
                    label.Perm = LoanFieldSecurityManager.SecureFieldPermissions(label.Id);

                    // 01/08/09 mf. OPM 25828. This is a bit of a hack to keep unsubscribed lenders 
                    // from seeing the external rate lock fields until we have the feature completed.
                    // For PML0094's eyes only. We should consider working these checks into the reporting
                    // framework, as it might be useful elsewhere such as hiding PML fields from non-PML clients.

                    // 2/19/14 gf - opm 126772, if the additional tax rows are not going to be shown in
                    // the loan editor, do not show the related custom reports fields.

                    if (label.Id == "sLoanOfficerRateLockStatusT" || label.Id == "sLoanOfficerRateLockedD")
                    {
                        continue;
                    }
                    else if (label.Id == "sAutoDisclosureStatusT" && !broker.IsEnableDocuTechAutoDisclosure)
                    {
                        continue;
                    }
                    else if ((label.Id == "sTaxTableU3DueD" || label.Id == "sTaxTableU3PaidD"
                        || label.Id == "sTaxTableU4DueD" || label.Id == "sTaxTableU4PaidD")
                        && !broker.EnableAdditionalSection1000CustomFees)
                    {
                        continue;
                    }
                    else if (label.Id == "sLateChargePc")
                    {
                        // OPM 243940.  This field was replaced by CR version.  We remove it from
                        // the choice list, but the old one will still remain in the current fields
                        // list of existing reports that use it.
                        continue;
                    }
                    else if ( ( label.Id == "sAutoPriceProcessResultT" || label.Id == "sAutoEligibilityProcessResultT")
                        && !broker.EnableAutoPriceEligibilityProcess )
                    {
                        // OPM 247399.  Feature-related fields.  Hidden when feature is off.
                        continue;
                    }
                    else if (this.HideSpecialFields && LoanReporting.IsSpecialField(label.Id))
                    {
                        continue;
                    }


                    string customName;
                    if( m_HideReportFields == true || m_ShowReportFields == true )
					{
						// 1/27/2005 kb - The naming of these options is a little
						// confusing -- they're not mutually exclusive.  Hiding
						// pertains to removing that which is already in the
						// report so that it no longer is selectable.  Showing
						// report fields turns on adding a new section to the top
						// of the list that shows all the fields in their layout
						// order.  Both will hide the field from the main list
						// body.  We must hide in such a way that the entry can
						// be turned back on when fields are removed from the
						// report.  The easiest way I can think of to do this is
						// to flip a class name around so that the display style
						// goes from 'none' to 'block'.

						if( m_View != null && m_View.Columns.Has( label.Id ) == true )
						{

                            if (broker.GetCustomFieldName(label.Id, m_Look, out customName))
                            {
                                    m_Data.Add(new SearchListItem(label.Id, customName + label.NameAndPerm.Substring(label.Name.Length), E_SearchListItemT.Hidden));       
                            }
                            else if (!broker.IsCustomFieldGloballyDefined(label.Id))
                                m_Data.Add(new SearchListItem(label.Id, label.NameAndPerm, E_SearchListItemT.Hidden));

							continue;
						}
					}
                    if (broker.GetCustomFieldName(label.Id,  m_Look, out customName))
                    {
                            m_Data.Add(new SearchListItem(label.Id, customName + label.NameAndPerm.Substring(label.Name.Length), E_SearchListItemT.Item));
                    }
                    else if(!broker.IsCustomFieldGloballyDefined(label.Id))
                        m_Data.Add(new SearchListItem(label.Id, label.NameAndPerm, E_SearchListItemT.Item));

					if( first == "" )
					{
						first = label.Id;
					}
				}
			}

			foreach( SearchListItem lItem in m_Data )
			{
                if (m_LookupTable.ContainsKey(lItem.Key) == false)
				{
					m_LookupTable.Add( lItem.Key , lItem );
				}
			}

            if( domain == "" && m_Group.Items.Count > 0 )
            {
                m_Group.SelectedIndex = 0;
            }

            if( m_Store.Value == "" )
            {
                m_Store.Value = first;
            }

            // Now databind the goods.
            m_List.DataSource = m_Data;
            m_List.DataBind();
        }

        /// <summary>
        /// Expose current selection key.  This id makes
        /// no sense to us, but should be useful to the
        /// caller who populated the list.
        /// </summary>

        public string Selection
        {
            get
            {
                return m_Store.Value;
            }
        }

        /// <summary>
        /// Render the web control.
        /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Write out a new tree view using current conditions.
            BrokerDB brokerDB = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;
            BindView(brokerDB);
		}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            var Page = this.Page as BasePage;
            Page.EnableJqueryMigrate = false;
            Page.RegisterJsScript("jquery-ui-1.11.4.min.js");
            Page.RegisterCSS("jquery-ui-1.11.custom.css");
            Page.RegisterJsScript("SearchList.js");

	        //
	        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
	        //
	        InitializeComponent();
	        base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PreRender += new System.EventHandler(this.ControlPreRender);
        }
        #endregion

        /// <summary>
        /// Construct default.
        /// </summary>

        public SearchListUserControl()
        {
            m_LookupTable = new Dictionary<string, SearchListItem>();
            m_Data = new List<SearchListItem>();
        }
	}

    /// <summary>
    /// Inidividual list entry for our search list.  Construct
    /// a list vector and populate it with items.  We will bind
    /// our list to the enumerable vector.  The list templates
    /// expect a bound item with these properties.
    /// </summary>

    public enum E_SearchListItemT
    {
        Invalid  = 0,
        Header   = 1,
        Item     = 2,
        Grayed   = 3,
		Hidden   = 4
    }

    public class SearchListItem
    {
        /// <summary>
        /// Hidden key to use for identifying selected
        /// </summary>
        public string Key {get; private set;}

        /// <summary>
        /// Visible label for the entry (heading included)
        /// </summary>
        public string Label {get; private set;}

        /// <summary>
        /// Description text -- optional
        /// </summary>
        public string Description {get; private set;}

        /// <summary>
        /// Type flag to determine view style
        /// </summary>
        public E_SearchListItemT Type {get; private set;}

        /// <summary>Construct search list item.</summary>
        /// <param name="sKey">Hidden key for item selection.</param>
        /// <param name="sLabel">Visible label for each entry.</param>
        /// <param name="sDescription">Descriptive text to include.</param>
        /// <param name="eType">Type flag for style.</param>
        public SearchListItem(string sKey, string sLabel, string sDescription, E_SearchListItemT eType)
        {
            Key = sKey;
            Label = sLabel;
            Description = sDescription;
            Type = eType;
            }

        /// <summary>
        /// Construct search list item.
        /// </summary>
        /// <param name="sKey">Hidden key for item selection.</param>
        /// <param name="sLabel">Visible label for each entry.</param>
        /// <param name="eType">Type flag for style.</param>

        public SearchListItem(string sKey, string sLabel, E_SearchListItemT eType)
        {
            Key = sKey;
            Label = sLabel;
            Type = eType;

            Description = "";
        }

        }

        /// <summary>
    /// Event handling arguments.
    /// </summary>

    public class SearchListEventArgs : EventArgs
    {
        /// <summary>
        /// Event args
        /// 
        /// Action is defined by this control's ui.  The
        /// target is always a search list item.
        /// </summary>

        public string         Action; // requested action
        public SearchListItem Target; // list item target

        /// <summary>
        /// Construct specific event args.
        /// </summary>
        /// <param name="sAction">
        /// Action requested.
        /// </param>
        /// <param name="lTarget">
        /// Target of action.
        /// </param>

        public SearchListEventArgs( string sAction , SearchListItem lTarget )
        {
            Action = sAction;
            Target = lTarget;
        }

        /// <summary>
        /// Construct default event args.
        /// </summary>

        public SearchListEventArgs()
        {
            Action = "";
            Target = null;
        }

    }

    /// <summary>
    /// Delegate declaration for firing events.
    /// </summary>

    public delegate void SearchListEventHandler( object sender , SearchListEventArgs e );

}
