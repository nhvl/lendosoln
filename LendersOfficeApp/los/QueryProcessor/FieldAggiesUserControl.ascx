<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FieldAggiesUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.FieldAggiesUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ASP:Panel id="m_Base" runat="server" EnableViewState="False">
    <INPUT type="hidden" id="m_CurrentSelection" runat="server" name="m_CurrentSelection" value="">
    <DIV style="MARGIN: 4px;">
        <SPAN style="WIDTH: 40%;" uid="<%= AspxTools.HtmlString(UniqueID) %>" key="Mn" onclick="onFunctionChanged( this );">
            <ASP:CheckBox id="m_Mn" runat="server" Text="Minimum" TextAlign="Right" EnableViewState="False">
            </ASP:CheckBox>
        </SPAN>
        <SPAN style="WIDTH: 40%;" uid="<%= AspxTools.HtmlString(UniqueID) %>" key="Mx" onclick="onFunctionChanged( this );">
            <ASP:CheckBox id="m_Mx" runat="server" Text="Maximum" TextAlign="Right" EnableViewState="False">
            </ASP:CheckBox>
        </SPAN>
        <SPAN style="WIDTH: 40%;" uid="<%= AspxTools.HtmlString(UniqueID) %>" key="Av" onclick="onFunctionChanged( this );">
            <ASP:CheckBox id="m_Av" runat="server" Text="Average" TextAlign="Right" EnableViewState="False">
            </ASP:CheckBox>
        </SPAN>
        <SPAN style="WIDTH: 40%;" uid="<%= AspxTools.HtmlString(UniqueID) %>" key="Sm" onclick="onFunctionChanged( this );">
            <ASP:CheckBox id="m_Sm" runat="server" Text="Total" TextAlign="Right" EnableViewState="False">
            </ASP:CheckBox>
        </SPAN>
    </DIV>
</ASP:Panel>
<script>
    function onFunctionChanged( oBox )
    {
        // Validate control.

        if( oBox == null || oBox.uid == null || oBox.key == null )
        {
            return;
        }

        // Parse out base control id and delegate.

        __doPostBack( oBox.uid , "Change:" + oBox.key );
    }
</script>
