<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FieldGroupsUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.FieldGroupsUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ASP:Panel id="m_Base" runat="server" style="PADDING-BOTTOM: 0px; PADDING-TOP: 0px">
<script>
    function chooseGrouping( oContainer , sFieldId ) { try
    {
        // Validate object set.

        if( oContainer == null || sFieldId == null )
        {
            return;
        }

		// Delegate to all listening handlers.

        if( fireReportEvent != null )
        {
            fireReportEvent( "select" , sFieldId , oContainer );
        }
	}
	catch( e )
	{
		window.status = e.message;
	}}

    function selectGrouping( oContainer , oStore , sFieldId ) { try
    {
        // Validate object set.

        if( oContainer == null || oStore == null || sFieldId == null )
        {
            return;
        }

        // Update the current selection identifier
        // and then color the background.

        var i;

        var all = oContainer.getElementsByTagName("*");
        for( i = 0 ; i < all.length ; ++i )
        {
            var curr = all[i];

            if( curr == null || curr.getAttribute('kind') == null || curr.getAttribute('kind') != "item" )
            {
                continue;
            }

            if( curr.getAttribute('key') == sFieldId )
            {
                curr.className = "FieldGroupsMarkItem";
                oStore.value   = sFieldId;
            }
            else
            {
                curr.className = "FieldGroupsListItem";
            }
        }

        // Raise the event for other controls.

        if( fireReportEvent != null )
        {
            fireReportEvent( "select" , sFieldId , oContainer );
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function shiftGroupingUp( oContainer , oStore , sFieldId ) { try
    {
        // Validate the container and field.
        var all = oContainer.getElementsByTagName("*");

        if( oStore.value == null || sFieldId == null )
        {
            return;
        }

        // Walk the children and swap the previous
        // entry with the one that matches.

        var i , prev = null;
        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a sort list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                if( prev != null && curr.getAttribute('key') == sFieldId )
                {
                    // Found a match.  Swap with
                    // the previous child.

                    swapNodePoly(curr, prev);

                    break;
                }

                prev = curr;
            }
        }

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a groupby list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                // Push the entry onto the end.

				var s , f , item = "";

				s = oStore.value.indexOf( "-" + curr.getAttribute('key') + "+" );

				f = oStore.value.indexOf( "-" , s + 1 );

				if( s != -1 && f > s )
				{
					item = oStore.value.substr( s , f - s + 1 );
				}

                oStore.value = oStore.value.replace( item , "" );
                oStore.value = oStore.value + item;
            }
        }

        // Raise the event for other controls.

        if( fireReportEvent != null )
        {
            fireReportEvent( "grpbup" , sFieldId , oContainer );
        }

        // We now have a dirty report.

        if( markReportAsDirty != null )
        {
            markReportAsDirty();
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function shiftGroupingDn( oContainer , oStore , sFieldId ) { try
    {
        // Validate the container and field.
        var all = oContainer.getElementsByTagName("*");

        if( oStore.value == null || sFieldId == null )
        {
            return;
        }

        // Walk the children and swap the current
        // entry with the previous that matches.

        var i , prev = null;

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a sort list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                if( prev != null && prev.getAttribute('key') == sFieldId )
                {
                    // Found a match.  Swap with
                    // the current child.

                    swapNodePoly(prev, curr);

                    break;
                }

                prev = curr;
            }
        }

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a sort list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                // Push the entry onto the end.

				var s , f , item = "";

				s = oStore.value.indexOf( "-" + curr.getAttribute('key') + "+" );

				f = oStore.value.indexOf( "-" , s + 1 );

				if( s != -1 && f > s )
				{
					item = oStore.value.substr( s , f - s + 1 );
				}

                oStore.value = oStore.value.replace( item , "" );
                oStore.value = oStore.value + item;
            }
        }

        // Raise the event for other controls.

        if( fireReportEvent != null )
        {
            fireReportEvent( "grpbdn" , sFieldId , oContainer );
        }

        // We now have a dirty report.

        if( markReportAsDirty() != null )
        {
            markReportAsDirty();
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function removeGrouping( oContainer , oStore , sFieldId ) { try
    {
        // Validate the container and field.
        var all = oContainer.getElementsByTagName("*");

        if( oStore.value == null || sFieldId == null )
        {
            return;
        }

        // Walk the children and swap the current
        // entry with the previous that matches.

        var i;

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a sort list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                if( curr.getAttribute('key') == sFieldId )
                {
					var s , f , item = "";

					s = oStore.value.indexOf( "-" + curr.getAttribute('key') + "+" );

					f = oStore.value.indexOf( "-" , s + 1 );

					if( s != -1 && f > s )
					{
						item = oStore.value.substr( s , f - s + 1 );
					}

					oStore.value = oStore.value.replace( item , "" );

                    curr.remove ? curr.remove() : curr.removeNode( true );

                    break;
                }
            }
        }

        // We now have a dirty report.

        if( markReportAsDirty() != null )
        {
            markReportAsDirty();
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function clickGroupingOption( oContainer , oStore , sFieldId ) { try
    {
		// Validate the container and field.
        var all = oContainer.getElementsByTagName("*");

        if( oStore.value == null || sFieldId == null )
        {
            return;
        }

        // Walk the children and update the status
        // of the checkbox based on our current
        // state -- we toggle.

        var i;

        for( i = 0 ; i < all.length ; ++i )
        {
            // Get current child and check if it is
            // a sort list item.

            var curr = all[i];

            if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
            {
                if( curr.getAttribute('key') == sFieldId )
                {
					var s , f , item = "";

					s = oStore.value.indexOf( "-" + curr.getAttribute('key') + "+" );

					f = oStore.value.indexOf( "-" , s + 1 );

					if( s != -1 && f > s )
					{
						item = oStore.value.substr( s , f - s + 1 );
					}

					if( item == ( "-" + curr.getAttribute('key') + "+0-" ) )
					{
						if( oStore.value.indexOf( "+1" , oStore.value.indexOf( "+1" ) + 1 ) == -1 )
						{
							oStore.value = oStore.value.replace( item , "-" + curr.getAttribute('key') + "+1-" );

							curr.children[ 3 ].className = "FieldGroupsCommand FieldGroupsChecked";
						}
					}
					else
					if( item == ( "-" + curr.getAttribute('key') + "+1-" ) )
					{
						oStore.value = oStore.value.replace( item , "-" + curr.getAttribute('key') + "+0-" );

						curr.children[ 3 ].className = "FieldGroupsCommand";
					}

                    break;
                }
            }
        }

        // We now have a dirty report.

        if( markReportAsDirty != null )
        {
            markReportAsDirty();
        }
	}
	catch( e )
	{
        window.status = e.message;
    }}

    function updateGrouping( oContainer , oStore , sFieldArg ) { try
    {
        // Validate the container and field.
        var all = oContainer.getElementsByTagName("*");

        if( oStore.value == null || sFieldArg == null )
        {
            return;
        }

        // Break the function into parts and
        // see if we're adding or removing.

        var parts;
        if (typeof (stringSplitRemainder) != 'undefined')
        {
            parts = stringSplitRemainder(sFieldArg, ":", 2);
        }
        else
        {
            parts = sFieldArg.split( ":" );
        }

        if( parts.length != 2 )
        {
			return;
		}

        if( oStore.value.indexOf( "-" + parts[ 0 ] + "+" ) == -1 )
        {
            // We need to add a new groupby item.  This is
            // usually a postback event.  We insert on the
            // fly to mimic insert handling.

			var name , id;

			id   = parts[ 0 ];
            name = parts[ 1 ];
            var h = hypescriptDom;
            oContainer.appendChild(
                h("div", {attrs:{kind:"item", key:id}, className:"FieldGroupsListItem"}, [
                    h("a", {href:"#", className:"FieldGroupsLinkCommand", attrs:{key:id}, onclick:function(){
                        this.className = "FieldGroupsLinkCommand";
                        shiftGroupingUp(oContainer, oStore, id);
                    }, innerHTML:"&#x25b2;"}),
                    h("a", {href:"#", className:"FieldGroupsLinkCommand", attrs:{key:id}, onclick:function(){
                        this.className = "FieldGroupsLinkCommand";
                        shiftGroupingDn(oContainer, oStore, id);
                    }, innerHTML:"&#x25bc;"}),
                    h("div", {className:"FieldGroupsLabel", attrs:{key:id}, onclick:function(){
                        chooseGrouping(oContainer, id);
                    }}, name),
                    h("div", {className:"FieldGroupsCommand", attrs:{key:id}, onclick:function(){
                        clickGroupingOption(oContainer, oStore, id);
                        blur();
                    }}, name),
                ]));
            oStore.value += "-" + parts[ 0 ] + "+0-";
        }
        else
        {
            // Remove the item from our grouping.  We don't
            // need to propagate an event because interested
            // users can key off of the click event.  If we
            // need to know when grouped columns are dropped
            // from this control, then add the firing of the
            // event here.

			var i , s , f;

			for( i = 0 ; i < all.length ; ++i )
			{
				// Get current child and check if it is
				// a sort list item.

				var curr = all[i];

				if( curr.getAttribute('kind') != null && curr.getAttribute('key') != null && curr.getAttribute('kind') == "item" )
				{
					if( curr.getAttribute('key') == parts[ 0 ] )
					{
						curr.remove ? curr.remove() : curr.removeNode( true );

						break;
					}
				}
			}

			s = oStore.value.indexOf( "-" + parts[ 0 ] + "+" );

			f = oStore.value.indexOf( "-" , s + 1 );

			if( s != -1 && f > s )
			{
				oStore.value = oStore.value.replace( oStore.value.substr( s , f - s + 1 ) , "" );
			}
        }

        // We now have a dirty report.

        if( markReportAsDirty() != null )
        {
            markReportAsDirty();
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}
</script>
<INPUT id=m_Store type=hidden name=m_Store runat="server"> <INPUT id=m_CurrF
type=hidden name=m_CurrF runat="server"> <ASP:Panel id=m_Header style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 2px; MARGIN: 0px; PADDING-TOP: 4px" runat="server" EnableViewState="False">
<TABLE cellSpacing=0 cellPadding=0 width="100%">
  <TR class=FieldLabel>
    <TD align=left>Fields names </TD>
    <TD align=right>Crossed? </TD></TR></TABLE></ASP:Panel><ASP:Panel id=m_Root style="OVERFLOW-Y: scroll;OVERFLOW-X: hidden; MARGIN: 0px" runat="server" EnableViewState="False" Width="100%"><ASP:Repeater id=m_List runat="server" EnableViewState="False">
            <ItemTemplate>
                <DIV kind="item" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' class='<%# AspxTools.HtmlString( ( Selection == DataBinder.Eval( Container.DataItem , "Id" ).ToString() ) ? "FieldGroupsMarkItem" : "FieldGroupsListItem" ) %>'>
					<A href="#" class="FieldGroupsLinkCommand" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' onclick="this.className = 'FieldGroupsLinkCommand'; shiftGroupingUp( <%# AspxTools.JsStringUnquoted(m_Root.ClientID) %> , document.getElementById(<%# AspxTools.JsString(m_Store.ClientID) %>) , this.getAttribute('key') );">
						&#x25b2;</A>
					<A href="#" class="FieldGroupsLinkCommand" key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' onclick="this.className = 'FieldGroupsLinkCommand'; shiftGroupingDn( <%# AspxTools.JsStringUnquoted(m_Root.ClientID) %> , document.getElementById(<%# AspxTools.JsString(m_Store.ClientID) %>) , this.getAttribute('key') );">
						&#x25bc;</A>
					<div key='<%# AspxTools.HtmlString( DataBinder.Eval( Container.DataItem , "Id" )) %>' class="FieldGroupsLabel" onclick="chooseGrouping( <%# AspxTools.JsStringUnquoted(m_Root.ClientID) %> , this.getAttribute('key') );">
						<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Name" )) %>
					</div>
					<div key='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' class='FieldGroupsCommand<%# AspxTools.HtmlString(Convert.ToInt32( DataBinder.Eval( Container.DataItem , "Option" ) ) != 0 ? " FieldGroupsChecked" : "") %>' onclick="clickGroupingOption( <%# AspxTools.JsStringUnquoted(m_Root.ClientID) %> , document.getElementById(<%# AspxTools.JsString(m_Store.ClientID) %>) , this.getAttribute('key') ); blur();"> </div>
                </DIV>
            </ItemTemplate>
        </ASP:Repeater></ASP:Panel>
<script>
    // If the event handling table is available, we
    // need to link up our event processing functions.

    if( registerForReportEvent != null )
    {
        var base = document.getElementById("<%= AspxTools.ClientId(m_Base) %>" );
        var root = document.getElementById("<%= AspxTools.ClientId(m_Root) %>" );
        var curr = document.getElementById("<%= AspxTools.ClientId(m_CurrF) %>" );
        var stor = document.getElementById("<%= AspxTools.ClientId(m_Store) %>" );

        registerForReportEvent( base , curr ,  selectGrouping , "select" );
        registerForReportEvent( root , stor , shiftGroupingUp , "grpbup" );
        registerForReportEvent( root , stor , shiftGroupingDn , "grpbdn" );
        registerForReportEvent( root , stor ,  updateGrouping , "groupd" );
        registerForReportEvent( root , stor ,  removeGrouping , "remove" );
    }
</script>
</ASP:Panel>
