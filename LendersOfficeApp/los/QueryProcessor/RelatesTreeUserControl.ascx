<%@ Control Language="c#" AutoEventWireup="false" Codebehind="RelatesTreeUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.RelatesTreeUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="UCR" TagName="BlockTreeUserControl" Src="../QueryProcessor/BlockTreeUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="TemplatedUserControl" Src="../QueryProcessor/TemplatedUserControl.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ASP:Panel id="m_Base" runat="server" style="PADDING-TOP: 4px; OVERFLOW-Y: auto; OVERFLOW-X: auto;" EnableViewState="False">
    <script>
        function fireGroupCommand( oCmd , sCmd ) { try
        {
            // Validate the command and dispatch it using
            // our drop handler (nobody uses this, right?)
            // to communicate page events.

            if( oCmd == null || sCmd == null )
            {
                return;
            }

            // Create an event and fire it.  We want the
            // event to bubble up to the group's main
            // wrapper control.

            var v = createEvent("drop");
            v.code = sCmd;
            dispatchEvent(oCmd, v);
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function showsAllPanels( oContainer , sPanelId ) { try
        {
    		// Validate the input.

            if( oContainer == null || sPanelId == null )
            {
                return;
            }

            // Walk all instances of the specified panel
            // and show them.  We fire the appropriate
            // show events as needed.

            var i , a = getElementByIdFromParent(oContainer, sPanelId );

            if( a == null )
            {
                return;
            }

            if( a.length != null )
            {
                for( i = 0 ; i < a.length ; ++i )
                {
                    // Fire showing event for this instance.
                    // Then, show the panel.

                    // ...

                    a[ i ].style.visibility = "visible";
                }
            }
            else
            {
                // Fire showing event for this instance.
                // Then, show the panel.

                // ...

                a.style.visibility = "visible";
            }
    	}
        catch( e )
        {
            window.status = e.message;
        }}

        function closeAllPanels( oContainer , sPanelId ) { try
        {
            // Validate the input.

            if( oContainer == null || sPanelId == null )
            {
                return;
            }

            // Walk all instances of the specified panel
            // and close them.  We fire the appropriate
            // close events as needed.

            var i , a = getElementByIdFromParent(oContainer, sPanelId );

            if( a == null )
            {
                return;
            }

            if( a.length != null )
            {
                for( i = 0 ; i < a.length ; ++i )
                {
                    // Fire closing event for this instance.
                    // Then, close the panel.

                    // ...

                    a[ i ].style.display = "none";
                }
            }
            else
            {
                // Fire closing event for this instance.
                // Then, close the panel.

                // ...

                a.style.display = "none";
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function disableConditionEntry( oContainer ) { try
        {
            // Validate the input.

            if( oContainer == null )
            {
                return;
            }

            // Forward the request to all listeners.

            if( fireReportEvent != null )
            {
                fireReportEvent( "closed" , "" , oContainer );
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function enableConditionEntry( oContainer , oGroup ) { try
        {
            // Validate the input.

            if( oContainer == null )
            {
                return;
            }

            // Forward the request to all listeners.  We pass
            // the primary field key to the entry panels so
            // that they can initialize with pertinent info.

            if( fireReportEvent != null )
            {
                fireReportEvent( "opened" , getElementByIdFromParent(oGroup, "m_PicF").getAttribute("key") , oContainer );
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

    	function openConditionForEdit( oContainer , oGroup ) { try
    	{
            // Validate the input.

            if( oContainer == null )
            {
                return;
            }

    		// Lock the editor so that we can't move away while
    		// condition entry is enabled.  This only prevents
    		// the editor from switching to other pages (user
    		// can still run, save, close, etc.).

    		if( lockEditorPage != null )
    		{
    			lockEditorPage( true , "Condition edit in progress.  Please close all condition edit windows before moving to a new view." );
    		}

            // Forward the request to all listeners.  We pass
            // the primary field key to the entry panels so
            // that they can initialize with pertinent info.

            if( fireReportEvent != null )
            {
                fireReportEvent( "edited" , getElementByIdFromParent(oGroup, "m_PicF").getAttribute("key") , oContainer );
            }
    	}
    	catch( e )
    	{
    		window.status = e.message;
    	}}

    	function closeConditionEditor( oContainer ) { try
    	{
            // Validate the input.

            if( oContainer == null )
            {
                return;
            }

    		// Unlock the editor so that we can move away after
    		// condition entry is disabled.

    		if( lockEditorPage != null )
    		{
    			lockEditorPage( false );
    		}
    	}
    	catch( e )
    	{
    		window.status = e.message;
    	}}

        function processGroupCommand( containerId , storeId , oGroup , sCmd , sUid ) { try
        {
            // Validate the input and dispatch
            // the event to the proper handler.
            var oContainer = document.getElementById(containerId);
            var oStore = document.getElementById(storeId);
            if( oContainer == null || oStore == null || oGroup == null || sCmd == null )
            {
                return;
            }

            switch( sCmd )
            {
                case "addc":
                {
                    // Show the condition editor panel and
                    // fire the activation event.  If any
                    // other panel is currently up, we close
                    // it and fire the closing event.

                    var p = oGroup.children.m_Edit;
                    var o = oGroup.children.m_AddG;

                    if( p.style.display != "none" || o.style.display != "none" )
                    {
                        break;
                    }

                    oStore.value = "";

                    closeAllPanels( oContainer , "m_AddG" );
                    closeAllPanels( oContainer , "m_Edit" );
                    showsAllPanels( oContainer , "m_Cmds" );

    				getElementByIdFromParent(oGroup, "m_Cmds").style.visibility = "hidden";

                    getElementByIdFromParent(p, "m_AokB").disabled = true;

                    getElementByIdFromParent(p, "m_PicF").innerHTML = "select field";
                    getElementByIdFromParent(p, "m_PicF").setAttribute("key", "");
                    getElementByIdFromParent(p, "m_PicF").src = "";

                    getElementByIdFromParent(p, "m_PicO").innerHTML = "select operator";
                    getElementByIdFromParent(p, "m_PicA").setAttribute("key", "");
                    getElementByIdFromParent(p, "m_PicA").src = "";

                    getElementByIdFromParent(p, "m_PicA").innerHTML = "select value";
                    getElementByIdFromParent(p, "m_PicA").setAttribute("key", "");
                    getElementByIdFromParent(p, "m_PicA").src = "";

                    getElementByIdFromParent(p, "m_PicF").className = "ConditionSpec";
                    getElementByIdFromParent(p, "m_PicO").className = "ConditionSpec";
                    getElementByIdFromParent(p, "m_PicA").className = "ConditionSpec";

                    p.style.display = "block";

    				// Tell anyone listening that the editor is
    				// in condition-edit mode.

                    openConditionForEdit( oContainer , oGroup );

                    // Close the condition entry panel.

                    disableConditionEntry( oContainer );
                }
                break;

                case "addg":
                {
                    // Show the condition group panel and
                    // fire the activation event.  If the
                    // other panel is currently up, we close
                    // it and fire the closing event.

                    var p = oGroup.children.m_AddG;
                    var o = oGroup.children.m_Edit;

                    if( p.style.display != "none" || o.style.display != "none" )
                    {
                        break;
                    }

                    oStore.value = "";

                    closeAllPanels( oContainer , "m_AddG" );
                    closeAllPanels( oContainer , "m_Edit" );
                    showsAllPanels( oContainer , "m_Cmds" );

    				getElementByIdFromParent(oGroup, "m_Cmds").style.visibility = "hidden";

                    getElementByIdFromParent(p, "m_AllG").checked = true;

                    p.style.display = "block";

    				// Tell anyone listening that the editor is
    				// in condition-edit mode.

                    openConditionForEdit( oContainer , oGroup );

                    // Close the condition entry panel.

                    disableConditionEntry( oContainer );
                }
                break;

                case "picf":
                {
                    // We have a new focus for the edit condition
                    // sub-gui.  We first unhilight all the parts
                    // and then focus the selected component.

                    var c = getElementByIdFromParent(oGroup, "m_PicF");

                    getElementByIdFromParent(oGroup, "m_PicF").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicO").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicA").removeAttribute(  "style" );

                    getElementByIdFromParent(oGroup, "m_PicO").className = "ConditionSpec";
                    getElementByIdFromParent(oGroup, "m_PicA").className = "ConditionSpec";

                    c.className = "ConditionSpecFocused";

                    // Set this group and condition component as
                    // the currently selected edit panel.

                    oStore.value = oGroup.parentElement.getAttribute("key") + ":field";

                    // Raise the event for other controls.

                    if( fireReportEvent != null )
                    {
                        fireReportEvent( "focusf" , getElementByIdFromParent(oGroup, "m_PicF").getAttribute("key") , oContainer );
                    }

                    enableConditionEntry( oContainer , oGroup );
                }
                break;

                case "pico":
                {
                    // We have a new focus for the edit condition
                    // sub-gui.  We first unhilight all the parts
                    // and then focus the selected component.

                    var c = getElementByIdFromParent(oGroup, "m_PicO");

                    getElementByIdFromParent(oGroup, "m_PicF").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicO").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicA").removeAttribute(  "style" );

                    getElementByIdFromParent(oGroup, "m_PicF").className = "ConditionSpec";
                    getElementByIdFromParent(oGroup, "m_PicA").className = "ConditionSpec";

                    c.className = "ConditionSpecFocused";

                    // Set this group and condition component as
                    // the currently selected edit panel.

                    oStore.value = oGroup.parentElement.getAttribute("key") + ":match";

                    // Raise the event for other controls.

                    if( fireReportEvent != null )
                    {
                        fireReportEvent( "focuso" , getElementByIdFromParent(oGroup, "m_PicF").getAttribute("key") , oContainer );
                    }

                    enableConditionEntry( oContainer , oGroup );
                }
                break;

                case "pica":
                {
                    // We have a new focus for the edit condition
                    // sub-gui.  We first unhilight all the parts
                    // and then focus the selected component.

                    var c = getElementByIdFromParent(oGroup, "m_PicA");

                    getElementByIdFromParent(oGroup, "m_PicF").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicO").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicA").removeAttribute(  "style" );

                    getElementByIdFromParent(oGroup, "m_PicF").className = "ConditionSpec";
                    getElementByIdFromParent(oGroup, "m_PicO").className = "ConditionSpec";

                    c.className = "ConditionSpecFocused";

                    // Set this group and condition component as
                    // the currently selected edit panel.

                    oStore.value = oGroup.parentElement.getAttribute("key") + ":value";

                    // Raise the event for other controls.

                    if( fireReportEvent != null )
                    {
                        fireReportEvent( "focusa" , getElementByIdFromParent(oGroup, "m_PicF").getAttribute("key") , oContainer );
                    }

                    enableConditionEntry( oContainer , oGroup );
                }
                break;

                case "comc":
                {
                    // User has committed the edit operation
                    // so close the edit window and fire the
                    // add new condition event with the parts
                    // that make up the condition.  If the
                    // condition is not fully specified, we
                    // punt.

                    var p = oGroup.children.m_Edit;

                    if( getElementByIdFromParent(p, "m_PicF").getAttribute("key") == "" || getElementByIdFromParent(p, "m_PicO").getAttribute("key") == "" )
                    {
                        break;
                    }

    				getElementByIdFromParent(oGroup, "m_Cmds").style.visibility = "visible";

                    p.style.display = "none";

                    // Fire postback event if the parameters are all
                    // defined.  Otherwise, punt.

                    __doPostBack(sUid, "AddCon:" + oGroup.parentElement.getAttribute("key")
                        + "*" + getElementByIdFromParent(p, "m_PicF").getAttribute("key") + "*"
                        + getElementByIdFromParent(p, "m_PicO").getAttribute("key") + "*"
                        + getElementByIdFromParent(p, "m_PicA").src + "/"
                        + getElementByIdFromParent(p, "m_PicA").getAttribute("key"));

                    // Close the condition entry panel.

                    disableConditionEntry( oContainer );

    				// Note condition editor closing.

    				closeConditionEditor( oContainer );
                }
                break;

                case "canc":
                {
                    // User has canceled the edit operation, so
                    // fire the closing event and close the edit.

                    var p = oGroup.children.m_Edit;

    				getElementByIdFromParent(oGroup, "m_Cmds").style.visibility = "visible";

                    p.style.display = "none";

                    // Close the condition entry panel.

                    disableConditionEntry( oContainer );

    				// Note condition editor closing.

    				closeConditionEditor( oContainer );
                }
                break;

                case "comg":
                {
                    // User has committed the group operation
                    // so close the group window and fire the
                    // add new group event with the type of
                    // group to add.  Fire the closing event
                    // before closing.

                    var p = oGroup.children.m_AddG;

    				getElementByIdFromParent(oGroup, "m_Cmds").style.visibility = "visible";

                    p.style.display = "none";

    				// Note condition editor closing.

    				closeConditionEditor( oContainer );
                }
                break;

                case "cang":
                {
                    // User has canceled the edit operation, so
                    // fire the closing event and close the edit.

                    var p = oGroup.children.m_AddG;

    				getElementByIdFromParent(oGroup, "m_Cmds").style.visibility = "visible";

                    p.style.display = "none";

    				// Note condition editor closing.

    				closeConditionEditor( oContainer );
                }
                break;

                case "updc":
                {
                    // Show the condition editor panel for an
                    // existing condition and fire the activation
                    // event.  If any other panel is currently up,
                    // we close it and fire the closing event.

                    var u = oGroup.children.m_Edit;

                    if( u.style.display != "none" )
                    {
                        break;
                    }

                    oStore.value = "";

                    closeAllPanels( oContainer , "m_AddG" );
                    closeAllPanels( oContainer , "m_Edit" );
                    showsAllPanels( oContainer , "m_Cmds" );

                    getElementByIdFromParent(u, "m_PicF").className = "ConditionSpec";
                    getElementByIdFromParent(u, "m_PicO").className = "ConditionSpec";
                    getElementByIdFromParent(u, "m_PicA").className = "ConditionSpec";

    				// We need to restore the cached condition details
    				// so that the editor always starts with the last
    				// committed condition.

    				var parts , sects = getElementByIdFromParent(u, "m_Cond").innerText.split( ";" );

    				if( sects.length == 3 )
    				{
    					parts = sects[ 0 ].split( ":" );

    					if( parts.length == 3 )
    					{
    						getElementByIdFromParent(u, "m_PicF").innerText = parts[ 0 ];
    						getElementByIdFromParent(u, "m_PicF").setAttribute("key"       , parts[ 1 ]);
    						getElementByIdFromParent(u, "m_PicF").src       = parts[ 2 ];
    					}

    					parts = sects[ 1 ].split( ":" );

    					if( parts.length == 3 )
    					{
    						getElementByIdFromParent(u, "m_PicO").innerText = parts[ 0 ];
    						getElementByIdFromParent(u, "m_PicO").setAttribute("key", parts[ 1 ]);
    						getElementByIdFromParent(u, "m_PicO").src       = parts[ 2 ];
    					}

    					parts = sects[ 2 ].split( ":" );

    					if( parts.length == 3 )
    					{
    						getElementByIdFromParent(u, "m_PicA").innerText = parts[ 0 ];
    						getElementByIdFromParent(u, "m_PicA").setAttribute("key"       , parts[ 1 ]);
    						getElementByIdFromParent(u, "m_PicA").src       = parts[ 2 ];
    					}
    				}

                    getElementByIdFromParent(u, "m_AokB").disabled = true;

                    u.style.display = "block";

    				// Tell anyone listening that the editor is
    				// in condition-edit mode.

                    openConditionForEdit( oContainer , oGroup );

                    // Close the condition entry panel.

                    disableConditionEntry( oContainer );
                }
                break;

                case "upcf":
                {
                    // We have a new focus for the edit condition
                    // sub-gui.  We first unhilight all the parts
                    // and then focus the selected component.

                    var c = getElementByIdFromParent(oGroup, "m_PicF");

                    getElementByIdFromParent(oGroup, "m_PicF").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicO").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicA").removeAttribute(  "style" );

                    getElementByIdFromParent(oGroup, "m_PicO").className = "ConditionSpec";
                    getElementByIdFromParent(oGroup, "m_PicA").className = "ConditionSpec";

                    c.className = "ConditionSpecFocused";

                    // Set this group and condition component as
                    // the currently selected edit panel.

                    oStore.value = oGroup.parentElement.getAttribute("key") + ":field";

                    // Raise the event for other controls.

                    if( fireReportEvent != null )
                    {
                        fireReportEvent( "focusf" , getElementByIdFromParent(oGroup, "m_PicF").getAttribute("key") , oContainer );
                    }

                    // Turn on the condition entry panel.

                    enableConditionEntry( oContainer , oGroup );
                }
                break;

                case "upco":
                {
                    // We have a new focus for the edit condition
                    // sub-gui.  We first unhilight all the parts
                    // and then focus the selected component.

                    var c = getElementByIdFromParent(oGroup, "m_PicO");

                    getElementByIdFromParent(oGroup, "m_PicF").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicO").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicA").removeAttribute(  "style" );

                    getElementByIdFromParent(oGroup, "m_PicF").className = "ConditionSpec";
                    getElementByIdFromParent(oGroup, "m_PicA").className = "ConditionSpec";

                    c.className = "ConditionSpecFocused";

                    // Set this group and condition component as
                    // the currently selected edit panel.

                    oStore.value = oGroup.parentElement.getAttribute("key") + ":match";

                    // Raise the event for other controls.

                    if( fireReportEvent != null )
                    {
                        fireReportEvent( "focuso" , getElementByIdFromParent(oGroup, "m_PicF").getAttribute("key") , oContainer );
                    }

                    // Turn on the condition entry panel.

                    enableConditionEntry( oContainer , oGroup );
                }
                break;

                case "upca":
                {
                    // We have a new focus for the edit condition
                    // sub-gui.  We first unhilight all the parts
                    // and then focus the selected component.

                    var c = getElementByIdFromParent(oGroup, "m_PicA");

                    getElementByIdFromParent(oGroup, "m_PicF").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicO").removeAttribute(  "style" );
                    getElementByIdFromParent(oGroup, "m_PicA").removeAttribute(  "style" );

                    getElementByIdFromParent(oGroup, "m_PicF").className = "ConditionSpec";
                    getElementByIdFromParent(oGroup, "m_PicO").className = "ConditionSpec";

                    c.className = "ConditionSpecFocused";

                    // Set this group and condition component as
                    // the currently selected edit panel.

                    oStore.value = oGroup.parentElement.getAttribute("key") + ":value";

                    // Raise the event for other controls.

                    if( fireReportEvent != null )
                    {
                        fireReportEvent( "focusa" , getElementByIdFromParent(oGroup, "m_PicF").getAttribute("key") , oContainer );
                    }

                    // Turn on the condition entry panel.

                    enableConditionEntry( oContainer , oGroup );
                }
                break;

                case "comu":
                {
                    // User has committed the edit operation
                    // so close the edit window and fire the
                    // add new condition event with the parts
                    // that make up the condition.  If the
                    // condition is not fully specified, we
                    // punt.

                    var u = oGroup.children.m_Edit;

                    if( getElementByIdFromParent(u, "m_PicF").getAttribute("key") == "" || getElementByIdFromParent(u, "m_PicO").getAttribute("key") == "" )
                    {
                        break;
                    }

                    u.style.display = "none";

                    // Fire postback event if the parameters are all
                    // defined.  Otherwise, punt.

                    __doPostBack(sUid, "UpdCon:" + oGroup.parentElement.getAttribute("key") + "*"
                        + getElementByIdFromParent(u, "m_PicF").getAttribute("key") + "*"
                        + getElementByIdFromParent(u, "m_PicO").getAttribute("key")
                        + "*" + getElementByIdFromParent(u, "m_PicA").src + "/" +
                        getElementByIdFromParent(u, "m_PicA").getAttribute("key"));

                    // Close the condition entry panel.

                    disableConditionEntry( oContainer );

    				// Note condition editor closing.

    				closeConditionEditor( oContainer );
                }
                break;

                case "canu":
                {
                    // User has canceled the edit operation, so
                    // fire the closing event and close the edit.

                    var u = oGroup.children.m_Edit;

                    u.style.display = "none";

                    // Close the condition entry panel.

                    disableConditionEntry( oContainer );

    				// Note condition editor closing.

    				closeConditionEditor( oContainer );
                }
                break;

                case "nixg":
                {
                    // Fabricate the right request and postback
                    // to the server for processing.  We are removing
                    // a single condition group from the tree.

                    if( displayQuery != null )
                    {
                        if( displayQuery( "Remove this condition group?" ) == false )
                        {
                            break;
                        }
                    }

    				getElementByIdFromParent(oGroup, "m_Cmds").style.visibility = "visible";

                    __doPostBack( sUid , 'Remove:' + oGroup.parentElement.getAttribute("key") );
                }
                break;

                case "nixc":
                {
                    // Fabricate the right request and postback
                    // to the server for processing.  We are removing
                    // a single condition from the tree.

                    if( displayQuery != null )
                    {
                        if( displayQuery( "Remove this condition?" ) == false )
                        {
                            break;
                        }
                    }

                    __doPostBack( sUid , 'Remove:' + oGroup.parentElement.getAttribute("key") );
                }
                break;

                default:
                {
                    // Lets me know I missed something somewhere.

                    if( displayError != null )
                    {
                        displayError( "Failed to handle " + sCmd );
                    }
                }
                break;
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function specifyFieldEntry( oContainer , oStore , sKeyInfo ) { try
        {
            // Validate the input.

            if( oContainer == null || oStore == null || sKeyInfo == null )
            {
                return;
            }

            if( oStore.value == "" )
            {
                return;
            }

            // Find the currently open edit panel and
            // populate the display span.

            var i;
            var parts;

            if(typeof(stringSplitRemainder) != 'undefined')
            {
                parts = stringSplitRemainder(sKeyInfo, ":", 2);
            }
            else
            {
                parts = sKeyInfo.split( ":" );
            }

            if( parts.length < 2 )
            {
                return;
            }

            var all = oContainer.getElementsByTagName("*");
            for( i = 0 ; i < all.length ; ++i )
            {
                // Find matching tree block node.

                var node = all[i];

                if( node.getAttribute("tid") == null || node.getAttribute("tid") != "nodeBox" )
                {
                    continue;
                }

                if( oStore.value != node.getAttribute("key") + ":field" )
                {
                    continue;
                }

                // We found it.  Set the values and
                // break out of this loop.

                var p = getElementByIdFromParent(node, "m_Edit");

                if( getElementByIdFromParent(p, "m_PicO").getAttribute("key") != "" && getElementByIdFromParent(p, "m_PicA").getAttribute("key") != "" )
                {
                    // Undim the ok button because we have a
                    // complete condition definition.

                    getElementByIdFromParent(p, "m_AokB").disabled = false;
                }

                getElementByIdFromParent(p, "m_PicF").innerText = parts[ 1 ];
                getElementByIdFromParent(p, "m_PicF").setAttribute("key"       , parts[ 0 ]);

                getElementByIdFromParent(p, "m_PicF").src = "field";

                break;
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function specifyMatchEntry( oContainer , oStore , sKeyInfo ) { try
        {
            // Validate the input.

            if( oContainer == null || oStore == null || sKeyInfo == null )
            {
                return;
            }

            if( oStore.value == "" )
            {
                return;
            }

            // Find the currently open edit panel and
            // populate the display span.

            var i;
            var parts;

            if(typeof(stringSplitRemainder) != 'undefined')
            {
                parts = stringSplitRemainder(sKeyInfo, ":", 2);
            }
            else
            {
                parts = sKeyInfo.split( ":" );
            }

            if( parts.length < 2 )
            {
                return;
            }

            var all = oContainer.getElementsByTagName("*");
            for( i = 0 ; i < all.length ; ++i )
            {
                // Find matching tree block node.

                var node = all[i];

                if( node.getAttribute("tid") == null || node.getAttribute("tid") != "nodeBox" )
                {
                    continue;
                }

                if( oStore.value != node.getAttribute("key") + ":match" )
                {
                    continue;
                }

                // We found it.  Set the values and
                // break out of this loop.

                var p = getElementByIdFromParent(node, "m_Edit");

                if( getElementByIdFromParent(p, "m_PicF").getAttribute("key") != "" && getElementByIdFromParent(p, "m_PicA").getAttribute("key") != "" )
                {
                    // Undim the ok button because we have a
                    // complete condition definition.

                    getElementByIdFromParent(p, "m_AokB").disabled = false;
                }

                getElementByIdFromParent(p, "m_PicO").innerText = parts[ 1 ];
                getElementByIdFromParent(p, "m_PicO").setAttribute("key"       , parts[ 0 ]);

                getElementByIdFromParent(p, "m_PicO").src = "match";

                break;
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function endsWith(str, suffix) {
            return str.indexOf(suffix, str.length - suffix.length) !== -1;
        }

        function specifyValueEntry( oContainer , oStore , sKeyInfo ) { try
        {
            // Validate the input.

            if( oContainer == null || oStore == null || sKeyInfo == null )
            {
                return;
            }

            if( oStore.value == "" )
            {
                return;
            }

            // Find the currently open edit panel and
            // populate the display span.

            var i;
            var parts;

            if (endsWith(sKeyInfo, ":value") === false && endsWith(sKeyInfo, ":field") === false && typeof (stringSplitRemainder) != 'undefined')
            {
                parts = stringSplitRemainder(sKeyInfo, ":", 2);
            }
            else
            {
                parts = sKeyInfo.split( ":" );
            }

            if( parts.length < 2 )
            {
                return;
            }

            var all = oContainer.getElementsByTagName("*");
            for( i = 0 ; i < all.length ; ++i )
            {
                // Find matching tree block node.

                var node = all[i];

                if( node.getAttribute("tid") == null || node.getAttribute("tid") != "nodeBox" )
                {
                    continue;
                }

                if( oStore.value != node.getAttribute("key") + ":value" )
                {
                    continue;
                }

                // We found it.  Set the values and
                // break out of this loop.  Note that
                // we check for a third parameter.
                // Only the value portion of the choose
                // entry user control sets this third
                // parameter.  If not found, assume it
                // is a field id and label.

                var p = getElementByIdFromParent(node, "m_Edit");

                if( getElementByIdFromParent(p, "m_PicF").getAttribute("key") != "" && getElementByIdFromParent(p, "m_PicO").getAttribute("key") != "" )
                {
                    // Undim the ok button because we have a
                    // complete condition definition.

                    getElementByIdFromParent(p, "m_AokB").disabled = false;
                }

                getElementByIdFromParent(p, "m_PicA").innerText = parts[ 1 ];
                getElementByIdFromParent(p, "m_PicA").setAttribute("key"       , parts[ 0 ]);

                if( parts[ 2 ] == "value" )
                {
                    getElementByIdFromParent(p, "m_PicA").src = "value";
                }
                else
                {
                    getElementByIdFromParent(p, "m_PicA").src = "field";
                }

                break;
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}
    </script>
    <INPUT id="m_Info" type="hidden" runat="server" name="m_Info">
    <UCR:BlockTreeUserControl id="m_Tree" runat="server" Indent="20px"/>
    <script>
        // If the event handling table is available, we
        // need to link up our event processing functions.

        if( registerForReportEvent != null )
        {
            var base = document.getElementById("<%= AspxTools.ClientId(m_Base) %>" );
            var info = document.getElementById("<%= AspxTools.ClientId(m_Info) %>" );

            registerForReportEvent( base , info , specifyFieldEntry , "picksf" );
            registerForReportEvent( base , info , specifyMatchEntry , "pickso" );
            registerForReportEvent( base , info , specifyValueEntry , "picksa" );
        }
    </script>
</ASP:Panel>
<UCR:TemplatedUserControl id="m_Node" runat="server">
    <Template>
        <div class="ConditionGroup" ondrop="processGroupCommand( '<%# AspxTools.ClientId(m_Base) %>' , '<%# AspxTools.ClientId(m_Info) %>' , this , event.code , <%# AspxTools.JsString(UniqueID) %> );">
            <TABLE cellpadding="1" cellspacing="0" style="WIDTH: 100%;">
            <TR>
            <TD nowrap align="left" style="PADDING-LEFT: 4px; FONT: 11px arial;">
				<%# AspxTools.HtmlString(Container[ "Type" ].ToString()) %>
            </TD>
            <TD nowrap align="right">
				<SPAN id="m_Cmds">
					<INPUT id="m_ConB" type="button" class="ButtonCommand" style="WIDTH: 100px;" value="Add condition" onclick="fireGroupCommand( this , 'addc' );">
					<INPUT id="m_GrpB" type="button" class="ButtonCommand" style="WIDTH:  80px;" value="Add group" onclick="fireGroupCommand( this , 'addg' );">
					<INPUT id="m_NixB" type="button" class="ButtonCommand" style="WIDTH:  20px;" value="X" onclick="fireGroupCommand( this , 'nixg' );">
				</SPAN>
            </TD>
            </TR>
            </TABLE>
            <SPAN id="m_Edit" style="WIDTH: 100%; DISPLAY: none;">
                <HR size="1" color="Whitesmoke">
                <div class="conditionbuttons">
                    <TABLE cellpadding="0" cellspacing="0" style="WIDTH: 100%;">
                    <TR>
                    <TD nowrap align="left">
                        <div id="m_PicF" class="ConditionSpec" key="" src="" onmouseover="if( className == 'ConditionSpec' ) this.style.color = 'orange'; this.style.borderColor = 'orange';" onmouseout="if( className == 'ConditionSpec' ) this.style.color = 'tomato'; this.style.borderColor = 'gainsboro';" onclick="fireGroupCommand( this , 'picf' );">
                            <U>select field
                            </U>
                        </div>
                        <div id="m_PicO" class="ConditionSpec" key="" src="" onmouseover="if( className == 'ConditionSpec' ) this.style.color = 'orange'; this.style.borderColor = 'orange';" onmouseout="if( className == 'ConditionSpec' ) this.style.color = 'tomato'; this.style.borderColor = 'gainsboro';" onclick="fireGroupCommand( this , 'pico' );">
                            <U>select operator
                            </U>
                        </div>
                        <div id="m_PicA" class="ConditionSpec" key="" src="" onmouseover="if( className == 'ConditionSpec' ) this.style.color = 'orange'; this.style.borderColor = 'orange';" onmouseout="if( className == 'ConditionSpec' ) this.style.color = 'tomato'; this.style.borderColor = 'gainsboro';" onclick="fireGroupCommand( this , 'pica' );">
                            <U>select value
                            </U>
                        </div>
                    </TD>
                    </TR>
                    </TABLE>
                    <TABLE cellpadding="0" cellspacing="0" style="WIDTH: 100%;">
                    <TR>
                    <TD nowrap align="left" style="PADDING-LEFT: 4px; FONT: italic 11px arial; COLOR: dimgray;">
                        <SPAN style="MARGIN: 8px;">
                            Specify three parameters, then click OK
                        </SPAN>
                    </TD>
                    <TD nowrap align="right">
						<INPUT id="m_AokB" type="button" class="ButtonCommit" style="MARGIN: 8px; MARGIN-RIGHT: 1px; WIDTH: 32px;" value="OK" onclick="fireGroupCommand( this , 'comc' );">
						<INPUT id="m_CclB" type="button" class="ButtonCommit" style="MARGIN: 8px; MARGIN-LEFT: 1px; WIDTH: 58px;" value="Cancel" onclick="fireGroupCommand( this , 'canc' );">
                    </TD>
                    </TR>
                    </TABLE>
                </div>
            </SPAN>
            <SPAN id="m_AddG" style="WIDTH: 100%; DISPLAY: none;" ondrop="if( event.code == 'comg' ) __doPostBack( <%# AspxTools.JsString(Container[ "Uid" ].ToString()) %> , ( getElementByIdFromParent(this, 'm_AllG').checked == true ? 'AddAll:' : 'AddAny:' ) + <%# AspxTools.JsString(Container[ "Qid" ].ToString()) %> );">
                <HR size="1" color="Whitesmoke">
                <div class="conditionbuttons">
                    <TABLE cellpadding="0" cellspacing="0" style="WIDTH: 100%; BACKGROUND-COLOR: white;">
                    <TR>
                    <TD nowrap align="left">
                        <div class="CGroupingSpec" onmouseover="this.style.borderColor = 'orange';" onmouseout="this.style.borderColor = 'gainsboro';" onclick="this.children.m_AllG.click();">
                            <INPUT id="m_AllG" type="radio" style="VERTICAL-ALIGN: middle; FONT: 11px arial;" name="AddG"/>
                            All children must be true
                        </div>
                        <div class="CGroupingSpec" onmouseover="this.style.borderColor = 'orange';" onmouseout="this.style.borderColor = 'gainsboro';" onclick="this.children.m_OneG.click();">
                            <INPUT id="m_OneG" type="radio" style="VERTICAL-ALIGN: middle;" name="AddG"/>
                            At least one must be true
                        </div>
                    </TD>
                    </TR>
                    </TABLE>
                    <TABLE cellpadding="0" cellspacing="0" style="WIDTH: 100%; BACKGROUND-COLOR: white;">
                    <TR>
                    <TD nowrap align="left" style="PADDING-LEFT: 4px; FONT: italic 11px arial; COLOR: dimgray;">
                        <SPAN style="MARGIN: 8px;">
                            Choose group type, then click OK
                        </SPAN>
                    </TD>
                    <TD nowrap align="right">
						<INPUT type="button" class="ButtonCommit" style="MARGIN: 8px; MARGIN-RIGHT: 1px; WIDTH: 32px;" value="OK" onclick="fireGroupCommand( this , 'comg' );">
						<INPUT type="button" class="ButtonCommit" style="MARGIN: 8px; MARGIN-LEFT: 1px; WIDTH: 58px;" value="Cancel" onclick="fireGroupCommand( this , 'cang' );">
                    </TD>
                    </TR>
                    </TABLE>
                </div>
            </SPAN>
        </div>
    </Template>
</UCR:TemplatedUserControl>
<UCR:TemplatedUserControl id="m_Item" runat="server">
    <Template>
        <div class="ConditionItem" ondrop="processGroupCommand( '<%# AspxTools.ClientId(m_Base) %>' , '<%# AspxTools.ClientId(m_Info)%>' , this , event.code , <%# AspxTools.JsString(UniqueID) %>);">
            <TABLE cellpadding="1" cellspacing="0">
            <TR>
            <TD nowrap>
				<INPUT type="button" class="ButtonCommand" style="WIDTH: 20px; VERTICAL-ALIGN: middle;" value="X" onclick="fireGroupCommand( this , 'nixc' );">
				<INPUT type="button" class="ButtonCommand" style="WIDTH: 50px; VERTICAL-ALIGN: middle;" value="Edit" onclick="fireGroupCommand( this , 'updc' );">
            </TD>
            <TD nowrap align="left" style="PADDING-LEFT: 8px; FONT: 11px arial;">
	            <%# AspxTools.HtmlString(Container[ "Name" ].ToString()) %> <%# AspxTools.HtmlString(Container[ "Type" ].ToString()) %> <%# AspxTools.HtmlString(Container[ "ArgV" ].ToString()) %>
            </TD>
            </TR>
            </TABLE>
            <SPAN id="m_Edit" style="WIDTH: 100%; DISPLAY: none;">
                <HR size="1" color="Gray">
                <div class="conditionbuttons">
                    <TABLE cellpadding="0" cellspacing="0" style="WIDTH: 100%;">
                    <TR>
                    <TD nowrap align="left">
                        <SPAN id="m_PicF" class="ConditionSpec" key="" src="" onmouseover="if( className == 'ConditionSpec' ) this.style.color = 'orange'; this.style.borderColor = 'orange';" onmouseout="if( className == 'ConditionSpec' ) this.style.color = 'tomato'; this.style.borderColor = 'gainsboro';" onclick="fireGroupCommand( this , 'upcf' );">
                        </SPAN>
                        <SPAN id="m_PicO" class="ConditionSpec" key="" src="" onmouseover="if( className == 'ConditionSpec' ) this.style.color = 'orange'; this.style.borderColor = 'orange';" onmouseout="if( className == 'ConditionSpec' ) this.style.color = 'tomato'; this.style.borderColor = 'gainsboro';" onclick="fireGroupCommand( this , 'upco' );">
                        </SPAN>
                        <SPAN id="m_PicA" class="ConditionSpec" key="" src="" onmouseover="if( className == 'ConditionSpec' ) this.style.color = 'orange'; this.style.borderColor = 'orange';" onmouseout="if( className == 'ConditionSpec' ) this.style.color = 'tomato'; this.style.borderColor = 'gainsboro';" onclick="fireGroupCommand( this , 'upca' );">
                        </SPAN>
                        <SPAN id="m_Cond" style="DISPLAY: none;">
							<%# AspxTools.HtmlString(Container[ "Data" ].ToString()) %>
                        </SPAN>
                    </TD>
                    </TR>
                    </TABLE>
                    <TABLE cellpadding="0" cellspacing="0" style="WIDTH: 100%;">
                    <TR>
                    <TD nowrap align="left" style="PADDING-LEFT: 4px; FONT: italic 11px arial; COLOR: dimgray;">
                        <SPAN style="MARGIN: 8px;">
                            Click OK to save update
                        </SPAN>
                    </TD>
                    <TD nowrap align="right">
						<INPUT id="m_AokB" type="button" class="ButtonCommit" style="MARGIN: 8px; MARGIN-RIGHT: 1px; WIDTH: 32px;" value="OK" onclick="fireGroupCommand( this , 'comu' );">
						<INPUT id="m_AclB" type="button" class="ButtonCommit" style="MARGIN: 8px; MARGIN-LEFT: 1px; WIDTH: 58px;" value="Cancel" onclick="fireGroupCommand( this , 'canu' );">
                    </TD>
                    </TR>
                    </TABLE>
                </div>
            </SPAN>
        </div>
    </Template>
</UCR:TemplatedUserControl>
<UCR:TemplatedUserControl id="m_List" runat="server">
    <Template>
        <div class="ConditionItem">
            <TABLE cellpadding="0" cellspacing="0" style="WIDTH: 100%;">
            <TR>
            <TD nowrap align="left" style="PADDING: 4px; FONT: 11px arial;">
	            <%# AspxTools.HtmlString(Container[ "Name" ].ToString()) %> <%# AspxTools.HtmlString(Container[ "Type" ].ToString()) %> <%# AspxTools.HtmlString(Container[ "ArgV" ].ToString()) %>
            </TD>
            </TR>
            </TABLE>
        </div>
    </Template>
</UCR:TemplatedUserControl>
<UCR:TemplatedUserControl id="m_Read" runat="server">
    <Template>
        <div class="ConditionGroup" style="WIDTH: 220px;">
            <TABLE cellpadding="0" cellspacing="0" style="WIDTH: 100%;">
            <TR>
            <TD nowrap align="left" style="PADDING: 4px; FONT: 11px arial;">
				<%# AspxTools.HtmlString(Container[ "Type" ].ToString()) %>
            </TD>
            </TR>
            </TABLE>
        </div>
    </Template>
</UCR:TemplatedUserControl>
