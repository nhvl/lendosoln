using System;
using System.Data;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for FieldGroupsControl.
	/// </summary>

	public partial  class FieldGroupsUserControl : System.Web.UI.UserControl , INamingContainer
	{
		private LendersOffice.QueryProcessor.Query            m_View;
		private LendersOffice.QueryProcessor.FieldLookup      m_Look;

		public String Selection
		{
			// Access member.

			set
			{
				m_CurrF.Value = value;
			}
			get
			{
				return m_CurrF.Value;
			}
		}

		public Query DataSource
		{
			// Access member.

			set
			{
				m_View = value;
			}
		}

		public FieldLookup DataLookup
		{
			// Access member.

			set
			{
				m_Look = value;
			}
		}

		public String CssClass
		{
			// Access member.

			set
			{
				m_Base.CssClass = value;
			}
		}

		public String Height
		{
			// Access member.

			set
			{
				m_Base.Height = Unit.Parse( value );

				if( m_Base.Height.Type == UnitType.Pixel )
				{
					m_Root.Height = Unit.Pixel( Convert.ToInt32( m_Base.Height.Value ) - 22 );
				}
				else
				{
					m_Root.Height = Unit.Percentage( 100 );
				}
			}
		}

		public String Width
		{
			// Access member.

			set
			{
				m_Base.Width = Unit.Parse( value );
			}
		}

		/// <summary>
		/// Bind the data to the current ui status.
		/// </summary>

		public void BindData()
		{
			// Reorder the groupby list to match that of
			// the data store, but only if the store is
			// valid (i.e., only on postback).  So, the
			// client drives the state of this list now.
			// Note that removal from the store list on
			// the client-side will propagate to removal
			// from the server image of the query's group
			// list.

			if( IsPostBack == true )
			{
				m_View.GroupBy.Clear();

				foreach( string field in m_Store.Value.Split( '-' ) )
				{
					string[] parts = field.Split( '+' );

					if( parts.Length != 2 )
					{
						continue;
					}

					m_View.GroupBy.Add( parts[ 0 ] , Convert.ToInt32( parts[ 1 ] ) );
				}
			}
		}

		/// <summary>
		/// Bind the report layout to our list.
		/// </summary>

		public void BindView()
		{
			// Use the current grouping and list the ordered
			// entries.

			if( IsPostBack == false )
			{
				HoldView();
			}
		}

		/// <summary>
		/// Bind the report layout to our list.
		/// </summary>

		public void HoldView()
		{
			// Use the current grouping and list the ordered
			// entries.

			ArrayList group = new ArrayList();

			m_Store.Value = "";

			if( m_View != null && m_Look != null )
			{
				foreach( Descriptor desc in m_View.GroupBy )
				{
					// Add groupby field in order.

					Field item = m_Look.LookupById( desc.Id );

					if( item != null )
					{
                        item.Perm = LoanFieldSecurityManager.SecureFieldPermissions(desc.Id);
						group.Add( new FieldEntry( item.NamePerm , desc.Id , desc.Option ) );

						m_Store.Value += "-" + desc.Id + "+" + desc.Option + "-";
					}
				}
			}

			m_List.DataSource = group;
			m_List.DataBind();
		}

		/// <summary>
		///	Bind data and render lists.
		/// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
			// Use current columns to list report.

			HoldView();
		}

		/// <summary>
		///	Initialize this control.
		/// </summary>

		protected void ControlLoad( object sender , System.EventArgs e )
		{
			// Update the data elements according
			// to the state of the event store.
			// When we bind the view, the changes
			// that we make here should already be
			// in effect.

			BindData();
		}

		/// <summary>
		///	Initialize this control.
		/// </summary>

		protected void ControlInit( object sender , System.EventArgs e )
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.ControlLoad);
            this.Init += new System.EventHandler(this.ControlInit);
            this.PreRender += new System.EventHandler(this.ControlPreRender);

        }
		#endregion

	}

}
