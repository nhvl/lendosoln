<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FilterRangeUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.FilterRangeUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ASP:Panel id="m_Base" runat="server">
    <script>
        function onSelectFilterEvent( baseId , storeId , sArg ) { try
        {
            // Validate input controls.

            var oBase = document.getElementById(baseId);
            var oStore = document.getElementById(storeId);
            if( oBase == null || oStore == null || sArg == null )
            {
                return;
            }

            // Check if the invalid value was set.  If so,
            // then punt by disabling all the selection
            // controls.  If not invalid, then reactivate.

            var i , n;

            var all = oBase.getElementsByTagName("*");
            for( i = 0 , n = all.length ; i < n ; ++i )
            {
                var item = all[i];

                if( item.id != null && item.id == "item" )
                {
                    if( item.getAttribute("key") != null && item.getAttribute("key") == sArg )
                    {
    					if( oStore.value.indexOf( "-" + sArg + "-" ) != -1 )
    					{
    	                    oStore.value = oStore.value.replace( "-" + sArg + "-" , "" );

    	                    item.className = "FilterEventItemInactive";
    					}
    					else
    					{
    	                    oStore.value = oStore.value.concat( "-" + sArg + "-" );

    	                    item.className = "FilterEventItemSelected";
    					}

    					break;
                    }
                }
            }

            // We now have a dirty report.

            if( markReportAsDirty() != null )
            {
                markReportAsDirty();
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function onChangeFilterRange( oBase , oItem , sArg ) { try
        {
            // Validate input controls.

            if( oBase == null || oItem == null || sArg == null )
            {
                return;
            }

            // We now have a dirty report.

            if( markReportAsDirty() != null )
            {
                markReportAsDirty();
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function enableFilterRBtn( oBase , oItem , sArg ) { try
        {
            // Validate input controls.

            if( oBase == null || oItem == null || sArg == null )
            {
                return;
            }

            // Disable the passed in control.

            oItem.disabled = false;
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function disableFilterBox( oBase , oItem , sArg ) { try
        {
            // Validate input controls.

            if( oBase == null || oItem == null || sArg == null )
            {
                return;
            }

            // Disable the passed in control.

            oItem.disabled = true;
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function enableFilterList( oBase , oList , sArg ) { try
        {
            // Validate input controls.

            if( oBase == null || oList == null || sArg == null )
            {
                return;
            }

            // Lighten up the background.  We turn on
            // the current selection in another call.

            oList.style.backgroundColor = "White";
            setDisabledAttr(oList, false);
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function darkenFilterList( oBase , oList , sArg ) { try
        {
            // Validate input controls.

            if( oBase == null || oList == null || sArg == null )
            {
                return;
            }

            // Turn off any selected list item.

            var i , n;

            var all = oBase.getElementsByTagName("*");
            for( i = 0 , n = all.length ; i < n ; ++i )
            {
                var item = all[i];

                if( item.id != null && item.id == "item" )
                {
                    item.className = "FilterEventItemInactive";
                }
            }

            // Darken the background.

            oList.style.backgroundColor = "Gainsboro";
            setDisabledAttr(oList, true);
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function resetFilterRange( oBase , oStore , sArg ) { try
        {
            // Validate input controls.

            if( oBase == null || oStore == null || sArg == null )
            {
                return;
            }

            // Reset the store value.  On postback, the
            // data model will be reset as well.

            if( oStore.value == "" )
            {
                return;
            }

            oStore.value = "";

            // We now have a dirty report.

            if( markReportAsDirty() != null )
            {
                markReportAsDirty();
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function focusOnStartList( oList , oDate , sArg ) { try
        {
            // Validate the controls.

            if( oList == null || oDate == null || sArg == null )
            {
                return;
            }

            // Enable the list and disable the date.

            oList.style.backgroundColor = "White";
            oList.disabled              = false;

            oDate.style.backgroundColor = "Gainsboro";
            oDate.disabled              = true;

            oDate.value = "";
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function focusOnStartDate( oDate , oList , sArg ) { try
        {
            // Validate the controls.

            if( oDate == null || oList == null || sArg == null )
            {
                return;
            }

            // Enable the list and disable the date.

            oDate.style.backgroundColor = "White";
            oDate.disabled              = false;

            oList.style.backgroundColor = "Gainsboro";
            oList.disabled              = true;
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function focusOnFinalList( oList , oDate , sArg ) { try
        {
            // Validate the controls.

            if( oList == null || oDate == null || sArg == null )
            {
                return;
            }

            // Enable the list and disable the date.

            oList.style.backgroundColor = "White";
            oList.disabled              = false;

            oDate.style.backgroundColor = "Gainsboro";
            oDate.disabled              = true;

            oDate.value = "";
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function focusOnFinalDate( oDate , oList , sArg ) { try
        {
            // Validate the controls.

            if( oDate == null || oList == null || sArg == null )
            {
                return;
            }

            // Enable the list and disable the date.

            oDate.style.backgroundColor = "White";
            oDate.disabled              = false;

            oList.style.backgroundColor = "Gainsboro";
            oList.disabled              = true;
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function checkRangeOption( oBase , oRadio , sArg ) { try
        {
            // Validate input controls.

            if( oBase == null || oRadio == null || sArg == null )
            {
                return;
            }

            // Delegate by checking the radio button.

            if( oRadio.checked != null && oRadio.checked == true )
            {
                oRadio.click();
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function clickRangeOption( oBase , oRadio , sArg ) { try
        {
            // Validate input controls.

            if( oBase == null || oRadio == null || sArg == null )
            {
                return;
            }

            // Delegate by clicking the radio button.

            oRadio.click();
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function controlFilterRange( oBase , oActive , sArg ) { try
        {
            // Validate the controls.

            if( oBase == null || oActive == null || sArg == null )
            {
                return;
            }

            // Switch on the argument to turn on the panel
            // or shut it off.

            if( sendReportEvent != null )
            {
                if( oActive.checked == true )
                {
                    sendReportEvent( "enable" , "" , oBase );
                }
                else
                {
                    sendReportEvent( "darken" , "" , oBase );
                }
            }

            // We now have a dirty report.

            if( markReportAsDirty() != null )
            {
                markReportAsDirty();
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function initFilterEvents( oBase , oStore , sArg ) { try
        {
            // Validate the controls.

            if( oBase == null || oStore == null || sArg == null )
            {
                return;
            }

            // Walk all the list items and highlight the
            // current selection.

            var i , n , seed = "";

            var all = oBase.getElementsByTagName("*");
            for( i = 0 , n = all.length ; i < n ; ++i )
            {
                var item = all[i];

                if( item.id != null && item.id == "item" )
                {
                    if (hasDisabledAttr(item.parentElement))
                    {
                        return;
                    }

                    if( item.getAttribute("key") != null )
                    {
                        if( seed == "" )
                        {
                            seed = item.getAttribute("key");
                        }

                        if( oStore.value.indexOf( "-" + item.getAttribute("key") + "-" ) != -1 )
                        {
                            item.className = "FilterEventItemSelected";

                            continue;
                        }
                    }

                    item.className = "FilterEventItemInactive";
                }
            }

            if( oStore.value == "" && seed != "" )
            {
                onSelectFilterEvent( oBase , oStore , seed );
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function initFilterPanels( oBase , oSwitch , sArg ) { try
        {
            // Validate the controls.

            if( oBase == null || oSwitch == null || sArg == null )
            {
                return;
            }

            // Check the status of the switch.  We always
            // start the ui in the enabled state, during
            // processing of the onload event chain, then
            // we darken if not active.

            if( sendReportEvent != null )
            {
                if( oSwitch.checked != true )
                {
                    sendReportEvent( "darken" , "" , oBase );
                }
            }
        }
        catch( e )
        {
            window.status = e.message;
        }}
    </script>
    <INPUT id=m_Store type=hidden value="" runat="server">
    <DIV style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; WIDTH: 100%; PADDING-TOP: 8px">
        <DIV style="WIDTH: 100%">
            <SPAN onclick="controlFilterRange( <%= AspxTools.ClientId(m_Base) %> , <%= AspxTools.ClientId(m_Active) %> , '' );">
                <ASP:CheckBox id="m_Active" runat="server" Checked="True"/>
            </SPAN>
            <SPAN style="PADDING-LEFT: 0px; FONT: bold 11px arial">
                Include only loans with statuses in range...
            </SPAN>
        </DIV>
        <DIV style="PADDING-RIGHT: 20px; MARGIN-TOP: 12px; PADDING-LEFT: 20px; ">
            <TABLE cellSpacing=0 cellPadding=0 width="100%">
            <TR vAlign=top>
            <TD align=left>
                <TABLE cellSpacing=0 cellPadding=0>
                <TR>
                <TD>
                    <ASP:Panel id=m_Root style="BORDER: 1px inset; BORDER-RIGHT: 0px inset; OVERFLOW-Y: scroll; BACKGROUND-COLOR: white" runat="server" EnableViewState="False" Width="160" Height="364">
                        <ASP:Repeater id=m_List runat="server" EnableViewState="False">
                            <ItemTemplate>
                                <DIV id="item" key=<%# AspxTools.HtmlAttribute(DataBinder.Eval( Container.DataItem , "Key" ).ToString()) %> 
                                    class="FilterEventItemInactive" onmouseover="if( className.indexOf( 'Inactive' ) != -1 ) style.backgroundColor = 'whitesmoke';" 
                                    onmouseout="style.backgroundColor = '';" 
                                    onclick="if( <%= AspxTools.ClientId(m_Active) %>.checked == false ) return; style.backgroundColor = ''; onSelectFilterEvent( '<%# AspxTools.ClientId(m_Base) %>' , '<%# AspxTools.ClientId(m_Store) %>' , this.getAttribute('key') );">
                                    <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" ).ToString()) %>
                                </DIV>
                            </ItemTemplate>
                        </ASP:Repeater>
                    </ASP:Panel>
                    <DIV style="PADDING: 4px; FONT: italic 11px arial; COLOR: dimgray; TEXT-ALIGN: center">
						Select multiple to include more
					</DIV>
                </TD>
                </TR>
                </TABLE>
            </TD>
            <TD align=right>
                <TABLE cellSpacing=0 cellPadding=0>
                <TR vAlign=top>
                <TD align=left>
                    <DIV class="FilterRangeLabel" style="FONT-WEIGHT: normal; TEXT-ALIGN: right">
                        From date:
                    </DIV>
                </TD>
                <TD>
                    <DIV onclick="if( <%= AspxTools.ClientId(m_Active) %>.checked == false ) return; 
                        focusOnStartList( document.getElementById('<%= AspxTools.ClientId(m_StartList) %>') , document.getElementById('<%= AspxTools.ClientId(m_StartDate) %>') , '' ); 
                        <%= AspxTools.ClientId(m_StartUseL) %>.checked = true;" 
                        ondrop="onChangeFilterRange( document.getElementById('<%= AspxTools.ClientId(m_Base) %>') , document.getElementById('<%= AspxTools.ClientId(m_StartList) %>') , '' );" 
                        option="slist">
                        <ASP:RadioButton id=m_StartUseL runat="server" GroupName="Start">
                        </ASP:RadioButton>
                        <ASP:DropDownList id=m_StartList runat="server" width="160" onchange="fireEvent( 'ondrop' );">
                        </ASP:DropDownList>
                    </DIV>
                    <DIV onclick="if( <%= AspxTools.ClientId(m_Active) %>.checked == false ) return; focusOnStartDate( document.getElementById('<%= AspxTools.ClientId(m_StartDate) %>') , document.getElementById('<%= AspxTools.ClientId(m_StartList) %>') , '' ); document.getElementById('<%= AspxTools.ClientId(m_StartUseD) %>').checked = true;" 
                        ondrop="onChangeFilterRange( document.getElementById('<%= AspxTools.ClientId(m_Base) %>') , document.getElementById('<%= AspxTools.ClientId(m_StartDate) %>') , '' );" 
                        option="sdate">
                        <ASP:RadioButton id=m_StartUseD runat="server" GroupName="Start">
                        </ASP:RadioButton>
                        <ML:DateTextBox id=m_StartDate style="PADDING-LEFT: 4px" runat="server" Width="138" helperalign="absmiddle" preset="date" onchange="fireEvent( 'ondrop' );">
                        </ML:DateTextBox>
                    </DIV>
                    <DIV style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; FONT: italic 11px arial; COLOR: dimgray; PADDING-TOP: 4px; TEXT-ALIGN: center">
                        Example: 3/01/06
                    </DIV>
                </TD>
                </TR>
                <TR vAlign=top>
                <TD align=left>
                <BR>
                <BR>
                </TD>
                </TR>
                <TR vAlign=top>
                <TD align=left>
                    <DIV class="FilterRangeLabel" style="FONT-WEIGHT: normal; TEXT-ALIGN: right">
                        To date:
                    </DIV>
                </TD>
                <TD>
                    <DIV onclick="if( <%= AspxTools.JsGetElementById(m_Active) %>.checked == false ) return; focusOnFinalList( <%= AspxTools.JsGetElementById(m_FinalList) %> , <%= AspxTools.JsGetElementById(m_FinalDate) %> , '' ); <%= AspxTools.JsGetElementById(m_FinalUseL) %>.checked = true;" ondrop="onChangeFilterRange( <%= AspxTools.JsGetElementById(m_Base) %> , <%= AspxTools.JsGetElementById(m_FinalList) %> , '' );" option="flist">
                        <ASP:RadioButton id=m_FinalUseL runat="server" GroupName="Final">
                        </ASP:RadioButton>
                        <ASP:DropDownList id=m_FinalList runat="server" Width="160" onchange="fireEvent( 'ondrop' );">
                        </ASP:DropDownList>
                    </DIV>
                    <DIV onclick="if( <%= AspxTools.ClientId(m_Active) %>.checked == false ) return; focusOnFinalDate( <%= AspxTools.JsGetElementById(m_FinalDate) %> , <%= AspxTools.JsGetElementById(m_FinalList) %> , '' ); <%= AspxTools.JsGetElementById(m_FinalUseD) %>.checked = true;" ondrop="onChangeFilterRange( <%= AspxTools.JsGetElementById(m_Base) %> , <%= AspxTools.JsGetElementById(m_FinalDate) %> , '' );" option="fdate">
                        <ASP:RadioButton id=m_FinalUseD runat="server" GroupName="Final">
                        </ASP:RadioButton>
                        <ML:DateTextBox id=m_FinalDate style="PADDING-LEFT: 4px" runat="server" Width="138" helperalign="absmiddle" preset="date" onchange="fireEvent( 'ondrop' );">
                        </ML:DateTextBox>
                    </DIV>
                    <DIV style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; FONT: italic 11px arial; COLOR: dimgray; PADDING-TOP: 4px; TEXT-ALIGN: center">
                        Example: 3/30/06
                    </DIV>
                </TD>
                </TR>
                </TABLE>
            </TD>
            </TR>
            </TABLE>
        </DIV>
    </DIV>
    <script>
        // If the event handling table is available, we
        // need to link up our event processing functions.

        if( registerForReportEvent != null )
        {
            // Note: these are highly coupled now -- sorry.  Watch out
            // for ordering dependencies.  For example, the choose date
            // and list functions require that a valid event is placed
            // in the store's first slot (event=Y,...).  So, make sure
            // on enable that the event list is properly selected prior
            // to calling the check focus functions in the enable event
            // chain.

            var susl = document.getElementById("<%= AspxTools.ClientId(m_StartUseL) %>" );
            var susd = document.getElementById("<%= AspxTools.ClientId(m_StartUseD) %>" );
            var fusl = document.getElementById("<%= AspxTools.ClientId(m_FinalUseL) %>" );
            var fusd = document.getElementById("<%= AspxTools.ClientId(m_FinalUseD) %>" );
            var slst = document.getElementById("<%= AspxTools.ClientId(m_StartList) %>" );
            var sdat = document.getElementById("<%= AspxTools.ClientId(m_StartDate) %>" );
            var flst = document.getElementById("<%= AspxTools.ClientId(m_FinalList) %>" );
            var fdat = document.getElementById("<%= AspxTools.ClientId(m_FinalDate) %>" );
            var chek = document.getElementById("<%= AspxTools.ClientId(m_Active) %>" );
            var stor = document.getElementById("<%= AspxTools.ClientId(m_Store) %>" );
            var root = document.getElementById("<%= AspxTools.ClientId(m_Root) %>" );
            var base = document.getElementById("<%= AspxTools.ClientId(m_Base) %>" );

            window.<%= AspxTools.ClientId(m_Active) %> = chek;

            registerForReportEvent( base , root , enableFilterList , "enable" );
            registerForReportEvent( base , susl , enableFilterRBtn , "enable" );
            registerForReportEvent( base , susd , enableFilterRBtn , "enable" );
            registerForReportEvent( base , fusl , enableFilterRBtn , "enable" );
            registerForReportEvent( base , fusd , enableFilterRBtn , "enable" );
            registerForReportEvent( base , susl , checkRangeOption , "onload" );
            registerForReportEvent( base , susd , checkRangeOption , "onload" );
            registerForReportEvent( base , fusl , checkRangeOption , "onload" );
            registerForReportEvent( base , fusd , checkRangeOption , "onload" );
            registerForReportEvent( base , chek , initFilterPanels , "onload" );
            registerForReportEvent( base , stor , initFilterEvents , "onload" );

            registerForReportEvent( base , root , enableFilterList , "enable" );
            registerForReportEvent( base , stor , initFilterEvents , "enable" );
            registerForReportEvent( base , susl , enableFilterRBtn , "enable" );
            registerForReportEvent( base , susd , enableFilterRBtn , "enable" );
            registerForReportEvent( base , fusl , enableFilterRBtn , "enable" );
            registerForReportEvent( base , fusd , enableFilterRBtn , "enable" );
            registerForReportEvent( base , susl , clickRangeOption , "enable" );
            registerForReportEvent( base , fusl , clickRangeOption , "enable" );

            registerForReportEvent( base , susl , disableFilterBox , "darken" );
            registerForReportEvent( base , susd , disableFilterBox , "darken" );
            registerForReportEvent( base , slst , disableFilterBox , "darken" );
            registerForReportEvent( base , sdat , disableFilterBox , "darken" );
            registerForReportEvent( base , fusl , disableFilterBox , "darken" );
            registerForReportEvent( base , fusd , disableFilterBox , "darken" );
            registerForReportEvent( base , flst , disableFilterBox , "darken" );
            registerForReportEvent( base , fdat , disableFilterBox , "darken" );
            registerForReportEvent( base , root , darkenFilterList , "darken" );
            registerForReportEvent( base , stor , resetFilterRange , "darken" );
        }
    </script>
</ASP:Panel>
