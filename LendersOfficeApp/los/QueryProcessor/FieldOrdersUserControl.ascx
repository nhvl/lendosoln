<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FieldOrdersUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.FieldOrdersUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ASP:Panel id="m_Base" runat="server" EnableViewState="False">
    <INPUT type="hidden" id="m_CurrentSelection" runat="server" name="m_CurrentSelection" value="">
    <DIV style="MARGIN: 4px;">
        <SPAN style="WIDTH: 40%;" uid=<%= AspxTools.HtmlAttribute(UniqueID) %> key="St" onclick="onOrderingChanged( this );">
            <ASP:CheckBox id="m_St" runat="server" Text="Sort" TextAlign="Right" EnableViewState="False">
            </ASP:CheckBox>
        </SPAN>
        <SPAN style="WIDTH: 40%;" uid=<%= AspxTools.HtmlAttribute(UniqueID) %> key="Gb" onclick="onOrderingChanged( this );">
            <ASP:CheckBox id="m_Gb" runat="server" Text="Group" TextAlign="Right" EnableViewState="False">
            </ASP:CheckBox>
        </SPAN>
    </DIV>
</ASP:Panel>
<script>
    function onOrderingChanged( oBox )
    {
        // Validate control.

        if( oBox == null || oBox.uid == null || oBox.key == null )
        {
            return;
        }

        // Parse out base control id and delegate.

        __doPostBack( oBox.uid , "Change:" + oBox.key );
    }
</script>
