<%@ Control Language="c#" AutoEventWireup="false" Codebehind="OptionEntryUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.OptionEntryUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="UCR" TagName="OperatorSetUserControl" Src="../QueryProcessor/OperatorSetUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="ChooseEntryUserControl" Src="../QueryProcessor/ChooseEntryUserControl.ascx" %>
<%@ Register TagPrefix="UCR" TagName="SearchListUserControl" Src="../QueryProcessor/SearchListUserControl.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ASP:Panel id="m_Base" runat="server" EnableViewState="False">
    <script>
        function displayFieldEntry( oContainer , oStore , sKeyId ) { try
        {
            // Get all the panels and close them.

            var f = oContainer.children.m_ListB;
            var o = oContainer.children.m_OSetB;
            var a = oContainer.children.m_ValuB;
            var w = oContainer.children.m_WallB;

            f.className = o.className = a.className = w.className = "Hidden";

            f.className = "Active";
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function displayMatchEntry( oContainer , oStore , sKeyId ) { try
        {
            // Get all the panels and close them.

            var f = oContainer.children.m_ListB;
            var o = oContainer.children.m_OSetB;
            var a = oContainer.children.m_ValuB;
            var w = oContainer.children.m_WallB;

            f.className = o.className = a.className = w.className = "Hidden";

            o.className = "Active";
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function displayValueEntry( oContainer , oStore , sKeyId ) { try
        {
            // Get all the panels and close them.

            var f = oContainer.children.m_ListB;
            var o = oContainer.children.m_OSetB;
            var a = oContainer.children.m_ValuB;
            var w = oContainer.children.m_WallB;

            f.className = o.className = a.className = w.className = "Hidden";

            a.className = "Active";
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function hideEntryEditing( oContainer , oStore , sArg ) { try
        {
            // The condition editing has closed, so disable
            // this edit panel.

            var f = oContainer.children.m_ListB;
            var o = oContainer.children.m_OSetB;
            var a = oContainer.children.m_ValuB;
            var w = oContainer.children.m_WallB;

            f.className = o.className = a.className = w.className = "Hidden";

            w.className = "Active";
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function showEntryEditing( oContainer , oStore , sArg ) { try
        {
            // The condition editing has opened, so enable
            // this edit panel.

            var w = oContainer.children.m_WallB;

            w.className = "Hidden";
        }
        catch( e )
        {
            window.status = e.message;
        }}

        function initializeEditing( oContainer , oBase , sArg ) { try
        {
            // The condition editing has opened, so enable
            // this edit panel.

            if( oContainer == null || oBase == null || sArg == null )
            {
                return;
            }

            // Initialize editing panel for subsequent use.

            hideEntryEditing( oContainer , oBase , sArg );
        }
        catch( e )
        {
            window.status = e.message;
        }}
    </script>
    <INPUT type="hidden" id="m_CurrE" name="m_CurrE" runat="server" value="">
    <DIV id="m_ListB" class="Hidden">
        <UCR:SearchListUserControl id="m_List" runat="server" CssClass="PanelControl Bordered Separated" ShowReportFields="True" SelectionLabel="select" SelectionCmd="picksf" HideSpecialFields="true">
        </UCR:SearchListUserControl>
    </DIV>
    <DIV id="m_OSetB" class="Hidden">
        <UCR:OperatorSetUserControl id="m_OSet" runat="server" CssClass="PanelControl Bordered Separated" SelectionLabel="select" SelectionCmd="pickso">
            <Item Label="Must match" Symbol="=" Code="Eq">
            </Item>
            <Item Label="Must not match" Symbol="!=" Code="Ne">
            </Item>
            <Item Label="Begins with" Symbol="=*" Code="Bw">
            </Item>
            <Item Label="Does not begin with" Symbol="!=*" Code="Nw">
            </Item>
            <Item Label="Is greater than or equal to" Symbol=">=" Code="Ge">
            </Item>
            <Item Label="Is greater than" Symbol=">" Code="Gt">
            </Item>
            <Item Label="Is less than or equal to" Symbol="<=" Code="Le">
            </Item>
            <Item Label="Is less than" Symbol="<" Code="Lt">
            </Item>
        </UCR:OperatorSetUserControl>
    </DIV>
    <DIV id="m_ValuB" class="Hidden">
        <UCR:ChooseEntryUserControl id="m_Valu" runat="server" CssClass="PanelControl Bordered Separated" SelectionLabel="select" SelectionCmd="picksa">
        </UCR:ChooseEntryUserControl>
    </DIV>
    <DIV id="m_WallB" class="Active">
        <ASP:Panel id="m_Wall" runat="server" CssClass="PanelControl Bordered Separated">
        </ASP:Panel>
    </DIV>
    <script>
        // If the event handling table is available, we
        // need to link up our event processing functions.

        if( registerForReportEvent != null )
        {
            var base = document.getElementById("<%= AspxTools.ClientId(m_Base) %>" );
            var list = document.getElementById("<%= AspxTools.ClientId(m_List) %>" );
            var oset = document.getElementById("<%= AspxTools.ClientId(m_OSet) %>" );
            var valu = document.getElementById("<%= AspxTools.ClientId(m_Valu) %>" );

            registerForReportEvent( base , list , displayFieldEntry , "focusf" );
            registerForReportEvent( base , oset , displayMatchEntry , "focuso" );
            registerForReportEvent( base , valu , displayValueEntry , "focusa" );
            registerForReportEvent( base , base ,  hideEntryEditing , "closed" );
            registerForReportEvent( base , base ,  showEntryEditing , "opened" );
            registerForReportEvent( base , base , initializeEditing , "onload" );
        }
    </script>
</ASP:Panel>
