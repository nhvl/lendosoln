using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for ReportLabelUserControl.
	/// </summary>

	public partial  class ReportLabelUserControl : System.Web.UI.UserControl , INamingContainer
	{
        private LendersOffice.QueryProcessor.Query            m_View;

        public Query DataSource
        {
            // Access member.

            set
            {
                m_View = value;
            }
        }

        public String CssClass
        {
            // Access member.

            set
            {
                m_Base.CssClass = value;
            }
        }

        public String Height
        {
            // Access member.

            set
            {
                m_Base.Height = Unit.Parse( value );
            }
        }

        public String Entry
        {
            // Access member.

            set
            {
                m_Text.Width = Unit.Parse( value );
            }
        }

        public String Width
        {
            // Access member.

            set
            {
                m_Base.Width = Unit.Parse( value );
            }
        }

        /// <summary>
        /// Bind the data to the current ui status.
        /// </summary>

        public void BindData()
        {
            // Set the data object's state, but only after
            // the first page load.

            if( IsPostBack == true )
            {
                if( m_View != null )
                {
                    m_View.Label.Title = m_Store.Value;
                }
            }
        }

        /// <summary>
        /// Bind the specified comments.
        /// </summary>

        public void BindView()
        {
            // Use what's in the query for our ui.

            m_Text.Attributes.Add( "onchange" , "changeLabelText( " + m_Base.ClientID + " , " + m_Store.ClientID + " , this.value );" );

            if( m_View != null )
            {
                if( IsPostBack == false )
                {
                    m_Store.Value = m_View.Label.Title;
                }

                m_Text.Text = m_View.Label.Title;
            }
        }

        /// <summary>
        /// Bind the report comments to our text box.
        /// </summary>

        public void HoldView()
        {
            // Use what's in the query for our ui.

            if( m_View != null )
            {
                m_Text.Text = m_Store.Value = m_View.Label.Title;
            }
        }

	    /// <summary>
	    ///	Bind data and render lists.
	    /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Use current columns to list report.

            HoldView();
		}

	    /// <summary>
	    ///	Initialize this control.
	    /// </summary>

		protected void ControlLoad( object sender , System.EventArgs e )
		{
            // Update the data elements according
            // to the state of the event store.
            // When we bind the view, the changes
            // that we make here should already be
            // in effect.

            BindData();
		}

	    /// <summary>
	    ///	Initialize this control.
	    /// </summary>

		protected void ControlInit( object sender , System.EventArgs e )
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.ControlInit);
			this.Load += new System.EventHandler(this.ControlLoad);
			this.PreRender += new System.EventHandler(this.ControlPreRender);
		}
		#endregion

	}

    /// <summary>
    /// Event handling arguments.
    /// </summary>

    public class ReportLabelEventArgs : EventArgs
    {
        /// <summary>
        /// Event args
        /// 
        /// Action is defined by this control's ui.  The
        /// target is always a string.
        /// </summary>

        public string Action; // requested action
        public string Target; // column target

        /// <summary>
        /// Construct specific event args.
        /// </summary>
        /// <param name="sAction">
        /// Action requested.
        /// </param>
        /// <param name="sTarget">
        /// Target of action.
        /// </param>

        public ReportLabelEventArgs( string sAction , String sTarget )
        {
            // Initialize members.

            Action = sAction;
            Target = sTarget;
        }

        /// <summary>
        /// Construct default event args.
        /// </summary>

        public ReportLabelEventArgs()
        {
            // Initialize members.

            Action = "";
            Target = null;
        }

    }

    /// <summary>
    /// Delegate declaration for firing events.
    /// </summary>

    public delegate void ReportLabelEventHandler( object sender , ReportLabelEventArgs e );

}
