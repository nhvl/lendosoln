<%@ Register TagPrefix="UCR" TagName="SearchListUserControl" Src="../QueryProcessor/SearchListUserControl.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ChooseEntryUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.ChooseEntryUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<STYLE>.choicelabel {
	FONT: 11px arial; WIDTH: 100%
}
</STYLE>
<ASP:PANEL id="m_Base" runat="server">
<script>
    function enableValueItems( oContainer , oStore , sFieldId ) { try
    {
        // Validate controls.

        if( oContainer == null || sFieldId == null )
        {
            return;
        }

		// Find and disable the type radio button

        var oItem = oContainer.parentElement.parentElement;
		var oItemRadB = getElementByIdFromParent(oItem, "m_RadB");
		oItemRadB.disabled = true;

        // Find and enable the value radio button

        var oValue = oItem;

        while (oValue.parentElement != null)
        {
			oValue = oValue.parentElement;
		}
        var valuePanel = getElementByIdFromParent(oValue, "m_ValuePanel")
		var oValueRadB = getElementByIdFromParent(valuePanel, "m_RadB");
        oValueRadB.disabled = false;



        while( oContainer.options.length > 0 )
        {
            oContainer.options.remove( 0 );
        }

        if( sFieldId == "" )
        {
            return;
        }

        // Walk the maps and find the associated primary
        // field id.  If found, populate the dropdown with
        // the list of options.  Remember, we catch this
        // event any time the chooser panel is enabled,
        // so that could be with each parameter click of
        // the condition entry view.

        var i , j , maps = oStore.value.split( "*" );

        for( i = 0 ; i < maps.length ; ++i )
        {
            // Break out the field id and list.

            var part = maps[ i ].split( ":" );

            if( part.length != 2 || part[ 0 ] != sFieldId )
            {
                continue;
            }

            // Break out the mapping items.

            var item = part[ 1 ].split( ";" );

            for( j = 0 ; j < item.length ; ++j )
            {
                // Validate current item.

                var o = document.createElement( "option" );

                if( item[ j ] == "" )
                {
                    continue;
                }

                // Get the pieces and add.

                var spec = item[ j ].split( "^" );

                if( spec.length != 2 )
                {
                    continue;
                }

                oContainer.options.add( o );

                o.text  = spec[ 0 ];
                o.value = spec[ 1 ];
            }

            break;
        }

        if( oContainer.options.length > 0 )
        {
            // Turn on type radio selection.  Also: find better
            // access point to radio button.
			oItemRadB.disabled = false;


            // OPM 17250 Cord -- Don't want to disable this button anymore
            // Disable the value radio button
            // oValueRadB.disabled = true;

            var valuePanel = getElementByIdFromParent(oValue, "m_ValuePanel");
            getElementByIdFromParent(valuePanel, "m_OptionEntry_m_Valu_m_Valu").value = "";


            //Select the type radio button
            oItemRadB.click();

        }
        else
        {
			//Select the value radio button
			oValueRadB.click();
		}
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function enableValuePanel( oControl , oBase , sArg ) { try
    {
        // Validate controls and base.

        if( oControl == null || oBase == null )
        {
            return;
        }

        // Turn on the controls.

        var valuePanel = getElementByIdFromParent(oBase, "m_ValuePanel");
        getElementByIdFromParent(valuePanel, "m_SetB").disabled = false;

        oControl.style.backgroundColor = "white";
        oControl.disabled = false;
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function dsableValuePanel( oControl , oBase , sArg ) { try
    {
        // Validate controls and base.

        if( oControl == null || oBase == null )
        {
            return;
        }

        // Turn off the controls.

        var valuePanel = getElementByIdFromParent(oBase, "m_ValuePanel");
        getElementByIdFromParent(valuePanel, "m_SetB").disabled = true;

        oControl.style.backgroundColor = "gainsboro";
        oControl.disabled = true;
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function enableItemsPanel( oControl , oBase , sArg ) { try
    {
        // Validate controls and base.

        if( oControl == null || oBase == null )
        {
            return;
        }

        // Turn on the controls.

        var valuePanel = getElementByIdFromParent(oBase, "m_ItemsPanel");
        getElementByIdFromParent(valuePanel, "m_SetB").disabled = false;

        oControl.style.backgroundColor = "white";
        oControl.disabled = false;
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function dsableItemsPanel( oControl , oBase , sArg ) { try
    {
        // Validate controls and base.

        if( oControl == null || oBase == null )
        {
            return;
        }

        // Turn off controls.

        var valuePanel = getElementByIdFromParent(oBase, "m_ItemsPanel");
        getElementByIdFromParent(valuePanel, "m_SetB").disabled = true;

        oControl.style.backgroundColor = "gainsboro";
        oControl.disabled = true;
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function activateChoosePanel( oValu , oItem , oList , sPanelId ) { try
    {
        // Validate controls.

        if( oValu == null || oItem == null || oList == null || sPanelId == null )
        {
            return;
        }

        if( sPanelId == "" )
        {
            return;
        }

        // Turn off all the panels by sending out the
        // disable message, then enable the selected.

        if( sendReportEvent != null )
        {
            sendReportEvent( "dsable" , "" , oValu );
            sendReportEvent( "dsable" , "" , oItem );
            sendReportEvent( "dsable" , "" , oList );

            switch( sPanelId )
            {
                case "value": sendReportEvent( "enable" , "" , oValu );
                break;

                case "items": sendReportEvent( "enable" , "" , oItem );
                break;

                case "field": sendReportEvent( "enable" , "" , oList );
                break;
            }
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function specifyValue( oContainer , oValue , sCmd ) { try
    {
        // Validate controls.

        if( oContainer == null || oValue == null || sCmd == null )
        {
            return;
        }

        if( sCmd == "" )
        {
            return;
        }

        // Fire the event to notify all listeners.

        if( fireReportEvent != null )
        {
            fireReportEvent( sCmd , oValue.value + ":" + oValue.value + ":" + "value" , oContainer );
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function specifyItems( oContainer , oValue , sCmd ) { try
    {
        // Validate controls.

        if( oContainer == null || oValue == null || sCmd == null )
        {
            return;
        }

        if( sCmd == "" )
        {
            return;
        }

        // Fire the event to notify all listeners.

        var i , oval , otxt , ocmd;

        for( i = 0 ; i < oValue.options.length ; ++i )
        {
            var option = oValue.options[ i ];

            if( option.value == oValue.value )
            {
                // When firing this command, we place each
                // option in with its text description and
                // a query processor specific value key.
                // With constants, the key is the same as
                // the text.  With macros, we place the field
                // id of the macro in the key.  The ui will
                // pass this key around to other user controls
                // when you specify a value using the drop
                // down items list.  This macro key is not
                // for display, but for telling the condition
                // tree code to add the condition so that
                // it compares against a macro field and not
                // a simple constant value.

                if( option.text != option.value )
                {
                    ocmd = "field";
                }
                else
                {
                    ocmd = "value";
                }

                otxt = option.text;
                oval = option.value;

                break;
            }
        }

        if( fireReportEvent != null )
        {
            fireReportEvent( sCmd , oval + ":" + otxt + ":" + ocmd , oContainer );
        }
    }
    catch( e )
    {
        window.status = e.message;
    }}

    function initializeChoice( oContainer , oValu , sArg ) { try
    {
        // Validate controls.

        if( oContainer == null || oValu == null )
        {
            return;
        }

        // Activate the initial view of this choice
        // entry.  We walk up the object tree to fire
        // the value's radio button, thus activating
        // the value panel and disabling the others.

        var panel = oValu;

        while( panel != oContainer && panel != null )
        {
            panel = panel.parentElement;

            if( panel.id != null && panel.id != "" )
            {
                break;
            }
        }

        if( panel != null )
        {
            getElementByIdFromParent(panel, "m_RadB").click();
        }

        oValu.value = "";
    }
    catch( e )
    {
        window.status = e.message;
    }}
</script>
	<INPUT id="m_Maps" type="hidden" name="m_Maps" runat="server">
	<DIV id="m_ValuePanel" style="MARGIN: 4px 4px 4px 0px; WIDTH: 100%">
		<TABLE style="WIDTH: 100%" cellSpacing="0" cellPadding="0">
			<TR>
				<TD style="WIDTH: 64px" align="left"><SPAN class="ChoiceLabel"> <INPUT
      id=m_RadB
      onclick="activateChoosePanel( <%= AspxTools.ClientId(m_Valu) %> , <%= AspxTools.ClientId(m_Item) %> , <%= AspxTools.ClientId(m_List) %> , 'value' );"
      type=radio CHECKED name=choose> Value: </SPAN>
				</TD>
				<TD align="left">
					<ASP:TextBox id="m_Valu" style="PADDING-LEFT: 4px; FONT: 11px arial" runat="server"></ASP:TextBox></TD>
				<TD align="right">
					<INPUT id=m_SetB style="BORDER-TOP-WIDTH: 1px; PADDING-RIGHT: 1px; PADDING-LEFT: 1px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 1px; PADDING-BOTTOM: 1px; FONT: 11px arial; WIDTH: 54px; PADDING-TOP: 1px; BORDER-RIGHT-WIDTH: 1px" onclick="specifyValue( <%= AspxTools.ClientId(m_Base) %> , <%= AspxTools.ClientId(m_Valu) %> , <%= AspxTools.JsString(SelectionCmd) %> );" type=button value=Select>
				</TD>
			</TR>
		</TABLE>
	</DIV>
	<DIV id="m_ItemsPanel" style="MARGIN: 4px 4px 4px 0px; WIDTH: 100%">
		<TABLE style="WIDTH: 100%" cellSpacing="0" cellPadding="0">
			<TR>
				<TD style="WIDTH: 64px" align="left"><SPAN class="ChoiceLabel"> <INPUT
      id=m_RadB
      onclick="activateChoosePanel( <%= AspxTools.ClientId(m_Valu) %> , <%= AspxTools.ClientId(m_Item) %> , <%= AspxTools.ClientId(m_List) %> , 'items' );"
      type=radio name=choose> Type: </SPAN>
				</TD>
				<TD align="left">
					<select id="m_Item" style="PADDING-LEFT: 4px; FONT: 11px arial" runat="server"></select></TD>
				<TD align="right">
					<INPUT id=m_SetB style="BORDER-TOP-WIDTH: 1px; PADDING-RIGHT: 1px; PADDING-LEFT: 1px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 1px; PADDING-BOTTOM: 1px; FONT: 11px arial; WIDTH: 54px; PADDING-TOP: 1px; BORDER-RIGHT-WIDTH: 1px" onclick="specifyItems( <%= AspxTools.ClientId(m_Base) %> , <%= AspxTools.ClientId(m_Item) %> , <%= AspxTools.JsString(SelectionCmd) %> );" type=button value=Select>
				</TD>
			</TR>
		</TABLE>
	</DIV>
	<DIV class="ChoiceLabel" style="MARGIN: 0px">
		<INPUT id=m_RadB
onclick="activateChoosePanel( <%= AspxTools.ClientId(m_Valu) %> , <%= AspxTools.ClientId(m_Item) %> , <%= AspxTools.ClientId(m_List) %> , 'field' );"
type=radio name=choose> Field:
	</DIV>
	<DIV id="m_FieldPanel" style="WIDTH: 100%">
		<UCR:SearchListUserControl id="m_List" runat="server" HideSpecialFields="true"></UCR:SearchListUserControl></DIV>
	<script>
        // If the event handling table is available, we
        // need to link up our event processing functions.

        if( registerForReportEvent != null )
        {
            var base = document.getElementById("<%= AspxTools.ClientId(m_Base) %>" );
            var valu = document.getElementById("<%= AspxTools.ClientId(m_Valu) %>" );
            var item = document.getElementById("<%= AspxTools.ClientId(m_Item) %>" );
            var maps = document.getElementById("<%= AspxTools.ClientId(m_Maps) %>" );

            registerForReportEvent( base , valu , initializeChoice , "onload" );
            registerForReportEvent( item , maps , enableValueItems , "focusa" );
            registerForReportEvent( valu , base , enableValuePanel , "enable" );
            registerForReportEvent( valu , base , dsableValuePanel , "dsable" );
            registerForReportEvent( item , base , enableItemsPanel , "enable" );
            registerForReportEvent( item , base , dsableItemsPanel , "dsable" );
        }
	</script>
</ASP:PANEL>
