using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOffice.QueryProcessor
{
    /// <summary>
    /// Event handling arguments.
    /// </summary>

    public class FieldAggiesEventArgs : EventArgs
    {
        /// <summary>
        /// Event args
        /// 
        /// Action is defined by this control's ui.  The
        /// target is always a column.
        /// </summary>

        public string Action; // requested action
        public Column Target; // column target

        /// <summary>
        /// Construct specific event args.
        /// </summary>
        /// <param name="sAction">
        /// Action requested.
        /// </param>
        /// <param name="cTarget">
        /// Target of action.
        /// </param>

        public FieldAggiesEventArgs( string sAction , Column cTarget )
        {
            // Initialize members.

            Action = sAction;
            Target = cTarget;
        }

        /// <summary>
        /// Construct default event args.
        /// </summary>

        public FieldAggiesEventArgs()
        {
            // Initialize members.

            Action = "";
            Target = null;
        }

    }

    /// <summary>
    /// Delegate declaration for firing events.
    /// </summary>

    public delegate void FieldAggiesEventHandler( object sender , FieldAggiesEventArgs e );

	/// <summary>
	///	Summary description for FieldAggiesUserControl.
	/// </summary>

	public partial  class FieldAggiesUserControl : System.Web.UI.UserControl , IPostBackEventHandler , INamingContainer
	{
        private LendersOffice.QueryProcessor.Query                       m_View;

        public event FieldAggiesEventHandler Calc;
        public event FieldAggiesEventHandler Drop;

        public void RaisePostBackEvent( string eventArgument )
        {
            // Parse command and dispatch event.

            string[] parameters = eventArgument.Split( ':' );

            if( m_View == null || parameters.Length <= 1 )
            {
                return;
            }

            // Get the current column and toggle the function.

            Column current = m_View.Columns.Find( CurrentSelection );

            if( current == null )
            {
                return;
            }

            switch( parameters[ 0 ].ToLower() )
            {
                case "change":
                {
                    // Update the state of the current condition's
                    // function set.  We toggle.

                    E_FunctionType func = E_FunctionType.Invalid;

                    switch( parameters[ 1 ] )
                    {
                        case "Mn": func = E_FunctionType.Mn;
                        break;

                        case "Mx": func = E_FunctionType.Mx;
                        break;

                        case "Sm": func = E_FunctionType.Sm;
                        break;

                        case "Av": func = E_FunctionType.Av;
                        break;
                    }

                    if( Calc != null && !current.Functions.Has( func ) )
                    {
                        // Delegate to container.

                        Calc( this , new FieldAggiesEventArgs( parameters[ 1 ] , current ) );
                    }
                    else
                    if( Drop != null && current.Functions.Has( func ) )
                    {
                        // Delegate to container.

                        Drop( this , new FieldAggiesEventArgs( parameters[ 1 ] , current ) );
                    }
                    else
                    {
                        // Handle this ourself.

                        if( !current.Functions.Has( func ) )
                        {
                            current.Functions.Add( func );
                        }
                        else
                        {
                            current.Functions.Nix( func );
                        }
                    }
                }
                break;
            }
        }

        public string CurrentSelection
        {
            // Access member.

            set
            {
                m_CurrentSelection.Value = value;
            }
            get
            {
                return m_CurrentSelection.Value;
            }
        }

        public Query DataSource
        {
            // Access member.

            set
            {
                m_View = value;
            }
        }

        public String CssClass
        {
            // Access member.

            set
            {
                m_Base.CssClass = value;
            }
        }

        public String Height
        {
            // Access member.

            set
            {
                m_Base.Height = Unit.Parse( value );
            }
        }

        public String Width
        {
            // Access member.

            set
            {
                m_Base.Width = Unit.Parse( value );
            }
        }

        /// <summary>
        /// Bind the specified condition options to this entry.
        /// </summary>

        public void BindView()
        {
            // Get the current field id and initialize the checkboxes
            // with any functions we have assigned.

            if( m_View != null )
            {
                Column column = m_View.Columns.Find( CurrentSelection );

                if( column == null )
                {
                    m_Mn.Checked = false;
                    m_Mx.Checked = false;
                    m_Sm.Checked = false;
                    m_Av.Checked = false;

                    return;
                }

                if( !column.Functions.Has( E_FunctionType.Mn ) )
                {
                    m_Mn.Checked = false;
                }
                else
                {
                    m_Mn.Checked = true;
                }

                if( !column.Functions.Has( E_FunctionType.Mx ) )
                {
                    m_Mx.Checked = false;
                }
                else
                {
                    m_Mx.Checked = true;
                }

                if( !column.Functions.Has( E_FunctionType.Sm ) )
                {
                    m_Sm.Checked = false;
                }
                else
                {
                    m_Sm.Checked = true;
                }

                if( !column.Functions.Has( E_FunctionType.Av ) )
                {
                    m_Av.Checked = false;
                }
                else
                {
                    m_Av.Checked = true;
                }
            }
        }

        /// <summary>
        /// Prepare the bound data sets.
        /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Bind current entry options using given field id.

            BindView();
		}

        /// <summary>
        /// Initialize this control.
        /// </summary>

		protected void ControlInit( object sender , System.EventArgs e )
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.ControlInit);
			this.PreRender += new System.EventHandler(this.ControlPreRender);
		}
		#endregion

	}

}
