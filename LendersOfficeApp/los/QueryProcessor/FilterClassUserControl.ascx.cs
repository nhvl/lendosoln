using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for FilterClassUserControl.
	/// </summary>

    [ ControlBuilderAttribute( typeof( FilterClassItemControlBuilder ) )
    ]
    [ ParseChildrenAttribute( false )
    ]
	public partial  class FilterClassUserControl : System.Web.UI.UserControl , INamingContainer
	{
        private LendersOffice.QueryProcessor.Query                       m_View;
        private LendersOffice.QueryProcessor.FilterClassStringPairArray m_Items;
        private System.String                                        m_FilterId;
        private System.String                                     m_GroupLablel;

        public event QueryUpdateEventHandler Dirty;

        public Query DataSource
        {
            // Access member.

            set
            {
                m_View = value;
            }
        }

        public String FilterId
        {
            // Access member.

            set
            {
                m_FilterId = value;
            }
            get
            {
                return m_FilterId;
            }
        }

        public String GroupLabel
        {
            // Access member.

            set
            {
                m_GroupLablel = value;
            }
            get
            {
                return m_GroupLablel;
            }
        }

        public String CssClass
        {
            // Access member.

            set
            {
                m_Base.CssClass = value;
            }
        }

        public String Height
        {
            // Access member.

            set
            {
                m_Base.Height = Unit.Parse( value );
            }
        }

        public String Width
        {
            // Access member.

            set
            {
                m_Base.Width = Unit.Parse( value );
            }
        }

        protected void SetClassOption( System.Object oArgument , bool isChecked )
        {
            // Find the matching condition in our filters and update
            // the tree according to the checked status specified.

            FilterClassStringPair pair = oArgument as FilterClassStringPair;

            if( m_View == null || pair == null )
            {
                return;
            }

            // Find the corresponding condition in the filter and
            // toggle it.  If it is not found or is not matching,
            // then we remove the condition and add a must match
            // condition in its place.

            Condition matching = null;

            foreach( object item in m_View.Filters.Walk )
            {
                Condition condition = item as Condition;

                if( condition == null )
                {
                    continue;
                }

                if( condition.Id == m_FilterId && condition.Argument.Type == E_ArgumentType.Const && condition.Argument.Value == pair.Argument )
                {
                    matching = condition;

                    break;
                }
            }

            if( matching != null )
            {
                // Check if this is a non-op.  If so,
                // don't continue to mark the tree
                // as dirty.

                if( matching.Type == E_ConditionType.Ne && isChecked == false )
                {
                    return;
                }
                else
                if( matching.Type == E_ConditionType.Eq && isChecked == true )
                {
                    return;
                }
            }

            if( matching != null )
            {
                // Remove this instance completely.

                foreach( object item in m_View.Filters.Walk )
                {
                    Conditions conditions = item as Conditions;

                    if( conditions == null )
                    {
                        continue;
                    }

                    conditions.Nix( matching );
                }

                m_View.Filters.Nix( matching );
            }

            // Now, we add a must match condition if the class
            // option is to be checked, or we add a must not
            // match condition if the option is to be turned
            // off and unchecked.  Either way, we make sure
            // we have the supporting condition group.

            if( isChecked == false )
            {
                // Add a new must not match condition to the root
                // all match conditions group.  If the root is not
                // all must match, we drop in a new conditions.
				//
				// 3/25/2005 kb - We previously used the root.  Now,
				// we need to make our own and-group to house the
				// classes.

                Conditions conditions = null;

				foreach( Object item in m_View.Filters )
				{
					Conditions current = item as Conditions;

					if( current == null )
					{
						continue;
					}

					if( current.Type == E_ConditionsType.An )
					{
						conditions = current;

						break;
					}
				}

                if( conditions == null )
                {
                    // And-group not found, so add one.

                    m_View.Filters.Add( conditions = new Conditions( E_ConditionsType.An ) );
				}

                // Add the must match condition.

				Condition condition = new Condition();

				condition.Type = E_ConditionType.Ne;
				condition.Id   = m_FilterId;
				
				condition.Argument.Type  = E_ArgumentType.Const;
                condition.Argument.Value = pair.Argument;

				conditions.Add( condition );
            }
            else
            {
                // Add a new must match condition to the any match
                // conditions group just under the filter root.
				//
				// 3/25/2005 kb - We previously used the root.  Now,
				// we need to make our own and-group to house the
				// classes.  Match classes must reside in an or-child
				// of a toplevel and-group.

				Conditions conditions = null;
				Conditions anymatches = null;

				foreach( Object item in m_View.Filters )
				{
					Conditions current = item as Conditions;

					if( current == null )
					{
						continue;
					}

					if( current.Type == E_ConditionsType.An )
					{
						conditions = current;

						break;
					}
				}

                if( conditions == null )
                {
                    // And-group not found, so add one.

                    m_View.Filters.Add( conditions = new Conditions( E_ConditionsType.An ) );
                }

				foreach( Object item in conditions )
				{
					Conditions current = item as Conditions;

					if( current == null )
					{
						continue;
					}

					if( current.Type == E_ConditionsType.Or )
					{
						anymatches = current;

						break;
					}
				}

				if( anymatches == null )
				{
					// All-group not found, so add one.

					conditions.Add( anymatches = new Conditions( E_ConditionsType.Or ) );
				}
				
				// Add the must match condition.

				Condition condition = new Condition();

				condition.Type = E_ConditionType.Eq;
				condition.Id   = m_FilterId;
				
				condition.Argument.Type  = E_ArgumentType.Const;
				condition.Argument.Value = pair.Argument;

				anymatches.Add( condition );
			}

            // We're dirty.  We should check if anything
            // actually changed -- oh well.

            if( Dirty != null )
            {
                Dirty( this , "Check" );
            }
        }

        protected bool IsBoundClassChecked( System.Object oArgument )
        {
            // Search the filter tree and look for a condition
            // with argument and determine if it should be checked.

            FilterClassStringPair pair = oArgument as FilterClassStringPair;

            if( m_View == null || pair == null )
            {
                return false;
            }

            foreach( object item in m_View.Filters.Walk )
            {
                // Locate matching condition.

                Condition condition = item as Condition;

                if( condition == null )
                {
                    continue;
                }

                if( condition.Id == m_FilterId && condition.Argument.Type == E_ArgumentType.Const && condition.Argument.Value == pair.Argument )
                {
                    // Add the checked attribute to reflect the filter
                    // status of our filter field id goven the current
                    // type of condition.

                    if( condition.Type == E_ConditionType.Eq )
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Add parsed item elements to this control.
        /// </summary>

        protected override void AddParsedSubObject( object obj )
        {
            // Only add child items.

            FilterClassItem item = obj as FilterClassItem;

            if( item != null )
            {
                m_Items.Add
                 ( item.Label
                 , item.Argument
                );
            }
            else

            base.AddParsedSubObject( obj );
        }

        /// <summary>
        /// Bind the data to the current ui status.
        /// </summary>

        public void BindData()
        {
            // Consider each option and check it if
            // we have it as marked on in our ui store,
            // or uncheck it if it is not listed.

            if( IsPostBack == true )
            {
                foreach( FilterClassStringPair pair in m_Items )
                {
                    if( m_Store.Value.IndexOf( "-" + pair.Argument + "-" ) == -1 )
                    {
                        SetClassOption( pair , false );
                    }
                    else
                    {
                        SetClassOption( pair , true );
                    }
                }
            }
        }

        /// <summary>
        /// Bind associated data elements to our ui.
        /// </summary>

        public void BindView()
        {
            // Start by adding the templated items to our list.

            if( IsPostBack == false )
            {
                // Only do this at startup -- otherwise,
                // you'll blow away the state on the call
                // that occurs after loading.

                m_Store.Value = "";

                foreach( FilterClassStringPair pair in m_Items )
                {
                    if( IsBoundClassChecked( pair ) == true )
                    {
                        m_Store.Value += "-" + pair.Argument + "-";
                    }
                }
            }

            m_List.DataSource = m_Items;
            m_List.DataBind();
        }

        /// <summary>
        /// Bind associated data elements to our ui.
        /// </summary>

        public void HoldView()
        {
			// Save out the final state before rendering to
			// the client.

            m_Store.Value = "";

            foreach( FilterClassStringPair pair in m_Items )
            {
                if( IsBoundClassChecked( pair ) == true )
                {
                    m_Store.Value += "-" + pair.Argument + "-";
                }
            }

            m_List.DataSource = m_Items;
            m_List.DataBind();
        }

        /// <summary>
        /// Render bound data elements.
        /// </summary>

		protected void ControlPreRender( object sender , System.EventArgs e )
		{
            // Bind the data elements to this view.

            HoldView();
		}

        /// <summary>
        /// Initialize this control.
        /// </summary>

		protected void ControlLoad( object sender , System.EventArgs e )
		{
            // Update the data elements according
            // to the state of the event store.
            // When we bind the view, the changes
            // that we make here should already be
            // in effect.

            BindData();
		}

        /// <summary>
        /// Initialize this control.
        /// </summary>

		protected void ControlInit( object sender , System.EventArgs e )
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.ControlInit);
			this.Load += new System.EventHandler(this.ControlLoad);
			this.PreRender += new System.EventHandler(this.ControlPreRender);
		}
		#endregion

        /// <summary>
        /// Construct default.
        /// </summary>

        public FilterClassUserControl()
        {
            // Initialize members.

            m_Items = new FilterClassStringPairArray();
        }

	}

    /// <summary>
    /// Provide item listing template for predefining check
    /// box set.
    /// </summary>

    public class FilterClassItemControlBuilder : ControlBuilder
    {
        /// <summary>
        /// Override child control filter and provide implementation
        /// when items are built.
        /// </summary>

        public override Type GetChildControlType( string tagName , IDictionary attributes )
        {
            if( tagName == "Item" || tagName == "item" )
            {
                return typeof( FilterClassItem );
            }

            return null;
        }

    }

    /// <summary>
    /// Provide container of custom children controls.
    /// </summary>

    public class FilterClassItem : Control , INamingContainer
    {
        /// <summary>
        /// Data item
        /// 
        /// We reference the bound content for templating.
        /// </summary>

        private String    m_Label; // displayed description
        private String m_Argument; // condition argument

        public string Label
        {
            // Access member.

            set
            {
                m_Label = value;
            }
            get
            {
                return m_Label;
            }
        }

        public string Argument
        {
            // Access member.

            set
            {
                m_Argument = value;
            }
            get
            {
                return m_Argument;
            }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public FilterClassItem()
        {
            // Initialize members.

            m_Label    = "";
            m_Argument = "";
        }

    }

    /// <summary>
    /// Maintain drop down bindings.
    /// </summary>

    public class FilterClassStringPair
    {
        /// <summary>
        /// Simple association
        /// 
        /// We maintain a labeled key binding for each entry
        /// in the drop down list.
        /// </summary>

        private string m_Label    = ""; // entry label
        private string m_Argument = ""; // condition predicate

        public string Label
        {
            // Access member.

            set
            {
                m_Label = value;
            }
            get
            {
                return m_Label;
            }
        }

        public string Argument
        {
            // Access member.

            set
            {
                m_Argument = value;
            }
            get
            {
                return m_Argument;
            }
        }

        /// <summary>
        /// Construct specified pair.
        /// </summary>
        /// <param name="sLabel">
        /// Label to display.
        /// </param>
        /// <param name="sArgument">
        /// Predicate to track.
        /// </param>

        public FilterClassStringPair( string sLabel , string sArgument )
        {
            // Initialize members.

            Label    = sLabel;
            Argument = sArgument;
        }

        /// <summary>
        /// Construct specified pair.
        /// </summary>
        /// <param name="sLabel">
        /// Label to display.
        /// </param>

        public FilterClassStringPair( string sLabel )
        {
            // Initialize members.

            Label = sLabel;
        }

    }

    /// <summary>
    /// Maintain drop down bindings.
    /// </summary>

    public class FilterClassStringPairArray : IEnumerable
    {
        private ArrayList m_Set = new ArrayList(); // set of pairs

        public FilterClassStringPair this[ int iPair ]
        {
            // Access member.

            get
            {
                return m_Set[ iPair ] as FilterClassStringPair;
            }
        }

        public int Count
        {
            // Access member.

            get
            {
                return m_Set.Count;
            }
        }

        /// <summary>
        /// Append to this set.
        /// </summary>
        /// <param name="sLabel">
        /// Label to display.
        /// </param>
        /// <param name="sArgument">
        /// Predicate to track.
        /// </param>

        public void Add( string sLabel , string sArgument )
        {
            // Append to this set.

            m_Set.Add( new FilterClassStringPair( sLabel , sArgument ) );
        }

        /// <summary>
        /// Append to this set.
        /// </summary>
        /// <param name="sLabel">
        /// Label to display.
        /// </param>

        public void Add( string sLabel )
        {
            // Append to this set.

            m_Set.Add( new FilterClassStringPair( sLabel ) );
        }

        /// <summary>
        /// Nix all entries.
        /// </summary>

        public void Clear()
        {
            // Nix all entries.

            m_Set.Clear();
        }

        /// <summary>
        /// Expose iteration interface.
        /// </summary>
        /// <returns>
        /// Returns enumerator.
        /// </returns>

        public IEnumerator GetEnumerator()
        {
            // Access iterator.

            return m_Set.GetEnumerator();
        }

    }

}
