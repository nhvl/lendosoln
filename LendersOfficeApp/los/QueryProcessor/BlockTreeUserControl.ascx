<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BlockTreeUserControl.ascx.cs" Inherits="LendersOffice.QueryProcessor.BlockTreeUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script type="text/javascript">
    function treeNodeClick(event) {
        var target = $(retrieveEventTarget(event));
        if (!target.hasClass("node-element")) {
            target = target.closest(".node-element");
        }

        var m_Selected = target.closest("input[id$='m_Selected']");
        m_Selected.val(target.attr("key"));

        //if (target.children.length > 0) {
        //    var event = createEvent("ondrop", true, true);
        //    event.code = "pick";
        //    fireEvent("ondrop", event);
        //}
    }
</script>
<INPUT id="m_Selected" type="hidden" runat="server" name="m_Selected"/>
<ASP:Panel id="m_Holder" runat="server" style="WIDTH: 100%; BACKGROUND-COLOR: transparent;" EnableViewState="False">
</ASP:Panel>
