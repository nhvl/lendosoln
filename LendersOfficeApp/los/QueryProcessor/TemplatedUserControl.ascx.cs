using System;
using System.Reflection;
using System.IO;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOffice.QueryProcessor
{
	/// <summary>
	///	Summary description for TemplatedUserControl.
	/// </summary>

    [ ParseChildren( true )
    ]
	public partial  class TemplatedUserControl : System.Web.UI.UserControl
	{
        private ITemplate m_Template;

        [ PersistenceMode( PersistenceMode.InnerProperty )
        ]
        [ TemplateContainer( typeof( Container ) )
        ]
        public ITemplate Template
        {
            // Access member.

            set
            {
                m_Template = value;
            }
            get
            {
                return m_Template;
            }
        }

        /// <summary>
        /// Any container that wants to bind to a template
        /// declared within our user control must inherit
        /// from this base interface.
        /// </summary>

        public class Container : Control , INamingContainer
        {
            public String this[ String sProp ]
            {
                // Access dynamic member.

                get
                {
                    // Access delegate of public get property
                    // and invoke it with this instance.

                    PropertyInfo prop = GetType().GetProperty( sProp );

                    if( prop != null )
                    {
                        MethodInfo meth = prop.GetGetMethod();

                        if( meth != null )
                        {
                            Object valu = meth.Invoke( this , null );

                            if( valu != null )
                            {
                                return valu.ToString();
                            }
                        }
                    }

                    return "";
                }
            }

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion

	}

}
