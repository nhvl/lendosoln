using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.los.CustomForms
{
	public partial class CustomForm : LendersOffice.Common.BasePage
	{
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            Guid customLetterID = RequestHelper.GetGuid("customletterid");
            SqlParameter[] parameters = new SqlParameter[] {
                                                               new SqlParameter("@CustomLetterID", customLetterID),
                                                               new SqlParameter("@BrokerID", BrokerUserPrincipal.CurrentPrincipal.BrokerId)
                                                           };

            Guid fileDBKey = Guid.Empty;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "RetrieveCustomForm", parameters)) 
            {
                if (reader.Read()) 
                {
                    fileDBKey = (Guid) reader["FileDBKey"];
                }
            }
            if (Guid.Empty != fileDBKey) 
            {
                this.SetEnabledPerformancePageSizeWarning(false); // 4/3/2008 dd - Custom form can be large.
                Response.Clear();
                Response.ContentType = "application/doc";
                if (RequestHelper.GetSafeQueryString("cmd") == "inline")
                    Response.AddHeader("Content-Disposition", "inline; filename=\"customform.doc\"");
                else
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"customform.doc\"");

                try
                {
                    Action<Stream> readHandler = delegate(Stream fs)
                    {
                        byte[] buffer = new byte[10000];
                        int count = fs.Read(buffer, 0, buffer.Length);
                        while (count > 0)
                        {
                            Response.OutputStream.Write(buffer, 0, count);
                            count = fs.Read(buffer, 0, buffer.Length);
                        }
                    };

                    FileDBTools.ReadData(E_FileDB.Normal, fileDBKey.ToString(), readHandler);
                }
                catch (FileNotFoundException)
                {
                    // 2/5/2007 nw - OPM 9824 - Unsaved Custom Forms will lead to error message when viewed.  
                    // We can't save an empty Word Doc when user creates a blank entry, so instead we will 
                    // load an empty Word Doc if user tries to preview an empty Custom Form.
                    string fileName = Tools.GetServerMapPath("./EmptyWordDoc.doc");
                    Response.WriteFile(fileName);
                }

                Response.Flush();
                Response.End();
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
