<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="CustomFormEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.CustomForms.CustomFormEdit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>CustomFormEdit</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
<body bgColor=gainsboro MS_POSITIONING="FlowLayout" scroll="no">
<script language=javascript>
  <!--
  function _init() {
    resize(450, 120);
    if(document.getElementById("m_title"))
		document.getElementById("m_title").focus();
  }
  //-->
  </script>
<h4 class="page-header">Set Custom Form Title</h4>
<form id=CustomFormEdit method=post runat="server" enctype="multipart/form-data">
<asp:Panel id=m_AccessDeniedPanel style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
		<b>Access Denied.  You do not have permission to create or edit custom forms.</b>
	</asp:Panel>
<asp:Panel id="m_AccessRestrictedPanel" style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
	<b>Access Denied.  You do not have permission to create or edit this custom form.</b>
</asp:Panel>
<asp:Panel id="m_AccessAllowedPanel" runat="server">
<table class=FormTable id=Table1 cellSpacing=0 cellPadding=0 width="100%"border=0>
  <tr>
    <td class=FieldLabel style="PADDING-LEFT: 5px" noWrap >Title</td>
    <td noWrap><asp:textbox id=m_title runat="server" Width="364px" MaxLength="36"></asp:textbox><IMG 
      src="../../images/require_icon.gif" 
      ><asp:RequiredFieldValidator id=RequiredFieldValidator1 runat="server" ControlToValidate="m_title">
          <img runat="server" src="../../images/error_icon.gif">
       </asp:RequiredFieldValidator></td></tr>
  <tr>
    <td noWrap></td>
    <td nowrap><asp:Button id=m_btnSave runat="server" Text="Save &amp; Close" onclick="m_btnSave_Click"></asp:Button><input type=button value="Cancel" onclick="onClosePopup();"></td></tr></table></asp:Panel><uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg></form>
	
  </body>
</html>
