using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
namespace LendersOfficeApp.los.CustomForms
{
	/// <summary>
	/// Summary description for CustomFormEdit.
	/// </summary>
	public partial class CustomFormEdit : LendersOffice.Common.BasePage
	{
		
    
		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private bool CanAccess()
		{
			if(CurrentUser.HasPermission(Permission.CanEditOthersCustomForms))
				return true;
			
			try
			{
				SqlParameter[] parameters = {
												new SqlParameter("@CustomLetterID", new Guid(Request["customletterid"])),
												new SqlParameter("@BrokerID", CurrentUser.BrokerId),
												new SqlParameter("@EmployeeID", CurrentUser.EmployeeId)
											};

				using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(CurrentUser.BrokerId, "GetCustomFormOwnerByEmployeeId", parameters)) 
				{
					return reader.Read();
				}
			}
			catch
			{
				return false;
			}
		}

        private void LoadData() 
        {
			Guid customLetterID = RequestHelper.GetGuid("customletterid", Guid.Empty);
            SqlParameter[] parameters = {
                                            new SqlParameter("@CustomLetterID", customLetterID),
                                            new SqlParameter("@BrokerID", CurrentUser.BrokerId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(CurrentUser.BrokerId, "RetrieveCustomForm", parameters)) 
            {
                if (reader.Read()) 
                {
                    m_title.Text = (string) reader["Title"];
                }
            }

        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Guid customLetterID = RequestHelper.GetGuid("customletterid", Guid.Empty);

			//Test for access permission
			if(CurrentUser.HasPermission(Permission.CanCreateCustomForms))
			{
				if((customLetterID == Guid.Empty) || CanAccess())
				{
					m_AccessAllowedPanel.Visible = true;
					m_AccessDeniedPanel.Visible = false;
					m_AccessRestrictedPanel.Visible = false;
				}
				else
				{
					m_AccessAllowedPanel.Visible = false;
					m_AccessDeniedPanel.Visible = false;
					m_AccessRestrictedPanel.Visible = true;
					return;
				}
			}
			else
			{
				m_AccessAllowedPanel.Visible = false;
				m_AccessRestrictedPanel.Visible = false;
				m_AccessDeniedPanel.Visible = true;
				return;
			}
			
            if (!Page.IsPostBack && (Guid.Empty != customLetterID))
                LoadData();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

        protected void m_btnSave_Click(object sender, System.EventArgs e)
        {
            try 
            {
                Guid customLetterID = RequestHelper.GetGuid("customletterid", Guid.Empty);
                Guid fileDBKey = Guid.Empty;
                string procedureName = "UpdateCustomForm";
                List<SqlParameter> parameters = new List<SqlParameter>();
				parameters.Add(new SqlParameter("@BrokerID", CurrentUser.BrokerId));
				parameters.Add(new SqlParameter("@CustomLetterID", customLetterID));
				parameters.Add(new SqlParameter("@Title", m_title.Text));
				parameters.Add(new SqlParameter("@FileDBKey", fileDBKey == Guid.Empty ? (object) DBNull.Value : (object) fileDBKey));

                StoredProcedureHelper.ExecuteNonQuery(CurrentUser.BrokerId, procedureName, 0, parameters);
            } 
            catch (Exception exc) 
            {
                Tools.LogErrorWithCriticalTracking(exc);
            }
            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this);
        
        }
	}
}
