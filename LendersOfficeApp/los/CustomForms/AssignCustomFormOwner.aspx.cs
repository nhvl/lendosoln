using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using System.Data.SqlClient;

namespace LendersOfficeApp.los.CustomForms
{
	/// <summary>
	/// Summary description for AssignCustomFormOwner.
	/// </summary>
	public partial class AssignCustomFormOwner : BasePage
	{

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		protected bool CanEditOthers
		{
			get { return BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanEditOthersCustomForms); }
		}

		protected string EmployeeId
		{
			get { return BrokerUserPrincipal.CurrentPrincipal.EmployeeId.ToString(); }			
		}

		private class EmployeeDesc
		{
			private String   m_EmployeeName = String.Empty;
			private Guid       m_EmployeeId = Guid.Empty;

			public String EmployeeName
			{
				// Access member.

				set
				{
					m_EmployeeName = value;
				}
				get
				{
					return m_EmployeeName;
				}
			}

			public Guid EmployeeId
			{
				// Access member.

				set
				{
					m_EmployeeId = value;
				}
				get
				{
					return m_EmployeeId;
				}
			}
		}
		
		protected void PageLoad(object sender, System.EventArgs e)
		{
			//Test for access permission
			if(CurrentUser.HasPermission(Permission.CanCreateCustomForms))
			{
				if(CanAccess())
				{
					m_AccessAllowedPanel.Visible = true;
					m_AccessDeniedPanel.Visible = false;
					m_AccessRestrictedPanel.Visible = false;
				}
				else
				{
					m_AccessAllowedPanel.Visible = false;
					m_AccessDeniedPanel.Visible = false;
					m_AccessRestrictedPanel.Visible = true;
					return;
				}
			}
			else
			{
				m_AccessAllowedPanel.Visible = false;
				m_AccessRestrictedPanel.Visible = false;
				m_AccessDeniedPanel.Visible = true;
				return;
			}

			if (!IsPostBack)
			{
				ViewState["customLetterId"] = Request["customletterid"];
				BindDataGrid();
			}
			else
				AssignCustomFormToUser();
		}

		private bool CanAccess()
		{
			if(CurrentUser.HasPermission(Permission.CanEditOthersCustomForms))
				return true;
			
			try
			{
				SqlParameter[] parameters = {
												new SqlParameter("@CustomLetterID", new Guid(Request["customletterid"])),
												new SqlParameter("@BrokerID", CurrentUser.BrokerId),
												new SqlParameter("@EmployeeID", CurrentUser.EmployeeId)
											};

				using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(CurrentUser.BrokerId, "GetCustomFormOwnerByEmployeeId", parameters)) 
				{
					return reader.Read();
				}
			}
			catch
			{
				return false;
			}
		}

		private void AssignCustomFormToUser()
		{
			Guid customLetterId = new Guid(ViewState["customLetterId"].ToString());
			SqlParameter[] parameters = new SqlParameter[] { 
												new SqlParameter("@BrokerID", CurrentUser.BrokerId),
												new SqlParameter("@CustomLetterID", customLetterId),
												new SqlParameter("@OwnerEmployeeId", new Guid(m_employeeId.Value)),
											};
			StoredProcedureHelper.ExecuteNonQuery(CurrentUser.BrokerId, "UpdateCustomForm", 0, parameters);
			string script = "<script language=javascript>onClosePopup();</script>";
			ClientScript.RegisterStartupScript(this.GetType(), "script", script);
		}

		private void BindDataGrid()
		{
			BrokerUserPermissionsSet pTable = new BrokerUserPermissionsSet();
			BrokerEmployeeNameTable  eTable = new BrokerEmployeeNameTable();
			ArrayList                showIt = new ArrayList();
			
			pTable.Retrieve( CurrentUser.BrokerId );
			eTable.Retrieve( CurrentUser.BrokerId );

			foreach( EmployeeDetails eD in eTable.Values )
			{
				if( eD.Type == "B" || eD.Type == "" )
				{
					if( eD.IsActive == true )
					{
						BrokerUserPermissions bUp = pTable[ eD.EmployeeId ];
						BrokerUserPermissions bUp2 = new BrokerUserPermissions(eD.BrokerId, eD.EmployeeId);
					
						if((bUp == null || bUp.IsInternalBrokerUser() == false) && bUp2.HasPermission(Permission.CanCreateCustomForms))
						{
							EmployeeDesc eE = new EmployeeDesc();
							eE.EmployeeId = eD.EmployeeId;
							eE.EmployeeName = eD.EmployeeName;
							showIt.Add( eE );
						}
					}
				}
			}
			m_dgUsers.DefaultSortExpression = "EmployeeName ASC";
			m_dgUsers.DataSource = ViewGenerator.Generate( showIt );
			m_dgUsers.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
