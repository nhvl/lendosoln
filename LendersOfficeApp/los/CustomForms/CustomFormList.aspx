<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<%@ Page Language="c#" CodeBehind="CustomFormList.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.los.CustomForms.CustomFormList" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Custom Form List</title>
    <style type="text/css">
        body {
            background-color: gainsboro;
        }

        table {
            width: 100%;
        }

        table.add-custom-fields-table {
            margin-bottom: 5px;
        }

        .header-div {
            line-height: 18px;
            margin-left: 1px;
            margin-bottom: 5px;
            padding-left: 1px;
        }

        #Categories {
            width: 175px;
        }

        #Fields {
            width: 250px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
    function _init() {
        resize(750, 540);
        initCategories();

        // Posting data back for deletions
        // and owner changes will cause the
        // header for IE > 11 and non-IE to
        // appear twice. Since this only appears
        // for IE < 11 and is removed otherwise,
        // show the DIV only for IE < 11.
        var version = getBrowserVersion();
        $('h4.page-header').toggle(version < 11);
    }

    function editCustomForm(id) {
        showModal('/los/customforms/CustomFormEdit.aspx?customletterid=' + id, null, null, null, function(){
            self.location = "CustomFormList.aspx";
        },{hideCloseButton:false});
    }

    function uploadForm(id) {
        var url = '/los/customforms/UploadForm.aspx';
        if (typeof id !== 'undefined') {
            url += '?customletterid=' + id;
        }

        showModal(url, null, null, null, function () {
            // Quirks mode 'quirk': window.location.reload will not 
            // reload the page. Instead, we need to assign the href
            // to itself.
            if (getBrowserVersion() < 11) {
                window.location.href = window.location.href;
            }
            else {
                window.location.reload(true);
            }
        });
        return false;
    }

    function deleteForm(id) {
        if (confirm('Do you want to delete this custom form?'))
            __doPostBack('', 'delete:' + id);
    }

    function assignNewOwner(id) {
        showModal('/los/customforms/AssignCustomFormOwner.aspx?customletterid=' + id, null, null, null, function(){
            self.location = "CustomFormList.aspx";
        },{hideCloseButton:false});
    }

    function removeOwner(id) {
        var msg = "Remove the currently assigned owner?";
        if(!ML.CanEditOthers) {
            msg += " (You will no longer be able to edit or delete this custom form)";
        }

        if (confirm(msg)) {
            __doPostBack('', 'removeOwner:' + id);
        }
    }

    function downloadForm(id) {
        var url = VRoot + '/los/CustomForms/DownloadCustomFormTemplate.aspx?customletterid=' + id;
        fetchFileAsync(url, 'application/octet-stream');
    }

    function initCategories($categories) {
        var $categories = $('#Categories');

        for (var category in FieldsByCategory) {
            var $option = $('<option/>').val(category).text(category);
            $categories.append($option);
        }

        $categories.change(onCategoryChange).change();
    }

    function onCategoryChange() {
        var selectedCategory = $('#Categories').val();
        var fieldList = FieldsByCategory[selectedCategory];

        $('#Fields option').remove();

        var fields = $('#Fields').get(0);
        $.each(fieldList, function (index, fieldName) {
            var option = document.createElement("option");
            option.text = option.value = fieldName;
            fields.add(option);
        });
    }

    function copyField() {
        var field = $('#Fields').val();
        copyStringToClipboard(field);
        alert('Field ' + field + " has been copied to your clipboard.");
    }
    </script>
    <h4 class="page-header">Custom Word Forms</h4>
    <form id="CustomFormList" method="post" runat="server">
        <div class="FormTableHeader header-div">
            Add Custom Form Fields
        </div>

        <div class="FieldLabel header-div">
            Select Custom Fields
        </div>

        <table class="FormTable add-custom-fields-table">
            <tr>
                <td>
                    Category
                </td>
                <td>
                    <select id="Categories"></select>
                </td>
                <td>
                    Field
                </td>
                <td>
                    <select id="Fields"></select>
                    <a onclick="copyField()">copy</a>
                </td>
            </tr>
        </table>

        <table class="FormTable add-custom-fields-table">
            <tr>
                <td>
                    <label class="FieldLabel">Directions</label>
                </td>
            </tr>
            <tr>
                <td>
                    (1) Select the custom form field to add using the dropdowns above, then click the "copy" link.
                </td>
            </tr>
            <tr>
                <td>
                    (2) Paste the custom form field in the desired location within your .docx Word document.
                </td>
            </tr>
            <tr>
                <td>
                    (3) After saving your Word document, click the "Add new custom form" button to add a brand new form, 
                    OR select the "replace with new doc" link to replace an existing custom word document.
                </td>
            </tr>
        </table>

        <table class="FormTable">
            <tr>
                <td nowrap class="FormTableHeader">Custom Form List</td>
            </tr>
            <tr>
                <td nowrap style="padding-top: 8px; padding-bottom: 8px">
                    <input type="button" value="Add new custom form" onclick="uploadForm();">
                </td>
            </tr>
            <tr>
                <td nowrap>
                    <ml:CommonDataGrid ID="m_dgShowOwner" runat="server">
                        <AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top" />
                        <ItemStyle CssClass="GridItem" VerticalAlign="Top" />
                        <HeaderStyle CssClass="GridHeader" />
                        <Columns>
                            <asp:TemplateColumn ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <a href="#" onclick=<%#AspxTools.HtmlAttribute("return editCustomForm(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "CustomLetterID").ToString()) +");")%>>rename</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Title" SortExpression="Title" HeaderText="Title"></asp:BoundColumn>
                            <asp:TemplateColumn ItemStyle-Width="50px">
                                <ItemTemplate>
                                    <a href="#" onclick=<%#AspxTools.HtmlAttribute("return downloadForm(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "CustomLetterID").ToString()) + ");")%>>download</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-Width="130px">
                                <ItemTemplate>
                                    <a href="#" onclick=<%#AspxTools.HtmlAttribute("return uploadForm(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "CustomLetterID").ToString()) + ");")%>>replace with new doc</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn ItemStyle-Width="220px" HeaderText="Owner" SortExpression="EmployeeName">
                                <ItemTemplate>
                                    <%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EmployeeName"))%>&nbsp;
            <a href="#" onclick=<%#AspxTools.HtmlAttribute("return assignNewOwner(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "CustomLetterID").ToString()) + ");")%>>assign</a>

                                    <a href="#" style=<%#AspxTools.HtmlAttribute(GetDisabledVal(DataBinder.Eval(Container.DataItem, "EmployeeName")))%> onclick=<%#AspxTools.HtmlAttribute("return removeOwner(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "CustomLetterID").ToString()) + ");")%>>remove</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn ItemStyle-Width="40px">
                                <ItemTemplate>
                                    <a href="#" onclick=<%#AspxTools.HtmlAttribute("return deleteForm(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "CustomLetterID").ToString()) + ");")%>>delete</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </ml:CommonDataGrid>
                </td>
            </tr>
            <tr>
                <td align="center" style="padding-top: 8px">
                    <input type="button" value="Close" onclick="onClosePopup();">
                </td>
            </tr>
        </table>
    </form>
    <uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>
</body>
</html>
