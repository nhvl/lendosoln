namespace LendersOfficeApp.los.CustomForms
{
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.CustomMergeFields;
using LendersOffice.Security;

    public partial class CustomFormList : BasePage
{
        /// <summary>
        /// Gets the required permissions for loading the page.
        /// </summary>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage => new[]
	{
            Permission.CanCreateCustomForms
        };

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        protected void Page_Init(object sender, EventArgs e)
		{
            this.EnableJqueryMigrate = false;
		}
    
		protected void Page_Load(object sender, EventArgs e)
		{
            if (this.Page.IsPostBack) 
			{
                this.ProcessPostbackCommand();
			}

            this.RegisterJsGlobalVariables("CanEditOthers", this.CurrentUser.HasPermission(Permission.CanEditOthersCustomForms));
            this.LoadCustomFormFields();
            this.BindDataGrid();
        }

		protected string GetDisabledVal(object EmployeeName) 
			{
			return (EmployeeName.ToString().Equals("None")) ? "display:none" : string.Empty;
			}
			
        private void ProcessPostbackCommand()
            {
				if (Request.Form["__EVENTARGUMENT"].StartsWith("removeOwner:")) 
				{
					Guid customLetterID = new Guid(Request.Form["__EVENTARGUMENT"].Substring(12));

					SqlParameter[] parameters = new SqlParameter[] { 
																	   new SqlParameter("@BrokerID", CurrentUser.BrokerId),
																	   new SqlParameter("@CustomLetterID", customLetterID),
																	   new SqlParameter("@OwnerEmployeeId", Guid.Empty),
					};
					StoredProcedureHelper.ExecuteNonQuery(CurrentUser.BrokerId, "UpdateCustomForm", 0, parameters);
				}
				// 6/2/2004 dd - Check to see if there is a delete command in __EVENTARGUMENT.
                else if (Request.Form["__EVENTARGUMENT"].StartsWith("delete:")) 
                {
                    try 
                    {
                        Guid fileDBKey = Guid.Empty;
                        Guid customLetterID = new Guid(Request.Form["__EVENTARGUMENT"].Substring(7));

                        SqlParameter[] parameters = {
                                                        new SqlParameter("@BrokerID", CurrentUser.BrokerId),
                                                        new SqlParameter("@CustomLetterID", customLetterID)
                                                    };

                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(CurrentUser.BrokerId, "RetrieveCustomForm", parameters)) 
                        {
                            if (reader.Read())
                                fileDBKey = (Guid) reader["FileDBKey"];
                        }
                        if (Guid.Empty != fileDBKey) 
                        {
                            FileDBTools.Delete(E_FileDB.Normal, fileDBKey.ToString());
                        }

                        // 6/2/2004 dd - I wish I could recycle the SqlParameter.
                        parameters = new SqlParameter[] {
                                                        new SqlParameter("@BrokerID", CurrentUser.BrokerId),
                                                        new SqlParameter("@CustomLetterID", customLetterID)
                                                    };
                        StoredProcedureHelper.ExecuteNonQuery(CurrentUser.BrokerId, "DeleteCustomForm", 3, parameters);
                    } 
                catch (Exception exc)
                {
                        Tools.LogErrorWithCriticalTracking(exc);
                    }
                }
            }

        /// <summary>
        /// Loads the fields for the page.
        /// </summary>
        private void LoadCustomFormFields()
        {
            var fieldsByCategory = new Dictionary<string, IEnumerable<string>>();

            foreach (CustomMergeCategory category in CustomMergeFieldRepository.AllCategories)
            {
                if (category.Special == "Hide" ||
                    (category.Special == "PmlOnly" && !this.CurrentUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan)))
                {
                    continue;
		}

                var fields = CustomMergeFieldRepository.GetFields(category.Name);
                if (fields == null)
		{
                    fieldsByCategory.Add(category.Name, Enumerable.Empty<string>());
                    continue;
		}

                fieldsByCategory.Add(category.Name, fields.Cast<CustomMergeField>().Select(field => field.FormattedWordCodeField));
            }

            this.RegisterJsObjectWithJsonNetSerializer("FieldsByCategory", fieldsByCategory);
        }

		private void BindDataGrid() 
		{
			DataSet ds = new DataSet();
			string procedureName;
            List<SqlParameter> parameters = new List<SqlParameter>();
			if(BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanEditOthersCustomForms))
			{
				parameters.Add(new SqlParameter("@BrokerID", CurrentUser.BrokerId));
				procedureName = "ListCustomFormByBrokerID";
			}
			else
			{
				parameters.Add(new SqlParameter("@BrokerID", CurrentUser.BrokerId));
				parameters.Add(new SqlParameter("@OwnerEmployeeId", CurrentUser.EmployeeId));
				procedureName = "ListCustomFormByOwnerEmployeeId";
			}
			
			DataSetHelper.Fill(ds, CurrentUser.BrokerId, procedureName, parameters);
			
			m_dgShowOwner.DataSource = ds.Tables[0].DefaultView;
			m_dgShowOwner.DataBind();
		}
		}
}