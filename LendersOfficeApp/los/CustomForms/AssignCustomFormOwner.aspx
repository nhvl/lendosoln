<%@ Page language="c#" Codebehind="AssignCustomFormOwner.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.CustomForms.AssignCustomFormOwner" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>AssignCustomFormOwner</title>
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
    <script language="javascript">
		function _init()
		{
			resize( 500 , 700 );
		}
		
		function assignUser(employeeId)
		{
			<%= AspxTools.JsGetElementById(m_employeeId) %>.value = employeeId;
			var msg = "Assign new owner to custom form?";
			
			if((!<%=AspxTools.JsBool(CanEditOthers)%>) && (employeeId != <%=AspxTools.JsString(EmployeeId)%>))
				msg += " (You will no longer be able to edit or delete this custom form)";
				
			if(confirm(msg))
				document.forms[0].submit();
		}
		</script>
  </head>
  <body onload="_init();" MS_POSITIONING="FlowLayout">
    <h4 class="page-header">Choose a user to assign the custom form to</h4>
    <form id="AssignCustomFormOwner" method="post" runat="server">
    <input type="hidden" value="" id=m_employeeId name=m_employeeId runat=server>
    <asp:Panel id=m_AccessDeniedPanel style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
		<b>Access Denied.  You do not have permission to assign custom forms.</b>
	</asp:Panel>
	<asp:Panel id="m_AccessRestrictedPanel" style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
		<b>Access Denied.  You do not have permission to assign this custom form.</b>
	</asp:Panel>
	<asp:Panel id="m_AccessAllowedPanel" runat="server">   
		<TABLE id="Table1" cellSpacing="2" cellPadding="3" width="100%" border="0">
			<tr>
				<td>
					<input type="button" onclick="onClosePopup();" value="Close">
				</td>
			</tr>
		<tr>
			<td>
		<ml:commondatagrid id="m_dgUsers" runat="server" AutoGenerateColumns="False" CssClass="DataGrid" EnableViewState="False" AllowSorting="True" Width="100%" DataKeyField="EmployeeId">
			<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
			<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
			<HeaderStyle CssClass="GridHeader"></HeaderStyle>
			<Columns>
				<asp:BoundColumn DataField="EmployeeName" HeaderText="User"></asp:BoundColumn>
				<asp:TemplateColumn HeaderText="Assign to user" itemstyle-width="90px">
					<ItemTemplate>
						<a href="#" onclick=<%#AspxTools.HtmlAttribute("assignUser(" + AspxTools.JsString(DataBinder.Eval(Container.DataItem, "EmployeeId").ToString()) + "); return false ;") %>>
							Assign</a>
					</ItemTemplate>
				</asp:TemplateColumn>
			</Columns>
		</ml:commondatagrid>
		</asp:Panel>
		</td>
		</tr>
		</TABLE>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg></form>
  </body>
</html>
