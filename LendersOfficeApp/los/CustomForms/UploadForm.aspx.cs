namespace LendersOfficeApp.los.CustomForms
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Summary description for UploadForm.
    /// </summary>
    public partial class UploadForm : BasePage
	{
        private Guid CustomFormId => RequestHelper.GetGuid("customletterid", Guid.Empty);

        private BrokerUserPrincipal CurrentUser => BrokerUserPrincipal.CurrentPrincipal;

        protected void PageLoad(object sender, System.EventArgs e)
		{
			if (this.CurrentUser.HasPermission(Permission.CanCreateCustomForms) && this.CanAccess())
			{
                m_AccessAllowedPanel.Visible = true;
                m_AccessDeniedPanel.Visible = false;
                m_AccessRestrictedPanel.Visible = false;
            }
			else
			{
				m_AccessAllowedPanel.Visible = false;
				m_AccessRestrictedPanel.Visible = false;
				m_AccessDeniedPanel.Visible = true;
				return;
			}

            this.RegisterJsGlobalVariables("IsNewForm", this.CustomFormId == Guid.Empty);

            if (this.Page.IsPostBack) 
            {
                this.SaveData();
            }
            else
            {
                this.LoadTitle();
                this.FileUploadValidator.ValidationExpression = LqbGrammar.DataTypes.RegularExpressionString.DocXFile.ToString();
                this.FileUploadValidator.ErrorMessage = ErrorMessages.CustomWordFormsMustBeDocX;
            }
		}

        private void LoadTitle()
        {
            if (this.CustomFormId == Guid.Empty)
            {
                return;
            }

            var parameters = new[]
            {
                new SqlParameter("@CustomLetterID", this.CustomFormId),
                new SqlParameter("@BrokerID", BrokerUserPrincipal.CurrentPrincipal.BrokerId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "RetrieveCustomForm", parameters))
            {
                if (reader.Read())
                {
                    this.CustomFormTitle.Text = reader["Title"].ToString();
                }
            }
        }

        private bool CanAccess()
        {
            if (this.CurrentUser.HasPermission(Permission.CanEditOthersCustomForms) || this.CustomFormId == Guid.Empty)
            {
                return true;
            }

            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@CustomLetterID", this.CustomFormId),
                                                new SqlParameter("@BrokerID", CurrentUser.BrokerId),
                                                new SqlParameter("@EmployeeID", CurrentUser.EmployeeId)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(CurrentUser.BrokerId, "GetCustomFormOwnerByEmployeeId", parameters))
                {
                    return reader.Read();
                }
            }
            catch
            {
                return false;
            }
        }

        private void SaveData()
        {
            if (!string.Equals(".docx", System.IO.Path.GetExtension(m_file.PostedFile.FileName), StringComparison.OrdinalIgnoreCase))
            {
                throw new CBaseException(ErrorMessages.CustomWordFormsMustBeDocX, ErrorMessages.CustomWordFormsMustBeDocX);
            }

            var fileDBKey = this.RetrieveFileDbKey() ?? Guid.NewGuid();
            var wordFile = m_file.PostedFile;

            FileDBTools.WriteData(E_FileDB.Normal, fileDBKey.ToString(), stream =>
            {
                wordFile.InputStream.CopyTo(stream);
            });

            this.SaveCustomForm(fileDBKey);
        }

        private Guid? RetrieveFileDbKey()
        {
            if (this.CustomFormId == Guid.Empty)
            {
                return null;
            }

            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@CustomLetterID", this.CustomFormId),
                new SqlParameter("@BrokerID", this.CurrentUser.BrokerId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "RetrieveCustomForm", parameters))
            {
                if (reader.Read())
                {
                    return (Guid)reader["FileDBKey"];
                }
            }

            return null;
        }

        private void SaveCustomForm(Guid fileDBKey)
        {
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerID", this.CurrentUser.BrokerId),
                new SqlParameter("@Title", this.CustomFormTitle.Text),
                new SqlParameter("@FileDBKey", fileDBKey)
            };

            var procedureName = "UpdateCustomForm";
            var customLetterID = this.CustomFormId;
            if (customLetterID == Guid.Empty)
            {
                procedureName = "CreateCustomForm";
                customLetterID = Guid.NewGuid();
                parameters.Add(new SqlParameter("@OwnerEmployeeId", this.CurrentUser.EmployeeId));
            }

            parameters.Add(new SqlParameter("@CustomLetterID", customLetterID));
            StoredProcedureHelper.ExecuteNonQuery(this.CurrentUser.BrokerId, procedureName, 0, parameters);
            this.ClientScript.RegisterStartupScript(typeof(UploadForm), nameof(SaveData), "onClosePopup();", addScriptTags: true);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
