<%@ Page Language="c#" CodeBehind="UploadForm.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.CustomForms.UploadForm" %>

<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Upload Form</title>
    <style type="text/css">
        body { background-color: gainsboro; }
    </style>
</head>
<body>
    <script type="text/javascript">
    function _init() {
        resize(500, 120);
        $('.title-row').toggle(ML.IsNewForm);
    }
    </script>
    <h4 class="page-header">Upload Word Document</h4>
    <form id="UploadForm" method="post" runat="server" enctype="multipart/form-data">
        <asp:Panel ID="m_AccessDeniedPanel" Style="padding-right: 30px; padding-left: 30px; padding-bottom: 30px; color: red; padding-top: 30px; text-align: center" runat="server">
            <b>Access Denied.  You do not have permission to edit custom forms.</b>
        </asp:Panel>

        <asp:Panel ID="m_AccessRestrictedPanel" Style="padding-right: 30px; padding-left: 30px; padding-bottom: 30px; color: red; padding-top: 30px; text-align: center" runat="server">
            <b>Access Denied.  You do not have permission to edit this custom form.</b>
        </asp:Panel>

        <asp:Panel ID="m_AccessAllowedPanel" runat="server">
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr class="title-row">
                    <td class="FieldLabel" style="padding-left: 5px" nowrap>Title</td>
                    <td nowrap>
                        <asp:TextBox ID="CustomFormTitle" runat="server" Width="364px" MaxLength="36"></asp:TextBox>
                        <img src="../../images/require_icon.gif" alt="Required">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="CustomFormTitle">
                            <img runat="server" alt="Error" src="../../images/error_icon.gif">
                        </asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td nowrap style="padding-left: 5px" class="FieldLabel">File</td>
                    <td nowrap>
                        <input id="m_file" style="width: 447px" type="file" size="55" name="m_file" runat="server" accept=".docx">
                        <br />
                        <asp:RequiredFieldValidator runat="server" ID="FileUploadRequiredValidator" ErrorMessage="Please select a file to upload." ControlToValidate="m_file"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server" ID="FileUploadValidator" Display="Static" ControlToValidate="m_file"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td nowrap colspan="2" align="center">
                        <asp:Button ID="m_btnUpload" runat="server" Text="Upload"></asp:Button>&nbsp;
                        <input type="button" value="Close" onclick="onClosePopup();">
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </form>
    <uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>

</body>
</html>
