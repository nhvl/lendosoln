﻿#region Generated Code
namespace LendersOfficeApp.los.CustomForms
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.FileSystem;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a means for download custom form templates.
    /// </summary>
    public partial class DownloadCustomFormTemplate : BasePage
    {
        /// <summary>
        /// Gets the required permissions for loading the page.
        /// </summary>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage => new[]
        {
            Permission.CanCreateCustomForms
        };

        /// <summary>
        /// Gets the current user adding fields.
        /// </summary>
        private BrokerUserPrincipal CurrentUser => BrokerUserPrincipal.CurrentPrincipal;

        /// <summary>
        /// Gets the ID of the current form.
        /// </summary>
        private Guid CustomFormId => RequestHelper.GetGuid("customletterid");

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.CurrentUserCanEditForm())
            {
                throw new AccessDenied();
            }

            this.DownloadFile();
        }

        /// <summary>
        /// Determines whether the current user can edit the
        /// current custom form.
        /// </summary>
        /// <returns>
        /// True if the current user can edit the form, false otherwise.
        /// </returns>
        private bool CurrentUserCanEditForm()
        {
            if (this.CurrentUser.HasPermission(Permission.CanEditOthersCustomForms))
            {
                return true;
            }

            var procedureName = StoredProcedureName.Create("GetCustomFormOwnerByEmployeeId").ForceValue();
            SqlParameter[] parameters =
            {
                new SqlParameter("@CustomLetterID", this.CustomFormId),
                new SqlParameter("@BrokerID", this.CurrentUser.BrokerId),
                new SqlParameter("@EmployeeID", this.CurrentUser.EmployeeId)
            };

            using (var connection = DbConnectionInfo.GetConnection(this.CurrentUser.BrokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                return reader.Read();
            }
        }

        /// <summary>
        /// Downloads the custom form.
        /// </summary>
        private void DownloadFile()
        {
            var fileDbKeyTitlePair = Tools.GetCustomFormInfo(this.CurrentUser.BrokerId, this.CustomFormId);
            var tempFilePath = FileDBTools.CreateCopy(E_FileDB.Normal, fileDbKeyTitlePair.Key.ToString());
            var escapedTitle = $"\"{Tools.EscapeInvalidFilenameCharacters(fileDbKeyTitlePair.Value)}.docx\"";

            RequestHelper.SendFileToClient(this.Context, tempFilePath, "application/octet-stream", escapedTitle);
            this.Context.Response.SuppressContent = true;
            this.Context.ApplicationInstance.CompleteRequest();

            FileOperationHelper.Delete(LocalFilePath.Create(tempFilePath).Value);
        }
    }
}