<%@ Page language="c#" Codebehind="LoanTemplateList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.LoanTemplateList" enableViewState="False"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>LoanTemplateList</title>
    <style>
    ul { margin-left:15px;}
    </style>
    </head>
    
<body class=bgcolor1 scroll=no MS_POSITIONING="FlowLayout">
<script type="text/javascript">
<!--
var isFirstClick = true;
function f_createBlankLoan()
{
    f_createLoan(<%= AspxTools.JsString(Guid.Empty) %>);
}
function f_createLoan(templateID) {
    var url = ML.VirtualRoot + "/newlos/LoanCreate.aspx?type=loan&templateid=" + encodeURIComponent(templateID) + "&loantype=" + <%= AspxTools.JsString(Purpose) %>;
    if (<%= AspxTools.JsBool(IsTest) %>)
    {
        url += "&test=1";
    }
    if (<%= AspxTools.JsBool(IsLead) %>)
    {
        url += "&islead=t";
    }

    location.href = url;
}

function validateDoubleClick() {
    if (isFirstClick) {
        isFirstClick = false;
        return true;
    } else {
        return false;
    }
}
//-->
    </script>
<form id=LoanTemplateList method=post runat="server">
<TABLE cellSpacing=8 cellPadding=0 width="100%" border=0>
<TR>
<TD class=PortletHeader>
	<%= AspxTools.HtmlString( IsLead ? "Lead" : "Loan" ) %> File Creation
</TD>
</TR>
<TR>
<TD>
  <% if (m_displayNoTemplateMessage) { %>
  <b>No loan templates available</b>. &nbsp;You can create a new loan template or ask an administrator to enable our blank loan option.
  <% } %>
	<ul>
		<asp:Panel id="m_blankPanel" runat="server">
			<li onclick="return validateDoubleClick();">
				<A class=PortletLink onclick='f_createBlankLoan();' href="#" >
					Create a Blank <%= AspxTools.HtmlString( IsLead ? "Lead" : "Loan" ) %> File
				</A>
			</li>
		</asp:Panel>
	</ul>
	
	<% if (!m_displayNoTemplateMessage) { %>
	
	<b>Create <%= AspxTools.HtmlString( IsLead ? "Lead" : "Loan" ) %> Using a Template:</b><br><br>
	<div style="Height:265px; OVERFLOW-Y:scroll; ">
	<ul>
		<asp:repeater id=m_templateRepeater runat="server">
			<itemtemplate>
            <li onclick="return validateDoubleClick();">
				<a href='#' class='PortletLink' onclick="f_createLoan(<%# AspxTools.JsString(Eval("sLId").ToString())%>);"><%# AspxTools.HtmlString(Eval("sLNm").ToString())%></a>
			</li>
			</ItemTemplate>
		</asp:repeater>
	</ul>
	</div>
</TD>
</TR>
<% } %>
<TR>
<TD align=middle>
	<INPUT onclick=onClosePopup(); type=button value=Cancel>
</TD>
</TR>
</TABLE>
</form>
</body>
</html>
