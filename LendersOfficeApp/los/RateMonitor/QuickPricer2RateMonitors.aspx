﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="QuickPricer2RateMonitors.aspx.cs" Inherits="LendersOfficeApp.los.RateMonitor.QuickPricer2RateMonitors" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Rate Monitors</title>
    <style type="text/css">
        div.Content
        {
            width: 100%;
        }
        table.rateMonitorSets thead tr th
        {
            background-color: #979797;
            border: solid 1px black;
            font-size: 13px;
        }
        table.rateMonitorSets
        {
            text-decoration: none;
            border-spacing: 0px;
            border-collapse: collapse;
            width: 100%;
            border-bottom: solid 1px black;
        }
        table.rateMonitorSets > tbody > tr > td > a
        {
            text-decoration: none;
        }
        table.rateMonitorSets > tbody > tr > td
        {
            border-left: solid 1px black;
            border-right: solid 1px black;
            padding-left: 2px;
            padding-right: 2px;
            font-size: 12px;
        }
        table.rateMonitorSets tbody tr.alternateRow
        {
            background-color:White;
        }
        input.Close
        {
            margin-left: 45%;
            margin-top:5px;
        }
        div.MainDiv
        {
            max-height:150px;
            overflow-y: auto;
            overflow-x: hidden;
            border: solid 1px black;
            margin-left: 5px;
            margin-right: 5px;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <form id="form1" runat="server">
    <div class="Content">
        <div class="FormTableHeader">Monitored Scenarios</div>	
        <div class="MainDiv">
            <table class="rateMonitorSets">
                <thead>
                    <tr>
                        <th></th>
                        <th>Scenario Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <input type="button" value="Close" class="Close" />
    </div>
    </form>
    <script id="rateMonitorSetRowTemplate" type="text/x-jquery-tmpl">
	<tr id="${LoanID}">
		<td>
			<a href="#" class="viewLink" data-url="${URL}">view</a>
		</td>
		<td>
		    ${Name}
		</td>
		<td>
			<a href="#" class="deleteLink">delete</a>
		</td>
	</tr>
    </script>
    <script type="text/javascript">
        $(function($) {
            $('.Close').on('click', function() {
                onClosePopup();
            });

            var rateMonitorScenarioIDGetter = function(rateMonitorSet) {
                return rateMonitorSet.LoanID;
            }

            var theTableUpdater = new tableUpdater(
                $('table.rateMonitorSets'),
                $('#rateMonitorSetRowTemplate'),
                rateMonitorSets,
                rateMonitorScenarioIDGetter);

            RateMonitorSet.prototype.config(window.location.href, function(result, args) { theTableUpdater.deleteRow({ LoanID: args.loanID }) }, null);
            var RateMonitorSetsByID = {};
            for (var monitorIndex in rateMonitorSets) {
                var monitor = rateMonitorSets[monitorIndex];
                RateMonitorSetsByID[monitor.LoanID] = new RateMonitorSet(monitor.LoanID, monitor.Name);
            }
            var w = screen.availWidth - 10;
            var h = screen.availHeight - 100;
            var options = 'width='+w +',height='+h+',left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes';

            $('table.rateMonitorSets').on('click', '.viewLink', function(event) {
                event.preventDefault();
                window.open($(this).data('url'), '_blank', options);
            });
            $('table.rateMonitorSets').on('click', '.deleteLink', function(event) {
                event.preventDefault();
                var loanId = $(this).closest('tr').attr('id');
                RateMonitorSetsByID[loanId].deleteSet();
            });
        });
    </script>
</body>
</html>
