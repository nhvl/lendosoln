﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Web.Services;
using DataAccess;
using LendersOffice.ObjLib.QuickPricer;

namespace LendersOfficeApp.los.RateMonitor
{
    public partial class QuickPricer2RateMonitors : BasePage
    {
        private AbstractUserPrincipal CurrentPrincipal
        {
            get
            {
                return (AbstractUserPrincipal)PrincipalFactory.CurrentPrincipal;
            }
        }

        protected void PageInit(object sender, EventArgs e)
        {
            if (false == CurrentPrincipal.IsActuallyUsePml2AsQuickPricer)
            {
                throw new AccessDenied("Can not access rate monitors without new quickpricer enabled.");
            }
            if (false == CurrentPrincipal.BrokerDB.AllowRateMonitorForQP2FilesInEmdeddedPml)
            {
                throw new AccessDenied("Your lender has not enabled rate monitors scenario tracking.");
            }

            EnableJqueryMigrate = false;
            RegisterJsScript("jquery.tmpl.js");
            RegisterJsScript("tableUpdater.js");
            RegisterJsScript("rateMonitorSet.js");
            
            if (false == Page.IsPostBack)
            {
                var scenarios = QuickPricer2LoanPoolManager.GetRateMonitoredLoansByUserID(
                    CurrentPrincipal.BrokerId,
                    CurrentPrincipal.UserId);
                bool isLoanTypeSupported = false;
                bool doBreakOut = false;
                var scenariosAndUrls = from s in scenarios
                                       orderby s.Name
                                       select new
                                       {
                                           LoanID = s.LoanID,
                                           Name = s.Name,
                                           URL = Tools.GetEmbeddedPmlUrl(
                                            (BrokerUserPrincipal)PrincipalFactory.CurrentPrincipal,
                                            s.LoanID,
                                            true,
                                            ref isLoanTypeSupported,
                                            ref doBreakOut)};
                                       
                RegisterJsObjectWithJavascriptSerializer(
                    "rateMonitorSets",
                    scenariosAndUrls);
            }
        }

        [WebMethod]
        public static void DeleteSet(Guid loanID)
        {
            try
            {
               QuickPricer2LoanPoolManager.DeleteMonitoredScenario((AbstractUserPrincipal)PrincipalFactory.CurrentPrincipal, loanID); 
            }
            catch (Exception e)
            {
                var msg = "DeleteSet failed for loan" + loanID;
                Tools.LogError(msg, e);
                throw;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }
        private void InitializeComponent()
        {
            //this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
    }
}
