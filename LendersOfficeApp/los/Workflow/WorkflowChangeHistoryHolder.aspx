﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkflowChangeHistoryHolder.aspx.cs" Inherits="LendersOfficeApp.los.Workflow.WorkflowChangeHistoryHolder" %>
<%@ Register TagPrefix="uc" TagName="WorkflowChangeHistory" Src="~/common/Workflow/WorkflowChangeHistory.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Workflow Change History</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc:WorkflowChangeHistory runat="server"></uc:WorkflowChangeHistory>
    </div>
    </form>
</body>
</html>
