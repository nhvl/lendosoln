﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditBranchGroup.aspx.cs" Inherits="LendersOfficeApp.los.Workflow.EditBranchGroup" %>
<%@ Register TagPrefix="uc" TagName="EditBranchGroupControl" Src="~/common/Workflow/EditBranchGroupControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Branch Group</title>    
</head>
<body>
    <script type="text/javascript">
        function _init()
        {
            if (typeof (_initControl) != "undefined")
            {
                _initControl();
            }
        }
      </script>
    <form id="form1" runat="server">    
        <uc:EditBranchGroupControl id="editBranchGroupControl" runat="server" />
    </form>
</body>
</html>