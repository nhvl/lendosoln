﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditWorkflowConfigCustomVariable.aspx.cs" Inherits="LendersOfficeApp.los.Workflow.EditWorkflowConfigCustomVariable" %>
<%@ Register TagPrefix="uc" TagName="EditWorkflowConfigCustomVariableControl" Src="~/common/Workflow/EditWorkflowConfigCustomVariableControl.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Workflow Config Item Editor</title>
</head>
<body>
    <script type="text/javascript">
        function _init()
        {
            if (typeof (_initControl) != "undefined")
            {
                _initControl();
            }
        }
    </script>
    <form id="form1" runat="server">
        <uc:EditWorkflowConfigCustomVariableControl id="editWorkflowConfigCustomVariableControl" runat="server" />
    </form>
</body>
</html>