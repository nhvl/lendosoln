﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditWorkflowConfigItem.aspx.cs" Inherits="LendersOfficeApp.los.Workflow.EditWorkflowConfigItem" %>
<%@ Register TagPrefix="uc" TagName="EditWorkflowConfigItemControl" Src="~/common/Workflow/EditWorkflowConfigItemControl.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Workflow Config Item Editor</title>
</head>
<body>
    <script type="text/javascript">
        function _init()
        {
            if (typeof (_initControl) != "undefined")
            {
                _initControl();
            }
        }
    </script>
    <form id="form1" runat="server">
	    <uc:EditWorkflowConfigItemControl id="editWorkflowConfigItemControl" runat="server" />
    </form>
</body>
</html>
