﻿<%@ Page Language="C#" AutoEventWireup="true"
    CodeBehind="EditWorkflowConfiguration.aspx.cs"
    Inherits="LendersOfficeApp.los.Workflow.EditWorkflowConfiguration"
    MaintainScrollPositionOnPostback="true"
%>
<%@ Register TagPrefix="uc" TagName="EditWorkflowConfigurationControl" Src="~/common/Workflow/EditWorkflowConfigurationControl.ascx" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Workflow Config Item Editor</title>
</head>
<body>
    <script type="text/javascript">
        function _init()
        {
            if (typeof (_initControl) != "undefined")
            {
                _initControl();
            }
        }
    </script>
    <form id="EditWorkflowConfiguration" runat="server">
	    <uc:EditWorkflowConfigurationControl id="editWorkflowConfigurationControl" runat="server" />
    </form>
</body>
</html>