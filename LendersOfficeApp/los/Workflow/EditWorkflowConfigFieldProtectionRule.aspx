﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditWorkflowConfigFieldProtectionRule.aspx.cs" Inherits="LendersOfficeApp.los.Workflow.EditWorkflowConfigFieldProtectionRule" %>
<%@ Register TagPrefix="uc" TagName="EditWorkflowConfigFieldProtectionRuleControl" Src="~/common/Workflow/EditWorkflowConfigFieldProtectionRuleControl.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>Workflow Config Item Editor</title>
</head>
<body>
    <script type="text/javascript">
        function _init()
        {
            if (typeof (_initControl) != "undefined")
            {
                _initControl();
            }
        }
    </script>
    <form id="form1" runat="server">
        <uc:EditWorkflowConfigFieldProtectionRuleControl id="editWorkflowConfigFieldProtectionRuleControl" runat="server" />
    </form>
</body>
</html>