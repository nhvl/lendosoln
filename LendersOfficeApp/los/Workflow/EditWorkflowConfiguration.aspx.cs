﻿namespace LendersOfficeApp.los.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Web.Services;
    using LendersOffice.Common;
    using LendersOffice.Common.Workflow;
    using LendersOfficeApp.common.Workflow;

    public partial class EditWorkflowConfiguration : WorkflowLosPage
    {
        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }
        
        [WebMethod]
        public static IEnumerable<object> GetParameterNamesFromRules(Guid brokerId, bool draftMode, string conditionIds, string constraintIds,
                                                                     string restraintIds, string triggerIds)
        {
            return EditWorkflowConfigurationControl.GetParameterNamesFromRules(brokerId, draftMode, conditionIds, constraintIds, restraintIds, triggerIds);
        }

        [WebMethod]
        public static object DeleteParametersValidate(Guid brokerId, bool draftMode, string conditionIds, string constraintIds,
                                                      string restraintIds, string triggerIds, string removedParameterNames)
        {
            return EditWorkflowConfigurationControl.DeleteParametersValidate(brokerId, draftMode, conditionIds, constraintIds,
                                                      restraintIds, triggerIds, removedParameterNames);
        }
    }
}