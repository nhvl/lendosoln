<%@ Page language="c#" Codebehind="EmployeeRole.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.EmployeeRole" %>
<%@ Register TagPrefix="ils" TagName="EmployeeRoleChooserLink" Src="../los/common/EmployeeRoleChooserLink.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>EmployeeRole</title>

	</HEAD>

	<body MS_POSITIONING="FlowLayout" scroll="no" bgcolor="gainsboro" onload="onInit();">
    <style type="text/css">
        form{
            margin-bottom: 0em;
        }
        table {
            border-spacing: 0em;
        }

        #m_Grid {
            table-layout: fixed;
        }
    </style>
	<script type="text/javascript">
		function onInit()
		{
		    var argsToWrite = document.getElementById("m_argsToWrite");
		    var errorMessage = document.getElementById("m_errorMessage");
		    var commandToDo = document.getElementById("m_commandToDo");

			if( errorMessage != null )
			{
				alert( errorMessage.value );
			}

			if( argsToWrite != null )
			{
				var i , arg = window.dialogArguments || {} , opts = argsToWrite.value.split( "|" );

				for( i in opts )
				{
					eval( "arg." + opts[ i ] + ";" );
				}
			}

			if( commandToDo != null )
			{
				if( commandToDo.value == "Close" )
				{
					onClosePopup(arg);
				}
			}

            <%= AspxTools.JsGetElementById(m_SearchFilter) %>.focus();

			<% if( IsPostBack == false ) { %>

			resize( 600 , 500 );

			<% } %>
		}

		function warn_assign_lo(loanId, loanofficerId, branchId, bCopyToOfficialAgent, loanOfficerName, loanOfficerEmail, role)
		{
		    var data = {
		        loanId: loanId,
		        loanofficerId: loanofficerId,
		        branchId: branchId,
		        bCopyToOfficialAgent:bCopyToOfficialAgent,
                loanOfficerName: loanOfficerName,
                loanOfficerEmail: loanOfficerEmail,
                roleTStr: role
            };

		    var jsonSerializedData = JSON.stringify(data);
            if (confirm("This will assign the loan to a different branch. Are you sure you want to continue?")) {
                //call the backend method to assign the loan officer
                callWebMethodAsync({
					type: "POST",
					contentType: "application/json; charset=utf-8",
					url: 'EmployeeRole.aspx/AssignLOAndBranch',
					data: jsonSerializedData,//'{loanId:\'' + loanId + '\', loanofficerId:\'' + loanofficerId + '\', branchId:\'' + branchId + '\', bCopyToOfficialAgent:\'' + bCopyToOfficialAgent+ '\', loanOfficerName:\''+ loanOfficerName +'\', loanOfficerEmail:\''+ loanOfficerEmail +'\'}',
					dataType: 'json',
					async : false,
					success: onSuccess,
					failure: onFail,
					error: function(XMLHttpRequest, textStatus, errorThrown) {alert(textStatus);}
              	});
            }

            return true;
        }
          function onSuccess(response)
          {
              if (response.d.indexOf('ERROR:') == 0)
              {
                alert(response.d);
                return;
              }
              var arg = window.dialogArguments  || {};
              arg.OK = true;
              arg.EmployeeName = response.d.substring(0,response.d.indexOf(","));
              arg.Email = response.d.substring(response.d.indexOf(",")+1,response.d.length);
              onClosePopup(arg);
          }
          function onFail(response)
          {
              alert("An error occurred. " + response);
              return false;
          }
	</script>
        <h4 class="page-header"><%= AspxTools.HtmlString(DialogTitle) %></h4>
		<form id="EmployeeRole" method="post" runat="server">
			<table cellspacing="2" cellpadding="0" width="100%">
				<tr>
					<td height="0" style="PADDING-RIGHT: 0px;PADDING-LEFT: 0px;PADDING-BOTTOM: 0px;PADDING-TOP: 0px">
						<div style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 8px; BORDER-TOP: 2px groove; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; BORDER-LEFT: 2px groove; PADDING-TOP: 8px; BORDER-BOTTOM: 2px groove">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td nowrap style="PADDING-RIGHT: 4px">
										Search for:
									</td>
									<td nowrap>
										<asp:TextBox id="m_SearchFilter" runat="server" style="PADDING-LEFT: 4px" Width="169px" />

									</td>
									<td style="FONT: 11px arial; COLOR: dimgray">
									    <%= AspxTools.HtmlString(HelpText) %>
									</td>
								</tr>
								<tr runat="server" id="StatusRow">
                                    <td style="padding-right: 4px" nowrap>
                                        <ml:EncodedLiteral ID="m_employeeStatusLabel" runat="server" />
                                    </td>
                                    <TD noWrap colSpan="2"><asp:RadioButton id="m_activeRB" runat="server" Text="Active" GroupName="status" Checked="True"></asp:RadioButton>
										<asp:RadioButton id="m_inactiveRB" runat="server" Text="Inactive" GroupName="status"></asp:RadioButton>
										<asp:RadioButton id="m_anyRB" runat="server" Text="Any status" GroupName="status"></asp:RadioButton></TD>
								</tr>
								<% if (Request["roledesc"].ToLower() == "supervisor") { %>
								<tr>
								    <td>
								        Originating Company:
								    </td>
								    <td colspan="2">
								    	<ils:employeerolechooserlink id="m_CompanyChoice" GenerateScripts="true" runat="server" Width="130px" Mode="Search" Desc="Originating Company" Role="Originating Company" Text="Any" Data="Any">
											<FIXEDCHOICE Text="Any" Data="Any">Any</FIXEDCHOICE>
											<FIXEDCHOICE Text="assign" Data="assign">Pick Company</FIXEDCHOICE>
										</ils:employeerolechooserlink>
								    </td>
								</tr>
								<% } %>
								<TR>
									<TD style="PADDING-RIGHT: 4px" noWrap></TD>
									<TD noWrap colSpan="2"><asp:Button id="m_Search" runat="server" Text="Search" /></TD>
								</TR>
								<%if( AspxTools.HtmlString(DisplayCopyToOfficialContact() && !isTeam).Equals(true.ToString())){ %>
								<TR>
									<TD style="PADDING-RIGHT: 4px" noWrap></TD>
									<TD noWrap colSpan="2">&nbsp;</TD>
								</TR>

								<TR>
									<TD style="PADDING-RIGHT: 4px" noWrap></TD>
									<TD noWrap colSpan="2"><asp:CheckBox id="m_copyToOfficialContact" runat="server" Text="Copy user info to Official Contact List and appropriate forms" /></TD>
								</TR>
								<% } %>
							</table>
						</div>
					</td>
				</tr>
				<tr valign="top">
					<td height="100%">
						<div style="PADDING-RIGHT: 4px; OVERFLOW: scroll; HEIGHT: 300px; position: relative">
						    <asp:PlaceHolder runat="server" ID="OriginatingCompanyLimitation" Visible="false">
						       <div style="color:Red" class="FieldLabel">Only <ml:EncodedLiteral runat="server" ID="OrigRoleDesc"/> from the same company as the <ml:EncodedLiteral runat="server" ID="OriginationCompanyLimitingRoleDesc"/> may be assigned.
						        </div>
						    </asp:PlaceHolder>
							<ml:CommonDataGrid id="m_Grid" runat="server" CellPadding="2" OnItemCommand="EmployeeAssignmentClick" OnItemDataBound="EmployeeAssignmentDataBound">
								<Columns>
									<asp:TemplateColumn SortExpression="FullName" HeaderText="Employee">
										<ItemTemplate>
											<asp:LinkButton runat="server" ID="Linkbutton1" NAME="Linkbutton1">
												<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "FullNameWithIsActiveStatus" ).ToString()) %>
											</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="User Type">
										<ItemTemplate>
											<%# AspxTools.HtmlString(GetPmlLabel(Container.DataItem )) %>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn SortExpression="BranchNm" HeaderText="Branch">
									    <ItemTemplate>
									        <%# AspxTools.HtmlString(GetBranchNm(Container.DataItem)) %>
									    </ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="CompanyName" SortExpression="CompanyName" HeaderText="External Broker"></asp:BoundColumn>
								</Columns>
							</ml:CommonDataGrid>
							<ml:CommonDataGrid id="m_SupervisorGrid" runat="server" CellPadding="2" OnItemCommand="EmployeeAssignmentClick" AllowSorting="true" OnItemDataBound="m_SupervisorGrid_ItemDataBound">
								<Columns>
									<asp:TemplateColumn HeaderText="Supervisor" SortExpression="UserFullName">
										<ItemTemplate>
										    <asp:LinkButton runat="server" CommandName="Assign" ID="SupervisorLink">
										    </asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateColumn>

                                    <asp:TemplateColumn HeaderText="Branch" SortExpression="BranchNm">
										<ItemTemplate>
										    <ml:EncodedLiteral runat="server" ID="BranchNm"/>
										</ItemTemplate>
									</asp:TemplateColumn>

                                    <asp:TemplateColumn HeaderText="Originating Company" SortExpression="CompanyName">
										<ItemTemplate>
										    <ml:EncodedLiteral runat="server" ID="CompanyName"/>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</ml:CommonDataGrid>
							<ml:CommonDataGrid id="m_CompanyGrid" runat="server" CellPadding="2" OnItemCommand="CompanyAssignmentClick" AllowSorting="true" OnItemDataBound="m_CompanyGrid_ItemDataBound">
								<Columns>
									<asp:TemplateColumn HeaderText="Originating Company" SortExpression="Name">
										<ItemTemplate>
										    <asp:LinkButton runat="server" CommandName="Assign" ID="CompanyLink">
										    </asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateColumn>

									<asp:TemplateColumn HeaderText="Full Address" SortExpression="Addr">
										<ItemTemplate>
									        <ml:EncodedLiteral runat="server" ID="Addr"/>
										</ItemTemplate>
									</asp:TemplateColumn>

                                    <asp:TemplateColumn HeaderText="Company ID" SortExpression="CompanyId">
										<ItemTemplate>
									        <ml:EncodedLiteral runat="server" ID="CompanyId"/>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</ml:CommonDataGrid>
							<asp:Panel id="m_TooMany" runat="server" style="PADDING-RIGHT: 50px; PADDING-LEFT: 50px; PADDING-BOTTOM: 50px; FONT: 11px arial; COLOR: tomato; PADDING-TOP: 50px; TEXT-ALIGN: justify" Visible="False">
					            Too many
					            <% if (Request["roledesc"].ToLower() == "loan officer") { %>
					                users
                                <% } else if (Request["roledesc"].ToLower() == "originating company") { %>
					                companies found.
					            <% } else { %>
					                employees
					            <% } %>
                                <% if (Request["roledesc"].ToLower() != "originating company") { %>
                                with this role.
					            <% } %>
                                Please refine your search using the above filters.
                                <DIV style="MARGIN-TOP: 12px;">
                                    <% if (Request["roledesc"].ToLower() != "originating company") { %>
                                        We match against
									    <% if (Request["roledesc"].ToLower() == "loan officer") { %>
									        user
									    <% } else { %>
									        employee
									    <% } %>
									    's full name, e.g. "Jo", will find all the
									    <% if (Request["roledesc"].ToLower() == "loan officer") { %>
									        users
									    <% } else { %>
									        employees
									    <% } %>
									    with a first or last name that begins with "Jo". To search for
									    <% if (Request["roledesc"].ToLower() == "loan officer") { %>
									        users
									    <% } else { %>
									        employees
									    <% } %>
									    by first *and* last name, use two patterns separated by a space. For example:
									    use "bo sm" to return "Bob Smith", "Boaz Smalls", etc.
					                <% } %>
								</DIV>
							</asp:Panel>
							<asp:Panel id="m_Empty" runat="server" style="PADDING-RIGHT: 80px; PADDING-LEFT: 80px; PADDING-BOTTOM: 80px; FONT: 11px arial; COLOR: dimgray; PADDING-TOP: 80px; TEXT-ALIGN: justify" Visible="False">
					            No
					            <% if (Request["roledesc"].ToLower() == "loan officer") { %>
                                    users
                                <% } else if( Request["roledesc"].ToLower() == "processor (external)" || Request["roledesc"].ToLower() == "secondary (external)" || Request["roledesc"].ToLower() == "post-closer (external)") { %>
						            PML users
					                <% if (PmlBrokerIdFilter != Guid.Empty) { %>
						                who belong to the same originating company and match the specified criteria were found.
                                    <% } %>
						        <% } else if (Request["roledesc"].ToLower() == "originating company") { %>
                                    companies
                                <% } else { %>
                                    employees
                                <% } %>
                                <% if ((Request["roledesc"].ToLower() != "processor (external)" && Request["roledesc"].ToLower() != "secondary (external)" && Request["roledesc"].ToLower() != "post-closer (external)") || PmlBrokerIdFilter == Guid.Empty) { %>
                                    matching the specified criteria.  Nothing to show.
                                <% } %>
				            </asp:Panel>
						</div>
					</td>
				</tr>
				<tr>
					<td height="0" align="center" style="PADDING-RIGHT: 8px;PADDING-LEFT: 8px;PADDING-BOTTOM: 8px;PADDING-TOP: 8px">
						<asp:Button id="m_ClearAssignment" runat="server" Text="Clear assignment" OnClick="ClearAssignmentClick" />
						<input type="button" value="Cancel" onclick="onClosePopup({OK: false});">
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
