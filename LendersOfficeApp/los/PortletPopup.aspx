﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PortletPopup.aspx.cs" Inherits="LendersOfficeApp.los.PortletPopup" %>

<%@ Register TagPrefix="uc1" TagName="PublishedReportsPortlet" Src="Portlets/PublishedReportsPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LoanStatisticsPortlet" Src="Portlets/LoanStatisticsPortlet.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body
        {
            background-color:#003366;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:loanstatisticsportlet Visible="false" id="LoanStatisticsPortlet" runat="server"></uc1:loanstatisticsportlet>
        <div id="PublishedreportsContainer" runat="server" visible="false">
            <uc1:PublishedReportsPortlet runat="server" id="PublishedReportsPortlet"></uc1:PublishedReportsPortlet>
        </div>
    </div>
    </form>
</body>
</html>
