﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;

namespace LendersOfficeApp.los
{

    public partial class PortletPopup : LendersOffice.Common.BaseServicePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            EnableJqueryMigrate = false;

            string portlet = RequestHelper.GetSafeQueryString("Portlet");

            switch(portlet)
            {
                case "BranchStat":
                    LoanStatisticsPortlet.Visible = true;
                    break;
                case "PublishedReports":
                    PublishedreportsContainer.Visible = true;
                    break;
                default:
                    throw new Exception("There is no portlet of that type.");
            }
        }
    }
}
