﻿namespace LendersOfficeApp.los
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a left-right picker for the user to select from a list of items.
    /// </summary>
    public partial class LeftRightPicker : System.Web.UI.UserControl
    {
        /// <summary>
        /// The available options for selection.
        /// </summary>
        public object AvailableSrc;

        /// <summary>
        /// The options that have been selected.
        /// </summary>
        public object AssignedSrc;

        public string TextField;
        public string ValueField;

        public string AvailableHeader { get; set; }
        public string AssignedHeader { get; set; }

        public bool EnableModalBehavior { get; set; } = true;

        public bool EnableSaveOnUnload { get; set; } = true;

        protected override void OnInit(EventArgs e)
        {
            var myPage = Page as BasePage;
            if (myPage != null)
            {
                myPage.RegisterJsScript("json.js");
                myPage.RegisterJsGlobalVariables(nameof(EnableSaveOnUnload), this.EnableSaveOnUnload);
            }

            base.OnInit(e);
        }

        public override void DataBind()
        {
            Bind();
            base.DataBind();

        }

        private void Bind()
        {
            m_btnSave.Visible = this.EnableModalBehavior;

            LeftColumnHeader.Text = AvailableHeader;
            RightColumnHeader.Text = AssignedHeader;

            Available.DataSource = AvailableSrc;
            Available.DataTextField = TextField;
            Available.DataValueField = ValueField;
            Available.DataBind();

            Assigned.DataSource = AssignedSrc;
            Assigned.DataTextField = TextField;
            Assigned.DataValueField = ValueField;
            Assigned.DataBind();
        }
    }
}
