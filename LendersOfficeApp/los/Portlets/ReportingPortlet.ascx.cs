using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOfficeApp.los.admin;

namespace LendersOfficeApp.los.Portlets
{
	public partial  class ReportingPortlet : System.Web.UI.UserControl
	{
		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		protected Boolean ShowPublishedReports
		{
            get { return BrokerUser.HasPermission( Permission.CanPublishCustomReports ); } 
		}

        public void SetVisibility(bool visible)
        {
            PermissionDependent1.Visible = visible;
            PermissionDependent2.Visible = visible;
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            // Implement control screening here.
			//
			// 1/6/2005 kb - We now check against a user's permissions.
			// Using roles to determine reporting access is being
			// deprecated.
            // EM - 220332 - Visibility of links is now determined instead of the Portlet itself.

			if( BrokerUser.HasPermission( Permission.CanRunCustomReports ) )
			{
                SetVisibility(true);
                
			}
			else
			{
                SetVisibility(false);
			}

            if (BrokerUser.LoginNm == "rpicker" || BrokerUser.LoginNm == "david_acc" || BrokerUser.LoginNm == "rjreynolds" || BrokerUser.LoginNm == "rmontiel@iservelending.com")
            {
                m_accountingReportPlaceHolder.Visible = true;
            }
            else
            {
                m_accountingReportPlaceHolder.Visible = false;
            }
            if (BrokerUser.BrokerDB.IsEnableProvidentFundingSubservicingExport)
            {
                m_subservicingExportPlaceHolder.Visible = true;
            }
            else
            {
                m_subservicingExportPlaceHolder.Visible = false;
            }

            m_securityEventLogPlaceHolder.Visible = BrokerUser.HasPermission(Permission.AllowAccessingSecurityEventLogsPage);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

	}

}
