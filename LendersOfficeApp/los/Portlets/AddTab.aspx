﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddTab.aspx.cs" Inherits="LendersOfficeApp.los.Portlets.AddTab" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc" TagName="ChooseReport" Src="ChooseReport.ascx" %>

<!DOCTYPE HTML>
<html>
<head>
	<title>Choose Pipeline Report</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	<style type="text/css">
		.highlightreport
		{ BACKGROUND-COLOR: gainsboro; }
	</style>
	<script type="text/javascript">
			function onInit()
			{
				if( document.getElementById("m_errorMessage") != null && document.getElementById("m_errorMessage").value != "" )
				{
					alert( document.getElementById("m_errorMessage").value );
				}

				if( document.getElementById("m_commandToDo") != null )
				{
					if( document.getElementById("m_commandToDo").value == "Close" )
					{
						var args = { OK: true };
						onClosePopup(args);
					}
				}

				resize( 550 , 500 );
			}

			var isRun = false;
			function DisablePage() {
				if ( isRun ) {
					return false;
				}

				isRun = true;
				return true;
			}

			function Validate() {
				var tabName = document.getElementById('m_tabName').value;
				var matchingChars = tabName.match(/[<>&]/);
				if (matchingChars != null && matchingChars.length > 0) {
					document.getElementById('m_invalidErr').style.display = '';
					document.getElementById('m_pipelineName').scrollIntoView();
					return false;
				}
				else {
					document.getElementById('m_invalidErr').style.display = 'none';
					return DisablePage();
				}
			}
	</script>
</head>
<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto;" onload="onInit();">
<h4 class="page-header">Tab Editor</h4>
<form id="AddTab" method="post" runat="server">
	<table cellpadding="4" cellspacing="0" border="0" width="100%" height="100%">
		<tr height="30%" valign="top">
			<td>
				<div style="MARGIN-TOP: 8px; PADDING: 8px; BACKGROUND-COLOR: whitesmoke; BORDER: 2px solid lightgrey;">
					<span class="FieldLabel" style="font-size: 105%;">
						Select a recently used pipeline view
					</span>
					<div style="MARGIN-TOP: 8px; PADDING: 8px; BACKGROUND-COLOR: whitesmoke; BORDER: 2px solid lightgrey;">
						<table cellpadding="2" cellspacing="0" border="0" width="100%">
							<tr>
								<td class="FieldLabel" colspan="3">
									Recently used pipeline views
									<hr color="LightGrey" size="2">
								</td>
							</tr>
							<asp:Repeater id="m_recentTabs" runat="server" OnItemDataBound="RecentTabs_OnItemDataBound" OnItemCommand="RecentTabClick">
								<ItemTemplate>
									<tr onmouseover="className = 'HighlightReport';" onmouseout="className = '';">
										<td style="padding-top: 6px;">
											<table cellpadding="0" cellspacing="0">
												<tr>
													<td>
														<b>Name:</b> <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TabName").ToString()) %>
													</td>
												</tr>
												<tr>
													<td>
														<b>Report:</b> <%# AspxTools.HtmlControl(GenerateName(DataBinder.Eval( Container.DataItem , "ReportTitle" ).ToString())) %>
														<ml:EncodedLiteral ID="m_visiblity" runat=server/>
													</td>
												</tr>
											</table>
										</td>
										<td align=right>
											<asp:LinkButton runat="server" OnClientClick="return DisablePage();" CommandName="Choose" ForeColor="Tomato" ID="recTabChooseLink">
											choose
											</asp:LinkButton>
										</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater>
							<tr>
								<td>
									<ml:EncodedLiteral ID="m_noTabsMsg" runat="server" Text="None" Visible="false"/>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</td>
		</tr>
		<tr height="70%" valign="top">
			<td>
				<div style="MARGIN-TOP: 8px; PADDING: 8px; BACKGROUND-COLOR: whitesmoke; BORDER: 2px solid lightgrey;">
					<span class="FieldLabel" style="font-size: 105%;">
							Or configure your own tab
					</span>
					<asp:Panel ID="m_pipelineName" runat="server">
						<div style="MARGIN-TOP: 8px; PADDING: 8px; BACKGROUND-COLOR: whitesmoke; BORDER: 1px solid lightgrey;">
							<table cellpadding="2" cellspacing="0" border="0" width="100%">
								<tr>
									<td class="FieldLabel">
										Name
										<hr color="LightGrey" size="2" />
									</td>
								</tr>
								<tr>
									<td style="padding-left:30px;">
										<asp:TextBox ID="m_tabName" GroupName="tabName" runat="server" MaxLength=50 Width=200px/>
									</td>
								</tr>
								<tr id="m_invalidErr" style="display:none;">
									<td style="font-size:105%; color:Red;">
										Invalid tab name. You may not use the following characters: < > &
									</td>
								</tr>
							</table>
						</div>
					</asp:Panel>
					<asp:Panel ID="m_pipelineVisibility" runat="server">
						<div style="MARGIN-TOP: 8px; PADDING: 8px; BACKGROUND-COLOR: whitesmoke; BORDER: 1px solid lightgrey;">
							<table cellpadding="2" cellspacing="0" border="0" width="100%">
								<tr>
									<td class="FieldLabel">
										Visibility
										<hr color="LightGrey" size="2" />
									</td>
								</tr>
								<tr>
									<td style="padding-left:30px;">
										<asp:RadioButton ID="m_showAssigned" GroupName="pipelineVisibility" Text="Show only loans you are assigned to" runat="server" /><br />
										<asp:RadioButton ID="m_showDefault" GroupName="pipelineVisibility" Text="Show all loans you have access to" runat="server" />
									</td>
								</tr>
							</table>
						</div>
					</asp:Panel>
					<uc:ChooseReport runat="server" ID="ChooseReport" OnReportChosen="ChooseReport_OnReportChosen" OnReportError="ChooseReport_OnReportError" />				</div>
			</td>
		</tr>
		<tr><td>
			<div style="PADDING: 4px; TEXT-ALIGN: center;">
				<input type="button" value="Cancel" style="WIDTH: 60px;" onclick="onClosePopup();">
			</div>
		</td></tr>
	</table>
	<uc:CModalDlg id="m_Modal" runat="server"/>
</form>
</body>
</html>
