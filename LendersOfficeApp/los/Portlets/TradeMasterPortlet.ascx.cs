﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using System.Web.UI.HtmlControls;

namespace LendersOfficeApp.los.Portlets
{
    public partial class TradeMasterPortlet : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var basepage = Page as BasePage;
            if (basepage == null)
            {
                throw new Exception("Cannot use the Trade Table on something that is not a BasePage");
            }

            basepage.IncludeStyleSheet("~/css/Tabs.css");
            if (!this.Visible) return;
            basepage.EnableJqueryMigrate = false;
            basepage.RegisterCSS("TradeMasterPortlet.css");
            basepage.RegisterJsScript("TradeMasterPortlet.js");
            basepage.RegisterJsScript("utilities.js");

            var subPage = RequestHelper.GetSafeQueryString("tabSubPage");
            int tab = string.IsNullOrEmpty(subPage) ? 0 : int.Parse(subPage);

            HtmlGenericControl selectedTab = null;
            switch (tab)
            {
                case 0:
                    SearchTrades.Visible = true;
                    selectedTab = SearchTab;
                    break;  
                case 1:
                    OpenTrades.Visible = true;
                    selectedTab = OpenTradesTab;
                    break;
                case 2:
                    SettlementReport.Visible = true;
                    selectedTab = SettlementReportTab;
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unsupported tab number");
            }

            selectedTab.Attributes.Add("class", "selected");
        }
    }
}
