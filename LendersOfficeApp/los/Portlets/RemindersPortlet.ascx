<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="RefreshButton" Src="RefreshButton.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="RemindersPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.RemindersPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Reminders" %>
<script>
    function onClickLoanLinkSet( oBase , oTarget ) { try
	{
       // Check for valid targets.

        if( oBase == null || oTarget == null )
        {
            return;
        }
			// Show link set and display loan in bold
            if( oTarget.nextSibling.style.display == "none" )
            {
                // Turn off all previously activated link sets within
                // this grid and then turn on the selected one.
                oTarget.nextSibling.style.display    = "block";
                oTarget.nextSibling.style.fontWeight = "normal";
                oTarget.style.fontWeight = "bold";
            }
            else
            {
                // Turn off the selected one and leave the
                // other link sets alone.

                oTarget.nextSibling.style.display = "none";
				oTarget.style.fontWeight = "normal";
           }
    }
    catch( e )
    {
        window.status = "Selecting " + oTarget.innerText + " failed.";
    }}
</script>

<TABLE width="100%" border="0" cellpadding="3" cellspacing="0">
<TR>
<TD align="left">
	<INPUT type="button" value="Add new task" onclick="addReminder();">
	<uc1:RefreshButton runat="server">
	</uc1:RefreshButton>
	<span style="color:red;font-weight:bold;padding-left:15px"><ml:EncodedLiteral ID="m_tooManyLabel" Runat="server" EnableViewState="False"></ml:EncodedLiteral></span>
</TD>
</TR>
</TABLE>
<ml:CommonDataGrid id="m_remindersDG" runat="server" DataKeyField="NotifId" Width="100%" AllowSorting="True" EnableViewState="False" CssClass="DataGrid" AutoGenerateColumns="False">
	<AlternatingItemStyle CssClass="GridAlternatingItem"/>
	<ItemStyle CssClass="GridItem"/>
	<HeaderStyle CssClass="GridHeader"/>
	<Columns>
		<asp:TemplateColumn>
			<ItemTemplate>
				<a href="#" onclick="editNotif(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "NotifId").ToString()) %>); return false;">edit</a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscStatus" HeaderText="Status">
			<ItemTemplate>
                <span class="<%# AspxTools.HtmlString(GetDiscStatusCss((E_DiscStatus)DataBinder.Eval(Container.DataItem, "DiscStatus"))) %>">
                    <%# AspxTools.HtmlString(GetDiscStatusDesc((E_DiscStatus)DataBinder.Eval(Container.DataItem, "DiscStatus"))) %>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscPriority" HeaderText="Priority">
			<ItemStyle Width="50px"></ItemStyle>
			<ItemTemplate>
                <span class="<%# AspxTools.HtmlString(GetDiscPriorityCss((E_DiscPriority)DataBinder.Eval(Container.DataItem, "DiscPriority"))) %>">
                    <%# AspxTools.HtmlString(GetDiscPriorityDesc((E_DiscPriority)DataBinder.Eval(Container.DataItem, "DiscPriority"))) %>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscSubject" HeaderText="Subject">
			<ItemTemplate>
                <span class="<%# AspxTools.HtmlString(GetDiscSubjectCss(Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "NotifIsRead")))) %>">
                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DiscSubject")) %>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscRefObjNm1" HeaderText="Loan Number">
			<ItemTemplate>
				<asp:Panel Runat="server" ID="LoanNumberLinks" style="CURSOR: pointer;">
					<div id="m_loanNumberTag" style="DISPLAY: block; COLOR: blue;" onmouseover="style.color = 'orange';" onmouseout="style.color = 'blue';" onclick="onClickLoanLinkSet(<%# AspxTools.JsString(m_remindersDG.ClientID) %>, this);">
						<u>
							<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DiscRefObjNm1")) %>
						</u>
					</div><div style="DISPLAY: none; PADDING: 0px; MARGIN-TOP: 2px; MARGIN-BOTTOM: 8px;">
						<asp:Panel id="EditLink" runat="server" style="PADDING-LEFT: 2px;">
							&#x2022;
							<asp:Panel id="Show" runat="server" style="DISPLAY: inline;">
								<a href="javascript:editLoan(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="Edit loan">
									edit
								</a>
							</asp:Panel>
							<asp:HyperLink id="Hide" runat="server" Enabled="False">
								edit
							</asp:HyperLink>
				        </asp:Panel>
						<asp:Panel id="LeadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<asp:Panel id="ShowLeadEdit" runat="server" style="DISPLAY: inline;">
								<a href="javascript:editLead(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="Edit lead">
									edit
								</a>
							</asp:Panel>
							<asp:HyperLink id="DisabledLeadEdit" runat="server" Enabled="False">
								edit
							</asp:HyperLink>
						</asp:Panel>
	   					<asp:Panel id="LoadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:editLoan(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="View loan in editor">
								view
							</a>
	                    </asp:Panel>
	   					<asp:Panel id="LoadLeadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:editLead(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="View lead in editor">
								view
							</a>
	                    </asp:Panel>
						<asp:Panel id="ViewLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:viewLoan(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="View loan summary">
								view
							</a>
						</asp:Panel>
						<asp:Panel id="ViewLeadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:viewLoan(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="View lead summary">
								view
							</a>
						</asp:Panel>
						<div nowrap style="PADDING-LEFT: 2px;">
							&#x2022;
							<a href="javascript:createTask(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="Insert new task for this loan">
								new task
							</a>
						</div>
					</div>
				</asp:Panel>
				<asp:Panel Runat="server" ID="LoanNumberPlainText" Visible="False">
					<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DiscRefObjNm1")) %>
				</asp:Panel>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:BoundColumn DataField="DiscRefObjNm2" SortExpression="DiscRefObjNm2" HeaderText="Borr. Name"></asp:BoundColumn>
		<asp:TemplateColumn SortExpression="DiscDueDate" HeaderText="Due Date">
			<ItemTemplate>
                <span class="<%# AspxTools.HtmlString((DataBinder.Eval(Container.DataItem, "DiscDueDate") is DateTime) ? GetDiscDueDateCss((DateTime)DataBinder.Eval(Container.DataItem, "DiscDueDate")) : string.Empty) %>">
                    <%# AspxTools.HtmlString((DataBinder.Eval(Container.DataItem, "DiscDueDate") is DateTime) ? ((DateTime)DataBinder.Eval(Container.DataItem, "DiscDueDate")).ToString() : string.Empty) %>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="NotifAlertDate" HeaderText="Warning Date">
			<ItemTemplate>
                <span class="<%# AspxTools.HtmlString((DataBinder.Eval(Container.DataItem, "NotifAlertDate") is DateTime) ? GetDiscNotifAlertDateCss((DateTime)DataBinder.Eval(Container.DataItem, "NotifAlertDate")) : string.Empty) %>">
                    <%# AspxTools.HtmlString((DataBinder.Eval(Container.DataItem, "NotifAlertDate") is DateTime) ? ((DateTime)DataBinder.Eval(Container.DataItem, "NotifAlertDate")).ToString() : string.Empty) %>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="UserLastNm" HeaderText="Created By">
			<ItemTemplate>
				<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "UserFirstNm")) %>
				<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "UserLastNm" )) %>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn>
			<ItemTemplate>
				<a href="#" onclick="deleteReminder(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "NotifId").ToString()) %>);">quit</a>
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</ml:CommonDataGrid>
<TABLE width="100%" border="0" cellpadding="3" cellspacing="0">
<TR>
<TD align="left">
	<INPUT type="button" value="Add new task" onclick="addReminder();">
</TD>
</TR>
</TABLE>
<SCRIPT>
	function addReminder()
	{
		showModal( "/los/reminders/TaskEditor.aspx" , null, null, null, function(args){
			if( args != null && args.OK != null && args.OK == true )
			{
			<%-- 01/18/06 OPM 3780 MF. Display refresh warning message instead of refreshing --%>
				refreshPipeline();
			}
		});
	}

	function editNotif( notifId )
	{
		showModal( "/los/reminders/TaskEditor.aspx?notifId=" + notifId , null, null, null, function(args){
			if( args != null && args.OK != null && args.OK == true )
			{
			<%-- 01/18/06 OPM 3780 MF Disable links and disable row if user modifies entry.
				Then display refresh warning message --%>

				var oClicked = document.activeElement;
				if ( oClicked && oClicked.parentNode
					&& oClicked.parentNode.parentNode )
					{
						var oRow = oClicked.parentNode.parentNode;
						var oRowLinks = oRow.getElementsByTagName("A");
						for (var i = 0; i < oRowLinks.length; i++) {
							oRowLinks(i).title = 'Click \"Refresh\" to see changes to this task.';
							oRowLinks(i).onclick = '';
							oRowLinks(i).href='#';
						}
						setDisabledAttr(oRow, true);
						oRow.title = "Click \"Refresh\" to see changes to this task.";
					}
				refreshPipeline();
			}
		});
	}

	function deleteReminder( notifId )
	{
		if (confirm('Are you sure you want to remove yourself from the task?'))
			__doPostBack('<%= AspxTools.ClientId(m_remindersDG) %>', notifId);
	}
</SCRIPT>
