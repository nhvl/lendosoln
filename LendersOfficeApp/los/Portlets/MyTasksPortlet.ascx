﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyTasksPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.MyTasksPortlet" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="RefreshButton" Src="RefreshButton.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>


<asp:HiddenField runat="server" ID="ShowCircumstances" />
<table width="100%" border="0" cellpadding="3" cellspacing="0">
<tr>
<td align="left" nowrap style="padding:0;"  >
	<uc:RefreshButton id="m_refreshPanel" runat="server">
	</uc:RefreshButton>
</td>
</tr>
</table>

<script type="text/javascript">
<!--
    var gSelectionCount = 0;
    var userOwnedTaskIds = {};
    function _init() {
        disableButtons(true);
        checkSearch();
        getUserOwnedTasks();
    }
    function getUserOwnedTasks() {
        var idsCsv = $j(<%=AspxTools.JsGetElementById(userOwnedTaskIds)%>).val();
        var ids = idsCsv.split(',');
        var len = ids.length;
        userOwnedTaskIds = {};
        for (var i = 0; i < len; i++) {
            if (ids[i] == "") continue;
            userOwnedTaskIds[ids[i]] = true;
        }
    }
    function checkIfUserOwnsTasks() {
        var selectedIds = getCheckList();
        var len = selectedIds.length;
        var userOwnsSelectedTasks = true;
        for (var i = 0; i < len; i++) {
            if (userOwnedTaskIds[selectedIds[i]] != true) {
                userOwnsSelectedTasks = false;
                break;
            }
        }
        if (!userOwnsSelectedTasks) {
            document.getElementById('btnTakeOwnerShip').disabled = false;
        }
        else {
            document.getElementById('btnTakeOwnerShip').disabled = true;
        }
    }
    function onCheckBoxClick(cBox) {

        if (cBox.checked) {
            gSelectionCount++;
        }
        else {
            gSelectionCount--;
        }

        highlightRowByCheckbox(cBox);

        disableButtons(gSelectionCount == 0);

        // Toggle the take ownership button
        if (gSelectionCount > 0) {
            checkIfUserOwnsTasks();
        }
    }
    function onTaskOpen(loanid, taskid) {
        var taskDialogOptions = 'width=750,height=400,center=yes,resizable=yes,scrollbars=yes,status=yes,help=no';
        var url = VRoot + "/newlos/Tasks/TaskViewer.aspx?loanid=" + encodeURIComponent(loanid) + "&taskId=" + encodeURIComponent(taskid);
        var handle = window.open('', 'LOtaskWindow__' + taskid, taskDialogOptions);
        if (handle.location.href === 'about:blank') {
            handle.location.href = url;
        }
        handle.focus(); // if the window already exists, focus on it
    }
    function getCheckList() {
        var list = new Array();

        var collection = getAllElementsByIdFromParent(document, "TaskId");

        var length = collection.length;
        for (var i = 0; i < length; i++) {
            if (collection[i].checked)
                list.push(collection[i].value);
        }
        if (collection.length == null) {
            if (collection.checked)
                list.push(collection.value);
        }
        return list;
    }

    // Determine if any of the checked tasks are hidden conditions
    function anyHiddenCondsChecked() { // We could get this in one pass by integrating with getChecklist
        var collection = getAllElementsByIdFromParent(document, "TaskId");

        var length = collection.length;
        for (var i = 0; i < length; i++) {
            if (collection[i].checked) {
                $item = $j(collection[i]);
                if($item.siblings('*[id$=TaskHidden]').val() === 'True')
                    return true;
            }
        }
        return false;
    }


    function disableButtons(bDisable) {
        var ids = ["btnAssignTasks", "btnSetDueDate", "btnSetFollowupDate", "btnTakeOwnerShip", "btnSubscribeToTasks", "btnUnsubscribeTasks"];

        for (var i = 0; i < ids.length; i++) {
            if (document.getElementById(ids[i]) != null)
                document.getElementById(ids[i]).disabled = bDisable;
        }

        chkBoxes = $j("#TaskId");
        document.getElementById("btnExportToCsv").disabled = (chkBoxes.length == 0);
    }
    function f_toggleLoanOptions(loanId, taskId) {

        var optionDiv = document.getElementById('LoanOptions_' + taskId);

        if (optionDiv.style.display === 'block') {
            optionDiv.style.display = 'none';
            return;
        }
        else {
            optionDiv.style.display = 'block';
        }

        var data = {'loanId': loanId};

        var str_data = JSON.stringify(data);

            callWebMethodAsync({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'PipelineService.aspx/GetLoanOptions',
            data: str_data,
            dataType: 'json',
            async: false,
            success: function(response) {optionDiv.innerHTML = response.d; },
            failure: function(response) { alert("error - " + response); },
            error: function(XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); }
        });
    }

    function PerformOperation(opName, data) {

        var ret = false;

        if(data == null)
        {
            data = { 'taskIds': getCheckList().join(',') };
        }

        var strParams = JSON.stringify(data);

        //alert(opName + '---' + strParams);

        callWebMethodAsync({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'PipelineService.aspx/' + opName,
            data: strParams,
            dataType: 'json',
            async: false,
            success: function(msg) {ret = true; if(msg.hasOwnProperty('d') && msg.d.length > 0) {alert(msg.d); ret = false;}},
            failure: function(response) { alert("error - " + response); ret = false;},
            error: function(XMLHttpRequest, textStatus, errorThrown) { alert(textStatus + errorThrown); ret = false;}
        });

        return ret;
    }

    function f_AssignTasks() {

        var anyHiddenConds = anyHiddenCondsChecked();
        var queryString = '?taskIds=' + getCheckList().join(',')
                    + '&IsBatch=' + true
                    + '&IsTemplate=' + false
                    + '&IsPipeline=' + true;
        if (anyHiddenConds === true) {
            queryString += '&nopml=' + true;
        }
        showModal("/newlos/Tasks/RoleAndUserPicker.aspx" + queryString, null, null, null, function(modalResult){
            if (modalResult.OK) {
                var data =
                { 'taskIds': getCheckList().join(','),
                'id' : modalResult.id,
                'type' : modalResult.type
                }

                SetPostbackParams('', '');

                if(PerformOperation('AssignTasks', data))
                {
                    refresh();
                }
            }
        });
    }

    function f_ExportToCsv() {

        SetPostbackParams('csv', '');

        __doPostBack('','');

        SetPostbackParams('', '');
    }

    function taskSearch() {
        var taskId = $j.trim(document.getElementById("txtSearchTask").value);
        if(taskId!=null && taskId.length > 0)
        {
            var url = "/newlos/Tasks/TaskViewer.aspx?taskId=" + taskId;
            showModal(url, '', 'center:yes;resizable:yes;scroll:no;status=no;', null, null, {hideCloseButton:true});
        }
    }

    function f_TakeOwnership() {
        if (confirm('Are you sure you want to take ownership of the selected tasks?')) {

            SetPostbackParams('', '');

            if(PerformOperation('TakeTaskOwnership') == true)
            {
                refresh()
            }
        }
    }

    function f_SetDueDate() {
        showModal('/newlos/Tasks/TaskListSetDueDateModal.aspx', null, null, null, function(modalResult){
            //alert(modalResult.offset)
            if (modalResult.OK && (modalResult.id != '' || modalResult.date!='')) {
                var data =
                { 'taskIds': getCheckList().join(','),
                dateString : modalResult.date,
                fieldId : modalResult.id,
                offset : (modalResult.offset == '' ? 0 : modalResult.offset)
                };

                SetPostbackParams('', '');
                if(PerformOperation('SetDueDate', data) == true)
                {
                    refresh();
                }
            }
        });
    }

    function f_SetFollowupDate() {

     showModal('/newlos/Tasks/TaskListSetFollowupDateModal.aspx', null, null, null, function(modalResult){
        if (modalResult.OK && modalResult.date!='') {
            var data =
                { 'taskIds': getCheckList().join(','),
                    'date': modalResult.date
                };

            SetPostbackParams('', '');

            if(PerformOperation('SetFollowupDate', data) == true)
            {
                    refresh();
            }
        }
     });
    }

    function f_SubscribeTasks() {
        if (confirm('Are you sure you want to subscribe to the selected tasks?')) {
            if(PerformOperation('SubscribeToTasks') == true)
            {
                alert("You have been subscribed to the selected tasks");
            }
        }
    }

    function f_UnsubscribeTasks() {
        if (confirm('Are you sure you want to un-subscribe from the selected tasks?')) {
            if(PerformOperation('UnsubscribeTasks') == true)
            {
                alert("You have been un-subscribed from the selected tasks");
            }
        }
    }

    function SetPostbackParams(cmd, params)
    {
        <%=AspxTools.JsGetElementById(inputCmd)%>.value = cmd;
        <%=AspxTools.JsGetElementById(inputParams)%>.value =params;
    }
    function checkSearch(evt){
        var $txtSearchTask = $j("#txtSearchTask");
        var l = $txtSearchTask.val().length;
        $j("#btnSearchTask").prop('disabled', l == 0);
        if( l > 0 && evt!= null && evt.keyCode === 13)
        {
            taskSearch();
        }
    }

    function refresh() {
         SetPostbackParams('', '');
          __doPostBack('','');
    }

    jQuery(function($){

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'PipelineService.aspx/GetResolvedOrClosedTaskCount',
            dataType: 'json',
            async: true,
            success: function(msg)
                {
                    $('#LoanListOptions div.TaskMeter.Scale').css({'background-image':'url(../images/scale.png)'});
                    $('#LoanListOptions div.TaskMeter.Gradient.Parent span').hide();
                    $('#LoanListOptions div.TaskMeter.Gradient.Child').css({'background-image': 'url(../images/measure_gradient.png)'});
                    setResolvedTasks(msg.d)
                },
            failure: function(response) { alert("error - " + response);},
            error: function(XMLHttpRequest, textStatus, errorThrown) { alert(textStatus + errorThrown);}
        });

        function setResolvedTasks(count)
        {
            var maxResolvedTasks = 40;
            var percent = count/maxResolvedTasks * 100;
            percent = Math.max(1, percent);
            percent = Math.min(100, percent);

            $('#LoanListOptions div.TaskMeter.Gradient.Child').css({width: percent + "%"});
        }


    });
-->
</script>

<style type="text/css">

#LoanListOptionsHolder
{
    padding-bottom: 5px;
}

#LoanListOptions
{
    position:relative;
    right:10px;
    width: 100%;
    color: black;
    overflow:hidden;
    padding-bottom: 0px;
}
#LoanListOptions
{
    list-style: none;
    margin-bottom: 0px;
    padding: 0;
}
#LoanListOptions li
{
    float:left;
    padding-right: 3px;
    vertical-align: middle;
}
#LoanListOptions li.TaskMeter
{
    overflow:hidden;
    width:280px;
    height:32px;
}

#LoanListOptions div.TaskMeter.Scale
{
    background-position: left bottom;
    background-repeat: no-repeat;
    width:280px;
    height:20px;
}
#LoanListOptions div.TaskMeter.Gradient.Child
{
    background-position: left center;
    width:0%;
    height:15px;
}

#LoanListOptions div.TaskMeter.Gradient.Parent
{
    background-color: Black;
    overflow:hidden;
    position:relative;
    top: 1px;
    left:3px;
    width:99%;
    height:15px;
}

#LoanListOptions div.TaskMeter.Gradient.Parent span
{
    color: White;
}


#LoanListOptions li span
{
    font-size: 12px;
    font-weight: bold;
    font-family: Arial;
}
</style>

<div id="LoanListOptionsHolder" style="">
    <ul id="LoanListOptions">
        <li><span>Display </span>
            <asp:DropDownList runat="server" AutoPostBack="true" ID="m_ddlTaskStatus">
                <asp:ListItem Value="0" Text="Active" Selected="True"></asp:ListItem>
                <asp:ListItem Value="1" Text="Resolved"></asp:ListItem>
            </asp:DropDownList>
        </li>
        <li><span>tasks assigned to </span>
            <asp:DropDownList runat="server" AutoPostBack="true" ID="m_TasksAssignedTo">
                <asp:ListItem Value="1" Text="me"></asp:ListItem>
                <asp:ListItem Value="0" Text="anyone" Selected="True"></asp:ListItem>
            </asp:DropDownList>
        </li>
        <li class="TaskMeter">
            <div class="TaskMeter Gradient Parent" title="Number of tasks you have resolved or closed in the past week">
                <span>Loading...</span>
                <div class="TaskMeter Gradient Child">
                </div>
            </div>
            <div class="TaskMeter Scale" title="Number of tasks you have resolved or closed in the past week">
            </div>
        </li>
        <li style="float: right; padding-right: 1em;"><span>Task #
        </span>
            <input type="text" id="txtSearchTask" size="15" onkeyup="checkSearch(event);" />
            <input type="submit" id="btnSearchTask" onclick="taskSearch(); return false;" value="Search" />
        </li>
    </ul>
</div>


<asp:GridView id="gvTasks" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="false" DataKeyField="TaskId" Width="100%" CssClass="DataGrid" AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem" HeaderStyle-CssClass="GridHeader" EnableViewState="false" RowStyle-VerticalAlign="Top" onsorting="gvTasks_Sorting" OnRowDataBound="OnTasksGridBound">
<PagerStyle VerticalAlign="Middle" horizontalalign="right" ></PagerStyle>
<columns>
		<asp:TemplateField>
			<itemtemplate>
				<input type="checkbox" id="TaskId" value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString())%>' onclick="onCheckBoxClick(this);"/>
				<input type="hidden" id="TaskHidden" value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CondIsHidden").ToString())%>' />
			</itemtemplate>
		</asp:TemplateField>

		<asp:TemplateField HeaderText="Task">
			<itemtemplate>
			    <a href="#" onclick="onTaskOpen('<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "LoanId").ToString())%>', '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString())%>');"> <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString())%> </a>
			</itemtemplate>
		</asp:TemplateField>

		<asp:TemplateField HeaderText="Subject">
			<itemtemplate>
			    <a href="#" title='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskSubject").ToString())%>'" onclick="onTaskOpen('<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "LoanId").ToString())%>', '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskId").ToString())%>');"> <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TaskSubject").ToString().Length > 70 ? DataBinder.Eval(Container.DataItem, "TaskSubject").ToString().Substring(0, 67) + "..." : DataBinder.Eval(Container.DataItem, "TaskSubject").ToString() )%></a>
			</itemtemplate>
		</asp:TemplateField>

		<asp:BoundField DataField="TaskStatus_rep" HeaderText="Status"></asp:BoundField>
		<asp:TemplateField HeaderText="Loan Number">
		<ItemTemplate>
            <a href="javascript:void(0);" onclick="f_toggleLoanOptions('<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "LoanId").ToString().Replace("-","")) %>', '<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>'); return false;" >
                <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "LoanNumCached").ToString())%>
            </a>
            <div id='LoanOptions_<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "TaskId").ToString()) %>' style="padding:0.2em;font-size:0.8em">
            </div>
        </ItemTemplate>
        </asp:TemplateField>
		<asp:BoundField DataField="BorrowerNmCached" HeaderText="Borrower Name"></asp:BoundField>
		<asp:BoundField DataField="TaskDueDate" HeaderText="Due Date" DataFormatString="{0:d}" SortExpression="TaskDueDate"></asp:BoundField>
		<asp:BoundField DataField="TaskFollowUpDate" HeaderText="Follow-up Date" DataFormatString="{0:d}" SortExpression="TaskFollowUpDate"></asp:BoundField>
		<asp:BoundField DataField="TaskLastModifiedDate" HeaderText="Last Updated" SortExpression="TaskLastModifiedDate"></asp:BoundField>
		<asp:BoundField DataField="AssignedUserFullName" HeaderText="Assigned To"></asp:BoundField>
		<asp:BoundField DataField="OwnerFullName" HeaderText="Owner"></asp:BoundField>
	</columns>
</asp:GridView>

<div  id="divControls">
<table border="0" cellpadding="3" cellspacing="0">
    <tr>
        <td align="left">
	        <input type="button" id="btnAssignTasks" value="Assign" onclick="f_AssignTasks();" />
        </td>
        <td align="left">
	        <input type="button" id="btnSetDueDate" value="Set Due Date" onclick="f_SetDueDate();" />
        </td>
        <td align="left">
	        <input type="button" id="btnSetFollowupDate" value="Set Follow-up Date" onclick="f_SetFollowupDate();" />
        </td>
        <td align="left">
	        <input type="button" id="btnTakeOwnerShip" value="Take Ownership" onclick="f_TakeOwnership();" />
        </td>
        <td align="left">
	        <input type="button" id="btnExportToCsv" type="submit" value="Export to CSV" onclick="f_ExportToCsv();"/>
        </td>
        <td align="left">
	        <input type="button" id="btnSubscribeToTasks" value="Subscribe" onclick="f_SubscribeTasks();" />
        </td>
        <td align="left">
	        <input type="button" id="btnUnsubscribeTasks" value="Un-subscribe" onclick="f_UnsubscribeTasks();" />
        </td>
    </tr>
</table>
</div>

<input type="hidden" id="inputCmd" enableviewstate="false" runat="server" />
<input type="hidden" id="inputParams" enableviewstate="false" runat="server"/>
<asp:HiddenField ID="userOwnedTaskIds" runat="server" Value="" />
