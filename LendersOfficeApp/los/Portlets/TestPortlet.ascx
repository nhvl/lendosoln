<%@ Control Language="c#" AutoEventWireup="false" Codebehind="TestPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.TestPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="uc" TagName="AssignLoanButton" Src="AssignLoanButton.ascx" %>
<%@ Register TagPrefix="uc" TagName="DuplicateLoanButton" Src="DuplicateLoanButton.ascx" %>
<%@ Register TagPrefix="uc" TagName="RefreshButton" Src="RefreshButton.ascx" %>
<%@ Register TagPrefix="uc" TagName="DeleteLoanButton" Src="DeleteLoanButton.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<asp:HiddenField runat="server" ID="ShowCircumstances" />
<table width="100%" border="0" cellpadding="3" cellspacing="0">
    <tr>
        <td align="left" nowrap style="padding:1;"  >
    	    <uc:AssignLoanButton id="m_assignButton" runat="server">
	        </uc:AssignLoanButton>
	        <uc:DuplicateLoanButton id="m_copySelected" runat="server">
	        </uc:DuplicateLoanButton>
	        <uc:DeleteLoanButton id="m_deleteButton" runat="server">
	        </uc:DeleteLoanButton>
	        <uc:RefreshButton id="m_refreshPanel" runat="server">
	        </uc:RefreshButton>
	        <span style="color:red;font-weight:bold;padding-left:15px;padding-right:15px;" title="Please use our Search page to find more loans"><ml:EncodedLiteral ID="m_tooManyLabel" Runat="server" EnableViewState="False"></ml:EncodedLiteral></span>
        </td>
    </tr>
</table>

<asp:Panel id="m_rowsPanel" runat="server" style="BORDER: 0px solid lightgray; WIDTH: 100%; PADDING: 0px;">
    <ml:CommonDataGrid runat="server" ID="m_TestGrid" DataKeyField="sLId" OnItemDataBound="BindRows">
        <HeaderStyle CssClass="GridHeader" />
        <ItemStyle CssClass="GridItem" />
        <AlternatingItemStyle CssClass="GridAlternatingItem" />
        <Columns>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:Panel id="CheckBox" runat="server">
                        <input id="loanid" type="checkbox" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId"))%>" title="Select this loan" onclick="onCheckBoxClick( this );" />
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="LoanNumber" SortExpression="sLNm">
                <ItemTemplate>
                    <div style="CURSOR: pointer;">
                        <div style="COLOR: blue;" onmouseover="style.color = 'orange';" onmouseout="style.color = 'blue';" onclick="onClickLoanLinkSet( <%# AspxTools.ClientId(m_rowsPanel) %> , this );">
                            <u>
                                <ml:EncodedLabel id="sLNm" runat="server" ></ml:EncodedLabel>
                            </u>
                        </div>
                        <div class="Div0" style="display:none">
                            <asp:Panel id="EditLink" runat="server" class="pl0"  EnableViewState="False">
                                &#x2022;
                                <asp:Panel id="Show" runat="server" style="DISPLAY: inline;" EnableViewState="False">
                                    <a href="javascript:editLoan( '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>' );" title="Edit loan">edit</a>
                                </asp:Panel>
                                <asp:HyperLink id="Hide" runat="server" Enabled="False" EnableViewState="False" style="cursor:default" title="You do not have permission to edit this loan.">edit  <a href='#' onclick="javascript:f_showReasonDetails( '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>', event );" >?</a></asp:HyperLink>
                            </asp:Panel>
                            <asp:Panel id="LeadLink" runat="server" class="pl0" EnableViewState="False">
                                &#x2022;
                                <asp:Panel ID="LeadShow" Runat="server" EnableViewState="False" style="display:inline;">
                                    <a href="javascript:editLead( '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>' );" title="Edit lead">edit</a>
                                </asp:Panel>
                                <asp:HyperLink id="LeadHide" Visible="false" runat="server" Enabled="False" EnableViewState="False" style="cursor:default" title="You do not have permission to edit this lead.">edit</asp:HyperLink>
                            </asp:Panel>
                            <asp:Panel id="ViewLink" runat="server" class="pl0"  EnableViewState="False">
                                &#x2022; <a href="javascript:viewLoan( '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>' );" title="View loan summary">summary</a>
                            </asp:Panel>
                            <div nowrap class="pl0">
                                &#x2022; <a href="javascript:createTask( '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>' );" title="Insert new task for this loan">new task</a>
                            </div>
                            <div nowrap id="extend_<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>" style="display:none" class="pl0">
                                &#x2022; <a href="javascript:rateLock( '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>', 'extend' );" >extend lock</a>
                            </div>
                            <div nowrap id="floatdown_<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>" style="display:none" class="pl0">
                                &#x2022; <a href="javascript:rateLock( '<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>', 'floatdown' );" >float down lock</a>
                            </div>
                            <div nowrap id="relock_<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>" style="display:none" class="pl0">
                                &#x2022; <a href="javascript:rateLock( <%# AspxTools.JsString((Guid) DataBinder.Eval(Container.DataItem, "sLId")) %>, 'relock' );" >rate re-lock</a>
                            </div>
                            <asp:Repeater ID="GenericFrameworkVendorList" runat="server" EnableViewState="false">
                                <ItemTemplate>
                                    <div nowrap style="display:none">
                                        &#x2022; <a id="GenericFrameworkLaunchLink" class="GenericFrameworkVendor" runat="server" />
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:TemplateColumn>

            <asp:BoundColumn DataField="aBLastNm" HeaderText="Borr Last Name" SortExpression="aBLastNm" ></asp:BoundColumn>
            <asp:BoundColumn DataField="aBFirstNm" HeaderText="Borr First Name" SortExpression="aBFirstNm" ></asp:BoundColumn>
            <asp:BoundColumn DataField="sEmployeeLoanRepName" HeaderText="Assigned Loan Officer Name" SortExpression="sEmployeeLoanRepName" ></asp:BoundColumn>
            <asp:BoundColumn DataField="LoanStatus" HeaderText="Loan Status" SortExpression="LoanStatus" ></asp:BoundColumn>
            <asp:BoundColumn DataField="sStatusD" HeaderText="Loan Status Date" DataFormatString="{0:d}" SortExpression="sStatusD" ></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Rate Lock Status" SortExpression="sRateLockStatusT">
                <ItemTemplate>
                    <ml:EncodedLabel id="sRateLockStatusT" runat="server" ></ml:EncodedLabel>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="sRLckdExpiredD" HeaderText="Lock Expiration Date" DataFormatString="{0:d}" SortExpression="sRLckdExpiredD" ></asp:BoundColumn>
            <asp:BoundColumn DataField="sEstCloseD" HeaderText="Estimated Close Date" DataFormatString="{0:d}" SortExpression="sEstCloseD" ></asp:BoundColumn>
            <asp:BoundColumn DataField="sLtvR" HeaderText="LTV" SortExpression="sLtvR" ></asp:BoundColumn>
        </Columns>
    </ml:CommonDataGrid>
</asp:Panel>

<asp:Panel id="m_emptyNote" runat="server" style="COLOR: dimgray; PADDING: 160px; TEXT-ALIGN: center;" Visible="False" EnableViewState="False">
	Nothing to show.  Your pipeline is empty.
</asp:Panel>
<asp:Panel id="m_errorNote" runat="server" style="COLOR: red; PADDING: 160px; TEXT-ALIGN: center;" Visible="True" EnableViewState="False">
	Nothing to show.  Unable to load pipeline at this time.
</asp:Panel>
<div id="ReasonDetails" style="border-right: black 3px solid; padding-right: 5px;
    border-top: black 3px solid; display: none; padding-left: 5px; padding-bottom: 5px;
    border-left: black 3px solid; width: 330px; padding-top: 5px; border-bottom: black 3px solid;
    position: absolute; height: 50px; background-color: whitesmoke; color: #000 !important;"> 
    <table width="100%">
        <tr id="LoadingRow_rd" style="display:none" >
            <td style="color:#000; background-color: whitesmoke; ">
                Loading...
            </td>
        </tr>
        <tr id="DataRow_rd" >
            <td id="reasonDetails_t" style="color:#000; background-color: whitesmoke; ">
              You do not have permission to edit this loan.
            </td>
        </tr>
        <tr>
            <td align="center" style="color: Black;">
                [ <a href="#" onclick="f_closeReasonDetails();return false;">Close</a> ]
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">
<!--
    var gSelectionCount = 0;

    function _init()
    {
	    disableButtons( true );
    }
	
    function disableButtons( bDisable )
    {
	    var ids = ["DuplicateLoanBtn", "AssignLoanBtn", "DeleteLoanBtn"];

        for (var i = 0; i < ids.length; i++) {
            if (document.getElementById(ids[i]) != null) {
                document.getElementById(ids[i]).disabled = bDisable;
            }
        }
    }

	function f_showReasonDetails(id, event) 
	{
		var circumstances = document.getElementById('<%= AspxTools.ClientId(ShowCircumstances) %>');
		    
		if( circumstances.value === 'True') {
		    document.getElementById('LoadingRow_rd').style.display = '';
		    document.getElementById('DataRow_rd').style.display = 'none';
		}
		    
		var msg = document.getElementById("ReasonDetails");
		msg.style.display = "block";
		msg.style.top = (document.body.scrollTop + event.clientY - 10)+ "px";
		msg.style.left = (event.clientX + 10 ) + "px";
			
		if( circumstances.value === 'True' ) {
			    window.setTimeout(function(){
			        var args = { 
		            LoanId : id
		        };
    		        
		        var results = gService.loanutils.call("GetReasonsUserCannotEdit", args);
		        var data = JSON.parse(results.value.Result);
		        document.getElementById('reasonDetails_t').innerHTML =  createList(data);
		        document.getElementById('LoadingRow_rd').style.display = 'none';
		        document.getElementById('DataRow_rd').style.display = '';
            }, 0);
        }
		return false;
	}

	function f_closeReasonDetails() 
	{
		var fid = "ReasonDetails";
		document.getElementById(fid).style.display = "none";
	}

    function createList(data) {
        var builder = ['You do not have permission to edit this loan.'], i;
        
        if( data.length === 0 ) {
            return builder[0];
        }
        
        builder.push('The loan can be edited under any of the following circumstances. <ul>');
        for( i = 0; i < data.length; i++) {
            builder.push('<li>');
            builder.push(data[i]);
            builder.push('</li>');
        }
        builder.push('</ul>');
        return builder.join('');
        
    }

    function onCheckBoxClick( cBox )
    {
		
	    if( cBox.checked )
	    {
		    gSelectionCount++;
	    }
	    else
	    {
		    gSelectionCount--;
	    }

	    highlightRowByCheckbox( cBox );
		
	    disableButtons(gSelectionCount == 0);
    }

    function onClickLoanLinkSet( oBase , oTarget ) { try
    {
        // Check for valid targets.

        if( oBase == null || oTarget == null )
        {
            return;
        }

        // Hide all others and show selected link set.
        var nextSibling = oTarget.nextSibling;
        while(nextSibling.nodeType !== 1)
        {
            nextSibling = nextSibling.nextSibling;
        }
        
        if( nextSibling != null )
        {
            // Find containing row and prepare to make all the
            // text bold in the following cells.

            var oRow = oTarget;

            while( oRow != null )
            {
                if( oRow.tagName.toUpperCase() == "TR" )
                {
                    break;
                }

                oRow = oRow.parentElement;
            }
            if( nextSibling.style.display == "none" )
            {
                // Turn off all previously activated link sets within
                // this grid and then turn on the selected one.

                nextSibling.style.display    = "";
                nextSibling.style.fontWeight = "normal";

                if( oRow != null )
                {
                    oRow.style.fontWeight = "bold";
                }
               
                var editLoanHref = nextSibling.firstChild.firstChild.nextSibling.firstChild.href;
                if(editLoanHref.indexOf('javascript:editLead(') == 0)
                {
                    return; // don't even attempt to get the lock links for leads
                }
		        var loanId = editLoanHref.replace('javascript:editLoan(','').replace(');','').substring(2,38);
                if ( loanId != null )
                {
                    var args = new Object();
                    args['loanid'] = loanId;
                    setTimeout(function() {
                    var result = gService.loanutils.call("GetRateLockStatus", args);
                    if (result.value['extend'] == 'True' )
                    {
                        var link = document.getElementById('extend_' + loanId);
                        if ( link != null)
                            link.style.display='inline';
                    }
                    if (result.value['floatdown'] == 'True' )
                    {
                        var link = document.getElementById('floatdown_' + loanId);
                        if ( link != null)
                            link.style.display='inline';
                    }
                    if (result.value['relock'] == 'True' )
                    {
                        var link = document.getElementById('relock_' + loanId);
                        if ( link != null)
                            link.style.display='inline';
                    }
                    
                    updateGenericFrameworkPermissions(loanId);
                    }, 0);
	            }
                
            }
            else
            {
                // Turn off the selected one and leave the
                // other link sets alone.

                nextSibling.style.display = "none";

                if( oRow != null )
                {
                    oRow.style.fontWeight = "normal";
                }
            }
        }
    }
    catch( e )
    {
        window.status = "Selecting " + oTarget.innerText + " failed.";
    }}
    
    function updateGenericFrameworkPermissions(loanID)
    {
        var providerIDs = [];
        var $genericFrameworkVendors = $j('tr.Loan_' + loanID + ' .GenericFrameworkVendor');
        $genericFrameworkVendors.each(function()
        {
            providerIDs.push(this.getAttribute('data-providerid'));
            this.parentNode.style.display = 'none';
        });
        if (providerIDs.length > 0)
        {
            var args =
            {
                LoanID : loanID,
                ProviderIDs : JSON.stringify(providerIDs)
            };
            var genericFrameworkResult = gService.loanutils.call("GetGenericFrameworkVendors", args);
            if (!genericFrameworkResult.error && genericFrameworkResult.value) {
                var providerIDMapping = JSON.parse(genericFrameworkResult.value.ProviderIDPermissions);
                for (var i = 0; i < providerIDMapping.length; ++i) {
                    $genericFrameworkVendors.filter('[data-providerid="' + providerIDMapping[i].Key + '"]').each(function() {if (providerIDMapping[i].Value){this.parentNode.style.display = 'inline';}});
                }
            }
        }
    }

    function launchGenericFramework(providerID, sLId)
    {
      var args = 
      {
        sLId: sLId,
        ProviderID: providerID
      };
      
      var result = gService.loanutils.call("LoadGenericFrameworkVendor", args);
      if (result.error) {
        alert(result.UserMessage);
      } else if (result.value["HasError"] === 'True') {
        alert(result.value["ErrorMessage"]);
      } else {
        var url = result.value["PopupURL"];
        var height = result.value["PopupHeight"];
        var width = result.value["PopupWidth"];
        var isModal = result.value["PopupIsModal"] === 'True';
        if (isModal) {
            showModal(url, null, 'dialogHeight:' + height + 'px; dialogWidth:' + width + 'px; center: yes; resizable: yes;');
        } else {
            window.open(url, '_blank', 'height=' + height + ', width=' + width + ', resizable=yes, scrollbars=yes');
        }
      }
    }
// -->
</script>
