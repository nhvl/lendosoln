using System;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using DataAccess;
using LendersOfficeApp.common;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using System.Data.SqlClient;
using LendersOffice.Reminders;
using LendersOffice.Common;

namespace LendersOfficeApp.los.Portlets
{
	/// <summary>
	///	Summary description for DiscTrackPortlet.
	/// </summary>

	public partial  class DiscTrackPortlet : CDiscUIBase, IAutoLoadUserControl
	{
		protected System.Collections.Hashtable m_loanAccessPermissions = new Hashtable();

		public void LoadData() 
		{
			LoadFilterStatus();
			m_trackDg_DataBinding();
		}

		public void SaveData() 
		{
		}

		/// <summary>
		/// Initialize page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
		}

		/// <summary>
		/// Render page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Set filter state.
			if( m_UIState.Value.ToLower().IndexOf( "filter=hide" ) != -1 )
			{
				m_panelTaskFilter.Style[ "DISPLAY" ] = "none";
			}
			else
			{
				m_panelTaskFilter.Style[ "DISPLAY" ] = "inline";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_trackDg.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.m_trackDg_ItemDataBound);
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		private void m_trackDg_DataBinding(/*object sender, System.EventArgs e*/)
		{
			// 01/24/06 MF. OPM	3841 The task list filter is now applied at the
			// stored proc level so fewer tasks are loaded into memory.
			// The data is now immediately bound without subsequent filtering.
            DataSet taskList = new DataSet();
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", BrokerUser.UserId),
				                            new SqlParameter("@Filter", new String(createCbFlagString()))
                                        };

			DataSetHelper.Fill(taskList, BrokerUser.BrokerId, "ListFilteredDiscTracksByCreatorUserId", parameters);

			if (taskList != null)
			{
				m_trackDg.DataSource = taskList.Tables[0].DefaultView;
				m_trackDg.DefaultSortExpression = "TrackNotifIsRead ASC";
				
				//7/22/08 - jk. Load sort order from cookie and after databind, store current sort to the cookie
				HttpCookie cookie = Request.Cookies["discTrack_sortOrder"];
				if(cookie != null && cookie.Value!= null && cookie.Value != "")
				{
					m_trackDg.DefaultSortExpression = cookie.Value;
				}	

				m_trackDg.DataBind();

				LendersOffice.Common.RequestHelper.StoreToCookie("discTrack_sortOrder", taskList.Tables[0].DefaultView.Sort, false);
				
			}
		}

		protected void SaveFilterClick(object sender, System.EventArgs args) 
		{
			// 01/24/06 MF. OPM 3747. Handle filter Save button click event.
			// Save the values to the DB and hide the filter settings panel if successful.
			
			char[] checkBoxSettings = createCbFlagString();

			BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal; //Page.User as BrokerUserPrincipal;
			if (brokerUser != null)
			{
				SqlParameter[] parameters = new SqlParameter[] {
																   new SqlParameter("@UserID", brokerUser.UserId),
																   new SqlParameter("@DiscFilterSettings", new String(checkBoxSettings)),
																   new SqlParameter("@TaskTab", "T")
																};
				try 
				{
					int ret = StoredProcedureHelper.ExecuteNonQuery(brokerUser.BrokerId, "UpdateTaskFilterSettings", 0, parameters);
					if (ret > 0) 
					{
						// Hide the filter panel when save is complete
						m_UIState.Value = "Filter=Hide";
						m_panelTaskFilter.Style[ "DISPLAY" ] = "none";
					}
				} 
				catch (Exception exc)
				{
					Tools.LogError("Failed to update task filter settings in Tasks in Your Loans", exc);
				}
			}
		}

		private void LoadFilterStatus()
		{
			// MF 01/24/06 OPM 3747. Load the User's saved filter checkbox status.
			// Load the CB settings from the DB if they have been set

			// TODO: Consider handling these checkboxes as a CheckBoxList to avoid
			// too much repeated code among task tabs

			if (!Page.IsPostBack)
			{
				string checkBoxSettings = string.Empty; 
				BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal; //Page.User as BrokerUserPrincipal;
				if (brokerUser != null)
				{
					try
					{
						SqlParameter[] parameters = new SqlParameter[] {
													new SqlParameter("@UserID", brokerUser.UserId),
													new SqlParameter("@TaskTab", "T")
											   };

						using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerUser.BrokerId, "RetrieveTaskFilterSettingsByID", parameters))
						{
							while (reader.Read()) 
								checkBoxSettings = (string) reader["DiscFilterSettings"];
						}
					}
					catch (Exception exc)
					{
						Tools.LogError("Failed to load filter settings for Tracked Tasks", exc);
					}
				}
				
				// If there is saved data, set the status of the checkboxes
				// and hide the panel.
				if ( checkBoxSettings != null && checkBoxSettings.TrimWhitespaceAndBOM().Length > 0 )
				{
					applyFlagsToCheckBoxes(checkBoxSettings.ToCharArray());
					m_UIState.Value = "Filter=Hide";
					m_panelTaskFilter.Style[ "DISPLAY" ] = "none";
				}
			} 
		}
		
		private void m_trackDg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			// Generate the proper links for this row's loan
			GenerateLoanLinks(sender, e, m_loanAccessPermissions);
			
			// Disable row highlighting for now.  Makes it hard to read loans.		
						
			//			switch( e.Item.ItemType )
			//			{
			//				case ListItemType.Item:
			//					e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Yellow'");
			//					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'");
			//					break;
			//				case ListItemType.AlternatingItem: 
			//					e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Yellow'");
			//					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#CCCCCC'");
			//					break;
			//				default:
			//					break;
			//			}

		}

		#region Check Box Methods
		private void applyFlagsToCheckBoxes(char[] checkBoxFlags)
		{
			// Sets the checkboxes based on the passed flag string

			if (checkBoxFlags.Length >= 9)
			{
				m_cbReadStatusRead.Checked = (checkBoxFlags[ (int) trackCheckBoxes.ReadStatusRead ] == '1');
				m_cbReadStatusNotRead.Checked = (checkBoxFlags[ (int) trackCheckBoxes.ReadStatusNotRead ] == '1');
				m_cbTaskStatusActive.Checked = (checkBoxFlags[ (int) trackCheckBoxes.TaskStatusActive ] == '1');
				m_cbTaskStatusDone.Checked = (checkBoxFlags[ (int) trackCheckBoxes.TaskStatusDone ] == '1');
				m_cbTaskStatusSuspended.Checked = (checkBoxFlags[ (int) trackCheckBoxes.TaskStatusSuspended ] == '1');
				m_cbTaskStatusCancelled.Checked = (checkBoxFlags[ (int) trackCheckBoxes.TaskStatusCancelled ] == '1');
				m_cbPriorityLow.Checked = (checkBoxFlags[ (int) trackCheckBoxes.PriorityLow ] == '1');
				m_cbPriorityNormal.Checked = (checkBoxFlags[ (int) trackCheckBoxes.PriorityNormal ] == '1');
				m_cbPriorityHigh.Checked = (checkBoxFlags[ (int) trackCheckBoxes.PriorityHigh ] == '1');
			}
		}

		private char[] createCbFlagString()
		{
			// Returns a fixed-length char array that represents the status of the checkboxes

			char[] checkBoxSettings = new char[MAX_FILTER_LENGTH];
			for (int i = 0; i < MAX_FILTER_LENGTH; i++)
				checkBoxSettings[i] = ' ';

			checkBoxSettings[ (int) trackCheckBoxes.ReadStatusRead ] = m_cbReadStatusRead.Checked?'1':'0';
			checkBoxSettings[ (int) trackCheckBoxes.ReadStatusNotRead ] = m_cbReadStatusNotRead.Checked?'1':'0';
			checkBoxSettings[ (int) trackCheckBoxes.TaskStatusActive ] = m_cbTaskStatusActive.Checked?'1':'0';
			checkBoxSettings[ (int) trackCheckBoxes.TaskStatusDone ]	= m_cbTaskStatusDone.Checked?'1':'0';
			checkBoxSettings[ (int) trackCheckBoxes.TaskStatusSuspended ] = m_cbTaskStatusSuspended.Checked?'1':'0';
			checkBoxSettings[ (int) trackCheckBoxes.TaskStatusCancelled ] = m_cbTaskStatusCancelled.Checked?'1':'0';
			checkBoxSettings[ (int) trackCheckBoxes.PriorityLow ] = m_cbPriorityLow.Checked?'1':'0';
			checkBoxSettings[ (int) trackCheckBoxes.PriorityNormal ] = m_cbPriorityNormal.Checked?'1':'0';
			checkBoxSettings[ (int) trackCheckBoxes.PriorityHigh ] = m_cbPriorityHigh.Checked?'1':'0';
			return checkBoxSettings;
		}

		#endregion
	}

}
