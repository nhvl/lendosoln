<%@ Control Language="c#" AutoEventWireup="false" Codebehind="QuickSearchPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.QuickSearchPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<TABLE class="Portlet" id="Table1" cellSpacing="0" cellPadding="0" width="140" border="0" onkeypress="return DisableEnter(event)">
    <tr>
        <td class="PortletHeader" nowrap>Search </td>
    </tr>
	<TR>
		<TD noWrap>
			<div>
			    <% /* OPM 66600 - If you change the fields in the dropdown make sure to edit the conditionals in
			        * PageLoad of loanfind.aspx to check whether all dates should be selected by default. */ %>
				<asp:DropDownList id="m_searchTypeDDL" runat="server" Width="133px">
					<asp:ListItem Value="0" Text="Loan Number" />
                    <asp:ListItem Value="5" Text="Loan Reference Number" />
					<asp:ListItem Value="1" Text="First Name" />
					<asp:ListItem Value="2" Text="Last Name" />
					<asp:ListItem Value="3" Text="SSN (Last Four)" />
					<asp:ListItem Value="4" Text="Property Address" />
				</asp:DropDownList>
			</div>
		</TD>
	</TR>
	<TR>
		<TD noWrap>
			<asp:Panel id="m_rootP" runat="server">
                <span id="m_Text" style="display: align;">
                    <asp:TextBox ID="m_keywordTF" runat="server" Width="109px" onkeydown="return SubmitOnEnter(event);" onkeyup="return TextChange();" MaxLength="36" />
                </span><span style="vertical-align: bottom;">
                    <asp:Button ID="m_searchBtn" runat="server" Style="height: 21px;" Text="Go" />
                </span>
            </asp:Panel>
		</TD>
	</TR>
	<tr>
	    <td align="center">
	        <b>Tip: Tack on an * for partial matching</b>
	    </td>
	</tr>
	<TR>
		<TD noWrap>
			<A class="PortletLink" href="loanfind.aspx">Advanced Search </A>
		</TD>
	</TR>
	<TR>
		<TD><IMG src="../images/shadow_line.gif" width="130" height="2"></TD>
	</TR>
</TABLE>
<script type="text/javascript">
    
    function DisableEnter(event) {
        if(event.keyCode == 13) {
            return false;
        }
        
        return true;
    }
    
    function SubmitOnEnter(event) {
        if(event.keyCode == 13) {
            var v = <%= AspxTools.JsGetElementById(m_keywordTF) %>.value;   
		    v = v.replace(/^\s+|\s+$/g, "");
		    if( v.length != 0 ) { 
		        setTimeout(function() {
		            <%= AspxTools.JsGetElementById(m_searchBtn) %>.click();
                }, 5);
            }
            return false;
        } 

        return true;
    }
    
    function TextChange() {
        // 3/23/2007 nw - OPM 6662 - Strip any asterisks from the search phrase when user does SSN search
        // 08/07/2008 ck - OPM 6487 - Disable "Go" button when no search criteria is specified
        // 08/12/2008 ck - OPM 24053 - "Go" button will only be clicked when the search button is clicked
        //							   or when "enter" is pressed    
	    var v = <%= AspxTools.JsGetElementById(m_keywordTF) %>.value;   
	    v = v.replace(/^\s+|\s+$/g, "");
		
	    <%= AspxTools.JsGetElementById(m_searchBtn) %>.disabled = v === "";

	    if (<%= AspxTools.JsGetElementById(m_searchTypeDDL) %>.value == "3")	// SSN selected in drop down
	    {
	        var newVal = <%= AspxTools.JsGetElementById(m_keywordTF) %>.value.replace(/\*/g, "");
	        if(<%= AspxTools.JsGetElementById(m_keywordTF) %>.value !== newVal)
	        {
		        <%= AspxTools.JsGetElementById(m_keywordTF) %>.value = newVal;
		    }
	    }    
	    return true;
    }
</script>

