using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Reminders;
using LendersOffice.Security;
using LendersOfficeApp.common;
using LendersOfficeApp.los.admin;
using LendersOffice.Common;

namespace LendersOfficeApp.los.Portlets
{
	/// <summary>
	///	Summary description for MyLoanRemindersPortlet.
	/// </summary>

	public partial  class MyLoanRemindersPortlet : CDiscUIBase, IAutoLoadUserControl
	{
		protected System.Web.UI.WebControls.Button 		 m_changeFilterBtn;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Button Button2;
		protected System.Collections.Hashtable m_loanAccessPermissions = new Hashtable();

		private void BindDataGrid()
		{
			// 01/24/06 MF. OPM	3841 The task list filter is now applied at the
			// stored proc level so fewer tasks are loaded into memory.
			// The data is now immediately bound without subsequent filtering.
			
			BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;
			if ( brokerUser != null )
			{
                DataSet taskList = new DataSet();

                SqlParameter[] parameters = {
                                                new SqlParameter( "@EmployeeID"     , brokerUser.EmployeeId )
					                            , new SqlParameter( "@UserId"         , brokerUser.UserId     )
					                            , new SqlParameter( "@BrokerId"       , brokerUser.BrokerId   )
					                            , new SqlParameter( "@Filter"         , new String(createCbFlagString())   )
                                            };
				DataSetHelper.Fill(taskList, brokerUser.BrokerId, "ListFilteredDiscAccessibleByEmployeeId", parameters);

				if (taskList != null)
				{		

					m_remindersDG.DataSource = taskList.Tables[0].DefaultView;
					m_remindersDG.DefaultSortExpression = "DiscRefObjNm1 ASC";
					

					//7/22/08 - jk. Load sort order from cookie and after databind, store current sort to the cookie
					HttpCookie cookie = Request.Cookies["myLoan_sortOrder"];
					if(cookie != null && cookie.Value!= null && cookie.Value != "")
					{
						m_remindersDG.DefaultSortExpression = cookie.Value;
					}	

					m_remindersDG.DataBind();

					LendersOffice.Common.RequestHelper.StoreToCookie("myLoan_sortOrder", taskList.Tables[0].DefaultView.Sort, false);
				}
				if ( m_remindersDG.Items.Count == 500 )
					m_tooManyLabel.Text = "Too many tasks -- only 500 are displayed.";
			}
		}

		private void LoadFilterStatus()
		{
			// MF 01/10/06 OPM 3506. Load the User's saved filter checkbox status
			// and hide the filter panel if successful.

			// TODO: Consider handling these checkboxes as a CheckBoxList to avoid
			// too much repeated code among task tabs

			if (!Page.IsPostBack)
			{
				string checkBoxSettings = string.Empty; 
				BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;
				if (brokerUser != null)
				{
					try
					{
                        SqlParameter[] parameters = {
                                                        new SqlParameter("@UserID", brokerUser.UserId),
                                                        new SqlParameter("@TaskTab", "L")
                                                    };

						using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerUser.BrokerId, "RetrieveTaskFilterSettingsByID", parameters))
						{
							while (reader.Read()) 
								checkBoxSettings = (string) reader["DiscFilterSettings"];
						}
					}
					catch (Exception exc)
					{
						Tools.LogError("Failed to load filter settings for Tasks in Your Loans", exc);
					}
				}
				
				// If there is saved data, set the status of the checkboxes
				// and hide the panel.
				if (checkBoxSettings != null && checkBoxSettings.TrimWhitespaceAndBOM().Length > 0)
				{
					applyFlagsToCheckBoxes( checkBoxSettings.ToCharArray() );
					m_UIState.Value = "Filter=Hide";
					m_panelTaskFilter.Style[ "DISPLAY" ] = "none";
				}
			}
		}

		protected string GetImage(object value) 
		{
			string img = "";

			// TODO: Need to replace different gif image for overdue item.
			//            if (value is DateTime) 
			//            {
			//                DateTime dt = (DateTime) value;
			//
			//                if (dt.CompareTo(DateTime.Now) <= 0) 
			//                { 
			//                    img = @"<img src='../images/redalert.gif'>&nbsp;";
			//                }
			//            }
			return img;
		}

		public void LoadData() 
		{
			LoadFilterStatus();
			BindDataGrid();
		}

		public void SaveData() 
		{
		}

		/// <summary>
		/// Initialize page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{

		}

		/// <summary>
		/// Render page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Set filter visibility state.
			if( m_UIState.Value.ToLower().IndexOf( "filter=hide" ) != -1 )
			{
				m_panelTaskFilter.Style[ "DISPLAY" ] = "none";
			}
			else
			{
				m_panelTaskFilter.Style[ "DISPLAY" ] = "inline";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_remindersDG.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.m_remindersDG_ItemDataBound);
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

		private void m_remindersDG_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			// Generate the proper links for this row's loan
			GenerateLoanLinks(sender, e, m_loanAccessPermissions);
		}

		protected void SaveFilterClick(object sender, System.EventArgs args) 
		{
			// 01/10/06 MF. OPM 3506. Handle filter Save button click event.
			// Save the values to the DB and hide the filter settings panel.
	
			char[] checkBoxSettings = createCbFlagString();
			
			// Attempt to write value to DB
			BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal; //Page.User as BrokerUserPrincipal;
			if (brokerUser != null)
			{
 
			SqlParameter[] parameters = new SqlParameter[] {
															   new SqlParameter("@UserID", brokerUser.UserId),
															   new SqlParameter("@DiscFilterSettings", new String(checkBoxSettings)),
															   new SqlParameter("@TaskTab", "L")
														       };
				try 
				{
					int ret = StoredProcedureHelper.ExecuteNonQuery(brokerUser.BrokerId, "UpdateTaskFilterSettings", 0, parameters);
					if (ret > 0) 
					{
						// Hide the filter panel when save is complete
						m_UIState.Value = "Filter=Hide";
						m_panelTaskFilter.Style[ "DISPLAY" ] = "none";
					}
				} 
				catch (Exception exc)
				{
					Tools.LogError("Failed to update task filter settings in Tasks in Your Loans", exc);
				}
			}
		}

		#region Check Box Methods
		private void applyFlagsToCheckBoxes(char[] checkBoxFlags)
		{
			// Sets the checkboxes based on the passed flag string

			if (checkBoxFlags.Length >= 27)
			{
				m_cbTaskStatusActive.Checked = (checkBoxFlags[ (int) checkBoxes.TaskStatusActive ] == '1');
				m_cbTaskStatusDone.Checked = (checkBoxFlags[ (int) checkBoxes.TaskStatusDone ] == '1');
				m_cbTaskStatusSuspended.Checked = (checkBoxFlags[ (int) checkBoxes.TaskStatusSuspended ] == '1');
				m_cbTaskStatusCancelled.Checked = (checkBoxFlags[ (int) checkBoxes.TaskStatusCancelled ] == '1');
				m_cbParticipationActive.Checked = (checkBoxFlags[ (int) checkBoxes.ParticipationActive ] == '1');
				m_cbParticipationInactive.Checked = (checkBoxFlags[ (int) checkBoxes.ParticipationInactive ] == '1');
				m_cbParticipationOther.Checked = (checkBoxFlags[ (int) checkBoxes.ParticipationOther ] == '1');
				m_cbPriorityLow.Checked = (checkBoxFlags[ (int) checkBoxes.PriorityLow ] == '1');
				m_cbPriorityNormal.Checked = (checkBoxFlags[ (int) checkBoxes.PriorityNormal ] == '1');
				m_cbPriorityHigh.Checked = (checkBoxFlags[ (int) checkBoxes.PriorityHigh ] == '1');
				m_cbLoanStatusOpen.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusOpen ] == '1');
				m_cbLoanStatusPreQual.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusPreQual ] == '1');
				m_cbLoanStatusPreApprove.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusPreApprove ] == '1');
				m_cbLoanStatusSubmitted.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusSubmitted ] == '1');
				m_cbLoanStatusApproved.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusApproved ] == '1');
				m_cbLoanStatusDocs.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusDocs ] == '1');
				m_cbLoanStatusFunded.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusFunded ] == '1');
				m_cbLoanStatusOnHold.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusOnHold ] == '1');
				m_cbLoanStatusUnderwritten.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusUnderwritten ] == '1');
				m_cbLoanStatusRecorded.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusRecorded ] == '1');
				m_cbLoanStatusSuspended.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusSuspended ] == '1');
				m_cbLoanStatusRejected.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusRejected ] == '1');
				m_cbLoanStatusCancelled.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusCancelled ] == '1');
				m_cbLoanStatusClosed.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusClosed ] == '1');
				m_cbLoanStatusOther.Checked = (checkBoxFlags[ (int) checkBoxes.LoanStatusOther ] == '1');
				m_cbTaskAuthorMine.Checked = (checkBoxFlags[ (int) checkBoxes.TaskAuthorMine ] == '1');
				m_cbTaskAuthorOthers.Checked = (checkBoxFlags[ (int) checkBoxes.TaskAuthorOthers ] == '1');
			}
		}

		private char[] createCbFlagString()
		{
			// Returns a fixed-length char array that represents the status of the checkboxes
			
			char[] checkBoxSettings = new char[MAX_FILTER_LENGTH];
			for (int i =0; i < MAX_FILTER_LENGTH; i++)
				checkBoxSettings[i] = ' ';

			checkBoxSettings[ (int) checkBoxes.TaskStatusActive ] = m_cbTaskStatusActive.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.TaskStatusDone ] = m_cbTaskStatusDone.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.TaskStatusSuspended ] = m_cbTaskStatusSuspended.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.TaskStatusCancelled ] = m_cbTaskStatusCancelled.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.ParticipationActive ] = m_cbParticipationActive.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.ParticipationInactive ] = m_cbParticipationInactive.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.ParticipationOther ] = m_cbParticipationOther.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.PriorityLow ] = m_cbPriorityLow.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.PriorityNormal ] = m_cbPriorityNormal.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.PriorityHigh ] = m_cbPriorityHigh.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusOpen ] = m_cbLoanStatusOpen.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusPreQual ] = m_cbLoanStatusPreQual.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusPreApprove ] = m_cbLoanStatusPreApprove.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusSubmitted ] = m_cbLoanStatusSubmitted.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusApproved ] = m_cbLoanStatusApproved.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusDocs ] = m_cbLoanStatusDocs.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusFunded ] = m_cbLoanStatusFunded.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusOnHold ] = m_cbLoanStatusOnHold.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusUnderwritten ] = m_cbLoanStatusUnderwritten.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusRecorded ] = m_cbLoanStatusRecorded.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusSuspended ] = m_cbLoanStatusSuspended.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusRejected ] = m_cbLoanStatusRejected.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusCancelled ] = m_cbLoanStatusCancelled.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusClosed ] = m_cbLoanStatusClosed.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.LoanStatusOther ] = m_cbLoanStatusOther.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.TaskAuthorMine ] = m_cbTaskAuthorMine.Checked?'1':'0';
			checkBoxSettings[ (int) checkBoxes.TaskAuthorOthers ] = m_cbTaskAuthorOthers.Checked?'1':'0';

			return checkBoxSettings;
		}

		#endregion

	}

}
