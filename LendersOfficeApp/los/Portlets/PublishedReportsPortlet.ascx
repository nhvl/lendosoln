<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PublishedReportsPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.PublishedReportsPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<table cellspacing="0" cellpadding="0" border="0" class="Portlet">
<tr>
<td NoWrap class="PortletHeader">Published Reports</td>
</tr>
<tr>
<td>
    <div id="emptyReportsDiv" visible="false" runat="server">There are no published reports available.</div>
	<div >
		<asp:Repeater id="m_Published" runat="server" EnableViewState="True">
		<SeparatorTemplate>
			<div style="MARGIN-TOP: 2px;">
			</div>
		</SeparatorTemplate>
		<ItemTemplate>
			<div>
			  <img src="../images/bullet.gif" align="absMiddle">&nbsp;<a href="#" onclick="f_displayReport(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "QueryId").ToString()) %>);" class='PortletLink'>
			  <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "NamePublishedAs").ToString()) %>
			  </a>
			</div>
		</ItemTemplate>
		</asp:Repeater>
	</div>
</td>
</tr>
</table>
<script>
	function f_displayReport(id) {
		window.open(<%= AspxTools.SafeUrl(Tools.VRoot) %> + "/los/Reports/TmpReportResult.aspx" + "?" + "reportid" + "=" + id , "_blank" , "resizable=yes,status=no,menubar=yes,scrollbars=yes,width=780,height=500");
	}
</script>

