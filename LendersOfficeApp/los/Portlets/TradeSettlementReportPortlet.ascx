﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TradeSettlementReportPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.TradeSettlementReportPortlet" %>
<%@ Register TagPrefix="uc1" TagName="TradeTable" Src="~/los/MortgagePools/Controls/TradeTable.ascx" %>

<div id="TradeSettlementReportMain">
    <asp:PlaceHolder runat="server" ID="ControlsPanel">
        <div class="filters">
            <div class="column">
                <label>Settlement Dates</label>
            </div>
            <div class="column">
                <div class="label">
                    <label>From date:</label>
                </div>
                <div class="control">
                    <div class="exclusive">
                        <asp:RadioButton runat="server" ID="UseFromDDL" GroupName="From" />
                        <asp:DropDownList runat="server" ID="FromD_DDL" ></asp:DropDownList>
                    </div>
                    <div class="exclusive">
                        <asp:RadioButton runat="server" ID="UseFromTB" GroupName="From" />
                        <ml:DateTextBox runat="server" ID="FromD_TB" Width="160px" ></ml:DateTextBox>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="label">
                    <label>To date:</label>
                </div>
                <div class="control">
                    <div class="exclusive">
                        <asp:RadioButton runat="server" ID="UseToDDL" GroupName="To" />
                        <asp:DropDownList runat="server" ID="ToD_DDL"></asp:DropDownList>
                    </div>
                    <div class="exclusive">
                        <asp:RadioButton runat="server" ID="UseToTB" GroupName="To" />
                        <ml:DateTextBox runat="server" ID="ToD_TB" Width="160"></ml:DateTextBox>
                    </div>
                </div>    
            </div>
        </div>
        
        <div class="clear">
            <input type="button" runat="server" value="Refresh" onserverclick="RefreshClicked" />
            <input type="button" runat="server" value="Export to Excel (csv)..." onserverclick="ExportToCsvClicked" />
            <input type="button" runat="server" value="Export to Text..." onserverclick="ExportToTxtClicked" />
            <input type="button" value="Export to PDF..." class="export-pdf" />
            <input type="button" value="Print..." class="print" />
            <input type="hidden" runat="server" id="ReportTitle" class="report-title" />
            <input type="hidden" runat="server" id="LogoSrc" class="logo-src" />
            <input type="hidden" runat="server" id="BaseStyleSheet" class="base-style-sheet" />
            <input type="hidden" runat="server" id="TradeReportStyleSheet" class="trade-report-style-sheet" />
        </div>
    </asp:PlaceHolder>
    
    <uc1:TradeTable id="Trades" runat="server" HideDeleteLinks="true" DisplayExpanded="true" />
</div>

