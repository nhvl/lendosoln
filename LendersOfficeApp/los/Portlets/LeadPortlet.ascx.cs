namespace LendersOfficeApp.los.Portlets
{
    using System;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;

    public partial  class LeadPortlet : System.Web.UI.UserControl
	{
        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected bool IsNewPmlUIEnabled
        {
            get
            {
                if (this.BrokerUser.BrokerDB.IsNewPmlUIEnabled)
                {
                    return true;
                }
                else
                {
                    var employeeDB = EmployeeDB.RetrieveById(BrokerUser.BrokerId, BrokerUser.EmployeeId);
                    if (employeeDB.IsNewPmlUIEnabled)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        protected bool IsEditLeadsInFullLoanEditor
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerDB.IsEditLeadsInFullLoanEditor;
            }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowCreatingNewLeadFiles) || 
				 !BrokerUserPrincipal.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.MarketingTools))
            {
                this.Visible = false;
                return;
            }

            if (ConstStage.DisableUserInitatedLoanFileCreation)
            {
                Visible = false;
            }

            CreateHelocLeadPanel.Visible = this.BrokerUser.BrokerDB.IsEnableHELOC;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
