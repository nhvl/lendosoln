using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;

namespace LendersOfficeApp.los.Portlets
{

	public partial  class LoanPortlet : System.Web.UI.UserControl
	{
		protected BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal;  }
		}

        protected bool IsNewPmlUIEnabled
        {
            get
            {
                var user = this.BrokerUser;

                if (user.BrokerDB.IsNewPmlUIEnabled)
                {
                    return true;
                }
                else
                {
                    var employeeDB = EmployeeDB.RetrieveById(user.BrokerId, user.EmployeeId);
                    if (employeeDB.IsNewPmlUIEnabled)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            ((BaseServicePage)Page).RegisterService("loanTemplates", "/los/LoantemplateListService.aspx");
            var user = this.BrokerUser;

            this.Visible = user.HasPermission(Permission.AllowCreatingNewLoanFiles);

            if (ConstStage.DisableUserInitatedLoanFileCreation)
            {
                Visible = false;
            }

            CreateHelocLoanPanel.Visible = user.BrokerDB.IsEnableHELOC;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);
        }
		#endregion

	}
}
