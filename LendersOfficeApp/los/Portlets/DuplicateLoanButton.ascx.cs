using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;

namespace LendersOfficeApp.los.Portlets
{
	public partial  class DuplicateLoanButton : System.Web.UI.UserControl
	{
        protected System.Web.UI.HtmlControls.HtmlInputButton DuplicateLoanBtn;
    
		protected void PageLoad(object sender, System.EventArgs e)
		{
            if (ConstStage.DisableUserInitatedLoanFileCreation || PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanDuplicateLoans) == false )
            {
                this.Visible = false; 
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

	}
}
