namespace LendersOfficeApp.los.Portlets
{
    using System;
    using EDocs;
    using LendersOffice.ObjLib.UserDropbox;
    using LendersOffice.Security;

    /// <summary>
    ///	Summary description for SettingsPortlet.
    /// </summary>

    public partial  class SettingsPortlet : System.Web.UI.UserControl
	{
        private int docCount = 0;
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);

            var principal = BrokerUserPrincipal.CurrentPrincipal;

            Page.ClientScript.RegisterHiddenField("EmployeeResourcesUrl", principal.BrokerDB.EmployeeResourcesUrl);

            this.docCount = Dropbox.CountFilesInDropbox(principal.ConnectionInfo, principal.UserId);
            if (principal.HasPermission(Permission.AllowManagingFailedDocs))
            {
                this.docCount += LostEDocument.GetBarcodeDocumentCount(principal.ConnectionInfo, principal.BrokerId);
            }

            if (principal.BrokerDB.HideEdocDropboxInSettingsPortlet)
            {
                this.DropboxPlaceholder.Visible = this.docCount != 0;
            }

            this.Page.ClientScript.RegisterHiddenField("EnableLqbMobileApp", principal.BrokerDB.EnableLqbMobileApp.ToString());
        }
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
    
		}
		#endregion

        public int UploadedCount => this.docCount;
    }
}
