using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOfficeApp.los.admin;
using LendersOffice.Constants;

namespace LendersOfficeApp.los.Portlets
{

	public partial class AdminPortlet : System.Web.UI.UserControl
	{
        private BrokerDB x_brokerDB = null;

        private BrokerDB CurrentBroker
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = this.BrokerUser.BrokerDB;
                }
                return x_brokerDB;
            }
        }
		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		protected Boolean ShowCustomFieldChoices
		{
			get
			{
                if (CurrentBroker.HasLenderDefaultFeatures 
                    //&& BrokerUser.HasRole(E_RoleT.Administrator)
                    && BrokerUser.HasPermission(Permission.AllowViewingAndEditingCustomFieldChoices))
                {
                    return true;
                }
				return false;
			}
		}

		protected Boolean ShowConditionChoices
		{
			get
			{
                if (CurrentBroker.HasLenderDefaultFeatures
                    && BrokerUser.HasPermission(Permission.AllowViewingAndEditingConditionChoices)
                    //&& BrokerUser.HasRole(E_RoleT.Administrator) 
                    && CurrentBroker.IsUseNewTaskSystem == false)
                {
                    return true;
                }
				return false;
			}
		}

        protected bool ShowNewConditionChoices
        {
            get
            {
                
                if (CurrentBroker.HasLenderDefaultFeatures 
                    && BrokerUser.HasPermission(Permission.AllowViewingAndEditingConditionChoices)
                    //&& BrokerUser.HasRole(E_RoleT.Administrator) 
                    && CurrentBroker.IsUseNewTaskSystem)
                {
                    return true;
                }
                return false;
            }
        }

		protected Boolean ShowLeadSources
		{
			get
			{
                return BrokerUser.HasPermission(Permission.AllowViewingAndEditingLeadSources);
                //if (BrokerUser.HasRole(E_RoleT.Administrator))
                //{
                //    return true;
                //}
                //return false;
			}
		}

		// OPM 1438
		protected Boolean ShowRateLockPeriods
		{
			get
			{
				return false;
			}
		}

		// OPM 2251
		protected Boolean ShowTimezonePreferences
		{
			get
			{
				return false;
			}
		}

		protected Boolean ShowARMIndexEntries
		{
			get
			{
                return BrokerUser.HasPermission(Permission.AllowViewingAndEditingArmIndexEntries); 
                    //BrokerUser.HasRole(E_RoleT.Administrator);
			}
		}

		protected Boolean ShowContacts
		{
			get
			{
                return BrokerUser.HasPermission(Permission.AllowReadingFromRolodex);
			}
		}

        protected bool ShowCaptureAudit => this.BrokerUser.HasPermission(Permission.AllowAccessingCorporateAdminDocumentCaptureAuditPage);

        protected bool ShowCaptureReview => this.BrokerUser.HasPermission(Permission.AllowAccessingCorporateAdminDocumentCaptureReviewPage);

        protected Boolean ShowElectronicDocs
        {
            get
            {
                return CurrentBroker.IsEDocsEnabled 
                    && BrokerUser.HasPermission(Permission.AllowViewingAndEditingEdocsConfiguration);
                    //&& BrokerUser.HasPermission(Permission.CanViewEDocs) && BrokerUser.HasRole(E_RoleT.Administrator);
            }
        }

        protected Boolean ShowCustomForms
		{
			get
			{
                return BrokerUser.HasPermission(Permission.CanCreateCustomForms);
			}
		}

        protected bool ShowPrintGroups
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowViewingPrintGroups);
            }
        }

        protected bool ShowGeneralSettings
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowViewingAndEditingGeneralSettings);
            }
        }

        protected bool ShowBranchSettings
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowViewingAndEditingBranches);
            }
        }

        protected bool ShowTeamSettings
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowViewingAndEditingTeams);
            }
        }

        protected bool ShowEventNotification
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowViewingAndEditingEventNotifications);
            }
        }

		protected Boolean ShowAdminSettings
		{
			get { return BrokerUser.HasRole(E_RoleT.Administrator); }
		}

        protected Boolean ShowConsumerPortals
        {
            get
            {
                return CurrentBroker.IsEnableNewConsumerPortal && BrokerUser.HasPermission(Permission.AllowViewingAndEditingConsumerPortalConfigs);
            }
        }

        protected Boolean ShowBranchStatistics
        {
            get
            {
                return (BrokerUser.HasRole(E_RoleT.Manager) && BrokerUser.HasPermission(Permission.BrokerLevelAccess));
            }
        }

        protected Boolean ShowCustomFolderNavigation
        {
            get
            {
                return BrokerUser.HasPermission(Permission.AllowViewingAndEditingCustomNavigation);
            }
        }

		/// <summary>
		/// Initialize this user control.
		/// </summary>

		protected void PageInit( object sender , System.EventArgs a )
		{
            if (ShowConsumerPortals == false
            && ShowAdminSettings == false
            && ShowEventNotification == false
            && ShowTeamSettings == false
            && ShowBranchSettings == false
            && ShowGeneralSettings == false
            && ShowPrintGroups == false
            && ShowCustomForms == false
            && ShowElectronicDocs == false
            && ShowContacts == false
            && ShowARMIndexEntries == false
            && ShowTimezonePreferences == false
            && ShowRateLockPeriods == false
            && ShowLeadSources == false
            && ShowNewConditionChoices == false
            && ShowConditionChoices == false
            && ShowCustomFieldChoices == false
            && ShowBranchStatistics == false
            && this.ShowCustomFolderNavigation == false
			)
			{
				this.Visible = false;
				return;
			}

            CustomNavigationContainer.Visible = this.ShowCustomFolderNavigation;

			// Initialize this user control.
            TeamsContainer.Visible = CurrentBroker.EnableTeamsUI;
			try
			{
                Billing1.Visible = false;
                Billing3.Visible = false;
                if (CurrentBroker.BillingVersion == E_BrokerBillingVersion.Classic)
                {
                    Billing1.Visible = true;
                }
                else if (CurrentBroker.BillingVersion == E_BrokerBillingVersion.PerTransaction)
                {
                    Billing3.Visible = true;
                }
                
			}
			catch( Exception e )
			{
				Tools.LogError( "Failed to load broker." , e );
			}

            ((BasePage)Page).RegisterJsScript("LQBPopup.js");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Page.Init += new System.EventHandler( PageInit );
        }
		#endregion

	}

}
