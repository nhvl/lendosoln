﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.QueryProcessor;
using LendersOffice.Common;
using LendersOffice.Reports;
using LendersOffice.Security;
using DataAccess;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;

namespace LendersOfficeApp.los.Portlets
{
    public partial class DisclosurePortlet : System.Web.UI.UserControl, IAutoLoadUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var basepage = Page as BasePage;
            if (basepage == null)
            {
                throw new Exception("Cannot use the Disclosures Portal on something that is not a BasePage");
            }

            if (!this.Visible) return;
            
            basepage.EnableJqueryMigrate = false;
            basepage.RegisterCSS("DisclosurePortlet.css");
            basepage.RegisterJsScript("jquery.tablesorter.min.js");
            basepage.RegisterJsScript("disclosureportlet.js");

            this.AutomatedDisclosurePH.Visible = PrincipalFactory.CurrentPrincipal.BrokerDB
                .IsEnableDocuTechAutoDisclosure;
        }

        private void ControlPreRender(object sender, EventArgs e)
        {
            var reportRunner = new LoanReporting();
            var principal = PrincipalFactory.CurrentPrincipal;
            var report = reportRunner.DisclosurePipeline(principal, false);

            Rows.DataSource = report.Flatten();
            Rows.DataBind();
        }

        public void LoadData()
        {
            // Add the render handler to this page only on load.
            PreRender += new EventHandler(ControlPreRender);
        }

        public void SaveData()
        {
        }

        protected void RowsBound(object sender, RepeaterItemEventArgs args)
        {
            var row = args.Item.DataItem as Row;
            
            var loanId = args.Item.FindControl("LoanId") as HtmlInputHidden;
            loanId.Value = row.Key.ToString();

            var rowEntries = args.Item.FindControl("RowEntries") as Repeater;

            if (PrincipalFactory.CurrentPrincipal.BrokerDB.UseCustomCocFieldList)
            {
                // when custom coc field is enabled, copy the custom CoC description into the status column.
                if (row.Count == 8 && !string.IsNullOrEmpty(row.Items[7].Text))
                {
                    row.Items[5] = row.Items[7];
                }

                // remove the last item, which is the custom description.
                row.Items = row.Items.Take(row.Items.Count() - 1).ToArray();
            }

            rowEntries.DataSource = from Value val in row.Items select val.Text;
            rowEntries.DataBind();
        }
    }
}
