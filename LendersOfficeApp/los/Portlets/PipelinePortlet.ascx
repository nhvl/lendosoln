﻿<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PipelinePortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.PipelinePortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="uc" TagName="AssignLoanButton" Src="AssignLoanButton.ascx" %>
<%@ Register TagPrefix="uc" TagName="DuplicateLoanButton" Src="DuplicateLoanButton.ascx" %>
<%@ Register TagPrefix="uc" TagName="RefreshButton" Src="RefreshButton.ascx" %>
<%@ Register TagPrefix="uc" TagName="DeleteLoanButton" Src="DeleteLoanButton.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:HiddenField runat="server" ID="ShowCircumstances" />
<table width="100%" border="0" cellpadding="3" cellspacing="0">
<tr>
    <td align="left" nowrap style="padding:1;"  >
	    <uc:AssignLoanButton id="m_assignButton" runat="server">
	    </uc:AssignLoanButton>
	    <uc:DuplicateLoanButton id="m_copySelected" runat="server">
	    </uc:DuplicateLoanButton>
	    <uc:DeleteLoanButton id="m_deleteButton" runat="server">
	    </uc:DeleteLoanButton>
	    <uc:RefreshButton id="m_refreshPanel" runat="server">
	    </uc:RefreshButton>
	    <span style="color:red;font-weight:bold;padding-left:15px;padding-right:15px;" title="Please use our Search page to find more loans"><ml:EncodedLiteral ID="m_tooManyLabel" Runat="server" EnableViewState="False"></ml:EncodedLiteral></span>
    </td>
    <td align="center" nowrap style="padding:0;">
        <span style="FONT: bold 12px arial; COLOR: black;">
            Now showing:
        </span>
        &nbsp;
        <ml:EncodedLabel id="m_reportTitle" runat="server" style="MARGIN-RIGHT: 4px;" ForeColor="Black" Font-Bold="True" EnableViewState="False">
            Choose new pipeline report
        </ml:EncodedLabel>
        <a href="#" style="COLOR: tomato;" onclick="onChooseClick();">(change)</a>
        &nbsp;
        <a href="#" style="color: TOMATO;" onclick="onCloseBtnClick();" id="A1">close tab</a>
        <a href="#" style="color: TOMATO;" onclick="onCloseBtnClick();" id="A2">[X]</a>
    </td>
</tr>
</table>
<input id="m_sortOrder" runat="server" type="hidden">
<asp:Panel id="m_rowsPanel" runat="server" style="BORDER: 0px solid lightgray; WIDTH: 100%; PADDING: 0px;">
    <table cellpadding="2" cellspacing="1" border="0" width="100%">
    <asp:Repeater id="m_rowTables" runat="server" OnItemDataBound="BindReport" EnableViewState="True">
    <ItemTemplate>
        <tr class="GridHeader">
        <td width="32px">
        </td>
        <asp:Repeater id="m_headerTag" runat="server" OnItemDataBound="BindHeader" OnItemCommand="HeaderClick" EnableViewState="True">
        <ItemTemplate>
            <td id="Cell" runat="server">
                <asp:LinkButton id="Column" runat="server">
                </asp:LinkButton>
                <ml:EncodedLabel id="Desc" runat="server">
                    ▼
                </ml:EncodedLabel>
                <ml:EncodedLabel id="Asc" runat="server">
                    ▲
                </ml:EncodedLabel>
            </td>
        </ItemTemplate>
        </asp:Repeater>
        </tr>
        <asp:Repeater id="Rows" runat="server" OnItemDataBound="BindRows" EnableViewState="False">
        <ItemTemplate>
            <tr valign="top" class="<%# AspxTools.HtmlString(Container.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem") %> Loan_<%# AspxTools.HtmlString(Container.DataItem) %>"  data-loanid='<%# AspxTools.HtmlString(Container.DataItem) %>'>
            <td align="center">
                <asp:Panel id="CheckBox" runat="server">
                    <input id="loanid" name="loanIdCheckBox" class="loan-select" type="checkbox" value="<%# AspxTools.HtmlString(Container.DataItem) %>" title="Select this loan" onclick="onCheckBoxClick( this );">
                    <input id="isLead" type="checkbox" runat="server" style="display:none"/>
                </asp:Panel>
            </td>
            <td style="CURSOR: pointer;">
                <div onclick="onClickLoanLinkSet(this)" class="hover">
                    <u>
                        <ml:EncodedLabel id="Label" runat="server">
                        </ml:EncodedLabel>
                    </u>
                </div>
                <div class="Div0" style="display:none">
                    <asp:Panel id="EditLink" runat="server" class="pl0"  EnableViewState="False">
                        &#x2022;
                        <asp:Panel id="Show" runat="server" style="DISPLAY: inline;" EnableViewState="False">
                            <a href="javascript:editLoan(<%# AspxTools.JsString(Container.DataItem.ToString()) %>);" title="Edit loan" class="EditLoanLink">edit</a>
                        </asp:Panel>
                      <asp:HyperLink id="Hide" runat="server" Enabled="False" EnableViewState="False" style="cursor:default" title="You do not have permission to edit this loan.">edit  <a href='#' onclick="javascript:f_showReasonDetails( <%#AspxTools.JsString(Container.DataItem.ToString()) %>, event );" >?</a></asp:HyperLink>
                    </asp:Panel>
                    <asp:Panel id="LeadLink" runat="server" class="pl0" EnableViewState="False">
                      &#x2022;
                      <asp:Panel ID="LeadShow" Runat="server" EnableViewState="False" style="display:inline;">
                        <a href="javascript:editLead(<%# AspxTools.JsString(Container.DataItem.ToString()) %>);" title="Edit lead">edit</a>
                      </asp:Panel>
                      <asp:HyperLink id="LeadHide" Visible="false" runat="server" Enabled="False" EnableViewState="False" style="cursor:default" title="You do not have permission to edit this lead.">edit</asp:HyperLink>
                    </asp:Panel>
                    <asp:Panel id="ViewLink" runat="server" class="pl0"  EnableViewState="False">
              &#x2022; <a href="javascript:viewLoan(<%# AspxTools.JsString(Container.DataItem.ToString()) %>);" title="View loan summary">summary</a>
                    </asp:Panel>
                    <div nowrap class="pl0">
                        &#x2022; <a href="javascript:createTask(<%# AspxTools.JsString(Container.DataItem.ToString()) %>);" title="Insert new task for this loan">new task</a>
                    </div>
                    <asp:Panel ID="SandboxLink" runat="server" EnableViewState="false" class="p10" Visible=false>
                    &#x2022; <a href="#" onclick="return createSandbox(<%# AspxTools.JsString(Container.DataItem.ToString()) %>);" title="Create new sandbox file">create sandbox</a>
                    </asp:Panel>
                    <div nowrap id="extend_<%# AspxTools.HtmlString(Container.DataItem) %>" style="display:none" class="pl0">
                        &#x2022; <a href="javascript:rateLock(<%# AspxTools.JsString(Container.DataItem.ToString()) %>, 'extend' );" >extend lock</a>
                    </div>

                    <div nowrap id="floatdown_<%# AspxTools.HtmlString(Container.DataItem) %>" style="display:none" class="pl0">
                        &#x2022; <a href="javascript:rateLock(<%# AspxTools.JsString(Container.DataItem.ToString()) %>, 'floatdown' );" >float down lock</a>
                    </div>

                    <div nowrap id="relock_<%# AspxTools.HtmlString(Container.DataItem) %>" style="display:none" class="pl0">
                        &#x2022; <a href="javascript:rateLock(<%# AspxTools.JsString(Container.DataItem.ToString()) %>, 'relock' );" >rate re-lock</a>
                    </div>
                    <asp:Panel ID="EditNewUILink" runat="server" CssClass="p10" EnableViewState="false" Visible="false">
                        &#x2022;<a href="javascript:editLoanNewUI(<%# AspxTools.JsString(Container.DataItem.ToString()) %>);" title="Edit loan">edit in new UI (ALPHA)</a>
                    </asp:Panel>
                    <asp:Panel ID="StyleEditorLink" runat="server" CssClass="p10" EnableViewState="false" Visible="false">
                        &#x2022;<a href="javascript:viewStyleEditor();" title="Edit loan">View Style Editor</a>
                    </asp:Panel>
                    <asp:Repeater ID="GenericFrameworkVendorList" runat="server" EnableViewState="false">
                        <ItemTemplate>
                        <div nowrap style="display:none">
                            &#x2022; <a id="GenericFrameworkLaunchLink" class="GenericFrameworkVendor" runat="server" />
                        </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
            <asp:Repeater id="Row" runat="server" OnItemDataBound="BindRow" EnableViewState="False">
            <ItemTemplate>
                <td id="Cell" runat="server">
                    <ml:EncodedLiteral id="Value" runat="server">
                    </ml:EncodedLiteral>
                </td>
            </ItemTemplate>
            </asp:Repeater>
            </tr>
        </ItemTemplate>
        </asp:Repeater>
        <tr valign="top">
        <td>
        </td>
        <asp:Repeater id="Footer" runat="server" OnItemDataBound="BindFooter" EnableViewState="False">
        <ItemTemplate>
            <td align="right">
                <table cellpadding="0" cellspacing="0">
                <asp:Repeater id="Calcs" runat="server" OnItemDataBound="BindResult">
                <ItemTemplate>
                    <tr>
                    <td>
                        <ml:EncodedLabel id="What" runat="server" style="FONT-WEIGHT: bold; COLOR: steelblue;">
                        </ml:EncodedLabel>
                    </td>
                    <td width="8px">
                    </td>
                    <td align="right">
                        <ml:EncodedLabel id="Result" runat="server" style="FONT-WEIGHT: bold; COLOR: black;">
                        </ml:EncodedLabel>
                    </td>
                    </tr>
                </ItemTemplate>
                </asp:Repeater>
                </table>
            </td>
        </ItemTemplate>
        </asp:Repeater>
        </tr>
    </ItemTemplate>
    <SeparatorTemplate>
        <tr style="HEIGHT: 16px; BACKGROUND-COLOR: transparent;">
        <td>
        </td>
        </tr>
    </SeparatorTemplate>
    </asp:Repeater>
    </table>

    <table width="100%" border="0" cellpadding="3" cellspacing="0">
        <tr>
            <td align="center" nowrap>
                <span style="color:red;font-weight:bold;" title="Please use our Search page to find more loans"><ml:EncodedLiteral ID="m_tooManyLabelbottom" Runat="server" EnableViewState="False" /></span>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel id="m_isMissing" runat="server" style="COLOR: dimgray; PADDING: 160px; TEXT-ALIGN: center;" Visible="False" EnableViewState="False">
	The custom report you selected for your pipeline is no longer available.
	<br>
	Please <a href="#" style="COLOR: tomato;" onclick="onChooseClick();">choose</a>
	a new custom report.
</asp:Panel>
<asp:Panel id="m_emptyNote" runat="server" style="COLOR: dimgray; PADDING: 160px; TEXT-ALIGN: center;" Visible="False" EnableViewState="False">
	Nothing to show.  Your pipeline is empty.
</asp:Panel>
<asp:Panel id="m_errorNote" runat="server" style="COLOR: red; PADDING: 160px; TEXT-ALIGN: center;" Visible="True" EnableViewState="False">
	Nothing to show.  Unable to load pipeline at this time.
</asp:Panel>
<asp:Panel id="m_permissionNote" runat="server" style="COLOR: red; PADDING: 160px; TEXT-ALIGN: center;" Visible="False" EnableViewState="False">
	This pipeline view contains restricted fields that require permission to access. Please select another query.
</asp:Panel>
<div id="ReasonDetails" style="border-right: black 3px solid; padding-right: 5px;
    border-top: black 3px solid; display: none; padding-left: 5px; padding-bottom: 5px;
    border-left: black 3px solid; width: 330px; padding-top: 5px; border-bottom: black 3px solid;
    position: absolute; height: 50px; background-color: whitesmoke; color: #000 !important;">
    <table width="100%">
        <tr id="LoadingRow_rd" style="display:none" >
            <td style="color:#000; background-color: whitesmoke; ">
                Loading...
            </td>
        </tr>
        <tr id="DataRow_rd" >
            <td id="reasonDetails_t" style="color:#000; background-color: whitesmoke; ">
              You do not have permission to edit this loan.
            </td>
        </tr>
        <tr>
            <td align="center" style="color: Black;">
                [ <a href="#" onclick="f_closeReasonDetails();return false;">Close</a> ]
            </td>
        </tr>
    </table>
</div>

<style type="text/css">
    div.hover {
        color: blue;
    }

    div.hover:hover {
        color: orange;
        cursor: pointer;
    }

</style>

<script type="text/javascript">

	var gSelectionCount = 0;

function createSandbox(loanId) {

    if (confirm('Do you want to create sandbox file?') == false)
    {
    return false;
    }

  var args = {};
  args["LoanID"] = loanId;
  var result = gService.loanutils.call("CreateSandbox", args);
  if (!result.error) {
    if (result.value.Error == "False") {
      editLoan(result.value.NewLoanID, result.value.LoanName);
    } else {
      alert('Unable to create sandbox file.');
    }
  }
   return false;
}

  function _init()
	{
		disableButtons( true );

	}

  function disableButtons( bDisable )
  {
	  var ids = ["DuplicateLoanBtn", "AssignLoanBtn", "DeleteLoanBtn"];

    for (var i = 0; i < ids.length; i++) {
      if (document.getElementById(ids[i]) != null)
        document.getElementById(ids[i]).disabled = bDisable;
	}
  }

		function f_showReasonDetails(id, event)
		{
		    var circumstances = document.getElementById('<%= AspxTools.ClientId(ShowCircumstances) %>');

		    if( circumstances.value === 'True') {
		        document.getElementById('LoadingRow_rd').style.display = '';
		        document.getElementById('DataRow_rd').style.display = 'none';
		    }

			var msg = document.getElementById("ReasonDetails");
			msg.style.display = "block";
			msg.style.top = (document.body.scrollTop + event.clientY - 10)+ "px";
			msg.style.left = (event.clientX + 10 ) + "px";

			if( circumstances.value === 'True' ) {
			        window.setTimeout(function(){
			           var args = {
		                LoanId : id
		            };

		            var results = gService.loanutils.call("GetReasonsUserCannotEdit", args);
		            var data = JSON.parse(results.value.Result);
		            document.getElementById('reasonDetails_t').innerHTML =  createList(data);
		            document.getElementById('LoadingRow_rd').style.display = 'none';
		            document.getElementById('DataRow_rd').style.display = '';
                }, 0);
            }
			return false;
		}

		function f_closeReasonDetails()
		{
		    var fid = "ReasonDetails";
			document.getElementById(fid).style.display = "none";
		}
   function createList(data) {
        var builder = ['You do not have permission to edit this loan.'], i;

        if( data.length === 0 ) {
            return builder[0];
        }

        builder.push('The loan can be edited under any of the following circumstances. <ul>');
        for( i = 0; i < data.length; i++) {
            builder.push('<li>');
            builder.push(data[i]);
            builder.push('</li>');
        }
        builder.push('</ul>');
        return builder.join('');

   }
  function onCheckBoxClick( cBox )
  {

		if( cBox.checked )
		{
			gSelectionCount++;
		}
		else
		{
			gSelectionCount--;
		}

		highlightRowByCheckbox( cBox );

		disableButtons(gSelectionCount == 0);
  }

	function onChooseClick()
	{
		showModal( "/los/Portlets/ChoosePipeline.aspx", null, null, null, onChooseClickCallback, { hideCloseButton: true} );
	}

	function onChooseClickCallback(args)
	{
	    if( args != null && args.OK == true )
	    {
	        document.getElementById("<%= AspxTools.ClientId(m_sortOrder) %>").value = "";

	        if( document.getElementById("RefreshBtn") != null )
	        {
	            document.getElementById("RefreshBtn").click();
	        }
	        else
	        {
	            self.location = self.location;
	        }
	    }
    }

    function onClickLoanLinkSet(link) {

        var row = $j(link).closest('tr');
        var links = row.find('div.Div0');

        if (links.is(':visible'))
        {
            links.hide();
            row.css('font-weight', 'normal');
            return;
        }

        row.css('font-weight', 'bold');
        links.css('font-weight', 'normal');
        links.show();

        var editLoanHref = row.find('a.EditLoanLink');
        var loanId = row.attr('data-loanid');

        if(editLoanHref.length == 0) {
            return; // don't even attempt to get the lock links for leads
        }

        setTimeout(function() {

            var args = {
                loanId : loanId
            };

            var result = gService.loanutils.call("GetRateLockStatus", args);

            if (result.value['extend'] == 'True' ) {
                var link = document.getElementById('extend_' + loanId);
                if ( link != null) {
                    link.style.display='inline';
                }
            }

            if (result.value['floatdown'] == 'True' ) {
                var link = document.getElementById('floatdown_' + loanId);
                if ( link != null) {
                    link.style.display='inline';
                }
            }

            if (result.value['relock'] == 'True' ) {
                var link = document.getElementById('relock_' + loanId);
                if ( link != null) {
                    link.style.display='inline';
                }
            }

            updateGenericFrameworkPermissions(loanId);
        }, 0);
    }


    function updateGenericFrameworkPermissions(loanID)
    {
        var providerIDs = [];
        var $genericFrameworkVendors = $j('tr.Loan_' + loanID + ' .GenericFrameworkVendor');
        $genericFrameworkVendors.each(function()
        {
            providerIDs.push(this.getAttribute('data-providerid'));
            this.parentNode.style.display = 'none';
        });
        if (providerIDs.length > 0)
        {
            var args =
            {
                LoanID : loanID,
                ProviderIDs : JSON.stringify(providerIDs)
            };
            var genericFrameworkResult = gService.loanutils.call("GetGenericFrameworkVendors", args);
            if (!genericFrameworkResult.error && genericFrameworkResult.value) {
                var providerIDMapping = JSON.parse(genericFrameworkResult.value.ProviderIDPermissions);
                for (var i = 0; i < providerIDMapping.length; ++i) {
                    $genericFrameworkVendors.filter('[data-providerid="' + providerIDMapping[i].Key + '"]').each(function() {if (providerIDMapping[i].Value){this.parentNode.style.display = 'inline';}});
                }
            }
        }
    }

    function launchGenericFramework(providerID, sLId)
    {
      var args =
      {
        sLId: sLId,
        ProviderID: providerID
      };

      var result = gService.loanutils.call("LoadGenericFrameworkVendor", args);
      if (result.error) {
        alert(result.UserMessage);
      } else if (result.value["HasError"] === 'True') {
        alert(result.value["ErrorMessage"]);
      } else {
        var url = result.value["PopupURL"];
        var height = result.value["PopupHeight"];
        var width = result.value["PopupWidth"];
        var isModal = result.value["PopupIsModal"] === 'True';
          if (isModal) {
            showModal(url, null, 'dialogHeight:' + height + 'px; dialogWidth:' + width + 'px; center: yes; resizable: yes;');
        } else {
            window.open(url, '_blank', 'height=' + height + ', width=' + width + ', resizable=yes, scrollbars=yes');
        }
      }
    }
</script>
