﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.CapitalMarkets;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Portlets
{
    public partial class OpenTradesPortlet : System.Web.UI.UserControl
    {
        #region Properties
        private Guid BrokerID
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
        }

        private bool RenderForPdf
        {
            get { return RequestHelper.GetBool("toPdf"); }
        }

        private string BaseUrl
        {
            get { return Request.Url.Scheme + "://" + Request.Url.Authority + '/'; }
        }

        private string LogoSrcUrl
        {
            get
            {
                var pmlSiteId = BrokerDB.RetrieveById(BrokerID).PmlSiteID;
                return TradeReportUtilities.HasLogo(pmlSiteId) ? ResolveUrl("~/LogoPL.aspx?id=" + pmlSiteId.ToString()) : "";
            }
        }

        private const string REPORT_TITLE = "Open Trades";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            var basepage = Page as BasePage;
            if (basepage == null)
            {
                throw new Exception("Cannot use the Trade Portal on something that is not a BasePage");
            }

            if (!this.Visible) return;
            basepage.EnableJqueryMigrate = false;
            basepage.RegisterJsScript("OpenTradesPortlet.js");
            basepage.RegisterJsScript("utilities.js");

            ReportTitle.Value = REPORT_TITLE;
            LogoSrc.Value = LogoSrcUrl;
            BaseStyleSheet.Value = ResolveUrl("~/css/TradeReport.css");
            TradeReportStyleSheet.Value = ResolveUrl("~/css/stylesheetnew.css");

            if (RenderForPdf)
            {
                ControlsPanel.Visible = false;
            }

            AssignData();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (RenderForPdf)
            {
                var sw = new StringWriter();
                var htmlWriter = new HtmlTextWriter(sw);
                base.Render(htmlWriter);
                var stylesheets = new List<string>()
                {
                    this.ResolveUrl("~/css/TradeReport.css"),
                    this.ResolveUrl("~/css/stylesheetnew.css")
                };
                var wrappedHtml = TradeReportUtilities.GenerateHtmlForPdf(REPORT_TITLE, LogoSrcUrl, sw.ToString(), stylesheets, false);
                var pdf = TradeReportUtilities.GetPdfFromHtml(BaseUrl, wrappedHtml);
                var fileName = "OpenTrades.pdf";

                Response.Clear();
                Response.ClearHeaders();
                Response.Filter = null;
                pdf.Save(Response, false, fileName);
                pdf.Close(); // Need to clean up resource.
                Response.Flush();
                Response.End();
            }
            else
            {
                base.Render(writer);
            }
        }
        
        private IEnumerable<TradeGroup> GetDataSource()
        {
            var f = new OpenTradesPortletFilter(this);
            var groups = Trade.GetTradesByBroker(BrokerID)
                .Filter(f)
                .GroupBy(t => t.DealerInvestor);
            var tradeGroups = groups.Select(g => new TradeGroup(g.Key, g.ToList()))
                .OrderBy(tg => tg.Header);

            return tradeGroups;
        }

        private void AssignData()
        {
            Trades.DataSource = GetDataSource();
            Trades.DataBind();
        }

        #region Click Handling
        protected void RefreshClicked(object sender, EventArgs e)
        {
            AssignData();
        }

        protected void ExportToCsvClicked(object sender, EventArgs e)
        {
            var tradeGroups = GetDataSource();
            var exporter = new TradeExporter(REPORT_TITLE, tradeGroups, false);
            var csv = exporter.ExportToCsv();

            ExportFile("OpenTrades.csv", csv);
        }

        protected void ExportToTxtClicked(object sender, EventArgs e)
        {
            var tradeGroups = GetDataSource();
            var exporter = new TradeExporter(REPORT_TITLE, tradeGroups, false);
            var txt = exporter.ExportToTxt();

            ExportFile("OpenTrades.txt", txt);
        }

        private void ExportFile(string fileName, string fileContent)
        {
            Response.ClearHeaders();
            Response.Filter = null; // Need to remove any compression filtering, otherwise it comes out garbled.
            Response.AppendHeader("Content-Type", "text/plain");
            Response.AppendHeader("Content-Disposition", $"attachment; filename=\"{fileName}\"");
            Response.Output.Write(fileContent);
            Response.Flush();
            Response.End();
        }
        #endregion

        private class OpenTradesPortletFilter : TradeFilter
        {
            private OpenTradesPortlet Source;
            public OpenTradesPortletFilter(OpenTradesPortlet source)
            {
                Source = source;
            }

            public override Guid BrokerId
            {
                get { return Source.BrokerID; }
            }

            public override TradeStatus? Status
            {
                get { return TradeStatus.Open; }
            }

            #region Unused Filter Elements
            public override string TradeNum
            {
                get { return null; }
            }

            public override bool TradeNumPartialMatch
            {
                get { return false; }
            }

            public override E_TradeType? Type
            {
                get { return null; }
            }

            public override E_TradeDirection? Direction
            {
                get { return null; }
            }

            public override int? Month
            {
                get { return null; }
            }

            public override Guid? DescriptionId
            {
                get { return null; }
            }

            public override string Coupon
            {
                get { return null; }
            }

            public override bool FilterTradesInPools
            {
                get { return false; }
            }

            public override DateTime? SettlementDateLowerBound
            {
                get { return null; }
            }

            public override DateTime? SettlementDateUpperBound
            {
                get { return null; }
            }
            #endregion Unused Filter Elements
        }

    }
}
