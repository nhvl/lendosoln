using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.QueryProcessor;
using LendersOffice.Reports;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOfficeApp.los.admin;
using ConfigSystem.DataAccess;
using LendersOffice.ConfigSystem;
using System.Collections.Generic;
using LendersOffice.AntiXss;
using CommonLib;
using LendersOffice.Integration.GenericFramework;
using LendersOffice.LoanSearch;

namespace LendersOfficeApp.los.Portlets
{
	/// <summary>
	/// Summary description for PipelinePortlet.
	/// 
	/// 4/1/2005 kb - Implemented as part of query driven pipeline
	/// task: case #932.
	/// </summary>

	public partial  class TestPortlet : System.Web.UI.UserControl , IAutoLoadUserControl
	{
		protected LendersOfficeApp.los.Portlets.AssignLoanButton    m_assignButton;
		protected LendersOfficeApp.los.Portlets.DuplicateLoanButton m_copySelected;
		protected LendersOfficeApp.los.Portlets.DeleteLoanButton    m_deleteButton;
		protected LendersOfficeApp.los.Portlets.RefreshButton       m_refreshPanel;

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{
			set { Page.ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." ); }
		}


		/// <summary>
		/// Initialize this control.
		/// </summary>
		protected void PageLoad( object sender , System.EventArgs a )
		{
            ShowCircumstances.Value = bool.FalseString;

			// Check visibility for this control.
			try
			{
				// 10/8/2004 kb - We need to prevent lender account execs
				// from deleting and duplicating.

                // 12/10/2013 gf - opm 145015 New roles should have access to assign/duplicate/delete.

				bool showIt = false;
                if (BrokerUser.HasAtLeastOneRole(new E_RoleT[] { 
                    E_RoleT.Manager, E_RoleT.Accountant, E_RoleT.Processor,
                    E_RoleT.LenderAccountExecutive, E_RoleT.LoanOfficer, 
                    E_RoleT.RealEstateAgent, E_RoleT.LoanOpener,
                    E_RoleT.LockDesk, E_RoleT.Underwriter, E_RoleT.Closer, 
                    E_RoleT.CreditAuditor, E_RoleT.DisclosureDesk, 
                    E_RoleT.JuniorProcessor, E_RoleT.JuniorUnderwriter,
                    E_RoleT.LegalAuditor, E_RoleT.LoanOfficerAssistant,
                    E_RoleT.Purchaser, E_RoleT.QCCompliance, E_RoleT.Secondary, 
                    E_RoleT.Servicing}))
                {
                    showIt = true;
                }
				
				m_assignButton.Visible = showIt;
				m_copySelected.Visible = showIt;
				m_deleteButton.Visible = showIt;
            }
			catch( CBaseException e )
			{
				// Oops!

				if( e.UserMessage != "" )
				{
					Tools.LogError( ErrorMessage = "Failed to load web form: " + e.UserMessage , e );
				}
				else
				{
					Tools.LogError( ErrorMessage = "Failed to load web form" , e );
				}
			}
		}

		/// <summary>
		/// Render this control.
		/// </summary>
		private void PagePreRender( object sender , System.EventArgs a )
		{
			// Load the current report.  We want latest every time the
			// user posts back.
            try
            {
                LendingQBSearchFilter searchFilter = new LendingQBSearchFilter();
                searchFilter.IsTest = true;
                searchFilter.sStatusT = -1;
                searchFilter.sLT = -1;

                int numberOfLoans;
                DataSet dS = LendingQBSearch.Search(BrokerUserPrincipal.CurrentPrincipal, searchFilter, "sLNm", 0, LendersOffice.Constants.ConstAppDavid.MaxViewableRecordsInPipeline, out  numberOfLoans);

                if (numberOfLoans > 0)
                {
                    try
                    {
                        m_TestGrid.DataSource = dS.Tables[0].DefaultView;
                        m_TestGrid.DataBind();

                    }
                    catch (CBaseException)
                    {
                        // Bust!
                        m_emptyNote.Visible = false;
                        m_rowsPanel.Visible = false;

                        m_errorNote.Visible = true;
                        throw;
                    }

                    // Display found loans.
                    m_emptyNote.Visible = false;
                    m_errorNote.Visible = false;

                    m_rowsPanel.Visible = true;
                }
                else
                {
                    // DataSet is empty.
                    m_errorNote.Visible = false;
                    m_rowsPanel.Visible = false;

                    m_emptyNote.Visible = true;
                }
            }
            catch (PermissionException e)
            {
                Tools.LogError("User tried to run pipeline that contains restricted fields. Error Message: " + e.UserMessage, e);
            }
            catch (CBaseException e)
            {
                // Oops!

                if (e.UserMessage != "")
                {
                    Tools.LogError(ErrorMessage = "Failed to render web form: " + e.UserMessage, e);
                }
                else
                {
                    Tools.LogError(ErrorMessage = "Failed to render web form", e);
                }
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

		/// <summary>
		/// Bind the report element.
		/// </summary>
        Guid _curr_loan_id = Guid.Empty;
		protected void BindRows( object sender, DataGridItemEventArgs e )
		{
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dr = e.Item.DataItem as DataRowView;
            Guid loanID = (Guid) dr["sLId"];

            if (dr != null)
			{
                Label sLNm = e.Item.FindControl("sLNm") as Label;

                if (sLNm != null)
				{
					// Get the access control lookup results.  If not found,
					// then we assume no access is provided.  The row should
					// be removed from the set (hidden).
					//
					// 7/14/2005 kb - We now put the access control info in
					// each row.
                    sLNm.Text = (string)dr["sLNm"];

                    bool canRead = (int) dr["CanRead"] == 1;
                    bool canEdit = (int) dr["CanWrite"] == 1;

                    bool isLead = false;
                    switch( ( E_sStatusT ) dr["sStatusT"] )
					{
						case E_sStatusT.Lead_New:
						case E_sStatusT.Lead_Other:
						case E_sStatusT.Lead_Declined:
						case E_sStatusT.Lead_Canceled:
						{
							isLead = true;
						}
						break;
					}

                    Panel aPanel;

                    aPanel = e.Item.FindControl("CheckBox") as Panel;

                    if (aPanel != null)
					{
						// Decide whether we want to be able to select the given loan.

						if( canEdit == false )
						{
                            aPanel.Visible = false;
						}
						else
						{
                            aPanel.Visible = true;
						}
					}

					aPanel = e.Item.FindControl( "ViewLink" ) as Panel;

					if( aPanel != null )
					{
						// Set visibility for viewing the loan summary.
                        aPanel.Visible = canRead;
					}

					aPanel = e.Item.FindControl( "EditLink" ) as Panel;

					if( aPanel != null )
					{
						// Set visibility for editing the loan.
                        aPanel.Visible = !isLead; //Make load link always visible if not lead as there can be field level write privilege. So edit link is not necessary
                        WebControl cHide = aPanel.FindControl("Hide") as WebControl;
                        cHide.Visible = false;
					}

					aPanel = e.Item.FindControl( "LeadLink" ) as Panel;
					if( aPanel != null )
					{
                        aPanel.Visible = isLead;						
					}

                    var genericFrameworkVendorList = e.Item.FindControl("GenericFrameworkVendorList") as Repeater;
                    if (genericFrameworkVendorList != null)
                    {
                        genericFrameworkVendorList.DataSource = this.GenericFrameworkVendors;
                        genericFrameworkVendorList.ItemDataBound += new RepeaterItemEventHandler((sender2, a2) => GenericFrameworkVendorList_OnItemDataBound(sender2, a2, loanID));
                        genericFrameworkVendorList.DataBind();
                    }
				}

                Label sRateLockStatusT = e.Item.FindControl("sRateLockStatusT") as Label;
                if (sRateLockStatusT != null)
                {
                    sRateLockStatusT.Text = CPageBase.sRateLockStatusT_map_rep( (E_sRateLockStatusT) dr["sRateLockStatusT"] );
                }
			}
		}

        private IEnumerable<GenericFrameworkVendor> genericFrameworkVendors;
        private IEnumerable<GenericFrameworkVendor> GenericFrameworkVendors
        {
            get
            {
                if (this.genericFrameworkVendors == null)
                {
                    this.genericFrameworkVendors = GenericFrameworkVendor.LoadVendors(this.BrokerUser.BrokerId).Where(vendor => vendor.LaunchLinkConfig.IsDisplayLinkForUser(LinkLocation.LQBPipeline));
                }

                return this.genericFrameworkVendors;
            }
        }

        protected void GenericFrameworkVendorList_OnItemDataBound(object sender, RepeaterItemEventArgs a, Guid loanID)
        {
            var vendor = (GenericFrameworkVendor)a.Item.DataItem;
            var link = a.Item.FindControl("GenericFrameworkLaunchLink") as HtmlAnchor;
            if (link != null)
            {
                link.InnerText = vendor.LaunchLinkConfig.LinkDisplayName(LinkLocation.LQBPipeline);
                link.Attributes.Add("data-providerid", vendor.ProviderID);
                link.HRef = string.Format("javascript:launchGenericFramework({0}, {1});", AspxTools.JsString(vendor.ProviderID), AspxTools.JsString(loanID));
            }
        }

		/// <summary>
		/// Trigger rendering the current pipeline.
		/// </summary>

		public void LoadData()
		{
			// Add the render handler to this page only on load.

			PreRender += new EventHandler( PagePreRender );
		}

		/// <summary>
		/// Save something.  Nothing to do.
		/// </summary>

		public void SaveData()
		{
		}

	}

}
