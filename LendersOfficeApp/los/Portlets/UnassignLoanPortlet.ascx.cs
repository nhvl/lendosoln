namespace LendersOfficeApp.los.Portlets
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.LoanSearch;
    using LendersOffice.Security;

    public partial class UnassignLoanPortlet : UserControl, IAutoLoadUserControl
	{

		private Hashtable m_accessBinding = new Hashtable();

		private BrokerUserPrincipal CurrentUser
		{
			// Get the current user principal.
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion

        private void BindDataGrid()
        {
            DataSet ds = LendingQBSearch.ListUnassignedLoans(CurrentUser);

            m_unassignLoansDG.DataSource = ds.Tables[0].DefaultView;
            m_unassignLoansDG.DataBind();

            //7/22/08 jk. Load sort order from the cookie, and then afterward save the new sort order back to the cookie
            HttpCookie cookie = Request.Cookies["newLoan_sortOrder"];
            if (cookie != null && cookie.Value != null && cookie.Value != "")
            {
                m_unassignLoansDG.DefaultSortExpression = cookie.Value;
            }
            else
            {
                m_unassignLoansDG.DefaultSortExpression = "aBLastNm ASC";
            }
            LendersOffice.Common.RequestHelper.StoreToCookie("newLoan_sortOrder", ds.Tables[0].DefaultView.Sort, false);
        }

		protected void LoanDataBound( object sender , DataGridItemEventArgs a )
		{
			// 11/4/2004 kb - Find the conditional controls and hide or
			// show depending on what the access privelages are.
			//
			// If you want to modify what is shown and when, then put the
			// logic for each loan row here...

			if( a.Item.DataItem != null )
			{
				Panel aPanel; 
                
                //AccessBinding aB = m_accessBinding[ DataBinder.Eval( a.Item.DataItem , "sLId" ) ] as AccessBinding;
                bool canWrite = (int) DataBinder.Eval(a.Item.DataItem, "CanWrite") == 1;
                bool canRead = (int)DataBinder.Eval(a.Item.DataItem, "CanRead") == 1;

				aPanel = a.Item.FindControl( "SelectBox" ) as Panel;

				if( aPanel != null )
				{
					if( canWrite == false )
					{
						aPanel.Visible = false;
					}
					else
					{
						aPanel.Visible = true;
					}
				}

				aPanel = a.Item.FindControl( "LoadLink" ) as Panel;

				if( aPanel != null )
				{
					if( CurrentUser.HasPermission( Permission.CanViewLoanInEditor ) == false || canWrite == true || canRead == false )
					{
						aPanel.Visible = false;
					}
					else
					{
						aPanel.Visible = true;
					}
				}

				aPanel = a.Item.FindControl( "ViewLink" ) as Panel;

				if( aPanel != null )
				{
					if( CurrentUser.HasPermission( Permission.CanViewLoanInEditor ) == true && canWrite == false || canRead == false )
					{
						aPanel.Visible = false;
					}
					else
					{
						aPanel.Visible = true;
					}
				}

				aPanel = a.Item.FindControl( "EditLink" ) as Panel;

				if( aPanel != null )
				{
					if( canWrite == false )
					{
						aPanel.Visible = false;
					}
					else
					{
						aPanel.Visible = true;
					}
				}
			}
		}

        public void LoadData() 
        {
            if (! BrokerUserPrincipal.CurrentPrincipal.HasRole(E_RoleT.Manager)) 
            {
                this.Visible = false;
                return;
            }

			BindDataGrid();

            if (m_unassignLoansDG.Items.Count == 0) 
            {
                m_countLabel.Text = "No open loans without loan officer.";
            }
            else if (m_unassignLoansDG.Items.Count < 2) 
            {
                m_countLabel.Text = "Showing: 1 open loan without loan officer.";
            } 
            else if (m_unassignLoansDG.Items.Count < ConstAppDavid.MaxViewableRecordsInPipeline)
            {
                m_countLabel.Text = "Showing: " + m_unassignLoansDG.Items.Count + " open loans without loan officer.";
            } 
            else 
            {
                m_countLabel.Text = "Too many open loans without loan officer -- displaying first " + ConstAppDavid.MaxViewableRecordsInPipeline + ".";
				m_countLabel.ToolTip = "Please use our Search page to find more loans";
            }
        }

		public void SaveData() 
        {
        }
	}
}
