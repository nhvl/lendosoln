<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="RefreshButton" Src="RefreshButton.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Reminders" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MyLoanRemindersPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.MyLoanRemindersPortlet" %>

<STYLE>
	.MyLRFilterLabel { FONT: 11px arial; WIDTH: 90px; COLOR: black }

	.MyLRFilterCheck { FONT: 11px arial; WIDTH: 84px; COLOR: black; MARGIN-RIGHT: 8px }
</STYLE>

<SCRIPT>
	function _init()
	{
		if (<%= AspxTools.JsGetElementById(m_UIState) %>.value.indexOf("Filter=Hide") == -1)
		{
			document.getElementById('changeFilterBtn').disabled = true;
		}

	    onToggleMyLRFilter( <%= AspxTools.ClientId(m_panelTaskFilter) %> , <%= AspxTools.ClientId(m_UIState) %> );
        onToggleMyLRFilter( <%= AspxTools.ClientId(m_panelTaskFilter) %> , <%= AspxTools.ClientId(m_UIState) %> );
	}

	function onToggleMyLRFilter( oPanel, oStore ) { try
	{
		if( oPanel == null || oStore == null )
		{
			return;
		}

		if( oStore.value.indexOf( "Filter=Hide" ) == -1 )
		{
			oPanel.style.display = "none";
			oStore.value = "Filter=Hide";
			document.getElementById('changeFilterBtn').disabled = false;
		}
		else
		{
			oPanel.style.display = "block";
			oStore.value = "Filter=Show";
			document.getElementById('changeFilterBtn').disabled = true;
		}
	}
	catch( e )
	{
	}}

	<%-- 01/25/06 MF. OPM 3899. Allow user to (de)select all --%>
	function setCheckBoxes(status){
		try{
			var filterPanel = <%= AspxTools.JsGetElementById(m_panelTaskFilter) %>;
			if(filterPanel){
				var oInputElements = filterPanel.getElementsByTagName('input');
				for (var i = 0; i < oInputElements.length; i++){
					var inputElement = oInputElements[i];
					if (inputElement.type === 'checkbox'){
						inputElement.checked = status;
					}
				}
			}
		} catch(e){
		}
	}

	    function onClickLoanLinkSet( oBase , oTarget ) { try
		{
        // Check for valid targets.

        if( oBase == null || oTarget == null )
        {
            return;
        }
			// Show link set and display loan in bold
            if( oTarget.nextSibling.style.display == "none" )
            {
                // Turn off all previously activated link sets within
                // this grid and then turn on the selected one.
                oTarget.nextSibling.style.display    = "block";
                oTarget.nextSibling.style.fontWeight = "normal";
                oTarget.style.fontWeight = "bold";
            }
            else
            {
                // Turn off the selected one and leave the
                // other link sets alone.

                oTarget.nextSibling.style.display = "none";
				oTarget.style.fontWeight = "normal";
            }
        }
    catch( e )
    {
        window.status = "Selecting " + oTarget.innerText + " failed.";
    }}
</SCRIPT>

<INPUT type="hidden" id="m_UIState" runat="server" NAME="m_UIState">
<TABLE cellspacing="0" cellpadding="3" width="100%" border="0">
<TR valign="top">
<TD align="left">
	<UC1:RefreshButton runat="server" id="RefreshButton1"/>
	<INPUT type="button" id="changeFilterBtn" onfocus="this.blur();" onclick="onToggleMyLRFilter( <%= AspxTools.ClientId(m_panelTaskFilter) %> , <%= AspxTools.ClientId(m_UIState) %> );" value="Show Filter Settings">
	<span style="color:red;font-weight:bold;padding-left:15px"><ml:EncodedLiteral ID="m_tooManyLabel" Runat="server" EnableViewState="False"/></span>
</TD>
</TR>
</TABLE>

<ASP:Panel id="m_panelTaskFilter" style="BORDER-RIGHT: 1px inset; PADDING-RIGHT: 6px; BORDER-TOP: 1px inset; DISPLAY: none; PADDING-LEFT: 6px; BACKGROUND: whitesmoke; PADDING-BOTTOM: 6px; MARGIN: 12px 20px 20px; BORDER-LEFT: 1px inset; PADDING-TOP: 6px; BORDER-BOTTOM: 1px inset; TEXT-ALIGN: left" runat="server" Width="100%">
<TABLE cellSpacing=0 cellPadding=0 border=0>
	<tr>
		<td colspan="2" style="FONT-SIZE: 10px; COLOR: black">
			<a href="#" onclick="setCheckBoxes(true)">Check All</a> /
			<a href="#" onclick="setCheckBoxes(false)">Uncheck All</a>
		</td>
	</tr>
  	<TR>
		<TD class=MyLRFilterLabel>Task status: </TD>
		<TD><ASP:CheckBox class=MyLRFilterCheck id=m_cbTaskStatusActive runat="server" Text="Active" Checked="True">
			</ASP:CheckBox><ASP:CheckBox class=MyLRFilterCheck id=m_cbTaskStatusDone runat="server" Text="Done" Checked="True">
			</ASP:CheckBox><ASP:CheckBox class=MyLRFilterCheck id=m_cbTaskStatusSuspended runat="server" Text="Suspended" Checked="True">
			</ASP:CheckBox><ASP:CheckBox class=MyLRFilterCheck id=m_cbTaskStatusCancelled runat="server" Text="Cancelled" Checked="True">
			</ASP:CheckBox></TD></TR>
  	<TR>
		<TD class=MyLRFilterLabel>Participation: </TD>
		<TD><ASP:CheckBox class=MyLRFilterCheck id=m_cbParticipationActive runat="server" Text="Active" Checked="True">
			</ASP:CheckBox><ASP:CheckBox class=MyLRFilterCheck id=m_cbParticipationInactive runat="server" Text="Inactive" Checked="True">
			</ASP:CheckBox><ASP:CheckBox class=MyLRFilterCheck id=m_cbParticipationOther runat="server" Text="Other" Checked="True">
			</ASP:CheckBox></TD>
	</TR>
  	<TR>
		<TD class=MyLRFilterLabel>Priority: </TD>
		<TD><ASP:CheckBox class=MyLRFilterCheck id=m_cbPriorityLow runat="server" Text="Low" Checked="True">
			</ASP:CheckBox><ASP:CheckBox class=MyLRFilterCheck id=m_cbPriorityNormal runat="server" Text="Normal" Checked="True">
			</ASP:CheckBox><ASP:CheckBox class=MyLRFilterCheck id=m_cbPriorityHigh runat="server" Text="High" Checked="True">
			</ASP:CheckBox></TD>
	</TR>
	<TR>
		<TD valign="top" class=MyLRFilterLabel>Loan status: </TD>
		<TD><ASP:CheckBox id=m_cbLoanStatusOpen runat="server" Text="Open" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusPreQual runat="server" Text="Pre-qual" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusPreApprove runat="server" Text="Pre-approve" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusSubmitted runat="server" Text="Submitted" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusApproved runat="server" Text="Approved" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusDocs runat="server" Text="Docs" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusFunded runat="server" Text="Funded" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusOnHold runat="server" Text="On Hold" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusUnderwritten runat="server" Text="Underwritten" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusRecorded runat="server" Text="Recorded" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusSuspended runat="server" Text="Suspended" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusRejected runat="server" Text="Denied" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusCancelled runat="server" Text="Cancelled" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusClosed runat="server" Text="Closed" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox><ASP:CheckBox id=m_cbLoanStatusOther runat="server" Text="Other" Checked="True" CssClass="MyLRFilterCheck">
			</ASP:CheckBox></TD>
	</TR>
	<TR>
		<TD class=MyLRFilterLabel>Task author: </TD>
		<TD><ASP:CheckBox class=MyLRFilterCheck id=m_cbTaskAuthorMine runat="server" Text="Me" Checked="True">
			</ASP:CheckBox><ASP:CheckBox class=MyLRFilterCheck id=m_cbTaskAuthorOthers runat="server" Text="Other" Checked="True">
			</ASP:CheckBox></TD>
	</TR>
	<TR>
		<TD></TD>
		<TD>
			<asp:Button Text=" Preview " Runat="server"/>&nbsp;
			<asp:Button onclick=SaveFilterClick Text=" Save " Runat="server"/>&nbsp;
			<INPUT id=hideBtn onclick="onToggleMyLRFilter( <%= AspxTools.ClientId(m_panelTaskFilter) %> , <%= AspxTools.ClientId(m_UIState) %> );" type=button value=" Hide ">
		</TD>
	</TR>
</TABLE>
</ASP:Panel>
<ml:commondatagrid id="m_remindersDG" AutoGenerateColumns="False" CssClass="DataGrid" EnableViewState="False" AllowSorting="True" Width="100%" DataKeyField="DiscLogId" runat="server">
	<AlternatingItemStyle CssClass="GridAlternatingItem"/>
	<ItemStyle CssClass="GridItem"/>
	<HeaderStyle CssClass="GridHeader"/>
	<Columns>
		<asp:TemplateColumn SortExpression="NotifIsValid">
			<ItemTemplate>
                <a href="#" onclick="<%# AspxTools.JsStringUnquoted(GetDiscJoinLinkFunctionName(DataBinder.Eval(Container.DataItem, "NotifIsValid"))) %>(<%# AspxTools.JsString(GetDiscJoinId(DataBinder.Eval(Container.DataItem, "DiscLogId"), DataBinder.Eval(Container.DataItem, "NotifId"), DataBinder.Eval(Container.DataItem, "NotifIsValid"))) %>); return false;">
                    <%# AspxTools.HtmlString(GetDiscJoinLinkDescription(DataBinder.Eval(Container.DataItem, "NotifIsValid"))) %>
                </a>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscStatus" HeaderText="Status">
			<ItemTemplate>
                <span class="<%# AspxTools.HtmlString(GetDiscStatusCss((E_DiscStatus)DataBinder.Eval(Container.DataItem, "DiscStatus"))) %>">
                    <%# AspxTools.HtmlString(GetDiscStatusDesc((E_DiscStatus)DataBinder.Eval(Container.DataItem, "DiscStatus"))) %>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscPriority" HeaderText="Priority">
			<ItemStyle Width="50px"></ItemStyle>
			<ItemTemplate>
                <span class="<%# AspxTools.HtmlString(GetDiscPriorityCss((E_DiscPriority)DataBinder.Eval(Container.DataItem, "DiscPriority"))) %>">
                    <%# AspxTools.HtmlString(GetDiscPriorityDesc((E_DiscPriority)DataBinder.Eval(Container.DataItem, "DiscPriority"))) %>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscSubject" HeaderText="Subject">
			<ItemTemplate>
                <span class="<%# AspxTools.HtmlString(GetDiscSubjectCss(true)) %>">
                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DiscSubject")) %>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscRefObjNm1" HeaderText="Loan Number">
			<ItemTemplate>
				<asp:Panel Runat="server" ID="LoanNumberLinks" style="CURSOR: pointer;">
					<div id="m_loanNumberTag" style="DISPLAY: block; COLOR: blue;" onmouseover="style.color = 'orange';" onmouseout="style.color = 'blue';" onclick="onClickLoanLinkSet( <%# AspxTools.JsString(m_remindersDG.ClientID) %> , this );">
						<u>
							<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DiscRefObjNm1")) %>
						</u>
					</div><div style="DISPLAY: none; PADDING: 0px; MARGIN-TOP: 2px; MARGIN-BOTTOM: 8px;">
						<asp:Panel id="EditLink" runat="server" style="PADDING-LEFT: 2px;">
							&#x2022;
							<asp:Panel id="Show" runat="server" style="DISPLAY: inline;">
								<a href="javascript:editLoan(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="Edit loan">
									edit
								</a>
							</asp:Panel>
							<asp:HyperLink id="Hide" runat="server" Enabled="False">
								edit
							</asp:HyperLink>
				        </asp:Panel>
						<asp:Panel id="LeadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<asp:Panel id="ShowLeadEdit" runat="server" style="DISPLAY: inline;">
								<a href="javascript:editLead(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="Edit lead">
									edit
								</a>
							</asp:Panel>
							<asp:HyperLink id="DisabledLeadEdit" runat="server" Enabled="False">
								edit
							</asp:HyperLink>
						</asp:Panel>
	   					<asp:Panel id="LoadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:editLoan(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="View loan in editor">
								view
							</a>
	                    </asp:Panel>
	   					<asp:Panel id="LoadLeadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:editLead(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="View lead in editor">
								view
							</a>
	                    </asp:Panel>
						<asp:Panel id="ViewLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:viewLoan(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="View loan summary">
								view
							</a>
						</asp:Panel>
						<asp:Panel id="ViewLeadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:viewLoan(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="View lead summary">
								view
							</a>
						</asp:Panel>
						<div nowrap style="PADDING-LEFT: 2px;">
							&#x2022;
							<a href="javascript:createTask(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString()) %>);" title="Insert new task for this loan">
								new task
							</a>
						</div>
					</div>
				</asp:Panel>
				<asp:Panel Runat="server" ID="LoanNumberPlainText" Visible="False">
					<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DiscRefObjNm1")) %>
				</asp:Panel>
			</ItemTemplate>
		</asp:TemplateColumn>

		<asp:BoundColumn DataField="DiscRefObjNm2" SortExpression="DiscRefObjNm2" ReadOnly="True" HeaderText="Borrower"/>
		<asp:TemplateColumn SortExpression="DiscDueDate" HeaderText="Due Date">
			<ItemTemplate>
                <span class="<%# AspxTools.HtmlString((DataBinder.Eval(Container.DataItem, "DiscDueDate") is DateTime) ? GetDiscDueDateCss((DateTime)DataBinder.Eval(Container.DataItem, "DiscDueDate")) : string.Empty) %>">
                    <%# AspxTools.HtmlString((DataBinder.Eval(Container.DataItem, "DiscDueDate") is DateTime) ? ((DateTime)DataBinder.Eval(Container.DataItem, "DiscDueDate")).ToString() : string.Empty) %>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:BoundColumn DataField="DiscCreatorLoginNm" SortExpression="DiscCreatorLoginNm" ReadOnly="True" HeaderText="Task Author"/>
		<asp:BoundColumn DataField="DiscCreatedDate" SortExpression="DiscCreatedDate" ReadOnly="True" HeaderText="Created On"/>
		<asp:BoundColumn Visible="False" DataField="NotifId" ReadOnly="True"/>
		<asp:BoundColumn Visible="False" DataField="NotifIsValid" ReadOnly="True"/>
	</Columns>
</ml:commondatagrid>

<script>
	function addReminder()
	{
		showModal( "/los/reminders/TaskEditor.aspx" , null, null, null, function(args){

			if( args != null && args.OK != null && args.OK == true )
			{
				self.location = self.location;
			}
		})
	}

	function activateNotif( notifId )
	{
		showModal( "/los/reminders/TaskEditor.aspx?notifId=" + notifId , null, null, null, function(args){

			if( args != null && args.OK != null && args.OK == true )
			{
			<%-- 01/18/06 OPM 3780 MF Hide link and disable row if user modifies entry.
				Then display refresh warning message --%>
				var oClicked = document.activeElement;
				if ( oClicked && oClicked.parentNode
					&& oClicked.parentNode.parentNode )
					{
						var oRow = oClicked.parentNode.parentNode;
						var oRowLinks = oRow.getElementsByTagName("A");
						for (var i = 0; i < oRowLinks.length; i++) {
							oRowLinks(i).title = 'Click \"Refresh\" to see changes to this task.';
							oRowLinks(i).onclick = '';
							oRowLinks(i).href='#';
						}
						setDisabledAttr(oRow, true);
						oRow.title = "Click \"Refresh\" to see changes to this task.";
					}
				refreshPipeline();
			}
		})
	}

	function editNotif( notifId )
	{
		showModal( "/los/reminders/TaskEditor.aspx?notifId=" + notifId , null, null, null, function(args){
			if( args != null && args.OK != null && args.OK == true )
			{
			<%-- 01/18/06 OPM 3780 MF Disable link and table row if user modifies entry.
				Then display refresh warning message --%>
				var oClicked = document.activeElement;
				if ( oClicked && oClicked.parentNode
					&& oClicked.parentNode.parentNode )
					{
						var oRow = oClicked.parentNode.parentNode;
						var oRowLinks = oRow.getElementsByTagName("A");
						for (var i = 0; i < oRowLinks.length; i++) {
							oRowLinks(i).title = 'Click \"Refresh\" to see changes to this task.';
							oRowLinks(i).onclick = '';
							oRowLinks(i).href='#';
						}
						setDisabledAttr(oRow, true);
						oRow.title = "Click \"Refresh\" to see changes to this task.";
					}
				refreshPipeline();
			}
		})
	}

	function joinDiscussion( discLogId )
	{
		showModal( "/los/reminders/TaskEditor.aspx?discLogId=" + discLogId , null, null, null, function(args){

			if( args != null && args.OK != null && args.OK == true )
			{
			<%-- 01/18/06 OPM 3780 MF Disable links and disable row if user modifies entry.
				Then display refresh warning message --%>

				var oClicked = document.activeElement;
				if ( oClicked && oClicked.parentNode
					&& oClicked.parentNode.parentNode )
					{
						var oRow = oClicked.parentNode.parentNode;
						var oRowLinks = oRow.getElementsByTagName("A");
						for (var i = 0; i < oRowLinks.length; i++) {
							oRowLinks(i).title = 'Click \"Refresh\" to see changes to this task.';
							oRowLinks(i).onclick = '';
							oRowLinks(i).href='#';
						}
						setDisabledAttr(oRow, true);
						oRow.title = "Click \"Refresh\" to see changes to this task.";
					}
				refreshPipeline();

			}
		})

	}
</script>
