// <copyright file="DocumentIndexingPortlet.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOfficeApp.los.Portlets
{
    using System;
    using LendersOffice.Security;

    /// <summary>
    /// Responsible for adding  Document Indexing Portlet to the pipeline.
    /// </summary>
    public partial class DocumentIndexingPortlet : System.Web.UI.UserControl
    {
        /// <summary>
        /// The PageInit method.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="a">The arguments of the event.</param>
        protected void PageInit(object sender, System.EventArgs a)
        {
            var principal = PrincipalFactory.CurrentPrincipal;


            if (!principal.HasPermission(Permission.AllowAccessingOCRReviewPortal))
            {
                this.Visible = false;
                return;
            }

            var brokerDB = principal.BrokerDB;
            if (!brokerDB.IsOCREnabled || !brokerDB.OCRHasConfiguredAccount)
            {
                this.Visible = false;
            }


        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Page.Init += new System.EventHandler(PageInit);
        }
        #endregion
    }
}
