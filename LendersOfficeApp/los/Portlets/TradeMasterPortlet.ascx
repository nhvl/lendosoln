﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TradeMasterPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.TradeMasterPortlet" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SearchTrades" Src="~/los/Portlets/TradePortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="OpenTrades" Src="~/los/Portlets/OpenTradesPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SettlementReport" Src="~/los/Portlets/TradeSettlementReportPortlet.ascx" %>

<div id="TradeMasterPortlet">
    <div class="Tabs">
        <ul class="tabnav">
            <li runat="server" id="SearchTab"><a href="#" class="tab search-trades">Search</a></li>
            <li runat="server" id="OpenTradesTab"><a href="#" class="tab open-trades">Open Trades</a></li>
            <li runat="server" id="SettlementReportTab"><a href="#" class="tab settlement-report">Settlement Report</a></li>
        </ul>
    </div>

    <uc1:SearchTrades runat="server" ID="SearchTrades" Visible="false" />
    <uc1:OpenTrades runat="server" ID="OpenTrades" Visible="false" />
    <uc1:SettlementReport runat="server" ID="SettlementReport" Visible="false" />
</div>
