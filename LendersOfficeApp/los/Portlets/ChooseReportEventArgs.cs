﻿#region Generated Code 
namespace LendersOfficeApp.los.Portlets
#endregion 
{
    /// <summary>
    /// Represents the data of the chosen report.
    /// </summary>
    public class ChooseReportEventArgs : System.EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChooseReportEventArgs"/> class.
        /// </summary>
        /// <param name="desc">The details of the chosen report.</param>
        public ChooseReportEventArgs(ReportDesc desc)
        {
            this.SelectedReport = desc;
        }

        /// <summary>
        /// Gets the selected report. 
        /// </summary>
        /// <value>The selected report.</value>
        public ReportDesc SelectedReport { get; private set; }
    }
}