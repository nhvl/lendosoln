namespace LendersOfficeApp.los.Portlets
{
    using System;
    using System.Data;
    using System.Web;
    using System.Web.UI;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.LoanSearch;
    using LendersOffice.Security;

    public partial class MyLeadPortlet : UserControl, IAutoLoadUserControl
	{
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
        }
		#endregion

        private void BindDataGrid()
        {
            DataSet ds = LendingQBSearch.ListMyLeads(BrokerUserPrincipal.CurrentPrincipal);

            //7/22/08 - jk. Load sort order from cookie and after databind, store current sort to the cookie
            HttpCookie cookie = Request.Cookies["myLead_sortOrder"];
            if (cookie != null && cookie.Value != null)
            {
                m_leadsDG.DefaultSortExpression = cookie.Value;
            }
            else
                m_leadsDG.DefaultSortExpression = "aBLastNm ASC";

            m_leadsDG.DataSource = ds.Tables[0].DefaultView;
            m_leadsDG.DataBind();

            LendersOffice.Common.RequestHelper.StoreToCookie("myLead_sortOrder", ds.Tables[0].DefaultView.Sort, false);
        }

        public void LoadData() 
        {
            if (! BrokerUserPrincipal.CurrentPrincipal.HasRole(E_RoleT.CallCenterAgent)) 
            {
                Visible = false;
                return;
            }
            BindDataGrid();

            if (m_leadsDG.Items.Count == 0) 
            {
                m_countLabel.Text = "No leads assigned to you.";
            } 
            else if (m_leadsDG.Items.Count < 2) 
            {
                m_countLabel.Text = "1 lead assigned to you.";
            } 
            else if (m_leadsDG.Items.Count < ConstAppDavid.MaxViewableRecordsInPipeline)
            {
                m_countLabel.Text = m_leadsDG.Items.Count + " leads assigned to you.";
            } 
            else 
            {
                m_countLabel.Text = "Too many files -- displaying first " + ConstAppDavid.MaxViewableRecordsInPipeline + ".";
				m_countLabel.ToolTip = "Please use our Search page to find more leads";
            }
        }

        public void SaveData() 
        {
        }
	}
}
