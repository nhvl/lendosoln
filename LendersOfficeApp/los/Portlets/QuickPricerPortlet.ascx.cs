﻿#region Using "Generated Code" region to disable StyleCop failure for SA1300 due to 'los' namespace.
namespace LendersOfficeApp.los.Portlets
#endregion
{
    using System;
    using LendersOffice.Security;

    /// <summary>
    /// Portlet for Quick Pricer items.
    /// </summary>
    public partial class QuickPricerPortlet : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets the user principal.
        /// </summary>
        /// <value>The user's principal.</value>
        protected BrokerUserPrincipal BrokerUser => BrokerUserPrincipal.CurrentPrincipal;

        /// <summary>
        /// Gets a value indicating whether the user has permission to create new lead files.
        /// </summary>
        private bool AllowCreatingNewLeadFiles => this.BrokerUser.HasPermission(Permission.AllowCreatingNewLeadFiles);

        /// <summary>
        /// Gets a value indicating whether the user has permission to create new loan files.
        /// </summary>
        private bool AllowCreatingNewLoanFiles => this.BrokerUser.HasPermission(Permission.AllowCreatingNewLoanFiles);

        /// <summary>
        /// Updates visibility on page load.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.BrokerUser.IsQuickPricerEnable
                || (!this.AllowCreatingNewLeadFiles && !this.AllowCreatingNewLoanFiles))
            {
                this.Visible = false;
                return;
            }

            this.MonitoredScenarios.Visible = this.BrokerUser.IsActuallyUsePml2AsQuickPricer
                && this.BrokerUser.BrokerDB.AllowRateMonitorForQP2FilesInEmdeddedPml;
        }
    }
}