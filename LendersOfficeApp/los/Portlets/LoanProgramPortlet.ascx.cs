using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOfficeApp.los.admin;
using LendersOffice.ObjLib.LockPolicies;
using System.Linq;
using LendersOffice.Admin;
using Mismo3Specification.Version4Schema;
using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
using System.Collections.Generic;

namespace LendersOfficeApp.los.Portlets
{

    public partial class LoanProgramPortlet : System.Web.UI.UserControl
    {
        protected System.Web.UI.WebControls.Label Label1;
        private BrokerDB broker;

        private BrokerUserPrincipal CurrentUser
        {
            // Access credentials.
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private BrokerDB Broker
        {
            get
            {
                if (broker == null)
                {
                    broker = this.CurrentUser.BrokerDB;
                }
                return broker;
            }
        }

        protected Boolean ShowLoanPrograms
        {
            get
            {
                return CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms)
                     || CurrentUser.HasPermission(Permission.CanModifyLoanPrograms);  //.HasRole(E_RoleT.Administrator);
            }
        }

        protected Boolean ShowMiPrograms
        {
            get
            {
                return CurrentUser.LoginNm.ToLower().Equals("pmlmaster");
            }
        }

        protected Boolean ShowAdjustments
        {
            get
            {
                return CurrentUser.HasPermission(Permission.AllowViewingAndEditingAdjustmentsAndOtherCreditsSetup);
            }
        }

        protected Boolean ShowFeeTypes
        {
            get
            {
                return CurrentUser.HasPermission(Permission.AllowViewingAndEditingFeeTypeSetup); //.HasRole(E_RoleT.Administrator);
            }
        }

        protected Boolean ShowGFEFeeService
        {
            get
            {
                return CurrentUser.HasPermission(Permission.AllowViewingAndEditingGFEFeeService) //.HasRole(E_RoleT.Administrator) 
                    && Broker.IsEnabledGfeFeeService;
            }
        }

        protected Boolean ShowCCTemplates
        {
            get
            {
                return CurrentUser.HasPermission(Permission.CanAccessCCTemplates);
            }
        }

        protected bool m_pricingVisible
        {
            get
            {
                return CurrentUser.HasRole(E_RoleT.Administrator)
                    && CurrentUser.HasPermission(Permission.CanModifyLoanPrograms);
            }
        }

        protected bool m_pricingGroupVisible
        {
            get
            {
                BrokerUserPrincipal principal = CurrentUser;
                return principal.HasPermission(Permission.AllowViewingAndEditingPriceGroups)//principal.HasRole(E_RoleT.Administrator) 
                    && principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
            }
        }

        protected bool m_lockDeskQuestionsVisible
        {
            get
            {
                BrokerUserPrincipal principal = CurrentUser;
                return principal.HasPermission(Permission.AllowViewingAndEditingLockDeskQuestions) // .HasRole(E_RoleT.Administrator) 
                    && principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
            }
        }


        //opm 22789 fs 12/12/08
        protected bool m_disablePricingVisible
        {
            get
            {
                return CurrentUser.HasPermission(Permission.AllowViewingAndEditingDisablePricing) //CurrentUser.HasAtLeastOneRole(new E_RoleT[] { E_RoleT.LockDesk, E_RoleT.Administrator}) && 
                    && LockPolicy.RetrieveAllForBroker(Broker.BrokerID).Any(lp => lp.IsUsingRateSheetExpirationFeature);
                // show only if any of lp's have it enabled.
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            // 01/05/09 mf. OPM 27037. Hide entire "Loan Program"
            // section if user does not have access to any of the links. 
            if (ShowLoanPrograms == false
            && ShowCCTemplates == false
            && m_pricingVisible == false
            && m_pricingGroupVisible == false
            && m_disablePricingVisible == false
            && m_lockDeskQuestionsVisible == false
            && ShowGFEFeeService == false
            && ShowFeeTypes == false
            && ShowMiPrograms == false
            && !ShowAdjustments 
            )
            {
                this.Visible = false;
                return;
            }
            string js = "showModal('/los/Template/ClosingCostList.aspx','', null, null, null, {hideCloseButton:true, height:520 });return false;";
            m_closingCostFeePanelRactive.Visible = Broker.IsEnableGfe2015 && ShowFeeTypes;
            if (Broker.AreAutomatedClosingCostPagesVisible)
            {
                js = "showModal('/los/Template/ClosingCost/ClosingCostSystem.aspx','', null, null, null, {hideCloseButton:true, height:520 });return false;";
            }

            closingcostlink.Attributes.Add("onclick", js);
            if (ShowAdjustments)
            {
                ((BasePage)Page).RegisterJsScript("LQBPopup.js");
                ((BasePage)Page).EnableJqueryMigrate = false;
                ((BasePage)Page).RegisterJsScript("jquery.tmpl.js");
                ((BasePage)Page).RegisterJsScript("AdjustmentsAndOtherCreditsSetup.js");
                ((BaseServicePage)this.Page).RegisterService("adjustments", "/los/AdjustmentsAndOtherCreditsSetup/AdjustmentsAndOtherCreditsSetupService.aspx");
            }            
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion

    }

}
