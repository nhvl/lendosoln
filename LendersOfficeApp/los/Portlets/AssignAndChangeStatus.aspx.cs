using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Events;
using LendersOffice.Roles;
using DataAccess;
using LendersOffice.Constants;
using CommonProjectLib.Common.Lib;
using System.Linq;
using System.Linq.Expressions;

namespace LendersOfficeApp.los.Portlets
{
    public partial class AssignAndChangeStatus : LendersOffice.Common.BasePage
    {
        private class TempAgentInformation
        {
            public string FullName = "";
            public string Phone = "";
            public string FaxNum = "";
            public string StreetAddr = "";
            public string City = "";
            public string State = "";
            public string Zip = "";
            public string EmailAddr = "";
            public string CompanyName = "";
            public string LicenseXmlContent = "";
            public string CompanyLicenseXmlContent = "";
            public Guid PmlBrokerId = Guid.Empty;
            public Guid BranchId = Guid.Empty;
        }
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_processorChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_telemarketerChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_managerChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_realEstateChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_openerChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_lenderExecChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_lockDeskChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_underwriterChoice;

        private BrokerDB m_Broker = null;

        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

       
        private bool m_isAssignToManagerOnly
        {
            get
            {
                // If the user only has "Telemarketer" role and
                // the broker setting "OptionTelemarketerCanOnlyAssignToManager" is true
                // then user can only assign loan to manager.
                // Any other role can assign to other processor, agent and/or manager.

                if (BrokerUser.HasAtLeastOneRole(new E_RoleT[] { E_RoleT.Manager, E_RoleT.Processor, E_RoleT.LoanOfficer }))
                {
                    return false;
                }
                else if (BrokerUser.HasRole(E_RoleT.CallCenterAgent))
                {
                    return m_Broker.OptionTelemarketerCanOnlyAssignToManager;
                }

                return false;
            }
        }

        private List<Guid> GetLoanIds()
        {
            return ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Guid>>(sLIds.Value);
        }

        protected void PageLoad(object sender, System.EventArgs a)
        {
            // Initialize this page.
            RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            m_Broker = this.BrokerUser.BrokerDB;

            m_bpSection.Visible = this.BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
            NonPmlScript.Visible = m_bpSection.Visible == false;
            PmlEnabledBrokerScripts.Visible = m_bpSection.Visible;
            ExternalSecondary.Visible = m_bpSection.Visible;
            ExternalPostCloser.Visible = m_bpSection.Visible;

            if (!Page.IsPostBack)
            {
                string key  = RequestHelper.GetSafeQueryString("key");
                string status = RequestHelper.GetSafeQueryString("status");
                sLIds.Value = AutoExpiredTextCache.GetFromCache(key);
                AutoExpiredTextCache.ExpireCache(key);

                // 10/19/2004 kb - We are no longer hitting the db with
                // each bind.  Because we will show all types of users
                // in the drop downs, we need to get all the employee
                // info up front and hash it into lists for each role.
                // If the user lacks ability to see all, we limit the
                // search to this branch.
                //
                // 7/22/2005 kb - Load using revised role table loader.
                //
                // 8/4/2005 kb - We have a new method of selecting
                // employees for particular roles (see case 2533).
                // Now, we supply a link to the last selected option.
                // Clicking the link will launch a role-based chooser.

                if (m_isAssignToManagerOnly == true)
                {

                    LoSection.Visible = false;
                    m_officerOldRB.Enabled = false;
                    m_officerNewRB.Enabled = false;

                    m_processorChoice.Enabled = false;
                    m_processorOldRB.Enabled = false;
                    m_processorNewRB.Enabled = false;

                    m_telemarketerChoice.Enabled = false;
                    m_telemarketerOldRB.Enabled = false;
                    m_telemarketerNewRB.Enabled = false;

                    m_realEstateChoice.Enabled = false;
                    m_realEstateNewRB.Enabled = false;
                    m_realEstateOldRB.Enabled = false;

                    m_openerChoice.Enabled = false;
                    m_openerNewRB.Enabled = false;
                    m_openerOldRB.Enabled = false;

                    m_lenderExecChoice.Enabled = false;
                    m_lenderExecOldRB.Enabled = false;
                    m_lenderExecNewRB.Enabled = false;

                    m_lockDeskChoice.Enabled = false;
                    m_lockDeskOldRB.Enabled = false;
                    m_lockDeskNewRB.Enabled = false;

                    m_underwriterChoice.Enabled = false;
                    m_underwriterOldRB.Enabled = false;
                    m_underwriterNewRB.Enabled = false;

                    m_bpSection.Visible = false;
                    ExternalSecondary.Visible = false;
                    ExternalPostCloser.Visible = false;

                    m_closerChoice.Enabled = false;
                    m_closerNewRB.Enabled = false;
                    m_closerOldRB.Enabled = false;

                }

                if (!string.IsNullOrEmpty(status))
                {
                    if (status.Equals("lead", StringComparison.OrdinalIgnoreCase))
                    {
                        loanLeadStatusLabel.Text = "Lead Status";
                        Tools.Bind_sLeadStatusT(m_statusDDL);
                    }
                    else if (status.Equals("loan", StringComparison.OrdinalIgnoreCase))
                    {
                        Tools.Bind_sLoanStatusT(m_statusDDL, BrokerUser.BrokerId, false);
                    }
                    else
                    {
                        loanLeadStatusLabel.Text = "Status";
                        Tools.Bind_sLeadStatusT(m_statusDDL);
                        Tools.Bind_sLoanStatusT(m_statusDDL, BrokerUser.BrokerId, false);
                    }
                }
                else if(GetLoanIds()[0] != null)
                {
                    CPageData dataLoan = new CEmployeeDataWithAccessControl(GetLoanIds()[0]);
                    dataLoan.InitLoad();

                    if (Tools.IsStatusLead(dataLoan.sStatusT))
                    {
                        Tools.Bind_sLeadStatusT(m_statusDDL);
                        loanLeadStatusLabel.Text = "Lead Status";
                    }
                    else
                    {
                        Tools.Bind_sLoanStatusT(m_statusDDL, BrokerUser.BrokerId, false);
                    }
                }
                // 
                if (m_Broker.CopyInternalUsersToOfficalContactsByDefault)
                {
                    m_copyToOfficialContact.Checked = true;
                }
            }

            // 6/22/2004 dd - Only person with manager role and have
            // corporate access level can change branch.
            //
            // 7/25/2005 kb - During reworking this page, the branch
            // binding was placed outside of the postback protection.
            // So, the ddl rebinds before any event can process the
            // currently selected value.

            if (BrokerUser.IsInRole(ConstApp.ROLE_MANAGER) && BrokerUser.HasPermission(Permission.BrokerLevelAccess))
            {
                if (IsPostBack == false)
                {
                    BindBranchDropDownList(m_branchDDL);
                }
            }
            else
            {
                m_branchDDL.Items.Add(new ListItem("<-- N/A -->", Guid.Empty.ToString()));
                m_branchDDL.Enabled = false;
                m_branchNewRB.Enabled = false;
                m_branchNewRB.ToolTip = "Only Managers with 'Corporate' access can modify this";
            }

        }

        private void BindBranchDropDownList(DropDownList ddl)
        {
            using (DbDataReader reader = BranchDB.GetBranches(BrokerUser.BrokerId))
            {
                ddl.DataSource = reader;
                ddl.DataValueField = "BranchID";
                ddl.DataTextField = "Name";
                ddl.DataBind();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }


        #endregion


        private AssignAndChangeStatus.TempAgentInformation LoadEmployeeInfo(Guid employeeID)
        {
            TempAgentInformation agent = new TempAgentInformation();

            if (employeeID != Guid.Empty)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@EmployeeID", employeeID)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.ConnectionInfo, "GetEmployeeDetailsByEmployeeId", parameters))
                {
                    if (reader.Read())
                    {
                        agent.FullName = (string)reader["FullName"];
                        agent.Phone = (string)reader["Phone"];
                        agent.FaxNum = (string)reader["Fax"];
                        agent.StreetAddr = (string)reader["Addr"];
                        agent.City = (string)reader["City"];
                        agent.State = (string)reader["State"];
                        agent.Zip = (string)reader["Zip"];
                        agent.EmailAddr = (string)reader["Email"];
                        agent.CompanyName = (string)reader["PmlBrokerCompany"];
                        agent.CompanyLicenseXmlContent = reader["PmlBrokerLicenseXmlContent"] == DBNull.Value ? "" : (string)reader["PmlBrokerLicenseXmlContent"];
                        agent.LicenseXmlContent = reader["LicenseXmlContent"] == DBNull.Value ? "" : (string)reader["LicenseXmlContent"];
                        agent.PmlBrokerId = reader["PmlBrokerId"] == DBNull.Value ? Guid.Empty : (Guid)reader["PmlBrokerId"];
                        agent.BranchId = reader["BranchId"] == DBNull.Value ? Guid.Empty : (Guid)reader["BranchId"];
                    }
                }

            }
            return agent;
        }

        private bool ValidateOriginatingCompany(
            TempAgentInformation tempAgent,
            TempAgentInformation tempBp,
            TempAgentInformation tempExternalSecondary,
            TempAgentInformation tempExternalPostCloser)
        {
            bool assignedLoanOfficer = tempAgent != null;
            bool assignedBrokerProcessor = tempBp != null;
            bool assignedExternalSecondary = tempExternalSecondary != null;
            bool assignedExternalPostCloser = tempExternalPostCloser != null;

            var errors = new List<string>();

            if (assignedLoanOfficer && assignedBrokerProcessor &&
                tempAgent.PmlBrokerId != tempBp.PmlBrokerId)
            {
                errors.Add("Loan Officer and Processor (External) must be from the same originating company.");
            }

            if (assignedLoanOfficer && assignedExternalSecondary &&
                tempAgent.PmlBrokerId != tempExternalSecondary.PmlBrokerId)
            {
                errors.Add("Loan Officer and Secondary (External) must be from the same originating company.");
            }

            if (assignedLoanOfficer && assignedExternalPostCloser &&
                tempAgent.PmlBrokerId != tempExternalPostCloser.PmlBrokerId)
            {
                errors.Add("Loan Officer and Post-Closer (External) must be from the same originating company.");
            }

            if (assignedBrokerProcessor && assignedExternalSecondary &&
                tempBp.PmlBrokerId != tempExternalSecondary.PmlBrokerId)
            {
                errors.Add("Processor (External) and Secondary (External) must be from the same originating company.");
            }

            if (assignedBrokerProcessor  && assignedExternalPostCloser &&
                tempBp.PmlBrokerId != tempExternalPostCloser.PmlBrokerId)
            {
                errors.Add("Processor (External) and Post-Closer (External) must be from the same originating company.");
            }

            if (assignedExternalSecondary && assignedExternalPostCloser &&
                tempExternalSecondary.PmlBrokerId != tempExternalPostCloser.PmlBrokerId)
            {
                errors.Add("Secondary (External) and Post-Closer (External) must be from the same originating company.");
            }

            if (errors.Any())
            {
                ShowFixableError(string.Join(" ", errors.ToArray()));
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool ValidateRoleAssignmentsAgainstExisting(
            CPageData dataLoan,
            TempAgentInformation tempAgent,
            TempAgentInformation tempBp,
            TempAgentInformation tempExternalSecondary,
            TempAgentInformation tempExternalPostCloser)
        {
            bool useNewLoanOfficer = m_officerNewRB.Checked && tempAgent != null;
            bool isValidLoanOfficerAssigned = dataLoan.sEmployeeLoanRepId != Guid.Empty;
            TempAgentInformation existingLoanOfficerInfo = isValidLoanOfficerAssigned ?
                LoadEmployeeInfo(dataLoan.sEmployeeLoanRepId) : null;

            bool useNewBrokerProcessor = m_bpSectionNewRb.Checked && tempBp != null;
            bool isValidBrokerProcessorAssigned = dataLoan.sEmployeeBrokerProcessorId != Guid.Empty;
            TempAgentInformation existingBP = isValidBrokerProcessorAssigned ?
                LoadEmployeeInfo(dataLoan.sEmployeeBrokerProcessorId) : null;

            bool useNewExternalPostCloser = m_externalPostCloserNewRb.Checked && tempExternalPostCloser != null;
            bool isValidExternalPostCloserAssigned = dataLoan.sEmployeeExternalPostCloserId != Guid.Empty;
            TempAgentInformation existingExternalPostCloser = isValidExternalPostCloserAssigned ?
                LoadEmployeeInfo(dataLoan.sEmployeeExternalPostCloserId) : null;

            bool useNewExternalSecondary = m_externalSecondaryNewRb.Checked && tempExternalSecondary != null;
            bool isValidExternalSecondaryAssigned = dataLoan.sEmployeeExternalSecondaryId != Guid.Empty;
            TempAgentInformation existingExternalSecondary = isValidExternalSecondaryAssigned ?
                LoadEmployeeInfo(dataLoan.sEmployeeExternalSecondaryId) : null;

            // If you're assinging a new LO with an existing BP
            if (useNewLoanOfficer && !useNewBrokerProcessor && isValidBrokerProcessorAssigned
                && existingBP.PmlBrokerId != tempAgent.PmlBrokerId)
            {
                return false;
            }
            // If you're assigning a new LO with an existing External Secondary
            else if (useNewLoanOfficer && !useNewExternalSecondary && isValidExternalSecondaryAssigned &&
                existingExternalSecondary.PmlBrokerId != tempAgent.PmlBrokerId)
            {
                return false;
            }
            // If you're assigning a new LO with an existing External Post-Closer
            else if (useNewLoanOfficer && !useNewExternalPostCloser && isValidExternalPostCloserAssigned &&
                existingExternalPostCloser.PmlBrokerId != tempAgent.PmlBrokerId)
            {
                return false;
            }

            // If you're assigning a new BP and there is an existing LO.
            if (useNewBrokerProcessor && !useNewLoanOfficer && isValidLoanOfficerAssigned &&
                existingLoanOfficerInfo.PmlBrokerId != tempBp.PmlBrokerId)
            {
                return false;
            }
            // If you're assigning a new BP and there is an existing External Secondary
            else if (useNewBrokerProcessor && !useNewExternalSecondary && isValidExternalSecondaryAssigned &&
                existingExternalSecondary.PmlBrokerId != tempBp.PmlBrokerId)
            {
                return false;
            }
            // If you're assigning a new BP and there is an existing Corr Proc
            else if (useNewBrokerProcessor && !useNewExternalPostCloser && isValidExternalPostCloserAssigned &&
                existingExternalPostCloser.PmlBrokerId != tempBp.PmlBrokerId)
            {
                return false;
            }

            // If you're assigning a new Corr LO with an existing PML LO
            if (useNewExternalSecondary && !useNewLoanOfficer && isValidLoanOfficerAssigned &&
                existingLoanOfficerInfo.PmlBrokerId != tempExternalSecondary.PmlBrokerId)
            {
                return false;
            }
            // If you're assigning a new Corr LO with an existing BP
            else if (useNewExternalSecondary && !useNewBrokerProcessor && isValidBrokerProcessorAssigned &&
                existingBP.PmlBrokerId != tempExternalSecondary.PmlBrokerId)
            {
                return false;
            }
            // If you're assinging a new Corr LO with an existing External Post-Closer
            else if (useNewExternalSecondary && !useNewExternalPostCloser && isValidExternalPostCloserAssigned &&
                existingExternalPostCloser.PmlBrokerId != tempExternalSecondary.PmlBrokerId)
            {
                return false;
            }

            // If you're assinging a new Corr Proc with an existing PML LO
            if (useNewExternalPostCloser && !useNewLoanOfficer && isValidLoanOfficerAssigned &&
                existingLoanOfficerInfo.PmlBrokerId != tempExternalPostCloser.PmlBrokerId)
            {
                return false;
            }
            // If you're assinging a new Corr Proc with an existing BP
            else if (useNewExternalPostCloser && !useNewBrokerProcessor && isValidBrokerProcessorAssigned &&
                existingBP.PmlBrokerId != tempExternalPostCloser.PmlBrokerId)
            {
                return false;
            }
            // If you're assinging a new Corr Proc with an existing External Secondary
            else if (useNewExternalPostCloser && !useNewExternalSecondary && isValidExternalSecondaryAssigned &&
                existingExternalSecondary.PmlBrokerId != tempExternalPostCloser.PmlBrokerId)
            {
                return false;
            }

            return true;
        }
            

        protected void OnOKClick(object sender, System.EventArgs a)
        {
            ErrorRow.Visible = false;
            bool bCopyToOfficialAgent = m_copyToOfficialContact.Checked;
            // Do assignment and change new status for list of loans.

            //List<string> errorList = new List<string>();

            Guid agentID = Guid.Empty;
            Guid processorID = Guid.Empty;
            Guid openerID = Guid.Empty;
            Guid telemarketerID = Guid.Empty;
            Guid managerID = Guid.Empty;
            Guid realEstateID = Guid.Empty;
            Guid lenderExecID = Guid.Empty;
            Guid lockDeskID = Guid.Empty;
            Guid underwriterID = Guid.Empty;
            Guid closerID = Guid.Empty;
            Guid brokerProcessorId = Guid.Empty;
            Guid funderId = Guid.Empty;
            Guid shipperId = Guid.Empty;
            Guid postCloserId = Guid.Empty;
            Guid insuringId = Guid.Empty;
            Guid collateralAgentId = Guid.Empty;
            Guid docDrawerId = Guid.Empty;
            Guid creditAuditorId = Guid.Empty;
            Guid disclosureDeskId = Guid.Empty;
            Guid juniorProcessorId = Guid.Empty;
            Guid juniorUnderwriterId = Guid.Empty;
            Guid legalAuditorId = Guid.Empty;
            Guid loanOfficerAssistantId = Guid.Empty;
            Guid purchaserId = Guid.Empty;
            Guid qcComplianceId = Guid.Empty;
            Guid secondaryId = Guid.Empty;
            Guid servicingId = Guid.Empty;
            Guid externalSecondaryId = Guid.Empty;
            Guid externalPostCloserId = Guid.Empty;

            TempAgentInformation tempAgent = null;
            TempAgentInformation tempBp = null;
            TempAgentInformation tempExternalSecondary = null;
            TempAgentInformation tempExternalPostCloser = null;

            List<Tuple<string, string,string>> recentlyModifiedLoans = new List<Tuple<string, string, string>>();
            List<Tuple<string, string>> accessDeniedLoans = new List<Tuple<string, string>>();
            List<Tuple<string, string, string>> genericErrorLoans = new List<Tuple<string, string, string>>();
            List<Tuple<string, string>> ocErrorLoans = new List<Tuple<string, string>>();
            List<Tuple<string, string>> leadToLoanConversionErrors = new List<Tuple<string, string>>();
            List<Tuple<string, string>> loanToLeadConversionErrors = new List<Tuple<string, string>>();

            #region set ids 
            if (m_officerChoice_m_Text.Value != "None")
            {
                agentID = new Guid(m_officerChoice_m_Data.Value);

                //load it anyways along with LO so we can make sure they are being set correctly.
                tempAgent = LoadEmployeeInfo(agentID);
            }

            if (m_processorChoice.Data != "None")
            {
                processorID = new Guid(m_processorChoice.Data);

            }

            if (m_openerChoice.Data != "None")
            {
                openerID = new Guid(m_openerChoice.Data);

            }

            if (m_telemarketerChoice.Data != "None")
            {
                telemarketerID = new Guid(m_telemarketerChoice.Data);

            }

            if (m_realEstateChoice.Data != "None")
            {
                realEstateID = new Guid(m_realEstateChoice.Data);

            }

            if (m_lenderExecChoice.Data != "None")
            {
                lenderExecID = new Guid(m_lenderExecChoice.Data);
            }

            if (m_lockDeskChoice.Data != "None")
            {
                lockDeskID = new Guid(m_lockDeskChoice.Data);
            }

            if (m_underwriterChoice.Data != "None")
            {
                underwriterID = new Guid(m_underwriterChoice.Data);

            }

            if (m_managerChoice.Data != "None")
            {
                managerID = new Guid(m_managerChoice.Data);

            }

            if (m_closerChoice.Data != "None")
            {
                closerID = new Guid(m_closerChoice.Data);
            }

            if (false == m_bpChoice_Text.Value.Equals("None", StringComparison.InvariantCultureIgnoreCase))
            {
                brokerProcessorId = new Guid(m_bpChoice_Data.Value);

                //load it anyways along with LO so we can make sure they are being set correctly.
                tempBp = LoadEmployeeInfo(brokerProcessorId);
            }

            if (!m_externalSecondaryChoice_Text.Value.Equals("None", StringComparison.InvariantCultureIgnoreCase))
            {
                externalSecondaryId = new Guid(m_externalSecondaryChoice_Data.Value);

                tempExternalSecondary = LoadEmployeeInfo(externalSecondaryId);
            }

            if (!m_externalPostCloserChoice_Text.Value.Equals("None", StringComparison.InvariantCultureIgnoreCase))
            {
                externalPostCloserId = new Guid(m_externalPostCloserChoice_Data.Value);

                tempExternalPostCloser = LoadEmployeeInfo(externalPostCloserId);
            }

            if (m_funderChoice.Data != "None")
            {
                funderId = new Guid(m_funderChoice.Data);
            }

            if (m_shipperChoice.Data != "None")
            {
                shipperId = new Guid(m_shipperChoice.Data);
            }

            if (m_postCloserChoice.Data != "None")
            {
                postCloserId = new Guid(m_postCloserChoice.Data);
            }

            if (m_insuringChoice.Data != "None")
            {
                insuringId = new Guid(m_insuringChoice.Data);
            }

            if (m_collateralAgentChoice.Data != "None")
            {
                collateralAgentId = new Guid(m_collateralAgentChoice.Data);
            }

            if (m_docDrawerChoice.Data != "None")
            {
                docDrawerId = new Guid(m_docDrawerChoice.Data);
            }

            if (m_creditAuditorChoice.Data != "None")
            {
                creditAuditorId = new Guid(m_creditAuditorChoice.Data);
            }

            if (m_disclosureDeskChoice.Data != "None")
            {
                disclosureDeskId = new Guid(m_disclosureDeskChoice.Data);
            }

            if (m_juniorProcessorChoice.Data != "None")
            {
                juniorProcessorId = new Guid(m_juniorProcessorChoice.Data);
            }

            if (m_juniorUnderwriterChoice.Data != "None")
            {
                juniorUnderwriterId = new Guid(m_juniorUnderwriterChoice.Data);
            }

            if (m_legalAuditorChoice.Data != "None")
            {
                legalAuditorId = new Guid(m_legalAuditorChoice.Data);
            }

            if (m_loanOfficerAssistantChoice.Data != "None")
            {
                loanOfficerAssistantId = new Guid(m_loanOfficerAssistantChoice.Data);
            }

            if (m_purchaserChoice.Data != "None")
            {
                purchaserId = new Guid(m_purchaserChoice.Data);
            }

            if (m_qcComplianceChoice.Data != "None")
            {
                qcComplianceId = new Guid(m_qcComplianceChoice.Data);
            }

            if (m_secondaryChoice.Data != "None")
            {
                secondaryId = new Guid(m_secondaryChoice.Data);
            }

            if (m_servicingChoice.Data != "None")
            {
                servicingId = new Guid(m_servicingChoice.Data);
            }

            bool isValidOriginatingCompanySetting = this.ValidateOriginatingCompany(
                tempAgent,
                tempBp,
                tempExternalSecondary,
                tempExternalPostCloser);

            if (!isValidOriginatingCompanySetting)
            {
                return;
            }

            #endregion

            // Wrap the assignment update with queries to the database
            // that fetch the assignments before and after the command.
            // This way, we can calculate the delta and send out a
            // descriptive email to every affected employee.
            //
            // 8/9/2004 kb - We have moved loan role assignment change
            // processing to a background thread that will gather affected
            // broker's current state in batches, and hit the db a lot
            // less.  This populate calculation will go away soon.

            List<Guid> loanIds = GetLoanIds();

            // 3/20/2007 nw - OPM 11805 - when changing files to a lead status, restrict to max 10 files at a time
            if (m_statusNewRB.Checked)
            {
                E_sStatusT newStatus = (E_sStatusT)Tools.GetDropDownListValue(m_statusDDL);
                if (Tools.IsStatusLead(newStatus))
                {
                    int nFiles = loanIds.Count;

                    if (nFiles > 10)
                    {
                        ShowFixableError(nFiles.ToString() + " files selected for conversion to a lead status.  You can only convert up to 10 lead files at a time.");
                        return;
                    }
                }
            }

            bool convertedLeadToLoan = false;
            string convertedLeadIds = "";
            List<Guid> notConvertedLeadIds = new List<Guid>();
            List<Guid> notConvertedLoanIds = new List<Guid>();

            foreach (Guid sLId in loanIds)
            {
                // 8/16/2004 kb - We now send assignment change requests to a
                // message queue for background processing.  All we need is
                // the loaded loan data and the final assignment id for each
                // role that was checked for change.
                //
                // Note: It seems wierd how we assign the loan data's role
                // id, and then get that same property back out as if it
                // was the old value.  Well, it is the old value.  Set ops
                // don't stick until after save and reload.
                string id = sLId.ToString();
                CPageData dataLoan = null;
                try
                {
                    dataLoan = new CEmployeeDataWithAccessControl(sLId);
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    id = dataLoan.sLNm;

                    RoleChangeSet rcS = new RoleChangeSet();
                    E_sStatusT currentStatus = E_sStatusT.Loan_Other;

                    List<string> modifiedUserList = CPageData.GetRecentModificationUserName(sLId, BrokerUser);
                    if (modifiedUserList.Count > 0)
                    {
                        recentlyModifiedLoans.Add(Tuple.Create(dataLoan.sLNm, dataLoan.sPrimaryNm, String.Join(",", modifiedUserList.ToArray())));
                        continue;
                    }

                    currentStatus = dataLoan.sStatusT;
                    //if both set ignore
                    //if one set check other
                    // if none set ignore 

                    #region handle assignments chunk

                    bool newSettingsAreValid = this.ValidateRoleAssignmentsAgainstExisting(
                        dataLoan,
                        tempAgent,
                        tempBp,
                        tempExternalSecondary,
                        tempExternalPostCloser);

                    if (!newSettingsAreValid)
                    {
                        throw new CBaseException("OC", "There was an originating company conflict.");
                    }

                    if (m_officerNewRB.Checked)
                    {
                        dataLoan.sEmployeeLoanRepId = agentID;

                        rcS.Insert(CEmployeeFields.s_LoanRepRoleId, dataLoan.sEmployeeLoanRepId, agentID, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.LoanOfficer, agentID);
                        }
                    }

                    if (m_bpSectionNewRb.Checked)
                    {
                        dataLoan.sEmployeeBrokerProcessorId = brokerProcessorId;
                        rcS.Insert(CEmployeeFields.s_BrokerProcessorId, dataLoan.sEmployeeBrokerProcessorId, brokerProcessorId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.BrokerProcessor, brokerProcessorId);
                        }
                    }

                    if (m_bpSectionNewRb.Checked)
                    {
                        dataLoan.sEmployeeBrokerProcessorId = brokerProcessorId;
                    }

                    if (m_externalSecondaryNewRb.Checked)
                    {
                        dataLoan.sEmployeeExternalSecondaryId = externalSecondaryId;

                        rcS.Insert(
                            CEmployeeFields.s_ExternalSecondaryId,
                            dataLoan.sEmployeeExternalSecondaryId,
                            externalSecondaryId,
                            E_RoleChangeT.Update);

                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(
                                dataLoan,
                                E_AgentRoleT.ExternalSecondary,
                                externalSecondaryId);
                        }
                    }

                    if (m_externalPostCloserNewRb.Checked)
                    {
                        dataLoan.sEmployeeExternalPostCloserId = externalPostCloserId;

                        rcS.Insert(
                            CEmployeeFields.s_ExternalPostCloserId,
                            dataLoan.sEmployeeExternalPostCloserId,
                            externalPostCloserId,
                            E_RoleChangeT.Update);

                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(
                                dataLoan,
                                E_AgentRoleT.ExternalPostCloser,
                                externalPostCloserId);
                        }
                    }


                    if (m_processorNewRB.Checked)
                    {
                        dataLoan.sEmployeeProcessorId = processorID;

                        rcS.Insert(CEmployeeFields.s_ProcessorRoleId, dataLoan.sEmployeeProcessorId, processorID, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.Processor, processorID);

                        }
                    }

                    if (m_openerNewRB.Checked)
                    {
                        dataLoan.sEmployeeLoanOpenerId = openerID;

                        rcS.Insert(CEmployeeFields.s_LoanOpenerId, dataLoan.sEmployeeLoanOpenerId, openerID, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.LoanOpener, openerID);

                        }

                    }

                    if (m_telemarketerNewRB.Checked)
                    {
                        dataLoan.sEmployeeCallCenterAgentId = telemarketerID;

                        rcS.Insert(CEmployeeFields.s_CallCenterAgentRoleId, dataLoan.sEmployeeCallCenterAgentId, telemarketerID, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.CallCenterAgent, telemarketerID);

                        }

                    }

                    if (m_realEstateNewRB.Checked)
                    {
                        dataLoan.sEmployeeRealEstateAgentId = realEstateID;

                        rcS.Insert(CEmployeeFields.s_RealEstateAgentRoleId, dataLoan.sEmployeeRealEstateAgentId, realEstateID, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.Realtor, realEstateID);

                        }

                    }

                    if (m_lenderExecNewRB.Checked)
                    {
                        dataLoan.sEmployeeLenderAccExecId = lenderExecID;

                        rcS.Insert(CEmployeeFields.s_LenderAccountExecRoleId, dataLoan.sEmployeeLenderAccExecId, lenderExecID, E_RoleChangeT.Update);
                    }

                    if (m_lockDeskNewRB.Checked)
                    {
                        dataLoan.sEmployeeLockDeskId = lockDeskID;

                        rcS.Insert(CEmployeeFields.s_LockDeskRoleId, dataLoan.sEmployeeLockDeskId, lockDeskID, E_RoleChangeT.Update);
                    }

                    if (m_underwriterNewRB.Checked)
                    {
                        dataLoan.sEmployeeUnderwriterId = underwriterID;

                        rcS.Insert(CEmployeeFields.s_UnderwriterRoleId, dataLoan.sEmployeeUnderwriterId, underwriterID, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.Underwriter, underwriterID);

                        }

                    }

                    if (m_managerNewRB.Checked)
                    {
                        dataLoan.sEmployeeManagerId = managerID;

                        rcS.Insert(CEmployeeFields.s_ManagerRoleId, dataLoan.sEmployeeManagerId, managerID, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.Manager, managerID);

                        }

                    }

                    if (m_closerNewRB.Checked)
                    {
                        dataLoan.sEmployeeCloserId = closerID;
                        rcS.Insert(CEmployeeFields.s_CloserId, dataLoan.sEmployeeCloserId, closerID, E_RoleChangeT.Update);
                    }

                    if (m_shipperNewRB.Checked)
                    {
                        dataLoan.sEmployeeShipperId = shipperId;

                        rcS.Insert(CEmployeeFields.s_ShipperId, dataLoan.sEmployeeShipperId, shipperId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.Shipper, shipperId);
                        }
                    }

                    if (m_funderNewRB.Checked)
                    {
                        dataLoan.sEmployeeFunderId = funderId;

                        rcS.Insert(CEmployeeFields.s_FunderRoleId, dataLoan.sEmployeeFunderId, funderId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.Funder, funderId);
                        }
                    }

                    if (m_postCloserNewRB.Checked)
                    {
                        dataLoan.sEmployeePostCloserId = postCloserId;

                        rcS.Insert(CEmployeeFields.s_PostCloserId, dataLoan.sEmployeePostCloserId, postCloserId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.PostCloser, postCloserId);
                        }
                    }

                    if (m_insuringNewRB.Checked)
                    {
                        dataLoan.sEmployeeInsuringId = insuringId;

                        rcS.Insert(CEmployeeFields.s_InsuringId, dataLoan.sEmployeeInsuringId, insuringId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.Insuring, insuringId);
                        }
                    }

                    if (m_collateralAgentNewRB.Checked)
                    {
                        dataLoan.sEmployeeCollateralAgentId = collateralAgentId;

                        rcS.Insert(CEmployeeFields.s_CollateralAgentId, dataLoan.sEmployeeCollateralAgentId, collateralAgentId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.CollateralAgent, collateralAgentId);
                        }
                    }

                    if (m_docDrawerNewRB.Checked)
                    {
                        dataLoan.sEmployeeDocDrawerId = docDrawerId;

                        rcS.Insert(CEmployeeFields.s_DocDrawerId, dataLoan.sEmployeeDocDrawerId, docDrawerId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.DocDrawer, docDrawerId);
                        }
                    }

                    if (m_creditAuditorNewRB.Checked)
                    {
                        dataLoan.sEmployeeCreditAuditorId = creditAuditorId;

                        rcS.Insert(CEmployeeFields.s_CreditAuditorId, dataLoan.sEmployeeCreditAuditorId, creditAuditorId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.CreditAuditor, creditAuditorId);
                        }
                    }

                    if (m_disclosureDeskNewRB.Checked)
                    {
                        dataLoan.sEmployeeDisclosureDeskId = disclosureDeskId;

                        rcS.Insert(CEmployeeFields.s_DisclosureDeskId, dataLoan.sEmployeeDisclosureDeskId, disclosureDeskId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.DisclosureDesk, disclosureDeskId);
                        }
                    }

                    if (m_juniorProcessorNewRB.Checked)
                    {
                        dataLoan.sEmployeeJuniorProcessorId = juniorProcessorId;

                        rcS.Insert(CEmployeeFields.s_JuniorProcessorId, dataLoan.sEmployeeJuniorProcessorId, juniorProcessorId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.JuniorProcessor, juniorProcessorId);
                        }
                    }

                    if (m_juniorUnderwriterNewRB.Checked)
                    {
                        dataLoan.sEmployeeJuniorUnderwriterId = juniorUnderwriterId;

                        rcS.Insert(CEmployeeFields.s_JuniorUnderwriterId, dataLoan.sEmployeeJuniorUnderwriterId, juniorUnderwriterId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.JuniorUnderwriter, juniorUnderwriterId);
                        }
                    }

                    if (m_legalAuditorNewRB.Checked)
                    {
                        dataLoan.sEmployeeLegalAuditorId = legalAuditorId;

                        rcS.Insert(CEmployeeFields.s_LegalAuditorId, dataLoan.sEmployeeLegalAuditorId, legalAuditorId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.LegalAuditor, legalAuditorId);
                        }
                    }

                    if (m_loanOfficerAssistantNewRB.Checked)
                    {
                        dataLoan.sEmployeeLoanOfficerAssistantId = loanOfficerAssistantId;

                        rcS.Insert(CEmployeeFields.s_LoanOfficerAssistantId, dataLoan.sEmployeeLoanOfficerAssistantId, loanOfficerAssistantId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.LoanOfficerAssistant, loanOfficerAssistantId);
                        }
                    }

                    if (m_purchaserNewRB.Checked)
                    {
                        dataLoan.sEmployeePurchaserId = purchaserId;

                        rcS.Insert(CEmployeeFields.s_PurchaserId, dataLoan.sEmployeePurchaserId, purchaserId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.Purchaser, purchaserId);
                        }
                    }

                    if (m_qcComplianceNewRB.Checked)
                    {
                        dataLoan.sEmployeeQCComplianceId = qcComplianceId;

                        rcS.Insert(CEmployeeFields.s_QCComplianceId, dataLoan.sEmployeeQCComplianceId, qcComplianceId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.QCCompliance, qcComplianceId);
                        }
                    }

                    if (m_secondaryNewRB.Checked)
                    {
                        dataLoan.sEmployeeSecondaryId = secondaryId;

                        rcS.Insert(CEmployeeFields.s_SecondaryId, dataLoan.sEmployeeSecondaryId, secondaryId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.Secondary, secondaryId);
                        }
                    }

                    if (m_servicingNewRB.Checked)
                    {
                        dataLoan.sEmployeeServicingId = servicingId;

                        rcS.Insert(CEmployeeFields.s_ServicingId, dataLoan.sEmployeeServicingId, servicingId, E_RoleChangeT.Update);
                        if (bCopyToOfficialAgent)
                        {
                            PopulateAgent(dataLoan, E_AgentRoleT.Servicing, servicingId);
                        }
                    }

                    #endregion

                    #region handle new status
                    if (m_statusNewRB.Checked)
                    {
                        var newStatus = (E_sStatusT)Tools.GetDropDownListValue(m_statusDDL);
                        if (!dataLoan.BrokerDB.GetEnabledStatusesByChannelAndProcessType(dataLoan.sBranchChannelT, dataLoan.sCorrespondentProcessT).Contains(newStatus)
                           && !Tools.IsStatusLead(newStatus))
                        {
                            genericErrorLoans.Add(
                                Tuple.Create(
                                    dataLoan.sLNm
                                    , dataLoan.sPrimaryNm
                                    , CPageBase.sStatusT_map_rep(newStatus) 
                                        + " is not enabled for your organization for the " 
                                        + Enum.GetName(typeof(E_BranchChannelT), dataLoan.sBranchChannelT)
                                        + " channel"
                                    )
                                );
                            continue;
                        }

                        // 4/22/2016 BS - Case 240838. Batch status change from Lead to Loan or Loan to Lead is not allowed
                        if (Tools.IsStatusLead(currentStatus) && !Tools.IsStatusLead(newStatus))
                        {
                            notConvertedLeadIds.Add(dataLoan.sLId);
                            leadToLoanConversionErrors.Add(Tuple.Create(dataLoan.sLNm, dataLoan.sPrimaryNm));
                        }
                        else if (!Tools.IsStatusLead(currentStatus) && Tools.IsStatusLead(newStatus))
                        {
                            notConvertedLoanIds.Add(dataLoan.sLId);
                            loanToLeadConversionErrors.Add(Tuple.Create(dataLoan.sLNm, dataLoan.sPrimaryNm));
                        }
                        else
                        {
                            dataLoan.sStatusLckd = true;
                            dataLoan.sStatusT = newStatus;

                            //opm 10033 preserve date if its already set av 12 13 2010
                            //Func<CDateTime, CDateTime> setIfEmpty = p => p.IsValid ? p : new CDateTime(DateTime.Now, null); // don't use this, see opm 145740.
                            Func<string, string> setIfEmptyString = p => string.IsNullOrEmpty(p) ? (CDateTime.Create(DateTime.Now)).ToString(dataLoan.m_convertLos) : p;

                            switch (dataLoan.sStatusT)
                            {
                                case E_sStatusT.Lead_New: dataLoan.sLeadD_rep = setIfEmptyString(dataLoan.sLeadD_rep); break;
                                case E_sStatusT.Lead_Canceled: dataLoan.sLeadCanceledD_rep = setIfEmptyString(dataLoan.sLeadCanceledD_rep); break;
                                case E_sStatusT.Lead_Declined: dataLoan.sLeadDeclinedD_rep = setIfEmptyString(dataLoan.sLeadDeclinedD_rep); break;
                                case E_sStatusT.Lead_Other:
                                    dataLoan.sLeadOtherD_rep = setIfEmptyString(dataLoan.sLeadOtherD_rep);
                                    break;

                                case E_sStatusT.Loan_Open: dataLoan.sOpenedD_rep = setIfEmptyString(dataLoan.sOpenedD_rep); break;
                                case E_sStatusT.Loan_Prequal: dataLoan.sPreQualD_rep = setIfEmptyString(dataLoan.sPreQualD_rep); break;
                                case E_sStatusT.Loan_Registered: dataLoan.sSubmitD_rep = setIfEmptyString(dataLoan.sSubmitD_rep); break;
                                case E_sStatusT.Loan_Processing: dataLoan.sProcessingD_rep = setIfEmptyString(dataLoan.sProcessingD_rep); break;
                                case E_sStatusT.Loan_Preapproval: dataLoan.sPreApprovD_rep = setIfEmptyString(dataLoan.sPreApprovD_rep); break;
                                case E_sStatusT.Loan_LoanSubmitted: dataLoan.sLoanSubmittedD_rep = setIfEmptyString(dataLoan.sLoanSubmittedD_rep); break;
                                case E_sStatusT.Loan_Underwriting: dataLoan.sUnderwritingD_rep = setIfEmptyString(dataLoan.sUnderwritingD_rep); break;
                                case E_sStatusT.Loan_Approved: dataLoan.sApprovD_rep = setIfEmptyString(dataLoan.sApprovD_rep); break;
                                case E_sStatusT.Loan_FinalUnderwriting: dataLoan.sFinalUnderwritingD_rep = setIfEmptyString(dataLoan.sFinalUnderwritingD_rep); break;
                                case E_sStatusT.Loan_ClearToClose: dataLoan.sClearToCloseD_rep = setIfEmptyString(dataLoan.sClearToCloseD_rep); break;
                                case E_sStatusT.Loan_Docs: dataLoan.sDocsD_rep = setIfEmptyString(dataLoan.sDocsD_rep); break;
                                case E_sStatusT.Loan_DocsBack: dataLoan.sDocsBackD_rep = setIfEmptyString(dataLoan.sDocsBackD_rep); break;
                                case E_sStatusT.Loan_FundingConditions: dataLoan.sFundingConditionsD_rep = setIfEmptyString(dataLoan.sFundingConditionsD_rep); break;
                                case E_sStatusT.Loan_Funded: dataLoan.sFundD_rep = setIfEmptyString(dataLoan.sFundD_rep); break;
                                case E_sStatusT.Loan_Recorded: dataLoan.sRecordedD_rep = setIfEmptyString(dataLoan.sRecordedD_rep); break;
                                case E_sStatusT.Loan_FinalDocs: dataLoan.sFinalDocsD_rep = setIfEmptyString(dataLoan.sFinalDocsD_rep); break;
                                case E_sStatusT.Loan_Closed: dataLoan.sClosedD_rep = setIfEmptyString(dataLoan.sClosedD_rep); break;
                                case E_sStatusT.Loan_Shipped: dataLoan.sShippedToInvestorD_rep = setIfEmptyString(dataLoan.sShippedToInvestorD_rep); break;
                                case E_sStatusT.Loan_LoanPurchased: dataLoan.sLPurchaseD_rep = setIfEmptyString(dataLoan.sLPurchaseD_rep); break;
                                case E_sStatusT.Loan_OnHold: dataLoan.sOnHoldD_rep = setIfEmptyString(dataLoan.sOnHoldD_rep); break;
                                case E_sStatusT.Loan_Canceled: dataLoan.sCanceledD_rep = setIfEmptyString(dataLoan.sCanceledD_rep); break;
                                case E_sStatusT.Loan_Rejected: dataLoan.sRejectD_rep = setIfEmptyString(dataLoan.sRejectD_rep); break;
                                case E_sStatusT.Loan_Suspended: dataLoan.sSuspendedD_rep = setIfEmptyString(dataLoan.sSuspendedD_rep); break;
                                case E_sStatusT.Loan_PreProcessing: dataLoan.sPreProcessingD_rep = setIfEmptyString(dataLoan.sPreProcessingD_rep); break;
                                case E_sStatusT.Loan_DocumentCheck: dataLoan.sDocumentCheckD_rep = setIfEmptyString(dataLoan.sDocumentCheckD_rep); break;
                                case E_sStatusT.Loan_DocumentCheckFailed: dataLoan.sDocumentCheckFailedD_rep = setIfEmptyString(dataLoan.sDocumentCheckFailedD_rep); break;
                                case E_sStatusT.Loan_PreUnderwriting: dataLoan.sPreUnderwritingD_rep = setIfEmptyString(dataLoan.sPreUnderwritingD_rep); break;
                                case E_sStatusT.Loan_ConditionReview: dataLoan.sConditionReviewD_rep = setIfEmptyString(dataLoan.sConditionReviewD_rep); break;
                                case E_sStatusT.Loan_PreDocQC: dataLoan.sPreDocQCD_rep = setIfEmptyString(dataLoan.sPreDocQCD_rep); break;
                                case E_sStatusT.Loan_DocsOrdered: dataLoan.sDocsOrderedD_rep = setIfEmptyString(dataLoan.sDocsOrderedD_rep); break;
                                case E_sStatusT.Loan_DocsDrawn: dataLoan.sDocsDrawnD_rep = setIfEmptyString(dataLoan.sDocsDrawnD_rep); break;
                                case E_sStatusT.Loan_InvestorConditions: dataLoan.sSuspendedByInvestorD_rep = setIfEmptyString(dataLoan.sSuspendedByInvestorD_rep); break;
                                case E_sStatusT.Loan_InvestorConditionsSent: dataLoan.sCondSentToInvestorD_rep = setIfEmptyString(dataLoan.sCondSentToInvestorD_rep); break;
                                case E_sStatusT.Loan_ReadyForSale: dataLoan.sReadyForSaleD_rep = setIfEmptyString(dataLoan.sReadyForSaleD_rep); break;
                                case E_sStatusT.Loan_SubmittedForPurchaseReview: dataLoan.sSubmittedForPurchaseReviewD_rep = setIfEmptyString(dataLoan.sSubmittedForPurchaseReviewD_rep); break;
                                case E_sStatusT.Loan_InPurchaseReview: dataLoan.sInPurchaseReviewD_rep = setIfEmptyString(dataLoan.sInPurchaseReviewD_rep); break;
                                case E_sStatusT.Loan_PrePurchaseConditions: dataLoan.sPrePurchaseConditionsD_rep = setIfEmptyString(dataLoan.sPrePurchaseConditionsD_rep); break;
                                case E_sStatusT.Loan_SubmittedForFinalPurchaseReview: dataLoan.sSubmittedForFinalPurchaseD_rep = setIfEmptyString(dataLoan.sSubmittedForFinalPurchaseD_rep); break;
                                case E_sStatusT.Loan_InFinalPurchaseReview: dataLoan.sInFinalPurchaseReviewD_rep = setIfEmptyString(dataLoan.sInFinalPurchaseReviewD_rep); break;
                                case E_sStatusT.Loan_ClearToPurchase: dataLoan.sClearToPurchaseD_rep = setIfEmptyString(dataLoan.sClearToPurchaseD_rep); break;
                                case E_sStatusT.Loan_Purchased: dataLoan.sPurchasedD_rep = setIfEmptyString(dataLoan.sPurchasedD_rep); break;
                                case E_sStatusT.Loan_CounterOffer: dataLoan.sCounterOfferD_rep = setIfEmptyString(dataLoan.sCounterOfferD_rep); break;
                                case E_sStatusT.Loan_Withdrawn: dataLoan.sWithdrawnD_rep = setIfEmptyString(dataLoan.sWithdrawnD_rep); break;
                                case E_sStatusT.Loan_Archived: dataLoan.sArchivedD_rep = setIfEmptyString(dataLoan.sArchivedD_rep); break;
                            }
                        }
                    }
                    #endregion

                    // 6/22/2004 dd - Only allow person with Manager role and have
                    // Corporate access level can change branch.

                    if (BrokerUser.IsInRole(ConstApp.ROLE_MANAGER) && BrokerUser.HasPermission(Permission.BrokerLevelAccess))
                    {
                        if (m_branchNewRB.Checked)
                        {
                            dataLoan.AssignBranch(new Guid(m_branchDDL.SelectedItem.Value));
                        }
                    }

                    // 10/13/2010 dd - OPM 41485 - WHen change branch or assign loan officer recalculate TPO.
                    dataLoan.RecalculateTpoValue();

                    if (m_officerChoice_m_Text.Value != "None" &&
                        dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent)
                    {
                        //Change branch to that of loan officer - OPM 51224
                        if (agentID != Guid.Empty)
                        {
                            EmployeeDB emp = new EmployeeDB(agentID, BrokerUser.BrokerId);
                            emp.Retrieve();
                            dataLoan.AssignBranch(emp.BranchID);
                        }
                    }
                    else if (m_externalSecondaryChoice_Text.Value != "None" &&
                        dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
                    {
                        var emp = new EmployeeDB(externalSecondaryId, this.BrokerUser.BrokerId);
                        emp.Retrieve();

                        Guid branchToAssign;

                        if (dataLoan.sCorrespondentProcessT == E_sCorrespondentProcessT.MiniCorrespondent)
                        {
                            branchToAssign = emp.MiniCorrespondentBranchID;
                        }
                        else
                        {
                            branchToAssign = emp.CorrespondentBranchID;
                        }

                        if (branchToAssign != Guid.Empty)
                        {
                            dataLoan.AssignBranch(tempExternalSecondary.BranchId);
                        }
                    }

                    dataLoan.Save();

                    #region send out emails for status change
                    //if (currentStatus != dataLoan.sStatusT)
                    //{
                    //    // 11/18/2004 kb - We now track status changes and
                    //    // email out to the lender account exec and loan
                    //    // officer if the broker option is enabled.
                    //    //
                    //    // 8/26/2005 kb - For now, we send to all assigned
                    //    // employees to make sure all are covered.  A new
                    //    // notification subscription framework that's in
                    //    // the works will obsolete this.  This loop will
                    //    // get obsoleted by a revised notification subscription
                    //    // config module (see case 1800).

                    //    LoanAssignments loanEmp = new LoanAssignments();
                    //    ArrayList empList = new ArrayList();

                    //    loanEmp.Retrieve(dataLoan.sLId);

                    //    foreach (LoanAssignments.Spec lS in loanEmp)
                    //    {
                    //        if (lS.RoleName != "Administrator")
                    //        {
                    //            if (empList.Contains(lS.EmployeeId) == false)
                    //            {
                    //                empList.Add(lS.EmployeeId);
                    //            }
                    //        }
                    //    }

                    //    foreach (Guid sendEmployeeId in empList)
                    //    {
                    //        LoanStatusChanged loanEvt = new LoanStatusChanged();

                    //        loanEvt.Initialize
                    //            (dataLoan.sStatusT_rep
                    //            , BrokerUser.DisplayName
                    //            , sendEmployeeId
                    //            , dataLoan.sLId
                    //            , BrokerUser.ApplicationType
                    //            );

                    //        loanEvt.Send();
                    //    }

                    //    int nCount = dataLoan.GetAgentRecordCount();
                    //    ArrayList agentEmailList = new ArrayList();
                    //    for (int i = 0; i < nCount; i++)
                    //    {
                    //        CAgentFields agent = dataLoan.GetAgentFields(i);

                    //        if (!agent.IsNotifyWhenLoanStatusChange)
                    //            continue;
                    //        if (agentEmailList.Contains(agent.EmailAddr.ToLower()))
                    //            continue;

                    //        agentEmailList.Add(agent.EmailAddr.ToLower());

                    //        LoanStatusChanged loanEvt = new LoanStatusChanged();
                    //        loanEvt.InitializeWithAgent(dataLoan.sStatusT_rep, BrokerUser.DisplayName, agent.RecordId, BrokerUser.BrokerId, dataLoan.sLId, BrokerUser.ApplicationType);
                    //        loanEvt.Send();
                    //    }

                    //}
                    #endregion

                    foreach (RoleChangeEntry rcEntry in rcS)
                    {
                        // 11/11/2004 kb - Send out notification for each
                        // assignment that has changed.  We attempt for
                        // each change, and we only do it on successful
                        // saving of the loan.

                        RoleChange.Mark(sLId, BrokerUser.BrokerId, dataLoan
                            , rcEntry.Prev
                            , rcEntry.Curr
                            , rcEntry.Role
                            , rcEntry.Type
                            );

                        // CommonFunctions.FixLoanTasksOnRoleAssignement(dataLoan.sBrokerId, dataLoan.sLId, rcEntry.Curr, rcEntry.Role);
                    }
                }
                catch (PageDataLoadDenied)
                {
                    accessDeniedLoans.Add(Tuple.Create(dataLoan.ReadOnlysLNm ?? "", ""));
                }
                catch (AccessDenied)
                {
                    if (dataLoan != null)
                    {
                        accessDeniedLoans.Add(Tuple.Create(dataLoan.sLNm, dataLoan.sPrimaryNm));
                    }
                    //per opm  64193 if dataloan did not load they did not even have read access its fine to not show it in the error list.
                }
                catch (CBaseException e)
                {
                    if (dataLoan == null)
                    {
                        continue;
                    }
                    if (e.UserMessage == "OC")
                    {
                        ocErrorLoans.Add(Tuple.Create(dataLoan.sLNm, dataLoan.sPrimaryNm));
                    }
                    else if (e.UserMessage == ErrorMessages.GenericAccessDenied)
                    {
                        accessDeniedLoans.Add(Tuple.Create(dataLoan.sLNm, dataLoan.sPrimaryNm));
                    }
                    else
                    {
                        genericErrorLoans.Add(Tuple.Create(dataLoan.sLNm, dataLoan.sPrimaryNm, e.UserMessage));
                        Tools.LogError("Employee Assignment", e); //so we can better handle it
                    }
                }
                catch (Exception e)
                {
                    if (dataLoan != null)
                    {
                        genericErrorLoans.Add(Tuple.Create(dataLoan.sLNm, dataLoan.sPrimaryNm, string.Empty));
                    }

                    Tools.LogErrorWithCriticalTracking("Employee assignment: Error occurred during delta calculation and notification.", e);
                }
            }

            if (genericErrorLoans.Count > 0 || ocErrorLoans.Count > 0 || accessDeniedLoans.Count > 0 || recentlyModifiedLoans.Count > 0 || leadToLoanConversionErrors.Count > 0 || loanToLeadConversionErrors.Count > 0)
            {
                MainErrorPanel.Visible = true;
                MainPanel.Visible = false;

                AccessDeniedPanel.Visible = accessDeniedLoans.Count > 0;
                AccessDeniedErrorGrid.DataSource = accessDeniedLoans;
                AccessDeniedErrorGrid.DataBind();

                GenericErrorPanel.Visible = genericErrorLoans.Count > 0;
                GenericErrorGrid.DataSource = genericErrorLoans;
                GenericErrorGrid.DataBind();

                OCErrorPanel.Visible = ocErrorLoans.Count > 0;
                OCErrorLoansGrid.DataSource = ocErrorLoans;
                OCErrorLoansGrid.DataBind();

                RecentlyModifiedErrorPanel.Visible = recentlyModifiedLoans.Count > 0;
                RecentlyModifiedLoansGrid.DataSource = recentlyModifiedLoans;
                RecentlyModifiedLoansGrid.DataBind();

                OCErrorMessage.Text = "Assignment failed because ";
                OCErrorMessage.Text = "The Loan Officer, Processor (External), " +
                    "Secondary (External), and Post-Closer (External) must " +
                    "belong to the same originating company. The following loan " +
                    "files have conflicts: ";

                LeadToLoanConversionErrorMessage.Text = "The status of the following leads were not changed because a loan status was selected:";
                LeadToLoanConversionErrorsPanel.Visible = leadToLoanConversionErrors.Count > 0;
                LeadToLoanConversionErrorsGrid.DataSource = leadToLoanConversionErrors;
                LeadToLoanConversionErrorsGrid.DataBind();

                LoanToLeadConversionErrorMessage.Text = "The status of the following loans were not changed because a lead status was selected:";
                LoanToLeadConversionErrorsPanel.Visible = loanToLeadConversionErrors.Count > 0;
                LoanToLeadConversionErrorsGrid.DataSource = loanToLeadConversionErrors;
                LoanToLeadConversionErrorsGrid.DataBind();
            }
            else
            {
                if (convertedLeadToLoan)
                {
                    // Get rid of the trailing comma
                    string idsParam = convertedLeadIds.Substring(0, convertedLeadIds.Length - 1);
                    this.AddInitScriptFunctionWithArgs("f_close", "true" + ",'" + idsParam + "'");
                }
                else
                    this.AddInitScriptFunctionWithArgs("f_close", "false,''");
            }
        }

        private void PopulateAgent(CPageData dataLoan, E_AgentRoleT role, Guid employeeId)
        {
            CAgentFields destAgent = dataLoan.GetAgentOfRole(role, E_ReturnOptionIfNotExist.CreateNew);
            destAgent.ClearExistingData();

            //just deleting contact
            if (employeeId == Guid.Empty)
            {
                destAgent.Update();

                return;
            }

            // 10/26/17 JK - Allow updates to LO licensing information even when reassigning to same LO. opm 447286
            bool bCopyCommissionToAgent = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(employeeId, dataLoan.sLId) == E_AgentRoleT.Other;
            CommonFunctions.CopyEmployeeInfoToAgent(BrokerUser.BrokerId, destAgent, employeeId, bCopyCommissionToAgent);

            destAgent.Update();

        }

        private void ShowFixableError(string message)
        {
            FixableErrorMessage.Text = message;
            ErrorRow.Visible = true;
        }



    }

}
