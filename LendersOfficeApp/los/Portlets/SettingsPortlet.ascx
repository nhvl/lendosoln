<%@ Control Language="c#" AutoEventWireup="True" Codebehind="SettingsPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.SettingsPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<script language="javascript">
	function onMyProfileClick()
	{
		showModal('/los/myprofile.aspx', null, null, null, null, { hideCloseButton:true, width:800 } );
		return false;
	}
	function onDownloadClick()
	{
		showModal('/common/download.aspx', null, null, null, null, { hideCloseButton:true });
		return false;
    }

    function onDropboxClick() {
        var args = { Applied: false };

        window.open(<%=AspxTools.SafeUrl(Tools.GetEDocsLink(Tools.VRoot + "/los/UploadedFiles/FileList.aspx"))%>,
                    'FileDropbox', 
                    "width=700,height=760,center=yes,resizable=yes,scrollbars=yes,status=yes,help=no");
        return false;
    }

    function updateDropboxFiles(count) {
        var count = count || "empty";
        document.getElementById("DropboxLink").textContent = "EDoc Dropbox (" + count + ")";
    }
    
    function onResourcesClick()
    {
        window.open(document.getElementById("EmployeeResourcesUrl").value,
                    'Resources', 
                    "center=yes,resizable=yes,scrollbars=yes,status=yes,help=no");
        return false;       
    }
    function onRegMobileDeviceClick()
    {
        showModal('/los/registermobiledevice.aspx', null, null, null, null, {hideCloseButton: true} );
		return false;
	}


</script>
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" class="Portlet">
	<TR>
		<TD class="PortletHeader">Your Settings</TD>
	</TR>
	<TR>
		<TD nowrap>
			<img src="../images/bullet.gif" align="absMiddle">&nbsp; <A href="#" onclick="onMyProfileClick();" class="PortletLink">
				Your Profile</A>
			<br>
			<span id="RegisterMobileDeviceSpan">
                <img src="../images/bullet.gif" align="absMiddle">&nbsp; <A href="#" onclick="onRegMobileDeviceClick();" class="PortletLink">
				    Register Mobile Device</A>
			    <br>
			</span>
			<asp:PlaceHolder runat="server" ID="DropboxPlaceholder">
			<img src="../images/bullet.gif" align="absMiddle">&nbsp; <a href="#" onclick="onDropboxClick();" class="PortletLink" id="DropboxLink">
				EDoc Dropbox (<%=LendersOffice.AntiXss.AspxTools.HtmlString(UploadedCount) %>)</a>
			<br>
			</asp:PlaceHolder>
			<span runat="server" ID="EmployeeResourcesSpan">
			    <img src="../images/bullet.gif" align="absMiddle">&nbsp; <a href="#" onclick="onResourcesClick();" class="PortletLink">
				    Your Resources</a>
			    <br>
			</span>
			<img src="../images/bullet.gif" align="absMiddle">&nbsp; <a href="#" onclick="onDownloadClick();" class="PortletLink">
				Download</a>
			<br>
			
		</TD>
	</TR>
	<tr>
		<td><img src="../images/shadow_line.gif" width="130" height="2"></td>
	</tr>	
</TABLE>

<script>
(function(){
    var EnableLqbMobileApp = document.getElementById('EnableLqbMobileApp');
    var RegisterMobileDeviceSpan = document.getElementById('RegisterMobileDeviceSpan');
    var EmployeeResourcesUrl = document.getElementById('EmployeeResourcesUrl');
    var EmployeeResourcesSpan = document.getElementById('SettingsPortlet1_EmployeeResourcesSpan');
	
	if (EnableLqbMobileApp != null) {
    	RegisterMobileDeviceSpan.style.display = EnableLqbMobileApp.value === 'True' ? '' : 'none';
	}
	EmployeeResourcesSpan.style.display = EmployeeResourcesUrl.value == "" ? "none" : "";
})();
</script>
