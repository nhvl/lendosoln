﻿<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="LoanPoolPortlet.ascx.cs"
    Inherits="LendersOfficeApp.los.Portlets.LoanPoolPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="DataAccess" %>
<%@ Import Namespace="LendersOffice.Security" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace=" LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="DeleteLoanButton" Src="DeleteLoanButton.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AssignLoanButton" Src="AssignLoanButton.ascx" %>
<%@ Register TagPrefix="uc1" TagName="RefreshButton" Src="RefreshButton.ascx" %>

<script type="text/javascript">
    jQuery(function($) {
        //We may be in jq 1.6 or jq 1.7, so don't use $.on.
        $('#PoolPortlet td.action').click(function() {
            var poolId = $(this).find('.pool-id').val();
            var poolName = $(this).find('.pool-name').text();

            $('#PoolPortlet').trigger('PoolSelected', [poolId, poolName]);
            if ($('#OpenPoolOnSelect').length) openPoolDetails(poolId);
        });
    });

    function openPoolDetails(poolId) {
        var w = screen.availWidth - 10;
        var h = screen.availHeight - 50;

        var win = window.open('MortgagePools/PoolDetails.aspx?PoolId=' + poolId, 'blank', 'width=' + w + 'px,height=' + h + 'px,left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
        win.focus();
    }
</script>
<style type="text/css">

#PoolPortlet .Filter select
{
    margin-right: 20px;
}

#PoolPortlet div
{
    margin-bottom: 10px;
}

#PoolPortlet span
{
    margin-right: 8px;
}

#PoolPortlet span.PackageNumPartial input
{
    vertical-align:bottom
}
#PoolPortlet span.PackageNumPartial input[type="checkbox"] { vertical-align: middle; }
#PoolPortlet td.action
{
    cursor: pointer;
    text-decoration: underline;
    color: Blue;
}

#PoolPortlet td.action:hover
{
    color: #0099FF;
    text-decoration: none;
}
</style>
<div id="PoolPortlet">
<div class="Filter">
    <div>
        <ml:EncodedLabel ID="Label1" runat="server" EnableViewState="False" ForeColor="Black" Font-Bold="True">Pool Status</ml:EncodedLabel>
        <select id="m_PoolStatus" name="sPoolStatus" runat="server">
            <option value="-1">Any</option>
            <option value="1" selected="true">Open</option>
            <option value="0">Closed</option>
        </select>
        <ml:EncodedLabel ID="Label2" runat="server" EnableViewState="False" ForeColor="Black" Font-Bold="True">Pool Agency</ml:EncodedLabel>
        <select id="m_PoolAgency" name="sPoolAgency" runat="server">
            <option value="-1" selected="true">Any</option>
            <option value="1">Fannie Mae</option>
            <option value="2">Freddie Mac</option>
            <option value="3">Ginnie Mae</option>
        </select>
        <ml:EncodedLabel ID="Label3" runat="server" EnableViewState="False" ForeColor="Black" Font-Bold="True">Amortization Type</ml:EncodedLabel>
        <select id="m_AmortizationType" name="sAmortizationType" runat="server">
            <option value="-1" selected="true">Any</option>
            <option value="0">Fixed</option>
            <option value="1">ARM</option>
        </select>
    </div>

    <div>
        <ml:EncodedLabel runat="server" EnableViewState="False" CssClass="PackageNum" ForeColor="Black" Font-Bold="True">Pool ID </ml:EncodedLabel>
        <asp:TextBox ID="m_InternalId" runat="server"></asp:TextBox>
        <asp:CheckBox ID="m_PoolIdPartialMatch" runat="server" EnableViewState="False" ForeColor="Black" CssClass="PackageNumPartial"
            Text="Return partial match" />
    </div>
    <div>
        <ml:EncodedLabel ID="Label4" runat="server" EnableViewState="False" CssClass="PackageNum" ForeColor="Black" Font-Bold="True">Pool/Package Number </ml:EncodedLabel>
        <asp:TextBox ID="m_PoolNumber" runat="server"></asp:TextBox>
        <asp:CheckBox ID="m_PoolNumPartialMatch" runat="server" EnableViewState="False" ForeColor="Black" CssClass="PackageNumPartial"
            Text="Return partial match" />
    </div>
    <div>
        <ml:EncodedLabel runat="server" EnableViewState="False" CssClass="PackageNum" Style="margin-right: 13px" ForeColor="Black" Font-Bold="True">Commitment Number</ml:EncodedLabel>
        <asp:TextBox ID="m_CommitmentNumber" runat="server"></asp:TextBox>
        <asp:CheckBox ID="m_CommitmentNumberPartialMatch" runat="server" EnableViewState="False" ForeColor="Black" CssClass="PackageNumPartial"
            Text="Return partial match" />
    </div>
</div>
<div>
    <asp:Button runat="server" ID="m_submit" Text="Search" Style="margin-top: 5px;" />
    <br />
    <ml:EncodedLabel runat="server" ID="m_displayCount" Style="color: Black"></ml:EncodedLabel>
</div>
<div>
    <asp:Button runat="server" ID="m_newPool" Text="Create New Pool" OnClick="CreatePool" />
</div>
<div>
    <ml:CommonDataGrid runat="server" ID="m_PoolGrid" DataKeyField="PoolId">
        <Columns>
            <asp:BoundColumn DataField="PoolId" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="InternalId" HeaderText="Pool ID" SortExpression="InternalId"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Pool Number" SortExpression="PoolNumberByAgency" ItemStyle-CssClass="action">
                <ItemTemplate>
                    <span class="pool-name">
                        <%# AspxTools.HtmlString(ValueOrDefault(DataBinder.Eval(Container.DataItem, "PoolNumberByAgency").ToString(), "(NEW)"))%>
                    </span>
                    <input type="hidden" class="pool-id" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PoolId").ToString()) %>" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="PoolStatus" HeaderText="Pool Status" SortExpression="PoolStatus">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="AgencyT" HeaderText="Pool Agency" SortExpression="AgencyT">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="AmortizationT" HeaderText="Amortization Type" SortExpression="AmortizationT">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="CommitmentNum" HeaderText="Commitment Number" SortExpression="CommitmentNum">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="DeliveredD" HeaderText="Pool Delivery Date" SortExpression="DeliveredD">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="SecurityR" HeaderText="Security Rate" SortExpression="SecurityR">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="LoanCount" HeaderText="Number of Loans" SortExpression="LoanCount">
            </asp:BoundColumn>
            <asp:BoundColumn DataField="TotalCurrentBalance" HeaderText="Total Current Balance"
                SortExpression="TotalCurrentBalance"></asp:BoundColumn>
        </Columns>
    </ml:CommonDataGrid>
</div>
</div>
