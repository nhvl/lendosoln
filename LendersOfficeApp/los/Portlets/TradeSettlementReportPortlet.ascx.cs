﻿namespace LendersOfficeApp.los.Portlets
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using ExpertPdf.HtmlToPdf;
    using ExpertPdf.HtmlToPdf.PdfDocument;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.CapitalMarkets;
    using LendersOffice.Security;
    using PdfRasterizerLib;

    public partial class TradeSettlementReportPortlet : System.Web.UI.UserControl
    {
        #region Properties
        private Guid BrokerID
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
        }

        private bool RenderForPdf
        {
            get { return RequestHelper.GetBool("toPdf"); }
        }

        private string SearchCookieName
        {
            get { return String.Format("SRSearchConfig{0}", BrokerUserPrincipal.CurrentPrincipal.UserId); }
        }

        private string BaseUrl
        {
            get { return Request.Url.Scheme + "://" + Request.Url.Authority + '/'; }
        }

        private const string REPORT_TITLE = "Settlement Report";

        private string LogoSrcUrl
        {
            get
            {
                var pmlSiteId = BrokerDB.RetrieveById(BrokerID).PmlSiteID;
                return TradeReportUtilities.HasLogo(pmlSiteId) ? ResolveUrl("~/LogoPL.aspx?id=" + pmlSiteId.ToString()) : "";
            }
        }

        private readonly SearchConfig DEFAULT_SEARCH_CONFIG = new SearchConfig(true, E_FromDateT.Today.ToString("d"), "",
            true, E_ToDateT.Today.ToString("d"), "");
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            var basepage = Page as BasePage;
            if (basepage == null)
            {
                throw new Exception("Cannot use the Trade Portal on something that is not a BasePage");
            }

            if (!this.Visible) return;
            basepage.EnableJqueryMigrate = false;
            basepage.RegisterCSS("TradeSettlementReportPortlet.css");
            basepage.RegisterJsScript("TradeSettlementReportPortlet.js");
            basepage.RegisterJsScript("utilities.js");

            if (!Page.IsPostBack)
            {
                Tools.Bind_FromDateDDL(FromD_DDL);
                Tools.Bind_ToDateDDL(ToD_DDL);
                ReportTitle.Value = REPORT_TITLE;
                LogoSrc.Value = LogoSrcUrl;
                BaseStyleSheet.Value = ResolveUrl("~/css/TradeReport.css");
                TradeReportStyleSheet.Value = ResolveUrl("~/css/stylesheetnew.css");
                
                var config = RetrieveSearchConfigFromCookie();
                LoadSearchConfig(config);
                AssignData();
            }

            if (RenderForPdf)
            {
                ControlsPanel.Visible = false;
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!RenderForPdf)
            {
                base.Render(writer);
                return;
            }

            var sw = new StringWriter();
            var htmlWriter = new HtmlTextWriter(sw);
            base.Render(htmlWriter);

            var stylesheets = new List<string>()
                {
                    this.ResolveUrl("~/css/TradeReport.css"),
                    this.ResolveUrl("~/css/stylesheetnew.css")
                };
            string swString = sw.ToString();
            var wrappedHtml = TradeReportUtilities.GenerateHtmlForPdf(REPORT_TITLE, LogoSrcUrl, swString, stylesheets, true);
            var fileName = "SettlementReport.pdf";

            if (ConstStage.UsePDFRasterizerServiceForPDFConversion)
            {
                string htmlFile = TempFileUtils.NewTempFilePath();

                TextFileHelper.WriteString(htmlFile, swString, true);

                PDFConversionOptions options = new PDFConversionOptions()
                {
                    LeftMargin = 10,
                    TopMargin = 10,
                    RightMargin = 10,
                    BottomMargin = 10,
                    PageSize = (int)PdfPageSize.Letter,
                    PageOrientation = (int)PDFPageOrientation.Landscape,
                    ShowPageNumber = true,
                    PageNumberText = "Page"
                };

                string pdfFile = Tools.ConvertHtmlFileToPdfFile(BaseUrl, htmlFile, options);
                RequestHelper.SendFileToClient(Context, pdfFile, "application/pdf", fileName);
                Response.End();
                return;
            }

            var pdf = TradeReportUtilities.GetPdfFromHtml(BaseUrl, wrappedHtml);

            Response.Clear();
            Response.ClearHeaders();
            Response.Filter = null;
            pdf.Save(Response, false, fileName);
            pdf.Close(); // Need to cleanup resource.
            Response.Flush();
            Response.End();
        }

        private IEnumerable<TradeGroup> GetDataSource()
        {
            var f = new TradeSettlementReportFilter(this);
            var baseTrades = Trade.GetTradesByBroker(BrokerID).Filter(f).ToList();

            var groups = baseTrades
                .GroupBy(t => new { t.DealerInvestor, t.Description_rep, t.Type_rep });
            var tradeGroups = groups.Select(g =>
            {
                var header = string.Format("{0} - {1} - {2}", g.Key.DealerInvestor,
                    g.Key.Description_rep, g.Key.Type_rep);
                return new TradeGroup(header, g.ToList());
            }).OrderBy(tg => tg.Header);

            return tradeGroups;
        }

        private void AssignData()
        {
            Trades.DataSource = GetDataSource();
            Trades.DataBind();
        }

        #region Click Handling
        protected void RefreshClicked(object sender, EventArgs e)
        {
            StoreSearchConfigToCookie();
            AssignData();
        }

        protected void ExportToCsvClicked(object sender, EventArgs e)
        {
            var tradeGroups = GetDataSource();
            var exporter = new TradeExporter("Settlement Report", tradeGroups, true);
            var csv = exporter.ExportToCsv();

            ExportFile("SettlementReport.csv", csv);
        }

        protected void ExportToTxtClicked(object sender, EventArgs e)
        {
            var tradeGroups = GetDataSource();
            var exporter = new TradeExporter("Settlement Report", tradeGroups, true);
            var txt = exporter.ExportToTxt();

            ExportFile("SettlementReport.txt", txt);
        }

        private void ExportFile(string fileName, string fileContent)
        {
            Response.ClearHeaders();
            Response.Filter = null; // Need to remove any compression filtering, otherwise it comes out garbled.
            Response.AppendHeader("Content-Type", "text/plain");
            Response.AppendHeader("Content-Disposition", $"attachment; filename=\"{fileName}\"");
            Response.Output.Write(fileContent);
            Response.Flush();
            Response.End();
        }
        #endregion

        #region Getting Dates
        protected string FromDateStr
        {
            get { return UseFromTB.Checked ? FromD_TB.Text : FromD_DDL.SelectedValue; }
        }

        protected DateTime FromDate
        {
            get
            {
                if (UseFromTB.Checked) return GetDateTimeFromTB(FromD_TB, DateTime.MinValue);
                return Tools.GetDateTimeForFromD((E_FromDateT)Tools.GetDropDownListValue(FromD_DDL));
            }
        }

        protected string ToDateStr
        {
            get { return UseToTB.Checked ? ToD_TB.Text : ToD_DDL.SelectedValue; }
        }

        protected DateTime ToDate
        {
            get
            {
                if (UseToTB.Checked) return GetDateTimeFromTB(ToD_TB, DateTime.MaxValue);
                return Tools.GetDateTimeForToD((E_ToDateT)Tools.GetDropDownListValue(ToD_DDL));
            }
        }


        private DateTime GetDateTimeFromTB(TextBox tb, DateTime defaultVal)
        {
            var dt = defaultVal;
            try
            {
                dt = DateTime.Parse(tb.Text);
            }
            catch (FormatException)
            {
                Tools.LogWarning("Trying to parse invalid datetime string.");
            }
            return dt;
        }

        #endregion

        #region Search Configuration
        private SearchConfig GetSearchConfig()
        {
            return new SearchConfig(UseFromDDL.Checked, FromD_DDL.SelectedValue,
                FromD_TB.Text, UseToDDL.Checked, ToD_DDL.SelectedValue,
                ToD_TB.Text);
        }

        private void StoreSearchConfigToCookie()
        {   
            var serializedConfig = ObsoleteSerializationHelper.JavascriptJsonSerialize(GetSearchConfig());
            RequestHelper.StoreToCookie(SearchCookieName, serializedConfig, SmallDateTime.MaxValue);
        }

        private SearchConfig RetrieveSearchConfigFromCookie()
        {
            var cookie = Request.Cookies.Get(SearchCookieName);
            if (cookie == null)
            {
                return DEFAULT_SEARCH_CONFIG;
            }

            SearchConfig config = null;
            try
            {
                config = ObsoleteSerializationHelper.JavascriptJsonDeserializer<SearchConfig>(cookie.Value);
            }
            catch (ArgumentException) { }
            catch (InvalidOperationException) { }

            return config;
        }

        private void LoadSearchConfig(SearchConfig config)
        {
            if (config == null)
            {
                config = DEFAULT_SEARCH_CONFIG;
            }

            UseFromDDL.Checked = config.UseFromDDL;
            UseFromTB.Checked = !config.UseFromDDL;
            Tools.SetDropDownListValue(FromD_DDL, config.FromD_DDLVal);
            FromD_TB.Text = config.FromD_TBVal;
            
            UseToDDL.Checked = config.UseToDDL;
            UseToTB.Checked = !config.UseToDDL;
            Tools.SetDropDownListValue(ToD_DDL, config.ToD_DDLVal);
            ToD_TB.Text = config.ToD_TBVal;
        }

        public class SearchConfig
        {
            public bool UseFromDDL { get; set; }
            public string FromD_DDLVal { get; set; }
            public string FromD_TBVal { get; set; }
            public bool UseToDDL { get; set; }
            public string ToD_DDLVal { get; set; }
            public string ToD_TBVal { get; set; }

            public SearchConfig(bool useFromDDL, string fromD_DDLVal, string fromD_TBVal,
                bool useToDDL, string toD_DDLVal, string toD_TBVal)
            {
                UseFromDDL = useFromDDL;
                FromD_DDLVal = fromD_DDLVal;
                FromD_TBVal = fromD_TBVal;
                UseToDDL = useToDDL;
                ToD_DDLVal = toD_DDLVal;
                ToD_TBVal = toD_TBVal;
            }
            
            public SearchConfig()
            {
            }
        }
        #endregion

        private class TradeSettlementReportFilter : TradeFilter
        {
            private TradeSettlementReportPortlet Source;
            public TradeSettlementReportFilter(TradeSettlementReportPortlet source)
            {
                Source = source;
            }

            public override Guid BrokerId
            {
                get { return Source.BrokerID; }
            }

            public override DateTime? SettlementDateLowerBound
            {
                get { return Source.FromDate; }
            }

            public override DateTime? SettlementDateUpperBound
            {
                get { return Source.ToDate; }
            }

            #region Unused Filter Elements
            public override TradeStatus? Status
            {
                get { return null; }
            }

            public override string TradeNum
            {
                get { return null; }
            }

            public override bool TradeNumPartialMatch
            {
                get { return false; }
            }

            public override E_TradeType? Type
            {
                get { return null; }
            }

            public override E_TradeDirection? Direction
            {
                get { return null; }
            }

            public override int? Month
            {
                get { return null; }
            }

            public override Guid? DescriptionId
            {
                get { return null; }
            }

            public override string Coupon
            {
                get { return null; }
            }

            public override bool FilterTradesInPools
            {
                get { return false; }
            }
            #endregion Unused Filter Elements
        }

    }
}
