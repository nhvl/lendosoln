<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CapitalMarketsPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.CapitalMarketsPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<% if( HasCapitalMarketsAccess ) { %>
<script language="javascript">
<!--
function onTradeClick()
{
	var win = window.open(gVirtualRoot + '/los/CapitalMarkets/Trades.aspx','Trades', 'scrollbars=yes,toolbar=no,resizable=yes');
	win.focus() ;
}

//-->
</script>
<TABLE class="Portlet" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
	<TR>
		<TD noWrap>
			<font class="PortletHeader" style="HEIGHT: 20px">Capital Markets</font>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">
		    <a href="#" onclick="onTradeClick();" class="PortletLink" title="Track trades in the MBS-TBA Market">
		        Trades
			</a>
			
		</TD>
	</TR>
	<tr>
		<td><img src="../images/shadow_line.gif" width="130" height="2"></td>
	</tr>
</TABLE>

<% } %>