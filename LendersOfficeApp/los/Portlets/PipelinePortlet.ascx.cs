using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.QueryProcessor;
using LendersOffice.Reports;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOfficeApp.los.admin;
using ConfigSystem.DataAccess;
using LendersOffice.ConfigSystem;
using System.Collections.Generic;
using LendersOffice.AntiXss;
using CommonLib;
using LendersOffice.Integration.GenericFramework;

namespace LendersOfficeApp.los.Portlets
{
	/// <summary>
	/// Summary description for PipelinePortlet.
	/// 
	/// 4/1/2005 kb - Implemented as part of query driven pipeline
	/// task: case #932.
	/// </summary>

	public partial  class PipelinePortlet : System.Web.UI.UserControl , IAutoLoadUserControl
	{
		protected LendersOfficeApp.los.Portlets.AssignLoanButton    m_assignButton;
		protected LendersOfficeApp.los.Portlets.DuplicateLoanButton m_copySelected;
		protected LendersOfficeApp.los.Portlets.DeleteLoanButton    m_deleteButton;
		protected LendersOfficeApp.los.Portlets.RefreshButton       m_refreshPanel;
		protected System.Web.UI.WebControls.Repeater                   m_headerSet;

        private const string ROLE_DEFAULT_PIPELINE_NAME = "Simple pipeline";
		private LoanReporting m_lR = new LoanReporting();
		private ArrayList     m_lC = new ArrayList();
        private CPipelineView m_tabView;

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{
			set { Page.ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." ); }
		}


		/// <summary>
		/// Initialize this control.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{

            //IConfigRepository m_configRepository = ConfigHandler.GetRepository();  
            //ReleaseConfigData data = m_configRepository.LoadActiveRelease(Broker.BrokerID);
            ShowCircumstances.Value = bool.FalseString;

			// Check visibility for this control.
			try
			{
				// 10/8/2004 kb - We need to prevent lender account execs
				// from deleting and duplicating.

                // 12/10/2013 gf - opm 145015 New roles should have access to assign/duplicate/delete.

				bool showIt = false;
                if (BrokerUser.HasAtLeastOneRole(new E_RoleT[] { 
                    E_RoleT.Manager, E_RoleT.Accountant, E_RoleT.Processor,
                    E_RoleT.LenderAccountExecutive, E_RoleT.LoanOfficer, 
                    E_RoleT.RealEstateAgent, E_RoleT.LoanOpener,
                    E_RoleT.LockDesk, E_RoleT.Underwriter, E_RoleT.Closer, 
                    E_RoleT.CreditAuditor, E_RoleT.DisclosureDesk, 
                    E_RoleT.JuniorProcessor, E_RoleT.JuniorUnderwriter,
                    E_RoleT.LegalAuditor, E_RoleT.LoanOfficerAssistant,
                    E_RoleT.Purchaser, E_RoleT.QCCompliance, E_RoleT.Secondary, 
                    E_RoleT.Servicing, E_RoleT.CallCenterAgent}))
                {
                    showIt = true;
                }
				
				m_assignButton.Visible = showIt;
				m_copySelected.Visible = showIt;
				m_deleteButton.Visible = showIt;

                m_tabView = Tools.GetMainTabsSettingXmlContent(BrokerUser.ConnectionInfo, BrokerUser.UserId);
            }
			catch( CBaseException e )
			{
				// Oops!

				if( e.UserMessage != "" )
				{
					Tools.LogError( ErrorMessage = "Failed to load web form: " + e.UserMessage , e );
				}
				else
				{
					Tools.LogError( ErrorMessage = "Failed to load web form" , e );
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to load web form" , e );
			}
		}

		/// <summary>
		/// Render this control.
		/// </summary>

		private void PagePreRender( object sender , System.EventArgs a )
		{
			// Load the current report.  We want latest every time the
			// user posts back.
			//
			// 4/1/2005 kb - Implemented as part of query driven pipeline
			// task: case 932.

            try
            {
                // Get the currently selected report and render it in our
                // pipeline.

                Report rR;

                // 11/13/2014 ejm - Get the sort order from the tab view
                m_sortOrder.Value = "";
                if (!string.IsNullOrEmpty(m_tabView.Active.SortOrder))
                {
                    m_sortOrder.Value = m_tabView.Active.SortOrder;
                }

                E_ReportExtentScopeT scope = E_ReportExtentScopeT.Assign;
                Guid reportId = Guid.Empty;

                // 2/7/2006 dd - OPM 4036 - Cap the pipeline.
                m_lR.MaxViewableRowCount = LendersOffice.Constants.ConstAppDavid.MaxViewableRecordsInPipeline;
                m_lR.MaxSqlRowCount = ConstStage.MaxSqlRecordsForPipeline;


                //if (Broker.IsAllowSharedPipeline) //opm 34381 fs 11/09/09 - Enable Shared Pipeline for everybody. If no perf hit remove broker option completely.
                //{
                if (m_tabView.Count != 0)
                {
                    // Get scope and id from current tab's settings
                    CTab tab = m_tabView.Active;
                    reportId = tab.ReportId;
                    scope = tab.Scope;
                }
                //}

                if (reportId == LoanReporting.DefaultSimplePipelineReportId || reportId == LoanReporting.DefaultSimplePipelineNonPMLReportId)
                { //Make sure the proper default pipeline is loaded based on broker settings.
                    reportId = (this.BrokerUser.BrokerDB.HasLenderDefaultFeatures) ? LoanReporting.DefaultSimplePipelineReportId : LoanReporting.DefaultSimplePipelineNonPMLReportId;
                }

                bool usingRoleDefaultReport = reportId == Guid.Empty;
                try
                {
                    rR = m_lR.Pipeline(reportId, BrokerUser, m_sortOrder.Value, scope);

                    if (rR.Stats.HitCount == ConstAppDavid.MaxViewableRecordsInPipeline)
                    {
                        m_tooManyLabel.Text = string.Format("Too many files -- displaying first {0}.", ConstAppDavid.MaxViewableRecordsInPipeline);
                        m_tooManyLabelbottom.Text = m_tooManyLabel.Text;
                    }

                    if (rR == null)
                    {
                        throw new InvalidOperationException("Pipeline result is null.");
                    }
                }
                catch (ArgumentException)
                {
                    // Oops!

                    m_rowsPanel.Visible = false;
                    m_emptyNote.Visible = false;
                    m_errorNote.Visible = false;

                    m_isMissing.Visible = true;

                    throw;
                }
                catch (PermissionException)
                {
                    // Oops!
                    m_rowsPanel.Visible = false;
                    m_emptyNote.Visible = false;
                    m_errorNote.Visible = false;
                    m_isMissing.Visible = false;
                    m_permissionNote.Visible = true;
                    throw;
                }
                catch
                {
                    // Oops!

                    m_rowsPanel.Visible = false;
                    m_isMissing.Visible = false;
                    m_emptyNote.Visible = false;

                    m_errorNote.Visible = true;

                    throw;
                }

                if (rR.Columns.Count > 0)
                {
                    foreach (Column rC in rR.Columns)
                    {
                        m_lC.Add(rC);
                    }
                }

                if (rR.Tables.Count > 0)
                {

                    // Load up the access control content so we can validate
                    // which loans are editable, viewable, etc.  We do this
                    // once, here, so that binding can use the hash table of
                    // results without hitting the db each time.

                    ArrayList toDo = new ArrayList();
                    ArrayList allTables = new ArrayList();

                    foreach (Rows rT in rR.Tables)
                    {
                        toDo.Add(rT);
                    }

                    for (int i = 0; i < toDo.Count; ++i)
                    {
                        bool isContent = false;

                        foreach (Object dO in toDo[i] as Rows)
                        {
                            Row dR = dO as Row;

                            if (dR != null)
                            {
                                if (isContent == false)
                                {
                                    allTables.Add(toDo[i]);

                                    isContent = true;
                                }
                            }
                            else
                            {
                                toDo.Add(dO);
                            }
                        }
                    }

                    try
                    {

                        m_rowTables.DataSource = allTables;
                        m_rowTables.DataBind();

                    }
                    catch
                    {
                        // Bust!

                        m_isMissing.Visible = false;
                        m_emptyNote.Visible = false;
                        m_rowsPanel.Visible = false;

                        m_errorNote.Visible = true;

                        throw;
                    }

                    m_isMissing.Visible = false;
                    m_emptyNote.Visible = false;
                    m_errorNote.Visible = false;

                    m_rowsPanel.Visible = true;
                }
                else
                {
                    m_isMissing.Visible = false;
                    m_errorNote.Visible = false;
                    m_rowsPanel.Visible = false;

                    m_emptyNote.Visible = true;
                }

                if (usingRoleDefaultReport)
                {
                    m_reportTitle.Text = ROLE_DEFAULT_PIPELINE_NAME;
                }
                else
                {
                    m_reportTitle.Text = rR.Label.Title;
                }
                //if (Broker.IsAllowSharedPipeline)
                //{
                m_reportTitle.Text += (scope == E_ReportExtentScopeT.Assign) ? " [My loans]" : " [All loans]";
                //}
            }
            catch (PermissionException e)
            {
                Tools.LogError("User tried to run pipeline that contains restricted fields. Error Message: " + e.UserMessage, e);
            }
            catch (CBaseException e)
            {
                // Oops!

                if (e.UserMessage != "")
                {
                    Tools.LogError(ErrorMessage = "Failed to render web form: " + e.UserMessage, e);
                }
                else
                {
                    Tools.LogError(ErrorMessage = "Failed to render web form", e);
                }
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError(ErrorMessage = "Failed to render web form", e);
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

		/// <summary>
		/// Bind the report element.
		/// </summary>

		protected void BindReport( object sender , RepeaterItemEventArgs a )
		{
			Rows rR = a.Item.DataItem as Rows;

			if( rR != null )
			{
				// Bind each table of our report with its own handler.

				Repeater rList = a.Item.FindControl( "Rows" ) as Repeater;

				if( rList != null )
				{
					rList.DataSource = rR.Items;
					rList.DataBind();
				}

				// Bind each table's header and footer to display.

				Repeater hList = a.Item.FindControl( "m_headerTag" ) as Repeater;
				Repeater fList = a.Item.FindControl( "Footer" ) as Repeater;

				if( hList != null )
				{
					hList.DataSource = m_lC;
					hList.DataBind();
				}

				if( fList != null && rR.Results.Count > 0 )
				{
					ArrayList[] aCalcs = new ArrayList[ m_lC.Count ];

					foreach( CalculationResult cRes in rR.Results )
					{
						if( cRes.Column < aCalcs.Length )
						{
							if( aCalcs[ cRes.Column ] == null )
							{
								aCalcs[ cRes.Column ] = new ArrayList();
							}

							aCalcs[ cRes.Column ].Add( cRes );
						}
					}

					fList.DataSource = aCalcs;
					fList.DataBind();
				}
			}
		}

		/// <summary>
		/// Bind the report element.
		/// </summary>

		protected void BindHeader( object sender , RepeaterItemEventArgs a )
		{
			Column cC = a.Item.DataItem as Column;

			if( cC != null )
			{
				// Bind the current column into its slot.

				LinkButton  rColumn = a.Item.FindControl( "Column" ) as LinkButton;
				Label         rDesc = a.Item.FindControl( "Desc"   ) as Label;
				Label          rAsc = a.Item.FindControl( "Asc"    ) as Label;
				HtmlTableCell rCell = a.Item.FindControl( "Cell"   ) as HtmlTableCell;

				if( rColumn != null )
				{
					// Write out the current cell using the formatting
					// we cached at the time of load.
					//
					// 4/5/2005 Sorting is handled by clicking a column
					// label.  We cache the sorting in between postbacks.

					rColumn.Text = cC.Name;
                    rColumn.CommandName = "Sort";
                    rColumn.CommandArgument = cC.ToString();
                    //opm 6732 fs 08/21/08 disable UI sorting for 'unsortable' columns
                    if (cC.Kind == Field.E_ClassType.LongText)
					{
						rColumn.Attributes.Add("OnClick", "return false;");
						rColumn.Style.Add("cursor","default");
						rColumn.Style.Add("color","white");
						rColumn.Style.Add("text-decoration","none");
					}

					if( rCell != null && a.Item.ItemIndex > 0 )
					{
						switch( cC.Kind )
						{
							case Field.E_ClassType.Cal:
							case Field.E_ClassType.Cnt:
							case Field.E_ClassType.Csh:
							case Field.E_ClassType.Pct:
							{
								rCell.Align = "right";
							}
							break;

							default:
							{
								rCell.Align = "left";
							}
							break;
						}
					}

					if( m_sortOrder.Value.StartsWith( cC.Id + ":" ) == true )
					{
						if( m_sortOrder.Value.EndsWith( ":desc" ) == true )
						{
							rDesc.Visible = true;
							rAsc.Visible  = false;
						}
						else
						{
							rDesc.Visible = false;
							rAsc.Visible  = true;
						}
					}
					else
					{
						rDesc.Visible = false;
						rAsc.Visible  = false;
					}
				}
			}
		}

		/// <summary>
		/// Bind the report element.
		/// </summary>
        Guid _curr_loan_id = Guid.Empty;
		protected void BindRows( object sender , RepeaterItemEventArgs a )
		{
			Row rR = a.Item.DataItem as Row;
            Guid loanID = Guid.Empty;
			if( rR != null )
			{
				// Bind the current row.  We fixup the first entry to be
				// our link table for executing commands on the loan.

				Repeater rSet = a.Item.FindControl( "Row" ) as Repeater;
                loanID = rR.Key;
                _curr_loan_id = rR.Key;

				if( rSet != null )
				{
					rSet.DataSource = rR.Items;
					rSet.DataBind();
				}

				// Now place the lookup entry (always the first column
				// of the result) in place with links.

				Label rKey = a.Item.FindControl( "Label" ) as Label;

				if( rKey != null )
				{
					// Get the access control lookup results.  If not found,
					// then we assume no access is provided.  The row should
					// be removed from the set (hidden).
					//
					// 7/14/2005 kb - We now put the access control info in
					// each row.

					if( rSet.Items.Count > 0 )
					{
						Literal rValue = rSet.Items[ 0 ].FindControl( "Value" ) as Literal;
						if( rValue != null && rValue.Text != "" )
						{
							rKey.Text = rValue.Text;
						}
						else
						{
							rKey.Text = "Loan";
						}

						rSet.Items[ 0 ].Visible = false;
					}

					// 5/9/2005 kb - We have added lead editing by keying off the
					// loan's status to determine which editor to launch, per
					// case #1796.

					Panel aPanel; Boolean isLead = false , canRead = false , canEdit = false;

                    // 4/22/2016 BS - Case 240838. Invisible checkbox to determine status whether it's a lead or a loan
                    HtmlInputCheckBox checkBox = (HtmlInputCheckBox) a.Item.FindControl("isLead");

					switch( ( E_sStatusT ) rR.Arg )
					{
						case E_sStatusT.Lead_New:
						case E_sStatusT.Lead_Other:
						case E_sStatusT.Lead_Declined:
						case E_sStatusT.Lead_Canceled:
						{
							isLead = true;
						}
						break;
					}

                    checkBox.Checked = isLead;

					if( rR.Level == E_RowAccessLevel.Edit || rR.Level == E_RowAccessLevel.NotChecked )
					{
						canRead = true;
						canEdit = true;
					}

					if( rR.Level == E_RowAccessLevel.Read )
					{
						canRead = true;
					}

					aPanel = a.Item.FindControl( "CheckBox" ) as Panel;

					if( aPanel != null )
					{
						// Decide whether we want to be able to select the given loan.

						if( canEdit == false )
						{
							aPanel.Visible = false;
						}
						else
						{
							aPanel.Visible = true;
						}
					}

					aPanel = a.Item.FindControl( "ViewLink" ) as Panel;

					if( aPanel != null )
					{
						// Set visibility for viewing the loan summary.
                        aPanel.Visible = canRead;
					}

					aPanel = a.Item.FindControl( "EditLink" ) as Panel;

					if( aPanel != null )
					{
						// Set visibility for editing the loan.
                        aPanel.Visible = !isLead; //Make load link always visible if not lead as there can be field level write privilege. So edit link is not necessary
                        WebControl cHide = aPanel.FindControl("Hide") as WebControl;
                        cHide.Visible = false;
					}

					aPanel = a.Item.FindControl( "LeadLink" ) as Panel;
					if( aPanel != null )
					{
                        aPanel.Visible = isLead;						
					}

                    // 6/11/2014 dd - Not ready.
                    //aPanel = a.Item.FindControl("SandboxLink") as Panel;
                    //if (aPanel != null)
                    //{
                    //    aPanel.Visible = Broker.IsSandboxEnabled;
                    //}

                    // 11/19/2015 - dd - Will remove this temporary check once we fully roll out new UI.
                    aPanel = a.Item.FindControl("EditNewUILink") as Panel;
                    if (aPanel != null)
                    {
                        aPanel.Visible = this.BrokerUser.ActuallyShowNewLoanEditorUI;
                    }

                    aPanel = a.Item.FindControl("StyleEditorLink") as Panel;
                    if (aPanel != null)
                    {
                        aPanel.Visible = this.BrokerUser.BrokerDB.EnableStyleEditor;
                    }

                    var genericFrameworkVendorList = a.Item.FindControl("GenericFrameworkVendorList") as Repeater;
                    if (genericFrameworkVendorList != null)
                    {
                        genericFrameworkVendorList.DataSource = this.GenericFrameworkVendors;
                        genericFrameworkVendorList.ItemDataBound += new RepeaterItemEventHandler((sender2, a2) => GenericFrameworkVendorList_OnItemDataBound(sender2, a2, loanID));
                        genericFrameworkVendorList.DataBind();
                    }
				}
			}
		}

        private IEnumerable<GenericFrameworkVendor> genericFrameworkVendors;
        private IEnumerable<GenericFrameworkVendor> GenericFrameworkVendors
        {
            get
            {
                if (this.genericFrameworkVendors == null)
                {
                    this.genericFrameworkVendors = GenericFrameworkVendor.LoadVendors(this.BrokerUser.BrokerId).Where(vendor => vendor.LaunchLinkConfig.IsDisplayLinkForUser(LinkLocation.LQBPipeline));
                }

                return this.genericFrameworkVendors;
            }
        }

        protected void GenericFrameworkVendorList_OnItemDataBound(object sender, RepeaterItemEventArgs a, Guid loanID)
        {
            var vendor = (GenericFrameworkVendor)a.Item.DataItem;
            var link = a.Item.FindControl("GenericFrameworkLaunchLink") as HtmlAnchor;
            if (link != null)
            {
                link.InnerText = vendor.LaunchLinkConfig.LinkDisplayName(LinkLocation.LQBPipeline);
                link.Attributes.Add("data-providerid", vendor.ProviderID);
                link.HRef = string.Format("javascript:launchGenericFramework({0}, {1});", AspxTools.JsString(vendor.ProviderID), AspxTools.JsString(loanID));
            }
        }

		/// <summary>
		/// Bind the report element.
		/// </summary>

		protected void BindRow( object sender , RepeaterItemEventArgs a )
        {
            Value rV = a.Item.DataItem as Value;

            if (rV != null)
            {
                // Bind the current value into its slot.

                Literal rLabel = a.Item.FindControl("Value") as Literal;
                HtmlTableCell rCell = a.Item.FindControl("Cell") as HtmlTableCell;

                if (rLabel != null)
                {
                    // Write out the current cell using the formatting
                    // we cached at the time of load.

                    if (a.Item.ItemIndex < m_lC.Count)
                    {
                        Column cC = m_lC[a.Item.ItemIndex] as Column;
                        if (cC.Format != "")
                        {
                            rLabel.Text = string.Format("{0:" + cC.Format + "}", rV.Data);
                        }
                        else
                        {
                            rLabel.Text = string.Format("{0}", rV.Data);
                        }

                        if (cC.Name.ToLower().Contains("task"))
                        { 
                            //custom formatting task fields. 1. Task fields need to be displayed as link to the loan task page. 2. Display fields in red if past due
                            HtmlAnchor editLink = new HtmlAnchor();

                            if (rV.Data != null)
                            {
                                string sDataTxt = rV.Data.ToString();


                                if (sDataTxt.Contains("Due"))
                                {
                                    if (sDataTxt.Contains("/"))
                                    {
                                        int idx = sDataTxt.IndexOf("/");
                                        sDataTxt = FormatTaskDueField(sDataTxt.Substring(0, idx)) + "/" + FormatTaskDueField(sDataTxt.Substring(idx + 1, sDataTxt.Length - idx - 1));
                                    }
                                    else
                                    {
                                        sDataTxt = FormatTaskDueField(sDataTxt);
                                    }
                                    editLink.InnerHtml = sDataTxt;
                                }
                                else
                                {
                                    editLink.InnerText = sDataTxt;
                                }
                            }
                            else
                            {
                                //Handle formatting of empty task fields. They need to be filled in with default values so that users can go to the loan tasks page from the pipeline.
                                string displayTxt = string.Empty;
                                switch (cC.Name)
                                {
                                    case "Active Tasks":
                                        displayTxt = "0 Tasks / 0 Due";
                                        break;
                                    case "Tasks Due / Past Due":
                                        displayTxt = "0 Due / 0 Past Due";
                                        break;
                                    default:
                                        displayTxt = "0";
                                        break;
                                }
                                editLink.InnerText = displayTxt;
                            }
                            editLink.Attributes.Add("onclick", "editTaskList('" + _curr_loan_id + "'); return false;");
                            editLink.Attributes.Add("href", "#");
                            rCell.Controls.Add(editLink);

                            rLabel.Text = string.Empty;
                        }

                        if (rCell != null && a.Item.ItemIndex > 0)
                        {
                            switch (cC.Kind)
                            {
                                case Field.E_ClassType.Cal:
                                case Field.E_ClassType.Cnt:
                                case Field.E_ClassType.Csh:
                                case Field.E_ClassType.Pct:
                                    {
                                        rCell.Align = "right";
                                    }
                                    break;

                                default:
                                    {
                                        rCell.Align = "left";
                                    }
                                    break;
                            }
                        }
                    }
                    else
                    {
                        rLabel.Text = rV.Text;
                    }
                }
            }            
        }

        private string FormatTaskDueField(string sDataTxt)
        {
            if (!sDataTxt.Contains("Due")) return sDataTxt;

            int start = 0;
            int end = sDataTxt.IndexOf(" ", start + 1);

            string sFollowupTxt = sDataTxt.Substring(end);

            if (start >= 0 && end >= 0)
            {
                int nDue = SafeConvert.ToInt(sDataTxt.Substring(start, end - start));
                if (nDue > 0)
                {
                    sDataTxt = AspxTools.HtmlString(sDataTxt.Substring(0, start)) 
                        + " <span style='color:red'>" 
                        + nDue.ToString() 
                        + AspxTools.HtmlString(sFollowupTxt) 
                        + "!</span>";
                }
            }
            return sDataTxt;
        }
		/// <summary>
		/// Bind the report element.
		/// </summary>

		protected void BindResult( object sender , RepeaterItemEventArgs a )
		{
			CalculationResult cR = a.Item.DataItem as CalculationResult;

			if( cR != null )
			{
				// Bind the list for this column to the footer cell.

				Label   cWhat = a.Item.FindControl( "What"   ) as Label;
				Label cResult = a.Item.FindControl( "Result" ) as Label;

				if( cWhat != null && cResult != null )
				{
					// Bind the individual result in the order we get
					// them from the parent level.

					Column cC = m_lC[ cR.Column ] as Column;

                    switch( cR.What )
					{
						case E_FunctionType.Av: cWhat.Text = "Avg";
						break;

						case E_FunctionType.Ct: cWhat.Text = "Num";
						break;

						case E_FunctionType.Mn: cWhat.Text = "Min";
						break;

						case E_FunctionType.Mx: cWhat.Text = "Max";
						break;

						case E_FunctionType.Sm: cWhat.Text = "Tot";
						break;
					}

                    string format = null;

                    switch (cC.Kind)
                    {
                        case Field.E_ClassType.Pct:
                            format = "N3";
                            break;
                        case Field.E_ClassType.Csh:
                            format = "C2";
                            break;
                        case Field.E_ClassType.Cnt:
                            format = "N0";
                            break;
                        default:
                            format = "N";
                            break;
                    }

                    cResult.Text = cR.GetFormattedResult(format);
                }
			}
		}

		/// <summary>
		/// Bind the report element.
		/// </summary>

		protected void BindFooter( object sender , RepeaterItemEventArgs a )
		{
			IEnumerable rS = a.Item.DataItem as IEnumerable;

			if( rS != null )
			{
				// Bind the list for this column to the footer cell.

				Repeater cList = a.Item.FindControl( "Calcs" ) as Repeater;

				cList.DataSource = rS;
				cList.DataBind();
			}
		}

		/// <summary>
		/// Handle header click.
		/// </summary>

		protected void HeaderClick( object sender , RepeaterCommandEventArgs a )
		{
			// Handle click event and process sort commands.

			try
			{
				// Get who we selected and set the new sort by column.
				// We now need to re-render the query with the new
				// sort specification.

				if( a.CommandName.ToLower().TrimWhitespaceAndBOM() == "sort" )
				{
					String colId = a.CommandArgument.ToString();

					if( m_sortOrder.Value.StartsWith( colId + ":" ) == true && m_sortOrder.Value.EndsWith( ":asc" ) == true )
					{
						m_sortOrder.Value = colId + ":desc";
					}
					else
					{
						m_sortOrder.Value = colId + ":asc";
					}

                    m_tabView.Active.SortOrder = m_sortOrder.Value;
                    Tools.UpdateMainTabsSettingXmlContent(BrokerUser.ConnectionInfo, BrokerUser.UserId, m_tabView);
				}
			}
			catch( CBaseException e )
			{
				// Oops!

				if( e.UserMessage != "" )
				{
					Tools.LogError( ErrorMessage = "Failed to sort pipeline: " + e.UserMessage , e );
				}
				else
				{
					Tools.LogError( ErrorMessage = "Failed to sort pipeline" , e );
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to sort pipeline" , e );
			}
		}

		/// <summary>
		/// Trigger rendering the current pipeline.
		/// </summary>

		public void LoadData()
		{
			// Add the render handler to this page only on load.

			PreRender += new EventHandler( PagePreRender );
		}

		/// <summary>
		/// Save something.  Nothing to do.
		/// </summary>

		public void SaveData()
		{
		}

	}

}
