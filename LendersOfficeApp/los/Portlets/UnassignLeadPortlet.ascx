<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="DeleteLoanButton" Src="DeleteLoanButton.ascx" %>
<%@ Register TagPrefix="uc1" TagName="RefreshButton" Src="RefreshButton.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="UnassignLeadPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.UnassignLeadPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="uc1" TagName="AssignLoanButton" Src="AssignLoanButton.ascx" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.AntiXss" %>

<table cellSpacing="0" cellPadding="3" width="100%" border="0">
	<TR>
		<td nowrap><uc1:AssignLoanButton id="AssignLoanButton1" runat="server"></uc1:AssignLoanButton>
			<uc1:deleteloanbutton id="DeleteLoanButton1" runat="server"></uc1:deleteloanbutton><uc1:RefreshButton id="RefreshButton1" runat="server"></uc1:RefreshButton></td>
		<TD align="right"><ml:EncodedLabel id="m_countLabel" Font-Bold="True" Font-Size="12px" ForeColor="Black" EnableViewState="False" runat="server"></ml:EncodedLabel></TD>
	</TR>
</table>
<script>
    var gSelectionCount = 0;
    function _init() {
      disableButtons(true);
    }

	function disableButtons( b )
	{
		if( document.getElementById("AssignLoanBtn") != null )
		{
			document.getElementById("AssignLoanBtn").disabled = b;
		}

		if( document.getElementById("DeleteLoanBtn") != null )
		{

			document.getElementById("DeleteLoanBtn").disabled = b;
		}
	}

    function onCheckBoxClick(cb) {
        if (cb.checked)
            gSelectionCount++;
        else
            gSelectionCount--;
        highlightRowByCheckbox(cb);
        disableButtons(gSelectionCount == 0);
    }

    function onClickUnalLinkSet( oBase , oTarget ) { try
    {
        // Check for valid targets.

        if( oBase == null || oTarget == null )
        {
            return;
        }

        // Hide all others and show selected link set.

        if( oTarget.nextSibling != null )
        {
            // Find containing row and prepare to make all the
            // text bold in the following cells.

            var oRow = oTarget;

            while( oRow != null )
            {
                if( oRow.tagName.toUpperCase() == "TR" )
                {
                    break;
                }

                oRow = oRow.parentElement;
            }

            if( oTarget.nextSibling.style.display == "none" )
            {
                // Turn off all previously activated link sets within
                // this grid and then turn on the selected one.

                oTarget.nextSibling.style.display    = "block";
                oTarget.nextSibling.style.fontWeight = "normal";

                if( oRow != null )
                {
                    oRow.style.fontWeight = "bold";
                }
            }
            else
            {
                // Turn off the selected one and leave the
                // other link sets alone.

                oTarget.nextSibling.style.display = "none";

                if( oRow != null )
                {
                    oRow.style.fontWeight = "normal";
                }
            }
        }
    }
    catch( e )
    {
        window.status = "Selecting " + oTarget.innerText + " failed.";
    }}
</script>
<ml:commondatagrid id="m_unassignLeadsDG" runat="server" DataKeyField="sLId" OnItemDataBound="LoanDataBound">
	<Columns>
		<asp:TemplateColumn>
			<ItemTemplate>
				<asp:Panel id="SelectBox" runat="server">
                    <input type='checkbox' id='loanid' value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>' onclick='onCheckBoxClick(this);'>
				</asp:Panel>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="sLNm" HeaderText="Loan Number">
			<ItemTemplate>
				<div style="CURSOR: hand;">
					<div id="m_leadNameTag" style="DISPLAY: block; COLOR: blue;" onmouseover="style.color = 'orange';" onmouseout="style.color = 'blue';" onclick="onClickUnalLinkSet( <%# AspxTools.JsString(m_unassignLeadsDG.ClientID) %> , this );">
						<u>
							<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLNm")) %>
						</u>
					</div>
					<div id="m_leadLinkSet" style="DISPLAY: none; PADDING: 0px; MARGIN-TOP: 2px; MARGIN-BOTTOM: 8px;">
						<asp:Panel id="EditLink" runat="server" style="PADDING-LEFT: 2px;">
                            &#x00a0;
                            &#x2022;
                            <a href="javascript:editLead(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "sLId" ).ToString()) %>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "sLNm").ToString() + " - " + DataBinder.Eval(Container.DataItem, "aBLastNm").ToString()) %>);" title="Edit lead">edit</a>
                        </asp:Panel>
						<asp:Panel id="LoadLink" runat="server" style="PADDING-LEFT: 2px;">
                            &#x00a0;
                            &#x2022;
                            <a href="javascript:editLead(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "sLId").ToString()) %>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "sLNm").ToString() + " - " + DataBinder.Eval(Container.DataItem, "aBLastNm").ToString()) %>);" title="View lead in editor">view</a>
                        </asp:Panel>
						<asp:Panel id="ViewLink" runat="server" style="PADDING-LEFT: 2px;">
                            &#x00a0;
                            &#x2022;
                            <a href="javascript:viewLoan(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "sLId").ToString()) %>);" title="View lead summary">view</a>
                        </asp:Panel>
						<div nowrap style="PADDING-LEFT: 2px;">
							&#x00a0;
                            &#x2022;
                            <a href="javascript:createTask(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "sLId").ToString()) %>);" title="Insert new task for this lead">new task</a>
						</div>
					</div>
				</div>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:templatecolumn headertext="Last Name" sortexpression="aBLastNm">
			<itemtemplate>
				<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "aBLastNm")) %>
			</itemtemplate>
		</asp:templatecolumn>
		<asp:templatecolumn headertext="First Name" sortexpression="aBFirstNm">
			<itemtemplate>
				<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "aBFirstNm")) %>
			</itemtemplate>
		</asp:templatecolumn>
		<asp:BoundColumn DataField="aBSsn" HeaderText="SSN"></asp:BoundColumn>
		<asp:BoundColumn DataField="LoanType" HeaderText="Type" SortExpression="LoanType"></asp:BoundColumn>
		<asp:templatecolumn headertext="Status" sortexpression="LoanStatus">
			<itemtemplate>
				<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "LoanStatus")) %>
			</itemtemplate>
		</asp:templatecolumn>
		<asp:templatecolumn headertext="Branch" sortexpression="sBranchNm">
			<itemtemplate>
				<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sBranchNm")) %>
			</itemtemplate>
		</asp:templatecolumn>
		<asp:BoundColumn DataField="sStatusD" HeaderText="Status Date" DataFormatString="{0:d}" SortExpression="sStatusD"></asp:BoundColumn>
	</Columns>
</ml:commondatagrid>
