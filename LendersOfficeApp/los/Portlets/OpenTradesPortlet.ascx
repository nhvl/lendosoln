﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OpenTradesPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.OpenTradesPortlet" %>
<%@ Register TagPrefix="uc1" TagName="TradeTable" Src="~/los/MortgagePools/Controls/TradeTable.ascx" %>

<div id="OpenTradesPortletMain">
    <asp:PlaceHolder runat="server" ID="ControlsPanel">
        <input type="button" runat="server" value="Refresh" onserverclick="RefreshClicked" />
        <input type="button" runat="server" value="Export to Excel (csv)..." onserverclick="ExportToCsvClicked" />
        <input type="button" runat="server" value="Export to Text..." onserverclick="ExportToTxtClicked" />
        <input type="button" runat="server" value="Export to PDF..." class="export-pdf" />
        <input type="button" value="Print..." class="print" />
        <input type="hidden" runat="server" id="ReportTitle" class="report-title" />
        <input type="hidden" runat="server" id="LogoSrc" class="logo-src" />
        <input type="hidden" runat="server" id="BaseStyleSheet" class="base-style-sheet" />
        <input type="hidden" runat="server" id="TradeReportStyleSheet" class="trade-report-style-sheet" />
    </asp:PlaceHolder>
    
    <uc1:TradeTable runat="server" id="Trades" HideDeleteLinks="true"/>
</div>