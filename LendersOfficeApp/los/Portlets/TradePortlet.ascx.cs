﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.CapitalMarkets;
using DataAccess;
using LendersOfficeApp.ObjLib.Licensing;
using LendersOffice.Security;
using System.Web.UI.HtmlControls;
using LendersOffice.Common.TextImport;

namespace LendersOfficeApp.los.Portlets
{
    public static class TradePortletExtensions
    {
        public static bool IsAny(this DropDownList my)
        {
            return my.SelectedValue == "ANY"; //set by AddAny
        }

        public static bool IsAny(this TextBox my)
        {
            return string.IsNullOrEmpty(my.Text);
        }

    }

    public partial class TradePortlet : System.Web.UI.UserControl
    {
        protected Guid BrokerID
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            }
        }

        public bool SelectTradeOnly;
        public bool HideCreateTrade;
        public bool HideDeleteLinks;
        public bool FilterTradesInPools;

        protected void Page_Load(object sender, EventArgs e)
        {
            Trades.SelectTradeOnly = SelectTradeOnly;
            Trades.HideDeleteLinks = HideDeleteLinks;
            if (HideCreateTrade) Page.ClientScript.RegisterHiddenField("HideCreateTrade", "true");

            var basepage = Page as BasePage;
            if (basepage == null)
            {
                throw new Exception("Cannot use the Trade Portal on something that is not a BasePage");
            }

            if (!this.Visible) return;
            basepage.EnableJqueryMigrate = false;
            basepage.RegisterCSS("TradePortlet.css");
            basepage.RegisterJsScript("TradePortlet.js");

            //originally it would load on !ispostback only, but since the main page tabs now postback to change
            //tabs, you have to load on that postback, which is why we're checking if the postback was caused
            //by changing tabs
            if (!Page.IsPostBack || Page.Request.Params["__EVENTTARGET"] == "changeTab")
            {
                var dropDownLists = new DropDownList[] { Month, Type, Direction, TradeStatus, Security};
                foreach (var dropDown in dropDownLists)
                {
                    dropDown.Items.Clear();
                }

                Tools.BindMonthNamesDropDown(Month);
                Tools.Bind_Generic(Type, default(E_TradeType), n => n.Replace("Option", ""));

                Tools.Bind_Generic(Direction, default(E_TradeDirection));
                Tools.Bind_Generic(TradeStatus, default(TradeStatus));

                var securityItems = Description.GetDescriptions().Select(x => new ListItem(x.DescriptionName, x.DescriptionId.ToString()));
                Security.Items.AddRange(securityItems.ToArray());

                AddAny(dropDownLists);

                AssignData(Trade.GetTradesByBroker(BrokerID));
            }
        }

        protected void SearchClicked(object sender, EventArgs e)
        {
            TradePortletFilter f = new TradePortletFilter(this);
            AssignData(Trade.GetTradesByBroker(BrokerID).Filter(f));
        }

        private int startIdx = 0;
        private int window = 20;
        private void AssignData(IEnumerable<Trade> data)
        {
            var displayIdx = startIdx + 1;
            var totalResults = data.Count();
            var displayedTrades = data.Skip(startIdx).Take(window);

            var tradeGroup = new TradeGroup("", displayedTrades);
            Trades.DataSource = new[] { tradeGroup }; 
            Trades.DataBind();

            DisplayCount.InnerText = displayIdx + "-" + Math.Min(displayIdx + window, totalResults) + " of " + totalResults;
        }


        private static void AddAny(DropDownList[] Lists)
        {
            foreach (var list in Lists)
            {
                if (list.Items.Count > 0 && string.IsNullOrEmpty(list.Items[0].Text))
                {
                    list.Items.RemoveAt(0);
                }

                list.Items.Insert(0, new ListItem("<-- any -->", "ANY"));
                list.SelectedIndex = 0;
            }
        }


        private class TradePortletFilter : TradeFilter
        {
            private static int? GetInt(DropDownList input)
            {
                if (input.IsAny()) return null;

                var selected = input.SelectedValue;
                int ret;
                if (int.TryParse(selected, out ret)) return ret;
                return null;
            }

            private static T? GetEnum<T>(DropDownList input) where T : struct
            {
                if (input.IsAny()) return null;

                var selected = input.SelectedValue;
                try
                {
                    return (T)Enum.Parse(typeof(T), selected);
                }
                catch (ArgumentException)
                {
                    return null;
                }
            }

            private static string GetString(DropDownList input)
            {
                if (input.IsAny()) return null;
                return input.SelectedValue;
            }

            TradePortlet Source;
            public override Guid BrokerId
            {
                get { return Source.BrokerID; }
            }

            public override TradeStatus? Status
            {
                get { return GetEnum<TradeStatus>(Source.TradeStatus); }
            }

            public override string TradeNum
            {
                get { return Source.TradeNumber.Text; }
            }

            public override bool TradeNumPartialMatch
            {
                get { return Source.ReturnPartialMatch.Checked; }
            }

            public override E_TradeType? Type
            {
                get { return GetEnum<E_TradeType>(Source.Type); }
            }

            public override E_TradeDirection? Direction
            {
                get { return GetEnum<E_TradeDirection>(Source.Direction); }
            }

            public override int? Month
            {
                get { return GetInt(Source.Month); }
            }

            public override Guid? DescriptionId
            {
                get
                {
                    var idStr = Source.Security.SelectedValue;
                    Guid id;
                    if (DataParsing.TryParseGuid(idStr, out id))
                    {
                        return id;
                    }
                    return null;
                }
            }

            public override string Coupon
            {
                get { return Source.Coupon.Text; }
            }

            public TradePortletFilter(TradePortlet source)
            {
                Source = source;
            }

            public override bool FilterTradesInPools
            {
                get
                {
                    return Source.FilterTradesInPools;
                }
            }

            public override DateTime? SettlementDateLowerBound
            {
                get { return null; }
            }

            public override DateTime? SettlementDateUpperBound
            {
                get { return null; }
            }
        }

    }
}
