<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ReportingPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.ReportingPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="javascript">
<!--

function onScheduledReportsClick()
{

    window.open(VRoot + "/CustomReportsScheduler/ScheduledCustomReports.aspx", "_blank", "resizable=yes,status=yes,menubar=no,scrollbars=yes,width=790,height=470");
}
function onAccountingReportClick()
{
  window.open(VRoot + "/los/Reports/AccountingReport.aspx", "_blank", "resizable=yes,status=yes,menubar=no,scrollbars=yes,width=470,height=470");
}
function onCustomReportsClick()
{
	var w = null

	try
	{
	    var gReportingWindow = retrieveFrameProperty(parent, "frmCode", "gReportingWindow");
	    if (gReportingWindow == null || gReportingWindow.closed == true) {
	        gReportingWindow = window.open(VRoot + "/los/reports/LoanReports.aspx", "_blank", "resizable=yes,status=yes,menubar=no,scrollbars=yes,width=800,height=540");
	        setFrameProperty(parent, "frmCode", "gReportingWindow", gReportingWindow);
	    }

	    w = gReportingWindow;
	}
	catch( e )
	{
		window.status = e.message;

		w = null;
	}

	if( w == null )
	{
		w = window.open( VRoot + "/los/reports/LoanReports.aspx" , "_blank" , "resizable=yes,status=yes,menubar=no,scrollbars=yes,width=800,height=540" );
	}
	
	w.focus();
}
function onSubservicingExportClick() {
    window.open( VRoot + "/los/Reports/SubservicingExport.aspx", "_blank", "resizable=yes,status=yes,menubar=no,scrollbars=yes,width=790,height=550");
}

function onBatchExportClick() {
    window.open(VRoot + "/los/Reports/BatchExport.aspx", "_blank", "resizable=yes,status=yes,menubar=no,scrollbars=yes,width=890,height=550");
}
function onBatchExportDownloadClick()
{
  window.open(VRoot + "/los/Reports/BatchExportDownload.aspx", "_blank", "resizable=yes,status=yes,menubar=no,scrollbars=yes,width=790,height=550");
}
function onPublishedReportsClick() {
    window.open(gVirtualRoot + '/los/PortletPopup.aspx?Portlet=PublishedReports', 'PublishedReports', 'resizable=yes,menubar=no,status=no,scrollbars=yes,height=500,width=300');
    return false;
}
function onSecurityEventLogClick() {
    LQBPopup.Show(VRoot + "/los/Reports/SecurityEventLog.aspx", {
        hideCloseButton: true,
        width: 1065,
        height: 550
    });
}
//-->
</script>
<table cellSpacing="0" cellPadding="0" border="0" width="100%" class="Portlet">
	<tr>
	<td noWrap class="PortletHeader">Reporting</TD>
	</tr>
	<tr>
	<td nowrap>		
        <div runat="server" id="PermissionDependent1" >
		    <img src="../images/bullet.gif" align="absMiddle">&nbsp; <a href="#" onclick="onCustomReportsClick(); return false;" class="PortletLink">
			    Custom Reports</a>
		    <br>
		    <img src="../images/bullet.gif" align="absMiddle">&nbsp; <a href="#" onclick="onScheduledReportsClick(); return false;" class="PortletLink">
		        Scheduled Reports</a>
        </div>
	    <img src="../images/bullet.gif" align="absMiddle">&nbsp; <a href="#" onclick="onPublishedReportsClick(); return false;" class="PortletLink">
		    Published Reports</a>
        <div runat="server" id="PermissionDependent2">
		    <asp:PlaceHolder ID="m_subservicingExportPlaceHolder" runat="server">
		    <img src="../images/bullet.gif" align="absMiddle">&nbsp; <a href="#" onclick="return onSubservicingExportClick();" class="PortletLink" style="display:inline-block">
		        Provident Funding<br/>Subservicing Export</a>
		    <br />
		    </asp:PlaceHolder>
		    <asp:PlaceHolder ID="m_accountingReportPlaceHolder" runat="server">
		    <img src="../images/bullet.gif" align="absMiddle">&nbsp; <a href="#" onclick="return onAccountingReportClick();" class="PortletLink">
		        Accounting Reports</a>
		    <br />
		
		    </asp:PlaceHolder>
		      <img src="../images/bullet.gif" align="absMiddle">&nbsp; <a href="#" onclick="return onBatchExportClick();" class="PortletLink">
		        Batch Export</a>
		  
		    </br>
		      <img src="../images/bullet.gif" align="absMiddle">&nbsp; <a href="#" onclick="return onBatchExportDownloadClick();" class="PortletLink">
		        Report Download</a>

            </br>
            <asp:PlaceHolder ID="m_securityEventLogPlaceHolder" runat="server">
		      <img src="../images/bullet.gif" align="absMiddle">&nbsp; <a onclick="return onSecurityEventLogClick();" class="PortletLink">
		        Security Event Log</a>
                </br>
            </asp:PlaceHolder>
        </div>	
	</td>
	</tr>
	<tr>
	<td><img src="../images/shadow_line.gif" width="130" height="2"></td>
	</tr>
</table>
