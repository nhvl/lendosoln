﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Reports;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using LendersOffice.QueryProcessor;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.Portlets
{
    public partial class AddTab : LendersOffice.Common.BasePage
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal;}
        }

       private CPipelineView m_tabView = null;

        private BrokerDB m_broker = null;
        private BrokerDB Broker
        {
            get
            {
                if (m_broker == null)
                {
                    m_broker = this.BrokerUser.BrokerDB;
                }
                return m_broker;
            }
        }

        private String ErrorMessage
        {
            // Set a new error message.
            set
            {
                Page.ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + ".");
            }
        }

        private String CommandToDo
        {
            // Set a new command to do.
            set
            {
                Page.ClientScript.RegisterHiddenField("m_commandToDo", value);
            }
        }

        protected void PageLoad(object sender, System.EventArgs a)
        {
            m_tabView = Tools.GetMainTabsSettingXmlContent(BrokerUser.ConnectionInfo, BrokerUser.UserId);

            if (!IsPostBack)
            {
                    bool bDefault = false;
                    m_tabName.Text = "";
                    m_showAssigned.Checked = !bDefault;
                    m_showDefault.Checked = bDefault;

                // For Recently used pipelines section
                m_recentTabs.DataSource = m_tabView.recentlyClosedTabs;
                m_recentTabs.DataBind();
                this.ChooseReport.BindData(Guid.Empty);
            }
        }

        protected void RecentTabs_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            Repeater tabRepeater = sender as Repeater;
            // Show "None" if no recently closed tabs
            if (((List<CTab>)tabRepeater.DataSource).Count < 1)
            {
                if (args.Item.ItemType == ListItemType.Footer)
                {
                    EncodedLiteral noTabMsg = args.Item.FindControl("m_noTabsMsg") as EncodedLiteral;

                    if (noTabMsg != null)
                    {
                        noTabMsg.Visible = true;
                    }
                }
            }
            else
            {
                if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
                {
                    return;
                }

                CTab currTab = args.Item.DataItem as CTab;
                EncodedLiteral visiblity = args.Item.FindControl("m_visiblity") as EncodedLiteral;
                visiblity.Text = "[" + GetVisibility(currTab.Scope) + "]";
                LinkButton recTabChooseLink = args.Item.FindControl("recTabChooseLink") as LinkButton;
                recTabChooseLink.CommandArgument = currTab.ToString();
            }
        }

        private string GetVisibility(E_ReportExtentScopeT scope)
        {
            if (E_ReportExtentScopeT.Assign == scope)
                return "My loans";
            else
                return "All loans";
        }

        protected void RecentTabClick(object sender, RepeaterCommandEventArgs a)
        {
            LendersOffice.Common.RequestHelper.StoreToCookie("sortOrder", "", false);

            if (a.CommandName == "Choose")
            {
                CTab currTab = ObsoleteSerializationHelper.JavascriptJsonDeserializer<CTab>(a.CommandArgument.ToString());
                Guid reportId = currTab.ReportId;

                Query q = Query.GetQuery(BrokerUser.BrokerId, reportId);

                if (q == null)
                {
                    this.ErrorMessage = $"Report {currTab.ReportTitle} cannot be found.";
                    return;
                }

                if (q.hasSecureFields && !q.CanUserRun(this.BrokerUser))
                {
                    this.ErrorMessage = $"Report {currTab.ReportTitle} has restricted fields you cannot see.";
                    return;
                }


                if (m_tabView.Add(currTab, true))
                {
                    m_tabView.LastCustomReportId = reportId;
                    Tools.UpdateMainTabsSettingXmlContent(BrokerUser.ConnectionInfo, BrokerUser.UserId, m_tabView);
                }

                CommandToDo = "Close";
            }
        }

        protected void ChooseReport_OnReportError(object sender, ErrorEventArgs args)
        {
            this.ErrorMessage = args.ErrorMessage;
        }

        protected void ChooseReport_OnReportChosen(object sender, ChooseReportEventArgs args)
        {
            LendersOffice.Common.RequestHelper.StoreToCookie("sortOrder", "", false);
            Guid reportId = args.SelectedReport.QueryId;

            E_ReportExtentScopeT scope;

            if (m_showDefault.Checked)
            {
                scope = E_ReportExtentScopeT.Default;
            }
            else
            {
                scope = E_ReportExtentScopeT.Assign;
            }

            string tabName = string.IsNullOrEmpty(m_tabName.Text) ? args.SelectedReport.QueryName : m_tabName.Text;
            CTab newTab = new CTab(E_TabTypeT.PipelinePortlet, tabName, reportId, args.SelectedReport.QueryName, scope);
            if (m_tabView.Add(newTab, true))
            {
                m_tabView.LastCustomReportId = reportId;
                Tools.UpdateMainTabsSettingXmlContent(BrokerUser.ConnectionInfo, BrokerUser.UserId, m_tabView);
            }

            CommandToDo = "Close";
        }

        protected HtmlGenericControl GenerateName(string name)
        {
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.InnerText = name;
            return span;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
