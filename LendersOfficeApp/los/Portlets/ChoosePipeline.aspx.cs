using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Reports;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using LendersOffice.QueryProcessor;
using System.Text.RegularExpressions;

namespace LendersOfficeApp.los.Portlets
{
	/// <summary>
	/// Summary description for ChoosePipeline.
	///
	/// 4/1/2005 kb - Implemented as part of case #932.
	/// </summary>
    
	public partial class ChoosePipeline : LendersOffice.Common.BasePage
	{
        
		private BrokerUserPrincipal BrokerUser
		{
			// Get the current user principal.
            get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

        private CPipelineView m_tabView = null;

        private BrokerDB m_broker = null;
        private BrokerDB Broker
        {
            get
            {
                if (m_broker == null)
                {
                    m_broker = this.BrokerUser.BrokerDB;
                }
                return m_broker;
            }
        }

		private String ErrorMessage
		{
			// Set a new error message.

			set
			{
				Page.ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		private String CommandToDo
		{
			// Set a new command to do.

			set
			{
				Page.ClientScript.RegisterHiddenField( "m_commandToDo" , value );
			}
		}

        /// <summary>
        /// Initialize this form.
        /// </summary>

        protected void PageLoad(object sender, System.EventArgs a)
        {
            m_tabView = Tools.GetMainTabsSettingXmlContent(BrokerUser.ConnectionInfo, BrokerUser.UserId);

            if (!IsPostBack)
            {
                this.ChooseReport.BindData(m_tabView.Active.ReportId);

                m_pipelineVisibility.Visible = true; //Broker.IsAllowSharedPipeline;
                bool bDefault = false;
                CTab tab = null;
                if (m_tabView.Count != 0)
                {
                    tab = m_tabView.Active;
                }

                if (tab != null)
                {
                    bDefault = (tab.Scope == E_ReportExtentScopeT.Default);
                }

                m_tabName.Text = tab.TabName;
                m_tabUid.Value = tab.UniqueId.ToString();
                m_showAssigned.Checked = !bDefault;
                m_showDefault.Checked = bDefault;
            }
        }

        protected void ChooseReport_OnReportChosen(object sender, ChooseReportEventArgs args)
        {
            this.Save(args.SelectedReport);
        }

        private bool IsValidTabName(string name)
        {
            Match result = Regex.Match(name, "[<>&]");
            return !result.Success;
        }

        protected void ChooseReport_OnReportError(object sender, ErrorEventArgs args)
        {
            this.ErrorMessage = args.ErrorMessage;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

        protected void Okay_OnClick(object sender, EventArgs args)
        {
            this.Save(null);
        }

        private void Save(ReportDesc selectedReport)
        {
            if (!IsValidTabName(m_tabName.Text))
            {
                return;
            }

            try
            {
                E_ReportExtentScopeT scope;

                if (m_showAssigned.Checked)
                {
                    scope = E_ReportExtentScopeT.Assign;
                }
                else
                {
                    scope = E_ReportExtentScopeT.Default;
                }

                CTab activeTab = m_tabView.GetById(Guid.Parse(this.m_tabUid.Value));
                activeTab.TabName = m_tabName.Text;
                activeTab.Scope = scope;

                if (selectedReport != null)
                {
                    activeTab.ReportId = selectedReport.QueryId;
                    activeTab.ReportTitle = selectedReport.QueryName;
                    activeTab.SortOrder = "";
                    m_tabView.LastCustomReportId = selectedReport.QueryId;
                }

                Tools.UpdateMainTabsSettingXmlContent(BrokerUser.ConnectionInfo, BrokerUser.UserId, m_tabView);
                CommandToDo = "Close";
            }
            catch (CBaseException e)
            {
                // Oops!
                if (e.UserMessage != "")
                {
                    Tools.LogError(ErrorMessage = "Failed to choose report: " + e.UserMessage, e);
                }
                else
                {
                    Tools.LogError(ErrorMessage = "Failed to choose report", e);
                }
            }
        }
	}

	/// <summary>
	/// Store simple report description.
	/// </summary>

	public class ReportDesc
	{
		/// <summary>
		/// Store simple report description.
		/// </summary>

		private String m_QueryName = String.Empty;
		private String  m_Selected = String.Empty;
		private Guid     m_QueryId = Guid.Empty;

		public String QueryName
		{
			// Access member.

			set
			{
				m_QueryName = value;
			}
			get
			{
				return m_QueryName;
			}
		}

		public String Selected
		{
			// Access member.

			set
			{
				m_Selected = value;
			}
			get
			{
				return m_Selected;
			}
		}

		public Guid QueryId
		{
			// Access member.

			set
			{
				m_QueryId = value;
			}
			get
			{
				return m_QueryId;
			}
		}

		/// <summary>
		/// Return id as a key for default binding.
		/// </summary>

		public override string ToString()
		{
            // Serialize it so we can access all members from CommandArguments
            return ObsoleteSerializationHelper.JavascriptJsonSerialize(this);
		}

	}

}

