﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TradePortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.TradePortlet" %>
<%@ Register TagPrefix="uc1" TagName="TradeTable" Src="~/los/MortgagePools/Controls/TradeTable.ascx" %>
<%@ Import Namespace="LendersOffice.ObjLib.CapitalMarkets" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<div id="TradePortletMain">
    <div class="filters">
        <div class="column left">
            <label><span>Trade Status</span> <asp:DropDownList ID="TradeStatus" runat="server"/></label>
            <label><span>Trade Number</span> <asp:TextBox runat="server" id="TradeNumber" /></label>
            <span class="pusher"></span><label class="return-partial"><asp:CheckBox runat="server" id="ReturnPartialMatch" /> Return partial match</label>
        </div>
        <div class="column right">
            <label><span>Security</span> <asp:DropDownList ID="Security" runat="server"/></label>
            <label><span>Type</span> <asp:DropDownList ID="Type" runat="server"/> <asp:DropDownList ID="Direction" runat="server"/></label>
            <label><span>Month</span> <asp:DropDownList ID="Month" runat="server"/></label>
            <label><span>Coupon</span> <asp:TextBox id="Coupon" runat="server" /></label>
        </div>
        <div class="clear"></div>
    </div>
    <div class="controls">
        <input type="button" value="Search" runat="server" id="Search" onserverclick="SearchClicked"/>
        <span class="displaying">Displaying <span class="display-count" runat="server" id="DisplayCount"></span></span>
        <input type="button" value="Create New Trade" id="NewTrade" style="display:none;"/>
    </div>

    <uc1:TradeTable runat="server" ID="Trades" />
</div>
