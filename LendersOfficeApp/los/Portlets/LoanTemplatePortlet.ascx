<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LoanTemplatePortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.LoanTemplatePortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>






<TABLE class="Portlet" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD class="PortletHeader" noWrap>Loan Templates</TD>
</TR>
<TR>
<TD noWrap>
	
    <% if (m_isAllowCreateTemplate){ %>
		<img src="../images/bullet.gif" align="absMiddle">&nbsp;
		<a href="#" id="createTemplateLink" class="PortletLink" title="Create new loan template">Create Loan Template</a>
		<br>
	<% } %>
	
	<img src="../images/bullet.gif" align="absMiddle">&nbsp;
	<A href="loanfind.aspx?template=true" class="PortletLink" title="Advance search for your loan and/or loan template">Find Loan Template</A><br>

</TD>
</TR>
<TR>
<TD><img src="../images/shadow_line.gif" width="130" height="2"></td>
</TR>
</TABLE>

<script language="javascript">

function onCreateTemplateClick() 
{
    var args = {
        isTemplate: true
    };

    openTemplateDialog("", true);
    createLoanWithArgs(args);

    return false;
}

$j("#createTemplateLink").click(onCreateTemplateClick);

</script>