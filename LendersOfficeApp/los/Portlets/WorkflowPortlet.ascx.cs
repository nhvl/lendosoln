﻿#region Generated Code
namespace LendersOfficeApp.los.Portlets
#endregion
{
    using System;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a set of links allowing clients to configure
    /// their own workflow configurations.
    /// </summary>
    public partial class WorkflowPortlet : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets the principal of the current user.
        /// </summary>
        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        /// <summary>
        /// Handles loading the portlet.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Visible = this.BrokerUser.BrokerDB.ExposeClientFacingWorkflowConfiguration &&
                this.BrokerUser.BrokerDB.WorkflowRulesControllerId == this.BrokerUser.UserId;
        }
    }
}