﻿namespace LendersOfficeApp.los.Portlets
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    public partial class MyTasksPortlet : System.Web.UI.UserControl
    {

        private string _SortExp = string.Empty;
        private string _SortDir = string.Empty;

        private string SortExp
        {
            get
            {
                if (string.IsNullOrEmpty(_SortExp))
                {
                    HttpCookie cookie = Request.Cookies["sortExp"];
                    if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
                    {
                        return cookie.Value;
                    }
                    else
                        return "TaskDueDate";
                }
                return _SortExp;
            }
            set 
            {
                RequestHelper.StoreToCookie("sortExp", value, true);
                _SortExp = value;
            }
        }

        private SortDirection SortDir
        {
            get
            {
                if (string.IsNullOrEmpty(_SortDir))
                {
                    HttpCookie cookie = Request.Cookies["sortDir"];
                    if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
                    {
                        return cookie.Value == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
                    }
                    else
                        return SortDirection.Ascending;
                }
                return _SortDir == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
            }
            set
            {
                string sSortDir =  value == SortDirection.Ascending ? "ASC" : "DESC";
                RequestHelper.StoreToCookie("sortDir", sSortDir, true);
                _SortDir = sSortDir;
            }
        }
        private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}
        protected void Page_Load(object sender, EventArgs e)
        {            
            if(Page.IsPostBack)
            {
                if (inputCmd.Value == "csv")
                {
                    ExportToCsv(inputParams.Value);
                }
            }
        }

        private void PagePreRender(object sender, System.EventArgs a)
        {            

            if (!Page.IsPostBack)
            {
                string sTasksAssignedTo = m_TasksAssignedTo.SelectedValue;                
                HttpCookie cookie = Request.Cookies["tasksTo"];                
                if (cookie != null && cookie.Value != null)
                {
                    sTasksAssignedTo = cookie.Value;
                    m_TasksAssignedTo.SelectedValue = sTasksAssignedTo;
                }
                m_TasksAssignedTo.SelectedValue = sTasksAssignedTo;

                string sTasksStatus = m_TasksAssignedTo.SelectedValue;
                cookie = Request.Cookies["taskStatus"];
                if (cookie != null && cookie.Value != null)
                {
                    sTasksStatus = cookie.Value;
                    m_ddlTaskStatus.SelectedValue = sTasksStatus;
                }
            }

            // Bind gridview and set user owned tasks.
            BindTasks();

            //SortExp = gvTasks.SortExpression;
            //SortDir = gvTasks.SortDirection;

            RequestHelper.StoreToCookie("tasksTo", m_TasksAssignedTo.SelectedValue, true);
            RequestHelper.StoreToCookie("taskStatus", m_ddlTaskStatus.SelectedValue, true);
        }

        private void BindTasks()
        {            
            Guid userId = m_TasksAssignedTo.SelectedValue == "1" /*Me*/ ? BrokerUserPrincipal.CurrentPrincipal.UserId : Guid.Empty;
            //List<Task> tasks = Task.GetOpenTasksByAssignedEmployeeId(userId);

            E_TaskStatus taskStatus = m_ddlTaskStatus.SelectedValue == "1" ? E_TaskStatus.Resolved : E_TaskStatus.Active;

            //List<Task> tasks = Task.GetOpenTasksByAssignedUserId(userId, BrokerUser.BrokerId, SortExp, SortDir == SortDirection.Ascending ? "ASC" : "DESC", taskStatus);
            List<ReadOnlyTask> tasks = TaskUtilities.GetTasksInPipeline(BrokerUserPrincipal.CurrentPrincipal, userId, taskStatus, SortExp, SortDir == SortDirection.Ascending ? SortOrder.Ascending : SortOrder.Descending);

            StringBuilder ownedTaskIds = new StringBuilder();
            foreach (ReadOnlyTask t in tasks)
            {
                if (t.TaskOwnerUserId == BrokerUserPrincipal.CurrentPrincipal.UserId)
                {
                    ownedTaskIds.Append(String.Format("{0},", t.TaskId));
                }
            }
            userOwnedTaskIds.Value = ownedTaskIds.ToString().TrimEnd(new char[] { ',' });

            //OrderTasks(ref tasks, SortExp, SortDir);

            gvTasks.DataSource = tasks;
            gvTasks.DataBind();
        }



        

        private void OrderTasks(ref List<Task> tasks, string sortExp, SortDirection sortDir)
        {
            if(string.IsNullOrEmpty(sortExp)) return;

            Type t = typeof(Task);

            if (sortDir == SortDirection.Ascending)
            {

                tasks = tasks.OrderBy(
                    a => t.InvokeMember(
                        sortExp
                        , System.Reflection.BindingFlags.GetProperty
                        , null
                        , a
                        , null
                    )
                ).ToList();
            }
            else
            {
                tasks = tasks.OrderByDescending(
                    a => t.InvokeMember(
                        sortExp
                        , System.Reflection.BindingFlags.GetProperty
                        , null
                        , a
                        , null
                    )
                ).ToList();
            }
        }

        private void ExportToCsv(string csvTaskIds)
        {

            this.Response.Clear();
            this.Response.AddHeader("Content-Disposition", "attachment; filename=\"Tasks.csv\"");
            this.Response.ContentType = "application/text";
            
            //Write header
            this.Response.Write("Task,Subject,Status,Loan Number,Borrower Name,Due Date,Follow-up Date,Last Update,Assigned To,Owner" + Environment.NewLine);


            Guid userId = m_TasksAssignedTo.SelectedValue == "1" /*Me*/ ? BrokerUserPrincipal.CurrentPrincipal.UserId : Guid.Empty;
            E_TaskStatus taskStatus = m_ddlTaskStatus.SelectedValue == "1" ? E_TaskStatus.Resolved : E_TaskStatus.Active;

            List<ReadOnlyTask> tasks = TaskUtilities.GetTasksInPipeline(BrokerUserPrincipal.CurrentPrincipal, userId, taskStatus, SortExp, SortDir == SortDirection.Ascending ? SortOrder.Ascending : SortOrder.Descending);
        

            //List<Task> tasks = Task.GetOpenTasksByAssignedUserId(userId, BrokerUser.BrokerId, SortExp, SortDir == SortDirection.Ascending ? "ASC" : "DESC");


            foreach (ReadOnlyTask task in tasks)
            {
                this.Response.Write(Task.ToCSV(BrokerUserPrincipal.CurrentPrincipal.BrokerId, task.TaskId) + Environment.NewLine);
            }

            Response.Flush();
            this.Response.End();
        }
      
        protected void OnTasksGridBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;

            if (row.RowType == DataControlRowType.Header)
            {
                EncodedLabel sortLbl = new EncodedLabel();
                sortLbl.Text = (SortDir == SortDirection.Ascending ?  "▲" : "▼");
                foreach (TableCell cell in row.Cells)
                {
                    if(cell.Controls.Count > 0 && SortExp ==  ((LinkButton)cell.Controls[0]).CommandArgument)
                    {
                        cell.Controls.Add(sortLbl);                    
                    }
                }
            }


            if (row.DataItem == null)
            {
                return;
            }
            //if (e.Row.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
            {
                bool isRedFlag = false;

                ReadOnlyTask task = (ReadOnlyTask)row.DataItem;

                if (task.TaskDueDate != null)
                {
                    DateTime dtDue = (task.TaskDueDate.GetValueOrDefault()).Date;
                    if (dtDue <= DateTime.Now.Date)
                    {
                        isRedFlag = true;
                        row.Cells[6].Style.Add("color", "red");
                    }
                }
                                

                if (task.TaskFollowUpDate != null)
                {
                    DateTime dt = (task.TaskFollowUpDate.GetValueOrDefault()).Date;
                    if (dt <= DateTime.Now.Date)
                    {
                        isRedFlag = true;
                        row.Cells[7].Style.Add("color", "red");
                    }
                }

                if (isRedFlag) e.Row.Style.Add("font-weight", "bold");
            }
        }

        protected void gvTasks_Sorting(object sender, GridViewSortEventArgs e)
        {
            //string[] strSortExpression = ViewState["SortExpression"].ToString().Split(' ');

            // If the sorting column is the same as the previous one, 
            // then change the sort order.
            //if (strSortExpression[0] == e.SortExpression)
            if (SortExp == e.SortExpression)
            {
                if (SortDir == SortDirection.Ascending)
                {
                    SortDir = SortDirection.Descending;
                }
                else
                {
                    SortDir = SortDirection.Ascending;
                }
            }
            // If sorting column is another column, 
            // then specify the sort order to "Ascending".
            else
            {
                SortExp = e.SortExpression;
                SortDir = SortDirection.Ascending;
                //ViewState["SortExpression"] = e.SortExpression + " " + "ASC";
            }

            // Rebind the GridView control to show sorted data.
            BindTasks();
        }
      
        #region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
        {            
            this.PreRender += new EventHandler(PagePreRender);
            Page.ClientScript.GetPostBackEventReference(this, "");
        }
		#endregion
    }
}
