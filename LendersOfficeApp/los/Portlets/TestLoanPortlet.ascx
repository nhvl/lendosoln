<%@ Control Language="c#" AutoEventWireup="false" Codebehind="TestLoanPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.TestLoanPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script language="javascript">
    function onNewTestClick(purpose) {
        if (purpose == null) {
            purpose = "";
        }

        var args = {
            isTest: true,
            purpose: purpose
        };

        retrieveTemplates(args, "Test Loan");
        return false;
    }

</script>

<TABLE class="Portlet" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD class="PortletHeader" noWrap>Test Loans</TD>
</TR>
<TR>
<TD noWrap>
	<% if (IsNewPmlUIEnabled) { %>
	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<a href="#" onclick="return onNewTestClick('purchase');" class="PortletLink" title="Create new test loan file">Create Purchase Test Loan</a>
	<br>
	
	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<a href="#" onclick="return onNewTestClick('refinance');" class="PortletLink" title="Create new test loan file">Create Refinance Test Loan</a>
	<br>
	<% } else { %>
	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<a href="#" onclick="onNewTestLoanClick()" class="PortletLink" title="Create new test loan file">Create Test Loan</a>
	<br>
	<% } %>
	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<A href="loanfind.aspx?test=1" class="PortletLink" title="Advance search for your test loan">Find Test Loan</A><br>
</TD>
</TR>
<TR>
<TD><img src="../images/shadow_line.gif" width="130" height="2"></td>
</TR>
</TABLE>
