<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PmlPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.PmlPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<SCRIPT language="javascript">
    var tpoWindowHandle = null;
    function onTpoPortalConfigClick() 
    {
        if (tpoWindowHandle != null && !tpoWindowHandle.closed) {
            tpoWindowHandle.focus();
            return false;
        }
        tpoWindowHandle = window.open(<%= AspxTools.JsString(DataAccess.Tools.VRoot) %> + '/los/BrokerAdmin/TPOPortalConfig.aspx', 'TpoPortalConfig', 'width=920,height=780,resizable=yes,status=yes,menubar=no,titlebar=no,top=50,left=100,scrollbars=yes');
        tpoWindowHandle.focus();
    }
	
	function onPmlUserAdminClick()
	{
		window.open( <%= AspxTools.JsString(DataAccess.Tools.VRoot) %> + '/los/BrokerAdmin/PmlUserList.aspx?isfirstload=true' , '_blank' , 'width=1100,height=840,resizable=yes,status=yes,menubar=no,titlebar=no' );
        return false;
    }
    function onPmlCompanyClick() {
        window.open(<%= AspxTools.JsString(DataAccess.Tools.VRoot) %> + '/los/BrokerAdmin/PmlCompanyList.aspx', '_blank', 'width=1400,height=925,resizable=yes,status=yes,menubar=no,titlebar=no, scrollbars=yes');
        return false;
    }

    function onPmlCompanyTiersClick() {
        showModal('/los/BrokerAdmin/PmlCompanyTiers.aspx', '_blank', null, null, null, {hideCloseButton:true});
        return false;
    }
</SCRIPT>
<TABLE cellSpacing="0" cellPadding="0" border="0" width="100%" class="Portlet">
	<TR>
	<TD noWrap class="PortletHeader">
		Originator Portal Admin
	</TD>
	</TR>
	<TR>
	<TD nowrap>
	    <asp:PlaceHolder ID="TpoPortalConfigLink" runat="server">
	        <IMG src=<%= AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp;
		    <A href="#" onclick="onTpoPortalConfigClick(); return false;" class="PortletLink" title="Edit Originator Portal Configuration">
			    Originator Portal Configuration</A>
	        <br />

	    </asp:PlaceHolder>
	    <asp:PlaceHolder ID="TpoCompaniesAndAdminLinks" runat="server">
	        <IMG src=<%= AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp;
            <A href="#" onclick="onPmlCompanyClick();" class="PortletLink" title="Edit PML originating companies">
			    Originating Companies</A>
	        <br />
	        <asp:PlaceHolder ID="TpoCompanyTiersLink" runat="server">
                <IMG src=<%= AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp;
                <A href="#" onclick="onPmlCompanyTiersClick();" class="PortletLink" title="Edit PML originating company tiers">
			        Originating Company Tiers</A>
	            <br />
	        </asp:PlaceHolder>
            <IMG src=<%= AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp;
            <A href="#" onclick="onPmlUserAdminClick();" class="PortletLink" title="Edit PriceMyLoan users">
			    OC User Admin</A>
			    <br />
	    </asp:PlaceHolder>
	</TD>
	</TR>
	<TR>
	<TD><IMG src=<%= AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/images/shadow_line.gif")%> width="130" height="2"></TD>
	</TR>
</TABLE>
