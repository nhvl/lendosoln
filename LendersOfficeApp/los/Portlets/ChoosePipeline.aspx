<%@ Page language="c#" Codebehind="ChoosePipeline.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Portlets.ChoosePipeline" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="ChooseReport" Src="ChooseReport.ascx" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Choose Pipeline Report</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name=vs_defaultClientScript content="JavaScript">
		<meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<style type="text/css">

		.highlightreport
		{ BACKGROUND-COLOR: gainsboro;
		}
		.w-383 { width: 383px; }

		</style>
		<script type="text/javascript">

		function onInit()
		{
			if( document.getElementById("m_errorMessage") != null && document.getElementById("m_errorMessage").value != "" )
			{
				alert( document.getElementById("m_errorMessage").value );
			}

			if( document.getElementById("m_commandToDo") != null )
			{
				if( document.getElementById("m_commandToDo").value == "Close" )
				{
				    var args = { OK: true };
				    onClosePopup(args);
				}
			}

			resize(400, 500);
			Validate(true);
		}
		var isRun = false;
		function DisablePage() {
		    if ( isRun ) {
		        return false;
		    }

		    isRun = true;
		    return true;
		}
		function Validate(textBox) {
		    var tabName = document.getElementById('m_tabName').value;
		    var matchingChars = tabName.match(/[<>&]/);
		    if (matchingChars != null && matchingChars.length > 0) {
		        document.getElementById('m_invalidErr').style.display = '';
		        document.getElementById('m_pipelineName').scrollIntoView();
		        if (!textBox) {
		            return false;
		        }
		    }
		    else {
		        document.getElementById('m_invalidErr').style.display = 'none';
		        if (!textBox) {
		            return DisablePage();
		        }
		    }
		}
		</script>
	</head>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: scroll; overflow-x: hidden;" onload="onInit();">
		<h4 class="page-header">Tab Editor</h4>
		<form id="ChoosePipeline" method="post" runat="server">
			<table cellpadding="4" cellspacing="0" border="0" height="100%" class="w-383">
			<tr height="100%" valign="top">
				<td style="text-align: center; >
				    <asp:Panel ID="m_pipelineName" runat="server">
				        <div style="MARGIN-TOP: 8px; PADDING: 8px; BACKGROUND-COLOR: whitesmoke; BORDER: 2px solid lightgrey;">
				            <table cellpadding="2" cellspacing="0" border="0" width="100%">
				                <tr>
				                    <td class="FieldLabel">
				                        Name
				                        <hr color="LightGrey" size="2" />
				                    </td>
				                </tr>
				                <tr>
				                    <td style="padding-left:30px;">
				                        <asp:TextBox ID="m_tabName" GroupName="tabName" runat="server" onChange="Validate(true)" MaxLength=50 Width=200px></asp:TextBox>
	                                    <asp:HiddenField ID="m_tabUid" runat="server" />
				                    </td>
				                </tr>
				                <tr id="m_invalidErr" style="display:none;">
				                    <td style="font-size:105%; color: Red;">
				                        Invalid tab name. You may not use the following characters: < > &
				                    </td>
				                </tr>
				            </table>
				        </div>
				    </asp:Panel>
				    <asp:Panel ID="m_pipelineVisibility" runat="server">
				        <div style="MARGIN-TOP: 8px; PADDING: 8px; BACKGROUND-COLOR: whitesmoke; BORDER: 2px solid lightgrey;">
						    <table cellpadding="2" cellspacing="0" border="0" width="100%">
						        <tr>
						            <td class="FieldLabel">
							            Visibility
							            <hr color="LightGrey" size="2" />
						            </td>
						        </tr>
						        <tr>
							        <td style="padding-left:30px;">
							            <asp:RadioButton ID="m_showAssigned" GroupName="pipelineVisibility" Text="Show only loans you are assigned to" runat="server" /><br />
							            <asp:RadioButton ID="m_showDefault" GroupName="pipelineVisibility" Text="Show all loans you have access to" runat="server" />
							        </td>
					            </tr>
						    </table>
					    </div>
					</asp:Panel>

				<uc:ChooseReport runat="server" ID="ChooseReport" OnReportChosen="ChooseReport_OnReportChosen" OnReportError="ChooseReport_OnReportError"  />

				</td>
			</tr>
			<tr>
			<td>
				<div style="PADDING: 4px; TEXT-ALIGN: center; WIDTH: 100%;">
				    <ml:nodoubleclickbutton runat="server" ID="OkayBtn" OnClientClick="return Validate(false);" UseSubmitBehavior="true" OnClick="Okay_OnClick"  Text="OK" Width="60px"/>
					<input type="button" value="Cancel" style="WIDTH: 60px;" onclick="onClosePopup();">
				</div>
			</td>
			</tr>
			</table>
			<uc:CModalDlg id="m_Modal" runat="server">
			</uc:cModalDlg>
		</form>
	</body>
</html>
