using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Caching;
using LendersOffice.QueryProcessor;
using LendersOffice.Reports;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.los.Portlets
{
	public partial  class PublishedReportsPortlet : System.Web.UI.UserControl
	{

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		protected void ControlLoad( object sender , System.EventArgs a )
		{
			// Bind published reports to this list.

			try
			{
				if( Page.IsPostBack == false )
				{

                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@BrokerId" , BrokerUser.BrokerId ),
                                                    new SqlParameter( "@IsPublished" , 1 )
                                                };

					using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( BrokerUser.ConnectionInfo, "ListReportQuery" ,  parameters) )
					{
                        m_Published.DataSource = reader;
						m_Published.DataBind();
					}
				}

				if( m_Published.Items.Count == 0 )
				{
                    emptyReportsDiv.Visible = true;
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Unable to load control." , e );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.ControlLoad);

        }
		#endregion



	}

}
