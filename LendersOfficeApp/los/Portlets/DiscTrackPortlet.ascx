<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="RefreshButton" Src="RefreshButton.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DiscTrackPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.DiscTrackPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<STYLE>

	.TrakFilterLabel
	{ COLOR: black
	; FONT: 11px arial
	; WIDTH: 80px
	}

	.TrakFilterCheck
	{ COLOR: black
	; FONT: 11px arial
	; MARGIN-RIGHT: 8px
	; WIDTH: 70px
	}


</STYLE>
<script>
	function _init()
	{
		if (<%= AspxTools.JsGetElementById(m_UIState) %>.value.indexOf("Filter=Hide") == -1)
		{
			document.getElementById('changeFilterBtn').disabled = true;
		}

	    onToggleTrakFilter( <%= AspxTools.ClientId(m_panelTaskFilter) %> , <%= AspxTools.ClientId(m_UIState) %> );
        onToggleTrakFilter( <%= AspxTools.ClientId(m_panelTaskFilter) %> , <%= AspxTools.ClientId(m_UIState) %> );
	}

	function onToggleTrakFilter( oPanel , oStore ) { try
	{
		if( oPanel == null || oStore == null )
		{
			return;
		}

		if( oStore.value.indexOf( "Filter=Hide" ) == -1 )
		{
			oPanel.style.display = "none";
			oStore.value = "Filter=Hide";
			document.getElementById('changeFilterBtn').disabled = false;
		}
		else
		{
			oPanel.style.display = "block";
			oStore.value = "Filter=Show";
			document.getElementById('changeFilterBtn').disabled = true;
		}
	}
	catch( e )
	{
	}}

	<%-- 01/25/06 MF. OPM 3899. Allow user to (de)select all --%>
	function setCheckBoxes(status) { try
	{
		var oInputElements = <%= AspxTools.JsGetElementById(m_panelTaskFilter) %>.getElementsByTagName('input');
		if (oInputElements) {
			for (var i = 0; i < oInputElements.length; i++) {
				var input = oInputElements[i];
				if (input.type == 'checkbox') input.checked = status;
			}
		}
	}
	catch (e)
	{
	}}

    function onClickLoanLinkSet( oBase , oTarget ) { try
	{
	    // Check for valid targets.

	    if( oBase == null || oTarget == null )
	    {
	        return;
	    }
		// Show link set and display loan in bold
        if( oTarget.nextSibling.style.display == "none" )
        {
            // Turn off all previously activated link sets within
            // this grid and then turn on the selected one.
            oTarget.nextSibling.style.display    = "block";
            oTarget.nextSibling.style.fontWeight = "normal";
            oTarget.style.fontWeight = "bold";
        }
        else
        {
            // Turn off the selected one and leave the
            // other link sets alone.

            oTarget.nextSibling.style.display = "none";
			oTarget.style.fontWeight = "normal";
        }
    }
    catch( e )
    {
        window.status = "Selecting " + oTarget.innerText + " failed.";
    }}
</script>
<INPUT type="hidden" id="m_UIState" runat="server">
<TABLE cellspacing="0" cellpadding="3" width="100%" border="0">
<TR valign="top">
<TD align="left">
	<UC1:RefreshButton runat="server" ID="Refreshbutton1" NAME="Refreshbutton1">
	</UC1:RefreshButton>
	<INPUT type="button" id="changeFilterBtn" onfocus="this.blur();" onclick="onToggleTrakFilter( <%= AspxTools.ClientId(m_panelTaskFilter) %> , <%= AspxTools.ClientId(m_UIState) %> );" value="Show Filter Settings">
</TD>
</TR>
</TABLE>
<ASP:Panel id="m_panelTaskFilter" style="DISPLAY: none; TEXT-ALIGN: left; BACKGROUND: whitesmoke; MARGIN: 20px; MARGIN-TOP: 12px; PADDING: 6px; BORDER: 1px inset; WIDTH: 100%;" runat="server">
	<TABLE cellSpacing=0 cellPadding=0 border=0>
		<tr>
			<td colspan=2 style="FONT-SIZE: 10px; COLOR: black"><a href="#" onclick="setCheckBoxes(true)">Check All</a> / <a href="#" onclick="setCheckBoxes(false)">Uncheck All</a>
			</td>
		</tr>
		<tr>
			<td class="TrakFilterLabel">Read status:
			</td>
			<td>
				<ASP:CheckBox id="m_cbReadStatusRead" class="TrakFilterCheck" runat="server" Text="Read" Checked="True">
				</ASP:CheckBox>
				<ASP:CheckBox id="m_cbReadStatusNotRead" class="TrakFilterCheck" runat="server" Text="Not read" Checked="True">
				</ASP:CheckBox>
			</td>
		</tr>
		<tr>
			<td class="TrakFilterLabel">Task status:
			</td>
			<td>
				<ASP:CheckBox id="m_cbTaskStatusActive" class="TrakFilterCheck" runat="server" Text="Active" Checked="True">
				</ASP:CheckBox>
				<ASP:CheckBox id="m_cbTaskStatusDone" class="TrakFilterCheck" runat="server" Text="Done" Checked="True">
				</ASP:CheckBox>
				<ASP:CheckBox id="m_cbTaskStatusSuspended" class="TrakFilterCheck" runat="server" Text="Suspended" Checked="True">
				</ASP:CheckBox>
				<ASP:CheckBox id="m_cbTaskStatusCancelled" class="TrakFilterCheck" runat="server" Text="Cancelled" Checked="True">
				</ASP:CheckBox>
			</td>
		</tr>
		<tr>
			<td class="TrakFilterLabel">Priority:
			</td>
			<td>
				<ASP:CheckBox id="m_cbPriorityLow" class="TrakFilterCheck" runat="server" Text="Low" Checked="True">
				</ASP:CheckBox>
				<ASP:CheckBox id="m_cbPriorityNormal" class="TrakFilterCheck" runat="server" Text="Normal" Checked="True">
				</ASP:CheckBox>
				<ASP:CheckBox id="m_cbPriorityHigh" class="TrakFilterCheck" runat="server" Text="High" Checked="True">
				</ASP:CheckBox>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><asp:Button Text=" Preview " Runat="server"></asp:Button>&nbsp;
				<asp:Button onclick=SaveFilterClick Text=" Save " Runat="server"></asp:Button>&nbsp;
				<INPUT id=hideBtn onclick="onToggleTrakFilter( <%= AspxTools.ClientId(m_panelTaskFilter) %> , <%= AspxTools.ClientId(m_UIState) %> );" type=button value=" Hide ">
			</td>
		</tr>
	</table>
</ASP:Panel>
<ml:CommonDataGrid id="m_trackDg" runat="server" Width="100%" AllowSorting="True" EnableViewState="False" CssClass="DataGrid" AutoGenerateColumns="False">
	<AlternatingItemStyle CssClass="GridAlternatingItem"></AlternatingItemStyle>
	<ItemStyle CssClass="GridItem"></ItemStyle>
	<HeaderStyle CssClass="GridHeader"></HeaderStyle>
	<Columns>
		<asp:TemplateColumn SortExpression="TrackNotifIsRead" HeaderText="Read?">
			<ItemTemplate>
                <span class=<%#AspxTools.HtmlAttribute(GetTrackNotifIsReadCss((bool)DataBinder.Eval(Container.DataItem,"TrackNotifIsRead"), (bool)DataBinder.Eval(Container.DataItem,"NotifIsValid")))%>>
                    <%#AspxTools.HtmlString(DisplayTrackNotifIsReadValue((bool)DataBinder.Eval(Container.DataItem,"TrackNotifIsRead"))) %>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscSubject" HeaderText="Subject">
			<ItemTemplate>
                <span class=<%#AspxTools.HtmlAttribute(GetDiscSubjectCss((bool) DataBinder.Eval(Container.DataItem, "TrackNotifIsRead")))%>>
                    <%#AspxTools.HtmlString((string) DataBinder.Eval(Container.DataItem, "DiscSubject"))%>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:BoundColumn DataField="TrackCreatedDate" SortExpression="TrackCreatedDate" HeaderText="Created Date"></asp:BoundColumn>
		<asp:TemplateColumn SortExpression="DiscRefObjNm1" HeaderText="Loan Number">
			<ItemTemplate>
				<asp:Panel Runat="server" ID="LoanNumberLinks" style="CURSOR: pointer;">
					<div id="m_loanNumberTag" style="DISPLAY: block; COLOR: blue;" onmouseover="style.color = 'orange';" onmouseout="style.color = 'blue';" onclick="onClickLoanLinkSet( <%#AspxTools.JsGetClientIdString(m_trackDg)%> , this );">
						<u>
							<%#AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "DiscRefObjNm1" ))%>
						</u>
					</div><div style="DISPLAY: none; PADDING: 0px; MARGIN-TOP: 2px; MARGIN-BOTTOM: 8px;">
						<asp:Panel id="EditLink" runat="server" style="PADDING-LEFT: 2px;">
							&#x2022;
							<asp:Panel id="Show" runat="server" style="DISPLAY: inline;">
								<a href="javascript:editLoan(<%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString())%>);" title="Edit loan">
									edit
								</a>
							</asp:Panel>
							<asp:HyperLink id="Hide" runat="server" Enabled="False">
								edit
							</asp:HyperLink>
				        </asp:Panel>
						<asp:Panel id="LeadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<asp:Panel id="ShowLeadEdit" runat="server" style="DISPLAY: inline;">
								<a href="javascript:editLead(<%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString())%>);" title="Edit lead">
									edit
								</a>
							</asp:Panel>
							<asp:HyperLink id="DisabledLeadEdit" runat="server" Enabled="False">
								edit
							</asp:HyperLink>
						</asp:Panel>
	   					<asp:Panel id="LoadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:editLoan(<%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString())%>);" title="View loan in editor">
								view
							</a>
	                    </asp:Panel>
	   					<asp:Panel id="LoadLeadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:editLead(<%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString())%>);" title="View lead in editor">
								view
							</a>
	                    </asp:Panel>
						<asp:Panel id="ViewLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:viewLoan(<%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString())%>);" title="View loan summary">
								view
							</a>
						</asp:Panel>
						<asp:Panel id="ViewLeadLink" runat="server" style="PADDING-LEFT: 2px;">
		                    &#x2022;
							<a href="javascript:viewLoan(<%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString())%>);" title="View lead summary">
								view
							</a>
						</asp:Panel>
						<div nowrap style="PADDING-LEFT: 2px;">
							&#x2022;
							<a href="javascript:createTask(<%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "DiscRefObjId").ToString())%>);" title="Insert new task for this loan">
								new task
							</a>
						</div>
					</div>
				</asp:Panel>
				<asp:Panel Runat="server" ID="LoanNumberPlainText" Visible="False">
					<%#AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "DiscRefObjNm1" ))%>
				</asp:Panel>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:BoundColumn DataField="DiscRefObjNm2" SortExpression="DiscRefObjNm2" HeaderText="Borr. Name"></asp:BoundColumn>
		<asp:TemplateColumn SortExpression="DiscPriority" HeaderText="Priority">
			<ItemTemplate>
                <span class=<%#AspxTools.HtmlAttribute(DisplayDiscPriorityClass((int)DataBinder.Eval(Container.DataItem, "DiscPriority")))%>>
                    <%#AspxTools.HtmlString(DisplayDiscPriorityValue((int)DataBinder.Eval(Container.DataItem, "DiscPriority")))%>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:BoundColumn Visible="False" DataField="TrackId">
		</asp:BoundColumn>
		<asp:TemplateColumn SortExpression="UserLastNm" HeaderText="Tracked Person">
			<ItemTemplate>
				<%#AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "UserFirstNm" ))%>
				<%#AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "UserLastNm"  ))%>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscDueDate" HeaderText="Due Date">
			<ItemTemplate>
                <span class=<%#AspxTools.HtmlAttribute(DisplayDiscDueDateClass(DataBinder.Eval( Container.DataItem, "DiscDueDate" )))%>>
                    <%#AspxTools.HtmlString(DataBinder.Eval( Container.DataItem, "DiscDueDate" ).ToString())%>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="DiscStatus" HeaderText="Status">
			<ItemTemplate>
                <span class=<%#AspxTools.HtmlAttribute(DisplayDiscStatusClass((int)DataBinder.Eval(Container.DataItem,"DiscStatus")))%>>
                    <%#AspxTools.HtmlString(DisplayDiscStatusValue((int)DataBinder.Eval(Container.DataItem,"DiscStatus")))%>
                </span>
			</ItemTemplate>
		</asp:TemplateColumn>
	</Columns>
</ml:CommonDataGrid>
