<%@ Register TagPrefix="ils" TagName="EmployeeRoleChooserLink" Src="../common/EmployeeRoleChooserLink.ascx" %><%@ Import Namespace="LendersOffice.AntiXss" %>

<%@ Page Language="c#" CodeBehind="AssignAndChangeStatus.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Portlets.AssignAndChangeStatus" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head id="Head1" runat="server">
  <title>AssignAndChangeStatus</title>
  <style>
    .ErrorMessage { color:Red; font-weight:bold}
    .MainDiv { padding: 10px;  height: 450px; overflow: auto; }
    h2 { padding: 0; margin: 0; padding-top: 10px;  }
    .ErrorGrid { width: 400px; }

    #MainPanel {
        flex: 1 1 100%;
        display: flex;
        flex-direction: column;
    }

    #MainPanel div, #MainPanel h4 {
        flex: 0 0 auto;
    }

    #MainPanel div.assignment-table-container {
        flex: 1 1 100%;
    }

    .assignment-table {
        height: 100%;
    }

    .form-content-container {
        display: flex;
        flex-direction: column;
        height: 100%;
    }

    .form-content-container div {
        flex: 1 0 auto;
    }
  </style>
  
  <script type="text/javascript">
    var sPmlBrokerId;
    function _init()
    {
      resizeForIE6And7(550, 600);
    }

    function selectNewRB(id)
    {
      var o = document.getElementById(id);
      if (o != null)
      {
        o.checked = true;
      }
    }

    function f_close(convertedLeadToLoan, convertedIds)
    {
        var arg = {};
      arg.OK = true;
      arg.bConvertedLeadToLoan = convertedLeadToLoan;
      arg.convertedIds = convertedIds.split(',');
      onClosePopup(arg);
    }

  </script>
</head>
<body ms_positioning="FlowLayout" scroll="no" bgcolor="gainsboro">
  <form id="AssignAndChangeStatus" method="post" runat="server">
  <asp:HiddenField runat="server" ID="sLIds" />
  <asp:PlaceHolder runat="server" ID="NonPmlScript">

    <script type="text/javascript">
      function onLoClearRestriction()
      {
        var text = document.getElementById("<%= AspxTools.ClientId(m_officerChoice_m_Text) %>");
        var data = document.getElementById("<%= AspxTools.ClientId(m_officerChoice_m_Data) %>");
        var span = document.getElementById("m_officerChoice_m_User");

        span.innerText = 'None';
        text.value = 'None';
        data.value = 'None';
      }

      function onLoPickLink()
      {
        var text = document.getElementById("<%= AspxTools.ClientId(m_officerChoice_m_Text) %>");
        var data = document.getElementById("<%= AspxTools.ClientId(m_officerChoice_m_Data) %>");
        var span = document.getElementById("m_officerChoice_m_User");

        var url = "/los/EmployeeRole.aspx?role=Agent&roledesc=Loan+Officer&show=Search";


        showModal(url, null, null, null, function(arg){
          
          if (arg.OK != null && arg.OK)
          {
            span.innerText = arg.EmployeeName;
            text.value = arg.EmployeeName;
            data.value = arg.EmployeeId;
            selectNewRB('<%=AspxTools.ClientId(m_officerNewRB)%>');
          }
        });
      }
    </script>
 
  </asp:PlaceHolder>
  <asp:PlaceHolder runat="server" ID="PmlEnabledBrokerScripts">

    <script type="text/javascript">
      var loPmlBrokerId = '';
      var bpPmlBrokerId = '';
      var externalSecondaryPmlBrokerId = '';
      var externalPostCloserPmlBrokerId = '';

      function updateLoPmlBrokerId(newvalue)
      {
        loPmlBrokerId = newvalue;
      }

      function updateBpPmlBrokerId(newvalue)
      {

        bpPmlBrokerId = newvalue;
    }

    function updateExternalSecondaryPmlBrokerId(newvalue) {
        externalSecondaryPmlBrokerId = newvalue;
    }
    
    function updateExternalPostCloserPmlBrokerId(newvalue) {
        externalPostCloserPmlBrokerId = newvalue;
    }

      function onLoClearRestriction()
      {
        var text = document.getElementById("<%= AspxTools.ClientId(m_officerChoice_m_Text) %>");
        var data = document.getElementById("<%= AspxTools.ClientId(m_officerChoice_m_Data) %>");
        var span = document.getElementById("m_officerChoice_m_User");

        span.innerText = 'None';
        text.value = 'None';
        data.value = 'None';
        updateLoPmlBrokerId('');
        disableBpPickLink(false);
        disableExternalSecondaryPickLink(false);
        disableExternalPostCloserPickLink(false);
      }

      function onLoPickLink()
      {
        var text = document.getElementById("<%= AspxTools.ClientId(m_officerChoice_m_Text) %>");
        var data = document.getElementById("<%= AspxTools.ClientId(m_officerChoice_m_Data) %>");
        var span = document.getElementById("m_officerChoice_m_User");

        var url = "/los/EmployeeRole.aspx?role=Agent&roledesc=Loan+Officer&show=Search&copyContact=false";

        if (bpPmlBrokerId != '') {
            url += '&origid=' + bpPmlBrokerId;
        }
        else if (externalSecondaryPmlBrokerId != '') {
            url += '&origid=' + externalSecondaryPmlBrokerId;
        }
        else if (externalPostCloserPmlBrokerId != '') {
            url += '&origid=' + externalPostCloserPmlBrokerId;
        }
        
        showModal(url, null, null, null, function(arg){
          
            if (arg.OK != null && arg.OK)
            {
                span.innerText = arg.EmployeeName;
                text.value = arg.EmployeeName;
                data.value = arg.EmployeeId;
                updateLoPmlBrokerId(arg.PmlBrokerId);
                var assignedLqbUserLO = loPmlBrokerId == '00000000-0000-0000-0000-000000000000';
                disableBpPickLink(assignedLqbUserLO);
              disableExternalSecondaryPickLink(assignedLqbUserLO);
              disableExternalPostCloserPickLink(assignedLqbUserLO);
              selectNewRB('<%=AspxTools.ClientId(m_officerNewRB)%>');
            }
        });
      }

      function onBpClearRestriction()
      {
        var text = document.getElementById("<%= AspxTools.ClientId(m_bpChoice_Text) %>");
        var data = document.getElementById("<%= AspxTools.ClientId(m_bpChoice_Data) %>");
        var span = document.getElementById("m_bpChoice_UserText");

        span.innerText = 'None';
        text.value = 'None';
        data.value = 'None';
        updateBpPmlBrokerId('');
      }

      function onBpPickLink()
      {
        var text = document.getElementById("<%= AspxTools.ClientId(m_bpChoice_Text) %>");
        var data = document.getElementById("<%= AspxTools.ClientId(m_bpChoice_Data) %>");
        var bppicklink = document.getElementById('bppicklink');

        var span = document.getElementById("m_bpChoice_UserText");

        if (bppicklink.style.color == 'gray')
        {
          return;
        }

        var url = "/los/EmployeeRole.aspx?role=BrokerProcessor&roledesc=Processor+(External)&show=Search&copyContact=false";

        if (loPmlBrokerId != '') {
            url += '&origid=' + loPmlBrokerId;
        }
        else if (externalSecondaryPmlBrokerId != '') {
            url += '&origid=' + externalSecondaryPmlBrokerId;
        }
        else if (externalPostCloserPmlBrokerId != '') {
            url += '&origid=' + externalPostCloserPmlBrokerId;
        }

        showModal(url, null, null, null, function(arg){ 
          if (arg.OK != null && arg.OK)
          {
            span.innerText = arg.EmployeeName;
            text.value = arg.EmployeeName;
            data.value = arg.EmployeeId;
            updateBpPmlBrokerId(arg.PmlBrokerId)
            selectNewRB('<%=AspxTools.ClientId(m_bpSectionNewRb)%>');
  
          }
        });
      }

      function disableBpPickLink(disable)
      {
        var bppicklink = document.getElementById('bppicklink');
        if (disable)
        {
          bppicklink.style.color = "gray";
          bppicklink.style.cursor = 'default';
          bppicklink.innerText = '(LO is required to be a PML user)';
          var text = document.getElementById("<%= AspxTools.ClientId(m_bpChoice_Text) %>");
          var data = document.getElementById("<%= AspxTools.ClientId(m_bpChoice_Data) %>");
          var span = document.getElementById("m_bpChoice_UserText");

          text.value = 'None';
          data.value = 'None';
          span.innerText = 'None';
        }
        else
        {
          bppicklink.style.color = "blue";
          bppicklink.style.cursor = 'hand';

          bppicklink.innerText = 'pick user';
        }
    }

    function onExternalSecondaryClearRestriction() {
        var text = document.getElementById("<%= AspxTools.ClientId(m_externalSecondaryChoice_Text) %>");
        var data = document.getElementById("<%= AspxTools.ClientId(m_externalSecondaryChoice_Data) %>");
        var span = document.getElementById("m_externalSecondaryChoice_UserText");

        span.innerText = 'None';
        text.value = 'None';
        data.value = 'None';

        updateExternalSecondaryPmlBrokerId('');
    }

    function onExternalSecondaryPickLink() {
        var text = document.getElementById("<%= AspxTools.ClientId(m_externalSecondaryChoice_Text) %>");
        var data = document.getElementById("<%= AspxTools.ClientId(m_externalSecondaryChoice_Data) %>");
        var pickLink = document.getElementById('externalSecondaryPickLink');

        var span = document.getElementById("m_externalSecondaryChoice_UserText");

        if (pickLink.style.color == 'gray') {
            return;
        }

        var url = "/los/EmployeeRole.aspx?role=ExternalSecondary&roledesc=Secondary+(External)&show=Search&copyContact=false";

        if (loPmlBrokerId != '') {
            url += '&origid=' + loPmlBrokerId;
        } else if (bpPmlBrokerId != '') {
            url += '&origid=' + bpPmlBrokerId;
        } else if (externalPostCloserPmlBrokerId != '') {
            url += '&origid=' + externalPostCloserPmlBrokerId;
        }

        showModal(url, null, null, null, function(arg){
          if (arg.OK != null && arg.OK) {
              span.innerText = arg.EmployeeName;
              text.value = arg.EmployeeName;
              data.value = arg.EmployeeId;
              updateExternalSecondaryPmlBrokerId(arg.PmlBrokerId)
              selectNewRB('<%=AspxTools.ClientId(m_externalSecondaryNewRb)%>');
          }
        });
    }

    function disableExternalSecondaryPickLink(disable) {
        var pickLink = document.getElementById('externalSecondaryPickLink');

        if (disable) {
            pickLink.style.color = "gray";
            pickLink.style.cursor = 'default';
            pickLink.innerText = '(LO is required to be a PML user)';

            var text = document.getElementById("<%= AspxTools.ClientId(m_externalSecondaryChoice_Text) %>");
            var data = document.getElementById("<%= AspxTools.ClientId(m_externalSecondaryChoice_Data) %>");
            var span = document.getElementById("m_externalSecondaryChoice_UserText");

            text.value = 'None';
            data.value = 'None';
            span.innerText = 'None';
        }
        else {
            pickLink.style.color = "blue";
            pickLink.style.cursor = 'hand';

            pickLink.innerText = 'pick user';
        }
    }
    
    function onExternalPostCloserClearRestriction() {
        var text = document.getElementById("<%= AspxTools.ClientId(m_externalPostCloserChoice_Text) %>");
        var data = document.getElementById("<%= AspxTools.ClientId(m_externalPostCloserChoice_Data) %>");
        var span = document.getElementById("m_externalPostCloserChoice_UserText");

        span.innerText = 'None';
        text.value = 'None';
        data.value = 'None';

        updateExternalPostCloserPmlBrokerId('');
    }

    function onExternalPostCloserPickLink() {
        var text = document.getElementById("<%= AspxTools.ClientId(m_externalPostCloserChoice_Text) %>");
        var data = document.getElementById("<%= AspxTools.ClientId(m_externalPostCloserChoice_Data) %>");
        var pickLink = document.getElementById('externalPostCloserPickLink');

        var span = document.getElementById("m_externalPostCloserChoice_UserText");

        if (pickLink.style.color == 'gray') {
            return;
        }

        var url = "/los/EmployeeRole.aspx?role=ExternalPostCloser&roledesc=Post-Closer+(External)&show=Search&copyContact=false";

        if (loPmlBrokerId != '') {
            url += '&origid=' + loPmlBrokerId;
        } else if (bpPmlBrokerId != '') {
            url += '&origid=' + bpPmlBrokerId;
        } else if (externalSecondaryPmlBrokerId != '') {
            url += '&origid=' + externalSecondaryPmlBrokerId;
        }

        showModal(url, null, null, null, function(arg){
          if (arg.OK != null && arg.OK) {
            span.innerText = arg.EmployeeName;
            text.value = arg.EmployeeName;
            data.value = arg.EmployeeId;
            updateExternalPostCloserPmlBrokerId(arg.PmlBrokerId)
            selectNewRB('<%=AspxTools.ClientId(m_externalPostCloserNewRb)%>');
          }
        });

    }
    
    function disableExternalPostCloserPickLink(disable) {
        var pickLink = document.getElementById('externalPostCloserPickLink');

        if (disable) {
            pickLink.style.color = "gray";
            pickLink.style.cursor = 'default';
            pickLink.innerText = '(LO is required to be a PML user)';

            var text = document.getElementById("<%= AspxTools.ClientId(m_externalPostCloserChoice_Text) %>");
            var data = document.getElementById("<%= AspxTools.ClientId(m_externalPostCloserChoice_Data) %>");
            var span = document.getElementById("m_externalPostCloserChoice_UserText");

            text.value = 'None';
            data.value = 'None';
            span.innerText = 'None';
        }
        else {
            pickLink.style.color = "blue";
            pickLink.style.cursor = 'hand';

            pickLink.innerText = 'pick user';
        }
    }
    </script>

  </asp:PlaceHolder>
      <div class="form-content-container">
  <h4 class="page-header">Set new loan status and origination branch</h4>
  <div width="100%" border="0" cellspacing="2" cellpadding="3" id="MainPanel" runat="server">
        <table>
          <tr>
            <td class="FieldLabel" width="152" nowrap><ml:EncodedLabel ID="loanLeadStatusLabel" runat="server" Text="Loan Status" /></td>
            <td nowrap><asp:RadioButton ID="m_statusOldRB" runat="server" GroupName="status" Checked="True" Text="Unchanged" /> </td>
            <td nowrap><asp:RadioButton ID="m_statusNewRB" runat="server" GroupName="status" /></td>
            <td nowrap>
              <asp:DropDownList ID="m_statusDDL" runat="server" OnChange="selectNewRB('m_statusNewRB');" Width="185px" />
            </td>
          </tr>
            <tr>
              <td class="FieldLabel" nowrap>Branch</td>
              <td nowrap><asp:RadioButton ID="m_branchOldRB" runat="server" Checked="True" GroupName="branch" Text="Unchanged" /></td>
              <td nowrap><asp:RadioButton ID="m_branchNewRB" runat="server" GroupName="branch" /></td>
              <td nowrap title="<% if ( !m_branchDDL.Enabled ) { %>Only Managers with 'Corporate' access can modify this<% } %>">
                <asp:DropDownList ID="m_branchDDL" runat="server" OnChange="selectNewRB('m_branchNewRB');" Width="185px" />
              </td>
            </tr>
        </table>
      <div class="FormTableHeader" height="0">Assignments </div>
      <div class="FieldLabel"><asp:CheckBox ID="m_copyToOfficialContact" runat="server" Text="Copy to official contact list" /></div>
        <div class="assignment-table-container" style="overflow-y: scroll;">
        <table border="0" class="assignment-table" style="overflow-y: scroll">
      <tr>
        <td class="FieldLabel" nowrap>Call Center Agent</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_telemarketerOldRB" runat="server" GroupName="telemarketer" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_telemarketerNewRB" runat="server" GroupName="telemarketer"></asp:RadioButton> </td>
              <td nowrap onclick="selectNewRB( 'm_telemarketerNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_telemarketerChoice" runat="server" Data="None" Text="None" Role="Telemarketer" Desc="Call Center Agent" Mode="Search" Width="80px">
						<FixedChoice Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Closer</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_closerOldRB" runat="server" GroupName="closer" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_closerNewRB" runat="server" GroupName="closer" /> </td>
              <td nowrap onclick="selectNewRB( 'm_closerNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_closerChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="Closer" Desc="Closer" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Collateral Agent</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_collateralAgentOldRB" runat="server" GroupName="collateralAgent" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_collateralAgentNewRB" runat="server" GroupName="collateralAgent" /> </td>
              <td nowrap onclick="selectNewRB( 'm_collateralAgentNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_collateralAgentChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="CollateralAgent" Desc="Collateral Agent" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Credit Auditor</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_creditAuditorOldRB" runat="server" GroupName="creditAuditor" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_creditAuditorNewRB" runat="server" GroupName="creditAuditor" /> </td>
              <td nowrap onclick="selectNewRB( 'm_creditAuditorNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_creditAuditorChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="CreditAuditor" Desc="Credit Auditor" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Disclosure Desk</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_disclosureDeskOldRB" runat="server" GroupName="disclosureDesk" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_disclosureDeskNewRB" runat="server" GroupName="disclosureDesk" /> </td>
              <td nowrap onclick="selectNewRB( 'm_disclosureDeskNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_disclosureDeskChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="DisclosureDesk" Desc="Disclosure Desk" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Doc Drawer</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_docDrawerOldRB" runat="server" GroupName="docDrawer" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_docDrawerNewRB" runat="server" GroupName="docDrawer" /> </td>
              <td nowrap onclick="selectNewRB( 'm_docDrawerNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_docDrawerChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="DocDrawer" Desc="Doc Drawer" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Funder</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_funderOldRB" runat="server" GroupName="funder" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_funderNewRB" runat="server" GroupName="funder" /> </td>
              <td nowrap onclick="selectNewRB( 'm_funderNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_funderChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="Funder" Desc="Funder" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Insuring</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_insuringOldRB" runat="server" GroupName="insuring" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_insuringNewRB" runat="server" GroupName="insuring" /> </td>
              <td nowrap onclick="selectNewRB( 'm_insuringNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_insuringChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="Insuring" Desc="Insuring" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Junior Processor</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_juniorProcessorOldRB" runat="server" GroupName="juniorProcessor" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_juniorProcessorNewRB" runat="server" GroupName="juniorProcessor" /> </td>
              <td nowrap onclick="selectNewRB( 'm_juniorProcessorNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_juniorProcessorChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="JuniorProcessor" Desc="Junior Processor" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Junior Underwriter</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_juniorUnderwriterOldRB" runat="server" GroupName="juniorUnderwriter" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_juniorUnderwriterNewRB" runat="server" GroupName="juniorUnderwriter" /> </td>
              <td nowrap onclick="selectNewRB( 'm_juniorUnderwriterNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_juniorUnderwriterChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="JuniorUnderwriter" Desc="Junior Underwriter" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Legal Auditor</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_legalAuditorOldRB" runat="server" GroupName="legalAuditor" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_legalAuditorNewRB" runat="server" GroupName="legalAuditor" /> </td>
              <td nowrap onclick="selectNewRB( 'm_legalAuditorNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_legalAuditorChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="LegalAuditor" Desc="Legal Auditor" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Lender Account Executive</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_lenderExecOldRB" runat="server" GroupName="lenderacctexec" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_lenderExecNewRB" runat="server" GroupName="lenderacctexec" /> </td>
              <td nowrap onclick="selectNewRB( 'm_lenderExecNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_lenderExecChoice" runat="server" Data="None" Text="None" Role="LenderAccountExec" Desc="Lender Account Executive" Mode="Search" Width="80px">
						<FixedChoice Data="None" Text="None"> clear </FixedChoice>
						<FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr runat="server" id="LoSection">
        <td class="FieldLabel" width="152" nowrap>Loan Officer </td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton onclick="onLoClearRestriction(true);" ID="m_officerOldRB" runat="server" GroupName="officer" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_officerNewRB" runat="server" GroupName="officer" /></td>
              <td nowrap onclick="selectNewRB( 'm_officerNewRB');">
                <div style="display: inline; cursor: default" id="m_officerChoice_base">
                  <span style="width: 80px" id="m_officerChoice_m_User">None</span> 
                  <input id="m_officerChoice_m_Text" runat="server" value="None" type="hidden"> 
                  <input id="m_officerChoice_m_Data" runat="server" value="None" type="hidden"> 
                  [ <span><a style="cursor: pointer; text-decoration: underline" onclick="onLoClearRestriction(); return false; ">clear</a> </span><span style="padding-left: 2px; padding-right: 2px">| </span><span><a style="cursor: pointer; text-decoration: underline" onclick="onLoPickLink()">pick user</a> </span>]
                </div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Loan Officer Assistant</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_loanOfficerAssistantOldRB" runat="server" GroupName="loanOfficerAssistant" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_loanOfficerAssistantNewRB" runat="server" GroupName="loanOfficerAssistant" /> </td>
              <td nowrap onclick="selectNewRB( 'm_loanOfficerAssistantNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_loanOfficerAssistantChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="LoanOfficerAssistant" Desc="Loan Officer Assistant" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Loan Opener</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_openerOldRB" runat="server" GroupName="opener" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_openerNewRB" runat="server" GroupName="opener" /> </td>
              <td nowrap onclick="selectNewRB( 'm_openerNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_openerChoice" runat="server" Data="None" Text="None" Role="LoanOpener" Desc="Loan Opener" Mode="Search" Width="80px">
						<FixedChoice Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Lock Desk</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_lockDeskOldRB" runat="server" GroupName="lockdesk" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_lockDeskNewRB" runat="server" GroupName="lockdesk"></asp:RadioButton> </td>
              <td nowrap onclick="selectNewRB( 'm_lockDeskNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_lockDeskChoice" runat="server" Data="None" Text="None" Role="LockDesk" Desc="Lock Desk" Mode="Search" Width="80px">
						<FixedChoice Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Manager</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_managerOldRB" runat="server" GroupName="manager" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_managerNewRB" runat="server" GroupName="manager" /> </td>
              <td nowrap onclick="selectNewRB( 'm_managerNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_managerChoice" runat="server" Data="None" Text="None" Role="Manager" Desc="Manager" Mode="Search" Width="80px">
						<FixedChoice Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Post-Closer</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_postCloserOldRB" runat="server" GroupName="postCloser" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_postCloserNewRB" runat="server" GroupName="postCloser" /> </td>
              <td nowrap onclick="selectNewRB( 'm_postCloserNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_postCloserChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="PostCloser" Desc="Post-Closer" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <asp:PlaceHolder runat="server" ID="ExternalPostCloser">
        <tr>
          <td class="FieldLabel" width="152" nowrap>Post-Closer (External)</td>
          <td nowrap>
            <table cellspacing="0" cellpadding="0">
              <tr>
                <td nowrap><asp:RadioButton ID="m_externalPostCloserOldRb" onclick="onExternalPostCloserClearRestriction()" runat="server" GroupName="externalPostCloser" Checked="True" Text="Unchanged" /> </td>
                <td nowrap><asp:RadioButton ID="m_externalPostCloserNewRb" runat="server" GroupName="externalPostCloser" /> </td>
                <td nowrap>
                  <div style="display: inline; cursor: default" id="m_externalPostCloserChoice_base">
                    <span style="width: 80px" id="m_externalPostCloserChoice_UserText">None</span> 
                    <input id="m_externalPostCloserChoice_Text" runat="server" value="None" type="hidden"> 
                    <input id="m_externalPostCloserChoice_Data" runat="server" value="None" type="hidden"> 
                    [<span>
                        <a style="cursor: pointer; text-decoration: underline" onclick="onExternalPostCloserClearRestriction();  selectNewRB( '<%= AspxTools.ClientId(m_externalPostCloserNewRb) %>' );return false; ">clear</a>
                     </span>
                     <span style="padding-left: 2px; padding-right: 2px">|</span>
                     <span>
                         <a style="cursor: pointer; text-decoration: underline" onclick="onExternalPostCloserPickLink()" runat="server" id="externalPostCloserPickLink">pick user</a>
                     </span>]
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </asp:PlaceHolder>
      <tr>
        <td class="FieldLabel" nowrap>Processor</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_processorOldRB" runat="server" GroupName="processor" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_processorNewRB" runat="server" GroupName="processor" /> </td>
              <td nowrap onclick="selectNewRB( 'm_processorNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_processorChoice" runat="server" Data="None" Text="None" Role="Processor" Desc="Processor" Mode="Search" Width="80px" EnableCopyToOfficialContact="false">
						<FixedChoice Data="None" Text="None"> clear </FixedChoice>
						<FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <asp:Panel runat="server" ID="m_bpSection">
        <tr>
          <td class="FieldLabel" width="152" nowrap>Processor (External)</td>
          <td nowrap>
            <table cellspacing="0" cellpadding="0">
              <tr>
                <td nowrap><asp:RadioButton ID="m_bpSectionOldRb" onclick="onBpClearRestriction()" runat="server" GroupName="bp" Checked="True" Text="Unchanged" /> </td>
                <td nowrap><asp:RadioButton ID="m_bpSectionNewRb" runat="server" GroupName="bp" /> </td>
                <td nowrap>
                  <div style="display: inline; cursor: default" id="m_bpChoice_base">
                    <span style="width: 80px" id="m_bpChoice_UserText">None</span> 
                    <input id="m_bpChoice_Text" runat="server" value="None" type="hidden"> 
                    <input id="m_bpChoice_Data" runat="server" value="None" type="hidden"> 
                    [ <span><a style="cursor: pointer; text-decoration: underline" onclick="onBpClearRestriction();  selectNewRB( '<%= AspxTools.ClientId(m_bpSectionNewRb) %>' );return false; ">clear</a> </span><span style="padding-left: 2px; padding-right: 2px">| </span><span><a style="cursor: pointer; text-decoration: underline" onclick="onBpPickLink()" runat="server" id="bppicklink">pick user</a> </span>]
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </asp:Panel>
      <tr>
        <td class="FieldLabel" nowrap>Purchaser</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_purchaserOldRB" runat="server" GroupName="purchaser" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_purchaserNewRB" runat="server" GroupName="purchaser" /> </td>
              <td nowrap onclick="selectNewRB( 'm_purchaserNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_purchaserChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="Purchaser" Desc="Purchaser" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>QC Compliance</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_qcComplianceOldRB" runat="server" GroupName="qcCompliance" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_qcComplianceNewRB" runat="server" GroupName="qcCompliance" /> </td>
              <td nowrap onclick="selectNewRB( 'm_qcComplianceNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_qcComplianceChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="QCCompliance" Desc="QC Compliance" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Real Estate Agent</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_realEstateOldRB" runat="server" GroupName="realestate" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_realEstateNewRB" runat="server" GroupName="realestate" /> </td>
              <td nowrap onclick="selectNewRB( 'm_realEstateNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_realEstateChoice" runat="server" Data="None" Text="None" Role="RealEstateAgent" Desc="Real Estate Agent" Mode="Search" Width="80px">
						<FixedChoice Data="None" Text="None"> clear </FixedChoice>
						<FixedChoice Data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Secondary</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_secondaryOldRB" runat="server" GroupName="secondary" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_secondaryNewRB" runat="server" GroupName="secondary" /> </td>
              <td nowrap onclick="selectNewRB( 'm_secondaryNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_secondaryChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="Secondary" Desc="Secondary" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <asp:PlaceHolder runat="server" ID="ExternalSecondary">
        <tr>
          <td class="FieldLabel" width="152" nowrap>Secondary (External)</td>
          <td nowrap>
            <table cellspacing="0" cellpadding="0">
              <tr>
                <td nowrap><asp:RadioButton ID="m_externalSecondaryOldRb" onclick="onExternalSecondaryClearRestriction()" runat="server" GroupName="externalSecondary" Checked="True" Text="Unchanged" /> </td>
                <td nowrap><asp:RadioButton ID="m_externalSecondaryNewRb" runat="server" GroupName="externalSecondary" /> </td>
                <td nowrap>
                  <div style="display: inline; cursor: default" id="m_externalSecondaryChoice_base">
                    <span style="width: 80px" id="m_externalSecondaryChoice_UserText">None</span> 
                    <input id="m_externalSecondaryChoice_Text" runat="server" value="None" type="hidden"> 
                    <input id="m_externalSecondaryChoice_Data" runat="server" value="None" type="hidden"> 
                    [<span>
                        <a style="cursor: pointer; text-decoration: underline" onclick="onExternalSecondaryClearRestriction();  selectNewRB( '<%= AspxTools.ClientId(m_externalSecondaryNewRb) %>' );return false; ">clear</a>
                    </span>
                    <span style="padding-left: 2px; padding-right: 2px">|</span>
                    <span>
                        <a style="cursor: pointer; text-decoration: underline" onclick="onExternalSecondaryPickLink()" runat="server" id="externalSecondaryPickLink">pick user</a> 
                    </span>]
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </asp:PlaceHolder>
      <tr>
        <td class="FieldLabel" nowrap>Servicing</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_servicingOldRB" runat="server" GroupName="servicing" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_servicingNewRB" runat="server" GroupName="servicing" /> </td>
              <td nowrap onclick="selectNewRB( 'm_servicingNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_servicingChoice" runat="server" GenerateScripts="true" Data="None" Text="None" Role="Servicing" Desc="Servicing" Mode="Search" Width="80px">
						<FixedChoice  Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Shipper</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_shipperOldRB" runat="server" GroupName="shipper" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_shipperNewRB" runat="server" GroupName="shipper" /> </td>
              <td nowrap onclick="selectNewRB( 'm_shipperNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_shipperChoice" runat="server" Data="None" Text="None" Role="Shipper" Desc="Shipper" Mode="Search" Width="80px">
						<FixedChoice Data="None" Text="None"> clear </FixedChoice>
						<FixedChoice Data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="FieldLabel" nowrap>Underwriter</td>
        <td nowrap>
          <table cellspacing="0" cellpadding="0">
            <tr>
              <td nowrap><asp:RadioButton ID="m_underwriterOldRB" runat="server" GroupName="underwriter" Checked="True" Text="Unchanged" /> </td>
              <td nowrap><asp:RadioButton ID="m_underwriterNewRB" runat="server" GroupName="underwriter"></asp:RadioButton> </td>
              <td nowrap onclick="selectNewRB( 'm_underwriterNewRB' );">
                <ils:EmployeeRoleChooserLink ID="m_underwriterChoice" runat="server" Data="None" Text="None" Role="Underwriter" Desc="Underwriter" Mode="Search" Width="80px">
						<FixedChoice Data="None" Text="None">
							clear
						</FixedChoice><FixedChoice data="assign" Text="assign" > pick user </FixedChoice>
                </ils:EmployeeRoleChooserLink>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
        </div>
    <div id="ErrorRow" runat="server" visible="false">
        <div class="ErrorMessage"> <ml:EncodedLiteral runat="server" ID="FixableErrorMessage"></ml:EncodedLiteral> 
    </div>
    </div>
        <div style="padding-right: 8px; padding-left: 8px; padding-bottom: 8px; padding-top: 8px; text-align: center">
          <asp:Button ID="m_okBtn" runat="server" Text="  OK  " OnClientClick="this.disabled=true;" UseSubmitBehavior="false" OnClick="OnOKClick"></asp:Button>
          <input type="button" value="Cancel" onclick="onClosePopup();">
        </div>
  </div>
  
    <asp:Panel CssClass="MainDiv" runat="server" ID="MainErrorPanel" Visible="false" >
  
      <asp:PlaceHolder runat="server" ID="AccessDeniedPanel" Visible="false">
      <h2 class="FieldLabel ErrorMessage" >The selected
          operation failed for the following loan files. 
          <br />You have read-only access to them:</h2>
          <asp:GridView runat="server" ID="AccessDeniedErrorGrid" CssClass="ErrorGrid" AutoGenerateColumns="false" HeaderStyle-CssClass="GridHeader" AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem" > 
              <Columns>
                  <asp:BoundField HeaderText="Loan Number" DataField="Item1" />
                  <asp:BoundField HeaderText="Borrower Name" DataField="Item2" />
              </Columns>
          </asp:GridView>
      </asp:PlaceHolder>
      <asp:PlaceHolder  runat="server" ID="GenericErrorPanel" Visible="false">
        <h2 class="FieldLabel ErrorMessage" >The selected
          operation failed for the following loan files:</h2>
          <asp:GridView runat="server" ID="GenericErrorGrid" CssClass="ErrorGrid" AutoGenerateColumns="false" HeaderStyle-CssClass="GridHeader" AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem" >
              <Columns>
                  <asp:BoundField HeaderText="Loan Number" DataField="Item1" />
                  <asp:BoundField HeaderText="Borrower Name" DataField="Item2" />
                  <asp:BoundField HeaderText="Message" DataField="Item3" />
              </Columns>
          </asp:GridView>
      </asp:PlaceHolder> 
      <asp:PlaceHolder runat="server" ID="RecentlyModifiedErrorPanel" Visible="false">
        <h2 class="FieldLabel ErrorMessage" >The
          following loans are being edited:</h2>
          <asp:GridView runat="server" ID="RecentlyModifiedLoansGrid" CssClass="ErrorGrid" AutoGenerateColumns="false" HeaderStyle-CssClass="GridHeader" AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem" >
              <Columns>
                  <asp:BoundField HeaderText="Loan Number" DataField="Item1"   />
                  <asp:BoundField HeaderText="Borrower Name" DataField="Item2" />
                  <asp:BoundField HeaderText="Modified By" DataField="Item3" />
              </Columns>
          </asp:GridView>
      </asp:PlaceHolder>
      
      <asp:PlaceHolder runat="server" ID="OCErrorPanel">
         <h2 class="FieldLabel ErrorMessage"> <ml:EncodedLiteral runat="server" ID="OCErrorMessage"></ml:EncodedLiteral>
          <asp:GridView runat="server" ID="OCErrorLoansGrid" CssClass="ErrorGrid" AutoGenerateColumns="false" HeaderStyle-CssClass="GridHeader" AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem" >
              <Columns>
                  <asp:BoundField HeaderText="LoanNumber" DataField="Item1" />
                  <asp:BoundField HeaderText="Borrower Name" DataField="Item2" />
              </Columns>
          </asp:GridView>
      </asp:PlaceHolder>
      
      <asp:PlaceHolder runat="server" ID="LeadToLoanConversionErrorsPanel">
         <h2 class="FieldLabel ErrorMessage"> <ml:EncodedLiteral runat="server" ID="LeadToLoanConversionErrorMessage"></ml:EncodedLiteral>
          <asp:GridView runat="server" ID="LeadToLoanConversionErrorsGrid" CssClass="ErrorGrid" AutoGenerateColumns="false" HeaderStyle-CssClass="GridHeader" AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem" >
              <Columns>
                  <asp:BoundField HeaderText="LoanNumber" DataField="Item1" />
                  <asp:BoundField HeaderText="Borrower Name" DataField="Item2" />
              </Columns>
          </asp:GridView>
      </asp:PlaceHolder>

      <asp:PlaceHolder runat="server" ID="LoanToLeadConversionErrorsPanel">
         <h2 class="FieldLabel ErrorMessage"> <ml:EncodedLiteral runat="server" ID="LoanToLeadConversionErrorMessage"></ml:EncodedLiteral>
          <asp:GridView runat="server" ID="LoanToLeadConversionErrorsGrid" CssClass="ErrorGrid" AutoGenerateColumns="false" HeaderStyle-CssClass="GridHeader" AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem" >
              <Columns>
                  <asp:BoundField HeaderText="LoanNumber" DataField="Item1" />
                  <asp:BoundField HeaderText="Borrower Name" DataField="Item2" />
              </Columns>
          </asp:GridView>
      </asp:PlaceHolder>
      <div style="padding: 8px; text-align: center">
      <input type="button" value="  OK  " onclick="onClosePopup();">
      </div>
      </asp:Panel>
 </div>     
  </form>
</body>
</html>
