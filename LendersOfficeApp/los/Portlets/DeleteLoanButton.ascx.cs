using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Portlets
{
	public partial  class DeleteLoanButton : System.Web.UI.UserControl
	{

		private BrokerUserPrincipal BrokerUser
		{
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		/// <summary>
		/// Initialize control.
		/// </summary>

		protected void PageLoad(object sender, System.EventArgs e)
		{
			// 1/18/2005 kb - If user lacks permission to delete
			// loans, we don't even show it.

            //if( BrokerUser.HasPermission( Permission.CanDeleteLoan ) == false )
            //{
            //    DeleteLoanPanel.ToolTip = "Access denied.  You do not have permission to delete loans.";
            //    DeleteLoanPanel.Enabled = false;
            //}
            //else
            //{
            //    DeleteLoanPanel.Enabled = true;
            //    DeleteLoanPanel.ToolTip = "";
            //}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

	}

}
