<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LoanProgramPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.LoanProgramPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script type="text/javascript">
function onLoanProgramClick() {
    var win = window.open(gVirtualRoot + '/los/Template/LoanProductFrame.aspx', 'LoanProgramEdit', 'toolbar=no,menubar=no,resizable=yes');
    win.focus();
    return false;
}
function onPMIProgramClick() {
    var win = window.open(gVirtualRoot + '/los/Template/PMIProductFrame.aspx', 'PMIProgramEdit', 'toolbar=no,menubar=no,resizable=yes');
    win.focus();
    return false;
}
function onAdjustmentsClick() {
    openAdjustmentsPopup();
}
function onFeeTypesClick() {
    showModal('/los/FeeTypes/FeeTypes.aspx', "", null, null, null, {hideCloseButton:true, width:600, height:600});
    return false;
}
function onGfeFeeServiceClick() {
  showModal('/los/FeeService/FeeService.aspx', "", null, null, null, {hideCloseButton:true, width:900, hideCloseButton: true});
    return false;
}
function onRegionsClick() {
    var win = window.open(gVirtualRoot + '/los/Regions/EditRegions.aspx', 'EditRegions', 'scrollbars=no,toolbar=no,menubar=no,resizable=yes,width=800,height=600');
    win.focus();
    return false;
}
function onProductPairingClick() {
    showModal('/los/RatePrice/ProductPairing.aspx');
    return false;
}
function onDisableLoanProgramClick() {
    showModal('/los/Template/DisableLoanPrograms.aspx', "", null, null, null, {hideCloseButton:true, width:600, height:600});
    return false;
}

function onPricingPolicyClick() {
    var win = window.open(gVirtualRoot + '/los/RatePrice/PricePolicyList.aspx', 'PricingRulesEdit', 'toolbar=no,menubar=no,resizable=yes');
    win.focus();
    return false;
}

function onGfeFeeClick() {
    var win = window.open(gVirtualRoot + '/los/ClosingCostFeeSetup/FeeSetup.aspx', 'LenderClosingCostFeeSetup', 'scrollbars=yes,toolbar=no,menubar=no,resizable=yes,width=1120');
    win.focus();
    return false;
}
function f_onPricingGroupClick() {
    var win = openWindowWithArguments(gVirtualRoot + '/los/PricingGroup/PricingGroupMainFrame.aspx', 'PricingGroupEdit', 'toolbar=no,menubar=no,resizable=yes');
    win.focus();
    return false;
}

function f_onLockDeskQuestionsClick() {
    var win = window.open(gVirtualRoot + '/los/RateLockQuestions/RateLockQuestions.aspx', 'RateLockQuestions', 'toolbar=no,menubar=no,resizable=yes');
    win.focus();
    return false;
}

function f_onDisablePricingClick() {
    showModal('/los/Template/ListDisabledProductInvestor.aspx', "", null, null, null, {hideCloseButton:true, width:900, height:550}) ;
    return false;
}
function f_mortgagePoolsClick() {
    window.open(gVirtualRoot + '/los/MortgagePools/PoolDetails.aspx', 'MortgagePools');
    return false;
}

function f_mortgagePoolsClick() {
    window.open(gVirtualRoot + '/los/MortgagePools/PoolDetails.aspx', 'MortgagePools');
    return false;
}
</script>
<TABLE class="Portlet" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
	<TR>
		<TD noWrap>
			<font class="PortletHeader" style="HEIGHT: 20px">Loan Programs</font>
			<% if (ShowLoanPrograms) { %>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return onLoanProgramClick();" class="PortletLink" title="Add/edit loan program">Loan
				Programs</a>

			<% } %>
			<% if (ShowMiPrograms) { %>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return onPMIProgramClick();" class="PortletLink" title="PMI Programs">MI Programs</a>
			<% } %>
            <% if (ShowAdjustments) { %>
            <br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return onAdjustmentsClick();" class="PortletLink" title="Edit Adjustments and Other Credits Setup">Adjustments Setup</a>
            <% } %>
			<% if( ShowFeeTypes ) { %>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return onFeeTypesClick();" class="PortletLink" title="Create/Edit Fee Types">Fee Type Setup</a>
			<% } %>
            <asp:PlaceHolder ID="m_closingCostFeePanelRactive" Visible="false" runat="server">
			    <br />
			    <img src="../images/bullet.gif" align="absMiddle" />&nbsp;&nbsp;<a href="#" onclick="return onGfeFeeClick();" class="PortletLink" title="Create/Edit Fee Types">New Fee Type Setup</a>
			</asp:PlaceHolder>
			<% if( ShowGFEFeeService ) { %>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return onGfeFeeServiceClick();" class="PortletLink" title="Upload/Edit GFE Fee Service File">Fee Service</a>
            <br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return onRegionsClick();" class="PortletLink" title="Edit Regions">Regions</a>
			<% } %>

			<% if( ShowCCTemplates ) { %>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" runat="server" id="closingcostlink" class="PortletLink" title="Add/edit closing cost template">Closing
				Cost Templates</a>
			<% } %>
			<% if (m_pricingVisible) { %>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return onPricingPolicyClick();" class="PortletLink" title="Add/edit pricing policies">Pricing
				Rules</a>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return onDisableLoanProgramClick();" class="PortletLink" title="Disable loan program">Disable Loan Programs</a>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return onProductPairingClick();" class="PortletLink" title="Product Pairing">Product Pairing</a>
			<% } %>
			<% if (m_disablePricingVisible) { %>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return f_onDisablePricingClick();" class="PortletLink" title="Disable Pricing">Disable Pricing</a>
			<% } %>
			<% if (m_pricingGroupVisible) { %>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return f_onPricingGroupClick();" class="PortletLink" title="Add/Edit price group">Price Groups</a>
			<%} %>
			<% if(m_lockDeskQuestionsVisible){ %>
			<br />
			<img src="../images/bullet.gif" align="absMiddle">&nbsp;&nbsp;<a href="#" onclick="return f_onLockDeskQuestionsClick();" class="PortletLink" title="Define a set of questions that must be answered when the user submits a rate lock request">Lock Desk Questions</a>
			<% } %>

		</TD>
	</TR>
	<tr>
		<td><img src="../images/shadow_line.gif" width="130" height="2"></td>
	</tr>
</TABLE>
