﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DisclosurePortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.DisclosurePortlet" %>
<%@ Register TagPrefix="uc" TagName="RefreshButton" Src="~/los/Portlets/RefreshButton.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<div id="DisclosuresPipeline">
    <uc:RefreshButton id="m_refreshPanel" runat="server"></uc:RefreshButton>
    <div class="heading"></div>
    <table id="DisclosuresTable" class="FormTable tablesorter" cellpadding="2" cellspacing="1" style="width: 100%;">
        <thead>
            <tr class="GridHeader">
                <th class="Action">Loan Number</th>
                <th class="Action">Loan Officer</th>
                <th class="Action">Borrower</th>
                <th class="Action">Disclosure Needed</th>
                <th class="Action">Due Date</th>
                <th class="Action">Status</th>
                <th class="Action">Disclosure Desk</th>
                <asp:PlaceHolder runat="server" ID="AutomatedDisclosurePH" Visible="false">
                    <th class="Action">Automated Disclosure</th>
                </asp:PlaceHolder>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater runat="server" ID="Rows" OnItemDataBound="RowsBound">
                <ItemTemplate>
                    <tr>
                        <asp:Repeater runat="server" ID="RowEntries">
                            <ItemTemplate>
                                <td style="vertical-align: top;"><span><%# AspxTools.HtmlString((string)Container.DataItem) %></span></td>
                            </ItemTemplate>
                        </asp:Repeater>
                        <td style="display: none;" runat="server" id="HiddenCell">
                            <input type="hidden" runat="server" id="LoanId" class="LoanId" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
</div>