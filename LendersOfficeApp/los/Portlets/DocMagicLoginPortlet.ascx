﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="DocMagicLoginPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.DocMagicLoginPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script language="javascript">
    function onEDisclosureClick() {
        window.open("https://www.docmagic.com/esign/user/");
    }
    function onAccountAdminClick() {
        window.open("https://www.docmagic.com/webservices/admin/main.htm");
    }
</script>



<TABLE class="Portlet" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD class="PortletHeader" noWrap>DocMagic</TD>
</TR>
<TR>

<TD noWrap>
    <img src="../images/bullet.gif" align="absMiddle">&nbsp;
	<a href="#" onclick="onAccountAdminClick()" class="PortletLink" title="Account login">Account Admin</a>
	<br>
	
	
	<img src="../images/bullet.gif" align="absMiddle">&nbsp;
	<a href="#" onclick="onEDisclosureClick()" class="PortletLink" title="eDisclosure console">eDisclosure Console</a>
	

</TD>
</TR>
<TR>
<TD><img src="../images/shadow_line.gif" width="130" height="2"></td>
</TR>
</TABLE>