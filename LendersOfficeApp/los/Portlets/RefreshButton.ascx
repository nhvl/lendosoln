<%@ Control Language="c#" AutoEventWireup="false" Codebehind="RefreshButton.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.RefreshButton" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import namespace="LendersOffice.AntiXss" %>
<input id="RefreshBtn" type=button value="Refresh" onclick=<%= AspxTools.HtmlAttribute("__doPostBack('changeTab', " + AspxTools.JsString(Context.Items["MainTabSelectedPgKey"].ToString()) + ");") %> NoHighlight>
