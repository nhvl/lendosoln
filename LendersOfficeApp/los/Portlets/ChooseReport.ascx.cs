﻿#region Generated Code 
namespace LendersOfficeApp.los.Portlets
#endregion 
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Security;

    /// <summary>
    /// A control that list all the reports in the lender's account. The user can 
    /// then choose a report.
    /// </summary>
    public partial class ChooseReport : System.Web.UI.UserControl
    {
        /// <summary>
        /// The code we print in front of the selected report.
        /// </summary>
        private const string SelectedPointer = "\u25ba";

        /// <summary>
        /// The simple pipeline text we use in the UI.
        /// </summary>
        private const string DefaultPipelineText = "Simple pipeline (default)";

        /// <summary>
        /// An event to notify the parent of a report being chosen.
        /// </summary>
        public event EventHandler<ChooseReportEventArgs> ReportChosen;

        /// <summary>
        /// An event to notify the parent of an error that was encountered.
        /// </summary>
        public event EventHandler<ErrorEventArgs> ReportError;

        /// <summary>
        /// Gets the users current principal.
        /// </summary>
        /// <value>The users current principal.</value>
        private AbstractUserPrincipal Principal
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal;
            }
        }

        /// <summary>
        /// Fetches and populates the reports the user has access to. 
        /// If this is not called the control will render as empty.
        /// </summary>
        /// <param name="selectedReportId">The report that is currently chosen.</param>
        public void BindData(Guid selectedReportId)
        {
            List<ReportDesc> ownedReports = new List<ReportDesc>();
            List<ReportDesc> otherReports = new List<ReportDesc>();

            ReportDesc report = new ReportDesc();
            report.QueryName = DefaultPipelineText;
            report.QueryId = Guid.Empty;

            if (selectedReportId == Guid.Empty)
            {
                report.Selected = SelectedPointer;
            }

            SqlParameter[] parameters =
                {
                new SqlParameter("@BrokerId", this.Principal.BrokerId)
            };

            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(this.Principal.ConnectionInfo, "ListReportQueryDetails", parameters))
            {
                while (reader.Read())
                {
                    ReportDesc reportDescription = new ReportDesc();

                    reportDescription.QueryName = (string)reader["QueryName"];
                    reportDescription.QueryId = (Guid)reader["QueryId"];
                    Guid employeeId = (Guid)reader["EmployeeId"];

                    if (reportDescription.QueryId == selectedReportId)
                    {
                        reportDescription.Selected = SelectedPointer;
                    }

                    if (employeeId == this.Principal.EmployeeId)
                    {
                        ownedReports.Add(reportDescription);
                    }
                    else
                    {
                        otherReports.Add(reportDescription);
                    }
                }
            }

            var defaultList = new[] { report };

            m_yourQueries.DataSource = defaultList.Concat(ownedReports.OrderBy(p => p.QueryName));
            m_yourQueries.DataBind();
            m_othrQueries.DataSource = otherReports.OrderBy(p => p.QueryName);
            m_othrQueries.DataBind();
        }

        /// <summary>
        /// Handles the on item data bound event on the repeater for the other queries control.
        /// </summary>
        /// <param name="sender">The repeater.</param>
        /// <param name="args">The item associated with the event.</param>
        protected void YourQueries_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            LinkButton chooseLink = args.Item.FindControl("YourChooseLink") as LinkButton;

            ReportDesc report = args.Item.DataItem as ReportDesc;
            chooseLink.CommandArgument = report.ToString();
        }

        /// <summary>
        /// Handles the on item data bound event on the repeater for the other queries control.
        /// </summary>
        /// <param name="sender">The repeater.</param>
        /// <param name="args">The item associated with the event.</param>
        protected void OtherQueries_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            LinkButton othersChooseLink = args.Item.FindControl("OthersChooseLink") as LinkButton;

            ReportDesc report = args.Item.DataItem as ReportDesc;
            othersChooseLink.CommandArgument = report.ToString();
        }

        /// <summary>
        /// Handles the user picking a report from the list. It will ensure the user has access to the form as well.
        /// </summary>
        /// <param name="sender">The repeater containing the item.</param>
        /// <param name="a">The arguments of the repeater.</param>
        protected void ReportClick(object sender, RepeaterCommandEventArgs a)
        {
            if (a.CommandName != "Choose")
            {
                return;
            }

            ReportDesc repDesc = ObsoleteSerializationHelper.JavascriptJsonDeserializer<ReportDesc>(a.CommandArgument.ToString());
            ChooseReportEventArgs args = new ChooseReportEventArgs(repDesc);

            if (repDesc.QueryId == Guid.Empty)
            {
                repDesc.QueryName = "Simple pipeline";
                this.ReportChosen?.Invoke(this, args);
                return;
            }

            Query q = Query.GetQuery(this.Principal.BrokerId, repDesc.QueryId);

            if (q == null)
            {
                this.SendReportError($"Could not find report {repDesc.QueryName}.");
                return;
            }

            if (q.hasSecureFields && !q.CanUserRun((BrokerUserPrincipal)this.Principal))
            {
                this.SendReportError($"The report {repDesc.QueryName} contains fields you do not have access to. ");
                return;
            }

            this.ReportChosen?.Invoke(this, args);
        }

        /// <summary>
        /// Sends the parent the error message the control encountered.
        /// </summary>
        /// <param name="error">The error message.</param>
        private void SendReportError(string error)
        {
            ErrorEventArgs args = new ErrorEventArgs(error);
            this.ReportError?.Invoke(this, args);
        }
    }
}