<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LoanStatisticsPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.LoanStatisticsPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script>
    $j(document).ready(function() {
        $j(".PortletLink").click(function() {
            var el = $j(this);
            window.opener.location = (el.attr("href"));
            
            return false;
        });
    });
</script>

<TABLE class="Portlet" id="Table1" cellSpacing="0" cellPadding="0" width="140" border="0">
	<TR>
		<TD class="PortletHeader" noWrap>All Branch Statistics</TD>
	</TR>
	<tr>
		<td><asp:DropDownList id="m_datesDDL" runat="server" AutoPostBack="True">
				<asp:ListItem Value="1">Last 7 days</asp:ListItem>
				<asp:ListItem Value="2">This month</asp:ListItem>
				<asp:ListItem Value="3" Selected="True">Previous month</asp:ListItem>
				<asp:ListItem Value="4">Previous 2 months</asp:ListItem>
				<asp:ListItem Value="5">Previous 3 months</asp:ListItem>
				<asp:ListItem Value="6">Year to date</asp:ListItem>
				<asp:ListItem Value="8">Previous year</asp:ListItem>
				<asp:ListItem Value="9">Today</asp:ListItem>
			</asp:DropDownList></td>
	</tr>
	<asp:Repeater id="m_repeater" runat="server">
		<itemtemplate>
			<tr>
				<td nowrap>
        <asp:HyperLink ID="m_portletLink" CssClass="PortletLink" runat="server" Font-Bold="true"></asp:HyperLink>
				</td>
			</tr>
		</itemtemplate>
	</asp:Repeater>
	<tr>
		<td><ml:EncodedLabel ID="m_cacheDateTimeLabel" style="COLOR: tomato" Runat="server"></ml:EncodedLabel></td>
	</tr>
	<tr>
		<td><asp:Button ID="m_updateButton" Runat="server" Text="Update Statistics"></asp:Button></td>
	</tr>
</TABLE>
