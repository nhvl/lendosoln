using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;

namespace LendersOfficeApp.los.Portlets
{
	/// <summary>
	///		OPM Case 3156 - Create loan template management interface?
	///		by intern Ethan
	/// </summary>


	public partial  class LoanTemplatePortlet : System.Web.UI.UserControl
	{
		protected bool m_isAllowCreateTemplate = false;



		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal;  }
		}



		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Check permission to hide link.

			m_isAllowCreateTemplate = BrokerUser.HasPermission( Permission.CanCreateLoanTemplate );

			// Check role to adjust visibility.
            // 12/10/2013 gf - opm 145015 Give new roles same portlets access as loan officer.
            if (!BrokerUser.HasAtLeastOneRole(new E_RoleT[] { E_RoleT.LoanOfficer, E_RoleT.Processor, E_RoleT.LockDesk, E_RoleT.Underwriter, 
                E_RoleT.LoanOpener, E_RoleT.Manager, E_RoleT.LenderAccountExecutive, E_RoleT.CreditAuditor, E_RoleT.DisclosureDesk, 
                E_RoleT.JuniorProcessor, E_RoleT.JuniorUnderwriter, E_RoleT.LegalAuditor, E_RoleT.LoanOfficerAssistant, E_RoleT.Purchaser, 
                E_RoleT.QCCompliance, E_RoleT.Secondary, E_RoleT.Servicing}))
			{
				Visible = false;
			}
         
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
