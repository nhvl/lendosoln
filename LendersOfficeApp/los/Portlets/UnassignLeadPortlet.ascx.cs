namespace LendersOfficeApp.los.Portlets
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.LoanSearch;
    using LendersOffice.Security;

    public partial class UnassignLeadPortlet : UserControl, IAutoLoadUserControl
	{

		private Hashtable m_accessBinding = new Hashtable();

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
        /// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
        }
		#endregion

        private void BindDataGrid()
        {
            DataSet ds = LendingQBSearch.ListUnassignedLeads(CurrentUser);


            //7/22/08 - jk. Load sort order from cookie and after databind, store current sort to the cookie
            HttpCookie cookie = Request.Cookies["newLead_sortOrder"];
            if (cookie != null && cookie.Value != null && cookie.Value != "")
            {
                m_unassignLeadsDG.DefaultSortExpression = cookie.Value;
            }
            else
            {
                m_unassignLeadsDG.DefaultSortExpression = "aBLastNm ASC";
            }

            m_unassignLeadsDG.DataSource = ds.Tables[0].DefaultView;
            m_unassignLeadsDG.DataBind();

            LendersOffice.Common.RequestHelper.StoreToCookie("newLead_sortOrder", ds.Tables[0].DefaultView.Sort, false);
        }

		protected void LoanDataBound( object sender , DataGridItemEventArgs a )
		{
			// 11/4/2004 kb - Find the conditional controls and hide or
			// show depending on what the access privelages are.
			//
			// If you want to modify what is shown and when, then put the
			// logic for each loan row here...

			if( a.Item.DataItem != null )
			{
				Panel aPanel; 
                
                //AccessBinding aB = m_accessBinding[ DataBinder.Eval( a.Item.DataItem , "sLId" ) ] as AccessBinding;
                bool canRead = (int)DataBinder.Eval(a.Item.DataItem, "CanRead") == 1;
                bool canWrite = (int)DataBinder.Eval(a.Item.DataItem, "CanWrite") == 1;
				
				aPanel = a.Item.FindControl( "SelectBox" ) as Panel;

				if( aPanel != null )
				{
					if( canWrite == false )
					{
						aPanel.Visible = false;
					}
					else
					{
						aPanel.Visible = true;
					}
				}

				aPanel = a.Item.FindControl( "LoadLink" ) as Panel;

				if( aPanel != null )
				{
					if( CurrentUser.HasPermission( Permission.CanViewLoanInEditor ) == false || canWrite == true || canRead == false )
					{
						aPanel.Visible = false;
					}
					else
					{
						aPanel.Visible = true;
					}
				}

				aPanel = a.Item.FindControl( "ViewLink" ) as Panel;

				if( aPanel != null )
				{
					if( CurrentUser.HasPermission( Permission.CanViewLoanInEditor ) == true && canWrite == false || canRead == false )
					{
						aPanel.Visible = false;
					}
					else
					{
						aPanel.Visible = true;
					}
				}

				aPanel = a.Item.FindControl( "EditLink" ) as Panel;

				if( aPanel != null )
				{
					if( canWrite == false )
					{
						aPanel.Visible = false;
					}
					else
					{
						aPanel.Visible = true;
					}
				}
			}
		}

		public void LoadData() 
		{
			if (! CurrentUser.HasRole(E_RoleT.Manager)) 
			{
				this.Visible = false;
				return;
			}

			BindDataGrid();

			if (m_unassignLeadsDG.Items.Count == 0) 
			{
				m_countLabel.Text = "No leads without call center agent.";
			}
			else if (m_unassignLeadsDG.Items.Count < 2) 
			{
				m_countLabel.Text = "Showing: 1 lead without call center agent.";
			} 
			else if (m_unassignLeadsDG.Items.Count < ConstAppDavid.MaxViewableRecordsInPipeline)
			{
				m_countLabel.Text = "Showing: " + m_unassignLeadsDG.Items.Count + " leads without call center agent.";
			} 
            else 
            {
                m_countLabel.Text = "Too many leads without call center agent -- displaying first " + ConstAppDavid.MaxViewableRecordsInPipeline + ".";
				m_countLabel.ToolTip = "Please use our Search page to find more leads";
            }
		}

		public void SaveData() 
        {
        }
	}
}
