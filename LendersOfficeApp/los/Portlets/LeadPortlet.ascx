<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LeadPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.LeadPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<table class="Portlet" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
  <tr>
    <td class="PortletHeader" noWrap>Leads</td>
  </tr>
  <tr>
    <td noWrap>
    <% if (IsNewPmlUIEnabled) { %>
	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<a purpose="purchase" class="lead-create-link PortletLink" title="Create new loan file">Create Purchase Lead</a>
	<br>
	
	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<a purpose="refinance" class="lead-create-link PortletLink" title="Create new loan file">Create Refinance Lead</a>
    <br />
	<% } else { %>
	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
    <a purpose="" class="lead-create-link PortletLink" title="Create new lead">Create Lead</a>
    <br />
	<% } %>
    <span id="CreateHelocLeadPanel" runat="server">
        <img src="../images/bullet.gif" align="absMiddle" />&nbsp;
        <a purpose="HELOC" class="lead-create-link PortletLink" title="Create new HELOC lead file">Create HELOC Lead</a>
        <br />
    </span>
    <img src="../images/bullet.gif" align="absMiddle" />&nbsp;
    <a purpose="construction" class="lead-create-link PortletLink" title="Create new construction lead file">Create Construction Lead</a>
    <br />
	</td>
  </tr>
  <tr>
	  <td><img src="../images/shadow_line.gif" width="130" height="2"></td>
  </tr>    
</table>
<uc1:cModalDlg id=CModalDlg1 runat="server"></uc1:cModalDlg>

<script language=javascript>

    $j(".lead-create-link").click(onNewLeadClick);

    function onNewLeadClick() {
        var purpose = $j(this).attr("purpose");

        var args = {
            isLead: true,
            purpose: purpose
        };

        if (<%= AspxTools.JsBool(!IsEditLeadsInFullLoanEditor)%>) {
            
            openTemplateDialog("", true);
            createLoanWithArgs(args);
        }
        else {
            retrieveTemplates(args, "Lead");
        }

        return false;
    }

</script>