<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DuplicateLoanButton.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.DuplicateLoanButton" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import namespace="LendersOffice.AntiXss" %>
<script language="javascript">
<!--
function duplicateLoan() {
  var list = getCheckList();
  if (list.length == 0) {
    alert(<%= AspxTools.JsString(JsMessages.NoLoanSelected) %>);
    return;
  }

  if (list.length > 1) {
    alert(<%= AspxTools.JsString(JsMessages.CannotDuplicateMoreThanOneLoan) %>);
    return;
  }
  
  var args = new Object();
  args["LoanID"] = list[0];
  var result = gService.loanutils.call("DuplicateLoanFile", args);
  if (!result.error) {
    if (result.value.Error == "False") {
      editLoan(result.value.NewLoanID, result.value.LoanName);
    } else {
      alert(<%= AspxTools.JsString(JsMessages.UnableDuplicateLoanFile) %>);
    }
  } 
   
}
//-->
</script>

<input id="DuplicateLoanBtn" type="button" value="Duplicate File" onclick="duplicateLoan();" NoHighlight>
