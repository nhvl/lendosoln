using System;
using System.Text.RegularExpressions;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Portlets
{
	/// <summary>
	///	Summary description for QuickSearchPortlet.
	/// </summary>
	public partial  class QuickSearchPortlet : System.Web.UI.UserControl
	{

		private BrokerUserPrincipal BrokerUser
		{
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}
        private string SearchCookieKey
        {
            get { return String.Format("QuickSearchFilter-{0}", (BrokerUser == null) ? "" : BrokerUser.UserId.ToString()); }
        }
        private const string DEFAULT_SEARCH_FILTER = "0"; // Default to Loan Number

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Enables/disables m_searchBtn based on whether or not the m_kewwordTF is empty
			if(m_keywordTF.Text.Equals(""))
			{
				m_searchBtn.Enabled = false;
			}
			else
			{
				m_searchBtn.Enabled = true;
			}

			if (IsPostBack)
			{
				// 06/20/06 mf - As per OPM 4806, we need to redirect
				// from this user control as early as we can, so we
				// don't hit the DB to populate the controls that will
				// not reach their render phase.  We use the Load phase
				// instead of waiting for postback event handling.
				// We use a bit of a hack to determine if the button
				// caused this postback.

				foreach (string str in Request.Form.AllKeys)
				{
					if ( Request.Form[str] == "Go")
					{
						onSearchClick(this, EventArgs.Empty);
						break;
					}
				}
			}
			else
			{
                SetSearchTypeFromCookie();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 08/12/2008 ck - OPM 24053 - This was taken out because it would cause the form to be 
			// submitted when the date from the statistics was changed or when the "Update Statistics"
			// button was clicked

			//this.m_keywordTF.TextChanged += new System.EventHandler(this.onKeyWordChange);
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

        private void onSearchClick(object sender, System.EventArgs e)
        {
            string searchType = m_searchTypeDDL.SelectedItem.Value;
            string searchValue = m_keywordTF.Text;
            string key = "";

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            if (searchType == "0") // Search by Loan Number
            {
                key = "loannumber";
            } 
            else if (searchType == "1") // Search by last name.
            {
                key = "firstname";
            } 
            else if (searchType == "2") // Search by last name.
            {
                key = "lastname";
            } 
            else if (searchType == "3") // Search by ssn 
            {
				// 3/23/2007 nw - OPM 6662 - Strip any asterisks from the search phrase when user does SSN search
				searchValue = searchValue.Replace("*", "");

                // If SSN # doesn't have a dash then insert it.

                Regex ssnPattern = new Regex("(\\d{3})-{0,1}(\\d{2})-{0,1}(\\d{4})");
                searchValue = ssnPattern.Replace(searchValue, "${1}-${2}-${3}");

                key = "ssn";
            } 
            else if (searchType == "4") // Search by property address.
            {
                key = "propertyaddress";
            }
            else if (searchType == "5") // Search by loan reference number.
            {
                key = "loanreferencenumber";
            }

            if ( key == "" || searchValue.TrimWhitespaceAndBOM() == "" )
            {
                return;
            }

            RequestHelper.StoreToCookie(SearchCookieKey, searchType, SmallDateTime.MaxValue);
            Response.Redirect( "loanfind.aspx?" + key + "=" + searchValue + "&quicksearch=yes" );
        }

        private void SetSearchTypeFromCookie()
        {
            if (Request.Cookies[SearchCookieKey] != null)
            {
                m_searchTypeDDL.SelectedValue = Request.Cookies[SearchCookieKey].Value;
            }
            else
            {
                m_searchTypeDDL.SelectedValue = DEFAULT_SEARCH_FILTER;
            }
        }
	}

}
