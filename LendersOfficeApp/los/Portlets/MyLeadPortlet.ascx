<%@ Register TagPrefix="uc1" TagName="AssignLoanButton" Src="AssignLoanButton.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="MyLeadPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.MyLeadPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="uc1" TagName="RefreshButton" Src="RefreshButton.ascx" %>
<%@ Register TagPrefix="uc1" TagName="DeleteLoanButton" Src="DeleteLoanButton.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<table width="100%" border="0" cellpadding="3" cellspacing="0">
	<tr>
		<TD align="left" width="100%"><uc1:AssignLoanButton id="AssignLoanButton1" runat="server"></uc1:AssignLoanButton><uc1:DeleteLoanButton id="DeleteLoanButton1" runat="server"></uc1:DeleteLoanButton><uc1:RefreshButton id="RefreshButton1" runat="server"></uc1:RefreshButton></TD>
		<td width="100%" align="right" noWrap><ml:EncodedLabel id="m_countLabel" runat="server" EnableViewState="False" ForeColor="Black" Font-Bold="True"></ml:EncodedLabel>
		</td>
	</tr>
</table>
<script>
    var gSelectionCount = 0;
    function _init() {
        disableButtons(true);
    }
    function disableButtons(b) {
      document.getElementById("AssignLoanBtn").disabled = b;
      document.getElementById("DeleteLoanBtn").disabled = b;
    }

    function onCheckBoxClick(cb) {
      if (cb.checked)
        gSelectionCount++;
      else
        gSelectionCount--;
      highlightRowByCheckbox(cb);
      disableButtons(gSelectionCount == 0);
    }
    function onClickLeadLinkSet( oBase , oTarget ) { try
    {
        // Check for valid targets.

        if( oBase == null || oTarget == null )
        {
            return;
        }

        // Hide all others and show selected link set.

        if( oTarget.nextSibling != null )
        {
            // Find containing row and prepare to make all the
            // text bold in the following cells.

            var oRow = oTarget;

            while( oRow != null )
            {
                if( oRow.tagName.toUpperCase() == "TR" )
                {
                    break;
                }

                oRow = oRow.parentElement;
            }

            if( oTarget.nextSibling.style.display == "none" )
            {
                // Turn off all previously activated link sets within
                // this grid and then turn on the selected one.

                oTarget.nextSibling.style.display    = "block";
                oTarget.nextSibling.style.fontWeight = "normal";

                if( oRow != null )
                {
                    oRow.style.fontWeight = "bold";
                }
            }
            else
            {
                // Turn off the selected one and leave the
                // other link sets alone.

                oTarget.nextSibling.style.display = "none";

                if( oRow != null )
                {
                    oRow.style.fontWeight = "normal";
                }
            }
        }
    }
    catch( e )
    {
        window.status = "Selecting " + oTarget.innerText + " failed.";
    }}
</script>
<ml:CommonDataGrid id="m_leadsDG" runat="server" DataKeyField="sLId">
	<columns>
		<asp:TemplateColumn>
			<itemtemplate>
				<input type="checkbox" id="loanid" value='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLId")) %>' onclick="onCheckBoxClick(this);"/>
			</itemtemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn SortExpression="sLNm" HeaderText="Loan Number">
			<itemtemplate>
				<div style="CURSOR: hand;">
					<div id="m_leadNameTag" style="DISPLAY: block; COLOR: blue;" onmouseover="style.color = 'orange';" onmouseout="style.color = 'blue';" onclick="onClickLeadLinkSet( <%# AspxTools.JsString(m_leadsDG.ClientID) %> , this );">
						<u>
							<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "sLNm")) %>
						</u>
					</div>
					<div id="m_leadLinkSet" style="DISPLAY: none; PADDING: 0px; MARGIN-TOP: 2px; MARGIN-BOTTOM: 8px;">
						<div nowrap style="PADDING-LEFT: 2px;">
							&#x00a0;
                            &#x2022;
                            <a href="javascript:editLead(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "sLId").ToString()) %>);" title="Edit Lead">edit</a>
						</div>
						<div nowrap style="PADDING-LEFT: 2px;">
							&#x00a0;
                            &#x2022;
                            <a href="javascript:viewLoan(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "sLId").ToString()) %>);" title="View lead summary">view</a>
						</div>
						<div nowrap style="PADDING-LEFT: 2px;">
							&#x00a0;
                            &#x2022;
                            <a href="javascript:createTask(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "sLId").ToString()) %>);" title="Insert new task for this lead">new task</a>
						</div>
					</div>
				</div>
			</itemtemplate>
		</asp:TemplateColumn>
		<asp:templatecolumn headertext="Last Name" sortexpression="aBLastNm">
			<itemtemplate>
				<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "aBLastNm")) %>
			</itemtemplate>
		</asp:templatecolumn>
		<asp:templatecolumn headertext="First Name" sortexpression="aBFirstNm">
			<itemtemplate>
				<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "aBFirstNm")) %>
			</itemtemplate>
		</asp:templatecolumn>
		<asp:BoundColumn DataField="aBSsn" HeaderText="SSN"></asp:BoundColumn>
		<asp:BoundColumn DataField="aBHPhone" HeaderText="Home Phone"></asp:BoundColumn>
		<asp:BoundColumn DataField="LoanType" HeaderText="Type" SortExpression="LoanType"></asp:BoundColumn>
		<asp:BoundColumn DataField="sStatusD" HeaderText="Status Date" DataFormatString="{0:d}" SortExpression="sStatusD"></asp:BoundColumn>
	</columns>
</ml:CommonDataGrid>
