<%@ Control Language="c#" AutoEventWireup="false" Codebehind="AdminPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.AdminPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import namespace="DataAccess" %>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript">
    var windowHandle = null;
    
	function onBrokerInfoClick()
	{
	    showModal('/los/admin/BrokerInfo.aspx', null, null, null, function(){
	    	if (typeof(__doPostBack) == 'function')
	        	__doPostBack();	
	    },{
	    	width:1200,
	    	height:600,
	    	hideCloseButton:true
	    });
	}
	function onBranchListClick()
	{
		showModal('/los/admin/EditBranch.aspx', null, null, null, null, {width: 1200, hideCloseButton:true });
    }
    function onTeamsClick() {
        showModal('/los/admin/EditTeams.aspx', "", "dialogWidth: 850px; dialogHeight: 600px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;", null, null, {hideCloseButton:true});
    }
    function onCustomFolderNavigationClick() {
        showModal('/los/admin/CustomFolderNavigation.aspx', "", "dialogWidth: 850px; dialogHeight: 600px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;", true, null, {hideCloseButton:true});
    }
	function onEmployeeAdminClick()
	{
		showModal('/los/admin/EmployeeList.aspx', null, null, null, null, {
			width:680,
			hideCloseButton:true
		});
	}
	function onEventConfigurationClick()
	{
	    LQBPopup.Show('admin/EventConfiguration.aspx', {
	        height: '630',
	        width: '810',
			hideCloseButton: true
	    }, window);
	}
	function onBillingInfoClick()
	{
		
		showModal('/los/admin/LicenseList.aspx');
	}

	function onCustomFieldChoicesClick()
	{
		showModal('/los/admin/FieldChoices.aspx', null, null, null, null, {
			width:500,
			height:520,
			hideCloseButton:true
		});
	}
	function onConditionChoicesClick()
	{
		showModal('/los/admin/ConditionChoices.aspx');
    }
    function onPTLClick() 
    {
        LQBPopup.Show('admin/PerTransactionList.aspx', {
            height: '480',
            width: '1000',
            hideCloseButton: true
        }, window);
    }
	
	function onConditionChoices2Click() {
	    if (windowHandle != null && !windowHandle.closed) {
	        windowHandle.focus();
	        return false;
	    }

	    windowHandle = window.open(gVirtualRoot + '/los/admin/ConditionChoices2.aspx', 'FormList', 'resizable=yes,menubar=no,status=no,scrollbars=yes,height=500,width=900');
	    windowHandle.focus();
	}
	function onLeadSourcesClick()
	{
		showModal('/los/admin/LeadSources.aspx', null, null, null, null, {
			width:500,
			height:450,
			hideCloseButton:true
		});
	}
	function onRateLockPeriodsClick()
	{
		showModal('/los/BrokerAdmin/RateLockPeriods.aspx');
	}
	//OPM 2251
	function onTimezonePreferencesClick()
	{
		//showModal('/los/BrokerAdmin/TimezonePreferences.aspx');
	}
	function onARMIndexEntriesClick()
	{
		showModal('/los/admin/ARMIndexEntries.aspx', null, null, null, null, {
			height:560,
			hideCloseButton:true
		});
    }
	function onElectronicDocsClick() 
	{
	    showModal('/los/ElectronicDocs/ElectronicDocs.aspx', null, 'dialogWidth:650px;dialogHeight:650px;center:yes;resizable:yes;scroll:yes;status=yes;help=no;', true, null, {
	        width: '80%',
	        height: 560,
	        hideCloseButton:true
	    });
	    return false;
	}
	function onCustomFormClick()
	{
	    showModal('/los/CustomForms/CustomFormList.aspx', null, null, null, null, { hideCloseButton: true });
		return false;
	}
	function onCustomPdfFormsClick()
	{
	    if (windowHandle != null && !windowHandle.closed) {
	        windowHandle.focus();
	        return false;
	    }

	    windowHandle = window.open(gVirtualRoot + '/los/CustomPDFForms/FormList.aspx', 'FormList', 'resizable=yes,menubar=no,status=no,scrollbars=yes,height=500,width=550');
	    windowHandle.focus();
		return false;
	}
	function onPrintGroupClick()
	{
		showModal('/los/PrintGroup.aspx', null, null, null, null, {
			width:1000,
			height:700,
			hideCloseButton:true
		});
		return false;
	}
	function onRolodexClick()
	{
		showModal('/los/rolodex/rolodexlist.aspx', null, null, null, null, {
			width:810,
			hideCloseButton:true
		});
		return false;
    }
    function onConsumerPortalsClick() {
        showModal('/los/BrokerAdmin/ConsumerPortalList.aspx');
        return false;
    }
    function onStatisticsClick() {
        window.open(gVirtualRoot + '/los/PortletPopup.aspx?Portlet=BranchStat', 'BranchStatistics', 'resizable=yes,menubar=no,status=no,scrollbars=yes,height=500,width=300');
        return false;
    }
    function onCaptureAuditClick() {
        window.open(gVirtualRoot + '/newlos/ElectronicDocs/DocumentCaptureAudit.aspx?c=t', 'CaptureAudit', 'resizable=yes,menubar=no,status=no,scrollbars=yes,height=500,width=750');
    }
    function onCaptureReviewClick() {
        window.open(gVirtualRoot + '/newlos/ElectronicDocs/DocumentCaptureReview.aspx?c=t', 'CaptureReview', 'resizable=yes,menubar=no,status=no,scrollbars=yes,height=500,width=750');
    }
</script>
<TABLE cellSpacing="0" cellPadding="0" border="0" width="100%" class="Portlet">
	<TR>
		<TD noWrap class="PortletHeader">Corporate Admin</TD>
	</TR>
	<tr>
		<td nowrap>
			<% if( ShowGeneralSettings ) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onBrokerInfoClick(); return false ;" class="PortletLink" title="Edit broker information">
				General Settings</A>
			<br>
			<%} %>
			<% if (ShowBranchSettings){ %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onBranchListClick(); return false;" class="PortletLink" title="Add/edit branch information">
				Branches</A>
			<br>
			<%} %>
			<% if (ShowBranchStatistics){ %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onStatisticsClick(); return false;" class="PortletLink" title="View Branch Statistics">
				All Branch Statistics</A>
			<br>
			<%} %>
			<% if (ShowTeamSettings){ %>
			<span id="TeamsContainer" runat="server">
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onTeamsClick(); " class="PortletLink">
				Teams</A>
			<br>
			</span>
			<%} %>
			<% if (ShowAdminSettings){ %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onEmployeeAdminClick(); return false;" class="PortletLink" title="Add/edit/remove employee information">
				Employees</A>
			<br>
			<%} %>
			<% if( ShowConsumerPortals ) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onConsumerPortalsClick();" class="PortletLink">
				Consumer Portals</A>
			<br>
			<% } %>		
			<% if (ShowAdminSettings){ %>
			<asp:PlaceHolder runat="server" ID="Billing1">
            <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <a 
                class="PortletLink" href="#" title="Examine current billing details and activation settings" onclick="onBillingInfoClick(); return false;" >
                Billing</a>
			<br>
			</asp:PlaceHolder>
						<asp:PlaceHolder runat="server" ID="Billing3">
			    <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <a  id="A2" runat="server"
                class="PortletLink" href="#" title="Examine billable loans" onclick="onPTLClick(); return false;" >
                Billing</a>
                
			<br>
			</asp:PlaceHolder>
			<%} %>
			<%if (ShowEventNotification) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onEventConfigurationClick(); return false;" class="PortletLink" title="Update loan event preferences">
				Event Notification</A>
			<br>
			<%} %>

			<% if( ShowCustomFieldChoices ) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onCustomFieldChoicesClick();" class="PortletLink">
				Custom Field Choices</A>
			<br>
			<% } %>
			<% if( ShowConditionChoices ) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onConditionChoicesClick();" class="PortletLink">
				Condition Choices</A>
			<br>
			<% } %>
			<% if( ShowNewConditionChoices) { %> 
				<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onConditionChoices2Click();" class="PortletLink">
				Condition Choices</a><br/>
			<% } %>
			<% if( ShowLeadSources ) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onLeadSourcesClick();" class="PortletLink">
				Lead Sources</A>
			<br>
			<% } %>
			<% if( ShowRateLockPeriods ) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onRateLockPeriodsClick();" class="PortletLink">
				Rate Lock Periods</A>
			<br>
			<% } %>
			<% if( ShowTimezonePreferences ) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onTimezonePreferencesClick();" class="PortletLink">
				Timezone Preferences</A>
			<br>
			<% } %>
			<% if( ShowARMIndexEntries ) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onARMIndexEntriesClick();" class="PortletLink">
				ARM Index Entries</A>
			<br>
			<% } %>
			<% if( ShowElectronicDocs ) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="return onElectronicDocsClick();" class="PortletLink">
				EDocs</A>
			<br>
			<% } %>
            <% if(this.ShowCaptureAudit) { %>
	        <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A onclick="onCaptureAuditClick();" class="PortletLink">
				Capture Audit</A>
            <br />
            <% } %>
            <% if(this.ShowCaptureReview) { %>
	        <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A onclick="onCaptureReviewClick();" class="PortletLink">
				Capture Review</A>
            <br />
            <% } %>
			<% if( ShowCustomForms ) { %>
            <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="return onCustomFormClick(); " class="PortletLink">
				Custom Word Forms</A>
			<br>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="return onCustomPdfFormsClick(); " class="PortletLink">
				Custom PDF Forms</A>
			<br>
			<% } %>
            <asp:PlaceHolder ID="CustomNavigationContainer" runat="server">
                <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; 
                <A href="#" onclick="return onCustomFolderNavigationClick(); " class="PortletLink"> Custom Folder Navigation</A>
                <br />
            </asp:PlaceHolder>

			<% if ( ShowPrintGroups ){ %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onPrintGroupClick();" class="PortletLink">
				Print Groups</A>
			<br>
			<%} %>
			<% if( ShowContacts ) { %>
			<img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <A href="#" onclick="onRolodexClick();" class="PortletLink">
				Contacts</A>
            <br />
			<% } %>
		</td>
	</tr>
	<tr>
		<td><img src="../images/shadow_line.gif" width="130" height="2"></td>
	</tr>
</TABLE>
