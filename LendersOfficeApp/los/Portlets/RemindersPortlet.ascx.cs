using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Reminders;
using LendersOffice.Security;


namespace LendersOfficeApp.los.Portlets
{

	public partial  class RemindersPortlet : CDiscUIBase, IAutoLoadUserControl
	{
		protected System.Collections.Hashtable m_loanAccessPermissions = new Hashtable();


		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        private void BindDataGrid()
        {
            // HACK: Don't know why the OnDelete event never get fired for this datagrid.
            //       Have to handle it manually here. dd 5/27/2003
            //       I saw grid handlers not get called when I bound the grid after
            //       page load.  I think it needs to be bound then so the event can
            //       locate the firing cell. kb 5/14/2004

            if (Page.IsPostBack) 
            {
                string target = Request.Form["__EVENTTARGET"];
                string arg = Request.Form["__EVENTARGUMENT"];
                Tools.LogInfo("EVENTTARGET=" + target + ", clienID = " + m_remindersDG.ClientID);
                if (target == m_remindersDG.ClientID) 
                {
                    try 
                    {
                        Guid notifId = new Guid(arg);
                        CDiscNotif notif = new CDiscNotif( BrokerUser.BrokerId, notifId, Guid.Empty );
                        notif.DeleteNotif();
                        Tools.LogInfo("Deleting task notification was successful.");
                    } 
                    catch {}
                }
            }

            // Load the user's notifications and grid them out to show all
            // his/her assigned tasks.

			String taskBy ="";
			DataSet    dS = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter( "@UserId" , BrokerUser.UserId )
                                        };

			DataSetHelper.Fill(dS, BrokerUser.BrokerId, "ListDiscNotifByUserID", parameters);	
			

			if( RequestHelper.GetSafeQueryString( "taskby" ) != null )
			{
				taskBy = RequestHelper.GetSafeQueryString( "taskby" ).TrimWhitespaceAndBOM().ToLower();
			


				switch( taskBy )
				{
					case "pastdue": m_remindersDG.DefaultSortExpression = "HasDiscDueDate DESC, DiscDueDate ASC";
						break;

					case "warning": m_remindersDG.DefaultSortExpression = "HasNotifAlertDate DESC, NotifAlertDate ASC";
						break;

					case "updated": m_remindersDG.DefaultSortExpression = "NotifIsRead ASC, DiscLastModifyD DESC";
						break;

					default:
					{
						m_remindersDG.DefaultSortExpression = "NotifIsRead ASC";
					}
						break;
				}
			}
			
			//7/22/08 - jk. Retrieve the sort order from the cookie, and save it into the sort
			HttpCookie cookie = Request.Cookies["reminders_sortOrder"];
			if(cookie != null && cookie.Value!= null && cookie.Value != "")
			{
				m_remindersDG.DefaultSortExpression = cookie.Value;
			}	
            

            m_remindersDG.DataSource = dS.Tables[ 0 ].DefaultView;
            m_remindersDG.DataBind();
			
			LendersOffice.Common.RequestHelper.StoreToCookie("reminders_sortOrder", dS.Tables[0].DefaultView.Sort, false);

			if ( m_remindersDG.Items.Count == 500 )
				m_tooManyLabel.Text = "Too many tasks -- only 500 are displayed.";
        }


        public void LoadData() 
        {
            BindDataGrid();
        }

		public void SaveData() 
        {
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_remindersDG.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.m_remindersDG_ItemDataBound);
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		private void m_remindersDG_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
		
			// Generate the proper links for this row's loan
			GenerateLoanLinks(sender, e, m_loanAccessPermissions);
			
			// Disable row highlighting for now.  Makes it hard to read loans.		

//			switch( e.Item.ItemType )
//			{
//				case ListItemType.Item:
//					e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Yellow'");
//					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='white'");
//					break;
//				case ListItemType.AlternatingItem: 
//					e.Item.Attributes.Add("onmouseover", "this.style.backgroundColor='Yellow'");
//					e.Item.Attributes.Add("onmouseout", "this.style.backgroundColor='#CCCCCC'");
//					break;
//				default:
//					break;
//			}
		}
	}
}
