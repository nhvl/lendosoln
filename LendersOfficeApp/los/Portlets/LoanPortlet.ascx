<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LoanPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.LoanPortlet" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>

<TABLE class="Portlet" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD class="PortletHeader" noWrap>Loans</TD>
</TR>
<TR>
<TD noWrap>
	<% if (IsNewPmlUIEnabled) { %>
	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<a class="loan-create-link PortletLink" purpose="purchase" title="Create new loan file">Create Purchase Loan</a>
	<br>
	
	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<a class="loan-create-link PortletLink" purpose="refinance" title="Create new loan file">Create Refinance Loan</a>
	<br>
	<% } else { %>
	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<a class="loan-create-link PortletLink" purpose="" title="Create new loan file">Create Loan</a>
	<br>
	<% } %>
	<span id="CreateHelocLoanPanel" runat="server">
	    <img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	    <a class="loan-create-link PortletLink" purpose="HELOC" class="PortletLink" title="Create new HELOC file">Create HELOC</a>
	    <br>
	</span>

	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<a class="loan-create-link PortletLink" purpose="construction" class="PortletLink" title="Create new loan file">Create Construction Loan</a>
	<br>

	<img src="../images/bullet.gif" align="absMiddle" />&nbsp;
	<A href="loanfind.aspx" class="PortletLink" title="Advance search for your loan and/or loan template">Find Loan</A><br>
	
	<img src="../images/bullet.gif" align="absmiddle" />&nbsp;
	<a onclick="onBatchImport();" class="PortletLink" title="Import Loans from Calyx Point, Fannie Mae, and MISMO 2.3">Import Loans</a>

</TD>
</TR>
<TR>
<TD><img src="../images/shadow_line.gif" width="130" height="2"></td>
</TR>
</TABLE>

<div class="template-list">

</div>

<script language="javascript">
    $(".loan-create-link").click(onNewLoanClick);

	function onNewLoanClick() 
	{
		var purpose = $(this).attr("purpose");

    var args = {
        isLead: false,
        isTest: false,
        purpose: purpose
    };

    retrieveTemplates(args, "Loan");
    return false;
}

function onBatchImport() 
{
    window.open(gVirtualRoot + '/los/Imports/BatchImport.aspx', '', 'height=780,width=850,scrollbars=yes,resizable=yes', true);
}

</script>

