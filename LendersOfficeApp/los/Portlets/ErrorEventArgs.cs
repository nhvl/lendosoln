﻿#region Generated Code 
namespace LendersOfficeApp.los.Portlets
#endregion 
{
    /// <summary>
    /// Event arguments containing an error message.
    /// </summary>
    public class ErrorEventArgs : System.EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorEventArgs"/> class.
        /// </summary>
        /// <param name="error">The error encountered.</param>
        public ErrorEventArgs(string error)
        {
            this.ErrorMessage = error;
        }

        /// <summary>
        /// Gets the user friendly error message encountered.
        /// </summary>
        /// <value>The user error message.</value>
        public string ErrorMessage { get; private set; }
    }
}