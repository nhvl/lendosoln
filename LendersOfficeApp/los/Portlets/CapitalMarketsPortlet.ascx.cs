using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.Portlets
{

    public partial class CapitalMarketsPortlet : System.Web.UI.UserControl
    {
        private BrokerDB broker;

        private BrokerUserPrincipal CurrentUser
        {
            // Access credentials.
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private BrokerDB Broker
        {
            get
            {
                if (broker == null)
                {
                    broker = BrokerDB.RetrieveById(CurrentUser.BrokerId);
                }
                return broker;
            }
        }

        protected bool HasCapitalMarketsAccess
        {
            get { return CurrentUser.HasPermission(Permission.AllowCapitalMarketsAccess); }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {

        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion

    }

}
