﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WorkflowPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.WorkflowPortlet" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess" %>

<script type="text/javascript">
    function onBranchGroupsClick() {
        window.open(VRoot + '/los/Workflow/BranchGroups.aspx', '', 'resizable=yes,scrollbars=yes');
    }

    function onEmployeeGroupsClick() {
        window.open(VRoot + '/los/Workflow/EmployeeGroups.aspx', '', 'resizable=yes,scrollbars=yes');
    }

    function onOcGroupsClick() {
        window.open(VRoot + '/los/Workflow/PmlBrokerGroups.aspx', '', 'resizable=yes,scrollbars=yes');
    }

    function onWorkflowRulesClick() {
        openMaxSizeWindow(VRoot + '/los/Workflow/EditWorkflowConfiguration.aspx', '', 'resizable=yes,scrollbars=yes');
    }

    function onWorkflowChangeHistoryClick() {
        window.open(VRoot + '/los/Workflow/WorkflowChangeHistoryHolder.aspx', '', 'resizable=yes,scrollbars=yes');
    }
</script>

<table class="Portlet">
    <tr>
        <td class="PortletHeader">Workflow Admin</td>
    </tr>
    <tr>
        <td>
            <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <a onclick="onBranchGroupsClick();" class="PortletLink">Branch Groups</a>
            <br />
            <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <a onclick="onEmployeeGroupsClick();" class="PortletLink">Employee Groups</a>
            <br />
            <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <a onclick="onOcGroupsClick();" class="PortletLink">OC Groups</a>
            <br />
            <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <a onclick="onWorkflowRulesClick();" class="PortletLink">Workflow Rules</a>
            <br />
            <img src=<%= AspxTools.SafeUrl(Tools.VRoot + "/images/bullet.gif") %> align="absMiddle">&nbsp; <a onclick="onWorkflowChangeHistoryClick();" class="PortletLink">Workflow Change History</a>
        </td>
    </tr>
	<tr>
		<td><img src="../images/shadow_line.gif" width="130" height="2"></td>
	</tr>
</table>