<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DeleteLoanButton.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.DeleteLoanButton" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript">
<!--
function onDeleteClick() {
	//Keeps the user from deleting when the pipeline needs a refresh
	if ( typeof(refreshIsNeeded) == "function"  && refreshIsNeeded() )  {
		alert( <%= AspxTools.JsString(JsMessages.PipelineRefreshNeededForDelete) %> ); 
		return false;
	}
		
    var list = getCheckList();
    if (list.length == 0) {
      alert(<%= AspxTools.JsString(JsMessages.NoLoanSelected) %>);
      return;
    }
    
    
    if (confirm(<%= AspxTools.JsString(JsMessages.ConfirmLoanDelete)%>)) {
      var args = {};
      args["Count"] = list.length + '';
      for (var i = 0; i < list.length; i++)
        args["LoanID_" + i] = list[i];
        
      var result = gService.loanutils.call("BatchDelete", args);
      
      if (result.error == false) 
      {
        if (result.value.Error == 'True')
        {
          alert(result.value.UserMsg);
        }
      }

      if (typeof(refreshPipeline) == "function") refreshPipeline(true);      

    }


}

-->
</script>
<asp:Panel id="DeleteLoanPanel" runat="server" style="DISPLAY: inline;">
	<input id="DeleteLoanBtn" type="button" value="Delete" onclick="if(!hasDisabledAttr(parentElement)) onDeleteClick();" NoHighlight>
</asp:Panel>
