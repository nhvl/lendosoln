<%@ Control Language="c#" AutoEventWireup="false" Codebehind="AssignLoanButton.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.AssignLoanButton" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.AntiXss" %>
<script>
    function onAssignClick() {
        var list = getCheckList();
        var status = getStatus();
        if (list.length == 0) {
            alert(<%= AspxTools.JsString(JsMessages.NoLoanSelected) %>);
            return;
        }

        if( list.length > 100 ) {
            alert(<%= AspxTools.JsString(JsMessages.TooManyLoansSelected) %>);
            return;
        }

        var args = {
            Loans  : "['" + list.join("','")+"']"
        };

        var result = gService.utils.call("CacheLoans", args);

        if(result.error )
        {
            alert('Unable to store assigned loans');
            return;
        }

        showModal(
            "/los/Portlets/AssignAndChangeStatus.aspx?status=" + status + "&key=" + result.value.Key, null,
            'dialogWidth:200px;dialogHeight:200px;center:yes;resizable:no;scroll:yes;status=yes;help=no;', null,
            onAssignClickCallback, { width:"550px", height:"600px", hideCloseButton:true });
    }

    function onAssignClickCallback(args)
    {
        // If isCustomRefresh is false, then assume this method was called
        // from the main page. Therefore refresh the los/main.aspx. Else
        // return args and let the caller handle refresh.
        if (args.OK) {
            // Close all of the converted lead windows that are currently open if
            // converted lead to a loan.
            var removeFromCurrentLoanListExists = (typeof (removeFromCurrentLoanList) == "function");

            if (args.bConvertedLeadToLoan && removeFromCurrentLoanListExists) {
                var length = args.convertedIds.length;
                for (var i = 0; i < length; i++) {
                    removeFromCurrentLoanList(args.convertedIds[i]);
                    callFrameMethod(parent, "frmCode", "closeWindow", [args.convertedIds[i]]);
                }
            }
            if (typeof (refreshPipeline) == "function") refreshPipeline(args.bConvertedLeadToLoan);
        }
    }
</script>
<input id="AssignLoanBtn" type="button" value="Assign &amp; Change Status" style="WIDTH: 140px;" onclick="onAssignClick();" NoHighlight>
