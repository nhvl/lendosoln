using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.Portlets
{
	public class AllBranchStatisticsRow
	{
		private string m_sStatusT;
		private string m_Count;
		private string m_LoanStatus;

		public AllBranchStatisticsRow(string sStatusT, string Count, string LoanStatus)
		{
			m_sStatusT = sStatusT;
			m_Count = Count;
			m_LoanStatus = LoanStatus;
		}

		public string sStatusT
		{
			get { return m_sStatusT; }
		}

		public string Count
		{
			get { return m_Count; }
		}

		public string LoanStatus
		{
			get { return m_LoanStatus; }
		}
	}

	public class AllBranchStatistics
	{
		private DateTime m_CacheDateTime;
		private string m_TimeZone;
		private List<AllBranchStatisticsRow> m_ArrayList;

		public AllBranchStatistics(DateTime time, string timeZone, List<AllBranchStatisticsRow> list)
		{
			m_CacheDateTime = time;
			m_TimeZone = timeZone;
			m_ArrayList = list;
		}

		public DateTime CacheDateTime
		{
			get { return m_CacheDateTime; }
		}

		public string TimeZone
		{
			get { return m_TimeZone; }
		}

		public List<AllBranchStatisticsRow> AllBranchStatisticsRowList
		{
			get { return m_ArrayList; }
		}
	}

	public partial  class LoanStatisticsPortlet : System.Web.UI.UserControl
	{
		protected static Hashtable hash = new Hashtable();

		protected void PageLoad(object sender, System.EventArgs e)
		{
            HttpCookie cookie = null;
            // 2/12/2004 dd - Load status date range in user cookie.
            if (!Page.IsPostBack) 
            {
                cookie = Request.Cookies["sStatusD"];
                if (null != cookie) 
                {
					// 8/3/2006 nw - OPM 6740 - For users with "All dates" selected, default them to "Year to date"
					if ("0" == cookie.Value)
						Tools.SetDropDownListValue(m_datesDDL, "6");
                    else
						Tools.SetDropDownListValue(m_datesDDL, cookie.Value);
                }
            }

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            if (!(principal.HasRole(E_RoleT.Manager) && principal.HasPermission(Permission.BrokerLevelAccess))) 
            {
                // 5/28/2004 dd - Only person has Manager role and a Corporate level access can see the statistics of whole broker.
                // WHo might have a cow about this feature J&J.
                this.Visible = false;
                return;
            }

			m_updateButton.Click += new EventHandler(this.UpdateButton_Click);
			Guid brokerID = principal.BrokerId;
			string sHashKey = string.Format("{0}&DateRange={1}", brokerID.ToString(), m_datesDDL.SelectedItem.Value);

			if (hash.ContainsKey(sHashKey))
			{
				// 3/22/2007 nw - OPM 6782 - Load "All Branch Statistics" result from cache
				AllBranchStatistics stats = (AllBranchStatistics) hash[sHashKey];
				m_repeater.DataSource = stats.AllBranchStatisticsRowList;
				m_repeater.DataBind();
				m_cacheDateTimeLabel.Text = string.Format("(as of {0} {1})", stats.CacheDateTime.ToString(), stats.TimeZone);
			}
			else
			{
				UpdateAllBranchStatistics();
			}

            // 2/12/2004 dd - Store status date range in user cookie
            cookie = new HttpCookie("sStatusD", m_datesDDL.SelectedItem.Value);
            cookie.Expires = DateTime.MaxValue; // Not expiring.

            Response.Cookies.Add(cookie);
		}

		protected void UpdateButton_Click( object sender , System.EventArgs a )
		{
			UpdateAllBranchStatistics();
		}

		private void UpdateAllBranchStatistics()
		{
			// 10/1/2004 kb - We need today's date, but not the time too.
			// We want our edge to lie one instant after midnight for
			// this day.
			//
			// 10/5/2004 kv - We now have an upper bound on the date
			// ranges to keep things on the respective boundaries.
			//
			// 8/11/2006 nw - OPM 6740 - Simplify the code for date range

			DateTime dt = DateTime.Now.Date;
			DateTime lt = DateTime.Now.Date;

			switch (m_datesDDL.SelectedItem.Value) 
			{
				case "1": // last 7 days
					dt = dt.AddDays(-7);
					break;
				case "2": // this month
					dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
					break;
				case "3": // last month
					dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1);
					lt = dt.AddMonths(1).AddDays(-1);
					break;
				case "4": // last 2 months
					dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-2);
					lt = dt.AddMonths(2).AddDays(-1);
					break;
				case "5": // last 3 months
					dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-3);
					lt = dt.AddMonths(3).AddDays(-1);
					break;
				case "6": // year to date
					dt = new DateTime(DateTime.Now.Year, 1, 1);
					break;
				case "8": // 8/11/2006 nw - OPM 6740 - New value "Previous year"
					dt = new DateTime(DateTime.Now.Year - 1, 1, 1);
					lt = new DateTime(DateTime.Now.Year - 1, 12, 31);
					break;
					// 8/3/2006 nw - OPM 6740 - default used to be set to "All dates", but that is now removed 
					// from the drop down list, so we will now default to "Year to date"
                case "9": //Today
                    dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    lt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                    break;
				default:
					dt = new DateTime(DateTime.Now.Year, 1, 1);
					break;
			}

			BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
			Guid brokerID = principal.BrokerId;

			SqlParameter[] parameters = { 
											new SqlParameter("@BrokerID" , brokerID) ,
											new SqlParameter("@sStatusD" , dt == DateTime.MinValue ? (object) DBNull.Value : (object) dt),
											new SqlParameter("@LimitD"   , dt == DateTime.MinValue ? (object) DBNull.Value : (object) lt)
										};

			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "LoanStatisticsByBroker", parameters)) 
			{
				// 3/22/2007 nw - OPM 6782 - Cache "All Branch Statistics" result for each broker
				DateTime cacheDateTime = DateTime.Now;
                List<AllBranchStatisticsRow> arrayList = new List<AllBranchStatisticsRow>();
				while (reader.Read())
				{
					if (!(reader["sStatusT"] is DBNull) && !(reader["Count"] is DBNull))
					{
                        E_sStatusT sStatusT = (E_sStatusT)reader["sStatusT"];
                        string statusDescription = CPageBase.sStatusT_map_rep(sStatusT);
                        AllBranchStatisticsRow row = new AllBranchStatisticsRow(reader["sStatusT"].ToString(), reader["Count"].ToString(), statusDescription);
						arrayList.Add(row);
					}
				}
                List<AllBranchStatisticsRow> sortedList = new List<AllBranchStatisticsRow>();
                foreach (E_sStatusT status in CPageBase.s_sStatusT_Order)
                {
                    AllBranchStatisticsRow hit = arrayList.Find(i => i.sStatusT == status.ToString("d"));
                    if (hit != null)
                    {
                        sortedList.Add(hit);
                        arrayList.Remove(hit);
                    }
                }
                arrayList = sortedList;
				m_repeater.DataSource = arrayList;
				m_repeater.DataBind();
				string sTimeZone = TimeZone.CurrentTimeZone.IsDaylightSavingTime(DateTime.Now) ? "PDT" : "PST";
				m_cacheDateTimeLabel.Text = string.Format("(as of {0} {1})", cacheDateTime.ToString(), sTimeZone);

				AllBranchStatistics stats = new AllBranchStatistics(cacheDateTime, sTimeZone, arrayList);
				string sHashKey = string.Format("{0}&DateRange={1}", brokerID.ToString(), m_datesDDL.SelectedItem.Value);
				if (hash.ContainsKey(sHashKey))
					hash[sHashKey] = stats;
				else
					hash.Add(sHashKey, stats);
			}
		}

        void LoanStatisticsPortlet_Init(object sender, EventArgs e)
        {
            m_repeater.ItemDataBound += new RepeaterItemEventHandler(m_repeater_ItemDataBound);
        }

        void m_repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HyperLink link = (HyperLink)e.Item.FindControl("m_portletLink");
            
            AllBranchStatisticsRow row = e.Item.DataItem as AllBranchStatisticsRow;
            if (row != null)
            {
                link.Text = AspxTools.HtmlString(row.Count + " " + row.LoanStatus);
                link.NavigateUrl = "~/los/LoanFind.aspx?loanstatus=" + Uri.EscapeDataString(row.sStatusT) + "&sStatusD=" + Uri.EscapeDataString(m_datesDDL.SelectedItem.Value);
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new EventHandler(LoanStatisticsPortlet_Init);

        }


		#endregion
	}
}
