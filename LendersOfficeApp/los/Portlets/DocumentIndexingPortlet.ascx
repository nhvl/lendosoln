<%@ Control Language="c#" AutoEventWireup="True" CodeBehind="DocumentIndexingPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.DocumentIndexingPortlet" %>

<script type="text/javascript">
    function LoadDocumentPortalClick()
    {
        var response = gService.loanutils.call('GetOCRPortalReviewLink');

        if (response.error) {
            alert(response.UserMessage);
        }
        else if (response.value["Error"]) {
            alert(response.value["Error"]);
        }
        else {
            var url = response.value["URL"];
            window.open(url);
        }
        return false;
    }
</script>

<table cellspacing="0" cellpadding="0" border="0" width="100%" class="Portlet">
    <tr>
        <td nowrap class="PortletHeader">Document Indexing</td>
    </tr>
    <tr>
        <td >
 		<img src="<%=LendersOffice.AntiXss.AspxTools.SafeUrl(DataAccess.Tools.VRoot)%>/images/bullet.gif" align="absMiddle">&nbsp; <A href="#" onclick="return LoadDocumentPortalClick()" class="PortletLink" title="Document Review Portal">
				Document Review Portal</A>
			<br>
        </td>
    </tr>
    	<tr>
		<td><img src="../images/shadow_line.gif" width="130" height="2"></td>
	</tr>
</table>
