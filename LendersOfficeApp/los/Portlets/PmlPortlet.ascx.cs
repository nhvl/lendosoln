using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.Portlets
{

	public partial  class PmlPortlet : System.Web.UI.UserControl
	{
		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		/// <summary>
		/// Check access control.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Hide if not accessible.

			try
			{
				// You must have the feature and have the specific permission.
				
				if(    CurrentUser.HasFeatures( E_BrokerFeatureT.PriceMyLoan ) == false 
                    || (   CurrentUser.HasPermission(Permission.CanAdministrateExternalUsers) == false
                        && CurrentUser.HasPermission(Permission.AllowTpoPortalConfiguring) == false
                        )
                    )
					Visible = false;

                TpoPortalConfigLink.Visible = CurrentUser.HasPermission(Permission.AllowTpoPortalConfiguring);
                TpoCompaniesAndAdminLinks.Visible = CurrentUser.HasPermission(Permission.CanAdministrateExternalUsers);
                TpoCompanyTiersLink.Visible = CurrentUser.HasPermission(Permission.AllowEditingOriginatingCompanyTiers);
			}
			catch( Exception e )
			{
				// Oops!

				DataAccess.Tools.LogError( e );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}

}
