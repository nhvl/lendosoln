﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChooseReport.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.ChooseReport" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<div style="margin-top: 8px; padding: 8px; background-color: whitesmoke; border: 2px solid lightgrey;">
    <table cellpadding="2" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="FieldLabel" colspan="3">
                Your Reports
                <hr color="LightGrey" size="2">
            </td>
        </tr>
        <asp:Repeater ID="m_yourQueries" runat="server" OnItemDataBound="YourQueries_OnItemDataBound" OnItemCommand="ReportClick">
            <ItemTemplate>
                <tr onmouseover="className = 'HighlightReport';" onmouseout="className = '';" onclick="cells[ 2 ].children[ 0 ].click();">
                    <td style="width: 20px; color: tomato;" align="center">
                        <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Selected" )) %>
                    </td>
                    <td>
                        <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryName" )) %>
                    </td>
                    <td align="right">
                        <asp:LinkButton runat="server" CommandName="Choose" ForeColor="Tomato" ID="YourChooseLink">
								choose
                        </asp:LinkButton>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</div>
<div style="margin-top: 8px; padding: 8px; background-color: whitesmoke; border: 2px solid lightgrey;">
    <table cellpadding="2" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="FieldLabel" colspan="3">
                Others' Reports
                <hr style="color: LightGrey;" size="2" />
            </td>
        </tr>
        <asp:Repeater ID="m_othrQueries" runat="server" OnItemDataBound="OtherQueries_OnItemDataBound" OnItemCommand="ReportClick">
            <ItemTemplate>
                <tr onmouseover="className = 'HighlightReport';" onmouseout="className = '';" onclick="cells[ 2 ].children[ 0 ].click();">
                    <td style="width: 20px; color: tomato;" align="center">
                        <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Selected" )) %>
                    </td>
                    <td>
                        <%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "QueryName" )) %>
                    </td>
                    <td align="right">
                        <asp:LinkButton ID="OthersChooseLink" runat="server" CommandName="Choose" ForeColor="Tomato">
								choose
                        </asp:LinkButton>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</div>
