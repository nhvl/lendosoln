﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.MortgagePool;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.los.Portlets
{
    public partial class LoanPoolPortlet : System.Web.UI.UserControl
    {
        public bool HideDeleteLinks { get; set; }
        public bool HideCreatePool { get; set; }
        public bool SelectPoolOnly { get; set; }

        public void PageLoad(object sender, EventArgs args)
        {
            if (!this.Visible) return;

            var basepage = Page as BasePage;
            if (basepage == null) throw new Exception("Loan Pool Portlet expects to be used on a BasePage");

            basepage.EnableJqueryMigrate = false;

            if(!SelectPoolOnly) Page.ClientScript.RegisterHiddenField("OpenPoolOnSelect", "true");

            m_newPool.Visible = !HideCreatePool;
            SetupDeleteLinks();
            BindDataGrid();
        }

        private void SetupDeleteLinks()
        {
            if (HideDeleteLinks) return;
            //Programatically add Delete Buttons (Problem with setting enable/disable in ASPX)
            TemplateColumn tc = new TemplateColumn {ItemTemplate = new DeleteButton(this)};
            tc.ItemStyle.Wrap = false;
            m_PoolGrid.Columns.Add(tc);
        }

        protected void CreatePool(object sender, EventArgs args)
        {
            //If creation failed, don't reset/rebind
            string internalId = MortgagePool.GetPoolAutoId();
            var newId = MortgagePool.CreatePool(internalId);
            if (newId == -1)
                return;

            //Reset Filters so new pool is displayed
            if (m_PoolStatus.SelectedIndex > 1)
                m_PoolStatus.SelectedIndex = 1;
            m_PoolAgency.SelectedIndex = 0;
            m_AmortizationType.SelectedIndex = 0;
            m_PoolNumber.Text = "";

            Page.ClientScript.RegisterStartupScript(this.GetType(), "autoOpenPool", "openPoolDetails(" + newId + ");", true);

            BindDataGrid();
        }

        protected string ValueOrDefault(string value, string df)
        {
            if (string.IsNullOrEmpty(value.TrimWhitespaceAndBOM())) return df;
            return value;
        }

        protected void DeletePool(object sender, CommandEventArgs a)
        {
            //If nothing deleted, don't rebind
            if (MortgagePool.DeletePool(BrokerUserPrincipal.CurrentPrincipal.BrokerId, long.Parse(a.CommandArgument.ToString())))
            {
                BindDataGrid();
            }
        }

        protected void BindDataGrid()
        {
            int MAX_ROWS = 250,
                INITIAL_LOAD_MAX_ROWS = 100;

            bool? isOpen = null;
            E_MortgagePoolAgencyT? agencyT = null;
            E_sFinMethT? amortT = null;
            string poolNum = m_PoolNumber.Text.TrimWhitespaceAndBOM();
            bool allowPartialPoolNum = m_PoolNumPartialMatch.Checked;
            string poolId = m_InternalId.Text.TrimWhitespaceAndBOM();
            bool allowPartialPoolId = m_PoolIdPartialMatch.Checked;
            string commitmentNum = m_CommitmentNumber.Text.TrimWhitespaceAndBOM();
            bool allowPartialCommitmentNum = m_CommitmentNumberPartialMatch.Checked;
            DataTable dt;

            // OPM 149524 - Switched to unified search
            if (m_PoolStatus.Value == "0")
                isOpen = false;
            else if (m_PoolStatus.Value == "1")
                isOpen = true;

            if (m_PoolAgency.Value != "-1")
                agencyT = (E_MortgagePoolAgencyT)Enum.Parse(typeof(E_MortgagePoolAgencyT), m_PoolAgency.Value);

            if (m_AmortizationType.Value != "-1")
                amortT = (E_sFinMethT)Enum.Parse(typeof(E_sFinMethT), m_AmortizationType.Value);

            dt = MortgagePool.FindMortgagePools(
                isOpen,
                agencyT,
                amortT,
                poolNum,
                allowPartialPoolNum,
                poolId,
                allowPartialPoolId,
                commitmentNum,
                allowPartialCommitmentNum);

            m_displayCount.Style["color"] = "Black";
            m_displayCount.Style["Font-weight"] = "normal";

            if (dt.Rows.Count > MAX_ROWS)
            {
                if (!IsPostBack)
                {
                    m_displayCount.Text = "Displaying first " + INITIAL_LOAD_MAX_ROWS + " loan pools.";
                    DataTable newDt = dt.Clone();
                    for (int index = 0; index < INITIAL_LOAD_MAX_ROWS; index++)
                    {
                        newDt.Rows.Add(dt.Rows[index].ItemArray);
                    }
                    dt = newDt;
                }
                else
                {
                    m_displayCount.Text = "Too many files -- displaying first " + MAX_ROWS + ".";
                    m_displayCount.Style["Color"] = "Red";
                    m_displayCount.Style["Font-weight"] = "bold";
                    DataTable newDt = dt.Clone();
                    for (int index = 0; index < MAX_ROWS; index++)
                    {
                        newDt.Rows.Add(dt.Rows[index].ItemArray);
                    }
                    dt = newDt;
                }
            }
            else if (dt.Rows.Count > 0)
            {
                m_displayCount.Text = "Displaying 1-" + dt.Rows.Count + " of " + dt.Rows.Count+".";
            }
            else
            {
                m_displayCount.Text = "No loan pools found.";
            }

            m_PoolGrid.DataSource = dt.DefaultView;
            m_PoolGrid.DataBind();
        }
        protected class DeleteButton : ITemplate
        {
            LoanPoolPortlet parentPage;
            public DeleteButton(LoanPoolPortlet parentPage)
            {
                this.parentPage = parentPage;
            }
            public void InstantiateIn(Control container)
            {
                if (parentPage.HideDeleteLinks) return;
                LinkButton button = new LinkButton {Text = "delete pool"};
                button.Command += parentPage.DeletePool;
                button.DataBinding += this.BindData;
                container.Controls.Add(button);
            }
            private void BindData(object sender, EventArgs args)
            {
                LinkButton button = (LinkButton)sender;
                button.Enabled = ((int)DataBinder.Eval(((DataGridItem)button.Parent.Parent).DataItem, "LoanCount")) == 0;
                button.CommandArgument = DataBinder.Eval(((DataGridItem)button.Parent.Parent).DataItem, "PoolId").ToString();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
