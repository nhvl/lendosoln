﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuickPricerPortlet.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.QuickPricerPortlet" %>
<script>
    function onQuickPricerClick() {
        callFrameMethod(parent, "frmCode", "f_openQuickPricer")
    }
    function onMonitoredScenariosClick() {
        callFrameMethod(parent, "frmCode", "f_openMonitoredScenarios")
    }
</script>

<table class="Portlet">
    <tr>
        <td class="PortletHeader">Quick Pricer</td>
    </tr>
    <tr>
        <td>
            <img src="../images/bullet.gif" align="absMiddle" />&nbsp;
            <a href="#" onclick="onQuickPricerClick()" class="PortletLink" title="Quickpricer">Quick Pricer</a>
            <br>
            <asp:PlaceHolder runat="server" ID="MonitoredScenarios">
            <img src="../images/bullet.gif" align="absMiddle" />&nbsp;
            <a href="#" onclick="onMonitoredScenariosClick()" class="PortletLink" title="Monitored Scenarios">Monitored Scenarios</a>
            </asp:PlaceHolder>
        </td>	
    </tr>
    <tr>
        <td><img src="../images/shadow_line.gif" width="130" height="2" /></td>
    </tr>
</table>
