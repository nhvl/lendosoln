using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.LoanPrograms;
using LendersOffice.ObjLib.DynaTree;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Template
{

    internal class RemoveElement
	{
		Guid m_id;
		string m_type;

        public RemoveElement(string id, string typ)
		{
			if (id == null || typ == null)
				throw new ArgumentNullException();
			m_id = id.Equals("root") ? Guid.Empty : new Guid(id);
			m_type = typ;
		}
		
		public string id
		{ get	{ return m_id.ToString(); } }
		
		public string type
		{ get	{ return m_type; } }
	}


	public partial class LoanProductFolder : BasePage
	{
        private DynaTreeNode rootNode;

        private Hashtable m_hashTableDyna = new Hashtable();
        private ArrayList  m_errorSet = new ArrayList();
		private String  m_currentSelection =   "";

		private BrokerUserPrincipal CurrentUser
		{
			// Get current user.
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		protected bool CanModifyLoanPrograms
		{
			get { return CurrentUser.HasPermission( Permission.CanModifyLoanPrograms); }
		}

		private String ErrorMessage
		{
			// Access member.

			set
			{
				// Save this error so we can report it at the end.

				if( m_errorSet.Count < 20 )
				{
					if( value.EndsWith( "." ) == false )
					{
						m_errorSet.Add( value + "." );
					}
					else
					{
						m_errorSet.Add( value );
					}
				}
			}
		}

		private String Feedback
		{
			// Access member.

			set
			{
				// Place text inside a hidden variable for feedback
				// to the client.

				if( value.EndsWith( "." ) == false )
				{
					Page.ClientScript.RegisterHiddenField( "m_feedBack" , value + "." );
				}
				else
				{
					Page.ClientScript.RegisterHiddenField( "m_feedBack" , value );
				}
			}
		}

        private DynaTreeNode GetProgramNode(Guid key)
        {
            DynaTreeNode node = (DynaTreeNode)m_hashTableDyna[key];
            if (node == null)
            {
                node = new DynaTreeNode();
                m_hashTableDyna[key] = node;
            }
            return node;
        }
		
		private Guid m_brokerID 
		{
			get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
		}

		protected void PageLoad( object sender , System.EventArgs a )
		{
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterJsScript("LQBPopup.js");
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");

            RegisterJsGlobalVariables("CanModifyLoanPrograms", CanModifyLoanPrograms);

            // Get the currently selected tree node.  If not available,
            // then mark the current as unselected.  TODO: Pull this
            // info from the tree, not a hack client var.

            m_currentSelection = currentNode.Value;

			// 7/15/2004 kb - Setup the input buttons to reflect the
			// current feature set and permissions.

			if( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == false )
			{
				m_exportPanel.Visible = false;
				m_RecursiveDelBtn.Visible = false;
                m_derivePanel.Visible = false;
                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true && CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
				{
					m_addnewPanel.Enabled = false;
					m_deletePanel.Enabled = false;
					m_propsPanel.Enabled  = false;

				}
				else
				{
					m_addnewPanel.Enabled = true;
					m_deletePanel.Enabled = true;
					m_propsPanel.Enabled  = true;
				}
                m_forBatchUpdate.Visible = false;
				m_forEditor.Visible = false;
			}
			else
			{
				m_exportPanel.Visible = true;
                m_derivePanel.Visible = true;

                m_forEditor.Visible = true;
                m_forBatchUpdate.Visible = true;
			}

            // Default expand one level.  Erase current contents
            // in case its caching the previous loading state.  We
            // now support deleting, so refresh with every post
            // back to the server.

            this.rootNode = new DynaTreeNode();
            rootNode.title = "Root Folder";
            rootNode.isFolder = true;
            rootNode.href = "LoanProgramList.aspx";
            rootNode.key = "root";

            using (DbDataReader sR = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListLoanProductFolderByBrokerID", new SqlParameter("@BrokerId", m_brokerID), new SqlParameter("@IsLpe", CurrentUser.HasPermission(Permission.CanModifyLoanPrograms))))
            {
                while (sR.Read())
                {
                    DynaTreeNode parentNode = rootNode;
                    DynaTreeNode childNode = GetProgramNode((Guid)sR["FolderID"]);

                    childNode.title = Server.HtmlEncode(sR["FolderName"].ToString()).TrimWhitespaceAndBOM();
                    childNode.key = sR["FolderID"].ToString();
                    childNode.href = "LoanProgramList.aspx?folderid=" + childNode.key;
                    childNode.isFolder = true;

                    if (sR["ParentFolderID"] != DBNull.Value)
                    {
                        parentNode = GetProgramNode((Guid)sR["ParentFolderID"]);
                    }                    

                    parentNode.children.Add(childNode);
                }
            }
            
            RegisterJsObjectWithJsonNetSerializer("nodes", rootNode);
            RegisterJsGlobalVariables("currentNode", string.Empty);
        }

		/// <summary>
		/// Render the current state according to the db.
		/// </summary>
		protected void PagePreRender( object sender , System.EventArgs a )
		{
            // List all the errors found during processing of the request.
            String errorMessage = "";

			foreach( String error in m_errorSet )
			{
				errorMessage += error + "\n";
			}

			if( errorMessage.TrimWhitespaceAndBOM() != "" )
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , errorMessage + "* " + ( ( m_errorSet.Count == 1 ) ? " 1 error." : " " + m_errorSet.Count + " errors." ) );
			}

            if (errorMessage != string.Empty)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "callInit", "init()");
            }
        }

		#region Web Form Designer generated code

        
		override protected void OnInit(EventArgs e)
		{
            EnableJqueryMigrate = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

		/// <summary>
		/// Remove folder as long as no loan products are nested
		/// below (at any level).
		/// </summary>
		protected void OnDeleteClick( object sender , System.EventArgs a )
		{
			// Nix the specified folder and all its children.

			try
			{
				// 7/15/2004 kb - Block access if pricing engine enabled
				// and user lacks permission.

                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
				{
                    if (CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false && CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
					{
						ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

						return;
					}
				}

				// Start by gathering the children of this loan
				// program folder.  We scrutinize each folder
				// before deleting.

				ArrayList gather = new ArrayList();

				if( m_currentSelection == "root" )
				{
					ErrorMessage = "Unable to delete root node.";

					return;
				}

				try
				{
					gather.Add( new Guid( m_currentSelection ) );
				}
				catch
				{
					throw new CBaseException(ErrorMessages.InvalidLoanProgramFolder, "Invalid loan program folder id for " + m_currentSelection);
				}

				for( int i = 0 ; i < gather.Count ; ++i )
				{
					// Get the current folder and look for any loan
					// products that may be associated in this one.
					// If found, punt by listing the name and failing.
					// We only delete folders when all the children
					// are empty.
                    DataSet ds = new DataSet();
                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@BrokerId", CurrentUser.BrokerId)
						                            , new SqlParameter( "@IsLpe", CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) )
						                            , new SqlParameter( "@FolderId", gather[ i ])
                                                };
					DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListLoanProductFolderChildrenByBrokerID", parameters);

					foreach( DataRow row in ds.Tables[ 0 ].Rows )
					{
						gather.Add( row[ "FolderId" ] );
					}

					// Now check if this folder has any immediate
					// loan products associated with it.  If so,
					// then skip the delete.
                    DataSet ps = new DataSet();
                    parameters = new SqlParameter[] {
                                                        new SqlParameter( "@BrokerId" , CurrentUser.BrokerId                                          )
						                                , new SqlParameter( "@IsLpe"    , CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) )
						                                , new SqlParameter( "@FolderId" , gather[ i ]                                                   )
                                                    };
					DataSetHelper.Fill(ps, DataSrc.LpeSrc, "ListLoanProgramsByBrokerID", parameters);

					if( ps.Tables[ 0 ].Rows.Count > 0 )
					{
						ErrorMessage = "Folder contains loan products.  Please remove all associated loan products from within this folder before deleting.";

						return;
					}
				}

				for( int i = gather.Count ; i > 0 ; --i )
				{
					// Delete this folder.  Note that we remove in
					// reverse.  The leaves need to go first, and
					// they were gathered last.

					StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "DeleteLoanProductFolder", 0
						, new SqlParameter( "@BrokerId" , CurrentUser.BrokerId )
						, new SqlParameter( "@FolderId" , gather[ i - 1 ]      )
						);
				}

                RegisterJsGlobalVariables("deleteSuccess", true);
            }
			catch( Exception e )
			{
				// Pass on the love.

				ErrorMessage = "Unable to delete selected folder.";

				Tools.LogError( e );
			}
		}

		//opm 21419 fs 06/27/08
		protected void m_RecursiveDelBtn_Click(object sender, System.EventArgs e)
		{
			try
			{
				//Check proper permissions
                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
				{
					if( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == false && CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false )
					{
						ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";
						return;
					}
				}

				//Do not allow recursive removal from pmlmaster account.
				if( CurrentUser.LoginNm.Equals("pmlmaster") || CurrentUser.BrokerName.Equals("MASTER PML"))
				{
					ErrorMessage = "Recursive folder delete cannot be used from the PML master account.";
					return;
				}
				
				ArrayList elementsToDelete = new ArrayList();

				try
				{
					elementsToDelete.Add(new RemoveElement(m_currentSelection,"folder"));
				}
				catch
				{
					//do something else with this error
                    throw new CBaseException(ErrorMessages.InvalidLoanProgramFolder, "Problem creating a new RemoveElement with " + m_currentSelection);
				}

				//Traverse folders recursively
				for( int i = 0 ; i < elementsToDelete.Count ; ++i )
				{
					if (((RemoveElement)elementsToDelete[i]).type.Equals("folder"))
					{
						string currentFolder = ((RemoveElement)elementsToDelete[i]).id;
						AddFolderLoans(currentFolder, elementsToDelete, "masterloan");
						AddSubfolders(currentFolder, elementsToDelete);
						AddFolderLoans(currentFolder, elementsToDelete, "loan");
						
					}
				}

				//Remove loans in the proper order...
				if (((RemoveElement)elementsToDelete[0]).id.Equals(Guid.Empty.ToString()))
					elementsToDelete.RemoveAt(0);
				elementsToDelete.Reverse();
				
				Hashtable table = new Hashtable();
				int foldersRemoved = 0;
				int loansDeleted = 0;
				int error = 0;
				
				foreach (RemoveElement item in elementsToDelete)
				{
					switch(item.type)
					{
						case "folder":
							error = DelFolder(new Guid(item.id),table);
							if (error == 0)
								foldersRemoved++;
							break;
						case "masterloan":
						case "loan":
							loansDeleted += DelLoanProductAndDerived(new Guid(item.id),table);
							break;
					}
				}
				
				Feedback = "Deleted " + loansDeleted + " loan programs in " + foldersRemoved + " folders.";

                RegisterJsGlobalVariables("recursiveDeleteSuccess", true);
            }
			catch( Exception ex )
			{
				ErrorMessage = "Unable to fully delete the selected folder.";
				Tools.LogError("Exception performed when trying to perform a recursive folder delete.\n" + ex );
			}
		}

		public int DelFolder( Guid folderId, Hashtable deleted )
		{
			try
			{
                if (folderId == System.Guid.Empty || deleted == null)
                {
                    throw new ArgumentException("Invalid Arguments.");
                }

                DataSet ds = new DataSet();
                SqlParameter[] parameters = {
                                                new SqlParameter( "@BrokerId" , CurrentUser.BrokerId)
					                            , new SqlParameter( "@IsLpe", CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) )
					                            , new SqlParameter( "@FolderId" , folderId)
                                            };
				DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListLoanProductFolderChildrenByBrokerID", parameters);

				if (ds.Tables[ 0 ].Rows.Count > 0  )
				{
					//folder not empty!
					throw new CBaseException(ErrorMessages.FailedToDeleteFolderNotEmpty, ErrorMessages.FailedToDeleteFolderNotEmpty);
				}

                DataSet ps = new DataSet();
                parameters = new SqlParameter[] {
                                                new SqlParameter( "@BrokerId" , CurrentUser.BrokerId)
					                            , new SqlParameter( "@IsLpe"    , CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) )
					                            , new SqlParameter( "@FolderId" , folderId)
                                            };
				DataSetHelper.Fill(ps, DataSrc.LpeSrc, "ListLoanProgramsByBrokerID", parameters);

				if( ps.Tables[ 0 ].Rows.Count > 0 )
				{
					//folder not empty!
					throw new CBaseException(ErrorMessages.FailedToDeleteFolderNotEmpty, ErrorMessages.FailedToDeleteFolderNotEmpty);
				}

				// Delete this folder.
				int result = StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "DeleteLoanProductFolder", 0
					, new SqlParameter( "@BrokerId" , CurrentUser.BrokerId )
					, new SqlParameter( "@FolderId" , folderId     )
					);
				if (result >= 0)
				{
					Tools.LogInfo("Deleted loan program folder " + folderId.ToString() + " by " + CurrentUser.LoginNm + " in a recursive delete transaction.");
					deleted.Add(folderId,"folder");
					return 0;
				}
				return -1;
			}
			catch (ArgumentException ex)
			{
				string msg = "Method 'DelFolder' was not called with the proper parameters.";
				Tools.LogError(msg, ex);
				throw new CBaseException(ErrorMessages.FailedToDeleteFolder, msg);
			}
			catch (SqlException ex2)
			{
				string msg = "SQL error when trying to recursively delete folder " + folderId.ToString() + " by " + CurrentUser.LoginNm + ".";
				Tools.LogError(msg, ex2);
				throw new CBaseException(ErrorMessages.FailedToDeleteFolder, msg);
			}
			catch (Exception ex3)
			{
				string msg = "Error found when trying to recursively delete folder " + folderId.ToString() + " by " + CurrentUser.LoginNm + ".";
				Tools.LogError(msg, ex3);
				throw new CBaseException(ErrorMessages.FailedToDeleteFolder, msg);
			}
		}

		public int DelLoanProductAndDerived( Guid loanProductId, Hashtable deleted )
		{
			ArrayList idsToLookup = new ArrayList();
			ArrayList idsToDelete = new ArrayList();

			idsToLookup.Add( loanProductId );

			while ( idsToLookup.Count > 0 )
			{
				ArrayList nextSet = new ArrayList();
				foreach (Guid id in idsToLookup )
				{
					using( var sR = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListLoanProductDerivations" , new SqlParameter( "@ProductId" , id ) ) )
					{
						while( sR.Read() == true )
						{
							nextSet.Add( ( Guid ) sR[ "lLpTemplateId" ] );
						}
					}
				}
				idsToDelete.AddRange( idsToLookup );
				idsToLookup = nextSet;
			}
			
			idsToDelete.Reverse();

			int programsDeleted = 0;
			foreach ( Guid id in idsToDelete )
			{
				try
				{
					if (!deleted.Contains(id))
					{
                        int iRes = CLoanProductBase.DeleteLoanProduct(id);

						if( iRes < 0 )
							continue;
						else
						{
							Tools.LogInfo("Deleted loan program " + id.ToString() + " by " + CurrentUser.LoginNm + " in a recursive delete transaction.");
							deleted.Add(id,"loan");
							programsDeleted++;
						}
					}
				}
				catch (Exception e )
				{
					Tools.LogError("Failed to delete loan program: " + id + " by " + CurrentUser.LoginNm + " in recursive delete transaction.", e);
					continue;
				}
			}
			return programsDeleted;
		}

		private void AddSubfolders(string folder, ArrayList list)
		{
			SqlParameter s1 = new SqlParameter( "@BrokerId" , CurrentUser.BrokerId);
			SqlParameter s2 = new SqlParameter( "@IsLpe"    , CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ));
			SqlParameter s3 = !folder.Equals(Guid.Empty.ToString()) ? new SqlParameter( "@FolderId" , new Guid(folder)) : new SqlParameter( "@FolderId" , System.DBNull.Value);

            SqlParameter[] parameters = {
                                            s1, s2, s3
                                        };
            DataSet ds = new DataSet();
			DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListLoanProductFolderChildrenByBrokerID", parameters);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                list.Add(new RemoveElement(row["FolderId"].ToString(), "folder"));
            }
		}

		private void AddFolderLoans(string folder, ArrayList list, string type)
		{
			SqlParameter s1 = new SqlParameter( "@BrokerId" , CurrentUser.BrokerId);
			SqlParameter s2 = new SqlParameter( "@IsLpe"    , CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ));
			SqlParameter s3 = !folder.Equals(Guid.Empty.ToString()) ? new SqlParameter( "@FolderId" , new Guid(folder)) : new SqlParameter( "@FolderId" , System.DBNull.Value);

            SqlParameter[] parameters = {
                                            s1, s2, s3
                                        };
            DataSet ps = new DataSet();

			DataSetHelper.Fill(ps, DataSrc.LpeSrc, "ListLoanProgramsByBrokerID", parameters);

			foreach( DataRow row in ps.Tables[ 0 ].Rows )
			{
				if ((bool)row["IsMaster"] == type.Equals("masterloan"))
					list.Add(new RemoveElement(row["lLpTemplateId"].ToString(), type));
			}
		}
		
	}
}