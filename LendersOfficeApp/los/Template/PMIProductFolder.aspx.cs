using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Services;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.Template
{
    public partial class PMIProductFolder : BasePage
    {
        private void RegisterResource()
        {
            EnableJquery = false;
            EnableJqueryMigrate = false;
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");
        }

        protected void PageLoad(object sender, System.EventArgs a)
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.LoginNm.Equals("pmlmaster"))
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, ErrorMessages.GenericAccessDenied);
            }

            RegisterResource();

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("state", LoadData());
        }

        [WebMethod]
        public static object LoadData()
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.LoginNm.Equals("pmlmaster"))
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, ErrorMessages.GenericAccessDenied);
            }

            var folders = new List<object>();
            using (var sR = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListPMIProductFolders"))
            {
                while (sR.Read())
                {
                    folders.Add(new
                    {
                        FolderID = sR["FolderID"],
                        FolderName = sR["FolderName"], //WebUtility.HtmlEncode((string) sR["FolderName"]).TrimWhitespaceAndBOM(),
                        ParentFolderID = sR["ParentFolderID"],
                    });
                }
            }
            return (new { folders });
        }

        [WebMethod]
        public static object Delete(Guid folderId)
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.LoginNm.Equals("pmlmaster"))
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, ErrorMessages.GenericAccessDenied);
            }

            if (folderId == Guid.Empty)
            {
                return new { errorMessage = "" };
            }


            SqlParameter[] parameters =
            {
                new SqlParameter("@FolderId", folderId)
            };
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "DeletePMIProductFolder", 1, parameters);

            return new { errorMessage = "" };
        }

        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.LoadComplete += this.PageLoad;
            base.OnInit(e);
        }
    }

}
