<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="LpSummaryReport.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.LpSummaryReport" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LpSummaryReport</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="LpSummaryReport" method="post" runat="server">
		<%
			if (m_FolderList != null)
			{
				foreach (LendersOfficeApp.los.Template.FolderInfo fInfo in m_FolderList)
				{
					if (fInfo.rgPrograms.Count == 0) continue ;
		%>
				<b>
					<%=AspxTools.HtmlString(fInfo.sFolderName + "::" + fInfo.sFolderId)%>
				</b>
				<table class="DataGrid" cellspacing="0" rules="all" border="1" style="width:100%;border-collapse:collapse;">
					<tr class="GridHeader">
						<td>LpName</td>
						<td>FinMeth</td>
						<td>Term</td>
						<td>Due</td>
						<td>1stAdj Mon</td>
						<td>Adj Period</td>
						<td>Lock Days</td>
						<td>Pre Pay</td>
						<td>Max Ysp</td>
						<td>Min Ysp</td>
						<td>Rate Adj</td>
						<td>Fee Adj</td>
						<td>Enabled</td>
						<td>LienPos</td>
						<td>1st Lien Links</td>
						<td>Investor</td>
					</tr>
		<%
					System.Text.StringBuilder sbPairList = new System.Text.StringBuilder() ;
					string sPrev1stLinks = "" ;
					foreach (LendersOfficeApp.los.Template.ProgramInfo pInfo in fInfo.rgPrograms)
					{
						string s1stLienLinksText = "" ;
						if (pInfo.s1stLienLinks != sPrev1stLinks)
						{
							s1stLienLinksText = "NEW" ;
							sPrev1stLinks = pInfo.s1stLienLinks ;
						}
						else if (pInfo.s1stLienLinks != "")
							s1stLienLinksText = "DUP" ;
						
						Response.Write("<tr>") ;
						Response.Write("<td>" + pInfo.sLpName + "</td>") ;
						Response.Write("<td>" + pInfo.sFinMeth + "</td>") ;
						Response.Write("<td>" + pInfo.sTerm + "</td>") ;
						Response.Write("<td>" + pInfo.sDue + "</td>") ;
						Response.Write("<td>" + pInfo.s1stAdjMon + "</td>") ;
						Response.Write("<td>" + pInfo.sAdjPeriod + "</td>") ;
						Response.Write("<td>" + pInfo.sLockDays + "</td>") ;
						Response.Write("<td>" + pInfo.sPrePay + "</td>") ;
						Response.Write("<td>" + pInfo.sMaxYsp + "</td>") ;
						Response.Write("<td>" + pInfo.sMinYsp + "</td>") ;
						Response.Write("<td>" + pInfo.sRateAdj + "</td>") ;
						Response.Write("<td>" + pInfo.sFeeAdj + "</td>") ;
						Response.Write("<td>" + pInfo.sEnabled + "</td>") ;
						Response.Write("<td>" + pInfo.sLienPos + "</td>") ;
						Response.Write("<td>" + s1stLienLinksText + "</td>") ;
						Response.Write("<td>" + pInfo.sInvestor + "</td>") ;
						Response.Write("</tr>") ;
						
						sbPairList.Append(pInfo.sLpName + ";" + pInfo.s1stLienPairId + ";<br>") ;
					}
		%>
					</table>
					<br>
					<br>
					<table>
						<tr>
							<td class="FieldLabel">Pair List</td>
						</tr>
						<tr>
							<td><%=AspxTools.HtmlString(sbPairList.ToString())%></td>
						</tr>
					</table>
					<hr>
		<%
				}
			}
		%>

		<asp:Panel id="m_Denied" runat="server" style="COLOR: red; FONT: 11px arial; TEXT-ALIGN: center; PADDING: 120px;" Visible="False">
			Access denied.  You do not have permission to view this content.
		</asp:Panel>

		</form>
	</body>
</HTML>
