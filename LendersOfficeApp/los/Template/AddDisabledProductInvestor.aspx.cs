﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ManualInvestorProductExpiration;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.Security;
using LendersOfficeApp.common.ModalDlg;

namespace LendersOfficeApp.los.Template
{
    public partial class AddDisabledProductInvestor : LendersOffice.Common.BasePage
    {
        private BrokerDB broker;
        private List<LockPolicy> lockPolicies = null;

        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected BrokerDB Broker
        {
            get
            {
                if (this.broker == null)
                {
                    this.broker = BrokerDB.RetrieveById(this.BrokerUser.BrokerId);
                }

                return this.broker;
            }
        }

        /// <summary>
        /// Gets Lock Policies that this window governs (either for the specified id or all Lock Policies,<para />
        /// filtered to only include policies with IsUsingRateSheetExpirationFeature enabled).
        /// </summary>
        protected IEnumerable<LockPolicy> LockPolicies
        {
            get
            {
                if (this.lockPolicies == null)
                {
                    Guid lockPolicyId = RequestHelper.GetGuid("policyid", Guid.Empty);
                    this.lockPolicies = lockPolicyId != Guid.Empty ? new List<LockPolicy>(1) { LockPolicy.Retrieve(this.Broker.BrokerID, lockPolicyId) } : LockPolicy.RetrieveAllForBroker(this.Broker.BrokerID);
                    this.lockPolicies.RemoveAll(policy => !policy.IsUsingRateSheetExpirationFeature);
                }

                return this.lockPolicies;
            }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingDisablePricing
                };
            }
        }

        protected bool IsManualExpirationEnabled
        {
            get { return this.LockPolicies.Any(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool enabledForUser = this.IsManualExpirationEnabled;
            this.AccessDeniedPanel.Visible = !enabledForUser;
            this.AccessGrantedPanel.Visible = enabledForUser;

            if (!Page.IsPostBack)
            {
                this.InvestorList.Items.Add(new ListItem("<-- Select Investor-->", string.Empty));
                this.InvestorList.Items.Add(new ListItem("All", "All"));
                List<string> list = InvestorProductManager.ListAllInvestors(this.BrokerUser.BrokerDB);
                foreach (string str in list)
                {
                    this.InvestorList.Items.Add(str);
                }
            }
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            string investor = InvestorList.SelectedItem.Text;
            bool isAllChecked = false;
            bool isAllInvestorChecked = false;
            E_InvestorProductDisabledStatus disableType = disableType_0.Checked ? E_InvestorProductDisabledStatus.DisabledTillNextLpeUpdate : E_InvestorProductDisabledStatus.Disabled;

            foreach (ListItem invEntry in this.InvestorList.Items)
            {
                // if you're selected, or all investors are selected, then populate the list with your products
                if (invEntry.Selected || isAllInvestorChecked)
                {
                    investor = invEntry.Text;
                    if (investor == "All")
                    {
                        // if "All" was selected, every investor will be checked
                        isAllInvestorChecked = true;
                        isAllChecked = true;
                    }
                    else
                    {
                        this.PopulateProductCodeList(investor);
                    }
                }

                // iterate through the selected disabled products
                foreach (ListItem entry in this.ProductCodeList.Items)
                {
                    if (entry.Selected || isAllChecked)
                    {
                        if (entry.Text == "All")
                        {
                            isAllChecked = true;
                        }
                        else
                        {
                            try
                            {
                                foreach (var lockPolicy in this.LockPolicies)
                                {
                                    InvestorProductManager.Remove(this.Broker.BrokerID, lockPolicy.LockPolicyId, investor, entry.Text);
                                    InvestorProductManager.Add(this.Broker.BrokerID, lockPolicy.LockPolicyId, investor, entry.Text, disableType, this.LoMessage.Text, this.pricingResult_Hide.Checked, this.Notes.Text, this.BrokerUser);
                                    Tools.LogInfo(string.Format("MRE: Product has been disabled.\r\nInvestor: {0}\r\nProduct Code: {1}\r\nPricing Result: {2}\r\nEnable Method: {3}\r\nWho: {4}\r\nWhen: {5}\r\nLockPolicy: {6} ({7})"
                                        , investor
                                        , entry.Text
                                        , this.pricingResult_Hide.Checked ? "Hidden" : "Red"
                                        , disableType == E_InvestorProductDisabledStatus.Disabled ? "Manual Enable" : "Auto Enable"
                                        , this.BrokerUser.LoginNm
                                        , DateTime.Now
                                        , lockPolicy.PolicyNm
                                        , lockPolicy.LockPolicyId));
                                }
                            }
                            catch (SqlException)
                            {
                                // this one is due to a primary key violation. The item is already in the database;
                            }
                            catch (Exception exc)
                            {
                                ErrorUtilities.DisplayErrorPage(exc, false, Guid.Empty, Guid.Empty);
                                ////"An error was found when trying to disable the selected product codes.");
                            }
                        }
                    }
                }
            }

            cModalDlg.CloseDialog(this.Page, "OK", "true");
        }

        protected void PopulateProductCodeList(string investor)
        {
            List<string> list = InvestorProductManager.ListAllProductCodeByInvestor(this.Broker, investor);
            this.ProductCodeList.Items.Clear();
            foreach (string str in list)
            {
                this.ProductCodeList.Items.Add(new ListItem(str));
            }
        }

        protected void InvestorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.InvestorList.Items[0].Text == "<-- Select Investor-->")
            {
                this.InvestorList.Items.RemoveAt(0);
            }

            ListItem selected = this.InvestorList.SelectedItem;
            if (selected != null)
            {
                try
                {
                    if (selected.Text == "All")
                    {
                        this.AddButton.Enabled = true;
                        this.ProductCodeList.Items.Clear();
                    }
                    else
                    {
                        this.AddButton.Enabled = false;
                        this.PopulateProductCodeList(selected.Text);
                        if (this.ProductCodeList.Items.Count > 1)
                        {
                            this.ProductCodeList.Items.Insert(0, new ListItem("All", "All"));
                        }
                    }
                }
                catch (Exception exc)
                {
                    ////Tools.LogError(exc);
                    ErrorUtilities.DisplayErrorPage(exc, false, Guid.Empty, Guid.Empty);
                    //// ErrorUtilities.DisplayErrorPage("The product codes related to the selected investor could not be retrieved.");
                }
            }
        }
    }
}
