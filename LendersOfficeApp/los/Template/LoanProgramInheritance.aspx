<%@ Page language="c#" Codebehind="LoanProgramInheritance.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.LoanProgramInheritance" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="CompositeUserControl" Src="../../Common/CompositeUserControl.ascx" %>
<%@ Register TagPrefix="ml" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html>
	<head>
		<title>Loan Program Inheritance</title>
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</head>
	<body MS_POSITIONING="FlowLayout" scroll="no" onload="onInit();">
		<script>
			function onInit()
			{
				if( document.getElementById("m_errorMessage") != null )
				{
					alert( document.getElementById("m_errorMessage").value );
				}

				<% if( !IsPostBack ) { %>

				resize( 760 , 480 );

				<% } %>
			}

			function onRulesClick( fileId , ProductName )
			{
				showModal( '/los/RatePrice/PricePolicyListByProduct.aspx?FileId=' + fileId + "&ProductName=" + escape( ProductName ),
					null, null, true);
			}

			function onViewClick( fileId )
			{
				showModal( '/los/Template/LoanProgramInheritance.aspx?fileId=' + fileId, null, null, null, null, { hideCloseButton: true } );
			}

			function onEditClick( fileId )
			{
				showModal( '/los/Template/LoanProgramTemplate.aspx?fileId=' + fileId );
			}
		</script>
		<h4 class="page-header">Loan Program Inheritance Tree</h4>
		<form id="LoanProgramInheritance" method="post" runat="server">
			<div style="HEIGHT: 334px; FONT: 12px arial; BACKGROUND: whitesmoke; OVERFLOW-Y: scroll; BORDER: 2px groove; PADDING: 8px; MARGIN: 8px;">
				<ml:CommonDataGrid id="m_Parents" runat="server">
					<Columns>
						<asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<span style="FONT: 12px arial; WIDTH: 20px;">
									<%# AspxTools.HtmlString(GetDerivationSymbol( Container.DataItem )) %>
								</span>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<a href="#" style="COLOR: tomato;" onclick="onEditClick( '<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "lLpTemplateId").ToString())%>' ); return false;">
									edit
								</a>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<%-- 7/26/07 - 17173 mf.  Both un-escaped single and double quotes in template names can cause unterminated string error for this link.  --%>
								<a href="#" style="COLOR: tomato;" onclick="onRulesClick( '<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "lLpTemplateId").ToString())%>' , '<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "lLpTemplateNm").ToString() ) %>' ); return false;">
									rules
								</a>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="Broker Name">
						    <ItemTemplate>
						        <%# AspxTools.HtmlString(DisplayBrokerName((Guid)Eval("BrokerId"))) %>
						    </ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="lLpPath" HeaderText="Product Path">
						</asp:BoundColumn>
						<asp:BoundColumn DataField="lLpTemplateNm" HeaderText="Program Name">
						</asp:BoundColumn>
						<asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
							<ItemTemplate>
								<a href="#" onclick="onViewClick( '<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "lLpTemplateId").ToString())%>' ); return false;" style="color: tomato;">
									view
								</a>
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</ml:CommonDataGrid>
				<asp:Panel id="m_ParentsDenied" runat="server">
					<div style="PADDING: 60px; TEXT-ALIGN: center; COLOR: red;">
						Access denied.  You do not have permission to view loan product derivation chains.
					</div>
				</asp:Panel>
				<asp:Panel id="m_ParentsEmpty" runat="server">
					<div style="PADDING: 40px; TEXT-ALIGN: center; COLOR: dimgray;">
						Nothing to show.  Loan program must not inherit from any others.
					</div>
				</asp:Panel>
				<div style="BORDER: 1px dotted lightgrey; BORDER-TOP: none; PADDING: 8px; MARGIN-TOP: 1px;">
					<uc:CompositeUserControl id="m_Derived" runat="server" Indent="24">
						<ItemTemplate>
							<div style="MARGIN-BOTTOM: 8px;" nowrap>
								<span>
									<span style="COLOR: dimgray;">
										&#x25b2;
									</span>
									<span style="COLOR: dimgray;">
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Broker" ).ToString()) %>
										&nbsp;(<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "CustomerCode" ).ToString()) %>)
									</span>
									::
									<span>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Name" ).ToString()) %>
									</span>
								</span>
								<span style="COLOR: tomato;" onmouseover="style.cursor = 'hand';" onmouseout="style.cursor = '';">
									(
									<a style="COLOR: tomato;" onclick="onEditClick( '<%# AspxTools.HtmlString(Container.DataItem) %>' );">
										edit
									</a>
									|
									<%-- 7/26/07 - 17173 mf.  Both un-escaped single and double quotes in template names can cause unterminated string error for this link.  --%>
									<a style="COLOR: tomato;" onclick="onRulesClick( '<%# AspxTools.HtmlString(Container.DataItem) %>' , '<%# AspxTools.HtmlString( DataBinder.Eval( Container.DataItem , "Name" ) ) %>' );">
										rules
									</a>
									|
									<a style="COLOR: tomato;" onclick="onViewClick( '<%# AspxTools.HtmlString(Container.DataItem) %>' );">
										view
									</a>
									)
								</span>
							</div>
						</ItemTemplate>
					</uc:CompositeUserControl>
					<asp:Panel id="m_DerivedEmpty" runat="server">
						<div style="PADDING: 20px; TEXT-ALIGN: center; COLOR: dimgray;">
							Nothing to show.  Nobody inherits from this loan program.
						</div>
					</asp:Panel>
				</div>
			</div>
			<div style="TEXT-ALIGN: center; MARGIN: 8px; PADDING: 0px; MARGIN-TOP: 20px;">
				<asp:Button id="m_Refresh" runat="server" Text="Refresh">
				</asp:Button>
				<input type="button" value="Close" onclick="onClosePopup();">
			</div>
			<ml:CModalDlg id="Modal" runat="server">
			</ml:CModalDlg>
		</form>
	</body>
</html>
