﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateDraft.aspx.cs" Inherits="LendersOfficeApp.los.Template.ClosingCost.CreateDraft" %>

<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Open a new Automation Draft</title>
    <style type="text/css">
        body { background-color: gainsboro; line-height: 1.6em ;}
        table { margin: 20px; }
        #layout_table ul { list-style: none; margin: 0; padding: 0; }
        #layout_table li select { margin-left: 20px; }
         #layout_table ul label, #layout_table ul input { vertical-align: middle; }
        tr.heightrow { height: 20px; }
        #Description { width:200px; }
    </style>
</head>
<body>
    <h4 class="page-header">Open a new Automation Draft</h4>
    <form id="form1" runat="server" onsubmit="return false">
    <input type="hidden" id="currid" />
    <div>        
        <table id="layout_table">
            <tbody>
                <tr>
                    <td><label> Draft Description </label> </td>
                    <td>&nbsp;<input type="text" id="Description" maxlength="200"  runat="server" />
                    <img alt="Description Is Required" runat="server" id="RequiredImage" />
                    </td>
                </tr>
                <tr class="heightrow">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">Starting data</td>
                    <td>
                        <ul>
                            <li runat="server" id="CurrentAutomatedtemplate"><label><input type="radio" value="current" name="DataSource" runat="server"/> Current Automated Closing Cost Templates</label></li>
                            <li runat="server" id="PriorAutomatedClosingCostTemplateVersion"><label><input type="radio" value="automated" name="DataSource"/> Prior Automated Closing Cost Templates version: <br />
                                <asp:DropDownList runat="server" ID="PreviousReleases"></asp:DropDownList> </label>
                            </li>
                            <li runat="server" id="CurrentManualClosingCostTemplates"><label><input type="radio" value="manual" name="DataSource"/> Current Manual Closing Cost Templates</label></li>
                            <li runat="server" id="NoData"><label><input type="radio" value="none" name="DataSource" /> None</label></li>
                        </ul>
                    </td>
                </tr>
                           <tr class="heightrow">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" align="center">         
                        <input type="button" value="  OK  " id="CreateBtn" /> <input type="button" value="Cancel" id="Cancel" /> 
                    </td>
                </tr>
        </tbody>
        </table>
    </div>
    <uc1:cModalDlg ID="CModalsDlg1" runat="server"></uc1:cModalDlg>
    
    </form>
    
    <script type="text/javascript">
        jQuery(function($) {
            var required = $('#RequiredImage'), desc = $('#Description'), ok = $('#CreateBtn');

            function validatePage(e) {
                if (e) {
                    var code = e.keyCode || e.which;
                    if (code && code == 13) {
                        ok.click();
                    }
                }
                if ($.trim(desc.val()).length > 0) {
                    ok.prop('disabled', false);
                    required.css('visibility', 'hidden');
                }
                else {
                    ok.prop('disabled', true);
                    required.css('visibility', 'visible');
                }
            }

            $('input[name=DataSource]').click(function() {
                if ($(this).closest('li').prop('id') == 'PriorAutomatedClosingCostTemplateVersion') {
                    $('#PreviousReleases').prop('disabled', false);
                }
                else {
                    $('#PreviousReleases').prop('disabled', true);
                }
            });

            $("input[name=DataSource]:first").prop('checked', true).triggerHandler('click');
            $('#Cancel').click(function() {
                onClosePopup();
            });
            desc.on('keyup change', validatePage).bind('cut paste', function () {
                window.setTimeout(validatePage, 0); //paste and cut happen before field changes
            }).focus();

            ok.on('click', function () {
                var data = {
                    description: desc.val(),
                    option: $("input[name=DataSource]:checked:first").val(),
                    previousid: $('#PreviousReleases').val()
                }

                if ($.trim(data.description).length == 0) {
                    return;
                }
                callWebMethodAsync({
                    url: 'SystemService.aspx?method=CreateDraft',
                    type: 'POST',
                    async: false,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(data),
                    success: function (e) {
                        var args = window.dialogArguments || {};
                        args.Reload = true;

                        onClosePopup(args);
                    },
                    error: function (e) {
                        var msg = e.UserMessage;
                        if (!msg) {
                            msg = JSON.parse(e.responseText).UserMessage;
                        }
                        alert('Error ' + msg);
                    }
                });
            });
            validatePage({});
            resize(540, 300);
        });
    </script>
    
</body>
</html>
