﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReleaseHistory.aspx.cs" Inherits="LendersOfficeApp.los.Template.ClosingCost.ReleaseHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Audit History</title>
    <style type="text/css">
        body { background-color: #DCDCDC; }
        .scrollcontent { padding-top: 5px; padding-left: 5px; overflow: auto; height: 300px;  }
        .center { text-align: center; padding: 5px; }
    </style>
</head>
<body>
    <h4 class="page-header">Audit History</h4>
    <form id="form1" runat="server">
    <div class="scrollcontent">
        <asp:GridView runat="server" ID="Audit" HeaderStyle-CssClass="GridHeader" AutoGenerateColumns="false" CellPadding="2" AlternatingRowStyle-CssClass="GridAlternatingItem" 
        RowStyle-CssClass="GridItem">
            <Columns>
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:BoundField DataField="DraftOpenedD" HeaderText="Draft opened date"  />
                <asp:BoundField DataField="DraftUserNm" HeaderText="Draft opened by" />
                <asp:BoundField DataField="ReleasedD" HeaderText="Draft released date" />
                <asp:BoundField DataField="ReleaseUserNm" HeaderText="Draft released by" />             
            </Columns>
        </asp:GridView>
    </div>
    <div class="center">
        <input type="button"  value="Close" id="Close" />
    </div>
    </form>
    
    <script type="text/javascript">
        jQuery(function($) {
        resize(600, 350);
            $("#Close").click(function(){onClosePopup();});
        });
        
        
    </script>
</body>
</html>
