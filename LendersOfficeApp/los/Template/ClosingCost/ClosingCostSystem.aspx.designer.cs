﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LendersOfficeApp.los.Template.ClosingCost {
    
    
    public partial class ClosingCostSystem {
        
        /// <summary>
        /// TabControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.common.BaseTabPage TabControl;
        
        /// <summary>
        /// ManualClosingCostList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.los.Template.ClosingCost.ManualClosingCostList ManualClosingCostList;
        
        /// <summary>
        /// AutomationProduction control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.los.Template.ClosingCost.AutomationProduction AutomationProduction;
        
        /// <summary>
        /// AutomationDraft control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.los.Template.ClosingCost.AutomationDraft AutomationDraft;
        
        /// <summary>
        /// CModalsDlg1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.common.ModalDlg.cModalDlg CModalsDlg1;
    }
}
