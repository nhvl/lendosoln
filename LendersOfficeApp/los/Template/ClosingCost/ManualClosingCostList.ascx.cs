﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using System.Web.UI.HtmlControls;
using System.Web.Services;

namespace LendersOfficeApp.los.Template.ClosingCost
{
    public partial class ManualClosingCostList : UserControl, IAutoLoadUserControl 
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region IAutoLoadUserControl Members

        public void LoadData()
        {
            ManualTemplates.DataSource = CCcTemplateData.RetrieveManualClosingCostTemplateInfo(PrincipalFactory.CurrentPrincipal);
            ManualTemplates.DataBind();
        }

        public void SaveData()
        {
        }

        #endregion

        protected void ManualTemplates_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            
            CCcTemplateInfo closingCostTemplate = args.Item.DataItem as CCcTemplateInfo;
            HtmlAnchor editLink = args.Item.FindControl("EditLink") as HtmlAnchor;
            HtmlAnchor duplicateLink = args.Item.FindControl("DuplicateLink") as HtmlAnchor;
            HtmlAnchor deleteLink = args.Item.FindControl("DeleteLink") as HtmlAnchor;
            Literal name = args.Item.FindControl("Name") as Literal;
            Literal version = args.Item.FindControl("Version") as Literal;

            if (closingCostTemplate.GfeVersion == "2008")
            {
                duplicateLink.Visible = false;
            }

            editLink.Attributes.Add("data-id", closingCostTemplate.Id.ToString("N"));
            editLink.Attributes.Add("data-version", closingCostTemplate.GfeVersion);
            duplicateLink.Attributes.Add("data-id", closingCostTemplate.Id.ToString("N"));
            deleteLink.Attributes.Add("data-id", closingCostTemplate.Id.ToString("N"));
            
            name.Text = closingCostTemplate.Name;
            version.Text = closingCostTemplate.GfeVersion;
        }
    }
}