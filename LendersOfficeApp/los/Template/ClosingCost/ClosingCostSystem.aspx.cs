﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using System.Web.Services;
using DataAccess;
using LendersOffice.Security;
using System.Web.UI.HtmlControls;
using DataAccess.ClosingCostAutomation;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.Template.ClosingCost
{
    public partial class ClosingCostSystem : BaseServicePage
    {
        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            IncludeStyleSheet("~/css/Tabs.css");
            RegisterJsScript("jquery.tablesorter.min.js");
            RegisterService("System","/los/Template/ClosingCost/SystemService.aspx");
            RegisterJsScript("LQBPopup.js");
            
            TabControl.IsInFrameset = false;
            ClosingCostAutomationStorage storage = new ClosingCostAutomationStorage(PrincipalFactory.CurrentPrincipal);

            
            if (PrincipalFactory.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            {
               var broker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
               if (broker.AutomatedClosingCost)
               {
                   TabControl.RegisterControl("Automation Production", AutomationProduction);
                   if (storage.HasDraft())
                   {
                       TabControl.RegisterControl("Automation Draft", AutomationDraft);
                   }
               }
               else
               {
                   AutomationDraft.Visible = false;
                   AutomationProduction.Visible = false;
               }
            }
            TabControl.RegisterControl("Manual", ManualClosingCostList);
            
        }

        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;
            base.OnInit(e);
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE8;
        }
    }
}
