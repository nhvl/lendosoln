﻿using System;
using System.Collections.Generic;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using DataAccess.ClosingCostAutomation;
using LendersOffice.ObjLib.TitleProvider;
using DataAccess.FeeService;
using DataAccess.Utilities;

namespace LendersOfficeApp.los.Template.ClosingCost
{
    public partial class SystemService : BaseSimpleServiceXmlPage
    {
        private ClosingCostAutomationStorage m_storage;
        protected override void Initialize()
        {
            m_storage = new ClosingCostAutomationStorage(PrincipalFactory.CurrentPrincipal);

            // sk 109250.  Note that Permission.CanAccessCCTemplates in UI is called "Allow editing closing cost templates"
        }
        protected override void Process(string methodName)
        {
            
            if(PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanAccessCCTemplates) == false)
            {
                if (methodName != "ApplyToLoan" && methodName != "UpdateClosingCostVendorId")    // allowing ApplyToLoan through, even if they can't EDIT cctemplates. 
                {
                    throw new AccessDenied();
                }
            }

            switch (methodName)
            {
                case "DuplicateTemplate":
                    DuplicateTemplate();
                    break;
                case "DeleteManualTemplate":
                    DeleteManualTemplate();
                    break;
                case "CreateDraft":
                    CreateDraft();
                    break;
                case "DiscardDraft":
                    DiscardDraft();
                    break;
                case "SaveClosingCostFieldsAndOrder":
                    SaveClosingCostFieldsAndOrder();
                    break;
                case "CacheData":
                    CacheData();
                    break;
                case "CreateBlankAutomationCCTemplate":
                    CreateBlankAutomatedClosingCostTemplate();
                    break;
                case "DeleteATemplate":
                    DeleteAutoTemplate();
                    break;
                case "Release":
                    ReleaseDraft();
                    break;
                case "ApplyToLoan":
                    ApplyTemplateToLoan();
                    return;
                case "CreateBlankTemplateForManual":
                    CreateBlankTEmplateForManual();
                    return;
                case "UpdateClosingCostVendorId":
                    UpdateClosingCostVendorId();
                    return;
                default:
                    break;
            }
        }

        protected void UpdateClosingCostVendorId()
        {
            if (PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowViewingAndEditingGFEFeeService) == false)
            {
                throw new AccessDenied(ErrorMessages.GenericAccessDenied, "User does not have AllowViewingAndEditingGFEFeeService permission");
            }

            int id = GetInt("ClosingCostId");
            if (id == -1)
            {
                SetResult("Disclaimer", "");
            }
            else
            {
                var service = TitleProvider.GetService(PrincipalFactory.CurrentPrincipal.BrokerId, id);
                SetResult("Disclaimer", service.ClosingCostDisclaimer);
            }

            bool isFeeService = GetBool("IsFeeService", false);
            if (isFeeService)
            {
                FeeServiceUtilities.RecordTitleVendorChange(PrincipalFactory.CurrentPrincipal, id);
            }

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            var brokerDb = principal.BrokerDB;
            brokerDb.ClosingCostTitleVendorId = id == -1 ? new int?() : id;
            brokerDb.Save();

            Tools.LogInfo($"BrokerDB.ClosingCostTitleVendorId value changed to {id}. BrokerId= {principal.BrokerId}. UserId= {principal.UserId}. LoginNm={principal.LoginNm}. IsFeeService={isFeeService}.");
        }

        protected void ApplyTemplateToLoan()
        {
            Guid slId = GetGuid("sLId");
            Guid ccId = GetGuid("ccid");

            ClosingCostTemplate.ApplyClosingCostTemplate(slId, ccId);
            SetResult("OK", true);
        }

        protected void ReleaseDraft()
        {
            CCTemplateSystem system =  m_storage.GetDraftAndTemplates();
            SetResult("Success", system.Release(PrincipalFactory.CurrentPrincipal));
        }

        protected void CreateBlankAutomatedClosingCostTemplate()
        {
            int expectedDraftId = GetInt("DraftId");
            CCTemplateSystem system = m_storage.GetDraftAndTemplates();
            if( system.Id != expectedDraftId )
            {
                SetResult("Status", "NewDraft");
                return;
            }
            Guid templateID = CCcTemplateBase.CreateAutomatedCCTemplate(PrincipalFactory.CurrentPrincipal.BrokerId, system.Id);
            SetResult("TemplateId", templateID.ToString("N"));
        }

        protected void CreateBlankTEmplateForManual()
        {
            Guid templateID = CCcTemplateBase.CreateCCTemplate(PrincipalFactory.CurrentPrincipal.BrokerId, (int)E_GfeVersion.Gfe2010);
            SetResult("TemplateId", templateID.ToString("N"));
        }


        protected void CacheData()
        {
            SetResult("key", AutoExpiredTextCache.AddToCache(GetString("Data"), TimeSpan.FromHours(4))); 
        }

        protected void DiscardDraft()
        {
            m_storage.DeleteDraft();
            SetResult("Status", "OK");
        }

        protected void SaveClosingCostFieldsAndOrder()
        {
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            int id = GetInt("id");

            List<string> fields = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(GetString("ids"));

            BrokerAutomationFields bauf = m_storage.GetAutomationConditionForDraft(id);
            bauf.SetAutomationFieldsWithListingOrder(fields);
            bool saved = m_storage.SaveDraftAutomationConditions(bauf);
            SetResult("Status", saved ? "OK" : "NewDraft");
        }

        protected void CreateDraft()
        {
            string description = GetString("description");
            string option = GetString("option");

            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            Guid userId = PrincipalFactory.CurrentPrincipal.UserId;

            switch (option)
            {
                case "current":
                    m_storage.CreateDraftFromCurrentRelease(description);
                    break;
                case "automated":
                    int id = GetInt("previousid");
                    m_storage.CreateDraftFrom(description, id);
                    break;
                case "manual":
                    m_storage.CreateDraftFromManualTemplates(description);
                    break;
                case "none":
                    m_storage.CreateEmptyDraft(description);
                    break;
                default:
                    break;
            }

            SetResult("Status", "OK");

        }

        private void DuplicateTemplate()
        {
            Guid ccid = GetGuid("ccid");
            int id = GetInt("DraftId", -1);
            var dupeId = id > -1 
                ? CCcTemplateBase.CreateAutomatedCCTemplate(BrokerUserPrincipal.CurrentPrincipal.BrokerId, id) 
                : CCcTemplateBase.DuplicateCCTemplate(BrokerUserPrincipal.CurrentPrincipal.BrokerId, 1, ccid);
            SetResult("NewId", dupeId.ToString("N"));
        }

        private void DeleteManualTemplate()
        {
            Guid ccid = GetGuid("ccid");
            CCcTemplateBase.DelCCTemplate(BrokerUserPrincipal.CurrentPrincipal.BrokerId, ccid);
            SetResult("Status", "OK");

        }

        private void DeleteAutoTemplate()
        {
            Guid ccid = GetGuid("ccid");
            int draftId = GetInt("DraftId");

            var deleted = m_storage.DeleteAutomatedDraftTemplate(ccid, draftId);
            SetResult("Status", deleted ? "OK" : "Fail");
        }
    }
}
