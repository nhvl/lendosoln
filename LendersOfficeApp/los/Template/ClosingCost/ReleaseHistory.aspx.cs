﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess.ClosingCostAutomation;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Template.ClosingCost
{
    public partial class ReleaseHistory : BasePage
    {
        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ClosingCostAutomationStorage storage = new ClosingCostAutomationStorage(PrincipalFactory.CurrentPrincipal);
            Audit.DataSource = storage.GetAuditEvents();
            Audit.DataBind();
        }

        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;
            base.OnInit(e);
        }
    }
}
