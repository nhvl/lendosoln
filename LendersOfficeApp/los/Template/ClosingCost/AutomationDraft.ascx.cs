﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using System.Web.UI.HtmlControls;
using DataAccess.ClosingCostAutomation;

namespace LendersOfficeApp.los.Template.ClosingCost
{
    public partial class AutomationDraft : UserControl, IAutoLoadUserControl 
    {
        #region IAutoLoadUserControl Members

        public void LoadData()
        {
            ClosingCostAutomationStorage storage = new ClosingCostAutomationStorage(PrincipalFactory.CurrentPrincipal);
            CCTemplateSystem system = storage.GetDraftAndTemplates(); 
            
            DraftData.Text = string.Format("{0} (opened {1} by {2})", system.Description, 
                system.DraftOpenedD, system.DraftOpenedByUserNm);

            SystemId.Value = system.Id.ToString();
            Templates.DataSource = system.Templates.OrderBy(p => p.Name);
            Templates.DataBind();
        }

        public void SaveData()
        {
            //no op
        }

        #endregion
        protected void Templates_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            CCcTemplateInfo info = args.Item.DataItem as CCcTemplateInfo;
            Literal templatename = args.Item.FindControl("templatename") as Literal;

            HtmlAnchor edit = args.Item.FindControl("edit") as HtmlAnchor;
            HtmlAnchor dupe = args.Item.FindControl("dupe") as HtmlAnchor;
            HtmlAnchor delete = args.Item.FindControl("delete") as HtmlAnchor;

            templatename.Text = info.Name;
            edit.Attributes.Add("data-id", info.Id.ToString("N"));
            dupe.Attributes.Add("data-id", info.Id.ToString("N"));
            delete.Attributes.Add("data-id", info.Id.ToString("N"));

//            edit.Attributes.Add("onclick", string.Format("showModal('/los/template/CCTemplateGFE2010.aspx?ccid={0}&draftid={1}');return false;", info.Id.ToString("N"), SystemId.Value));
        }
    }
}