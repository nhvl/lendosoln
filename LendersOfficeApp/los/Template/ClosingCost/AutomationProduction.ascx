﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutomationProduction.ascx.cs" Inherits="LendersOfficeApp.los.Template.ClosingCost.AutomationProduction" %>
<div id="prod_section">

<asp:PlaceHolder runat="server" ID="CurrentReleaseInfo">
    <div>
        Production release description: <ml:EncodedLiteral runat="server" ID="ReleaseDescription"></ml:EncodedLiteral> <a href="#" id="ViewHistory">history</a>
    </div>
</asp:PlaceHolder>

    <input type="button" value="Open Automation Draft..." id="OpenAutomationDraft"  /> <br />
    
    <ml:EncodedLabel runat="server" AssociatedControlID="ClosingCostTitleVendorId" Font-Bold=true>Obtain Escrow/Title/Recording/Transfer Tax From:</ml:EncodedLabel>
    <asp:DropDownList runat="server" CssClass="ccvendor" ID="ClosingCostTitleVendorId"></asp:DropDownList>
    <span id="disclaimer" runat="server" class="disclaimer" style="font-weight: bold; color: Red;" ></span>
    <div class="scrollable">
    <asp:Repeater runat="server" ID="TemplateList"  OnItemDataBound="TemplateList_OnItemDataBound">
        <HeaderTemplate><ul id="Listing"></HeaderTemplate>
        <FooterTemplate></ul></FooterTemplate>
        <ItemTemplate>
            <li>
                <a href="#" class="edit" runat="server" id="EditLink">edit</a>
                <span class="name"><ml:EncodedLiteral runat="server" ID="TemplateName"></ml:EncodedLiteral></span>
            </li>
        </ItemTemplate>
    </asp:Repeater>
    </div>
    
    <div id="history">
        
    </div>
</div>

<script type="text/javascript">
    $j(function() {
        var $listing = $j('#Listing');
        function restripify() {
            var alt = true;
            $listing.find('li').each(function(o) {
                var $li = $j(this);
                if (alt) {
                    $li.addClass('GridItem');
                }
                else {
                    $li.removeClass('GridItem');
                }
                alt = !alt;
            });
        }
        function updateName($row, args) {
            if (args && args.Name) {
                $row.children('span.name').text(args.Name);
            }
        }
        $listing.delegate('a.edit', 'click', function() {
            var $a = $j(this);
            var releaseid = $j('#ReleaseId').val();
            showModal("/los/template/CCtemplateGFE2010.aspx?auto=1&ccid=" + $a.attr('data-id') + "&RID=" + releaseid, null, null, null, function(args){ 
                updateName($a.parent(), args);
            });
            return false;
        });
        $j('#OpenAutomationDraft').click(function() {
            showModal("/los/template/ClosingCost/CreateDraft.aspx", null, null, null, function(args){ 
                if (args.Reload) {
                    window.location.href = window.location.href.replace("pg=1", "pg=2");
                }
            });

        });

        $j('#ViewHistory').click(function() {
            showModal("/los/template/ClosingCost/ReleaseHistory.aspx");
            return false;
        });
        restripify();
        
        $j('.ccvendor').change(function(){
            var val = this.value;
            var results = gService.System.call('UpdateClosingCostVendorId', { ClosingCostId : val });
            
            if ( results.error ){
                alert('Could not update title vendor. \nError:' + results.UserMessage);
            }
            else {
                if (results.value.Disclaimer){
                    $j('span.disclaimer').html('*' + results.value.Disclaimer);
                }
                else {
                    $j('span.disclaimer').empty();
                
                }
            }
        });
    });
</script>
