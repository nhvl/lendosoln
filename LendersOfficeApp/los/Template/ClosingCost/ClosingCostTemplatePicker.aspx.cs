﻿using System;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using System.Web.UI.HtmlControls;
using DataAccess.ClosingCostAutomation;
using LendersOffice.Security;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.Template.ClosingCost
{
    public partial class ClosingCostTemplatePicker : BaseServicePage
    {
        private Guid LoanID { get; set; }

        protected override bool EnableJQueryInitCode => true;

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("jquery.tablesorter.min.js");

            LoanID = RequestHelper.GetGuid("sLId", Guid.Empty);
            if (LoanID != Guid.Empty)
            {
                ClientScript.RegisterHiddenField("sLId", LoanID.ToString("N"));
            }

            ClosingCostAutomationStorage storage = new ClosingCostAutomationStorage(PrincipalFactory.CurrentPrincipal);
            ClosingCostTemplateTable.DataSource = storage.GetAllTemplateDetails();
            ClosingCostTemplateTable.DataBind();
        }

        protected void ClosingCostList_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem &&
                args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            CCTemplateDetails details = (CCTemplateDetails)args.Item.DataItem;

            Literal templateName = args.Item.FindControl("TemplateName") as Literal;
            Literal templateType = args.Item.FindControl("TemplateType") as Literal;
            HtmlAnchor copyLink = args.Item.FindControl("Picker") as HtmlAnchor;

            if (LoanID != Guid.Empty)  //it is being launched to apply to a loan.
            {
                copyLink.InnerText = "apply";
                copyLink.Attributes.Add("class", "apply");
                copyLink.Title = "Apply Template";
            }

            templateName.Text = details.Name;
            templateType.Text = details.TemplateType;

            copyLink.Attributes.Add("data-id", details.Id.ToString("N"));
        }

        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            base.OnInit(e);
        }
    }
}
