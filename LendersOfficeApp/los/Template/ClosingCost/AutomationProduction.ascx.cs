﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using DataAccess.ClosingCostAutomation;
using System.Web.UI.HtmlControls;
using LendersOffice.ObjLib.TitleProvider;

namespace LendersOfficeApp.los.Template.ClosingCost
{
    public partial class AutomationProduction : UserControl, IAutoLoadUserControl 
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private int ReleaseId { get; set; }

        #region IAutoLoadUserControl Members
        public void LoadData()
        {
            ClosingCostAutomationStorage storage = new ClosingCostAutomationStorage(PrincipalFactory.CurrentPrincipal);
            CCTemplateSystem system = storage.GetReleaseVersionAndTemplates();
            if (system == null)
            {
                CurrentReleaseInfo.Visible = false;
                TemplateList.Visible = false;
            }
            else
            {
                Page.ClientScript.RegisterHiddenField("ReleaseId", system.Id.ToString());
                ReleaseDescription.Text = string.Format("{0} (released {1} by {2})", system.Description, system.ReleasedD, system.ReleasedByUserFullName);
                TemplateList.DataSource = system.Templates.OrderBy(t => t.Name);
                TemplateList.DataBind();
            }


            BindClosingCostDDL(ClosingCostTitleVendorId);
        }

        public void SaveData()
        {
        }
        #endregion

        public void TemplateList_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            CCcTemplateInfo info = (CCcTemplateInfo)args.Item.DataItem;
            HtmlAnchor editLink = args.Item.FindControl("EditLink") as HtmlAnchor;
            Literal templateName = args.Item.FindControl("TemplateName") as Literal;
            templateName.Text = info.Name;

            editLink.Attributes.Add("data-id", info.Id.ToString("N"));

        }

        private void BindClosingCostDDL(DropDownList ddl)
        {
            var associations = TitleProvider.GetAssociations(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);

            ddl.Items.Add(new ListItem("Closing Cost Templates", "-1"));

            foreach (var association in associations)
            {
                ddl.Items.Add(new ListItem(association.VendorName, association.VendorId.ToString()));
            }

            if (db.ClosingCostTitleVendorId.HasValue)
            {
                Tools.SetDropDownListValue(ddl, db.ClosingCostTitleVendorId.Value);
                BrokerVendorAssociation association = associations.Where(p => p.VendorId == db.ClosingCostTitleVendorId.Value).First();
                TitleService service = TitleService.GetServiceFor(association);
                disclaimer.InnerText = service.ClosingCostDisclaimer;
            }
            else
            {
                ddl.Items[0].Selected = true;
            }
        }
    }
}