﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectAutomationFields.aspx.cs" Inherits="LendersOfficeApp.los.Template.ClosingCost.SelectAutomationFields" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Closing Cost Template Automation Fields</title>
       <style type="text/css">
        body { background-color: gainsboro; line-height: 1.6em ;}
        #AvailableFields, #SelectedFields, #AvailableFieldsHeader, #SelectedFieldsHeader
        {
            float: left;
            width: 300px; 
            margin-left: 5px;

        }
        #AvailableFieldsHeader, #SelectedFieldsHeader 
        {
            font-weight: bold;
        }
        #AvailableFields, #SelectedFields 
        {
            height: 300px; 
            overflow: auto; 
            border: 1px solid black; 
            background-color: White;
        }
        #AvailableFields a.up, #AvailableFields a.down 
        {
            display:none; 
            height: 15px;
            
        }
        a.up, a.down 
        {
            text-decoration: none;
        }
        a.up img, a.down img
        {
            height: 9px;
        }
        #AvailableFields ul, #SelectedFields  ul
        {
            list-style: none;
            padding: 0 0 0 10px; 
        }
        #buttons { 
            clear: both;
            text-align: center;
            padding-top: 10px;
         }
         
         #buttons input { 
            margin: 0 auto; 
         }
    </style>
</head>
<body>
    <h4 class="page-header">Closing Cost Template Automation Editor</h4>
    <form id="form1" runat="server">
        <asp:HiddenField runat="server" ID="SystemId" />
        <div id="AvailableFieldsHeader">Available Fields</div>
        <div id="SelectedFieldsHeader">Fields needed to determine which template to use</div> 
        <br />
        <div id="AvailableFields">
            <asp:Repeater runat="server" ID="FieldChoices" OnItemDataBound="FieldRepeater_OnItemDataBound">
                <HeaderTemplate><ul></HeaderTemplate>
                <FooterTemplate></ul></FooterTemplate>
                <ItemTemplate>
                    <li>
                        <a href="#" class="up"><asp:Image runat="server" ImageUrl="~/images/up-blue.gif" /> </a>
                        <a href="#" class="down"><asp:Image runat="server" ImageUrl="~/images/down-blue.gif" /> </a>
                        <a href="#" runat="server" class="pick" id="IncludeUrl">include</a>
                        <ml:EncodedLiteral runat="server" ID="FieldName"></ml:EncodedLiteral>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        
        <div id="SelectedFields">
            
            <asp:Repeater runat="server" ID="IncludedFields" OnItemDataBound="FieldRepeater_OnItemDataBound">
                <HeaderTemplate><ul></HeaderTemplate>
                <FooterTemplate></ul></FooterTemplate>
                <ItemTemplate>
                    <li>
                        <a href="#" class="up"><asp:Image runat="server" ImageUrl="~/images/up-blue.gif" /> </a>
                        <a href="#" class="down"><asp:Image runat="server" ImageUrl="~/images/down-blue.gif" /> </a>
                        <a href="#" runat="server" class="pick" id="IncludeUrl">remove</a>
                        <ml:EncodedLiteral runat="server" ID="FieldName"></ml:EncodedLiteral>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div id="buttons">
            <input type="button" id="Save" value=" OK " />
            <input type="button" id="Cancel" value="Cancel" />
        </div>
        
    </form>
    <script type="text/javascript">
        jQuery(function($) {
            var $availableFieldsUl = $('#AvailableFields ul'), $selectedFieldsUl = $('#SelectedFields ul');
            function moveLI(anchor, $dest, anchorText) {
                var $this = $(anchor), $li = $this.parents('li');
                $this.text(anchorText);
                $li.clone().appendTo($dest);
                $li.remove();
                return false;
            }
            
            $availableFieldsUl.on('click', 'a.pick', function () {
                return moveLI(this, $selectedFieldsUl, 'remove');
            });

            $selectedFieldsUl.on('click', 'a.pick', function () {
                return moveLI(this, $availableFieldsUl, 'include');
            });

            $selectedFieldsUl.on('click', 'a.up', function () {
                var $this = $(this), $lis = $selectedFieldsUl.children('li'), $li = $this.parents('li'),
                index = $lis.index($li), $removedNode = $li.remove();
                if (index == 0) {
                    $selectedFieldsUl.append($removedNode);
                }
                else {
                    $lis.eq(index - 1).before($removedNode);
                }
                return false;
            });


            $selectedFieldsUl.on('click', 'a.down', function () {
                var $this = $(this), $lis = $selectedFieldsUl.children('li'), $li = $this.parents('li'),
                index = $lis.index($li), $removedNode = $li.remove();
                if (index + 1 == $lis.length) {
                    $selectedFieldsUl.prepend($removedNode);
                }
                else {
                    $lis.eq(index + 1).after($removedNode);
                }
                return false;
            });

            $('#Cancel').click(function() {
                onClosePopup();
            });

            $('#Save').click(function() {
                var $selectedAnchors = $selectedFieldsUl.find('li a.pick'), ids = [];
                $selectedAnchors.each(function() {
                    ids.push($(this).attr('data-id'));
                });

                var data = { ids: JSON.stringify(ids), id: $('#SystemId').val() };
                callWebMethodAsync({
                    url: 'SystemService.aspx?method=SaveClosingCostFieldsAndOrder',
                    type: 'POST',
                    async: false,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(data),
                    success: function(e) {
                        result = e.d;
                        if (e.d.Status != 'OK') {
                            alert('Could not save fields. Draft has changed. Please reload.');
                        }
                        
                        onClosePopup();
                    },
                    error: function(e) {
                    var msg = e.UserMessage;
                    if (!msg) {
                        msg = JSON.parse(e.responseText).UserMessage;
                    }
                    alert('Error ' + msg);
                    }
                });
            });

            resize(635, 425);
        });
    </script>
</body>
</html>
