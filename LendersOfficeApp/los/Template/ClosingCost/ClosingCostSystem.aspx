﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClosingCostSystem.aspx.cs" Inherits="LendersOfficeApp.los.Template.ClosingCost.ClosingCostSystem" EnableViewState="false" %>
<%@ Register TagPrefix="uc1" TagName="Tabs" Src="~/common/BaseTabPage.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ManualClosingCostList" Src="~/los/Template/ClosingCost/ManualClosingCostList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AutomaticDraft" Src="~/los/Template/ClosingCost/AutomationDraft.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AutomationProduction" Src="~/los/Template/ClosingCost/AutomationProduction.ascx"  %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Closing Cost Template List</title>
    <style type="text/css">
        body 
        {
            background-color: gainsboro;
        }
        div.fixed_height 
        {
            height: 400px; 
            overflow-y: scroll;
        }
        #ManualTemplates
        {
            width: 98%;
            table-layout: fixed;
        }
        #draft_section ul, #prod_section ul 
        {
            list-style-type: none;
            margin: 5px 0 0 0;
            padding: 0;
        }
        #draft_section ul li, #prod_section ul li 
        {
            line-height: 1.6em;
        }
        #draft_section ul li a, #prod_section ul li a
        {
            margin-right: 10px; 
        }
        #draft_section input { 
            width: 180px; 
        }
        
        div.content 
        {
            margin: 20px; 
        }
        
        .scrollable 
        {
            overflow: auto;
            height: 400px;
        }
    </style>
</head>
<body>
    <h4 class="page-header">Closing Cost Template List</h4>
    <form runat="server">
    <div class="Tabs">
        <uc1:Tabs runat="server" ID="TabControl" />
    </div>
    <div class="content">
    <uc1:ManualClosingCostList runat="server" ID="ManualClosingCostList"></uc1:ManualClosingCostList>
    <uc1:AutomationProduction runat="server" ID="AutomationProduction" />
    <uc1:AutomaticDraft runat="server" ID="AutomationDraft" Visible="false" />
    <br />
    <input type="button" value="Close" id="closeBtn" />
    <uc1:cModalDlg ID="CModalsDlg1" runat="server"></uc1:cModalDlg>
    </div>
    </form>
    
    <script type="text/javascript" >
        jQuery(function($) {
            resize(700, 580);
            $('#closeBtn').click(function(){onClosePopup();});
        });
    </script>
</body>
</html>
