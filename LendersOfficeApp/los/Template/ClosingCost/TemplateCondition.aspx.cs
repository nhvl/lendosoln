﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using CommonProjectLib.Common.Lib;
using MeridianLink.CommonControls;
using System.Web.UI.HtmlControls;
using DataAccess.ClosingCostAutomation;

namespace LendersOfficeApp.los.Template.ClosingCost
{
    public partial class TemplateCondition : BasePage
    {
        protected int FieldCount { get; private set; }
        protected Dictionary<string, BareboneSelectedAutomationValues> SelectedValues { get; set; }

        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            TemplateName.Text = RequestHelper.GetSafeQueryString("nm");

            string key = RequestHelper.GetSafeQueryString("k");

            if (!string.IsNullOrEmpty(key)) 
            {
                string data = AutoExpiredTextCache.GetFromCache(key);
                SelectedValues = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<BareboneSelectedAutomationValues>>(data).ToDictionary(p => p.FieldId);
            }

            int draftId = RequestHelper.GetInt("Id");
            Guid templateId = RequestHelper.GetGuid("TemplateId");

            ClosingCostAutomationStorage storage = new ClosingCostAutomationStorage(PrincipalFactory.CurrentPrincipal);
            BrokerAutomationFields bauf = storage.GetAutomationConditionForDraft(draftId);

            SelectedAutomationField[] automationFields = bauf.GetConditionsFor(templateId).ToArray();

            FieldCount = automationFields.Length;
            PossibleValues.DataSource = automationFields;
            PossibleValues.DataBind();
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            HtmlMeta meta = new HtmlMeta();
            meta.Attributes.Add("http-equiv", "X-UA-Compatible");
            meta.Attributes.Add("content", "IE=8");
            Header.Controls.AddAt(0, meta);
        }

        protected void Values_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }
            SelectedAutomationField v = args.Item.DataItem as SelectedAutomationField;
            Literal fieldName = args.Item.FindControl("FieldName") as Literal;
            HtmlContainerControl combinationText = args.Item.FindControl("CombinationText") as HtmlContainerControl;
            ListBox valuesList = args.Item.FindControl("Values") as ListBox;
            combinationText.Visible = !(args.Item.ItemIndex + 1 == FieldCount);
            fieldName.Text = v.Name;
            valuesList.Attributes.Add("data-id", v.FieldId);
            valuesList.Attributes.Add("data-rep", v.Name);


            AutomationFieldValue[] values = v.Values.OrderBy(val => val.Rep).ToArray();

            if (SelectedValues != null )
            {
                if (SelectedValues.ContainsKey(v.FieldId))
                {
                    BareboneSelectedAutomationValues cacheValue = SelectedValues[v.FieldId];
                    foreach (AutomationFieldValue af in values)
                    {
                        af.IsSelected = cacheValue.Values.Contains(af.Value); //restore cache values if they exist.
                    }
                }
                else
                {
                    foreach (AutomationFieldValue af in values)
                    {
                        af.IsSelected = false; //the chose all
                    }
                }
            }
            
            valuesList.DataSource = values;
            valuesList.DataTextField = "Rep";
            valuesList.DataValueField = "Value";
            valuesList.DataBind();




            for (int x = 0; x < values.Length; x++)
            {
                if (values[x].IsSelected)
                {
                    valuesList.Items[x].Selected = true;
                }
            }

            valuesList.Items.Insert(0,new ListItem()
            {
                Text = String.Format("<-- All {0}s -->", v.Name),
                Value = "-999"
            });

            if (valuesList.SelectedIndex < 0)
            {
                valuesList.SelectedIndex = 0;
            }
        }

    }
}
