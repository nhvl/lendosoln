﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Auditor.aspx.cs" Inherits="LendersOfficeApp.los.Template.ClosingCost.Auditor" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Closing Cost Automation Auditor</title>
    <style type="text/css">
        body { background-color: #DCDCDC; }
        .scrollcontent {  }
        .center { text-align: center; padding: 5px; }
        h2{ font-size: 8pt;  padding: 0; margin: 4px;}
        table.summaries, ul.messages { margin-bottom: 10px; overflow: auto; margin-left: 4px; }
        ul.messages { list-style-type: none;  padding: 0; margin-left: 0; }
        ul.messages li { line-height: 1.5 }
        
        .content { padding-top: 5px; padding-left: 5px;  }
        #Release { width: 120px; }
        
    </style>
</head>
<body>
    <h4 class="page-header">Release Closing Cost Automation Draft to Production</h4>
    <form id="form1" runat="server">   
        <div class="content" runat="server" id="MainResults">
            
            <ul runat="server" id="AuditResults" class="messages">
                <li runat="server" id="FailureMsg">Error: There are conflicts in Closing Cost Template conditions. Please see below for details.</li>
                <li runat="server" id="FailureMsg2"></li>
                <li runat="server" id="TemplatesWithNoScenarioError">Error: There are templates with no scenario. Please see below for details.</li>
                <li runat="server" id="WarningMsg">Warning: There are scenarios that do not  have closing cost Templates assigned to them. Please see below for details.</li>
            </ul>
            
            <div runat="server" id="ReleaseSection" class="center">
                <input type="button" value="Release" id="Release" runat="server" />
            </div>
            
            <div class="scrollcontent">
                <h2>Closing Cost Template Automation Summary</h2>
                <asp:GridView CssClass="summaries" runat="server" ID="TempalteConditionSummary" HeaderStyle-CssClass="GridHeader" 
                    AlternatingRowStyle-CssClass="GridAlternatingItem"  RowStyle-CssClass="GridItem"
                     OnRowCreated="FixHeaders_OnRowCreated"></asp:GridView>

                <asp:PlaceHolder runat="server" ID="ConflictingSummaryPlaceholder">
                    <h2>Conflicting Templates</h2>
                    <asp:GridView  CssClass="summaries" runat="server" ID="ConflictSummary" HeaderStyle-CssClass="GridHeader" 
                        AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem"
                         OnRowCreated="FixHeaders_OnRowCreated"></asp:GridView>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder runat="server" ID="MissingTemplateSummaryPlaceHolder">
                    <h2>Templates with no Scenario</h2>
                     <asp:GridView  ShowHeader="false" CssClass="summaries" runat="server" ID="MissingTemplateSummary" HeaderStyle-CssClass="GridHeader" 
                        AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem"></asp:GridView>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder runat="server" ID="MissingScenarioPlaceHolder">
                    <h2>Missing Scenarios</h2>
                    <asp:GridView runat="server" ID="MissingConditionSummary" HeaderStyle-CssClass="GridHeader" 
                        AlternatingRowStyle-CssClass="GridAlternatingItem" RowStyle-CssClass="GridItem"
                         OnRowCreated="FixHeaders_OnRowCreated"></asp:GridView>
                </asp:PlaceHolder>
                
            </div>
        </div>
            
        <div class="content" id="NoScenarios" runat="server" visible="false"> 
            There are no scenarios.
        </div>
    </form>
    
    <script type="text/javascript">
        jQuery(function($) {
            resize(600, 400);


            function service(method, data) {
                var result = null;
                callWebMethodAsync({
                    url: 'SystemService.aspx?method=' + method,
                    type: 'POST',
                    async: false,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(data),
                    success: function(e) {
                        result = e.d;
                    },
                    error: function(e) {
                        var msg = e.UserMessage;
                        if (!msg) {
                            msg = JSON.parse(e.responseText).UserMessage;
                        }
                        alert('Error ' + msg);
                    }
                });
                return result
            }



            $('#Release').click(function() {
                var args = service("Release", {});
                if (args && args.Success == 'True') {
                    var returnArgs = new Object();
                    returnArgs.Reload = true;
                    onClosePopup(returnArgs);
                }
            });
        });
    </script>
</body>
</html>
