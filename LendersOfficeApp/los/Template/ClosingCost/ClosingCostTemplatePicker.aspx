﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClosingCostTemplatePicker.aspx.cs" EnableViewState="false" Inherits="LendersOfficeApp.los.Template.ClosingCost.ClosingCostTemplatePicker" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Closing Cost Template Chooser</title>
    <style type="text/css">
        body { background-color: #DCDCDC; }
        .scrollcontent { padding-top: 5px; padding-left: 5px; overflow: auto; height: 300px;  }
        .center { text-align: center; padding: 5px; }
    </style>
</head>
<body>
    <h4 class="page-header">Closing Cost Template List</h4>
    <form id="form1" runat="server">
    <div class="scrollcontent">
        
        <asp:Repeater runat="server" ID="ClosingCostTemplateTable" OnItemDataBound="ClosingCostList_OnItemDataBound">
            <HeaderTemplate>
                <table cellpadding="0" cellspacing="0" id="ClosingCostList" width="500">
                    <thead>
                        <tr  class="GridHeader">
                            <th>Closing Cost Template Name</th>
                            <th>Type</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
            <ItemTemplate>
                <tr class="GridItem">
                    <td><ml:EncodedLiteral runat="server" ID="TemplateName"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="TemplateType"></ml:EncodedLiteral></td>
                    <td><a href="#" title="Copy Template" runat="server" class="copy-a" id="Picker">copy</a></td>
                </tr>
            </ItemTemplate>
            
            <AlternatingItemTemplate>
                <tr class="GridAlternatingItem">
                    <td><ml:EncodedLiteral runat="server" ID="TemplateName"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="TemplateType"></ml:EncodedLiteral></td>
                    <td><a href="#" title="Copy Template" runat="server" class="copy-a" id="Picker">copy</a></td>
                </tr>
            </AlternatingItemTemplate>

        </asp:Repeater>
        
    </div>
    <div class="center">
        <input type="button" value="Close"  id="Close"/>
    </div>
    </form>
    <script type="text/javascript">
        jQuery(function($) {
            function service(method, data) {
                var result = null;
                callWebMethodAsync({
                    url: 'SystemService.aspx?method=' + method,
                    type: 'POST',
                    async: false,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(data),
                    success: function(e) {
                        result = e.d;
                    },
                    error: function(e) {
                        alert('Error ' + JSON.parse(e.responseText).UserMessage);
                    }
                });
                return result
            }
            var $table = $('#ClosingCostList');

            $('#Close').click(function() {
                onClosePopup();
            });

            function recolorTable() {
                var isAlt = 0;
                $table.find('tbody tr').each(function() {
                    $(this).toggleClass('GridItem GridAlternatingItem');
                });
            }

            $table.tablesorter();

            $table.on('sortEnd', recolorTable);
            $table.on("click", "a.copy-a", function () {
                var args = {};
                args.Id = $(this).attr('data-id');
                onClosePopup(args);
            });
            $table.on('click', 'a.apply', function () {
                var result = service("ApplyToLoan", { sLId: $('#sLId').val(), ccid: $(this).attr('data-id') });
                if (result && result.OK) {
                    var args = {};
                    args.OK = true;
                    onClosePopup(args);
                }

                return false;
            });

            resize(540,390);
        });
    </script>
</body>
</html>
