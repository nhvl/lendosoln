﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AutomationDraft.ascx.cs" Inherits="LendersOfficeApp.los.Template.ClosingCost.AutomationDraft" %>
<asp:HiddenField runat="server" ID="SystemId" />
<div>
    Draft data: <ml:EncodedLiteral runat="server" ID="DraftData"></ml:EncodedLiteral>
</div>
<div id="draft_section">
<input type="button" value="Edit Automation Fields" id="EditFields" />
<input type="button" value="Add New Closing Cost Template" id="AddNewTemplate" />
<input type="button" value="Copy Existing Template" id="DuplicateTemplate" />
<br />
<div class="scrollable">
    <asp:Repeater runat="server" ID="Templates" OnItemDataBound="Templates_OnItemDataBound">
        <HeaderTemplate>
            <ul id="Listing">
        </HeaderTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
        <ItemTemplate>
            <li>
                <a href="#" runat="server" class="edit" id="edit">edit</a>
                <a href="#" runat="server" class="dupe" id="dupe">duplicate</a>
                <a href="#" runat="server" class="delete" id="delete">delete</a>
                <span class="name">
                    <ml:EncodedLiteral runat="server" id="templatename"></ml:EncodedLiteral>
                </span>
            </li>
        </ItemTemplate>
    </asp:Repeater>
</div>
<br />
<input type="button" value="Audit Closing Cost Templates" id="RunAudit" />
<input type="button" value="Release Draft to Production..." id="ReleaseToProduction" />
<input type="button" value="Discard Draft..." id="DiscardDraft"  />
</div>

<script type="text/javascript">
    $j(function() {
        var id = $j('#AutomationDraft_SystemId').val(), $listing = $j('#Listing'),
            isalt = $listing.children('li').length % 2 == 1;

        function restripify() {
            var alt = true;
            $listing.find('li').each(function(o) {
                var $li = $j(this);
                if (alt) {
                    $li.addClass('GridItem');
                }
                else {
                    $li.removeClass('GridItem');
                }
                alt = !alt;
            });
        }

        function updateName($row, args) {
            if (args && args.Name) {
                $row.children('span.name').text(args.Name);
            }
        }

        function generateItemHtml(ccid, name) {
            var cssClass = isalt ? "GridAlternatingItem" : "GridItem";
            isalt = !isalt;
            var h = hypescriptDom;
            return h("li", {}, [
                h("a", {href:"#", className:"edit"  , attrs:{"data-id":ccid}}, "edit"     ), " ",
                h("a", {href:"#", className:"dupe"  , attrs:{"data-id":ccid}}, "duplicate"), " ",
                h("a", {href:"#", className:"delete", attrs:{"data-id":ccid}}, "delete"   ), " ",
                h("span", {className:"name"}, name)
            ]);
        }



        function insertRow(id, args) {
            var name = '';
            if (args && args.Name) {
                name = args.Name;
            }
            $listing.append(generateItemHtml(id, name));
            restripify();
        }

        function service(method, data) {
            var result = null;
            callWebMethodAsync({
                url: 'SystemService.aspx?method=' + method,
                type: 'POST',
                async: false,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function(e) {
                    result = e.d;
                },
                error: function(e) {
                    var msg = e.UserMessage;
                    if (!msg) {
                        msg = JSON.parse(e.responseText).UserMessage;
                    }
                    alert('Error ' + msg);
                }
            });
            return result
        }


        $listing.delegate('a.edit', 'click', function() {
            var $a = $j(this);
            showModal("/los/template/CCtemplateGFE2010.aspx?auto=1&ccid=" + $a.attr('data-id') + "&draftid=" + id, null, null, null, function(args){
                updateName($a.parent(), args);
                return false;
            });
        });

        $listing.delegate('a.dupe', 'click', function() {
            var oldid = $j(this).attr('data-id');
            var results = service("DuplicateTemplate", { ccid: oldid, DraftId: id });
            if (results.NewId) {
                showModal("/los/template/CCtemplateGFE2010.aspx?auto=1&ccid=" + oldid + "&ccid2=" + results.NewId + "&draftid=" + id, null, null, null, function(resultArgs){
                    insertRow(results.NewId, resultArgs);
                });
            }
            return false;
        });

        $listing.delegate('a.delete', 'click', function() {
            var $a = $j(this), oldid = $j(this).attr('data-id'),
                results = service("DeleteATemplate", { ccid: oldid, DraftId: id });

            if (!results) {
                return false;
            }
            if (results.Status === 'OK') {
                $a.parent().remove();
                restripify();
            }
            else {
                alert('Template could not be deleted.'); //maybe reload?
            }
            return false;
        });


        $j('#DiscardDraft').click(function() {
            if (confirm('Discarding a draft cannot be undone. Are you sure you want to discard this draft?')) {
                var result = service('DiscardDraft', {});
                if (result != null) {
                    window.location.href = window.location.href.replace("pg=2", "pg=1");
                }
            }
        });

        $j('#EditFields').click(function() {
            showModal("/los/template/ClosingCost/SelectAutomationFields.aspx?id=" + id, null, null, null, function(args){
                updateName(args);
            });
        });

        $j('#AddNewTemplate').click(function() {
            var results = service("CreateBlankAutomationCCTemplate", { DraftId: id });
            if (results) {
                var url = ['/los/template/CCTemplateGFE2010.aspx?auto=1&ccid=', results.TemplateId, '&draftid=', id];
                showModal(url.join(''), null, null, null, function(args){
                    insertRow(results.TemplateId, args);
                });
            }
        });

        $j('#DuplicateTemplate').click(function() {
            showModal('/los/template/ClosingCost/ClosingCostTemplatePicker.aspx', null, null, null, function(args){
                if (args && args.Id) {
                    var results = service("DuplicateTemplate", { ccid: args.Id, DraftId: id });
                    if (results.NewId) {
                        insertRow(results.NewId, results2);
                    }
                }
            });
        });

        $j('#RunAudit').click(function() {
            showModal('/los/template/ClosingCost/Auditor.aspx');
        });

        $j('#ReleaseToProduction').click(function() {
            showModal('/los/template/ClosingCost/Auditor.aspx?r=1', null, null, null, function(args){
                if (args && args.Reload) {
                    window.location.href = window.location.href.replace("pg=1", "pg=0");
                }
            });
        });

        restripify();
    });
</script>
