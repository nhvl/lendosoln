﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using CommonProjectLib.Common.Lib;
using System.Web.UI.HtmlControls;
using DataAccess;
using System.IO;
using System.Xml;
using LendersOffice.Security;
using DataAccess.ClosingCostAutomation;

namespace LendersOfficeApp.los.Template.ClosingCost
{
    public partial class SelectAutomationFields : BasePage
    {
        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            ClosingCostAutomationStorage storage = new ClosingCostAutomationStorage(PrincipalFactory.CurrentPrincipal);
            BrokerAutomationFields bauf = storage.GetAutomationConditionForDraft(RequestHelper.GetInt("id"));

                ////we have a new draft. 
                //ClientScript.RegisterStartupScript(typeof(SelectAutomationFields), "closeme" ,"alert('There is a new draft. Please reopen the tab.'); onClosePopup();", true);
                //return;

            FieldChoices.DataSource = bauf.GetAutomationFieldsUnused();
            FieldChoices.DataBind();

            IncludedFields.DataSource =  bauf.GetAutomationFieldsUsed();
            IncludedFields.DataBind();

            SystemId.Value = bauf.DraftVersion.ToString();

        }


        protected void FieldRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }


            AutomationField item = (AutomationField)args.Item.DataItem;
            HtmlAnchor includeUrl = args.Item.FindControl("IncludeUrl") as HtmlAnchor;
            Literal fieldName = args.Item.FindControl("FieldName") as Literal;

            includeUrl.Attributes.Add("data-id", item.FieldId);
            fieldName.Text = item.Name;
        }

        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;
            base.OnInit(e);
        }
    }
}
