﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using System.Web.UI.HtmlControls;
using DataAccess.ClosingCostAutomation;

namespace LendersOfficeApp.los.Template.ClosingCost
{
    public partial class CreateDraft : BasePage
    {
        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

       

        protected void Page_Load(object sender, EventArgs e)
        {

            ClosingCostAutomationStorage storage = new ClosingCostAutomationStorage(PrincipalFactory.CurrentPrincipal);

            CCTemplateSystem releaseVersion = storage.GetReleaseVersion();
            bool hasManualTemplates = storage.HasManualTemplates();
            List<CCTemplateSystemListingInfo> previousReleases = storage.GetPreviousReleasesInfo().ToList();

            if (releaseVersion == null)
            {
                CurrentAutomatedtemplate.Visible = false;
            }

            if (hasManualTemplates == false)
            {
                CurrentManualClosingCostTemplates.Visible = false;
            }

            if (previousReleases.Count == 0)
            {
                PriorAutomatedClosingCostTemplateVersion.Visible = false;
            }

            else
            {
                PreviousReleases.DataSource = previousReleases;
                PreviousReleases.DataTextField = "Listing";
                PreviousReleases.DataValueField = "Id";
                PreviousReleases.DataBind();
            }


            RequiredImage.Src = ResolveUrl("~/images/error_icon.gif");

        }

        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;
            base.OnInit(e);
        }
    }
}
