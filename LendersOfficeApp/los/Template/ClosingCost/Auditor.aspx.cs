﻿namespace LendersOfficeApp.los.Template.ClosingCost
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess.ClosingCostAutomation;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public partial class Auditor : BasePage
    {
        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ClosingCostAutomationStorage storage = 
                new ClosingCostAutomationStorage(PrincipalFactory.CurrentPrincipal);

            CCTemplateSystem system = storage.GetDraftAndTemplates();

            


            var names = (from a in system.Templates
                         select new { a.Name, a.Id }).ToDictionary(p => p.Id, y => y.Name);

            var conditions = system.TemplateConditions;

            if (!conditions.CanPerformAudit)
            {
                NoScenarios.Visible = true;
                MainResults.Visible = false;
                return;
            }

            var success = conditions.PerformAudit(names);

            TempalteConditionSummary.DataSource = conditions.GenerateSummaryTable();
            TempalteConditionSummary.DataBind();

            ConflictSummary.DataSource = conditions.GenerateConflictTable();
            ConflictSummary.DataBind();
            ConflictingSummaryPlaceholder.Visible = ConflictSummary.Rows.Count != 0;


            MissingConditionSummary.DataSource = conditions.GenerateMissingTable();
            MissingConditionSummary.DataBind();
            MissingScenarioPlaceHolder.Visible = MissingConditionSummary.Rows.Count != 0;


            MissingTemplateSummary.DataSource = conditions.GenerateListOfTemplatesWithNoConditions();
            MissingTemplateSummary.DataBind();
            MissingTemplateSummaryPlaceHolder.Visible = MissingTemplateSummary.Rows.Count > 0;
            TemplatesWithNoScenarioError.Visible = MissingTemplateSummary.Rows.Count > 0;



            FailureMsg.Visible = ConflictSummary.Visible;
            WarningMsg.Visible = MissingScenarioPlaceHolder.Visible;

            Release.Disabled = !success;
            ReleaseSection.Visible = RequestHelper.GetBool("r");

            string[] unusedFields = conditions.GetUnusedFields().ToArray();

            FailureMsg2.Visible = unusedFields.Length > 0;
            FailureMsg2.InnerText = "Error: The following fields were selected but are unused in the templates, " + string.Join( ", ",unusedFields);
        }

        protected void FixHeaders_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                DataTable data = (DataTable)((GridView)sender).DataSource;
                for (int i = 0; i < data.Columns.Count; i++)
                {
                    e.Row.Cells[i].Text = AspxTools.HtmlString(data.Columns[i].Caption);
                }
            }
        }
    }
}
