﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateCondition.aspx.cs" Inherits="LendersOfficeApp.los.Template.ClosingCost.TemplateCondition" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Template Condition</title>
    <style type="text/css">
        body { background-color: gainsboro;  }
        div.field-section { display: inline-block; width: 240px; }
        div.content { padding: 10px 0 0 10px; line-height: 2; }
        div.field-section select { height: 150px; width: 175px; }
        span.combination { vertical-align: 70px; margin-left: 10px;}
        #btnSection input { margin: 0 auto;  }
        #btnSection  { text-align: center; }
        .scrollable { overflow:auto; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="MainRightHeader">Edit closing cost template condition</div>
        <div class="content">
        Apply the closing cost template named "<ml:EncodedLiteral runat="server" ID="TemplateName"></ml:EncodedLiteral>" when <br />
        Hold "Ctrl" to select more than one option for each field <br />
        <div class="scrollable">
            <asp:Repeater runat="server" ID="PossibleValues" OnItemDataBound="Values_OnItemDataBound">
                <ItemTemplate>
                    <div class="field-section">
                        <ml:EncodedLiteral runat="server" ID="FieldName"> </ml:EncodedLiteral> <br />
                        <asp:ListBox runat="server" ID="Values" SelectionMode="Multiple" CssClass="Selectable"></asp:ListBox> 
                        <span runat="server" id="CombinationText"  class="combination">and</span>
                        <br />
                        is any of the above selected
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <br />
        <div id="btnSection">
            <input type="button" id="OKBtn" value=" OK " />
            <input type="button" id="Cancel" value="Cancel" />
        </div>
        </div>
    </form>
    <script type="text/javascript">
        jQuery(function($) {

            function service(method, data) {
                var result = null;
                callWebMethodAsync({
                    url: 'SystemService.aspx?method=' + method,
                    type: 'POST',
                    async: false,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(data),
                    success: function(e) {
                        result = e.d;
                    },
                    error: function(e) {
                        alert('Error ' + e.Message);
                    }
                });
                return result
            }

            resize(560, 450);
            $('select.Selectable').change(function() {
                var $el = $(this), values = $el.val();
                if (!values) {
                    this.options[0].selected = true;
                }
                else if (values.length > 1) {
                    this.options[0].selected = false; //besure all is turned off
                }
            });

            $('#Cancel').click(function() {
                onClosePopup();
            });

            $('#OKBtn').click(function() {
                var data = [], friendlyRule = [];
                $('.scrollable select.Selectable').each(function() {
                    var $e = $(this), values = $e.val();

                    //skip ones with ALL selected
                    if (values.length == 1 && values[0] == '-999') {
                        return;
                    }

                    friendlyRule.push("(" + $e.attr('data-rep') + ' is ');
                    data.push({
                        FieldId: $e.attr('data-id'),
                        Values: values
                    });

                    $e.find('option:selected').each(function() {
                        friendlyRule.push($(this).text());
                        friendlyRule.push(" or ");
                    });

                    friendlyRule.pop();
                    friendlyRule.push(")");
                    friendlyRule.push(" and ");
                });

                friendlyRule.pop();

                var result = service('CacheData', { Data: JSON.stringify(data) });
                if (result) {
                    var args = {};
                    args.key = result.key;
                    args.msg = friendlyRule.join('');
                    onClosePopup(args);
                }
            });

        });
    </script>
</body>
</html>
