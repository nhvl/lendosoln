﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManualClosingCostList.ascx.cs"
    Inherits="LendersOfficeApp.los.Template.ClosingCost.ManualClosingCostList" EnableViewState="false" %>
<input type="button" value="Add New GFE 2010 Closing Cost Template" id="AddNew2010" />
<div class="fixed_height"> 
<asp:Repeater runat="server" ID="ManualTemplates" OnItemDataBound="ManualTemplates_OnItemDataBound">
    <HeaderTemplate>
        <table id="ManualTemplates" cellpadding="2" cellspacing="0">
            <thead>
                <tr class="GridHeader">
                    <th width="25">
                        &nbsp;
                    </th>
                    <th width="50">
                        &nbsp;
                    </th>
                    <th width="50">
                        &nbsp;
                    </th>
                    <th>
                        Closing Cost Template Name
                    </th>
                    <th width="50">
                        Version
                    </th>
                </tr>
            </thead>
            <tbody>
    </HeaderTemplate>
    <FooterTemplate>
        </tbody> </table>
    </FooterTemplate>
    
    <ItemTemplate>
        <tr class="GridItem">
            <td>
                <a href="#" runat="server" class="edit_template" id="EditLink">edit</a>
            </td>
            <td>
                <a href="#" runat="server" class="dupe_template"  id="DuplicateLink">duplicate</a>
            </td>
            <td>
                <a href="#" runat="server" id="DeleteLink" class="delete_template">delete</a>
            </td>
            <td>
                <span  class="nm"><ml:EncodedLiteral runat="server" ID="Name"></ml:EncodedLiteral> </span>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="Version"></ml:EncodedLiteral>
            </td>
        </tr>
    </ItemTemplate>
        <AlternatingItemTemplate>
        <tr class="GridAlternatingItem">
            <td>
                <a href="#" runat="server" class="edit_template" id="EditLink">edit</a>
            </td>
            <td>
                <a href="#" runat="server" class="dupe_template" id="DuplicateLink">duplicate</a>
            </td>
            <td>
                <a href="#" runat="server"  class="delete_template" id="DeleteLink">delete</a>
            </td>
            <td>
                <span  class="nm"><ml:EncodedLiteral runat="server" ID="Name"></ml:EncodedLiteral> </span>
            </td>
            <td>
                <ml:EncodedLiteral runat="server" ID="Version"></ml:EncodedLiteral>
            </td>
        </tr>
    </AlternatingItemTemplate>
</asp:Repeater>
</div>
<script type="text/javascript">
    $j(function() {
        var table;

        function service(method, data) {
            var result = null;
            callWebMethodAsync({
                url: 'SystemService.aspx?method=' + method,
                type: 'POST',
                async: false,
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data),
                success: function(e) {
                    result = e.d;
                },
                error: function(e) {
                    var msg = e.UserMessage;
                    if (!msg) {
                        msg = JSON.parse(e.responseText).UserMessage;
                    }
                    alert('Error ' + msg);
                }
            });
            return result
        }


        function stripeTable() {
            var isAlt = false, cssClass = 'GridItem';
            table.find('tbody tr').each(function(i, o) {
                $j(this).removeClass(isAlt ? 'GridItem' : 'GridAlternatingItem');
                $j(this).addClass(isAlt ? 'GridAlternatingItem' : 'GridItem');
                isAlt = !isAlt;
            });
        }


        function showEditor(cid, version, onReturnCallBack) {
            var url = '/los/Template/CCTemplate';
            if (version === '2010') {
                url += 'GFE2010.aspx';
            }
            else {
                url += '.aspx';
            }

            url += '?ccid=' + cid;

            showModal(url, null, "", null, onReturnCallBack,{ hideCloseButton:true });
        }

        function duplicateTemplate(cid, version, row) {
            var args;
            if (version === '2008') {
                alert('Cannot duplicate 2008 templates.');
                return;
            }
            var results = service("DuplicateTemplate", { ccid: cid });
            if (results) {
                var newRow = row.clone();
                newRow.find('a').removeProp('id');
                newRow.attr('data-id', results.NewId);
                args = showEditor(results.NewId, '2010', function(args){
                    if (args) {
                        newRow.find('span.nm').text(args.Name);
                    }
                });

                table.children('tbody').append(newRow);
                table.trigger('update');
                stripeTable();
            }
        }

        function deleteTemplate(cid, version, row) {
            var results = service("DeleteManualTemplate", { ccid: cid });
            if (results) {
                row.remove();
                table.trigger('update');
                stripeTable();
            }
        }

        $j('#AddNew2010').click(function() {
            var results = service("CreateBlankTemplateForManual", {});
            if (results) {
                var url = ['/los/template/CCTemplateGFE2010.aspx?auto=1&ccid=', results.TemplateId];
              showModal(url.join(''), null, null, null, function(){
                    window.location = window.location;
                },{ hideCloseButton:true });
            }
        });

        table = $j('#ManualTemplates').tablesorter({
            headers: {
                0: { sorter: false },
                1: { sorter: false },
                2: { sorter: false }
            }
        }).bind("sortEnd", function() {
            stripeTable();
        }).delegate('a.edit_template,a.dupe_template,a.delete_template', 'click', function() {
            var $a = $j(this), cid = $a.attr('data-id'), version = $a.attr('data-version'),
                $row = $a.parents('tr');

            if ($a.hasClass('edit_template')) {
                showEditor(cid, version,function(){
                    window.location = window.location;
                });
            }
            else if ($a.hasClass('dupe_template')) {
                duplicateTemplate(cid, version, $row);
            }
            else if ($a.hasClass('delete_template')) {
                deleteTemplate(cid, version, $row);
            }
            else {
                alert('Dev error.');
            }

            return false;
        });

    });
</script>