using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;
using System.Data.SqlClient;
using LendersOffice.Events;
using LendersOffice.RatePrice;

namespace LendersOfficeApp.los.Template
{

	/// <summary>
	//	OPM 16984 Cord
	/// A menu for admins to manage Investor's Products.
	/// </summary>
	/// 
	public partial class InvestorProductListAdmin : LendersOffice.Common.BasePage
	{

		private void BindDataGrid()
		{
			// Load the list of Products
            DataSet ds = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@InvestorName", m_InvestorDDL.SelectedItem.Text)
                                        };
            DataSetHelper.Fill(ds, DataAccess.DataSrc.LpeSrc, "ListInvestorProductCodesByInvestorName", parameters);
			m_ProductDG.DataSource = ds.Tables[0].DefaultView;
			
			m_ProductDG.DefaultSortExpression = "ProductCode ASC";
			m_ProductDG.DataBind();
		}

		private void BindInvestorList()
		{
            var list = InvestorNameUtils.ListActiveInvestorNames(BrokerUserPrincipal.CurrentPrincipal?.BrokerDB);

			m_InvestorDDL.DataSource = list;
			m_InvestorDDL.DataTextField = "InvestorName";
			m_InvestorDDL.DataBind();
		}

		protected void PageLoad( object sender , System.EventArgs a )
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

			if (!Page.IsPostBack)
			{
				BindInvestorList();
				m_InvestorDDL.SelectedIndex = int.Parse(RequestHelper.GetSafeQueryString("InvestorIndex"));

				BindDataGrid();
			}

		}

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// On postback re-bind in PreRender so UI reflects 
			// the data changes from event
			if (Page.IsPostBack)
			{
				if ( m_ProductDG.DoesClientSendSortCmd )
					m_ProductDG.EditItemIndex = -1;
				BindDataGrid();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

		protected void OnItemDataBound_ProductDG(Object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.EditItem)
			{
				// Load up the editing controls for the edited item
				DropDownList isValidDdl = (DropDownList)e.Item.FindControl("IsValidDdl");
				bool isValid = (bool) DataBinder.Eval(e.Item.DataItem, "IsValid");
				isValidDdl.SelectedIndex =  isValid ? 0 : 1;

				DropDownList useRsExpirationDdl = (DropDownList)e.Item.FindControl("IsRateSheetExpirationFeatureDeployedDdl");
				bool useRsExpiration = (bool) DataBinder.Eval(e.Item.DataItem, "IsRateSheetExpirationFeatureDeployed");
				useRsExpirationDdl.SelectedIndex =  useRsExpiration ? 0 : 1;
			}
		}

		protected void ProductDG_Cancel(Object sender, DataGridCommandEventArgs e) 
		{
			m_ProductDG.EditItemIndex = -1;
		}

		protected void ProductDG_Edit(Object sender, DataGridCommandEventArgs e) 
		{
			m_ProductDG.EditItemIndex = e.Item.ItemIndex;
		}

		// Update edited product
		protected void ProductDG_Update(Object sender, DataGridCommandEventArgs e)
		{
			DropDownList isValidDdl = (DropDownList) e.Item.FindControl("IsValidDdl");
			DropDownList useRsExpirationDdl = (DropDownList) e.Item.FindControl("IsRateSheetExpirationFeatureDeployedDdl");

			string investor = m_InvestorDDL.SelectedItem.Text;
			string productCode = e.Item.Cells[1].Text.Replace("&nbsp;", ""); // Grid can add non-breakable space in empty cell for table formatting
			bool prodIsValid = isValidDdl.SelectedIndex == 0;
			bool useRsExpiration = useRsExpirationDdl.SelectedIndex == 0;

			try
			{
				if ( StoredProcedureHelper.ExecuteNonQuery(
					DataSrc.LpeSrc
					, "SetInvestorProductCodeValid"
					, false
					, 2
					, new SqlParameter("@InvestorName", investor)
					, new SqlParameter("@ProductCode", productCode)
					, new SqlParameter("@IsValid", prodIsValid)
					, new SqlParameter("@IsRateSheetExpirationFeatureDeployed", useRsExpiration)
					) > 0 )
					m_Result.Text = string.Format("Updated {0}:{1}.", investor, productCode);
				else
					m_Result.Text = string.Format("Failed to update {0}:{1}.", investor, productCode);
			}
			catch ( SqlException exc )
			{
				m_Result.Text = "Failed to modify product";
				Tools.LogError( "Failed to modify product", exc );
			}

			m_ProductDG.EditItemIndex = -1;
		}

		// Add the new product to the list.
		protected void OnProductAddClick( Object sender, EventArgs e )
		{
			if (string.IsNullOrWhiteSpace(m_ProductAddTB.Text))
            {
                m_Result.Text = "Please specify a product";
            }
            else
            {
				// Add this product to the DB
				try
				{
                    string productName = m_ProductAddTB.Text.Trim().ToUpper();
					if (StoredProcedureHelper.ExecuteNonQuery(
						DataSrc.LpeSrc
						, "AddProductCode"
						, false
						, 2
						, new SqlParameter("@InvestorName", m_InvestorDDL.SelectedItem.Text)
						, new SqlParameter("@ProductCode", productName)
						, new SqlParameter("@IsRateSheetExpirationFeatureDeployed", m_newExpirationChoiceRBL.SelectedItem.Value)) > 0)
					{
						m_Result.Text = "Added Product " + productName
							+ " to Investor " + m_InvestorDDL.SelectedItem.Text + ".";
					}
					else
					{
						m_Result.Text = "Failed to add product.";
					}
				}
				catch ( SqlException exc )
				{
					m_Result.Text = "Failed to add product.";
					Tools.LogError( "Failed to add product to investor's product list", exc );
				}
			}
		}
	}
}
