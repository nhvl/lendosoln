using System;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Template
{
	/// <summary>
	/// Summary description for ClosingCostList.
	/// </summary>
	public partial class ClosingCostList : BasePage
	{
		protected bool m_deleteInvalid;

        private Guid m_brokerID => BrokerUserPrincipal.CurrentPrincipal.BrokerId;

        protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			RegisterJsScript("LQBPopup.js");
			//OPM 3453
		    var hasPermision = BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanAccessCCTemplates);

            m_ccListPanel.Visible = hasPermision;
            m_AccessDeniedPanel.Visible = !hasPermision;

            if (!Page.IsPostBack) 
            {
                BindDataGrid();
            }            
            else 
            {
                // Page is postback.
                // This is the temporary code requires for sorting to work. dd 5/7/2003.
               
				if (Request.Form["__EVENTTARGET"] == m_closingCostsDG.ClientID) 
                {
                    BindDataGrid();
                }
				//grabs the ccid if the user wants to duplicate a template
				if(m_hiddenCmd.Value == "duplicate")
				{
					onDuplicateClick(m_hiddenCcid.Value, m_hiddenGfeVersion.Value);
				}
				if(m_hiddenCmd.Value == "delete")
				{
					onDeleteClick(m_hiddenCcid.Value);
				}
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            this.EnableJqueryMigrate = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
        private void BindDataGrid()
        {
            SqlParameter[] parameters = { new SqlParameter("@BrokerID", m_brokerID) };

            using (var reader = StoredProcedureHelper.ExecuteReader(m_brokerID, "ListClosingCostByBrokerID", parameters))
            {
                m_closingCostsDG.DataSource = reader;
                m_closingCostsDG.DataBind();
            }
        }

        private void showModal(string href) 
        {
            string script = $"<script language=\'JavaScript1.2\'>showModal(\'{href}\', null, null, null, function(){{window.location = self.location;}});</script>";
            ClientScript.RegisterStartupScript(this.GetType(), "showModal", script);
        }

        protected void onAddClick(object sender, System.EventArgs e)
        {
            Guid templateID = CCcTemplateBase.CreateCCTemplate(m_brokerID, 0);
            showModal("/los/Template/CCTemplate.aspx?ccid=" + templateID);
        }

        protected void onAddGFE2010Click(object sender, System.EventArgs e)
        {
            Guid templateID = CCcTemplateBase.CreateCCTemplate(m_brokerID, 1);
            showModal("/los/Template/CCTemplateGFE2010.aspx?ccid=" + templateID);
        }

        private void onDuplicateClick(string ccid, string GfeVersion)
		{
			m_hiddenCmd.Value = "";
		    var link = GfeVersion == "0" ? "/los/Template/CCTemplate.aspx" : "/los/Template/CCTemplateGFE2010.aspx";
			Guid dupTemplateID = CCcTemplateBase.CreateCCTemplate(m_brokerID, Convert.ToInt32(GfeVersion));
			showModal(link + "?ccid=" + ccid + "&ccid2=" + dupTemplateID);
		}

		private void onDeleteClick(string ccid)
		{
			m_hiddenCmd.Value = "";

            try
            {
                Guid templateId = new Guid(ccid);
                bool allowDelete = IsAllowDelete(m_brokerID, templateId);

                m_deleteInvalid = !allowDelete;
                if (allowDelete)
				{
					CCcTemplateBase.DelCCTemplate(m_brokerID, templateId);
				}

				BindDataGrid();
			}
			catch(SqlException sqle)
			{
				Tools.LogError(sqle.Message);
			}
		}

		protected void m_closingCostsDG_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			
		}

        private static bool IsAllowDelete(Guid brokerId, Guid templateId)
	    {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@lCcTemplateId", templateId)
            };
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "IsCCTemplateAssociatedWithLoanProgram", parameters))
            {
                return !reader.Read();
            }
        }
	}
}
