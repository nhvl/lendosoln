<%@ Page language="c#" Codebehind="LoanProgramTemplate.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.LoanProgramTemplate1" %>
<%@ Register TagPrefix="uc1" TagName="LoanProgramTemplate" Src="LoanProgramTemplate.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>LoanProgramTemplate</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgcolor="#cccccc" onload="init();">
		<script language="javascript">

			<!--

			function init()
			{
				<% if( IsPostBack == false ) { %>

					resize( 770 , 550 );

				<% } %>

				if( document.getElementById("m_errorMessage") != null )
				{
					alert( document.getElementById("m_errorMessage").value );
				}
				
				if( document.getElementById("m_commandToDo") != null )
				{
					if( document.getElementById("m_commandToDo").value == "Close" )
					{
						args = {};
						args.OK = true;
						onClosePopup(args);
					}
					
					if( document.getElementById("m_commandToDo").value == "Reopen" )
					{
						self.location = <%= AspxTools.JsString(VirtualRoot + "/los/Template/LoanProgramTemplate.aspx?FileID=" +  Request["FileId"] + "&mode=edit") %>;
					}

				}
			}

			//-->

		</script>
		<form id="LoanProgramTemplate" method="post" runat="server">
			<uc1:LoanProgramTemplate id="m_ctrl" runat="server"></uc1:LoanProgramTemplate>
		</form>
	</body>
</HTML>
