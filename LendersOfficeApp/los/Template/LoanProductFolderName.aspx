<%@ Page language="c#" Codebehind="LoanProductFolderName.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.LoanProductFolderName" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LoanProductFolderName</title>
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout" scroll="no" onload="init();">
	<script type="text/javascript">
        function init() {
            resize(480, 160);
            document.getElementById("m_nameTF").focus();

            if( document.getElementById("m_errorMessage") != null )
            {
	            alert( document.getElementById("m_errorMessage").value );
            }
        }
	</script>
		<h4 class="page-header"><ml:EncodedLabel id="m_headerLabel" runat="server"></ml:EncodedLabel></h4>
		<form id="LoanProductFolderName" method="post" runat="server">
			<table cellSpacing="2" cellPadding="0" width="100%" border="0" class="FormTable">
				<TR>
					<TD class="FieldLabel">
						Folder Id:
					</TD>
					<TD>
						<asp:TextBox id="m_FolderId_ed" runat="server" style="BORDER: none; PADDING-LEFT: 4px;" Width="300px" ReadOnly="True" ForeColor="Black" BackColor="#E0E0E0"></asp:TextBox>
					</TD>
				</TR>
				<tr>
					<TD class="FieldLabel">
						Folder Name:
					</TD>
					<TD>
						<asp:TextBox id="m_nameTF" runat="server" style="PADDING-LEFT: 4px;" MaxLength="36" Width="300px"></asp:TextBox><IMG src="../../images/require_icon.gif">
                        <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="m_nameTF">
                            <img runat="server" src="../../images/error_icon.gif">
                        </asp:RequiredFieldValidator>
					</TD>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<asp:Button id="m_okBtn" runat="server" Text="  OK  " OnClick="OnOKClick">
						</asp:Button>
						<input type="button" value="Cancel" onclick="onClosePopup();">
					</td>
				</tr>
			</table>
		</form>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
	</body>
</HTML>
