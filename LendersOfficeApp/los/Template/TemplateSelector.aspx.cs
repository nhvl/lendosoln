﻿namespace LendersOfficeApp.los.Template
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using LendersOffice.Common;
    using CommonProjectLib.Common.Lib;
    using System.Data.SqlClient;
    using LendersOffice.Security;
    using DataAccess;
    using System.Web.Services;
    using LendersOfficeApp.los.RatePrice;
    using LendersOffice.AntiXss;

    /// <summary>
    /// This page supports 3 commands
    /// - edit (default): select programs for batch edit. UI: user checks a checkbox => automatically check all its descendant.
    /// - productcode: Select non-derived Programs to modify their investor/product code. UI: user checks a checkbox => automatically check all its descendant.
    /// - derive: select programs/folders for batch derive. UI: user checks a checkbox => don't automatically check all its descendant.
    /// - view: view selected programs/folders from cacheKey. UI: user cannot change checkbox's status.
    /// </summary>

    public partial class TemplateSelector : BasePage
    {
        private sealed class DynaTreeNodeBasic
        {
            public string title { get; set; }
            public bool isFolder { get; set; }
            public Guid identifier { get; set; }
            public List<DynaTreeNodeBasic> children { get; set; }
        }

        protected string m_cmd = "edit";
        protected IEnumerable<string> m_selectedProgramList = System.Linq.Enumerable.Empty<string>();

        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;
            base.OnInit(e);
        }
        private DynaTreeNodeBasic Add(LoanProductContainer container)
        {
            DynaTreeNodeBasic parent = new DynaTreeNodeBasic();
            parent.isFolder = true;
            parent.title = container.Name;
            parent.identifier = container.Id;

            parent.children = new List<DynaTreeNodeBasic>();

            List<LoanProductNode> childrenPrograms = new List<LoanProductNode>();
            List<LoanProductNode> childrenFolders = new List<LoanProductNode>();

            foreach (LoanProductNode node in container)
            {
                if (node.Type == LoanProductType.Folder)
                {
                    childrenFolders.Add(node);
                }
                else if (node.Type == LoanProductType.Product)
                {
                    childrenPrograms.Add(node);
                }
            }


            foreach (LoanProductNode node in childrenFolders.OrderBy(p => p.Name))
            {
                parent.children.Add(Add((LoanProductContainer)node));
            }

            foreach (LoanProductNode node in childrenPrograms.OrderBy(p => p.Name))
            {
                parent.children.Add(GetProgram(node.Name, node.Id));
            }

            return parent;
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE9;
        }

        private DynaTreeNodeBasic GetProgram(string name, Guid id)
        {
            DynaTreeNodeBasic root = new DynaTreeNodeBasic();
            root.isFolder = false;
            root.title = name;
            root.identifier = id;
            root.children = new List<DynaTreeNodeBasic>();
            return root;
        }

        [WebMethod]
        public static string CacheSelectedPrograms(Guid[] programIds)
        {
            return AutoExpiredTextCache.InsertUserObject(PrincipalFactory.CurrentPrincipal, programIds, TimeSpan.FromHours(1));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");

            string cmd = RequestHelper.GetSafeQueryString("cmd");
            if (cmd != "view" && cmd != "derive" && cmd != "productcode")
            {
                cmd = "edit";
            }

            m_cmd = cmd;

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            LoanProductSet set = new LoanProductSet();
            Func<LoanProductTemplate, bool> filter = null;
            if (m_cmd == "productcode")
            {
                filter = (LoanProductTemplate loanProductTemplate) => loanProductTemplate.Base == Guid.Empty; // select normal program only.
            }

            set.Retrieve(principal.BrokerId, principal.HasPermission(Permission.CanModifyLoanPrograms), folderOnly:false, acceptFilter: filter);

            DynaTreeNodeBasic root = Add(set);
            root.title = "Root Folder";

            RegisterJsStruct("NavNodes", new DynaTreeNodeBasic[] { root });

            if (cmd == "view" )
            {
                List<Guid> items = null;

                if (!String.IsNullOrEmpty(RequestHelper.GetSafeQueryString("s")))
                {
                    items = AutoExpiredTextCache.GetUserObject<List<Guid>>(PrincipalFactory.CurrentPrincipal, RequestHelper.GetSafeQueryString("s"));
                }

                if (items != null)
                {
                    m_selectedProgramList = items.Select( i => i.ToString()).ToList();
                }
            }
        }
    }
}
