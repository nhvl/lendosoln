<%@ Page language="c#" Codebehind="DisableLoanPrograms.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.DisableLoanPrograms" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>DisableLoanPrograms</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<STYLE type="text/css">
		.contextoverridden { FONT-WEIGHT: bold }
		</STYLE>
  </head>
  <body bgColor="gainsboro" scroll="yes" onload="init();" MS_POSITIONING="FlowLayout">
		<script language="javascript">
		<!--
			function init()
			{
				resize(800, 600);
			}
			
			function onNew()
			{
				showModal('/los/Template/EditDisabledLoanProgramInfo.aspx?cmd=add', null, null, null, function(arg){
					if (arg.OK) 
						document.DisableLoanPrograms.submit() ;	// refresh
				});
			}

			function onEdit(pcode, investorName)
			{
				showModal('/los/Template/EditDisabledLoanProgramInfo.aspx?cmd=edit&pcode=' + encodeURIComponent(pcode) + '&investorname=' + encodeURIComponent(investorName), null, null, null, function(arg){ 
					if (arg.OK) 
						document.DisableLoanPrograms.submit() ;	// refresh
				});
			}
	        
			function onDelete(pcode, investorName)
			{
				if(confirm("Remove this loan program from the disabled list?"))
				{
					<%= AspxTools.JsGetElementById(m_Command) %>.value = "delete";
					<%= AspxTools.JsGetElementById(m_pcode) %>.value = pcode;
					<%= AspxTools.JsGetElementById(m_investorName) %>.value = investorName;
					document.DisableLoanPrograms.submit();
				}
			}
		//-->
		</script>
	<h4 class="page-header">Disabled Loan Programs</h4>
    <form id="DisableLoanPrograms" method="post" runat="server">
	<TABLE class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
	<input type="hidden" runat="server" id="m_Command" NAME="m_Command"> 
	<input type="hidden" runat="server" id="m_pcode" NAME="m_pcode">
	<input type="hidden" runat="server" id="m_investorName" NAME="m_investorName">
	<tr>
		<TD align="left">
			<INPUT type="button" value="Add new" onclick="onNew();">
			<INPUT onclick="onClosePopup();" type="button" value="Close">
		</TD>
	</tr>
		<tr>
			<td>
				<ML:CommonDataGrid id="m_Grid" runat="server" CellPadding="2" DefaultSortExpression="PriceServerId ASC">
					<Columns>
						<ASP:TemplateColumn ItemStyle-Width="1%">
							<ItemTemplate>
								<A href="#" onclick="onEdit(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "ProductCode").ToString())%>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "InvestorName").ToString())%>); return false;">
									edit </A>
							</ItemTemplate>
						</ASP:TemplateColumn>
						<ASP:TemplateColumn ItemStyle-Width="1%">
							<ItemTemplate>
									<A href="#" onclick="onDelete(<%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "ProductCode").ToString())%>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "InvestorName").ToString())%>); return false;">
									delete</A>&nbsp;&nbsp;&nbsp;
							</ItemTemplate>
						</ASP:TemplateColumn>
						<ASP:BoundColumn DataField="InvestorName" ItemStyle-Width="15%" HeaderText="Investor Name"></ASP:BoundColumn>
						<ASP:BoundColumn DataField="ProductCode" ItemStyle-Width="15%" HeaderText="Product Code"></ASP:BoundColumn>
						<ASP:BoundColumn DataField="DisableStatus" ItemStyle-Width="15%" HeaderText="Disabled Status"></ASP:BoundColumn>
						<ASP:BoundColumn DataField="SaeName" ItemStyle-Width="20%" HeaderText="SAE Name"></ASP:BoundColumn>
						<ASP:BoundColumn DataField="Notes" ItemStyle-Width="40%" HeaderText="Notes"></ASP:BoundColumn>
					</Columns>
				</ML:CommonDataGrid>
				<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
			</td>
		</tr>
	</table>
     </form>
  </body>
</html>
