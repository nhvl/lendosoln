﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.ObjLib.PriceGroups;
using LendersOffice.ObjLib.PriceGroups.Model;

namespace LendersOfficeApp.los.Template
{
    // 12/02/2015 mf.  This is meant to be a quick INTERNAL-ONLY page
    // to allow SAEs to batch set visibility of programs across many
    // price groups without having SDE directly update the live
    // production database.  

    // We can add more batch features as needed, for now I only added 
    // what we usually need right now, which is batch visibility.
    // Also notice this one asks SAE to provide the programIds rather 
    // than give selection UI. This was done for speed of implementation
    // and because one of the reasons they need to this is because some
    // lenders have more programs than our price group editor supports, 
    // which could give us the same issue here.  Typically the list of
    // programs needing to be visible is small.

    public partial class BatchPriceGroupOperation : LendersOffice.Common.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms))
            {
                throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
            }


            Result.Rows.Clear();
            ToggleElementDisplay(PriceGroupInputRow, false);
            ToggleElementDisplay(RunOperation, false);
            ToggleElementDisplay(ResultRow, IsPostBack);
        }

        protected void verifyPrograms_Click(object sender, EventArgs e)
        {
            List<Guid> programs;
            if (ParsePrograms(out programs))
            {
                BindPriceGroupList();
                ToggleElementDisplay(ProgramInputRow, false);
                ToggleElementDisplay(PriceGroupInputRow, true);
                ToggleElementDisplay(RunOperation, true);
            }
        }

        private bool ParsePrograms(out List<Guid> programList)
        {
            // Split on whatever delimiter is there. Valid if we end up with a list of GUIDs.
            var str = Regex.Replace(ProgramIds.Text, @"\s+", " ");
            List<string> programIdStr = str.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim()).ToList();

            var programIds = new List<Guid>();
            List<String> errors = new List<string>();
            bool isOk = false;

            programIdStr.ForEach(p =>
            {
                Guid programId;
                if (Guid.TryParse(p, out programId))
                {
                    programIds.Add(programId);
                }
                else
                {
                    errors.Add("Not a valid id: " + p + ".");
                }
            });

            if (errors.Count() != 0)
            {
                AddResultText("ERROR: Parsing Error.");
                errors.ForEach(err => AddResultText( err ));
            }
            else if (programIds.Count() == 0)
            {
                AddResultText("ERROR: Cannot find programIds");
            }
            else
            {
                // Verify they exist and write their names to UI.
                // This is more than convenience, it is for safety
                // to ensure we are exposing the correct programs.

                if (VerifyProgramData(programIds))
                {
                    isOk = true;
                }
            }

            programList = programIds;
            return isOk;
        }

        private void ToggleElementDisplay(HtmlControl element, bool shouldShow)
        {
            element.Style.Add(HtmlTextWriterStyle.Display, shouldShow ? "" : "none");
        }

        private void ToggleElementDisplay(WebControl element, bool shouldShow)
        {
            element.Style.Add(HtmlTextWriterStyle.Display, shouldShow ? "" : "none");
        }

        private bool VerifyProgramData(List<Guid> programIds)
        {
            // Get list of programs at this lender.  Add the program info if found, otherwise error.
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", brokerId),
                                                new SqlParameter("@IsLpe", true)
                                            };
            Dictionary<Guid, string> lenderPrograms = new Dictionary<Guid, string>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListLoanProgramNamesByBrokerID", parameters))
            {
                while (reader.Read())
                {
                    Guid programId = (Guid)reader["lLpTemplateId"];
                    if (programIds.Contains(programId))
                    {
                        lenderPrograms.Add(programId, (reader["lLpTemplateNm"].ToString()));
                    }
                }
            }


            if (programIds.All(p => lenderPrograms.ContainsKey(p)))
            {
                programIds.ForEach(p => AddResultText(lenderPrograms[p] + " (" + p.ToString() + ")"));
                return true;
            }
            else
            {
                AddResultText("ERROR: Not all programs found at this lender.");
                foreach (var missingId in programIds.Where(p => !lenderPrograms.ContainsKey(p)))
                {
                    AddResultText("Program not found: " + missingId + ".");
                }
                return false;
            }
        }

        private void BindPriceGroupList()
        {
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", brokerId)
                                            };
            Dictionary<Guid, string> lenderPrograms = new Dictionary<Guid, string>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListPricingGroupByBrokerId ", parameters))
            {
                while (reader.Read())
                {
                    PriceGroupList.Items.Add( new ListItem(reader["LpePriceGroupName"].ToString(), reader["LpePriceGroupId"].ToString()));
                }
            } 
        }

        private void AddResultText(string textToAdd)
        {
            TableRow itemRow = new TableRow();
            itemRow.Attributes.Add("class", Result.Rows.Count % 2 == 0 ? "GridItem" : "GridAlternatingItem");
            itemRow.Cells.Add(new TableCell() { Text = textToAdd });
            Result.Rows.Add(itemRow);
        }

        protected void RunOperation_Click(object sender, EventArgs e)
        {
            List<Guid> programs;
            ParsePrograms( out programs);
            Result.Rows.Clear();

            List<Guid> priceGroups = new List<Guid>();
            foreach (ListItem item in PriceGroupList.Items)
            {
                if (item.Selected)
                {
                    priceGroups.Add(new Guid(item.Value));
                }
            }

            if (programs.Count() == 0 || priceGroups.Count() == 0)
            {
                AddResultText("Nothing selected, no action taken.");
                return;
            }

            SetProgramsVisibleInPriceGroups(priceGroups, programs);

            AddResultText("Update Complete!");
        }

        private void SetProgramsVisibleInPriceGroups(List<Guid> priceGroups, List<Guid> programs)
        {
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;

            foreach (var priceGroupId in priceGroups)
            {
                var pgData = LendersOffice.ObjLib.PriceGroups.PriceGroupProduct.ListByPriceGroupId(brokerId, priceGroupId);
                List<LpePriceGroupProduct> updatePriceGroupProductList = new List<LpePriceGroupProduct>();

                foreach (var programId in programs)
                {
                    decimal margin = 0;
                    bool yspLock = false;
                    decimal ysp = 0;
                    decimal rateMargin = 0;
                    decimal marginMargin = 0;
                    bool isValid = VisibleChoice.SelectedIndex == 0;

                    var existingData = pgData.FirstOrDefault(p => p.LpTemplateId == programId);
                    if (existingData != null)
                    {
                        if (existingData.IsValid == isValid)
                        {
                            // Already set.
                            continue;
                        }

                        // Exists.  Preserve current settings.
                        margin = existingData.LenderProfitMargin;
                        yspLock = existingData.LenderMaxYspAdjustLckd;
                        ysp = existingData.LenderMaxYspAdjust;
                        rateMargin = existingData.LenderRateMargin;
                        marginMargin = existingData.LenderMarginMargin;
                    }
                    else
                    {
                        if (isValid == false)
                        {
                            // Already false because no row exists.
                            // No reason to create a new row.
                            continue;
                        }
                    }

                    LpePriceGroupProduct item = new LpePriceGroupProduct();
                    item.IsValid = isValid;
                    item.LoanProgramTemplateId = programId;
                    item.LenderProfitMargin = margin;
                    item.LenderMaxYspAdjustLckd = yspLock;
                    item.LenderMaxYspAdjust = ysp;
                    item.LenderRateMargin = rateMargin;
                    item.LenderMarginMargin = marginMargin;
                    updatePriceGroupProductList.Add(item);
                }

                PriceGroup priceGroup = PriceGroup.RetrieveByID(priceGroupId, brokerId);
                priceGroup.Save(BrokerUserPrincipal.CurrentPrincipal, updatePriceGroupProductList);
            }
        }
    }
}