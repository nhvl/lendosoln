namespace LendersOfficeApp.los.Template
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    /// <summary>
    /// 
    /// </summary>
    public partial class DeleteCCTemplateFailure : BasePage
	{
	
		private Guid m_ccTemplateId 
		{
			get { return new Guid( Request[ "ccid" ] ); }
		}

		private string m_ccTemplateName 
		{
			get { return Request[ "ccname" ]; }
		}

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		private Guid m_brokerID 
		{
			get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
		}

		private String ErrorMessage
		{
			// Write out popup message to alert user of error.
			set { Page.ClientScript.RegisterHiddenField( "m_errorMessage" , value ); }
		}

		private String FeedBack
		{
			// Write out popup message to alert user of error.
			set { Page.ClientScript.RegisterHiddenField( "m_feedBack" , value ); }
		}
		
		protected void PageLoad(object sender, System.EventArgs e)
		{
			m_cctname.Text = "Loan Programs associated with Closing Cost Template: " + m_ccTemplateName;

			if(Page.IsPostBack)
			{
				m_initialPanel.Visible = false;
				m_viewList.Visible = true;
				BindDataGrid();
			}
			else
			{
				m_initialPanel.Visible = true;
				m_viewList.Visible = false;
			}
		}

		private void BindDataGrid()
		{
			DataSet ds = new DataSet();
			SqlParameter[] parameters = {
											new SqlParameter("@BrokerId", m_brokerID),
											new SqlParameter("@lCcTemplateId", m_ccTemplateId)
										};
			DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListLoanProgramsByCCTemplateID", parameters );
			m_programsDG.DataSource = ds.Tables[0].DefaultView;
			m_programsDG.DataBind();
		}

		protected void OnDeleteClick( object sender , System.EventArgs a )
		{
			try
			{
				// 7/15/2004 kb - Block access if pricing engine enabled
				// and user lacks permission.
                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
				{
					if( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == false && CurrentUser.IsInRole( ConstApp.ROLE_ADMINISTRATOR ) == false )
					{
						ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";
						return;
					}
				}

                // Get template IDs.
                HashSet<Guid> templateIds = new HashSet<Guid>();
                foreach (DataGridItem item in m_programsDG.Items)
                {
                    HtmlInputCheckBox checkbox = (HtmlInputCheckBox)item.FindControl("templateid");
                    if (checkbox.Checked)
                    {
                        templateIds.Add(new Guid(checkbox.Value));
                    }
                }

                if (templateIds.Count == 0)
                {
                    return;
                }

                int numDeleted = 0, numToDelete = 0;
                foreach (Guid id in templateIds) 
				{
					try
					{
						++numToDelete;
						CLoanProductBase.DelLoanProduct(id);
						++numDeleted;
					}
					catch( CBaseException e )
					{
						ErrorMessage = e.UserMessage;
					}
				}

				if( numDeleted < numToDelete )
					FeedBack = "Deleted " + numDeleted + " of " + numToDelete + " loan programs.";
				else
				{
					if(numDeleted == 1)
						FeedBack = "Deleted " + numDeleted + " loan program.";
					else
						FeedBack = "Deleted " + numDeleted + " loan programs.";
				}

				BindDataGrid();
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to delete loan program." , e );
			}
		}

        protected void m_programsDG_ItemDataBound(object sender, DataGridItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView row = args.Item.DataItem as DataRowView;

            Guid lLpTemplateId = (Guid)row["lLpTemplateId"];

            HtmlInputCheckBox checkbox = args.Item.FindControl("templateid") as HtmlInputCheckBox;
            checkbox.Value = lLpTemplateId.ToString();

            LinkButton editLink = args.Item.FindControl("editLink") as LinkButton;
            editLink.OnClientClick = "onEditClick(" + LendersOffice.AntiXss.AspxTools.JsString(lLpTemplateId) + "); return false;";

            EncodedLabel isEnabledOverrideBit = args.Item.FindControl("IsEnabledOverrideBit") as EncodedLabel;
            isEnabledOverrideBit.Text = (string)row["Enabled"];
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            EnableJqueryMigrate = false;
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
