<%@ Page language="c#" Codebehind="PMIProductTemplate.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.PMIProductTemplate" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PMIProductTemplate</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" bgcolor="#cccccc" onload="resize(450,180)">
	    <script language=javascript>
            function _closeMe() {
                onClosePopup();
            }
            //-->
        </script>
        <h4 class="page-header">Mortgage Program Template</h4>
		<form id="PMIProductTemplate" method="post" runat="server">
		    <TABLE id="Table1" width="100%" border="0">
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:TextBox ID="ProductId" runat="server" ReadOnly="true" Width="220px"></asp:TextBox>
                    </td>
                </tr>
	            <tr>
		            <td class="FieldLabel">Mortgage Insurance Company
		            </td>
		            <td class="FieldLabel">MI Type
		            </td>
	            </tr>
	            <tr>
	                <td>
	                    <asp:dropdownlist id="MICompany" runat="server"></asp:dropdownlist>
	                </td>
	                <td>
	                    <asp:dropdownlist id="MIType" runat="server"></asp:dropdownlist>
	                </td>
	            </tr>
	            <TR>
		            <TD colspan="2" align="center" class="FieldLabel">
		                <asp:CheckBox runat="server"  ID="UfmipIsRefundableOnProRataBasis"  Text="UFMIP is refundable on a pro-rata basis" />
			            <asp:checkbox id="IsEnabled" runat="server" Text="Allow Using This Program" TextAlign="Right"></asp:checkbox>
		            </TD>
	            </TR>
	            <TR>
		            <TD colspan="2" align="center">
		                <asp:Button ID="m_btnSave" Text="Save" runat="server" OnClick="SaveClick" />
			            <input id="m_btnClose" type="button" value="Cancel" onclick="onClosePopup()">
		            </TD>
	            </TR>
            </TABLE>
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
		</form>
	</body>
</HTML>
