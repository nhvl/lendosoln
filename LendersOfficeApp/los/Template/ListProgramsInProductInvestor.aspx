﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListProgramsInProductInvestor.aspx.cs"
    Inherits="LendersOfficeApp.los.Template.ListProgramsInProductInvestor" %>

<%@ Register TagPrefix="uc" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Loan Programs</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        .topHeader
        {
            padding: 5px 20px;
            background-color: #003366;
            color: White;
            font-weight: bold;
            font-size: 12px;
        }
        .ButtonStyle
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            font-weight: bold;
            color: Black;
        }
    </style>
</head>
<body style="overflow-y: scroll;" bgcolor="gainsboro">
    <script>
        function _init() {
        <% if (!IsPostBack ) { %>
            resizeForIE6And7(800, 500);
        <% } %>
        }
    </script>

    <form runat="server">
        <div class="topHeader">Loan Programs</div>
        <div style="margin: 15px;">
            <asp:Panel id="m_AccessDeniedPanel" style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
                Access Denied. You do not have access to view this page.
            </asp:Panel>
            <asp:Panel id="m_AccessGrantedPanel" runat="server">
                <div style="width: 730px; height: 400px; border: inset 2px silver;">
                    <table width="100%" style="height: 100%;" border="0" cellpadding="0" cellspacing="0">
                        <tr valign="top" style="height: 1%;">
                            <td>
                                <ml:CommonDataGrid ID="m_LoanProgramList" BorderWidth="0" CellPadding="2" runat="server" DefaultSortExpression="Investor ASC">
                                    <AlternatingItemStyle CssClass="GridAlternatingItem"/>
                                    <ItemStyle CssClass="GridAlternatingItem"/>
                                    <Columns>
                                        <asp:BoundColumn DataField="Investor"        SortExpression="Investor"        HeaderText="Investor"             HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                        <asp:BoundColumn DataField="ProductCode"     SortExpression="ProductCode"     HeaderText="Product Code"         HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                        <asp:BoundColumn DataField="LoanProgramName" SortExpression="LoanProgramName" HeaderText="Loan Program Name"    HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                        <asp:BoundColumn DataField="RateStatus"      SortExpression="RateStatus"      HeaderText="Rate Status"          HeaderStyle-Width="40px" ItemStyle-Width="40px" />
                                    </Columns>
                                </ml:CommonDataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="center">
                                <ml:EncodedLabel ID="m_emptyMsg" Font-Italic="true" ForeColor="GrayText" Text="No loan program was found." Visible="false" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <div style="text-align: center;">
                    <input type="button" class="ButtonStyle" onclick="onClosePopup();" value="Close" />
                </div>
            </asp:Panel>
        </div>
    </form>
    <uc:cModalDlg ID="modalDlg" runat="server" />
</body>
</html>
