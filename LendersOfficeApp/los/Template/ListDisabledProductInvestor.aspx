﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListDisabledProductInvestor.aspx.cs" Inherits="LendersOfficeApp.los.Template.ListDisabledProductInvestor" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Disable Pricing</title>

    <style type="text/css">
        .ButtonStyle
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            font-weight: bold;
            color: Black;
        }
        .Hidden
        {
            display: none;
        }
    </style>
</head>
<body style="overflow-y: scroll;" bgcolor="gainsboro">

<script>
    function _init() {
        <% if (!IsPostBack) { %>
            resizeForIE6And7(900, 520);
        <% } %>
        <% if (this.IsManualExpirationEnabled) { %>
            f_setAllBtnStatus();
        <% } %>
        <% if (this.LockPolicyId != Guid.Empty) { %>
            f_HideLockPolicyColumn();
        <% } %>
    }
    function f_AddDisabled() {
        var wargs;
        showModal('/los/Template/AddDisabledProductInvestor.aspx?policyId='+<%= AspxTools.JsString(this.LockPolicyId) %>, null, null, null, function(wargs){
            if (wargs && wargs.OK) {
                __doPostBack(<%=AspxTools.JsGetClientIdString(this.DisabledList)%>,'ReloadGrid');
            }
        },{ hideCloseButton:true });
        return false;
    }
    function f_listPrograms(inv, prod, policyId) {
        var args = new Object();
        args.investor = inv;
        args.pcode = prod;
        args.lockPolicyId = policyId;
        var addr = '/los/Template/ListProgramsInProductInvestor.aspx';
        var param = '?inv=' + args.investor + '&prod=' + args.pcode + '&policyid=' + args.lockPolicyId;
        var url =  addr + param;
        showModal(url, args, 'Loan Programs', null, null, {hideCloseButton:true});
    }
    function f_SelectAll(select) {
        var tbody = <%= AspxTools.JsGetElementById(DisabledList)%>;
        if (tbody.rows.length == 1)
            return;

        for (var i = 1; i < tbody.rows.length; i++)
            tbody.rows[i].children[0].children[0].checked = select;

        f_setAllBtnStatus();
    }
    function f_HideLockPolicyColumn()
    {
        var tbody = <%= AspxTools.JsGetElementById(DisabledList)%>;
        var header = tbody.rows[0];



        for (var col = 0; col < header.children.length; col++)
        {
            if (header.children[col].innerText.replace(/^\s+|\s+$/g,'') === 'Lock Policy')
            {
                for(var row = 0; row < tbody.rows.length; row++) {
                    tbody.rows[row].children[col].className = 'Hidden';
                }
            }
        }
    }
    function f_setAllBtnStatus()
    {
        f_setRemoveBtnStatus();
        f_setDownloadRatesheetBtnStatus();
    }
    function f_setRemoveBtnStatus() { f_setBtnStatus(<%= AspxTools.JsGetElementById(RemoveBtn)%>, "Remove"); }
    function f_setDownloadRatesheetBtnStatus() { f_setBtnStatus(<%= AspxTools.JsGetElementById(DownloadRatesheetBtn)%>, "Download"); }
    function f_setBtnStatus(btn, permissionName)
    {
        var tbody = <%= AspxTools.JsGetElementById(DisabledList)%>;

        if (typeof(tbody) == 'undefined' || tbody == null)
        return;
        if (typeof(btn) == 'undefined' || btn == null)
        return;

        if (tbody.rows.length == 1)
        {
        btn.disabled = true;
        return;
        }

        for (var i = 1; i < tbody.rows.length; i++)
        {
        if (tbody.rows[i].children[0].children[0].checked && f_RowAllowsPermission(tbody.rows[i], permissionName))
        {
            btn.disabled = false;
            return;
        }
        }
        btn.disabled = true;
    }

    function f_RowAllowsPermission(row, permissionName)
    {
        var children = row.children;
        for (var i = 0; i < children.length; ++i)
        {
        if (children[i].children.length > 0 && children[i].children[0].className === <%= AspxTools.JsString(PermittedActions)%>)
        {
            return children[i].children[0].value.indexOf(permissionName) >= 0;
        }
        }
        return false;
    }
</script>
<h4 class="page-header">Disable Pricing - <ml:EncodedLiteral ID="LockPolicyNm" runat="server" /></h4>
<form runat="server">
    <asp:Panel id="AccessDeniedPanel" style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
		Access Denied. You do not have access to view this page.
	</asp:Panel>
	<asp:Panel id="AccessGrantedPanel" runat="server">
    <div style="padding: 10px 20px;">
        <input type="button" class="ButtonStyle" style="width: 250px" onclick="f_AddDisabled();" value="Add Product To Disabled Pricing List" />&nbsp;
        <asp:Button ID="RemoveBtn" class="ButtonStyle" style="width: 160px" Text="Remove from the List" runat="server"
            onclick="RemoveBtn_Click" />&nbsp;
        <asp:Button ID="DownloadRatesheetBtn" class="ButtonStyle" style="width: 160px" Text="Download Ratesheet" runat="server"
            onclick="DownloadRatesheetBtn_Click" />&nbsp;
        <input type="button" class="ButtonStyle" onclick="onClosePopup();" value="Close" />
        <br />
        <br />
        <div style="height: 400px; border: inset 2px silver;">
            <table width="100%" style="height: 100%;" border="0" cellpadding="0" cellspacing="0">
                <tr valign="top" style="height: 1%;">
                    <td>
                        <ml:CommonDataGrid ID="DisabledList" EnableViewState="true" BorderWidth="0" CellPadding="2" runat="server" DefaultSortExpression="Investor ASC" OnItemDataBound="DisabledListOnItemDataBound">
                            <AlternatingItemStyle CssClass="GridAlternatingItem"></AlternatingItemStyle>
                            <ItemStyle CssClass="GridItem"></ItemStyle>
                            <HeaderStyle CssClass="GridHeader"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <input type="checkbox" onclick="f_SelectAll(checked);" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="selectItem" runat="server" onclick="f_setAllBtnStatus();"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Investor" ItemStyle-Width="100px" HeaderText="Investor"
                                    SortExpression="Investor" />
                                <asp:TemplateColumn ItemStyle-Width="130px" HeaderText="Product" SortExpression="ProductCode"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton id="pCodeLink" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="LockPolicy" HeaderText="Lock Policy" ItemStyle-Width="80px" SortExpression="LockPolicy" />
                                <asp:BoundColumn HeaderText="Pricing Result" SortExpression="PriceResultLabel" DataField="PriceResultLabel" />
                                <asp:BoundColumn DataField="DisableType" ItemStyle-Width="60px" HeaderText="Enable Method" SortExpression="DisableType" />
                                <asp:BoundColumn DataField="Message" HeaderText="Message to Loan Officer" ItemStyle-Width="100px" SortExpression="Message" />
                                <asp:BoundColumn DataField="Notes" HeaderText="Internal Notes" ItemStyle-Width="100px" SortExpression="Notes" />
                                <asp:BoundColumn DataField="LoginName" HeaderText="Disabled By" ItemStyle-Width="70px" SortExpression="LoginName" />
                                <asp:BoundColumn DataField="TimeStamp" HeaderText="Disabled On" ItemStyle-Width="140px" SortExpression="TimeStamp" />
                                <asp:BoundColumn DataField="PolicyId" Visible="false" />
                                <asp:BoundColumn DataField="DownloadListId" Visible="false" />
                                <asp:TemplateColumn HeaderStyle-CssClass="Hidden" ItemStyle-CssClass="Hidden">
                                    <ItemTemplate>
                                        <input type="hidden" id="PermittedActions" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </ml:CommonDataGrid>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="center">
                        <ml:EncodedLabel ID="EmptyMsg" Font-Italic="true" ForeColor="GrayText" Text="Click the Add button to select to Disable pricing"
                            Visible="false" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
        </div>
        </asp:Panel>
</form>
<uc:cModalDlg ID="modalDlg" runat="server" />
</body>
</html>
