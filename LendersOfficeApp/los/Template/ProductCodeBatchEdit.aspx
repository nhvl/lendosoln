﻿<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductCodeBatchEdit.aspx.cs" Inherits="LendersOfficeApp.Los.Template.ProductCodeBatchEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body onload=onInit();>
    <script language="javascript">
    <!--
		function onInit() {
		    if( document.getElementById("m_errorMessage") != null ) {
		        alert( document.getElementById("m_errorMessage").value );
		    }

		    if( document.getElementById("m_cmdToDo") != null )	{
			    if( document.getElementById("m_cmdToDo").value == "Close" ) {
					onClosePopup();
				}
		    }

			<% if( IsPostBack == false ) { %>

			resize( 900 , 600 );

			<% } %>

		}

        var gCachedKey = <%= AspxTools.JsString(SelectedPrgsCacheKey) %>;
        var  srcProductCodes = <%=AspxTools.JsArray(this.SrcProductCodes??new List<string>()) %>;
        var  newProductCodes = <%=AspxTools.JsArray(this.NewProductCodes??new List<string>()) %>;
        function destProductCodeChange(idx, val)
		{
            newProductCodes[idx] = val;
        }

        function productCodeMappingCheck()
        {
            for(var i=0; i < newProductCodes.length; i++) {
                if( newProductCodes[i] == '') {
                    alert("Please select destination product code for '" + srcProductCodes[i] + "'" );
                    return false;
                }
            }

            document.getElementById("saveNewProductCode").value = newProductCodes.join(";");
            return true;
        }

    //-->
	</script>

    <form id="Underive" runat="server">
        <h1 class="MainRightHeader" >Modify investor/product code for multiple programs.</h1>

        <TABLE height="100%" cellSpacing=4 cellPadding=0 width="100%" border=0>
            <tr><td>
                <TABLE id="Table1" cellspacing="2" cellpadding="2" width="100%" border="1">
                    <tr class="GridHeader">
                        <td> Product Codes From Source Investor  &nbsp 
                            <asp:DropDownList ID="ddlSrcInvestor" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td> Product Codes From Destination Investor&nbsp
                            <asp:DropDownList ID="ddlDestInvestor" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>

		            <asp:repeater id="productCodeRepeater" runat="server" OnItemDataBound="ProductCodeRepeater_OnItemDataBound">
		                <itemtemplate>
			                <tr class="<%# AspxTools.HtmlString(Container.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem") %>" >
			                    <td class="FieldLabel">
                                    <%#AspxTools.HtmlString((string) DataBinder.Eval(Container.DataItem, "ProductCode"))%>
			                    </td>
			                    <td>
                                    <asp:DropDownList ID="ddlDestProductCodes" runat="server" ></asp:DropDownList>
			                    </td>
			                </tr>
		                </itemtemplate>
		            </asp:repeater>
                </TABLE>
            </td></tr>
			<tr>
                <td>&nbsp</td>                
            </tr>

			<tr>
				<td align="center" height=1>
                    <INPUT id="viewSelectedPrgs"  type="button" value="View Selected Prg" onclick="showModal('/los/Template/TemplateSelector.aspx?cmd=view&s='+gCachedKey);">
                    <asp:Button ID="runBtn" runat="server" Text='Run' OnClientClick="return productCodeMappingCheck();" onclick="RunClick" /> 
				</td>
			</tr>

        </TABLE>


        <input type="hidden" id="saveNewProductCode" runat="server" value="none" enableviewstate="false" />
    </form>
</body>
</html>
