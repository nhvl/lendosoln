using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.Template
{
	public partial class LoanProgramInheritance : BasePage
	{
		protected LendersOffice.Common.CompositeUserControl  m_Derived;

		private BrokerUserPrincipal BrokerUser
		{
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{
			set
			{
				Page.ClientScript.RegisterHiddenField( "m_errorMessage" , value );
			}
		}
        private IDictionary<Guid, BrokerDbLite> m_brokerDictionary = null;
        protected string DisplayBrokerName(Guid brokerId)
        {
            return m_brokerDictionary[brokerId].BrokerNm;
        }
		/// <summary>
		/// Associate inheritance tree to a data grid.
		/// </summary>

		private void BindDataGrid()
		{
			// Expect each row of each table to be a link in
			// the inheritance chain.  Must use a dataset and
			// fill to pull this off.  We then swap the order
			// so our product is on the bottom.
            m_brokerDictionary = BrokerDbLite.ListAllSlow();

			ArrayList aL = new ArrayList();
			DataSet   pS = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter( "@ProductId" , Request[ "fileId" ] )
                                        };
			DataSetHelper.Fill(pS, DataSrc.LpeSrc, "ListLoanProductInheritance", parameters);

			for( int i = 0 ; i < pS.Tables.Count ; ++i )
			{
				aL.Insert( 0 , pS.Tables[ i ].Rows[ 0 ].ItemArray );
			}

			pS.Tables[ 0 ].Rows.Clear();

			foreach( Object[] iA in aL )
			{
				pS.Tables[ 0 ].Rows.Add( iA );
			}

			m_Parents.DataSource = pS.Tables[ 0 ].DefaultView;
			m_Parents.DataBind();

			// 2/14/2005 kb - Now bind the deriveds list.  We show the
			// entire derivation tree (laterally) given the current
			// loan product.

			DerivationTree dT = new DerivationTree();

			try
			{
				dT.Retrieve( new Guid( Request[ "fileId" ] ) , string.Empty, m_brokerDictionary );
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to load derivation tree." , e );
			}

			m_Derived.DataSource = dT.Tree;
			m_Derived.DataBind();
		}

		/// <summary>
		/// Investigate product by its row and see if it is a base.
		/// If not derived, then show a capping symbol.  If the
		/// current product, then show a special marker.
		/// </summary>

		protected String GetDerivationSymbol( Object oRow )
		{
			// Get the row and look for the base and self ids.

			DataRowView dRow = oRow as DataRowView;

			if( dRow != null )
			{
				if( dRow[ "lBaseLpId" ] is DBNull )
				{
					return "\u25cf";
				}
			}

			return "\u25b2";
		}

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Initialize this page by binding the grid.

            // Secure the view and only bind when not
            // denied.

            bool isDenied = false;

            if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false)
            {
                isDenied = true;
            }

            if (isDenied == false)
            {
                // Bind the grid because we depend on the tree to be
                // initialized when determining if empty or not.

                BindDataGrid();

                // Show the empty panels if the results are empty.

                if (m_Parents.Items.Count == 0)
                {
                    m_Parents.Visible = false;
                    m_ParentsEmpty.Visible = true;
                }
                else
                {
                    m_Parents.Visible = true;
                    m_ParentsEmpty.Visible = false;
                }

                if (m_Derived.Tree.Count == 0)
                {
                    m_Derived.Visible = false;
                    m_DerivedEmpty.Visible = true;
                }
                else
                {
                    m_Derived.Visible = true;
                    m_DerivedEmpty.Visible = false;
                }

                m_ParentsDenied.Visible = false;
            }
            else
            {
                m_ParentsDenied.Visible = true;
                m_Parents.Visible = false;
                m_ParentsEmpty.Visible = false;

                m_Derived.Visible = false;
                m_DerivedEmpty.Visible = false;
            }

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}

	/// <summary>
	/// Build up a derivation tree given the current loan product
	/// identifier.  We build up a ui tree that can be used for
	/// binding to a composite control.
	/// </summary>

	public class DerivationTree
	{
		/// <summary>
		/// Build up a derivation tree given the current loan
		/// product identifier.  We build up a ui tree that can
		/// be used for binding to a composite control.
		/// </summary>

		private CompositeTree m_Tree = new CompositeTree();

		public CompositeTree Tree
		{
			// Access member.

			get
			{
				return m_Tree;
			}
		}

		/// <summary>
		/// Keep track of each loan product's content in the tree.
		/// </summary>

		private class Node
		{
			/// <summary>
			/// Keep track of each loan product's content in the
			/// tree.
			/// </summary>

			private String   m_Name = String.Empty;
			private String m_Broker = String.Empty;
			private String m_CustomerCode = String.Empty; // OPM 18191
			private Guid       m_Id = Guid.Empty;

			#region ( Node Properties )

			public String Name
			{
				// Access member.

				set
				{
					m_Name = value;
				}
				get
				{
					return m_Name;
				}
			}

			public String Broker
			{
				// Access member.

				set
				{
					m_Broker = value;
				}
				get
				{
					return m_Broker;
				}
			}

			public String CustomerCode
			{
				set	{ m_CustomerCode = value; }
				get { return m_CustomerCode; }
			}

			public Guid Id
			{
				// Access member.

				set
				{
					m_Id = value;
				}
				get
				{
					return m_Id;
				}
			}

			#endregion

			/// <summary>
			/// Return default value of this node's identifier.
			/// </summary>

			public override string ToString()
			{
				// Return default as the id of this node.

				return m_Id.ToString();
			}

			#region ( Constructors )

			/// <summary>
			/// Construct default.
			/// </summary>

			public Node( String sName , String sBroker , Guid uId, String CustomerCode )
			{
				// Initialize members.

				m_Name   = sName;
				m_Broker = sBroker;
				m_Id     = uId;
				m_CustomerCode = CustomerCode;
			}

			/// <summary>
			/// Construct default.
			/// </summary>

			public Node()
			{
			}

			#endregion

		}

		/// <summary>
		/// Build tree using the given loan product identifier.
		/// </summary>

		public void Retrieve( Guid loanProductId , string sLabel, IDictionary<Guid, BrokerDbLite> brokerDictionary )
		{
			try
			{
				// Initialize our tree with the loan product as a starting
				// node.  All children will branch off this root.

				CompositeNode rootN = new CompositeNode( loanProductId , new Node( sLabel , "" , loanProductId, "" ) );

				if( sLabel != "" )
				{
					m_Tree.Add( rootN );
				}

				// Build up a recursive list of unique composite ids so
				// we can systematically build the tree.  We check the
				// hash table to see if we already grabbed someone.  A
				// cycle like this should *never* happen.

				Hashtable lookUp = new Hashtable();
				ArrayList idList = new ArrayList();

				lookUp.Add( rootN.Id , rootN );

				idList.Add( rootN.Id );

				for( int i = 0 ; i < idList.Count ; ++i )
				{
					CompositeNode parentN = lookUp[ idList[ i ] ] as CompositeNode;

					using( DbDataReader sR = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListLoanProductDerivations" , new SqlParameter( "@ProductId" , idList[ i ] ) ) )
					{
						while( sR.Read() == true )
						{
							// Pull the child product's details from the reader.
							// We construct a matching ui node element if the id
							// is intact and not empty.

							Node childProd = new Node();

							childProd.Id = ( Guid ) sR[ "lLpTemplateId" ];

							if( childProd.Id != Guid.Empty )
							{
                                Guid brokerId = (Guid)sR["BrokerId"];
                                BrokerDbLite brokerDbLite = brokerDictionary[brokerId];

								childProd.Name   = sR[ "lLpTemplateNm" ].ToString();
                                childProd.Broker = brokerDbLite.BrokerNm;;
								childProd.CustomerCode = brokerDbLite.CustomerCode;

								if( lookUp.Contains( childProd.Id ) == true )
								{
									continue;
								}
							}

							// Enter new ui node element for this current child
							// product.  This guy will come around later in the
							// loop.

							CompositeNode childN = new CompositeNode();

							childN.Id   = childProd.Id;
							childN.Data = childProd;

							lookUp.Add( childN.Id , childN );

							idList.Add( childProd.Id );

							parentN.Add( childN );
						}
					}
				}

				// Transfer nodes if we opted to hide the root
				// node from view.  If so, then we want each direct
				// child to be flush left as a starting branch.

				if( sLabel == "" )
				{
					foreach( CompositeNode childN in rootN )
					{
						m_Tree.Add( childN );
					}
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Unable to build composite tree for loan product " + loanProductId + "." , e );

				throw;
			}
		}

	}

}
