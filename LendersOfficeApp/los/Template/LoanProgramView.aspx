<%@ Page language="c#" Codebehind="LoanProgramView.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.LoanProgramView" %>
<%@ Register TagPrefix="uc" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>LoanProgramView</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="FlowLayout" onload="init();">
    <script language="javascript">
            <!--
            function init()
            {
				
              resize( 780 , 550 );
				

              self.focus();
            }
			function printMe()
            {
                self.focus();
                window.print();
            }
            //-->
    </script>
    <form id="LoanProgramView" method="post" runat="server">
      <TABLE class="FormTable" id="Table10" cellSpacing="0" cellPadding="0" width="100%" border="0">
        <TR>
          <TD>
            <TABLE id="Table12" cellSpacing="0" cellPadding="0" border="0">
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Loan Program Name</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lLpTemplateNmLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Loan Type</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lLTLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Lender Name</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lLenderNmLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Investor Name</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lLpInvestorNmLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Term/Due</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lTermLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Finance Method</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lFinMethTLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Finance Description</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lFinMethDescLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Lien Position</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lLienPosTLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Note Rate</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lNoteRLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Qualify Rate</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lQualRLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Points</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lLOrigFPcLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD class="FieldLabel" style="WIDTH: 166px">Lock Period</TD>
                <TD class="FieldLabel">
                  <ml:EncodedLabel id="lLockedDaysLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
              <TR>
                <TD style="WIDTH: 166px"></TD>
                <TD></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD>
            <TABLE id="Table1" style="BORDER-RIGHT: gray 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: gray 1px solid; MARGIN-TOP: 5px; PADDING-LEFT: 5px; FONT-SIZE: 10pt; MARGIN-BOTTOM: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: gray 1px solid; MARGIN-RIGHT: 5px; PADDING-TOP: 5px; BORDER-BOTTOM: gray 1px solid" cellSpacing="0" cellPadding="0" border="0">
			<TR>
            <TD style="FONT-WEIGHT: bold" colSpan="9">Adjustable Rate Mortgage</TD>
            </TR>
            <TR>
            <TD>1st Adjust Cap</TD>
            <TD>1st Change</TD>
            <TD>Adjust Cap</TD>
            <TD>Adjust Period</TD>
			<TD>Life Cap</TD>
			<TD>Margin</TD>
			<TD>Index</TD>
			<TD>Floor</TD>
			<TD nowrap>Round <ml:EncodedLabel id="lRAdjRound" runat="server"></ml:EncodedLabel>&nbsp;to</TD>
			</TR>
			<TR>
			<TD>
				<ml:EncodedLabel id="lRAdj1stCapRLabel" runat="server"></ml:EncodedLabel></TD>
			<TD>
				<ml:EncodedLabel id="lRAdj1stCapMonLabel" runat="server"></ml:EncodedLabel></TD>
			<TD>
				<ml:EncodedLabel id="lRAdjCapRLabel" runat="server"></ml:EncodedLabel></TD>
			<TD>
				<ml:EncodedLabel id="lRAdjCapMonLabel" runat="server"></ml:EncodedLabel></TD>
			<TD>
				<ml:EncodedLabel id="lRAdjLifeCapRLabel" runat="server"></ml:EncodedLabel></TD>
			<TD>
				<ml:EncodedLabel id="lRAdjMarginRLabel" runat="server"></ml:EncodedLabel></TD>
			<TD>
				<ml:EncodedLabel id="lRAdjIndexRLabel" runat="server"></ml:EncodedLabel></TD>
			<TD>
				<ml:EncodedLabel id="lRAdjFloorRLabel" runat="server"></ml:EncodedLabel></TD>
			<TD>
				<ml:EncodedLabel id="lRAdjRoundToRLabel" runat="server"></ml:EncodedLabel></TD>
			</TR>
			<TR>
			<TD colspan="9">
				<asp:Panel id="lArmIndexPanel" runat="server">
					<div style="MARGIN-BOTTOM: 8px; WIDTH: 100%;">
						<b>
							ARM Index
						</b>
						<div style="WIDTH: 400px; PADDING: 4px;">
							<ml:EncodedLabel id="lArmInddexNameVstrLabel" runat="server">
							</ml:EncodedLabel>
						</div>
					</div>
					<div style="MARGIN-BOTTOM: 8px; WIDTH: 100%;">
						<b>
							The interest will be based on
						</b>
						<div style="WIDTH: 500px; PADDING: 4px;">
							<ml:EncodedLabel id="lArmIndexBasedOnVstrLabel" runat="server">
							</ml:EncodedLabel>
						</div>
						<br>
						<b>
							Information about the index can be found
						</b>
						<div style="WIDTH: 500px; PADDING: 4px;">
							<ml:EncodedLabel id="lArmIndexCanBeFoundVstrLabel" runat="server">
							</ml:EncodedLabel>
						</div>
					</div>
					<div style="MARGIN-BOTTOM: 8px; PADDING: 4px; WIDTH: 500px;">
						Borrower will be notified in writing of index rate changes
						at least
						[
						<ml:EncodedLabel id="lArmIndexNotifyAtLeastDaysVstrLabel" runat="server">
						</ml:EncodedLabel>
						]
						days, but not more than
						[
						<ml:EncodedLabel id="lArmIndexNotifyNotBeforeDaysVstrLabel" runat="server">
						</ml:EncodedLabel>
						]
						days before the due date of a payment at a new level.
					</div>
					<div style="MARGIN-BOTTOM: 0px; WIDTH: 100%;" onclick="return false;">
						<asp:CheckBox id="lArmIndexAffectInitIRBitCheck" runat="server" Text="Your initial interest rate is not based on the index">
						</asp:CheckBox>
					</div>
				</asp:Panel>
			</TD>
			</TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD>
            <TABLE id="Table4" style="BORDER-RIGHT: gray 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: gray 1px solid; MARGIN-TOP: 5px; PADDING-LEFT: 5px; FONT-SIZE: 10pt; MARGIN-BOTTOM: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: gray 1px solid; MARGIN-RIGHT: 5px; PADDING-TOP: 5px; BORDER-BOTTOM: gray 1px solid" cellSpacing="0" cellPadding="0" border="0">
              <TR>
                <TD style="FONT-WEIGHT: bold" colSpan="5">Adjustable Payment Mortgage</TD>
              </TR>
              <TR>
                <TD>Adjust Cap</TD>
                <TD>Adjust Period</TD>
                <TD>Recast Period</TD>
                <TD>Recast Stop
                </TD>
                <TD>Max Balance</TD>
              </TR>
              <TR>
                <TD>
                  <ml:EncodedLabel id="lPmtAdjCapRLabel" runat="server"></ml:EncodedLabel></TD>
                <TD>
                  <ml:EncodedLabel id="lPmtAdjCapMonLabel" runat="server"></ml:EncodedLabel></TD>
                <TD>
                  <ml:EncodedLabel id="lPmtAdjRecastPeriodMonLabel" runat="server"></ml:EncodedLabel></TD>
                <TD>
                  <ml:EncodedLabel id="lPmtAdjRecastStopLabel" runat="server"></ml:EncodedLabel></TD>
                <TD>
                  <ml:EncodedLabel id="lPmtAdjMaxBalLabel" runat="server"></ml:EncodedLabel></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD>
            <TABLE id="Table11" cellSpacing="0" cellPadding="0" width="300" border="0">
              <TR>
                <TD vAlign="top">
                  <TABLE id="Table6" style="BORDER-RIGHT: gray 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: gray 1px solid; PADDING-LEFT: 5px; FONT-SIZE: 10pt; PADDING-BOTTOM: 5px; MARGIN: 5px; MARGIN-LEFT: 0px; BORDER-LEFT: gray 1px solid; WIDTH: 148px; PADDING-TOP: 5px; BORDER-BOTTOM: gray 1px solid; HEIGHT: 192px" cellSpacing="0" cellPadding="0" width="148" border="0">
                    <TR>
                      <TD style="FONT-WEIGHT: bold" colSpan="3">Buydown Mortgage</TD>
                    </TR>
                    <TR>
                      <TD style="WIDTH: 16px"></TD>
                      <TD style="WIDTH: 45px">Rate:</TD>
                      <TD style="WIDTH: 137px">Term (mths):</TD>
                    </TR>
                    <TR>
                      <TD style="WIDTH: 16px">1</TD>
                      <TD style="WIDTH: 45px">
                        <ml:EncodedLabel id="lBuydwnR1Label" runat="server"></ml:EncodedLabel></TD>
                      <TD style="WIDTH: 137px">
                        <ml:EncodedLabel id="lBuydwnMon1Label" runat="server"></ml:EncodedLabel></TD>
                    </TR>
                    <TR>
                      <TD style="WIDTH: 16px">2</TD>
                      <TD style="WIDTH: 45px">
                        <ml:EncodedLabel id="lBuydwnR2Label" runat="server"></ml:EncodedLabel></TD>
                      <TD style="WIDTH: 137px">
                        <ml:EncodedLabel id="lBuydwnMon2Label" runat="server"></ml:EncodedLabel></TD>
                    </TR>
                    <TR>
                      <TD style="WIDTH: 16px">3</TD>
                      <TD style="WIDTH: 45px">
                        <ml:EncodedLabel id="lBuydwnR3Label" runat="server"></ml:EncodedLabel></TD>
                      <TD style="WIDTH: 137px">
                        <ml:EncodedLabel id="lBuydwnMon3Label" runat="server"></ml:EncodedLabel></TD>
                    </TR>
                    <TR>
                      <TD style="WIDTH: 16px">4</TD>
                      <TD style="WIDTH: 45px">
                        <ml:EncodedLabel id="lBuydwnR4Label" runat="server"></ml:EncodedLabel></TD>
                      <TD style="WIDTH: 137px">
                        <ml:EncodedLabel id="lBuydwnMon4Label" runat="server"></ml:EncodedLabel></TD>
                    </TR>
                    <TR>
                      <TD style="WIDTH: 16px">5</TD>
                      <TD style="WIDTH: 45px">
                        <ml:EncodedLabel id="lBuydwnR5Label" runat="server"></ml:EncodedLabel></TD>
                      <TD style="WIDTH: 137px">
                        <ml:EncodedLabel id="lBuydwnMon5Label" runat="server"></ml:EncodedLabel></TD>
                    </TR>
                  </TABLE>
                </TD>
                <TD style="PADDING-RIGHT: 10px;" vAlign="top">
                  <TABLE id="Table7" style="BORDER-RIGHT: gray 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: gray 1px solid; PADDING-LEFT: 5px; FONT-SIZE: 10pt; PADDING-BOTTOM: 5px; MARGIN: 5px; BORDER-LEFT: gray 1px solid; WIDTH: 153px; PADDING-TOP: 5px; BORDER-BOTTOM: gray 1px solid; HEIGHT: 69px" cellSpacing="0" cellPadding="0" width="153" border="0">
                    <TR>
                      <TD style="FONT-WEIGHT: bold" colSpan="2">Other Arrangements</TD>
                    </TR>
                    <TR>
                      <TD style="WIDTH: 16px">Years:</TD>
                      <TD style="WIDTH: 45px">
                        <ml:EncodedLabel id="lGradPmtYrsLabel" runat="server"></ml:EncodedLabel></TD>
                    </TR>
                    <TR>
                      <TD style="WIDTH: 16px">Rate:</TD>
                      <TD style="WIDTH: 45px">
                        <ml:EncodedLabel id="lGradPmtRLabel" runat="server"></ml:EncodedLabel></TD>
                    </TR>
                  </TABLE>
                  <TABLE id="Table9" style="BORDER-RIGHT: gray 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: gray 1px solid; PADDING-LEFT: 5px; FONT-SIZE: 10pt; PADDING-BOTTOM: 5px; MARGIN: 5px; MARGIN-TOP: 10px; BORDER-LEFT: gray 1px solid; WIDTH: 196px; PADDING-TOP: 5px; BORDER-BOTTOM: gray 1px solid; HEIGHT: 70px" cellSpacing="0" cellPadding="0" width="196" border="0">
                    <TR>
                      <TD style="FONT-WEIGHT: bold" colSpan="2">Graduated Payment Mortgage</TD>
                    </TR>
                    <TR>
                      <TD style="WIDTH: 101px" noWrap>Interest only(mths):</TD>
                      <TD style="WIDTH: 45px">
                        <ml:EncodedLabel id="lIOnlyMonLabel" runat="server"></ml:EncodedLabel></TD>
                    </TR>
                  </TABLE>
                </TD>
                <TD vAlign="top"></TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD onclick="return false;">
            <asp:checkbox id="lAprIncludesReqDeposit_chk" runat="server" Width="603px" Text="Required deposit: The annual percentage rate does not take into account your required deposit."></asp:checkbox></TD>
        </TR>
        <TR>
          <TD onclick="return false;">
            <asp:checkbox id="lHasDemandFeature_chk" runat="server" Width="603px" Text="Demand feature: This obligation has a demand feature."></asp:checkbox></TD>
        </TR>
        <TR>
          <TD onclick="return false;">
            <asp:checkbox id="lHasVarRFeature_chk" runat="server" Width="717px" Text="Variable rate feature: This loan contains a variable rate feature. A variable rate disclosure has been provided earlier."></asp:checkbox></TD>
        </TR>
        <TR>
          <TD>
            <ml:EncodedLabel id="lVarRNotesLabel" runat="server"></ml:EncodedLabel></TD>
        </TR>
        <TR>
          <TD style="FONT-SIZE: 10pt"><STRONG>Filing Fees:</STRONG>
            <ml:EncodedLabel id="lFilingFLabel" runat="server"></ml:EncodedLabel></TD>
        </TR>
        <TR>
          <TD style="FONT-SIZE: 10pt"></TD>
        </TR>
        <TR>
        <TD style="FONT-SIZE: 10pt"><STRONG>Late charge:</STRONG> If a payment is more 
            than
            <ml:EncodedLabel id="lLateDaysLabel" runat="server">
            </ml:EncodedLabel>
            days late, you will be charged
            <ml:EncodedLabel id="lLateChargePcLabel" runat="server">
            </ml:EncodedLabel>
            of
            <ml:EncodedLabel id="lLateChargeBaseLabel" runat="server">
            </ml:EncodedLabel>
        </TD>
        </TR>
        <TR>
          <TD style="FONT-SIZE: 10pt"><STRONG>Prepayment:</STRONG> If you pay off early 
            you,</TD>
        </TR>
        <TR>
          <TD>
            <TABLE id="Table13" style="FONT-SIZE: 10pt; WIDTH: 602px" cellSpacing="0" cellPadding="0" width="602" border="0">
              <TR>
                <TD style="WIDTH: 48px; HEIGHT: 31px">
					<ml:EncodedLabel id="lPrepmtPenaltyTLabel" runat="server">
					</ml:EncodedLabel>
				</TD>
				<TD style="HEIGHT: 31px">
					have to pay a penalty.
					<asp:Panel id="lPrepmtPenaltyTPanel" runat="server" style="DISPLAY: inline;">
						Prepayment penalty will be in force for first
						<ml:EncodedLabel id="lPrepmtPenaltyMonLabel" runat="server">
						</ml:EncodedLabel>
						months.
					</asp:Panel>
				</TD>
              </TR>
              <TR>
                <TD style="WIDTH: 48px">
                  <ml:EncodedLabel id="lPrepmtRefundTLabel" runat="server"></ml:EncodedLabel></TD>
                <TD>be entitled to a refund of part of the finance charge.</TD>
              </TR>
            </TABLE>
          </TD>
        </TR>
        <TR>
          <TD style="FONT-SIZE: 10pt"><STRONG>Assumption:</STRONG> Someone buying your 
            property&nbsp;&nbsp;
            <ml:EncodedLabel id="lAssumeLTLabel" runat="server"></ml:EncodedLabel>&nbsp;assume the 
            remainder of your loan on the original terms.</TD>
        </TR>
        <TR>
          <TD></TD>
        </TR>
      </TABLE>
      <uc:CModalDlg runat="server">
      </uc:CModalDlg>
    </form>
  </body>
</html>
