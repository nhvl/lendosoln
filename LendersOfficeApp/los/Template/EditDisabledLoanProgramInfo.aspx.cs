using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.RatePrice;

namespace LendersOfficeApp.los.Template
{
	/// <summary>
	/// Summary description for EditDisabledLoanProgramInfo.
	/// </summary>
	public partial class EditDisabledLoanProgramInfo : LendersOffice.Common.BasePage
	{
		private string m_action;
		protected System.Web.UI.HtmlControls.HtmlButton m_OkSeen;

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private void BindInvestorList()
		{
            var list = InvestorNameUtils.ListActiveInvestorNames(this.CurrentUser.BrokerDB);

			m_InvestorName.DataSource = list;
			m_InvestorName.DataTextField = "InvestorName";
			m_InvestorName.DataBind();
			m_InvestorName.Items.Insert(0, new ListItem("<-- Choose Investor -->", String.Empty));
		}

		private void BindProductCodeList()
		{
            DataSet ds = new DataSet();
            SqlParameter[] parameters = {
                                            new SqlParameter("@InvestorName", m_InvestorName.SelectedItem.Value)
                                        };
            DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListProductCodesByInvestorName", parameters);

			m_ProductCode.DataSource = ds.Tables[0].DefaultView;
			m_ProductCode.DataTextField = "ProductCode";
			m_ProductCode.DataBind();
			m_ProductCode.Items.Insert(0, new ListItem("<-- Choose Product Code -->", String.Empty));
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			m_action = RequestHelper.GetSafeQueryString("cmd");
			string pCode = RequestHelper.GetSafeQueryString("pcode");
			string investorName = RequestHelper.GetSafeQueryString("investorname");

			if(!IsPostBack)
			{
				if ( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == false)
				{
					m_OkSeen.Visible = false;
				}

				BindInvestorList();
				m_ProductCode.Items.Insert(0, new ListItem("<-- Choose Product Code -->", String.Empty));

				if (null != m_action && m_action.ToLower() == "edit") 
				{
					if (null != pCode && null != investorName) 
					{
						SqlParameter[] parameters = { new SqlParameter("@ProductCode", pCode), new SqlParameter("@InvestorName", investorName) };					
						using( DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "GetDisabledLoanProgramInfo", parameters ) )
						{
							if ( reader.Read() )
							{
								string invName = reader["InvestorName"].ToString();
								m_InvestorName.SelectedIndex = m_InvestorName.Items.IndexOf(m_InvestorName.Items.FindByValue(invName));
								BindProductCodeList();
								string prodCode = reader["ProductCode"].ToString();
								m_ProductCode.SelectedIndex = m_ProductCode.Items.IndexOf(m_ProductCode.Items.FindByValue(prodCode));
								m_SaeName.Text = reader["SaeName"].ToString();
								m_Notes.Text = reader["Notes"].ToString();
							}
						}
						
						m_ProductCode.Enabled = false;
						m_InvestorName.Enabled = false;
					}
					else
					{
						if(null == pCode)
							m_ErrorMessage.Text = "Invalid product code";
						else
							m_ErrorMessage.Text = "Invalid investor name";
						m_OkSeen.Visible = false;
					}
				}
			}
			else
			{
				if(!m_Command.Value.Equals("ok"))
					BindProductCodeList();
			}
		}

		protected void OKClick(object sender, System.EventArgs e)
		{
			if( !Page.IsValid )
				return;

			string procedureName;
			if (null != m_action && m_action.ToLower() == "add") 
			{
				procedureName = "CreateDisabledLoanProgramInfo";
				SqlParameter[] parameters = { new SqlParameter("@ProductCode", m_ProductCode.SelectedItem.Value), new SqlParameter("@InvestorName", m_InvestorName.SelectedItem.Value) };									
				using( DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "CheckIfProductCodeValidByInvestorName", parameters ) )
				{
					if ( !reader.Read() )
					{
						m_ErrorMessage.Text = "Invalid Product Code";
						return;
					}
				}
				SqlParameter[] parameters2 = { new SqlParameter("@ProductCode", m_ProductCode.SelectedItem.Value), new SqlParameter("@InvestorName", m_InvestorName.SelectedItem.Value) };					
				using( DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "GetDisabledLoanProgramInfo", parameters2 ) )
				{
					if ( reader.Read() )
					{
						m_ErrorMessage.Text = "There is already an entry for this loan program and investor name";
						return;
					}
				}
			}
			else if(null != m_action && m_action.ToLower() == "edit")
				procedureName = "UpdateDisabledLoanProgramInfo";
			else
			{
				LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "OK", "true") ;
				return;
			}

			int result = 0;
			try
			{
				SqlParameter[] parameters3 = { 
												 new SqlParameter("@ProductCode", m_ProductCode.SelectedItem.Value), 
												 new SqlParameter("@InvestorName", m_InvestorName.SelectedItem.Value), 
												 new SqlParameter("@DisableStatus", m_DisabledStatus.Text), 
												 new SqlParameter("@SaeName", m_SaeName.Text), 
												 new SqlParameter("@Notes", m_Notes.Text)
											 };													
				result = StoredProcedureHelper.ExecuteNonQuery
					(DataSrc.LpeSrc
					, procedureName
					, 1
					, parameters3);
			}
			catch(Exception exc)
			{
				Tools.LogError(exc);
				result = 0;
			}
			if (result > 0)
				LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "OK", "true") ;
			else 
				m_ErrorMessage.Text = "Unable to save the disabled loan program info.";
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
