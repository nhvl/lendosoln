<%@ Import Namespace="LendersOffice.Common"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="LoanProgramList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.LoanProgramList" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>LoanProgramList</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<STYLE type="text/css">.contextoverridden { FONT-WEIGHT: bold }
		</STYLE>
	</HEAD>
	<body bgColor="gainsboro" scroll="yes" onload="init();" MS_POSITIONING="FlowLayout">
	<script type="text/javascript">
      function init()
      {
		resize(750, 540);
		
		if( document.getElementById("m_errorMessage") != null && document.getElementById("m_errorMessage").value != "" )
		{
			alert( document.getElementById("m_errorMessage").value );
		}

		if( document.getElementById("m_feedBack") != null && document.getElementById("m_feedBack").value != "" )
		{
			alert( document.getElementById("m_feedBack").value );
		}
		
		disableEnableButtons();
		
		// OPM 16984 Cord		
		load_ProductCode_Data();

		// OPM 471850. Let IE open dialog in new window.
		document.getElementById("TransferForm").target = (isInternetExplorer() ? "_blank" : "LpBatchEditIFrame");
		
      }    

		  function onEditClick(fileID, productCode, investorName)
		  {
				var win = window.open( VRoot + "/los/Template/LoanProgramTemplate.aspx?fileid=" + fileID 
					+ "&mode=edit" + "&productCode=" + escape( productCode ) + "&investorName=" + escape( investorName )
					, "_blank" , "resizable=yes,status=yes,scrollbars=yes" );
				var winTimer = setInterval(function() {
				    if (win.closed) {
				        window.location.reload();
				        clearInterval(winTimer);
				    }
				}, 100);
		  }
		  
		  function onViewClick(fileID)
		  {
			    showModalTopLevel('/los/Template/LoanProgramView.aspx?fileid=' + fileID);
		  }

		  function onGuidelineClick(fileID) 
		  {
			    showModalTopLevel('/los/Template/GuidelineList.aspx?fileid=' + fileID);
		  }
		  function onPricingClick(fileID, ProductName) 
		  {
		  	  showModalTopLevel('/los/RatePrice/PricePolicyListByProduct.aspx?FileId=' + fileID + "&ProductName=" + escape( ProductName ),
			      null, null, true);
		  }
		  function onInheritanceClick(fileID)
		  {
		      showModalTopLevel('/los/Template/LoanProgramInheritance.aspx?fileid=' + fileID, null, null, null, null, {hideCloseButton: true});
		  }
		  function onRatesheetClick(fileID) 
		  {
		      showModalTopLevel('/los/Template/EditRateSheet.aspx?fileId=' + fileID, null, null, null, null, {hideCloseButton: true});
		  }		  
		  function onCCClick(ccID) {
			    showModalTopLevel('/los/Template/CCTemplate.aspx?ccid=' + ccID, null, null, null, function(){
					self.location = self.location;
				});
		  }
		  function validateCheckList() {
				var collection = getAllElementsByIdFromParent(document, "templateid");
				var isChecked = false;
				var length = collection.length;
				for (var i = 0; i < length; i++) {
					if (collection[i].checked == true) {
						isChecked = true;
						break;
					}
				}

				if (!isChecked) {
					alert('No loan program is selected.');
					return false;
				}    
				
				<% if (m_forEditor.Visible) { %>
				var deleteDerived = <%= AspxTools.JsGetElementById(m_deleteDerivedCb) %>;
				if ( deleteDerived && deleteDerived.checked == true )
				{
				  var code = Math.floor(Math.random()*1000) + '';
				  var ans = prompt("WARNING: Please be sure that you want to perform this *irreversible* deletion!  Enter '" + code+ "' below to confirm.", "");
				  var doIt = (ans != null &&  ans == code );
				  
				  if ( doIt == false)
				    alert("Code does not match.  Deletions will not be performed.");
				  
				  return doIt;
				}
				<% } %>
				
				return confirm('Do you want to delete these selected loan programs?');
		  }
		  function checkAll()
		  {
				var collection = getAllElementsByIdFromParent(document, "templateid");
				if(collection != null)
				{
					var bChecked = document.getElementById("chkCheckAll").checked ;
				
					var length = collection.length;
					for (var i = 0; i < length; i++) {
						collection[i].checked = bChecked ;
					}
				}
				disableEnableButtons();
		  }
		  
		  function disableEnableButtons()
		  {	
				var collection = getAllElementsByIdFromParent(document, "templateid");
				var dupButton = <%= AspxTools.JsGetElementById(m_duplicateBtn) %>;
				var delButton = <%= AspxTools.JsGetElementById(m_deleteBtn) %>;
				var delDerived = <%= AspxTools.JsGetElementById(m_deleteDerivedCb) %>;
				var batButton = document.getElementById("m_batchEdit");
				var addMasterButton = document.getElementById("masterAdd");
				var masterIsChecked = isMasterChecked();
				var hasSubfolders = <%= AspxTools.JsBool(m_hasSubfolders)%>;
				
				if(addMasterButton)
				{
					addMasterButton.disabled = (folderHasMaster())?true:false;
					addMasterButton.title = (folderHasMaster())? <%= AspxTools.JsString(JsMessages.FolderAlreadyContainsMaster)%>:"";
				}
				
				if(delButton)
					delButton.title = "";
				if(dupButton)
					dupButton.title = "";
				if(collection != null)
				{
					var length = collection.length;
					var count = 0;
					for (var i = 0; i < length; i++) 
					{
						if(collection[i].checked)
							++count;
					}
					if(collection.length > 1 && count > 0) //if there are more than one items in the list (datagrid)
					{
						if(delButton)
						{	
							if(masterIsChecked)
							{
								delButton.disabled = true;
								if (delDerived) delDerived.disabled = true;
								delButton.title = <%=AspxTools.JsString(JsMessages.CannotDeleteMaster_OtherProgramsInFolder)%>;
							}
							else
							{
								delButton.disabled = false;
								if (delDerived) delDerived.disabled = false;
							}
						}
						if(batButton)
							batButton.disabled = false;
						if(dupButton)
						{
							if(count != 1)
							{
								dupButton.disabled = true;
								dupButton.title = <%=AspxTools.JsString(JsMessages.CannotDuplicateMultiple)%>;
							}
							else if(masterIsChecked)
							{
								dupButton.disabled = true;
								dupButton.title = <%=AspxTools.JsString(JsMessages.CannotDuplicateMaster)%>;
							}
							else
								dupButton.disabled = false;
						}
						return;
					} 
					else if(collection.length == 1 && count == 1) //if there is only one item in the list (datagrid)
					{
						if(dupButton)
						{
							if(masterIsChecked)
							{
								dupButton.disabled = true;
								dupButton.title = <%=AspxTools.JsString(JsMessages.CannotDuplicateMaster)%>;
							}
							else
								dupButton.disabled = false;
						}
						if(delButton)
						{
							if(masterIsChecked && hasSubfolders)
							{
								delButton.disabled = true;
								delButton.title = <%=AspxTools.JsString(JsMessages.CannotDeleteMaster_HasSubfolders)%>;
								if (delDerived) delDerived.disabled = true;
							}
							else
							{
								delButton.disabled = false;
								if (delDerived) delDerived.disabled = false;
							}
						}
						if(batButton)
							batButton.disabled = false;
						return; 
					}
				}
					
				if(dupButton)
				dupButton.disabled = true;
				if(delButton)
					delButton.disabled = true;
				if(batButton)
					batButton.disabled = true;
				if (delDerived) 
					delDerived.disabled = true;
		  }
		  
		  function folderHasMaster()
		  {
			var collection = getAllElementsByIdFromParent(document, "templateid");
			if(collection != null)
			{
				var length = collection.length;
				for (var i = 0; i < length; i++) 
				{
					if(collection[i].value == <%=AspxTools.JsString(m_masterId)%>)
						return true;
				}
			}
			return false;
		  }
		  
		  function isMasterChecked()
		  {
			var collection = getAllElementsByIdFromParent(document, "templateid");
			if(collection != null)
			{
				var length = collection.length;
				for (var i = 0; i < length; i++) 
				{
					if(collection[i].checked)
					{
						if(collection[i].value == <%=AspxTools.JsString(m_masterId)%>)
							return true;
					}
				}
			}
			return false;
		  }
		  
		  function onBatchEdit()
		  {
				var collection = getAllElementsByIdFromParent(document, "templateid");
				var length = collection.length;
				var sIds = "" ;
				for (var i = 0; i < length; i++) {
					if (collection[i].checked)
						sIds += collection[i].value + "," ;
				}
				
				if (sIds == "")
				{
					alert('No loan program is selected.');
					return false;
				}
				
				TransferForm.LpIds.value = sIds.substring(0, sIds.length-1) ; // strip the last comma
				if(!isInternetExplorer()){
					$('#LpBatchEditPopup').dialog({
				        resizable: true,
				        modal: true,
				        width: 1000,
				        height: 700,
				        position: {my: "center", at: "center"},
				        open: function (event, ui) {
				        	$(".ui-dialog-titlebar-close").show();
			            },
			            title:'Batch Edit Loan Programs',
				        dialogClass: "LQBDialogBox"
				    });	
				}
				TransferForm.submit();
		}
  		function onModifyInvestorsClick() {
          showModalTopLevel('/los/Template/InvestorListAdmin.aspx', null, null, null, function(args){ 
			  if (args.OK) {
				  window.location = window.location; // refresh
				}
			});
        }
        
        //OPM 16984 Cord
        // Opens the Edit Investor Products window, with the default investor
        // being the currently selected investor.
        function onModifyInvestorProductsClick() {
          showModalTopLevel('/los/Template/InvestorProductListAdmin.aspx?InvestorIndex=' + <%= AspxTools.JsGetElementById(m_investorList) %>.selectedIndex, null, null, null, function(args){
			  if (args.OK) {
				window.location = window.location; // refresh
			  }
		   }, {hideCloseButton: true});
        }
        
        // OPM 16984 Cord
        // Used to disable adding programs without selecting a ProductCode
        function allowAddProgram(bAllow)
        {
			if ( bAllow == false)
			{
				<%= AspxTools.JsGetElementById(m_newFlp) %>.disabled = true;
				<%= AspxTools.JsGetElementById(m_newSlp) %>.disabled = true;
				
				// We don't want to mess with the Add Master
				// visibility if it's controlled elsewhere.
				//  (Like if it is already disabled because 
				//  there is already a master program).
				if ( folderHasMaster() == false)
				{
					document.getElementById("masterAdd").disabled = true;
				}
			}
			else
			{
				<%= AspxTools.JsGetElementById(m_newFlp) %>.disabled = false;
				<%= AspxTools.JsGetElementById(m_newSlp) %>.disabled = false;
				
				// We don't want to mess with the Add Master
				// visibility if it's controlled elsewhere.
				//  (Like if it is already disabled because 
				//  there is already a master program).
				if ( folderHasMaster() == false)
				{
					 document.getElementById("masterAdd").disabled = false;
				}
			}
		}
        
		 
		// OPM 16984 Cord
		// Creates two parallel arrays InvestorArray[] and ProductArray[].
		// InvestorArray[] holds the names of Investors which have valid products.
		// and ProductArray[] holds the corresponding Valid products in a subarray.
		function load_ProductCode_Data()
		{
			<% InjectInvestorProductArrays(); %>
			
			<% // OPM 22740 fs 06/11/08 Stores the initially loaded Product Code
			if ( CurrentUser.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) ) { %>			
				var hf_prodCode = <%= AspxTools.JsGetElementById(m_investorProductList_value) %>;
				var ddl_prodCode = <%= AspxTools.JsGetElementById(m_investorProductList) %>;
				if ( hf_prodCode != null && ddl_prodCode != null)
					hf_prodCode.value = ddl_prodCode.value;
			<% } %>
		}


		// OPM 16984 Cord
		// List the ProductCodes for the selected investor
		<% if ( CurrentUser.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) ) { %>
			function populateProductCodes() 
			{
				var Product_ddl = <%= AspxTools.JsGetElementById(m_investorProductList) %>;
				var Investor_ddl = <%= AspxTools.JsGetElementById(m_investorList) %>;
				
				var investorName = Investor_ddl.value;
				var investorIndex = -1;
				
				// Find the index of the investor's index so we can
				// access their productCode list.
				for(var j = 0; j < InvestorArray.length; j++)
				{
					if ( investorName == InvestorArray[j] )
					{
						investorIndex = j;
					}
				}
				
				// Clear out the old ProductCode options
				Product_ddl.selectedIndex = 0;
				Product_ddl.options.length = 0;
				
				// Show the ProductCodes for this investor
				if ( investorIndex != -1 )
				{
					for(var i = 0; i < ProductArray[investorIndex].length; i++)
					{
						Product_ddl.options[i] = new Option( ProductArray[investorIndex][i], ProductArray[investorIndex][i] );
					}
				}
					
				// If the investor has no associated ProductCodes
				// then we cannot let them add programs.
				if (Product_ddl.options.length == 0)
				{
					Product_ddl.options[0] = new Option( "--No Valid Products--", "" );
					Product_ddl.disabled = true;
					
					allowAddProgram(false);	
				}
				else
				{
					Product_ddl.disabled = false;
					
					allowAddProgram(true);
				}
				

				// Reselects the last Product Code selected
				// after a postback
				var selectedProductCode = <%= AspxTools.JsGetElementById(m_investorProductList_value) %>.value;
								
				if ( newInvestorSelected == false )		// Reselect ProductCode
				{
					for(var k = 0; k < Product_ddl.options.length; k++)
					{			
						if (Product_ddl.options[k].value == selectedProductCode)
						{
							Product_ddl.selectedIndex = k;
						}
					}
				}
				else // (newInvestorSelected == true) -- Select Default
				{
					// Store the selected ProductCode
					<%= AspxTools.JsGetElementById(m_investorProductList_value) %>.value = <%= AspxTools.JsGetElementById(m_investorProductList) %>.value;
				}
			}
		<% } %>


			// OPM 16984 Cord
			// Tells the ProductCode dropdown to repopulate it's options
			// with the selected investor's product codes
			var newInvestorSelected = false;
			
			function onchange_Investor_ddl()
			{
				//Clear the old saved selection
				newInvestorSelected = true;
				<%= AspxTools.JsGetElementById(m_investorProductList_value) %>.value = '';
				
				// Load the new ProductCodes for this investor
				populateProductCodes();
				
				// Save the selected productcode
				newInvestorSelected = false;
				<%= AspxTools.JsGetElementById(m_investorProductList_value) %>.value = <%= AspxTools.JsGetElementById(m_investorProductList) %>.value;
			}

			// OPM 16984 Cord
			// Stores the Product Code that was selected by the user
			function onchange_Product_ddl()
			{
				var Product_ddl = <%= AspxTools.JsGetElementById(m_investorProductList) %>;
				
				<%= AspxTools.JsGetElementById(m_investorProductList_value) %>.value = Product_ddl.value;
			}
			        
			        
			        
					<%-- 
					// 11/15/06 mf.  OPM 5553.  We have to pull the old 
					// click-the-invisible-button-after-confirm trick here.
					// .NET 2.0 lets us the the OnClientClick attribute for
					// the button server controls. Someday we will migrate.
					--%>
					function MasterAddConfirm()
					{
					if ( confirm( "Warning: Master programs cannot be deleted. Create master program?" ) )
						<%= AspxTools.JsGetElementById(m_addMasterBtn) %>.click();
			          
					return false;
					}
        </script>
		<form id="TransferForm" action="LpBatchEdit.aspx" method="post" target="LpBatchEditIFrame">
			<input name="folderid" type="hidden" value=<%=AspxTools.JsString(m_folderID)%>> <input type="hidden" name="LpIds">
		</form>
		<div class="hidden">
			<div id="LpBatchEditPopup">
				<iframe name="LpBatchEditIFrame" border="0" frameBorder="0" width="100%" height="100%"></iframe>	
			</div>
		</div>
		
		<form id="LoanProgramList" method="post" runat="server">
			<TABLE class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="FormTableHeader">Loan Programs<ml:EncodedLiteral id="m_folderCreateD" Runat="server"></ml:EncodedLiteral></TD>
				</TR>
				<TR>
					<TD><asp:panel id="m_forSimple" style="DISPLAY: inline" runat="server">
							<asp:button id="m_newBtn" onclick="OnNewProductClick" runat="server" Width="103px" Text="Add new program"></asp:button>
						</asp:panel><asp:panel id="m_forLender" style="DISPLAY: inline" runat="server">
							<asp:button id="m_newFlp" onclick="OnNewProductClick" runat="server" Text="Add 1st lien program"></asp:button>
							<asp:button id="m_newSlp" onclick="OnNewSecondProductClick" runat="server" Text="Add 2nd lien program"></asp:button>
						</asp:panel><asp:button id="m_duplicateBtn" onclick="OnDuplicateClick" runat="server" Text="Duplicate" Width="59px"></asp:button>
						<span onclick="if( children[ 0 ].disabled == false ) return validateCheckList();">
						  <asp:button id="m_deleteBtn" onclick="OnDeleteClick" runat="server" Text="Delete selected program" Width="135px"></asp:button>						  
						</span>
				<asp:panel id="m_forEditor" style="DISPLAY: inline" runat="server">
				<asp:CheckBox id="m_deleteDerivedCb" Runat="server" Text="Allow Deletion of Derived Programs" ></asp:CheckBox>
<asp:button id="m_moveBtn" onclick="OnMoveClick" runat="server" Width="123px" Text="Move to folder with id:" Enabled="False" Visible="False"></asp:button>
<asp:textbox id="m_newFolderId" runat="server" Width="236px" Enabled="False" Visible="False">Move feature is currently disabled.</asp:textbox>
<INPUT id="masterAdd" onclick="MasterAddConfirm();" type="button" value="Add Master"> 
<asp:Button id="m_addMasterBtn" style="DISPLAY: none" onclick="OnAddMasterClick" runat="server" Text="Add Master"></asp:Button>
<INPUT id="m_batchEdit" onclick="onBatchEdit();" type="button" value="Batch Edit"> 
      Investor For New:&nbsp; 
<asp:dropdownlist id="m_investorList" runat="server" onchange="onchange_Investor_ddl()" EnableViewState="True"></asp:dropdownlist>&nbsp; 
      Product Code:&nbsp; <select id="m_investorProductList" Runat="server" onchange="onchange_Product_ddl()"></select>
<INPUT id="m_investorProductList_value" type="hidden" name="m_investorProductList_value" runat="server">&nbsp; <A onclick="onModifyInvestorsClick(); return false;" href="#">Edit 
								Investor List</A> &nbsp; <A onclick="onModifyInvestorProductsClick(); return false;" href="#">Edit 
								Investor Products</A> </asp:panel></TD>
				</TR>
				<TR>
					<TD align="center"><ml:EncodedLabel id="m_noRecordLabel" runat="server" Visible="False" EnableViewState="False" Font-Bold="True">No loan programs available.</ml:EncodedLabel></TD>
				</TR>
				<TR>
					<TD><ml:commondatagrid id="m_programsDG" runat="server" Width="100%" EnableViewState="False" AllowSorting="True" CssClass="DataGrid" AutoGenerateColumns="False">
							<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
							<HeaderStyle CssClass="GridHeader"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<input type="checkbox" id="chkCheckAll" name="chkCheckAll" onclick="checkAll();">
									</HeaderTemplate>
									<ItemTemplate>
										<input id="templateid" name="templateid" onclick='disableEnableButtons("<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Type").ToString())%>");' type="checkbox" value='<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "lLpTemplateId").ToString())%>'>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate> <!-- "lLpTemplateId" "ProductCode" "lLpInvstorNm" -->
										<a href="#" onclick="onEditClick('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "lLpTemplateId").ToString())%>', '<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "ProductCode").ToString() )%>', '<%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "lLpInvestorNm").ToString() )%>'); return false;">
											edit</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<a href="#" onclick='onViewClick("<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "lLpTemplateId").ToString())%>"); return false;'>
											view</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Type" HeaderText="Type"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Enabled?">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<ml:EncodedLabel id="IsEnabledOverrideBit" runat="server">
											<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Enabled" ).ToString()) %>
										</ml:EncodedLabel>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="lLpTemplateNm" SortExpression="lLpTemplateNm" HeaderText="Program Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="lTerm" SortExpression="lTerm" HeaderText="Term"></asp:BoundColumn>
								<asp:BoundColumn DataField="lDue" SortExpression="lDue" HeaderText="Due"></asp:BoundColumn>
								<asp:TemplateColumn SortExpression="lLockedDays" HeaderText="Lock Per">
									<ItemTemplate>

										<ml:EncodedLabel id="lLockedDaysOverrideBit" runat="server">
											<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "lLockedDaysLowerSearch" ).ToString()) %>
											-
											<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "lLockedDays" ).ToString()) %>
										</ml:EncodedLabel>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="lFinMethT" HeaderText="Fin Meth">
									<ItemTemplate>
										<%#AspxTools.HtmlString(GetFinanceMethod( DataBinder.Eval( Container.DataItem , "lFinMethT" ) ))%>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="lLpeFeeMin" HeaderText="Back End Max Ysp">
									<ItemTemplate>
										<ml:EncodedLabel id="lLpeFeeMinOverrideBit" runat="server">
											<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "lLpeFeeMin" ).ToString()) %>
										</ml:EncodedLabel>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="lLpeFeeMax" HeaderText="Min Ysp">
									<ItemTemplate>
										<ml:EncodedLabel id="lLpeFeeMaxOverrideBit" runat="server">
											<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "lLpeFeeMax" ).ToString()) %>
										</ml:EncodedLabel>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="lRateDelta" SortExpression="lRateDelta" HeaderText="Rate Delta" />
								<asp:BoundColumn DataField="lFeeDelta" SortExpression="lFeeDelta" HeaderText="Fee Delta" />
								<asp:TemplateColumn>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<a href="#" uid="<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem, "lLpTemplateId" ).ToString()) %>" pnm="<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem, "lLpTemplateNm" ).ToString()) %>" onclick='onPricingClick( this.getAttribute("uid") , this.getAttribute("pnm") ); return false;'>
											rules</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<a href="#" uid="<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem, "lLpTemplateId" ).ToString()) %>" onclick='onRatesheetClick( this.getAttribute("uid") ); return false;'>
											rates</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<a href="#" uid="<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem, "lLpTemplateId" ).ToString()) %>" onclick='onInheritanceClick( this.getAttribute("uid") ); return false;'>
											chains</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="lLpCustomCode1" SortExpression="lLpCustomCode1" HeaderText="Custom #1" />
								<asp:BoundColumn DataField="lLpCustomCode2" SortExpression="lLpCustomCode2" HeaderText="Custom #2" />
								<asp:BoundColumn DataField="lLpCustomCode3" SortExpression="lLpCustomCode3" HeaderText="Custom #3" />
								<asp:BoundColumn DataField="lLpCustomCode4" SortExpression="lLpCustomCode4" HeaderText="Custom #4" />
								<asp:BoundColumn DataField="lLpCustomCode5" SortExpression="lLpCustomCode5" HeaderText="Custom #5" />
								<asp:BoundColumn DataField="lLpInvestorCode1" SortExpression="lLpInvestorCode1" HeaderText="Investor #1" />
								<asp:BoundColumn DataField="lLpInvestorCode2" SortExpression="lLpInvestorCode2" HeaderText="Investor #2" />
								<asp:BoundColumn DataField="lLpInvestorCode3" SortExpression="lLpInvestorCode3" HeaderText="Investor #3" />
								<asp:BoundColumn DataField="lLpInvestorCode4" SortExpression="lLpInvestorCode4" HeaderText="Investor #4" />
								<asp:BoundColumn DataField="lLpInvestorCode5" SortExpression="lLpInvestorCode5" HeaderText="Investor #5" />
							</Columns>
						</ml:commondatagrid></TD>
				</TR>
				<TR>
					<!--
					<TD align="middle"><INPUT onclick="onClosePopup();" type="button" value="Close"></TD>
					--></TR>
			</TABLE>
			<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></form>
	</body>
</HTML>
