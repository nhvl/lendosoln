<%@ Page language="c#" Codebehind="PMIProductFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.PMIProductFrame" %>
<!DOCTYPE HTML>
<HTML>
<HEAD runat="server">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <TITLE>PMIProductFrame</TITLE>
</HEAD>
<body class="body-iframe">
    <form runat="server">
        <div class="PMI-product-frame-left">
            <iframe name="folder" src="PMIProductFolder.aspx"></iframe>
        </div>
        <div class="PMI-product-frame-right">
            <iframe name="body" src="PMIProgramList.aspx"></iframe>
        </div>
    </form>
</body>
<script language=javascript>
    resize (700, 600);
</script>
</HTML>
