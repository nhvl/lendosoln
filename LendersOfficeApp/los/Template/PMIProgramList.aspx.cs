using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.Template
{
	public partial class PMIProgramList : LendersOffice.Common.BasePage
    {
        public bool IsRoot
        {
            get
            {
                try
                {
                    Guid temp = FolderId;
                }
                catch (ArgumentNullException)
                {
                    return true;
                }
                return false;
            }
        }

        public Guid FolderId
        {
            get
            {
                string id = RequestHelper.GetSafeQueryString("FolderId");
                return new Guid(id);
            }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (IsRoot)
            {
                m_addBtn.Visible = false;
                m_deleteBtn.Visible = false;
                m_enableBtn.Visible = false;
                return;
            }

            if (!BrokerUserPrincipal.CurrentPrincipal.LoginNm.Equals("pmlmaster"))
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, ErrorMessages.GenericAccessDenied);
            }

            if (!Page.IsPostBack)
            {
                m_addBtn.Attributes.Add("onclick", "onNewClick('" + FolderId + "');");
                BindGrid();
            }
        }

        private void BindGrid()
        {
            m_programsRepeater.DataSource = PMIProgramTemplateData.GetTemplatesByFolder(FolderId);
            m_programsRepeater.DataBind();
        }

        protected void DeleteClick(object sender, System.EventArgs a)
        {
            string[] idList = selectedCheckboxes.Value.Split(',', ' ');
            foreach (string id in idList)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    PMIProgramTemplateData.DeleteById(new Guid(id));
                }
            }

            BindGrid();
        }

        protected void EnableClick(object sender, System.EventArgs a)
        {
            string[] idList = selectedCheckboxes.Value.Split(',', ' ');
            foreach (string id in idList)
            {
                if (!string.IsNullOrEmpty(id))
                {
                    PMIProgramTemplateData.EnableById(new Guid(id));
                }
            }

            BindGrid();
        }

        protected void Programs_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            PMIProgramTemplateData data = args.Item.DataItem as PMIProgramTemplateData;
            if (data == null)
                return;

            HtmlAnchor editLink = args.Item.FindControl("editLink") as HtmlAnchor;
            HtmlAnchor rulesLink = args.Item.FindControl("rulesLink") as HtmlAnchor;
            HtmlInputCheckBox selection = args.Item.FindControl("selection") as HtmlInputCheckBox;
            Literal isEnabled = args.Item.FindControl("isEnabled") as Literal;
            Literal company = args.Item.FindControl("company") as Literal;
            Literal programType = args.Item.FindControl("programType") as Literal;
            Literal createdDate = args.Item.FindControl("createdDate") as Literal;

            if (data.MIType == E_PmiTypeT.MASTER)
            {
                selection.Visible = false;
                editLink.Visible = false;
            }
            else
            {
                editLink.Attributes.Add("onclick", string.Format("onEditClick('{0}', '{1}'); return false;", AspxTools.JsStringUnquoted(data.ProductId.ToString()), AspxTools.JsStringUnquoted(data.FolderId.ToString())));
                selection.Value = data.ProductId.ToString();
                company.Text = data.MICompany_rep;
            }

            rulesLink.Attributes.Add("onclick", string.Format("onRulesClick('{0}', '{1}'); return false;", AspxTools.JsStringUnquoted(data.ProductId.ToString()), AspxTools.JsStringUnquoted(data.MIType_rep.ToString())));
            isEnabled.Text = data.IsEnabled ? "Yes" : "No";
            programType.Text = data.MIType_rep;
            createdDate.Text = data.CreatedD.ToString();
        }

        public void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
	}

}
