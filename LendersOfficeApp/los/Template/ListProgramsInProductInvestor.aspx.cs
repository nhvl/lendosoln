﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ManualInvestorProductExpiration;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.Security;
using LendersOffice.Admin;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.Template
{
    public partial class ListProgramsInProductInvestor : LendersOffice.Common.BasePage
    {
        protected CommonDataGrid m_LoanProgramList;
        protected Label m_emptyMsg;
        protected Panel m_AccessDeniedPanel;
        protected Panel m_AccessGrantedPanel;

        private BrokerDB broker;
        private LockPolicy lockPolicy;
        private DataSet ds;
        private string m_investor;
        private string m_product;


        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private BrokerDB Broker
        {
            get
            {
                if (broker == null)
                {
                    broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                }
                return broker;
            }
        }

        private LockPolicy LockPolicy
        {
            get
            {
                if (this.lockPolicy == null)
                {
                    this.lockPolicy = LockPolicy.Retrieve(this.Broker.BrokerID, RequestHelper.GetGuid("policyId"));
                }

                return this.lockPolicy;
            }
        }

        protected bool IsManualExpirationEnabled
        {
            get { return (this.BrokerUser.IsInRole(ConstApp.ROLE_LOCK_DESK) || this.BrokerUser.IsInRole(ConstApp.ROLE_ADMINISTRATOR)) && this.LockPolicy.IsUsingRateSheetExpirationFeature; }
        }

        private void BindDataGrid()
        {
            try
            {
                ds = new DataSet("LoanProgramsInDisabled");
                DataColumn dcInv = new DataColumn("Investor");
                DataColumn dcProd = new DataColumn("ProductCode");
                DataColumn dcProg = new DataColumn("LoanProgramName");
                DataColumn dcRate = new DataColumn("RateStatus");

                DataTable dt = new DataTable("LoanProgramList");
                dt.Columns.Add(dcInv);
                dt.Columns.Add(dcProd);
                dt.Columns.Add(dcProg);
                dt.Columns.Add(dcRate);
                ds.Tables.Add(dt);

                List<LoanProgramSummaryItem> list = InvestorProductManager.ListProgramSummaryByInvestorProduct(this.Broker.BrokerID, this.LockPolicy, m_investor, m_product);

                foreach (LoanProgramSummaryItem item in list)
                {
                    ds.Tables["LoanProgramList"].Rows.Add(item.InvestorName, item.ProductCode, item.LoanProgramName, item.PricingStatus);
                }

                if (list.Count == 0)
                    m_emptyMsg.Visible = true;

                m_LoanProgramList.DataSource = ds.Tables["LoanProgramList"].DefaultView;
                m_LoanProgramList.DataBind();

                StringWriter sw = new System.IO.StringWriter();
                ds.WriteXml(sw);
                ViewState.Add("dataset", sw.ToString());
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
                //"An error was found when retrieving the disabled Investors and Product Codes."
                ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.Generic, exc), false, Guid.Empty, Guid.Empty);
            }
        }

        private void RefreshGrid()
        {
            try
            {
                if ((ds == null) && (ViewState["dataset"] != null))
                {
                    StringReader sr = new StringReader((string)ViewState["dataset"]);
                    if (ds == null)
                        ds = new DataSet();
                    ds.ReadXml(sr);
                }
                m_LoanProgramList.DataSource = ds.Tables["LoanProgramList"].DefaultView;
                m_LoanProgramList.DataBind();
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
                //ErrorUtilities.DisplayErrorPage("The datagrid could not be updated properly.");
                ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.Generic, exc), false, Guid.Empty, Guid.Empty);
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsManualExpirationEnabled)
            {
                m_AccessDeniedPanel.Visible = false;
                m_AccessGrantedPanel.Visible = true;
            }
            else
            {
                m_AccessDeniedPanel.Visible = true;
                m_AccessGrantedPanel.Visible = false;
                return;
            }

            if (!Page.IsPostBack)
            {
                m_investor = RequestHelper.GetSafeQueryString("inv");
                m_product = RequestHelper.GetSafeQueryString("prod");
                
                if (m_investor != null && m_product != null)
                {
                    BindDataGrid();
                }
                else
                {
                    //show error
                }
            }
            else
            {
                if (Request.Form["__EVENTTARGET"] == m_LoanProgramList.ClientID)
                    RefreshGrid();
            }
            if (m_LoanProgramList.Items.Count == 0)
                m_emptyMsg.Visible = true;
            else
                m_emptyMsg.Visible = false;
        }
    }
}
