﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.PMLNavigation;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.AntiXss;
using LendersOffice.Common;

namespace LendersOfficeApp.los.Template.Product
{
    public partial class Products : BaseServicePage
    {

        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_7_1;
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            EnableJquery = true;
            RegisterJsScript("jquery-ui-1.8.23.min.js");
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");

            DynaTreeNode[] node = new DynaTreeNode[] { GetFolderStructure() };
            RegisterJsStruct("ProgramFolderStructure", node);
        }

        private DynaTreeNode GetFolderStructure()
        {
            Dictionary<Guid, DynaTreeNode> nodesById = new Dictionary<Guid, DynaTreeNode>();
            DynaTreeNode root = GetNode(nodesById,  Guid.Empty, "Root Folder");
            root.expand = true;

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            using (SqlDataReader sR = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, 
                "ListLoanProductFolderByBrokerID", 
                new SqlParameter("@BrokerId", principal.BrokerId ),
                new SqlParameter("@IsLpe", principal.HasPermission(Permission.CanModifyLoanPrograms))))
            {
                while (sR.Read())
                {
                    string name = (string)sR["FolderName"];
                    Guid id = (Guid)sR["FolderID"];
                    Guid parentId = Guid.Empty;

                    if (DBNull.Value != sR["ParentFolderID"])
                    {
                        parentId = (Guid)sR["ParentFolderID"];
                    }

                    DynaTreeNode parent = GetNode(nodesById, parentId);
                    parent.children.Add(GetNode(nodesById, id, name));
                }
            }


            using (SqlDataReader sR = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, 
                "ListLoanProgramNamesByBrokerID",
                new SqlParameter("@BrokerID", principal.BrokerId),
                new SqlParameter("@IsLpe", principal.HasPermission(Permission.CanModifyLoanPrograms))))
            {
                while (sR.Read())
                {
                    DynaTreeNode parentFolder = root;
                    if (DBNull.Value != sR["FolderId"])
                    {
                        Guid folderId = (Guid)sR["FolderId"];
                        parentFolder = nodesById[folderId];
                    }

                    parentFolder.children.Add(new DynaTreeNode() { 
                        Id = (Guid)sR["lLpTemplateId"],
                        ExternalName = (string)sR["lLpTemplateNm"],
                        key = ((Guid)sR["lLpTemplateId"]).ToString("N"),
                        isFolder = false,
                        title = (string)sR["lLpTemplateNm"]
                    });
                }
            }

            return root;
        }

        private static DynaTreeNode GetNode(Dictionary<Guid, DynaTreeNode> item, Guid id, string title)
        {
            DynaTreeNode node;
            if (!item.TryGetValue(id, out node))
            {
                node = new DynaTreeNode()
                {
                    isFolder = true,
                    Id = id,
                    title = title,
                    key = id.ToString("N"),
                    target = "LoanProductList",
                    href = String.Format("LoanProgramList.aspx?folderid={0}", id),
                    children = new List<DynaTreeNode>()
                };

                item.Add(id, node);
            }
            else
            {
                node.title = title;
            }

            return node;
        }

        private static DynaTreeNode GetNode(Dictionary<Guid, DynaTreeNode> item, Guid id)
        {
            if (!item.ContainsKey(id))
            {
                DynaTreeNode node = new DynaTreeNode()
                {
                    isFolder = true,
                    Id = id,
                    key = id.ToString("N"),
                    target = "LoanProductList",
                    href = String.Format("LoanProgramList.aspx?folderid={0}", id),
                    children = new List<DynaTreeNode>()
                };

                item.Add(id, node);
            }

            return item[id];
        }
    }
}
