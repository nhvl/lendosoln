<%@ Page language="c#" Codebehind="PMIProductFolder.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.PMIProductFolder" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>PMIProductFolder</title>
	<link href="../../css/stylesheet.css" rel="stylesheet">
	<style>
		#programTree {
			width: 100%;
			height: 100%;
			width: calc(100vw - 28px);
			height: calc(100vh - 81px);
			padding: 4px;
			background :whitesmoke;
			border: 2px groove;
		}
	</style>
</head>
<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: scroll">

<form method="post" runat="server">
	<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
		<tr>
			<td>
				<asp:Panel id="m_addnewPMIPanel" style="display: inline" runat="server">
					<button type="button" style="width: 180px; height: 21px" id="btnAdd">Add child folder...</button>
				</asp:Panel>

				<asp:Panel id="m_deletePMIPanel" style="display: inline" runat="server">
				    <button type="button" style="width: 90px;" id="btnDelete">Delete</button>
				</asp:Panel>

				<button type="button" style="height: 21px;" id="btnRefresh">Refresh</button>
			</td>
		</tr>
		<tr>
			<td noWrap>
				<div id="m_PMIProgramTree"></div>
			</td>
		</tr>
	</table>
	<uc1:cModalDlg id="CModalDlg2" runat="server" />
</form>
<script>
	(function($){
		var guidEmpty = "00000000-0000-0000-0000-000000000000";
		var programTree = $("#m_PMIProgramTree");

		$("#btnRefresh").on("click", function(event){
			event.currentTarget.disabled = true;
			fetchData().then(function() {
				event.currentTarget.disabled = false;
			});
		});
		$("#btnDelete").on("click", function(event){
			var activeNode = programTree.dynatree("getActiveNode");
			var selectedNodes = programTree.dynatree("getSelectedNodes");
			if (activeNode == null) return;
			var FolderID = activeNode.data.key;

			if (!confirm("WARNING: This action will remove the current folder and ALL of its descendants. Continue?")) return;
			event.currentTarget.disabled = true;

			post("Delete", {folderId: FolderID}).then(function(d){
				event.currentTarget.disabled = false;
				if (!!d.errorMessage) {
					alert(d.errorMessage);
					return;
				}
				return fetchData();
			}, function(err){
				event.currentTarget.disabled = false;
				alert("ERROR" +  err.Message);
			});
		});
		$("#btnAdd").on("click", function(event) {
			var activeNode = programTree.dynatree("getActiveNode");
			var selectedNodes = programTree.dynatree("getSelectedNodes");
			if (activeNode == null) return;
			var FolderID = activeNode.data.key;

			parent.body.showModal('/los/Template/PMIProductFolderName.aspx?parentid=' + FolderID, null, null, null, function(args) {
				if (!args.OK) return;
				var folder = ({FolderID: args.folderid, FolderName:args.name});

				var children = activeNode.getChildren();
				activeNode.addChild({key:folder.FolderID, title:folder.FolderName, isFolder:true },
					(children == null || children.length < 1) ? null : children[0]); // insert at first position
				activeNode.expand(true);
			});
		});

		renderTree(state.folders);

		function renderTree(folders){
			try { programTree.dynatree("destroy"); } catch(e) { }

            var dTree = list2DynaTree(folders);

			programTree.dynatree({
				children: [dTree],
				onPostInit: function(){
					programTree.dynatree("getRoot").visit(function(node){
						if (node.getLevel() < 2) node.expand(true);
					});
				},
				onActivate: function(node) {
					var FolderID = node.data.key;
					parent.body.location.href = FolderID == "root" ? "PMIProgramList.aspx?" :
						"PMIProgramList.aspx?Folderid=" + encodeURIComponent(FolderID);
				},
    			onExpand: function(flag, node){
					if (!!flag) return;

					// collapse
					var FolderID = node.data.key;
					parent.body.location.href = FolderID == "root" ? "PMIProgramList.aspx?" :
						"PMIProgramList.aspx?Folderid=" + encodeURIComponent(FolderID);
				}
			});
		}

		function list2DynaTree(folders) {
			var p2cMap = {}; // Dictionary map parentFolder.FolderId -> to it's children DynatreeNode[]
			var id2Node = {}; // Dictionary map the folder.FolderId -> to the corresponding DynatreeNode.

			var rootChildren = folders.map(function(node){
				var dNode = ({key:node.FolderID, title:node.FolderName, isFolder:true });

				id2Node[node.FolderID] = dNode;

				if (node.ParentFolderID == null) return dNode;

				var children = p2cMap[node.ParentFolderID] = p2cMap[node.ParentFolderID] || [];
				children.push(dNode);
				return null;
			}).filter(function(n){ return n != null});

			Object.keys(p2cMap).forEach(function(pId){
				var p = id2Node[pId];
				if (p == null) { return;}
				p.children = p2cMap[pId];
			});

			return ({ key:"root", title:"Root Folder", children:rootChildren, id2Node: id2Node, unselectable:true });
		}

		function fetchData() {
            return post("LoadData", ({ })).then(function(d) {
                if (d.errorMessage != null) {
                    alert(d.errorMessage);
                    return;
                }

                state.folders = d.folders;
                renderTree(state.folders);
            }, function(err){
				alert(err);
			});
		}

		function post(op, data) {
			return callWebMethodAsync({
				url:location.pathname+"/"+op,
				data: JSON.stringify(data),
				async: true
			}).then(function(data) { return data.d; });
		}
	})(jQuery);
</script>
</body>
</html>
