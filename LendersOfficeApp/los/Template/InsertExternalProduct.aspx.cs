using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.Template
{
    public partial class InsertExternalProduct : LendersOffice.Common.BasePage
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private string ErrorMessage
        {
            set { ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + "."); }
        }

        private void RegisterResource()
        {
            EnableJquery = false;
            EnableJqueryMigrate = false;
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");
        }

        /// <summary>
        /// Initialize this page.
        /// </summary>
        protected void PageLoad(object sender, System.EventArgs a)
        {
            RegisterResource();

            // Load up current broker's tree.  We don't save tree
            // structure between postbacks.

            // Check security access control bits and deny
            // if not allowed.

            if (!BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
            {
                ErrorMessage = "Access denied.";
                return;
            }

            // Get the broker list on the first load.  We
            // assume the list saves state between posts.

            if (IsPostBack == false)
            {
                var selectedBrokerId = BrokerUser.BrokerId;
                this.RegisterJsObjectWithJsonNetAnonymousSerializer("state", LoadData(BrokerUser.BrokerId, true));

                var brokerList = LoadBrokerList();
                m_BrokerList.DataValueField = "Key";
                m_BrokerList.DataTextField = "Value";
                m_BrokerList.DataSource = brokerList.OrderBy(o => o.Value);
                m_BrokerList.DataBind();
                m_BrokerList.SelectedIndex = m_BrokerList.Items.IndexOf(m_BrokerList.Items.FindByValue(selectedBrokerId.ToString()));
            }
        }


        [WebMethod]
        public static object LoadData(Guid selectedBrokerId)
        {
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanModifyLoanPrograms))
            {
                return new { errorMessage = "Access denied." };
            }

            return LoadData(selectedBrokerId, true);
        }
        [WebMethod]
        public static object DoInsert(Guid selectedBrokerId, Guid targetId, Guid[] selectedIds)
        {
            var BrokerUser = PrincipalFactory.CurrentPrincipal;
            if (!BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
            {
                return new { errorMessage = "Access denied." };
            }

            try {
                var set = new LoanProductSet();
                set.Retrieve(selectedBrokerId, true);

                var selectedNodes = new List<LoanProductNode>();
                try
                {
                    foreach (Guid checkedId in selectedIds)
                    {
                        var lpN = set[checkedId];

                        LoanProductNode lpP;
                        for (lpP = set[lpN.Parent]; lpP != null; lpP = set[lpP.Parent])
                        {
                            if (selectedIds.Contains(lpP.Id))
                            {
                                break;
                            }
                        }

                        if (lpP == null)
                        {
                            selectedNodes.Add(lpN);
                        }
                    }
                }
                catch (Exception)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Failed to translate copy id.");
                }

                // Load up the products we're transfering and make sure
                // to make duplicated of the folders in their current
                // structure.

                // Transfer object to target id (folder) for user's broker.

                for (int r = 1; r <= 3; ++r)
                {
                    try
                    {
                        using (CStoredProcedureExec spExec = new CStoredProcedureExec(DataSrc.LpeSrc))
                        {
                            spExec.BeginTransactionForWrite();

                            try
                            {
                                foreach (LoanProductNode lpN in selectedNodes)
                                {
                                    set.Derive_CalledFromInsertExternalProduct
                                        (lpN
                                        , BrokerUser.HasPermission(Permission.CanModifyLoanPrograms)
                                        , targetId
                                        , BrokerUser.BrokerId
                                        , new ArrayList() // 08/06/07 mf.  OPM 17199
                                        , spExec);
                                }
                            }
                            catch
                            {
                                // D'oh!

                                spExec.RollbackTransaction();

                                throw;
                            }

                            spExec.CommitTransaction();
                        }

                        foreach (LoanProductNode lpN in selectedNodes)
                        {
                            try
                            {
                                set.Spread
                                    (lpN
                                    , BrokerUser.BrokerId
                                    , becomeIndependent: false
                                    );
                            }
                            catch (Exception e)
                            {
                                // Oops!

                                Tools.LogErrorWithCriticalTracking($"Failed spreading {lpN.Type} {lpN.Name} :: {lpN.Id}.", e);
                            }
                        }

                        return new { errorMessage = "" };
                    }
                    catch (CBaseException e)
                    {
                        // Oops!

                        Tools.LogError("Failed deriving products.", e);
                        return new { errorMessage = e.UserMessage };
                    }
                    catch (Exception e)
                    {
                        // Oops!

                        Tools.LogError("Failed deriving products.", e);
                    }
                }

                return new { errorMessage = "Unable to derive from selected base." };
            }
            catch (Exception e)
            {
                // Oops!
                Tools.LogError(e);

                return new { errorMessage = "Failed to insert node into program set." };
            }
        }

        private static object LoadData(Guid selectedBrokerId, bool isLpe)
        {
            return new
            {
                selectedBrokerId,
                tree = LoadTree(selectedBrokerId, isLpe),
            };
        }
        private static object LoadTree(Guid selectedBrokerId, bool isLpe)
        {
            var set = new LoanProductSet();
            set.Retrieve(selectedBrokerId, isLpe);
            return new {
                set.Id,
                Name = "Root Folder",
                children = LoadTreeChildren(set),
            };
        }
        internal static object LoadTreeChildren(LoanProductContainer container)
        {
            var children = new List<object>();
            foreach (LoanProductNode node in container)
            {
                children.Add(LoadTreeNode(node));
            }
            return children;
        }
        private static object LoadTreeNode(LoanProductNode node)
        {
            if (node.Type == LoanProductType.Product)
            {
                var t = node as LoanProductTemplate;
                return new
                {
                    t.Id,
                    t.Name,
                    t.IsEnabled,
                };
            }

            if (node.Type == LoanProductType.Folder)
            {
                return new
                {
                    node.Id,
                    node.Name,
                    children = LoadTreeChildren(node as LoanProductContainer),
                };
            }

            return null;
        }

        private static List<KeyValuePair<Guid, string>> LoadBrokerList()
        {
            var list = new List<KeyValuePair<Guid, string>>();

            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@HasLenderDefaultFeatures", true)
                };
                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListBrokers", parameters))
                {
                    while (reader.Read())
                    {
                        list.Add(new KeyValuePair<Guid, string>((Guid)reader["BrokerId"], (string)reader["BrokerNm"]));
                    }
                }
            }

            return list;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
