<%@ Page language="c#" Codebehind="EditRateSheet.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.EditRateSheet" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc" TagName="RenderReadOnly" Src="../../Common/RenderReadOnly.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Edit Rate Sheet</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
<body bgColor=gainsboro scroll=yes onload=onInit(); MS_POSITIONING="FlowLayout">
<script language=javascript>

			<!--

			function onInit()
			{
				<% if( IsPostBack == false ) { %>
				resize( 520 , 1000 );
				<% } %>

		    <% if (IsLpDerived) { %>
				f_initOverrideCb();
				<% } %>

				if( document.getElementById("m_errorMessage") != null && document.getElementById("m_errorMessage").value != "" )
				{
					alert( document.getElementById("m_errorMessage").value );
				}

				if( document.getElementById("m_feedBack") != null && document.getElementById("m_feedBack").value != "" )
				{
					alert( document.getElementById("m_feedBack").value );
				}

				if( document.getElementById("m_commandToDo") != null )
				{
					if( document.getElementById("m_commandToDo").value == "Close" )
					{
						var args = new Object();
						args.OK = true;
						onClosePopup(args.OK);
					}
				}
			}
			<% if (IsLpDerived) { %>
			function f_initOverrideCb() {
			  f_onclick_lPpmtPenaltyMonOverrideBit();
			  f_onclick_lLockedDaysOverrideBit();
			  f_onclick_lIOnlyMonLowerOverrideBit();
			  f_onclick_lIOnlyMonUpperOverrideBit();
			  f_onclick_source();
			}
			function f_onclick_lPpmtPenaltyMonOverrideBit() {
			  var cb = <%= AspxTools.JsGetElementById(lPpmtPenaltyMonOverrideBit) %>;
			  var tb0 = <%= AspxTools.JsGetElementById(lPpmtPenaltyMon) %>;
			  var tb1 = <%= AspxTools.JsGetElementById(lPpmtPenaltyMonLowerSearch) %>;

			  tb0.readOnly = !cb.checked;
			  tb1.readOnly = !cb.checked;

			}
			function f_onclick_lLockedDaysOverrideBit() {
			  var cb = <%= AspxTools.JsGetElementById(lLockedDaysOverrideBit) %>;
			  var tb0 = <%= AspxTools.JsGetElementById(lLockedDays) %>;
			  var tb1 = <%= AspxTools.JsGetElementById(lLockedDaysLowerSearch) %>;

			  tb0.readOnly = !cb.checked;
			  tb1.readOnly = !cb.checked;

			}
			function f_onclick_lIOnlyMonLowerOverrideBit()
			{
			  var cb = <%= AspxTools.JsGetElementById(lIOnlyMonLowerSearchOverrideBit) %>;
			  var tb0 = <%= AspxTools.JsGetElementById(lIOnlyMonLowerSearch) %>;

			  tb0.readOnly = !cb.checked;
			}
			function f_onclick_lIOnlyMonUpperOverrideBit()
			{
			  var cb = <%= AspxTools.JsGetElementById(lIOnlyMonUpperSearchOverrideBit) %>;
			  var tb0 = <%= AspxTools.JsGetElementById(lIOnlyMonUpperSearch) %>;

			  tb0.readOnly = !cb.checked;
			}


			function f_onclick_source() {
			  var bUseOverrides = <%= AspxTools.JsGetElementById(m_UseOverrides) %>.checked ;

			  <%= AspxTools.JsGetElementById(m_OverridesRates) %>.readOnly = ! bUseOverrides ;
			  document.getElementById("DownloadInterval").style.display = bUseOverrides ? 'block' : 'none' ;

			  var LpeAcceptableRsFileId =  <%= AspxTools.JsGetElementById(LpeAcceptableRsFileId) %>;
			  LpeAcceptableRsFileId.readOnly = ! bUseOverrides;
			  if (LpeAcceptableRsFileId.oldValue  && LpeAcceptableRsFileId.readOnly )
			    LpeAcceptableRsFileId.value = oldValue;

			  var LpeAcceptableRsFileVersionNumber =  <%= AspxTools.JsGetElementById(LpeAcceptableRsFileVersionNumber) %>;
			  LpeAcceptableRsFileVersionNumber.readOnly = ! bUseOverrides;
			  if (LpeAcceptableRsFileVersionNumber.oldValue  && LpeAcceptableRsFileVersionNumber.readOnly )
			    LpeAcceptableRsFileVersionNumber.value = oldValue;

			}

			<% } %>

			//-->

		</script>
<h4 class="page-header">Edit Loan Program Rate Sheet</h4>
<form id=EditRateSheet method=post runat="server">
<div style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 8px; BORDER-TOP: 2px groove; PADDING-LEFT: 8px; BACKGROUND: whitesmoke; PADDING-BOTTOM: 8px; MARGIN: 8px; BORDER-LEFT: 2px groove; PADDING-TOP: 8px; BORDER-BOTTOM: 2px groove; ">
<table cellSpacing=4 cellPadding=2 width="100%">
  <tr>
    <td class=FieldLabel>Program Name</td>
    <td><asp:textbox id=lLpTemplateNm runat="server" ReadOnly="True" Width="300px"></asp:textbox></td></tr>
  <tr>
    <td class=FieldLabel noWrap>Rate sheet identifier</td>
    <td noWrap width="100%"><asp:textbox id="lRateOptionBaseId" style="PADDING-LEFT: 4px" runat="server" ReadOnly="True" Width="300px" MaxLength="60" /></td></tr>
  <tr>
    <td class=FieldLabel noWrap>Rate Lock Period
(days)</td>
    <td><asp:textbox id=lLockedDaysLowerSearch runat="server" Width="50px"></asp:textbox>&nbsp;to
<asp:textbox id=lLockedDays runat="server" Width="50px"></asp:textbox><asp:checkbox id=lLockedDaysOverrideBit onclick=f_onclick_lLockedDaysOverrideBit(); runat="server" Text="override?"></asp:checkbox></td></tr>
  <tr>
    <td class=FieldLabel noWrap>PP Period (months)</td>
    <td><asp:textbox id=lPpmtPenaltyMonLowerSearch runat="server" Width="50px"></asp:textbox>&nbsp;to
<asp:textbox id=lPpmtPenaltyMon runat="server" Width="50px"></asp:textbox><asp:checkbox id=lPpmtPenaltyMonOverrideBit onclick=f_onclick_lPpmtPenaltyMonOverrideBit(); runat="server" Text="override?"></asp:checkbox></td></tr>
    <td class=FieldLabel noWrap>IO Period (months)</td>
    <td>
      <asp:textbox id="lIOnlyMonLowerSearch" runat="server" Width="50px"></asp:textbox>
      <asp:checkbox id="lIOnlyMonLowerSearchOverrideBit" onclick=f_onclick_lIOnlyMonLowerOverrideBit(); runat="server" Text="override?"></asp:checkbox> &nbsp;to
      <asp:textbox id="lIOnlyMonUpperSearch" runat="server" Width="50px"></asp:textbox>
      <asp:checkbox id="lIOnlyMonUpperSearchOverrideBit" onclick=f_onclick_lIOnlyMonUpperOverrideBit(); runat="server" Text="override?"></asp:checkbox>
     </td></tr>
     <tr>
       <td class="FieldLabel">Filename</td>
       <td><asp:TextBox id="LpeAcceptableRsFileId" Runat="server" MaxLength="100" Width="300px" /></td>
     </tr>
     <tr>
       <td class="FieldLabel">File Version</td>
       <td><asp:TextBox id="LpeAcceptableRsFileVersionNumber" Runat="server" MaxLength="7" Width="50px" /></td>
     </tr>

</table><asp:panel
id=m_InheritedPanel runat="server"><asp:RadioButton id=m_UseInherited onclick=f_onclick_source(); runat="server" Text="Inherit base product's rate sheet options" GroupName="Source" CssClass="FieldLabel"></asp:RadioButton>
<DIV style="HEIGHT: 8px"></DIV>
<DIV
style="BORDER-RIGHT: lightgrey 0px solid; PADDING-RIGHT: 4px; BORDER-TOP: lightgrey 0px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; FONT: 12px arial; BORDER-LEFT: lightgrey 0px solid; WIDTH: 100%; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 0px solid; HEIGHT: 110px"><ml:CommonDataGrid id=m_InheritedRates runat="server">
							<Columns>
						  <asp:TemplateColumn HeaderText="Rate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						    <ItemTemplate>
						      <%# AspxTools.HtmlString(DisplayRate(Container.DataItem)) %>
						    </ItemTemplate>
						  </asp:TemplateColumn>
						  <asp:TemplateColumn HeaderText="Point" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						    <ItemTemplate>
						      <%# AspxTools.HtmlString(DisplayPoint(Container.DataItem)) %>
						    </ItemTemplate>
						  </asp:TemplateColumn>
						  <asp:TemplateColumn HeaderText="Margin" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						    <ItemTemplate>
						      <%# AspxTools.HtmlString(DisplayMargin(Container.DataItem)) %>
						    </ItemTemplate>
						  </asp:TemplateColumn>
  						  <asp:TemplateColumn HeaderText="Qual Rate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						    <ItemTemplate>
						      <%# AspxTools.HtmlString(DisplayQualRate(Container.DataItem)) %>
						    </ItemTemplate>
						  </asp:TemplateColumn>
  						  <asp:TemplateColumn HeaderText="Teaser Rate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						    <ItemTemplate>
						      <%# AspxTools.HtmlString(DisplayTeaserRate(Container.DataItem)) %>
						    </ItemTemplate>
						  </asp:TemplateColumn>
							</Columns>
						</ml:CommonDataGrid><asp:Panel id=m_InheritedDenied style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">Access denied. You do not have permission to view.
</asp:Panel><asp:Panel id=m_InheritedEmpty style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: dimgray; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">Rate sheet is empty. Nothing to show.
</asp:Panel></DIV></asp:panel><asp:panel id=m_OverridesPanel runat="server">
<DIV style="HEIGHT: 8px"></DIV><asp:RadioButton id=m_UseOverrides onclick=f_onclick_source(); runat="server" Text="Override rate sheet options" GroupName="Source" CssClass="FieldLabel"></asp:RadioButton></asp:panel><ml:EncodedLabel id=m_MsgOverrides style="MARGIN-LEFT: 6px" runat="server" CssClass="FieldLabel">
					Enter rate sheet options
				</ml:EncodedLabel>
<div style="HEIGHT: 8px"></div>
<div style="WIDTH: 100%"><asp:textbox id=m_OverridesRates style="BORDER-RIGHT: lightgrey 2px solid; PADDING-RIGHT: 16px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 16px; PADDING-BOTTOM: 16px; BORDER-LEFT: lightgrey 2px solid; PADDING-TOP: 16px; BORDER-BOTTOM: lightgrey 2px solid" runat="server" Width="100%" AlternateHeight="274px" Height="110px" TextMode="MultiLine">
					</asp:textbox>
<div style="PADDING-LEFT: 5px; COLOR: gray; TEXT-ALIGN: right">(Enter each rate option on a single line: 'note rate' , 'fee' ,
'margin', 'qual rate', 'teaser rate(only used if option arm)') </div></div>
<div style="HEIGHT: 24px"></div>
<div
style="BORDER-RIGHT: lightgrey 2px solid; PADDING-RIGHT: 8px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; BORDER-LEFT: lightgrey 2px solid; PADDING-TOP: 8px; BORDER-BOTTOM: lightgrey 2px solid; TEXT-ALIGN: center; HEIGHT: 130px">
<table cellSpacing=8 cellPadding=0 border=0>
  <tr>
    <td class=FieldLabel>Rate Delta </td>
    <td><ml:percenttextbox id=m_RateDelta style="PADDING-LEFT: 4px" runat="server" preset="percent">
						</ml:percenttextbox></td>
    <td class=FieldLabel>Fee Delta </td>
    <td><ml:percenttextbox id=m_FeeDelta style="PADDING-LEFT: 4px" runat="server" preset="percent">
						</ml:percenttextbox></td></tr>
  <tbody id="DownloadInterval">
  <tr>
    <td class="FieldLabel">Download Start</td>
    <td colspan="3"><ml:DateTextBox id="lRateSheetDownloadStartD_date" preset="date" runat="server" width="75px"></ml:DateTextBox>
      <ml:TimeTextBox id="lRateSheetDownloadStartD_time" runat="server"></ml:TimeTextBox>
    </td>
  </tr>
  <tr>
    <td class="FieldLabel">Download End</td>
    <td colspan="3"><ml:DateTextBox id="lRateSheetDownloadEndD_date" preset="date" runat="server" width="75px"></ml:DateTextBox>
      <ml:TimeTextBox id="lRateSheetDownloadEndD_time" runat="server"></ml:TimeTextBox>
    </td>
  </tr></tbody>

						</table></div>
<div style="HEIGHT: 24px"></div>
<div
style="BORDER-RIGHT: lightgrey 2px dashed; PADDING-RIGHT: 4px; BORDER-TOP: lightgrey 2px dashed; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; FONT: 12px arial; BORDER-LEFT: lightgrey 2px dashed; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px dashed; HEIGHT: 110px"><ml:commondatagrid id=m_CurrentRates runat="server">
						<Columns>
						  <asp:TemplateColumn HeaderText="Rate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						    <ItemTemplate>
						      <%# AspxTools.HtmlString(DisplayRate(Container.DataItem)) %>
						    </ItemTemplate>
						  </asp:TemplateColumn>
						  <asp:TemplateColumn HeaderText="Point" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						    <ItemTemplate>
						      <%# AspxTools.HtmlString(DisplayPoint(Container.DataItem)) %>
						    </ItemTemplate>
						  </asp:TemplateColumn>
						  <asp:TemplateColumn HeaderText="Margin" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						    <ItemTemplate>
						      <%# AspxTools.HtmlString(DisplayMargin(Container.DataItem)) %>
						    </ItemTemplate>
						  </asp:TemplateColumn>
  						  <asp:TemplateColumn HeaderText="Qual Rate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						    <ItemTemplate>
						      <%# AspxTools.HtmlString(DisplayQualRate(Container.DataItem)) %>
						    </ItemTemplate>
						  </asp:TemplateColumn>
  						  <asp:TemplateColumn HeaderText="Teaser Rate" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
						    <ItemTemplate>
						      <%# AspxTools.HtmlString(DisplayTeaserRate(Container.DataItem)) %>
						    </ItemTemplate>
						  </asp:TemplateColumn>
						</Columns>
					</ml:commondatagrid><asp:panel id=m_CurrentDenied
style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">Access denied. You do not have permission to
view. </asp:panel><asp:panel id=m_CurrentEmpty
style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: dimgray; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">Rate sheet is empty. Nothing to show.
</asp:panel></div></div>
<div
style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 20px 8px 8px; PADDING-TOP: 0px; TEXT-ALIGN: center"><asp:button id=m_Ok onclick=OkClick runat="server" Width="60px" Text="OK">
				</asp:button><input onclick=onClosePopup(); type=button value=Cancel width="60px">
<asp:button id=m_Apply onclick=ApplyClick runat="server" Width="60px" Text="Apply">
				</asp:button></div><uc:renderreadonly id=readOnly runat="server">
			</uc:renderreadonly><uc:cmodaldlg id=modalDlg runat="server">
			</uc:cmodaldlg></form>
	</body>
</HTML>
