﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonProjectLib.Common.Lib;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ManualInvestorProductExpiration;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.RatePrice.Helpers;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;

namespace LendersOfficeApp.los.Template
{
    public partial class ListDisabledProductInvestor : LendersOffice.Common.BasePage
    {
        protected const string PRODUCTCODE = "ProductCode";
        protected const string INVESTOR = "Investor";
        protected const string PermittedActions = "PermittedActions";
        protected string triggerPostback;
        private BrokerDB broker;
        private DataSet dataSet;
        private List<LockPolicy> lockPolicies = null;
        private Dictionary<Guid, string> lockPolicyNames = null;
        private Dictionary<int, HashSet<string>> lpLiveDownloadListRows = null;
        private HashSet<Tuple<Guid, InvestorProduct, int>> lpLiveExpiredProductsWithDownloadListRow = null;

        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingDisablePricing
                };
            }
        }

        private BrokerDB Broker
        {
            get
            {
                if (this.broker == null)
                {
                    this.broker = BrokerDB.RetrieveById(this.BrokerUser.BrokerId);
                }

                return this.broker;
            }
        }

        private Dictionary<int, HashSet<string>> LpLiveDownloadListRows
        {
            get
            {
                if (this.lpLiveDownloadListRows == null)
                {
                    this.lpLiveDownloadListRows = this.LoadLpLiveBotsFromDownloadListToAcceptableRsFileIds();
                }

                return this.lpLiveDownloadListRows;
            }
        }

        private HashSet<Tuple<Guid, InvestorProduct, int>> LpLiveExpiredProductsWithDownloadListRow
        {
            get
            {
                if (this.lpLiveExpiredProductsWithDownloadListRow == null)
                {
                    this.lpLiveExpiredProductsWithDownloadListRow = (HashSet<Tuple<Guid, InvestorProduct, int>>)ViewState["lpLiveExpiredProductsWithDownloadListRowId"] ?? new HashSet<Tuple<Guid, InvestorProduct, int>>();
                }

                return this.lpLiveExpiredProductsWithDownloadListRow;
            }
        }

        // 7/24/2014 tj - Only exposing this so we can pass the value to the Add page.
        /// <summary>
        /// Gets the Lock Policy Id specified by the page call.<para />
        /// Warning: May be Guid.Empty.
        /// </summary>
        protected Guid LockPolicyId
        {
            get { return RequestHelper.GetGuid("policyid", Guid.Empty); }
        }

        /// <summary>
        /// Gets Lock Policies that this window governs (either for the specified id or all Lock Policies,<para />
        /// filtered to only include policies with IsUsingRateSheetExpirationFeature enabled).
        /// </summary>
        protected IEnumerable<LockPolicy> LockPolicies
        {
            get
            {
                if (this.lockPolicies == null)
                {
                    this.lockPolicies = this.LockPolicyId != Guid.Empty ? new List<LockPolicy>(1) { LockPolicy.Retrieve(this.Broker.BrokerID, this.LockPolicyId) } : LockPolicy.RetrieveAllForBroker(this.Broker.BrokerID);
                    this.lockPolicies.RemoveAll(policy => !policy.IsUsingRateSheetExpirationFeature || policy.BrokerId != this.Broker.BrokerID);
                }

                return this.lockPolicies;
            }
        }

        protected Dictionary<Guid, string> LockPolicyNames
        {
            get
            {
                if (this.lockPolicyNames == null)
                {
                    this.lockPolicyNames = this.LockPolicies.ToDictionary(lp => lp.LockPolicyId, lp => lp.PolicyNm);
                }

                return this.lockPolicyNames;
            }
        }

        protected bool IsManualExpirationEnabled
        {
            get { return this.LockPolicies.Any(); }
        }

        /// <summary>
        /// Creates a new DataSet for the DisabledList DataTable.
        /// </summary>
        /// <returns>The DataSet that will represent our grid in the UI.</returns>
        private static DataSet CreateDataSet()
        {
            DataSet newDataSet = new DataSet("DisabledDataSet");

            DataTable dt = new DataTable("DisabledList");
            dt.Columns.Add(new DataColumn("Investor"));
            dt.Columns.Add(new DataColumn("ProductCode"));
            dt.Columns.Add(new DataColumn("LockPolicy"));
            dt.Columns.Add(new DataColumn("DisableType"));
            dt.Columns.Add(new DataColumn("Message"));
            dt.Columns.Add(new DataColumn("Notes"));
            dt.Columns.Add(new DataColumn("LoginName"));
            dt.Columns.Add(new DataColumn("TimeStamp"));
            dt.Columns.Add(new DataColumn("PriceResultLabel"));
            dt.Columns.Add(new DataColumn("PolicyId"));
            dt.Columns.Add(new DataColumn("DownloadListId")); // Only used for LP_Live products
            dt.Columns.Add(new DataColumn(PermittedActions));
            newDataSet.Tables.Add(dt);

            return newDataSet;
        }

        /// <summary>
        /// Creates a new row in the table for the included data.
        /// </summary>
        /// <param name="disabledProducts">The table receiving the data.</param>
        /// <param name="item">The disabled product to add to the table.</param>
        /// <param name="policyNm">The name of the lock policy for the row.</param>
        private static void AddDataRow(DataTable disabledProducts, InvestorProductItem item, string policyNm)
        {
            DataRow newRow = disabledProducts.NewRow();
            newRow["Investor"] = item.InvestorName;
            newRow["ProductCode"] = item.ProductCode;
            newRow["LockPolicy"] = policyNm;
            newRow["DisableType"] = item.DisableStatus == E_InvestorProductDisabledStatus.Disabled ? "Manual" : "Auto";
            newRow["Message"] = item.MessagesToSubmittingUsers;
            newRow["Notes"] = item.Notes;
            newRow["LoginName"] = item.DisableEntryModifiedByUserName;
            newRow["TimeStamp"] = item.DisableEntryModifiedDate;
            newRow["PriceResultLabel"] = item.PriceResultLabel;
            newRow["PolicyId"] = item.LockPolicyId;
            newRow[PermittedActions] = "Remove";
            disabledProducts.Rows.Add(newRow);
        }

        /// <summary>
        /// Determines if a given row of the DataGrid has the specified permission
        /// </summary>
        /// <param name="row">The row to check.</param>
        /// <param name="permissionName">The permission name to check, e.g. "Remove" or "Download"</param>
        /// <returns>true if it has the specified permssion; false otherwise.</returns>
        private static bool HasPermission(DataGridItem row, string permissionName)
        {
            var hiddenInput = FindChildControls<System.Web.UI.HtmlControls.HtmlInputHidden>(row).Single(input => input.Attributes["class"] == PermittedActions);
            return hiddenInput.Value.Contains(permissionName, StringComparison.OrdinalIgnoreCase);
        }

        public static IEnumerable<T> FindChildControls<T>(Control baseControl) where T : Control
        {
            if (baseControl == null)
            {
                throw new ArgumentNullException("baseControl");
            }

            List<T> matchingControls = new List<T>();
            FindChildControlsImpl<T>(baseControl, matchingControls);
            return matchingControls;
        }

        private static void FindChildControlsImpl<T>(Control baseControl, List<T> matchingControls) where T : Control
        {
            if (baseControl == null)
            {
                return;
            }
            else if (baseControl is T)
            {
                matchingControls.Add((T)baseControl);
            }

            foreach (Control child in baseControl.Controls)
            {
                FindChildControlsImpl<T>(child, matchingControls);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;
            base.OnInit(e);
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterCSS("stylesheet.css");
            this.RegisterJsScript("utilities.js");
            this.RegisterJsScript("LQBPopup.js");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.LockPolicyNm.Text = this.LockPolicyId == Guid.Empty ? "All Policies" : this.LockPolicyNames[this.LockPolicyId];

            bool enabledForUser = this.IsManualExpirationEnabled;
            this.AccessDeniedPanel.Visible = !enabledForUser;
            this.AccessGrantedPanel.Visible = enabledForUser;

            if (!Page.IsPostBack)
            {
                this.BindDataGrid();
            }
            else
            {
                if (this.Request.Form["__EVENTTARGET"] == this.DisabledList.ClientID)
                {
                    if (this.Request.Form["__EVENTARGUMENT"] == "ReloadGrid")
                    {
                        this.BindDataGrid();
                    }
                    else
                    {
                        // Required for sort to work
                        this.RefreshGrid();
                    }

                    this.EmptyMsg.Visible = this.DisabledList.Items.Count == 0;
                }
            }
        }

        private void BindDataGrid()
        {
            this.CheckForUnexpiredPrograms();
            this.dataSet = CreateDataSet();

            foreach (var lockPolicy in this.LockPolicies)
            {
                foreach (InvestorProductItem item in InvestorProductManager.ListManualDisabledInvestorProductItem(this.Broker.BrokerID, lockPolicy.LockPolicyId))
                {
                    AddDataRow(this.dataSet.Tables["DisabledList"], item, lockPolicy.PolicyNm);
                }
            }

            if (this.Broker.HasLpLiveProductsEnabled)
            {
                this.LoadExpiredLpLiveProducts(this.dataSet.Tables["DisabledList"]);
            }
            else
            {
                this.DownloadRatesheetBtn.CssClass = "Hidden";
            }

            this.DisabledList.DataSource = this.dataSet.Tables["DisabledList"].DefaultView;
            this.DisabledList.DataBind();

            this.EmptyMsg.Visible = this.DisabledList.Items.Count == 0;

            StringWriter sw = new System.IO.StringWriter();
            this.dataSet.WriteXml(sw);
            this.ViewState.Add("dataset", sw.ToString());
        }

        private void RefreshGrid()
        {
            try
            {
                if (this.dataSet == null && this.ViewState["dataset"] != null)
                {
                    this.dataSet = CreateDataSet();
                    using (StringReader sr = new StringReader((string)this.ViewState["dataset"]))
                    {
                        this.dataSet.ReadXml(sr);
                    }
                }

                this.DisabledList.DataSource = this.dataSet.Tables["DisabledList"].DefaultView;
                this.DisabledList.DataBind();
            }
            catch (Exception exc)
            {
                ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.Generic, exc), false, Guid.Empty, Guid.Empty);
            }
        }

        protected void DisabledListOnItemDataBound(object sender, DataGridItemEventArgs e)
        {
            LinkButton productLink = (LinkButton)e.Item.FindControl("pCodeLink");
            var permittedActions = (System.Web.UI.HtmlControls.HtmlInputHidden)e.Item.FindControl("PermittedActions");
            DataRowView dataRow = (DataRowView)e.Item.DataItem;

            if (productLink != null)
            {
                string investor = (string)dataRow[INVESTOR];
                string productCode = (string)dataRow[PRODUCTCODE];
                Guid lockPolicyId = new Guid((string)dataRow["PolicyId"]);
                productLink.Text = productCode;
                productLink.OnClientClick = "f_listPrograms(" + AspxTools.JsString(investor) + ',' + AspxTools.JsString(productCode) + ',' + AspxTools.JsString(lockPolicyId) + ");return false;";
                permittedActions.Value = (string)dataRow[PermittedActions];
                permittedActions.Attributes.Add("class", PermittedActions);
            }
        }

        protected void RemoveBtn_Click(object sender, EventArgs e)
        {
            bool isDirty = false;
            foreach (DataGridItem row in this.DisabledList.Items)
            {
                CheckBox cb = (CheckBox)row.FindControl("selectItem");
                if (cb != null && cb.Checked && HasPermission(row, "Remove"))
                {
                    isDirty = true;
                    Guid lockPolicyId = new Guid(row.Cells[10].Text);
                    LinkButton lb = (LinkButton)row.FindControl("pCodeLink");
                    InvestorProductManager.Remove(this.Broker.BrokerID, lockPolicyId, row.Cells[1].Text, lb.Text);
                    Tools.LogInfo(string.Format("MRE: Product Manually Enabled\r\nProduct: {0}\r\nInvestor: {1}\r\nBy: {2}\r\nDate: {3}\r\nLockPolicy: {4} ({5})"
                        , lb.Text
                        , row.Cells[1].Text
                        , this.BrokerUser.LoginNm
                        , DateTime.Now
                        , this.LockPolicyNames[lockPolicyId]
                        , lockPolicyId));
                }
            }

            if (isDirty)
            {
                this.BindDataGrid();
            }
        }

        protected void DownloadRatesheetBtn_Click(object sender, EventArgs e)
        {
            HashSet<int> downloadListRowsToActivate = new HashSet<int>();
            Dictionary<Tuple<InvestorProduct, Guid>, bool> downloadRequestedUIStatus = new Dictionary<Tuple<InvestorProduct, Guid>, bool>();
            foreach (DataGridItem row in DisabledList.Items)
            {
                int downloadListId;
                CheckBox cb = (CheckBox)row.FindControl("selectItem");
                if (cb != null && HasPermission(row, "Download") && int.TryParse(row.Cells[11].Text, out downloadListId))
                {
                    var lockPolicyId = new Guid(row.Cells[10].Text);
                    var investorProduct = new InvestorProduct(row.Cells[1].Text, ((LinkButton)row.FindControl("pCodeLink")).Text);
                    bool isDownloadRequested = cb.Checked;
                    downloadRequestedUIStatus.Add(Tuple.Create(investorProduct, lockPolicyId), isDownloadRequested);
                    if (isDownloadRequested)
                    {
                        downloadListRowsToActivate.Add(downloadListId);
                    }
                }
            }

            Func<Tuple<InvestorProduct, Guid>, bool> isDownloadExplicitlyRequested = productWithLockPolicy => downloadRequestedUIStatus.ContainsKey(productWithLockPolicy) && downloadRequestedUIStatus[productWithLockPolicy];

            HashSet<InvestorProduct> processedProducts = new HashSet<InvestorProduct>(); // Ensures we don't process the same thing twice

            // Update each row affected by this click of Download Ratesheet.
            foreach (DataGridItem row in DisabledList.Items)
            {
                InvestorProduct rowProduct = new InvestorProduct(row.Cells[1].Text, ((LinkButton)row.FindControl("pCodeLink")).Text);

                int downloadListId;
                if (int.TryParse(row.Cells[11].Text, out downloadListId)
                    && downloadListRowsToActivate.Contains(downloadListId)
                    && HasPermission(row, "Download")
                    && processedProducts.Add(rowProduct))
                {
                    this.UpdateDisabledPricingForAllLockPolicies(rowProduct, isDownloadExplicitlyRequested);
                }
            }

            foreach (int downloadListId in downloadListRowsToActivate)
            {
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "RS_UpdateDownloadListEntryActiveByID", 3, new SqlParameter("ID", downloadListId), new SqlParameter("Active", "T"));
                this.LpLiveExpiredProductsWithDownloadListRow.RemoveWhere(row => row.Item3 == downloadListId);
            }

            if (downloadListRowsToActivate.Count > 0)
            {
                this.ViewState.Add("lpLiveExpiredProductsWithDownloadListRowId", this.LpLiveExpiredProductsWithDownloadListRow);
                this.BindDataGrid();
            }
        }

        /// <summary>
        /// Loads all Investor products and ratesheets to run determine rate sheet expiration for
        /// each lock policy on the page.
        /// </summary>
        private void CheckForUnexpiredPrograms()
        {
            // 4/12/2010 dd - OPM 44996 - Before load up the manual disable pricing list, we will execute code to remove "expire investor/product" that is no longer valid.
            // 4/12/2010 dd - I am only perform this check upon the page load for first time.
            if (!Page.IsPostBack && this.IsManualExpirationEnabled)
            {
                // We can't grab the needed info for the loader in time so we're stuck loading the entire thing.
                RateOptionExpirationDataLoader dataLoader = new RateOptionExpirationDataLoader(null, null, null, this.Broker);
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListInvestorProductCodeRsFileByBrokerId", new SqlParameter("@BrokerId", this.Broker.BrokerID)))
                {
                    while (reader.Read())
                    {
                        string lLpInvestorNm = (string)reader["lLpInvestorNm"];
                        string productCode = (string)reader["ProductCode"];
                        string lpeAcceptableRsFileId = (string)reader["LpeAcceptableRsFileId"];
                        long lpeAcceptableRsFileVersionNumber = (long)reader["LpeAcceptableRsFileVersionNumber"];

                        int lpLiveDownloadListRowId = -1;
                        if (this.Broker.HasLpLiveProductsEnabled)
                        {
                            lpLiveDownloadListRowId = this.FindLpLiveDownloadListRow(lpeAcceptableRsFileId);
                        }

                        foreach (var lockPolicy in this.LockPolicies)
                        {
                            RateOptionExpirationResult expirationResult = CApplicantPrice.DetermineRateOptionExpiration(
                                lockPolicy, 
                                E_LpeRsExpirationByPassPermissionT.NoByPass, 
                                lpeAcceptableRsFileId, 
                                lpeAcceptableRsFileVersionNumber, 
                                lLpInvestorNm, 
                                productCode, 
                                false, /* byPassManualDisabled */
                                dataLoader);
                            if (this.Broker.HasLpLiveProductsEnabled && lpLiveDownloadListRowId != -1 && expirationResult.IsBlockedRateLockSubmission)
                            {
                                this.LpLiveExpiredProductsWithDownloadListRow.Add(Tuple.Create(lockPolicy.LockPolicyId, new InvestorProduct(lLpInvestorNm, productCode), lpLiveDownloadListRowId));
                            }
                        }
                    }
                }

                if (this.Broker.HasLpLiveProductsEnabled && this.LpLiveExpiredProductsWithDownloadListRow.Count > 0)
                {
                    ViewState.Add("lpLiveExpiredProductsWithDownloadListRowId", this.LpLiveExpiredProductsWithDownloadListRow);
                }
            }
        }

        /// <summary>
        /// For a given Acceptable Rate sheet File id, returns the row of the Download List corresponding
        /// to the acceptable rate sheet, if the bot uses LP_Live.
        /// </summary>
        /// <param name="lpeAcceptableRsFileId">The file id of the acceptable rate sheet.</param>
        /// <returns>
        /// The download list row corresponding to the XLS file that generated the acceptable rate sheet 
        /// <paramref name="lpeAcceptableRsFileId"/> identifies, provided that this row's bot is LP_Live,
        /// or -1 if not found.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">
        /// More than one row of the download list corresponds to <paramref name="lpeAcceptableRsFileId"/>.
        /// </exception>
        private int FindLpLiveDownloadListRow(string lpeAcceptableRsFileId)
        {
            var rows = this.LpLiveDownloadListRows.Where(pair => pair.Value.Contains(lpeAcceptableRsFileId));
            if (rows.Any())
            {
                return rows.Single().Key;
            }

            return -1;
        }

        /// <summary>
        /// Populates the UI DataTable with the LP_Live products. We will update<para />
        /// the permissions of the rows that are already in the table and append<para />
        /// new rows for products not in the disabled products table in the database.<para />
        /// See Case 184983 for details.
        /// </summary>
        /// <param name="disabledProducts">The disabled products table.  We will append LP_Live products not already in the table.</param>
        private void LoadExpiredLpLiveProducts(DataTable disabledProducts)
        {
            List<DataRow> rowsToAdd = new List<DataRow>();
            foreach (var expiredProductWithDownloadListRow in this.LpLiveExpiredProductsWithDownloadListRow)
            {
                Guid lockPolicyId = expiredProductWithDownloadListRow.Item1;
                InvestorProduct expiredProduct = expiredProductWithDownloadListRow.Item2;
                int downloadListId = expiredProductWithDownloadListRow.Item3;

                var existingRowInTable = disabledProducts.Rows.Cast<DataRow>().SingleOrDefault(
                    row => lockPolicyId == new Guid((string)row["PolicyId"])
                        && expiredProduct.InvestorName == (string)row["Investor"]
                        && expiredProduct.ProductCode == (string)row["ProductCode"]);

                if (existingRowInTable != null)
                {
                    existingRowInTable["DownloadListId"] = downloadListId;
                    existingRowInTable[PermittedActions] = "RemoveOrDownload";
                    continue;
                }

                DataRow newRow = disabledProducts.NewRow();
                newRow["Investor"] = expiredProduct.InvestorName;
                newRow["ProductCode"] = expiredProduct.ProductCode;
                newRow["LockPolicy"] = this.LockPolicyNames[lockPolicyId];
                newRow["DisableType"] = "Auto";
                newRow["LoginName"] = "System";
                newRow["PriceResultLabel"] = "Red Pricing";
                newRow["Notes"] = "This product was disabled per the lock policy.";
                newRow["PolicyId"] = lockPolicyId;
                newRow["DownloadListId"] = downloadListId;
                newRow[PermittedActions] = "Download";
                rowsToAdd.Add(newRow);
            }

            // Do this later so that we don't search newly created products in the loop.
            foreach (var row in rowsToAdd)
            {
                disabledProducts.Rows.Add(row);
            }
        }

        /// <summary>
        /// Loads all LP_Live bots that are currently not active on the download list. We then parse through<para />
        /// the XLS file names and create a set of acceptable rate sheet file IDs for each download list id.
        /// </summary>
        /// <returns>
        /// A dictionary relating the ID of the LP_Live Download_List row to a HashSet of Acceptable Rate
        /// sheet file Ids generated by that entry.
        /// </returns>
        private Dictionary<int, HashSet<string>> LoadLpLiveBotsFromDownloadListToAcceptableRsFileIds()
        {
            // Find the list of all XLS files generated for each row of the Download List
            Dictionary<int, IEnumerable<string>> downloadListLpLiveFileEntries = new Dictionary<int, IEnumerable<string>>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_RetrieveDownloadList", new SqlParameter("Active", "F")))
            {
                while (reader.Read())
                {
                    if (reader["BotType"].ToString().Contains(ConstAppDavid.LPLiveBotFlag, StringComparison.OrdinalIgnoreCase))
                    {
                        int id = (int)reader["ID"];
                        IEnumerable<string> xlsFileNames = reader["FileName"].ToString()
                            .Split(',')
                            .Select(name => name.TrimWhitespaceAndBOM().ToUpper())
                            .Where(str => !string.IsNullOrEmpty(str));

                        downloadListLpLiveFileEntries.Add(id, xlsFileNames);
                    }
                }
            }

            // We now create a HashSet<string> of the Acceptable ratesheet generated for each XLS file.
            // We maintain the association to the row of the Download_List so that we will know which entry to access.
            Dictionary<int, HashSet<string>> downloadListIdToAllAcceptableRateSheets = new Dictionary<int, HashSet<string>>();
            foreach (var downloadListRow in downloadListLpLiveFileEntries)
            {
                HashSet<string> lpeAcceptableRsFileIds = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                foreach (var xlsFileName in downloadListRow.Value)
                {
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_File_GetAccRsFileIdsByInvXlsFileName", new SqlParameter("InvestorXlsFileName", xlsFileName)))
                    {
                        while (reader.Read())
                        {
                            lpeAcceptableRsFileIds.Add((string)reader["LpeAcceptableRsFileId"]);
                        }
                    }
                }

                downloadListIdToAllAcceptableRateSheets.Add(downloadListRow.Key, lpeAcceptableRsFileIds);
            }

            return downloadListIdToAllAcceptableRateSheets;
        }

        /// <summary>
        /// Updates the Disabled Pricing in the database for all Lock Policies of the current page.
        /// </summary>
        /// <param name="prod">The Investor and Product to update across the policies.</param>
        /// <param name="isDownloadExplicitlyRequested">
        /// true if the user had the row for <paramref name="prod"/> and a Lock Policy checked when they requested the download.
        /// </param>
        private void UpdateDisabledPricingForAllLockPolicies(InvestorProduct prod, Func<Tuple<InvestorProduct, Guid>, bool> isDownloadExplicitlyRequested)
        {
            // Update by lockPolicies individually so that we can preserve messagesToSubmittingUsers and isHiddenFromResult
            // for items that were already manually disabled.
            RateOptionExpirationDataLoader dataLoader = new RateOptionExpirationDataLoader(null, null, null, this.broker);
            foreach (var lockPolicy in this.LockPolicies)
            {
                string messagesToSubmittingUsers = string.Empty;
                bool isHiddenFromResult = false;
                string notes = "This product will be disabled until the requested download finishes processing.";

                InvestorProductItem disabledItem = InvestorProductManager.RetrieveManualInvestorProduct(this.Broker.BrokerID, lockPolicy.LockPolicyId, prod.InvestorName, prod.ProductCode, dataLoader);
                if (disabledItem != null)
                {
                    if (disabledItem.DisableStatus != E_InvestorProductDisabledStatus.DisabledTillNextLpeUpdate && !isDownloadExplicitlyRequested(Tuple.Create(prod, lockPolicy.LockPolicyId)))
                    {
                        continue;
                    }

                    messagesToSubmittingUsers = disabledItem.MessagesToSubmittingUsers;
                    isHiddenFromResult = disabledItem.IsHiddenFromResult;
                    if (!string.IsNullOrEmpty(disabledItem.Notes))
                    {
                        notes = disabledItem.Notes.StartsWith(notes) ? disabledItem.Notes : (notes + Environment.NewLine + disabledItem.Notes);
                    }

                    InvestorProductManager.Remove(this.Broker.BrokerID, lockPolicy.LockPolicyId, prod.InvestorName, prod.ProductCode);
                }

                InvestorProductManager.Add(
                    this.Broker.BrokerID,
                    lockPolicy.LockPolicyId,
                    prod.InvestorName,
                    prod.ProductCode,
                    E_InvestorProductDisabledStatus.DisabledTillNextLpeUpdate,
                    messagesToSubmittingUsers,
                    isHiddenFromResult,
                    notes,
                    this.BrokerUser);
            }
        }

        /// <summary>
        /// Cheap class for Investor/Product correlation, used to keep our head straight.
        /// </summary>
        [Serializable]
        private class InvestorProduct : IEquatable<InvestorProduct>
        {
            public string InvestorName { get; set; }
            public string ProductCode { get; set; }
            public InvestorProduct(string investorName, string productCode)
            {
                this.InvestorName = investorName;
                this.ProductCode = productCode;
            }
            public bool Equals(InvestorProduct other)
            {
                return other != null && string.Equals(this.InvestorName, other.InvestorName, StringComparison.OrdinalIgnoreCase) && string.Equals(this.ProductCode, other.ProductCode, StringComparison.OrdinalIgnoreCase);
            }
            public override bool Equals(object obj)
            {
                return this.Equals(obj as InvestorProduct);
            }
            public override int GetHashCode()
            {
                return Tuple.Create(this.InvestorName, this.ProductCode).GetHashCode();
            }
        }
    }

}
