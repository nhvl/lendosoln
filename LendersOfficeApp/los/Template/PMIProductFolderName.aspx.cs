using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOfficeApp.common.ModalDlg;

namespace LendersOfficeApp.los.Template
{

    public partial class PMIProductFolderName : LendersOffice.Common.BasePage
    {

		private BrokerUserPrincipal BrokerUser
		{
			// Get current user.
            get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		private String ErrorMessage
		{
			// Set current error.

			set
			{
				Page.ClientScript.RegisterHiddenField( "m_errorMessage" , value );
			}
		}

        private Guid m_parentFolderID 
        {
            get { return RequestHelper.GetGuid("parentid", Guid.Empty); }
        }

        private string m_parentFolderIDForSave
        {
            get { return m_parentFolderID == Guid.Empty ? null : m_parentFolderID.ToString(); }
        }
        private Guid m_folderID 
        {
            get { return RequestHelper.GetGuid("folderid", Guid.Empty); }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.LoginNm.Equals("pmlmaster"))
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, ErrorMessages.GenericAccessDenied);
            }

            if (!Page.IsPostBack)
            {
                m_FolderId.Text = Guid.NewGuid().ToString();
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

        protected void OnOKClick(object sender, System.EventArgs a)
        {
            
            int ret = StoredProcedureHelper.ExecuteNonQuery
                (DataSrc.LpeSrc, "CreatePMIProductFolder"
                , 1
                , new SqlParameter("@FolderName", m_nameTF.Text.TrimWhitespaceAndBOM())
                , new SqlParameter("@FolderId", m_FolderId.Text.TrimWhitespaceAndBOM())
                , new SqlParameter("@ParentFolderId", m_parentFolderIDForSave)
                );

            if (ret >= 0)
            {
                try
                {
                    cModalDlg.CloseDialog(this.Page, new string[] { 
                        "OK=true", 
                        "name='" + Utilities.SafeJsString(m_nameTF.Text.TrimWhitespaceAndBOM()) + "'", 
                        "folderid='" + m_FolderId.Text.TrimWhitespaceAndBOM() + "'" 
                    });
                }
                catch (System.Threading.ThreadAbortException)
                {
                    // Pass.

                    throw new CBaseException(ErrorMessages.Generic, "Done.");
                }
            }
            else
            {
                ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.Generic, "Unable to create new folder."), false, Guid.Empty, Guid.Empty);
            }
        }
    }
}
