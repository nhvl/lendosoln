﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchPriceGroupOperation.aspx.cs" Inherits="LendersOfficeApp.los.Template.BatchPriceGroupOperation" %>
<%@ Register TagPrefix="uc" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BatchPriceGroupOperation</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet" />
</head>
<body MS_POSITIONING="FlowLayout">
<script>
    function _init() {
		<% if( IsPostBack == false ) { %>
	       resize(670, 600);
		<% } %>
    }

    function selectAllNone(set) {
        var boxList = <%= AspxTools.JsGetElementById(PriceGroupList) %>;
        var allBoxes = boxList.getElementsByTagName("input");
        for (var i = 0; i < allBoxes.length; i++)
        {
            allBoxes[i].checked = set;
      	}
   }
</script>
    <h4 class="page-header">Batch Price Group Operations</h4>
    <form id="BatchPriceGroupOperation" method="post" runat="server">
	<table width="100%" border="0" cellspacing="2" cellpadding="3">
	<tr valign=top id="ProgramInputRow" runat="server">
        <td>
            <div>Enter program Ids separated by spaces, tabs, commas or semi-colons.</div>
            <asp:TextBox ID="ProgramIds" runat="server" TextMode="MultiLine" Width="550px" Rows="5"></asp:TextBox>
            <br /> <asp:Button ID="VerifyPrograms" Text="Verify List" runat="server" OnClick="verifyPrograms_Click" />
        </td>
    </tr>
	<tr valign=top id="ResultRow" runat="server">
        <td>
            <div style="BORDER-RIGHT: 2px; PADDING-RIGHT: 4px; BORDER-LEFT: lightgrey 2px solid; PADDING-LEFT: 4px; BORDER-TOP: lightgrey 2px solid; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; PADDING-BOTTOM: 4px; OVERFLOW-Y: scroll; WIDTH: 550px; HEIGHT: 180px">
                <asp:Table ID="Result" runat="server" Width="520px" />
            </div>
        </td>
    </tr>
	<tr valign=top id="PriceGroupInputRow" runat="server" >
        <td>

            <div class="FieldLabel">Price Groups<span style="padding-left:4px; font:smaller"><input type="checkbox" id="SelectAllCb" onclick="selectAllNone(this.checked);" /><label for="SelectAllCb">Select All</label></span></div>
            <div style="BORDER-RIGHT: 2px; PADDING-RIGHT: 4px; BORDER-LEFT: lightgrey 2px solid; PADDING-LEFT: 4px; BORDER-TOP: lightgrey 2px solid; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; PADDING-BOTTOM: 4px; OVERFLOW-Y: scroll; WIDTH: 550px; HEIGHT: 220px">

                <asp:CheckBoxList id="PriceGroupList" runat="server" />
            </div>
            <div class="FieldLabel">
            Visibility:<asp:DropDownList ID="VisibleChoice" runat="server">
                <asp:ListItem Selected="True" Text="Show" Value="1" ></asp:ListItem>
                <asp:ListItem Text="Hide" Value ="0" ></asp:ListItem>
            </asp:DropDownList>
            <br /></div>
        </td>
    </tr>
    <tr>
      <td align=center>
        <asp:Button Text="Perform Update" ID="RunOperation" runat="server" OnClick="RunOperation_Click" />
        <input type="button" value=" Close " onclick="onClosePopup()" >

      </td>
    </tr>
    </table>
     </form>
	<uc:cModalDlg id="CModalDlg" runat="server"></uc:cModalDlg>
  </body>
</html>
