using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
//using LendersOffice.Features;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.los.Template
{
	public partial class EditRateSheet : LendersOffice.Common.BasePage
	{
		protected RenderReadOnly                                     readOnly;
 //OPM 19511

		//08/22/07 mf. OPM 12259.

		private CLoanProductData m_Data = null;

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{
			set { ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." ); }
		}

		private String FeedBack
		{
			set { ClientScript.RegisterHiddenField( "m_feedBack" , value.TrimEnd( '.' ) + "." ); }
		}

		private String Command
		{
			set { ClientScript.RegisterHiddenField( "m_commandToDo" , value ); }
		}

		private Guid ProductId 
		{
			get { return RequestHelper.GetGuid("FileId"); }
		}

        protected bool IsLpDerived 
        {
            get 
            { 
                if (null != m_Data) 
                    return m_Data.IsDerived;
                else
                    return false;
            }
        }

        protected void PageInit(object sender, System.EventArgs a) 
        {
            if ( !BrokerUser.HasPermission(Permission.CanModifyLoanPrograms)  )
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "No permission to modify loan programs");
            }
        }

		/// <summary>
		/// Initialize page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Open up and check access privelages and bind grids.
            if (!IsPostBack) 
            {
                LoadData();
            }


		}

        private void LoadData() 
        {
            try
            {
                // Load up the inherited rate sheet so users can compare.
                // If not defined, show empty message.

                m_Data = new CLoanProductData( ProductId );

                m_Data.InitLoad();
                lLpTemplateNm.Text = m_Data.lLpTemplateNm;

				lRateSheetDownloadStartD_date.Text = m_Data.lRateSheetDownloadStartD.ToShortDateString();
				lRateSheetDownloadStartD_time.Text = m_Data.lRateSheetDownloadStartD.ToShortTimeString();
				lRateSheetDownloadEndD_date.Text   = m_Data.lRateSheetDownloadEndD.ToShortDateString();
				lRateSheetDownloadEndD_time.Text   = m_Data.lRateSheetDownloadEndD.ToShortTimeString();

				lPpmtPenaltyMonOverrideBit.Checked = m_Data.lPpmtPenaltyMonOverrideBit;
                lPpmtPenaltyMonOverrideBit.Visible = m_Data.IsDerived;
                lPpmtPenaltyMon.Text = m_Data.lPpmtPenaltyMon_rep;
                lPpmtPenaltyMonLowerSearch.Text = m_Data.lPpmtPenaltyMonLowerSearch_rep;

                lLockedDaysOverrideBit.Checked = m_Data.lLockedDaysOverrideBit;
                lLockedDaysOverrideBit.Visible = m_Data.IsDerived;
                lLockedDays.Text = m_Data.lLockedDays_rep;
                lLockedDaysLowerSearch.Text = m_Data.lLockedDaysLowerSearch_rep;

				lIOnlyMonLowerSearchOverrideBit.Checked = m_Data.lIOnlyMonLowerSearchOverrideBit;
				lIOnlyMonLowerSearchOverrideBit.Visible = m_Data.IsDerived;
				lIOnlyMonUpperSearchOverrideBit.Checked = m_Data.lIOnlyMonUpperSearchOverrideBit;
				lIOnlyMonUpperSearchOverrideBit.Visible = m_Data.IsDerived;
				lIOnlyMonLowerSearch.Text = m_Data.lIOnlyMonLowerSearch_rep;
				lIOnlyMonUpperSearch.Text = m_Data.lIOnlyMonUpperSearch_rep;

				LpeAcceptableRsFileId.Text = m_Data.LpeAcceptableRsFileId;
				LpeAcceptableRsFileVersionNumber.Text = m_Data.LpeAcceptableRsFileVersionNumber_rep;

				lRateOptionBaseId.Text = m_Data.lRateOptionBaseId;

                if( m_Data.lRateSheetXmlContentOverrideBit == false )
                {
                    m_UseInherited.Checked = true;
                }
                else
                {
                    m_UseOverrides.Checked = true;
                }

                m_OverridesRates.Text = "";
                foreach( CRateSheetFields rSpec in m_Data.RateSheetWithoutDeltas )
                {
                    if( m_OverridesRates.Text.Length > 0 )
                    {
                        m_OverridesRates.Text += "\n";
                    }

					m_OverridesRates.Text += string.Format( "{0} , {1} , {2} , {3} , {4}"
						, rSpec.Rate_rep
						, rSpec.Point_rep
						, rSpec.Margin_rep
						, rSpec.IsSpecialKeyword ? "0" : rSpec.QRateBase_rep  // Regular expression and special keyword schema
						, rSpec.IsSpecialKeyword ? "0" : rSpec.TeaserRate_rep // need a value here for import.
						);
                }

                m_RateDelta.Text = m_Data.lRateDelta_rep;

                m_FeeDelta.Text = m_Data.lFeeDelta_rep;

                // 1/5/2005 kb - If the product is derived, then it is
                // to be marked as readonly.  If the product is not of
                // this broker, then it is to be marked as readonly.

                if( m_Data.BrokerId == BrokerUser.BrokerId )
                {
                    if( m_Data.lBaseLpId == Guid.Empty )
                    {
                        readOnly.Skip = m_OverridesRates;
                        readOnly.Skip = m_CurrentRates;
                        readOnly.Skip = m_CurrentEmpty;
                    }
                    else
                    {
                        readOnly.Enabled = false;
                    }
                }
                else
                {
                    readOnly.Enabled = true;
                }
            }
            catch( Exception e )
            {
                // Oops!

                Tools.LogError( ErrorMessage = "Failed to load web form." , e );
            }
        }

		private void SaveData() 
		{
			try 
			{
				m_Data = new CLoanProductData( ProductId );
				m_Data.InitSave();

				if (m_Data.IsDerived) 
				{
					m_Data.lPpmtPenaltyMonOverrideBit = lPpmtPenaltyMonOverrideBit.Checked;
					m_Data.lLockedDaysOverrideBit = lLockedDaysOverrideBit.Checked;
					m_Data.lIOnlyMonUpperSearchOverrideBit = lIOnlyMonUpperSearchOverrideBit.Checked;
					m_Data.lIOnlyMonLowerSearchOverrideBit = lIOnlyMonLowerSearchOverrideBit.Checked;
				}

				m_Data.lPpmtPenaltyMon_rep = lPpmtPenaltyMon.Text;
				m_Data.lPpmtPenaltyMonLowerSearch_rep = lPpmtPenaltyMonLowerSearch.Text;
				m_Data.lLockedDays_rep = lLockedDays.Text;
				m_Data.lLockedDaysLowerSearch_rep = lLockedDaysLowerSearch.Text;
				m_Data.lIOnlyMonLowerSearch_rep = lIOnlyMonLowerSearch.Text;
				m_Data.lIOnlyMonUpperSearch_rep = lIOnlyMonUpperSearch.Text;

				m_Data.lRateSheetXmlContentOverrideBit = m_UseOverrides.Checked;

				if (!m_Data.IsDerived || (m_Data.IsDerived && m_Data.lRateSheetXmlContentOverrideBit)) 
				{
					m_Data.UploadCSVRateSheet( m_OverridesRates.Text );
					m_Data.lRateSheetDownloadStartD = DateTime.Parse( lRateSheetDownloadStartD_date.Text.TrimWhitespaceAndBOM() + " " + lRateSheetDownloadStartD_time.Text.TrimWhitespaceAndBOM() );
					m_Data.lRateSheetDownloadEndD = DateTime.Parse( lRateSheetDownloadEndD_date.Text.TrimWhitespaceAndBOM() + " " + lRateSheetDownloadEndD_time.Text.TrimWhitespaceAndBOM() );
					m_Data.LpeAcceptableRsFileId = LpeAcceptableRsFileId.Text;
					m_Data.LpeAcceptableRsFileVersionNumber_rep = LpeAcceptableRsFileVersionNumber.Text;
				}

				// OPM 19511.  Rate Option Id can only be set in base
				if ( !m_Data.IsDerived )
					m_Data.lRateOptionBaseId = lRateOptionBaseId.Text;

				m_Data.lRateDelta_rep = m_RateDelta.Text;
				m_Data.lFeeDelta_rep  = m_FeeDelta.Text;

				m_Data.Save();
			}
			catch (CBaseException e) 
			{
				ErrorMessage = e.UserMessage;
				Tools.LogError(e);
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to save." , e );
			}

		}
		/// <summary>
		/// Initialize page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Show final state of the editor.

			try
			{
                if (null == m_Data)
                    LoadData();

                // Disable the inherited table if not selected.
                // Bind the table either way (at least they'll
                // see a shadow of the table in overriding).

                if( m_UseInherited.Checked == false )
                {
                    m_InheritedRates.Enabled = false;
                    m_OverridesRates.ReadOnly = false;
                }
                else
                {
                    m_InheritedRates.Enabled = true;
                    m_OverridesRates.ReadOnly = true;
                }

                m_InheritedRates.DataSource = m_Data.RateSheetInherit;
                m_InheritedRates.DataBind();

                if( m_InheritedRates.Items.Count > 0 )
                {
                    m_InheritedRates.Visible = true;
                    m_InheritedEmpty.Visible = false;
                }
                else
                {
                    m_InheritedRates.Visible = false;
                    m_InheritedEmpty.Visible = true;
                }

                // Now bind our state to the final result.  Keep in
                // mind that the deltas should be applied at this
                // point.

                m_CurrentRates.DataSource = m_Data.GetRawRateSheetWithDeltas();
                m_CurrentRates.DataBind();

                if( m_CurrentRates.Items.Count > 0 )
                {
                    m_CurrentEmpty.Visible = false;
                    m_CurrentRates.Visible = true;
                }
                else
                {
                    m_CurrentEmpty.Visible = true;
                    m_CurrentRates.Visible = false;
                }

                // Cleanup denial panels so that we don't
                // leave them on by accident.

                m_InheritedDenied.Visible = false;
                m_CurrentDenied.Visible   = false;

                readOnly.SkipOff = false;

				// Now, close off the inherit ability if the
				// loan product is not derived.  Don't worry
				// about reversing this hiding because the
				// derivation state doesn't change here.

				if( m_Data.lBaseLpId == Guid.Empty )
				{
					if( m_OverridesRates.Attributes[ "AlternateHeight" ] != null )
					{
						m_OverridesRates.Height = Unit.Parse( m_OverridesRates.Attributes[ "AlternateHeight" ] );
					}

					m_InheritedPanel.Visible = false;
					m_OverridesPanel.Visible = false;

					m_MsgOverrides.Visible = true;
					lRateOptionBaseId.ReadOnly = false;
				}
				else
				{
					m_InheritedPanel.Visible = true;

					m_MsgOverrides.Visible = false;
					lRateOptionBaseId.ReadOnly = true;
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to render web form." , e );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
            this.PreRender += new System.EventHandler(this.PagePreRender);

        }
		#endregion


        protected string DisplayRate(object data) 
        {
            CRateSheetFields o = (CRateSheetFields) data;

            if (o.IsSpecialKeyword)
                return o.RateSheetKeyword.ToString();
            else
                return o.Rate_rep;
        }

        protected string DisplayPoint(object data) 
        {
            CRateSheetFields o = (CRateSheetFields) data;

            if (o.IsSpecialKeyword)
                return "Min: " + o.LowerLimit;
            else
                return o.Point_rep;

        }

        protected string DisplayMargin(object data) 
        {
            CRateSheetFields o = (CRateSheetFields) data;

            if (o.IsSpecialKeyword)
                return "Max: " + o.UpperLimit;
            else
                return o.Margin_rep;
        }

		protected string DisplayQualRate(object data) 
		{
			CRateSheetFields o = (CRateSheetFields) data;

			if (o.IsSpecialKeyword)
				return string.Empty;
			else
				return o.QRateBase_rep;
		}

		protected string DisplayTeaserRate(object data) 
		{
			CRateSheetFields o = (CRateSheetFields) data;

			if (o.IsSpecialKeyword)
				return string.Empty;
			else
				return o.TeaserRate_rep;
		}


		protected void ApplyClick( object sender , System.EventArgs a )
		{
            SaveData();
            LoadData();
		}

		protected void OkClick( object sender , System.EventArgs a )
		{
			// Commit current settings to the database.
            SaveData();
            Command = "Close";
		}

	}

}
