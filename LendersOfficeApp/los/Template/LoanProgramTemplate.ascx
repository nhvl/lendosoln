<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LoanProgramTemplate.ascx.cs" Inherits="LendersOfficeApp.los.Template.LoanProgramTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="LoanProgramArmDisclosure" Src="LoanProgramArmDisclosure.ascx" %>
<%@ Register TagPrefix="uc" TagName="CachedArrayList" Src="CachedArrayList.ascx" %>
<%@ Register TagPrefix="uc" TagName="RenderReadOnly" Src="../../Common/RenderReadOnly.ascx" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<input type="hidden" runat="server" id="DisableQualRateCalculationFieldsForShowDerived" />
<input type="hidden" runat="server" id="QualRateCalculationFieldT1" />
<input type="hidden" runat="server" id="QualRateCalculationFieldT2" />
<h4 class="page-header">Loan Program</h4>
<TABLE cellSpacing="0" cellPadding="8" width="100%" border="0">
	<tr>
		<td>
			<table cellSpacing="0" cellPadding="2" width="100%" border="0">
				<tr>
					<td class="FieldLabel" align="left"><asp:checkbox id="IsMaster" runat="server" Text="Is The Master Product for Its Folder" Enabled="False" /></td>
					<td align="right"><asp:textbox id="lLpTemplateId" style="TEXT-ALIGN: center" runat="server" Width="220px" ReadOnly="True" BorderStyle="None" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<TR>
		<TD>
			<table cellSpacing="0" cellPadding="2" width="100%" border="0">
				<tr>
					<td class="FormTableHeader1" colSpan="4">Main Characteristics
					</td>
				</tr>
			</table>
			<table cellSpacing="8" cellPadding="0" border="0">
				<tr>
					<td class="FieldLabel">Investor Name
					</td>
				</tr>
				<tr>
					<td>
					 <asp:textbox id="lLpInvestorNm_ed" runat="server" Width="200px"></asp:textbox><asp:dropdownlist id="lLpInvestorNm_ddl" runat="server" onchange="onchange_Investor_ddl()" Width="200px"></asp:dropdownlist>
					       <asp:RequiredFieldValidator id="lLpInvestorNm_ddlValidator"
                               ControlToValidate="lLpInvestorNm_ddl"
                               InitialValue="-1"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
					</td>
					<td>
				        <asp:CheckBox ID="lWholesaleChannelProgram" runat="server" Text="Is Wholesale Channel Program?" />
					</td>
                    <td>
                        <asp:CheckBox ID="IsNonQmProgram" runat="server" Text="Is this a non-QM program?" onclick="onIsNonQmProgramClick();"/>
                    </td>
                    <td>
                        <asp:CheckBox ID="IsDisplayInNonQmQuickPricer" runat="server" Text="Display in Non-QM QP?" />
				        <asp:checkbox id="IsDisplayInNonQmQuickPricer_in" runat="server" Enabled="False" Text="Display in Non-QM QP?" AutoPostBack="True" TextAlign="Right"></asp:checkbox>
				        <asp:checkbox id="IsDisplayInNonQmQuickPricer_ov" runat="server" Text="override?" AutoPostBack="True" TextAlign="Right"></asp:checkbox>
                    </td>
				</tr>
			</table>
			<table cellSpacing="8" cellPadding="0" border="0">
				<tr>
					<TD class="FieldLabel" noWrap>Product Code</TD>
					<td class="FieldLabel">Program Name</td>
					<td class="FieldLabel">Lender Name</td>
                    <td class="FieldLabel" noWrap>Product Identifier</td>
				</tr>
				<tr>
					<TD vAlign="top">
						<asp:textbox id="ProductCode" runat="server"></asp:textbox>
						<select id="ProductCode_ddl" onchange="onchange_Product_ddl()" runat="server"></select>
						<asp:dropdownlist id="ProductCode_in" runat="server" Enabled="False"></asp:dropdownlist>
						<input id="ProductCode_ddl_value" type="hidden" runat="server">
											       <asp:RequiredFieldValidator id="ProductCodeValidator"
           ControlToValidate="ProductCode_ddl"
           InitialValue=""
           EnableClientScript="true"
           runat="server"
           Enabled="false"
           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
           <asp:checkbox id="ProductCode_ov" runat="server" Text="override?" AutoPostBack="True" TextAlign="Right"></asp:checkbox>

					</TD>
                    <td>
                        <asp:TextBox ID="lLpTemplateNm_ed" runat="server" Width="265px"></asp:TextBox>
                        <asp:TextBox ID="lLpTemplateNm_in" runat="server" Width="265px" ReadOnly="True"></asp:TextBox>
                        <asp:CheckBox ID="lLpTemplateNm_ov" runat="server" Text="override?" AutoPostBack="True" TextAlign="Right"></asp:CheckBox></td>
					<td><asp:textbox id="lLendNm_ed" runat="server" Width="200px"></asp:textbox><asp:textbox id="lLendNm_in" runat="server" Width="200px" ReadOnly="True"></asp:textbox><asp:checkbox id="lLendNm_ov" runat="server" Text="override?" AutoPostBack="True" TextAlign="Right"></asp:checkbox></td>
                    <td>
                        <asp:TextBox ID="ProductIdentifier" runat="server" Width="256px"></asp:TextBox>
                        <asp:TextBox ID="ProductIdentifierInherited" runat="server" Width="265px" ReadOnly="true"></asp:TextBox>
                        <asp:CheckBox ID="ProductIdentifierOverride" runat="server" Text="override?" AutoPostBack="true" TextAlign="Right" />
                    </td>
				</tr>
			</table>
			<table cellSpacing="8" cellPadding="0" border="0">
				<tr>
					<td class="FieldLabel">Closing Cost Template
					</td>
				</tr>
				<tr>
					<td><asp:dropdownlist id="lCcTemplateId_dd" runat="server"></asp:dropdownlist><asp:dropdownlist id="lCcTemplateId_in" runat="server" Enabled="False"></asp:dropdownlist><asp:checkbox id="lCcTemplateId_ov" runat="server" Text="override?" AutoPostBack="True" TextAlign="Right"></asp:checkbox></td>
				</tr>
			</table>
			<table cellSpacing="8" cellPadding="0" border="0">
				<tr>
					<td class="FieldLabel">Loan Type
					</td>
					<td class="FieldLabel">Term / Due
					</td>
					<td class="FieldLabel">Amort. Type
					</td>
					<td class="FieldLabel">Amort. Description
					</td>
					<td class="FieldLabel">Lien Position
					</td>
				</tr>
				<tr>
					<td><asp:dropdownlist id="lLT_dd" onchange="onchange_lLT();" runat="server" Width="154px">
							<asp:ListItem Value="0">Conventional</asp:ListItem>
							<asp:ListItem Value="2">VA</asp:ListItem>
							<asp:ListItem Value="1">FHA</asp:ListItem>
							<asp:ListItem Value="3">USDA/Rural Housing</asp:ListItem>
							<asp:ListItem Value="4">Other</asp:ListItem>
						</asp:dropdownlist>
				       <asp:RequiredFieldValidator id="lLT_ddValidator"
																	       InitialValue="-1"
           ControlToValidate="lLT_dd"
           EnableClientScript="true"
           runat="server"
           Enabled="false"
           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
</td>


					<td><asp:textbox id="lTerm_ed" runat="server" Width="42px" MaxLength="3"></asp:textbox>
									       <asp:RequiredFieldValidator id="lTerm_edValidator"
           ControlToValidate="lTerm_ed"
           EnableClientScript="true"
           runat="server"
           Enabled="false"
           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
					&nbsp;/
						<asp:textbox id="lDue_ed" runat="server" Width="41px" MaxLength="3" onchange="f_onchange_lDue_ed();"></asp:textbox>
															       <asp:RequiredFieldValidator id="lDue_edValidator"
           ControlToValidate="lDue_ed"
           EnableClientScript="true"
           runat="server"
           Enabled="false"
           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>

						</td>
					<td><asp:dropdownlist id="lFinMethT_dd" onchange="onchange_lFinMethT()" runat="server">
							<asp:ListItem Value="0">Fixed</asp:ListItem>
							<asp:ListItem Value="1">ARM</asp:ListItem>
							<asp:ListItem Value="2">GPM</asp:ListItem>
						</asp:dropdownlist>
											       <asp:RequiredFieldValidator id="lFinMethT_ddValidator"
                               ControlToValidate="lFinMethT_dd"
                               InitialValue="-1"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>

						</td>
					<td><asp:textbox id="lFinMethDesc_ed" runat="server"></asp:textbox></td>
					<td><asp:dropdownlist id="lLienPosT_dd" runat="server" Width="70px" AutoPostBack="True">
							<asp:ListItem Value="0">
						First
					</asp:ListItem>
							<asp:ListItem Value="1">
						Second
					</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
				    <td colspan="5">
				        <asp:CheckBox ID="lLpmiSupportedOutsidePmi" runat="server" Text="LPMI Pricing Supported Outside of PMI Program Pairing?" />
				        <asp:checkbox id="lLpmiSupportedOutsidePmi_in" runat="server" Enabled="False" Text="LPMI Pricing Supported Outside of PMI Program Pairing?" AutoPostBack="True" TextAlign="Right"></asp:checkbox>
				        <asp:checkbox id="lLpmiSupportedOutsidePmi_ov" runat="server" Text="override?" AutoPostBack="True" TextAlign="Right"></asp:checkbox>
				    </td>
				</tr>
				<tr>
					<td colSpan="5" height="5"><IMG height="5" src="../../images/spacer.gif" width="100"></td>
				</tr>
				<tr>
					<td class="FieldLabel">Note Rate</td>
					<td class="FieldLabel">Qualify Rate</td>
					<td class="FieldLabel">Points</td>
					<td class="FieldLabel">Lock Period (days)</td>
					<td class="FieldLabel"></td>
				</tr>
				<TR>
					<TD><ml:percenttextbox id="lNoteR_ed" runat="server" width="70" preset="percent"></ml:percenttextbox><asp:hyperlink id="m_ratesheetLink2" runat="server" NavigateUrl="#" Visible="False" EnableViewState="False">
					See Rate Options
				</asp:hyperlink></TD>
					<td><ml:percenttextbox id="lQualR_ed" runat="server" width="70" preset="percent"></ml:percenttextbox><asp:hyperlink id="m_ratesheetLink4" NavigateUrl="#" Visible="False" EnableViewState="False" Runat="server">
					See Rate Options
					</asp:hyperlink></td>
					<td><asp:textbox id="lLOrigFPc_ed" runat="server" Width="90px"></asp:textbox><asp:hyperlink id="m_ratesheetLink1" runat="server" NavigateUrl="#" Visible="False" EnableViewState="False">
					See Rate Options
				</asp:hyperlink></td>
					<td><asp:textbox id="lLockedDays_ed" runat="server" Width="56px"></asp:textbox><asp:hyperlink id="m_ratesheetLink3" runat="server" NavigateUrl="#" Visible="False" EnableViewState="False">
					See Rate Options
				</asp:hyperlink></td>
					<td></td>
				</TR>
			</table>
			<asp:panel id="lLpeFeePanel_dv" style="TEXT-ALIGN: left" runat="server">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR>
						<TD class="FormTableHeader1">Pricing Engine Specific</TD>
					</TR>
					<TR>
						<TD style="PADDING-TOP: 8px">
							<DIV style="BORDER-RIGHT: dimgray 2px solid; PADDING-RIGHT: 12px; BORDER-TOP: dimgray 2px solid; PADDING-LEFT: 12px; PADDING-BOTTOM: 12px; BORDER-LEFT: dimgray 2px solid; WIDTH: 96%; PADDING-TOP: 12px; BORDER-BOTTOM: dimgray 2px solid; background-color: whitesmoke;">
								<TABLE cellSpacing="4" cellPadding="0" border="0">
									<TR vAlign="baseline">
										<TD class="FieldLabel">Back End Max Ysp</TD>
										<TD></TD>
										<TD colspan="3">
											<DIV><SPAN>static inherit </SPAN>&nbsp;
												<ml:PercentTextBox id="lLpeFeeMin_in" runat="server" ReadOnly="True" Width="70" preset="percent" /></DIV>
											<DIV>
												<asp:RadioButton id="lLpeFeeMin_ad" Text="add" runat="server" AutoPostBack="True" GroupName="lLpeFeeMin" />
												<asp:RadioButton id="lLpeFeeMin_ov" Text="override" runat="server" AutoPostBack="True" GroupName="lLpeFeeMin" />&nbsp;
												<ml:PercentTextBox id="lLpeFeeMin_pm" runat="server" Width="70" preset="percent" />&nbsp;
												<SPAN>= static max </SPAN>&nbsp;
												<ml:PercentTextBox id="lLpeFeeMin_ed" runat="server" ReadOnly="True" Width="70" preset="percent" /></DIV>
											<DIV><SPAN>Include rule adj: </SPAN>
												<asp:CheckBox id="lLpeFeeMin_ib" Enabled="False" Text="inherited" runat="server" />
												<asp:CheckBox id="lLpeFeeMin_ab" Enabled="False" Text="this program" runat="server" /></DIV>
										</TD>
									</TR>
									<TR>
										<TD class="FieldLabel">Min Ysp</TD>
										<TD></TD>
										<TD colspan="3">
											<ml:PercentTextBox id="lLpeFeeMax_ed" style="PADDING-LEFT: 4px" runat="server" Width="70" preset="percent" />
											<ml:PercentTextBox id="lLpeFeeMax_in" style="PADDING-LEFT: 4px" runat="server" ReadOnly="True" Width="70" preset="percent" />
											<asp:CheckBox id="lLpeFeeMax_ov" Text="override?" runat="server" TextAlign="Right" AutoPostBack="True" /></TD>
									</TR>
									<TR>
										<TD class="FieldLabel">Pairing</TD>
										<TD></TD>
										<TD colspan="3">
											<TABLE cellSpacing="0" cellPadding="0">
												<TR>
													<TD>
														<asp:Panel id="PairingProductIds_pn" runat="server">
															<asp:TextBox id="PairingProductIds_ed" runat="server" Width="360px" Height="90px" TextMode="MultiLine" />
															<asp:TextBox id="PairingProductIds_in" runat="server" ReadOnly="True" Width="360px" Height="90px" TextMode="MultiLine" />
															<DIV style="FONT: 11px arial; COLOR: dimgray">(Separate lien program ids by
																semicolon)</DIV>
														</asp:Panel>
														<asp:Panel id="PairIdFor1stLienProdGuid_pn" runat="server">
															<asp:TextBox id="PairIdFor1stLienProdGuid_tx" style="PADDING-LEFT: 4px" runat="server" ReadOnly="True" Width="220" />
															<asp:TextBox id="PairIdFor1stLienProdGuid_in" style="PADDING-LEFT: 4px" runat="server" ReadOnly="True" Width="220" />
														</asp:Panel></TD>
													<TD>
														<asp:CheckBox id="PairingProductIds_ov" Text="override?" runat="server" TextAlign="Right" AutoPostBack="True" /></TD>
												</TR>
												<TR>
													<TD>
														<asp:CheckBox id="IsPairedOnlyWithSameInvestor" Text="Only Pair With Same Investor" runat="server" /></TD>
												</TR>
												<TR>
													<TD>
														<asp:HyperLink id="PairingProd" onclick="onProdPairingClick();" NavigateUrl="#" Runat="server">Product Level Pairing</asp:HyperLink></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD class="FieldLabel"></TD>
										<TD></TD>
										<TD colspan="3">
											<asp:checkbox id="CanBeStandAlone2nd" Text="Can Be Stand-Alone Second" runat="server" /></TD>
									</TR>
									<TR>
										<TD class="FieldLabel"></TD>
										<TD></TD>
										<TD colspan="3" style="DISPLAY:none">
											<asp:checkbox id="lIsArmMarginDisplayed" Text="Display Margin" runat="server"/></TD>
									</TR>
									<TR>
										<TD class="FieldLabel"></TD>
										<TD></TD>
										<TD colspan="3">
											<asp:CheckBox id="IsOptionArm" Text="Is Option ARM" runat="server" onclick="onclick_IsOptionArm();" Visible="True" />&nbsp;
											<asp:CheckBox id="lHasQRateInRateOptions" onclick="onclick_lHasQRateInRateOptions();" Text="Use Q Rate" runat="server" /></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD></TD>
										<TD colspan="3">
											<asp:CheckBox id="IsLpeDummyProgram" Text="Dummy Program" Runat="server" /></TD>
									</TR>
									<TR id="lDtiUsingMaxBalPcrow">
										<TD></TD>
										<TD></TD>
										<TD colspan="3">
											<asp:CheckBox id="lDtiUsingMaxBalPc" Text="DTI using Max Balance" Runat="server" /></TD>
									</TR>
                                    <tr>
                                        <td class="FieldLabel">Q Rate Calculation</td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <input type="radio" id="QualRateCalculationFlatValue" name="QualRateCalculationT" runat="server" class="QualRateCalculationT" /> Flat Value
                                            &nbsp;
                                            <input type="radio" id="QualRateCalculationMaxOf" name="QualRateCalculationT" runat="server" class="QualRateCalculationT" /> Max Of
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                        <td>
                                            <select id="lQualRateCalculationFieldT1" class="QualRateCalculationField">
                                                <option value="<%=AspxTools.JsStringUnquoted(QualRateCalculationFieldT.LpeUploadValue)%>">LPE Upload Value</option>
                                                <option value="<%=AspxTools.JsStringUnquoted(QualRateCalculationFieldT.NoteRate)%>" selected="selected">Note Rate</option>
                                                <option value="<%=AspxTools.JsStringUnquoted(QualRateCalculationFieldT.FullyIndexedRate)%>">Fully Indexed Rate</option>
                                                <option value="<%=AspxTools.JsStringUnquoted(QualRateCalculationFieldT.Index)%>">Index</option>
                                                <option value="<%=AspxTools.JsStringUnquoted(QualRateCalculationFieldT.Margin)%>">Margin</option>
                                            </select>
                                            +
                                            <ml:PercentTextBox ID="lQualRateCalculationAdjustment1" runat="server"></ml:PercentTextBox>
                                        </td>
                                    </tr>
                                    <tr id="SecondCalculationRow">
                                        <td colspan="2">&nbsp;</td>
                                        <td>
                                            <select id="lQualRateCalculationFieldT2" class="QualRateCalculationField">
                                                <option value="<%=AspxTools.JsStringUnquoted(QualRateCalculationFieldT.NoteRate)%>">Note Rate</option>
                                                <option value="<%=AspxTools.JsStringUnquoted(QualRateCalculationFieldT.FullyIndexedRate)%>">Fully Indexed Rate</option>
                                                <option value="<%=AspxTools.JsStringUnquoted(QualRateCalculationFieldT.Index)%>">Index</option>
                                                <option value="<%=AspxTools.JsStringUnquoted(QualRateCalculationFieldT.Margin)%>">Margin</option>
                                            </select>
                                            +
                                            <ml:PercentTextBox ID="lQualRateCalculationAdjustment2" runat="server"></ml:PercentTextBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="FieldLabel">Q Term Calculation</td>
                                        <td>&nbsp;</td>
                                        <td colspan="3">
                                            <asp:DropDownList id="lQualTermCalculationType" runat="server" CssClass="lQualTermCalculationType" />
                                            &nbsp
                                            <input type="text" id="lQualTerm" runat="server" preset="numeric" class="lQualTerm" />
                                        </td>
                                    </tr>

									<TR>
										<TD class="FieldLabel">Display Payment</TD>
										<TD></TD>
										<TD colspan="3">
											<asp:DropDownList id="lLpDPmtT" runat="server">
												<asp:ListItem Value="1" Selected="True">DRate + P&I</asp:ListItem>
												<asp:ListItem Value="2">DRate + I/O</asp:ListItem>
												<asp:ListItem Value="3">QRate</asp:ListItem>
											</asp:DropDownList>
											<span style="color:red">HELOC ONLY:</span>
											<asp:DropDownList ID="lHelocPmtFormulaT" runat="server" />
											<asp:DropDownList ID="lHelocPmtBaseT" runat="server" />
											<asp:DropDownList ID="lHelocPmtFormulaRateT" runat="server" />
											<asp:DropDownList ID="lHelocPmtAmortTermT" runat="server" />
											<ml:moneytextbox id=lHelocPmtMb width="80" runat="server" preset="money" />
										</TD>
									</TR>
									<TR>
										<TD class="FieldLabel">Qualify Payment</TD>
										<TD></TD>
										<TD colspan="3">
											<asp:DropDownList id="lLpQPmtT" runat="server">
												<asp:ListItem Value="1" Selected="True">QRate + P&I</asp:ListItem>
												<asp:ListItem Value="2">QRate + I/O</asp:ListItem>
											</asp:DropDownList>
											<span style="color:red">HELOC ONLY:</span>
											<asp:DropDownList ID="lHelocQualPmtFormulaT" runat="server" />
											<asp:DropDownList ID="lHelocQualPmtBaseT" runat="server" />
											<asp:DropDownList ID="lHelocQualPmtFormulaRateT" runat="server" />
											<asp:DropDownList ID="lHelocQualPmtAmortTermT" runat="server" />
											<ml:moneytextbox id=lHelocQualPmtMb width="80" runat="server" preset="money" />
										</TD>
									</TR>
									<TR>
										<TD class="FieldLabel">Loan Product Type</TD>
										<TD></TD>
										<TD colspan="3">
											<asp:DropDownList id="lLpProductType_ddl" Width="165px" runat="server"></asp:DropDownList>
				       <asp:RequiredFieldValidator id="lLpProductType_ddlValidator"
           ControlToValidate="lLpProductType_ddl"
           EnableClientScript="true"
           runat="server"
           Enabled="false"
           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>

											</TD>
									</TR>
									<tr>
									    <td class="FieldLabel">HELOC Draw Period</td>
									    <td></td>
									    <td colspan="3"><span style="color:red">HELOC ONLY:</span><asp:TextBox ID="lHelocDraw" runat="server" width="50"/> mths</td>

									</tr>
                                    <tr>
									    <td></td>
									    <td></td>
									    <td colspan="3">
                                            <asp:CheckBox ID="lHelocCalculatePrepaidInterest" runat="server" /><ml:PassthroughLabel ID="lHelocCalculatePrepaidInterestLabel" runat="server" AssociatedControlID="lHelocCalculatePrepaidInterest">Calculate and collect prepaid interest <span style="color:red">HELOC ONLY</span></ml:PassthroughLabel>
									    </td>
									</tr>
									<tr>
									    <td colspan="5" style="padding-left:50px;">SAE NOTES: Due to time constraint there is no client side logic for enable/disable specific option relate to HELOC loan product type.
									    Therefore it is a responsibility of SAE to fill out value properly.
									    <br />See OPM 142243 for how Doug wants UI to work.
									    <br />
									    Note: When Loan Product Type is HELOC, ignore the drop down for display/qualify payment. Only modify field after the HELOC ONLY text.
									    <br />
									    Repay Period = Due - Draw Period. (When there is time SDE will add the field that perform calculation.)
									    </td>
									</tr>
									<tr>
									    <td class="FieldLabel">Custom Code 1</td>
									    <td></td>
									    <td><asp:TextBox ID="lLpCustomCode1" runat="server" width="200" />
									    <asp:CheckBox ID="lLpCustomCode1_ov" runat="server" AutoPostBack="true" Text="override?" />
									    </td>
                                        <td class="FieldLabel">Investor Code 1</td>
                                        <td>
                                          <asp:TextBox ID="lLpInvestorCode1" runat="server" width="200" />
                                          <asp:CheckBox ID="lLpInvestorCode1_ov" runat="server" AutoPostBack="true" Text="override?" />
                                        </td>
									</tr>
									<tr>
									    <td class="FieldLabel">Custom Code 2</td>
									    <td></td>
									    <td><asp:TextBox ID="lLpCustomCode2" runat="server" width="200" />
									    <asp:CheckBox ID="lLpCustomCode2_ov" runat="server" AutoPostBack="true" Text="override?" />
									    </td>
                                        <td class="FieldLabel">Investor Code 2</td>
                                        <td>
                                          <asp:TextBox ID="lLpInvestorCode2" runat="server" width="200" />
                                          <asp:CheckBox ID="lLpInvestorCode2_ov" runat="server" AutoPostBack="true" Text="override?" />
                                        </td>
									</tr>
									<tr>
									    <td class="FieldLabel">Custom Code 3</td>
									    <td></td>
									    <td><asp:TextBox ID="lLpCustomCode3" runat="server" width="200" />
									    <asp:CheckBox ID="lLpCustomCode3_ov" runat="server" AutoPostBack="true" Text="override?" />
									    </td>
                                        <td class="FieldLabel">Investor Code 3</td>
                                        <td>
                                          <asp:TextBox ID="lLpInvestorCode3" runat="server" width="200" />
                                          <asp:CheckBox ID="lLpInvestorCode3_ov" runat="server" AutoPostBack="true" Text="override?" />
                                        </td>
									</tr>
									<tr>
									    <td class="FieldLabel">Custom Code 4</td>
									    <td></td>
									    <td><asp:TextBox ID="lLpCustomCode4" runat="server" width="200" />
									    <asp:CheckBox ID="lLpCustomCode4_ov" runat="server" AutoPostBack="true" Text="override?" />
									    </td>
                                        <td class="FieldLabel">Investor Code 4</td>
                                        <td>
                                          <asp:TextBox ID="lLpInvestorCode4" runat="server" width="200" />
                                          <asp:CheckBox ID="lLpInvestorCode4_ov" runat="server" AutoPostBack="true" Text="override?" />
                                        </td>
									</tr>
									<tr>
									    <td class="FieldLabel">Custom Code 5</td>
									    <td></td>
									    <td><asp:TextBox ID="lLpCustomCode5" runat="server" width="200" />
									    <asp:CheckBox ID="lLpCustomCode5_ov" runat="server" AutoPostBack="true" Text="override?" />
									    </td>
                                        <td class="FieldLabel">Investor Code 5</td>
                                        <td>
                                          <asp:TextBox ID="lLpInvestorCode5" runat="server" width="200" />
                                          <asp:CheckBox ID="lLpInvestorCode5_ov" runat="server" AutoPostBack="true" Text="override?" />
                                        </td>
									</tr>
								</TABLE>
							</DIV>
						</TD>
					</TR>
				</TABLE>
			</asp:panel><br>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0" id="armTable">
				<tr>
					<td class="FormTableHeader1">Adjustable Rate Mortgage
					</td>
				</tr>
				<tr>
					<td style="PADDING-TOP: 8px"><uc:loanprogramarmdisclosure id="armDisclosure" runat="server" Width="100%"></uc:loanprogramarmdisclosure></td>
				</tr>
			</table>
			<br>
			<table id="negAmTable" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td class="FormTableHeader1" colSpan="5">Adjustable Payment Mortgage (Potential
						Negative Amortization)</td>
				</tr>
				<TR>
					<TD class="FieldLabel">Adjust Cap</TD>
					<TD class="FieldLabel">Adjust Period</TD>
					<TD class="FieldLabel">Recast Period</TD>
					<TD class="FieldLabel">Recast Stop
					</TD>
					<TD class="FieldLabel">Max Balance</TD>
				</TR>
				<TR>
					<TD><ml:percenttextbox id="lPmtAdjCapR_ed" runat="server" width="70" preset="percent"></ml:percenttextbox></TD>
					<TD><asp:textbox id="lPmtAdjCapMon_ed" runat="server" Width="31px" MaxLength="3"></asp:textbox>&nbsp;months</TD>
					<TD><asp:textbox id="lPmtAdjRecastPeriodMon_ed" runat="server" Width="31px" MaxLength="3"></asp:textbox>&nbsp;months</TD>
					<TD><asp:textbox id="lPmtAdjRecastStop_ed" runat="server" Width="33px" MaxLength="3"></asp:textbox>&nbsp;months</TD>
					<TD><ml:percenttextbox id="lPmtAdjMaxBal_ed" runat="server" width="70" preset="percent"></ml:percenttextbox></TD>
				</TR>
			</table>
			<br>
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="40%" rowSpan="3">
						<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="FormTableHeader1" colSpan="3">Buydown Mortgage</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="WIDTH: 16px"></TD>
								<TD class="FieldLabel" style="WIDTH: 45px">Rate:</TD>
								<TD class="FieldLabel" style="WIDTH: 137px">Term (mths):</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="WIDTH: 16px">1</TD>
								<TD style="WIDTH: 45px"><ml:percenttextbox id="lBuydwnR1_ed" runat="server" width="70" preset="percent"></ml:percenttextbox></TD>
								<TD style="WIDTH: 137px"><asp:textbox id="lBuydwnMon1_ed" runat="server" Width="80px" MaxLength="3"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="WIDTH: 16px">2</TD>
								<TD style="WIDTH: 45px"><ml:percenttextbox id="lBuydwnR2_ed" runat="server" width="70" preset="percent"></ml:percenttextbox></TD>
								<TD style="WIDTH: 137px"><asp:textbox id="lBuydwnMon2_ed" runat="server" Width="78px" MaxLength="3"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="WIDTH: 16px">3</TD>
								<TD style="WIDTH: 45px"><ml:percenttextbox id="lBuydwnR3_ed" runat="server" width="70" preset="percent"></ml:percenttextbox></TD>
								<TD style="WIDTH: 137px"><asp:textbox id="lBuydwnMon3_ed" runat="server" Width="78px" MaxLength="3"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="WIDTH: 16px">4</TD>
								<TD style="WIDTH: 45px"><ml:percenttextbox id="lBuydwnR4_ed" runat="server" width="70" preset="percent"></ml:percenttextbox></TD>
								<TD style="WIDTH: 137px"><asp:textbox id="lBuydwnMon4_ed" runat="server" Width="78px" MaxLength="3"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel" style="WIDTH: 16px">5</TD>
								<TD style="WIDTH: 45px"><ml:percenttextbox id="lBuydwnR5_ed" runat="server" width="70" preset="percent"></ml:percenttextbox></TD>
								<TD style="WIDTH: 137px"><asp:textbox id="lBuydwnMon5_ed" runat="server" Width="78px" MaxLength="3"></asp:textbox></TD>
							</TR>
						</TABLE>
					</TD>
					<TD width="6" rowSpan="3">&nbsp;</TD>
					<TD vAlign="top" width="60%">
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0" id="graduatedTable">
							<TR>
								<TD class="FormTableHeader1" colSpan="2">Graduated Payment Mortgage</TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Years:</TD>
								<TD width="100%"><asp:textbox id="lGradPmtYrs_ed" runat="server" Width="83px"></asp:textbox></TD>
							</TR>
							<TR>
								<TD class="FieldLabel">Rate:</TD>
								<TD width="100%"><ml:percenttextbox id="lGradPmtR_ed" runat="server" width="70" preset="percent"></ml:percenttextbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top">
						<TABLE id="Table9" cellSpacing="0" cellPadding="0" width="100%" border="0">
						    <tr>
						        <td class="FormTableHeader1" colspan="2">Prepayment Penalty Info</td>
						    </tr>
						    <tr>
						        <td class="FieldLabel" noWrap>Prepayment Penalty (mths):</td>
						        <td width="100%">
						            <asp:TextBox ID="lHardPrepmtPeriodMonths" runat="server" Width="40px" onchange="onchange_lHardPrepmtPeriodMonths();" Text="0"></asp:TextBox>hard
						            <asp:CustomValidator ID="lHardPrepmtPeriodMonthsValidator"
						                runat="server"
						                ControlToValidate="lHardPrepmtPeriodMonths"
						                ValidateEmptyText="true"
						                ClientValidationFunction="f_validate_lHardPrepmtPeriodMonths"
						                OnServerValidate="Validate_lHardPrepmtPeriodMonths"
						                Enabled="false">
						                <img runat="server" src="../../images/error_icon.gif">
						            </asp:CustomValidator>
						            <asp:TextBox ID="lSoftPrepmtPeriodMonths" runat="server" Width="40px" onchange="onchange_lSoftPrepmtPeriodMonths();" Text="0"></asp:TextBox>soft
						            <asp:CustomValidator ID="lSoftPrepmtPeriodMonthsValidator"
						                runat="server"
						                ControlToValidate="lSoftPrepmtPeriodMonths"
						                ValidateEmptyText="true"
						                ClientValidationFunction="f_validate_lSoftPrepmtPeriodMonths"
						                OnServerValidate="Validate_lSoftPrepmtPeriodMonths"
						                Enabled="false">
						                <img runat="server" src="../../images/error_icon.gif">
						            </asp:CustomValidator>
						        </td>
						    </tr>
							<TR>
								<TD class="FormTableHeader1" colSpan="2">Interest Only</TD>
							</TR>
							<TR>
								<TD class="FieldLabel" noWrap>Interest only(mths):</TD>
								<TD width="100%"><asp:textbox id="lIOnlyMon_ed" runat="server" Width="40px" MaxLength="3"></asp:textbox>

								<asp:RequiredFieldValidator id="lIOnlyMon_edValidator"
                               ControlToValidate="lIOnlyMon_ed"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
                           <asp:RegularExpressionValidator ID="lIOnlyMon_edFormatValidator"
                               ControlToValidate="lIOnlyMon_ed"
                               EnableClientScript="true"
                               ValidationExpression="[0-9]+"
                               runat="server"
                               Enabled="false"
                               ><img runat="server" runat="server" src="../../images/error_icon.gif"></asp:RegularExpressionValidator>

								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			<br>
			<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD noWrap><asp:checkbox id="lAprIncludesReqDeposit_chk" runat="server" Text="Required deposit: The annual percentage rate does not take into account your required deposit." Width="603px"></asp:checkbox></TD>
				</TR>
				<TR>
					<TD noWrap><asp:checkbox id="lHasDemandFeature_chk" runat="server" Text="Demand feature: This obligation has a demand feature." Width="603px"></asp:checkbox></TD>
				</TR>
				<TR>
					<TD noWrap><asp:checkbox id="lHasVarRFeature_chk" runat="server" Text="Variable rate feature: This loan contains a variable rate feature. A variable rate disclosure has been provided earlier." Width="717px"></asp:checkbox></TD>
				</TR>
				<TR>
					<TD noWrap><asp:textbox id="lVarRNotes_ed" runat="server" Width="641px" Height="77px"></asp:textbox></TD>
				</TR>
			</TABLE>
			<br>
			<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="FieldLabel" noWrap>Filling Fees:</TD>
					<TD><asp:textbox id="lFilingF_ed" runat="server" Width="70px"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="FieldLabel" noWrap>Late charge:</TD>
					<TD>If a payment is more than
						<asp:textbox id="lLateDays_ed" runat="server" Width="51px"></asp:textbox>&nbsp;days
						late, you will be charged&nbsp;&nbsp;<ml:percenttextbox id="lLateChargePc_ed" runat="server" width="70" preset="percent"></ml:percenttextbox>of
						<asp:textbox id="lLateChargeBase_ed" runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="FieldLabel" noWrap>Prepayment:</TD>
					<TD>If you pay off early, then you</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD><asp:dropdownlist id="lPrepmtPenaltyT_dd" runat="server" Width="120px">
							<asp:ListItem Value="1" Selected="True">
						Will not
					</asp:ListItem>
							<asp:ListItem Value="2">
						May
					</asp:ListItem>
						</asp:dropdownlist>
																	       <asp:RequiredFieldValidator id="lPrepmtPenaltyT_ddValidator"
                               ControlToValidate="lPrepmtPenaltyT_dd"
                               InitialValue="-1"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>


						have to pay a penalty of
						<asp:textbox id="lPpmtPenaltyMon_ed" runat="server" Width="60px"></asp:textbox>months.
					</TD>
				</TR>
				<TR>
					<TD></TD>
					<TD><asp:dropdownlist id="lPrepmtRefundT_dd" runat="server" Width="120px">
							<asp:ListItem Value="1" Selected="True">Will not</asp:ListItem>
							<asp:ListItem Value="2">May</asp:ListItem>
						</asp:dropdownlist>

																	       <asp:RequiredFieldValidator id="lPrepmtRefundT_ddValidator"
                               ControlToValidate="lPrepmtRefundT_dd"
                               InitialValue="-1"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>

						&nbsp;be entitled to a refund of part of the finance charge.</TD>
				</TR>
				<TR>
					<TD class="FieldLabel" noWrap>Assumption:</TD>
					<TD>Someone buying your property
						<asp:dropdownlist id="lAssumeLT_dd" runat="server">
							<asp:ListItem Value="1" Selected="True">may not</asp:ListItem>
							<asp:ListItem Value="2">may</asp:ListItem>
							<asp:ListItem Value="3">may, subject to conditions</asp:ListItem>
						</asp:dropdownlist>
																	       <asp:RequiredFieldValidator id="lAssumeLT_ddValidator"
                               ControlToValidate="lAssumeLT_dd"
                               InitialValue="-1"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>


						&nbsp;assume the remainder of your loan on the original
						terms.</TD>
				</TR>
			</TABLE>
			<br>
			<hr>
			<asp:panel id="isEnabled_pn" style="TEXT-ALIGN: center" runat="server">
				<asp:CheckBox id="isEnabled_cb" Text="Allow Using This Program" runat="server" CssClass="FieldLabel"></asp:CheckBox>
				<asp:CheckBox id="isEnabled_in" Enabled="False" Text="Allow Using This Program" runat="server" CssClass="FieldLabel"></asp:CheckBox>
				<asp:CheckBox id="isEnabled_ov" Text="override?" runat="server" TextAlign="Right" AutoPostBack="True"></asp:CheckBox>
			</asp:panel></TD>
	</TR>
	<TR>
		<TD align="center">
            <span>
                <asp:button id="m_okBtn" style="DISPLAY: none" onclick="OkClick" runat="server" Text="OK" Width="60"></asp:button>
                <input id="m_okPrx" style="WIDTH: 60px" onclick="if (validateProductCode() && Page_ClientValidate('') ) { populateCalculationFields(); parentElement.children[ 0 ].click(); disabled = true; } else { return false; }" type="submit" value="OK" runat="server">
			</span>
			<% if(m_isAuthorCreateMode) { %>
			<asp:button ID="authorCancel" OnClick="OnAuthorCancelClick" Text="Cancel" runat="server" CausesValidation="false" Visible=false />
			<asp:button ID="authorApply" OnClick="OnAuthorApplyClick" OnClientClick="populateCalculationFields();" Text="Apply" runat=server Visible=false />

			<% } else { %>
			<INPUT style="WIDTH: 60px" onclick="onClosePopup();" type="button" value="Cancel" id="CancelBtn">
			<asp:button id="m_applyBtn" onclick="ApplyClick" OnClientClick="populateCalculationFields();" runat="server" Text="Apply" Width="60"></asp:button>
			<% } %>
			</TD>
	</TR>
</TABLE>
<uc:renderreadonly id="readOnly" runat="server"></uc:renderreadonly>
<script>
var InvestorArray;
var ProductArray;
var bProductCodeValid = false;
var ShouldUseQualRate = null;
var LegacyValueName = 'LPE Upload Value';
var QualRateCalculationFlatValue = null;
var QualRateCalculationMaxOf = null;
var QualRateCalculationFieldT1 = null;
var QualRateCalculationFieldT2 = null;
var QualRateCalculationAdjustment1 = null;
var QualRateCalculationAdjustment2 = null;

function _init()
{
    ShouldUseQualRate = document.getElementById(QualRateFieldIds.ShouldUseQualRate);
    QualRateCalculationFlatValue = document.getElementById(QualRateFieldIds.QualRateCalculationFlatValue);
    QualRateCalculationMaxOf = document.getElementById(QualRateFieldIds.QualRateCalculationMaxOf);
    QualRateCalculationFieldT1 = document.getElementById('lQualRateCalculationFieldT1');
    QualRateCalculationFieldT2 = document.getElementById('lQualRateCalculationFieldT2');
    QualRateCalculationAdjustment1 = document.getElementById(QualRateFieldIds.QualRateCalculationAdjustment1);
    QualRateCalculationAdjustment2 = document.getElementById(QualRateFieldIds.QualRateCalculationAdjustment2);

  <%--// These are based on Pricing Engine fields. cannot use when in non-SAE mode. --%>
  <% if (BrokerUser.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms)) { %>

    $j(QualRateCalculationFieldT1).val(document.getElementById(QualRateFieldIds.QualRateField1ValuePlaceholder).value);
    $j(QualRateCalculationFieldT2).val(document.getElementById(QualRateFieldIds.QualRateField2ValuePlaceholder).value);

  var isShowDerived = document.getElementById(QualRateFieldIds.DisableQualRateCalculationFieldsForShowDerived).value === "True";
  if (!isShowDerived) {
    $j('select.QualRateCalculationField, input.QualRateCalculationT').change(updateQualRateCalculationModel);
  }

  onclick_lHasQRateInRateOptions();
  onclick_IsOptionArm();
  Page_ClientValidate('');
  <% } %>

  onchange_lFinMethT();
  onchange_lLT();
  // OPM 16984 Cord
  load_ProductCode_Data();

  $(".lQualTermCalculationType").change();

  onIsNonQmProgramClick();
}

function onIsNonQmProgramClick() {
    <%=AspxTools.JQuery(IsDisplayInNonQmQuickPricer)%>.prop('disabled', !<%=AspxTools.JQuery(IsNonQmProgram)%>.prop('checked'));
    <%=AspxTools.JQuery(IsDisplayInNonQmQuickPricer)%>.prop('checked', <%=AspxTools.JQuery(IsNonQmProgram)%>.prop('checked'));   
}

function populateCalculationFields() {
    document.getElementById(QualRateFieldIds.QualRateField1ValuePlaceholder).value = $j(QualRateCalculationFieldT1).val();
    document.getElementById(QualRateFieldIds.QualRateField2ValuePlaceholder).value = $j(QualRateCalculationFieldT2).val();
}

function shouldQualRateModelUpdateReturnEarly() {
    var isShowDerived = document.getElementById(QualRateFieldIds.DisableQualRateCalculationFieldsForShowDerived).value === "True";
    if (isShowDerived) {
        disableDDL(QualRateCalculationFieldT1, true);
        disableDDL(QualRateCalculationFieldT2, true);
    }

    return isShowDerived;
}

function hasLegacyOption() {
    // Always allow SAEs to select the legacy LPE upload value.
    // This method will be called when updating the qual rate
    // calculation dropdowns from code in QualRateCalculation.js
    return true;
}

function onProdPairingClick()
{
  var prodCode = <%= AspxTools.JsGetElementById(ProductCode_ddl) %>.value;
  var investor = <%= AspxTools.JsGetElementById(lLpInvestorNm_ddl) %>.value;
  showModal('/los/RatePrice/ProductPairing.aspx?investor=' + escape(investor) + '&prodcode=' + escape(prodCode));
  return false;
}

  //
  // OPM 16984 Cord
  // Creates two parallel arrays InvestorArray[] and ProductArray[].
  // InvestorArray[] holds the names of Investors which have valid products.
  // and ProductArray[] holds the corresponding Valid products.
  // ----
  // TODO: Consider changing to callback
  // on Investor selected index change.
  // That would require disabling mousewheel/keyboard scrolling, etc.
  // to prevent spamming the database with callbacks for onchange events.

function load_ProductCode_Data()
{
  <% if ( BrokerUser.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) ) { %>
		InvestorArray = new Array(<%=AspxTools.JsNumeric(GetInvestorCount())%>);
		ProductArray = new Array(InvestorArray.length);

		<%
			System.Data.Common.DbDataReader dR = DataAccess.StoredProcedureHelper.ExecuteReader(DataAccess.DataSrc.LpeSrc, "ListInvestorProductWithValidProductCode");

			int count = -1;
			String currentInvestor = "";
			String lastInvestor = "";

			// Build the arrays
			if( dR.Read() )
			{
				count++;

				currentInvestor = dR.GetString(0);
				lastInvestor = currentInvestor;

				//Add new Investor
				Response.Write( "InvestorArray[0] = \"" + SafeJsString(currentInvestor) + "\";" );	// InvestorArray[0] = "currentInvestor";

				//Begin new array in products
				Response.Write( "ProductArray[0] = new Array(" );	// ProductArray[0] = new Array(

				//Add the first entry to products
				Response.Write( "\"" + SafeJsString(dR.GetString(1)) + "\"" );	// "item"

				while( dR.Read() )
				{
					currentInvestor = dR.GetString(0);

					if( lastInvestor != currentInvestor )
					{
						//End current product array declaration
						Response.Write( ");" );

						lastInvestor = currentInvestor;
						count++;

						//Add new investor;
						Response.Write( "InvestorArray[" + count + "] = \"" + SafeJsString(currentInvestor) + "\";" ); // Investor[count] = "currentInvestor";

						//Begin new array in products
						Response.Write( "ProductArray[" + count + "] = new Array(" );	// ProductArray[count] = new Array(

						//Add the first entry to products
						Response.Write( "\"" + SafeJsString(dR.GetString(1)) + "\"" );		// "item"
					}
					else
					{
						// Add another product to the existing list
						Response.Write( ", \"" + SafeJsString(dR.GetString(1)) + "\"" );		// , "item"
					}
				}

				Response.Write( ");" );	//	);
			}

			dR.Close();
		%>

		populateProductCodes();

   <% } %>

}


// OPM 16984 Cord
// List the ProductCodes for the selected investor
<% if ( BrokerUser.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) ) { %>
	function populateProductCodes()
	{
		var Product_ddl = <%= AspxTools.JsGetElementById(ProductCode_ddl) %>;
		var Investor_ddl = <%= AspxTools.JsGetElementById(lLpInvestorNm_ddl) %>;

		var investorName = Investor_ddl.value;
		var investorIndex = -1;


		// Find the index of the investor's entry in the InvestorArray,
		// which corresponds to their ProductCode list in the ProductArray.
		for(var j = 0; j < InvestorArray.length; j++)
		{
			if ( investorName == InvestorArray[j] )
			{
				investorIndex = j;
			}
		}

		// Clear out old ProductCodes from the dropdownlist.
		Product_ddl.selectedIndex = 0;
		Product_ddl.options.length = 0;
		bProductCodeValid = false;

		var offset = 0;

		// If the page loaded in 'edit' mode, try to
		// put the Program's current ProductCode at the top.
		<% if ( RequestHelper.GetSafeQueryString("mode") == "edit" ) { %>
		if (investorName == <%=AspxTools.JsString( RequestHelper.GetSafeQueryString("investorName")) %>)
		{
			Product_ddl.options[0] = new Option( <%=AspxTools.JsString( RequestHelper.GetSafeQueryString("productCode") )%>, <%=AspxTools.JsString( RequestHelper.GetSafeQueryString("productCode"))%>);
			offset = 1;
		}
		<% } %>

		var iDuplicate = -1;

		// If the investor has valid ProductCodes
		// then populate the lists
		if ( investorIndex != -1 )
		{
			for(var i = 0; i < ProductArray[investorIndex].length; i++)
			{
				Product_ddl.options[i + offset] = new Option( ProductArray[investorIndex][i], ProductArray[investorIndex][i] );
			}

			// We have to check for duplicates
			// if we put the current Product Code
			// at the top.
			if (offset == 1)
			{
				var duplicate = false;

				for(var j = 1; j < Product_ddl.options.length; j++)
				{
					if (Product_ddl.options[0].value == Product_ddl.options[j].value)
					{
						duplicate = true;

						// There can only be one duplicate
						// so no worries about changing the
						// options.length while in our loop
						Product_ddl.options.remove(j);
					}
				}

				// If a duplicate wasn't found while in
				// edit mode, then we know that the
				// ProductCode did not appear again because
				// it is not currently Valid
				if(duplicate == false)
				{
					Product_ddl.options[0].text += " (Invalid)";
				}
			}
		}
		else if ( offset == 1 )
		{
			Product_ddl.options[0].text += " (Invalid)";
		}

		// We want to disable the dropdown when there are no valid
		// products to choose from. However if we are editing a editing
		// a derived loan program, we need to skip the enabling/disabling
		// to avoid accidentally re-enabling the dropdown when it should be
		// totally disabled.
		if (Product_ddl.options.length == 0)
		{
			Product_ddl.options[0] = new Option( "--No Valid Products--", "" );

			// Check if we are editing a derived loan (in which case
			// the investor dropdownlist is disabled). If not, proceed with disabling.
			if (<%= AspxTools.JsGetElementById(lLpInvestorNm_ddl) %>.disabled != true)
				Product_ddl.disabled = true;

			bProductCodeValid = false;
		}
		else
		{
			// Check if we are editing a derived loan (in which case
			// the investor dropdownlist is disabled). If not, proceed with enabling.
			if (<%= AspxTools.JsGetElementById(lLpInvestorNm_ddl) %>.disabled != true)
				Product_ddl.disabled = false;

			bProductCodeValid = true;
		}


		// Reselects the last Product Code selected
		// after a postback
		var selectedProductName = <%= AspxTools.JsGetElementById(ProductCode_ddl_value) %>.value;

		if ( bNewInvestorSelected == false )
		{
			for(var k = 0; k < Product_ddl.options.length; k++)
			{
				if (Product_ddl.options[k].value == selectedProductName)
				{
					Product_ddl.selectedIndex = k;

				}
			}
		}
		else
		{
			// Store the selected ProductCode
			<%= AspxTools.JsGetElementById(ProductCode_ddl_value) %>.value = <%= AspxTools.JsGetElementById(ProductCode_ddl) %>.value;
		}
	}
<% } %>

<%-- OPM 10103 fs 06/25/08 --%>
function f_removeSpecialCharacters()
{
	for (var i = 0; i < document.forms.length; i++)
	{
		for (var j = 0; j < document.forms[i].elements.length; j++)
		{
			var tbox = document.forms[i].elements[j];
			if ((tbox != null) && (tbox.type == 'text') || (tbox.type == 'textarea'))
			{	<%--Any character which is not a letter, digit or '-%+_/()#.,:' will be removed --%>
				var progNameId = <%= AspxTools.JsGetClientIdString(lLpTemplateNm_ed) %>;
                var lateChargeBaseId = <%= AspxTools.JsGetClientIdString(lLateChargeBase_ed) %>;
				if(progNameId == tbox.id) { // opm 124898: Program Name allows <> characters
				    tbox.value = tbox.value.replace(/[^a-z0-9-%+/_()=#.,:<> ]/gi,'');
				}
                else if (tbox.id === lateChargeBaseId) {
                    // opm 244861 - allow '&' in late charge base description.
				    tbox.value = tbox.value.replace(/[^a-z0-9-%+/_()=#.,: &]/gi,'');
				}
				else {
				    tbox.value = tbox.value.replace(/[^a-z0-9-%+/_()=#.,: ]/gi,'');
                }
			}
		}
	}
}

// OPM 16984 Cord
function validateProductCode()
{
f_removeSpecialCharacters();
<% if ( BrokerUser.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) ) { %>

	var isOverride = false;

	var overrideCb =  <%= AspxTools.JsGetElementById(ProductCode_ov) %>;
	if ( overrideCb != null && overrideCb.checked )
	{
	    isOverride = true;
	}

	if ( isOverride == true && bProductCodeValid == false )
	{
		alert('No product code selected.');
		return false;
	}
<% } %>
	return true;
}

// OPM 16984 Cord
// Tells the ProductCode dropdown to repopulate it's data
// for the newly selected investor.
var bNewInvestorSelected = false;

function onchange_Investor_ddl()
{
	// Toss the stored selected value for the old investor's product.
	bNewInvestorSelected = true;
	<%= AspxTools.JsGetElementById(ProductCode_ddl_value) %>.value = '';

	// Load the new ProductCodes for this investor.
	populateProductCodes();

	<% if(m_isAuthorCreateMode) { %>

	// Need to add a blank entry for prod code
	// when switching investors.
	var Product_ddl = <%= AspxTools.JsGetElementById(ProductCode_ddl) %>;

	if ( Product_ddl.disabled == false)
	{
	    var oBlank = document.createElement("option");
	    oBlank.text = "";
	    oBlank.value = ""
	    Product_ddl.add(oBlank, 0);
	    Product_ddl.selectedIndex = 0;
	    Page_ClientValidate('');
	}

	<% } %>

	// Store the selected ProductCode.
	bNewInvestorSelected = false;
	<%= AspxTools.JsGetElementById(ProductCode_ddl_value) %>.value = <%= AspxTools.JsGetElementById(ProductCode_ddl) %>.value;
}

// OPM 16984 Cord
// Stores the Product Code that was selected by the user
function onchange_Product_ddl()
{
	var productCode_ddl = <%= AspxTools.JsGetElementById(ProductCode_ddl)%>;

	<%= AspxTools.JsGetElementById(ProductCode_ddl_value) %>.value = productCode_ddl.value;
}

function onchange_lLT()
{
    var lpmiCB = <%= AspxTools.JsGetElementById(lLpmiSupportedOutsidePmi) %>;

    if (lpmiCB == null)
        lpmiCB = <%= AspxTools.JsGetElementById(lLpmiSupportedOutsidePmi_in) %>;

    var lLT_ddl = <%= AspxTools.JsGetElementById(lLT_dd) %>;

    if(lpmiCB != null)
    {
        if (lLT_ddl.value != '0')
        {
            lpmiCB.disabled = true;
            lpmiCB.checked = false;
        }
        else
        {
            if (lLT_ddl.disabled == false)
                lpmiCB.disabled = false;
        }
    }
}

function onchange_lFinMethT()
{
    var marginCB = <%= AspxTools.JsGetElementById(lIsArmMarginDisplayed) %>;
    var variableRateCB = <%= AspxTools.JsGetElementById(lHasVarRFeature_chk) %>;
    var armArea = document.getElementById("armTable");
    var gradArea = document.getElementById("graduatedTable");
//    if ( marginCB != null )
//    {
        var lFinMethT_ddl = <%= AspxTools.JsGetElementById(lFinMethT_dd) %>;
        if ( lFinMethT_ddl.value == "1" ) // ARM...
        {
          if (f_updateForArm != null)
            f_updateForArm(true);

          if ( marginCB != null )
          {
            marginCB.checked = "checked";
            variableRateCB.checked = "checked";
          }

          armArea.style.display = '';
          gradArea.style.display = 'none';
        }
        else if ( lFinMethT_ddl.value == "2" ) // GPM...
        {
            if (f_updateForArm != null)
                f_updateForArm(false);
            armArea.style.display = 'none';
            gradArea.style.display = 'block';
        }
        else
        {
            if (f_updateForArm != null)
                 f_updateForArm(false);

            armArea.style.display = 'none';
            gradArea.style.display = 'none';
        }
//    }
}


function onclick_lHasQRateInRateOptions()
{
  var lLpQPmtT = <%= AspxTools.JsGetElementById(lLpQPmtT) %>;
  var lHasQRateInRateOptions = <%= AspxTools.JsGetElementById(lHasQRateInRateOptions) %>;
  if ( lHasQRateInRateOptions && lHasQRateInRateOptions.disabled == false )
    lLpQPmtT.disabled = !lHasQRateInRateOptions.checked;

  var displayPaymentDDL = <%= AspxTools.JsGetElementById(lLpDPmtT) %>;
  var qRateOptionValue = <%= AspxTools.JsString(E_sLpDPmtT.QRate.ToString("d")) %>;
  var qRateOptionIndex = -1;
  for (var i = 0; i < displayPaymentDDL.length; i++) {
    if (displayPaymentDDL.options[i].value === qRateOptionValue) {
      qRateOptionIndex = i;
      break;
    }
  }
  if (lHasQRateInRateOptions.checked && qRateOptionIndex === -1) {
    var qRateOption = document.createElement('option');
    qRateOption.value = qRateOptionValue;
    qRateOption.innerHTML = 'QRate';
    displayPaymentDDL.appendChild(qRateOption);
  }
  else if (!lHasQRateInRateOptions.checked) {
    displayPaymentDDL.remove(qRateOptionIndex);
  }

  updateQualRateCalculationModel();
}

function onclick_IsOptionArm()
{
  // hide dti using.., adjustable payment mortgage
  var lDtiUsingMaxBalPc = document.getElementById('lDtiUsingMaxBalPcrow');
  var negAmTable = document.getElementById('negAmTable');

  if ( <%= AspxTools.JsGetElementById(IsOptionArm) %>.checked )
  {
    lDtiUsingMaxBalPc.style.display = 'inline';
    negAmTable.style.display = 'block';
  }
  else
  {
    lDtiUsingMaxBalPc.style.display = 'none';
    negAmTable.style.display = 'none';
  }


}

function onchange_lLpDPmtT()
{
<%--
//  var lLienPosT = <%= AspxTools.JsGetElementById(lLienPosT_dd) %>;
//  var lLpDPmtT = <%= AspxTools.JsGetElementById(lLpDPmtT) %>;
//  if ( lLienPosT && lLienPosT.value == "1" && lLpDPmtT && lLpDPmtT.value == "1" )
//  {
//    alert('This option is not allowed for 2nd lien at this point. Please see internal opm case # 9541 for more details.');
//    lLpDPmtT.selectedIndex = 0;
//  }
--%>
}
	// -->

function f_onchange_lDue_ed() {
    var hardValidator = <%= AspxTools.JsGetElementById(lHardPrepmtPeriodMonthsValidator) %>;
    var softValidator = <%= AspxTools.JsGetElementById(lSoftPrepmtPeriodMonthsValidator) %>;

    ValidatorValidate(hardValidator);
    ValidatorValidate(softValidator);

    ValidatorUpdateIsValid();
}

function f_validate_lHardPrepmtPeriodMonths(source, args) {
    args.IsValid = f_validatePrepmtPeriodMonths(parseInt(<%= AspxTools.JsGetElementById(lHardPrepmtPeriodMonths) %>.value, 10));
}
function f_validate_lSoftPrepmtPeriodMonths(source, args) {
    args.IsValid = f_validatePrepmtPeriodMonths(parseInt(<%= AspxTools.JsGetElementById(lSoftPrepmtPeriodMonths) %>.value, 10));
}

function f_validatePrepmtPeriodMonths(val) {
    // Must be >= 0 and < due
    var due = parseInt(<%= AspxTools.JsGetElementById(lDue_ed) %>.value, 10);
    if (isNaN(val) || isNaN(due)) return false;
    else if (val >= 0 && val < due ) return true;
}

function f_validate_lPrepmtPenaltyT_dd() {
    var prepmtPenaltyTValidator = <%= AspxTools.JsGetElementById(lPrepmtPenaltyT_ddValidator) %>;
    ValidatorValidate(prepmtPenaltyTValidator);
    ValidatorUpdateIsValid();
}

function onchange_lHardPrepmtPeriodMonths() {
    f_update_lPrepmtPenaltyT();
    f_validate_lPrepmtPenaltyT_dd();
}

function onchange_lSoftPrepmtPeriodMonths() {
    f_update_lPrepmtPenaltyT();
    f_validate_lPrepmtPenaltyT_dd();
}

function f_update_lPrepmtPenaltyT() {
    var hard = parseInt(<%= AspxTools.JsGetElementById(lHardPrepmtPeriodMonths) %>.value, 10);
    var soft = parseInt(<%= AspxTools.JsGetElementById(lSoftPrepmtPeriodMonths) %>.value, 10);

    // If either of these values is > 0, then set sPrepmtPenaltyT to "may"
    if (hard > 0 || soft > 0) {
        <%= AspxTools.JsGetElementById(lPrepmtPenaltyT_dd) %>.value = <%= AspxTools.JsString(E_sPrepmtPenaltyT.May) %>;
    }
    else {
        <%= AspxTools.JsGetElementById(lPrepmtPenaltyT_dd) %>.value = <%= AspxTools.JsString(E_sPrepmtPenaltyT.WillNot) %>;
    }
}

    $(".lQualTermCalculationType").change(function () {
        var calcType = this.value;
        var $lQualTerm = $(".lQualTerm");
        var lTerm = <%=AspxTools.JsGetElementById(lTerm_ed)%>
        var lIOnlyMon = <%=AspxTools.JsGetElementById(lIOnlyMon_ed)%>

        // Set Qual Term value.
        switch (calcType)
        {
            case '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Standard)%>':
                $lQualTerm.val(lTerm.value);
                break;
            case '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Amortizing)%>':
                $lQualTerm.val(lTerm.value - lIOnlyMon.value);
                break;
            case '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.InterestOnly)%>':
                $lQualTerm.val(lIOnlyMon.value);
                break;
        }

        // Set Qual Term readonly.
        var isManual = calcType == '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Manual)%>';
        $lQualTerm.prop("readonly", !isManual);
    });
</script>
