using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.los.Template
{
	public partial class LoanProgramTemplate1 : LendersOffice.Common.BasePage
	{
		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		protected void PageLoad( object sender , System.EventArgs a )
		{
            if (false == BrokerUser.HasPermission(Permission.CanModifyLoanPrograms)
                && false == BrokerUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms))
            {
                throw new AccessDenied("You do not have the necessary permissions to access this page.");
            }

            this.RegisterJsScript("QualRateCalculation.js");
		}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            this.EnableJqueryMigrate = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

	}

}
