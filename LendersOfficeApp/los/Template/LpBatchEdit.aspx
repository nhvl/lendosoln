<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="LpBatchEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.LpBatchEdit" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>LpBatchEdit</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body onload="init();" MS_POSITIONING="FlowLayout">
		<form id="LpBatchEdit" method="post" runat="server">
            <script type="text/javascript" language="javascript">
		    var ShouldUseQualRate = null;
	        var LegacyValueName = 'LPE Upload Value';
	        var QualRateCalculationFlatValue = null;
	        var QualRateCalculationMaxOf = null;
	        var QualRateCalculationFieldT1 = null;
	        var QualRateCalculationFieldT2 = null;
	        var QualRateCalculationAdjustment1 = null;
	        var QualRateCalculationAdjustment2 = null;

		    function populateCalculationFields() 
		    {
		        document.getElementById(QualRateFieldIds.QualRateField1ValuePlaceholder).value = $(QualRateCalculationFieldT1).val();
		        document.getElementById(QualRateFieldIds.QualRateField2ValuePlaceholder).value = $(QualRateCalculationFieldT2).val();
		    }

		    function hasLegacyOption() 
		    {
		        // Always allow SAEs to select the legacy LPE upload value.
		        // This method will be called when updating the qual rate 
		        // calculation dropdowns from code in QualRateCalculation.js
		        return true;
		    }

		    function onUpdateQRateCalculationChange()
		    {
		        var updateQualRateCalculation = document.getElementById('QRateCalculation_upd').checked;

		        setQualRateCalculationOptionsDisabled(!updateQualRateCalculation);

		        if (updateQualRateCalculation) {
		            updateQualRateCalculationModel();
		        }
		    }

		    function updateQualTermCalc_click() {
		        var updateQualTermCalc =  <%= AspxTools.JsGetElementById(updateQualTermCalc) %>;
                var lQualTermCalculationType = <%= AspxTools.JsGetElementById(lQualTermCalculationType) %>;
		        var lQualTerm = <%= AspxTools.JsGetElementById(lQualTerm) %>;

		        onUpdateChange(updateQualTermCalc, lQualTermCalculationType);
		        onUpdateChange(updateQualTermCalc, lQualTerm);
		        lQualTermCalculationType_change();
		    }

            function lQualTermCalculationType_change() {
                var updateQualTermCalc =  <%= AspxTools.JsGetElementById(updateQualTermCalc) %>;
                var isManual = $(".lQualTermCalculationType").val() == '<%=AspxTools.JsStringUnquoted(QualTermCalculationType.Manual)%>';
                $(".lQualTerm").prop("readonly", !updateQualTermCalc.checked || !isManual);
            }

		function onUpdateChange(upd, ed, ov)
		{
			ed.disabled = !upd.checked ;
			if (ed.tagName == "INPUT" && ed.type == "text" || ed.tagName == "TEXTAREA")
				ed.readOnly = !upd.checked;
			
			if (!upd.checked && (ed.tagName == "INPUT" && ed.type == "text" || ed.tagName == "TEXTAREA")) 
			{
				ed.style.backgroundColor = 'silver' ;
			}
			else
			{
				ed.style.backgroundColor = '' ;
			}
			if (ov != null)
			{
			    if(ov.tagName == "span")
			    {
			        $(ov).find("input, select, textarea").prop("disabled", !upd.checked);
			    }
			    else {
				ov.disabled = !upd.checked ;
				ov.checked = true ;
			}
		}
		}
		
		var InvestorArray;
		var ProductArray;
		var bProductCodeValid = false;


		function load_ProductCode_Data()
		{
		<% if ( BrokerUser.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) ) { %>  
			InvestorArray = new Array(<%=AspxTools.JsNumeric(GetInvestorCount())%>);
			ProductArray = new Array(InvestorArray.length);
		    
		<%
			System.Data.Common.DbDataReader dR = DataAccess.StoredProcedureHelper.ExecuteReader(DataAccess.DataSrc.LpeSrc, "ListInvestorProductWithValidProductCode");

			int count = -1;
			String currentInvestor = "";
			String lastInvestor = "";
			
			// Build the arrays
			if( dR.Read() )
			{
				count++;	

				currentInvestor = dR.GetString(0);
				lastInvestor = currentInvestor;

				//Add new Investor
				Response.Write( "InvestorArray[0] = \"" + SafeJsString(currentInvestor) + "\";" );	// InvestorArray[0] = "currentInvestor";
				
				//Begin new array in products
				Response.Write( "ProductArray[0] = new Array(" );	// ProductArray[0] = new Array(

				//Add the first entry to products
				Response.Write( "\"" + SafeJsString(dR.GetString(1)) + "\"" );	// "item"
				
				while( dR.Read() )
				{
					currentInvestor = dR.GetString(0);
			
					if( lastInvestor != currentInvestor )
					{
						//End current product array declaration
						Response.Write( ");" );
						
						lastInvestor = currentInvestor;
						count++;		
			
						//Add new investor;					
						Response.Write( "InvestorArray[" + count + "] = \"" + SafeJsString(currentInvestor) + "\";" ); // Investor[count] = "currentInvestor";
			
						//Begin new array in products
						Response.Write( "ProductArray[" + count + "] = new Array(" );	// ProductArray[count] = new Array(
			
						//Add the first entry to products
						Response.Write( "\"" + SafeJsString(dR.GetString(1)) + "\"" );		// "item"
					}
					else
					{
						// Add another product to the existing list
						Response.Write( ", \"" + SafeJsString(dR.GetString(1)) + "\"" );		// , "item"
					}
				}
				
				Response.Write( ");" );	//	);
			}

			dR.Close();
		%>
   
			populateProductCodes();
   
		<% } %>

		}


	// OPM 16984 Cord
	// List the ProductCodes for the selected investor
	<% if ( BrokerUser.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) ) { %>
		function populateProductCodes() 
		{
			var Product_ddl = <%= AspxTools.JsGetElementById(lLpProductCode_ddl) %>;
			var Investor_ddl = <%= AspxTools.JsGetElementById(InvestorName_ddl) %>;
	   
			var investorName = Investor_ddl.value;
			var investorIndex = -1;
		
		
			// Find the index of the investor's entry in the InvestorArray,
			// which corresponds to their ProductCode list in the ProductArray.
			for(var j = 0; j < InvestorArray.length; j++)
			{
				if ( investorName == InvestorArray[j] )
				{
					investorIndex = j;
				}
			}
		
			// Clear out old ProductCodes from the dropdownlist.
			Product_ddl.selectedIndex = 0;
			Product_ddl.options.length = 0;
			bProductCodeValid = false;
		
			var offset = 0;
		
			// If the page loaded in 'edit' mode, try to
		    // put the Program's current ProductCode at the top.
			<% if ( RequestHelper.GetSafeQueryString("mode") == "edit") { %>
				if (investorName == <%=AspxTools.JsString( RequestHelper.GetSafeQueryString("investorName") )%>)
				{
					Product_ddl.options[0] = new Option( <%=AspxTools.JsString( RequestHelper.GetSafeQueryString("productCode") )%>, <%=AspxTools.JsString( RequestHelper.GetSafeQueryString("productCode") )%>);
					offset = 1;
				}
			<% } %>
		
			// If the investor has valid ProductCodes
			// then populate the lists
			if ( investorIndex != -1 )
			{
				for(var i = 0; i < ProductArray[investorIndex].length; i++)
				{
					Product_ddl.options[i + offset] = new Option( ProductArray[investorIndex][i], ProductArray[investorIndex][i] );
				}
			
				// We have to check for duplicates
				// if we put the current Product Code
				// at the top.
				if (offset == 1)
				{
					var duplicate = false;
				
					for(var j = 1; j < Product_ddl.options.length; j++)
					{
						if (Product_ddl.options[0].value == Product_ddl.options[j].value)
						{
							duplicate = true;
							
							// There can only be one duplicate
							// so no worries about changing the
							// options.length while in our loop
							Product_ddl.options.remove(j);  
						}
					}
				
					// If a duplicate wasn't found while in
					// edit mode, then we know that the
					// ProductCode did not appear again because
					// it is not currently Valid
					if(duplicate == false)
					{
						Product_ddl.options[0].text += " (Invalid)";
					}
				}
			}
			else if ( offset == 1 )
			{
				Product_ddl.options[0].text += " (Invalid)";
			}
		
			// We want to disable the dropdown when there are no valid 
			// products to choose from. However if we are editing a 
			// derived loan program, we need to skip the enabling/disabling
			// to avoid accidentally re-enabling the dropdown when it should be
			// totally disabled.
			if (Product_ddl.options.length == 0)
			{
				Product_ddl.options[0] = new Option( "--No Valid Products--", "" );
			
				// Check if we are editing a derived loan (in which case
				// the investor dropdownlist is disabled). If not, proceed with disabling.
				if (<%= AspxTools.JsGetElementById(InvestorName_ddl) %>.disabled != true)
					Product_ddl.disabled = true;
			
				bProductCodeValid = false;
			}
			else
			{
				// Check if we are editing a derived loan (in which case
				// the investor dropdownlist is disabled). If not, proceed with enabling.
				if (<%= AspxTools.JsGetElementById(InvestorName_ddl) %>.disabled != true)
					Product_ddl.disabled = false;
				
				bProductCodeValid = true;
			}
		
		
			// Reselects the last Product Code selected
			// after a postback
			var selectedProductName = <%= AspxTools.JsGetElementById(lLpProductCode_ddl_value) %>.value;

			if ( bNewInvestorSelected == false )
			{
				for(var k = 0; k < Product_ddl.options.length; k++)
				{			
					if (Product_ddl.options[k].value == selectedProductName)
					{
						Product_ddl.selectedIndex = k;
					}	
				}
			}
			else
			{
				// Store the selected ProductCode
				<%= AspxTools.JsGetElementById(lLpProductCode_ddl_value) %>.value = <%= AspxTools.JsGetElementById(lLpProductCode_ddl) %>.value;
			}	
		}
	<% } %>

		function setdefault_ProductCode()
		{
			<%= AspxTools.JsGetElementById(lLpProductCode_ddl_value) %>.value = <%= AspxTools.JsGetElementById(lLpProductCode_ddl) %>.value;
		}

		// OPM 16984 Cord
		// Tells the ProductCode dropdown to repopulate it's data
		// for the newly selected investor.
		var bNewInvestorSelected = false;

		function onchange_Investor_ddl()
		{
			// Toss the stored selected value for the old investor's product.
			bNewInvestorSelected = true;
			<%= AspxTools.JsGetElementById(lLpProductCode_ddl_value) %>.value = '';
			// Load the new ProductCodes for this investor.
			populateProductCodes();
			// Store the selected ProductCode.
			bNewInvestorSelected = false;
			<%= AspxTools.JsGetElementById(lLpProductCode_ddl_value) %>.value = <%= AspxTools.JsGetElementById(lLpProductCode_ddl) %>.value;
		}

		// OPM 16984 Cord
		// Stores the Product Code that was selected by the user
		function onchange_ProductCode_ddl()
		{
			var productCode_ddl = <%= AspxTools.JsGetElementById(lLpProductCode_ddl) %>;
			
			<%= AspxTools.JsGetElementById(lLpProductCode_ddl_value) %>.value = productCode_ddl.value;
		}
				
		function onLpNmUpdateChange()
		{
			if (document.getElementById("lLpTemplateNm_upd").checked)
			{
				document.getElementById("lLpTemplateNm_text").disabled = false ;
				document.getElementById("lLpTemplateNm_text").style.backgroundColor = '' ;
				
				document.getElementById("lLpTemplateNm_ov").getElementsByTagName("input")[0].disabled = false ;
				document.getElementById("lLpTemplateNm_ov").getElementsByTagName("input")[1].disabled = false ;
				document.getElementById("lLpTemplateNm_ov").getElementsByTagName("input")[2].disabled = false ;
				
				onLpNmOverrideChange() ;
			}
			else
			{
				document.getElementById("lLpTemplateNm_text").disabled = true ;
				document.getElementById("lLpTemplateNm_text").style.backgroundColor = 'silver' ;
				document.getElementById("lLpTemplateNm_search").disabled = false ;
				document.getElementById("lLpTemplateNm_search").style.backgroundColor = 'silver' ;
				
				document.getElementById("lLpTemplateNm_ov").getElementsByTagName("input")[0].disabled = true ;
				document.getElementById("lLpTemplateNm_ov").getElementsByTagName("input")[1].disabled = true ;
				document.getElementById("lLpTemplateNm_ov").getElementsByTagName("input")[2].disabled = true ;
			}
		}
		function onLpNmOverrideChange()
		{
			if (document.getElementById("lLpTemplateNm_ov").getElementsByTagName("input")[0].checked)
			{
				document.getElementById("lLpTemplateNm_text").disabled = true ;
				document.getElementById("lLpTemplateNm_text").style.backgroundColor = 'silver' ;
			}
			else
			{
				document.getElementById("lLpTemplateNm_text").disabled = false ;
				document.getElementById("lLpTemplateNm_text").style.backgroundColor = '' ;
			}
			if (document.getElementById("lLpTemplateNm_ov").getElementsByTagName("input")[1].checked)
			{
				document.getElementById("lLpTemplateNm_search").disabled = false ;
				document.getElementById("lLpTemplateNm_search").style.backgroundColor = '' ;
			}
			else
			{
				document.getElementById("lLpTemplateNm_search").disabled = true ;
				document.getElementById("lLpTemplateNm_search").style.backgroundColor = 'silver' ;
			}
		}
		
		
		function init()
		{
		    resize(1000, 900);
			if(isLqbPopup(window) || isJqueryUIDialog(window)){
				$('td.FormTableHeader').hide();	
			} 

	        if (document.getElementById('m_errorMessage') !== null &&
                document.getElementById('m_errorMessage').value.length > 0) {
	            alert(document.getElementById('m_errorMessage').value);
	        }

	        QualRateCalculationFlatValue = document.getElementById(QualRateFieldIds.QualRateCalculationFlatValue);
	        QualRateCalculationMaxOf = document.getElementById(QualRateFieldIds.QualRateCalculationMaxOf);
	        QualRateCalculationFieldT1 = document.getElementById('lQualRateCalculationFieldT1');
	        QualRateCalculationFieldT2 = document.getElementById('lQualRateCalculationFieldT2');
	        QualRateCalculationAdjustment1 = document.getElementById(QualRateFieldIds.QualRateCalculationAdjustment1);
	        QualRateCalculationAdjustment2 = document.getElementById(QualRateFieldIds.QualRateCalculationAdjustment2);

	        $('select.QualRateCalculationField, input.QualRateCalculationT').change(updateQualRateCalculationModel);

	        updateQualRateCalculationModel();
	        onUpdateQRateCalculationChange();
	        updateQualTermCalc_click();
	        lQualTermCalculationType_change();

			load_ProductCode_Data();
			setdefault_ProductCode();
			// BD-have to do this because .NET uses SPAN text to disable the checkbox if I use Server-Side tags
			onUpdateChange(document.getElementById("lLpeFeeMin_upd"), document.getElementById("lLpeFeeMin_ed"), document.getElementById("lLpeFeeMin_ov"));
			onUpdateChange(document.getElementById("lLpeFeeMax_upd"), document.getElementById("lLpeFeeMax_ed"), document.getElementById("lLpeFeeMax_ov"));
			onUpdateChange(document.getElementById("LienLinks_upd"), document.getElementById("LienLinks_ed"), document.getElementById("LienLinks_ov"));
			onUpdateChange(document.getElementById("PairId_upd"), document.getElementById("PairId_ov"), document.getElementById("PairId_ov"));
			onUpdateChange(document.getElementById("isEnabled_upd"), document.getElementById("isEnabled_cb"), document.getElementById("isEnabled_ov"));
			onUpdateChange(document.getElementById("lRateSheet_upd"), document.getElementById("lRateSheet_ed"), document.getElementById("lRateSheet_ov"));
			onUpdateChange(document.getElementById("IsOptionArm_upd"), document.getElementById("IsOptionArm"));
			onUpdateChange(document.getElementById("DisplayMargin_upd"), document.getElementById("DisplayMargin"));
			onUpdateChange(document.getElementById('UseQRate_upd'), document.getElementById('UseQRate'));
			onUpdateChange(document.getElementById('DisplayPayment_upd'), document.getElementById('DisplayPayment'));
			onUpdateChange(document.getElementById('QualifyPayment_upd'), document.getElementById('QualifyPayment'));
			onUpdateChange(document.getElementById('InvestorNameProductCode_upd'), document.getElementById('InvestorName_ddl'));
			onUpdateChange(document.getElementById('InvestorNameProductCode_upd'), document.getElementById('lLpProductCode_ddl'), document.getElementById('lLpProductCode_ov'));
			onUpdateChange(document.getElementById('CanBeStandAlone2nd_upd'), document.getElementById('CanBeStandAlone2nd'));
			onUpdateChange(document.getElementById('lLT_upd'), document.getElementById('lLT'));
			onUpdateChange(document.getElementById('lLpProductType_upd'), document.getElementById('lLpProductType_ddl'));  <%--//opm 22179 fs 09/02/08 --%>
			onUpdateChange(document.getElementById('lLpCustomCode1_upd'), document.getElementById('lLpCustomCode1_ed'), document.getElementById('lLpCustomCode1_ov'));
			onUpdateChange(document.getElementById('lLpCustomCode2_upd'), document.getElementById('lLpCustomCode2_ed'), document.getElementById('lLpCustomCode2_ov'));
			onUpdateChange(document.getElementById('lLpCustomCode3_upd'), document.getElementById('lLpCustomCode3_ed'), document.getElementById('lLpCustomCode3_ov'));
			onUpdateChange(document.getElementById('lLpCustomCode4_upd'), document.getElementById('lLpCustomCode4_ed'), document.getElementById('lLpCustomCode4_ov'));
			onUpdateChange(document.getElementById('lLpCustomCode5_upd'), document.getElementById('lLpCustomCode5_ed'), document.getElementById('lLpCustomCode5_ov'));
			onUpdateChange(document.getElementById('lLpInvestorCode1_upd'), document.getElementById('lLpInvestorCode1_ed'), document.getElementById('lLpInvestorCode1_ov'));
			onUpdateChange(document.getElementById('lLpInvestorCode2_upd'), document.getElementById('lLpInvestorCode2_ed'), document.getElementById('lLpInvestorCode2_ov'));
			onUpdateChange(document.getElementById('lLpInvestorCode3_upd'), document.getElementById('lLpInvestorCode3_ed'), document.getElementById('lLpInvestorCode3_ov'));
			onUpdateChange(document.getElementById('lLpInvestorCode4_upd'), document.getElementById('lLpInvestorCode4_ed'), document.getElementById('lLpInvestorCode4_ov'));
			onUpdateChange(document.getElementById('lLpInvestorCode5_upd'), document.getElementById('lLpInvestorCode5_ed'), document.getElementById('lLpInvestorCode5_ov'));
			onUpdateChange(document.getElementById('lLateDays_upd'), document.getElementById('lLateDays_ed'));
			onUpdateChange(document.getElementById('lLateChargePc_upd'), document.getElementById('lLateChargePc_ed'));
			onUpdateChange(document.getElementById('lLateChargeBaseDesc_upd'), document.getElementById('lLateChargeBaseDesc_ed'));
			onUpdateChange(document.getElementById('lPrepmtPenaltyT_upd'), document.getElementById('lPrepmtPenaltyT_ddl'));
			onUpdateChange(document.getElementById('lPrepmtRefundT_upd'), document.getElementById('lPrepmtRefundT_ddl'));
			onUpdateChange(document.getElementById('lAssumeLT_upd'), document.getElementById('lAssumeLT_ddl'));
            onUpdateChange(document.getElementById('lLpmiSupportedOutsidePmi_upd'), document.getElementById('lLpmiSupportedOutsidePmi'), document.getElementById('lLpmiSupportedOutsidePmi_ov'));
            onUpdateChange(document.getElementById('IsNonQmProgram_upd'), document.getElementById('IsNonQmProgram'));
            onUpdateChange(document.getElementById('IsDisplayInNonQmQuickPricer_upd'), document.getElementById('IsDisplayInNonQmQuickPricer'), document.getElementById('IsDisplayInNonQmQuickPricer_ov'));
			onLpNmUpdateChange();
			var edits = document.getElementById("Edits").getElementsByTagName("*");
			
			for( var i = 0 ; i < edits.length ; ++i )
			{
				if( edits[ i ].tagName == "INPUT" && edits[ i ].type == "text" || edits[ i ].tagName == "TEXTAREA" )
				{
					if( edits[ i ].disabled == true )
					{
						edits[ i ].style.backgroundColor = 'silver';
					}
				}
			}

			
		}

            </script>
            <input type="hidden" runat="server" id="QualRateCalculationFieldT1" />
            <input type="hidden" runat="server" id="QualRateCalculationFieldT2" />
		
			<TABLE class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="FormTableHeader">Batch Edit Loan Programs</TD>
				</TR>
				<tr>
					<TD class="FormTableHeader1">Programs</TD>
				</tr>
				<tr>
					<td><i>Master programs will not be edited via this page.</i></td>
				</tr>
				<TR>
					<td><asp:datagrid id="m_dgPrograms" runat="server" DataKeyField="lLpTemplateId" AutoGenerateColumns="False" CssClass="DataGrid" AllowSorting="True" Width="100%">
							<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
							<HeaderStyle CssClass="GridHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="lLpTemplateNm" HeaderText="Program Name" />
								<asp:BoundColumn DataField="IsEnabled" HeaderText="Enabled?" />
								<asp:BoundColumn DataField="lLpeFeeMin" HeaderText="Back End Max Ysp" />
								<asp:BoundColumn DataField="lLpeFeeMax" HeaderText="Min Ysp" />
								<asp:BoundColumn DataField="lRateDelta" HeaderText="Rate Delta" />
								<asp:BoundColumn DataField="lFeeDelta" HeaderText="Fee Delta" />
								<asp:BoundColumn DataField="lLienPosT" HeaderText="Lien Position" />
								<asp:BoundColumn DataField="CanBeStandAlone2nd" HeaderText="Stand-Alone 2nd?" />
								<asp:BoundColumn DataField="lRateSheetxmlContentOverrideBit" HeaderText="Rate Sheet Overriden?" />
								<asp:BoundColumn DataField="lIsArmMarginDisplayed" HeaderText="Display Margin" />
								<asp:BoundColumn DataField="IsOptionArm" HeaderText="Is Option ARM?" />
								<asp:BoundColumn DataField="lHasQRateInRateOptions" HeaderText="Use Q Rate?" />
                                <asp:TemplateColumn HeaderText="Q Rate Calculation Type">
                                    <ItemTemplate>
                                        <%#AspxTools.HtmlString(this.RenderQRateCalculationType(Container.DataItem))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Q Rate Calculation Field 1">
                                    <ItemTemplate>
                                        <%#AspxTools.HtmlString(this.RenderQRateCalculationFieldType(Container.DataItem, "lQualRateCalculationFieldT1"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Q Rate Calculation Adjustment 1">
                                    <ItemTemplate>
                                        <%#AspxTools.HtmlString(this.RenderQRateAdjustment(Container.DataItem, "lQualRateCalculationAdjustment1"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Q Rate Calculation Field 2">
                                    <ItemTemplate>
                                        <%#AspxTools.HtmlString(this.RenderQRateCalculationFieldType(Container.DataItem, "lQualRateCalculationFieldT2"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Q Rate Calculation Adjustment 2">
                                    <ItemTemplate>
                                        <%#AspxTools.HtmlString(this.RenderQRateAdjustment(Container.DataItem, "lQualRateCalculationAdjustment2"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Q Term Calculation Type">
                                    <ItemTemplate>
                                        <%#AspxTools.HtmlString(this.RenderQTermCalcType(Container.DataItem))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Q Term">
                                    <ItemTemplate>
                                        <%#AspxTools.HtmlString(this.RenderQTerm(Container.DataItem))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Display Payment">
									<ItemTemplate>
										<%# AspxTools.HtmlString(RenderDisplayPayment ( Container.DataItem ))  %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Qualify Payment">
									<ItemTemplate>
										<%# AspxTools.HtmlString(RenderQualifyPayment ( Container.DataItem ))  %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="lLpInvestorNm" HeaderText="Investor Name" />
								<asp:TemplateColumn HeaderText="Loan Type">
									<ItemTemplate>
										<%# AspxTools.HtmlString(RenderLoanType ( Container.DataItem ))  %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="lLpProductType" HeaderText="Loan Product Type" />
                                <asp:BoundColumn DataField="lLpmiSupportedOutsidePmi" HeaderText="LPMI Pricing Supported Outside of PMI Program Pairing"></asp:BoundColumn>
								<asp:BoundColumn DataField="lLpCustomCode1" HeaderText="Custom #1" />
								<asp:BoundColumn DataField="lLpCustomCode2" HeaderText="Custom #2" />
								<asp:BoundColumn DataField="lLpCustomCode3" HeaderText="Custom #3" />
								<asp:BoundColumn DataField="lLpCustomCode4" HeaderText="Custom #4" />
								<asp:BoundColumn DataField="lLpCustomCode5" HeaderText="Custom #5" />
								<asp:BoundColumn DataField="lLpInvestorCode1" HeaderText="Investor #1" />
								<asp:BoundColumn DataField="lLpInvestorCode2" HeaderText="Investor #2" />
								<asp:BoundColumn DataField="lLpInvestorCode3" HeaderText="Investor #3" />
								<asp:BoundColumn DataField="lLpInvestorCode4" HeaderText="Investor #4" />
								<asp:BoundColumn DataField="lLpInvestorCode5" HeaderText="Investor #5" />
                                <asp:BoundColumn DataField="IsNonQmProgram" HeaderText="Is this a non-QM program?" />
                                <asp:BoundColumn DataField="IsDisplayInNonQmQuickPricer" HeaderText="Display in Non-QM QP?" />
							</Columns>
						</asp:datagrid>
					</td>
				</TR>
				<tr>
					<TD class="FormTableHeader1">New Data
					</TD>
				</tr>
				<tr>
					<td>
						<table id="Edits" border="1">
							<tr>
								<td class="FieldLabel">Program Name
								</td>
								<td>
									<asp:checkbox id="lLpTemplateNm_upd" onclick="onLpNmUpdateChange();" runat="server" TextAlign="Right" Text="update"></asp:checkbox>
								</td>
								<td>
									<table border="0">
										<tr>
											<td class="FieldLabel">
												Search Text:
											</td>
											<td>
												<asp:TextBox id="lLpTemplateNm_search" runat="server" Columns="30"></asp:TextBox>
											</td>
											<td rowspan="2">
												<asp:radiobuttonlist id="lLpTemplateNm_ov" runat="server" RepeatLayout="Flow" Selected="True" inherit onclick="onLpNmOverrideChange();">
													<asp:ListItem Value="inherit" Selected="True">inherit</asp:ListItem>
													<asp:ListItem Value="replace">search &amp; replace</asp:ListItem>
													<asp:ListItem Value="override">override</asp:ListItem>
												</asp:radiobuttonlist>
											</td>
										</tr>
										<TR>
											<TD class="FieldLabel">New Text:
											</TD>
											<TD>
												<asp:TextBox id="lLpTemplateNm_text" runat="server" Columns="30"></asp:TextBox>
											</TD>
										</TR>
									</table>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel">Back End Max Ysp
								</td>
								<td><asp:checkbox id="lLpeFeeMin_upd" onclick="onUpdateChange(document.getElementById('lLpeFeeMin_upd'), document.getElementById('lLpeFeeMin_ed'), document.getElementById('lLpeFeeMin_ov'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</td>
								<td><ml:percenttextbox id="lLpeFeeMin_ed" runat="server" Width="70" Enabled="False" preset="percent"></ml:percenttextbox>&nbsp;or
									<asp:radiobuttonlist id="lLpeFeeMin_ov" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
										<asp:ListItem Value="add" Selected="True">add</asp:ListItem>
										<asp:ListItem Value="override">override</asp:ListItem>
									</asp:radiobuttonlist>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel" style="HEIGHT: 23px">Min Ysp
								</td>
								<td style="HEIGHT: 23px"><asp:checkbox id="lLpeFeeMax_upd" onclick="onUpdateChange(document.getElementById('lLpeFeeMax_upd'), document.getElementById('lLpeFeeMax_ed'), document.getElementById('lLpeFeeMax_ov'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</td>
								<td style="HEIGHT: 23px"><ml:percenttextbox id="lLpeFeeMax_ed" runat="server" Width="70" Enabled="False" preset="percent"></ml:percenttextbox>&nbsp;or
									<asp:checkbox id="lLpeFeeMax_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"></asp:checkbox>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel">Rate Delta
								</td>
								<td><asp:checkbox id="lRateDelta_upd" onclick="onUpdateChange(document.getElementById('lRateDelta_upd'), document.getElementById('lRateDelta_ed'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</td>
								<td><ml:percenttextbox id="lRateDelta_ed" runat="server" Width="70" Enabled="False" preset="percent"></ml:percenttextbox>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel">Fee Delta
								</td>
								<td><asp:checkbox id="lFeeDelta_upd" onclick="onUpdateChange(document.getElementById('lFeeDelta_upd'), document.getElementById('lFeeDelta_ed'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</td>
								<td><ml:percenttextbox id="lFeeDelta_ed" runat="server" Width="70" Enabled="False" preset="percent"></ml:percenttextbox>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel">Enable
								</td>
								<td><asp:checkbox id="isEnabled_upd" onclick="onUpdateChange(document.getElementById('isEnabled_upd'), document.getElementById('isEnabled_cb'), document.getElementById('isEnabled_ov'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</td>
								<td><asp:checkbox id="isEnabled_cb" runat="server" CssClass="FieldLabel" Text="Enabled?"></asp:checkbox>&nbsp;or
									<asp:checkbox id="isEnabled_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"></asp:checkbox>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel">1st Lien Links
								</td>
								<td><asp:checkbox id="LienLinks_upd" onclick="onUpdateChange(document.getElementById('LienLinks_upd'), document.getElementById('LienLinks_ed'), document.getElementById('LienLinks_ov'));" runat="server" Text="update"></asp:checkbox><asp:panel id="LienLinks_msg" style="FONT: 11px arial; COLOR: dimgray; TEXT-ALIGN: center" runat="server" EnableViewState="False" Visible="False">(mixed) </asp:panel>
								</td>
								<td><asp:textbox id="LienLinks_ed" runat="server" Width="400px" Enabled="False" Rows="5" TextMode="MultiLine"></asp:textbox><asp:checkbox id="LienLinks_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"></asp:checkbox>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel">Pair Id
								</td>
								<td><asp:checkbox id="PairId_upd" onclick="onUpdateChange(document.getElementById('PairId_upd'), document.getElementById('PairId_ov'), document.getElementById('PairId_ov'));" runat="server" Text="update"></asp:checkbox><asp:panel id="PairId_msg" style="FONT: 11px arial; COLOR: dimgray; TEXT-ALIGN: center" runat="server" EnableViewState="False" Visible="False">(mixed) </asp:panel>
								</td>
								<td><asp:checkbox id="PairId_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"></asp:checkbox>
								</td>
							</tr>
							<tr>
								<td class="FieldLabel">Rate Sheet
								</td>
								<td><asp:checkbox id="lRateSheet_upd" onclick="onUpdateChange(document.getElementById('lRateSheet_upd'), document.getElementById('lRateSheet_ed'), document.getElementById('lRateSheet_ov'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</td>
								<td>
									<asp:textbox id="lRateSheet_ed" runat="server" Width="400px" Enabled="False" TextMode="MultiLine" Rows="5"></asp:textbox><asp:checkbox id="lRateSheet_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"></asp:checkbox>
								</td>
							</tr>
							<TR>
								<TD class="FieldLabel">Investor Name/Product Code
								</TD>
								<TD>
									<asp:checkbox id="InvestorNameProductCode_upd" onclick="onUpdateChange(document.getElementById('InvestorNameProductCode_upd'), document.getElementById('InvestorName_ddl'));onUpdateChange(document.getElementById('InvestorNameProductCode_upd'), document.getElementById('lLpProductCode_ddl'), document.getElementById('lLpProductCode_ov'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</TD>
								<td>
									<table border="0">
										<tr>
											<td class="FieldLabel">Investor Name
											</td>
											<td><asp:DropDownList id="InvestorName_ddl" runat="server" onchange="onchange_Investor_ddl();"></asp:DropDownList>
											</td>
										</tr>
										<tr>
											<td class="FieldLabel">Product Code</td>
											<td>
                                                <select id="lLpProductCode_ddl" runat="server" onchange="onchange_ProductCode_ddl();"></select>
                                                <input id="lLpProductCode_ddl_value" type="hidden" runat="server">
                                                <asp:CheckBox ID="lLpProductCode_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="true" />
											</td>
										</tr>
									</table>
								</td>
							</TR>
							<TR>
								<TD class="FieldLabel">Display Margin
								</TD>
								<TD><asp:checkbox id="DisplayMargin_upd" onclick="onUpdateChange(document.getElementById('DisplayMargin_upd'), document.getElementById('DisplayMargin'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</TD>
								<TD><asp:CheckBox id="DisplayMargin" runat="server" Text="Display Margin" class="FieldLabel"></asp:CheckBox>
								</TD>
							</TR>

							<TR>
								<TD class="FieldLabel">Is Option ARM
								</TD>
								<TD><asp:checkbox id="IsOptionArm_upd" onclick="onUpdateChange(document.getElementById('IsOptionArm_upd'), document.getElementById('IsOptionArm'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</TD>
								<TD><asp:CheckBox id="IsOptionArm" runat="server" Text="Is Option ARM" class="FieldLabel"></asp:CheckBox>
								</TD>
							</TR>
							<tr>
								<TD class="FieldLabel">Use Q Rate
								</TD>
								<TD><asp:checkbox id="UseQRate_upd" onclick="onUpdateChange(document.getElementById('UseQRate_upd'), document.getElementById('UseQRate'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</TD>
								<TD><asp:CheckBox id="UseQRate" runat="server" Text="Use QRate" class="FieldLabel"></asp:CheckBox>
								</TD>
							</tr>
							<tr>
                                <td class="FieldLabel">Q Rate Calculation</td>
                                <td>
                                    <asp:CheckBox ID="QRateCalculation_upd" onclick="onUpdateQRateCalculationChange();" runat="server" Text="update" TextAlign="Right" />
                                </td>
                                <td>
                                    <input type="radio" id="QualRateCalculationFlatValue" name="QualRateCalculationT" runat="server" class="QualRateCalculationT" /> Flat Value
                                    &nbsp;
                                    <input type="radio" id="QualRateCalculationMaxOf" name="QualRateCalculationT" runat="server" class="QualRateCalculationT" /> Max Of
                                    <br />
                                    <select id="lQualRateCalculationFieldT1" class="QualRateCalculationField">
                                        <option value="<%=AspxTools.JsStringUnquoted(DataAccess.QualRateCalculationFieldT.LpeUploadValue)%>">LPE Upload Value</option>
                                        <option value="<%=AspxTools.JsStringUnquoted(DataAccess.QualRateCalculationFieldT.NoteRate)%>">Note Rate</option>
                                        <option value="<%=AspxTools.JsStringUnquoted(DataAccess.QualRateCalculationFieldT.FullyIndexedRate)%>">Fully Indexed Rate</option>
                                        <option value="<%=AspxTools.JsStringUnquoted(DataAccess.QualRateCalculationFieldT.Index)%>">Index</option>
                                        <option value="<%=AspxTools.JsStringUnquoted(DataAccess.QualRateCalculationFieldT.Margin)%>">Margin</option>
                                    </select>
                                    +
                                    <ml:PercentTextBox ID="lQualRateCalculationAdjustment1" runat="server"></ml:PercentTextBox>
                                    <br />
                                    <span id="SecondCalculationRow">
                                        <select id="lQualRateCalculationFieldT2" class="QualRateCalculationField">
                                            <option value="<%=AspxTools.JsStringUnquoted(DataAccess.QualRateCalculationFieldT.NoteRate)%>">Note Rate</option>
                                            <option value="<%=AspxTools.JsStringUnquoted(DataAccess.QualRateCalculationFieldT.FullyIndexedRate)%>">Fully Indexed Rate</option>
                                            <option value="<%=AspxTools.JsStringUnquoted(DataAccess.QualRateCalculationFieldT.Index)%>">Index</option>
                                            <option value="<%=AspxTools.JsStringUnquoted(DataAccess.QualRateCalculationFieldT.Margin)%>">Margin</option>
                                        </select>
                                        +
                                        <ml:PercentTextBox ID="lQualRateCalculationAdjustment2" runat="server"></ml:PercentTextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="FieldLabel">Q Term Calculation</td>
                                <td>
                                    <asp:CheckBox ID="updateQualTermCalc" onclick="updateQualTermCalc_click();" runat="server" Text="update" TextAlign="Right" />
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList id="lQualTermCalculationType" onchange="lQualTermCalculationType_change();" runat="server" CssClass="lQualTermCalculationType" />
                                    &nbsp
                                    <input type="text" id="lQualTerm" runat="server" preset="numeric" class="lQualTerm" />
                                </td>
                            </tr>

							<tr>
								<TD class="FieldLabel">Display Payment
								</TD>
								<TD><asp:checkbox id="DisplayPayment_upd" onclick="onUpdateChange(document.getElementById('DisplayPayment_upd'), document.getElementById('DisplayPayment'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</TD>
								<TD><asp:DropDownList id="DisplayPayment" runat="server">
										<asp:ListItem Value="0" Selected="True">DRate + user selection of I/O</asp:ListItem>
										<asp:ListItem Value="1">DRate + P&I</asp:ListItem>
										<asp:ListItem Value="2">DRate + I/O</asp:ListItem>
									</asp:DropDownList>
								</TD>
							</tr>
							<tr>
								<TD class="FieldLabel">Qualify Payment
								</TD>
								<TD><asp:checkbox id="QualifyPayment_upd" onclick="onUpdateChange(document.getElementById('QualifyPayment_upd'), document.getElementById('QualifyPayment'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</TD>
								<TD><asp:DropDownList id="QualifyPayment" runat="server">
										<asp:ListItem Value="0" Selected="True">QRate + user selection of I/O</asp:ListItem>
										<asp:ListItem Value="1">QRate + P&I</asp:ListItem>
										<asp:ListItem Value="2">QRate + I/O</asp:ListItem>
									</asp:DropDownList>
								</TD>
							</tr>
							<tr>
								<TD class="FieldLabel">Can Be Stand Alone 2nd
								</TD>
								<TD><asp:checkbox id="CanBeStandAlone2nd_upd" onclick="onUpdateChange(document.getElementById('CanBeStandAlone2nd_upd'), document.getElementById('CanBeStandAlone2nd'));" runat="server" Text="update" TextAlign="Right"></asp:checkbox>
								</TD>
								<TD><asp:CheckBox id="CanBeStandAlone2nd" runat="server" Text="Can Be Stand-Alone Second" class="FieldLabel"></asp:CheckBox>
								</TD>
							</tr>
							<tr>
								<TD class="FieldLabel">Loan Type
								</TD>
								<TD><asp:checkbox id="lLT_upd" onclick="onUpdateChange(document.getElementById('lLT_upd'), document.getElementById('lLT'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD>
									<asp:dropdownlist id="lLT" runat="server" Width="154px">
										<asp:ListItem Value="0">Conventional</asp:ListItem>
										<asp:ListItem Value="2">VA</asp:ListItem>
										<asp:ListItem Value="1">FHA</asp:ListItem>
										<asp:ListItem Value="3">USDA/Rural Housing</asp:ListItem>
										<asp:ListItem Value="4">Other</asp:ListItem>
									</asp:dropdownlist>
								</TD>
							</tr>
							<tr>
								<TD class="FieldLabel">Loan Product Type</TD>
								<TD><asp:checkbox id="lLpProductType_upd" onclick="onUpdateChange(document.getElementById('lLpProductType_upd'), document.getElementById('lLpProductType_ddl'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:DropDownList id="lLpProductType_ddl" Width="165px" runat="server"></asp:DropDownList>
								</TD>
							</tr>
							<tr>
								<TD class="FieldLabel">Custom Code 1</TD>
								<TD><asp:checkbox id="lLpCustomCode1_upd" onclick="onUpdateChange(document.getElementById('lLpCustomCode1_upd'), document.getElementById('lLpCustomCode1_ed'), document.getElementById('lLpCustomCode1_ov'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:TextBox id="lLpCustomCode1_ed" Width="200px" runat="server" />
								<asp:CheckBox id="lLpCustomCode1_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
								</TD>
							</tr>
							
							<tr>
								<TD class="FieldLabel">Custom Code 2</TD>
								<TD><asp:checkbox id="lLpCustomCode2_upd" onclick="onUpdateChange(document.getElementById('lLpCustomCode2_upd'), document.getElementById('lLpCustomCode2_ed'), document.getElementById('lLpCustomCode2_ov'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:TextBox id="lLpCustomCode2_ed" Width="200px" runat="server" />
								<asp:CheckBox id="lLpCustomCode2_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
								</TD>
							</tr>

							<tr>
								<TD class="FieldLabel">Custom Code 3</TD>
								<TD><asp:checkbox id="lLpCustomCode3_upd" onclick="onUpdateChange(document.getElementById('lLpCustomCode3_upd'), document.getElementById('lLpCustomCode3_ed'), document.getElementById('lLpCustomCode3_ov'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:TextBox id="lLpCustomCode3_ed" Width="200px" runat="server" />
								<asp:CheckBox id="lLpCustomCode3_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
								</TD>
							</tr>

							<tr>
								<TD class="FieldLabel">Custom Code 4</TD>
								<TD><asp:checkbox id="lLpCustomCode4_upd" onclick="onUpdateChange(document.getElementById('lLpCustomCode4_upd'), document.getElementById('lLpCustomCode4_ed'), document.getElementById('lLpCustomCode4_ov'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:TextBox id="lLpCustomCode4_ed" Width="200px" runat="server" />
								<asp:CheckBox id="lLpCustomCode4_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
								</TD>
							</tr>

							<tr>
								<TD class="FieldLabel">Custom Code 5</TD>
								<TD><asp:checkbox id="lLpCustomCode5_upd" onclick="onUpdateChange(document.getElementById('lLpCustomCode5_upd'), document.getElementById('lLpCustomCode5_ed'), document.getElementById('lLpCustomCode5_ov'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:TextBox id="lLpCustomCode5_ed" Width="200px" runat="server" />
								<asp:CheckBox id="lLpCustomCode5_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
								</TD>
							</tr>

							<tr>
								<TD class="FieldLabel">Investor Code 1</TD>
								<TD><asp:checkbox id="lLpInvestorCode1_upd" onclick="onUpdateChange(document.getElementById('lLpInvestorCode1_upd'), document.getElementById('lLpInvestorCode1_ed'), document.getElementById('lLpInvestorCode1_ov'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:TextBox id="lLpInvestorCode1_ed" Width="200px" runat="server" />
								<asp:CheckBox id="lLpInvestorCode1_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
								</TD>
							</tr>
							<tr>
								<TD class="FieldLabel">Investor Code 2</TD>
								<TD><asp:checkbox id="lLpInvestorCode2_upd" onclick="onUpdateChange(document.getElementById('lLpInvestorCode2_upd'), document.getElementById('lLpInvestorCode2_ed'), document.getElementById('lLpInvestorCode2_ov'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:TextBox id="lLpInvestorCode2_ed" Width="200px" runat="server" />
								<asp:CheckBox id="lLpInvestorCode2_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
								</TD>
							</tr>
							<tr>
								<TD class="FieldLabel">Investor Code 3</TD>
								<TD><asp:checkbox id="lLpInvestorCode3_upd" onclick="onUpdateChange(document.getElementById('lLpInvestorCode3_upd'), document.getElementById('lLpInvestorCode3_ed'), document.getElementById('lLpInvestorCode3_ov'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:TextBox id="lLpInvestorCode3_ed" Width="200px" runat="server" />
								<asp:CheckBox id="lLpInvestorCode3_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
								</TD>
							</tr>
							<tr>
								<TD class="FieldLabel">Investor Code 4</TD>
								<TD><asp:checkbox id="lLpInvestorCode4_upd" onclick="onUpdateChange(document.getElementById('lLpInvestorCode4_upd'), document.getElementById('lLpInvestorCode4_ed'), document.getElementById('lLpInvestorCode4_ov'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:TextBox id="lLpInvestorCode4_ed" Width="200px" runat="server" />
								<asp:CheckBox id="lLpInvestorCode4_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
								</TD>
							</tr>
							<tr>
								<TD class="FieldLabel">Investor Code 5</TD>
								<TD><asp:checkbox id="lLpInvestorCode5_upd" onclick="onUpdateChange(document.getElementById('lLpInvestorCode5_upd'), document.getElementById('lLpInvestorCode5_ed'), document.getElementById('lLpInvestorCode5_ov'));" runat="server" Text="update" TextAlign="Right" />
								</TD>
								<TD><asp:TextBox id="lLpInvestorCode5_ed" Width="200px" runat="server" />
								<asp:CheckBox id="lLpInvestorCode5_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
								</TD>
							</tr>

							<tr>
							    <td class="FieldLabel">Late Days (lLateDays)</td>
							    <td><asp:CheckBox ID="lLateDays_upd" onclick="onUpdateChange(document.getElementById('lLateDays_upd'), document.getElementById('lLateDays_ed'));" runat="server" Text="update" TextAlign="Right" /></td>
							    <td><asp:TextBox ID="lLateDays_ed" runat="server" /></td>
							</tr>											
							<tr>
							    <td class="FieldLabel">Late Charge % (lLateChargePc)</td>
							    <td><asp:CheckBox ID="lLateChargePc_upd" onclick="onUpdateChange(document.getElementById('lLateChargePc_upd'), document.getElementById('lLateChargePc_ed'));" runat="server" Text="update" TextAlign="Right" /></td>
							    <td><asp:TextBox ID="lLateChargePc_ed" runat="server" /></td>
							</tr>									
							<tr>
							    <td class="FieldLabel">Late Charge Desc (lLateChargeBaseDesc)</td>
							    <td><asp:CheckBox ID="lLateChargeBaseDesc_upd" onclick="onUpdateChange(document.getElementById('lLateChargeBaseDesc_upd'), document.getElementById('lLateChargeBaseDesc_ed'));" runat="server" Text="update" TextAlign="Right" /></td>
							    <td><asp:TextBox ID="lLateChargeBaseDesc_ed" runat="server" /></td>
							</tr>			
							<tr>
							    <td class="FieldLabel">Pay Prepay Penalty (lPrepmtPenaltyT)</td>
							    <td><asp:CheckBox ID="lPrepmtPenaltyT_upd" onclick="onUpdateChange(document.getElementById('lPrepmtPenaltyT_upd'), document.getElementById('lPrepmtPenaltyT_ddl'));" runat="server" Text="update" TextAlign="Right" /></td>
							    <td><asp:DropDownList ID="lPrepmtPenaltyT_ddl" runat="server" /></td>
							</tr>																																		
                            <tr>
							    <td class="FieldLabel">Pay Prepay Refund (lPrepmtRefundT)</td>
							    <td><asp:CheckBox ID="lPrepmtRefundT_upd" onclick="onUpdateChange(document.getElementById('lPrepmtRefundT_upd'), document.getElementById('lPrepmtRefundT_ddl'));" runat="server" Text="update" TextAlign="Right" /></td>
							    <td><asp:DropDownList ID="lPrepmtRefundT_ddl" runat="server" /></td>
							</tr>		
                            <tr>
							    <td class="FieldLabel">Assumption (lAssumeLT)</td>
							    <td><asp:CheckBox ID="lAssumeLT_upd" onclick="onUpdateChange(document.getElementById('lAssumeLT_upd'), document.getElementById('lAssumeLT_ddl'));" runat="server" Text="update" TextAlign="Right" /></td>
							    <td><asp:DropDownList ID="lAssumeLT_ddl" runat="server" /></td>
							</tr>																		
							<tr>
                                <td class="FieldLabel">LPMI Pricing Supported Outside of PMI Program Pairing?</td>
                                <td>
                                    <asp:CheckBox ID="lLpmiSupportedOutsidePmi_upd" onclick="onUpdateChange(document.getElementById('lLpmiSupportedOutsidePmi_upd'), document.getElementById('lLpmiSupportedOutsidePmi'), document.getElementById('lLpmiSupportedOutsidePmi_ov'));" runat="server" Text="update" TextAlign="Right"></asp:CheckBox>
                                </td>
                                <td>
                                    <asp:CheckBox ID="lLpmiSupportedOutsidePmi" runat="server" Text="LPMI Pricing Supported Outside of PMI Program Pairing" class="FieldLabel"></asp:CheckBox>
                                    <asp:CheckBox id="lLpmiSupportedOutsidePmi_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Is this a non-QM program?</td>
                                <td>
                                    <asp:CheckBox ID="IsNonQmProgram_upd" onclick="onUpdateChange(document.getElementById('IsNonQmProgram_upd'), document.getElementById('IsNonQmProgram'));" runat="server" Text="update" TextAlign="Right"></asp:CheckBox>
                                </td>
                                <td>
                                    <asp:CheckBox ID="IsNonQmProgram" runat="server" Text="Is this a non-QM program?" class="FieldLabel"></asp:CheckBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">Display in Non-QM QP?</td>
                                <td>
                                    <asp:CheckBox ID="IsDisplayInNonQmQuickPricer_upd" onclick="onUpdateChange(document.getElementById('IsDisplayInNonQmQuickPricer_upd'), document.getElementById('IsDisplayInNonQmQuickPricer'), document.getElementById('IsDisplayInNonQmQuickPricer_ov'));" runat="server" Text="update" TextAlign="Right"></asp:CheckBox>
                                </td>
                                <td>
                                    <asp:CheckBox ID="IsDisplayInNonQmQuickPricer" runat="server" Text="Display in Non-QM QP?" class="FieldLabel"></asp:CheckBox>
                                    <asp:CheckBox id="IsDisplayInNonQmQuickPricer_ov" runat="server" CssClass="FieldLabel" Text="Override?" Checked="True"/>
                                    (NOTE: This will always return false if the program is not a non-QM program)
                                </td>
                            </tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
                        <ml:nodoubleclickbutton id="btnOK" runat="server" Text="  OK  " onclick="btnOK_Click" OnClientClick="populateCalculationFields();"></ml:nodoubleclickbutton>&nbsp;
						<ml:nodoubleclickbutton id="btnCancel" runat="server" Text="Cancel" onclick="btnCancel_Click"></ml:nodoubleclickbutton>
					</td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
