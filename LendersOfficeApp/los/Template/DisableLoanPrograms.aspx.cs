using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using DataAccess;
using System.Data.SqlClient;
using LendersOffice.Constants;
using LendersOffice.Common;

namespace LendersOfficeApp.los.Template
{
	/// <summary>
	/// Summary description for DisableLoanPrograms.
	/// </summary>
	public partial class DisableLoanPrograms : BasePage
	{

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
            RegisterJsScript("LQBPopup.js");
			if( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == false )
			{
				throw new CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, LendersOffice.Common.ErrorMessages.GenericAccessDenied);
			}
			if(m_Command.Value.Equals("delete"))
				DeleteDisabledLoanProgramInfo();
			BindDataGrid();
		}

		private void DeleteDisabledLoanProgramInfo()
		{
			m_Command.Value = "";
			if(m_pcode.Value == null || m_pcode.Value.TrimWhitespaceAndBOM() == "" || m_investorName.Value == null || m_investorName.Value.TrimWhitespaceAndBOM() == "")
				return;
			try
			{
				SqlParameter[] parameters = { new SqlParameter("@ProductCode", m_pcode.Value), new SqlParameter("@InvestorName", m_investorName.Value) };
				int result = StoredProcedureHelper.ExecuteNonQuery
					(DataSrc.LpeSrc
					, "DeleteDisabledLoanProgramInfo"
					, 1
					, parameters);
			}
			catch(Exception e)
			{
				Tools.LogError(e);
			}
		}

		private void BindDataGrid() 
		{
			using(DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListDisabledBySae" ) )
			{
				m_Grid.DataSource = reader ;
				m_Grid.DataBind() ;
			}
		}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            EnableJqueryMigrate = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
