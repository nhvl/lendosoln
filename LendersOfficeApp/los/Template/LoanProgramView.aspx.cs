using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.Template
{
	/// <summary>
	/// Summary description for LoanProgramView.
	/// </summary>
	public partial class LoanProgramView : BasePage
	{

        private Guid m_fileId 
        {
            get { return new Guid( Request[ "FileId" ] ); }
        }
        private void LoadData()
        {
            CLoanProductData data = new DataAccess.CLoanProductData( m_fileId );
            data.InitLoad();

            lLpTemplateNmLabel.Text = data.lLpTemplateNm ;
            lLenderNmLabel.Text = data.lLendNm ;
			lLpInvestorNmLabel.Text = data.lLpInvestorNm ;
            switch (data.lLT) 
            {
                case E_sLT.Conventional:
                    lLTLabel.Text = "Conventional"; break;
                case E_sLT.FHA:
                    lLTLabel.Text = "FHA"; break;
                case E_sLT.UsdaRural:
                    lLTLabel.Text = "USDA/Rural Housing"; break;
                case E_sLT.Other:
                    lLTLabel.Text = "Other"; break;
                case E_sLT.VA:
                    lLTLabel.Text = "VA"; break;
            }

            lNoteRLabel.Text = data.lNoteR_rep ;
            lQualRLabel.Text = data.lQualR_rep ;
            lTermLabel.Text = data.lTerm_rep + " / " + data.lDue_rep;
			switch( data.lLienPosT )
			{
				case E_sLienPosT.First:
					lLienPosTLabel.Text = "First";
					break;
				case E_sLienPosT.Second:
					lLienPosTLabel.Text = "Second";
					break;
			}
            lLOrigFPcLabel.Text = data.lLOrigFPc_rep ;
            lLockedDaysLabel.Text = data.lLockedDays_rep ;
            lRAdj1stCapRLabel.Text = data.lRadj1stCapR_rep ;
            lRAdj1stCapMonLabel.Text = data.lRadj1stCapMon_rep ;
            lRAdjCapMonLabel.Text = data.lRAdjCapR_rep ;
            lRAdjCapMonLabel.Text = data.lRAdjCapMon_rep ;
            lRAdjLifeCapRLabel.Text = data.lRAdjLifeCapR_rep ;
            lRAdjMarginRLabel.Text = data.lRAdjMarginR_rep ;
            lRAdjIndexRLabel.Text = data.lRAdjIndexR_rep ;
            lRAdjFloorRLabel.Text = data.lRAdjFloorR_rep ;
            switch (data.lRAdjRoundT) 
            {
                case E_sRAdjRoundT.Down:
                    lRAdjRound.Text = "Down"; break;
                case E_sRAdjRoundT.Normal:
                    lRAdjRound.Text = "Normal"; break;
                case E_sRAdjRoundT.Up:
                    lRAdjRound.Text = "Up"; break;
            }
			if( data.lArmIndexGuid != Guid.Empty )
			{
				lArmInddexNameVstrLabel.Text = data.lArmIndexNameVstr ;
				lArmIndexBasedOnVstrLabel.Text = data.lArmIndexBasedOnVstr ;
				lArmIndexCanBeFoundVstrLabel.Text = data.lArmIndexCanBeFoundVstr ;
				lArmIndexAffectInitIRBitCheck.Checked = data.lArmIndexAffectInitIRBit ;
				lArmIndexNotifyAtLeastDaysVstrLabel.Text = data.lArmIndexNotifyAtLeastDaysVstr;
				lArmIndexNotifyNotBeforeDaysVstrLabel.Text = data.lArmIndexNotifyNotBeforeDaysVstr;
			}
			else
			{
				lArmIndexPanel.Visible = false;
			}
			lPmtAdjCapRLabel.Text = data.lPmtAdjCapR_rep ;
            lPmtAdjCapMonLabel.Text = data.lPmtAdjCapMon_rep ;
            lPmtAdjRecastPeriodMonLabel.Text = data.lPmtAdjRecastPeriodMon_rep ;
            lPmtAdjRecastStopLabel.Text = data.lPmtAdjRecastStop_rep ;
            lPmtAdjMaxBalLabel.Text = data.lPmtAdjMaxBalPc_rep ;
            lBuydwnMon1Label.Text = data.lBuydwnMon1_rep ;
            lBuydwnR1Label.Text = data.lBuydwnR1_rep ;
            lBuydwnMon2Label.Text = data.lBuydwnMon2_rep ;
            lBuydwnR2Label.Text = data.lBuydwnR2_rep ;
            lBuydwnMon3Label.Text = data.lBuydwnMon3_rep ;
            lBuydwnR3Label.Text = data.lBuydwnR3_rep ;
            lBuydwnMon4Label.Text = data.lBuydwnMon4_rep ;
            lBuydwnR4Label.Text = data.lBuydwnR4_rep ;
            lBuydwnMon5Label.Text = data.lBuydwnMon5_rep ;
            lBuydwnR5Label.Text = data.lBuydwnR5_rep ;
            lGradPmtYrsLabel.Text = data.lGradPmtYrs_rep ;
            lGradPmtRLabel.Text = data.lGradPmtR_rep ;
            lIOnlyMonLabel.Text = data.lIOnlyMon_rep ;
            lHasVarRFeature_chk.Checked = data.lHasVarRFeature ;
            lVarRNotesLabel.Text = data.lVarRNotes ;
            lAprIncludesReqDeposit_chk.Checked = data.lAprIncludesReqDeposit ;
            lHasDemandFeature_chk.Checked = data.lHasDemandFeature ;
            lFilingFLabel.Text = data.lFilingF ;
            lLateDaysLabel.Text = data.lLateDays ;
            lLateChargePcLabel.Text = data.lLateChargePc ;
            lLateChargeBaseLabel.Text = data.lLateChargeBaseDesc ;
            lFinMethDescLabel.Text = data.lFinMethDesc;

            switch (data.lPrepmtPenaltyT) 
            {
                case E_sPrepmtPenaltyT.May:
                    lPrepmtPenaltyTLabel.Text = "May"; break;
                case E_sPrepmtPenaltyT.WillNot:
                    lPrepmtPenaltyTLabel.Text = "Will not"; break;
            }

			if( data.lPrepmtPenaltyT == E_sPrepmtPenaltyT.May )
			{
				lPrepmtPenaltyMonLabel.Text = data.lPpmtPenaltyMon_rep;

				lPrepmtPenaltyTPanel.Visible = true;
			}
			else
			{
				lPrepmtPenaltyTPanel.Visible = false;
			}

			switch (data.lPrepmtRefundT) 
            {
                case E_sPrepmtRefundT.May:
                    lPrepmtRefundTLabel.Text = "May"; break;
                case E_sPrepmtRefundT.WillNot:
                    lPrepmtRefundTLabel.Text = "Will not"; break;
            }

			switch (data.lAssumeLT) 
            {
                case E_sAssumeLT.May:
                    lAssumeLTLabel.Text = "May"; break;
                case E_sAssumeLT.MayNot:
                    lAssumeLTLabel.Text = "Will not"; break;
            }

			switch (data.lFinMethT) 
            {
                case E_sFinMethT.Fixed:
                    lFinMethTLabel.Text = "Fixed"; break;
                case E_sFinMethT.ARM:
                    lFinMethTLabel.Text = "ARM"; break;
                case E_sFinMethT.Graduated:
                    lFinMethTLabel.Text = "Graduated"; break;
            }
        }
    
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            LoadData();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
