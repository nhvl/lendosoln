using System;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.LoanPrograms;
using LendersOffice.RatePrice;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Template
{
    /// <summary>
    /// Summary description for LoanProductExport.
    /// </summary>

    public partial class LoanProductExport : BasePage
	{
		private BrokerUserPrincipal CurrentUser
		{
			// Access current user.
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		/// <summary>
		/// Initialize and export specified contents.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

            if (Request.QueryString["cmd"] == "xlsx")
            {
                ExportExcel();
                return;
            }
			// Load the specified folder and stream out it and its
			// children with rate options using our xml doc model.

			try
			{
				// Get the set of objects to export.  If the user
				// passes in all, then we need to lookup the current
				// tree for the user's broker and export that, though
				// the tree may have changed from when they viewed it
				// and clicked export all.

				RateOptionExport exPorts = new RateOptionExport();

				if( RequestHelper.GetSafeQueryString( "folderId" ) != null && RequestHelper.GetSafeQueryString( "folderId" ).ToLower() != "all" )
				{
					Guid folderId = new Guid( RequestHelper.GetSafeQueryString( "folderId" ) );

					exPorts.Load
						( CurrentUser.BrokerId
						, CurrentUser.HasPermission( Permission.CanModifyLoanPrograms )
						, folderId
						);
				}
				else
				{
					exPorts.Load
						( CurrentUser.BrokerId
						, CurrentUser.HasPermission( Permission.CanModifyLoanPrograms )
						);
				}

				// Send out the loan product folder as an csv doc.

				Response.AddHeader( "Content-Disposition" , "attachment; filename=\"Folder.csv\"" );
				Response.ContentType = "text/csv";

				// Stream out the batch of product folders so that
				// the user can save it as an xml document.

				Response.Output.Write( exPorts.Export() );

				// Close up shop and return.  We bypass event handling
				// and rendering because our export is the output.  If
				// we throw out of here and not reach this point, then
				// we need to show the failure message as text and let
				// the user click back to the tree.

                Response.Flush();
				Response.End();
			}
			catch( System.Threading.ThreadAbortException e )
			{
				// Spread the love and reach out to touch someone.

				throw e;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( e );
			}
		}

        private void ExportExcel()
        {
            string type = RequestHelper.GetSafeQueryString("type");
            Guid folderId = RequestHelper.GetGuid("folderId", Guid.Empty);

            BrokerDB brokerDB = BrokerDB.RetrieveById(CurrentUser.BrokerId);
            string customerCode = brokerDB.CustomerCode;
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"LOAN_PROGRAM_TEMPLATE_{customerCode}.xlsx\"");
            Response.ContentType = "application/vnd.ms-excel";

            byte[] content = LoanProgramTemplateExport.ExportToExcel(CurrentUser.BrokerId, folderId, type);

            Response.BinaryWrite(content);
            Response.Flush();
            Response.End();
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}

}
