using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using System.Collections.Generic;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Template
{
	/// <summary>
	/// Summary description for LoanProductFrame.
	/// </summary>
	public partial class LoanProductFrame : BasePage
	{
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    //Permission.AllowViewingAndEditingManualLoanPrograms
                };
            }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            var principal = PrincipalFactory.CurrentPrincipal;
            if(false == principal.HasPermission(Permission.CanModifyLoanPrograms)
                && false == principal.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms))
            {
                throw new AccessDenied("You do not have permission to view this page.", 
                    "User does not have CanModifyLoanPrograms or AllowViewingAndEditingManualLoanPrograms");
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //            

            InitializeComponent();
			base.OnInit(e);
		}

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE10;
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}