using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOffice.RatePrice;

namespace LendersOfficeApp.los.Template
{
    /// <summary>
    /// 04/23/07 mf OPM 11856.
    /// This ia a serialized version of InsertExternalProduct
    ///
    /// 08/06/07 mf. OPM 17199. We need to allow SAEs to set programs as enabled
    /// upon derival as well.
    /// </summary>
    public partial class CreateDerivationJob : LendersOffice.Common.BasePage
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private string ErrorMessage
        {
            set { ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + "."); }
        }

        private void RegisterResource()
        {
            EnableJquery = false;
            EnableJqueryMigrate = false;
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");
        }

        /// <summary>
        /// Initialize this page.
        /// </summary>

        protected void PageLoad(object sender, System.EventArgs a)
        {
            RegisterResource();

            // Check security access control bits and deny
            // if not allowed.

            if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false)
            {
                ErrorMessage = "Access denied.";

                return;
            }


            DateTime now = DateTime.Now;
            bool isWeekday = now.DayOfWeek != DayOfWeek.Saturday && now.DayOfWeek != DayOfWeek.Sunday;
            if (isWeekday && (now.TimeOfDay > new TimeSpan(5, 0, 0) && now.TimeOfDay < new TimeSpan(17, 0, 0)))
            {
                timeWarning.Text = "It is now " + now.ToShortTimeString() + " on a weekday. " +
                "Derivation is an expensive operation that can slow down the system during times of heavy traffic. " +
                "Please consider if this can wait until after 5pm when it would be less likely to impact clients.";

                timeWarning.Visible = true;
            }
            else
            {
                timeWarning.Visible = false;
            }


            // Get the broker list on the first load.  We
            // assume the list saves state between posts.

            if (IsPostBack == false)
            {
                // Load up the brokers and store by their id.
                var list = LoadBrokerList();

                m_BrokerList.DataValueField = "Key";
                m_BrokerList.DataTextField = "Value";
                m_BrokerList.DataSource = list.OrderBy(o => o.Value);
                m_BrokerList.DataBind();
                // for the default loan program set, a request was made to be chaned to the "MASTER PML" set
                m_BrokerList.SelectedIndex = m_BrokerList.Items.IndexOf(m_BrokerList.Items.FindByText("MASTER PML"));

                var generator = new CPmlFIdGenerator();
                m_jobLabelTb.Text = generator.GenerateNewFriendlyId();

                var selectedBrokerId = new Guid(m_BrokerList.SelectedItem.Value);
                this.RegisterJsObjectWithJsonNetAnonymousSerializer("state", LoadData(selectedBrokerId));
            }
        }

        [WebMethod]
        public static object LoadData(Guid selectedBrokerId)
        {
            var BrokerUser = PrincipalFactory.CurrentPrincipal;
            if (!BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
            {
                return new { errorMessage = "Access denied." };
            }

            var set = new LoanProductSet();
            set.Retrieve(selectedBrokerId, true);

            return new
            {
                selectedBrokerId,
                tree = new
                {
                    set.Id,
                    Name = "Root Folder",
                    children = InsertExternalProduct.LoadTreeChildren(set),
                },
            };
        }

        [WebMethod]
        public static object DoInsert(
            Guid[] selectedIds, Guid targetId, Guid selectedBrokerId, bool enableAfterDerive, bool independentAfterCreate,
            string jobPriority,  string jobNotifyEmailAddr, string jobUserNotes, string jobUserLabel
        )
        {
            var BrokerUser = PrincipalFactory.CurrentPrincipal;
            if (!BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
            {
                return new { errorMessage = "Access denied." };
            }

            if (selectedIds.Length < 1)
            {
                return new {errorMessage = "Nothing Checked.  No job created"};
            }

            try
            {
                // Add this task to queue.  The queue processor will see that it gets done.
                // We want to save the list of selected folders and programs here, so when
                // we finally derive (may be in future), it is based on current folder/program
                // structure, (Not how it looks now).

                var job = new DerivationJobBuilder(new ArrayList(selectedIds), targetId, BrokerUser.BrokerId, selectedBrokerId, enableAfterDerive, independentAfterCreate)
                {
                    JobPriority        = jobPriority,
                    JobNotifyEmailAddr = jobNotifyEmailAddr,
                    JobUserNotes       = jobUserNotes,
                    JobUserLabel       = jobUserLabel,
                    UserLoginNm        = BrokerUser.LoginNm,
                };

                job.SendToQueue();
                return new {errorMessage = ""};
            }
            catch (Exception e)
            {
                // Oops!
                Tools.LogError(e);
                return new { errorMessage = "Failed to insert job into program queue." };
            }
        }

        private static List<KeyValuePair<Guid, string>> LoadBrokerList()
        {
            var list = new List<KeyValuePair<Guid, string>>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@HasLenderDefaultFeatures", true),
                    new SqlParameter("@BrokerStatus", 1)
                };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListBrokers", parameters))
                {
                    while (reader.Read())
                    {
                        list.Add(new KeyValuePair<Guid, string>((Guid) reader["BrokerId"], (string) reader["BrokerNm"]));
                    }
                }
            }

            return list;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

            notifEmailTbValidator.ValidationExpression = ConstApp.EmailValidationExpression;

        }
        #endregion
    }
}
