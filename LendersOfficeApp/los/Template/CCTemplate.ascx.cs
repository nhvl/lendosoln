using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOfficeApp.los.common;
using LendersOfficeApp.los.LegalForm;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.Template
{


	/// <summary>
	///		Summary description for CCTemplate.
	/// </summary>
	public partial  class CCTemplate : System.Web.UI.UserControl
	{

        #region Protected member variables

		protected System.Web.UI.WebControls.Button Button1;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cLOrigFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cLDiscntProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cApprFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cCrFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cInspectFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cMBrokFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cTxServFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cProcFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cUwFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cWireFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn c800U1FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn c800U2FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn c800U3FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn c800U4FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn c800U5FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cIPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cMipPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cHazInsPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn c904PiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cVaFfProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn c900U1PiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cHazInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cMInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cSchoolTxRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cRealETxRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cFloodInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn c1006RsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn c1007RsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cAggregateAdjRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cEscrowFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cDocPrepFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cNotaryFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cAttorneyFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cTitleInsFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU1TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU2TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU3TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU4TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cRecFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cCountyRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cStateRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU1GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU2GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU3GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cPestInspectFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU1ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU2ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU3ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU4ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimateRightColumn cU5ScProps_ctrl;
		private LosConvert m_convertLos;
        #endregion
        private void InitItemProps( GoodFaithEstimateRightColumn ctrl, int props )
        {
            ctrl.Apr = LosConvert.GfeItemProps_Apr( props );
            ctrl.ToBrok = LosConvert.GfeItemProps_ToBr( props );
            ctrl.PdByT = LosConvert.GfeItemProps_Payer( props );
			ctrl.Fha = LosConvert.GfeItemProps_FhaAllow( props );
            ctrl.Poc = LosConvert.GfeItemProps_Poc(props);
        }

        private Guid m_closingCostID 
        {
            get { return RequestHelper.GetGuid("ccid"); }
        }

		private String m_dupClosingCostID = RequestHelper.GetSafeQueryString("ccid2");

		private Guid m_brokerID 
		{
			get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
		}

        private int RetrieveItemProps( GoodFaithEstimateRightColumn ctrl )
        {
            return LosConvert.GfeItemProps_Pack( ctrl.Apr, ctrl.ToBrok, ctrl.PdByT, ctrl.Fha, ctrl.Poc );
        }

        public void LoadData()
        {
            CCcTemplateData loanData = new CCcTemplateData(m_brokerID, m_closingCostID);
            m_convertLos =loanData.m_convertLos;
            loanData.InitLoad();

            cGfeVersion.SelectedValue = E_GfeVersion.Gfe2009.ToString();
            cDaysInYr.Text = loanData.cDaysInYr_rep;
			
			//if we are duplicating a template, clear the name field to force user to enter a new one
			if((m_dupClosingCostID != "") && (m_dupClosingCostID != null))
				cCcTemplateNm.Text = "";
			else
				cCcTemplateNm.Text = loanData.cCcTemplateNm;

			// cRecFPc, cCountyRtcDesc, and cStateRtcDesc added per OPM 8251
			cRecFDesc.Text = loanData.cRecFDesc;
			cRecFDesc.ToolTip = "Enter description";

			cCountyRtcDesc.Text = loanData.cCountyRtcDesc;
			cCountyRtcDesc.ToolTip = "Enter description";

			cStateRtcDesc.Text = loanData.cStateRtcDesc;
			cStateRtcDesc.ToolTip = "Enter description";



            cU1Fntc.Text = loanData.cU1Fntc_rep;
            cU1FntcDesc.Text = loanData.cU1FntcDesc;
            cTotCcPbsLocked.Checked = loanData.cTotCcPbsLocked;
            cTotCcPbs.Text = loanData.cTotCcPbs_rep;

            c800U5F.Text = loanData.c800U5F_rep;
            c800U5FDesc.Text = loanData.c800U5FDesc;
            c800U5FCode.Text = loanData.c800U5FCode;

            c800U4F.Text = loanData.c800U4F_rep;
            c800U4FDesc.Text = loanData.c800U4FDesc;
            c800U4FCode.Text = loanData.c800U4FCode;

            c800U3F.Text = loanData.c800U3F_rep;
            c800U3FDesc.Text = loanData.c800U3FDesc;
            c800U3FCode.Text = loanData.c800U3FCode;

            c800U2F.Text = loanData.c800U2F_rep;
            c800U2FDesc.Text = loanData.c800U2FDesc;
            c800U2FCode.Text = loanData.c800U2FCode;

            c800U1F.Text = loanData.c800U1F_rep;
            c800U1FDesc.Text = loanData.c800U1FDesc;
            c800U1FCode.Text = loanData.c800U1FCode;

            cWireF.Text = loanData.cWireF_rep;
            cUwF.Text = loanData.cUwF_rep;
            cProcF.Text = loanData.cProcF_rep;
            cProcFPaid.Checked = loanData.cProcFPaid;
            cTxServF.Text = loanData.cTxServF_rep;
            cMBrokFMb.Text = loanData.cMBrokFMb_rep;
            cMBrokFPc.Text = loanData.cMBrokFPc_rep;
            cInspectF.Text = loanData.cInspectF_rep;
            cCrF.Text = loanData.cCrF_rep;
            cCrFPaid.Checked = loanData.cCrFPaid;
            cApprF.Text = loanData.cApprF_rep;
            cApprFPaid.Checked = loanData.cApprFPaid;
            cLDiscntFMb.Text = loanData.cLDiscntFMb_rep;
            cLDiscntPc.Text = loanData.cLDiscntPc_rep;
            cLOrigFMb.Text = loanData.cLOrigFMb_rep;
            cLOrigFPc.Text = loanData.cLOrigFPc_rep;
            cIPiaDy.Text = loanData.cIPiaDy_rep;
            c900U1Pia.Text = loanData.c900U1Pia_rep;
            c900U1PiaDesc.Text = loanData.c900U1PiaDesc;
            c900U1PiaCode.Text = loanData.c900U1PiaCode;
            c904Pia.Text = loanData.c904Pia_rep;
            c904PiaDesc.Text = loanData.c904PiaDesc;
            cHazInsPiaMon.Text = loanData.cHazInsPiaMon_rep;
            Tools.SetDropDownListValue(cProHazInsT, loanData.cProHazInsT);
            
            cProHazInsR.Text = loanData.cProHazInsR_rep;

            cMipPiaMon.Text = loanData.cMipPiaMon_rep;
            Tools.SetDropDownListValue(cProMInsT, loanData.cProMInsT);
            cProMInsR.Text = loanData.cProMInsR_rep;
            cAggregateAdjRsrv.Text = loanData.cAggregateAdjRsrv_rep;

            c1007ProHExp.Text = loanData.c1007ProHExp_rep;
            c1007RsrvMon.Text = loanData.c1007RsrvMon_rep;
            c1007ProHExpDesc.Text = loanData.c1007ProHExpDesc;

            c1006ProHExp.Text = loanData.c1006ProHExp_rep;
            c1006RsrvMon.Text = loanData.c1006RsrvMon_rep;
            c1006ProHExpDesc.Text = loanData.c1006ProHExpDesc;

            cProFloodIns.Text = loanData.cProFloodIns_rep;
            cFloodInsRsrvMon.Text = loanData.cFloodInsRsrvMon_rep;
//            cProRealETx.Text = loanData.cProRealETx_rep; // 11/9/2006 dd - Remove the tax rate from CC template.
            cRealETxRsrvMon.Text = loanData.cRealETxRsrvMon_rep;
            cProSchoolTx.Text = loanData.cProSchoolTx_rep;
            cSchoolTxRsrvMon.Text = loanData.cSchoolTxRsrvMon_rep; //YES
            cMInsRsrvMon.Text = loanData.cMInsRsrvMon_rep;
            cHazInsRsrvMon.Text = loanData.cHazInsRsrvMon_rep;

            cU4Tc.Text = loanData.cU4Tc_rep;
            cU4TcDesc.Text = loanData.cU4TcDesc;
            cU4TcCode.Text = loanData.cU4TcCode;

            cU3Tc.Text = loanData.cU3Tc_rep;
            cU3TcDesc.Text = loanData.cU3TcDesc;
            cU3TcCode.Text = loanData.cU3TcCode;

            cU2Tc.Text = loanData.cU2Tc_rep;        
            cU2TcDesc.Text = loanData.cU2TcDesc;
            cU2TcCode.Text = loanData.cU2TcCode;

            cU1Tc.Text = loanData.cU1Tc_rep;
            cU1TcDesc.Text = loanData.cU1TcDesc;
            cU1TcCode.Text = loanData.cU1TcCode;

            //cTitleInsF.Text = loanData.cTitleInsF_rep;
            Tools.SetDropDownListValue(cTitleInsFBaseT, loanData.cTitleInsFBaseT);
            cTitleInsFMb.Text = loanData.cTitleInsFMb_rep;
            cTitleInsFPc.Text = loanData.cTitleInsFPc_rep;
            cTitleInsFTable.Text = loanData.cTitleInsFTable;

            cAttorneyF.Text = loanData.cAttorneyF_rep;
            cNotaryF.Text = loanData.cNotaryF_rep;
            cDocPrepF.Text = loanData.cDocPrepF_rep;
            //cEscrowF.Text = loanData.cEscrowF_rep;
            Tools.SetDropDownListValue(cEscrowFBaseT, loanData.cEscrowFBaseT);
            cEscrowFPc.Text = loanData.cEscrowFPc_rep;
            cEscrowFMb.Text = loanData.cEscrowFMb_rep;
            cEscrowFTable.Text = loanData.cEscrowFTable;

            cU3GovRtcMb.Text = loanData.cU3GovRtcMb_rep;
            Tools.SetDropDownListValue(cU3GovRtcBaseT, loanData.cU3GovRtcBaseT);
            cU3GovRtcPc.Text = loanData.cU3GovRtcPc_rep;
            cU3GovRtcDesc.Text = loanData.cU3GovRtcDesc;
            cU3GovRtcCode.Text = loanData.cU3GovRtcCode;

            cU2GovRtcMb.Text = loanData.cU2GovRtcMb_rep;
            Tools.SetDropDownListValue(cU2GovRtcBaseT, loanData.cU2GovRtcBaseT);
            cU2GovRtcPc.Text = loanData.cU2GovRtcPc_rep;
            cU2GovRtcDesc.Text = loanData.cU2GovRtcDesc;
            cU2GovRtcCode.Text = loanData.cU2GovRtcCode;

            cU1GovRtcMb.Text = loanData.cU1GovRtcMb_rep;
            Tools.SetDropDownListValue(cU1GovRtcBaseT, loanData.cU1GovRtcBaseT);
            cU1GovRtcPc.Text = loanData.cU1GovRtcPc_rep;
            cU1GovRtcDesc.Text = loanData.cU1GovRtcDesc;
            cU1GovRtcCode.Text = loanData.cU1GovRtcCode;

            cStateRtcMb.Text = loanData.cStateRtcMb_rep;
            Tools.SetDropDownListValue(cStateRtcBaseT, loanData.cStateRtcBaseT);
            cStateRtcPc.Text = loanData.cStateRtcPc_rep;
            cCountyRtcMb.Text = loanData.cCountyRtcMb_rep;
            Tools.SetDropDownListValue(cCountyRtcBaseT, loanData.cCountyRtcBaseT);
            cCountyRtcPc.Text = loanData.cCountyRtcPc_rep; 
            cRecFMb.Text = loanData.cRecFMb_rep;
            Tools.SetDropDownListValue(cRecBaseT, loanData.cRecBaseT);
            cRecFPc.Text = loanData.cRecFPc_rep;

            cU5Sc.Text = loanData.cU5Sc_rep;
            cU5ScDesc.Text = loanData.cU5ScDesc;
            cU5ScCode.Text = loanData.cU5ScCode;

            cU4Sc.Text = loanData.cU4Sc_rep;
            cU4ScDesc.Text = loanData.cU4ScDesc;
            cU4ScCode.Text = loanData.cU4ScCode;

            cU3Sc.Text = loanData.cU3Sc_rep;
            cU3ScDesc.Text = loanData.cU3ScDesc;
            cU3ScCode.Text = loanData.cU3ScCode;

            cU2Sc.Text = loanData.cU2Sc_rep;
            cU2ScDesc.Text = loanData.cU2ScDesc;
            cU2ScCode.Text = loanData.cU2ScCode;

            cU1Sc.Text = loanData.cU1Sc_rep;
            cU1ScDesc.Text = loanData.cU1ScDesc;
            cU1ScCode.Text = loanData.cU1ScCode;

            cPestInspectF.Text = loanData.cPestInspectF_rep;
            cBrokComp2.Text = loanData.cBrokComp2_rep;
            cBrokComp2Desc.Text = loanData.cBrokComp2Desc;
            cBrokComp1.Text = loanData.cBrokComp1_rep;
            cBrokComp1Desc.Text = loanData.cBrokComp1Desc;
            cGfeProvByBrok.Checked = loanData.cGfeProvByBrok;
            cDisabilityIns.Text = loanData.cDisabilityIns_rep; 

            InitItemProps( cLOrigFProps_ctrl, loanData.cLOrigFProps );
            InitItemProps( cLDiscntProps_ctrl, loanData.cLDiscntProps );
            InitItemProps( cApprFProps_ctrl, loanData.cApprFProps );
            InitItemProps( cCrFProps_ctrl, loanData.cCrFProps );
            InitItemProps( cInspectFProps_ctrl, loanData.cInspectFProps );
            InitItemProps( cMBrokFProps_ctrl, loanData.cMBrokFProps );
            InitItemProps( cTxServFProps_ctrl, loanData.cTxServFProps );
            InitItemProps( cProcFProps_ctrl, loanData.cProcFProps );
            InitItemProps( cUwFProps_ctrl, loanData.cUwFProps );
            InitItemProps( cWireFProps_ctrl, loanData.cWireFProps );
            InitItemProps( c800U1FProps_ctrl, loanData.c800U1FProps );
            InitItemProps( c800U2FProps_ctrl, loanData.c800U2FProps );
            InitItemProps( c800U3FProps_ctrl, loanData.c800U3FProps );	
            InitItemProps( c800U4FProps_ctrl, loanData.c800U4FProps );
            InitItemProps( c800U5FProps_ctrl, loanData.c800U5FProps );
            InitItemProps( cIPiaProps_ctrl, loanData.cIPiaProps );
            InitItemProps( cMipPiaProps_ctrl, loanData.cMipPiaProps );
            InitItemProps( cHazInsPiaProps_ctrl, loanData.cHazInsPiaProps );
            InitItemProps( c904PiaProps_ctrl, loanData.c904PiaProps );
            InitItemProps( cVaFfProps_ctrl, loanData.cVaFfProps );
            InitItemProps( c900U1PiaProps_ctrl, loanData.c900U1PiaProps );
            InitItemProps( cHazInsRsrvProps_ctrl, loanData.cHazInsRsrvProps );
            InitItemProps( cMInsRsrvProps_ctrl, loanData.cMInsRsrvProps );
            InitItemProps( cSchoolTxRsrvProps_ctrl, loanData.cSchoolTxRsrvProps );
            InitItemProps( cRealETxRsrvProps_ctrl, loanData.cRealETxRsrvProps );
            InitItemProps( cFloodInsRsrvProps_ctrl, loanData.cFloodInsRsrvProps );
            InitItemProps( c1006RsrvProps_ctrl, loanData.c1006RsrvProps );
            InitItemProps( c1007RsrvProps_ctrl, loanData.c1007RsrvProps );
            InitItemProps( cAggregateAdjRsrvProps_ctrl, loanData.cAggregateAdjRsrvProps );
            InitItemProps( cEscrowFProps_ctrl, loanData.cEscrowFProps );
            InitItemProps( cDocPrepFProps_ctrl, loanData.cDocPrepFProps );
            InitItemProps( cNotaryFProps_ctrl, loanData.cNotaryFProps );
            InitItemProps( cAttorneyFProps_ctrl, loanData.cAttorneyFProps );
            InitItemProps( cTitleInsFProps_ctrl, loanData.cTitleInsFProps );
            InitItemProps( cU1TcProps_ctrl, loanData.cU1TcProps );
            InitItemProps( cU2TcProps_ctrl, loanData.cU2TcProps );
            InitItemProps( cU3TcProps_ctrl, loanData.cU3TcProps );
            InitItemProps( cU4TcProps_ctrl, loanData.cU4TcProps );
            InitItemProps( cRecFProps_ctrl, loanData.cRecFProps );
            InitItemProps( cCountyRtcProps_ctrl, loanData.cCountyRtcProps );
            InitItemProps( cStateRtcProps_ctrl, loanData.cStateRtcProps );
            InitItemProps( cU1GovRtcProps_ctrl, loanData.cU1GovRtcProps );
            InitItemProps( cU2GovRtcProps_ctrl, loanData.cU2GovRtcProps );
            InitItemProps( cU3GovRtcProps_ctrl, loanData.cU3GovRtcProps );
            InitItemProps( cPestInspectFProps_ctrl, loanData.cPestInspectFProps );
            InitItemProps( cU1ScProps_ctrl, loanData.cU1ScProps );
            InitItemProps( cU2ScProps_ctrl, loanData.cU2ScProps );
            InitItemProps( cU3ScProps_ctrl, loanData.cU3ScProps );
            InitItemProps( cU4ScProps_ctrl, loanData.cU4ScProps );
            InitItemProps( cU5ScProps_ctrl, loanData.cU5ScProps );

			//if we are duplicating a template, save right away so if the user cancels, the duplicated data persists
			if((m_dupClosingCostID != "") && (m_dupClosingCostID != null))
				SaveData();
        }


        public void SaveData()
        {
            CCcTemplateData loanData;
            m_convertLos = new LosConvert();

            // we are duplicating an existing template, which is at this point essentially adding a new one
            if ((m_dupClosingCostID != "") && (m_dupClosingCostID != null))
            {
                Guid m_newClosingCostID = new Guid(m_dupClosingCostID);
                loanData = new CCcTemplateData(m_brokerID, m_newClosingCostID);
            }
            else
            {
                loanData = new CCcTemplateData(m_brokerID, m_closingCostID);
            }

            loanData.InitSave();

			// cRecFPc, cCountyRtcDesc, and cStateRtcDesc added per OPM 8251
			loanData.cRecFDesc = cRecFDesc.Text;
			loanData.cCountyRtcDesc = cCountyRtcDesc.Text;
			loanData.cStateRtcDesc = cStateRtcDesc.Text;

			loanData.cDaysInYr_rep = cDaysInYr.Text;
            loanData.cU1Fntc_rep = cU1Fntc.Text ;
            loanData.cU1FntcDesc = cU1FntcDesc.Text;
            loanData.cTotCcPbsLocked = cTotCcPbsLocked.Checked;
            loanData.cTotCcPbs_rep = cTotCcPbs.Text ;
            loanData.cCcTemplateNm = cCcTemplateNm.Text;
            loanData.c800U5F_rep = c800U5F.Text;
            loanData.c800U5FDesc = c800U5FDesc.Text;
            loanData.c800U5FCode = c800U5FCode.Text;
            loanData.c800U4F_rep = c800U4F.Text ;
            loanData.c800U4FDesc = c800U4FDesc.Text;
            loanData.c800U4FCode = c800U4FCode.Text;
            loanData.c800U3F_rep = c800U3F.Text ;
            loanData.c800U3FDesc = c800U3FDesc.Text;
            loanData.c800U3FCode = c800U3FCode.Text;
            loanData.c800U2F_rep = c800U2F.Text ;
            loanData.c800U2FDesc = c800U2FDesc.Text;
            loanData.c800U2FCode = c800U2FCode.Text;
            loanData.c800U1F_rep = c800U1F.Text ;
            loanData.c800U1FDesc = c800U1FDesc.Text;
            loanData.c800U1FCode = c800U1FCode.Text;
            loanData.cWireF_rep = cWireF.Text ;
            loanData.cUwF_rep = cUwF.Text ;
            loanData.cProcF_rep = cProcF.Text ;
            loanData.cProcFPaid = cProcFPaid.Checked;
            loanData.cTxServF_rep = cTxServF.Text ;
            loanData.cMBrokFMb_rep = cMBrokFMb.Text ;
            loanData.cMBrokFPc_rep = cMBrokFPc.Text ;
            loanData.cInspectF_rep = cInspectF.Text ;
            loanData.cCrF_rep = cCrF.Text;
            loanData.cCrFPaid = cCrFPaid.Checked;
            loanData.cApprF_rep = cApprF.Text;
            loanData.cApprFPaid = cApprFPaid.Checked;
            loanData.cLDiscntFMb_rep = cLDiscntFMb.Text;
            loanData.cLDiscntPc_rep = cLDiscntPc.Text;
            loanData.cLOrigFMb_rep = cLOrigFMb.Text;
            loanData.cLOrigFPc_rep = cLOrigFPc.Text;
            loanData.cIPiaDy_rep = cIPiaDy.Text ;
            loanData.c900U1Pia_rep = c900U1Pia.Text ;
            loanData.c900U1PiaDesc = c900U1PiaDesc.Text;
            loanData.c900U1PiaCode = c900U1PiaCode.Text;
            loanData.c904Pia_rep = c904Pia.Text ;
            loanData.c904PiaDesc = c904PiaDesc.Text;
            loanData.cHazInsPiaMon_rep = cHazInsPiaMon.Text ;
            loanData.cProHazInsT = (E_PercentBaseT)Tools.GetDropDownListValue(cProHazInsT);
            loanData.cProHazInsR_rep = cProHazInsR.Text ;
            loanData.cMipPiaMon_rep = cMipPiaMon.Text;
            loanData.cProMInsT = (E_PercentBaseT)Tools.GetDropDownListValue(cProMInsT);
            loanData.cProMInsR_rep = cProMInsR.Text ;
            loanData.c1007ProHExp_rep = c1007ProHExp.Text ;
            loanData.c1007RsrvMon_rep = c1007RsrvMon.Text;
            loanData.c1007ProHExpDesc = c1007ProHExpDesc.Text;
            loanData.c1006ProHExp_rep = c1006ProHExp.Text ;
            loanData.c1006RsrvMon_rep = c1006RsrvMon.Text;
            loanData.c1006ProHExpDesc = c1006ProHExpDesc.Text;
            loanData.cProFloodIns_rep = cProFloodIns.Text ;
            loanData.cFloodInsRsrvMon_rep = cFloodInsRsrvMon.Text;
            loanData.cRealETxRsrvMon_rep = cRealETxRsrvMon.Text;
            loanData.cProSchoolTx_rep = cProSchoolTx.Text ;
            loanData.cSchoolTxRsrvMon_rep = cSchoolTxRsrvMon.Text;
            loanData.cMInsRsrvMon_rep = cMInsRsrvMon.Text;
            loanData.cHazInsRsrvMon_rep = cHazInsRsrvMon.Text;
            loanData.cU4Tc_rep = cU4Tc.Text ;
            loanData.cU4TcDesc = cU4TcDesc.Text;
            loanData.cU4TcCode = cU4TcCode.Text;
            loanData.cU3Tc_rep = cU3Tc.Text ;
            loanData.cU3TcDesc = cU3TcDesc.Text;
            loanData.cU3TcCode = cU3TcCode.Text;
            loanData.cU2Tc_rep = cU2Tc.Text ;
            loanData.cU2TcDesc = cU2TcDesc.Text;
            loanData.cU2TcCode = cU2TcCode.Text;
            loanData.cU1Tc_rep = cU1Tc.Text ;
            loanData.cU1TcDesc = cU1TcDesc.Text;
            loanData.cU1TcCode = cU1TcCode.Text;
            loanData.cTitleInsFPc_rep = cTitleInsFPc.Text;
            loanData.cTitleInsFBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cTitleInsFBaseT);
            loanData.cTitleInsFMb_rep = cTitleInsFMb.Text;
            //loanData.cTitleInsF_rep = cTitleInsF.Text ;
            loanData.cTitleInsFTable = cTitleInsFTable.Text;
            loanData.cAttorneyF_rep = cAttorneyF.Text ;
            loanData.cNotaryF_rep = cNotaryF.Text ;
            loanData.cDocPrepF_rep = cDocPrepF.Text ;
            //loanData.cEscrowF_rep = cEscrowF.Text ;
            loanData.cEscrowFPc_rep = cEscrowFPc.Text;
            loanData.cEscrowFBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cTitleInsFBaseT);
            loanData.cEscrowFMb_rep = cEscrowFMb.Text;
            loanData.cEscrowFTable = cEscrowFTable.Text;
            loanData.cU3GovRtcMb_rep = cU3GovRtcMb.Text ;
            loanData.cU3GovRtcBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cU3GovRtcBaseT);
            loanData.cU3GovRtcPc_rep = cU3GovRtcPc.Text ;
            loanData.cU3GovRtcDesc = cU3GovRtcDesc.Text;
            loanData.cU3GovRtcCode = cU3GovRtcCode.Text;
            loanData.cU2GovRtcMb_rep = cU2GovRtcMb.Text ;
            loanData.cU2GovRtcBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cU2GovRtcBaseT);
            loanData.cU2GovRtcPc_rep = cU2GovRtcPc.Text ;
            loanData.cU2GovRtcDesc = cU2GovRtcDesc.Text;
            loanData.cU2GovRtcCode = cU2GovRtcCode.Text;
            loanData.cU1GovRtcMb_rep = cU1GovRtcMb.Text ;
            loanData.cU1GovRtcBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cU1GovRtcBaseT);
            loanData.cU1GovRtcPc_rep = cU1GovRtcPc.Text ;
            loanData.cU1GovRtcDesc = cU1GovRtcDesc.Text;
            loanData.cU1GovRtcCode = cU1GovRtcCode.Text;
            loanData.cStateRtcMb_rep = cStateRtcMb.Text ;
            loanData.cStateRtcBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cStateRtcBaseT);
            loanData.cStateRtcPc_rep = cStateRtcPc.Text ;
            loanData.cCountyRtcMb_rep = cCountyRtcMb.Text ;
            loanData.cCountyRtcBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cCountyRtcBaseT);
            loanData.cCountyRtcPc_rep = cCountyRtcPc.Text ;
            loanData.cRecFMb_rep = cRecFMb.Text ;
            loanData.cRecBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cRecBaseT);
            loanData.cRecFPc_rep = cRecFPc.Text ;
            loanData.cU5Sc_rep = cU5Sc.Text ;
            loanData.cU5ScDesc = cU5ScDesc.Text;
            loanData.cU5ScCode = cU5ScCode.Text;
            loanData.cU4Sc_rep = cU4Sc.Text ;
            loanData.cU4ScDesc = cU4ScDesc.Text;
            loanData.cU4ScCode = cU4ScCode.Text;
            loanData.cU3Sc_rep = cU3Sc.Text ;
            loanData.cU3ScDesc = cU3ScDesc.Text;
            loanData.cU3ScCode = cU3ScCode.Text;
            loanData.cU2Sc_rep = cU2Sc.Text ;
            loanData.cU2ScDesc = cU2ScDesc.Text;
            loanData.cU2ScCode = cU2ScCode.Text;
            loanData.cU1Sc_rep = cU1Sc.Text ;
            loanData.cU1ScDesc = cU1ScDesc.Text;
            loanData.cU1ScCode = cU1ScCode.Text;
            loanData.cPestInspectF_rep = cPestInspectF.Text ;
            loanData.cBrokComp2_rep = cBrokComp2.Text ;
            loanData.cBrokComp2Desc = cBrokComp2Desc.Text;
            loanData.cBrokComp1_rep = cBrokComp1.Text ;
            loanData.cBrokComp1Desc = cBrokComp1Desc.Text;
            loanData.cGfeProvByBrok = cGfeProvByBrok.Checked;
//            loanData.cProRealETx_rep = cProRealETx.Text ;
            loanData.cDisabilityIns_rep = cDisabilityIns.Text ;

            loanData.cLOrigFProps = RetrieveItemProps( cLOrigFProps_ctrl );
            loanData.cLDiscntProps = RetrieveItemProps( cLDiscntProps_ctrl );
            loanData.cApprFProps = RetrieveItemProps( cApprFProps_ctrl );
            loanData.cCrFProps = RetrieveItemProps( cCrFProps_ctrl );
            loanData.cInspectFProps = RetrieveItemProps( cInspectFProps_ctrl );
            loanData.cMBrokFProps = RetrieveItemProps( cMBrokFProps_ctrl );
            loanData.cTxServFProps = RetrieveItemProps( cTxServFProps_ctrl );
            loanData.cProcFProps = RetrieveItemProps( cProcFProps_ctrl );
            loanData.cUwFProps = RetrieveItemProps( cUwFProps_ctrl );
            loanData.cWireFProps = RetrieveItemProps( cWireFProps_ctrl );
            loanData.c800U1FProps = RetrieveItemProps( c800U1FProps_ctrl );
            loanData.c800U2FProps = RetrieveItemProps( c800U2FProps_ctrl );
            loanData.c800U3FProps = RetrieveItemProps( c800U3FProps_ctrl );	
            loanData.c800U4FProps = RetrieveItemProps( c800U4FProps_ctrl );
            loanData.c800U5FProps = RetrieveItemProps( c800U5FProps_ctrl );
            loanData.cIPiaProps = RetrieveItemProps( cIPiaProps_ctrl );
            loanData.cMipPiaProps = RetrieveItemProps( cMipPiaProps_ctrl );
            loanData.cHazInsPiaProps = RetrieveItemProps( cHazInsPiaProps_ctrl );
            loanData.c904PiaProps = RetrieveItemProps( c904PiaProps_ctrl );
            loanData.cVaFfProps = RetrieveItemProps( cVaFfProps_ctrl );
            loanData.c900U1PiaProps = RetrieveItemProps( c900U1PiaProps_ctrl );
            loanData.cHazInsRsrvProps = RetrieveItemProps( cHazInsRsrvProps_ctrl );
            loanData.cMInsRsrvProps = RetrieveItemProps( cMInsRsrvProps_ctrl );
            loanData.cSchoolTxRsrvProps = RetrieveItemProps( cSchoolTxRsrvProps_ctrl );
            loanData.cRealETxRsrvProps = RetrieveItemProps( cRealETxRsrvProps_ctrl );
            loanData.cFloodInsRsrvProps = RetrieveItemProps( cFloodInsRsrvProps_ctrl );
            loanData.c1006RsrvProps = RetrieveItemProps( c1006RsrvProps_ctrl );
            loanData.c1007RsrvProps = RetrieveItemProps( c1007RsrvProps_ctrl );
            loanData.cAggregateAdjRsrvProps = RetrieveItemProps( cAggregateAdjRsrvProps_ctrl );
            loanData.cAggregateAdjRsrv_rep = cAggregateAdjRsrv.Text;
            loanData.cEscrowFProps = RetrieveItemProps( cEscrowFProps_ctrl );
            loanData.cDocPrepFProps = RetrieveItemProps( cDocPrepFProps_ctrl );
            loanData.cNotaryFProps = RetrieveItemProps( cNotaryFProps_ctrl );
            loanData.cAttorneyFProps = RetrieveItemProps( cAttorneyFProps_ctrl );
            loanData.cTitleInsFProps = RetrieveItemProps( cTitleInsFProps_ctrl );
            loanData.cU1TcProps = RetrieveItemProps( cU1TcProps_ctrl );
            loanData.cU2TcProps = RetrieveItemProps( cU2TcProps_ctrl );
            loanData.cU3TcProps = RetrieveItemProps( cU3TcProps_ctrl );
            loanData.cU4TcProps = RetrieveItemProps( cU4TcProps_ctrl );
            loanData.cRecFProps = RetrieveItemProps( cRecFProps_ctrl );
            loanData.cCountyRtcProps = RetrieveItemProps( cCountyRtcProps_ctrl );
            loanData.cStateRtcProps = RetrieveItemProps( cStateRtcProps_ctrl );
            loanData.cU1GovRtcProps = RetrieveItemProps( cU1GovRtcProps_ctrl );
            loanData.cU2GovRtcProps = RetrieveItemProps( cU2GovRtcProps_ctrl );
            loanData.cU3GovRtcProps = RetrieveItemProps( cU3GovRtcProps_ctrl );
            loanData.cPestInspectFProps = RetrieveItemProps( cPestInspectFProps_ctrl );
            loanData.cU1ScProps = RetrieveItemProps( cU1ScProps_ctrl );
            loanData.cU2ScProps = RetrieveItemProps( cU2ScProps_ctrl );
            loanData.cU3ScProps = RetrieveItemProps( cU3ScProps_ctrl );
            loanData.cU4ScProps = RetrieveItemProps( cU4ScProps_ctrl );
            loanData.cU5ScProps = RetrieveItemProps( cU5ScProps_ctrl );

            loanData.GfeVersion = (E_GfeVersion)Enum.Parse(typeof(E_GfeVersion), cGfeVersion.SelectedValue);
            loanData.Save();
        }
		
		protected void PageLoad(object sender, System.EventArgs e)
		{
            if (m_hiddenChoice.Value == "Don't Save")
                Response.Redirect("CCTemplateGFE2010.aspx?ccid=" + AspxTools.HtmlStringFromQueryString("ccid"));
            m_errMsg.Text = "";
                        
            // OPM 3453
			if(!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanAccessCCTemplates))
			{
				m_AccessDeniedPanel.Visible = true;
				m_AccessAllowedPanel.Visible = false;
			}
			else
			{
				m_AccessDeniedPanel.Visible = false;
				m_AccessAllowedPanel.Visible = true;
			}
			
			// Put user code to initialize the page here
            if (!Page.IsPostBack) 
            {
                LoadData();
            } 
            else 
            {
                Page.Validate();
                if (Page.IsValid)
                {
                    bool saveSuccess = false;
                    try
                    {
                        SaveData();
                        saveSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        m_errMsg.Text = "An error occurred during the save process";
                        Tools.LogError(ex.Message);
                    }
                    if (m_hiddenChoice.Value == "Save" && saveSuccess)
                        Response.Redirect("CCTemplateGFE2010.aspx?ccid=" + AspxTools.HtmlStringFromQueryString("ccid"));
                }
                cGfeVersion.SelectedValue = E_GfeVersion.Gfe2009.ToString();
                m_hiddenChoice.Value = "";
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

        protected void onOKClick(object sender, System.EventArgs e)
        {
            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page);
        }

	}
}
