<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LoanProgramArmDisclosure.ascx.cs" Inherits="LendersOfficeApp.los.Template.LoanProgramArmDisclosure" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Panel id="m_Base" runat="server">
	<div id="Rc" style="BORDER-RIGHT: dimgray 2px solid; PADDING-RIGHT: 12px; BORDER-TOP: dimgray 2px solid; PADDING-LEFT: 12px; PADDING-BOTTOM: 12px; BORDER-LEFT: dimgray 2px solid; WIDTH: 96%; PADDING-TOP: 12px; BORDER-BOTTOM: dimgray 2px solid; background-color: whitesmoke;">
		<div style="MARGIN-BOTTOM: 16px; WIDTH: 100%;">
			How the interest rate can change:
		</div>
		<div style="MARGIN-BOTTOM: 8px; WIDTH: 100%;">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
			<td nowrap>
				<table cellpadding="0" cellspacing="0" border="0">
				<tr>
				<td nowrap class="FieldLabel">
					1st Change
				</td>
				<td>
				</td>
				<td nowrap>
					<asp:TextBox id="lRAdj1stCapMon" runat="server" style="PADDING-LEFT: 4px;" Width="120px">
					</asp:TextBox>
							<asp:RequiredFieldValidator id="lRAdj1stCapMonValidator"
                               ControlToValidate="lRAdj1stCapMon"
                               InitialValue="0"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
					(months)
				</td>
				</tr>
				<tr>
				<td nowrap class="FieldLabel">
					Adj Period
				</td>
				<td>
				</td>
				<td nowrap>
					<asp:TextBox id="lRAdjCapMon" runat="server" style="PADDING-LEFT: 4px;" Width="120px">
					</asp:TextBox>
					(months)
				</td>
				</tr>
				<tr>
				<td nowrap class="FieldLabel">
					1st Adj Cap
				</td>
				<td nowrap Width="16px">
				</td>
				<td nowrap>
					<ml:PercentTextBox id="lRAdj1stCapR" runat="server" style="PADDING-LEFT: 4px;" Width="120px" preset="percent">
					</ml:PercentTextBox>
												<asp:RequiredFieldValidator id="lRAdj1stCapRValidator"
                               ControlToValidate="lRAdj1stCapR"
                               InitialValue="0.000%"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
				</td>
				</tr>
				<tr>
				<td nowrap class="FieldLabel">
					Adj Cap
				</td>
				<td>
				</td>
				<td nowrap>
					<ml:PercentTextBox id="lRAdjCapR" runat="server" style="PADDING-LEFT: 4px;" Width="120px" preset="percent">
					</ml:PercentTextBox>
																	<asp:RequiredFieldValidator id="lRAdjCapRValidator"
                               ControlToValidate="lRAdjCapR"
                               InitialValue="0.000%"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>

				</td>
				</tr>
				<tr>
				<td nowrap class="FieldLabel">
					Life Adj Cap
				</td>
				<td>
				</td>
				<td nowrap>
					<ml:PercentTextBox id="lRAdjLifeCapR" runat="server" style="PADDING-LEFT: 4px;" Width="120px" preset="percent">
					</ml:PercentTextBox>
								<asp:RequiredFieldValidator id="lRAdjLifeCapRValidator"
                               ControlToValidate="lRAdjLifeCapR"
                               InitialValue="0.000%"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
				</td>
				</tr>
				<tr>
				<td nowrap class="FieldLabel">
					Rate Floor
				</td>
				<td>
				</td>
				<td nowrap>
					<asp:DropDownList id="lRAdjFloorBaseT" runat="server" style="PADDING-LEFT: 4px;">
						<asp:ListItem Value="0">Margin
						</asp:ListItem>
						<asp:ListItem Value="1">Start Rate
						</asp:ListItem>
						<asp:ListItem Value="2">0%
						</asp:ListItem>
					</asp:DropDownList>
												<asp:RequiredFieldValidator id="lRAdjFloorBaseTValidator"
                               ControlToValidate="lRAdjFloorBaseT"
                               InitialValue="-1"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
					 +
					<ml:PercentTextBox id="lRAdjFloorR" runat="server" style="PADDING-LEFT: 4px;" Width="75px" preset="percent">
					</ml:PercentTextBox>
						<asp:RequiredFieldValidator id="lRAdjFloorRValidator"
                               ControlToValidate="lRAdjFloorR"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
				</td>
				</tr>
				<tr>
				<td nowrap class="FieldLabel">
					Margin
				</td>
				<td>
				</td>
				<td nowrap>
					<ml:PercentTextBox id="lRAdjMarginR" runat="server" style="PADDING-LEFT: 4px;" Width="120px" preset="percent">
					</ml:PercentTextBox>
					<asp:Hyperlink id="lRAdjMarginLink" runat="server" style="TEXT-DECORATION: none;" NavigateUrl="#">
						Margins
					</asp:Hyperlink>
				</td>
				</tr>
				<tr>
				<td nowrap class="FieldLabel">
					Round
				</td>
				<td>
				</td>
				<td nowrap>
					<asp:DropDownList id="lRAdjRoundT" runat="server" style="PADDING-LEFT: 4px;">
						<asp:ListItem Value="0">Normal
						</asp:ListItem>
						<asp:ListItem Value="1">Up
						</asp:ListItem>
						<asp:ListItem Value="2">Down
						</asp:ListItem>
					</asp:DropDownList>
							<asp:RequiredFieldValidator id="lRAdjRoundTValidator"
                               ControlToValidate="lRAdjRoundT"
                               EnableClientScript="true"
                               InitialValue="-1"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
					to
					<ml:PercentTextBox id="lRAdjRoundToR" runat="server" style="PADDING-LEFT: 4px;" Width="45px" preset="percent">
					</ml:PercentTextBox>
								<asp:RequiredFieldValidator id="lRAdjRoundToRValidator"
                               ControlToValidate="lRAdjRoundToR"
                               EnableClientScript="true"

                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
				</td>
				</tr>
				</table>
			</td>
			<td width="32px">
			</td>
			<td>
				<div style="WIDTH: 240px;">
					Borrower will be notified in writing of index rate changes
					at least
					<asp:TextBox id="lArmIndexNotifyAtLeastDaysVstr" runat="server" style="PADDING-LEFT: 4px;" Width="50px">
					</asp:TextBox>
					days, but not more than
					<asp:TextBox id="lArmIndexNotifyNotBeforeDaysVstr" runat="server" style="PADDING-LEFT: 4px;" Width="50px">
					</asp:TextBox>
					days before the due date of a payment at a new level
				</div>
			</td>
			</tr>
			</table>
		</div>

		<div style="MARGIN-BOTTOM: 16px; WIDTH: 100%;">
			How the interest rate and payments are determined:
		</div>
		<div style="MARGIN-BOTTOM: 8px; WIDTH: 100%;">
			<div class="FieldLabel" style="DISPLAY: inline;">
				ARM Index to apply
			</div>
			&nbsp;
			<asp:DropDownList id="lArmIndexGuid" runat="server" onchange="onArmIndexChange( this.value );">
			</asp:DropDownList>
						<asp:RequiredFieldValidator id="lArmIndexGuidValidator"
                               ControlToValidate="lArmIndexGuid"
                               InitialValue="00000000-0000-0000-0000-000000000000||"
                               EnableClientScript="true"
                               runat="server"
                               Enabled="false"
                           ><img runat="server" src="../../images/error_icon.gif"></asp:RequiredFieldValidator>
		</div>
		<div style="MARGIN-BOTTOM: 8px; WIDTH: 100%;">
			<script>
				function onArmIndexChange( sEntry ) { try
				{
					var parts = sEntry.split( "|" );

					if( parts.length > 1 )
					{
						<%= AspxTools.JsGetElementById(lArmIndexBasedOnVstr) %>.value = parts[ 1 ];
					}

					if( parts.length > 2 )
					{
						<%= AspxTools.JsGetElementById(lArmIndexCanBeFoundVstr) %>.value = parts[ 2 ];
					}
				}
				catch( e )
				{
					window.status = e.Message; alert( e.Message );
				}}

				function f_updateForArm( bIsArm )
				{
				    if ( bIsArm )
				    {
				        <%= AspxTools.JsGetElementById(lArmIndexAffectInitIRBit) %>.checked = "checked";
				    } else {
				        // OPM 222484 - Reset ARM fields for non-Arm
				        <%= AspxTools.JsGetElementById(lRAdj1stCapR) %>.value = "0.000%";
				        <%= AspxTools.JsGetElementById(lRAdj1stCapMon) %>.value = "0";
				        <%= AspxTools.JsGetElementById(lRAdjCapR) %>.value = "0.000%";
				        <%= AspxTools.JsGetElementById(lRAdjCapMon) %>.value = "0";
				        <%= AspxTools.JsGetElementById(lRAdjLifeCapR) %>.value = "0.000%";
				        <%= AspxTools.JsGetElementById(lRAdjFloorR) %>.value = "";
				        <%= AspxTools.JsGetElementById(lRAdjRoundToR) %>.value = "";
				        <%= AspxTools.JsGetElementById(lArmIndexGuid) %>.selectedIndex = 0;

				        <%= AspxTools.JsGetElementById(lArmIndexNotifyAtLeastDaysVstr) %>.value = "";
				        <%= AspxTools.JsGetElementById(lArmIndexNotifyNotBeforeDaysVstr) %>.value = "";
				        <%= AspxTools.JsGetElementById(lArmIndexBasedOnVstr) %>.value = "";
				        <%= AspxTools.JsGetElementById(lArmIndexCanBeFoundVstr) %>.value = "";

				        var lRAdjMarginR = <%= AspxTools.JsGetElementById(lRAdjMarginR) %>;
                        if (lRAdjMarginR) {
                            lRAdjMarginR.value = "0.000%";
                        }
				    }

				    <% if(m_isAuthorCreateMode) { %>

				    ValidatorEnable(<%= AspxTools.JsGetElementById(lRAdj1stCapMonValidator) %>, bIsArm);
				    ValidatorEnable(<%= AspxTools.JsGetElementById(lRAdj1stCapRValidator) %>, bIsArm);
				    ValidatorEnable(<%= AspxTools.JsGetElementById(lRAdjCapRValidator) %>, bIsArm);
				    ValidatorEnable(<%= AspxTools.JsGetElementById(lRAdjLifeCapRValidator) %>, bIsArm);
				    ValidatorEnable(<%= AspxTools.JsGetElementById(lRAdjFloorBaseTValidator) %>, bIsArm);
				    ValidatorEnable(<%= AspxTools.JsGetElementById(lRAdjFloorRValidator) %>, bIsArm);
				    ValidatorEnable(<%= AspxTools.JsGetElementById(lRAdjRoundTValidator) %>, bIsArm);
				    ValidatorEnable(<%= AspxTools.JsGetElementById(lRAdjRoundToRValidator) %>, bIsArm);
				    ValidatorEnable(<%= AspxTools.JsGetElementById(lArmIndexGuidValidator) %>, bIsArm);

				    // In this mode, we offer a blank default, but if the SAE chooses an amort. type
				    // that would hide these, we have to select the legal value.
				    <%= AspxTools.JsGetElementById(lRAdjFloorBaseT) %>.selectedIndex =  bIsArm ? 0 : 1;
				    <%= AspxTools.JsGetElementById(lRAdjRoundT) %>.selectedIndex = bIsArm ? 0 : 1;

				    <% } %>

				}
			</script>
			<div class="FieldLabel">
				The interest will be based on
			</div>
			<asp:TextBox id="lArmIndexBasedOnVstr" runat="server" style="PADDING: 4px;" Height="40px" Width="500px" TextMode="MultiLine">
			</asp:TextBox>
			<br>
			<div class="FieldLabel">
				Information about the index can be found
			</div>
			<asp:TextBox id="lArmIndexCanBeFoundVstr" runat="server" style="PADDING: 4px;" Height="40px" Width="500px" TextMode="MultiLine">
			</asp:TextBox>
		</div>
		<div style="MARGIN-BOTTOM: 0px; WIDTH: 100%;">
			<asp:CheckBox id="lArmIndexAffectInitIRBit" runat="server" Text="Your initial interest rate is not based on the index">
			</asp:CheckBox>
		</div>
	    <div style="MARGIN-BOTTOM: 0px; WIDTH: 100%;">
			<asp:CheckBox id="IsConvertibleMortgage" runat="server" Text="Convertible Mortgage">
			</asp:CheckBox>
		</div>
	</div>
</asp:Panel>
