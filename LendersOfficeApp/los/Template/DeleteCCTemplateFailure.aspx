<%@ Page language="c#" Codebehind="DeleteCCTemplateFailure.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.DeleteCCTemplateFailure" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>ClosingCostList</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout" scroll="yes" onload="init();" bgcolor="gainsboro">
		<script language="javascript">
    <!--
    
    function init() 
    {
        <% if ( !IsPostBack ) { %>
        resize(400, 200); 
        <% } %>
        
        if( document.getElementById("m_errorMessage") != null && document.getElementById("m_errorMessage").value != "" )
		{
			alert( document.getElementById("m_errorMessage").value );
		}

		if( document.getElementById("m_feedBack") != null && document.getElementById("m_feedBack").value != "" )
		{
			alert( document.getElementById("m_feedBack").value );
		}
	}
    
    function f_viewList()
    {
		resize(650, 540);
		document.forms[0].submit();
    }
    
    function onEditClick(fileID)
	{
		window.open( VRoot + "/los/Template/LoanProgramTemplate.aspx?fileid=" + fileID , "_blank" , "resizable=yes,status=yes,menubar=yes,scrollbars=yes" );
	}
	
	function validateCheckList() {
	    var collection = $(".templateid:checked");
		if(collection.length == 0)
		{
			alert('No loan program is selected.');
			return false;
		}

		return confirm('Do you want to delete these selected loan programs?');
	}
	
	function checkAll()
	{
	    var bChecked = document.getElementById("chkCheckAll").checked;
	    $(".templateid").prop("checked", bChecked);
	}
    
    //-->
		</script>
		<h4 class="page-header">Loan Program List</h4>
		<form id="DeleteCCTemplateFailure" method="post" runat="server">
			<table width="100%">
				<asp:Panel id="m_initialPanel" runat="server" style="PADDING-RIGHT: 50px; PADDING-LEFT: 50px; PADDING-BOTTOM: 50px; FONT: 11px arial; COLOR: tomato; PADDING-TOP: 50px; TEXT-ALIGN: justify">
					<TBODY>
						<TR>
							<TD style="FONT-WEIGHT: bold; COLOR: black" align="center">
								<P><BR>
									Closing Cost Template cannot be deleted because it<BR>
									is still associated with one or more loan programs.</P>
							</TD>
						</TR>
						<TR>
							<TD>&nbsp;</TD>
						</TR>
						<TR>
							<TD align="center">
								<INPUT class="buttonstyle" style="WIDTH: 200px" onclick="f_viewList();" type="button" value="View Associated Loan Programs">
								&nbsp; <INPUT class="buttonstyle" style="WIDTH: 50px" onclick="onClosePopup();" type="button" value="Cancel">
							</TD>
						</TR>
				</asp:Panel>
				<asp:Panel id="m_viewList" runat="server" style="PADDING-RIGHT: 50px; PADDING-LEFT: 50px; PADDING-BOTTOM: 50px; FONT: 11px arial; COLOR: tomato; PADDING-TOP: 50px; TEXT-ALIGN: justify">
					<TR>
						<TD style="FONT-WEIGHT: bold; COLOR: black"><BR>
							<ml:EncodedLabel id="m_cctname" Runat="server"></ml:EncodedLabel><BR>
							<BR>
						</TD>
					</TR>
					<TR>
						<TD>
							<ml:commondatagrid id="m_programsDG" runat="server" Width="100%" EnableViewState="False" AllowSorting="True" CssClass="DataGrid" AutoGenerateColumns="False" CellPadding="2" OnItemDataBound="m_programsDG_ItemDataBound">
								<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
								<HeaderStyle CssClass="GridHeader"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn ItemStyle-Width="30">
										<HeaderTemplate>
											<input type="checkbox" id="chkCheckAll" name="chkCheckAll" onclick="checkAll();">
										</HeaderTemplate>
										<ItemTemplate>
											<input id="templateid" runat="server" type="checkbox" class="templateid">
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn ItemStyle-Width="30">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
                                            <asp:LinkButton ID="editLink" runat="server">edit</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn ItemStyle-Width="60" DataField="Type" HeaderText="Type"></asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Enabled?" ItemStyle-Width="30">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<ml:EncodedLabel id="IsEnabledOverrideBit" runat="server">
												<%#AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Enabled" ))%>
											</ml:EncodedLabel>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="lLpTemplateNm" SortExpression="lLpTemplateNm" HeaderText="Program Name"></asp:BoundColumn>
								</Columns>
							</ml:commondatagrid></TD>
					</TR>
					<TR>
						<TD><SPAN onclick="if( children[ 0 ].disabled == false ) return validateCheckList();">
								<asp:Button id="deleteButton" onclick="OnDeleteClick" Runat="server" Text="Delete Selected Loan Programs"></asp:Button></SPAN>
							<INPUT id="close" onclick="onClosePopup();" type="button" value="Close" Runat="server">
						</TD>
					</TR>
				</asp:Panel></TBODY>
			</table>
		</form>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
	</body>
</HTML>
