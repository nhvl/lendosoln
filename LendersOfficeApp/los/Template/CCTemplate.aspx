<%@ Register TagPrefix="uc1" TagName="CCTemplate" Src="CCTemplate.ascx" %>
<%@ Page language="c#" Codebehind="CCTemplate.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.CCTemplate" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>CCTemplate</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout">
    <script language=javascript>
    <!--
      var first = true;
      var ctrlName = 'CCTemplate1_';
      function _init() {
        resize(800, 1000);
        if (document.getElementById('CCTemplate1_cCcTemplateNm'))
            document.getElementById('CCTemplate1_cCcTemplateNm').focus();

        if (first) {
            first = false;
          
                addEventHandler(document.getElementById(ctrlName + 'cGfeVersion_1'), "click", launchOtherVersion, false);
        }
        window.scrollTo(0, 0);
      }

      function OnPaidChecking(obj) {
          if (obj.id == "CCTemplate1_cApprFPaid" && obj.checked)
              document.getElementById("CCTemplate1_cApprFProps_ctrl_Poc_chk").checked = true;
          if (obj.id == "CCTemplate1_cCrFPaid" && obj.checked)
              document.getElementById("CCTemplate1_cCrFProps_ctrl_Poc_chk").checked = true;
          if (obj.id == "CCTemplate1_cProcFPaid" && obj.checked)
              document.getElementById("CCTemplate1_cProcFProps_ctrl_Poc_chk").checked = true;
      }

      function OnPocCheck(obj) {
          if (obj.id == "CCTemplate1_cApprFProps_ctrl_Poc_chk" && obj.checked)
              document.getElementById("CCTemplate1_cApprFPaid").checked = true;
          if (obj.id == "CCTemplate1_cCrFProps_ctrl_Poc_chk" && obj.checked)
              document.getElementById("CCTemplate1_cCrFPaid").checked = true;
          if (obj.id == "CCTemplate1_cProcFProps_ctrl_Poc_chk" && obj.checked)
              document.getElementById("CCTemplate1_cProcFPaid").checked = true;
      }

      function launchOtherVersion() {
        var choice = ConfirmSave();
        if (choice == 2) { // cancel
          document.getElementById(ctrlName + 'cGfeVersion_1').checked = false;
          document.getElementById(ctrlName + 'cGfeVersion_0').checked = true;
        }
        else if (choice == 6) { // yes
          document.getElementById(ctrlName + 'm_hiddenChoice').value = "Save";
          document.forms[0].submit();
        }
        else { // no
          document.getElementById(ctrlName + 'm_hiddenChoice').value = "Don't Save";
          document.forms[0].submit();
        }
      }
    //-->
    </script>
    
    <form id="CCTemplate" method="post" runat="server">
      <uc1:CCTemplate id="CCTemplate1" runat="server"></uc1:CCTemplate>
      <uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
  </body>
</HTML>
