using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOfficeApp.los.common;
using LendersOfficeApp.los.LegalForm;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.AntiXss;
using System.Collections.Generic;
using System.Text;
using DataAccess.ClosingCostAutomation;
using LendersOfficeApp.ObjLib.Licensing;
using System.Web.UI;

namespace LendersOfficeApp.Template
{


	/// <summary>
	///		Summary description for CCTemplate.
	/// </summary>
	public partial  class CCTemplateGFE2010 : System.Web.UI.UserControl
	{

        public bool IsFeeService { get; set; }

        #region Protected member variables

		protected System.Web.UI.WebControls.Button Button1;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cLOrigFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cLDiscntProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cLCreditProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cApprFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cCrFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cInspectFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cMBrokFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cTxServFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cProcFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cUwFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cWireFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn c800U1FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn c800U2FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn c800U3FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn c800U4FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn c800U5FProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cIPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cMipPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cHazInsPiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn c904PiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cVaFfProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn c900U1PiaProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cHazInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cMInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cSchoolTxRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cRealETxRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cFloodInsRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn c1006RsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn c1007RsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU3RsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU4RsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cAggregateAdjRsrvProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cEscrowFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cDocPrepFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cNotaryFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cAttorneyFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cTitleInsFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU1TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU2TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU3TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU4TcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cRecFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cCountyRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cStateRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU1GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU2GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU3GovRtcProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cPestInspectFProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU1ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU2ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU3ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU4ScProps_ctrl;
        protected LendersOfficeApp.los.LegalForm.GoodFaithEstimate2010RightColumn cU5ScProps_ctrl;
		private LosConvert m_convertLos;
        #endregion
        private void InitItemProps( GoodFaithEstimate2010RightColumn ctrl, int props )
        {
            ctrl.Apr = LosConvert.GfeItemProps_Apr( props );
            ctrl.ToBrok = LosConvert.GfeItemProps_ToBr( props );
            ctrl.PdByT = LosConvert.GfeItemProps_Payer( props );
			ctrl.Fha = LosConvert.GfeItemProps_FhaAllow( props );
            ctrl.Poc = LosConvert.GfeItemProps_Poc(props);
            ctrl.Dflp = LosConvert.GfeItemProps_Dflp(props);
            ctrl.BF = LosConvert.GfeItemProps_BF(props);
            ctrl.ThirdParty = LosConvert.GfeItemProps_PaidToThirdParty(props);
            ctrl.Affiliate = LosConvert.GfeItemProps_ThisPartyIsAffiliate(props);
        }

        private Guid m_closingCostID 
        {
            get { return RequestHelper.GetGuid("ccid"); }
        }

		private String m_dupClosingCostID = RequestHelper.GetSafeQueryString("ccid2");

		private Guid m_brokerID 
		{
			get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
		}

        private int RetrieveItemProps( GoodFaithEstimate2010RightColumn ctrl )
        {
            return LosConvert.GfeItemProps_Pack( ctrl.Apr, ctrl.ToBrok, ctrl.PdByT, ctrl.Fha, ctrl.Poc, ctrl.Dflp, ctrl.GBF, ctrl.BF, ctrl.ThirdParty, ctrl.Affiliate, ctrl.QmWarning_Hidden );
        }

        public void LoadData()
        {
			CCcTemplateData loanData = new CCcTemplateData( PrincipalFactory.CurrentPrincipal.BrokerId, m_closingCostID );
            m_convertLos = loanData.m_convertLos;
            loanData.InitLoad();


            int draftId = RequestHelper.GetInt("draftid", -1);
            int releaseId = RequestHelper.GetInt("RID", -1);
            AutomationPanel.Visible = false;

             BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
             if (db.AreAutomatedClosingCostPagesVisible)
             {
                 cGfeVersion.Items[0].Enabled = false;
             }

            if (draftId >= 0 || releaseId >= 0)
            {
                ClosingCostAutomationStorage storage = new ClosingCostAutomationStorage(m_brokerID, PrincipalFactory.CurrentPrincipal.UserId);
                int id = draftId >= 0 ? draftId : releaseId;
                BrokerAutomationFields bauf = null;

                if (draftId >= 0)
                {
                    bauf = storage.GetAutomationConditionForDraft(id);
                }
                else
                {
                    var a = storage.GetReleaseVersion();
                    if (a.Id == releaseId)
                    {
                        bauf = a.TemplateConditions;
                    }
                }



                if (bauf != null)
                {
                    AutomationPanel.Visible = true;
                    editcondition.Visible = false;

                    if (bauf.GetNumberOfAutomationFieldsUsed() != 0)
                    {
                        string js = string.Format("openConditionTemplateEditor('{0}','{1}','{2}', {3});", AspxTools.JsNumeric(draftId), AspxTools.JsStringUnquoted(m_closingCostID.ToString("N")), Uri.EscapeDataString(loanData.cCcTemplateNm), AspxTools.JsGetClientIdString(ConditionTemplateKey));
                        editcondition.Attributes.Add("onclick", js);
                        editcondition.Visible = true;
                        Condition.Text = bauf.GetDescriptionFor(m_closingCostID);
                    }
                    else
                    {
                        Condition.Text = "The condition for this template cannot be created until automation fields are selected.";
                    }

                    editcondition.Visible = draftId >= 0;
                }
                else
                {
                    AutomationPanel.Visible = false;
                    editcondition.Visible = false;
                }                
            }
         
            
            cTitleInsurancePolicyTLabel.Visible = db.AreAutomatedClosingCostPagesVisible;
            cTitleInsurancePolicyT.Visible = db.AreAutomatedClosingCostPagesVisible;
            SavedName.Value = loanData.cCcTemplateNm;
            
            cGfeVersion.SelectedValue = E_GfeVersion.Gfe2010.ToString();
            cDaysInYr.Text = loanData.cDaysInYr_rep;
            cGfeUsePaidToFromOfficialContact.Checked = loanData.cGfeUsePaidToFromOfficialContact;
			
            //if we are duplicating a template, clear the name field to force user to enter a new one
			if((m_dupClosingCostID != "") && (m_dupClosingCostID != null))
				cCcTemplateNm.Text = "";
			else
				cCcTemplateNm.Text = loanData.cCcTemplateNm;

			// cRecFPc, cCountyRtcDesc, and cStateRtcDesc added per OPM 8251
            cRecFProps_ctrl.SetPaidTo(() => loanData.cRecFDesc);
            cCountyRtcProps_ctrl.SetPaidTo(()=> loanData.cCountyRtcDesc);
            cStateRtcProps_ctrl.SetPaidTo(()=> loanData.cStateRtcDesc);

            c800U5F.Text = loanData.c800U5F_rep;
            c800U5FDesc.Text = loanData.c800U5FDesc;

            c800U4F.Text = loanData.c800U4F_rep;
            c800U4FDesc.Text = loanData.c800U4FDesc;

            c800U3F.Text = loanData.c800U3F_rep;
            c800U3FDesc.Text = loanData.c800U3FDesc;

            c800U2F.Text = loanData.c800U2F_rep;
            c800U2FDesc.Text = loanData.c800U2FDesc;

            c800U1F.Text = loanData.c800U1F_rep;
            c800U1FDesc.Text = loanData.c800U1FDesc;

            cFloodCertificationF.Text = loanData.cFloodCertificationF_rep;
            cWireF.Text = loanData.cWireF_rep;
            cUwF.Text = loanData.cUwF_rep;
            cProcF.Text = loanData.cProcF_rep;
            cProcFProps_ctrl.Paid = loanData.cProcFPaid;
            cTxServF.Text = loanData.cTxServF_rep;
            cMBrokFMb.Text = loanData.cMBrokFMb_rep;
            cMBrokFPc.Text = loanData.cMBrokFPc_rep;
            Tools.SetDropDownListValue(cMBrokFBaseT, loanData.cMBrokFBaseT);
            cInspectF.Text = loanData.cInspectF_rep;
            cCrF.Text = loanData.cCrF_rep;
            cCrFProps_ctrl.Paid = loanData.cCrFPaid;
            cApprF.Text = loanData.cApprF_rep;
            cApprFProps_ctrl.Paid = loanData.cApprFPaid;
            cLDiscntFMb.Text = loanData.cLDiscntFMb_rep;
            cLDiscntPc.Text = loanData.cLDiscntPc_rep;
            Tools.SetDropDownListValue(cLDiscntBaseT, loanData.cLDiscntBaseT);
            cLOrigFMb.Text = loanData.cLOrigFMb_rep;
            cLOrigFPc.Text = loanData.cLOrigFPc_rep;
            cIPiaDy.Text = loanData.cIPiaDy_rep;
            c900U1Pia.Text = loanData.c900U1Pia_rep;
            c900U1PiaDesc.Text = loanData.c900U1PiaDesc;
            c904Pia.Text = loanData.c904Pia_rep;
            c904PiaDesc.Text = loanData.c904PiaDesc;
            cHazInsPiaMon.Text = loanData.cHazInsPiaMon_rep;
            //Tools.SetDropDownListValue(cProHazInsT, loanData.cProHazInsT);
            
            //cProHazInsR.Text = loanData.cProHazInsR_rep;
            //cProHazInsMb.Text = loanData.cProHazInsMb_rep;
            //cProRealETxR.Text = loanData.cProRealETxR_rep;
            //cProRealETxMb.Text = loanData.cProRealETxMb_rep;
            //Tools.SetDropDownListValue(cProRealETxT, loanData.cProRealETxT);

            //c1007ProHExp.Text = loanData.c1007ProHExp_rep;
            c1007RsrvMon.Text = loanData.c1007RsrvMon_rep;
            c1007ProHExpDesc.Text = loanData.c1007ProHExpDesc;

            //c1006ProHExp.Text = loanData.c1006ProHExp_rep;
            c1006RsrvMon.Text = loanData.c1006RsrvMon_rep;
            c1006ProHExpDesc.Text = loanData.c1006ProHExpDesc;

            //cProFloodIns.Text = loanData.cProFloodIns_rep;
            cFloodInsRsrvMon.Text = loanData.cFloodInsRsrvMon_rep;
//            cProRealETx.Text = loanData.cProRealETx_rep; // 11/9/2006 dd - Remove the tax rate from CC template.
            cRealETxRsrvMon.Text = loanData.cRealETxRsrvMon_rep;
            //cProSchoolTx.Text = loanData.cProSchoolTx_rep;
            cSchoolTxRsrvMon.Text = loanData.cSchoolTxRsrvMon_rep; //YES
            cMInsRsrvMon.Text = loanData.cMInsRsrvMon_rep;
            cHazInsRsrvMon.Text = loanData.cHazInsRsrvMon_rep;

            cU3RsrvDesc.Text = loanData.cU3RsrvDesc;
            cU3RsrvMon.Text = loanData.cU3RsrvMon_rep;

            cU4RsrvDesc.Text = loanData.cU4RsrvDesc;
            cU4RsrvMon.Text = loanData.cU4RsrvMon_rep;

            cU4Tc.Text = loanData.cU4Tc_rep;
            cU4TcDesc.Text = loanData.cU4TcDesc;

            cU3Tc.Text = loanData.cU3Tc_rep;
            cU3TcDesc.Text = loanData.cU3TcDesc;

            cU2Tc.Text = loanData.cU2Tc_rep;        
            cU2TcDesc.Text = loanData.cU2TcDesc;

            cU1Tc.Text = loanData.cU1Tc_rep;
            cU1TcDesc.Text = loanData.cU1TcDesc;

            //cTitleInsF.Text = loanData.cTitleInsF_rep;
            cTitleInsFPc.Text = loanData.cTitleInsFPc_rep;
            Tools.SetDropDownListValue(cTitleInsFBaseT, loanData.cTitleInsFBaseT);
            cTitleInsFMb.Text = loanData.cTitleInsFMb_rep;
            cTitleInsFProps_ctrl.SetPaidTo(()=> loanData.cTitleInsFTable);

            //cOwnerTitleInsF.Text = loanData.cOwnerTitleInsF_rep;
            Tools.SetDropDownListValue(cOwnerTitleInsFBaseT, loanData.cOwnerTitleInsFBaseT);
            cOwnerTitleInsFMb.Text = loanData.cOwnerTitleInsFMb_rep;
            cOwnerTitleInsFPc.Text = loanData.cOwnerTitleInsFPc_rep;

            cAttorneyF.Text = loanData.cAttorneyF_rep;
            cNotaryF.Text = loanData.cNotaryF_rep;
            cDocPrepF.Text = loanData.cDocPrepF_rep;
            
            //cEscrowF.Text = loanData.cEscrowF_rep;
            cEscrowFPc.Text = loanData.cEscrowFPc_rep;
            Tools.SetDropDownListValue(cEscrowFBaseT, loanData.cEscrowFBaseT);
            cEscrowFMb.Text = loanData.cEscrowFMb_rep;

            cEscrowFProps_ctrl.SetPaidTo(()=> loanData.cEscrowFTable);

            cU3GovRtcMb.Text = loanData.cU3GovRtcMb_rep;
            Tools.SetDropDownListValue(cU3GovRtcBaseT, loanData.cU3GovRtcBaseT);
            cU3GovRtcPc.Text = loanData.cU3GovRtcPc_rep;
            cU3GovRtcDesc.Text = loanData.cU3GovRtcDesc;

            cU2GovRtcMb.Text = loanData.cU2GovRtcMb_rep;
            Tools.SetDropDownListValue(cU2GovRtcBaseT, loanData.cU2GovRtcBaseT);
            cU2GovRtcPc.Text = loanData.cU2GovRtcPc_rep;
            cU2GovRtcDesc.Text = loanData.cU2GovRtcDesc;

            cU1GovRtcMb.Text = loanData.cU1GovRtcMb_rep;
            Tools.SetDropDownListValue(cU1GovRtcBaseT, loanData.cU1GovRtcBaseT);
            cU1GovRtcPc.Text = loanData.cU1GovRtcPc_rep;
            cU1GovRtcDesc.Text = loanData.cU1GovRtcDesc;

            cStateRtcMb.Text = loanData.cStateRtcMb_rep;
            Tools.SetDropDownListValue(cStateRtcBaseT, loanData.cStateRtcBaseT);
            cStateRtcPc.Text = loanData.cStateRtcPc_rep;
            cCountyRtcMb.Text = loanData.cCountyRtcMb_rep;
            Tools.SetDropDownListValue(cCountyRtcBaseT, loanData.cCountyRtcBaseT);
            cCountyRtcPc.Text = loanData.cCountyRtcPc_rep; 
            cRecFMb.Text = loanData.cRecFMb_rep;
            Tools.SetDropDownListValue(cRecBaseT, loanData.cRecBaseT);
            cRecFPc.Text = loanData.cRecFPc_rep;
            cRecFLckd.Checked = loanData.cRecFLckd;
            cRecDeed.Text = loanData.cRecDeed_rep;
            cRecMortgage.Text = loanData.cRecMortgage_rep;
            cRecRelease.Text = loanData.cRecRelease_rep;

            cU5Sc.Text = loanData.cU5Sc_rep;
            cU5ScDesc.Text = loanData.cU5ScDesc;

            cU4Sc.Text = loanData.cU4Sc_rep;
            cU4ScDesc.Text = loanData.cU4ScDesc;

            cU3Sc.Text = loanData.cU3Sc_rep;
            cU3ScDesc.Text = loanData.cU3ScDesc;

            cU2Sc.Text = loanData.cU2Sc_rep;
            cU2ScDesc.Text = loanData.cU2ScDesc;

            cU1Sc.Text = loanData.cU1Sc_rep;
            cU1ScDesc.Text = loanData.cU1ScDesc;

            cPestInspectF.Text = loanData.cPestInspectF_rep;
            InitItemProps( cLOrigFProps_ctrl, loanData.cLOrigFProps );
            InitItemProps( cLDiscntProps_ctrl, loanData.cLDiscntProps );
            InitItemProps( cLCreditProps_ctrl, loanData.cLCreditProps);
            InitItemProps( cApprFProps_ctrl, loanData.cApprFProps );
            InitItemProps( cCrFProps_ctrl, loanData.cCrFProps );
            InitItemProps( cInspectFProps_ctrl, loanData.cInspectFProps );
            InitItemProps( cMBrokFProps_ctrl, loanData.cMBrokFProps );
            InitItemProps( cTxServFProps_ctrl, loanData.cTxServFProps );
            InitItemProps( cProcFProps_ctrl, loanData.cProcFProps );
            InitItemProps( cFloodCertificationFProps_ctrl, loanData.cFloodCertificationFProps );
            InitItemProps( cUwFProps_ctrl, loanData.cUwFProps );
            InitItemProps( cWireFProps_ctrl, loanData.cWireFProps );
            InitItemProps( c800U1FProps_ctrl, loanData.c800U1FProps );
            InitItemProps( c800U2FProps_ctrl, loanData.c800U2FProps );
            InitItemProps( c800U3FProps_ctrl, loanData.c800U3FProps );	
            InitItemProps( c800U4FProps_ctrl, loanData.c800U4FProps );
            InitItemProps( c800U5FProps_ctrl, loanData.c800U5FProps );
            InitItemProps( cIPiaProps_ctrl, loanData.cIPiaProps );
            InitItemProps( cMipPiaProps_ctrl, loanData.cMipPiaProps );
            InitItemProps( cHazInsPiaProps_ctrl, loanData.cHazInsPiaProps );
            InitItemProps( c904PiaProps_ctrl, loanData.c904PiaProps );
            InitItemProps( cVaFfProps_ctrl, loanData.cVaFfProps );
            InitItemProps( c900U1PiaProps_ctrl, loanData.c900U1PiaProps );
            InitItemProps( cHazInsRsrvProps_ctrl, loanData.cHazInsRsrvProps );
            InitItemProps( cMInsRsrvProps_ctrl, loanData.cMInsRsrvProps );
            InitItemProps( cSchoolTxRsrvProps_ctrl, loanData.cSchoolTxRsrvProps );
            InitItemProps( cRealETxRsrvProps_ctrl, loanData.cRealETxRsrvProps );
            InitItemProps( cFloodInsRsrvProps_ctrl, loanData.cFloodInsRsrvProps );
            InitItemProps( c1006RsrvProps_ctrl, loanData.c1006RsrvProps );
            InitItemProps( c1007RsrvProps_ctrl, loanData.c1007RsrvProps );
            InitItemProps( cU3RsrvProps_ctrl, loanData.cU3RsrvProps );
            InitItemProps( cU4RsrvProps_ctrl, loanData.cU4RsrvProps );
            InitItemProps( cAggregateAdjRsrvProps_ctrl, loanData.cAggregateAdjRsrvProps );
            InitItemProps( cEscrowFProps_ctrl, loanData.cEscrowFProps );
            InitItemProps( cDocPrepFProps_ctrl, loanData.cDocPrepFProps );
            InitItemProps( cNotaryFProps_ctrl, loanData.cNotaryFProps );
            InitItemProps( cAttorneyFProps_ctrl, loanData.cAttorneyFProps );
            InitItemProps( cOwnerTitleInsFProps_ctrl, loanData.cOwnerTitleInsProps);
            InitItemProps( cTitleInsFProps_ctrl, loanData.cTitleInsFProps );
            InitItemProps( cU1TcProps_ctrl, loanData.cU1TcProps );
            InitItemProps( cU2TcProps_ctrl, loanData.cU2TcProps );
            InitItemProps( cU3TcProps_ctrl, loanData.cU3TcProps );
            InitItemProps( cU4TcProps_ctrl, loanData.cU4TcProps );
            InitItemProps( cRecFProps_ctrl, loanData.cRecFProps );
            InitItemProps( cCountyRtcProps_ctrl, loanData.cCountyRtcProps );
            InitItemProps( cStateRtcProps_ctrl, loanData.cStateRtcProps );
            InitItemProps( cU1GovRtcProps_ctrl, loanData.cU1GovRtcProps );
            InitItemProps( cU2GovRtcProps_ctrl, loanData.cU2GovRtcProps );
            InitItemProps( cU3GovRtcProps_ctrl, loanData.cU3GovRtcProps );
            InitItemProps( cPestInspectFProps_ctrl, loanData.cPestInspectFProps );
            InitItemProps( cU1ScProps_ctrl, loanData.cU1ScProps );
            InitItemProps( cU2ScProps_ctrl, loanData.cU2ScProps );
            InitItemProps( cU3ScProps_ctrl, loanData.cU3ScProps );
            InitItemProps( cU4ScProps_ctrl, loanData.cU4ScProps );
            InitItemProps( cU5ScProps_ctrl, loanData.cU5ScProps );

            cApprFProps_ctrl.SetPaidTo(()=> loanData.cApprFPaidTo);
            cCrFProps_ctrl.SetPaidTo(()=> loanData.cCrFPaidTo);
            cTxServFProps_ctrl.SetPaidTo(()=> loanData.cTxServFPaidTo);
            cFloodCertificationFProps_ctrl.SetPaidTo(()=> loanData.cFloodCertificationFPaidTo);
            cInspectFProps_ctrl.SetPaidTo(()=> loanData.cInspectFPaidTo);
            cProcFProps_ctrl.SetPaidTo(()=> loanData.cProcFPaidTo);
            cUwFProps_ctrl.SetPaidTo(()=> loanData.cUwFPaidTo);
            cWireFProps_ctrl.SetPaidTo(()=> loanData.cWireFPaidTo);
            c800U1FProps_ctrl.SetPaidTo(()=> loanData.c800U1FPaidTo);
            c800U2FProps_ctrl.SetPaidTo(()=> loanData.c800U2FPaidTo);
            c800U3FProps_ctrl.SetPaidTo(()=> loanData.c800U3FPaidTo);
            c800U4FProps_ctrl.SetPaidTo(()=> loanData.c800U4FPaidTo);
            c800U5FProps_ctrl.SetPaidTo(()=> loanData.c800U5FPaidTo);
            cOwnerTitleInsFProps_ctrl.SetPaidTo(()=> loanData.cOwnerTitleInsPaidTo);
            cDocPrepFProps_ctrl.SetPaidTo(()=> loanData.cDocPrepFPaidTo);
            cNotaryFProps_ctrl.SetPaidTo(()=> loanData.cNotaryFPaidTo);
            cAttorneyFProps_ctrl.SetPaidTo(()=> loanData.cAttorneyFPaidTo);
            cU1TcProps_ctrl.SetPaidTo(()=> loanData.cU1TcPaidTo);
            cU2TcProps_ctrl.SetPaidTo(()=> loanData.cU2TcPaidTo);
            cU3TcProps_ctrl.SetPaidTo(()=> loanData.cU3TcPaidTo);
            cU4TcProps_ctrl.SetPaidTo(()=> loanData.cU4TcPaidTo);
            cU1GovRtcProps_ctrl.SetPaidTo(()=> loanData.cU1GovRtcPaidTo);
            cU2GovRtcProps_ctrl.SetPaidTo(()=> loanData.cU2GovRtcPaidTo);
            cU3GovRtcProps_ctrl.SetPaidTo(()=> loanData.cU3GovRtcPaidTo);
            cPestInspectFProps_ctrl.SetPaidTo(()=> loanData.cPestInspectPaidTo);
            cU1ScProps_ctrl.SetPaidTo(()=> loanData.cU1ScPaidTo);
            cU2ScProps_ctrl.SetPaidTo(()=> loanData.cU2ScPaidTo);
            cU3ScProps_ctrl.SetPaidTo(()=> loanData.cU3ScPaidTo);
            cU4ScProps_ctrl.SetPaidTo(()=> loanData.cU4ScPaidTo);
            cU5ScProps_ctrl.SetPaidTo(()=> loanData.cU5ScPaidTo);
            cHazInsPiaProps_ctrl.SetPaidTo(()=> loanData.cHazInsPiaPaidTo);
            cMipPiaProps_ctrl.SetPaidTo(()=> loanData.cMipPiaPaidTo);
            cVaFfProps_ctrl.SetPaidTo(()=> loanData.cVaFfPaidTo);

            c800U1FProps_ctrl.Page2Selection = loanData.c800U1FGfeSection;
            c800U2FProps_ctrl.Page2Selection = loanData.c800U2FGfeSection;
            c800U3FProps_ctrl.Page2Selection = loanData.c800U3FGfeSection;
            c800U4FProps_ctrl.Page2Selection = loanData.c800U4FGfeSection;
            c800U5FProps_ctrl.Page2Selection = loanData.c800U5FGfeSection;
            cEscrowFProps_ctrl.Page2Selection = loanData.cEscrowFGfeSection;
            cDocPrepFProps_ctrl.Page2Selection = loanData.cDocPrepFGfeSection;
            cNotaryFProps_ctrl.Page2Selection = loanData.cNotaryFGfeSection;
            cAttorneyFProps_ctrl.Page2Selection = loanData.cAttorneyFGfeSection;
            cTitleInsFProps_ctrl.Page2Selection = loanData.cTitleInsFGfeSection;
            cU1TcProps_ctrl.Page2Selection = loanData.cU1TcGfeSection;
            cU2TcProps_ctrl.Page2Selection = loanData.cU2TcGfeSection;
            cU3TcProps_ctrl.Page2Selection = loanData.cU3TcGfeSection;
            cU4TcProps_ctrl.Page2Selection = loanData.cU4TcGfeSection;
            cU1GovRtcProps_ctrl.Page2Selection = loanData.cU1GovRtcGfeSection;
            cU2GovRtcProps_ctrl.Page2Selection = loanData.cU2GovRtcGfeSection;
            cU3GovRtcProps_ctrl.Page2Selection = loanData.cU3GovRtcGfeSection;
            cU1ScProps_ctrl.Page2Selection = loanData.cU1ScGfeSection;
            cU2ScProps_ctrl.Page2Selection = loanData.cU2ScGfeSection;
            cU3ScProps_ctrl.Page2Selection = loanData.cU3ScGfeSection;
            cU4ScProps_ctrl.Page2Selection = loanData.cU4ScGfeSection;
            cU5ScProps_ctrl.Page2Selection = loanData.cU5ScGfeSection;
            c904PiaProps_ctrl.Page2Selection = loanData.c904PiaGfeSection;
            c900U1PiaProps_ctrl.Page2Selection = loanData.c900U1PiaGfeSection;

           
            
            sGfeLenderCreditF.Text = "$0.00";
            sGfeLenderCreditFPc.Text = "0.000%";

            if (RequestHelper.GetBool("auto"))
            {
                trPricingEngine.Visible = true;
                //PricingEngineCostLabel.Visible = true;
                //cPricingEngineCostT.Visible = true;
                //cPricingEngineCreditTLabel.Visible = true;
                //cPricingEngineCreditT.Visible = true;
                cPricingEngineLimitCreditToTLabel.Visible = true;
                cPricingEngineLimitCreditToT.Visible = true;
            }

            //Tools.Bind_cPricingEngineCostT(cPricingEngineCostT);
            //Tools.Bind_cPricingEngineCreditT(cPricingEngineCreditT);
            //Tools.Bind_cTitleInsurancePolicy(cTitleInsurancePolicyT);
           
            //Tools.Bind_cPricingEngineLimitCreditToT(cPricingEngineLimitCreditToT);

            Tools.SetDropDownListValue(cPricingEngineCostT, loanData.cPricingEngineCostT);
            Tools.SetDropDownListValue(cPricingEngineCreditT, loanData.cPricingEngineCreditT);
            Tools.SetDropDownListValue(cTitleInsurancePolicyT, loanData.cTitleInsurancePolicyT);
            Tools.SetDropDownListValue(cPricingEngineLimitCreditToT, loanData.cPricingEngineLimitCreditToT);

            sGfeDiscountPointF.Text = "$0.00";
            sGfeDiscountPointFPc.Text = "0.000%";
            sGfeCreditLenderPaidItemF.Text = "$0.00";
            sGfeCreditLenderPaidItemT.SelectedIndex = 2;

            cGfeIsTPOTransaction.Checked = loanData.cGfeIsTPOTransaction;
			//if we are duplicating a template, save right away so if the user cancels, the duplicated data persists
			if((m_dupClosingCostID != "") && (m_dupClosingCostID != null))
				SaveData();
        }


        public void SaveData()
        {
			CCcTemplateData loanData;

		// we are duplicating an existing template, which is at this point essentially adding a new one
			if((m_dupClosingCostID != "") && (m_dupClosingCostID != null))
			{ 
				Guid m_newClosingCostID = new Guid(m_dupClosingCostID);
				m_convertLos = new LosConvert();
				loanData = new CCcTemplateData( m_brokerID, m_newClosingCostID );
			}
			else
			{
				m_convertLos = new LosConvert();
				loanData = new CCcTemplateData( m_brokerID, m_closingCostID);
			}
            
			loanData.InitSave();
			// cRecFPc, cCountyRtcDesc, and cStateRtcDesc added per OPM 8251


            int draftId = RequestHelper.GetInt("draftid", -1);
            if (draftId >= 0 && string.IsNullOrEmpty(ConditionTemplateKey.Value) == false)
            {
                ClosingCostAutomationStorage storage = new ClosingCostAutomationStorage(m_brokerID, PrincipalFactory.CurrentPrincipal.UserId);
                BrokerAutomationFields bauf = storage.GetAutomationConditionForDraft(draftId);

                string text = AutoExpiredTextCache.GetFromCache(ConditionTemplateKey.Value);
                if (text != null)
                {
                    //todo check the template belongs to rule xml.. should probably be in system rule, can include id in template to ensure in SP
                    AutoExpiredTextCache.ExpireCache(ConditionTemplateKey.Value);

                    var items = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<BareboneSelectedAutomationValues>>(text);
                    bauf.SetCondition(loanData.cCcTemplateId, items);
                    storage.SaveDraftAutomationConditions(bauf);
                    ConditionTemplateKey.Value = ""; //clear it out!

                    Condition.Text = bauf.GetDescriptionFor(loanData.cCcTemplateId);

                }
            }

            if (RequestHelper.GetBool("auto"))
            {

                loanData.cPricingEngineCostT = (E_cPricingEngineCostT)int.Parse(cPricingEngineCostT.Text);
                loanData.cPricingEngineCreditT = (E_cPricingEngineCreditT)int.Parse(cPricingEngineCreditT.Text);

                if (loanData.cPricingEngineCreditT == E_cPricingEngineCreditT._801LoanOriginationFee)
                {
                    loanData.cPricingEngineLimitCreditToT = E_cPricingEngineLimitCreditToT.OriginationCharges;
                }
                else
                {
                    loanData.cPricingEngineLimitCreditToT = (E_cPricingEngineLimitCreditToT)int.Parse(cPricingEngineLimitCreditToT.Text);
                }
            }

            loanData.cTitleInsurancePolicyT = (E_cTitleInsurancePolicyT)int.Parse(cTitleInsurancePolicyT.Text);  

            SavedName.Value = cCcTemplateNm.Text;
            loanData.cRecFDesc = cRecFProps_ctrl.PaidTo;
            loanData.cCountyRtcDesc = cCountyRtcProps_ctrl.PaidTo;
            loanData.cStateRtcDesc = cStateRtcProps_ctrl.PaidTo;

			loanData.cDaysInYr_rep = cDaysInYr.Text;
            loanData.cCcTemplateNm = cCcTemplateNm.Text;
            loanData.cGfeUsePaidToFromOfficialContact = cGfeUsePaidToFromOfficialContact.Checked;
            loanData.c800U5F_rep = c800U5F.Text;
            loanData.c800U5FDesc = c800U5FDesc.Text;
            loanData.c800U4F_rep = c800U4F.Text ;
            loanData.c800U4FDesc = c800U4FDesc.Text;
            loanData.c800U3F_rep = c800U3F.Text ;
            loanData.c800U3FDesc = c800U3FDesc.Text;
            loanData.c800U2F_rep = c800U2F.Text ;
            loanData.c800U2FDesc = c800U2FDesc.Text;
            loanData.c800U1F_rep = c800U1F.Text ;
            loanData.c800U1FDesc = c800U1FDesc.Text;
            loanData.cFloodCertificationF_rep = cFloodCertificationF.Text;
            loanData.cWireF_rep = cWireF.Text ;
            loanData.cUwF_rep = cUwF.Text ;
            loanData.cProcF_rep = cProcF.Text ;
            loanData.cProcFPaid = cProcFProps_ctrl.Paid;
            loanData.cTxServF_rep = cTxServF.Text ;
            loanData.cMBrokFMb_rep = cMBrokFMb.Text ;
            loanData.cMBrokFPc_rep = cMBrokFPc.Text ;
            loanData.cMBrokFBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cMBrokFBaseT);
            loanData.cInspectF_rep = cInspectF.Text;
            loanData.cCrF_rep = cCrF.Text;
            loanData.cCrFPaid = cCrFProps_ctrl.Paid;
            loanData.cApprF_rep = cApprF.Text;
            loanData.cApprFPaid = cApprFProps_ctrl.Paid;
            loanData.cLDiscntFMb_rep = cLDiscntFMb.Text;
            loanData.cLDiscntPc_rep = cLDiscntPc.Text;
            loanData.cLDiscntBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cLDiscntBaseT);
            loanData.cLOrigFMb_rep = cLOrigFMb.Text;
            loanData.cLOrigFPc_rep = cLOrigFPc.Text;
            loanData.cIPiaDy_rep = cIPiaDy.Text ;
            loanData.c900U1Pia_rep = c900U1Pia.Text ;
            loanData.c900U1PiaDesc = c900U1PiaDesc.Text;
            loanData.c904Pia_rep = c904Pia.Text ;
            loanData.c904PiaDesc = c904PiaDesc.Text;
            loanData.cHazInsPiaMon_rep = cHazInsPiaMon.Text ;
            //loanData.cProHazInsT = (E_PercentBaseT)Tools.GetDropDownListValue(cProHazInsT);
            //loanData.cProHazInsR_rep = cProHazInsR.Text ;
            //loanData.cProRealETxT = (E_PercentBaseT)Tools.GetDropDownListValue(cProRealETxT);
            //loanData.c1007ProHExp_rep = c1007ProHExp.Text ;
            loanData.c1007RsrvMon_rep = c1007RsrvMon.Text;
            loanData.c1007ProHExpDesc = c1007ProHExpDesc.Text;
            //loanData.c1006ProHExp_rep = c1006ProHExp.Text ;
            loanData.c1006RsrvMon_rep = c1006RsrvMon.Text;
            loanData.c1006ProHExpDesc = c1006ProHExpDesc.Text;
            if (BrokerDB.RetrieveById(m_brokerID).EnableAdditionalSection1000CustomFees)
            {
                loanData.cU3RsrvMon_rep = cU3RsrvMon.Text;
                loanData.cU3RsrvDesc = cU3RsrvDesc.Text;
                loanData.cU4RsrvMon_rep = cU4RsrvMon.Text;
                loanData.cU4RsrvDesc = cU4RsrvDesc.Text;
            }
            //loanData.cProFloodIns_rep = cProFloodIns.Text ;
            loanData.cFloodInsRsrvMon_rep = cFloodInsRsrvMon.Text;
            loanData.cRealETxRsrvMon_rep = cRealETxRsrvMon.Text;
            //loanData.cProSchoolTx_rep = cProSchoolTx.Text ;
            loanData.cSchoolTxRsrvMon_rep = cSchoolTxRsrvMon.Text;
            loanData.cMInsRsrvMon_rep = cMInsRsrvMon.Text;
            loanData.cHazInsRsrvMon_rep = cHazInsRsrvMon.Text;
            loanData.cU4Tc_rep = cU4Tc.Text;
            loanData.cU4TcDesc = cU4TcDesc.Text;
            loanData.cU3Tc_rep = cU3Tc.Text;
            loanData.cU3TcDesc = cU3TcDesc.Text;
            loanData.cU2Tc_rep = cU2Tc.Text;
            loanData.cU2TcDesc = cU2TcDesc.Text;
            loanData.cU1Tc_rep = cU1Tc.Text;
            loanData.cU1TcDesc = cU1TcDesc.Text;

            //loanData.cTitleInsF_rep = cTitleInsF.Text ;
            loanData.cTitleInsFPc_rep = cTitleInsFPc.Text;
            loanData.cTitleInsFMb_rep = cTitleInsFMb.Text;
            loanData.cTitleInsFBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cTitleInsFBaseT);

            loanData.cTitleInsFTable = cTitleInsFProps_ctrl.PaidTo;
            //loanData.cOwnerTitleInsF_rep = cOwnerTitleInsF.Text;
            loanData.cOwnerTitleInsFPc_rep = cOwnerTitleInsFPc.Text;
            loanData.cOwnerTitleInsFMb_rep = cOwnerTitleInsFMb.Text;
            loanData.cOwnerTitleInsFBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cOwnerTitleInsFBaseT);

            loanData.cAttorneyF_rep = cAttorneyF.Text;
            loanData.cNotaryF_rep = cNotaryF.Text ;
            loanData.cDocPrepF_rep = cDocPrepF.Text ;
            //loanData.cEscrowF_rep = cEscrowF.Text ;

            loanData.cEscrowFPc_rep = cEscrowFPc.Text;
            loanData.cEscrowFMb_rep = cEscrowFMb.Text;
            loanData.cEscrowFBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cEscrowFBaseT);


            loanData.cEscrowFTable = cEscrowFProps_ctrl.PaidTo;
            loanData.cU3GovRtcMb_rep = cU3GovRtcMb.Text ;
            loanData.cU3GovRtcBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cU3GovRtcBaseT);
            loanData.cU3GovRtcPc_rep = cU3GovRtcPc.Text ;
            loanData.cU3GovRtcDesc = cU3GovRtcDesc.Text;
            loanData.cU2GovRtcMb_rep = cU2GovRtcMb.Text ;
            loanData.cU2GovRtcBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cU2GovRtcBaseT);
            loanData.cU2GovRtcPc_rep = cU2GovRtcPc.Text ;
            loanData.cU2GovRtcDesc = cU2GovRtcDesc.Text;
            loanData.cU1GovRtcMb_rep = cU1GovRtcMb.Text ;
            loanData.cU1GovRtcBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cU1GovRtcBaseT);
            loanData.cU1GovRtcPc_rep = cU1GovRtcPc.Text ;
            loanData.cU1GovRtcDesc = cU1GovRtcDesc.Text;
            loanData.cStateRtcMb_rep = cStateRtcMb.Text ;
            loanData.cStateRtcBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cStateRtcBaseT);
            loanData.cStateRtcPc_rep = cStateRtcPc.Text ;
            loanData.cCountyRtcMb_rep = cCountyRtcMb.Text ;
            loanData.cCountyRtcBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cCountyRtcBaseT);
            loanData.cCountyRtcPc_rep = cCountyRtcPc.Text ;
            loanData.cRecFMb_rep = cRecFMb.Text ;
            loanData.cRecBaseT = (E_PercentBaseT)Tools.GetDropDownListValue(cRecBaseT);
            loanData.cRecFPc_rep = cRecFPc.Text ;
            loanData.cRecFLckd = cRecFLckd.Checked;
            loanData.cRecDeed_rep = cRecDeed.Text;
            loanData.cRecMortgage_rep = cRecMortgage.Text;
            loanData.cRecRelease_rep = cRecRelease.Text;
            loanData.cU5Sc_rep = cU5Sc.Text ;
            loanData.cU5ScDesc = cU5ScDesc.Text;
            loanData.cU4Sc_rep = cU4Sc.Text ;
            loanData.cU4ScDesc = cU4ScDesc.Text;
            loanData.cU3Sc_rep = cU3Sc.Text ;
            loanData.cU3ScDesc = cU3ScDesc.Text;
            loanData.cU2Sc_rep = cU2Sc.Text ;
            loanData.cU2ScDesc = cU2ScDesc.Text;
            loanData.cU1Sc_rep = cU1Sc.Text ;
            loanData.cU1ScDesc = cU1ScDesc.Text;
            loanData.cPestInspectF_rep = cPestInspectF.Text ;
            //loanData.cProHazInsMb_rep = cProHazInsMb.Text;
            //loanData.cProRealETxR_rep = cProRealETxR.Text;
            //loanData.cProRealETxMb_rep = cProRealETxMb.Text;
            
            loanData.cLOrigFProps = RetrieveItemProps( cLOrigFProps_ctrl );
            loanData.cLDiscntProps = RetrieveItemProps( cLDiscntProps_ctrl );
            loanData.cLCreditProps = RetrieveItemProps(cLCreditProps_ctrl);
            loanData.cApprFProps = RetrieveItemProps( cApprFProps_ctrl );
            loanData.cCrFProps = RetrieveItemProps( cCrFProps_ctrl );
            loanData.cInspectFProps = RetrieveItemProps( cInspectFProps_ctrl );
            loanData.cMBrokFProps = RetrieveItemProps( cMBrokFProps_ctrl );
            loanData.cTxServFProps = RetrieveItemProps( cTxServFProps_ctrl );
            loanData.cProcFProps = RetrieveItemProps( cProcFProps_ctrl );
            loanData.cUwFProps = RetrieveItemProps(cUwFProps_ctrl);
            loanData.cFloodCertificationFProps = RetrieveItemProps(cFloodCertificationFProps_ctrl);
            loanData.cWireFProps = RetrieveItemProps(cWireFProps_ctrl);
            loanData.c800U1FProps = RetrieveItemProps( c800U1FProps_ctrl );
            loanData.c800U2FProps = RetrieveItemProps( c800U2FProps_ctrl );
            loanData.c800U3FProps = RetrieveItemProps( c800U3FProps_ctrl );	
            loanData.c800U4FProps = RetrieveItemProps( c800U4FProps_ctrl );
            loanData.c800U5FProps = RetrieveItemProps( c800U5FProps_ctrl );
            loanData.cIPiaProps = RetrieveItemProps( cIPiaProps_ctrl );
            loanData.cMipPiaProps = RetrieveItemProps( cMipPiaProps_ctrl );
            loanData.cHazInsPiaProps = RetrieveItemProps( cHazInsPiaProps_ctrl );
            loanData.c904PiaProps = RetrieveItemProps( c904PiaProps_ctrl );
            loanData.cVaFfProps = RetrieveItemProps( cVaFfProps_ctrl );
            loanData.c900U1PiaProps = RetrieveItemProps( c900U1PiaProps_ctrl );
            loanData.cHazInsRsrvProps = RetrieveItemProps( cHazInsRsrvProps_ctrl );
            loanData.cMInsRsrvProps = RetrieveItemProps( cMInsRsrvProps_ctrl );
            loanData.cSchoolTxRsrvProps = RetrieveItemProps( cSchoolTxRsrvProps_ctrl );
            loanData.cRealETxRsrvProps = RetrieveItemProps( cRealETxRsrvProps_ctrl );
            loanData.cFloodInsRsrvProps = RetrieveItemProps( cFloodInsRsrvProps_ctrl );
            loanData.c1006RsrvProps = RetrieveItemProps( c1006RsrvProps_ctrl );
            loanData.c1007RsrvProps = RetrieveItemProps( c1007RsrvProps_ctrl );
            if (BrokerDB.RetrieveById(m_brokerID).EnableAdditionalSection1000CustomFees)
            {
                loanData.cU3RsrvProps = RetrieveItemProps( cU3RsrvProps_ctrl );
                loanData.cU4RsrvProps = RetrieveItemProps( cU4RsrvProps_ctrl );
            }
            loanData.cAggregateAdjRsrvProps = RetrieveItemProps( cAggregateAdjRsrvProps_ctrl );
            loanData.cEscrowFProps = RetrieveItemProps( cEscrowFProps_ctrl );
            loanData.cDocPrepFProps = RetrieveItemProps( cDocPrepFProps_ctrl );
            loanData.cNotaryFProps = RetrieveItemProps( cNotaryFProps_ctrl );
            loanData.cAttorneyFProps = RetrieveItemProps( cAttorneyFProps_ctrl );
            loanData.cOwnerTitleInsProps = RetrieveItemProps(cOwnerTitleInsFProps_ctrl);
            loanData.cTitleInsFProps = RetrieveItemProps(cTitleInsFProps_ctrl);
            loanData.cU1TcProps = RetrieveItemProps( cU1TcProps_ctrl );
            loanData.cU2TcProps = RetrieveItemProps( cU2TcProps_ctrl );
            loanData.cU3TcProps = RetrieveItemProps( cU3TcProps_ctrl );
            loanData.cU4TcProps = RetrieveItemProps( cU4TcProps_ctrl );
            loanData.cRecFProps = RetrieveItemProps( cRecFProps_ctrl );
            loanData.cCountyRtcProps = RetrieveItemProps( cCountyRtcProps_ctrl );
            loanData.cStateRtcProps = RetrieveItemProps( cStateRtcProps_ctrl );
            loanData.cU1GovRtcProps = RetrieveItemProps( cU1GovRtcProps_ctrl );
            loanData.cU2GovRtcProps = RetrieveItemProps( cU2GovRtcProps_ctrl );
            loanData.cU3GovRtcProps = RetrieveItemProps( cU3GovRtcProps_ctrl );
            loanData.cPestInspectFProps = RetrieveItemProps( cPestInspectFProps_ctrl );
            loanData.cU1ScProps = RetrieveItemProps( cU1ScProps_ctrl );
            loanData.cU2ScProps = RetrieveItemProps( cU2ScProps_ctrl );
            loanData.cU3ScProps = RetrieveItemProps( cU3ScProps_ctrl );
            loanData.cU4ScProps = RetrieveItemProps( cU4ScProps_ctrl );
            loanData.cU5ScProps = RetrieveItemProps( cU5ScProps_ctrl );

            loanData.cApprFPaidTo = cApprFProps_ctrl.PaidTo;
            loanData.cCrFPaidTo = cCrFProps_ctrl.PaidTo;
            loanData.cTxServFPaidTo = cTxServFProps_ctrl.PaidTo;
            loanData.cFloodCertificationFPaidTo = cFloodCertificationFProps_ctrl.PaidTo;
            loanData.cInspectFPaidTo = cInspectFProps_ctrl.PaidTo;
            loanData.cProcFPaidTo = cProcFProps_ctrl.PaidTo;
            loanData.cUwFPaidTo = cUwFProps_ctrl.PaidTo;
            loanData.cWireFPaidTo = cWireFProps_ctrl.PaidTo;
            loanData.c800U1FPaidTo = c800U1FProps_ctrl.PaidTo;
            loanData.c800U2FPaidTo = c800U2FProps_ctrl.PaidTo;
            loanData.c800U3FPaidTo = c800U3FProps_ctrl.PaidTo;
            loanData.c800U4FPaidTo = c800U4FProps_ctrl.PaidTo;
            loanData.c800U5FPaidTo = c800U5FProps_ctrl.PaidTo;
            loanData.cOwnerTitleInsPaidTo = cOwnerTitleInsFProps_ctrl.PaidTo;
            loanData.cDocPrepFPaidTo = cDocPrepFProps_ctrl.PaidTo;
            loanData.cNotaryFPaidTo = cNotaryFProps_ctrl.PaidTo;
            loanData.cAttorneyFPaidTo = cAttorneyFProps_ctrl.PaidTo;
            loanData.cU1TcPaidTo = cU1TcProps_ctrl.PaidTo;
            loanData.cU2TcPaidTo = cU2TcProps_ctrl.PaidTo;
            loanData.cU3TcPaidTo = cU3TcProps_ctrl.PaidTo;
            loanData.cU4TcPaidTo = cU4TcProps_ctrl.PaidTo;
            loanData.cU1GovRtcPaidTo = cU1GovRtcProps_ctrl.PaidTo;
            loanData.cU2GovRtcPaidTo = cU2GovRtcProps_ctrl.PaidTo;
            loanData.cU3GovRtcPaidTo = cU3GovRtcProps_ctrl.PaidTo;
            loanData.cPestInspectPaidTo = cPestInspectFProps_ctrl.PaidTo;
            loanData.cU1ScPaidTo = cU1ScProps_ctrl.PaidTo;
            loanData.cU2ScPaidTo = cU2ScProps_ctrl.PaidTo;
            loanData.cU3ScPaidTo = cU3ScProps_ctrl.PaidTo;
            loanData.cU4ScPaidTo = cU4ScProps_ctrl.PaidTo;
            loanData.cU5ScPaidTo = cU5ScProps_ctrl.PaidTo;
            loanData.cHazInsPiaPaidTo = cHazInsPiaProps_ctrl.PaidTo;
            loanData.cMipPiaPaidTo = cMipPiaProps_ctrl.PaidTo;
            loanData.cVaFfPaidTo = cVaFfProps_ctrl.PaidTo;

            loanData.c800U1FGfeSection = c800U1FProps_ctrl.Page2Selection;
            loanData.c800U2FGfeSection = c800U2FProps_ctrl.Page2Selection;
            loanData.c800U3FGfeSection = c800U3FProps_ctrl.Page2Selection;
            loanData.c800U4FGfeSection = c800U4FProps_ctrl.Page2Selection;
            loanData.c800U5FGfeSection = c800U5FProps_ctrl.Page2Selection;
            loanData.cEscrowFGfeSection = cEscrowFProps_ctrl.Page2Selection;
            loanData.cDocPrepFGfeSection = cDocPrepFProps_ctrl.Page2Selection;
            loanData.cNotaryFGfeSection = cNotaryFProps_ctrl.Page2Selection;
            loanData.cAttorneyFGfeSection = cAttorneyFProps_ctrl.Page2Selection;
            loanData.cTitleInsFGfeSection = cTitleInsFProps_ctrl.Page2Selection;
            loanData.cU1TcGfeSection = cU1TcProps_ctrl.Page2Selection;
            loanData.cU2TcGfeSection = cU2TcProps_ctrl.Page2Selection;
            loanData.cU3TcGfeSection = cU3TcProps_ctrl.Page2Selection;
            loanData.cU4TcGfeSection = cU4TcProps_ctrl.Page2Selection;
            loanData.cU1GovRtcGfeSection = cU1GovRtcProps_ctrl.Page2Selection;
            loanData.cU2GovRtcGfeSection = cU2GovRtcProps_ctrl.Page2Selection;
            loanData.cU3GovRtcGfeSection = cU3GovRtcProps_ctrl.Page2Selection;
            loanData.cU1ScGfeSection = cU1ScProps_ctrl.Page2Selection;
            loanData.cU2ScGfeSection = cU2ScProps_ctrl.Page2Selection;
            loanData.cU3ScGfeSection = cU3ScProps_ctrl.Page2Selection;
            loanData.cU4ScGfeSection = cU4ScProps_ctrl.Page2Selection;
            loanData.cU5ScGfeSection = cU5ScProps_ctrl.Page2Selection;
            loanData.c904PiaGfeSection = c904PiaProps_ctrl.Page2Selection;
            loanData.c900U1PiaGfeSection = c900U1PiaProps_ctrl.Page2Selection;
            
            loanData.cGfeIsTPOTransaction = cGfeIsTPOTransaction.Checked;
            loanData.GfeVersion = (E_GfeVersion)Enum.Parse(typeof(E_GfeVersion), cGfeVersion.SelectedValue);
            loanData.Save();
        }

        private void bindDropDowns()
        {
            if (IsFeeService)
            {
                Tools.Bind_sProRealETxT(cProRealETxT);
                Tools.Bind_PercentBaseT(cProHazInsT);
            }

            Tools.Bind_PercentBaseT(cRecBaseT);
            Tools.Bind_PercentBaseT(cCountyRtcBaseT);
            Tools.Bind_PercentBaseT(cStateRtcBaseT);
            Tools.Bind_PercentBaseT(cU1GovRtcBaseT);
            Tools.Bind_PercentBaseT(cU2GovRtcBaseT);
            Tools.Bind_PercentBaseT(cU3GovRtcBaseT);
            Tools.Bind_PercentBaseLoanAmountsT(cLDiscntBaseT);
            Tools.Bind_PercentBaseLoanAmountsT(cMBrokFBaseT);
            Tools.Bind_sGfeCreditLenderPaidItemT(sGfeCreditLenderPaidItemT);
            Tools.Bind_PercentBaseT(cTitleInsFBaseT);
            Tools.Bind_PercentBaseT(cOwnerTitleInsFBaseT);
            Tools.Bind_PercentBaseT(cEscrowFBaseT);

            Tools.Bind_cPricingEngineCostT(cPricingEngineCostT);
            Tools.Bind_cPricingEngineCreditT(cPricingEngineCreditT);
            Tools.Bind_cTitleInsurancePolicy(cTitleInsurancePolicyT);

            Tools.Bind_cPricingEngineLimitCreditToT(cPricingEngineLimitCreditToT);

            Tools.Bind_FloodCertificationDeterminationT(cFloodCertificationDeterminationT);
        }

        private void showHideFeeServiceFields()
        {


            // Set panel visibility
            m_AccessDeniedPanel.Visible = false;
            m_AccessAllowedPanel.Visible = true;

            AutomationPanel.Visible = false;
            editcondition.Visible = false;

            cTitleInsurancePolicyTLabel.Visible = false;
            cTitleInsurancePolicyT.Visible = false;

            // Set Element visibility
            cFloodCertificationDeterminationT.Visible = true;



            //cMBrokFProps_ctrl.PaidToTBVisible = true;
            cProHazInsR.ReadOnly = false;
            cProHazInsT.Enabled = true;
            cProHazInsMb.ReadOnly = false;

            cProRealETxR.ReadOnly = false;
            cProRealETxT.Enabled = true;
            cProRealETxMb.ReadOnly = false;

            cProSchoolTx.ReadOnly = false;

            cProFloodIns.ReadOnly = false;

            c1006ProHExp.ReadOnly = false;

            c1007ProHExp.ReadOnly = false;

            cProU3Rsrv.ReadOnly = false;

            cProU4Rsrv.ReadOnly = false;

            trWarning.Visible = true;
            trPricingEngine.Visible = true;
            //PricingEngineCostLabel.Visible = true;
            //cPricingEngineCostT.Visible = true;
            //cPricingEngineCreditTLabel.Visible = true;
            //cPricingEngineCreditT.Visible = true;
            cPricingEngineLimitCreditToTLabel.Visible = true;
            cPricingEngineLimitCreditToT.Visible = true;

            // Add N/A radio button to lines 904 and 906
            c904PiaProps_ctrl.Page2Option3Text = "N/A";
            c904PiaProps_ctrl.Page2Option3Value = "NotApplicable";

            c900U1PiaProps_ctrl.Page2Option3Text = "N/A";
            c900U1PiaProps_ctrl.Page2Option3Value = "NotApplicable";

            cTitleInsFProps_ctrl.Page2Option2Text = "";
            cTitleInsFProps_ctrl.Page2B_rbVisible = false;
            cTitleInsFProps_ctrl.Page2Value = "B4";

            //setup the escrow boxes
            PropCellHeader.ColSpan = 9;  //need to add the escrow cell
            PropCellDesc.ColSpan = 16;
            PropCellDesc.InnerText += " E = Escrowed?";

            var items = GetAllImmediateControlsOfType<GoodFaithEstimate2010RightColumn>(this);
            foreach (var item in items)
            {
                item.EscrowCellVisible = true;
            }

            cHazInsRsrvProps_ctrl.EscrowedVisible = true;
            cMInsRsrvProps_ctrl.EscrowedVisible = true;
            cRealETxRsrvProps_ctrl.EscrowedVisible = true;
            cSchoolTxRsrvProps_ctrl.EscrowedVisible = true;
            cFloodInsRsrvProps_ctrl.EscrowedVisible = true;
            c1006RsrvProps_ctrl.EscrowedVisible = true;
            c1007RsrvProps_ctrl.EscrowedVisible = true;
            cU3RsrvProps_ctrl.EscrowedVisible = true;
            cU4RsrvProps_ctrl.EscrowedVisible = true;


        }

        private void setFeeServiceDefaults()
        {
            cDaysInYr.Text = "360";

            c800U5F.Text = "$0.00";
            c800U4F.Text = "$0.00";
            c800U3F.Text = "$0.00";
            c800U2F.Text = "$0.00";
            c800U1F.Text = "$0.00";

            cFloodCertificationF.Text = "$0.00";
            cWireF.Text = "$0.00";
            cUwF.Text = "$0.00";
            cProcF.Text = "$0.00";
            cTxServF.Text = "$0.00";
            cMBrokFMb.Text = "$0.00";
            cMBrokFPc.Text = "0.000%";
            cInspectF.Text = "$0.00";
            cCrF.Text = "$0.00";
            cApprF.Text = "$0.00";
            cLDiscntFMb.Text = "$0.00";
            cLDiscntPc.Text = "0.000%";
            cLOrigFMb.Text = "$0.00";
            cLOrigFPc.Text = "0.000%";
            cIPiaDy.Text = "0";
            c900U1Pia.Text = "$0.00";
            c904Pia.Text = "$0.00";

            cProHazInsR.Text = "0.000%";
            cProHazInsMb.Text = "$0.00";
            cHazInsPiaMon.Text = "0";

            c1007ProHExp.Text = "$0.00";

            c1006ProHExp.Text = "$0.00";

            cProU3Rsrv.Text = "$0.00";

            cProU4Rsrv.Text = "$0.00";

            cProFloodIns.Text = "$0.00";

            cProRealETxR.Text = "0.000%";
            cProRealETxMb.Text = "$0.00";

            cProSchoolTx.Text = "$0.00";

            cU4Tc.Text = "$0.00";
            cU3Tc.Text = "$0.00";
            cU2Tc.Text = "$0.00";
            cU1Tc.Text = "$0.00";

            cTitleInsFPc.Text = "0.000%";
            cTitleInsFMb.Text = "$0.00";

            cOwnerTitleInsFMb.Text = "$0.00";
            cOwnerTitleInsFPc.Text = "0.000%";

            cAttorneyF.Text = "$0.00";
            cNotaryF.Text = "$0.00";
            cDocPrepF.Text = "$0.00";

            cEscrowFPc.Text = "0.000%";
            cEscrowFMb.Text = "$0.00";

            cU3GovRtcMb.Text = "$0.00";
            cU3GovRtcPc.Text = "0.000%";

            cU2GovRtcMb.Text = "$0.00";
            cU2GovRtcPc.Text = "0.000%";

            cU1GovRtcMb.Text = "$0.00";
            cU1GovRtcPc.Text = "0.000%";

            cStateRtcMb.Text = "$0.00";
            cStateRtcPc.Text = "0.000%";
            cCountyRtcMb.Text = "$0.00";
            cCountyRtcPc.Text = "0.000%";
            cRecFMb.Text = "$0.00";
            cRecFPc.Text = "0.000%";
            cRecFLckd.Checked = false;
            cRecDeed.Text = "$0.00";
            cRecMortgage.Text = "$0.00";
            cRecRelease.Text = "$0.00";

            cU5Sc.Text = "$0.00";
            cU4Sc.Text = "$0.00";
            cU3Sc.Text = "$0.00";
            cU2Sc.Text = "$0.00";
            cU1Sc.Text = "$0.00";

            cPestInspectF.Text = "$0.00";

            sGfeLenderCreditF.Text = "$0.00";
            sGfeLenderCreditFPc.Text = "0.000%";

            sGfeDiscountPointF.Text = "$0.00";
            sGfeDiscountPointFPc.Text = "0.000%";
            sGfeCreditLenderPaidItemF.Text = "$0.00";
            sGfeCreditLenderPaidItemT.SelectedIndex = 2;

            Tools.SetDropDownListValue(cPricingEngineCreditT, 0);

            c800U1FProps_ctrl.Page2Selection  = E_GfeSectionT.B1;
            c800U2FProps_ctrl.Page2Selection = E_GfeSectionT.B1;
            c800U3FProps_ctrl.Page2Selection = E_GfeSectionT.B1;
            c800U4FProps_ctrl.Page2Selection = E_GfeSectionT.B1;
            c800U5FProps_ctrl.Page2Selection = E_GfeSectionT.B1;
            cEscrowFProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cDocPrepFProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cNotaryFProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cAttorneyFProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cTitleInsFProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cU1TcProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cU2TcProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cU3TcProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cU4TcProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cU1ScProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cU2ScProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cU3ScProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cU4ScProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            cU5ScProps_ctrl.Page2Selection = E_GfeSectionT.B4;
            c904PiaProps_ctrl.Page2Selection = E_GfeSectionT.B3;
            c900U1PiaProps_ctrl.Page2Selection = E_GfeSectionT.B3;
        }

        private void hideB4andB6()
        {
            List<GoodFaithEstimate2010RightColumn> charges1100 = new List<GoodFaithEstimate2010RightColumn>();
            charges1100.Add(cEscrowFProps_ctrl);
            charges1100.Add(cOwnerTitleInsFProps_ctrl);
            charges1100.Add(cTitleInsFProps_ctrl);
            charges1100.Add(cDocPrepFProps_ctrl);
            charges1100.Add(cNotaryFProps_ctrl);
            charges1100.Add(cAttorneyFProps_ctrl);
            charges1100.Add(cU1TcProps_ctrl);
            charges1100.Add(cU2TcProps_ctrl);
            charges1100.Add(cU3TcProps_ctrl);
            charges1100.Add(cU4TcProps_ctrl);

            foreach (GoodFaithEstimate2010RightColumn rightCol in charges1100)
            {
                switch (rightCol.getIndexOfRadioButton("B6"))
                {
                    case 1:
                        rightCol.Page2Option1Text = "";
                        rightCol.Page2A_rbVisible = false;
                        break;
                    case 2:
                        rightCol.Page2Option2Text = "";
                        rightCol.Page2B_rbVisible = false;
                        break;
                    case 3:
                        rightCol.Page2Option3Text = "";
                        rightCol.Page2C_rbVisible = false;
                        break;
                }
            }


            List<GoodFaithEstimate2010RightColumn> charges1300 = new List<GoodFaithEstimate2010RightColumn>();
            charges1300.Add(cU1ScProps_ctrl);
            charges1300.Add(cU2ScProps_ctrl);
            charges1300.Add(cU3ScProps_ctrl);
            charges1300.Add(cU4ScProps_ctrl);
            charges1300.Add(cU5ScProps_ctrl);

            foreach (GoodFaithEstimate2010RightColumn rightCol in charges1300)
            {
                switch (rightCol.getIndexOfRadioButton("B4"))
                {
                    case 1:
                        rightCol.Page2Option1Text = "";
                        rightCol.Page2A_rbVisible = false;
                        break;
                    case 2:
                        rightCol.Page2Option2Text = "";
                        rightCol.Page2B_rbVisible = false;
                        break;
                    case 3:
                        rightCol.Page2Option3Text = "";
                        rightCol.Page2C_rbVisible = false;
                        break;
                }
            }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            if (db.IsB4AndB6DisabledIn1100And1300OfGfe)
                hideB4andB6();

            phAdditionalSection1000CustomFees.Visible = db.EnableAdditionalSection1000CustomFees;

            if (IsFeeService)
            {
                bindDropDowns();
                showHideFeeServiceFields();
                setFeeServiceDefaults();

                HazInsEscrowInfo.Visible = true;
                MIEscrowInfo.Visible = true;
                RealETxEscrowInfo.Visible = true;
                SchoolTxEscrowInfo.Visible = true;
                FloodInsEscrowInfo.Visible = true;
                UserDefined1EscrowInfo.Visible = true;
                UserDefined2EscrowInfo.Visible = true;
                UserDefined3EscrowInfo.Visible = true;
                UserDefined4EscrowInfo.Visible = true;

                return;
            }

            if(m_hiddenChoice.Value == "Don't Save")
                Response.Redirect("CCTemplate.aspx?ccid=" + AspxTools.HtmlStringFromQueryString("ccid"));
            m_errMsg.Text = "";

            // OPM 3453
            if(!BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanAccessCCTemplates))
            {
                m_AccessDeniedPanel.Visible = true;
                m_AccessAllowedPanel.Visible = false;
            }
            else
            {
                m_AccessDeniedPanel.Visible = false;
                m_AccessAllowedPanel.Visible = true;
            }

			// Put user code to initialize the page here
            if (!Page.IsPostBack) 
            {
                bindDropDowns();
                LoadData();
            } 
            else 
            {
                Page.Validate();
                if (Page.IsValid) 
                {
                    bool saveSuccess = false;
                    try
                    {
                        SaveData();
                        saveSuccess = true;
                    }
                    catch (Exception ex)
                    {
                        m_errMsg.Text = "An error occurred during the save process";
                        Tools.LogError(ex.Message);
                    }
                    if (m_hiddenChoice.Value == "Save" && saveSuccess)
                        Response.Redirect("CCTemplate.aspx?ccid=" + AspxTools.HtmlStringFromQueryString("ccid"));
                }
                m_hiddenChoice.Value = "";
                cGfeVersion.SelectedValue = E_GfeVersion.Gfe2010.ToString();
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

        protected void onOKClick(object sender, System.EventArgs e)
        {
            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new string[] { "Name=" + AspxTools.JsString(cCcTemplateNm.Text) });
        }

        protected IEnumerable<T> GetAllImmediateControlsOfType<T>(Control parent) where T : Control
        {
            var result = new List<T>();

            foreach (Control control in parent.Controls)
            {
                T ctrl = control as T;
                if (ctrl != null)
                {
                    result.Add(ctrl);
                }

                if (control.HasControls())
                {
                    result.AddRange(GetAllImmediateControlsOfType<T>(control));
                }
            }
            return result;
        }
	}
}
