<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="ClosingCostList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.ClosingCostList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ClosingCostList</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body scroll="yes" onload="init();" bgcolor="gainsboro">
		<script type="text/javascript">
    <!--
    
    function init() 
    {
        resize(650, 540);
        
        if(<%=AspxTools.JsBool(m_deleteInvalid)%>)
        {
            var ccid = document.getElementById("m_hiddenCcid").value;
            var ccname = document.getElementById("m_hiddenCcname").value;
            showModal('/los/Template/DeleteCCTemplateFailure.aspx?ccid=' + encodeURIComponent(ccid) + '&ccname=' + encodeURIComponent(ccname), null, "Cannot Delete CCTemplate");
        }
    }
		  
	function onEditClick(ccID, GfeVersion)
	{
	    var link;
	    if (GfeVersion == 0)
	        link = '/los/Template/CCTemplate.aspx';
	    else
	        link = '/los/Template/CCTemplateGFE2010.aspx';
		showModal(link + '?ccid=' + ccID, null, null, null, function(){
			window.location = self.location;
		});
	}
		  
	function onDuplicateClick(ccID, GfeVersion)
	{
		document.getElementById("m_hiddenCcid").value=ccID;
		document.getElementById("m_hiddenGfeVersion").value = GfeVersion;
		document.getElementById("m_hiddenCmd").value = "duplicate";
		document.forms[0].submit();
	}
		  
	function onDeleteClick(ccID, ccName)
	{
		if( confirm( "Delete Closing Cost Template?" ) == false )
			return;
			
		document.getElementById("m_hiddenCcid").value=ccID;
		document.getElementById("m_hiddenCcname").value=ccName;
		document.getElementById("m_hiddenCmd").value="delete";
		document.forms[0].submit();
	}
		  
		//-->
		</script>
		<h4 class="page-header">Closing Cost Template List</h4>
		<form id="ClosingCostList" method="post" runat="server">
			<input type="hidden" id="m_hiddenCcid" value="" runat="server" NAME="m_hiddenCcid"/> 
			<input type="hidden" id="m_hiddenCmd" value="" runat="server" NAME="m_hiddenCmd"/>
			<input type="hidden" id="m_hiddenCcname" value="" runat="server" NAME="m_hiddenCcname"/>
			<input type="hidden" id="m_hiddenGfeVersion" value="" runat="server" NAME="m_hiddenGfeVersion"/> 
			<TABLE class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<asp:Panel id=m_AccessDeniedPanel style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
							Access Denied.  You do not have permission to access Closing Cost Templates.
						</asp:Panel>
					</td>
				</tr>
				<asp:Panel ID=m_ccListPanel Runat=server>
				<TR>
					<TD>
						<asp:Button id="m_addBtn" runat="server" Text="Add New GFE 2009 Closing Cost Template" onclick="onAddClick"></asp:Button>
						<asp:Button id="m_addGFE2010Btn" runat="server" Text="Add New GFE 2010 Closing Cost Template" onclick="onAddGFE2010Click"></asp:Button>
					</TD>
				</TR>
				<TR>
					<TD>
						<ml:CommonDataGrid id="m_closingCostsDG" CellPadding="2" runat="server" DefaultSortExpression="cCcTemplateNm ASC" onselectedindexchanged="m_closingCostsDG_SelectedIndexChanged">
							<AlternatingItemStyle CssClass="GridAlternatingItem"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem"></ItemStyle>
							<HeaderStyle CssClass="GridHeader"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn ItemStyle-Width="30" ItemStyle-HorizontalAlign="Center">
									<ItemTemplate>
										<a href="#" onclick="onEditClick(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "cCcTemplateId").ToString())%>,<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "GfeVersion").ToString())%>); return false">
											edit</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn ItemStyle-Width="60" ItemStyle-HorizontalAlign="Center">
									<ItemTemplate>
										<a href="#" onclick="onDuplicateClick(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "cCcTemplateId").ToString())%>,<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "GfeVersion").ToString())%>); return false">
											duplicate</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn ItemStyle-Width="45" ItemStyle-HorizontalAlign="Center">
									<ItemTemplate>
										<a href="#" onclick="onDeleteClick(<%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "cCcTemplateId").ToString())%>,<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "cCcTemplateNm").ToString())%>); return false">
											delete</a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="cCcTemplateNm" HeaderText="Closing Cost Template Name" SortExpression="cCcTemplateNm" />
								<asp:TemplateColumn HeaderText="Version" SortExpression="GfeVersion">
									<ItemTemplate>
										<%# AspxTools.HtmlString(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "GfeVersion")) == 0 ? "GFE 2009" : "GFE 2010")%>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</ml:CommonDataGrid></TD>
				</TR>
				</asp:Panel>
				<TR>
					<TD align="center">
						<INPUT type="button" value="Close" onclick="onClosePopup();">
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
