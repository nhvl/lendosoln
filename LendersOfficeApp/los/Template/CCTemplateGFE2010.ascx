<%@ Control Language="c#" AutoEventWireup="false" CodeBehind="CCTemplateGFE2010.ascx.cs"
    Inherits="LendersOfficeApp.Template.CCTemplateGFE2010" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimate2010RightColumn" Src="../LegalForm/GoodFaithEstimate2010RightColumn.ascx" %>
<%@ Register TagPrefix="uc1" TagName="EscrowInfo" Src="../FeeService/EscrowInfo.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<h4 class="page-header">Closing Cost Template</h4>
<table class="FormTable" id="Table3" cellspacing="0" cellpadding="0" width="750"
    border="0" data-fsid="outerTable" >
    <tr>
        <td>
            <asp:Panel ID="m_AccessDeniedPanel" Style="padding-right: 30px; padding-left: 30px;
                padding-bottom: 30px; color: red; padding-top: 30px; text-align: center" runat="server">
                Access Denied. You do not have permission to access Closing Cost Templates.
            </asp:Panel>
        </td>
    </tr>
    <asp:Panel ID="m_AccessAllowedPanel" runat="server">
        <tr>
            <td>
                Template Name 
                <asp:TextBox ID="cCcTemplateNm" runat="server" Width="270px" Height="21px" MaxLength="36"></asp:TextBox><img
                    src="../../images/require_icon.gif">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cCcTemplateNm">
                    <IMG runat="server" src="../../images/error_icon.gif">
                </asp:RequiredFieldValidator>
                        
                <br />
                
                <asp:PlaceHolder runat="server" ID="AutomationPanel">
                Apply this template when <br />
                
                <span id="ConditionTxt"><ml:EncodedLiteral runat="server" ID="Condition"></ml:EncodedLiteral> </span> &nbsp;<a runat="server" title="Edit Condition" style="cursor: pointer; text-decoration: underline" id="editcondition">edit condition</a>
                <asp:HiddenField runat="server" ID="ConditionTemplateKey" />
                </asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="cGfeVersion" RepeatDirection="Horizontal" runat="server">
                    <asp:ListItem Text="GFE 2009" Value="Gfe2009" ></asp:ListItem>
                    <asp:ListItem Text="GFE 2010" Value="Gfe2010"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td style="color: Red">
                <ml:EncodedLiteral ID="m_errMsg" runat="server"></ml:EncodedLiteral>
            </td>
        </tr>
        <tr data-fsid="GfeRow">
            <td>
                <table id="Table1" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                    padding-top: 0px" cellspacing="0" cellpadding="0" width="740" border="0" data-fsid="GfeLines">
                    <tr>
                        <td class="FormTableHeader">
                        </td>
                        <td class="FormTableHeader" colspan="14">
                            Options Affecting Calculations
                        </td>
                    </tr>
                    <tr data-fsid="Options">
                        <td colspan="15" >
                            <table width="100%" >
                                <tr data-fsid="Options">
                                    <td> &nbsp;</td>
                                    <td>
                                        Days in Year
                                    </td>
                                    <td>
                                        <asp:TextBox ID="cDaysInYr" style="float:left;" TabIndex="-1" runat="server" Width="40px" MaxLength="3" data-fsid="sDaysInYr"></asp:TextBox>
                                    </td>
                                    <td> &nbsp;</td>
                                    <td> &nbsp;</td>
                                </tr>
                                <tr id="trWarning" runat="server" class="trWarning" data-fsid="Options" visible="false">
                                    <td> &nbsp;</td>
                                    <td colspan="4" style="color:Red;font-weight:bold">
                                        The following options only apply when credit/charge calculation method is "Set Manually"
                                    </td>
                                </tr>
                                <tr id="trPricingEngine" runat="server" data-fsid="Options" visible="false">
                                    <td> &nbsp;</td>
                                    <td>
                                        <ml:EncodedLabel runat="server" ID="PricingEngineCostLabel" AssociatedControlID="cPricingEngineCostT">
                                        Add pricing engine cost to
                                        </ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="cPricingEngineCostT" TabIndex="-1" data-fsid="sPricingEngineCostT"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <ml:EncodedLabel runat="server" ID="cPricingEngineCreditTLabel">
                                        Add pricing engine credit to 
                                        </ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="cPricingEngineCreditT" TabIndex="-1" onclick="oncreditupdate(this)" data-fsid="sPricingEngineCreditT"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr data-fsid="Options">
                                    <td>&nbsp;</td>
                                    <td>
                                        <ml:EncodedLabel runat="server" ID="cTitleInsurancePolicyTLabel">
                                        Title insurance policy type for purchase loans
                                        </ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="cTitleInsurancePolicyT" TabIndex="-1"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <ml:EncodedLabel runat="server" Visible="false" ID="cPricingEngineLimitCreditToTLabel">Limit credit to:</ml:EncodedLabel>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" Visible="false" ID="cPricingEngineLimitCreditToT" TabIndex="-1" data-fsid="sPricingEngineLimitCreditToT"></asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="FieldLabel">
                        <td colspan="15" align="right">
                            <asp:CheckBox Text="Link the &quot;Paid To&quot; to the Offical Contact List" ID="cGfeUsePaidToFromOfficialContact"
                                onclick="verifyLinkedPaidToTBStatus(this);" runat="server" />
                        </td>
                    </tr>
                    <tr data-fsid=Legend>
                        <td class="FormTableHeader" colspan="15" runat="server" id="PropCellDesc" align="center" nowrap>
                            BF = Bona Fide&nbsp;&nbsp;&nbsp; A = APR&nbsp;&nbsp;&nbsp; F = FHA Allowable&nbsp;&nbsp;&nbsp;
                            POC = Paid Outside of Closing&nbsp;&nbsp;&nbsp; PD = Paid&nbsp;&nbsp;&nbsp; DFLP = Deducted From Loan
                            Proceeds&nbsp;&nbsp;&nbsp; B = Paid to broker&nbsp;&nbsp;&nbsp; TP = Paid to Third Party
                            &nbsp;&nbsp;&nbsp; AFF = Third Party is Affiliate  
                        </td>
                    </tr>
                    <tr data-fsid=Legend>
                        <td class="FormTableHeader">
                        </td>
                        <td class="FormTableHeader">
                            Description of Charge
                        </td>
                        <td class="FormTableHeader">
                        </td>
                        <td class="FormTableHeader">
                            Amount
                        </td>
                        <td class="FormTableHeader">
                            Page 2
                        </td>
                        <td class="FormTableHeader">
                            Paid By
                        </td>
                        <td class="FormTableHeader" colspan="8" runat="server" id="PropCellHeader">
                        </td>
                        <td class="FormTableHeader">
                            Paid To
                        </td>
                    </tr>
                    <tr>
                        <td class="FormTableSubheader">
                            800
                        </td>
                        <td class="FormTableSubheader" colspan="14">
                            ITEMS PAYABLE IN CONNECTION WITH LOAN
                        </td>
                    </tr>
                    <tr data-fsid="801">
                        <td>
                            801
                        </td>
                        <td>
                            Loan origination fee
                        </td>
                        <td>
                            <ml:PercentTextBox ID="cLOrigFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="70" EnableViewState="False" data-fsid="sLOrigFPc"></ml:PercentTextBox>
                            +
                            <ml:MoneyTextBox ID="cLOrigFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="59" EnableViewState="False" data-fsid="sLOrigFMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cLOrigF" runat="server" preset="money" Width="77" ReadOnly="True"
                                EnableViewState="False"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cLOrigFProps_ctrl" CCTemplateMode="true"
                            Page2Value="A1" PocCBVisible="false" PaidToTBVisible="false" PaidToBCBVisible="true"
                            BFVisible="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="802">
                        <td>
                            802
                        </td>
                        <td>
                            Credit (-) or Charge (+)
                        </td>
                        <td nowrap>
                            <ml:PercentTextBox ID="cLDiscntPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="70" EnableViewState="False" data-fsid="sLDiscntPc"></ml:PercentTextBox>&nbsp;of
                            <asp:DropDownList ID="cLDiscntBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" data-fsid="sLDiscntBaseT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cLDiscntFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="59" EnableViewState="False" data-fsid="sLDiscntFMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="ccLDiscnt" runat="server" preset="money" Width="76px" ReadOnly="True"
                                EnableViewState="False"></ml:MoneyTextBox>
                        </td>
                    </tr>
                    <tr data-fsid="802">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            Credit for lender paid fees
                        </td>
                        <td align="right" style="padding-right: 7px">
                            <asp:DropDownList ID="sGfeCreditLenderPaidItemT" runat="server" Enabled="false"/>
                            <ml:MoneyTextBox ID="sGfeCreditLenderPaidItemF" Width="76px" runat="server" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr data-fsid="802">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            General Lender credit
                        </td>
                        <td align="right" style="padding-right: 7px">
                            <ml:PercentTextBox ID="sGfeLenderCreditFPc" runat="server" preset="percent" Width="76"
                                ReadOnly="true" /><ml:MoneyTextBox ID="sGfeLenderCreditF" runat="server" preset="money"
                                    Width="76px" ReadOnly="true" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cLCreditProps_ctrl" Page2A_rbVisible="false"
                            runat="server" PaidToTBVisible="false" PocCBVisible="false" PdByTDDVisible="false" Enabled="true" />
                    </tr>
                    <tr data-fsid="802">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            Discount points
                        </td>
                        <td align="right" style="padding-right: 7px">
                            <ml:PercentTextBox ID="sGfeDiscountPointFPc" runat="server" preset="percent" Width="76"
                                ReadOnly="true" /><ml:MoneyTextBox ID="sGfeDiscountPointF" runat="server" preset="money"
                                    Width="76px" ReadOnly="true" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cLDiscntProps_ctrl" CCTemplateMode="true"
                            Page2Value="A2" PocCBVisible="false" PaidToTBVisible="false" TpCBVisible="false"
                            BFVisible="true" runat="server">
                        </uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="2">
                            <asp:CheckBox ID="cGfeIsTPOTransaction" runat="server" Text="This transaction involves a TPO" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="2">
                            <asp:CheckBox ID="sIsItemizeBrokerCommissionOnIFW" runat="server" Text="Itemize originator compensation on Initial Fees Worksheet" Enabled="false"/>
                        </td>
                    </tr>
                    <tr data-fsid="804">
                        <td>
                            804
                        </td>
                        <td>
                            Appraisal fee
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cApprF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" EnableViewState="False" data-fsid="sApprF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cApprFProps_ctrl" CCTemplateMode="true"
                            Page2Value="B3" runat="server" PaidCBVisible="true"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="805">
                        <td>
                            805
                        </td>
                        <td>
                            Credit report
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cCrF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" EnableViewState="False" data-fsid="sCrF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cCrFProps_ctrl" CCTemplateMode="true" Page2Value="B3"
                            runat="server" PaidCBVisible="true"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="806">
                        <td>
                            806
                        </td>
                        <td>
                            Tax service fee
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cTxServF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sTxServF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cTxServFProps_ctrl" CCTemplateMode="true"
                            Page2Value="B3" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="807">
                        <td>
                            807
                        </td>
                        <td>
                            Flood Certification
                        </td>
                        <td>
                            <asp:dropdownlist id="cFloodCertificationDeterminationT" runat="server" data-fsid="sFloodCertificationDeterminationT" Visible="false"></asp:dropdownlist>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cFloodCertificationF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sFloodCertificationF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cFloodCertificationFProps_ctrl" CCTemplateMode="true"
                            Page2Value="B3" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="808">
                        <td>
                            808
                        </td>
                        <td>
                            Mortgage broker fee
                        </td>
                        <td>
                            <ml:PercentTextBox ID="cMBrokFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="70" EnableViewState="False" data-fsid="sMBrokFPc"></ml:PercentTextBox>&nbsp;of
                            <asp:DropDownList ID="cMBrokFBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" data-fsid="sMBrokFBaseT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cMBrokFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="59" EnableViewState="False" data-fsid="sMBrokFMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cMBrokF" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cMBrokFProps_ctrl" CCTemplateMode="true"
                            Page2Value="A1" PaidToTBVisible="false" PaidToBCBVisible="true" runat="server">
                        </uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="809">
                        <td>
                            809
                        </td>
                        <td>
                            Lender's inspection fee
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cInspectF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" EnableViewState="False" data-fsid="sInspectF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cInspectFProps_ctrl" CCTemplateMode="true"
                            Page2Value="A1" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="810">
                        <td>
                            810
                        </td>
                        <td>
                            Processing fee
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cProcF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sProcF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cProcFProps_ctrl" CCTemplateMode="true"
                            Page2Value="A1" PaidCBVisible="true" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="811">
                        <td>
                            811
                        </td>
                        <td>
                            Underwriting fee
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cUwF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sUwF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cUwFProps_ctrl" CCTemplateMode="true" Page2Value="A1"
                            runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="812">
                        <td>
                            812
                        </td>
                        <td>
                            Wire transfer
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cWireF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sWireF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cWireFProps_ctrl" CCTemplateMode="true"
                            Page2Value="A1" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="813">
                        <td>
                            813
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="c800U1FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" SkipMe="true" Width="95%" MaxLength="100" data-fsid="s800U1FDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="c800U1F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="s800U1F"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="c800U1FProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="814">
                        <td>
                            814
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="c800U2FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" SkipMe="true" Width="95%" MaxLength="100" data-fsid="s800U2FDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="c800U2F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="s800U2F"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="c800U2FProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="815">
                        <td>
                            815
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="c800U3FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" SkipMe="true" Width="95%" MaxLength="100" data-fsid="s800U3FDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="c800U3F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="s800U3F"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="c800U3FProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="816">
                        <td>
                            816
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="c800U4FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" SkipMe="true" Width="95%" MaxLength="100" data-fsid="s800U4FDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="c800U4F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="s800U4F"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="c800U4FProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="817">
                        <td>
                            817
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="c800U5FDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" SkipMe="true" Width="95%" MaxLength="100" data-fsid="s800U5FDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="c800U5F" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="s800U5F"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="c800U5FProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="A1" Page2Option1Value="B1" Page2Option2Text="B3" Page2Option2Value="B3"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr>
                        <td class="FormTableSubheader">
                            <a name="900"></a>900
                        </td>
                        <td class="FormTableSubheader" colspan="14">
                            ITEMS REQUIRED BY LENDER TO BE PAID IN ADVANCE
                        </td>
                    </tr>
                    <tr data-fsid="901">
                        <td>
                            901
                        </td>
                        <td>
                            Interest for
                        </td>
                        <td nowrap>
                            <asp:TextBox ID="cIPiaDy" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="40px" MaxLength="4" data-fsid="sIPiaDy"></asp:TextBox>&nbsp;days @
                            <ml:MoneyTextBox ID="cIPerDay" runat="server" preset="money" Width="55px" ReadOnly="true"
                                decimalDigits="4"></ml:MoneyTextBox>
                            per day
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cIPia" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cIPiaProps_ctrl" CCTemplateMode="true"
                            Page2Value="B10" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="902">
                        <td>
                            902
                        </td>
                        <td nowrap colspan="2" rowspan="1">
                            Mortgage Insurance Premium
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cMipPia" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cMipPiaProps_ctrl" CCTemplateMode="true"
                            Page2Value="B3" PaidToBCBVisible="false" TpCBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="903">
                        <td>
                            903
                        </td>
                        <td nowrap colspan="2">
                            Haz Ins. @
                            <ml:PercentTextBox ID="cProHazInsR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="70" DESIGNTIMEDRAGDROP="236" ReadOnly="true" data-fsid="sProHazInsR"></ml:PercentTextBox>
                            of
                            <asp:DropDownList ID="cProHazInsT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Enabled="false" Width="90px" data-fsid="sProHazInsT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cProHazInsMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="60px" ReadOnly="true" data-fsid="sProHazInsMb"></ml:MoneyTextBox>
                            for
                            <asp:TextBox ID="cHazInsPiaMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="30px" MaxLength="4" data-fsid="sHazInsPiaMon"></asp:TextBox>
                            mths
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cHazInsPia" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cHazInsPiaProps_ctrl" CCTemplateMode="true"
                            Page2Value="B11" PaidToBCBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="904">
                        <td>
                            904
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="c904PiaDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" Height="20px" data-fsid="s904PiaDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="c904Pia" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="s904Pia"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="c904PiaProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B3" Page2Option1Value="B3" Page2Option2Text="B11" Page2Option2Value="B11"
                            PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="905">
                        <td>
                            905
                        </td>
                        <td>
                            VA Funding Fee</A>
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cVaFf" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cVaFfProps_ctrl" CCTemplateMode="true"
                            Page2Value="B3" PaidToBCBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="906">
                        <td>
                            906</asp:textbox>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="c900U1PiaDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" data-fsid="s900U1PiaDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="c900U1Pia" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="s900U1Pia"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="c900U1PiaProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B3" Page2Option1Value="B3" Page2Option2Text="B11" Page2Option2Value="B11"
                            PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr>
                        <td class="FormTableSubheader">
                            <a name="1000"></a>1000
                        </td>
                        <td class="FormTableSubheader" colspan="14">
                            RESERVES DEPOSITED WITH LENDER
                        </td>
                    </tr>
                    <tr data-fsid="1002">
                        <td>
                            1002
                        </td>
                        <td>
                            Haz ins. reserve
                        </td>
                        <td>
                            <asp:TextBox ID="cHazInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="41" MaxLength="4" data-fsid="sHazInsRsrvMon"></asp:TextBox>&nbsp;mths @
                            <ml:MoneyTextBox ID="cProHazIns" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox>
                            / month
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cHazInsRsrv" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cHazInsRsrvProps_ctrl" CCTemplateMode="true"
                            Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <uc1:EscrowInfo runat="server" ID="HazInsEscrowInfo" Visible="false" EscrowItem="HazardInsurance" FeeServiceID="1002" />
                    <tr data-fsid="1003">
                        <td>
                            1003
                        </td>
                        <td colspan="2">
                            Mtg ins. reserve&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; =
                            <ml:MoneyTextBox ID="cProMIns" runat="server" preset="money" Width="90" ReadOnly="True"></ml:MoneyTextBox>&nbsp;&nbsp;for
                            <asp:TextBox ID="cMInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="41px" MaxLength="4" data-fsid="sMInsRsrvMon"></asp:TextBox>
                            mths
                        </td>
                        <td colspan="1">
                            <ml:MoneyTextBox ID="cMInsRsrv" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cMInsRsrvProps_ctrl" CCTemplateMode="true"
                            Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <uc1:EscrowInfo runat="server" ID="MIEscrowInfo" Visible="false" EscrowItem="MortgageInsurance" FeeServiceID="1003" />
                    <tr data-fsid="1004">
                        <td>
                            1004
                        </td>
                        <td colspan="2" nowrap>
                            Tax resrv @
                            <ml:PercentTextBox ID="cProRealETxR" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="70" ReadOnly="true" data-fsid="sProRealETxR"></ml:PercentTextBox>&nbsp;of
                            <asp:DropDownList ID="cProRealETxT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Enabled="false" Width="90" data-fsid="sProRealETxT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cProRealETxMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="59px" ReadOnly="true" data-fsid="sProRealETxMb"></ml:MoneyTextBox>
                            for
                            <asp:TextBox ID="cRealETxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="31px" MaxLength="4" data-fsid="sRealETxRsrvMon"></asp:TextBox>
                            mths
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cRealETxRsrv" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cRealETxRsrvProps_ctrl" CCTemplateMode="true"
                            Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <uc1:EscrowInfo runat="server" ID="RealETxEscrowInfo" Visible="false" EscrowItem="RealEstateTax" FeeServiceID="1004"/>
                    <tr data-fsid="1005">
                        <td>
                            1005
                        </td>
                        <td>
                            School taxes
                        </td>
                        <td>
                            <asp:TextBox ID="cSchoolTxRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="41px" MaxLength="4" data-fsid="sSchoolTxRsrvMon"></asp:TextBox>&nbsp;mths @
                            <ml:MoneyTextBox ID="cProSchoolTx" runat="server" preset="money" Width="90" ReadOnly="true" data-fsid="sProSchoolTx"></ml:MoneyTextBox>
                            / month
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cSchoolTxRsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cSchoolTxRsrvProps_ctrl" CCTemplateMode="true"
                            Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <uc1:EscrowInfo runat="server" ID="SchoolTxEscrowInfo" Visible="false" EscrowItem="SchoolTax" FeeServiceID="1005" />
                    <tr data-fsid="1006">
                        <td>
                            1006
                        </td>
                        <td>
                            Flood ins. reserve
                        </td>
                        <td>
                            <asp:TextBox ID="cFloodInsRsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="41px" MaxLength="4" data-fsid="sFloodInsRsrvMon"></asp:TextBox>&nbsp;mths @
                            <ml:MoneyTextBox ID="cProFloodIns" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="90" ReadOnly="true" data-fsid="sProFloodIns"></ml:MoneyTextBox>
                            / month
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cFloodInsRsrv" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cFloodInsRsrvProps_ctrl" CCTemplateMode="true"
                            Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <uc1:EscrowInfo runat="server" ID="FloodInsEscrowInfo" Visible="false" EscrowItem="FloodInsurance" FeeServiceID="1006" />
                    <tr data-fsid="1007">
                        <td>
                            1007
                        </td>
                        <td>
                            Aggregate adjustment
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cAggregateAdjRsrv" runat="server" ReadOnly="true" preset="money"
                                Width="76px"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cAggregateAdjRsrvProps_ctrl" CCTemplateMode="true"
                            Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr  data-fsid="1008">
                        <td>
                            1008
                        </td>
                        <td>
                            <asp:TextBox ID="c1006ProHExpDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="135" MaxLength="21" Height="20px" data-fsid="s1006ProHExpDesc"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="c1006RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="41px" MaxLength="4" data-fsid="s1006RsrvMon"></asp:TextBox>&nbsp;mths @
                            <ml:MoneyTextBox ID="c1006ProHExp" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="90" ReadOnly="true" data-fsid="s1006ProHExp"></ml:MoneyTextBox>
                            / month
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="c1006Rsrv" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="c1006RsrvProps_ctrl" CCTemplateMode="true"
                            Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <uc1:EscrowInfo runat="server" ID="UserDefined1EscrowInfo" Visible="false" EscrowItem="UserDefine1" FeeServiceID="1008" />
                    <tr data-fsid="1009">
                        <td>
                            1009
                        </td>
                        <td>
                            <asp:TextBox ID="c1007ProHExpDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="135" MaxLength="21" Height="20px" data-fsid="s1007ProHExpDesc"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="c1007RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="41px" MaxLength="4" data-fsid="s1007RsrvMon"></asp:TextBox>&nbsp;mths @
                            <ml:MoneyTextBox ID="c1007ProHExp" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="90" ReadOnly="true"></ml:MoneyTextBox>
                            / month
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="c1007Rsrv" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="c1007RsrvProps_ctrl" CCTemplateMode="true"
                            Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <uc1:EscrowInfo runat="server" ID="UserDefined2EscrowInfo" Visible="false" EscrowItem="UserDefine2" FeeServiceID="1009" />
                    <asp:PlaceHolder runat="server" ID="phAdditionalSection1000CustomFees">
                        <tr data-fsid="1010">
                            <td>
                                1010
                            </td>
                            <td>
                                <asp:TextBox ID="cU3RsrvDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                    runat="server" Width="135" MaxLength="21" Height="20px" data-fsid="sU3RsrvDesc"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="cU3RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                    runat="server" Width="41px" MaxLength="4" data-fsid="sU3RsrvMon"></asp:TextBox>&nbsp;mths @
                                <ml:MoneyTextBox ID="cProU3Rsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                    runat="server" preset="money" Width="90" ReadOnly="true" data-fsid="sProU3Rsrv"></ml:MoneyTextBox>
                                / month
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="cU3Rsrv" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                            </td>
                            <uc1:GoodFaithEstimate2010RightColumn ID="cU3RsrvProps_ctrl" CCTemplateMode="true"
                                Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                        </tr>
                        <uc1:EscrowInfo runat="server" ID="UserDefined3EscrowInfo" Visible="false" EscrowItem="UserDefine3" FeeServiceID="1010" />
                        <tr data-fsid="1011">
                            <td>
                                1011
                            </td>
                            <td>
                                <asp:TextBox ID="cU4RsrvDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                    runat="server" Width="135" MaxLength="21" Height="20px" data-fsid="sU4RsrvDesc"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="cU4RsrvMon" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                    runat="server" Width="41px" MaxLength="4" data-fsid="sU4RsrvMon"></asp:TextBox>&nbsp;mths @
                                <ml:MoneyTextBox ID="cProU4Rsrv" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                    runat="server" preset="money" Width="90" ReadOnly="true" data-fsid="sProU4Rsrv"></ml:MoneyTextBox>
                                / month
                            </td>
                            <td>
                                <ml:MoneyTextBox ID="cU4Rsrv" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                            </td>
                            <uc1:GoodFaithEstimate2010RightColumn ID="cU4RsrvProps_ctrl" CCTemplateMode="true"
                                Page2Value="B9" PaidToTBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                        </tr>
                        <uc1:EscrowInfo runat="server" ID="UserDefined4EscrowInfo" Visible="false" EscrowItem="UserDefine4" FeeServiceID="1011" />
                    </asp:PlaceHolder>
                    <tr>
                        <td class="FormTableSubheader">
                            1100
                        </td>
                        <td class="FormTableSubheader" colspan="14">
                            TITLE CHARGES
                        </td>
                    </tr>
                    <tr data-fsid="1102">
                        <td>
                            1102
                        </td>
                        <td>
                            Closing/Escrow Fee
                        </td>
                        <td>
                            <ml:PercentTextBox ID="cEscrowFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="51" data-fsid="sEscrowFPc"></ml:PercentTextBox>
                            of
                            <asp:DropDownList ID="cEscrowFBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" data-fsid="sEscrowFBaseT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cEscrowFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="56" data-fsid="sEscrowFMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ReadOnly="True" ID="cEscrowF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn  ID="cEscrowFProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1103">
                        <td>
                            1103
                        </td>
                        <td>
                            Owner's title Insurance
                        </td>
                        <td>
                                                    <ml:PercentTextBox ID="cOwnerTitleInsFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="51" data-fsid="sOwnerTitleInsFPc"></ml:PercentTextBox>
                            of
                            <asp:DropDownList ID="cOwnerTitleInsFBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" data-fsid="sOwnerTitleInsFBaseT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cOwnerTitleInsFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="56" data-fsid="sOwnerTitleInsFMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ReadOnly="True" ID="cOwnerTitleInsF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cOwnerTitleInsFProps_ctrl" CCTemplateMode="true"
                            Page2Value="B5" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1104">
                        <td>
                            1104
                        </td>
                        <td>
                            Lender's title Insurance
                        </td>
                        <td>
                        <ml:PercentTextBox ID="cTitleInsFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="51" data-fsid="sTitleInsFPc"></ml:PercentTextBox>
                            of
                            <asp:DropDownList ID="cTitleInsFBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" data-fsid="sTitleInsFBaseT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cTitleInsFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="56" data-fsid="sTitleInsFMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox  ReadOnly="True" ID="cTitleInsF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cTitleInsFProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1109">
                        <td>
                            1109
                        </td>
                        <td>
                            Doc preparation fee
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cDocPrepF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sDocPrepF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cDocPrepFProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1110">
                        <td>
                            1110
                        </td>
                        <td>
                            Notary fees
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cNotaryF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sNotaryF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cNotaryFProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1111">
                        <td>
                            1111
                        </td>
                        <td>
                            Attorney fees
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cAttorneyF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sAttorneyF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cAttorneyFProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1112">
                        <td>
                            1112
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="cU1TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" data-fsid="sU1TcDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU1Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sU1Tc"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU1TcProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1113">
                        <td>
                            1113
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="cU2TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" data-fsid="sU2TcDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU2Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sU2Tc"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU2TcProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1114">
                        <td>
                            1114
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="cU3TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" data-fsid="sU3TcDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU3Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sU3Tc"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU3TcProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1115">
                        <td>
                            1115
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="cU4TcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" data-fsid="sU4TcDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU4Tc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sU4Tc"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU4TcProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr>
                        <td class="FormTableSubheader">
                            1200
                        </td>
                        <td class="FormTableSubheader" nowrap colspan="14">
                            GOVERNMENT RECORDING &amp; TRANSFER CHARGES
                        </td>
                    </tr>
                    <tr data-fsid="1201">
                        <td>
                            1201&nbsp;&nbsp;
                        </td>
                        <td nowrap>
                            Recording fees
                        </td>
                        <td nowrap>
                            <span id="sp1201" style="float:left">
                                <ml:PercentTextBox ID="cRecFPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                    runat="server" preset="percent" Width="51" data-fsid="sRecFPc"></ml:PercentTextBox>
                                of
                                <asp:DropDownList ID="cRecBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                    runat="server" data-fsid="sRecBaseT">
                                </asp:DropDownList>
                                +
                                <ml:MoneyTextBox ID="cRecFMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                    runat="server" preset="money" Width="56" data-fsid="sRecFMb"></ml:MoneyTextBox>
                            </span>
                            <input type="checkbox" ID="cRecFLckd" runat="server" style="float:right" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" onclick="toggleRecFFields();" data-fsid="sRecFLckd"/>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cRecF" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cRecFProps_ctrl" CCTemplateMode="true"
                            Page2Value="B7" PaidToBCBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1201">
						<td class="FieldLabel">1202&nbsp;&nbsp;</td>
						<td class="FieldLabel" noWrap>Deed <ml:moneytextbox id="cRecDeed" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" data-fsid="sRecDeed"></ml:moneytextbox></td>
						<td class="FieldLabel" noWrap>
						    <span style="float:left">Mortgage <ml:moneytextbox id="cRecMortgage" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" data-fsid="sRecMortgage"></ml:moneytextbox></span>
						    <span style="float:right; margin-right:25px">Release <ml:moneytextbox id="cRecRelease" onblur="unhighlightRow(this);" onfocus="highlightRow(this);" runat="server" preset="money" width="56" data-fsid="sRecRelease"></ml:moneytextbox></span>
						</td>
					</tr>
                    <tr data-fsid="1204">
                        <td>
                            1204
                        </td>
                        <td nowrap>
                            City tax/stamps
                        </td>
                        <td nowrap>
                            <ml:PercentTextBox ID="cCountyRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="51px" data-fsid="sCountyRtcPc"></ml:PercentTextBox>
                            of
                            <asp:DropDownList ID="cCountyRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" data-fsid="sCountyRtcBaseT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cCountyRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="56px" data-fsid="sCountyRtcMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cCountyRtc" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cCountyRtcProps_ctrl" CCTemplateMode="true"
                            Page2Value="B8" PaidToBCBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1205">
                        <td>
                            1205
                        </td>
                        <td nowrap>
                            State tax/stamps
                        </td>
                        <td nowrap>
                            <ml:PercentTextBox ID="cStateRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="51px" data-fsid="sStateRtcPc"></ml:PercentTextBox>
                            of
                            <asp:DropDownList ID="cStateRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" data-fsid="sStateRtcBaseT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cStateRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="56px" data-fsid="sStateRtcMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cStateRtc" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cStateRtcProps_ctrl" CCTemplateMode="true"
                            Page2Value="B8" PaidToBCBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1206">
                        <td>
                            1206
                        </td>
                        <td>
                            <asp:TextBox ID="cU1GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="104" MaxLength="21" data-fsid="sU1GovRtcDesc"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:PercentTextBox ID="cU1GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="51px" data-fsid="sU1GovRtcPc"></ml:PercentTextBox>
                            of
                            <asp:DropDownList ID="cU1GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" data-fsid="sU1GovRtcBaseT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cU1GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="56px" data-fsid="sU1GovRtcMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU1GovRtc" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU1GovRtcProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B7" Page2Option1Value="B7" Page2Option2Text="B8" Page2Option2Value="B8" PaidToBCBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1207">
                        <td>
                            1207
                        </td>
                        <td>
                            <asp:TextBox ID="cU2GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="104px" MaxLength="21" Height="20px" data-fsid="sU2GovRtcDesc"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:PercentTextBox ID="cU2GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="51px" data-fsid="sU2GovRtcPc"></ml:PercentTextBox>
                            of
                            <asp:DropDownList ID="cU2GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" data-fsid="sU2GovRtcBaseT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cU2GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="56px" data-fsid="sU2GovRtcMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU2GovRtc" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU2GovRtcProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B7" Page2Option1Value="B7" Page2Option2Text="B8" Page2Option2Value="B8" PaidToBCBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1208">
                        <td>
                            1208
                        </td>
                        <td>
                            <asp:TextBox ID="cU3GovRtcDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="104px" MaxLength="21" Height="20px" data-fsid="sU2GovRtcDesc"></asp:TextBox>
                        </td>
                        <td nowrap>
                            <ml:PercentTextBox ID="cU3GovRtcPc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="percent" Width="51px" data-fsid="sU3GovRtcPc"></ml:PercentTextBox>
                            of
                            <asp:DropDownList ID="cU3GovRtcBaseT" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" data-fsid="sU3GovRtcBaseT">
                            </asp:DropDownList>
                            +
                            <ml:MoneyTextBox ID="cU3GovRtcMb" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="56px" data-fsid="sU3GovRtcMb"></ml:MoneyTextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU3GovRtc" runat="server" preset="money" Width="76px" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU3GovRtcProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B7" Page2Option1Value="B7" Page2Option2Text="B8" Page2Option2Value="B8" PaidToBCBVisible="false" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr>
                        <td class="FormTableSubheader">
                            1300
                        </td>
                        <td class="FormTableSubheader" colspan="14">
                            ADDITIONAL SETTLEMENT CHARGES
                        </td>
                    </tr>
                    <tr data-fsid="1302">
                        <td>
                            1302
                        </td>
                        <td>
                            Pest Inspection
                        </td>
                        <td>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cPestInspectF" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sPestInspectF"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cPestInspectFProps_ctrl" CCTemplateMode="true"
                            Page2Value="B6" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1303">
                        <td>
                            1303
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="cU1ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" data-fsid="sU1ScDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU1Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sU1Sc"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU1ScProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1304">
                        <td>
                            1304
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="cU2ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" Height="20px" data-fsid="sU2ScDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU2Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sU2Sc"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU2ScProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1305">
                        <td>
                            1305
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="cU3ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" data-fsid="sU3ScDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU3Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sU3Sc"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU3ScProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1306">
                        <td>
                            1306
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="cU4ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" data-fsid="sU4ScDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU4Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sU4Sc"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU4ScProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr data-fsid="1307">
                        <td>
                            1307
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="cU5ScDesc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" Width="95%" MaxLength="100" data-fsid="sU5ScDesc"></asp:TextBox>
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cU5Sc" onblur="unhighlightRow(this);" onfocus="highlightRow(this);"
                                runat="server" preset="money" Width="76px" data-fsid="sU5Sc"></ml:MoneyTextBox>
                        </td>
                        <uc1:GoodFaithEstimate2010RightColumn ID="cU5ScProps_ctrl" CCTemplateMode="true"
                            Page2Option1Text="B4" Page2Option1Value="B4" Page2Option2Text="B6" Page2Option2Value="B6"
                            Page2Option3Text="N/A" Page2Option3Value="NotApplicable" runat="server"></uc1:GoodFaithEstimate2010RightColumn>
                    </tr>
                    <tr>
                        <td colspan="3">
                            TOTAL ESTIMATED SETTLEMENT CHARGES
                        </td>
                        <td>
                            <ml:MoneyTextBox ID="cTotEstSc" runat="server" preset="money" Width="76" ReadOnly="True"></ml:MoneyTextBox>
                        </td>
                        <td nowrap colspan="3">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="m_okBtn" runat="server" Text="  OK  " OnClick="onOKClick"></asp:Button><input
                    onclick="closePage();" type="button" value="Close">
                <asp:Button ID="m_applyBtn" runat="server" Text="Apply"></asp:Button>
            </td>
        </tr>
    </asp:Panel>
</table>
<input type="hidden" id="m_hiddenChoice" value="" runat="server" name="m_hiddenChoice" />
<asp:HiddenField runat="server" ID="SavedName" />

<script type="text/javascript">
    function toggleRecFFields()
    {
      var cRecFLckd = <%= AspxTools.JsGetElementById(cRecFLckd)%>;
      var sp1201 = $("#sp1201");
      var cRecDeed = <%= AspxTools.JsGetElementById(cRecDeed)%>;
      var cRecMortgage = <%= AspxTools.JsGetElementById(cRecMortgage)%>;
      var cRecRelease = <%= AspxTools.JsGetElementById(cRecRelease)%>;
      
      if( cRecFLckd.checked )
      {
        sp1201.css('visibility','visible');
        cRecDeed.readOnly = true;
        cRecMortgage.readOnly = true;
        cRecRelease.readOnly = true;
      }
      else
      {
        sp1201.css('visibility','hidden');
        cRecDeed.readOnly = false;
        cRecMortgage.readOnly = false;
        cRecRelease.readOnly = false;
      }
    }
  
    function unload() {
        if (window.dialogArguments) {
            window.dialogArguments.Name = document.getElementById('<%= AspxTools.ClientId(SavedName) %>').value;
        }
    }
</script>
