using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.LoanPrograms;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice;
using LendersOffice.Constants;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.Template
{
	public partial  class LoanProgramArmDisclosure : System.Web.UI.UserControl , System.Web.UI.INamingContainer
	{

		private CLoanProductData m_Product = null;

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		public CLoanProductData DataSource
		{
			set { m_Product = value; }
			get { return m_Product; }
		}

		public string Height
		{
			set { m_Base.Height = Unit.Parse( value ); }
		}

		public string Width
		{
			set { m_Base.Width = Unit.Parse( value ); }
		}

		public string CssClass
		{
			set { m_Base.CssClass = value; }
		}

		/// <summary>
		/// Initialize this control.
		/// </summary>

		public void ControlInit( object sender , System.EventArgs a )
		{
			// Set visibility options.  The container binds, which
			// triggers are loading.

			try
			{
				// 7/15/2004 kb - Update control visibility for pricing engine.
				//
				// 1/19/2005 kb - We moved this logic into this user control,
				// so we transfer what applies to the margin parameter.

				if( BrokerUser.HasPermission( Permission.CanModifyLoanPrograms ) == true )
				{
					// Because pricing engine is enabled, show the links
					// and hide the simple edit.

					lRAdjMarginR.Visible    = false;
					lRAdjMarginLink.Visible = true;
				}
				else
				if( BrokerUser.HasFeatures( E_BrokerFeatureT.PricingEngine ) == false )
				{
					// Show simple, non-pricing engine ui that allows
					// for just one pair to be saved.

					lRAdjMarginLink.Visible = false;
					lRAdjMarginR.Visible    = true;
				}
				else
				{
					// User has pricing engine, but they can't use it, so hide
					// the rate point pair data.

					lRAdjMarginLink.Visible = false;

					lRAdjMarginR.Enabled  = false;
					lRAdjMarginR.ReadOnly = true;
					lRAdjMarginR.Visible  = true;
				}

				// 1/20/2005 kb - Populate the index drop down with what
				// the broker has designed.
				//
				// 1/21/2005 kb - We're using a client update approach,
				// so only initialize at the start.  When a user selects
				// an entry, we will populate the descriptions on the
				// client using cached content.

				if( IsPostBack == false )
				{
					ARMIndexDescriptorSet armIndexes = new ARMIndexDescriptorSet();

					try
					{
						armIndexes.Retrieve( BrokerUser.BrokerId );
					}
					catch( Exception e )
					{
						// Oops!

						Tools.LogError( "Failed to load control." , e );
					}

					foreach( ARMIndexDescriptor aDesc in armIndexes.Items )
					{
						ListItem lItem = new ListItem( aDesc.IndexNameVstr );

						lItem.Value = String.Format
							( "{0}|{1}|{2}"
							, aDesc.IndexIdGuid
							, aDesc.IndexBasedOnVstr
							, aDesc.IndexCanBeFoundVstr
							);

						lArmIndexGuid.Items.Add( lItem );
					}

                    if (BrokerUser.BrokerId != BrokerDB.PmlMaster) // (Avoid duplicating when editing in PMLMASTER).
                    {
                        foreach (SystemArmIndex armIndex in SystemArmIndex.ListSystemArmIndexes())
                        {
                            ListItem lItem = new ListItem(armIndex.IndexNameVstr + " (SYSTEM)");

                            lItem.Value = String.Format
                                ("{0}|{1}|{2}"
                                , armIndex.IndexIdGuid
                                , armIndex.IndexBasedOnVstr
                                , armIndex.IndexCanBeFoundVstr
                                );

                            lArmIndexGuid.Items.Add(lItem);

                        }
                    }


					lArmIndexGuid.Items.Insert( 0 , new ListItem( " <-- Select index --> " , String.Format( "{0}||" , Guid.Empty.ToString() )  ) );
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to init control." , e );
			}
		}

		/// <summary>
		/// Bind product data.  Pull the product information as is
		/// and overrite the current view values.
		/// </summary>

		public override void DataBind()
		{
			// Pull latest ui values from this product.

			try
			{
				// Refresh state of ui whenever parent calls.

				if( m_Product == null )
				{
					return;
				}

				lArmIndexBasedOnVstr.Text        = m_Product.lArmIndexBasedOnVstr;
				lArmIndexCanBeFoundVstr.Text     = m_Product.lArmIndexCanBeFoundVstr;
				lArmIndexAffectInitIRBit.Checked = m_Product.lArmIndexAffectInitIRBit;
                IsConvertibleMortgage.Checked = m_Product.IsConvertibleMortgage;
                
				lRAdj1stCapR.Text   = m_Product.lRadj1stCapR_rep;
				lRAdj1stCapMon.Text = m_Product.lRadj1stCapMon_rep;
				lRAdjCapR.Text      = m_Product.lRAdjCapR_rep;
				lRAdjCapMon.Text    = m_Product.lRAdjCapMon_rep;
				lRAdjLifeCapR.Text  = m_Product.lRAdjLifeCapR_rep;
				lRAdjFloorR.Text    = m_Product.lRAdjFloorR_rep;
                lRAdjFloorBaseT.SelectedIndex = lRAdjFloorBaseT.Items.IndexOf(lRAdjFloorBaseT.Items.FindByValue(m_Product.lRAdjFloorBaseT.ToString("D")));
				lRAdjRoundToR.Text  = m_Product.lRAdjRoundToR_rep;

				lRAdjMarginLink.Attributes.Add("onclick", "showModal('/los/Template/EditRateSheet.aspx?fileId=" + m_Product.lLpTemplateId + "'); return false;");

				lRAdjRoundT.SelectedIndex = lRAdjRoundT.Items.IndexOf( lRAdjRoundT.Items.FindByValue( m_Product.lRAdjRoundT.ToString( "D" ) ) );

				foreach( ListItem lItem in lArmIndexGuid.Items )
				{
					if( m_Product.lArmIndexGuid == new Guid( lItem.Value.Split( '|' )[ 0 ] ) )
					{
						lArmIndexGuid.SelectedIndex = lArmIndexGuid.Items.IndexOf( lItem );

						break;
					}
				}

				if( m_Product.lArmIndexGuid != Guid.Empty )
				{
                    if (m_Product.lBaseLpId != Guid.Empty) 
                    {
                        // 12/19/2005 dd - This loan product is derive. Therefore it will not contains ARM index
                        // from current broker. Just insert inherit value to drop down list.
                        lArmIndexGuid.Items.Add(new ListItem(m_Product.lArmIndexNameVstr, m_Product.lArmIndexGuid.ToString()));
                        lArmIndexGuid.SelectedIndex = lArmIndexGuid.Items.Count - 1;
                    } 
                    else 
                    {
                        Tools.Assert( m_Product.lArmIndexNameVstr == lArmIndexGuid.SelectedItem.Text.Replace(" (SYSTEM)", "") , String.Format
                            ( "Hey! Loan product {0} has an arm index name that doesn't match the real arm index '{1}'."
                            , m_Product.lLpTemplateId
                            ,  lArmIndexGuid.SelectedItem.Text
                            ));
                    }
				}

                if (!m_Product.IsLpe)
                {
                    //opm 41603 fs 03/23/10
                    lRAdjMarginR.Enabled = true;
                    lRAdjMarginR.ReadOnly = false;

                    try
                    {
                        IRateItem[] rates = m_Product.GetRawRateSheetWithDeltas();
                        if (rates != null && rates.Length > 0)
                        {
                            lRAdjMarginR.Text = rates[0].Margin_rep;
                        }
                    }
                    catch (CRateOptionNotFoundException){ }
                }
                else
                {
                    if (BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == false)
                    {
                        // Get the first rate and point from ratesheet to populate
                        // rate and point textfield.  Even if the pricing engine
                        // is enabled, and we don't show these guys, it would be
                        // good to have them initialized so if the pricing engine
                        // is turned off during edit, we will save the top entry,
                        // rather than 0%, 0%.
                        //
                        // 1/19/2005 kb - This also applies to the more recently
                        // added margin.
                        IRateItem[] rates = m_Product.GetRawRateSheetWithDeltas();

                        if (rates != null && rates.Length > 0)
                        {
                            lRAdjMarginR.Text = rates[0].Margin_rep;
                        }
                    }

                }

				lArmIndexNotifyNotBeforeDaysVstr.Text = m_Product.lArmIndexNotifyNotBeforeDaysVstr;
				lArmIndexNotifyAtLeastDaysVstr.Text   = m_Product.lArmIndexNotifyAtLeastDaysVstr;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to data bind product data." , e );
			}
		}

		/// <summary>
		/// Write back current view values to the bound model object.
		/// Make sure you call after view is loaded during postback.
		/// </summary>

		public virtual void DataSave()
		{
			// Write latest ui values to this product.

			try
			{
				// Commit values as is.  If nothing bound, then skip
				// request.  We pull arm index details from the table
				// (like fannie mae index code) so we can propagate
				// to a loan when we bind to it for submission.

				if( m_Product == null )
				{
					return;
				}

				m_Product.lArmIndexBasedOnVstr     = lArmIndexBasedOnVstr.Text;
				m_Product.lArmIndexCanBeFoundVstr  = lArmIndexCanBeFoundVstr.Text;
				m_Product.lArmIndexAffectInitIRBit = lArmIndexAffectInitIRBit.Checked;
                m_Product.IsConvertibleMortgage = IsConvertibleMortgage.Checked;
                

				if( lArmIndexGuid.SelectedItem != null )
				{
					// 1/21/2005 kb - Load the broker's index set and find the
					// match.  On match, we load all the carry-along values into
					// this loan product.

					ARMIndexDescriptorSet armIndexes = new ARMIndexDescriptorSet();

					try
					{
						armIndexes.Retrieve( BrokerUser.BrokerId );
					}
					catch( Exception e )
					{
						// Oops!

						Tools.LogError( e );
					}

					m_Product.lArmIndexGuid = Guid.Empty;

					foreach( ARMIndexDescriptor aDesc in armIndexes.Items )
					{
						// Look for a match and pull out the associated index
						// values so we can carry them over to new loans when
						// we bind through submission.  If no match, we clear
						// these values.

						if( aDesc.IndexIdGuid == new Guid( lArmIndexGuid.SelectedItem.Value.Split( '|' )[ 0 ] ) )
						{
							m_Product.lArmIndexNameVstr = aDesc.IndexNameVstr;
							m_Product.lArmIndexT        = aDesc.FannieMaeArmIndexT;
							m_Product.lFreddieArmIndexT = aDesc.FreddieArmIndexT;

							m_Product.lArmIndexEffectiveD_rep = aDesc.EffectiveD.ToString();

							m_Product.lArmIndexGuid = aDesc.IndexIdGuid;

							break;
						}
					}

                    if (m_Product.lArmIndexGuid == Guid.Empty)
                    {
                        // Maybe they use a system index
                        foreach (SystemArmIndex armIndex in SystemArmIndex.ListSystemArmIndexes())
                        {
                            if (armIndex.IndexIdGuid == new Guid(lArmIndexGuid.SelectedItem.Value.Split('|')[0]))
                            {
                                m_Product.lArmIndexNameVstr = armIndex.IndexNameVstr;
                                m_Product.lArmIndexT = armIndex.FannieMaeArmIndexT;
                                m_Product.lFreddieArmIndexT = armIndex.FreddieArmIndexT;

                                m_Product.lArmIndexEffectiveD_rep = armIndex.EffectiveD.ToString();

                                m_Product.lArmIndexGuid = armIndex.IndexIdGuid;

                                break;
                            }
                            
                        }

                    }

					if( m_Product.lArmIndexGuid == Guid.Empty )
					{
						// Ok.  Nothing matched, so let's reset the entry.
						// If the list item defines something novel, then
						// we have a bug (or someone is editing the arm
						// index list between postbacks).

						m_Product.lArmIndexEffectiveD_rep = SmallDateTime.MinValue.ToShortDateString();

						m_Product.lArmIndexT        = E_sArmIndexT.LeaveBlank;
						m_Product.lFreddieArmIndexT = E_sFreddieArmIndexT.LeaveBlank;

						m_Product.lArmIndexNameVstr = "";
					}
				}

				m_Product.lRAdjRoundT = ( E_sRAdjRoundT ) Convert.ToInt32( lRAdjRoundT.SelectedItem.Value );

				m_Product.lRadj1stCapR_rep   = lRAdj1stCapR.Text;
				m_Product.lRadj1stCapMon_rep = lRAdj1stCapMon.Text;
				m_Product.lRAdjCapR_rep      = lRAdjCapR.Text;
				m_Product.lRAdjCapMon_rep    = lRAdjCapMon.Text;
				m_Product.lRAdjLifeCapR_rep  = lRAdjLifeCapR.Text;
				m_Product.lRAdjFloorR_rep    = lRAdjFloorR.Text;
                m_Product.lRAdjFloorBaseT = (E_sRAdjFloorBaseT) Convert.ToInt32(lRAdjFloorBaseT.SelectedItem.Value);
				m_Product.lRAdjRoundToR_rep  = lRAdjRoundToR.Text;

				if( BrokerUser.HasPermission( Permission.CanModifyLoanPrograms ) == false )
				{
					if( m_Product.RateSheetWithoutDeltas == null || m_Product.RateSheetWithoutDeltas.Length == 0 )
					{
						// 1/19/2005 kb - We now write back only the margin
						// portion of an initial rate entry.

						CRateSheetFields rate = m_Product.GetRateSheetFieldsWithoutDeltas( Guid.Empty );

						rate.Point = 0;
						rate.Rate  = 0;
						
						rate.Margin_rep = lRAdjMarginR.Text;

						rate.Update();
					}
					else
					{
						// 1/19/2005 kb - Get the first entry and write out
						// the margin component.

						CRateSheetFields rate = m_Product.RateSheetWithoutDeltas[ 0 ];

						rate.Margin_rep = lRAdjMarginR.Text;

						rate.Update();
					}
				}

				m_Product.lArmIndexNotifyNotBeforeDaysVstr = lArmIndexNotifyNotBeforeDaysVstr.Text;
				m_Product.lArmIndexNotifyAtLeastDaysVstr   = lArmIndexNotifyAtLeastDaysVstr.Text;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to data save product data." , e );
			}
		}

        // Per OPM 13035, SAEs need a special view of this UI
        // to prevent mistakes that lead to inaccuracies.
        protected bool m_isAuthorCreateMode = false;
        public void SetAuthorCreateMode()
        {
            m_isAuthorCreateMode = true;

            // "If �Amort. Type = ARM� require �ARM Change, ARM Caps, and ARM Index� settings to be made before loan program can be created."
            lRAdj1stCapMonValidator.Enabled = true;
            lRAdj1stCapRValidator.Enabled = true;
            lRAdjCapRValidator.Enabled = true;
            lRAdjLifeCapRValidator.Enabled = true;
            lArmIndexGuidValidator.Enabled = true;

            //�	Require entries in both the �Rate Floor� fields in the ARM section when Amort type is ARM.
            //  o	The dropdown defaults to the blank value but requires selection of one of the non-blank values.
            lRAdjFloorBaseT.Items.Insert(0,new ListItem("", "-1"));
            lRAdjFloorBaseT.SelectedIndex = 0;
            lRAdjFloorBaseTValidator.Enabled = true;
            //  o	Number field defaults to blank, requires entry of any number, positive or negative or zero, formatted as a percent.
            lRAdjFloorR.Text = string.Empty;
            lRAdjFloorBaseTValidator.Enabled = true;

            // � Require the �Round� dropdown and text field in the ARM section required when Amort type is ARM.
            //  o	Default values for both should be blank, and requires a value to be selected or entered (0 counts as a value).
            lRAdjRoundT.Items.Insert(0, new ListItem("", "-1"));
            lRAdjRoundT.SelectedIndex = 0;
            lRAdjRoundTValidator.Enabled = true;

            lRAdjRoundToR.Text = string.Empty;
            lRAdjRoundToRValidator.Enabled = true;

        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Init += new System.EventHandler(this.ControlInit);
		}
		#endregion

	}

}