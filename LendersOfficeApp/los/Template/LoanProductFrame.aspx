<%@ Page language="c#" Codebehind="LoanProductFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.LoanProductFrame" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server"><TITLE>LoanProductFrame</TITLE>
    <style type="text/css">
        form {margin-bottom: 0;}
        iframe {width: 100%; height: 100%; border: none; min-height: 0; min-width: 0;}

        iframe, div {
            box-sizing: border-box;
        }

        body {margin: 0; padding: 0;}

        iframe[name='folder'] {
            display: flex;
            border: 0 solid gainsboro;
            -ms-flex: 0 0 23px;
            -webkit-flex: 0 0 23px; 
            flex: 1 0 23px;
        }

        iframe[name='programs'] {
            display: flex;
            border: 0 solid gainsboro;
            -ms-flex: 0 0 40px; 
            -webkit-flex: 0 0 40px; 
            flex: 4 0 40px;
        }

        .main-container {
            height: 100%;
            width: 100%;
            display: flex;
            -ms-flex-direction: row;
            -webkit-flex-direction: row;
            flex-direction: row;
        }
    </style>
  </HEAD>
    <body>
        <form runat="server">
            <div class="main-container body-container">
                <iframe name="folder" src="LoanProductFolder.aspx"></iframe>
                <div onmousedown="triggerTreeviewResize();" class="DragBar"></div>
                <iframe id="loanProgramList" name="programs" src="LoanProgramList.aspx"></iframe>
            </div>
            <div id='CalculationOverlay' class="Hidden"></div>
        </form>        
    </body>    
</HTML>
