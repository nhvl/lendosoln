<%@ Page language="c#" Codebehind="EditDisabledLoanProgramInfo.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.EditDisabledLoanProgramInfo" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Edit Disabled Loan Program Info</title>
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<STYLE type="text/css">
		.contextoverridden { FONT-WEIGHT: bold }
		</STYLE>
  </head>
  <body onload="init();" MS_POSITIONING="FlowLayout">
  
  <script language="javascript">
		<!--
		function init()
		{
			resize(600, 600);
		}
		
		function f_onOkClick()
		{
		    document.getElementById("m_Command").value = "ok";
		    var okBtn = document.getElementById("m_OK");
			okBtn.disabled = false;
			okBtn.click();
		}
		//-->
		</script>
	<h4 class="page-header">Edit Disabled Loan Program Info</h4>
    <form id="EditDisabledLoanProgramInfo" method="post" runat="server">
    <input type="hidden" runat="server" id="m_Command" NAME="m_Command"> 
		<DIV style="PADDING: 4px;">
				<SPAN style="MARGIN-RIGHT: 8px;">
					<input type=button id=m_OKSeen" onclick="f_onOkClick();" value="OK" style="width:40px">
					<asp:button id="m_OK" style="display:none" Enabled="false" runat="server" onkeydown="if (event.keyCode == 13) { f_onOkClick(); return false; }" Text="  OK  " OnClick="OKClick"></asp:button>
					<INPUT onclick="onClosePopup();" type="button" value="Cancel"> </SPAN>
				<ML:EncodedLabel id="m_ErrorMessage" runat="server" Font-Size="12px" ForeColor="Red"></ML:EncodedLabel>
			</DIV>
			<HR style="MARGIN-LEFT: 4px; MARGIN-RIGHT: 4px;">
			<TABLE cellSpacing="1" cellPadding="5" border="0">
				<TR>
					<TD>Investor Name</TD>
					<TD>
						<asp:DropDownList AutoPostBack=True ID="m_InvestorName" runat="server" ></asp:DropDownList>
						<asp:requiredfieldvalidator id="m_rfvinvname" runat="server" ControlToValidate="m_InvestorName" Display="Dynamic" ErrorMessage="This field is required."></asp:requiredfieldvalidator>
					</TD>
				</TR>
				<TR>
					<TD>Product Code</TD>
					<TD>
						<asp:DropDownList id="m_ProductCode" runat="server"></asp:DropDownList>
						<asp:requiredfieldvalidator id="m_rfvpcode" runat="server" ControlToValidate="m_ProductCode" Display="Dynamic" ErrorMessage="This field is required."></asp:requiredfieldvalidator>
					</TD>
				</TR>
				<TR>
					<TD>Disabled Status</TD>
					<TD>
						<asp:textbox id="m_DisabledStatus" Readonly=True onkeydown="if (event.keyCode == 13) { f_onOkClick(); return false; }" runat="server" MaxLength="65">NoPriceNoDisq</asp:textbox>
						<asp:requiredfieldvalidator id="m_rfvstatus" runat="server" ControlToValidate="m_DisabledStatus" Display="Dynamic" ErrorMessage="This field is required."></asp:requiredfieldvalidator>
					</TD>
				</TR>
				<TR>
					<TD>SAE Name</TD>
					<TD>
						<asp:textbox id="m_SaeName" onkeydown="if (event.keyCode == 13) { f_onOkClick(); return false; }" runat="server" MaxLength="65"></asp:textbox>
						<asp:requiredfieldvalidator id="m_rfvsaename" runat="server" ControlToValidate="m_SaeName" Display="Dynamic" ErrorMessage="This field is required."></asp:requiredfieldvalidator>
					</TD>
				</TR>
				<TR>
					<TD valign=top>Notes</TD>
					<TD>
						<asp:textbox id="m_Notes" onkeydown="if (event.keyCode == 13) { f_onOkClick(); return false; }" runat="server" TextMode=MultiLine Height=100px Width=300px></asp:textbox>
					</TD>
				</TR>
			</TABLE>
     </form>
	<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
  </body>
</html>
