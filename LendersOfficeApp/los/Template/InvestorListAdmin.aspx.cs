using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;
using System.Data.SqlClient;
using LendersOffice.Events;

namespace LendersOfficeApp.los.Template
{
	/// <summary>
	/// Summary description for InvestorListAdmin.
	/// </summary>
	public partial class InvestorListAdmin : LendersOffice.Common.BasePage
	{

		protected void PageLoad( object sender , System.EventArgs a )
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

	}
}