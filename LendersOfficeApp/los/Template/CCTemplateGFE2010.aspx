<%@ Register TagPrefix="uc1" TagName="CCTemplateGFE2010" Src="CCTemplateGFE2010.ascx" %>
<%@ Page language="c#" Codebehind="CCTemplateGFE2010.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.CCTemplateGFE2010" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="DataAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>CCTemplate</title>
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout" onunload="if(unload){unload();}" >
    <script type="text/javascript">
    <!--
      var first = true;
      var ctrlName = 'CCTemplate1_';
      var linkedPaidTo = new Array("cApprFProps_ctrl_PaidTo_tb"
                                  ,"cCrFProps_ctrl_PaidTo_tb"
                                  ,"cFloodCertificationFProps_ctrl_PaidTo_tb"
                                  ,"cEscrowFProps_ctrl_PaidTo_tb"
                                  ,"cTitleInsFProps_ctrl_PaidTo_tb"
                                  ,"cAttorneyFProps_ctrl_PaidTo_tb"
                                  );
    
      function _init() {
        resize(1200, 1000);
        if (document.getElementById('CCTemplate1_cCcTemplateNm') != null)
			    document.getElementById('CCTemplate1_cCcTemplateNm').focus();
			
		verifyPaidToTBStatus();
		verifyLinkedPaidToTBStatus(document.getElementById(ctrlName + 'cGfeUsePaidToFromOfficialContact'));
		oncreditupdate(document.getElementById(ctrlName + 'cPricingEngineCreditT'));
		var elements = document.forms[0];
		for (var j = 0; j < elements.length; j++) {
		  var id = elements[j].id;
		  clientID = id.substring(0, id.indexOf('_Poc_chk'));
		  if (clientID != '' && typeof (enableDisableDflp) == 'function')
		    enableDisableDflp(clientID);
        }
		
		if (first) {
		    first = false;
		        addEventHandler(document.getElementById(ctrlName + 'cGfeVersion_0'), "click", launchOtherVersion, false);
		}

		toggleRecFFields();
		
		window.scrollTo(0, 0);
      }
      
      function verifyPaidToTBStatus()
      {
        unlockPaidTo(ctrlName + 'cTxServFProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cInspectFProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cProcFProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cUwFProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cWireFProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'c800U1FProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'c800U2FProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'c800U3FProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'c800U4FProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'c800U5FProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cOwnerTitleInsFProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cDocPrepFProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cNotaryFProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cU1TcProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cU2TcProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cU3TcProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cU4TcProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cPestInspectFProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cU1ScProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cU2ScProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cU3ScProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cU4ScProps_ctrl_ToBrok_chk');
        unlockPaidTo(ctrlName + 'cU5ScProps_ctrl_ToBrok_chk');
      }
      
      function verifyLinkedPaidToTBStatus(cb) {
        var i, id, cb, tb, tbID;
        for (i in linkedPaidTo) {
          tbID = ctrlName + linkedPaidTo[i];
          tb = document.getElementById(tbID);
          id = tbID.substring(0, tbID.indexOf('_PaidTo_tb'));
          Brok_cb = document.getElementById(id + '_ToBrok_chk');
          
          if (Brok_cb.checked)
            tb.readOnly = 'true';
          else
            tb.readOnly = cb.checked;
        }
      }

      function oncreditupdate(el) {
          if (!el) { return; }
          var el2 = document.getElementById(ctrlName + 'cPricingEngineLimitCreditToT');
          if (el.value == '1') {
              el2.value = 0;
              el2.disabled = true;
          }
          else {
              el2.disabled = false;
          }
      }

      function unlockPaidTo(id) {
        var tbID = id.substring(0, id.indexOf('_ToBrok_chk')) + '_PaidTo_tb';
        var LinkCbChecked = document.getElementById(ctrlName + 'cGfeUsePaidToFromOfficialContact').checked;
        if(document.getElementById(tbID) && (!LinkCbChecked || !checkLinkedPaidTo(tbID)))
          document.getElementById(tbID).readOnly = document.getElementById(id).checked;
      }
      
      function checkLinkedPaidTo(tbID) {
        var i = "";
        for (i in linkedPaidTo) {
          if (tbID == ctrlName + linkedPaidTo[i])
            return true;
        }
        return false;
      }
      
      function launchOtherVersion() {
        var choice = ConfirmSave();
        if (choice == 2) { // cancel
          document.getElementById(ctrlName + 'cGfeVersion_0').checked = false;
          document.getElementById(ctrlName + 'cGfeVersion_1').checked = true;
          return;
        }
        else if (choice == 6) { // yes
          document.getElementById(ctrlName + 'm_hiddenChoice').value = "Save";
          document.forms[0].submit();
        }
        else { // no
          document.getElementById(ctrlName + 'm_hiddenChoice').value = "Don't Save";
          document.forms[0].submit();
        }
    }

    function openConditionTemplateEditor(id, tmplid, nm, fid) {
        var url = '/los/template/ClosingCost/TemplateCondition.aspx?Id=' + id + '&TemplateId=' + tmplid + '&nm=' + nm;
        var el = document.getElementById(fid);
        if (el.value.length > 0) {
            url = url + "&k=" + el.value;
        }
        
        showModal(url, null, null, null, function(args){
          if (args.key) {
            el.value = args.key;
            document.getElementById('ConditionTxt').innerHTML = args.msg;
          }
        }, { hideCloseButton:true });
      }

    function closePage() {
        onClosePopup(); //todo think about passing data or saving a refresh request...
    }
//-->
		</script>

    <form id="CCTemplate" method="post" runat="server">
      <uc1:CCTemplateGFE2010 id="CCTemplate1" runat="server"></uc1:CCTemplateGFE2010>
      <uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
  </body>
</HTML>
