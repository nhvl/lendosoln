<%@ Control Language="c#" AutoEventWireup="false" Codebehind="InvestorListControl.ascx.cs" Inherits="LendersOfficeApp.los.Template.InvestorListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script>
	function onCloseClick() { try
	{
	  var args = window.dialogArguments || {};
	  args.OK = true;
	  onClosePopup(args);
	}
	catch (e)
	{
	}}

	// Stores scroll position by storing the div's scrollTop value in the hidden input m_scrollPosition
	function storeScrollPosition() {
		var currentScrollPosition = document.getElementById('dataDiv').scrollTop;
		var savedScrollPosition = <%= AspxTools.JsGetElementById(m_scrollPosition) %>;
		savedScrollPosition.value = currentScrollPosition;
	}

	// Restores scroll position by setting the div's scrollTop value equal to m_scrollPosition's value
	function restoreScrollPosition() {
		var savedScrollPosition = <%= AspxTools.JsGetElementById(m_scrollPosition) %>.value;
		document.getElementById('dataDiv').scrollTop = savedScrollPosition;
	}

	window.onload = restoreScrollPosition;
</script>
<h4 class="page-header">Investor List</h4>
<input type="hidden" name="m_scrollPosition" id="m_scrollPosition" value="0" runat="server">
<table height="100%" cellSpacing="2" cellPadding="3" width="100%" border="0">
	<tr vAlign="top">
		<td height="100%">
			<div id="dataDiv" style="BORDER-RIGHT: 2px; PADDING-RIGHT: 4px; BORDER-TOP: lightgrey 2px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 100%; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; HEIGHT: 100%" onscroll="storeScrollPosition();"><ml:commondatagrid id="m_InvestorDG" runat="server" OnItemCommand="OnItemCommand_InvestorDG" EnableViewState="true">
					<AlternatingItemStyle cssclass="GridAlternatingItem"></AlternatingItemStyle>
					<ItemStyle cssclass="GridItem"></ItemStyle>
					<HeaderStyle cssclass="GridHeader"></HeaderStyle>
					<Columns>
					    <asp:BoundColumn DataField="InvestorId" SortExpression="InvestorId" HeaderText="Investor ID" ReadOnly="true"></asp:BoundColumn>
						<asp:BoundColumn DataField="InvestorName" SortExpression="InvestorName" HeaderText="Investor Name" ReadOnly="True"></asp:BoundColumn>
						<asp:TemplateColumn HeaderText="Valid" SortExpression="IsValid">
							<ItemTemplate>
                                <%# AspxTools.HtmlControl(CreateIsValidCheckbox(Container.DataItem)) %>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:CheckBox id="isValidCb" runat="server" Checked='<%# AspxTools.GetBool((bool)DataBinder.Eval( Container.DataItem, "IsValid")) %>' />
							</EditItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="RateLockCutOffTime">
							<ItemTemplate>
								<%#AspxTools.HtmlString(RenderCutOffTime(Container.DataItem))%>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:TextBox runat="server" id="edtRateLockCutOffTime" Text='<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "RateLockCutOffTime", "{0:T}" ).ToString()) %>'>
								</asp:TextBox>
								(PST) or&nbsp;<asp:CheckBox id="UseLenderTimezoneForRsExpirationCb" Runat="server" Text="Use Lender's Timezone" Checked='<%# AspxTools.GetBool((bool)DataBinder.Eval( Container.DataItem, "UseLenderTimezoneForRsExpiration")) %>' />
							</EditItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn>
							<ItemTemplate>
								<asp:LinkButton runat="server" Text="edit" CommandName="edit" CausesValidation="false"></asp:LinkButton>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:LinkButton runat="server" Text="update" CommandName="update"></asp:LinkButton>
								<asp:LinkButton runat="server" Text="cancel" CommandName="cancel" CausesValidation="false"></asp:LinkButton>
							</EditItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</ml:commondatagrid>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			Investor Name:&nbsp;<asp:textbox id="m_InvestorAddTB" runat="server"></asp:textbox>&nbsp;<asp:button id="m_InvestorAddBTN" onclick="OnInvestorAddClick" runat="server" Text="Add"></asp:button>
			<br>
            <span id="Result" runat="server" enableviewstate="false"></span>
		</td>
	</tr>
	<tr valign="top">
		<td align="center" colSpan="1">
			<hr>
			<asp:Panel id="m_CloseButtonPanel" runat="server">
				<INPUT id="m_btnClose" onclick="onCloseClick();" type="button" value="Close">
			</asp:Panel>
		</td>
	</tr>
</table>
<uc1:cModalDlg runat="server" id="CModalDlg1"></uc1:cModalDlg>
