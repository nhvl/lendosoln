using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using DataAccess;
using LendersOffice.RatePrice;

namespace LendersOfficeApp.los.Template
{
	// 4/23/07 mf OPM 11856.  List the derivation job queue contents here
	// Users can also cancel pending jobs or re-queue failed ones.
	public partial class DerivationJobList : LendersOffice.Common.BasePage
	{
		protected System.Web.UI.WebControls.TextBox m_threadStatus;
		protected System.Web.UI.WebControls.Button m_processorBtn;

		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

			BindJobGrid();
			if ( m_jobDg.Items.Count == 0 )
			{
				m_jobDg.Visible = false;
				m_status.Text = "No jobs currently in queue";
			}
		}

        private void BindJobGrid()
		{
            DataSet ds = new DataSet();
            DataSetHelper.Fill(ds, DataSrc.LOTransient, "ListDeriveJobs", null);

			m_jobDg.DataSource = ds.Tables[0].DefaultView;
			m_jobDg.DataBind();
		}

		protected void JobItemCommand( Object sender, DataGridCommandEventArgs e )
		{
			int jobId = (int) m_jobDg.DataKeys[e.Item.ItemIndex];
			if ( e.CommandName == "cancel" )
			{
				DerivationJob.DeleteJob( jobId, false );

			}
			if ( e.CommandName == "enqueue" )
			{
				DerivationJob.EnqueueJob( jobId );
			}
			BindJobGrid();
		}

		protected void JobItemDataBound( Object sender, DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item ||
				e.Item.ItemType == ListItemType.AlternatingItem)
			{

				switch (e.Item.Cells[2].Text)
				{ 
					case "InProgress":
						e.Item.Cells[2].Style.Add("FONT-WEIGHT", "bold");
						e.Item.Cells[2].Text += " (started " + DataBinder.Eval(e.Item.DataItem, "JobStartD").ToString() + ")";
						e.Item.Cells[0].Text = string.Empty;
						e.Item.Cells[1].Text = string.Empty;
						break;
					case "Error":
						string errorText = GetErrorText( DataBinder.Eval(e.Item.DataItem, "JobXmlContent").ToString() );
						e.Item.Cells[2].Text = "<a href='#' onclick=\"this.nextSibling.style.display = ( this.nextSibling.style.display == 'none' ? 'block' : 'none')\">Error</a><textarea style='height:130px;width:100%;display:none' readonly='readonly'>" + errorText + "</textarea>";
						break;
					case "Pending":
						e.Item.Cells[1].Text = string.Empty;
						break;
				}
			}
		}

		private string GetErrorText( string xmlContent )
		{
			// We just want to pull out the errors for listing
			System.Text.StringBuilder errors = new System.Text.StringBuilder();
			XmlDocument doc = Tools.CreateXmlDoc(xmlContent);

			XmlNodeList errorList = doc.SelectNodes("//DeriveJob/ErrorList/Error");
			
			foreach(XmlElement errorNode in errorList)
			{
				errors.AppendFormat( "{0}:{2}{1}" + (errors.Length > 0 ? "" : "{2}"), errorNode.GetAttribute("Time"), errorNode.GetAttribute("Message"), Environment.NewLine );
			}

			return errors.ToString();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
