using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.RatePrice;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Template
{
    public partial class LoanProgramList : LendersOffice.Common.BasePage
	{
		protected string m_masterId;
 // 11/24/08 fs - Replaced dropdownlist for vs2008 migration
		protected bool m_hasSubfolders = false;

		#region ( Server-side injection helpers )

		protected string GetTemplateId( object oItem )
		{
			return ( oItem as DataRowView )[ "lLpTemplateId" ].ToString();
		}

		protected string GetTemplateNm( object oItem )
		{
			return ( oItem as DataRowView )[ "lLpTemplateNm" ].ToString();
		}

	//  --- OPM 16984 Cord, more injection helpers for using
	//	--- the product code dropdownlist.
	  
	

		// Number of investors having valid product codes.
		protected int GetInvestorCount()
		{
			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataAccess.DataSrc.LpeSrc, "ListInvestorWithValidProductCode"))
			{
				if( reader.Read() )
				{
					return reader.GetInt32(0);
				}
				else
				{
					return -1;
				}
			}			
		}

		// OPM 16984 Cord
		// Make an injected string from 
		// codebehind safe for JavaScript.
		// Note: I avoided the Utilities' safe string methods
		// in order to also check for " and \.
		// Only works for strings using double-quotation marks
		// e.g. var myString = "<%= SafeJsString(m_String) %>"
		protected String SafeJsString(String s)
		{
			if ( s != null )
			{
				//Replace \ with \\
				s = s.Replace("\\", @"\\");

				//Replace ' with \'
				s = s.Replace("'", "\\'");

				//Replace " with \"
				s = s.Replace("\"", "\\\"");
			}

			return s;
		}

		// Makes an injected string from 
		// codebehind safe for a JavaScript
		// call from within html in this context:
		// e.g. <input id="x" onclick="fubar('<%=htmlSafeJsString(m_String)%>');" >
		protected String htmlSafeJsString(String s)
		{
			// Makes sure pre-existing \ don't get 
			// parsed as escape characters elsewhere.
			// Replace \ with \\  
			if ( s != null )
			{
				//Replace \ with \\
				s = s.Replace("\\", "\\\\");
			
				// Replace ' with \'
				s = s.Replace("'", "\\'");

				// Replace	" with &quot;
				//			& with &amp;
				// and all those other html characters
				s = Utilities.SafeHtmlString(s);
			}

			return s;
		}

		// Injects javascript arrays for Investors and ProductCodes.
		// It grabs the investor/product pairs from the lpe db
		// and generates parallel arrays InvestorArray and ProductArray
		// where InvestorArray[i] is the name of the investor who
		// has the valid products in the array stored at ProductArray[i]
		protected void InjectInvestorProductArrays()
		{
			if ( CurrentUser.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms)) 
			{
				// InvestorArray = new Array( ... );
				Response.Write( "InvestorArray = new Array(" + GetInvestorCount() + ");" );
				
				// ProductArray = new Array(InvestorArray.length);
				Response.Write( "ProductArray = new Array(InvestorArray.length);" );
				    
				
				using ( DbDataReader dR = DataAccess.StoredProcedureHelper.ExecuteReader(DataAccess.DataSrc.LpeSrc, "ListInvestorProductWithValidProductCode") )
				{
					//loop counter
					int count = -1;
					
					//loop controls
					String currentInvestor = "";
					String lastInvestor = "";
						
					if( dR.Read() )
					{
						count++;	
						currentInvestor = dR.GetString(0);
						lastInvestor = currentInvestor;

						//Add new Investor		
						// InvestorArray[0] = "currentInvestor";
						Response.Write( "InvestorArray[0] = \"" + SafeJsString(currentInvestor) + "\";" );
					
						//Begin new array in products
						// ProductArray[0] = new Array(
						Response.Write( "ProductArray[0] = new Array(" );

						//Add the first entry to products
						// "productCode"
						Response.Write( "\"" + SafeJsString(dR.GetString(1)) + "\"" );
							
						while( dR.Read() )
						{
							currentInvestor = dR.GetString(0);
					
							if( lastInvestor != currentInvestor )
							{
								//--If we have finished adding all the 
								// product codes of the previous investor
								
								//End current product array declaration
								// );
								Response.Write( ");" );
									
								lastInvestor = currentInvestor;
								count++;		
					
								//Add new investor
								// InvestorArray[count] = "currentInvestor";
								Response.Write( "InvestorArray[" + count + "] = \"" + SafeJsString(currentInvestor) + "\";" );
					
								//Begin new array in products
								// ProductArray[count] = new Array(
								Response.Write( "ProductArray[" + count + "] = new Array(" );
						
								//Add the first entry to products
								// "productCode"
								Response.Write( "\"" + SafeJsString(dR.GetString(1)) + "\"" );
							}
							else
							{
								//--If we still have more product codes
								// to add for the current investor

								// Add another product to the existing list
								// , "productCode"
								Response.Write( ", \"" + SafeJsString(dR.GetString(1)) + "\"" );
							}
						} // end while
						
						//End the final ProductArray's array declaration
						// );
						Response.Write( ");" );
					
					} //end if

				Response.Write( "populateProductCodes();" );

				} //end using
			
				
			} //end if
		}

		#endregion

		protected BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{
			// Write out popup message to alert user of error.
			set { ClientScript.RegisterHiddenField( "m_errorMessage" , value ); }
		}

		private String FeedBack
		{
			// Write out popup message to alert user of error.

			set { ClientScript.RegisterHiddenField( "m_feedBack" , value ); }
		}

		protected bool m_pricingVisible 
		{
			get { return CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ); }
		}

		protected Guid m_folderID 
		{
			get { return RequestHelper.GetGuid("folderid", Guid.Empty); }
		}

		private Guid m_brokerID 
		{
			get { return /*((BrokerUserPrincipal) User)*/ BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
		}

		protected string GetFinanceMethod( object fMeth )
		{
			try
			{
				return Convert.ToString( ( E_sFinMethT ) Convert.ToInt32( fMeth ) );
			}
			catch
			{
				return "?";
			}
		}

		private void SetHasSubfolders()
		{
			SqlParameter[] parameters = {
											new SqlParameter("@BrokerId", m_brokerID),
											new SqlParameter("@FolderID", m_folderID)
										};
				
			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "DoesLoanProductFolderHaveChildren", parameters))
			{
				if(reader.Read()) 
					m_hasSubfolders = true;
			}
		}

		private void GetFolderCreateDate()
		{
            // 11/16/2007 dd - Reviewed and safe
            var listParams = new SqlParameter[] { new SqlParameter("@p", this.m_folderID) };
            string query = "SELECT CASE WHEN CreatedD < '2001' THEN '' ELSE Convert(varchar, CreatedD, 9) END as CreatedDate FROM Loan_Product_Folder WHERE FolderId = @p";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    m_folderCreateD.Text = reader["CreatedDate"].ToString() == string.Empty ? "" : " (Folder Created " + reader["CreatedDate"].ToString() + ")";
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, query, null, listParams, readHandler);
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");

            if ( Page.IsPostBack == false )
			{
				BindInvestorList();
				
				// 04/23/07 mf. OPM 11856  SREs should see the creation date of this folder
				if ( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) )
					GetFolderCreateDate();
			}

			
			BindDataGrid();
			
			SetHasSubfolders();

			if (!Page.IsPostBack) 
			{
				if (m_programsDG.Items.Count == 0) 
				{
					m_programsDG.Visible = false;
					m_noRecordLabel.Visible = true;
				} 
				else 
				{
					m_programsDG.Visible = true;
					m_noRecordLabel.Visible = false;
				}
			}            
			else 
			{
				// Page is postback.
				// This is the temporary code requires for sorting to work. dd 5/7/2003.

				if (Request.Form["__EVENTTARGET"] == m_programsDG.ClientID) 
				{
					if (m_programsDG.Items.Count == 0) 
					{
						m_programsDG.Visible = false;
						m_noRecordLabel.Visible = true;
					} 
					else 
					{
						m_programsDG.Visible = true;
						m_noRecordLabel.Visible = false;
					}
				}
			}

			// 7/15/2004 kb - Setup the input buttons to reflect the
			// current feature set and permissions.

			if( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == false )
			{
				m_forEditor.Visible = false;

                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true && CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
				{
					m_deleteBtn.Enabled    = false;
					m_newBtn.Enabled       = false;
					m_newFlp.Enabled       = false;
					m_newSlp.Enabled       = false;
					m_duplicateBtn.Enabled = false;
					m_moveBtn.Enabled      = false;
				}
				else
				{
					m_deleteBtn.Enabled    = true;
					m_newBtn.Enabled       = true;
					m_newFlp.Enabled       = true;
					m_newSlp.Enabled       = true;
					m_duplicateBtn.Enabled = true;
					m_moveBtn.Enabled      = true;
				}
			}
			else
			{
				m_forEditor.Visible = true;
			}

			// 4/15/2005 kb - Update the buttons to show the correct
			// interface, depending on who is viewing.

			if( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == true )
			{
				m_forLender.Visible = true;
				m_forSimple.Visible = false;
			}
			else
			{
				m_forLender.Visible = false;
				m_forSimple.Visible = true;
			}
		}

		private void BindInvestorList()
		{
            var list = InvestorNameUtils.ListActiveInvestorNames(CurrentUser.BrokerDB);
			m_investorList.DataSource = list;
			m_investorList.DataTextField = "InvestorName";
			m_investorList.DataBind();
		}

		private void OnDataGridBind( Object sender , DataGridItemEventArgs a )
		{
			// We check each row to sniff out invalid loan programs
			// when they're displayed.  It sucks because we only note
			// the error when someone opens the folder that contains
			// the inconsistent state.
			
			DataRowView dRow = a.Item.DataItem as DataRowView;

			if( dRow != null )
			{
				// Validate the row.  We are looking for inconsistencies, but
				// here is not really the place.  Need to validate the entire
				// set through an admin interface.

				if( dRow[ "Type" ].ToString().ToLower() == "invalid" )
				{
					Tools.LogError( String.Format
						( "Loan program {0} ({1}) is master and derived (user = {2} , broker = {3})."
						, dRow[ "lLpTemplateNm" ]
						, dRow[ "lLpTemplateId" ]
						, CurrentUser.BrokerId
						, CurrentUser.LoginNm
						));
				}

				if( dRow[ "Type" ].ToString().ToLower() == "master" )
					m_masterId = dRow[ "lLpTemplateId" ].ToString();

				// Color the row.  We only care about showing context when the
				// advanced grid view is enabled.

				if( m_pricingVisible == true )
				{
					String[] contextSet =
						{
								"IsEnabledOverrideBit"
							, "lLockedDaysOverrideBit"
							, "lLpeFeeMinOverrideBit"
							, "lLpeFeeMaxOverrideBit"
						};

					foreach( String sC in contextSet )
					{
						WebControl wC = a.Item.FindControl( sC ) as WebControl;

						if( wC != null )
						{
							try
							{
								if( ( Boolean ) dRow[ sC ] == true )
									wC.CssClass = "ContextOverridden";
							}
							catch( Exception e )
							{
								Tools.LogError( "Failed to context-color " + sC + " in loan program grid." , e );
							}
						}
					}
				}
			}
		}

		private void BindDataGrid()
		{
            var listParams = new SqlParameter[] { new SqlParameter("@brokerid", this.m_brokerID),
                                                    new SqlParameter("@folderid", this.m_folderID),
                                                    new SqlParameter("@hasPerm", CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == true ? 1 : 0) };

            string sSql = @"SELECT IsMaster,IsEnabled,IsEnabledOverrideBit,lLpTemplateId,lLpTemplateNm,lLendNm,lTerm,lDue,
lLockedDays,lLockedDaysLowerSearch,lLockedDaysOverrideBit,lFinMethT,lRateSheetEffectiveD,lRateSheetExpirationD,lRateSheetXmlContent,lLpeFeeMin,
lLpeFeeMinOverrideBit,lLpeFeeMax,lLpeFeeMaxOverrideBit,lRateDelta,lFeeDelta, 
CASE WHEN IsMaster = 1 AND lBaseLpId IS NULL THEN 'Master' 
     WHEN IsMaster = 0 AND lBaseLpId IS NOT NULL THEN 'Derived' 
     WHEN IsMaster = 0 AND lBaseLpId IS NULL THEN '' 
     WHEN IsMaster = 1 AND lBaseLpId IS NOT NULL THEN 'Invalid' END AS Type,
Enabled = case IsEnabled when 1 then 'Yes' else 'No' end,

ProductCode, lLpInvestorNm, lLpCustomCode1, lLpCustomCode2, lLpCustomCode3, lLpCustomCode4, lLpCustomCode5,
lLpInvestorCode1, lLpInvestorCode2, lLpInvestorCode3, lLpInvestorCode4, lLpInvestorCode5 FROM Loan_Program_Template l " +

#if NOUSE_RATE_OPTION_TABLE == false
				"join Rate_Options r on l.SrcRateOptionsProgId = r.SrcProgId " +
#endif
                "WHERE l.BrokerID = @brokerid AND ISNULL(FolderID, '00000000-0000-0000-0000-000000000000') = ISNULL(@folderid, '00000000-0000-0000-0000-000000000000') AND IsLpe = @hasPerm AND IsMasterPriceGroup = 0 ORDER BY IsMaster desc, lLpTemplateNm";

            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(DataSrc.LpeSrc, ds, sSql, null, listParams);

			for( int c = 3 ; c < m_programsDG.Columns.Count ; ++c )
			{
				// 4/11/2005 kb - Put the always-visible columns within this
				// range and adjust when more are added.  Everything outside
				// is hidden in the simple view.
				//
				// 5/12/2005 kb - We now hide the enabled column since only
				// LPE products can be enabled or disabled.

				if( c >= 5 && c <= 7 )
					continue;

				m_programsDG.Columns[ c ].Visible = m_pricingVisible;
			}

            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true && CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false && CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
				m_programsDG.Columns[ 1 ].Visible = false;
			else
				m_programsDG.Columns[ 1 ].Visible = true;

			m_programsDG.Columns[ 2 ].Visible = !m_programsDG.Columns[ 1 ].Visible;

			m_programsDG.DataSource = ds.Tables[0].DefaultView;
			m_programsDG.DataBind();
		}

		#region Web Form Designer generated code

		override protected void OnInit(EventArgs e)
		{
            EnableJqueryMigrate = false;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_programsDG.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.OnDataGridBind);
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		private void showModal(string href) 
		{
			string script = "<script language='JavaScript1.2'>";
			script += $"showModal('{href}', null, null, null, function(){{ window.location = self.location; }});";
			script += "</script>";
			ClientScript.RegisterStartupScript(this.GetType(), "showModal", script);
		}

		protected void OnNewProductClick( object sender , System.EventArgs a )
		{		
			try
			{
				// 7/15/2004 kb - Block access if pricing engine enabled
				// and user lacks permission.

                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
				{
                    if (CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false && CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
					{
						ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

						return;
					}
				}

				// OPM 17145
				string investor;
				string productCode;

				// If this is a non-lpe, we use blank investor
				if  (( (Button) sender).ID == "m_newBtn")
				{
					investor = "";
					productCode = "";
				}
				else
				{
					investor = m_investorList.SelectedItem.Value;
					productCode = m_investorProductList_value.Value;
				}

				/* Replaced above
				string investor = ( ( (Button) sender).ID == "m_newBtn" ) ? "" : m_investorList.SelectedItem.Value;
				
				
				string productCode = "";
				if ( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ))
					productCode = m_investorProductList_value.Value;
				*/
				
				//OPM 17145 Cord
				CLoanProductData data = CLoanProductData.Create
					( E_sLienPosT.First
					, CurrentUser.HasPermission( Permission.CanModifyLoanPrograms )
					, false
					, Guid.Empty
					, m_folderID
					, m_brokerID
					, Guid.Empty // srcRateOptionsProgIdInherit
					, investor
					, productCode
					);

				// Make sure we handle formatting here
				// Encode % \ ' "  to not break our code insertions.
				showModal( "/los/Template/LoanProgramTemplate.aspx?FileID=" + data.lLpTemplateId
									+ "&mode="
                                    + ( CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) ? "new" : "add" )
                                    );

			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to add new loan product." , e );
			}
		}

		protected void OnNewSecondProductClick( object sender , System.EventArgs a )
		{		
			try
			{
				// 7/15/2004 kb - Block access if pricing engine enabled
				// and user lacks permission.

                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
				{
                    if (CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false && CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
					{
						ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

						return;
					}
				}
				
				// OPM 17145 Cord
				CLoanProductData data = CLoanProductData.Create
					( E_sLienPosT.Second
					, CurrentUser.HasPermission( Permission.CanModifyLoanPrograms )
					, false
					, Guid.Empty
					, m_folderID
					, m_brokerID
					, Guid.Empty // srcRateOptionsProgIdInherit
					, m_investorList.SelectedItem.Value
					, m_investorProductList_value.Value
					);

				showModal( "/los/Template/LoanProgramTemplate.aspx?FileID=" + data.lLpTemplateId
						+ "&mode=new" );
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to add new loan product." , e );
			}
		}
		
		protected void OnDeleteClick( object sender , System.EventArgs a )
		{
			try
			{
				// 7/15/2004 kb - Block access if pricing engine enabled
				// and user lacks permission.

                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
				{
                    if (CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false && CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
					{
						ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

						return;
					}
				}
				
				// Can only be checked in SAE view
				bool deleteDerived = m_deleteDerivedCb.Checked;

				string ids = Request.Form["templateid"];

				if (ids == null || ids =="") 
				{
					return;
				}

				string[] templateIDs = ids.Split(',');
				int numDeleted = 0 , numToDelete = 0, derivedDeleted = 0;

				foreach (string id in templateIDs) 
				{
					try
					{
						++numToDelete;

						if ( deleteDerived )
						{
							// 02/26/08 mf. OPM 17141.  Bases can be deleted with this checkbox
							// setting because we will delete their children (derived programs) first.
							derivedDeleted += CLoanProductBase.DelLoanProductAndDerived(new Guid(id));
						}
						else
							CLoanProductBase.DelLoanProduct(new Guid(id));

						++numDeleted;
					}
					catch( CBaseException e )
					{
						// Oops!

						ErrorMessage = e.UserMessage;
					}
				}

				if( numDeleted < numToDelete )
					FeedBack = "Deleted " + numDeleted + " of " + numToDelete + " loan programs."
						+ ( deleteDerived ? " Deleted " + derivedDeleted + " derived programs." : "" );
				else
				{
					if( numDeleted > 1 || numDeleted == 0 )
						FeedBack = "Deleted " + numDeleted + " loan programs."
							+ ( deleteDerived ? " Deleted " + derivedDeleted + " derived programs." : "" );
					else
						FeedBack = "Deleted " + numDeleted + " loan program."
							+ ( deleteDerived ? " Deleted " + derivedDeleted + " derived programs." : "" );
				}

				BindDataGrid();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to delete loan product." , e );
			}
		}

		protected void OnDuplicateClick( object sender , System.EventArgs a )
		{
			try
			{
				// 7/15/2004 kb - Block access if pricing engine enabled
				// and user lacks permission.

                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
				{
                    if (CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false && CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
					{
						ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

						return;
					}
				}
				
				/*if( Request.Form[ "templateId" ] == null || Request.Form[ "templateId" ].TrimWhitespaceAndBOM() == "" )
				{
					throw new CBaseException(ErrorMessages.Generic, "Nothing selected to duplicate.  Request ignored.");
				}*/

				String[] idList = Request.Form[ "templateid" ].Split( ',' , ' ' );

                /*if( idList.Length > 1 )
                {
                    throw new CBaseException(ErrorMessages.Generic, "Too many products selected.  Request ignored.");
                }*/

                CLoanProductData copy = CLoanProductData.Duplicate( new Guid( idList[ 0 ] ) );

				showModal( "/los/Template/LoanProgramTemplate.aspx?FileID=" + copy.lLpTemplateId );
			}
			catch( CBaseException e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = e.UserMessage , e );
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to duplicate loan products." , e );
			}
		}

		protected void OnMoveClick( object sender , System.EventArgs a )
		{
			try
			{
				// 7/15/2004 kb - Block access if pricing engine enabled
				// and user lacks permission.

                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
				{
                    if (CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false && CurrentUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
					{
						ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

						return;
					}
				}

				string ids = Request.Form["templateid"];
				if (ids == null || ids =="") 
				{
					return;
				}
				string[] templateIDs = ids.Split(',');
				try
				{
					Guid targetFolderId = new Guid ( m_newFolderId.Text );

					foreach( string id in templateIDs )
					{
						//@loanProductID uniqueidentifier,
						//@newFolderId uniqueidentifier
						List<SqlParameter> pars = new List<SqlParameter> ( 2 );
						pars.Add( new SqlParameter("@loanProductID", new Guid( id ) ) );
						pars.Add( new SqlParameter("@newFolderId", targetFolderId ) );

						StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "MoveLoanProduct", 0, pars );
					}
					
				}
				catch
				{
					//Response.Redirect("~/common/AppError.aspx?errmsg=Unable to move at least one selected loan product");
				}
				finally
				{
					BindDataGrid();
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to move loan product." , e );
			}
		}

		protected void OnAddMasterClick( object sender , System.EventArgs a )
		{
			try
			{
				// 7/15/2004 kb - Block access if pricing engine enabled
				// and user lacks permission.

				if( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == false )
				{
					ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

					return;
				}

				// 5/6/2005 kb - Create new loan product.

				CLoanProductData data = CLoanProductData.Create
					( E_sLienPosT.First
					, CurrentUser.HasPermission( Permission.CanModifyLoanPrograms )
					, true
					, Guid.Empty
					, m_folderID
					, m_brokerID
					, Guid.Empty /* srcRateOptionsProgIdInherit */
					, m_investorList.SelectedItem.Value
					, m_investorProductList_value.Value
					);

				showModal( "/los/Template/LoanProgramTemplate.aspx?FileID=" + data.lLpTemplateId
											+ "&mode=add" );
			}
			catch( Exception e )
			{
				// Oops!

				ErrorMessage = "Unable to create loan product master. There might be already one existing for the selected folder.";

				Tools.LogError( "Failed to add master product." , e );
			}
		}
	}
}