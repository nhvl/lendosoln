<%@ Page language="c#" Codebehind="InsertExternalProduct.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.InsertExternalProduct" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>insert external product</title>
	<link href="../../css/stylesheet.css" rel="stylesheet">
	<style>
		#programTree {
			width: 100%;
			height: 100%;
			width: calc(100vw - 28px);
			height: calc(100vh - 81px);
			padding: 4px;
			background :whitesmoke;
			border: 2px groove;
		}
		.empty-message {
			text-align: center;
			color: dimgray;
			padding: 100px;
		}
	</style>
</head>
<body MS_POSITIONING="FlowLayout" scroll="no" onload="onInit();">
<FORM method="post" runat="server">
	<TABLE cellspacing="8" cellpadding="0" border="0" width="100%" height="100%">
		<TR>
			<TD height="1px">
				<TABLE cellpadding="0" cellspacing="0" border="0" width="100%">
					<TR>
						<TD align="left">
							Loaded loan program set.
						</TD>
						<TD align="right">
							<ASP:DropDownList id="m_BrokerList" runat="server" />
						</TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
		<TR>
			<TD height="100%">
				<div id="programTree"></div>
			</TD>
		</TR>
		<TR>
			<TD height="1px" align="center">
				<button type="button" id="btnInsert" style="width:100px;">Insert</button>
				<button type="button" id="btnRefresh">Refresh</button>
				<button type="button" onclick="onClosePopup();">Cancel</button>
			</TD>
		</TR>
	</TABLE>
</FORM>
<script>
    function onInit() {
		var programTree = $("#programTree");

        if (typeof state != "undefined") {
			renderTree(state.tree);
		} else {
			alert("ERROR: 'state' is not defined");
			return;
		}

		$("#m_BrokerList").on("change", function(event){
			state.selectedBrokerId = event.currentTarget.value;
			fetchData(state.selectedBrokerId);
		});

        $("#btnRefresh").on("click", function(event) {
			fetchData(state.selectedBrokerId);
		});

		$("#btnInsert").on("click", function(event) {
			var tree = programTree.dynatree("getTree");
			var nodes = tree.getSelectedNodes();
			var selectedIds = nodes.map(function(n){ return n.data.key });
			if (selectedIds.length < 1) {
				alert("Nothing Checked.");
				return;
			}
			var targetId = getParameterByName("targetId");
			if (!targetId || targetId == "root") targetId = "00000000-0000-0000-0000-000000000000";

			event.currentTarget.disabled = true;
			post("DoInsert", ({
				selectedBrokerId: state.selectedBrokerId,
                targetId: targetId,
                selectedIds: selectedIds,
			})).then(function(result){
				event.currentTarget.disabled = false;
				if(!!result.errorMessage) {
					alert(result.errorMessage);
					return;
				}
				onClosePopup();
			});
			return false;
		});

        function fetchData(selectedBrokerId) {
            post("LoadData", ({ selectedBrokerId: selectedBrokerId })).then(function(d) {
                if (d.errorMessage != null) {
                    alert(d.errorMessage);
                    return;
                }

                state.tree = d.tree;
                renderTree(state.tree);
            });
		}

		function post(op, data) {
			return callWebMethodAsync({
				url  : location.pathname+"/"+op,
				data : JSON.stringify(data),
				async: true
			}).then(function(data) { return data.d; });
		}

        function renderTree(tree) {
			try { programTree.dynatree("destroy"); } catch(e) { }

            if (tree.children.length < 1) {
                programTree.html("<div class='empty-message'>Empty. No programs to display.</div>");
                return;
            }

            var dTree = nodeToDynaNode(state.tree, 0);
            dTree.hideCheckbox = dTree.unselectable = true;

			programTree.dynatree({
				checkbox:true,
				selectMode: 2,
				children: [dTree]
			});
		}

		function nodeToDynaNode(node, currentLevel) {
			if (node.children == null) return ({
				key:node.Id, title:node.Name,
				icon: node.IsEnabled ? "../../../../images/bullet.gif" : "../../../../images/disabled.gif"
			});

			node.children.sort(nodeCompare);
			return ({
				key:node.Id, title:node.Name,
				isFolder: true,
				expand: currentLevel < 1,
				children: node.children.map(function(node) {return nodeToDynaNode(node, currentLevel + 1)})
			});
		}

		function nodeCompare(a, b) {
			if (a.children != null && b.children == null) return -10;
			if (a.children == null && b.children != null) return  10;
			return stringCompare(a.Name, b.Name);
		}
		function stringCompare(a, b){
			var la = a.toLowerCase();
			var lb = b.toLowerCase();
			return (
				(la < lb) ? -1 : (
				(la > lb) ?  1 : (
				( a <  b) ? -1 : (
				( a >  b) ?  1 : (
				0)))));
		}

        if (document.getElementById("m_errorMessage") != null) {
            alert(document.getElementById("m_errorMessage").value);
        }

        if (document.getElementById("m_cmdToDo") != null) {
            if (document.getElementById("m_cmdToDo").value == "Close") {
                onClosePopup();
            }
        }

        <% if (IsPostBack == false) { %>
        resize(640, 480);
        <% } %>
    }
</script>
</BODY>
</HTML>
