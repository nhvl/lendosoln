namespace LendersOfficeApp.los.Template
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.RatePrice;
    using System.Web.UI.HtmlControls;


    /// <summary>
    ///		This control is extracted from los\Template\InvestorListAdmin.aspx 
    /// for reusing in C:\LendOSoln\LendOSoln\LendersOfficeApp\LOAdmin\SAE\InvestorList4SAE.aspx
    /// 
    ///    In next release, InvestorListAdmin will use this control
    /// </summary>
    public partial class InvestorListControl : System.Web.UI.UserControl
    {
        /// <summary>
        /// Convert a list of generic object to DataTable.
        /// </summary>
        /// <typeparam name="T">Generic Type.</typeparam>
        /// <param name="list">List of objects to convert.</param>
        /// <returns>The datatable.</returns>
        public static DataTable BuildDataTable<T>(IEnumerable<T> list)
        {
            DataTable dataTable = CreateTable<T>();

            var propertyList = typeof(T).GetProperties();

            foreach (var item in list)
            {
                DataRow dataRow = dataTable.NewRow();

                foreach (var prop in propertyList)
                {
                    var value = prop.GetValue(item);
                    if (Nullable.GetUnderlyingType(prop.PropertyType) != null)
                    {
                        // dd - If the type is nullable? i.e: DateTime? then we need to use DBNull.Value when value is null.
                        //      Otherwise we will get exception.
                        if (value == null)
                        {
                            value = DBNull.Value;
                        }
                    }
                    dataRow[prop.Name] = value;
                }
                dataTable.Rows.Add(dataRow);
            }

            return dataTable;
        }

        private static DataTable CreateTable<T>()
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            foreach (var prop in typeof(T).GetProperties())
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            return dataTable;
        }

        private void BindDataGrid()
        {
            // Load the list of investors
            var list = InvestorNameUtils.ListAllInvestorNames(null);

            var dataTable = BuildDataTable(list); // Build datatable so we support sorting in datagrid.
            m_InvestorDG.DataSource = dataTable.DefaultView;
            m_InvestorDG.DefaultSortExpression = "InvestorName ASC";
            m_InvestorDG.DataKeyField = "InvestorName";
            m_InvestorDG.DataBind();
        }

        protected void PageLoad(object sender, System.EventArgs a)
        {
            if (!Page.IsPostBack)
                BindDataGrid();
        }

        protected void PagePreRender(object sender, System.EventArgs a)
        {
            // On postback re-bind in PreRender so UI reflects 
            // the data changes from event
            if (Page.IsPostBack && m_InvestorDG.DoesClientSendSortCmd)
                BindDataGrid();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.PreRender += new System.EventHandler(this.PagePreRender);
        }

        #endregion

        protected void OnItemCommand_InvestorDG(Object sender, DataGridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
            case "edit":
                m_InvestorDG.EditItemIndex = e.Item.ItemIndex;
                break;

            case "cancel":
                m_InvestorDG.EditItemIndex = -1;
                break;

            case "update":
                {
                    string investor = e.Item.Cells[1].Text;

                    try
                    {
                        string timeStr = ((TextBox)(e.Item.FindControl("edtRateLockCutOffTime"))).Text.TrimWhitespaceAndBOM();
                        object cutOffTime = timeStr != "" ? (object)DateTime.Parse(timeStr) : DBNull.Value;
                        bool valid = ((CheckBox)(e.Item.FindControl("isValidCb"))).Checked;
                        bool UseLenderTimezoneForRsExpiration = (timeStr != string.Empty) ? ((CheckBox)(e.Item.FindControl("UseLenderTimezoneForRsExpirationCb"))).Checked : false;

                        bool result = StoredProcedureHelper.ExecuteNonQuery(
                            DataSrc.LpeSrc
                            , "InvestorUpdate"
                            , false
                            , 2
                            , new SqlParameter("@InvestorName", investor)
                            , new SqlParameter("@IsValid", valid)
                            , new SqlParameter("@RateLockCutOffTime", cutOffTime)
                            , new SqlParameter("@UseLenderTimezoneForRsExpiration", UseLenderTimezoneForRsExpiration)
                            ) > 0;

                        Result.InnerText = result ? string.Empty : "Failed modify Investor.";
                        Result.Style.Add("font-weight", "bold");
                        Result.Style.Add("color", "red");
                    }
                    catch (Exception exc)
                    {
                        Result.InnerText = "Failed to modify Investor : " + exc.Message;
                        Result.Style.Add("font-weight", "bold");
                        Result.Style.Add("color", "red");
                        Tools.LogError("Failed to modify Investor", exc);
                    }
                    //string   timeStr    = ((TextBox)(e.Item.FindControl("edtRateLockCutOffTime"))).Text;
                    //m_Result.Text = investor + " *** valid=" + e.Item.Cells[3].Text + " *** time=" + timeStr;

                    m_InvestorDG.EditItemIndex = -1;
                    break;
                }

            }
            BindDataGrid();

        }


        // Add the new investor to the list.  Name is primary key (unique constraint),
        // so we don't have to worry about checking for duplicates here.
        protected void OnInvestorAddClick(Object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(m_InvestorAddTB.Text))
            {
                Result.InnerText = "Please specify an investor";
            }
            else
            {
                // Add this investor to the DB.
                // Currently all new investor names are set to valid.
                try
                {
                    string investorName = m_InvestorAddTB.Text.Trim().ToUpper();
                    if (StoredProcedureHelper.ExecuteNonQuery(
                        DataSrc.LpeSrc
                        , "AddInvestorName"
                        , false
                        , 2
                        , new SqlParameter("@InvestorName", investorName)) > 0)
                    {
                        Result.InnerText = "Added Investor " + investorName;
                    }
                    else
                    {
                        Result.InnerText = "Failed to add investor";
                    }
                    BindDataGrid();
                }
                catch (SqlException exc)
                {
                    Result.InnerText = "Failed to add investor";
                    Tools.LogError("Failed to add investor to investor list", exc);
                }
            }
        }

        protected string RenderCutOffTime(object DataItem)
        {
            //05/01/08 mf. OPM 20141
            string cutOffTime = System.Web.UI.DataBinder.Eval(DataItem, "RateLockCutOffTime", "{0:T}");

            bool useLenderTimezone = (bool)System.Web.UI.DataBinder.Eval(DataItem, "UseLenderTimezoneForRsExpiration");

            if (cutOffTime != string.Empty)
            {
                return cutOffTime + (useLenderTimezone ? " (Lender's Timezone)" : " (Server's Timezone PST)");
            }
            else
            {
                return string.Empty;
            }
        }



        public void ShowCloseButton(bool visible)
        {
            m_CloseButtonPanel.Visible = visible;
            m_InvestorDG.Columns[2].HeaderText = visible ? "RateLockCutOffTime (PST)"
                                                           : "RateLockCutOffTime (Pacific Time)";
        }

        protected HtmlInputCheckBox CreateIsValidCheckbox(object dataItem)
        {
            DataRowView row = dataItem as DataRowView;

            HtmlInputCheckBox checkbox = null;

            if (row != null)
            {

                checkbox = new HtmlInputCheckBox();
                checkbox.Checked = (bool)row["IsValid"];
                checkbox.Disabled = true;
                checkbox.Attributes.Add("onclick", "return false;");
            }
            return checkbox;
        }
    }
}
