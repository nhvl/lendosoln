<Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="InvestorProductListAdmin.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.InvestorProductListAdmin" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<style>
		.product-container{
			margin: 0px 4px;
		}
		.product-list{
			BORDER-RIGHT: 2px;
			BORDER-TOP: lightgrey 2px solid;
			OVERFLOW-Y: scroll;
			PADDING-BOTTOM: 4px;
			BORDER-LEFT: lightgrey 2px solid;
			WIDTH: 100%;
			PADDING-TOP: 4px;
			BORDER-BOTTOM: lightgrey 2px solid;
			HEIGHT: 260px;
		}
	</style>
	<HEAD>
		<title>Investor Product Admin</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout" scroll="no">
		<script language="jscript">
      <!--
        function _init() {
          <% if ( !Page.IsPostBack ) { %>
            resize( 540 , 520 );
          <% } %>
        }

		function onCloseClick() { try
		{
		  var args = window.dialogArguments || {};
		  args.OK = true;
		  onClosePopup(args);
		}
		catch (e)
		{
		}}
		

    // -->
		</script>
		<h4 class="page-header">Product List</h4>
		<form id="BranchBrefix" method="post" runat="server">
			<table width="100%" height="370" border="0" cellspacing="2" cellpadding="3">
				<tr valign="top">
					<td>
						<div class="product-container">	
							<div class="product-list">
								<ml:CommonDataGrid id="m_ProductDG" runat="server" OnItemDataBound="OnItemDataBound_ProductDG" OnEditCommand="ProductDG_Edit" OnCancelCommand="ProductDG_Cancel" OnUpdateCommand="ProductDG_Update" EnableViewState="True">
									<AlternatingItemStyle cssclass="GridAlternatingItem"></AlternatingItemStyle>
									<ItemStyle cssclass="GridItem"></ItemStyle>
									<HeaderStyle cssclass="GridHeader"></HeaderStyle>
									<Columns>
										<asp:EditCommandColumn EditText="edit" CancelText="cancel" UpdateText="update" />
										<asp:BoundColumn DataField="ProductCode" SortExpression="ProductCode" HeaderText="Product Code" ReadOnly="True"></asp:BoundColumn>
										<asp:TemplateColumn HeaderText="Valid" SortExpression="IsValid">
											<ItemTemplate>
												<%# AspxTools.HtmlString( (bool) DataBinder.Eval(Container.DataItem, "IsValid") == true ? "Yes" : "No" ) %>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:DropDownList runat="server" ID="IsValidDdl">
													<asp:ListItem Value="1">Yes</asp:ListItem>
													<asp:ListItem Value="0">No</asp:ListItem>
												</asp:DropDownList>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="RateSheet Can Expire" SortExpression="IsRateSheetExpirationFeatureDeployed">
											<ItemTemplate>
												<%# AspxTools.HtmlString( (bool) DataBinder.Eval(Container.DataItem, "IsRateSheetExpirationFeatureDeployed") == true ? "Yes" : "No" ) %>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:DropDownList runat="server" ID="IsRateSheetExpirationFeatureDeployedDdl">
													<asp:ListItem Value="1">Yes</asp:ListItem>
													<asp:ListItem Value="0">No</asp:ListItem>
												</asp:DropDownList>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="IsValid" Visible="False"></asp:BoundColumn>
									</Columns>
								</ml:CommonDataGrid>
							</div>
						</div>
						Investor Name:&nbsp;<asp:DropDownList id="m_InvestorDDL" runat="server" AutoPostBack="True" /><br>
						Product Code:&nbsp;&nbsp;<asp:TextBox ID="m_ProductAddTB" Runat="server" /><br>
						RateSheet Expiration:
						<asp:RadioButtonList ID="m_newExpirationChoiceRBL" Runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="FieldLabel">
							<asp:ListItem Value="1">Yes</asp:ListItem>
							<asp:ListItem Selected="True" Value="0">No</asp:ListItem>
						</asp:RadioButtonList>
						<br>
						<asp:Button id="m_ProductAddBTN" runat="server" Text="Add" OnClick="OnProductAddClick" />
						<br>
						<ml:EncodedLiteral id="m_Result" Runat="server" EnableViewState="False" />
					</td>
				</tr>
				<tr valign="top">
					<td align="center" colSpan="1">
						<hr>
						<input type="button" onclick="onCloseClick();" value="Close">
					</td>
				</tr>
			</table>
			<uc1:cModalDlg runat="server" id="CModalDlg1"></uc1:cModalDlg>
		</form>
	</body>
</HTML>
