﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddDisabledProductInvestor.aspx.cs" Inherits="LendersOfficeApp.los.Template.AddDisabledProductInvestor" %>
<%@ Register TagPrefix="uc" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html>
<head runat="server">
    <title>Add New Program</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet"/>
    <style type="text/css">
        .ButtonStyle
        {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 11px;
            font-weight: bold;
            color: Black;
        }
        body
        {
            background-color: #DCDCDC;
            overflow: auto;
        }
        ul { list-style-type:none; padding-left: 0 ; margin-left: 0;}
        ul .last { padding-bottom: 0; margin-bottom: 0; }
    </style>
</head>
<body>
    <h4 class="page-header">Add Product To Disabled Pricing List</h4>
    <form id="form1" runat="server">
        <div style="padding-left:20px; padding-top:10px;">
        <asp:Panel id="AccessDeniedPanel" style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
							    Access Denied. You do not have access to view this page.
	    </asp:Panel>
	    <asp:Panel id="AccessGrantedPanel" runat="server">
            <table>
                <tr>
                    <td class="FieldLabel">Investor</td>
                    <td class="FieldLabel">Product</td>
                </tr>
                <tr>
                    <td valign="top">
                      <asp:DropDownList id="InvestorList" Width="180px" runat="server" 
                            AutoPostBack="True" 
                            onselectedindexchanged="InvestorList_SelectedIndexChanged" />
                    </td>
                    <td valign="top">
                        <asp:ListBox id="ProductCodeList" onclick="f_ProdSelect();" SelectionMode="Multiple" Width="180px" Rows="5" runat="server" />
                    </td>
                </tr>
                <tr><td></td><td style="font-style:italic">Press and Hold Ctrl to Select Multiple</td></tr>
            </table>
            <ul >
                <li > <span class="FieldLabel">Pricing Result</span> </li>
                <li><asp:RadioButton ID="pricingResult_ShowInRed" onclick="f_switchPricingResult();"  Checked="true" GroupName="pricingResult" CssClass="FieldLabel" Text="Display Pricing in Red and Restrict Submission to Registration" runat="server" /></li>
                <li><asp:RadioButton ID="pricingResult_Hide" onclick="f_switchPricingResult();" GroupName="pricingResult" CssClass="FieldLabel" Text="Exclude Product from Pricing Result" runat="server" /> </li>
            </ul>
            <ul class="last">
                <li ><span class="FieldLabel">Enable Method</span> </li>
                <li>
                    <asp:RadioButton ID="disableType_0" GroupName="disableType" Checked="true" class="FieldLabel" Text="Auto Enable After Downloading a New Rate Sheet"
                        runat="server" />
                </li>
                <li>
                    <asp:RadioButton ID="disableType_1" GroupName="disableType" class="FieldLabel" Text="Manual Enable"
                        runat="server" />
                </li>
            </ul>
        <span class="FieldLabel" style="padding-bottom:5px;"> <ml:EncodedLabel runat="server" AssociatedControlID="LoMessage">Message to Loan Officer</ml:EncodedLabel></span>
        <br/>
        <asp:textbox id="LoMessage" onkeydown="if (event.keyCode == 13) { f_submit(); return false; }" runat="server" TextMode="MultiLine" Height="70px" Width="370px"></asp:textbox>
        <br/>
        <br />
        <span class="FieldLabel" style="padding-bottom:5px;"> <ml:EncodedLabel ID="Label1" runat="server" AssociatedControlID="Notes">Internal Notes</ml:EncodedLabel></span>
        <br/>
        <asp:textbox id="Notes" onkeydown="if (event.keyCode == 13) { f_submit(); return false; }" runat="server" TextMode="MultiLine" Height="70px" Width="370px"></asp:textbox>
        
        <div style="text-align:center; padding-top:10px;">
            <asp:Button ID="AddButton" Width="70px" class="ButtonStyle" Text="Add" Enabled="false" runat="server" onclick="AddButton_Click"/>&nbsp;
            <input type="button" style="width:70px;" class="ButtonStyle" onclick="onClosePopup();" value="Close" />
        </div>
        <br/>
        </asp:Panel>
        </div>
        <script  type="text/javascript">
        <!--
        function _init() {
        <% if (!IsPostBack ) { %>
            resizeForIE6And7(450, 555);
         <% } %>
            f_switchPricingResult();
        }
        
        function f_ProdSelect()
        {
            var pBox = <%= AspxTools.JsGetElementById(ProductCodeList) %>;
            var btn = <%= AspxTools.JsGetElementById(AddButton) %>;
            var selected = false;
            
            for (var i=0; i<pBox.length; i++)
            {
                if (pBox.options[i].selected)
                    selected = true;
            }
            
            btn.disabled = !selected;
        }
        
        function f_submit()
        {
          var btn = <%= AspxTools.JsGetElementById(AddButton) %>;
          if (btn.disabled == false)
            btn.click();
        }
               
        function f_switchPricingResult() {
            var cb1 = <%= AspxTools.JsGetElementById(pricingResult_ShowInRed) %>;
            var cb2 = <%= AspxTools.JsGetElementById(pricingResult_Hide) %>;
            var loMsg = <%= AspxTools.JsGetElementById(LoMessage) %>;
            try{
                if (cb1.checked) {
                    loMsg.disabled = false;
                    loMsg.style.backgroundColor = 'white'
                }
                else if (cb2.checked) {
                    loMsg.value = '';
                    loMsg.disabled = true;
                    loMsg.style.backgroundColor = '#CCCCCC'
                }                
            }catch(e){}
        }
        // -->
        </script>
    </form>
    <uc:cmodaldlg id="modalDlg" runat="server"></uc:cmodaldlg>
</body>
</html>
