﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="LendersOfficeApp.los.Template.Product.Products" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
        #tree {
            width: 350px;
            float: left;
        }
        #LoanProductList { 
            float: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="tree">
        
    </div>
    <iframe src="about:blank" name="LoanProductList" id="LoanProductList"></iframe>
    </form>
    <script type="text/javascript">
        $j(function(){
            var inEventHandler = false, 
                $tree = $j('#tree'),
                $iframe = $j("iframe[name=LoanProductList]");
                
            
            function toggleNonFolders(show){
                $tree.dynatree("getRoot").visit(function(node){
                    if (!node.data.isFolder){
                        $j(node.li).toggle(show);
                    }
                })
            }
            
            $iframe.load(function(){
                 this.style.height = this.contentWindow.document.body.offsetHeight + 'px';
            });
            
            $j(window).resize(function(){
                var width = $j(window).width();
                $iframe.width(width - 400); 
            }).triggerHandler('resize');
            
            $tree.dynatree({
                persist: false,
                children: ProgramFolderStructure,
                checkbox: true, 
                onSelect : function(flag, node){
                    if(inEventHandler) return;
                    try {
                            inEventHandler = true;
                    
                            node.visit(function(childNode){
                                childNode.select(flag);
                            });
                    
                    } finally {
                        inEventHandler = false;
                    }
                },
                onActivate: function(node){
                    if( node.data.href ){
                        // Open target
                      $iframe.attr("src", node.data.href);
                    }
                }
            });
            
            
        });
    </script>
</body>
</html>
