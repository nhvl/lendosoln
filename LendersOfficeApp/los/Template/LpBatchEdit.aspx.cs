namespace LendersOfficeApp.los.Template
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Text;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.RatePrice;
    using LendersOffice.Security;

    public partial class LpBatchEdit : BasePage
	{
        private readonly LosConvert Converter = new LosConvert();

        protected BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}
		private String ErrorMessage
		{
			// Write out popup message to alert user of error.

			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

        protected void PageInit(object sender, EventArgs e)
        {
            this.RegisterJsScript("QualRateCalculation.js");

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("QualRateFieldIds", this.GetQualRateFieldIds());
        }

        protected void PageLoad(object sender, System.EventArgs e)
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

            if (!IsPostBack)
			{

                List<Guid> items;

                if (!String.IsNullOrEmpty(RequestHelper.GetSafeQueryString("k")))
                {
                    items = AutoExpiredTextCache.GetUserObject<List<Guid>>(PrincipalFactory.CurrentPrincipal, RequestHelper.GetSafeQueryString("k"));
                    if (items == null)
                    {
                        throw CBaseException.GenericException("Cache item time ran out");
                    }
                }
                else
                {
                    items = new List<Guid>();
                    foreach (string sid in Request["LpIds"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        items.Add(new Guid(sid));
                    }
                }
                if (items.Count ==0)
                {
                    throw CBaseException.GenericException("No chosen programs");
                }
                List<SqlParameter> parameters = new List<SqlParameter>();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < items.Count; i++)
                {
                    string id = "@lpId" + i.ToString();
                    sb.Append(String.Concat(id, ","));
                    parameters.Add(new SqlParameter(id, items[i]));
                }
                //remove trailing comma
                sb.Remove(sb.Length-1, 1);

                string sSql = string.Format(@"SELECT lLpTemplateId, lLpTemplateNm, lLpeFeeMin, lLpeFeeMax, lRateDelta, lFeeDelta, 
CASE( lLienPosT ) WHEN 0 THEN 'First' WHEN 1 THEN 'Second' ELSE '?' END AS lLienPosT, lRateSheetxmlContentOverrideBit , IsEnabled, 
IsOptionArm, lHasQRateInRateOptions, lLpDPmtT, lLpQPmtT, lLpInvestorNm, CanBeStandAlone2nd, lLT, lLpProductType, lIsArmMarginDisplayed,
lLpCustomCode1,  lLpCustomCode2, lLpCustomCode3, lLpCustomCode4, lLpCustomCode5,
lLpInvestorCode1,  lLpInvestorCode2, lLpInvestorCode3, lLpInvestorCode4, lLpInvestorCode5, lLpmiSupportedOutsidePmi, 
lQualRateCalculationT, lQualRateCalculationFieldT1, lQualRateCalculationAdjustment1, lQualRateCalculationFieldT2, lQualRateCalculationAdjustment2,
lQualTerm, lQualTermCalculationType, IsNonQmProgram, IsDisplayInNonQmQuickPricer
FROM LOAN_PROGRAM_TEMPLATE WHERE IsMaster=0 AND lLpTemplateId IN ({0})", sb.ToString());

				Tools.LogRegTest(sSql);

                // 4/15/2005 kb - Get the selected rows' details (including lien
                // position) so we can know what we're editing.

                DataSet ds = new DataSet();
                DBSelectUtility.FillDataSet(DataSrc.LpeSrc, ds, sSql, null, parameters);

                m_dgPrograms.DataSource = ds.Tables[0].DefaultView;
				m_dgPrograms.DataBind();

				// 4/15/2005 kb - Check the lien positions to see if we're editing
				// a mixed bag.  Product pairing is position specific, so in a
				// mixed mode, we turn off these updates.

				int nF = 0 , nS = 0 , nO = 0;

				foreach( DataRow dR in ds.Tables[0].Rows )
				{
					string lPos = ( String ) dR[ "lLienPosT" ];

					if( lPos == "First" )
					{
						++nF; continue;
					}

					if( lPos == "Second" )
					{
						++nS; continue;
					}

					++nO;
				}

				if( nF * nS != 0 || nF * nO != 0 || nS * nO != 0 )
				{
					// Mixed!  Turn off updates on pairing edits.

					LienLinks_upd.Enabled = false;
					LienLinks_msg.Visible = true;

					PairId_upd.Enabled = false;
					PairId_msg.Visible = true;
				}
				else
					if( nS > 0 )
				{
					LienLinks_upd.Enabled = true;
					LienLinks_msg.Visible = false;

					PairId_upd.Enabled = false;
					PairId_msg.Visible = false;
				}
				else
					if( nF > 0 )
				{
					LienLinks_upd.Enabled = false;
					LienLinks_msg.Visible = false;

					PairId_upd.Enabled = true;
					PairId_msg.Visible = false;
				}

				BindInvestorList();
				//lLpProductCode_ddl_value.Value = "";
				//				BindProductCodeList();
				Tools.Bind_lLpProductType(lLpProductType_ddl);
                Bind_lAssumeLT(lAssumeLT_ddl);
                Bind_lPrepPmtT(lPrepmtPenaltyT_ddl);
                Bind_lPrepPmtT(lPrepmtRefundT_ddl);

                Tools.Bind_QualTermCalculationType(lQualTermCalculationType);

                this.QualRateCalculationFlatValue.Checked = true;
			}
		}

        private object GetQualRateFieldIds()
        {
            return new
            {
                QualRateCalculationFlatValue = this.QualRateCalculationFlatValue.ClientID,
                QualRateCalculationMaxOf = this.QualRateCalculationMaxOf.ClientID,
                QualRateField1ValuePlaceholder = this.QualRateCalculationFieldT1.ClientID,
                QualRateField2ValuePlaceholder = this.QualRateCalculationFieldT2.ClientID,
                QualRateCalculationAdjustment1 = this.lQualRateCalculationAdjustment1.ClientID,
                QualRateCalculationAdjustment2 = this.lQualRateCalculationAdjustment2.ClientID
            };
        }

        private void BindInvestorList()
		{
            var list = InvestorNameUtils.ListActiveInvestorNames(null);
			InvestorName_ddl.DataSource = list;
			InvestorName_ddl.DataTextField = "InvestorName";
			InvestorName_ddl.DataBind();
		}

        private void Bind_lAssumeLT(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("may not", "1"));
            ddl.Items.Add(new ListItem("may", "2"));
            ddl.Items.Add(new ListItem("may, subject to conditions", "3"));
        }

        private void Bind_lPrepPmtT(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("Will not", "1"));
            ddl.Items.Add(new ListItem("May", "2"));
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            this.EnableJqueryMigrate = false;
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Init += new EventHandler(this.PageInit);
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void btnOK_Click(object sender, System.EventArgs args)
		{
            try
            {
                if (this.QRateCalculation_upd.Checked &&
                    this.QualRateCalculationMaxOf.Checked && 
                    this.QualRateCalculationFieldT1.Value == this.QualRateCalculationFieldT2.Value)
                {
                    throw new CBaseException(ErrorMessages.QualRateFieldsMustBeMutuallyExclusive, ErrorMessages.QualRateFieldsMustBeMutuallyExclusive);
                }

                foreach (Guid lpId in m_dgPrograms.DataKeys)
                {
                    UpdateProgram(lpId);
                }

                LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this);
            }
            catch (CBaseException e)
            {
                // Oops!

                Tools.LogError(ErrorMessage = "Unable to save: " + e.UserMessage + ".", e);
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError(ErrorMessage = "Failed to save.", e);
            }
        }

		// OPM 16984 Cord
/*		function validateProductCode()
		{
			if ( BrokerUser.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) ) 
			{
				if ( bProductCodeValid == false )
				{
					alert("Invalid product code selected.");
					return false;
				}	
			}
			return true;
		}
*/
		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this) ;
		}

		//OPM 16984 Cord
		protected int GetInvestorCount()
		{
			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataAccess.DataSrc.LpeSrc, "ListInvestorWithValidProductCode"))
			{
				if( reader.Read() )
				{
					return reader.GetInt32(0);
				}
				else
				{
					return -1;
				}
			}			
		}

		protected String SafeJsString(String s)
		{
			if ( s != null )
			{
				//Replace \ with \\
				s = s.Replace("\\", "\\\\");

				//Replace ' with \'
				s = s.Replace("'", "\\'");

				//Replace " with \"
				s = s.Replace("\"", "\\\"");
			}

			return s;
		}

        protected string RenderQRateCalculationType(object item)
        {
            var row = item as DataRowView;

            if (!Convert.ToBoolean(row["lHasQRateInRateOptions"]))
            {
                return string.Empty;
            }

            return EnumUtilities.GetDescription((QualRateCalculationT)row["lQualRateCalculationT"]);
        }

        protected string RenderQRateCalculationFieldType(object item, string fieldName)
        {
            var row = item as DataRowView;

            if (!Convert.ToBoolean(row["lHasQRateInRateOptions"]))
            {
                return string.Empty;
            }

            var calculationType = (QualRateCalculationT)row["lQualRateCalculationT"];
            if (fieldName == "lQualRateCalculationFieldT2" && calculationType == QualRateCalculationT.FlatValue)
            {
                return string.Empty;
            }

            return EnumUtilities.GetDescription((QualRateCalculationFieldT)row[fieldName]);
        }

        protected string RenderQTerm(object item)
        {
            DataRowView row = item as DataRowView;

            QualTermCalculationType calculationType = (QualTermCalculationType)row["lQualTermCalculationType"];
            if (calculationType != QualTermCalculationType.Manual)
            {
                return string.Empty;
            }

            return row["lQualTerm"].ToString();
        }

        protected string RenderQTermCalcType(object item)
        {
            DataRowView row = item as DataRowView;
            QualTermCalculationType calculationType = (QualTermCalculationType)row["lQualTermCalculationType"];
            return calculationType.ToString();
        }

        protected string RenderQRateAdjustment(object item, string fieldName)
        {
            var row = item as DataRowView;

            if (!Convert.ToBoolean(row["lHasQRateInRateOptions"]))
            {
                return string.Empty;
            }

            var calculationType = (QualRateCalculationT)row["lQualRateCalculationT"];
            if (fieldName == "lQualRateCalculationAdjustment2" && calculationType == QualRateCalculationT.FlatValue)
            {
                return string.Empty;
            }

            return this.Converter.ToRateString((decimal)row[fieldName]);
        }

        protected string RenderDisplayPayment( object item )
		{
			DataRowView row = item as DataRowView;
			switch ( (E_sLpDPmtT) row["lLpDPmtT"] )
			{
				case E_sLpDPmtT.UserSelection: return "DRate + user selection of I/O";
				case E_sLpDPmtT.InterestOnly: return "DRate + I/O";
				case E_sLpDPmtT.PAndI: return "DRate + P&I";
				default: Tools.LogBug("Display Payment Type"); break;
			}
			return "";
		}

		protected string RenderQualifyPayment( object item )
		{
			DataRowView row = item as DataRowView;
			switch ( (E_sLpQPmtT) row["lLpQPmtT"] )
			{
				case E_sLpQPmtT.UserSelection: return "QRate + user selection of I/O";
				case E_sLpQPmtT.InterestOnly: return "QRate + I/O";
				case E_sLpQPmtT.PAndI: return "QRate + P&I";
				default: Tools.LogBug("Qualify Payment Type"); break;
			}
			return "";
		}

		protected string RenderLoanType( object item )
		{
			DataRowView row = item as DataRowView;
			switch ( (E_sLT) row["lLT"] )
			{
				case E_sLT.Conventional: return "Conventional";
				case E_sLT.FHA: return "FHA";
				case E_sLT.Other: return "Other";
                case E_sLT.UsdaRural: return "USDA/Rural Housing";
				case E_sLT.VA: return "VA";
				default: Tools.LogBug("Unknown Loan Type"); break;
			}
			return "";
		}

		private void UpdateProgram(Guid lpId)
		{
			// Validate that the current credentials are enough to execute
			// a save operation on this data object.  The data object itself
			// should define the access control -- need to centralize this.

			CLoanProductData data = new DataAccess.CLoanProductData( lpId );

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine))
			{
				if( ! BrokerUser.HasPermission( Permission.CanModifyLoanPrograms )  )
				{
					ErrorMessage = "You don't have permission to modify loan programs.  Access denied.";

                    throw new CBaseException(ErrorMessages.InsufficientPermissionToEditLoan, "User tried to edit loan program without permission.");
				}
			}

            data.InitSave();

            if (lLpTemplateNm_upd.Checked)
            {
                switch (lLpTemplateNm_ov.SelectedItem.Value)
                {
                    case "inherit":
                        data.lLpTemplateNmOverrideBit = false;
                        data.lLpTemplateNm = data.lLpTemplateNmInherit;
                        break;
                    case "replace":
                        data.lLpTemplateNmOverrideBit = true;
                        data.lLpTemplateNm = data.lLpTemplateNm.Replace(lLpTemplateNm_search.Text, lLpTemplateNm_text.Text);
                        break;
                    case "override":
                        data.lLpTemplateNmOverrideBit = true;
                        data.lLpTemplateNm = lLpTemplateNm_text.Text;
                        break;
                }
            }
            if (lLpeFeeMin_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpeFeeMinOverrideBit = lLpeFeeMin_ov.SelectedItem.Value == "override";
                }
                data.lLpeFeeMinParam_rep = lLpeFeeMin_ed.Text;

                if (!data.lLpeFeeMinOverrideBit)
                {
                    data.lLpeFeeMin = data.lLpeFeeMinInherit + data.lLpeFeeMinParam;
                }
                else
                {
                    data.lLpeFeeMin = data.lLpeFeeMinParam;
                }

                /*
                if (data.lLpeFeeMinOverrideBit)
                    data.lLpeFeeMin_rep = lLpeFeeMin_ed.Text ;
                else
                    data.lLpeFeeMin		= data.lLpeFeeMinInherit ;
                */
            }

            if (lLpeFeeMax_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpeFeeMaxOverrideBit = lLpeFeeMax_ov.Checked;
                }

                if (data.lLpeFeeMaxOverrideBit)
                    data.lLpeFeeMax_rep = lLpeFeeMax_ed.Text;
                else
                    data.lLpeFeeMax = data.lLpeFeeMaxInherit;
            }

            if (isEnabled_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.IsEnabledOverrideBit = isEnabled_ov.Checked;
                }

                if (data.IsEnabledOverrideBit)
                    data.IsEnabled = isEnabled_cb.Checked;
                else
                    data.IsEnabled = data.IsEnabledInherit;
            }

            if (LienLinks_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.PairingProductIdsOverrideBit = LienLinks_ov.Checked;
                }

                if (data.PairingProductIdsOverrideBit)
                    data.PairingProductIds_rep = LienLinks_ed.Text;
                else
                    data.PairingProductIds = data.PairingProductIdsInherit;
            }

            if (PairId_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.PairingProductIdsOverrideBit = PairId_ov.Checked;
                }

                if (data.PairingProductIdsOverrideBit)
                    data.PairIdFor1stLienProdGuid = data.lLpTemplateId;
                else
                    data.PairIdFor1stLienProdGuid = data.PairIdFor1stLienProdGuidInherit;
            }

            if (lRateDelta_upd.Checked)
                data.lRateDelta_rep = lRateDelta_ed.Text;

            if (lFeeDelta_upd.Checked)
                data.lFeeDelta_rep = lFeeDelta_ed.Text;

            if (lRateSheet_upd.Checked)
            {
                data.lRateSheetXmlContentOverrideBit = lRateSheet_ov.Checked;
                if (lRateSheet_ov.Checked)
                    data.UploadCSVRateSheet(lRateSheet_ed.Text);
            }

            if (DisplayMargin_upd.Checked && data.lBaseLpId == Guid.Empty)
            {
                data.lIsArmMarginDisplayed = DisplayMargin.Checked;
            }

            if (IsOptionArm_upd.Checked && data.lBaseLpId == Guid.Empty)
            {
                data.IsOptionArm = IsOptionArm.Checked;
            }

            // 12/21/06 OPM 8838
            if (UseQRate_upd.Checked && data.lBaseLpId == Guid.Empty)
            {
                data.lHasQRateInRateOptions = UseQRate.Checked;
            }

            if (this.QRateCalculation_upd.Checked && data.lBaseLpId == Guid.Empty)
            {
                data.lQualRateCalculationT = this.QualRateCalculationMaxOf.Checked ? QualRateCalculationT.MaxOf : QualRateCalculationT.FlatValue;

                QualRateCalculationFieldT temp;
                if (!Enum.TryParse(this.QualRateCalculationFieldT1.Value, out temp) ||
                    !Enum.IsDefined(typeof(QualRateCalculationFieldT), temp))
                {
                    var msg = "Please enter a valid value for the first Qual Rate calculation field.";
                    throw new CBaseException(msg, msg);
                }

                data.lQualRateCalculationFieldT1 = temp;

                if (UseQRate.Checked &&
                    (!Enum.TryParse(this.QualRateCalculationFieldT2.Value, out temp) ||
                    !Enum.IsDefined(typeof(QualRateCalculationFieldT), temp)) &&
                    data.lQualRateCalculationT == QualRateCalculationT.MaxOf)
                {
                    var msg = "Please enter a valid value for the second Qual Rate calculation field.";
                    throw new CBaseException(msg, msg);
                }

                data.lQualRateCalculationFieldT2 = temp;

                data.lQualRateCalculationAdjustment1_rep = this.lQualRateCalculationAdjustment1.Text;
                data.lQualRateCalculationAdjustment2_rep = this.lQualRateCalculationAdjustment2.Text;
            }

            if (updateQualTermCalc.Checked)
            {
                data.lQualTerm_rep = this.lQualTerm.Value;

                QualTermCalculationType qTermCalcType;
                if (Enum.TryParse(this.lQualTermCalculationType.SelectedValue, out qTermCalcType) &&
                    Enum.IsDefined(typeof(QualTermCalculationType), qTermCalcType))
                {
                    data.lQualTermCalculationType = qTermCalcType;
                }
            }

            if (DisplayPayment_upd.Checked && data.lBaseLpId == Guid.Empty)
            {
                data.lLpDPmtT = (E_sLpDPmtT)Tools.GetDropDownListValue(DisplayPayment);
            }

            if (QualifyPayment_upd.Checked && data.lBaseLpId == Guid.Empty)
            {
                data.lLpQPmtT = (E_sLpQPmtT)Tools.GetDropDownListValue(QualifyPayment);
            }

            // 12/21/06 OPM 8328
            if (InvestorNameProductCode_upd.Checked)
            {
                data.ProductCodeOverrideBit = this.lLpProductCode_ov.Checked;

                if (data.lBaseLpId == Guid.Empty || this.lLpProductCode_ov.Checked)
                {
                    data.ProductCode = lLpProductCode_ddl_value.Value;
                }

                if (data.lBaseLpId == Guid.Empty)
                {
                    data.lLpInvestorNm = InvestorName_ddl.SelectedItem.Text;
                }
            }

            // 8/24/07 OPM 4442, 17401
            if (CanBeStandAlone2nd_upd.Checked
                && data.lBaseLpId == Guid.Empty
                && data.lLienPosT == E_sLienPosT.Second)
            {
                data.CanBeStandAlone2nd = CanBeStandAlone2nd.Checked;
            }

            // 05/27/08 mf. OPM 22179 - For Rate Merge feature.
            if (lLpProductType_upd.Checked && data.lBaseLpId == Guid.Empty)
            {
                data.lLpProductType = lLpProductType_ddl.SelectedItem.Text; //opm 22179 fs 09/02/08
            }

            if (lLT_upd.Checked && data.lBaseLpId == Guid.Empty)
            {
                data.lLT = (E_sLT)Tools.GetDropDownListValue(lLT);
            }
            if (lLpCustomCode1_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpCustomCode1OverrideBit = lLpCustomCode1_ov.Checked;

                    if (data.lLpCustomCode1OverrideBit)
                        data.lLpCustomCode1 = lLpCustomCode1_ed.Text;
                    else
                        data.lLpCustomCode1 = data.lLpCustomCode1Inherit;
                }
                else
                {
                    data.lLpCustomCode1OverrideBit = true;
                    data.lLpCustomCode1 = lLpCustomCode1_ed.Text;
                }
            }

            if (lLpCustomCode2_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpCustomCode2OverrideBit = lLpCustomCode2_ov.Checked;

                    if (data.lLpCustomCode2OverrideBit)
                        data.lLpCustomCode2 = lLpCustomCode2_ed.Text;
                    else
                        data.lLpCustomCode2 = data.lLpCustomCode2Inherit;
                }
                else
                {
                    data.lLpCustomCode2OverrideBit = true;
                    data.lLpCustomCode2 = lLpCustomCode2_ed.Text;
                }
            }

            if (lLpCustomCode3_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpCustomCode3OverrideBit = lLpCustomCode3_ov.Checked;

                    if (data.lLpCustomCode3OverrideBit)
                        data.lLpCustomCode3 = lLpCustomCode3_ed.Text;
                    else
                        data.lLpCustomCode3 = data.lLpCustomCode3Inherit;
                }
                else
                {
                    data.lLpCustomCode3OverrideBit = true;
                    data.lLpCustomCode3 = lLpCustomCode3_ed.Text;
                }
            }

            if (lLpCustomCode4_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpCustomCode4OverrideBit = lLpCustomCode4_ov.Checked;

                    if (data.lLpCustomCode4OverrideBit)
                        data.lLpCustomCode4 = lLpCustomCode4_ed.Text;
                    else
                        data.lLpCustomCode4 = data.lLpCustomCode4Inherit;
                }
                else
                {
                    data.lLpCustomCode4OverrideBit = true;
                    data.lLpCustomCode4 = lLpCustomCode4_ed.Text;
                }
            }

            if (lLpCustomCode5_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpCustomCode5OverrideBit = lLpCustomCode5_ov.Checked;

                    if (data.lLpCustomCode5OverrideBit)
                        data.lLpCustomCode5 = lLpCustomCode5_ed.Text;
                    else
                        data.lLpCustomCode5 = data.lLpCustomCode5Inherit;
                }
                else
                {
                    data.lLpCustomCode5OverrideBit = true;
                    data.lLpCustomCode5 = lLpCustomCode5_ed.Text;
                }
            }

            if (lLpInvestorCode1_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpInvestorCode1OverrideBit = lLpInvestorCode1_ov.Checked;

                    if (data.lLpInvestorCode1OverrideBit)
                        data.lLpInvestorCode1 = lLpInvestorCode1_ed.Text;
                    else
                        data.lLpInvestorCode1 = data.lLpInvestorCode1Inherit;
                }
                else
                {
                    data.lLpInvestorCode1OverrideBit = true;
                    data.lLpInvestorCode1 = lLpInvestorCode1_ed.Text;
                }
            }

            if (lLpInvestorCode2_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpInvestorCode2OverrideBit = lLpInvestorCode2_ov.Checked;

                    if (data.lLpInvestorCode2OverrideBit)
                        data.lLpInvestorCode2 = lLpInvestorCode2_ed.Text;
                    else
                        data.lLpInvestorCode2 = data.lLpInvestorCode2Inherit;
                }
                else
                {
                    data.lLpInvestorCode2OverrideBit = true;
                    data.lLpInvestorCode2 = lLpInvestorCode2_ed.Text;
                }
            }

            if (lLpInvestorCode3_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpInvestorCode3OverrideBit = lLpInvestorCode3_ov.Checked;

                    if (data.lLpInvestorCode3OverrideBit)
                        data.lLpInvestorCode3 = lLpInvestorCode3_ed.Text;
                    else
                        data.lLpInvestorCode3 = data.lLpInvestorCode3Inherit;
                }
                else
                {
                    data.lLpInvestorCode3OverrideBit = true;
                    data.lLpInvestorCode3 = lLpInvestorCode3_ed.Text;
                }
            }

            if (lLpInvestorCode4_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpInvestorCode4OverrideBit = lLpInvestorCode4_ov.Checked;

                    if (data.lLpInvestorCode4OverrideBit)
                        data.lLpInvestorCode4 = lLpInvestorCode4_ed.Text;
                    else
                        data.lLpInvestorCode4 = data.lLpInvestorCode4Inherit;
                }
                else
                {
                    data.lLpInvestorCode4OverrideBit = true;
                    data.lLpInvestorCode4 = lLpInvestorCode4_ed.Text;
                }
            }

            if (lLpInvestorCode5_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpInvestorCode5OverrideBit = lLpInvestorCode5_ov.Checked;

                    if (data.lLpInvestorCode5OverrideBit)
                        data.lLpInvestorCode5 = lLpInvestorCode5_ed.Text;
                    else
                        data.lLpInvestorCode5 = data.lLpInvestorCode5Inherit;
                }
                else
                {
                    data.lLpInvestorCode5OverrideBit = true;
                    data.lLpInvestorCode5 = lLpInvestorCode5_ed.Text;
                }
            }

            if (lLateDays_upd.Checked)
            {
                data.lLateDays = lLateDays_ed.Text;
            }

            if (lLateChargePc_upd.Checked)
            {
                data.lLateChargePc = lLateChargePc_ed.Text;
            }

            if (lLateChargeBaseDesc_upd.Checked)
            {
                data.lLateChargeBaseDesc = lLateChargeBaseDesc_ed.Text;
            }

            if (lPrepmtPenaltyT_upd.Checked)
            {
                data.lPrepmtPenaltyT = (E_sPrepmtPenaltyT)int.Parse(lPrepmtPenaltyT_ddl.SelectedValue);
            }

            if (lPrepmtRefundT_upd.Checked)
            {
                data.lPrepmtRefundT = (E_sPrepmtRefundT)int.Parse(lPrepmtRefundT_ddl.SelectedValue);
            }

            if (lAssumeLT_upd.Checked)
            {
                data.lAssumeLT = (E_sAssumeLT)int.Parse(lAssumeLT_ddl.SelectedValue);
            }


            if (lLpmiSupportedOutsidePmi_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.lLpmiSupportedOutsidePmiOverrideBit = lLpmiSupportedOutsidePmi_ov.Checked;
                }

                if (data.lLpmiSupportedOutsidePmiOverrideBit)
                    data.lLpmiSupportedOutsidePmi = lLpmiSupportedOutsidePmi.Checked;
                else
                    data.lLpmiSupportedOutsidePmi = data.lLpmiSupportedOutsidePmiInherit;
            }

            if (IsNonQmProgram_upd.Checked)
            {                
                data.IsNonQmProgram = IsNonQmProgram.Checked;
            }

            if (IsDisplayInNonQmQuickPricer_upd.Checked)
            {
                if (data.lBaseLpId != Guid.Empty)
                {
                    data.IsDisplayInNonQmQuickPricerOverride = IsDisplayInNonQmQuickPricer_ov.Checked;
                }

                if (data.IsDisplayInNonQmQuickPricerOverride)
                    data.IsDisplayInNonQmQuickPricer = IsDisplayInNonQmQuickPricer.Checked;
                else
                    data.IsDisplayInNonQmQuickPricer = data.IsDisplayInNonQmQuickPricerInherit;                
            }

            data.Save();
		}
	}
}
