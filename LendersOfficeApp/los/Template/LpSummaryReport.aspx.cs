using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.LoanPrograms;
using LendersOffice.Security;
using DataAccess;
using CommonLib;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;

namespace LendersOfficeApp.los.Template
{
	public partial class LpSummaryReport : LendersOffice.Common.BasePage
	{
		protected MeridianLink.CommonControls.CommonDataGrid CommonDataGrid1;
		protected ArrayList m_FolderList ;
	
		private BrokerUserPrincipal BrokerUser
		{
			// Access member.
            get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			// 4/18/2005 kb - Load the programs.  If not the special user,
			// then deny access.

			if( BrokerUser.HasPermission( Permission.CanModifyLoanPrograms ) == true )
			{
				m_FolderList = GetFolderList(Request["folderid"]) ;
				m_Denied.Visible = false ;
			}
			else
			{
				m_Denied.Visible = true ;
			}
		}
		ArrayList GetFolderList(string sFolderId)
		{
			FolderInfo parentFolderInfo = null;
			ArrayList folderList = new ArrayList() ;

            // process the top level folder
            // 11/16/2007 dd - Review and fixed.
            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@p", sFolderId) };
            string sSql = "SELECT FolderName FROM LOAN_PRODUCT_FOLDER WHERE FolderId = @p";

            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                sdr.Read();
                parentFolderInfo = new FolderInfo(sFolderId, sdr["FolderName"].ToString());
                folderList.Add(parentFolderInfo);
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sSql, null, listParams, readHandler);

			// process child folders
			Stack folderIdStack = new Stack() ;
			folderIdStack.Push(parentFolderInfo) ;
			while (folderIdStack.Count > 0)
			{
				parentFolderInfo = (FolderInfo)folderIdStack.Pop() ;
                // 11/16/2007 dd - Review and fixed
                listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@p", parentFolderInfo.sFolderId) };
                sSql = "SELECT FolderId, FolderName FROM LOAN_PRODUCT_FOLDER WHERE ParentFolderId = @p ORDER BY FolderName DESC";

                Action<IDataReader> readHandler2 = delegate(IDataReader sdr)
                {
                    while (sdr.Read())
                    {
                        FolderInfo fInfo = new FolderInfo(sdr["FolderId"].ToString(), parentFolderInfo.sFolderName + " \\ " + sdr["FolderName"].ToString());
                        folderList.Add(fInfo);
                        folderIdStack.Push(fInfo);
                    }
                };

                DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sSql, null, listParams, readHandler2);
			}

			return folderList ;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
	public class FolderInfo
	{
		public string sFolderId ;
		public string sFolderName ;
		public ArrayList rgPrograms ;

		public FolderInfo(string _sFolderId, string _sFolderName)
		{
			sFolderId = _sFolderId ;
			sFolderName = _sFolderName ;

			rgPrograms = new ArrayList() ;

            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@p", sFolderId) };
            string sSql = "SELECT lLpTemplateId, lLpTemplateNm, lLpInvestorNm, lLienPosT, lFinMethT, lTerm, lDue, lRadj1stCapMon, lRAdjCapMon, lLockedDaysLowerSearch, lLockedDays, lPpmtPenaltyMonLowerSearch, lPpmtPenaltyMon, lLpeFeeMin, lLpeFeeMax, lRateDelta, lFeeDelta, IsEnabled, PairingProductIds, PairIdFor1stLienProdGuid FROM LOAN_PROGRAM_TEMPLATE WHERE IsMaster=0 AND FolderId = @p ORDER BY lLpTemplateNm";

            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                while (sdr.Read())
                {
                    ProgramInfo pInfo = new ProgramInfo();
                    pInfo.sLpId = SafeConvert.ToString(sdr["lLpTemplateId"]);
                    pInfo.sLpName = SafeConvert.ToString(sdr["lLpTemplateNm"]);
                    pInfo.sInvestor = SafeConvert.ToString(sdr["lLpInvestorNm"]);
                    pInfo.sLienPos = Formatter.GetLienPosition(sdr["lLienPosT"]);
                    pInfo.sFinMeth = Formatter.GetFinanceMethod(sdr["lFinMethT"]);
                    pInfo.sTerm = SafeConvert.ToString(sdr["lTerm"]);
                    pInfo.sDue = SafeConvert.ToString(sdr["lDue"]);
                    pInfo.s1stAdjMon = SafeConvert.ToString(sdr["lRadj1stCapMon"]);
                    pInfo.sAdjPeriod = SafeConvert.ToString(sdr["lRAdjCapMon"]);
                    pInfo.sLockDays = SafeConvert.ToString(sdr["lLockedDaysLowerSearch"]) + ", " + SafeConvert.ToString(sdr["lLockedDays"]);
                    pInfo.sPrePay = SafeConvert.ToString(sdr["lPpmtPenaltyMonLowerSearch"]) + ", " + SafeConvert.ToString(sdr["lPpmtPenaltyMon"]);
                    pInfo.sMaxYsp = SafeConvert.ToString(sdr["lLpeFeeMin"]);
                    pInfo.sMinYsp = SafeConvert.ToString(sdr["lLpeFeeMax"]);
                    pInfo.sRateAdj = SafeConvert.ToString(sdr["lRateDelta"]);
                    pInfo.sFeeAdj = SafeConvert.ToString(sdr["lFeeDelta"]);
                    pInfo.sEnabled = Convert.ToInt32(sdr["IsEnabled"]) == 0 ? "NO" : "YES";
                    pInfo.s1stLienLinks = SafeConvert.ToString(sdr["PairingProductIds"]);
                    pInfo.s1stLienPairId = SafeConvert.ToString(sdr["PairIdFor1stLienProdGuid"]);

                    rgPrograms.Add(pInfo);
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sSql, null, listParams, readHandler);
		}
	}
	public class ProgramInfo
	{
		public string sLpId ;
		public string sLpName ;
		public string sInvestor ;
		public string sLienPos ;
		public string sFinMeth ;
		public string sTerm ;
		public string sDue ;
		public string s1stAdjMon ;
		public string sAdjPeriod ;
		public string sLockDays ;
		public string sPrePay ;
		public string sMaxYsp ;
		public string sMinYsp ;
		public string sRateAdj ;
		public string sFeeAdj ;
		public string sEnabled ;
		public string s1stLienLinks ;
		public string s1stLienPairId ;
	}
	class Formatter
	{
		public static string GetFinanceMethod( object fMeth )
		{
			try
			{
				return Convert.ToString( ( E_sFinMethT ) Convert.ToInt32( fMeth ) );
			}
			catch
			{
				return "?";
			}
		}
		public static string GetLienPosition(object fLienPos)
		{
			try
			{
				return Convert.ToString((E_sLienPosT) Convert.ToInt32(fLienPos)) ;
			}
			catch
			{
				return "?";
			}
		}
	}
}
