using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Services;
using LendersOffice.RatePrice;

namespace LendersOfficeApp.los.Template
{
    public partial class LoanProgramTemplate : System.Web.UI.UserControl
    {

        #region Protected member variables
        protected System.Web.UI.WebControls.DropDownList lLateChargeBaseT_dd;
        protected System.Web.UI.WebControls.DropDownList lRAdjRound_rad;
        protected LoanProgramArmDisclosure armDisclosure;
        protected RenderReadOnly readOnly;
        // 11/24/08 fs - Replaced dropdownlist for vs2008 migration
        // 08/22/07 OPM 12773
        //opm 22179 fs 09/02/08
        #endregion

        protected BrokerUserPrincipal BrokerUser => BrokerUserPrincipal.CurrentPrincipal;

        private string ErrorMessage
        {
            // Write out popup message to alert user of error.
            set
            {
                Page.ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + ".");
            }
        }

        private String CommandToDo
        {
            // Save a command for the script to execute.

            set { Page.ClientScript.RegisterHiddenField("m_commandToDo", value); }
        }

        private Guid m_fileId => RequestHelper.GetGuid("FileId");

        protected void PageInit(object sender, System.EventArgs a)
        {
            if (IsPostBack == false)
            {
                BindClosingCostList();
                BindInvestorList();
            }
            Tools.Bind_lLpProductType(lLpProductType_ddl);

            Tools.Bind_sHelocPmtBaseT(lHelocPmtBaseT);
            Tools.Bind_sHelocPmtFormulaT(lHelocPmtFormulaT);
            Tools.Bind_sHelocPmtFormulaRateT(lHelocPmtFormulaRateT);
            Tools.Bind_sHelocPmtAmortTermT(lHelocPmtAmortTermT);

            Tools.Bind_sHelocPmtBaseT(lHelocQualPmtBaseT);
            Tools.Bind_sHelocPmtFormulaT(lHelocQualPmtFormulaT);
            Tools.Bind_sHelocPmtFormulaRateT(lHelocQualPmtFormulaRateT);
            Tools.Bind_sHelocPmtAmortTermT(lHelocQualPmtAmortTermT);

            Tools.Bind_QualTermCalculationType(lQualTermCalculationType);

            var basePage = this.Page as BasePage;
            if (basePage == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Loan program template control is expected to be hosted on a BasePage.");
            }
            else
            {
                basePage.RegisterJsObjectWithJsonNetAnonymousSerializer("QualRateFieldIds", this.GetQualRateFieldIds());
            }
        }

        protected void PageLoad(object sender, System.EventArgs a)
        {
            if (!Page.IsPostBack)
            {
                //opm 10103 fs 06/25/08
                m_applyBtn.Attributes.Add("onclick", "f_removeSpecialCharacters();");

                LoadData();

                string mode = RequestHelper.GetSafeQueryString("mode");
                if (string.IsNullOrEmpty(mode) == false && mode == "new")
                {
                    // 04/17/12.  OPM 13035. Special mode for when SAE creates a new program.
                    // They want some special defaults and validation to help prevent errors,
                    // as well as a real "cancel".  This is a bit of a hacky way to provide
                    // that without disturbing much of this older code.

                    SetAuthorCreateMode();
                }
            }
            else
            {
                PostData();
            }

            // 7/15/2004 kb - Update control visibility for pricing engine.

            if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
            {
                // Because pricing engine is enabled, show the links
                // and hide the simple edit.

                lNoteR_ed.Visible = false;
                lLOrigFPc_ed.Visible = false;

                lLpInvestorNm_ed.Visible = false;
                lLpInvestorNm_ddl.Visible = true;

                //OPM 16984 Cord
                ProductCode.Visible = false;
                ProductCode_ddl.Visible = true;

                lQualR_ed.Visible = false;

                m_ratesheetLink1.Visible = true;
                m_ratesheetLink1.Attributes.Add("onclick", "showModal('/los/Template/EditRateSheet.aspx?fileId=" + m_fileId + "', null, null, null, null, { hideCloseButton: true } ); return false;");

                m_ratesheetLink2.Visible = true;
                m_ratesheetLink2.Attributes.Add("onclick", "showModal('/los/Template/EditRateSheet.aspx?fileId=" + m_fileId + "', null, null, null, null, { hideCloseButton: true } ); return false;");

                m_ratesheetLink3.Visible = true;
                m_ratesheetLink3.Attributes.Add("onclick", "showModal('/los/Template/EditRateSheet.aspx?fileId=" + m_fileId + "', null, null, null, null, { hideCloseButton: true } ); return false;");

                m_ratesheetLink4.Visible = true;
                m_ratesheetLink4.Attributes.Add("onclick", "showModal('/los/Template/EditRateSheet.aspx?fileId=" + m_fileId + "', null, null, null, null, { hideCloseButton: true } ); return false;");

                lLpeFeePanel_dv.Visible = true;

                lLpTemplateId.Visible = true;

                lLienPosT_dd.Enabled = false;

                isEnabled_pn.Visible = true;
            }
            else if (!BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine))
            {
                // Show simple, non-pricing engine ui that allows
                // for just one pair to be saved.

                m_ratesheetLink1.Visible = false;
                m_ratesheetLink2.Visible = false;

                lLpInvestorNm_ed.Visible = true;
                lLpInvestorNm_ddl.Visible = false;

                //OPM 16984 Cord
                ProductCode.Visible = true;
                ProductCode_ddl.Visible = false;


                lLpeFeePanel_dv.Visible = false;

                lNoteR_ed.Visible = true;
                lLOrigFPc_ed.Visible = true;

                lQualR_ed.Visible = true;

                lLpTemplateId.Visible = false;
                IsMaster.Visible = false;

                lLienPosT_dd.Enabled = true;

                isEnabled_pn.Visible = false;
            }
            else
            {
                // User has pricing engine, but they can't use it, so hide
                // the rate point pair data.

                m_ratesheetLink1.Visible = false;
                m_ratesheetLink2.Visible = false;

                lLpInvestorNm_ed.Visible = true;
                lLpInvestorNm_ddl.Visible = false;

                //OPM 16984 Cord
                ProductCode.Visible = true;
                ProductCode_ddl.Visible = false;

                lLpeFeePanel_dv.Visible = false;

                lLpTemplateId.Visible = false;
                IsMaster.Visible = false;

                lLienPosT_dd.Enabled = true;

                isEnabled_pn.Visible = false;
            }
        }

        private object GetQualRateFieldIds()
        {
            return new
            {
                DisableQualRateCalculationFieldsForShowDerived = this.DisableQualRateCalculationFieldsForShowDerived.ClientID,
                ShouldUseQualRate = this.lHasQRateInRateOptions.ClientID,
                QualRateCalculationFlatValue = this.QualRateCalculationFlatValue.ClientID,
                QualRateCalculationMaxOf = this.QualRateCalculationMaxOf.ClientID,
                QualRateField1ValuePlaceholder = this.QualRateCalculationFieldT1.ClientID,
                QualRateField2ValuePlaceholder = this.QualRateCalculationFieldT2.ClientID,
                QualRateCalculationAdjustment1 = this.lQualRateCalculationAdjustment1.ClientID,
                QualRateCalculationAdjustment2 = this.lQualRateCalculationAdjustment2.ClientID
            };
        }

        private void BindInvestorList()
        {
            var list = InvestorNameUtils.ListActiveInvestorNames(null);
            lLpInvestorNm_ddl.DataSource = list;
            lLpInvestorNm_ddl.DataTextField = "InvestorName";
            lLpInvestorNm_ddl.DataBind();
        }

        private void SetListValue(string investor)
        {
            foreach (ListItem item in lLpInvestorNm_ddl.Items)
            {
                if (item.Value == investor)
                {
                    item.Selected = true;
                    break;
                }
            }
        }

        protected void PagePreRender(object sender, System.EventArgs a)
        {
            // Bind our pairing set to what we've cached for
            // saving on apply.

            if (lLienPosT_dd.SelectedIndex != 0)
            {
                PairIdFor1stLienProdGuid_pn.Visible = false;
                PairingProductIds_pn.Visible = true;
                IsPairedOnlyWithSameInvestor.Visible = false;
                PairingProd.Visible = true;
                CanBeStandAlone2nd.Visible = true;
            }
            else
            {
                PairIdFor1stLienProdGuid_pn.Visible = true;
                PairingProductIds_pn.Visible = false;
                IsPairedOnlyWithSameInvestor.Visible = true;
                PairingProd.Visible = false;
                CanBeStandAlone2nd.Visible = false;
            }

            // Show latest override state for each.

            RedrawOverrides();
        }

        /// <summary>
        /// Update override text box visibility for managing what a user
        /// sees.  Resulting view should be what the final state of the
        /// loan program is.
        /// </summary>

        private void RedrawOverrides()
        {
            // Consider each overrideable field and update its visibility
            // based on what is checked.

            if (lLpTemplateNm_ov.Checked == true)
            {
                lLpTemplateNm_ov.ForeColor = Color.DarkGray;

                lLpTemplateNm_in.Visible = false;
                lLpTemplateNm_ed.Visible = true;
            }
            else
            {
                lLpTemplateNm_ov.ForeColor = Color.Black;

                lLpTemplateNm_ed.Visible = false;
                lLpTemplateNm_in.Visible = true;
            }

            if (lLendNm_ov.Checked == true)
            {
                lLendNm_ov.ForeColor = Color.DarkGray;

                lLendNm_in.Visible = false;
                lLendNm_ed.Visible = true;
            }
            else
            {
                lLendNm_ov.ForeColor = Color.Black;

                lLendNm_ed.Visible = false;
                lLendNm_in.Visible = true;
            }

            if (lCcTemplateId_ov.Checked == true)
            {
                lCcTemplateId_ov.ForeColor = Color.DarkGray;

                lCcTemplateId_in.Visible = false;
                lCcTemplateId_dd.Visible = true;
            }
            else
            {
                lCcTemplateId_ov.ForeColor = Color.Black;

                lCcTemplateId_dd.Visible = false;
                lCcTemplateId_in.Visible = true;
            }

            if (lLpeFeeMax_ov.Checked == true)
            {
                lLpeFeeMax_ov.ForeColor = Color.DarkGray;

                lLpeFeeMax_in.Visible = false;
                lLpeFeeMax_ed.Visible = true;
            }
            else
            {
                lLpeFeeMax_ov.ForeColor = Color.Black;

                lLpeFeeMax_ed.Visible = false;
                lLpeFeeMax_in.Visible = true;
            }

            if (PairingProductIds_ov.Checked)
            {
                PairingProductIds_ov.ForeColor = Color.DarkGray;

                PairIdFor1stLienProdGuid_in.Visible = false;
                PairIdFor1stLienProdGuid_tx.Visible = true;

                PairingProductIds_in.Visible = false;
                PairingProductIds_ed.Visible = true;
            }
            else
            {
                PairingProductIds_ov.ForeColor = Color.Black;

                PairIdFor1stLienProdGuid_tx.Visible = false;
                PairIdFor1stLienProdGuid_in.Visible = true;

                PairingProductIds_ed.Visible = false;
                PairingProductIds_in.Visible = true;
            }


            if (isEnabled_ov.Checked)
            {
                isEnabled_ov.ForeColor = Color.DarkGray;

                isEnabled_in.Visible = false;
                isEnabled_cb.Visible = true;
            }
            else
            {
                isEnabled_ov.ForeColor = Color.Black;

                isEnabled_cb.Visible = false;
                isEnabled_in.Visible = true;
            }

            if ( lLpmiSupportedOutsidePmi_ov.Checked == true)
            {
                lLpmiSupportedOutsidePmi_ov.ForeColor = Color.DarkGray;

                lLpmiSupportedOutsidePmi_in.Visible = false;

                if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    lLpmiSupportedOutsidePmi.Visible = true;
                }
            }
            else
            {
                lLpmiSupportedOutsidePmi_ov.ForeColor = Color.Black;

                lLpmiSupportedOutsidePmi.Visible = false;

                if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    lLpmiSupportedOutsidePmi_in.Visible = true;
                }
            }

            if (IsDisplayInNonQmQuickPricer_ov.Checked)
            {
                IsDisplayInNonQmQuickPricer_ov.ForeColor = Color.DarkGray;
                IsDisplayInNonQmQuickPricer_in.Visible = false;

                if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    IsDisplayInNonQmQuickPricer.Visible = true;
                }
            }
            else
            {
                IsDisplayInNonQmQuickPricer_ov.ForeColor = Color.Black;
                IsDisplayInNonQmQuickPricer.Visible = false;

                if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    IsDisplayInNonQmQuickPricer_in.Visible = true;
                }
            }

            if (ProductCode_ov.Checked == true)
            {
                ProductCode_ov.ForeColor = Color.DarkGray;

                ProductCode_in.Visible = false;

                if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    ProductCode_ddl.Visible = true;
                }
            }
            else
            {
                ProductCode_ov.ForeColor = Color.Black;

                ProductCode_ddl.Visible = false;

                if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    ProductCode_in.Visible = true;
                }
            }

            if (ProductIdentifierOverride.Checked)
            {
                ProductIdentifierOverride.ForeColor = Color.DarkGray;
                ProductIdentifierInherited.Visible = false;
                ProductIdentifier.Visible = true;
            }
            else
            {
                ProductIdentifierOverride.ForeColor = Color.Black;
                ProductIdentifier.Visible = false;
                ProductIdentifierInherited.Visible = true;
            }

            RedrawOverrides(lLpCustomCode1_ov, lLpCustomCode1);
            RedrawOverrides(lLpCustomCode2_ov, lLpCustomCode2);
            RedrawOverrides(lLpCustomCode3_ov, lLpCustomCode3);
            RedrawOverrides(lLpCustomCode4_ov, lLpCustomCode4);
            RedrawOverrides(lLpCustomCode5_ov, lLpCustomCode5);
            RedrawOverrides(lLpInvestorCode1_ov, lLpInvestorCode1);
            RedrawOverrides(lLpInvestorCode2_ov, lLpInvestorCode2);
            RedrawOverrides(lLpInvestorCode3_ov, lLpInvestorCode3);
            RedrawOverrides(lLpInvestorCode4_ov, lLpInvestorCode4);
            RedrawOverrides(lLpInvestorCode5_ov, lLpInvestorCode5);
        }


        private static void RedrawOverrides(CheckBox cb, TextBox editTextbox)
        {
            editTextbox.ReadOnly = !cb.Checked;
        }
        /// <summary>
        /// Fetch loan product details at the first.
        /// </summary>

        private void LoadData()
        {
            // Load the product from the database as-is and plop the id
            // in the upper right hand corner.

            CLoanProductData data = new DataAccess.CLoanProductData(m_fileId);
            data.InitLoad();

            lLpTemplateId.Text = data.lLpTemplateId.ToString();

            // 1/5/2005 kb - If the product is derived, then it is
            // to be marked as readonly.  If the product is not of
            // this broker, then it is to be marked as readonly.

            if (data.BrokerId != BrokerUser.BrokerId || data.lBaseLpId != Guid.Empty)
            {
                if (data.BrokerId == BrokerUser.BrokerId)
                {

                    readOnly.Skip = lLpmiSupportedOutsidePmi;
                    readOnly.Skip = lLpmiSupportedOutsidePmi_in;
                    readOnly.Skip = lLpmiSupportedOutsidePmi_ov;

                    readOnly.Skip = lLpTemplateNm_ed;
                    readOnly.Skip = lLpTemplateNm_in;
                    readOnly.Skip = lLpTemplateNm_ov;
                    readOnly.Skip = lCcTemplateId_dd;
                    readOnly.Skip = lCcTemplateId_in;
                    readOnly.Skip = lCcTemplateId_ov;
                    readOnly.Skip = lLpeFeeMin_ed;
                    readOnly.Skip = lLpeFeeMin_in;
                    readOnly.Skip = lLpeFeeMin_pm;
                    readOnly.Skip = lLpeFeeMin_ad;
                    readOnly.Skip = lLpeFeeMin_ov;
                    readOnly.Skip = lLpeFeeMax_ed;
                    readOnly.Skip = lLpeFeeMax_in;
                    readOnly.Skip = lLpeFeeMax_ov;
                    readOnly.Skip = PairingProductIds_ed;
                    readOnly.Skip = PairingProductIds_in;
                    readOnly.Skip = PairingProductIds_ov;
                    readOnly.Skip = lPpmtPenaltyMon_ed;
                    readOnly.Skip = isEnabled_cb;
                    readOnly.Skip = isEnabled_in;
                    readOnly.Skip = isEnabled_ov;
                    readOnly.Skip = lLendNm_ed;
                    readOnly.Skip = lLendNm_in;
                    readOnly.Skip = lLendNm_ov;
                    readOnly.Skip = m_applyBtn;
                    readOnly.Skip = m_okBtn;
                    readOnly.Skip = m_okPrx;

                    readOnly.Skip = lLpCustomCode1_ov;
                    readOnly.Skip = lLpCustomCode1;

                    readOnly.Skip = lLpCustomCode2_ov;
                    readOnly.Skip = lLpCustomCode2;

                    readOnly.Skip = lLpCustomCode3_ov;
                    readOnly.Skip = lLpCustomCode3;

                    readOnly.Skip = lLpCustomCode4_ov;
                    readOnly.Skip = lLpCustomCode4;

                    readOnly.Skip = lLpCustomCode5_ov;
                    readOnly.Skip = lLpCustomCode5;

                    readOnly.Skip = lLpInvestorCode1_ov;
                    readOnly.Skip = lLpInvestorCode1;

                    readOnly.Skip = lLpInvestorCode2_ov;
                    readOnly.Skip = lLpInvestorCode2;

                    readOnly.Skip = lLpInvestorCode3_ov;
                    readOnly.Skip = lLpInvestorCode3;

                    readOnly.Skip = lLpInvestorCode4_ov;
                    readOnly.Skip = lLpInvestorCode4;

                    readOnly.Skip = lLpInvestorCode5_ov;
                    readOnly.Skip = lLpInvestorCode5;

                    readOnly.Skip = ProductCode_ov;
                    readOnly.Skip = ProductCode_ddl;
                    readOnly.Skip = ProductCode_in;

                    readOnly.Skip = ProductIdentifier;
                    readOnly.Skip = ProductIdentifierInherited;
                    readOnly.Skip = ProductIdentifierOverride;

                    readOnly.Skip = IsDisplayInNonQmQuickPricer;
                    readOnly.Skip = IsDisplayInNonQmQuickPricer_ov;
                    readOnly.Skip = IsDisplayInNonQmQuickPricer_in;
                }

                readOnly.Enabled = true;
            }
            else
            {
                readOnly.Enabled = false;
            }

            // 2/8/2005 kb - We now check if we're derived so that
            // we can show the proper override ui.

            bool showDerived;

            if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) && data.lBaseLpId != Guid.Empty)
            {
                showDerived = true;
            }
            else
            {
                showDerived = false;
            }

            lLpTemplateNm_ov.Checked = data.lLpTemplateNmOverrideBit;
            lLpTemplateNm_ed.Text = data.lLpTemplateNm;
            lLpTemplateNm_in.Text = data.lLpTemplateNmInherit;

            if (showDerived)
            {
                lLpTemplateNm_in.Visible = true;
                lLpTemplateNm_ov.Visible = true;
            }
            else
            {
                lLpTemplateNm_in.Visible = false;
                lLpTemplateNm_ov.Visible = false;

                lLpTemplateNm_ov.Checked = true;
            }

            lLendNm_ov.Checked = data.lLendNmOverrideBit;
            lLendNm_ed.Text = data.lLendNm;
            lLendNm_in.Text = data.lLendNmInherit;

            if (showDerived)
            {
                lLendNm_in.Visible = true;
                lLendNm_ov.Visible = true;
            }
            else
            {
                lLendNm_in.Visible = false;
                lLendNm_ov.Visible = false;

                lLendNm_ov.Checked = true;
            }

            lCcTemplateId_ov.Checked = data.lCcTemplateIdOverrideBit;
            lCcTemplateId_in.SelectedIndex = lCcTemplateId_in.Items.IndexOf(lCcTemplateId_in.Items.FindByValue(data.lCcTemplateIdInherit.ToString()));
            lCcTemplateId_dd.SelectedIndex = lCcTemplateId_dd.Items.IndexOf(lCcTemplateId_dd.Items.FindByValue(data.lCcTemplateId.ToString()));

            if (showDerived)
            {
                lCcTemplateId_in.Visible = true;
                lCcTemplateId_ov.Visible = true;
            }
            else
            {
                lCcTemplateId_in.Visible = false;
                lCcTemplateId_ov.Visible = false;

                lCcTemplateId_ov.Checked = true;
            }


            lLockedDays_ed.Text = data.lLockedDays_rep;
            lLockedDays_ed.Visible = !data.IsLpe;

            lLpeFeeMin_ad.Checked = !data.lLpeFeeMinOverrideBit;
            lLpeFeeMin_ov.Checked = data.lLpeFeeMinOverrideBit;
            lLpeFeeMin_ed.Text = data.lLpeFeeMin_rep;
            lLpeFeeMin_in.Text = data.lLpeFeeMinInherit_rep;
            lLpeFeeMin_pm.Text = data.lLpeFeeMinParam_rep;
            lLpeFeeMin_ib.Checked = data.lLpeFeeMinAddRuleAdjInheritBit;
            lLpeFeeMin_ab.Checked = data.lLpeFeeMinAddRuleAdjBit;

            if (showDerived)
            {
                lLpeFeeMin_ad.Enabled = true;
                lLpeFeeMin_ov.Enabled = true;
            }
            else
            {
                lLpeFeeMin_ad.Enabled = false;
                lLpeFeeMin_ov.Enabled = false;
            }

            lLpeFeeMax_ov.Checked = data.lLpeFeeMaxOverrideBit;
            lLpeFeeMax_ed.Text = data.lLpeFeeMax_rep;
            lLpeFeeMax_in.Text = data.lLpeFeeMaxInherit_rep;

            // 01/04/2007 mf - OPM 7363
            if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) && data.lLienPosT == E_sLienPosT.Second)
            {
                IsOptionArm.Checked = false;
                IsOptionArm.Enabled = false;
            }
            else
            {
                IsOptionArm.Checked = data.IsOptionArm;
            }

            lHasQRateInRateOptions.Checked = data.lHasQRateInRateOptions;
            IsLpeDummyProgram.Checked = data.IsLpeDummyProgram;
            IsPairedOnlyWithSameInvestor.Checked = data.IsPairedOnlyWithSameInvestor;
            CanBeStandAlone2nd.Checked = data.CanBeStandAlone2nd;
            lDtiUsingMaxBalPc.Checked = data.lDtiUsingMaxBalPc;

            //opm 22179 fs 09/02/08
            ListItem item = lLpProductType_ddl.Items.FindByText(data.lLpProductType);
            if (item != null)
                item.Selected = true;
            else
                lLpProductType_ddl.SelectedIndex = 0;

            if (showDerived)
            {
                lHasQRateInRateOptions.Enabled = false;
                IsLpeDummyProgram.Enabled = false;
                IsOptionArm.Enabled = false;
                IsPairedOnlyWithSameInvestor.Enabled = false;
                CanBeStandAlone2nd.Enabled = false;
                lDtiUsingMaxBalPc.Enabled = false;
                lLpProductType_ddl.Enabled = false;

                this.QualRateCalculationFlatValue.Disabled = true;
                this.QualRateCalculationMaxOf.Disabled = true;
                this.lQualRateCalculationAdjustment1.Enabled = false;
                this.lQualRateCalculationAdjustment2.Enabled = false;
            }

            this.DisableQualRateCalculationFieldsForShowDerived.Value = showDerived.ToString();

            this.QualRateCalculationFlatValue.Checked = data.lQualRateCalculationT == QualRateCalculationT.FlatValue;
            this.QualRateCalculationMaxOf.Checked = data.lQualRateCalculationT == QualRateCalculationT.MaxOf;

            this.lQualRateCalculationAdjustment1.Text = data.lQualRateCalculationAdjustment1_rep;
            this.lQualRateCalculationAdjustment2.Text = data.lQualRateCalculationAdjustment2_rep;

            this.QualRateCalculationFieldT1.Value = data.lQualRateCalculationFieldT1.ToString("D");
            this.QualRateCalculationFieldT2.Value = data.lQualRateCalculationFieldT2.ToString("D");

            this.lQualTerm.Value = data.lQualTerm_rep;
            Tools.SetDropDownListValue(this.lQualTermCalculationType, data.lQualTermCalculationType);

            Tools.SetDropDownListValue(lLpDPmtT, data.lLpDPmtT);
            Tools.SetDropDownListValue(lLpQPmtT, data.lLpQPmtT);

            Tools.SetDropDownListValue(lHelocQualPmtBaseT, data.lHelocQualPmtBaseT);
            Tools.SetDropDownListValue(lHelocQualPmtFormulaT, data.lHelocQualPmtFormulaT);
            Tools.SetDropDownListValue(lHelocQualPmtFormulaRateT, data.lHelocQualPmtFormulaRateT);
            Tools.SetDropDownListValue(lHelocQualPmtAmortTermT, data.lHelocQualPmtAmortTermT);
            lHelocQualPmtMb.Text = data.lHelocQualPmtMb_rep;

            Tools.SetDropDownListValue(lHelocPmtBaseT, data.lHelocPmtBaseT);
            Tools.SetDropDownListValue(lHelocPmtFormulaT, data.lHelocPmtFormulaT);
            Tools.SetDropDownListValue(lHelocPmtFormulaRateT, data.lHelocPmtFormulaRateT);
            Tools.SetDropDownListValue(lHelocPmtAmortTermT, data.lHelocPmtAmortTermT);
            lHelocPmtMb.Text = data.lHelocPmtMb_rep;
            lHelocDraw.Text = data.lHelocDraw_rep;
            lHelocCalculatePrepaidInterest.Checked = data.lHelocCalculatePrepaidInterest;

            if (showDerived)
            {
                lLpeFeeMax_in.Visible = true;
                lLpeFeeMax_ov.Visible = true;
            }
            else
            {
                lLpeFeeMax_in.Visible = false;
                lLpeFeeMax_ov.Visible = false;

                lLpeFeeMax_ov.Checked = true;
            }

            PairingProductIds_ov.Checked = data.PairingProductIdsOverrideBit;
            PairingProductIds_ed.Text = data.PairingProductIds_rep;
            PairingProductIds_in.Text = data.PairingProductIdsInherit_rep;

            PairIdFor1stLienProdGuid_in.Text = data.PairIdFor1stLienProdGuidInherit_rep;
            PairIdFor1stLienProdGuid_tx.Text = data.PairIdFor1stLienProdGuid_rep;

            if (showDerived)
            {
                PairIdFor1stLienProdGuid_in.Visible = true;

                PairingProductIds_in.Visible = true;
                PairingProductIds_ov.Visible = true;
            }
            else
            {
                PairIdFor1stLienProdGuid_in.Visible = false;

                PairingProductIds_in.Visible = false;
                PairingProductIds_ov.Visible = false;

                PairingProductIds_ov.Checked = true;
            }

            lPpmtPenaltyMon_ed.Text = data.lPpmtPenaltyMon_rep;
            lPpmtPenaltyMon_ed.Visible = !data.IsLpe;

            isEnabled_ov.Checked = data.IsEnabledOverrideBit;
            isEnabled_cb.Checked = data.IsEnabled;
            isEnabled_in.Checked = data.IsEnabledInherit;

            if (showDerived)
            {
                isEnabled_in.Visible = true;
                isEnabled_ov.Visible = true;
            }
            else
            {
                isEnabled_in.Visible = false;
                isEnabled_ov.Visible = false;

                isEnabled_ov.Checked = true;
            }        

            lLpmiSupportedOutsidePmi_ov.Checked = data.lLpmiSupportedOutsidePmiOverrideBit;
            lLpmiSupportedOutsidePmi.Checked = data.lLpmiSupportedOutsidePmi;
            lLpmiSupportedOutsidePmi_in.Checked = data.lLpmiSupportedOutsidePmiInherit;

            if (showDerived)
            {
                lLpmiSupportedOutsidePmi_in.Visible = true;
                lLpmiSupportedOutsidePmi_ov.Visible = true;

                if (data.lLT != E_sLT.Conventional)
                {
                    lLpmiSupportedOutsidePmi_ov.Enabled = false;
                }
            }
            else
            {
                lLpmiSupportedOutsidePmi_in.Visible = false;
                lLpmiSupportedOutsidePmi_ov.Visible = false;

                lLpmiSupportedOutsidePmi_ov.Checked = true;
            }

            IsDisplayInNonQmQuickPricer_ov.Checked = data.IsDisplayInNonQmQuickPricerOverride;
            IsDisplayInNonQmQuickPricer.Checked = data.IsDisplayInNonQmQuickPricer;
            IsDisplayInNonQmQuickPricer_in.Checked = data.IsDisplayInNonQmQuickPricerInherit;

            if (showDerived)
            {
                IsDisplayInNonQmQuickPricer_in.Visible = true;
                IsDisplayInNonQmQuickPricer_ov.Visible = true;
            }
            else
            {
                IsDisplayInNonQmQuickPricer_in.Visible = false;
                IsDisplayInNonQmQuickPricer_ov.Visible = false;

                IsDisplayInNonQmQuickPricer_ov.Checked = true;
            }

            ProductCode_in.Items.Clear();
            ProductCode_in.Items.Add(data.ProductCodeInherit);
            ProductCode_in.SelectedIndex = 0;
            ProductCode_ov.Checked = data.ProductCodeOverrideBit;
            ProductCode_ov.Checked = data.ProductCodeOverrideBit;

            if (showDerived)
            {
                ProductCode_in.Visible = true;
                ProductCode_ov.Visible = true;
            }
            else
            {
                ProductCode_in.Visible = false;
                ProductCode_ov.Visible = false;

                ProductCode_ov.Checked = true;
            }

            // Get the product's name and type information from the
            // database for display.

            Tools.SetDropDownListValue(lLT_dd, data.lLT);

            // Get the first rate and point from ratesheet to populate
            // rate and point textfield.  Even if the pricing engine
            // is enabled, and we don't show these guys, it would be
            // good to have them initialized so if the pricing engine
            // is turned off during edit, we will save the top entry,
            // rather than 0%, 0%.

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == false ||
                (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false))
            {
                CRateSheetFields[] rates = data.RateSheetWithoutDeltas;

                if (rates != null && rates.Length > 0)
                {
                    lNoteR_ed.Text = rates[0].Rate_rep;
                    lLOrigFPc_ed.Text = rates[0].Point_rep;
                }
            }

            lQualR_ed.Text = data.lQualR_rep;

            lWholesaleChannelProgram.Checked = data.lWholesaleChannelProgram;
            lLpInvestorNm_ed.Text = data.lLpInvestorNm;
            SetListValue(data.lLpInvestorNm);
            ProductCode_ddl_value.Value = data.ProductCode;	//OPM 16984 Cord
            lTerm_ed.Text = data.lTerm_rep;
            lDue_ed.Text = data.lDue_rep;
            Tools.SetDropDownListValue(lLienPosT_dd, data.lLienPosT);
            lPmtAdjCapR_ed.Text = data.lPmtAdjCapR_rep;
            lPmtAdjCapMon_ed.Text = data.lPmtAdjCapMon_rep;
            lPmtAdjRecastPeriodMon_ed.Text = data.lPmtAdjRecastPeriodMon_rep;
            lPmtAdjRecastStop_ed.Text = data.lPmtAdjRecastStop_rep;
            lPmtAdjMaxBal_ed.Text = data.lPmtAdjMaxBalPc_rep;
            lBuydwnMon1_ed.Text = data.lBuydwnMon1_rep;
            lBuydwnR1_ed.Text = data.lBuydwnR1_rep;
            lBuydwnMon2_ed.Text = data.lBuydwnMon2_rep;
            lBuydwnR2_ed.Text = data.lBuydwnR2_rep;
            lBuydwnMon3_ed.Text = data.lBuydwnMon3_rep;
            lBuydwnR3_ed.Text = data.lBuydwnR3_rep;
            lBuydwnMon4_ed.Text = data.lBuydwnMon4_rep;
            lBuydwnR4_ed.Text = data.lBuydwnR4_rep;
            lBuydwnMon5_ed.Text = data.lBuydwnMon5_rep;
            lBuydwnR5_ed.Text = data.lBuydwnR5_rep;
            lGradPmtYrs_ed.Text = data.lGradPmtYrs_rep;
            lGradPmtR_ed.Text = data.lGradPmtR_rep;

            lHardPrepmtPeriodMonths.Text = data.lHardPrepmtPeriodMonths_rep;
            lSoftPrepmtPeriodMonths.Text = data.lSoftPrepmtPeriodMonths_rep;
            lIOnlyMon_ed.Text = data.lIOnlyMon_rep;

            lHasVarRFeature_chk.Checked = data.lHasVarRFeature;
            lVarRNotes_ed.Text = data.lVarRNotes;
            lAprIncludesReqDeposit_chk.Checked = data.lAprIncludesReqDeposit;
            lHasDemandFeature_chk.Checked = data.lHasDemandFeature;
            lFilingF_ed.Text = data.lFilingF;
            lLateDays_ed.Text = data.lLateDays;
            lLateChargePc_ed.Text = data.lLateChargePc;
            lLateChargeBase_ed.Text = data.lLateChargeBaseDesc;
            lFinMethDesc_ed.Text = data.lFinMethDesc;
            IsMaster.Checked = data.IsMaster && !data.IsMasterPriceGroup;

            Tools.SetDropDownListValue(lPrepmtPenaltyT_dd, data.lPrepmtPenaltyT);
            Tools.SetDropDownListValue(lAssumeLT_dd, data.lAssumeLT);
            Tools.SetDropDownListValue(lFinMethT_dd, data.lFinMethT);
            Tools.SetDropDownListValue(lPrepmtRefundT_dd, data.lPrepmtRefundT);

            // 8/5/2005 dd - Case 2549.
            ProductCode.Text = data.ProductCode;

            ProductIdentifier.Text = data.ProductIdentifier;
            ProductIdentifierInherited.Text = data.ProductIdentifierInherited;
            ProductIdentifierOverride.Checked = data.ProductIdentifierOverride;
            if (showDerived)
            {
                ProductIdentifierInherited.Visible = true;
                ProductIdentifierOverride.Visible = true;
            }
            else
            {
                ProductIdentifierInherited.Visible = false;
                ProductIdentifierOverride.Visible = false;

                ProductIdentifierOverride.Checked = true;
            }

            if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
            {
                lIsArmMarginDisplayed.Checked = data.lIsArmMarginDisplayed;
                if (showDerived)
                {
                    lIsArmMarginDisplayed.Enabled = false;
                }
            }
            else
            {
                lIsArmMarginDisplayed.Visible = false;
                lLpmiSupportedOutsidePmi.Visible = false;
                lWholesaleChannelProgram.Visible = false;
            }

            armDisclosure.DataSource = data;
            armDisclosure.DataBind();

            lLpCustomCode1.Text = data.lLpCustomCode1;
            lLpCustomCode1_ov.Checked = data.lLpCustomCode1OverrideBit;

            lLpCustomCode2.Text = data.lLpCustomCode2;
            lLpCustomCode2_ov.Checked = data.lLpCustomCode2OverrideBit;

            lLpCustomCode3.Text = data.lLpCustomCode3;
            lLpCustomCode3_ov.Checked = data.lLpCustomCode3OverrideBit;

            lLpCustomCode4.Text = data.lLpCustomCode4;
            lLpCustomCode4_ov.Checked = data.lLpCustomCode4OverrideBit;

            lLpCustomCode5.Text = data.lLpCustomCode5;
            lLpCustomCode5_ov.Checked = data.lLpCustomCode5OverrideBit;

            lLpInvestorCode1.Text = data.lLpInvestorCode1;
            lLpInvestorCode1_ov.Checked = data.lLpInvestorCode1OverrideBit;

            lLpInvestorCode2.Text = data.lLpInvestorCode2;
            lLpInvestorCode2_ov.Checked = data.lLpInvestorCode2OverrideBit;

            lLpInvestorCode3.Text = data.lLpInvestorCode3;
            lLpInvestorCode3_ov.Checked = data.lLpInvestorCode3OverrideBit;

            lLpInvestorCode4.Text = data.lLpInvestorCode4;
            lLpInvestorCode4_ov.Checked = data.lLpInvestorCode4OverrideBit;

            lLpInvestorCode5.Text = data.lLpInvestorCode5;
            lLpInvestorCode5_ov.Checked = data.lLpInvestorCode5OverrideBit;

            IsNonQmProgram.Checked = data.IsNonQmProgram;

            SetLoadUI(showDerived, lLpCustomCode1_ov);
            SetLoadUI(showDerived, lLpCustomCode2_ov);
            SetLoadUI(showDerived, lLpCustomCode3_ov);
            SetLoadUI(showDerived, lLpCustomCode4_ov);
            SetLoadUI(showDerived, lLpCustomCode5_ov);
            SetLoadUI(showDerived, lLpInvestorCode1_ov);
            SetLoadUI(showDerived, lLpInvestorCode2_ov);
            SetLoadUI(showDerived, lLpInvestorCode3_ov);
            SetLoadUI(showDerived, lLpInvestorCode4_ov);
            SetLoadUI(showDerived, lLpInvestorCode5_ov);
        }

        private void SetLoadUI(bool showDerived, CheckBox ov)
        {
            if (showDerived)
            {
                ov.Visible = true;
            }
            else
            {
                ov.Visible = false;
                ov.Checked = true;
            }
        }
        /// <summary>
        /// Take what's in the UI and re-post it to a data object.  We
        /// save these details when saving.
        /// </summary>

        private void PostData()
        {
            CLoanProductData data = new DataAccess.CLoanProductData(m_fileId);

            // 4/21/2005 kb - Prepare the data object for calculation.

            data.InitSave();

            // OPM 16984 Cord
            if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
            {
                data.ProductCodeOverrideBit = ProductCode_ov.Checked;
                data.ProductCode = ProductCode_ddl_value.Value;

                if (data.lBaseLpId == Guid.Empty)
                {
                    // For base programs, need to keep inherit in sync with product code
                    data.ProductCodeInherit = data.ProductCode;
                }
            }
            else
            {
                data.ProductCode = ProductCode.Text;
            }


            // 2/8/2005 kb - We save the overrideable fields, regardless of
            // the readonly state of the view.  We need to be careful here
            // to make sure we're not saving invalid values when the user
            // says inherit from base, but the product is not derived.  The
            // should make this impossible by hiding the checkboxes, but a
            // product could be in a state where overrides were turned on
            // when the product was previously derived, but now has lost
            // its base product (this should never happen, right?).
            data.ProductIdentifier = ProductIdentifier.Text;
            data.ProductIdentifierOverride = ProductIdentifierOverride.Checked;

            data.lLpTemplateNm = lLpTemplateNm_ed.Text;
            data.lLpTemplateNmOverrideBit = lLpTemplateNm_ov.Checked;

            data.lLendNm = lLendNm_ed.Text;
            data.lLendNmOverrideBit = lLendNm_ov.Checked;

            data.lCcTemplateId_rep = lCcTemplateId_dd.SelectedItem.Value;
            data.lCcTemplateIdOverrideBit = lCcTemplateId_ov.Checked;

            if (!data.IsLpe)
            {
                data.lLockedDays_rep = lLockedDays_ed.Text;
                data.lPpmtPenaltyMon_rep = lPpmtPenaltyMon_ed.Text;

            }

            data.lLpeFeeMinParam_rep = lLpeFeeMin_pm.Text;
            data.lLpeFeeMinOverrideBit = lLpeFeeMin_ov.Checked;

            data.lLpeFeeMax_rep = lLpeFeeMax_ed.Text;
            data.lLpeFeeMaxOverrideBit = lLpeFeeMax_ov.Checked;



            data.lLienPosT = Tools.GetDropDownListValue(lLienPosT_dd) == 0 ? E_sLienPosT.First : E_sLienPosT.Second;

            data.PairingProductIds_rep = PairingProductIds_ed.Text;
            data.PairingProductIdsOverrideBit = PairingProductIds_ov.Checked;

            data.PairIdFor1stLienProdGuid_rep = PairIdFor1stLienProdGuid_tx.Text;

            data.IsEnabled = isEnabled_cb.Checked;
            data.IsEnabledOverrideBit = isEnabled_ov.Checked;


            data.lLpmiSupportedOutsidePmi = lLpmiSupportedOutsidePmi.Checked;
            data.lLpmiSupportedOutsidePmiOverrideBit = lLpmiSupportedOutsidePmi_ov.Checked;

            data.lLpCustomCode1 = lLpCustomCode1.Text;
            data.lLpCustomCode1OverrideBit = lLpCustomCode1_ov.Checked;

            data.lLpCustomCode2 = lLpCustomCode2.Text;
            data.lLpCustomCode2OverrideBit = lLpCustomCode2_ov.Checked;

            data.lLpCustomCode3 = lLpCustomCode3.Text;
            data.lLpCustomCode3OverrideBit = lLpCustomCode3_ov.Checked;

            data.lLpCustomCode4 = lLpCustomCode4.Text;
            data.lLpCustomCode4OverrideBit = lLpCustomCode4_ov.Checked;

            data.lLpCustomCode5 = lLpCustomCode5.Text;
            data.lLpCustomCode5OverrideBit = lLpCustomCode5_ov.Checked;

            data.lLpInvestorCode1 = lLpInvestorCode1.Text;
            data.lLpInvestorCode1OverrideBit = lLpInvestorCode1_ov.Checked;

            data.lLpInvestorCode2 = lLpInvestorCode2.Text;
            data.lLpInvestorCode2OverrideBit = lLpInvestorCode2_ov.Checked;

            data.lLpInvestorCode3 = lLpInvestorCode3.Text;
            data.lLpInvestorCode3OverrideBit = lLpInvestorCode3_ov.Checked;

            data.lLpInvestorCode4 = lLpInvestorCode4.Text;
            data.lLpInvestorCode4OverrideBit = lLpInvestorCode4_ov.Checked;

            data.lLpInvestorCode5 = lLpInvestorCode5.Text;
            data.lLpInvestorCode5OverrideBit = lLpInvestorCode5_ov.Checked;

            if (readOnly.Enabled == false)
            {
                // 1/21/2005 kb - Only save content when we're not
                // in read only mode.  The above fields are safe
                // because if they're readonly, they can't change.

                data.lLT = (E_sLT)Tools.GetDropDownListValue(lLT_dd);

                data.lQualR_rep = lQualR_ed.Text;
                data.lTerm_rep = lTerm_ed.Text;
                data.lDue_rep = lDue_ed.Text;

                // Only save rate and point from text field if
                // user doesn't subscribe to pricing feature.

                if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false ||
                    (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false))
                {
                    if (data.RateSheetWithoutDeltas == null || data.RateSheetWithoutDeltas.Length == 0)
                    {
                        // 1/19/2005 kb - We now write back only the rate and
                        // point portion of the initial rate entry.

                        CRateSheetFields rate = data.GetRateSheetFieldsWithoutDeltas(Guid.Empty);

                        rate.Point_rep = lLOrigFPc_ed.Text;
                        rate.Rate_rep = lNoteR_ed.Text;

                        rate.Margin = 0;

                        rate.Update();
                    }
                    else
                    {
                        // 1/19/2005 kb - Get the first entry and write out
                        // the rate and point pieces, leaving the margin
                        // alone (because we now edit the margin in the arm
                        // disclosure control).

                        CRateSheetFields rate = data.RateSheetWithoutDeltas[0];

                        rate.Point_rep = lLOrigFPc_ed.Text;
                        rate.Rate_rep = lNoteR_ed.Text;

                        rate.Update();
                    }
                }

                data.lWholesaleChannelProgram = lWholesaleChannelProgram.Checked;
                data.lLpInvestorNm = (lLpInvestorNm_ddl.Visible) ? lLpInvestorNm_ddl.SelectedItem.Value : lLpInvestorNm_ed.Text;
                data.lPmtAdjCapR_rep = lPmtAdjCapR_ed.Text;
                data.lPmtAdjCapMon_rep = lPmtAdjCapMon_ed.Text;
                data.lPmtAdjRecastPeriodMon_rep = lPmtAdjRecastPeriodMon_ed.Text;
                data.lPmtAdjRecastStop_rep = lPmtAdjRecastStop_ed.Text;
                data.lPmtAdjMaxBalPc_rep = lPmtAdjMaxBal_ed.Text;
                data.lBuydwnMon1_rep = lBuydwnMon1_ed.Text;
                data.lBuydwnR1_rep = lBuydwnR1_ed.Text;
                data.lBuydwnMon2_rep = lBuydwnMon2_ed.Text;
                data.lBuydwnR2_rep = lBuydwnR2_ed.Text;
                data.lBuydwnMon3_rep = lBuydwnMon3_ed.Text;
                data.lBuydwnR3_rep = lBuydwnR3_ed.Text;
                data.lBuydwnMon4_rep = lBuydwnMon4_ed.Text;
                data.lBuydwnR4_rep = lBuydwnR4_ed.Text;
                data.lBuydwnMon5_rep = lBuydwnMon5_ed.Text;
                data.lBuydwnR5_rep = lBuydwnR5_ed.Text;
                data.lGradPmtYrs_rep = lGradPmtYrs_ed.Text;
                data.lGradPmtR_rep = lGradPmtR_ed.Text;

                data.lHardPrepmtPeriodMonths_rep = lHardPrepmtPeriodMonths.Text;
                data.lSoftPrepmtPeriodMonths_rep = lSoftPrepmtPeriodMonths.Text;
                data.lIOnlyMon_rep = lIOnlyMon_ed.Text;

                data.lHasVarRFeature = lHasVarRFeature_chk.Checked;
                data.lVarRNotes = lVarRNotes_ed.Text;
                data.lAprIncludesReqDeposit = lAprIncludesReqDeposit_chk.Checked;
                data.lHasDemandFeature = lHasDemandFeature_chk.Checked;
                data.lFilingF = lFilingF_ed.Text;
                data.lLateDays = lLateDays_ed.Text;
                data.lLateChargePc = lLateChargePc_ed.Text;
                data.lLateChargeBaseDesc = lLateChargeBase_ed.Text;
                data.lPrepmtPenaltyT = (E_sPrepmtPenaltyT)Tools.GetDropDownListValue(lPrepmtPenaltyT_dd);
                data.lAssumeLT = (E_sAssumeLT)Tools.GetDropDownListValue(lAssumeLT_dd);
                data.lFinMethDesc = lFinMethDesc_ed.Text;
                data.lFinMethT = (E_sFinMethT)Tools.GetDropDownListValue(lFinMethT_dd);
                data.IsMaster = IsMaster.Checked;

                data.lPrepmtRefundT = (E_sPrepmtRefundT)Tools.GetDropDownListValue(lPrepmtRefundT_dd);

                data.lLpCustomCode1 = lLpCustomCode1.Text;
                data.lLpCustomCode1OverrideBit = lLpCustomCode1_ov.Checked;

                if (this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    data.lQualRateCalculationT = this.QualRateCalculationMaxOf.Checked ? QualRateCalculationT.MaxOf : QualRateCalculationT.FlatValue;

                    QualRateCalculationFieldT temp;
                    if (!Enum.TryParse(this.QualRateCalculationFieldT1.Value, out temp) ||
                        !Enum.IsDefined(typeof(QualRateCalculationFieldT), temp))
                    {
                        this.ErrorMessage = "Please enter a valid value for the first Qual Rate calculation field.";
                        return;
                    }

                    data.lQualRateCalculationFieldT1 = temp;

                    if (lHasQRateInRateOptions.Checked &&
                        (!Enum.TryParse(this.QualRateCalculationFieldT2.Value, out temp) ||
                        !Enum.IsDefined(typeof(QualRateCalculationFieldT), temp)) &&
                        data.lQualRateCalculationT == QualRateCalculationT.MaxOf)
                    {
                        this.ErrorMessage = "Please enter a valid value for the second Qual Rate calculation field.";
                        return;
                    }

                    data.lQualRateCalculationFieldT2 = temp;

                    data.lQualRateCalculationAdjustment1_rep = this.lQualRateCalculationAdjustment1.Text;
                    data.lQualRateCalculationAdjustment2_rep = this.lQualRateCalculationAdjustment2.Text;
                }

                data.lQualTerm_rep = this.lQualTerm.Value;

                QualTermCalculationType qTermCalcType;
                if (Enum.TryParse(this.lQualTermCalculationType.SelectedValue, out qTermCalcType) &&
                    Enum.IsDefined(typeof(QualTermCalculationType), qTermCalcType))
                {
                    data.lQualTermCalculationType = qTermCalcType;
                }

                armDisclosure.DataSource = data;
                armDisclosure.DataSave();
            }

            // 5/5/2005 kb - Latch in what the editor contains and fixup
            // any cached field values so we render them properly in the
            // next section.

            data.FixupCachedFields();

            // 4/21/2005 kb - Now that we have a posted data object,
            // let's reset all the calculated fields.  Put calculated
            // guys that change on switch setting here...

            PairIdFor1stLienProdGuid_tx.Text = data.PairIdFor1stLienProdGuid_rep;

            lLpeFeeMin_ed.Text = data.lLpeFeeMin_rep;
            lLpeFeeMin_in.Text = data.lLpeFeeMinInherit_rep;
            lLpeFeeMin_pm.Text = data.lLpeFeeMinParam_rep;
            lLpeFeeMin_ib.Checked = data.lLpeFeeMinAddRuleAdjInheritBit;
            lLpeFeeMin_ab.Checked = data.lLpeFeeMinAddRuleAdjBit;

            lLpCustomCode1.Text = data.lLpCustomCode1;
            lLpCustomCode2.Text = data.lLpCustomCode2;
            lLpCustomCode3.Text = data.lLpCustomCode3;
            lLpCustomCode4.Text = data.lLpCustomCode4;
            lLpCustomCode5.Text = data.lLpCustomCode5;

            lLpInvestorCode1.Text = data.lLpInvestorCode1;
            lLpInvestorCode2.Text = data.lLpInvestorCode2;
            lLpInvestorCode3.Text = data.lLpInvestorCode3;
            lLpInvestorCode4.Text = data.lLpInvestorCode4;
            lLpInvestorCode5.Text = data.lLpInvestorCode5;
        }

        /// <summary>
        /// Commit changes to the database when user clicks ok or apply.
        /// </summary>

        private void SaveData(string commandToDo)
        {
            // Validate that the current credentials are enough to execute
            // a save operation on this data object.  The data object itself
            // should define the access control -- need to centralize this.

            CLoanProductData data = new DataAccess.CLoanProductData(m_fileId);

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
            {
                if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false && BrokerUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
                {
                    ErrorMessage = "You don't have permission to modify loan programs.  Access denied.";

                    throw new CBaseException(ErrorMessages.InsufficientPermissionToEditLoan, "User tried to edit loan program without permission.");
                }
            }

            try
            {
                if (this.lHasQRateInRateOptions.Checked &&
                    this.QualRateCalculationMaxOf.Checked &&
                    this.QualRateCalculationFieldT1.Value == this.QualRateCalculationFieldT2.Value)
                {
                    throw new CBaseException(ErrorMessages.QualRateFieldsMustBeMutuallyExclusive, ErrorMessages.QualRateFieldsMustBeMutuallyExclusive);
                }

                // 2/11/2005 kb - Commit latest and greatest to the database.
                // We now ripple changes throughout our spread out caching
                // of inherited variables.

                data.InitSave();

                //OPM 16984 Cord
                if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    data.ProductCode = ProductCode_ddl_value.Value;
                    data.ProductCodeOverrideBit = ProductCode_ov.Checked;

                    if (data.lBaseLpId == Guid.Empty)
                    {
                        // For base programs, need to keep inherit in sync with product code
                        data.ProductCodeInherit = data.ProductCode;
                    }
                }
                else
                {
                    data.ProductCode = ProductCode.Text;
                }

                data.ProductIdentifier = ProductIdentifier.Text;
                data.ProductIdentifierOverride = ProductIdentifierOverride.Checked;
                // 2/8/2005 kb - We save the overrideable fields, regardless of
                // the readonly state of the view.  We need to be careful here
                // to make sure we're not saving invalid values when the user
                // says inherit from base, but the product is not derived.  The
                // should make this impossible by hiding the checkboxes, but a
                // product could be in a state where overrides were turned on
                // when the product was previously derived, but now has lost
                // its base product (this should never happen, right?).

                data.lLpTemplateNm = lLpTemplateNm_ed.Text;
                data.lLpTemplateNmOverrideBit = lLpTemplateNm_ov.Checked;

                data.lLendNm = lLendNm_ed.Text;
                data.lLendNmOverrideBit = lLendNm_ov.Checked;

                data.lCcTemplateId_rep = lCcTemplateId_dd.SelectedItem.Value;
                data.lCcTemplateIdOverrideBit = lCcTemplateId_ov.Checked;

                if (!data.IsLpe)
                {
                    data.lLockedDays_rep = lLockedDays_ed.Text;
                    data.lPpmtPenaltyMon_rep = lPpmtPenaltyMon_ed.Text;

                }

                data.lLpeFeeMinParam_rep = lLpeFeeMin_pm.Text;
                data.lLpeFeeMinOverrideBit = lLpeFeeMin_ov.Checked;

                data.lLpeFeeMax_rep = lLpeFeeMax_ed.Text;
                data.lLpeFeeMaxOverrideBit = lLpeFeeMax_ov.Checked;

                data.lLienPosT = (E_sLienPosT)Tools.GetDropDownListValue(lLienPosT_dd);

                data.PairingProductIds_rep = PairingProductIds_ed.Text;
                data.PairingProductIdsOverrideBit = PairingProductIds_ov.Checked;

                data.PairIdFor1stLienProdGuid_rep = PairIdFor1stLienProdGuid_tx.Text;


                data.IsEnabled = isEnabled_cb.Checked;
                data.IsEnabledOverrideBit = isEnabled_ov.Checked;

                data.lLpCustomCode1 = lLpCustomCode1.Text;
                data.lLpCustomCode1OverrideBit = lLpCustomCode1_ov.Checked;

                data.lLpCustomCode2 = lLpCustomCode2.Text;
                data.lLpCustomCode2OverrideBit = lLpCustomCode2_ov.Checked;

                data.lLpCustomCode3 = lLpCustomCode3.Text;
                data.lLpCustomCode3OverrideBit = lLpCustomCode3_ov.Checked;

                data.lLpCustomCode4 = lLpCustomCode4.Text;
                data.lLpCustomCode4OverrideBit = lLpCustomCode4_ov.Checked;

                data.lLpCustomCode5 = lLpCustomCode5.Text;
                data.lLpCustomCode5OverrideBit = lLpCustomCode5_ov.Checked;

                data.lLpInvestorCode1 = lLpInvestorCode1.Text;
                data.lLpInvestorCode1OverrideBit = lLpInvestorCode1_ov.Checked;

                data.lLpInvestorCode2 = lLpInvestorCode2.Text;
                data.lLpInvestorCode2OverrideBit = lLpInvestorCode2_ov.Checked;

                data.lLpInvestorCode3 = lLpInvestorCode3.Text;
                data.lLpInvestorCode3OverrideBit = lLpInvestorCode3_ov.Checked;

                data.lLpInvestorCode4 = lLpInvestorCode4.Text;
                data.lLpInvestorCode4OverrideBit = lLpInvestorCode4_ov.Checked;

                data.lLpInvestorCode5 = lLpInvestorCode5.Text;
                data.lLpInvestorCode5OverrideBit = lLpInvestorCode5_ov.Checked;

                data.lLpmiSupportedOutsidePmi = lLpmiSupportedOutsidePmi.Checked;
                data.lLpmiSupportedOutsidePmiOverrideBit = lLpmiSupportedOutsidePmi_ov.Checked;

                if (readOnly.Enabled == false)
                {
                    // 1/21/2005 kb - Only save content when we're not
                    // in read only mode.  The above fields are safe
                    // because if they're readonly, they can't change.

                    data.lLT = (E_sLT)Tools.GetDropDownListValue(lLT_dd);

                    data.lQualR_rep = lQualR_ed.Text;
                    data.lTerm_rep = lTerm_ed.Text;
                    data.lDue_rep = lDue_ed.Text;

                    // Only save rate and point from text field if
                    // user doesn't subscribe to pricing feature.

                    if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false)
                    {
                        if (data.RateSheetWithoutDeltas == null || data.RateSheetWithoutDeltas.Length == 0)
                        {
                            // 1/19/2005 kb - We now write back only the rate and
                            // point portion of the initial rate entry.

                            CRateSheetFields rate = data.GetRateSheetFieldsWithoutDeltas(Guid.Empty);

                            rate.Point_rep = lLOrigFPc_ed.Text;
                            rate.Rate_rep = lNoteR_ed.Text;

                            rate.Margin = 0;

                            rate.Update();
                        }
                        else
                        {
                            // 1/19/2005 kb - Get the first entry and write out
                            // the rate and point pieces, leaving the margin
                            // alone (because we now edit the margin in the arm
                            // disclosure control).

                            CRateSheetFields rate = data.RateSheetWithoutDeltas[0];

                            rate.Point_rep = lLOrigFPc_ed.Text;
                            rate.Rate_rep = lNoteR_ed.Text;

                            rate.Update();
                        }
                    }

                    data.lWholesaleChannelProgram = lWholesaleChannelProgram.Checked;
                    data.lLpInvestorNm = (lLpInvestorNm_ddl.Visible) ? lLpInvestorNm_ddl.SelectedItem.Value : lLpInvestorNm_ed.Text;
                    data.lPmtAdjCapR_rep = lPmtAdjCapR_ed.Text;
                    data.lPmtAdjCapMon_rep = lPmtAdjCapMon_ed.Text;
                    data.lPmtAdjRecastPeriodMon_rep = lPmtAdjRecastPeriodMon_ed.Text;
                    data.lPmtAdjRecastStop_rep = lPmtAdjRecastStop_ed.Text;
                    data.lPmtAdjMaxBalPc_rep = lPmtAdjMaxBal_ed.Text;
                    data.lBuydwnMon1_rep = lBuydwnMon1_ed.Text;
                    data.lBuydwnR1_rep = lBuydwnR1_ed.Text;
                    data.lBuydwnMon2_rep = lBuydwnMon2_ed.Text;
                    data.lBuydwnR2_rep = lBuydwnR2_ed.Text;
                    data.lBuydwnMon3_rep = lBuydwnMon3_ed.Text;
                    data.lBuydwnR3_rep = lBuydwnR3_ed.Text;
                    data.lBuydwnMon4_rep = lBuydwnMon4_ed.Text;
                    data.lBuydwnR4_rep = lBuydwnR4_ed.Text;
                    data.lBuydwnMon5_rep = lBuydwnMon5_ed.Text;
                    data.lBuydwnR5_rep = lBuydwnR5_ed.Text;
                    data.lGradPmtYrs_rep = lGradPmtYrs_ed.Text;
                    data.lGradPmtR_rep = lGradPmtR_ed.Text;
                    data.lHardPrepmtPeriodMonths_rep = lHardPrepmtPeriodMonths.Text;
                    data.lSoftPrepmtPeriodMonths_rep = lSoftPrepmtPeriodMonths.Text;
                    data.lIOnlyMon_rep = lIOnlyMon_ed.Text;
                    data.lHasVarRFeature = lHasVarRFeature_chk.Checked;
                    data.lVarRNotes = lVarRNotes_ed.Text;
                    data.lAprIncludesReqDeposit = lAprIncludesReqDeposit_chk.Checked;
                    data.lHasDemandFeature = lHasDemandFeature_chk.Checked;
                    data.lFilingF = lFilingF_ed.Text;
                    data.lLateDays = lLateDays_ed.Text;
                    data.lLateChargePc = lLateChargePc_ed.Text;
                    data.lLateChargeBaseDesc = lLateChargeBase_ed.Text;
                    data.lPrepmtPenaltyT = (E_sPrepmtPenaltyT)Tools.GetDropDownListValue(lPrepmtPenaltyT_dd);
                    data.lAssumeLT = (E_sAssumeLT)Tools.GetDropDownListValue(lAssumeLT_dd);
                    data.lFinMethDesc = lFinMethDesc_ed.Text;
                    data.lFinMethT = (E_sFinMethT)Tools.GetDropDownListValue(lFinMethT_dd);
                    data.IsMaster = IsMaster.Checked;

                    data.lHasQRateInRateOptions = lHasQRateInRateOptions.Checked;

                    data.lLpDPmtT = (E_sLpDPmtT)Tools.GetDropDownListValue(lLpDPmtT);

                    data.lHelocPmtBaseT = (E_sHelocPmtBaseT)Tools.GetDropDownListValue(lHelocPmtBaseT);
                    data.lHelocPmtFormulaT = (E_sHelocPmtFormulaT)Tools.GetDropDownListValue(lHelocPmtFormulaT);
                    data.lHelocPmtFormulaRateT = (E_sHelocPmtFormulaRateT)Tools.GetDropDownListValue(lHelocPmtFormulaRateT);
                    data.lHelocPmtAmortTermT = (E_sHelocPmtAmortTermT)Tools.GetDropDownListValue(lHelocPmtAmortTermT);
                    data.lHelocPmtMb_rep = lHelocPmtMb.Text;
                    data.lHelocDraw_rep = lHelocDraw.Text;
                    data.lHelocCalculatePrepaidInterest = lHelocCalculatePrepaidInterest.Checked;

                    data.IsNonQmProgram = IsNonQmProgram.Checked;
                    data.IsDisplayInNonQmQuickPricer = IsDisplayInNonQmQuickPricer.Checked;
                    data.IsDisplayInNonQmQuickPricerOverride = IsDisplayInNonQmQuickPricer_ov.Checked;

                    // To match UI, we only save Qualify payment when Use Q Rate is checked
                    if (lHasQRateInRateOptions.Checked)
                    {
                        data.lLpQPmtT = (E_sLpQPmtT)Tools.GetDropDownListValue(lLpQPmtT);
                        data.lHelocQualPmtBaseT = (E_sHelocPmtBaseT)Tools.GetDropDownListValue(lHelocQualPmtBaseT);
                        data.lHelocQualPmtFormulaT = (E_sHelocPmtFormulaT)Tools.GetDropDownListValue(lHelocQualPmtFormulaT);
                        data.lHelocQualPmtFormulaRateT = (E_sHelocPmtFormulaRateT)Tools.GetDropDownListValue(lHelocQualPmtFormulaRateT);
                        data.lHelocQualPmtAmortTermT = (E_sHelocPmtAmortTermT)Tools.GetDropDownListValue(lHelocQualPmtAmortTermT);
                        data.lHelocQualPmtMb_rep = lHelocQualPmtMb.Text;

                        if (this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                        {
                            data.lQualRateCalculationT = this.QualRateCalculationMaxOf.Checked ? QualRateCalculationT.MaxOf : QualRateCalculationT.FlatValue;

                            QualRateCalculationFieldT temp;
                            if (!Enum.TryParse(this.QualRateCalculationFieldT1.Value, out temp) ||
                                !Enum.IsDefined(typeof(QualRateCalculationFieldT), temp))
                            {
                                var msg = "Please enter a valid value for the first Qual Rate calculation field.";
                                throw new CBaseException(msg, msg);
                            }

                            data.lQualRateCalculationFieldT1 = temp;

                            if (lHasQRateInRateOptions.Checked &&
                                (!Enum.TryParse(this.QualRateCalculationFieldT2.Value, out temp) ||
                                !Enum.IsDefined(typeof(QualRateCalculationFieldT), temp)) &&
                                data.lQualRateCalculationT == QualRateCalculationT.MaxOf)
                            {
                                var msg = "Please enter a valid value for the second Qual Rate calculation field.";
                                throw new CBaseException(msg, msg);
                            }

                            data.lQualRateCalculationFieldT2 = temp;

                            data.lQualRateCalculationAdjustment1_rep = this.lQualRateCalculationAdjustment1.Text;
                            data.lQualRateCalculationAdjustment2_rep = this.lQualRateCalculationAdjustment2.Text;
                        }
                    }

                    data.lQualTerm_rep = this.lQualTerm.Value;

                    QualTermCalculationType qTermCalcType;
                    if (Enum.TryParse(this.lQualTermCalculationType.SelectedValue, out qTermCalcType) &&
                        Enum.IsDefined(typeof(QualTermCalculationType), qTermCalcType))
                    {
                        data.lQualTermCalculationType = qTermCalcType;
                    }

                    data.IsOptionArm = IsOptionArm.Checked;
                    data.IsLpeDummyProgram = IsLpeDummyProgram.Checked;

                    if (IsPairedOnlyWithSameInvestor.Visible)
                        data.IsPairedOnlyWithSameInvestor = IsPairedOnlyWithSameInvestor.Checked;
                    if (CanBeStandAlone2nd.Visible)
                        data.CanBeStandAlone2nd = CanBeStandAlone2nd.Checked;
                    data.lDtiUsingMaxBalPc = lDtiUsingMaxBalPc.Checked;

                    //opm 22179 fs 09/02/08
                    data.lLpProductType = lLpProductType_ddl.SelectedItem.Text;

                    data.lPrepmtRefundT = (E_sPrepmtRefundT)Tools.GetDropDownListValue(lPrepmtRefundT_dd);
                    if (lIsArmMarginDisplayed.Visible)
                    {
                        data.lIsArmMarginDisplayed = lIsArmMarginDisplayed.Checked;
                    }
                    armDisclosure.DataSource = data;
                    armDisclosure.DataSave();

                }

                // 4/27/05 tn - At this point in time, masters only affect pricing
                // associations, changes made here for master products don't affect
                // any other products.
                //
                // 01/09/07 mf - OPM 9401 We also do not want to spread on non-lpe
                // programs.

                if (data.IsMaster || !data.IsLpe)
                    data.SuppressEventHandlerCall = true;

                // 5/5/2005 kb - Save the changes.  The save call now invokes a
                // fixup handler to correct any cached calculation fields according
                // to their override bits' state.

                data.Save();

                if (!string.IsNullOrEmpty(commandToDo))
                {
                    this.CommandToDo = commandToDo;
                }
            }
            catch (SqlException e)		//OPM 16984 Cord
            {							//Thrown if the Product Code or Investor Name
                if (e.Number == 50000)//does not exist in the database.
                    Tools.LogError(ErrorMessage = e.Message, e);
                else
                    throw e;
            }
            catch (CBaseException e)
            {
                // Oops!

                Tools.LogError(ErrorMessage = "Unable to save: " + e.UserMessage + ".", e);
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError(ErrorMessage = "Failed to save.", e);
            }
        }

        protected bool m_isAuthorCreateMode = false;
        private void SetAuthorCreateMode()
        {
            // Should be LPE program, non-master, non-derived,

            // This special UI is needed for SAEs avoid mistakes that create pricing inaccuracies.
            // Regular users should not get here.
            if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false)
            {
                throw new CBaseException(ErrorMessages.Generic, "Only pricing should be in create new mode");
            }

            m_isAuthorCreateMode = true;
            authorCancel.Visible = true;
            authorApply.Visible = true;
            // Based on spec of case 13035, we have to set up some defaults

            // "Have �Investor Name� default to a blank value, require selection to non-blank value."
            lLpInvestorNm_ddl.Items.Insert(0, new ListItem("", "-1"));
            lLpInvestorNm_ddl.SelectedIndex = 0;
            lLpInvestorNm_ddlValidator.Enabled = true;

            // "Have �Product Code� default to blank value, require selection to non-blank value."
            ProductCode_ddl.Items.Insert(0, new ListItem("", "-1"));
            ProductCode_ddl.SelectedIndex = 0;
            ProductCodeValidator.Enabled = true;

            // "Have �Loan Type� default to blank value, require selection of non-blank value."
            lLT_dd.Items.Insert(0, new ListItem("", "-1"));
            lLT_dd.SelectedIndex = 0;
            lLT_ddValidator.Enabled = true;

            // "Have both �Term� and �Due� default to blank, require entry of positive integer value."
            lTerm_ed.Text = string.Empty;
            lTerm_edValidator.Enabled = true;
            lDue_ed.Text = string.Empty;
            lDue_edValidator.Enabled = true;

            // "Have �Amort. Type� default to blank value, require selection of non-blank value."
            lFinMethT_dd.Items.Insert(0, new ListItem("", "-1"));
            lFinMethT_dd.SelectedIndex = 0;
            lFinMethT_ddValidator.Enabled = true;

            // "Require �Loan Product Type� selection to be made before loan program can be created; default blank value not accepted."
            lLpProductType_ddlValidator.Enabled = true;

            // "Have �Assumption� dropdown default to blank value, require selection of non-blank value."
            lAssumeLT_dd.Items.Insert(0, new ListItem("", "-1"));
            lAssumeLT_dd.SelectedIndex = 0;
            lAssumeLT_ddValidator.Enabled = true;

            // Required for new loan programs.
            lHardPrepmtPeriodMonths.Text = string.Empty;
            lHardPrepmtPeriodMonthsValidator.Enabled = true;
            lSoftPrepmtPeriodMonths.Text = string.Empty;
            lSoftPrepmtPeriodMonthsValidator.Enabled = true;

            // ""Interest only(mths)" field default to blank, require value of 0 or a positive integer to be entered."
            lIOnlyMon_ed.Text = string.Empty;
            lIOnlyMon_edValidator.Enabled = true;
            lIOnlyMon_edFormatValidator.Enabled = true;

            // "The two �Prepayment� dropdowns should default to a blank value, require selection of non-blank value."
            lPrepmtPenaltyT_dd.Items.Insert(0, new ListItem("", "-1"));
            lPrepmtPenaltyT_dd.SelectedIndex = 0;
            lPrepmtPenaltyT_ddValidator.Enabled = true;
            lPrepmtRefundT_dd.Items.Insert(0, new ListItem("", "-1"));
            lPrepmtRefundT_dd.SelectedIndex = 0;
            lPrepmtRefundT_ddValidator.Enabled = true;

            armDisclosure.SetAuthorCreateMode();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
            this.PreRender += new System.EventHandler(this.PagePreRender);

        }
        #endregion

        private void BindClosingCostList()
        {
            var dS = CCcTemplateData.GetClosingCostDataSet(BrokerUser.BrokerId);

            lCcTemplateId_dd.DataSource = dS;
            lCcTemplateId_dd.DataTextField = "cCcTemplateNm";
            lCcTemplateId_dd.DataValueField = "cCcTemplateId";
            lCcTemplateId_dd.DataBind();

            lCcTemplateId_dd.Items.Insert(0, new ListItem("<-- Select closing cost -->", Guid.Empty.ToString()));

            lCcTemplateId_in.DataSource = dS;
            lCcTemplateId_in.DataTextField = "cCcTemplateNm";
            lCcTemplateId_in.DataValueField = "cCcTemplateId";
            lCcTemplateId_in.DataBind();

            lCcTemplateId_in.Items.Insert(0, new ListItem("<-- Select closing cost -->", Guid.Empty.ToString()));
        }

        /// <summary>
        /// Handle UI click.
        /// </summary>

        protected void ApplyClick(object sender, System.EventArgs a)
        {
            try
            {
                SaveData(commandToDo: null);
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError(ErrorMessage = "Save failed.", e);
            }
        }

        /// <summary>
        /// Handle UI click.
        /// </summary>

        protected void OkClick(object sender, System.EventArgs a)
        {
            SaveData("Close");
        }

        protected void OnAuthorCancelClick(object sender, System.EventArgs a)
        {
            // OPM 13035. This is a fairly hacky way to get the behavior the SAEs want:
            // Cancelling of program creation should not leave a dead program.

            CLoanProductBase.DelLoanProduct(m_fileId);

            CommandToDo = "Close";
        }

        protected void OnAuthorApplyClick(object sender, System.EventArgs a)
        {
            // OPM 13035. This is a fairly hacky way to get the behavior the SAEs want:
            // Cancelling of program creation should not leave a dead program.

            SaveData("Reopen");
        }

        //OPM 16984 Cord
        protected int GetInvestorCount()
        {
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataAccess.DataSrc.LpeSrc, "ListInvestorWithValidProductCode"))
            {
                return reader.Read() ? reader.GetInt32(0) : -1;
            }
        }

        //OPM 16984 Cord
        // Make an injected string from
        // codebehind safe for JavaScript.
        protected String SafeJsString(String s)
        {
            if (s != null)
            {
                //Replace \ with \\
                s = s.Replace("\\", "\\\\");

                //Replace ' with \'
                s = s.Replace("'", "\\'");

                //Replace " with \"
                s = s.Replace("\"", "\\\"");
            }

            return s;
        }

        protected void Validate_lHardPrepmtPeriodMonths(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidatePrepmtPeriodMonths(lHardPrepmtPeriodMonths.Text);
        }

        protected void Validate_lSoftPrepmtPeriodMonths(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ValidatePrepmtPeriodMonths(lSoftPrepmtPeriodMonths.Text);
        }

        private bool ValidatePrepmtPeriodMonths(string value)
        {
            // Valid if >= 0 and < Due, if either is not an int -> invalid
            int due;
            int prePmtPeriod;

            bool dueIsInt = int.TryParse(lDue_ed.Text, out due);
            bool prepmtPeriodIsInt = int.TryParse(value, out prePmtPeriod);

            return dueIsInt && prepmtPeriodIsInt && prePmtPeriod >= 0 && prePmtPeriod < due;
        }
    }

}
