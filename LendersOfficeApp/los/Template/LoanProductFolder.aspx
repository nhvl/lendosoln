<%@ Page language="c#" Codebehind="LoanProductFolder.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.LoanProductFolder" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>LoanProductFolder</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: scroll">
<script type="text/javascript">
    // Replace + with %2B
    function m_escape(str) {
        return escape(str).replace('+', '%2B');
    }

    $(document).ready(function () {
        init();
    });

    function init() {
        $("#dynatree").dynatree({
            onActivate: function (nodes) {                
                window.parent.programs.location.href = nodes.data.href;
                onTreeChange(nodes.data);                
            },
            minExpandLevel: 2,            
            persist: false,
            children: nodes
        });

        $(".dynatree-container").css('background-color', 'gainsboro');

        if (ML.deleteSuccess) {
            $("#dynatree").dynatree("getTree").selectKey(document.getElementById("currentNode").value).remove();
        }

        if (ML.recursiveDeleteSuccess) {
            var nodeToDelete = $("#dynatree").dynatree("getTree").selectKey(document.getElementById("currentNode").value);
            nodeToDelete.removeChildren();
            nodeToDelete.remove();
        }

        // reset global data
        ML.deleteSuccess = false;
        ML.recursiveDeleteSuccess = false;
        document.getElementById("currentNode").value = "";

        parent.programs.location = "LoanProgramList.aspx";

        if( document.getElementById("m_addnewPanel").disabled == true )
        {
	        for( i = 0 ; i < document.getElementById("m_addnewPanel").children.length ; ++i )
	        {
		        document.getElementById("m_addnewPanel").children[ i ].disabled = true;
	        }
        }

        if( document.getElementById("m_propsPanel").disabled == true )
        {
	        for( i = 0 ; i < document.getElementById("m_propsPanel").children.length ; ++i )
	        {
		        document.getElementById("m_propsPanel").children[ i ].disabled = true;
	        }
        }

        document.getElementById("m_renamerBtn").disabled = true;
        document.getElementById("m_deleteBtn").disabled  = true;

        if( document.getElementById("m_errorMessage") != null )
        {
	        alert( document.getElementById("m_errorMessage").value );
        }

        if( document.getElementById("m_feedBack") != null )
        {
	        alert( document.getElementById("m_feedBack").value );
        }
    }

    function onTreeChange(data)
    {        
        if (data.key == "root") {
            document.getElementById("m_renamerBtn").disabled = true;
            document.getElementById("m_deleteBtn").disabled = true;
        }
        else {
            if (document.getElementById("m_propsPanel").disabled == true) {
                document.getElementById("m_renamerBtn").disabled = true;
            }
            else {
                document.getElementById("m_renamerBtn").disabled = false;
            }

            if (document.getElementById("m_deletePanel").disabled == true) {
                document.getElementById("m_deleteBtn").disabled = true;
            }
            else {
                document.getElementById("m_deleteBtn").disabled = false;
            }
        }
    }    

    function addChildFolder() {
        var activeNode = getActiveNode();

        showModalTopLevel('/los/Template/LoanProductFolderName.aspx?parentid=' + activeNode.data.key, null, null, null, function (args) {
            if (args.OK) {
                activeNode.addChild({
                    title: args.name,
                    isFolder: true,
                    key: args.folderid,
                    href: "LoanProgramList.aspx?folderid=" + args.folderid,
                    Target: "body"
                });
            }
        });
    }

    function renameFolder() {
        var activeNode = getActiveNode();
        var folderId = activeNode.data.key;
        var folderName = activeNode.data.title;
        showModalTopLevel('/los/Template/LoanProductFolderName.aspx?folderid=' + folderId + '&name=' + m_escape(folderName), null, null, null, function(args){
            if (args.OK) {
                activeNode.data.title = args.name;
                activeNode.render();
			}
	    });
    }

    function exportFolder() {
        var activeNode = getActiveNode();
        var folderId = activeNode.data.key;

        if( folderId.toLowerCase() != "root" )
        {
            window.open("LoanProductExport.aspx?folderId=" + folderId, "_parent");
	        //showModalTopLevel( "/los/Template/LoanProductExport.aspx?folderId=" + folderId );
        }
        else
        {
            window.open("LoanProductExport.aspx?folderId=all", "_parent");
	        //showModalTopLevel( "/los/Template/LoanProductExport.aspx?folderId=all" );
        }
    }

    function exportExcel(type) {
        var activeNode = getActiveNode();
        var folderId = activeNode.data.key;

        if( folderId.toLowerCase() != "root" )
        {
            window.open("LoanProductExport.aspx?cmd=xlsx&folderId=" + folderId + "&type=" + type, "_parent")
        }
        else
        {
            window.open("LoanProductExport.aspx?cmd=xlsx&folderId=all&type=" + type, "_parent");
        }
    }

    function deriveProducts( bAsJob ) {
        var activeNode = getActiveNode();
        var folderId = activeNode.data.key;

        var url = ( bAsJob )
            ? "/los/Template/CreateDerivationJob.aspx?targetId=" : "/los/Template/InsertExternalProduct.aspx?targetId=";

        showModalTopLevel(url + folderId, null, null, true);
        $("#dynatree").dynatree("getRoot").select(true);
        $("#dynatree").dynatree("getRoot").focus();
    }

    function viewDeriveJobs() {
        var activeNode = getActiveNode();
        var folderId = activeNode.data.key;

        showModalTopLevel( "/los/Template/DerivationJobList.aspx" );
        $("#dynatree").dynatree("getRoot").select(true);
        $("#dynatree").dynatree("getRoot").focus();
    }
    
    function onViewOrSummary(isView) {
        if (confirm("This report will recursively traverse the entire loan program tree. Do not run it on the ROOT folder.\n Do you want to continue?")) {
            var activeNode = getActiveNode();
            var folderId = activeNode.data.key;

            if (folderId == "root") {
                alert("!!! Ahem! What did I say about doing this on the ROOT folder?!?!? !!!");
                return;
            }
            
            if (isView) {
                var win = window.open("../RatePrice/Helpers/AssocView.aspx?productFolderId=" + folderId, 'AssocView', 'toolbar=no,menubar=no,resizable=yes');
            }
            else {
                var win = window.open("LpSummaryReport.aspx?folderId=" + folderId, 'LpSummaryReport');
            }
            win.focus();
        }
    }

    function f_onDeleteFolderTree() {
        if (ML.CanModifyLoanPrograms) {
	            var code = Math.floor(Math.random()*1000) + '';
	            var ans = prompt("WARNING: This action will remove the current folder and ALL of its descendants. " +
	            "It is IRREVERSIBLE, so please type '" + code+ "' below to confirm it:", "");

	            var doIt = (ans != null &&  ans == code );

	            if (doIt == false && ans != null) 
	            alert("Code does not match.  Deletions will not be performed.");
                
	            var activeNode = getActiveNode();
	            document.getElementById("currentNode").value = activeNode.data.key;

	            return doIt;
        } 
        else {
	        return false;
        }
    }

    function getActiveNode() {
        var tree = $("#dynatree");
        var activeNode = tree.dynatree("getActiveNode");

        if (activeNode === null) {
            activeNode = tree.dynatree("getRoot").childList[0];
        }

        return activeNode;
    }
    
    function onDeleteClick() {
        var activeNode = getActiveNode();
        document.getElementById("currentNode").value = activeNode.data.key;
    }
</script>
		<form id="LoanProductFolder" method="post" runat="server">
			<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<ASP:Panel id="m_addnewPanel" style="DISPLAY: inline" runat="server">
							<INPUT style="WIDTH: 99px; HEIGHT: 21px" onclick="addChildFolder();" type="button" value="Add child folder...">
						</ASP:Panel>
						<ASP:Panel id="m_deletePanel" style="DISPLAY: inline" runat="server">
							<ASP:Button id="m_deleteBtn" onclick="OnDeleteClick" onClientClick="onDeleteClick();" runat="server" Text="Delete"></ASP:Button>
							<SPAN onclick="if(children[0] != null && children[0].disabled == false ) return f_onDeleteFolderTree();">
								<asp:Button id="m_RecursiveDelBtn" runat="server" Text="Recursive Delete" onclick="m_RecursiveDelBtn_Click"></asp:Button></SPAN>
						</ASP:Panel>
						<ASP:Panel id="m_exportPanel" style="DISPLAY: inline" runat="server">
							<INPUT onclick="exportFolder();" type="button" value="Export" />
							<input onclick="exportExcel();" type="button" value="Export to XLSX" />
							<input onclick="exportExcel('batchrename');" type="button" value="Export for Batch Rename"/>
						</ASP:Panel>
                        <asp:Panel ID="m_derivePanel" style="display: inline" runat="server">
                            <INPUT id="m_insertFromOther" onclick="deriveProducts(true);" type="button" value="Derive...">
                            <INPUT id="m_batchDerive"  type="button" value="Batch Derive..." onclick="showModalTopLevel('/los/Template/TemplateSelector.aspx?cmd=derive');">
							<INPUT id="m_viewJobs" onclick="viewDeriveJobs();" type="button" value="View Derive Jobs">
                            <br />
                            <INPUT id="m_productcodeBatchEdit"  type="button" value="Batch-Edit Product Codes" onclick="showModalTopLevel('/los/Template/TemplateSelector.aspx?cmd=productcode');">
                        </asp:Panel>

						<ASP:Panel id="m_propsPanel" style="DISPLAY: inline" runat="server">
							<INPUT id="m_renamerBtn" style="WIDTH: 99px; HEIGHT: 21px" onclick="renameFolder();" type="button" value="Rename folder..." name="m_renamerBtn">
						</ASP:Panel>
						<asp:Panel id="m_forEditor" runat="server" style="DISPLAY: inline" Visible="False">
							<INPUT style="WIDTH: 80px" onclick="onViewOrSummary(true);" type="button" value="View Assoc">
							<INPUT onclick="onViewOrSummary(false);" type="button" value="Summary">
						    <input type="button" value="Program Price Group Visibility" onclick="showModalTopLevel('/los/Template/BatchPriceGroupOperation.aspx');" />
						</asp:Panel>
						<input id="m_refreshBtn" name="m_refreshBtn" type="button" value="Refresh" onclick="self.location=self.location" style="WIDTH: 53px; HEIGHT: 21px">
						<asp:Panel runat="server" ID="m_forBatchUpdate" style="display:inline" Visible="false">
						    <input id="m_batchUpdate" type="button" value="Batch Edit Programs" onclick="showModalTopLevel('/los/Template/TemplateSelector.aspx');" />
						</asp:Panel>
					</td>
				</tr>				
			</table>
            <div id="dynatree">
            </div>
            <input id="currentNode" type="hidden" runat="server" />
		</form>
	</body>
</HTML>