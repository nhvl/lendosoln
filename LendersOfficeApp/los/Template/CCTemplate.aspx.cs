using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;

namespace LendersOfficeApp.los.Template
{
	/// <summary>
	/// Summary description for CCTemplate.
	/// </summary>
	public partial class CCTemplate : LendersOffice.Common.BasePage
	{
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

        protected void PageInit(object sender, System.EventArgs e)
        {
            RegisterVbsScript("common.vbs");
            this.RegisterJsScript("LQBPopup.js");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.EnableJqueryMigrate = false;
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
		#endregion
	}
}
