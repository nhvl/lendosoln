<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="PMIProgramList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.PMIProgramList" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PMIProgramList</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bgColor="gainsboro" scroll="yes" onload="init();" MS_POSITIONING="FlowLayout">
		<script language="javascript">    
            function init() {
    		    resize(750, 540);
    		}

    		function onNewClick(folderId) {
    		    showModal("/los/Template/PMIProductTemplate.aspx?FolderId=" + encodeURIComponent(folderId), null, null, null, function(){
					window.location = self.location;
				});
    		}

    		function onEditClick(id, folderId) {
    		    showModal("/los/Template/PMIProductTemplate.aspx?Id=" + encodeURIComponent(id) + "&FolderId=" + encodeURIComponent(folderId), null, null, null, function(){
					window.location = self.location;
				});
    		}

    		function onRulesClick(id, name) {
    		    showModal("/los/RatePrice/PricePolicyListByProduct.aspx?PmiProductId=" + id + "&ProductName=" + name, null, null, true);
    		}

    		function setChecked() {
    		    var selected = document.getElementById('selectedCheckboxes');
    		    selected.value = '';
    		    var collection = document.getElementsByTagName('input');
    		    for (var i = 0; i < collection.length; i++) {
    		        if (collection[i].type == 'checkbox' && collection[i].checked 
    		            && collection[i] != document.getElementById('masterTemplateCB')) {
    		            selected.value += collection[i].value + ',';
    		        }
    		    }
    		}

    		function checkAllTemplateCB() {
    		    var checked = document.getElementById('masterTemplateCB').checked;
    		    var collection = document.getElementsByTagName('input');
    		    for (var i = 0; i < collection.length; i++) {
    		        if (collection[i].type == 'checkbox' && collection[i] != document.getElementById('masterTemplateCB')) {
    		            collection[i].checked = checked;
    		        }
    		    }
    		}
		</script>
		
		<form id="PMIProgramList" method="post" runat="server">
		    <asp:HiddenField ID="selectedCheckboxes" runat="server" Value="" />
			<TABLE class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="FormTableHeader">
					    PMI Programs
					</TD>
				</TR>
				<TR>
					<TD>
					    <asp:panel id="m_forSimple" style="DISPLAY: inline" runat="server">
					        <input type="button" id="m_addBtn" runat="server" value="Add Mortgage Insurance Program" />					
						    <asp:button id="m_deleteBtn" runat="server" Text="Delete Selected Program" Width="150px" OnClientClick="setChecked();" OnClick="DeleteClick"></asp:button>
						    <asp:button id="m_enableBtn" runat="server" Text="Enabled Selected" Width="120px" OnClientClick="setChecked();" OnClick="EnableClick"></asp:button>
						</asp:panel>
				    </TD>
				</TR>
				<TR>
					<TD>
					    <table cellpadding="2" cellspacing="0" border="1">
					        <asp:Repeater ID="m_programsRepeater" runat="server" EnableViewState="false" OnItemDataBound="Programs_OnItemDataBound">
					            <HeaderTemplate>
					                <tr class="GridHeader">
					                    <td>
										    <input type="checkbox" id="masterTemplateCB" onclick="checkAllTemplateCB();">					                
					                    </td>
					                    <td>
					                        &nbsp;
					                    </td>
					                    <td>
					                        Enabled?
					                    </td>
					                    <td>
					                        Company
					                    </td>
					                    <td>
					                        MI Program Type
					                    </td>
					                    <td>
					                        &nbsp;
					                    </td>
					                    <td>
					                        Created Date
					                    </td>
					                </tr>
					            </HeaderTemplate>
					            <ItemTemplate>
					                <tr class="GridItem">
					                    <td>
					                        <input type="checkbox" runat="server" id="selection" />&nbsp;
									    </td>
									    <td>
									        <a id="editLink" name="templateCB" href="#" runat="server">edit</a>&nbsp;
									    </td>
									    <td>
									        <ml:EncodedLiteral ID="isEnabled" runat="server"></ml:EncodedLiteral>
									    </td>
									    <td>
									        <ml:EncodedLiteral ID="company" runat="server"></ml:EncodedLiteral>&nbsp;
									    </td>
									    <td>
									        <ml:EncodedLiteral ID="programType" runat="server"></ml:EncodedLiteral>
									    </td>
									    <td>
									        <a id="rulesLink" href="#" runat="server">rules</a>&nbsp;
									    </td>
									    <td>
									        <ml:EncodedLiteral ID="createdDate" runat="server"></ml:EncodedLiteral>
									    </td>
					                </tr>
					            </ItemTemplate>
					            <AlternatingItemTemplate>
					                <tr class="GridAlternatingItem">
					                    <td>
					                        <input type="checkbox" runat="server" id="selection" />&nbsp;
									    </td>
									    <td>
									        <a id="editLink" name="templateCB" href="#" runat="server">edit</a>&nbsp;
									    </td>
									    <td>
									        <ml:EncodedLiteral ID="isEnabled" runat="server"></ml:EncodedLiteral>
									    </td>
									    <td>
									        <ml:EncodedLiteral ID="company" runat="server"></ml:EncodedLiteral>&nbsp;
									    </td>
									    <td>
									        <ml:EncodedLiteral ID="programType" runat="server"></ml:EncodedLiteral>
									    </td>
									    <td>
									        <a id="rulesLink" href="#" runat="server">rules</a>&nbsp;
									    </td>
									    <td>
									        <ml:EncodedLiteral ID="createdDate" runat="server"></ml:EncodedLiteral>
									    </td>
					                </tr>
					            </AlternatingItemTemplate>
					        </asp:Repeater>
					    </table>
				    </TD>
				</TR>
			</TABLE>
			<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></form>
	</body>
</HTML>
