using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.Template
{

    public partial class LoanProductFolderName : LendersOffice.Common.BasePage
    {

		private BrokerUserPrincipal BrokerUser
		{
			// Get current user.
            get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}

		private String ErrorMessage
		{
			// Set current error.

			set
			{
				Page.ClientScript.RegisterHiddenField( "m_errorMessage" , value );
			}
		}

        private Guid m_parentFolderID 
        {
            get { return RequestHelper.GetGuid("parentid", Guid.Empty); }
        }
        private Guid m_folderID 
        {
            get { return RequestHelper.GetGuid("folderid", Guid.Empty); }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                if (m_folderID != Guid.Empty) 
                {
					// OPM 18145 9/21/07 mf. Handle quotes better.
                    m_nameTF.Text = Server.HtmlDecode( RequestHelper.GetSafeQueryString("name") ).TrimWhitespaceAndBOM();
                    m_headerLabel.Text = "Rename folder to";
					m_FolderId_ed.Text = m_folderID.ToString();
                } 
                else 
                {
                    m_headerLabel.Text = "New folder";
                }
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

        protected void OnOKClick( object sender , System.EventArgs a )
        {
			try
			{
				// 7/15/2004 kb - Block access if pricing engine enabled
				// and user lacks permission.

                if (BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
				{
                    if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false && BrokerUser.HasPermission(Permission.AllowViewingAndEditingManualLoanPrograms) == false)
					{
						ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

						return;
					}
				}

				if (m_folderID == Guid.Empty) 
				{
					// Add new folder.

					SqlParameter folderIdParam = new SqlParameter( "@FolderId"       , Guid.Empty );
					SqlParameter parentIdParam = new SqlParameter( "@ParentFolderId" , Guid.Empty );

					if( m_parentFolderID != Guid.Empty )
					{
						parentIdParam.Value = m_parentFolderID;
					}
					else
					{
						parentIdParam.Value = DBNull.Value;
					}

					folderIdParam.Direction = ParameterDirection.Output;

					int ret = StoredProcedureHelper.ExecuteNonQuery
						( DataSrc.LpeSrc, "CreateLoanProductFolder"
						, 1
						, new SqlParameter( "@FolderName" , m_nameTF.Text.TrimWhitespaceAndBOM()                                         )
						, new SqlParameter( "@BrokerID"   , BrokerUser.BrokerId                                          )
						, new SqlParameter( "@IsLpe"      , BrokerUser.HasPermission( Permission.CanModifyLoanPrograms ) )
						, parentIdParam
						, folderIdParam
						);

					if (ret >= 0) 
					{
						try
						{
							LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new string[] {"OK=true", "name='" + Utilities.SafeJsString(m_nameTF.Text.TrimWhitespaceAndBOM()) + "'", "folderid='" + folderIdParam.Value + "'"});
						}
						catch( System.Threading.ThreadAbortException )
						{
							// Pass.

							throw new CBaseException(ErrorMessages.Generic, "Done.");
						}
					} 
					else 
					{
                        ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.Generic, "Unable to create new folder."), false, Guid.Empty, Guid.Empty);
					}
				} 
				else 
				{
					// Rename folder.
					SqlParameter[] parameters = { 
													new SqlParameter("@FolderID", m_folderID),
													new SqlParameter("@FolderName", m_nameTF.Text.TrimWhitespaceAndBOM()),
													new SqlParameter("@BrokerID", BrokerUser.BrokerId)
												};
					int ret = StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "UpdateLoanProductFolder", 0, parameters);
					if (ret >= 0) 
					{
						try
						{
							LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new string[] {"OK=true", "name='" + Server.HtmlEncode(m_nameTF.Text.TrimWhitespaceAndBOM()) + "'"});
						}
						catch( System.Threading.ThreadAbortException )
						{
							// Pass.

							throw new CBaseException(ErrorMessages.Generic, "Done.");
						}
					} 
					else 
					{
                        ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.Generic, "Unable to rename folder."), false, Guid.Empty, Guid.Empty);
                    }
			
				}
			}
			catch( CBaseException )
			{
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to save loan product name." , e );
			}
        }
    }
}
