﻿namespace LendersOfficeApp.Los.Template
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.RatePrice;
    using LendersOffice.Security;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Change investor name / product for multiple programs.
    /// </summary>
    public partial class ProductCodeBatchEdit : LendersOffice.Common.BasePage
    {
        /// <summary>
        ///  The program are selected to modify their investor/product codes.
        /// </summary>
        private List<ProgramInfo> selectedPrograms = null;

        /// <summary>
        ///  All active investor names and their product codes.
        /// </summary>
        private Dictionary<string, List<string>> investorProductCodeDict;

        /// <summary>
        ///  Source investor name that gets from selectedPrograms.
        /// </summary>
        private string srcInvestorName;

        /// <summary>
        ///  Destination investor name.
        /// </summary>
        private string destInvestorName;

        /// <summary>
        /// The destination investor's all produce codes.
        /// </summary>
        private List<string> destProductCodes;

        /// <summary>
        /// Gets or sets source product code.
        /// Note: map from SrcProductCodes[idx] to NewProductCodes[Idx].
        /// </summary>
        /// <value>Return source product code list.</value>
        protected List<string> SrcProductCodes { get; set; }

        /// <summary>
        /// Gets or sets new source product code.
        /// Note: map from SrcProductCodes[idx] to NewProductCodes[Idx].
        /// </summary>
        /// <value>Return new product code list.</value>
        protected List<string> NewProductCodes { get; set; }

        /// <summary>
        /// Gets/sets the cache key for selected program.
        /// </summary>
        /// <value>Cache key for selected programs.</value>
        protected string SelectedPrgsCacheKey { get; private set; } = string.Empty;

        /// <summary>
        /// Gets Current Broker Principal.
        /// </summary>
        /// <value>Broker User Principal.</value>
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        /// <summary>
        /// Sets error message that will show by js alert.
        /// </summary>
        private string ErrorMessage
        {
            set { ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + "."); }
        }

        /// <summary>
        /// Sets Command that will use by js client side.
        /// </summary>
        private string Command
        {
            set { ClientScript.RegisterHiddenField("m_cmdToDo", value); }
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false)
            {
                this.ErrorMessage = "Access denied.";
                return;
            }

            var srcProductCodeDict = new Dictionary<string, List<string>>();
            List<string> srcInvestors = null;

            Action initSrcInvestors = () =>
            {
                Dictionary<string, HashSet<string>> investorToProductCodes = new Dictionary<string, HashSet<string>>();
                foreach (var programInfo in selectedPrograms)
                {
                    if (!investorToProductCodes.ContainsKey(programInfo.InvestorName))
                    {
                        investorToProductCodes.Add(programInfo.InvestorName, new HashSet<string>());
                    }

                    investorToProductCodes[programInfo.InvestorName].Add(programInfo.ProductCode);
                }

                foreach (var investor in investorToProductCodes.Keys)
                {
                    var codes = investorToProductCodes[investor].ToList();
                    codes.Sort();
                    srcProductCodeDict.Add(investor, codes);
                }

                srcInvestors = srcProductCodeDict.Keys.ToList();
                srcInvestors.Sort();
            };

            bool initMap = true;
            if (this.IsPostBack == false)
            {
                this.selectedPrograms = this.GetSelectedPrograms();
                initSrcInvestors();
                this.srcInvestorName = srcInvestors[0];
                this.destInvestorName = string.Empty;

                this.ddlSrcInvestor.DataSource = srcInvestors;
                this.ddlSrcInvestor.DataBind();
                this.ddlSrcInvestor.Enabled = srcInvestors.Count > 1;

                HashSet<string> activeInvestor = new HashSet<string>(InvestorNameUtils.ListActiveInvestorNames(this.BrokerUser?.BrokerDB).Select(i => i.InvestorName));

                this.investorProductCodeDict = new Dictionary<string, List<string>>();
                using (var dR = DataAccess.StoredProcedureHelper.ExecuteReader(DataAccess.DataSrc.LpeSrc, "ListInvestorProductWithValidProductCode"))
                {
                    while (dR.Read())
                    {
                        string investor = dR.GetString(0);
                        string productCode = dR.GetString(1);
                        if (!activeInvestor.Contains(investor))
                        {
                            continue;
                        }

                        List<string> productCodes = null;
                        if (!this.investorProductCodeDict.TryGetValue(investor, out productCodes))
                        {
                            productCodes = new List<string>();
                            this.investorProductCodeDict.Add(investor, productCodes);
                        }

                        productCodes.Add(productCode);
                    }
                }

                var allInvestors = this.investorProductCodeDict.Keys.ToList();
                allInvestors.Sort();
                allInvestors.Insert(0, string.Empty);
                this.ddlDestInvestor.DataSource = allInvestors;
                this.ddlDestInvestor.DataBind();

                this.ViewState["selectedPrograms"] = this.selectedPrograms;
                this.ViewState["srcInvestorName"] = this.srcInvestorName;
                this.ViewState["destInvestorName"] = this.destInvestorName;
                this.ViewState["investorProductCodeDict"] = this.investorProductCodeDict;
                this.ViewState["cacheKey"] = this.SelectedPrgsCacheKey;
            }
            else
            {
                this.selectedPrograms = this.ViewState["selectedPrograms"] as List<ProgramInfo>;
                this.srcInvestorName = this.ViewState["srcInvestorName"] as string;
                this.destInvestorName = this.ViewState["destInvestorName"] as string;
                this.investorProductCodeDict = this.ViewState["investorProductCodeDict"] as Dictionary<string, List<string>>;
                this.SelectedPrgsCacheKey = (this.ViewState["cacheKey"] as string) ?? string.Empty;

                if (this.srcInvestorName != this.ddlSrcInvestor.SelectedValue || this.destInvestorName != this.ddlDestInvestor.SelectedValue)
                {
                    this.srcInvestorName = this.ddlSrcInvestor.SelectedValue;
                    this.destInvestorName = this.ddlDestInvestor.SelectedValue;
                    this.ViewState["srcInvestorName"] = this.srcInvestorName;
                    this.ViewState["destInvestorName"] = this.destInvestorName;
                }
                else
                {
                    initMap = false;
                }

                initSrcInvestors();
            }

            this.SrcProductCodes = srcProductCodeDict[this.srcInvestorName];

            List<string> tmpDestProductCodes = null;
            if (!this.investorProductCodeDict.TryGetValue(this.destInvestorName, out tmpDestProductCodes))
            {
                tmpDestProductCodes = new List<string>();
            }

            this.destProductCodes = new List<string>(tmpDestProductCodes);
            this.destProductCodes.Insert(0, string.Empty);

            if (initMap)
            {
                this.NewProductCodes = new List<string>();

                DataTable table = new DataTable("ProductCodeMapping");
                table.Columns.Add("Id", typeof(int));
                table.Columns.Add("ProductCode", typeof(string));
                table.Columns.Add("NewProductCode", typeof(string));

                for (int i = 0; i < this.SrcProductCodes.Count; i++)
                {
                    DataRow newRow = table.NewRow();
                    newRow["Id"] = i;
                    newRow["ProductCode"] = this.SrcProductCodes[i];
                    string newProductCode = this.destProductCodes.Contains(this.SrcProductCodes[i]) ? this.SrcProductCodes[i] : string.Empty;
                    newRow["NewProductCode"] = newProductCode;
                    table.Rows.Add(newRow);
                    this.NewProductCodes.Add(newProductCode);
                }

                this.productCodeRepeater.DataSource = table.DefaultView;
                this.productCodeRepeater.DataBind();
            }
        }

        /// <summary>
        /// On item dataBound.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="args">The "repeater item" event argument.</param>
        protected void ProductCodeRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            DropDownList ddlDestProductCodes = args.Item.FindControl("ddlDestProductCodes") as DropDownList;
            ddlDestProductCodes.DataSource = this.destProductCodes;
            ddlDestProductCodes.DataBind();

            DataRowView dataItem = args.Item.DataItem as DataRowView;
            int id = (int)dataItem["Id"];
            ddlDestProductCodes.SelectedValue = this.NewProductCodes[id];

            ddlDestProductCodes.Attributes["onChange"] = $"destProductCodeChange({id}, this.value);";
        }

        /// <summary>
        /// Batch-modify investor/product code.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void RunClick(object sender, System.EventArgs e)
        {
            var productCodeMap = new Dictionary<string, string>();

            this.NewProductCodes = new List<string>(this.saveNewProductCode.Value.Split(';'));

            for (int i = 0; i < this.SrcProductCodes.Count; i++)
            {
                productCodeMap[this.SrcProductCodes[i]] = this.NewProductCodes[i];
            }

            foreach (var prgInfo in this.selectedPrograms.Where(p => p.InvestorName == this.srcInvestorName))
            {
                try
                {
                    Guid prgId = prgInfo.Id;

                    var data = new CLoanProductData(prgId);
                    data.SuppressEventHandlerCall = true;
                    data.InitSave();
                    if (data.lBaseLpId != Guid.Empty)
                    {
                        Tools.LogError("Error: ProductCodeBatchEdit.aspx only modifies investor/roduct code for non-derived programs.");
                        continue;
                    }

                    data.lLpInvestorNm = this.destInvestorName;
                    data.ProductCodeOverrideBit = true;
                    data.ProductCode = productCodeMap[prgInfo.ProductCode];
                    data.ProductCodeInherit = data.ProductCode;

                    data.Save();
                }
                catch (SqlException exc)
                {
                    this.ErrorMessage = $"Fail to modify the loan program {prgInfo.Id} with new investor {this.destInvestorName}, product code {productCodeMap[prgInfo.ProductCode]}.\r\n" + exc.Message;
                    break;
                }
                catch (LqbException exc)
                {
                    this.ErrorMessage = $"Fail to underive the loan program {prgInfo.Id} with new investor {this.destInvestorName}, product code {productCodeMap[prgInfo.ProductCode]}.\r\n" + exc.Message;
                    break;
                }
                catch (Exception exc)
                {
                    Tools.LogError($"Fail to underive the loan program {prgInfo.Id} with new investor {this.destInvestorName}, product code {productCodeMap[prgInfo.ProductCode]}.\r\n" + exc.Message);
                    throw;
                }
            }

            // We're finished, so close the window.
            this.Command = "Close";
        }

        /// <summary>
        /// Get programs infomation from program ids that provided by SelectedPrgsCacheKey.
        /// </summary>
        /// <returns>List of ProgramInfo objects.</returns>
        private List<ProgramInfo> GetSelectedPrograms()
        {
            List<Guid> prgIds = null;

            this.SelectedPrgsCacheKey = RequestHelper.GetSafeQueryString("s") ?? string.Empty;
            if (!string.IsNullOrEmpty(this.SelectedPrgsCacheKey))
            {
                prgIds = AutoExpiredTextCache.GetUserObject<List<Guid>>(PrincipalFactory.CurrentPrincipal, this.SelectedPrgsCacheKey);
            }

            if (prgIds == null)
            {
                throw CBaseException.GenericException("Cache item time ran out");
            }

            var programs = new List<ProgramInfo>();

            // get derived programs (id, investorName, ProductCode)
            List<SqlParameter> parameters = new List<SqlParameter>();
            var sql = new StringBuilder("SELECT lLpTemplateId, lLpInvestorNm, ProductCode FROM LOAN_PROGRAM_TEMPLATE where lLpTemplateId ");
            sql.Append(DbTools.CreateParameterized4InClauseSql("lpId", prgIds, parameters));

            Action<IDataReader> readPrgHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    var prg = new ProgramInfo()
                    {
                        Id = reader.SafeGuid("lLpTemplateId"),
                        InvestorName = reader.SafeString("lLpInvestorNm"),
                        ProductCode = reader.SafeString("ProductCode")
                    };

                    programs.Add(prg);
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql.ToString(), null, parameters, readPrgHandler);

            return programs;
        }

        /// <summary>
        /// This class contains loan program's Id, investor name, product code.
        /// </summary>
        [Serializable]
        private class ProgramInfo
        {
            /// <summary>
            ///  Gets or sets loan program Guid.
            /// </summary>
            public Guid Id { get; set; }

            /// <summary>
            ///  Gets or sets loan program's investor name.
            /// </summary>
            public string InvestorName { get; set; }

            /// <summary>
            ///  Gets or sets loan program's product code.
            /// </summary>
            public string ProductCode { get; set; }
        }
    }
}