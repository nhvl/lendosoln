<%@ Page language="c#" Codebehind="DerivationJobList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.DerivationJobList" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html>
  <head>
    <title>DerivationJobList</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </head>
<body MS_POSITIONING="FlowLayout">
    <script>
        function _init()
		{
			<% if( IsPostBack == false ) { %>
				resize( 970 , 600 );
			<% } %>
		}
    </script>
    <h4 class="page-header">Derivation Job List</h4>
    <form id="DerivationJobList" method="post" runat="server">
	<table width="100%" height="370px" border="0" cellspacing="2" cellpadding="3">
	<tr valign=top>
		<td>
		<div style="BORDER-RIGHT: 2px; PADDING-RIGHT: 4px; BORDER-LEFT: lightgrey 2px solid; PADDING-LEFT: 4px; BORDER-TOP: lightgrey 2px solid; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; PADDING-BOTTOM: 4px; OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 470px">

    <ml:EncodedLiteral ID="m_status" Runat="server" EnableViewState=false></ml:EncodedLiteral>
    <ml:CommonDataGrid id="m_jobDg" Runat="server" DataKeyField="JobId" AutoGenerateColumns="False" OnItemCommand="JobItemCommand" OnItemDataBound="JobItemDataBound">
      <Columns>
        <asp:ButtonColumn ButtonType="LinkButton" CommandName="cancel" Text="cancel" />
        <asp:ButtonColumn ButtonType="LinkButton" CommandName="enqueue" Text="enqueue" />
        <asp:BoundColumn HeaderText="Status" DataField="JobStatus" />
        <asp:BoundColumn HeaderText="Priority" DataField="JobPriority" />
        <asp:BoundColumn HeaderText="Date Submitted" DataField="JobSubmitD" />
        <asp:BoundColumn HeaderText="Notify Email" DataField="JobNotifyEmailAddr" />
        <asp:BoundColumn HeaderText="Label" DataField="JobUserLabel" />
        <asp:BoundColumn HeaderText="Login Name" DataField="UserLoginNm" />
      </Columns>
    </ml:CommonDataGrid>
    </div>
    </td>
    </tr>
    <tr>
      <td align=center>
        <input type="button" value=" Close " onclick="onClosePopup()" >

      </td>
    </tr>
    </table>
     </form>
	<uc:cModalDlg id="CModalDlg" runat="server"></uc:cModalDlg>
  </body>
</html>
