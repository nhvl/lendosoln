<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CCTemplate.ascx.cs" Inherits="LendersOfficeApp.Template.CCTemplate" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="GoodFaithEstimateRightColumn" Src="../LegalForm/GoodFaithEstimateRightColumn.ascx" %>

<h4 class="page-header">Closing Cost Template</h4>
<table class="FormTable" id="Table3" cellSpacing="0" cellPadding="0" width="750" border="0">
	<tr>
		<td>
			<asp:Panel id=m_AccessDeniedPanel style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
				Access Denied.  You do not have permission to access Closing Cost Templates.
			</asp:Panel>
		</td>
	</tr>
	<asp:Panel ID=m_AccessAllowedPanel Runat=server>
  <TR>
    <TD>Template Name <asp:textbox id=cCcTemplateNm runat="server" Width="270px" Height="21px" MaxLength="36"></asp:textbox><IMG 
      src="../../images/require_icon.gif">
        <asp:requiredfieldvalidator id=RequiredFieldValidator1 runat="server" ControlToValidate="cCcTemplateNm">
            <IMG runat="server" src="../../images/error_icon.gif">
        </asp:requiredfieldvalidator></TD></TR>
  <TR>
    <TD><asp:RadioButtonList ID="cGfeVersion" RepeatDirection="Horizontal" runat="server">
      <asp:ListItem Text="GFE 2009" Value="Gfe2009"></asp:ListItem>
      <asp:ListItem Text="GFE 2010" Value="Gfe2010"></asp:ListItem>
      </asp:RadioButtonList></TD></TR>
  <TR>
    <TD style="color:Red"><ml:EncodedLiteral ID="m_errMsg" runat="server"></ml:EncodedLiteral></TD></TR>
  <TR>
    <TD>
      <TABLE id=Table1 
      style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px" 
      cellSpacing=0 cellPadding=0 width=740 border=0>
        <TR>
          <TD class=FormTableHeader></TD>
          <TD class=FormTableHeader colSpan=5>Options Affecting 
        Calculations</TD></TR>
        <TR>
          <TD></TD>
          <TD style="WIDTH: 163px">Days in Year</TD>
          <TD><asp:textbox id=cDaysInYr tabIndex=-1 runat="server" Width="40px" MaxLength="3"></asp:textbox></TD>
          <TD></TD>
          <TD></TD>
          <TD></TD>
          <TD></TD></TR>
        <TR>
          <TD colSpan=5>&nbsp;</TD></TR>
        <TR>
          <TD class="FormTableHeader"></TD>
          <TD class="FormTableHeader"></TD>
          <TD class="FormTableHeader" colSpan="5">B&nbsp;= Paid to broker&nbsp;&nbsp;&nbsp;A 
              = APR&nbsp;&nbsp;&nbsp;F = FHA Allowable&nbsp;&nbsp;&nbsp;POC = Paid Outside of 
              Closing</TD></TR>
        <TR>
          <TD class=FormTableHeader></TD>
          <TD class=FormTableHeader>Description of Charge</TD>
          <TD class=FormTableHeader></TD>
          <TD class=FormTableHeader>Amount</TD>
          <TD class=FormTableHeader>Paid By</TD>
          <TD class=FormTableHeader style="WIDTH: 44px"></TD>
          <TD class=FormTableHeader></TD></TR>
        <TR>
          <TD class=FormTableSubheader>800</TD>
          <TD class=FormTableSubheader colSpan=7>ITEMS PAYABLE IN CONNECTION 
            WITH LOAN</TD></TR>
        <TR>
          <TD>801</TD>
          <TD style="WIDTH: 163px">Loan origination fee</TD>
          <TD><ml:percenttextbox id=cLOrigFPc runat="server" preset="percent" width="70"></ml:percenttextbox>+ 
<ml:moneytextbox id=cLOrigFMb runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD><ml:moneytextbox id=cLOrigF runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cLOrigFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>802</TD>
          <TD style="WIDTH: 163px">Loan discount</TD>
          <TD noWrap><ml:percenttextbox id=cLDiscntPc runat="server" preset="percent" width="70"></ml:percenttextbox>+ 
<ml:moneytextbox id=cLDiscntFMb runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD><ml:moneytextbox id=cLDiscnt runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cLDiscntProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>803</TD>
          <TD style="WIDTH: 163px">Appraisal fee</TD>
          <TD align=right><asp:checkbox id=cApprFPaid runat="server" Width="49px" Text="Paid" onclick="OnPaidChecking(this);"></asp:checkbox>&nbsp;</TD>
          <TD><ml:moneytextbox id=cApprF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cApprFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>804</TD>
          <TD style="WIDTH: 163px">Credit report</TD>
          <TD align=right><asp:checkbox id=cCrFPaid runat="server" Width="49px" Text="Paid" onclick="OnPaidChecking(this);"></asp:checkbox>&nbsp;</TD>
          <TD><ml:moneytextbox id=cCrF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cCrFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>805</TD>
          <TD style="WIDTH: 163px">Lender's inspection fee</TD>
          <TD></TD>
          <TD><ml:moneytextbox id=cInspectF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cInspectFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>808</TD>
          <TD style="WIDTH: 163px">Mortgage broker fee</TD>
          <TD noWrap><ml:percenttextbox id=cMBrokFPc runat="server" preset="percent" width="70"></ml:percenttextbox>+ 
<ml:moneytextbox id=cMBrokFMb runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD><ml:moneytextbox id=cMBrokF runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cMBrokFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>809</TD>
          <TD style="WIDTH: 163px">Tax service fee</TD>
          <TD></TD>
          <TD><ml:moneytextbox id=cTxServF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cTxServFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>810</TD>
          <TD style="WIDTH: 163px">Processing fee</TD>
          <TD align=right><asp:checkbox id=cProcFPaid runat="server" Width="49px" Text="Paid" onclick="OnPaidChecking(this);"></asp:checkbox>&nbsp;</TD>
          <TD><ml:moneytextbox id=cProcF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cProcFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>811</TD>
          <TD style="WIDTH: 163px">Underwriting fee</TD>
          <TD></TD>
          <TD><ml:moneytextbox id=cUwF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cUwFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>812</TD>
          <TD style="WIDTH: 163px">Wire transfer</TD>
          <TD></TD>
          <TD><ml:moneytextbox id=cWireF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cWireFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=c800U1FCode runat="server" Width="39" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=c800U1FDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=c800U1F runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=c800U1FProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=c800U2FCode runat="server" Width="39" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=c800U2FDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=c800U2F runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=c800U2FProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=c800U3FCode runat="server" Width="39" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=c800U3FDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=c800U3F runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=c800U3FProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=c800U4FCode runat="server" Width="39" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=c800U4FDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=c800U4F runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=c800U4FProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=c800U5FCode runat="server" Width="39" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=c800U5FDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=c800U5F runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=c800U5FProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD class=FormTableSubheader>900</TD>
          <TD class=FormTableSubheader colSpan=7>ITEMS REQUIRED BY LENDER TO 
            BE PAID IN ADVANCE</TD></TR>
        <TR>
          <TD>901</TD>
          <TD style="WIDTH: 163px">Interest for</TD>
          <TD noWrap><asp:textbox id=cIPiaDy runat="server" Width="40px"></asp:textbox>&nbsp;days 
            @ <ml:moneytextbox id=cIPerDay runat="server" preset="money" width="42px" ReadOnly="True"></ml:moneytextbox>per 
            day</TD>
          <TD><ml:moneytextbox id=cIPia runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cIPiaProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>902</TD>
          <TD style="WIDTH: 163px">MIP @ <ml:percenttextbox id=cProMInsR runat="server" preset="percent" width="70"></ml:percenttextbox>&nbsp;of</TD>
          <TD noWrap><asp:dropdownlist id=cProMInsT runat="server">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Value</asp:ListItem>
						</asp:dropdownlist>&nbsp;for <asp:textbox id=cMipPiaMon runat="server" Width="44px" maxlength="3"></asp:textbox>&nbsp;months</TD>
          <TD><ml:moneytextbox id=cMipPia runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cMipPiaProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>903</TD>
          <TD noWrap>Haz Ins. @ <ml:percenttextbox id=cProHazInsR runat="server" preset="percent" width="69px"></ml:percenttextbox>of&nbsp;</TD>
          <TD><asp:dropdownlist id=cProHazInsT runat="server">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2" Selected="True">Appraisal Value</asp:ListItem>
						</asp:dropdownlist>&nbsp;for <asp:textbox id=cHazInsPiaMon runat="server" Width="44px" maxlength="3"></asp:textbox>&nbsp;months</TD>
          <TD><ml:moneytextbox id=cHazInsPia runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cHazInsPiaProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>904</TD>
          <TD colSpan=2><asp:textbox id=c904PiaDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=c904Pia runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=c904PiaProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>905</TD>
          <TD>VA Funding Fee</TD>
          <TD></TD>
          <TD><ml:moneytextbox id=cVaFf runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cVaFfProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=c900U1PiaCode runat="server" Width="39" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=c900U1PiaDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=c900U1Pia runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=c900U1PiaProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD class=FormTableSubheader><A name=1000></A>1000</TD>
          <TD class=FormTableSubheader colSpan=7>RESERVES DEPOSITED WITH 
          LENDER</TD></TR>
        <TR>
          <TD>1001</TD>
          <TD style="WIDTH: 163px">Haz ins. reserve</TD>
          <TD><asp:textbox id=cHazInsRsrvMon runat="server" Width="41" maxlength="3"></asp:textbox>&nbsp;mths 
            @ <ml:moneytextbox id=cProHazIns runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>/ 
            month</TD>
          <TD><ml:moneytextbox id=cHazInsRsrv runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cHazInsRsrvProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1002</TD>
          <TD style="WIDTH: 163px">Mtg ins. reserve</TD>
          <TD><asp:textbox id=cMInsRsrvMon runat="server" Width="41px" maxlength="3"></asp:textbox>&nbsp;mths 
            @ <ml:moneytextbox id=cProMIns runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>/ 
            month</TD>
          <TD><ml:moneytextbox id=cMInsRsrv runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cMInsRsrvProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1003</TD>
          <TD style="WIDTH: 163px">School taxes</TD>
          <TD><asp:textbox id=cSchoolTxRsrvMon runat="server" Width="41px" maxlength="3"></asp:textbox>&nbsp;mths 
            @ <ml:moneytextbox id=cProSchoolTx runat="server" preset="money" width="90"></ml:moneytextbox>/ 
            month</TD>
          <TD><ml:moneytextbox id=cSchoolTxRsrv runat="server" preset="money" width="90" readonly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cSchoolTxRsrvProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1004</TD>
          <TD style="WIDTH: 163px">Tax reserve</TD>
          <TD><asp:textbox id=cRealETxRsrvMon runat="server" Width="41px" maxlength="3"></asp:textbox>&nbsp;mths 
            @ <ml:moneytextbox id=cProRealETx runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox>/ 
            month</TD>
          <TD><ml:moneytextbox id=cRealETxRsrv runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cRealETxRsrvProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1005</TD>
          <TD style="WIDTH: 163px">Flood ins. reserve</TD>
          <TD><asp:textbox id=cFloodInsRsrvMon runat="server" Width="41px" maxlength="3"></asp:textbox>&nbsp;mths 
            @ <ml:moneytextbox id=cProFloodIns runat="server" preset="money" width="90"></ml:moneytextbox>/ 
            month</TD>
          <TD><ml:moneytextbox id=cFloodInsRsrv runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cFloodInsRsrvProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1006</TD>
          <TD style="WIDTH: 163px"><asp:textbox id=c1006ProHExpDesc runat="server" Width="135" Height="20px" maxlength="21"></asp:textbox></TD>
          <TD><asp:textbox id=c1006RsrvMon runat="server" Width="41px" maxlength="3"></asp:textbox>&nbsp;mths 
            @ <ml:moneytextbox id=c1006ProHExp runat="server" preset="money" width="90"></ml:moneytextbox>/ 
            month</TD>
          <TD><ml:moneytextbox id=c1006Rsrv runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=c1006RsrvProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1007</TD>
          <TD style="WIDTH: 163px"><asp:textbox id=c1007ProHExpDesc runat="server" Width="136" Height="20px" maxlength="21"></asp:textbox></TD>
          <TD><asp:textbox id=c1007RsrvMon runat="server" Width="41px" maxlength="3"></asp:textbox>&nbsp;mths 
            @ <ml:moneytextbox id=c1007ProHExp runat="server" preset="money" width="90"></ml:moneytextbox>/ 
            month</TD>
          <TD><ml:moneytextbox id=c1007Rsrv runat="server" preset="money" width="90" readonly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=c1007RsrvProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1008</TD>
          <TD style="WIDTH: 163px">Aggregate adjustment</TD>
          <TD></TD>
          <TD><ml:moneytextbox id=cAggregateAdjRsrv runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cAggregateAdjRsrvProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD class=FormTableSubheader>1100</TD>
          <TD class=FormTableSubheader colSpan=7>TITLE CHARGES</TD></TR>
        <TR>
          <TD>1101</TD>
          <TD>Closing/Escrow fee <asp:textbox id=cEscrowFTable runat="server" Width="20px" maxlength="150"></asp:textbox></TD>
          <TD>
       
                <ml:PercentTextBox ID="cEscrowFPc" runat="server" preset="percent" Width="50"></ml:PercentTextBox>
                of
                <asp:DropDownList ID="cEscrowFBaseT" runat="server">
                    <asp:ListItem Value="0">Loan Amount</asp:ListItem>
                    <asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
                    <asp:ListItem Value="2">Appraisal Price</asp:ListItem>
                </asp:DropDownList>
                +
                <ml:MoneyTextBox ID="cEscrowFMb" runat="server" preset="money" Width="90"></ml:MoneyTextBox>
            </td>
          <TD><ml:moneytextbox id=cEscrowF ReadOnly=true runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cEscrowFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1105</TD>
          <TD style="WIDTH: 163px">Doc preparation fee</TD>
          <TD></TD>
          <TD><ml:moneytextbox id=cDocPrepF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cDocPrepFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1106</TD>
          <TD style="WIDTH: 163px">Notary fees</TD>
          <TD></TD>
          <TD><ml:moneytextbox id=cNotaryF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cNotaryFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1107</TD>
          <TD style="WIDTH: 163px">Attorney fees</TD>
          <TD></TD>
          <TD><ml:moneytextbox id=cAttorneyF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cAttorneyFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1108</TD>
          <TD>Title Insurance <asp:textbox id=cTitleInsFTable runat="server" Width="30" maxlength="50"></asp:textbox></TD>
          <TD>
                
          
          <ml:percenttextbox id=cTitleInsFPc runat="server" preset="percent" width="50"></ml:percenttextbox>of 
          <asp:dropdownlist id=cTitleInsFBaseT runat="server">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
        </asp:dropdownlist>+ <ml:moneytextbox id=cTitleInsFMb runat="server" preset="money" width="90"></ml:moneytextbox>
        </td>
          <TD><ml:moneytextbox id=cTitleInsF  ReadOnly=true  runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cTitleInsFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU1TcCode runat="server" Width="39" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=cU1TcDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cU1Tc runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU1TcProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU2TcCode runat="server" Width="39" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=cU2TcDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cU2Tc runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU2TcProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU3TcCode runat="server" Width="39" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=cU3TcDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cU3Tc runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU3TcProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU4TcCode runat="server" Width="39" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=cU4TcDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cU4Tc runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU4TcProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD class=FormTableSubheader>1200</TD>
          <TD class=FormTableSubheader colSpan=7>GOVERNMENT RECORDING &amp; 
            TRANSFER CHARGES</TD></TR>
        <TR>
          <TD>1201</TD>
          <TD style="WIDTH: 163px">Recording fees
			<asp:textbox id=cRecFDesc onblur=unhighlightRow(this); onfocus=highlightRow(this); runat="server" Width="55px"></asp:textbox></TD>
          <TD noWrap>
          <ml:percenttextbox id=cRecFPc runat="server" preset="percent" width="70"></ml:percenttextbox>of 
<asp:dropdownlist id=cRecBaseT runat="server">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist>+ <ml:moneytextbox id=cRecFMb runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD><ml:moneytextbox id=cRecF runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cRecFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1202</TD>
          <TD style="WIDTH: 163px">City tax/stamps 
			<asp:textbox id=cCountyRtcDesc onblur=unhighlightRow(this); onfocus=highlightRow(this); runat="server" Width="56px"></asp:textbox></TD>
          <TD noWrap><ml:percenttextbox id=cCountyRtcPc runat="server" preset="percent" width="70"></ml:percenttextbox>of 
<asp:dropdownlist id=cCountyRtcBaseT runat="server">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist>+ <ml:moneytextbox id=cCountyRtcMb runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD><ml:moneytextbox id=cCountyRtc runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cCountyRtcProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD>1203</TD>
          <TD style="WIDTH: 163px">State tax/stamps
			<asp:textbox id=cStateRtcDesc onblur=unhighlightRow(this); onfocus=highlightRow(this); runat="server" Width="48px"></asp:textbox></TD>
          <TD noWrap><ml:percenttextbox id=cStateRtcPc runat="server" preset="percent" width="70"></ml:percenttextbox>of 
<asp:dropdownlist id=cStateRtcBaseT runat="server">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist>+ <ml:moneytextbox id=cStateRtcMb runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD><ml:moneytextbox id=cStateRtc runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cStateRtcProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU1GovRtcCode runat="server" Width="39px" maxlength="21"></asp:textbox></TD>
          <TD style="WIDTH: 163px"><asp:textbox id=cU1GovRtcDesc runat="server" Width="104" maxlength="21"></asp:textbox></TD>
          <TD noWrap><ml:percenttextbox id=cU1GovRtcPc runat="server" preset="percent" width="70"></ml:percenttextbox>of 
<asp:dropdownlist id=cU1GovRtcBaseT runat="server">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist>+ <ml:moneytextbox id=cU1GovRtcMb runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD><ml:moneytextbox id=cU1GovRtc runat="server" preset="money" width="90" readonly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU1GovRtcProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU2GovRtcCode runat="server" Width="39px" maxlength="21"></asp:textbox></TD>
          <TD style="WIDTH: 163px"><asp:textbox id=cU2GovRtcDesc runat="server" Width="104px" Height="20px" maxlength="21"></asp:textbox></TD>
          <TD noWrap><ml:percenttextbox id=cU2GovRtcPc runat="server" preset="percent" width="70"></ml:percenttextbox>of 
<asp:dropdownlist id=cU2GovRtcBaseT runat="server">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist>+ <ml:moneytextbox id=cU2GovRtcMb runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD><ml:moneytextbox id=cU2GovRtc runat="server" preset="money" width="90" readonly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU2GovRtcProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU3GovRtcCode runat="server" Width="39px" maxlength="21"></asp:textbox></TD>
          <TD style="WIDTH: 163px"><asp:textbox id=cU3GovRtcDesc runat="server" Width="104px" Height="20px" maxlength="21"></asp:textbox></TD>
          <TD noWrap><ml:percenttextbox id=cU3GovRtcPc runat="server" preset="percent" width="70"></ml:percenttextbox>of 
<asp:dropdownlist id=cU3GovRtcBaseT runat="server">
							<asp:ListItem Value="0">Loan Amount</asp:ListItem>
							<asp:ListItem Value="1" Selected="True">Purchase Price</asp:ListItem>
							<asp:ListItem Value="2">Appraisal Price</asp:ListItem>
						</asp:dropdownlist>+ <ml:moneytextbox id=cU3GovRtcMb runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD><ml:moneytextbox id=cU3GovRtc runat="server" preset="money" width="90" ReadOnly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU3GovRtcProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD class=FormTableSubheader>1300</TD>
          <TD class=FormTableSubheader colSpan=7>ADDITIONAL SETTLEMENT 
          CHARGES</TD></TR>
        <TR>
          <TD>1302</TD>
          <TD style="WIDTH: 163px">Pest Inspection</TD>
          <TD></TD>
          <TD><ml:moneytextbox id=cPestInspectF runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cPestInspectFProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU1ScCode runat="server" Width="39px" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=cU1ScDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cU1Sc runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU1ScProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU2ScCode runat="server" Width="39px" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=cU2ScDesc runat="server" Width="388px" Height="20px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cU2Sc runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU2ScProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU3ScCode runat="server" Width="39px" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=cU3ScDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cU3Sc runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU3ScProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU4ScCode runat="server" Width="39px" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=cU4ScDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cU4Sc runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU4ScProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD><asp:textbox id=cU5ScCode runat="server" Width="39px" maxlength="21"></asp:textbox></TD>
          <TD colSpan=2><asp:textbox id=cU5ScDesc runat="server" Width="388px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cU5Sc runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3><uc1:goodfaithestimaterightcolumn id=cU5ScProps_ctrl runat="server"></uc1:goodfaithestimaterightcolumn></TD></TR>
        <TR>
          <TD colSpan=3>TOTAL ESTIMATED SETTLEMENT CHARGES</TD>
          <TD><ml:moneytextbox id=cTotEstSc runat="server" preset="money" width="90" readonly="True"></ml:moneytextbox></TD>
          <TD noWrap colSpan=3></TD></TR></TABLE></TD></TR>
  <TR>
    <TD>
      <TABLE id=Table9 style="HEIGHT: 43px" cellSpacing=0 cellPadding=0 
      width=600 border=0>
        <TR>
          <TD>COMPENSATION TO BROKER (Not Paid Out of Loan Proceeds)</TD>
          <TD></TD></TR>
        <TR>
          <TD><asp:textbox id=cBrokComp1Desc runat="server" Width="427px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cBrokComp1 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR>
        <TR>
          <TD><asp:textbox id=cBrokComp2Desc runat="server" Width="427px" maxlength="150"></asp:textbox></TD>
          <TD><ml:moneytextbox id=cBrokComp2 runat="server" preset="money" width="90"></ml:moneytextbox></TD></TR></TABLE></TD></TR>
  <TR>
    <TD>
      <TABLE id=Table2 cellSpacing=0 cellPadding=0 width="100%" border=0>
        <TR>
          <TD class=FormTableHeader colSpan=4>Total Estimated Funds Needed to 
            Close</TD></TR>
        <TR>
          <TD>Settlement Charges Paid by Seller</TD>
          <TD>-</TD>
          <TD><ml:moneytextbox id=cTotCcPbs runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD><asp:checkbox id=cTotCcPbsLocked runat="server" Text="Lock"></asp:checkbox></TD></TR>
        <TR>
          <TD>Credit Life and/or Disability Insurance</TD>
          <TD>+</TD>
          <TD><ml:moneytextbox id=cDisabilityIns runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD></TD></TR>
        <TR>
          <TD><asp:textbox id=cU1FntcDesc runat="server" Width="381px" maxlength="150"></asp:textbox></TD>
          <TD>+</TD>
          <TD><ml:moneytextbox id=cU1Fntc runat="server" preset="money" width="90"></ml:moneytextbox></TD>
          <TD></TD></TR>
        <TR>
          <TD></TD>
          <TD></TD>
          <TD></TD>
          <TD></TD></TR></TABLE></TD></TR>
  <TR>
    <TD><asp:checkbox id=cGfeProvByBrok runat="server" Text="This Good Faith Estimate is being provided by broker"></asp:checkbox></TD></TR>
  <TR>
    <TD align=center><asp:button id=m_okBtn runat="server" Text="  OK  " onclick="onOKClick"></asp:button><INPUT onclick=onClosePopup(); type=button value=Cancel> 
<asp:button id=m_applyBtn runat="server" Text="Apply"></asp:button></TD></TR></asp:Panel></table>
<input type="hidden" id="m_hiddenChoice" value="" runat="server" NAME="m_hiddenChoice"/> 
