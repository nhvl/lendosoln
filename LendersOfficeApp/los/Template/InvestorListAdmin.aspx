<%@ Register TagPrefix="Investor" TagName="InvestorListControl" Src="../../los/Template/InvestorListControl.ascx"%>
<%@ Page language="c#" Codebehind="InvestorListAdmin.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.InvestorListAdmin" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Investor List Admin</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout" scroll="no">
	<script>
        function _init() {
          <% if ( !Page.IsPostBack ) { %>
            resize( 800 , 600 );
          <% } %>
        }
	</script>
		<form id="BranchBrefix" method="post" runat="server">
			<Investor:InvestorListControl runat="server" ID="m_investorList" NAME="m_investorList"></Investor:InvestorListControl>
		</form>
	</body>
</HTML>
