namespace LendersOfficeApp.Template
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.RatePrice;
    using LendersOffice.Security;
    using LendersOfficeApp.los.Template;
    using Microsoft.Web.UI.WebControls;
    using LendersOffice.ObjLib.DynaTree;

    /// <summary>
    /// Batch Derive loan programs. In other words, derive selected programs to multiple product folders (of multiple brokers).
    /// Require: any two target folders cannot have ancestor relationship.
    /// - The original code got from CreateDerivationJob.aspx, but modify.
    /// - Thien note 5/22/2017: Microsoft.Web.UI.WebControls.TreeView has some bug. Assume the server side checks 
    /// for some checkboxes and at client side captures the event click. Sometime the event doesn't get correct information 
    /// about check or uncheck. Therefore we don't check at server side, we provide "check" information and client will do it.
    /// - The hotfix version doesn't have complete code for schedule feature.
    /// </summary>
    public partial class BatchDerivationJob : LendersOffice.Common.BasePage
    {
        /// <summary>
        /// Selected programs or folders. They are source programs or folder.
        /// </summary>
        private List<Guid> selectedPrograms; 

        /// <summary>
        /// Map from brokerId to target folder Ids. Any two target folders cannot have ancestor relationship.
        /// </summary>
        private Dictionary<Guid, List<string>> targetsDict;

        /// <summary>
        /// Gets a value indicating whether "login" broker and target broker are same => don't allow source folder and target folder are same.
        /// </summary>
        /// <value>True for same broker.</value>
        protected bool SameBroker { get; private set; }

        /// <summary>
        /// Gets/sets the cache key for selected program.
        /// </summary>
        /// <value>Cache key for selected programs.</value>
        protected string SelectedPrgsCacheKey { get; private set; } = string.Empty;

        /// <summary>
        /// Gets Current Broker Principal.
        /// </summary>
        /// <value>Broker User Principal.</value>
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        /// <summary>
        /// Sets error message that will show by js alert.
        /// </summary>
        private string ErrorMessage
        {
            set { ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + "."); }
        }

        /// <summary>
        /// Sets feedback that will show by js alert.
        /// </summary>
        private string Feedback
        {
            set { ClientScript.RegisterHiddenField("m_feedBack", value.TrimEnd('.') + "."); }
        }

        /// <summary>
        /// Sets Command that will use by js client side.
        /// </summary>
        private string Command
        {
            set { ClientScript.RegisterHiddenField("m_cmdToDo", value); }
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="a">The event argument.</param>
        protected void PageLoad(object sender, System.EventArgs a)
        {
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");

            //// Check security access control bits and deny if not allowed.

            if (this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms) == false)
            {
                this.ErrorMessage = "Access denied.";
                return;
            }

            DateTime now = DateTime.Now;
            bool isWeekday = now.DayOfWeek != DayOfWeek.Saturday && now.DayOfWeek != DayOfWeek.Sunday;
            if (isWeekday && (now.TimeOfDay > new TimeSpan(5, 0, 0) && now.TimeOfDay < new TimeSpan(17, 0, 0)))
            {
                this.timeWarning.Text = "It is now " + now.ToShortTimeString() + " on a weekday. " +
                "Derivation is an expensive operation that can slow down the system during times of heavy traffic. " +
                "Please consider if this can wait until after 5pm when it would be less likely to impact clients.";

                this.timeWarning.Visible = true;
            }
            else
            {
                this.timeWarning.Visible = false;
            }

            //// Get the broker list on the first load.  We assume the list saves state between posts.

            if (this.IsPostBack == false)
            {
                this.m_ignoreTargetFolderIds.Value = string.Empty;
                this.PreprocessSelectedPrgs();

                // Load up the brokers and store by their id.
                List<KeyValuePair<Guid, string>> list = new List<KeyValuePair<Guid, string>>();

                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    SqlParameter[] parameters = 
                    {
                        new SqlParameter("@HasLenderDefaultFeatures", true),
                        new SqlParameter("@BrokerStatus", 1)
                    };

                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListBrokers", parameters))
                    {
                        while (reader.Read())
                        {
                            list.Add(new KeyValuePair<Guid, string>((Guid)reader["BrokerId"], (string)reader["BrokerNm"]));
                        }
                    }
                }

                this.m_BrokerList.DataValueField = "Key";
                this.m_BrokerList.DataTextField = "Value";
                this.m_BrokerList.DataSource = list.OrderBy(o => o.Value);
                this.m_BrokerList.DataBind();

                // for the default loan program set, a request was made to be changed to the "MASTER PML" set
                this.m_BrokerList.SelectedIndex = this.m_BrokerList.Items.IndexOf(this.m_BrokerList.Items.FindByText("MASTER PML"));

                CPmlFIdGenerator generator = new CPmlFIdGenerator();
                this.m_jobLabelTb.Text = generator.GenerateNewFriendlyId();

                this.m_scheduleTime.Text = "10:00PM";

                this.targetsDict = new Dictionary<Guid, List<string>>();
                this.ViewState["targetsDict"] = this.targetsDict;
                this.ViewState["selectedPrograms"] = this.selectedPrograms;
                this.ViewState["cacheKey"] = this.SelectedPrgsCacheKey;
            }
            else
            {
                this.selectedPrograms = this.ViewState["selectedPrograms"] as List<Guid>;
                this.SelectedPrgsCacheKey = (this.ViewState["cacheKey"] as string) ?? string.Empty;

                string targetFolderIds = Request.Form["m_targetFolderIds"];
                var targetFolderList = new List<string>(targetFolderIds.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));

                this.targetsDict = (this.ViewState["targetsDict"] as Dictionary<Guid, List<string>>) ?? new Dictionary<Guid, List<string>>();
                Guid prevBrokerId = (Guid)this.ViewState["brokerId"];

                if (targetFolderList.Count > 0)
                {
                    this.targetsDict[prevBrokerId] = targetFolderList;
                }
                else
                {
                    this.targetsDict.Remove(prevBrokerId);
                }

                //// the target broker will have red color if the user selects some its product folders
                for (int i = 0; i < this.m_BrokerList.Items.Count; i++)
                {
                    var item = this.m_BrokerList.Items[i];
                    Guid brokerIdItem = new Guid(item.Value);
                    if (this.targetsDict.ContainsKey(brokerIdItem))
                    {
                        item.Attributes.CssStyle.Add("color", "red");
                        item.Attributes.CssStyle.Add("text-align", "center");
                    }
                    else
                    {
                        item.Attributes.CssStyle.Remove("color");
                        item.Attributes.CssStyle.Remove("text-align");
                    }
                }
            }

            //// Load the current broker's loan folder into the tree.

            this.m_targetFolderIds.Value = string.Empty;

            Guid curBrokerId = Guid.Empty;
            if (this.m_BrokerList.SelectedItem != null)
            {
                this.m_BrokerList.SelectedItem.Attributes.CssStyle.Add("color", "blue");
                curBrokerId = new Guid(this.m_BrokerList.SelectedItem.Value);
                this.SameBroker = curBrokerId == this.BrokerUser.BrokerId;

                //// Create Loan Folder tree

                LoanProductSet loanFolders = new LoanProductSet();
                loanFolders.Retrieve(
                                  curBrokerId,
                                  this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms),
                                  folderOnly: true);
                
                if (this.targetsDict.ContainsKey(curBrokerId))
                {
                    this.m_targetFolderIds.Value = string.Join(";", this.targetsDict[curBrokerId]);
                }

                loanFolders.rootNode.isFolder = true;
                loanFolders.rootNode.title = "Root Folder";

                loanFolders.rootNode.children = SortNodes(loanFolders.rootNode.children);

                this.ExpandProgramTree(curBrokerId, loanFolders.rootNode);

                RegisterJsObjectWithJsonNetSerializer("nodes", loanFolders.rootNode);
            }

            this.m_Status.Text = "Target Folders.";
            this.ViewState["brokerId"] = curBrokerId;            
        }

        private List<DynaTreeNode> SortNodes(List<DynaTreeNode> nodes)
        {
            foreach (DynaTreeNode node in nodes)
            {
                if (node.children.Count != 0)
                {
                    node.children = SortNodes(node.children);
                }
            }

            return nodes.OrderBy(n => n.title).ToList();
        }

        /// <summary>
        /// Insert Derive Jobs to database.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="a">The event argument.</param>        
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Notify error to user.")]

        protected void InsertClick(object sender, System.EventArgs a)
        {
            try
            {
                if (this.selectedPrograms.Count == 0)
                {
                    this.ErrorMessage = "No selected programs/folders. No job created";
                    return;
                }

                if (this.targetsDict.Count == 0)
                {
                    this.ErrorMessage = "No target folders. No job created";
                    return;
                }

                DateTime now = DateTime.Now;
                DateTime scheduleTime = new DateTime(now.Year, now.Month, now.Day, this.m_scheduleTime.Time.Hour, this.m_scheduleTime.Time.Minute, 0);

                int counter = 0;
                ArrayList sourcePrograms = new ArrayList(this.selectedPrograms);
                foreach (Guid targetBrokerId in this.targetsDict.Keys)
                {
                    foreach (var targetFolderIdStr in this.targetsDict[targetBrokerId])
                    {
                        Guid targetId = new Guid(targetFolderIdStr);
                        DerivationJobBuilder job =
                            new DerivationJobBuilder(
                                sourcePrograms,
                                targetId,
                                targetBrokerId,
                                this.BrokerUser.BrokerId,
                                this.m_enableAfterDerive.Checked);

                        job.JobPriority = this.m_priorityRbList.SelectedItem.Value;
                        job.JobNotifyEmailAddr = this.m_notifEmailTb.Text;
                        job.JobUserNotes = this.m_jobNotesTb.Text;
                        job.JobUserLabel = this.m_jobLabelTb.Text + $"-JSA{scheduleTime.Hour:00}{scheduleTime.Minute:00}-{++counter:00}";
                        job.UserLoginNm = this.BrokerUser.LoginNm;
                        job.JobStartAfterD = scheduleTime;

                        job.SendToQueue();
                    }
                }

                // We're finished, so close the window.
                this.Command = "Close";
            }
            catch (Exception e)
            {
                this.ErrorMessage = "Failed to insert job into program queue.";
                Tools.LogError(e);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

            notifEmailTbValidator.ValidationExpression = ConstApp.EmailValidationExpression;

        }
        #endregion

        /// <summary>
        /// Pre process selected programs.
        /// </summary>
        private void PreprocessSelectedPrgs()
        {
            List<Guid> items = null;
            this.SelectedPrgsCacheKey = RequestHelper.GetSafeQueryString("s") ?? string.Empty;
            if (!string.IsNullOrEmpty(this.SelectedPrgsCacheKey))
            {
                items = AutoExpiredTextCache.GetUserObject<List<Guid>>(PrincipalFactory.CurrentPrincipal, this.SelectedPrgsCacheKey);
            }

            if (items == null)
            {
                throw CBaseException.GenericException("Cache item time ran out");
            }

            this.selectedPrograms = items;

            //// don't allow any target that is "source folder" or "source folder"'s descendants and parent.

            LoanProductSet source = new LoanProductSet();
            source.Retrieve(this.BrokerUser.BrokerId, this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms));

            HashSet<Guid> ignoreTargets = new HashSet<Guid>();

            Action<LoanProductContainer> getFolderAndItsDescendantFolders = null;
            getFolderAndItsDescendantFolders = (LoanProductContainer loanFolder) =>
            {
                ignoreTargets.Add(loanFolder.Id);
                foreach (LoanProductNode child in loanFolder)
                {
                    var subFolder = child as LoanProductContainer;
                    if (subFolder != null)
                    {
                        getFolderAndItsDescendantFolders(subFolder);
                    }
                }
            };

            foreach (Guid id in items)
            {
                LoanProductNode lpN = source[id];
                if (lpN == null)
                {
                    continue; // expect: no happen
                }

                if (lpN.Type == LoanProductType.Folder)
                {
                    getFolderAndItsDescendantFolders(lpN as LoanProductContainer);
                }

                if (lpN.Parent != Guid.Empty)
                {
                    ignoreTargets.Add(lpN.Parent);
                }
            }

            this.m_ignoreTargetFolderIds.Value = string.Join(";", ignoreTargets);
        }

        /// <summary>
        /// Expand target Folders and their ancestors => user can see them without click expand node.
        /// </summary>
        /// <param name="curBrokerId"> Current target brokerId.</param>
        private void ExpandProgramTree(Guid curBrokerId, DynaTreeNode rootNode)
        {
            HashSet<string> ignoreTargets = null;
            if (curBrokerId == this.BrokerUser.BrokerId)
            {
                ignoreTargets = new HashSet<string>(this.m_ignoreTargetFolderIds.Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
            }

            if (!this.targetsDict.ContainsKey(curBrokerId) && ignoreTargets == null)
            {
                return;
            }

            HashSet<string> selectedTargets = new HashSet<string>(this.m_targetFolderIds.Value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));

            //// expand target folders and their ancestors. Disable selected (source) folders and their descendants

            HashSet<string> targetFolderSet = this.targetsDict.ContainsKey(curBrokerId) ? new HashSet<string>(this.targetsDict[curBrokerId]) : new HashSet<string>();

            Action<DynaTreeNode> traversalProcess = null;

            traversalProcess = (DynaTreeNode node) =>
            {
                if (targetFolderSet.Contains(node.key))
                {
                    if (node.children.Count > 0)
                    {
                        node.expand = true;
                    }
                }                

                if (ignoreTargets != null && ignoreTargets.Contains(node.key))
                {
                    node.addClass += " gray-color";
                }

                if (selectedTargets != null & selectedTargets.Contains(node.key))
                {
                    node.select = true;
                }

                foreach (DynaTreeNode tChild in node.children)
                {
                    traversalProcess(tChild);
                }
            };

            foreach (DynaTreeNode treeNode in rootNode.children)
            {
                traversalProcess(treeNode);
            }
        }
    }
}
