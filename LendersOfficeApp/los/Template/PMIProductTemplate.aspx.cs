using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.los.Template
{
    public partial class PMIProductTemplate : BasePage
    {
        public bool IsNew
        {
            get
            {
                return string.IsNullOrEmpty(RequestHelper.GetSafeQueryString("Id"));
            }
        }

        public string Id
        {
            get
            {
                string id = RequestHelper.GetSafeQueryString("Id");
                if (!string.IsNullOrEmpty(id))
                    return id;

                id = ProductId.Text;
                if (!string.IsNullOrEmpty(id))
                    return id;

                id = Guid.NewGuid().ToString();
                return id;
            }
        }

        public Guid FolderId
        {
            get
            {
                string id = RequestHelper.GetSafeQueryString("FolderId");
                return new Guid(id);
            }
        }

        protected void PageInit(object sender, System.EventArgs a)
        {
            if (!Page.IsPostBack)
            {
                Tools.Bind_PmiTypeT(MIType);
                Tools.Bind_PmiCompanyT(MICompany);

                ProductId.Text = Id;

                if (!IsNew)
                {
                    PMIProgramTemplateData templateData = PMIProgramTemplateData.Retrieve(new Guid(Id));
                    Tools.SetDropDownListValue(MIType, templateData.MIType);
                    Tools.SetDropDownListValue(MICompany, templateData.MICompany);
                    IsEnabled.Checked = templateData.IsEnabled;
                    UfmipIsRefundableOnProRataBasis.Checked = templateData.UfmipIsRefundableOnProRataBasis; 
                }
            }
        }

        protected void SaveClick(object sender, System.EventArgs a)
        {
            PMIProgramTemplateData templateData = new PMIProgramTemplateData();
            templateData.ProductId = new Guid(Id);
            templateData.FolderId = FolderId;
            templateData.MICompany = (E_PmiCompanyT)Enum.Parse(typeof(E_PmiCompanyT), MICompany.SelectedValue);
            templateData.MIType = (E_PmiTypeT)Enum.Parse(typeof(E_PmiTypeT), MIType.SelectedValue);
            templateData.IsEnabled = IsEnabled.Checked;
            templateData.UfmipIsRefundableOnProRataBasis = UfmipIsRefundableOnProRataBasis.Checked;
            templateData.Save();

            this.AddInitScriptFunction("_closeMe");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
