<%@ Page language="c#" Codebehind="CreateDerivationJob.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Template.CreateDerivationJob" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Create Derivation Job</title>
    <link href="../../css/stylesheet.css" rel="stylesheet">
    <style>
        #programTree {
            width: 100%;
            height: 100%;
            width: calc(100vw - 45px);
            height: calc(100vh - 310px);
            min-height: 300px;
            padding: 4px;
            background :whitesmoke;
            border: 2px groove;
        }
        .empty-message {
            text-align: center;
            color: dimgray;
            padding: 100px;
        }
        .time-warning{
            color:red;
        }
    </style>
</head>
<body scroll="no" onload="onInit();" MS_POSITIONING="FlowLayout">
    <h4 class="page-header">Create Derivation Job</h4>
    <FORM method="post" runat="server">
        <TABLE cellSpacing="8" cellPadding="0" width="100%" border="0">
            <TR>
                <TD height="1">
                    <TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
                        <TR>
                            <TD align="left">
                                Loaded loan program set.
                            </TD>
                            <TD align="right">
                                <asp:DropDownList id="m_BrokerList" runat="server" />
                            </TD>
                        </TR>
                    </TABLE>
                </TD>
            </TR>
            <TR>
                <TD height="100%">
                    <div id="programTree"></div>
                </TD>
            </TR>
            <tr>
                <td>
                    <table cellSpacing="0" cellPadding="0" border="0">
                        <tr>
                            <td colspan="2">
                                <span class="time-warning">
                                    <ml:EncodedLiteral ID="timeWarning" runat="server" Visible="false" />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Job Priority </td>
                            <td>
                                <asp:RadioButtonList id="m_priorityRbList" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="Low" />
                                    <asp:ListItem Value="Normal" Selected="True" />
                                    <asp:ListItem Value="High" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="FieldLabel">
                                <asp:CheckBox id="m_enableAfterDerive" runat="server" Text="Enable Created Programs"/>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="FieldLabel">
                                <asp:CheckBox id="m_becomeIndependent" runat="server" Text="Become Independent Programs"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel" style="PADDING-RIGHT: 4px">Notification Email</td>
                            <td>
                                <asp:TextBox id="m_notifEmailTb" type="email" Width="220px" MaxLength="80" runat="server" Value="support@pricemyloan.com" required/>
                                <!-- <input type"email" id="m_notifEmailTb" style="width:220px" maxlength="80" value="support@pricemyloan.com" required> -->
                                <asp:RegularExpressionValidator id="notifEmailTbValidator" runat="server" Display="Dynamic" EnableClientScript="True" ErrorMessage="* Invalid Format" ControlToValidate="m_notifEmailTb" />
                                <asp:RequiredFieldValidator runat="server" Display="Dynamic" EnableClientScript="true" ErrorMessage="* Email is Required" ControlToValidate="m_notifEmailTb" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Job Label</td>
                            <td>
                                <asp:TextBox id="m_jobLabelTb" Width="220px" MaxLength="80" Runat="server"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">Job Notes</td>
                            <td>
                                <asp:TextBox id="m_jobNotesTb" Width="450px" MaxLength="1000" Runat="server" Rows="4" TextMode="MultiLine"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <TR>
                <TD align="center" height="1">
                    <button type="submit" style="width:100px">Create</button>
                    <button type="button" id="btnReferesh">Refresh</button>
                    <button type="button" onclick="onClosePopup();">Cancel</button>
                </TD>
            </TR>
        </TABLE>
    </FORM>

    <script>
        function onInit() {
            addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(notifEmailTbValidator) %>);

            var programTree = $("#programTree");
            if (typeof state != "undefined") {
                renderTree(state.tree);
            } else {
                alert("ERROR: 'state' is not defined");
                return;
            }

            $("#m_BrokerList").on("change", function(event){
                state.selectedBrokerId = event.currentTarget.value;
                fetchData(state.selectedBrokerId);
            });

            $("#btnRefresh").on("click", function(event) {
                fetchData(state.selectedBrokerId);
            });

            $("form").on("submit", function(event){
                event.preventDefault();

                if (state.tree.children.length < 1) {
                    alert("Nothing Checked. No job created");
                    return false;
                }

                var tree = programTree.dynatree("getTree");
                var nodes = tree.getSelectedNodes();
                var selectedIds = nodes.map(function(n){ return n.data.key });
                if (selectedIds.length < 1) {
                    alert("Nothing Checked. No job created");
                    return false;
                }
                var targetId = getParameterByName("targetId");
                if (!targetId || targetId == "root") targetId = "00000000-0000-0000-0000-000000000000";

                var enableAfterDerive      = document.getElementById("m_enableAfterDerive").checked;
                var independentAfterCreate = document.getElementById("m_becomeIndependent").checked;
                var jobPriority            = document.querySelector('input[name="m_priorityRbList"]:checked').value;
                var jobNotifyEmailAddr     = document.getElementById("m_notifEmailTb").value;
                var jobUserNotes           = document.getElementById("m_jobNotesTb").value;
                var jobUserLabel           = document.getElementById("m_jobLabelTb").value;

                $('form button[type="submit"]').prop("disabled", true);
                post("DoInsert", ({
                    selectedIds           : selectedIds           ,
                    targetId              : targetId              ,
                    selectedBrokerId      : state.selectedBrokerId,
                    enableAfterDerive     : enableAfterDerive     ,
                    independentAfterCreate: independentAfterCreate,
                    jobPriority           : jobPriority           ,
                    jobNotifyEmailAddr    : jobNotifyEmailAddr    ,
                    jobUserNotes          : jobUserNotes          ,
                    jobUserLabel          : jobUserLabel
                })).then(function(result){
                    $('form button[type="submit"]').prop("disabled", false);
                    if (!!result.errorMessage) {
                        alert(result.errorMessage);
                        return;
                    }
                    onClosePopup();
                }, function(){
                    $('form button[type="submit"]').prop("disabled", false);
                });
                return false;
            });

            function fetchData(selectedBrokerId) {
                post("LoadData", ({ selectedBrokerId: selectedBrokerId })).then(function(d) {
                    if (d.errorMessage != null) {
                        alert(d.errorMessage);
                        return;
                    }

                    state.tree = d.tree;
                    renderTree(state.tree);
                });
            }

            function post(op, data) {
                return callWebMethodAsync({
                    url:location.pathname+"/"+op,
                    data: JSON.stringify(data),
                    async: true
                }).then(function(data) { return data.d; });
            }

            function renderTree(tree) {
                try { programTree.dynatree("destroy"); } catch(e){ }

                if (tree.children.length < 1) {
                    programTree.html("<div class='empty-message'>Empty. No programs to display.</div>");
                    return;
                }

                var dTree = nodeToDynaNode(state.tree, 0);
                dTree.hideCheckbox = dTree.unselectable = true;

                programTree.dynatree({
                    checkbox:true,
                    selectMode: 2,
                    children: [dTree]
                });
            }

            function nodeToDynaNode(node, currentLevel) {
                if (node.children == null) return ({
                    key:node.Id, title:node.Name,
                    icon: node.IsEnabled ? "../../../../images/bullet.gif" : "../../../../images/disabled.gif"
                });

                node.children.sort(nodeCompare);
                return ({
                    key:node.Id, title:node.Name,
                    isFolder: true,
                    expand: currentLevel < 1,
                    children: node.children.map(function(node) {return nodeToDynaNode(node, currentLevel + 1)})
                });
            }

            function nodeCompare(a, b) {
                if (a.children != null && b.children == null) return -10;
                if (a.children == null && b.children != null) return  10;
                return stringCompare(a.Name, b.Name);
            }
            function stringCompare(a, b){
                var la = a.toLowerCase();
                var lb = b.toLowerCase();
                return (
                    (la < lb) ? -1 : (
                    (la > lb) ?  1 : (
                    ( a <  b) ? -1 : (
                    ( a >  b) ?  1 : (
                    0)))));
            }

            if (document.getElementById("m_errorMessage") != null) {
                alert(document.getElementById("m_errorMessage").value);
            }

            <% if (IsPostBack == false) { %>
                resize(800, 740);
            <% } %>
        }
    </script>
</body>
</html>
