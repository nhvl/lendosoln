﻿<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateSelector.aspx.cs" Inherits="LendersOfficeApp.los.Template.TemplateSelector" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        #BatchEdit { margin: 5px; }
    </style>
</head>
<body>
    <h4 class="page-header" id="HeaderContent">Select Programs to Batch Edit</h4>
    <form id="form1" runat="server">
    <input type="button" id="BatchEdit" value="Select programs..." />
    <div id="FolderNav">
    
    </div>
    </form>
    <script type="text/javascript">
        var cmd = <%= AspxTools.JsString(m_cmd) %>;        
        var viewIds = <%= AspxTools.JsArray(m_selectedProgramList) %>;
        var viewIdsMap = {};

        jQuery(function($){
            resizeForIE6And7(450, 700);

            if( cmd == 'derive')  {
                $('#HeaderContent').text("Select Programs to Batch Derive");
            } else if( cmd == 'productcode')  {
                $('#HeaderContent').text("Select non-derived Programs to modify their investor/product code.");
            } else if (cmd == 'view') {
                $('#HeaderContent').text(viewIds.length === 0 ? "Cached item expired. We can not review selected programs." : "View Selected Programs");
                $('#BatchEdit').hide();
            }

            if(NavNodes && NavNodes.length > 0) {
                NavNodes[0].expand = true; 
                if (cmd != 'edit') {
                    NavNodes[0].hideCheckbox = true;
                }
            }

            var navTree = $('#FolderNav');
            var inEventHandler = false;
            navTree.dynatree({
                debugLevel: 1,
                children : NavNodes,
                checkbox: true,
                onSelect: function (select, dtnode) {
                            if (cmd == 'derive') {
                                return;
                            }

                            if (cmd == 'view') {
                                dtnode.select(viewIdsMap.hasOwnProperty(dtnode.data.identifier));
                                return;
                            }

                            // Ignore, if this is a recursive call
                            if(inEventHandler) 
                                return;
                            // Select all children of currently selected node
                            try {
                                inEventHandler = true;
                                dtnode.visit(function(childNode){
                                    childNode.select(select);
                                });
                            } finally {
                                inEventHandler = false;
                            }
                        }
            });
            
            if (viewIds.length > 0) {
                viewIdsMap = {};
                for (var j = 0; j < viewIds.length; j++) {
                    viewIdsMap[viewIds[j]] = true;
                }

                navTree.dynatree('getRoot').visit(function (node) {
                    if (viewIdsMap.hasOwnProperty(node.data.identifier)) {
                        node.makeVisible();
                        node.select(true);
                    }
                });
            }
            
            $('#BatchEdit').click(function(){
                var selNodes = navTree.dynatree('getSelectedNodes');
                var programIds = [];
                $.each(selNodes, function(index, node){
                    if (!node.data.isFolder || cmd == 'derive') {
                        programIds.push(node.data.identifier);
                    }
                });
                
                if(programIds.length === 0)
                {
                    alert('No chosen programs.');
                    return;
                }
                    
	            callWebMethodAsync({
	                type: 'POST',
	                contentType: 'application/json; charset=utf-8',
	                url: 'TemplateSelector.aspx/CacheSelectedPrograms',
	                data: JSON.stringify({'programIds': programIds}),
	                dataType: 'json',
	                async: false,
	                success : function(d) {
	                    if (cmd == 'derive') {
	                        window.location = 'BatchDerivationJob.aspx?s='+ encodeURIComponent(d.d); // s means "selected programs/folders"
	                    } else if (cmd == 'productcode') {
	                        window.location = 'ProductCodeBatchEdit.aspx?s='+ encodeURIComponent(d.d); // s means "selected programs"
	                    } else {
	                        window.location = 'LpBatchEdit.aspx?k=' + encodeURIComponent(d.d);
	                    }
	                },
	                error :function() {
	                    alert('Error.');
	                }
	            });
            });
            
            
        });
    </script>
</body>
</html>
