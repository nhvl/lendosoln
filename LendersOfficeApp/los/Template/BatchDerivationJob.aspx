<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="LendersOffice.AntiXss"%>
<%@ Page language="c#" Codebehind="BatchDerivationJob.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.Template.BatchDerivationJob" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
    <title>Create Batch Derivation Jobs</title>
    <meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
    <meta content=C# name=CODE_LANGUAGE>
    <meta content=JavaScript name=vs_defaultClientScript>
    <meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><LINK href="../../css/stylesheet.css" type=text/css rel=stylesheet >
    <style type="text/css">
        .gray-color .dynatree-title {
            color: gray;
        }
        .hide-checkbox .dynatree-checkbox {
                display: none;
        }
    </style>
</HEAD>
<BODY scroll=no onload=onInit(); MS_POSITIONING="FlowLayout">
<script type="text/javascript">
    var org_oncheck;
    var gTreeNodeMap;
    var gRootNodeData;
    var gCachedKey = <%= AspxTools.JsString(SelectedPrgsCacheKey) %>;
    var gNeedInitIgnoreNodeMap = <%= AspxTools.JsBool(SameBroker) %>;
    var ignoreIds;    

	function onInit()
	{
	    if( document.getElementById("m_errorMessage") != null )
		{
	        alert( document.getElementById("m_errorMessage").value );
		}

	    if( document.getElementById("m_feedBack") != null )
		{
	        alert( document.getElementById("m_feedBack").value );
		}

	    if( document.getElementById("m_cmdToDo") != null )
		{
	        if( document.getElementById("m_cmdToDo").value == "Close" )
			{
				onClosePopup();
			}
		}

		<% if( IsPostBack == false ) { %>

		resize( 800 , 740 );

	    <% } %>
	}

    function hideChildrenCheckbox(children, select) {
        $j(children).map(function(index, node) {
            var nodeInfo = $j("#dynatree").dynatree("getTree").getNodeByKey(node.key) || node;
            $j(nodeInfo.span).find(".dynatree-checkbox").toggle(!select);
            nodeInfo.bSelected = false;
            var children = nodeInfo.getChildren();
            if (children && children.length > 0) {
                hideChildrenCheckbox(children, select);
            }
        });
    }

    function retrieveSelectedChild(children) {
        var nodeInfo;
        for (var i = 0; i < children.length; i++) {
            nodeInfo = $j("#dynatree").dynatree("getTree").getNodeByKey(children[i].key) || children[i];
            if (nodeInfo.isSelected()){
                return nodeInfo;
            }
            var nodeChildren = nodeInfo.getChildren();
            if (nodeChildren && nodeChildren.length > 0) {
                return retrieveSelectedChild(nodeChildren);
            }
        }
    }

    function selectNode(select, node) {
        var parent = node.getParent();
                
        while (parent != null) {
            $j(parent.span).find(".dynatree-checkbox").toggle(!select);
            parent.bSelected = false;
            parent = parent.getParent();
        }                                

        var children = node.getChildren();
        if (children && children.length > 0) {
            hideChildrenCheckbox(children, select);
        }

        // hide the checkbox on root
        $j($j("#dynatree").dynatree("getRoot").childList[0].span).find('.dynatree-checkbox').hide();
    }

    function _init() {
        addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(notifEmailTbValidator) %>);

        $j("#dynatree").dynatree({
            minExpandLevel: 2,
            checkbox: true,
            onSelect: function(select, node){
                selectNode(select, node);
                var ids = [];
                selectedNodes = $j("#dynatree").dynatree("getSelectedNodes");
                $j.map(selectedNodes, function(node){
                    ids.push(node.data.key);
                })
                document.getElementById("m_targetFolderIds").value = ids.join(";");
            },
            onExpand: function(flag, node) {
                // find if a node is selected within this expanded node's parents & descendants
                // if it is, "select" the node to have the same folder-hiding behavior.
                var selectedNode;

                if (!node.isSelected()) {
                    var parent = node.getParent();
                    while (parent != null) {
                        if (parent.isSelected()) {
                            selectedNode = parent;
                            break;
                        }
                        parent = parent.getParent();
                    }

                    if (!selectedNode) {
                        var children = node.getChildren();
                        if (children && children.length > 0) {
                            selectedNode = retrieveSelectedChild(children);
                        }
                    }                    
                }        
                else {
                    selectedNode = node;
                }

                if (selectedNode && selectedNode.isSelected()) {
                    selectNode(true, selectedNode);
                }

                // if there are nodes to be ignored/disabled, disable them.
                if (gNeedInitIgnoreNodeMap) {
                    disableIgnoreNodes();
                }
            },
            children: nodes
        });

        // hide the checkbox on root
        $j($j("#dynatree").dynatree("getRoot").childList[0].span).find('.dynatree-checkbox').hide();

        $j("#dynatree").toggle(nodes.children.length !== 0);
        $j("#m_Empty").toggle(nodes.children.length === 0);

        if (gNeedInitIgnoreNodeMap) {
            gIgnoreNodeMap = {};
            var ignoreIds = document.getElementById("m_ignoreTargetFolderIds").value.split(";");
            for (var j = 0; j < ignoreIds.length; j++) {
                gIgnoreNodeMap[ignoreIds[j]] = true;
            }
        }

        // set checked nodes
        var ids = [];
        selectedNodes = $j("#dynatree").dynatree("getSelectedNodes");
        $j.map(selectedNodes, function(node){
            ids.push(node.data.key);
        });        

        document.getElementById("m_targetFolderIds").value = ids.join(";");

        if (gNeedInitIgnoreNodeMap) {
            disableIgnoreNodes();
        }
    }

    function disableIgnoreNodes() {
        if (typeof ignoreIds == "undefined") {
            ignoreIds = document.getElementById("m_ignoreTargetFolderIds").value.split(";");
        }

        $j(ignoreIds).map(function(index, key){
            var ignoreNode = $j("#dynatree").dynatree("getTree").getNodeByKey(key);
            $j(ignoreNode.span).find(".dynatree-checkbox").hide();
            ignoreNode.bSelected = false;
            $j(ignoreNode).prop('disabled', true);
            $j(ignoreNode.li).prop('disabled', true);
        });
    }
</script>

<FORM id=InsertExternalProduct method=post runat="server">
<TABLE height="100%" cellSpacing=8 cellPadding=0 width="100%" border=0>
  <TR>
    <TD height=1>
      <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0
      >
        <TR>
          <TD align=left><ml:EncodedLiteral id=m_Status runat="server" EnableViewState="False"></ml:EncodedLiteral></TD>
          <TD align=right><ASP:DROPDOWNLIST id=m_BrokerList runat="server" AutoPostBack="True"></ASP:DROPDOWNLIST></TD></TR></TABLE></TD></TR>
  <TR>
    <TD height="100%">
		<ASP:PANEL id=m_Base runat="server" height=450 style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 4px; BORDER-TOP: 2px groove; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; BACKGROUND: whitesmoke; PADDING-BOTTOM: 4px; BORDER-LEFT: 2px groove; PADDING-TOP: 4px; BORDER-BOTTOM: 2px groove">
            <div id="dynatree">
            </div>
            <ASP:Panel id=m_Empty style="PADDING-RIGHT: 100px; PADDING-LEFT: 100px; PADDING-BOTTOM: 100px; COLOR: dimgray; PADDING-TOP: 100px; TEXT-ALIGN: center" runat="server" EnableViewState="False" Visible="True">Empty. No programs to display. 
            </ASP:Panel>
		</ASP:PANEL>
    </TD>
  </TR>
  <tr>
    <td>
      <table cellSpacing=0 cellPadding=0 border=0>
      <tr><td colspan="2"><span style="color:Red"><ml:EncodedLiteral ID="timeWarning" runat="server" Visible="false" /></span></td></tr>
        <tr>
          <td class=FieldLabel>Job Priority </td>
          <td><asp:radiobuttonlist id=m_priorityRbList runat="server" RepeatDirection="Horizontal">
				      <asp:ListItem Value="Low" />
				      <asp:ListItem Value="Normal" Selected="True" />
				      <asp:ListItem Value="High" />
				    </asp:radiobuttonlist></td></tr>
        <tr>
          <td></td>
          <td class=FieldLabel><asp:checkbox id=m_enableAfterDerive runat="server" Text="Enable Derived Programs"></asp:checkbox></td></tr>
        <tr>
          <td class=FieldLabel style="PADDING-RIGHT: 4px"
            >Notification Email </td>
          <td><asp:textbox id=m_notifEmailTb Width="220px" MaxLength="80" Text="support@pricemyloan.com" Runat="server" />
          <asp:regularexpressionvalidator id=notifEmailTbValidator Runat="server" Display="Dynamic" EnableClientScript="True" ErrorMessage="* Invalid Format" ControlToValidate="m_notifEmailTb" />
          <asp:RequiredFieldValidator id="notifEmailTbReqValidator" runat="server" Display="Dynamic" EnableClientScript="true" ErrorMessage="* Email is Required" ControlToValidate="m_notifEmailTb" />
          </td></tr>
        <tr>
          <td class=FieldLabel>Job Label </td>
          <td><asp:textbox id=m_jobLabelTb Width="220px" MaxLength="80" Runat="server"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel>Job Notes </td>
          <td><asp:textbox id=m_jobNotesTb Width="450px" MaxLength="1000" Runat="server" Rows="4" TextMode="MultiLine"></asp:textbox></td></tr>
        <tr>
          <td class=FieldLabel>Schedule Derivation for </td>
          <td><ml:TimeTextBox id="m_scheduleTime" runat="server"></ml:TimeTextBox></td></tr>
      </table></td></tr>
  <TR>
    <TD align=center height=1><SPAN><ASP:BUTTON id=m_Insert onclick=InsertClick runat="server" Text="Batch Derivation" Width="100px"></ASP:BUTTON></SPAN><ASP:BUTTON id=m_Refresh runat="server" Text="Refresh"></ASP:BUTTON><INPUT onclick=onClosePopup(); type=button value=Cancel>
        <INPUT id="m_viewSelectedPrgs"  type="button" value="View Selected Paths" onclick="showModal('/los/Template/TemplateSelector.aspx?cmd=view&s='+gCachedKey);">
    </TD></TR></TABLE>

    <input type="hidden" id="m_targetFolderIds" runat="server" value="none" enableviewstate="false" />
    <input type="hidden" id="m_ignoreTargetFolderIds" runat="server" value="none" enableviewstate="false" />
</FORM>
	</BODY>
</HTML>
