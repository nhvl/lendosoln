﻿<%@ Page     Language="C#" AutoEventWireup="true" CodeBehind="UploadProfileImage.aspx.cs" Inherits="LendersOfficeApp.los.UploadProfileImage" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Image Uploader</title>
    <link href="../css/stylesheet.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
    #EmptyImageHolder { margin:0 auto; width:128px; height:128px; text-align:center; vertical-align:middle; color:#999; font-style:italic; font-size:11px; border:dashed 1px #AAA; }
    .marginTop { margin-top: 5px; }
    .hide { display: none; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width:100%; text-align:center;">
        <asp:FileUpload ID="UploadImgControl" Width="320" ToolTip="Select the image to upload" runat="server" />
        <input type="button" value="Upload" onclick="f_uploadFunc(this);" />
        <asp:Button runat="server" id="UploadButton" CssClass="hide" text="Upload" OnClientClick="return f_IsValidFile();" onclick="UploadButton_Click" /><br />
        <ml:EncodedLabel ID="StatusLabel" runat="server"></ml:EncodedLabel><br />
        <asp:Image ID="ProfilePic" Width="128" Height="128" runat="server" /><br />
        <div id="EmptyImageHolder" runat="server"><p style="line-height:9em;">no image</p></div>
        <div>Current Profile Picture</div>
        <asp:Button runat="server" id="RemoveButton" CssClass="marginTop" text="Remove" onclick="RemoveButton_Click" />
    </div>
    </form>
    <script type="text/javascript">
    <!--
        function f_uploadFunc(o){
            var file = <%= AspxTools.JsGetElementById(UploadImgControl) %>;
            var btn = <%= AspxTools.JsGetElementById(UploadButton) %>;
            try {
                if (f_IsValidFile()){
                    o.disabled = true;
                    btn.click();
                }
                else{
                    alert("Please select a valid file to upload.");
                    file.focus();
                }
            }catch(e){ return false; }
        }
        
        function f_IsValidFile() {
            try {
                  var o = <%= AspxTools.JsGetElementById(UploadImgControl) %>;
                  if (o.value != ""){
                    var IsValid = ( o.value.match(/\.(png|jpg|gif|jpeg|bmp)$/i) !== null);
                    return IsValid;
                  }
                  return false;
            }catch(e){ return false; }
        }
    
    //-->   
    </script>
</body>
</html>
