﻿using System;
using System.Text;
using System.Web.UI;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.los
{
    public partial class UploadProfileImage : LendersOffice.Common.BaseServicePage
    {
        private readonly int MAX_IMG_SIZE = 500*1024; //500KB

        private AbstractUserPrincipal CurrentUser
        {
            get
            {
                return Page.User as AbstractUserPrincipal;
            }
        }

        private string FileDBKey
        {
            get
            {
                if (CurrentUser != null)
                    return CurrentUser.EmployeeId.ToString() + ".profilepic.jpg";
                return null;
            }
        }

        private bool m_userHasImage = false;
        private bool m_userHasImageSet = false;
        
        private bool UserHasImage
        {
            get
            {
                if (!m_userHasImageSet)
                {
                    this.m_userHasImage = FileDBTools.DoesFileExist(E_FileDB.Normal, this.FileDBKey);
                    this.m_userHasImageSet = true;
                }

                return m_userHasImage;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string imageId = RequestHelper.GetSafeQueryString("ImageId");

            if (!Page.IsPostBack && imageId != null)
            {
                byte[] image = GetImageFromFileDB(FileDBKey);
                
                Response.Clear();
                Response.ContentType = "image/jpeg";
                Response.BinaryWrite(image);
                Response.End();
            }
            
            ProfilePic.Visible = UserHasImage;
            RemoveButton.Visible = UserHasImage;
            ProfilePic.ImageUrl = "./UploadProfileImage.aspx?ImageId=load";
            
            EmptyImageHolder.Visible = !UserHasImage;
        }
        
        protected byte[] GetImageFromFileDB(string key)
        {
            byte[] image = FileDBTools.ReadData(E_FileDB.Normal, key);

            if (image.Length > MAX_IMG_SIZE)
            {
                throw new Exception("Image size too big. Maximum allowed size is 500KB. Found size: " + image.Length + " bytes.");
            }

            return image;
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (UploadImgControl.HasFile)
            {
                try
                {
                    if (!ImageUtils.IsValidImageType(UploadImgControl.PostedFile.ContentType))
                    {
                        StatusLabel.Text = "Upload status: Only valid image files (BMP/GIF/PNG/JPG/JPEG) are accepted.";
                        return;
                    }
                    if (UploadImgControl.PostedFile.ContentLength > MAX_IMG_SIZE)
                    {
                        StatusLabel.Text = "Upload status: The file has to be less than 500 KB.";
                        return;
                    }

                    System.Drawing.Image im = ImageUtils.ResizeImage(UploadImgControl.PostedFile.InputStream, 128, 128);

                    if (ImageUtils.SaveImageToFileDB(FileDBKey, im, System.Drawing.Imaging.ImageFormat.Jpeg))
                    {
                        StatusLabel.Text = "Upload status: File successfully uploaded!";
                    }
                    else
                    {
                        StatusLabel.Text = "Upload status: Unable to save file. Please try again.";
                        return;
                    }
                }
                catch (Exception ex)
                {
                    StatusLabel.Text = "Upload status: The image file could not be uploaded. Please try again.";
                    Tools.LogError("Error uploading profile picture image file.", ex);
                    try
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("Uploader: " + CurrentUser.LoginNm + " (employeeid='" + CurrentUser.EmployeeId + "', brokerid='" + CurrentUser.BrokerId + "')");
                        sb.AppendLine("File name: " + UploadImgControl.PostedFile.FileName);
                        sb.AppendLine("Content type: " + UploadImgControl.PostedFile.ContentType);
                        sb.AppendLine("Content length: " + UploadImgControl.PostedFile.ContentLength);                        
                        Tools.LogError(sb.ToString());
                    }catch(Exception){}
                }

                Response.Redirect("./UploadProfileImage.aspx", false);
            }
        }


        protected void RemoveButton_Click(object sender, EventArgs e)
        {
            if (UserHasImage)
            {
                FileDBTools.Delete(E_FileDB.Normal, this.FileDBKey);
                Response.Redirect("./UploadProfileImage.aspx");
            }
        }
    }
}