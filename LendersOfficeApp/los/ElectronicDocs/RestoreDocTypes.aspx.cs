﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EDocs;
using LendersOffice.Security;
using LendersOffice.Common;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class RestoreDocTypes : BaseServicePage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingEdocsConfiguration
                };
            }
        }

        private Dictionary<int, EDocumentFolder> m_folderNames;

        protected void Page_Load(object sender, EventArgs e)
        {
            m_folderNames = EDocumentFolder.GetFoldersInBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId).ToDictionary(folder => folder.FolderId);
            DeletedDocTypes.DataSource = DocType.GetDeletedDocTypes(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            DeletedDocTypes.DataBind();

            EnableJqueryMigrate = false;
            RegisterJsScript("jquery.tablesorter.min.js");
            RegisterService("main", "/los/ElectronicDocs/ElectronicDocsService.aspx");
        }


        protected void DeletedDocType_OnDataBound(object sender, RepeaterItemEventArgs args)
        {

            DocType docType = args.Item.DataItem as DocType;
            if (docType == null)
            {
                return;
            }

            EncodedLiteral folder = args.Item.FindControl("Folder") as EncodedLiteral;
            EncodedLiteral docTypeName = args.Item.FindControl("DocType") as EncodedLiteral;
            HtmlAnchor anchor = args.Item.FindControl("RestoreLink") as HtmlAnchor;

            folder.Text = docType.Folder.FolderNm;
            docTypeName.Text = docType.DocTypeName;

            anchor.Attributes.Add("docTypeId", docType.DocTypeId.ToString());

            

            
        }
    }
}
