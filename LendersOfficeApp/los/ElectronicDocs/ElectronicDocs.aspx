﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ElectronicDocs.aspx.cs" Inherits="LendersOfficeApp.los.ElectronicDocs.ElectronicDocs" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc1" TagName="DocTypePickerLink" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx" %>
<%@ Register TagPrefix="so" TagName="StackingOrderUserControl" Src="StackingOrder.ascx" %>
<%@ Register TagPrefix="so" TagName="Autosave" Src="Autosave.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >

    <title>Electronic Docs</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet" />
	<style type="text/css">
	    table a { text-decoration: underline; cursor: hand; }
        .button {    padding:0 1em 0 1em;width:auto;overflow:visible; }
.no-close .ui-dialog-titlebar-close {display: none }
.no-close .ui-dialog-titlebar {display:none}
.no-close p { font-weight: bold; font-size: 12px; }
        tr.GridItem:hover, tr.GridAlternatingItem:hover {
            background-color: #DCDCFF;
        }
        #m_dgDocTypes {
            width: 100%;
        }
        .ConfirmationContent {
            font-size: 12px;
            font-weight: bold;
            width: 75%;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
        }
        .ConfirmationButtons {
            position:absolute;
            bottom: 0px;
            width: 100%;
            text-align: center;
            margin-bottom: 5px;
        }
        .PageTopButtons {
            display: flex;
            justify-content: space-between;
        }
        .PageBottomButtons {
            display: flex;
            justify-content: flex-end;
        }
	</style>
</head>
<body>
    <script>
        window.onbeforeunload = function (e) {
            if ($('#__EVENTTARGET').val() == '' && isDirty && isDirty()) {
                var dialogText = "There are unsaved changes on the page that will be lost if you continue. Are you sure you want to leave?";
                e.returnValue = dialogText;
                return dialogText;
            }
        };
        function a() { return confirm("All Doc Types in the this folder will be automatically\nassigned to the UNCLASSIFIED folder. Continue?" ); }

        var changedDocTypeMaps = {};
        function _init() {
            $('.SelectedDocTypeId').change(function(event) {
                var sourceDocTypeId = $(event.target).closest('td').find('.TargetDocTypePickerContainer').data('docTypeId');
                updateDirtyBit(event);
                changedDocTypeMaps[sourceDocTypeId] = true;
                $('.SaveDocTypes').prop('disabled', !isDirty());
            });
            $('.SaveDocTypes').click(saveDoctypeMappings).prop('disabled', !isDirty());
        }

        function saveDoctypeMappings(callback) {
                $('.SaveDocTypes').prop('disabled', true).val('Saving...');
                var docTypeArray = [];

                $('.SelectedDocTypeId').each(function(index, element){
                    var sourceDocTypeId = $(element).closest('td').find('.TargetDocTypePickerContainer').data('docTypeId');
                    if (element.value && !changedDocTypeMaps[sourceDocTypeId])
                        return;
                    docTypeArray.push({
                        SourceDocTypeId : parseInt(sourceDocTypeId) || null,
                        ESignDocTypeId : element.value
                    });
                });

                var args = { docTypeArray: JSON.stringify(docTypeArray) };

                gService.main.callAsyncSimple("SaveDocTypeMapping", args, function(result) {
                    docTypeMappingCallback(result);
                    if (typeof(callback) == 'function') {
                        callback(result);
                    }
                });
        }

        function docTypeMappingCallback(result) {
            if (result.error) {
                alert(result.UserMessage ? result.UserMessage : "System Error. Please try refreshing the page.");
            }
            else if (result.value.Status === "Error" && result.value.UserMessage) {
                alert(result.value.UserMessage);
            }
            else if (result.value.Status === "Success") {
                clearDirty();
                changedDocTypeMaps = {};
                $('.SaveDocTypes').prop('disabled', !isDirty()).val('Save Page');
            }
        }

        function promptToSave(action) {
            if (isDirty()) {
                var settings = {
                    onReturn: function(args) {
                        if (args.Save) {
                            saveDoctypeMappings(args.Continue ? action : null);
                        }
                        else {
                            changedDocTypeMaps = {};
                            clearDirty();
                            $('.SaveDocTypes').prop('disabled', !isDirty()).val('Save Page');
                            if (args.Continue) {
                                action();
                            }
                        }
                    },
                    height: 100,
                    elementClasses: "BackgroundGainsboro"
                };
                LQBPopup.ShowElement($('#ConfirmDialog'), settings);
            }
            else {
                action();
            }
        }

        function onAddDocType(id) {
            var openAddDocTypePopup = function() {
                showModal("/los/ElectronicDocs/EditDocType.aspx", null, null, null, function(){
                    location.href = location.href + ( location.href.indexOf('?') > 0  ? '&' : '?' ) + "r=" + (new Date());
                    location.reload(true);
                }, { hideCloseButton:true });
            };

            promptToSave(openAddDocTypePopup);
        }

        function onEditDocType(id) {
            var openEditDocTypePopup = function() {
                  showModal("/los/ElectronicDocs/EditDocType.aspx?did="+id, null, null, null, function(){
                    __doPostBack('', '');
                    }, { hideCloseButton:true });
            }

            promptToSave(openEditDocTypePopup);
        }

        function onDeleteDocType(element, id){
            var data = {
                'DocTypeId' : id
            };

            var results = gService.main.call('CheckTemplatesInDocType', data );

            if( results.error ) {
                alert(results.UserMessage);
                return false;
            }

            if( results.value.HasTemplates  === 'False' ) {

                if( false == confirm('Delete doc type? Any documents assigned to this doc will be retained.') ) {
                    return;
                }
                results = gService.main.call('DeleteDocType', data );
                if( results.error  ) {
                    alert(results.UserMessage);
                }
                else {
                    $(element).closest('tr').hide();
                    window.location.href = window.location.href;
                }
                return false;
            }

            var templates = JSON.parse( results.value.TemplateNames );

            var message = 'This doc type is currently used in the following shipping templates:\r\n';
            var names = templates.join('\r\n   ');
            var ending = '\r\n\r\nThe doc type must be removed from these templates before it can be deleted.';

            alert(message + '   ' + names + ending);

            return false;

          }

        function onDeleteFolder(id) {
            if(a() == false ){
                return;
            }

            document.getElementById("m_hiddenId").value = id;
            document.getElementById("m_hiddenCmd").value = "deleteFolder";
            __doPostBack('', '');
        }

        function onAddShippingTemplate() {
            Modal.Hide();
          showModal('/los/ElectronicDocs/EditShippingTemplate.aspx', null, null, null, function(){
                __doPostBack('', '');
            }, { hideCloseButton:true });
        }

        function onEditFolder(id){
            Modal.Hide();
            showModal('/los/ElectronicDocs/EditFolder.aspx?fid=' + id, null, null, null, function() {
                location.reload();
            }, { hideCloseButton:true });
        }

        function onAddNewFolder(id){
            Modal.Hide();
          showModal('/los/ElectronicDocs/EditFolder.aspx', null, null, null, function(){
                __doPostBack('', '');
            }, { hideCloseButton:true });
        }

        function onEditShippingTemplate(id) {
            Modal.Hide();
          showModal('/los/ElectronicDocs/EditShippingTemplate.aspx?id=' + id, null, null, null, function(){
                __doPostBack('', '');
            }, { hideCloseButton:true });
        }

        function onDeleteShippingTemplate(id) {
            Modal.Hide();
            if (confirm("Delete Shipping Template?") == false)
                return;

            document.getElementById("m_hiddenId").value = id;
            document.getElementById("m_hiddenCmd").value = "deleteShippingTemplate";
            __doPostBack('', '');
        }

        function RestoreDocsOnClick() {
            var callback = function(args){
                if(!args || args.reload) {
                        location.href = location.href + ( location.href.indexOf('?') > 0  ? '&' : '?' ) + "r=" + (new Date());
                    location.reload(true);
                }
                else {
                    return false;
                }
            };
            var restoreDocs = function() { showModal('/los/ElectronicDocs/RestoreDocTypes.aspx', null, null, null, callback, { hideCloseButton:true, onClose: callback }); };
            promptToSave(restoreDocs);
        }
        function setStackOrder() {
            var args = new Object();
            args["order"] = getOrder();  // from StackOrder.ascx

            var res = gService.main.call("SetStackOrder", args );
            if (!res.error) {
                if ( res.value.invalidOp == 'True' )
                {
                    <%= AspxTools.JsGetElementById(m_stackOrder_errorLbl) %>.innerText = res.value.userMsg;
                }
            }
            else
            {
                alert(res.UserMessage);
            }
            document.getElementById('stackOrderApplyButton').disabled = 'disabled';
        }
        function stackOrderChanged()
        {
            document.getElementById('stackOrderApplyButton').disabled = '';
        }
    </script>
    <h4 class="page-header">Configure EDoc System</h4>
    <div id="ConfirmDialog" class="Hidden">
        <div class="MainRightHeader">Confirm Changes</div>
        <div class="ConfirmationContent">There are unsaved changes on this page that may be lost if you continue. Are you sure you want to leave?</div>
        <div class="ConfirmationButtons">
            <input type="button" class="ButtonStyle button" value="Save Changes and Continue" onclick="LQBPopup.Return({ Continue: true, Save: true });" /> 
            <input type="button" class="ButtonStyle button" value="Continue Without Saving" onclick="LQBPopup.Return({ Continue: true, Save: false });" />
            <input type="button" class="ButtonStyle button" value="Cancel" onclick="LQBPopup.Return({ Continue: false, Save: false });" />
        </div>
    </div>
    <form id="form1" runat="server">
		<input type="hidden" value="" runat="server" id="m_hiddenCmd" name="m_hiddenCmd"/>
		<input type="hidden" value="" runat="server" id="m_hiddenId" name="m_hiddenId"/>
		<table cellspacing="2" cellpadding="3" width="100%" border="0">
			<tr>
				<td>
				    <table cellSpacing="0" cellPadding="0" border="0" width="100%" >
				        <tr>
				            <td vAlign="bottom">
                                <div class="Tabs">
                                    <ul class="tabnav">
                                        <li runat="server" id="folderTab"><a href="ElectronicDocs.aspx?pg=0" >Folders</a></li>
                                        <li  runat="server" id="docTypeTab"><a href="ElectronicDocs.aspx?pg=1" >Doc Types</a></li>
                                        <li runat="server" id="stackOrderTab"><a href="ElectronicDocs.aspx?pg=2">Stack Order</a></li>
                                        <li runat="server" id="shipTemplateTab"><a href="ElectronicDocs.aspx?pg=3" >Shipping Templates</a></li>
                                        <li runat="server" id="autosaveTab"><a href="ElectronicDocs.aspx?pg=4" >Document AutoSave Options</a></li>
                                    </ul>
                                </div>
						    </td>
				        </tr>
				    </table>
					<table id="m_tblFolders" runat="server" cellSpacing="0" cellPadding="0" border="0" width="100%"   style="BORDER-TOP-WIDTH: 2px; BORDER-RIGHT: 2px outset; PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset; PADDING-TOP: 5px; BORDER-BOTTOM: 2px outset" vAlign="top">
				        <tbody>
				            <tr>
				                <td>
						            <input type="button" value="Add" class="ButtonStyle button" onclick="onAddNewFolder()" style="margin: 10px 0px" />
				                </td>
				            </tr>
				            <tr>
				                <td>
				                    <asp:DataGrid runat="server" id="m_dgFolders" OnItemDataBound="OnDataBound_m_dgFolders" Width="95%"  AutoGenerateColumns="false">
				                        <AlternatingItemStyle CssClass="GridAlternatingItem" />
				                        <ItemStyle CssClass="GridItem" />
				                        <HeaderStyle CssClass="GridHeader" />
				                        <Columns>
				                            <asp:TemplateColumn HeaderText="Folder">
				                                <ItemTemplate>
				                                    <ml:EncodedLiteral runat="server" id="FolderName"></ml:EncodedLiteral>
				                                </ItemTemplate>
				                            </asp:TemplateColumn>
				                            <asp:TemplateColumn HeaderText="Roles w/ Access">
				                                <ItemTemplate>
				                                    <ml:EncodedLiteral runat="server" ID="RoleAccess"></ml:EncodedLiteral>
				                                </ItemTemplate>
				                            </asp:TemplateColumn>
				                            <asp:TemplateColumn>
				                                <ItemTemplate>
				                                    <a runat="server" id="EditFolder" >edit</a>
				                                </ItemTemplate>
				                            </asp:TemplateColumn>
				                            <asp:TemplateColumn>
				                                <ItemTemplate>
				                                    <a runat="server" id="DeleteFolder" >delete</a>
				                                </ItemTemplate>
				                            </asp:TemplateColumn>
				                        </Columns>
				                    </asp:DataGrid>
				                </td>
				            </tr>
				        </tbody>
				    </table>
					<table id="m_tblDocTypes" runat="server" cellSpacing="0" cellPadding="0" border="0" width="100%" style="BORDER-TOP-WIDTH: 2px; BORDER-RIGHT: 2px outset; PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset; PADDING-TOP: 5px; BORDER-BOTTOM: 2px outset" vAlign="top">

						<tr>
						    <td>
                                <div class="PageTopButtons">
                                    <input type="button" value="Add" onclick="return onAddDocType();" class="ButtonStyle button" style="margin: 10px 0px" />
                                    <input type="button" value="Save Page" class="ButtonStyle button SaveDocTypes" runat="server" disabled="disabled" id="m_savePageTop" style="margin: 10px 0px" />
                                </div>
						    </td>
						</tr>
						<tr>
							<td>
							    <asp:DataGrid runat="server" id="m_dgDocTypes" AutoGenerateColumns="false" OnSortCommand="CustomDocTypes_OnSort" AllowSorting ="true"
							      OnItemDataBound="OnDataBound_m_dgDocTypes">
									<itemstyle cssclass="GridAutoItem"></itemstyle>
									<headerstyle cssclass="GridHeader"></headerstyle>
									<columns>
									    <asp:TemplateColumn HeaderText="Folder" SortExpression="FolderName"  >
									        <ItemTemplate>
									            <%# AspxTools.HtmlString( DataBinder.Eval(Container.DataItem, "Folder.FolderNm").ToString() ) %>
									        </ItemTemplate>
									    </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="DocTypeName" HeaderText="Doc Type" SortExpression="DocTypeName"  />
                                        <asp:TemplateColumn HeaderText="Assigned DocMagic Documents" SortExpression="AssignedDocMagicDocuments">
                                            <ItemTemplate>
                                                <ml:EncodedLiteral runat="server" id="assignedDocMagicDocTypesCellLiteral"></ml:EncodedLiteral>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Assigned DocuTech Documents" SortExpression="AssignedDocuTechDocuments">
                                            <ItemTemplate>
                                                <ml:EncodedLiteral runat="server" ID="assignedDocuTechDocTypesCellLiteral"></ml:EncodedLiteral>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Assigned IDS Documents" SortExpression="AssignedIDSDocuments">
                                            <ItemTemplate>
                                                <ml:EncodedLiteral runat="server" ID="assignedIDSDocTypesCellLiteral"></ml:EncodedLiteral>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="ESigned Document Target DocType" SortExpression="ESignTargetDocType" ItemStyle-Width="130px">
                                            <ItemTemplate>
                                                <div data-doc-type-id="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocTypeId").ToString())%>" class="TargetDocTypePickerContainer" ></div>
                                                <uc1:DocTypePickerLink runat="server" ID="esignTargetDocType" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn ItemStyle-Width="65px" >
                                            <ItemTemplate>
                                                <a href="#" onclick="onEditDocType(<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocTypeId").ToString())%>);">edit</a>
                                                &nbsp;&nbsp;
                                                <a href="#" onclick="return onDeleteDocType(this,<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DocTypeId").ToString())%>);">delete</a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
									</columns>
			                    </asp:DataGrid>
                                <div class="PageBottomButtons">
                                    <input type="button" class="ButtonStyle button SaveDocTypes" runat="server" id="m_savePageBottom" disabled="disabled" value="Save Page" style="margin-right: 10px;" />
                                    <input type="button" class="ButtonStyle button" runat="server" id="m_restoreDocs"  onclick="return RestoreDocsOnClick()" value="Restore Deleted Doc Types" />
			                    </div>
							</td>
						</tr>

					</table>
				    <table id="m_tblStackOrder" runat="server" cellSpacing="0" cellPadding="0" border="0" width="100%"   style="BORDER-TOP-WIDTH: 2px; BORDER-RIGHT: 2px outset; PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset; PADDING-TOP: 5px; BORDER-BOTTOM: 2px outset" vAlign="top">
				        <tbody>
				            <tr>
				                <td>
				                <div style="height:500px; overflow:scroll">
				                    <so:StackingOrderUserControl id="m_StackingOrder" runat="server" Width="100%" UseRemoveLink="false">
					                </so:StackingOrderUserControl>
					            </div>
				                </td>
				            </tr>
				            <tr>
				                <td>
                                    <ml:EncodedLabel ID="m_stackOrder_errorLbl" runat="server" ForeColor="Red"></ml:EncodedLabel>
                                </td>
				            </tr>
				            <tr>
				                <td align="center">
				                <asp:Button id="m_StackOrderOKButton" runat="server" class="ButtonStyle button" Text="OK" OnClientClick="setStackOrder(); onClosePopup(); return false;"></asp:Button>

				                <input type="button" class="ButtonStyle button" value="Cancel" onclick="onClosePopup();" />

				                <input type="button" class="ButtonStyle button" value="Apply" onclick="setStackOrder(); return false;" disabled="disabled" id="stackOrderApplyButton"/>
				                </td>

				            </tr>
				        </tbody>
				    </table>
				    <table id="m_tblShippingTemplates" runat="server" cellSpacing="0" cellPadding="0" border="0" width="100%" style="BORDER-TOP-WIDTH: 2px; BORDER-RIGHT: 2px outset; PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset; PADDING-TOP: 5px; BORDER-BOTTOM: 2px outset" vAlign="top">

						<tr>
						    <td>
						        <input type="button" class="ButtonStyle button" style="margin: 10px 0" value="Add" onclick="onAddShippingTemplate();"  />
						    </td>
						</tr>

						<tr>
							<td>
								<ml:CommonDataGrid id="m_dgCustomTemplates" runat="server">
									<alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
									<itemstyle cssclass="GridItem"></itemstyle>
									<headerstyle cssclass="GridHeader"></headerstyle>
								    <columns>
								        <asp:TemplateColumn ItemStyle-Width="80%" >
								            <ItemTemplate>
								               <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ShippingTemplateName").ToString()) %>
								            </ItemTemplate>
								        </asp:TemplateColumn>
										<asp:TemplateColumn ItemStyle-Width="10%">
											<itemtemplate>
												<a href="#" onclick="onEditShippingTemplate(<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ShippingTemplateId").ToString())%>);">edit</a>
											</itemtemplate>
										</asp:TemplateColumn>
									    <asp:TemplateColumn ItemStyle-Width="10%">
											<itemtemplate>
												<a href="#" onclick="onDeleteShippingTemplate(<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ShippingTemplateId").ToString())%>);">delete</a>
											</itemtemplate>
										</asp:TemplateColumn>
									</columns>
								</ml:CommonDataGrid>
							</td>
						</tr>

                    </table>
                    <asp:PlaceHolder id="m_Autosave" runat="server">
		                    <so:Autosave id="StackingOrderUserControl1" runat="server">
			                </so:Autosave>
                    </asp:PlaceHolder>
				</td>
			</tr>
			<tr>
			    <td align="right">
			        <input type="button" value="Close" class="ButtonStyle button" onclick="onClosePopup();" />
			    </td>
			</tr>
		</table>


		<div id="m_StandardTemplateCopyNameDiv" style="z-index: 900; font-weight:normal; BORDER-RIGHT: black 3px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 3px solid; DISPLAY: none; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 3px solid; PADDING-TOP: 5px; BORDER-BOTTOM: black 3px solid; POSITION: absolute; BACKGROUND-COLOR: whitesmoke">
            <table>
                <tr>
                    <td>
                        <strong>New template name: </strong>
                    </td>
                </tr>
                <tr>
                    <td noWrap>
                        <asp:TextBox runat="server" id="m_StandardTemplateCopyName"></asp:TextBox>
                        <img id="m_StandardTemplateCopyNameR" src="../../images/error_icon.gif" style="display:none" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <ml:EncodedLabel ID="m_lblError" runat="server" ForeColor="Red"></ml:EncodedLabel>
                    </td>
                </tr>
                <tr>
                    <td noWrap>
                        <input type="button" value="Copy" onclick="performCopyShippingTemplate();" />
                        <input type="button" value="Cancel" onclick="Modal.Hide();" />
                    </td>
                </tr>
            </table>
        </div>


		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
        <script type="text/javascript" src="../../inc/utilities.js" ></script>
    </form>
</body>
</html>
