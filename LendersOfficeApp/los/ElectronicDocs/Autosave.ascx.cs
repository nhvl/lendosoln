﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;
using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
using LendersOffice.Common;
using LendersOfficeApp.newlos.ElectronicDocs;
using System.Web.UI.HtmlControls;
using DataAccess;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class Autosave : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var basePage = Page as BasePage;
            if (basePage == null) throw new Exception("Autosave can only be used on pages that extend BasePage");

            basePage.RegisterJsScript("jquery.tablesorter.min.js");

            if (!Page.IsPostBack)
            {
                data.DataSource = AutoSaveDocTypeFactory.GetDefaultFolders(BrokerUserPrincipal.CurrentPrincipal.BrokerId, E_EnforceFolderPermissions.True);
                data.DataBind();
            }
        }

        protected void BindDocTypes(object sender, RepeaterItemEventArgs e)
        {
            var picker = e.Item.Controls.OfType<DocTypePickerControl>().FirstOrDefault();
            var checkbox = e.Item.Controls.OfType<HtmlInputCheckBox>().FirstOrDefault();
            var pageId = e.Item.Controls.OfType<HtmlInputHidden>().FirstOrDefault();
            var docTypeInfo = e.Item.DataItem as DefaultDocTypeInfo;

            SetupDocTypeEntry(picker, checkbox, pageId, docTypeInfo);
        }

        private void SetupDocTypeEntry(DocTypePickerControl picker, HtmlInputCheckBox checkbox, HtmlInputHidden pageId, DefaultDocTypeInfo docTypeInfo)
        {
            if (picker == null || checkbox == null || docTypeInfo == null || pageId == null) return;
            pageId.Value = docTypeInfo.PageId.ToString();
            if (docTypeInfo.DocType != null)
            {
                picker.Select(docTypeInfo.DocType.Id);
                checkbox.Checked = true;
            }
            else
            {
                picker.SetDefault(docTypeInfo.folderAndDoctypeName, docTypeInfo.PageId.ToString());
                picker.Clear();
                checkbox.Checked = false;
            }
        }

        protected void ApplyClick(object sender, EventArgs e)
        {
            foreach (RepeaterItem item in data.Items)
            {
                var picker = item.Controls.OfType<DocTypePickerControl>().FirstOrDefault();
                var checkbox = item.Controls.OfType<HtmlInputCheckBox>().FirstOrDefault();
                var pageId = item.Controls.OfType<HtmlInputHidden>().FirstOrDefault();
                var currentDocType = picker.SelectedDocType;
                var defaultPage = (E_AutoSavePage)Enum.Parse(typeof(E_AutoSavePage), string.IsNullOrEmpty(pageId.Value) ? "None" : pageId.Value);

                if (!checkbox.Checked)
                {
                    AutoSaveDocTypeFactory.ClearDocTypesForPageId(defaultPage, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                    continue;
                }

                if (currentDocType == null)
                {
                    //get it by id and then create it and assign
                    var defaultType = AutoSaveDocTypeFactory.GetDocTypeForPageId(defaultPage, BrokerUserPrincipal.CurrentPrincipal.BrokerId, E_EnforceFolderPermissions.True, fallbackToUnclassifiedForCertainDocTypes: false);
                    currentDocType = defaultType.GetOrCreate();
                }

                if (currentDocType.DefaultPage != defaultPage)
                {
                    AutoSaveDocTypeFactory.ClearDocTypesForPageId(defaultPage, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                }

                currentDocType.DefaultPage = defaultPage;
                currentDocType.Save();
            }

            Response.Redirect(Request.Url.ToString());
        }
    }
}