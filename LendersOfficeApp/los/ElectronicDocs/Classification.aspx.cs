﻿using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using EDocs;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.Security;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class Classification : BaseServicePage
    {
        private BrokerUserPrincipal CurrentUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private Dictionary<string, string> DocTypeList
        {
            get;
            set;
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingEdocsConfiguration
                };
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DocTypeList = EDocumentDocType.GetDocTypesNamesByClassificationName(CurrentUser.BrokerId);            
            DocTypeName.Text = RequestHelper.GetSafeQueryString("docType");
            m_docList.DataSource = EdocClassification.GetSystemClassifications().Values;
            m_docList.DataBind();
        }

        protected void OnDocListBound(object sender, RepeaterItemEventArgs args)
        {
            var classification = args.Item.DataItem as EdocClassification;
            if (classification == null || classification.Id == EdocClassification.NoClassification.Id)
            {
                return;
            }

            EncodedLiteral Classification = args.Item.FindControl("Classification") as EncodedLiteral;
            EncodedLiteral DocTypeName = args.Item.FindControl("DocTypeName") as EncodedLiteral;
            HtmlTableRow Row = args.Item.FindControl("TableRow") as HtmlTableRow;
            
            Classification.Text = classification.Name;

            string docTypeName;
            string notValidSelection = "false";
            if (DocTypeList.TryGetValue(classification.Name, out docTypeName))
            {
                DocTypeName.Text = docTypeName;
                notValidSelection = "true";
            }
            Row.Attributes.Add("onclick", string.Format("toggleCells(this, '{0}', '{1}', {2})", AspxTools.HtmlString(classification.Name), classification.Id, notValidSelection));
        }
    }
}
