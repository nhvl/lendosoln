﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using System.Data;
using EDocs;
using LendersOffice.Security;
using LendersOffice.Constants;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class EditFolder : LendersOffice.Common.BasePage
    {

        private int EditFolderId
        {
            get { return RequestHelper.GetInt("fid", -1); }
        }

        private bool IsNewFolder
        {
            get
            {
                return EditFolderId == -1; 
            }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingEdocsConfiguration
                };
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) || !BrokerUserPrincipal.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            //    throw new AccessDenied();
            
            if (Page.IsPostBack)
            {
                return;
            }

            EDocs.EDocumentFolder folder; 

            if ( IsNewFolder ) 
            {
                folder = new EDocs.EDocumentFolder(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            }
            else 
            {
                folder = new EDocs.EDocumentFolder(BrokerUserPrincipal.CurrentPrincipal.BrokerId, EditFolderId);
                FolderName.ReadOnly = folder.IsSystemFolder; 
            }

            FolderName.Text = folder.FolderNm;
            var roles = folder.FolderRolesList.OrderByDescending(p => p.IsAdmin).ThenBy( p => p.RoleDisplayDesc );
            AvailableRoles.DataSource = roles;
            AvailableRoles.DataBind();
        }

        protected void AvailableRoles_OnItemDataBound( object sender, RepeaterItemEventArgs args )
        {
            //we only care about item and alt item
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return; 
            }

            var folderRole = (EDocs.FolderRoles)args.Item.DataItem;

            CheckBox cb = (CheckBox)args.Item.FindControl("IsRoleInUseCB");
            EncodedLiteral lb = (EncodedLiteral)args.Item.FindControl("IsRoleInUseLabel");
            HiddenField hf = (HiddenField)args.Item.FindControl("roleid");
            HiddenField hf2 = (HiddenField)args.Item.FindControl("isPmlUser");
            
            hf.Value = folderRole.RoleId.ToString();
            hf2.Value = folderRole.IsPmlUser.ToString();
            cb.Checked = folderRole.IsIncludedInFolder;
            lb.Text = folderRole.RoleDisplayDesc;

            if (folderRole.IsAdmin)
            {
                cb.Enabled = false;
            }

        }

        protected void OKBtn_OnClick(object sender, EventArgs args)
        {
            EDocs.EDocumentFolder folder;

            if (EditFolderId == -1)
            {
                folder = new EDocs.EDocumentFolder(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            }
            else
            {
                folder = new EDocs.EDocumentFolder(BrokerUserPrincipal.CurrentPrincipal.BrokerId, EditFolderId);
            }

            folder.FolderNm = FolderName.Text.ToUpper();
            var items = folder.FolderRolesList.ToArray(); 

            for (var i = 0; i < AvailableRoles.Items.Count; i++)
            {
                var item = AvailableRoles.Items[i];

                CheckBox cb = (CheckBox)item.FindControl("IsRoleInUseCB");
                HiddenField hf = (HiddenField)item.FindControl("roleid");
                HiddenField hf2 = (HiddenField)item.FindControl("isPmlUser");
                Guid roleId = new Guid(hf.Value);
                bool isPmlUser = Convert.ToBoolean(hf2.Value);

                var folderRules = items.Single(p => p.RoleId == roleId && p.IsPmlUser == isPmlUser);
                if (folderRules.IsAdmin)
                {
                    continue; //cant edit this
                }
                folderRules.IsIncludedInFolder = cb.Checked;
            }

              try
            {
                folder.Save();
                ClientScript.RegisterClientScriptBlock(typeof(EditFolder), "close", "onClosePopup()", true);
            }
            catch (DuplicateNameSelectedException)
            {
                ErrorMsg.Text = ErrorMessages.EDocs.DuplicateFolderName;
            }
        }
    }                                           
}
