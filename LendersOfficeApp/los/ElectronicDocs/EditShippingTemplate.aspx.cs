﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOfficeApp.common.ModalDlg;
using EDocs;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class EditShippingTemplate : LendersOffice.Common.BaseServicePage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingEdocsConfiguration
                };
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) || !BrokerUserPrincipal.CurrentPrincipal.IsInRole(ConstApp.ROLE_ADMINISTRATOR))
            //    throw new AccessDenied();

            RegisterService("main", "/los/ElectronicDocs/EditShippingTemplateService.aspx");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            EnableJqueryMigrate = false;
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string id = RequestHelper.GetSafeQueryString("id");

                if (!String.IsNullOrEmpty(id))
                {
                    int shippingTemplateId = Convert.ToInt32(id);
                    m_PageHeader.Text = "Edit Shipping Template";
                    EDocumentShippingTemplate template = new EDocumentShippingTemplate();
                    m_StackingOrder.DataBind(template.GetShippingTemplateData(shippingTemplateId));
                    m_ShippingTemplateName.Text = template.GetShippingTemplateName(shippingTemplateId);
                    m_ShippingTemplateId.Value = id;
                }
                else
                {
                    m_PageHeader.Text = "Add Shipping Template";
                    CopyFromExistingRow.Visible = true;
                    EDocumentShippingTemplate shippingTemplates = new EDocumentShippingTemplate();
                    ExistingShippingTemplates.DataSource = shippingTemplates.BrokerShippingTemplateList.Where(p => p.EnabledForBroker);
                    ExistingShippingTemplates.DataValueField = "ShippingTemplateId";
                    ExistingShippingTemplates.DataTextField = "ShippingTemplateName";
                    ExistingShippingTemplates.DataBind();
                }

                var items = EDocumentDocType.GetDocTypesByBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId, E_EnforceFolderPermissions.True); 
                foreach (DocType dt in items)
                {
                    m_ddlDocTypes.Items.Add(new ListItem(dt.FolderAndDocTypeName, dt.DocTypeId));
                    if (dt.FolderAndDocTypeName.Length > 50)
                    {
                        m_ddlDocTypes.Width = 495;
                    }
                }


                var jsItems = from p in items
                              select new
                              {
                                  DocId = p.DocTypeId,
                                  FolderName = p.Folder.FolderNm
                              };

                ClientScript.RegisterClientScriptBlock(typeof(EditShippingTemplate), "items",
                    string.Format("var idmap = {0};", ObsoleteSerializationHelper.JavascriptJsonSerialize(jsItems)), true);


            }
        }
    }
}
