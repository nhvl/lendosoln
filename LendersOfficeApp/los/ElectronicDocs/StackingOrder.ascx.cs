﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using System.Web.UI.HtmlControls;
using System.Collections;
using LendersOffice.Common;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class StackingOrder : System.Web.UI.UserControl
    {
        private bool useRemoveLink = true;
        public bool UseRemoveLink
        {
            get { return useRemoveLink; }
            set { useRemoveLink = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (false == UseRemoveLink)
            {
                m_gvOrder.Columns[4].Visible = false; //! magic #
            }
        }

        public void DataBind(IEnumerable<DocType> list)
        {
            var newList = new ArrayList();
            foreach (var item in list)
            {
                newList.Add(new
                {
                    FolderName = item.Folder.FolderNm,
                    DocTypeName = item.DocTypeName,
                    DocTypeId = item.DocTypeId.TrimWhitespaceAndBOM()
                });
            }

            m_gvOrder.DataSource = newList;
            m_gvOrder.DataBind();
        }

        
        protected void GVOrder_PreRender(object sender, EventArgs e)
        {
            //! taken from http://justgeeks.blogspot.com/2008/09/add-tbody-and-thead-to-gridview.html

            if (m_gvOrder.Rows.Count > 0)
            {
                //This replaces <td> with <th> and adds the scope attribute
                m_gvOrder.UseAccessibleHeader = true;

                //This will add the <thead> and <tbody> elements
                m_gvOrder.HeaderRow.TableSection = TableRowSection.TableHeader;

                //This adds the <tfoot> element. 
                //Remove if you don't have a footer row
                m_gvOrder.FooterRow.TableSection = TableRowSection.TableFooter;
            }

        }
    }
}