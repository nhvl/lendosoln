﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class EditShippingTemplateService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingEdocsConfiguration
                };
            }
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Save":
                    Save();
                    break;
                case "ListDocTypesInTemplate":
                    ListDocTypesInTemplate();
                    break;
                default:
                    break;
            }
        }

        private void ListDocTypesInTemplate()
        {
            EDocumentShippingTemplate template = new EDocumentShippingTemplate();
            List<DocType> doctypes = template.GetShippingTemplateData(GetInt("ShippingTemplateId"));

            var result = from d in doctypes
                         select new
                         {
                             Id = d.DocTypeId,
                             Name = d.FolderAndDocTypeName
                         };

            SetResult("DocTypes", ObsoleteSerializationHelper.JavascriptJsonSerialize(result));

        }

        private void Save()
        {
            bool invalidOp = false;
            string shippingTemplateName = GetString("name");
            string id = GetString("id");
            string order = GetString("order");
            
            EDocumentShippingTemplate template = new EDocumentShippingTemplate();

            try
            {
                if (!String.IsNullOrEmpty(id))
                    template.EditShippingTemplateType(shippingTemplateName, order, Convert.ToInt32(id));
                else
                    template.AddShippingTemplateType(shippingTemplateName, order);
            }
            catch (DuplicateNameSelectedException ex1)
            {
                invalidOp = true;
                SetResult("userMsg", ex1.UserMessage);
            }
            catch (AccessDenied ex2)
            {
                invalidOp = true;
                SetResult("userMsg", ex2.UserMessage);
            }
            SetResult("invalidOp", invalidOp);
        }
    }
}
