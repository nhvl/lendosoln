﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EDocs;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Edocs.DocMagic;
using LendersOffice.AntiXss;
using LendersOffice.ObjLib.Edocs;
using DataAccess;
using LendersOffice.ObjLib.Edocs.DocMagic;
using LendersOffice.ObjLib.Edocs.DocuTech;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class ElectronicDocsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingEdocsConfiguration
                };
            }
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "Copy":
                    Copy();
                    break;
                case "CheckTemplatesInDocType":
                    CheckTemplatesInDocType();
                    break;
                case "CheckForDeletedDocType":
                    CheckForDeletedDocType();
                    break;

                case "RestoreDocType":
                    RestoreDocType();
                    break;

                case "DeleteDocType":
                    DeleteDocType();
                    break;
                case "SetStackOrder":
                    SetStackOrder();
                    break;
                case "SaveDocTypeMapping":
                    SaveDocTypeMapping();
                    break;
                default:
                    break;
            }
        }

        private void SaveDocTypeMapping()
        {
            var brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            bool docuSignEnabled = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(brokerId)?.DocuSignEnabled ?? false;
            if (!docuSignEnabled)
            {
                this.SetResult("Status", "Error");
                this.SetResult("UserMessage", "DocuSign ESign is not enabled. Please try refreshing the page.");
                return;
            }

            var docTypeMappings = SerializationHelper.JsonNetDeserialize<List<ESignDocTypeMap>>(GetString("docTypeArray"));
            List<string> errorMessages = new List<string>();
            List<string> successMessages = new List<string>();
            var validBrokerDocTypes = new HashSet<int>(EDocumentDocType.GetDocTypesByBroker(brokerId, E_EnforceFolderPermissions.True).Select(doctype => doctype.Id));
            foreach (var docTypeMapping in docTypeMappings)
            {
                var sourceDocTypeId = docTypeMapping.SourceDocTypeId;
                var esignDocTypeId = docTypeMapping.ESignDocTypeId;

                if (sourceDocTypeId == null || esignDocTypeId == null)
                {
                    errorMessages.Add($"Either or both doc types in a doctype-ESign mapping is null or non-integer.");
                }

                var validSourceDocType = validBrokerDocTypes.Contains(sourceDocTypeId.Value);
                var validEsignDocType = validBrokerDocTypes.Contains(esignDocTypeId.Value);
                if (validSourceDocType && !validEsignDocType)
                {
                    docTypeMapping.ESignDocTypeId = null;
                }
                else if (!validSourceDocType || !validEsignDocType)
                {
                    errorMessages.Add($"Invalid or deleted document type(s). Source: [{sourceDocTypeId}] ESignTarget: [{esignDocTypeId}].");
                }

            }

            if (errorMessages.Any())
            {
                this.SetResult("Status", "Error");
                this.SetResult("UserMessage", "Errors were encountered saving this page. Please refresh the page and try again.");
                Tools.LogInfo($"[ElectronicDocsService.aspx]: SaveDocTypeMapping Errors {Environment.NewLine}{string.Join(Environment.NewLine, errorMessages)}");
                return;
            }
            else
            {
                foreach (var docTypeMapping in docTypeMappings)
                {
                    EDocumentDocType.SaveEsignDocumentTypeMapping(docTypeMapping.SourceDocTypeId.Value, docTypeMapping.ESignDocTypeId, brokerId);
                    successMessages.Add(docTypeMapping.ESignDocTypeId == null ? $"Unmapped ESign target Doc Type for Doc Type ID [{docTypeMapping.SourceDocTypeId}]." : $"Mapped Doc Type ID [{docTypeMapping.SourceDocTypeId}] to ESigned Doc Type [{docTypeMapping.ESignDocTypeId}].");
                }
                Tools.LogInfo($"[ElectronicDocsService.aspx]: SaveDocTypeMapping Success {Environment.NewLine}{string.Join(Environment.NewLine, successMessages)}");
                this.SetResult("Status", "Success");
                this.SetResult("UserMessage", "Document Type mappings saved.");
                return;
            }
        }

        private void SetStackOrder()
        {
            bool invalidOp = false;

            string order = GetString("order");
            LendersOffice.Admin.BrokerDB db = LendersOffice.Admin.BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            try
            {
                db.SetStackOrder(order);
            }
            catch (FormatException)
            {
                invalidOp = true;
                SetResult("userMsg", "invalid ordering (not parsable as ints)");
                SetResult("invalidOp", invalidOp);
                return;
            }
            db.Save();
            
        }


        private void DeleteDocType()
        {
            EDocumentDocType docType = new EDocumentDocType();
            docType.DeleteDocType(Convert.ToInt32(GetInt("DocTypeId")));
        }
        private void RestoreDocType()
        {
            DocType.RestoreDocType(BrokerUserPrincipal.CurrentPrincipal.BrokerId, GetInt("DocTypeId"));
            
            string mapping = GetString("DocTypeMagicMapping", null);
            string mapping2 = GetString("DocTypeDocuTechMapping", null);
            string mapping3 = GetString("DocTypeIDSMapping", null);

            //If this exists, we also have to set up a doc magic mapping
            if (!string.IsNullOrEmpty(mapping))
            {
                DocTypeMapperFactory.Create(E_DocBarcodeFormatT.DocMagic, PrincipalFactory.CurrentPrincipal.BrokerId).SetAssociation(GetInt("DocTypeID"),
                    from DocMagicId in mapping.Split(',') select int.Parse(DocMagicId));
            }

            if(!string.IsNullOrEmpty(mapping2))
            {
                DocTypeMapperFactory.Create(E_DocBarcodeFormatT.DocuTech, PrincipalFactory.CurrentPrincipal.BrokerId).SetAssociation(GetInt("DocTypeID"),
                    from DocMagicId in mapping2.Split(',') select int.Parse(DocMagicId));
            }

            if (!string.IsNullOrEmpty(mapping3))
            {
                DocTypeMapperFactory.Create(E_DocBarcodeFormatT.IDS, PrincipalFactory.CurrentPrincipal.BrokerId).SetAssociation(GetInt("DocTypeID"),
                    from IdsId in mapping3.Split(',') select int.Parse(IdsId));
            }
        }

        private void CheckForDeletedDocType()
        {
            bool isdeleted;
            int id = DocType.GetDocTypeIdFromName(BrokerUserPrincipal.CurrentPrincipal.BrokerId, GetString("Name"), out isdeleted);
            SetResult("Id", id);
            SetResult("IsDeleted", isdeleted);
        }
        private void CheckTemplatesInDocType()
        {
            int docTypeId = GetInt("DocTypeId");

            IEnumerable<ShippingTemplate> templates = EDocumentShippingTemplate.GetShippingTemplatesWithDocType(BrokerUserPrincipal.CurrentPrincipal.BrokerId, docTypeId);
            SetResult("HasTemplates", templates.Any());

            var names = from template in  templates 
                        select template.ShippingTemplateName;
            SetResult("TemplateNames", ObsoleteSerializationHelper.JsonSerialize(names.ToList()));
        }

        private void Copy()
        {
            bool invalidOp = false;
            string name = GetString("name");
            int id = GetInt("id");

            EDocumentShippingTemplate template = new EDocumentShippingTemplate();
            
            try
            {
                template.CopyShippingTemplate(id, name);
            }
            catch(DuplicateNameSelectedException exc)
            {
                invalidOp = true;
                SetResult("userMsg", exc.UserMessage);
            }
            
            SetResult("invalidOp", invalidOp);
        }

        /// <summary>
        /// A single instance of mapping a source doc type ID to an esigned destination doc type.
        /// </summary>
        private class ESignDocTypeMap
        {
            /// <summary>
            /// Gets or sets the origin doc type ID.
            /// </summary>
            public int? SourceDocTypeId { get; set; }

            /// <summary>
            /// Gets or sets the target doc type ID for esigned docs.
            /// </summary>
            public int? ESignDocTypeId { get; set; }
        }
    }
}
