﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Autosave.ascx.cs" Inherits="LendersOfficeApp.los.ElectronicDocs.Autosave" %>
<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx" %>
<%@ Import Namespace="LendersOffice.ObjLib.Edocs.AutoSaveDocType" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<style type="text/css">
 #AutosaveMain table
 {
     border-collapse: collapse;
     width: 100%;
 }
 #AutosaveMain table td
 {
     padding-right: 5px;
     padding-left: 5px;
 }
 #controls
 {
     width: 6%;
     position:relative;
     left: 46%;
 }
 #errors
 {
     color: Red;
 }
 tr.error.GridItem
 {
     background-color: #FF9999;
 }
 
 tr.error.GridAlternatingItem
 {
     background-color: #FF3333;
 }
 
</style>
<script type="text/javascript">
    jQuery(function($) {
        jQuery.tablesorter.defaults.widgetZebra = {
            css: ["GridItem", "GridAlternatingItem"]
        };

        $('#AutosaveMain table').tablesorter({
            sortList: [[0, 0]],
            widgets: ['zebra']
        });

        $('input.enable').change(function() {
            var $this = $(this);
            var $pickerCell = $this.closest('tr').find('.default-doctype');
            var checked = $this.is(':checked');

            $pickerCell.find('.SelectedDocTypeId').toggleClass('enabled', checked);
            setDisabledAttr($pickerCell.find('a'), !checked);
        }).change();

        $('.SelectedDocTypeId, input.enable').change(function() {
            verifyDocTypeUniqueness();
        });
        
        $('.apply').click(function(){
            
            $('#dialog-message').dialog({
                modal: true,
                closeOnEscape: false,
                dialogClass: 'no-close',
                 resizable: false,
                 width: 200,
                 height: 75
            });
        });
        function verifyDocTypeUniqueness() {
            var seenDocTypes = {};
            $('.SelectedDocTypeId.enabled').each(function() {
                var value = $(this).val();
                if (value == -1) return; //multiple can use defaults at the same time
                
                var seen = seenDocTypes[value] || [];
                seen.push(this);
                seenDocTypes[value] = seen;
            });

            var duplicates = $.map(seenDocTypes, function(val, key) {
                if (val.length > 1) return val;
                return null;
            });

            $('.error').removeClass('error');
            $('#errors').text("");
            $('.apply').prop('disabled', false);

            if (duplicates.length > 0) {
                $(duplicates).closest('tr').addClass('error');
                $('#errors').text("Documents cannot be autosaved to the same DocType");
                $('.apply').prop('disabled', true);
            }
        }
    });
</script>
<div id="AutosaveMain">
    <table border="1" cellspacing="0" >
    <thead>
        <tr class="GridHeader">
        <th class="enable-autosave">
            <!--Checkboxes-->
        </th>
        <th class="document-name">
            LendingQB Document
        </th>
        <th class="default-doctype">
            Default Doc Type
        </th>
        </tr>
    </thead>
    <tbody>
    <asp:Repeater runat="server" ID="data" OnItemDataBound="BindDocTypes">
    <ItemTemplate>
        <tr>
        <td class="enable-autosave">
            <input type="hidden" class="page-id" runat="server" id="PageId"/>
            <input type="checkbox" id="Enabled" class="enable" runat="server" />
        </td>
        <td class="page-name">
            <span><%#AspxTools.HtmlString(((DefaultDocTypeInfo)Container.DataItem).PageFriendlyName)%></span>
        </td>
        <td class="default-doctype">
            <span><uc:DocTypePicker runat="server" ID="DocTypePicker" Width="200px"></uc:DocTypePicker></span>
        </td>
        </tr>
    </ItemTemplate>
    </asp:Repeater>
    </tbody>
    </table>
    <div>
    <span id="errors"></span>
    </div>
    <div id="controls">
        <input type="button" id="Apply" runat="server" value="Apply" onserverclick="ApplyClick" class="apply" />
    </div>
    
    <div id="dialog-message" style="display: none;" >
      <p>
            Please Wait...
      </p>
    </div>
 
</div>