﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditShippingTemplate.aspx.cs" Inherits="LendersOfficeApp.los.ElectronicDocs.EditShippingTemplate" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="so" TagName="StackingOrderUserControl" Src="StackingOrder.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Edit Shipping Template</title>
	<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	<style type="text/css">
        .button {    padding:0 1em 0 1em;width:auto;overflow:visible; }
        .td_StackingOrder table { width: 100%; }

	</style>
</head>
<body>
    <script language="javascript">
        function _init() {
            resize(600, 600);
            document.getElementById('m_ShippingTemplateName').focus();
        }

        function addDocType() {
            <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = '';
            var ddl = <%= AspxTools.JsGetElementById(m_ddlDocTypes) %>;
            var docTypeName = ddl.options[ddl.selectedIndex].text;
            var docTypeId = ddl.options[ddl.selectedIndex].value;

            if(checkListForId(docTypeId))
                addToList(docTypeName, docTypeId);
            else
                <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = 'Doc type has already been added to the order';
        }

        function validatePage() {
            var valid = true;
            var name = <%= AspxTools.JsGetElementById(m_ShippingTemplateName) %>.value.replace(new RegExp("[\\s]+$"), "");
            var nameR = document.getElementById('m_ShippingTemplateNameR');

            if(name == '') {
                valid = false;
                nameR.style.display = '';
            }
            else
                nameR.style.display = 'none';

            if(getStackingOrderSize() == 0) {
                valid = false;
                <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = 'Please add a doc type to the template before saving';
            }
            else
                <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = '';

            return valid;
        }

        function onSaveBtnClicked() {
            if(!validatePage())
                return;

            <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = '';
            var args = new Object();
            args["order"] = getOrder();
            args["id"] = <%= AspxTools.JsGetElementById(m_ShippingTemplateId) %>.value;
            args["name"] = <%= AspxTools.JsGetElementById(m_ShippingTemplateName) %>.value;

            var res = gService.main.call("Save", args );
            if (!res.error) {
                if ( res.value.invalidOp == 'True' )
                    <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = res.value.userMsg;
                else
                    onClosePopup();
            }
            else
                alert(res.UserMessage);
        }

        function copyFromTemplate() {

            var args = {
                ShippingTemplateId : document.getElementById('ExistingShippingTemplates').value
            };

            var res = gService.main.call("ListDocTypesInTemplate", args);

            if (!res.error){
                var docTypeJson = res.value.DocTypes;
                var items = JSON.parse(docTypeJson);
                clearExisting();
                for( var i = 0; i < items.length; i++){
                    if(checkListForId(items[i].Id)) {
                        addToList(items[i].Name, items[i].Id);
                    }
                    else
                        <%= AspxTools.JsGetElementById(m_lblError) %>.innerText = 'Doc type has already been added to the order';

                    }
            }
            else {
                alert('There was an error copying the shipping template.');
            }

        }

    </script>

    <form id="form1" runat="server" defaultbutton="m_okButton">
        <asp:HiddenField ID="m_ShippingTemplateId" runat="server" />
        <table width="100%" border="0" cellspacing="2" cellpadding="3">
			<tr>
				<td class="FormTableHeader">
					<ml:EncodedLabel ID="m_PageHeader" runat="server"></ml:EncodedLabel>
				</td>
			</tr>
		</table>
		<table style="padding:10px 0" cellspacing="8">
			<tr>
			    <td class="FieldLabel" noWrap>
			        Template Name: <asp:TextBox runat="server" ID="m_ShippingTemplateName" Width="160px" MaxLength="50"></asp:TextBox>
					<img id="m_ShippingTemplateNameR" src="../../images/error_icon.gif" style="display:none" />
                </td>
			</tr>
						<tr runat="server" id="CopyFromExistingRow" visible="false">
			    <td class="FieldLabel">
			        Copy From Existing Template: <asp:DropDownList runat="server" ID="ExistingShippingTemplates"></asp:DropDownList> <input type="button" id="CopyButton" value="Copy" onclick="copyFromTemplate()" />
			    </td>
			</tr>
			<tr>
			    <td class="td_StackingOrder">
			        <so:StackingOrderUserControl id="m_StackingOrder" runat="server" Width="100%">
					</so:StackingOrderUserControl>
			    </td>
			</tr>
			<tr>
			    <td>
			        <asp:DropDownList ID="m_ddlDocTypes" runat="server"></asp:DropDownList>
		            <input type="button" class="ButtonStyle button" value="Add" onclick="addDocType();" />
	            </td>
			</tr>
			<tr>
			    <td>
			        <ml:EncodedLabel ID="m_lblError" runat="server" ForeColor="Red"></ml:EncodedLabel>&nbsp;
			    </td>
			</tr>
			<tr>
			    <td align="left">
			    <asp:Button id="m_okButton" runat="server" class="ButtonStyle button" Text="OK" OnClientClick="onSaveBtnClicked(); return false;"></asp:Button>

			        <input type="button" class="ButtonStyle button" value="Cancel" onclick="onClosePopup();" />
			    </td>
			</tr>
		</table>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
</body>
</html>
