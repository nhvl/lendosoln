﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOfficeApp.common.ModalDlg;
using LendersOffice.Common;
using EDocs;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.AntiXss;
using LendersOffice.Edocs.DocMagic;
using DataAccess;
using LendersOffice.ObjLib.Edocs;
using LendersOffice.ObjLib.Edocs.DocMagic;
using LendersOffice.ObjLib.Edocs.DocuTech;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class EditDocType : LendersOffice.Common.BaseServicePage
    {
        private LendersOffice.Admin.BrokerDB m_brokerDB;

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingEdocsConfiguration
                };
            }
        }

        public bool isDocMagicBarcodeScanEnabled
        {
            get
            {
                return m_brokerDB.IsDocMagicBarcodeScannerEnabled;
            }
        }
        public bool isDocuTechBarcodeScanEnabled
        {
            get
            {
                return m_brokerDB.IsDocuTechBarcodeScannerEnabled;
            }
        }
        public bool isIDSBarcodeScanEnabled
        {
            get
            {
                return m_brokerDB.IsIDSBarcodeScannerEnabled;
            }
        }

        public bool IsDocumentCaptureEnabled => m_brokerDB.EnableKtaIntegration;

        public int NumBarcodeScanEnabled
        {
            get
            {
                var count = 0;
                if (isDocMagicBarcodeScanEnabled) count += 1;
                if (isDocuTechBarcodeScanEnabled) count += 1;
                if (isIDSBarcodeScanEnabled) count += 1;
                return count;
            }
        }
        private int DocTypeId
        {
            get
            {
                return RequestHelper.GetInt("did", -1);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterService("main", "/los/ElectronicDocs/ElectronicDocsService.aspx");
            RegisterJsScript("LQBPopup.js");
            EnableJqueryMigrate = false;
            m_brokerDB = LendersOffice.Admin.BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);

            //if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) || !BrokerUserPrincipal.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            //    throw new AccessDenied();
            
            if (Page.IsPostBack)
            {
                return; 
            }
            
            var folderList = EDocumentFolder.GetFoldersInBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId)
                                            .OrderBy(p => p.FolderNm)
                                            .Where(p => false == p.IsSystemFolder); // Do not show system folders

            EDocFolders.DataSource = folderList;
            EDocFolders.DataValueField = "FolderId";
            EDocFolders.DataTextField = "FolderNm";
            EDocFolders.DataBind();

            if (DocTypeId != -1)
            {
                DocTypeEditLabel.Text = "Edit Doc Type";
                DocType dt = EDocumentDocType.GetDocTypeById(BrokerUserPrincipal.CurrentPrincipal.BrokerId, DocTypeId);
                DocTypeName.Value = dt.DocTypeName;
                EDocFolders.SelectedValue = dt.Folder.FolderId.ToString();
                sClassification.Text = dt.EDocClassification.Name;
                ClassificationId.Value = dt.EDocClassification.Id.ToString();
                this.ESignTargetDocType.Value = dt.ESignTargetDocTypeId.ToString();
            }
            else
            {
                DocTypeEditLabel.Text = "Add Doc Type";
            }

            this.ESignTargetDocType.EnableClearLink();
            DocMagicBarcodeMapper.DocTypeId = DocTypeId;
            DocMagicBarcodeMapper.DocTypeName = DocTypeName.Value.ToUpper();

            DocuTechBarcodeMapper.DocTypeId = DocTypeId;
            DocuTechBarcodeMapper.DocTypeName = DocTypeName.Value.ToUpper();

            IDSBarcodeMapper.DocTypeId = DocTypeId;
            IDSBarcodeMapper.DocTypeName = DocTypeName.Value.ToUpper();


            //Only do this stuff if we're actually showing the user DocMagic stuff
            if (isDocMagicBarcodeScanEnabled)
            {
                DocMagicBarcodeMapper.DataSource = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.DocMagic, m_brokerDB.BrokerID);
                DocMagicBarcodeMapper.DataBind();
            }
            else
            {
                DocMagicBarcodeMapper.Visible = false;
            }

            if (isDocuTechBarcodeScanEnabled)
            {
                DocuTechBarcodeMapper.DataSource = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.DocuTech, m_brokerDB.BrokerID);
                DocuTechBarcodeMapper.DataBind();
            }
            else
            {
                DocuTechBarcodeMapper.Visible = false;
            }

            if (isIDSBarcodeScanEnabled)
            {
                IDSBarcodeMapper.DataSource = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.IDS, m_brokerDB.BrokerID);
                IDSBarcodeMapper.DataBind();
            }
            else
            {
                IDSBarcodeMapper.Visible = false;
            }
        }

        protected void OnSaveBtnClicked(object sender, EventArgs e)
        {
            m_lblError.Text = "";
            EDocumentDocType edocs = new EDocumentDocType();

            try
            {
                int safeId = -1;
                if (DocTypeId == -1)
                {
                    bool isDeleted;
                    int id = DocType.GetDocTypeIdFromName(BrokerUserPrincipal.CurrentPrincipal.BrokerId, DocTypeName.Value.ToUpper(), out isDeleted);
                    if (id < 0)
                    {
                        //Add the doc type
                        var esignTargetDocType = this.ESignTargetDocType.Value.ToNullable<int>(int.TryParse);
                        edocs.AddDocType(DocTypeName.Value.ToUpper(), Convert.ToInt32(EDocFolders.SelectedValue), BrokerUserPrincipal.CurrentPrincipal.BrokerId, Convert.ToInt32(ClassificationId.Value), esignTargetDocType == -1 ? null : esignTargetDocType);
                        if (isDocMagicBarcodeScanEnabled || isDocuTechBarcodeScanEnabled)
                        {
                            //And then get its ID so we can use it later if necessary
                            safeId = DocType.GetDocTypeIdFromName(BrokerUserPrincipal.CurrentPrincipal.BrokerId, DocTypeName.Value.ToUpper(), out isDeleted);
                        }
                    }
                    else if (isDeleted)
                    {
                        ClientScript.RegisterHiddenField("restore", id.ToString());
                        if (isDocMagicBarcodeScanEnabled)
                        {
                            ClientScript.RegisterHiddenField("restore_mapping", DocMagicBarcodeMapper.Ids);
                            
                            //Change the binding on the list, so it shows the names the user selected and not the names it used to have (e.g, none)
                            DocMagicBarcodeMapper.ChangeBinding(DocMagicBarcodeMapper.Names.Split(new string[] { "::" }, StringSplitOptions.None));
                        }
                        if(isDocuTechBarcodeScanEnabled)
                        {
                            ClientScript.RegisterHiddenField("restore_docutech_mapping", DocuTechBarcodeMapper.Ids);
                            
                            //Change the binding on the list, so it shows the names the user selected and not the names it used to have (e.g, none)
                            DocuTechBarcodeMapper.ChangeBinding(DocuTechBarcodeMapper.Names.Split(new string[] { "::" }, StringSplitOptions.None));
                        }
                        if (isIDSBarcodeScanEnabled)
                        {
                            ClientScript.RegisterHiddenField("restore_ids_mapping", IDSBarcodeMapper.Ids);

                            //Change the binding on the list, so it shows the names the user selected and not the names it used to have (e.g, none)
                            IDSBarcodeMapper.ChangeBinding(IDSBarcodeMapper.Names.Split(new string[] { "::" }, StringSplitOptions.None));
                        }
                        return;
                    }
                    else
                    {
                        throw new DuplicateNameSelectedException("Doctype Already Exist.");
                    }
                }
                else
                {
                    DocType dt = EDocumentDocType.GetDocTypeById(BrokerUserPrincipal.CurrentPrincipal.BrokerId, DocTypeId);
                    EDocumentFolder f = new EDocumentFolder(BrokerUserPrincipal.CurrentPrincipal.BrokerId, Convert.ToInt32(EDocFolders.SelectedValue));
                    dt.Folder = f;
                    dt.DocTypeName = DocTypeName.Value.ToUpper();
                    dt.EDocClassification = EdocClassification.Retrieve(Convert.ToInt32(ClassificationId.Value));
                    var isDocuSignEnabled = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(m_brokerDB.BrokerID)?.DocuSignEnabled ?? false;
                    ESignTargetRow.Visible = isDocuSignEnabled;
                    if (isDocuSignEnabled)
                    {
                        var esignTargetDocType = this.ESignTargetDocType.Value.ToNullable<int>(int.TryParse);
                        dt.ESignTargetDocTypeId = esignTargetDocType == -1 ? null : esignTargetDocType;
                    }
                    dt.Save();
                    safeId = DocTypeId;
                }
                if (isDocMagicBarcodeScanEnabled)
                {
                    DocMagicBarcodeMapper.DocTypeId = safeId;
                    DocMagicBarcodeMapper.DocTypeName = DocTypeName.Value.ToUpper();
                    DocMagicBarcodeMapper.DataSource = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.DocMagic, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                    DocMagicBarcodeMapper.SaveChanges();
                }
                if (isDocuTechBarcodeScanEnabled)
                {
                    DocuTechBarcodeMapper.DocTypeId = safeId;
                    DocuTechBarcodeMapper.DocTypeName = DocTypeName.Value.ToUpper();
                    DocuTechBarcodeMapper.DataSource = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.DocuTech, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                    DocuTechBarcodeMapper.SaveChanges();
                }
                if (isIDSBarcodeScanEnabled)
                {
                    IDSBarcodeMapper.DocTypeId = safeId;
                    IDSBarcodeMapper.DocTypeName = DocTypeName.Value.ToUpper();
                    IDSBarcodeMapper.DataSource = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.IDS, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                    IDSBarcodeMapper.SaveChanges();
                }
            }
            catch (DuplicateNameSelectedException exc)
            {
                m_lblError.Text = exc.UserMessage;
            }
            catch (FormatException)
            {
                m_lblError.Text = ErrorMessages.Generic;
            }
            
            if (String.IsNullOrEmpty(m_lblError.Text))
                cModalDlg.CloseDialog(this);
        }
    }
}
