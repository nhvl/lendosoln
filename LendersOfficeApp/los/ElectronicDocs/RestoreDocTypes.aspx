﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RestoreDocTypes.aspx.cs" Inherits="LendersOfficeApp.los.ElectronicDocs.RestoreDocTypes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Deleted Doc Types</title>
    <style type="text/css">
        body, h1 { 
               margin: 0;
            padding: 0;
        }
        
        body { 
            background-color: gainsboro;
        }
        .MainRightHeader {
            padding: 5px;
            background-color: Maroon; 
            font-family: Arial, Helvetica, sans-serif;
            color: White;
            font-size: 11px;
            font-weight: bold;
            margin-bottom: 5px;
        }
        
        table.Grid {
            width: 570px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <h4 class="page-header">Deleted Doc Types</h4>
    <form id="form1" runat="server">    
    <div>
        <asp:Repeater runat="server" ID="DeletedDocTypes" OnItemDataBound="DeletedDocType_OnDataBound">
            <ItemTemplate>
                <tr class="GridItem"> 
                    <td><ml:EncodedLiteral runat="server" ID="Folder"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="DocType"></ml:EncodedLiteral></td>
                    <td><a href="#" runat="server" id="RestoreLink">restore</a></td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                            <tr class="AlternatingGridItem"> 
                    <td><ml:EncodedLiteral runat="server" ID="Folder"></ml:EncodedLiteral></td>
                    <td><ml:EncodedLiteral runat="server" ID="DocType"></ml:EncodedLiteral></td>
                    <td><a href="#" runat="server" id="RestoreLink">restore</a></td>
                </tr>
            </AlternatingItemTemplate>
            <HeaderTemplate>
                <table class="Grid">
                    <thead>
                        <tr class="GridHeader">
                            <th>Folder</th>
                            <th>Doc Type</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <script type="text/javascript">
        function _init() {
            resizeForIE6And7(600,400);
        }
        
        $j(function(){
            var table = $j('table.Grid');
            $j('table.Grid a').click(function(e){
                e.preventDefault();
                var data =  { 
                    DocTypeId : $j(this).attr('docTypeId')
                };
                
               var results =  gService.main.call('RestoreDocType', data);
               if ( results.error ) {
                alert(results.UserMessage );
                return false;
               }
                
                $j(this).closest('tbody > tr').remove();
                
                	if( window.dialogArguments != null )
					{
						window.dialogArguments.reload = true;
					}
			
                reColorRows();
                table.trigger('update');
            });
            
            function reColorRows() {
                var alt = false;
                $j('table.Grid tbody > tr').each(function(i,o){
                    $j(o).removeClass(alt ? 'GridAlternatingItem' : 'GridItem');
                    $j(o).addClass(alt ? 'GridItem' : 'GridAlternatingItem' );
                    alt = !alt;
                });
            }
            
            table.tablesorter({sortList: [[0,0], [1,0]]}).bind('sortEnd', reColorRows);
        });
    </script>
    </form>
</body>
</html>
