<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocBarcodeMapper.ascx.cs" Inherits="LendersOfficeApp.los.ElectronicDocs.DocBarcodeMapper" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript">
    function <%= AspxTools.HtmlString(ClientID) %>_DocMagicEdit(id, name) {
        var arg = new Object();
        arg.init = new Array();
        var map = <%= AspxTools.JsGetElementById(DocMapping) %>;
        var format = <%= AspxTools.JsString(Format) %>;
        var len = map.options.length;
        for(var i = 0; i < len; i++)
        {
            arg.init.push(map.options[i].value);
        }
        showModal("/los/ElectronicDocs/EditDocMagicMapping.aspx?did="+id+"&dname="+name+"&dformat="+format, arg, null, null, function(ret){
            <%-- /*If no result, they just cancelled*/--%>
            if(!ret.result)
            {
                return;
            }
        
            <%--/*Remove all child nodes */--%>
            while(map.hasChildNodes())
            {
                map.removeChild(map.firstChild);
            }
            
            <%--/*And then re-create some new ones - we'll also fill out our hidden fields here*/ --%>
            var allnames = new Array();
            var allIds = new Array();
            var length = ret.result.length;
            for(var i = 0; i < length; i++)
            {
                var newChild = document.createElement('option');
                newChild.value = ret.result[i].value;
                newChild.text = ret.result[i].text;
                
                allIds.push(ret.result[i].value);
                allnames.push(ret.result[i].text);
                
                try
                {
                    map.add(newChild, map.options[null]);
                }
                catch(e)
                {
                    map.add(newChild, null);
                }
            }
            
            <%= AspxTools.JsGetElementById(MappedNames)%>.value = allnames.join("::");
            <%= AspxTools.JsGetElementById(MappedIds)%>.value = allIds.join(",");
            <%= AspxTools.JsGetElementById(ChangedField)%>.value = "true";
        }, { hideCloseButton: true }); 
    }
</script>
<div class="barcode-mapper">
    <input type="hidden" class="vendor-name" id="VendorName" runat="server" />
    <input type="hidden" class="doctype-id" id="DocType" runat="server" />
    <input type="hidden" class="names" id="MappedNames" runat="server" />
    <input type="hidden" class="ids" id="MappedIds" runat="server" />
    <input type="hidden" class="changed" id="ChangedField" value="false" runat="server" />
    <div>
        <ml:EncodedLabel runat="server" ID="m_assignedDocsLabel" Text="Documents assigned to this Doc Type"></ml:EncodedLabel>
    </div>
    <select multiple="true" size="10" class="doc-mapping" id="DocMapping" style="width:90%;" runat="server">
    </select>
    <div>
        <a runat="server" id="EditAssignedDocsLink">edit assigned documents</a>
    </div>
</div>
