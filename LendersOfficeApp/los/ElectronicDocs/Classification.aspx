﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Classification.aspx.cs"
    Inherits="LendersOfficeApp.los.ElectronicDocs.Classification" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOfficeApp.los.admin" %>
<%@ Import Namespace="LendersOffice.Security" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Assign Classification to Doc Type</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
    <style type="text/css">       
        .button {padding:0 1em 0 1em;width:auto;overflow:visible; }
    </style>
</head>
<body>
    <script language="javascript">
        var selectedName = 'None';
        var selectedId = 0;
        
        function _init() {
            resize(500, 370);
            setClass();
        }

        function toggleCells(element, name, id, notValidSelection) {
            setClass();
            element.className = 'GridSelectedItem';
            selectedName = name;
            selectedId = id;
            document.getElementById('assignBtn').disabled = notValidSelection;
        }

        function OnAssignBtnClicked() {
            var args = window.dialogArguments || {};
            args.classificationType = selectedName;
            args.classificationId = selectedId;
            args.OK = true;
            onClosePopup(args);
        }

        function setClass() {
            var table = document.getElementById('dataTable');
            for (var i = 0; i < table.rows.length; i++) {
                var row = table.getElementsByTagName("tr")[i];
                if (i % 2 == 1)
                    row.className = 'GridAlternatingItem';
                else
                    row.className = 'GridItem';
            }
        }        
    </script>
    <h4 class="page-header">Assign Classification to Doc Type</h4>
    <form id="form1" runat="server">
    <table width="100%" border="0" cellspacing="2" cellpadding="3">
        <tr>
            <td colspan="2" align="left" class="FieldLabel">
                Doc Type:
                <ml:EncodedLabel ID="DocTypeName" runat="server" ></ml:EncodedLabel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div style="background-color: white; border: 1px solid grey; height: 260px; overflow: auto;">
                    <table id="dataTable" cellpadding="2" cellspacing="0" border="1">
                        <tr>
                            <td class="GridHeader">
                                Document Classification
                            </td>
                            <td class="GridHeader">
                                Assigned Doc Type
                            </td>
                        </tr>
                        <asp:Repeater ID="m_docList" runat="server" OnItemDataBound="OnDocListBound">
                            <ItemTemplate>
                                <tr id="TableRow" runat="server">
                                    <td>
                                        <ml:EncodedLiteral ID="Classification" runat="server"></ml:EncodedLiteral>
                                    </td>
                                    <td>
                                        <ml:EncodedLiteral ID="DocTypeName" runat="server"></ml:EncodedLiteral>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table> 
                </div>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <input type="button" id="assignBtn" value="Assign" class="ButtonStyle button" onclick="OnAssignBtnClicked();"/>
                <input type="button" value="Cancel" class="ButtonStyle button" onclick="onClosePopup();" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
