using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.Edocs;
using LendersOffice.AntiXss;
using EDocs;
using DataAccess;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class DocBarcodeMapper : System.Web.UI.UserControl
    {
        public IDocTypeMapper DataSource;
        public int DocTypeId;
        public string DocTypeName;
        protected string Format = string.Empty;

        private bool Changed
        {
            get 
            {
                return bool.Parse(ChangedField.Value);
            }
        }
        public string Ids
        {
            get
            {
                return MappedIds.Value;
            }
        }
        public string Names
        {
            get
            {
                return MappedNames.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var BasePage = Page as BasePage;
            if (BasePage != null)
            {
                BasePage.RegisterJsScript("DocBarcodeMapper.js");
                BasePage.RegisterJsScript("json.js");
            }

            EditAssignedDocsLink.HRef = "javascript:" + ClientID + "_DocMagicEdit(" + AspxTools.JsString(DocTypeId) + ", " + AspxTools.JsString(DocTypeName) + ")";
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            if (DataSource == null) return;

            Format = DataSource.DocumentFormat.ToString("d");

            m_assignedDocsLabel.Text = String.Format("{0} Documents assigned to this Doc Type", DataSource.VendorName);

            DocMapping.DataValueField = "Id";
            DocMapping.DataTextField = "Description";
            DocMapping.DataSource = DataSource.GetTypesFor(DocTypeId).OrderBy(p => p.Description);

            DocMapping.DataBind();
        }

        public void ChangeBinding(IEnumerable<string> dataSource)
        {
            DocMapping.DataSource = from string desc in dataSource select new { Description = desc, Id = 0 };
            DocMapping.DataBind();
        }

        public void SaveChanges()
        {
            //If there haven't been any changes, don't bother saving.
            if (!Changed) return;

            //Grab the list of IDs we should be assigning to the current eDoc
            string assigned = Ids;

            IEnumerable<int> AssignedDocMagicIDs;
            if (!string.IsNullOrEmpty(assigned))
            {
                //If it's there, we get it as a csv of ids, so split them apart on commas 
                //(fortunately we don't have to implement a full csv parser)
                AssignedDocMagicIDs = from string entry in assigned.Split(',') select int.Parse(entry);
            }
            else
            {
                //Otherwise the user has nothing assigned, so make an empty list.
                AssignedDocMagicIDs = new int[] { };
            }

            //Save it off to the database
            DataSource.SetAssociation(DocTypeId, AssignedDocMagicIDs);
        }
    }
}
