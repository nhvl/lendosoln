﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditDocMagicMapping.aspx.cs"
    Inherits="LendersOfficeApp.los.ElectronicDocs.EditDocMagicMapping" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOfficeApp.los" %>
<%@ Register TagPrefix="uc" TagName="LRPicker" Src="../LeftRightPicker.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body>
    <h4 class="page-header">Edit DocMagic</h4>
    <form id="form1" runat="server">
    <%-- /*Hide the DocTypeID here for a sec*/ --%>
    <input type="hidden" id='did' value="<%=AspxTools.HtmlString(DocTypeId.ToString())%>" />
    <input type="hidden" id='dname' value="<%=AspxTools.HtmlString(DocTypeName)%>" />
    <uc:LRPicker id="Picker" runat="server"></uc:LRPicker>
    </form>
</body>
</html>
