﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditDocType.aspx.cs" Inherits="LendersOfficeApp.los.ElectronicDocs.EditDocType" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BarcodeMapper" Src="~/los/ElectronicDocs/DocBarcodeMapper.ascx" %>
<%@ Register TagPrefix="uc1" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOfficeApp.los.admin" %>
<%@ Import Namespace="LendersOffice.Security" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Edit Doc Type</title>
	<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	<style type="text/css">
        .button {    padding:0 1em 0 1em;width:auto;overflow:visible; }

	</style>
</head>
<body>
    <script language="javascript">
        function clearError() {
            document.getElementById('m_lblError').innerText = '';
        }

        function restoreDocType(id) {
            if( !confirm ( 'This Doc Type was previously deleted. Restore this Doc Type? ' ) ) {
                return false;
            }
            var args = {
                DocTypeId: id
                <% if(isDocMagicBarcodeScanEnabled) { /*Only want to pass this param if they have a doc magic mapping */%>
                , DocTypeMagicMapping: document.getElementById('restore_mapping').value
                <% } %>
                <% if(isDocuTechBarcodeScanEnabled) { %>
                , DocTypeDocuTechMapping : document.getElementById('restore_docutech_mapping').value
                <% } %>
                <% if(isIDSBarcodeScanEnabled) { %>
                , DocTypeIDSMapping: document.getElementById('restore_ids_mapping').value
                <% } %>
            };
            var results = gService.main.call('RestoreDocType',  args);
            if( results.error ) {
                alert ( results.UserMessage ) ;
                return false;
            }
            else {
                onClosePopup();
            }
        }

    <%-- /*Had to move this out of the header in order to put the conditional in*/ --%>
    <% if (isDocMagicBarcodeScanEnabled || isDocuTechBarcodeScanEnabled || isIDSBarcodeScanEnabled){ %>

        function _init() {
            var height = 400;
            <% if(NumBarcodeScanEnabled == 2) { %>
                height = 600;
            <% } %>
            <% if(NumBarcodeScanEnabled == 3) { %>
                height = 800;
            <% } %>
            resize(490, height);
            if ( document.getElementById('restore') ) {
                restoreDocType(document.getElementById('restore').value);
            }
        }

        <%} else { %>
        <%-- /*Only resize the window to this size if we're not doing DocMagic scan distribution*/ --%>

        function _init() {
            resize(400, 200);
            if ( document.getElementById('restore') ) {
                restoreDocType(document.getElementById('restore').value);
            }
        }

        <% } %>

        function onClassificationHelp() {
            alert("The classification is used to map Lender doctypes to LendingQB's document capture engine.");
        }

        function onClassificationClear() {
            document.getElementById('sClassification').innerText = "None";

            if (document.getElementById('ClassificationId') != null)
            {
	            document.getElementById('ClassificationId').value = '0';
	        }
        }

        function onClassificationAssign() {
          showModal("/los/ElectronicDocs/Classification.aspx?docType="+document.getElementById('DocTypeName').value, null, null, null, function(args){
                if (args.OK) {
                    document.getElementById('sClassification').innerText = args.classificationType;

                    if (document.getElementById('ClassificationId') != null)
                    {
                        document.getElementById('ClassificationId').value = args.classificationId;
                    }
                }
            }, { hideCloseButton: true });
        }

    </script>
    <h4 class="page-header"><ml:EncodedLiteral runat="server" ID="DocTypeEditLabel"></ml:EncodedLiteral></h4>
    <form id="form1" runat="server">
        <%if(isDocMagicBarcodeScanEnabled || isDocuTechBarcodeScanEnabled || isIDSBarcodeScanEnabled || IsDocumentCaptureEnabled){ %>
        <%--This is where the Javascript hides the doc magic names and ids
         Names are so we can display them in the select box after postback during a restore
         IDs are so we don't have to select all of the entries in the select box in order to get
          them to show up on post.
         DocMagicChanges keeps track of whether or not the user's touched the mapping, so we know
          whether or not we should touch the DB on ok--%>
        <input type="hidden" id="ClassificationId" runat="server" value="0"/>

        <%} %>
        <table width="100%" border="0" cellspacing="2" cellpadding="3">
			<tr>
			    <td></td>
			</tr>
			<tr>
			    <td align="right" class="FieldLabel">
			        Folder
			        </td>
			        <td>
			        <asp:DropDownList runat="server" ID="EDocFolders"></asp:DropDownList>
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel">
			        Doc Type
			        </td>
			        <td>
			        <input type="text" runat="server" ID="DocTypeName" onkeyup="clearError();" MaxLength="50" style="width:90%"/>
			        <asp:RequiredFieldValidator ID="m_DocTypeNameValidator" runat="server" ControlToValidate="DocTypeName">
                        <img runat="server" src="../../images/error_icon.gif">
			        </asp:RequiredFieldValidator>
			    </td>
			</tr>
			<tr>
			    <td align="right" class="FieldLabel">
			        Capture Classification 
			        <a onclick="onClassificationHelp()">?</a>
			    </td>
			    <td>
			        <ml:EncodedLabel ID="sClassification" runat="server">None</ml:EncodedLabel>
			        &nbsp&nbsp[
			        <a onclick="onClassificationClear()">Clear</a> |
			        <a onclick="onClassificationAssign()">Assign classification</a> ]
			    </td>
			</tr>
			<tr>
			    <td  colspan="2">
			        <ml:EncodedLabel ID="m_lblError" runat="server" ForeColor="Red"></ml:EncodedLabel>&nbsp;
			    </td>
			</tr>
			<% if (isDocMagicBarcodeScanEnabled)
               { %>
			<tr>
			    <td colspan="2">
    			    <uc1:BarcodeMapper id="DocMagicBarcodeMapper" runat="server"></uc1:BarcodeMapper>
			    </td>
			</tr>

			<% } %>
			<% if (isDocuTechBarcodeScanEnabled)
                { %>
			<tr>
			    <td colspan="2">
    			    <uc1:BarcodeMapper id="DocuTechBarcodeMapper" runat="server"></uc1:BarcodeMapper>
			    </td>
			</tr>
			<% } %>
			<% if (isIDSBarcodeScanEnabled)
                { %>
            <tr>
                <td colspan="2">
                    <uc1:BarcodeMapper ID="IDSBarcodeMapper" runat="server" />
                </td>
            </tr>
            <% } %>
            <tr id="ESignTargetRow" runat="server">
                <td colspan="2">
                    <div class="FieldLabel">When these documents come back from ESign, assign them to the following Doc Type: </div>
                    <uc1:DocTypePicker id="ESignTargetDocType" runat="server" />
                </td>
            </tr>
			<tr>
			    <td align="left" colspan="2">
			        <asp:Button ID="m_btnSave" CssClass="ButtonStyle button" runat="server" Text="OK" OnClick="OnSaveBtnClicked" />
			        <input type="button" value="Cancel" class="ButtonStyle button" onclick="onClosePopup();" />
			    </td>
			</tr>
		</table>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
</body>
</html>
