﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditFolder.aspx.cs" Inherits="LendersOfficeApp.los.ElectronicDocs.EditFolder" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Folder - Add/Edit</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet"/>

    <style type="text/css">
    form.table { table-layout: fixed; }

    label {
        display: block;
        padding-left: 15px;
        text-indent: -15px;
    }
    input.a {
        width: 13px;
        height: 13px;
        padding: 0;
        margin:0;
        vertical-align: bottom;
        position: relative;
        top: -1px;
        *overflow: hidden;
    }

    ul.nostyle
    {
        list-style-type: none;
        padding: 0px 30px;
    }

    li { padding: 0; margin: 0; display:inline;}


        .button {    padding:0 1em 0 1em;width:auto;overflow:visible; }




    </style>
</head>
<body>
    <h4 class="page-header">Folder - Add/Edit</h4>
    <form id="form1" runat="server">
    <div>
        <table width="100%" border="0" cellspacing="2" cellpadding="3">
            <tbody>
                <tr><td height="0"></td></tr>
			    <tr>
			        <td class="FieldLabel" style="width:41px">
			            Folder
			        </td>

			        <td>
			            <asp:TextBox runat="server" ID="FolderName" Width="300px" ></asp:TextBox>
			            <asp:RequiredFieldValidator ID="FolderNameValidator" runat="server" ControlToValidate="FolderName">
                            <img runat="server" src="../../images/error_icon.gif">
			            </asp:RequiredFieldValidator>
			        </td>
			    </tr>

			    <tr>
			        <td colspan="2" class="FieldLabel">Select the roles that will have access to this folder</td>
			    </tr>
			    <tr>
			        <td colspan="2" class="FieldLabel">
	                    <asp:Repeater runat="server" ID="AvailableRoles" OnItemDataBound="AvailableRoles_OnItemDataBound">
	                        <HeaderTemplate>
	                            <ul class="nostyle">
	                        </HeaderTemplate>

	                        <FooterTemplate>
	                            </ul>
	                        </FooterTemplate>
	                        <ItemTemplate>
	                            <li>
	                                <asp:HiddenField runat="server" ID="isPmlUser" />
	                                <asp:HiddenField runat="server" ID="roleid" />
	                                 <label>
	                                 <asp:CheckBox  CssClass="a"  runat="server" ID="IsRoleInUseCB" />
	                                    <ml:EncodedLiteral runat="server" ID="IsRoleInUseLabel"></ml:EncodedLiteral>
	                                 </label>
	                            </li>
	                        </ItemTemplate>
	                    </asp:Repeater>
			        </td>
			    </tr>
			    <tr>
			        <td colspan="2"  align="center" style="color: Red; " >
			            <ml:EncodedLiteral  runat="server" ID="ErrorMsg" ></ml:EncodedLiteral>
			        </td>
			    </tr>
			    <tr>
			        <td colspan="2">
			            <asp:Button runat="server" OnClick="OKBtn_OnClick" ID="OKBtn" Text="OK"  CssClass="ButtonStyle button" />
			            <input type="button" value="Cancel" class="ButtonStyle button" onclick="onClosePopup();" />
			        </td>
			    </tr>
            </tbody>
        </table>
    </div>
		<uc1:cModalDlg id="CModalDlg1"  runat="server"></uc1:cModalDlg>

    </form>

        <script type="text/javascript">
        function _init() {
            resizeForIE6And7(400, 605);

        }
    </script>
</body>
</html>
