﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StackingOrder.ascx.cs" Inherits="LendersOfficeApp.los.ElectronicDocs.StackingOrder" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<style type="text/css">
    .noDisplay
    {
        display:none;
    }
</style>

<script type="text/javascript" >
    $(function(){
        function stackChange() {
            if (typeof(stackOrderChanged) == 'function') {
                stackOrderChanged();
            }
        }

        $('table.doctypeGrid tbody').sortable({
            start : function(e,ui){
                ui.placeholder.html('<!--[if IE]><td>&nbsp;</td><![endif]-->');
            },
            change : stackChange,
            helper: function(e, tr)
            {
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function(index)
                {
                // Set helper cell sizes to match the original sizes
                $(this).width($originals.eq(index).width())
                });
                return $helper;
            }
        });
    });
    function checkListForId(id) {
        var tBody = <%=AspxTools.JsGetElementById(m_gvOrder)%>.getElementsByTagName("tbody")[0];
        for (var i = 0; i < tBody.rows.length; i++) {
            if (id == tBody.getElementsByTagName("tr")[i].children[0].innerText.replace(' ', '')) {
                return false;
            }
        }
        return true;
    }

    function getStackingOrderSize() {
        var tBody = <%=AspxTools.JsGetElementById(m_gvOrder)%>.getElementsByTagName("tbody")[0];
        return tBody.rows.length;
    }

    <%if(UseRemoveLink){ %>
    function addToList(name, id) {

        var folderName = '';

        if( typeof(idmap) != 'undefined' ) {
            for( var i = 0; i < idmap.length; i++){
                var entry = idmap[i];
                if (entry.DocId ==  id.toString() )
                {
                    folderName = entry.FolderName;
                    break;
                }
            }
        }
        var tBody = <%=AspxTools.JsGetElementById(m_gvOrder)%>.getElementsByTagName("tbody")[0];

        var row = document.createElement('tr');

        var cell = document.createElement('td');
        cell.innerHTML = id;
        cell.style.display = "none";
        row.appendChild(cell);


        cell = document.createElement('td');
        cell.setAttribute("align", "center");
        cell.innerHTML = '<a href="javascript:void(0);" style="text-decoration:none" onclick="moveUpInList(this.parentNode.parentNode);">&#x25b2;</a>&nbsp'
                       + '<a href="javascript:void(0);" style="text-decoration:none" onclick="moveDownInList(this.parentNode.parentNode);">&#x25bc;</a>';
        row.appendChild(cell);

        cell = document.createElement('td');
        cell.textContent = folderName;
        row.appendChild(cell);

        cell = document.createElement('td');
        cell.textContent = name;
        row.appendChild(cell);

        <%if(UseRemoveLink){ %>
        cell = document.createElement('td');
        cell.innerHTML = '<a href="javascript:void(0);" onclick="removeFromList(this.parentNode.parentNode);">remove</a>';
        row.appendChild(cell);
        <%} %>

        tBody.appendChild(row);
        setClass();
    }
    function removeFromList(row) {
        tBody = <%=AspxTools.JsGetElementById(m_gvOrder)%>.getElementsByTagName("tbody")[0];
        tBody.deleteRow(row.rowIndex - 1);
        setClass();
    }

    function clearExisting() {
        tBody = <%=AspxTools.JsGetElementById(m_gvOrder)%>.getElementsByTagName("tbody")[0];
        for(var i = tBody.rows.length - 1; i >= 0; i--){
            tBody.deleteRow(i);
        }
    }

    <%} %>

    function moveDownInList(row) {
        tBody = <%=AspxTools.JsGetElementById(m_gvOrder)%>.getElementsByTagName("tbody")[0];
        clickedRowIndex = row.rowIndex;
        if (clickedRowIndex != tBody.rows.length) {
            row1 = tBody.getElementsByTagName("tr")[clickedRowIndex - 1];
            row2 = tBody.getElementsByTagName("tr")[clickedRowIndex];
        }
        else {
            row1 = tBody.getElementsByTagName("tr")[0];
            row2 = tBody.getElementsByTagName("tr")[clickedRowIndex - 1];
        }
        tBody.insertBefore(row2, row1);
        setClass();
        if(typeof(stackOrderChanged) != 'undefined')
        {
            stackOrderChanged();
        }
    }

    function moveUpInList(row) {
        tBody = <%=AspxTools.JsGetElementById(m_gvOrder)%>.getElementsByTagName("tbody")[0];
        clickedRowIndex = row.rowIndex;
        if (clickedRowIndex != 1) {
            row1 = tBody.getElementsByTagName("tr")[clickedRowIndex - 2];
            row2 = tBody.getElementsByTagName("tr")[clickedRowIndex - 1];
            tBody.insertBefore(row2, row1);
        }
        else {
            row = tBody.getElementsByTagName("tr")[clickedRowIndex - 1];
            tBody.appendChild(row);
        }
        setClass();
        if(typeof(stackOrderChanged) != 'undefined')
        {
            stackOrderChanged();
        }
    }

    function getOrder() {
        tBody = <%=AspxTools.JsGetElementById(m_gvOrder)%>.getElementsByTagName("tbody")[0];
        var order = '';
        for (var i = 0; i < tBody.rows.length; i++)
            order += tBody.getElementsByTagName("tr")[i].children[0].innerHTML + ',';

        return order;
    }

    function setClass() {
        var tBody = <%=AspxTools.JsGetElementById(m_gvOrder)%>.getElementsByTagName("tbody")[0];
        for (var i = 0; i < tBody.rows.length; i++) {
            var row = tBody.getElementsByTagName("tr")[i];
            if (i % 2 == 1)
                row.className = 'GridAlternatingItem';
            else
                row.className = 'GridItem';
        }
    }
</SCRIPT>
<div id="theHolder">
<asp:GridView runat="server" ID="m_gvOrder" CssClass="doctypeGrid" AutoGenerateColumns="false" CellPadding="4" CellSpacing="1"
 OnPreRender="GVOrder_PreRender" EmptyDataText="lol, nothing was found">
    <HeaderStyle CssClass="GridHeader"/>
    <RowStyle CssClass="GridItem"/>
    <AlternatingRowStyle CssClass="GridAlternatingItem"/>
    <EmptyDataTemplate>
    </EmptyDataTemplate>
    <Columns>
        <asp:BoundField DataField="DocTypeId" ItemStyle-CssClass="noDisplay"  HeaderStyle-CssClass="noDisplay"/>
        <asp:TemplateField HeaderText="Re-Order" ItemStyle-HorizontalAlign="Center">
            <ItemTemplate>
                <a href='javascript:void(0);' style='text-decoration: none' onclick='  moveUpInList(this.parentNode.parentNode);'>&#x25b2;</a>
                <a href='javascript:void(0);' style='text-decoration: none' onclick='moveDownInList(this.parentNode.parentNode);'>&#x25bc;</a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="FolderName" HeaderText="Folder" />
        <asp:BoundField DataField="DocTypeName" HeaderText="Doc Type" />
        <asp:TemplateField>
            <ItemTemplate>
                <a href='javascript:void(0);' onclick='removeFromList(this.parentNode.parentNode);'>remove</a>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
</div>
<%if (m_gvOrder.Rows.Count == 0)
  { %>
<script type="text/javascript">
    // create the empty table.
    while(true){
        var theTable = <%=AspxTools.JsGetElementById(m_gvOrder)%>;
        if(typeof(theTable)!='undefined' && theTable != null)
        {
            break;
        }

        theTable = document.createElement('table');
        theTable.cellspacing = "1";
        theTable.cellpadding = "4";
        theTable.rules = "all";
        theTable.border = "1";


        theTable.id = <%=AspxTools.JsGetClientIdString(m_gvOrder)%>;
        var thead = document.createElement('thead');

        var theadrow = document.createElement('tr');
        theadrow.className ='GridHeader';
        var id = document.createElement('th');
        id.style.display = 'none';
        var ro = document.createElement('th');
        var fo = document.createElement('th');
        var dt = document.createElement('th');


        ro.innerHTML = "Re-Order";
        fo.innerHTML = "Folder";
        dt.innerHTML = "Doc Type";


        theadrow.appendChild(id);
        theadrow.appendChild(ro);
        theadrow.appendChild(fo);
        theadrow.appendChild(dt);




        var rl = document.createElement('th');
        <%if(false == UseRemoveLink){ %>
        rl.style.display = "none";
        <%} %>
        theadrow.appendChild(rl);



        thead.appendChild(theadrow);
        theTable.appendChild(thead);
        var tbody = document.createElement('tbody');
        theTable.appendChild(tbody);
        var tmpHolder = document.getElementById('theHolder');
        tmpHolder.parentNode.insertBefore(theTable, tmpHolder);
        tmpHolder.parentNode.removeChild(tmpHolder);

        break;
    }
</script>
<%}
  else
  { %>
  <script type="text/javascript">
  // remove the temporary holding div.
  while(true){
        var theTable = <%=AspxTools.JsGetElementById(m_gvOrder)%>.parentNode;

        var tmpHolder = document.getElementById('theHolder');
        tmpHolder.parentNode.insertBefore(theTable, tmpHolder);
        tmpHolder.parentNode.removeChild(tmpHolder);
        break;
    }
    </script>
  <%} %>
