﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using EDocs;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Edocs.DocMagic;
using LendersOffice.AntiXss;
using LendersOffice.Admin;
using LendersOffice.ObjLib.Edocs;
using DataAccess;
using LendersOffice.ObjLib.Edocs.DocMagic;
using LendersOffice.ObjLib.Edocs.DocuTech;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class EditDocMagicMapping : LendersOffice.Common.BaseServicePage 
    {
        private BrokerDB m_brokerDB;

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingEdocsConfiguration
                };
            }
        }

        public int DocTypeId
        {
            get
            {
               return RequestHelper.GetInt("did", -1);
            }
        }

        public string DocTypeName
        {
            get
            {
                return RequestHelper.GetSafeQueryString("dname");
            }
        }

        public E_DocBarcodeFormatT DocumentFormat
        {
            get
            {
                return (E_DocBarcodeFormatT)RequestHelper.GetInt("dformat");
            }
        }
        
        protected void Page_Init(object sender, EventArgs e)
        {
            //If they can't see the barcode scanner mappings, they shouldn't be here
            m_brokerDB = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            if (!m_brokerDB.IsDocMagicBarcodeScannerEnabled && !m_brokerDB.IsDocuTechBarcodeScannerEnabled && !m_brokerDB.IsIDSBarcodeScannerEnabled)
                throw new AccessDenied();

            //Otherwise copy the previous pages' security setting
            //if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.CanViewEDocs) || !BrokerUserPrincipal.CurrentPrincipal.HasRole(E_RoleT.Administrator))
            //    throw new AccessDenied();

            //Don't do all this again if it's a postback
            if (IsPostBack)
                return;

            IDocTypeMapper docTypeMapper = DocTypeMapperFactory.Create(DocumentFormat, PrincipalFactory.CurrentPrincipal.BrokerId);

            IEnumerable<IDocVendorDocType> assignedSource = docTypeMapper.GetTypesFor(DocTypeId);
            IEnumerable<IDocVendorDocType> availableSource = docTypeMapper.GetDisassociatedTypes();

            Picker.AssignedSrc = assignedSource;
            Picker.AvailableSrc = availableSource;
            Picker.TextField = "Description";
            Picker.ValueField = "Id";
            string docVendor = DataAccess.Tools.s_DocBarcodeFormatT_Map[DocumentFormat];
            Picker.AvailableHeader = String.Format("Unassigned {0} Documents", docVendor);
            Picker.AssignedHeader = String.Format("{0} Documents assigned to {1}", docVendor, DocTypeName);
            Picker.DataBind();
        }
    }
}
