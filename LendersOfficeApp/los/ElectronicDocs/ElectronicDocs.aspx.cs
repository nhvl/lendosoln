﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Data.SqlClient;
using LendersOffice.Security;
using EDocs;
using System.Web.UI.HtmlControls;
using System.Text;
using LendersOffice.Constants;
using LendersOffice.Common;
using LendersOffice.Edocs.DocMagic;
using LendersOffice.AntiXss;
using CommonProjectLib.Common.Lib;
using LendersOffice.ObjLib.Edocs;
using MeridianLink.CommonControls;
using LendersOfficeApp.newlos.ElectronicDocs;

namespace LendersOfficeApp.los.ElectronicDocs
{
    public partial class ElectronicDocs : LendersOffice.Common.BaseServicePage
    {
        private ILookup<int, IDocVendorDocType> m_assignedDocMagicDocTypesByDocTypeID;
        private ILookup<int, IDocVendorDocType> m_assignedDocuTechDocTypesByDocTypeID;
        private ILookup<int, IDocVendorDocType> m_assignedIDSDocTypesByDocTypeID;

        private LendersOffice.Admin.BrokerDB m_brokerDB;

        private int m_dgDocTypes_ESignDocumentTargetColumn = 5;

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingEdocsConfiguration
                };
            }
        }

        /// <summary>
        /// Gets a value indicating whether the DocuSign integration is enabled.
        /// </summary>
        protected bool docuSignEnabled { get; private set; }

        /// <summary>
        /// Keeps track of a list of valid document types for the broker.
        /// </summary>
        protected HashSet<int> validBrokerDocTypes = new HashSet<int>();

        protected void Page_Init(object sender, EventArgs e)
        {
            RegisterService("main", "/los/ElectronicDocs/ElectronicDocsService.aspx");
            RegisterJsScript("json.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("LQBPopup.js");
            EnableJqueryMigrate = false;
        }

        protected int PageNumber
        {
            get
            {
                return RequestHelper.GetInt("pg", 0);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((BasePage)Page).IncludeStyleSheet("~/css/Tabs.css");
            if (Page.IsPostBack)
            {

                if (m_hiddenCmd.Value == "deleteShippingTemplate")
                {
                    onDeleteShippingTemplateClicked();
                }
                else if (m_hiddenCmd.Value == "saveEnabledStandardTemplates")
                {
                    throw new NotSupportedException();
                }
                else if (m_hiddenCmd.Value == "deleteFolder")
                {
                    DeleteFolderClick();
                }
            }

            if (!Page.IsPostBack)
            {
                ViewState["DocTypeSortExpression"] = "FolderName";
                ViewState["DocTypeSortOrderIsAsc"] = true;
            }

            m_tblDocTypes.Visible = m_tblShippingTemplates.Visible = m_tblFolders.Visible = m_tblStackOrder.Visible = m_Autosave.Visible = false;
            m_brokerDB = LendersOffice.Admin.BrokerDB.RetrieveById(LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            this.docuSignEnabled = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(this.m_brokerDB.BrokerID)?.DocuSignEnabled ?? false;
            this.validBrokerDocTypes = new HashSet<int>(EDocumentDocType.GetDocTypesByBroker(m_brokerDB.BrokerID, E_EnforceFolderPermissions.True).Select(doctype => doctype.Id));

            if (PageNumber == 0)
            {
                BindFolders();
                folderTab.Attributes.Add("class", "selected");
            }
            else if (PageNumber  == 1)
            {
                if (m_brokerDB.IsDocMagicBarcodeScannerEnabled)
                {
                    m_assignedDocMagicDocTypesByDocTypeID = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.DocMagic, m_brokerDB.BrokerID).GetBrokerDocTypeIdsWithDocVendorMappings();
                }
                if (m_brokerDB.IsDocuTechBarcodeScannerEnabled)
                {
                    m_assignedDocuTechDocTypesByDocTypeID = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.DocuTech, m_brokerDB.BrokerID).GetBrokerDocTypeIdsWithDocVendorMappings();
                }
                if (m_brokerDB.IsIDSBarcodeScannerEnabled)
                {
                    m_assignedIDSDocTypesByDocTypeID = DocTypeMapperFactory.Create(E_DocBarcodeFormatT.IDS, m_brokerDB.BrokerID).GetBrokerDocTypeIdsWithDocVendorMappings();
                }

                BindDocTypes();
                docTypeTab.Attributes.Add("class", "selected");
            }
            else if (PageNumber == 2)
            {
                BindStackOrder();
                stackOrderTab.Attributes.Add("class", "selected");
            }
            else if (PageNumber == 3)
            {
                BindShippingTemplates();
                shipTemplateTab.Attributes.Add("class", "selected");
            }
            else if (PageNumber == 4)
            {
                BindAutosave();
                autosaveTab.Attributes.Add("class", "selected");
            }
            IncludeStyleSheet("~/css/Tabs.css");
        }

        private void BindStackOrder()
        {
            m_tblStackOrder.Visible = true;
            IEnumerable<DocType> brokerOrderedDocTypes = EDocumentDocType.GetStackOrderedDocTypesByBroker(m_brokerDB);
            m_StackingOrder.DataBind(brokerOrderedDocTypes);
        }

        private void BindAutosave()
        {
            m_Autosave.Visible = true;
        }

        
        private void BindFolders()
        {
            m_tblFolders.Visible = true;

            m_dgFolders.DataSource = EDocumentFolder.GetFoldersInBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            m_dgFolders.DataBind();
        }


        private void DeleteFolderClick() {
            int id = Convert.ToInt32(m_hiddenId.Value);
            EDocumentFolder.DeleteFolder(id, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            BindFolders();
        }
        private void BindDocTypes()
        {
            m_dgDocTypes.DataSource = SortedDocTypeList();
            int m_dgDocTypes_DocMagicColumn = 2;
            int m_dgDocTypes_DocuTechColumn = 3;
            int m_dgDocTypes_IDSColumn      = 4;

            if (!m_brokerDB.IsDocMagicBarcodeScannerEnabled)
            {
                m_dgDocTypes.Columns[m_dgDocTypes_DocMagicColumn].Visible = false;
            } 
            if (!m_brokerDB.IsDocuTechBarcodeScannerEnabled)
            {
                m_dgDocTypes.Columns[m_dgDocTypes_DocuTechColumn].Visible = false;
            }
            if (!m_brokerDB.IsIDSBarcodeScannerEnabled)
            {
                m_dgDocTypes.Columns[m_dgDocTypes_IDSColumn].Visible = false;
            }
            if (!docuSignEnabled)
            {
                m_dgDocTypes.Columns[m_dgDocTypes_ESignDocumentTargetColumn].Visible = false;
                m_savePageBottom.Visible = false;
                m_savePageTop.Visible = false;
            }

            //If all are disabled, don't bind assigned doc types
            if (!m_brokerDB.IsDocMagicBarcodeScannerEnabled && !m_brokerDB.IsDocuTechBarcodeScannerEnabled 
                && !m_brokerDB.IsIDSBarcodeScannerEnabled && !docuSignEnabled)
            {
                m_dgDocTypes.ItemDataBound -= OnDataBound_m_dgDocTypes;
            }

            m_dgDocTypes.DataBind();

            m_tblDocTypes.Visible = true;            
        }

        private void BindShippingTemplates()
        {
            EDocumentShippingTemplate docType = new EDocumentShippingTemplate();
            m_dgCustomTemplates.DataSource = docType.BrokerShippingTemplateList;
    

            m_dgCustomTemplates.DataBind();

            m_tblShippingTemplates.Visible = true;
        }

        private void onDeleteShippingTemplateClicked()
        {
            m_hiddenCmd.Value = "";
            EDocumentShippingTemplate st = new EDocumentShippingTemplate();
            st.DeleteShippingTemplate(Convert.ToInt32(m_hiddenId.Value));
        }

        protected void OnDataBound_m_dgFolders(object sender, DataGridItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            EDocumentFolder folder = (EDocumentFolder)args.Item.DataItem;

            EncodedLiteral folderName = (EncodedLiteral)args.Item.FindControl("FolderName");
            EncodedLiteral roleAccess = (EncodedLiteral)args.Item.FindControl("RoleAccess");
            HtmlAnchor editAnchor = (HtmlAnchor)args.Item.FindControl("EditFolder");
            HtmlAnchor deleteAnchor = (HtmlAnchor)args.Item.FindControl("DeleteFolder");

            folderName.Text = folder.FolderNm;
            roleAccess.Text = folder.RoleListDescString;

            if ( folder.IsUnclassifiedFolder )
            {
                editAnchor.Visible = false;
                deleteAnchor.Visible = false;
                folderName.Text += " (default)";
                return;
            }

            editAnchor.Attributes.Add("onclick", "onEditFolder(" + AspxTools.JsNumeric(folder.FolderId) + ")");
            if (folder.IsSystemFolder)
            {
                deleteAnchor.Visible = false;
            }
            else
            {
                deleteAnchor.Attributes.Add("onclick", "onDeleteFolder(" + AspxTools.JsNumeric(folder.FolderId) + ")");
            }
            //add roles

        }

        protected void CustomDocTypes_OnSort(object sender, DataGridSortCommandEventArgs args)
        {
         

            if (args.SortExpression == (string)ViewState["DocTypeSortExpression"])
            {
                ViewState["DocTypeSortOrderIsAsc"] = !((bool)ViewState["DocTypeSortOrderIsAsc"]);
            }
            else
            {
                ViewState["DocTypeSortExpression"] = args.SortExpression;
                ViewState["DocTypeSortOrderIsAsc"] = true;
            }

            BindDocTypes();

        }

        /// <summary>
        /// Binds the barcode scanner (DocMagic, DocuTech, IDS) and ESign doc type mapping columns if the user has permission to view those.
        /// Note that this event handler is REMOVED and not run if the conditions above are not met.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="args">Event arguments.</param>
        protected void OnDataBound_m_dgDocTypes(object sender, DataGridItemEventArgs args)
        {
            // currently this won't get run if user doesn't have permission to view doc magic doc types.
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            DocType docType = (DocType)args.Item.DataItem;
            int DocTypeId = Int32.Parse(docType.DocTypeId);

            IEnumerable<IDocVendorDocType> assignedDocMagicDocTypes;
            if (m_assignedDocMagicDocTypesByDocTypeID != null && m_assignedDocMagicDocTypesByDocTypeID.Contains(DocTypeId))
            {
                assignedDocMagicDocTypes = m_assignedDocMagicDocTypesByDocTypeID[DocTypeId].OrderBy(p => p.Description);
            }
            else
            {
                assignedDocMagicDocTypes = new List<IDocVendorDocType>();
            }

            EncodedLiteral assignedDocMagicDocTypesCellLiteral =
                (EncodedLiteral)args.Item.FindControl("assignedDocMagicDocTypesCellLiteral");
            assignedDocMagicDocTypesCellLiteral.Text =
                string.Join(", ", assignedDocMagicDocTypes.Select(p => p.Description).ToArray());


            IEnumerable<IDocVendorDocType> assignedDocuTechDocTypes;
            if (m_assignedDocuTechDocTypesByDocTypeID != null && m_assignedDocuTechDocTypesByDocTypeID.Contains(DocTypeId))
            {
                assignedDocuTechDocTypes = m_assignedDocuTechDocTypesByDocTypeID[DocTypeId];
            }
            else
            {
                assignedDocuTechDocTypes = new List<IDocVendorDocType>();
            }

            EncodedLiteral assignedDocuTechDocTypesCellLiteral =
                (EncodedLiteral)args.Item.FindControl("assignedDocuTechDocTypesCellLiteral");
            assignedDocuTechDocTypesCellLiteral.Text =
                string.Join(", ", assignedDocuTechDocTypes.Select(p => p.Description).ToArray());

            IEnumerable<IDocVendorDocType> assignedIDSDocTypes;
            if (m_assignedIDSDocTypesByDocTypeID != null && m_assignedIDSDocTypesByDocTypeID.Contains(DocTypeId))
            {
                assignedIDSDocTypes = m_assignedIDSDocTypesByDocTypeID[DocTypeId];
            }
            else
            {
                assignedIDSDocTypes = new List<IDocVendorDocType>();
            }

            EncodedLiteral assignedIDSDocTypesCellLiteral =
                (EncodedLiteral)args.Item.FindControl("assignedIDSDocTypesCellLiteral");
            assignedIDSDocTypesCellLiteral.Text =
                string.Join(", ", assignedIDSDocTypes.Select(p => p.Description).ToArray());

            DocTypePickerControl esignTargetDocType =
                args.Item.FindControl("esignTargetDocType") as DocTypePickerControl;
            if (esignTargetDocType != null)
            { 
                esignTargetDocType.DefaultText = "None";
                esignTargetDocType.LinkText = "select";
                bool targetDocTypeIsValid = docType.ESignTargetDocTypeId != null && validBrokerDocTypes.Contains(docType.ESignTargetDocTypeId.Value);
                if (targetDocTypeIsValid)
                {
                    esignTargetDocType.EnableClearLink();
                    esignTargetDocType.Select(docType.ESignTargetDocTypeId.Value);
                }
            }
            else
            {
                m_dgDocTypes.Columns[m_dgDocTypes_ESignDocumentTargetColumn].Visible = false;
                m_savePageBottom.Visible = false;
                m_savePageTop.Visible = false;
            }
        }

        private IEnumerable<DocType> SortedDocTypeList()
        {

            EDocumentDocType docType = new EDocumentDocType();
            //do not show doctypes part of system folders.
            IEnumerable<DocType> data = EDocumentDocType.GetDocTypesByBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId, E_EnforceFolderPermissions.True).Where(p => p.Folder.IsSystemFolder == false).ToList();
            

            Func<DocType, string> ordering;
            var viewStateSortExpression = (string)ViewState["DocTypeSortExpression"];
            
            if (viewStateSortExpression == "DocTypeName")
            {
                ordering = p => p.DocTypeName;
            }
            else if (viewStateSortExpression == "AssignedDocMagicDocuments")
            {
                if (false == m_brokerDB.IsDocMagicBarcodeScannerEnabled)
                {
                    string exc = "Can't sort by doc magic doc if don't have permission to do so.";
                    throw new CBaseException(exc, exc);
                }

                ordering = p => m_assignedDocMagicDocTypesByDocTypeID.Contains(Int32.Parse(p.DocTypeId)) ? string.Join(", ",
                    m_assignedDocMagicDocTypesByDocTypeID[Int32.Parse(p.DocTypeId)].Select(
                    x=>x.Description).ToArray()) : "";
            }
            else if (viewStateSortExpression == "AssignedDocuTechDocuments")
            {
                if (false == m_brokerDB.IsDocuTechBarcodeScannerEnabled)
                {
                    string exc = "Can't sort by DocuTech doc if don't have permission to do so.";
                    throw new CBaseException(exc, exc);
                }

                ordering = p => m_assignedDocuTechDocTypesByDocTypeID.Contains(Int32.Parse(p.DocTypeId)) ? string.Join(", ",
                    m_assignedDocuTechDocTypesByDocTypeID[Int32.Parse(p.DocTypeId)].Select(
                    x => x.Description).ToArray()) : "";
            }
            else if (viewStateSortExpression == "AssignedIDSDocuments")
            {
                if (false == m_brokerDB.IsIDSBarcodeScannerEnabled)
                {
                    string exc = "Can't sort by IDS doc if don't have permission to do so.";
                    throw new CBaseException(exc, exc);
                }

                ordering = p => m_assignedIDSDocTypesByDocTypeID.Contains(Int32.Parse(p.DocTypeId)) ? string.Join(", ",
                    m_assignedIDSDocTypesByDocTypeID[Int32.Parse(p.DocTypeId)].Select(
                    x => x.Description).ToArray()) : "";
            }
            else // if (viewStateSortExpression == "FolderName")
            {
                ordering = p => p.Folder.FolderNm;
            }

            if ((bool)ViewState["DocTypeSortOrderIsAsc"])
            {
                data = data.OrderBy(ordering);
            }
            else 
            {
                data = data.OrderByDescending(ordering);
            }

            return data;
        }

    }

}