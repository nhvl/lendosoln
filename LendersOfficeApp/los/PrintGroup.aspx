<%@ Page language="c#" Codebehind="PrintGroup.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.PrintGroup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head runat="server">
    <title>PrintGroup</title>
  </head>
  <script language=javascript>
        $(function(){
          var w = 1024;
          var h = 768;
          resize(w, h);
        });
        

        function closeMe() {
            onClosePopup();
        }
  </script>
  <body class="body-iframe">
    <h4 class="page-header">Print Groups</h4>
    <form runat="server">
      <div class="left">
        <iframe name=list src='PrintGroupList.aspx'></iframe>
      </div>
      <div class="right">
        <iframe name=edit src='PrintGroupBlankPage.aspx'></iframe>
      </div>
    </form>
  </body>  
</html>
