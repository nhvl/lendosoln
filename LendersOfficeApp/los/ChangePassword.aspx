<%@ Page language="c#" Codebehind="ChangePassword.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.ChangePassword" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="common/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="common/footer.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>LendingQB</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<link href="../css/stylesheet.css" type="text/css" rel="stylesheet">    
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="FlowLayout" class="bgcolor1" bottommargin="0">
  <script language=javascript>
  <!--
    function validatePassword(source, args) {

        var password1 = document.forms[0].elements["m_newPasswordTB"].value;
        var confirmpassword = document.forms[0].elements["m_confirmPasswordTB"].value;
        if (password1 != confirmpassword) {
            args.IsValid = false;
        } else {
            args.IsValid = true;
        }
    }
    function logOut() {
      location.href = <%= AspxTools.JsString(VirtualRoot) %> + '/logout.aspx';
    }
  //-->
  </script>
  
    <form id="ChangePassword" method="post" runat="server">
      <table height="100%" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
          <td><uc1:header id=Header1 runat="server"></uc1:header></td>
        </tr>
        <tr>
          <td height="100%" align="middle" valign="center">
            <table id=Table1 cellspacing=0 cellpadding=5 border=0>
              <tr>
                <td nowrap colspan=2 align="middle">Your password has expired.</td>
              </tr>
              <tr>
                <td nowrap colspan=2><asp:RegularExpressionValidator id=RegularExpressionValidator1 runat="server" ErrorMessage="Password needs to be at least 6 characters in length." ValidationExpression="(.){6,}" Display="Dynamic" ControlToValidate="m_newPasswordTB"></asp:RegularExpressionValidator><asp:CustomValidator id=m_comparePasswordValidator runat="server" ErrorMessage="Passwords do not match." Display="Dynamic" ClientValidationFunction="validatePassword" controltovalidate="m_confirmPasswordTB"></asp:CustomValidator><asp:CustomValidator id=m_compareOldPasswordValidator runat="server" ErrorMessage="Old password does not match." onservervalidate="m_compareOldPasswordValidator_ServerValidate"></asp:CustomValidator></td>
              </tr>
              <tr>
                <td nowrap>Old Password</td>
                <td nowrap><asp:TextBox id=m_oldPasswordTB runat="server" TextMode="Password"></asp:TextBox></td>
              </tr>
              <tr>
                <td nowrap>New Password</td>
                <td nowrap><asp:TextBox id=m_newPasswordTB runat="server" TextMode="Password"></asp:TextBox></td>
              </tr>
              <tr>
                <td nowrap>Retype Password</td>
                <td nowrap><asp:TextBox id=m_confirmPasswordTB runat="server" TextMode="Password"></asp:TextBox></td>
              </tr>
        <tr>
          <td nowrap align=middle colSpan=2 rowSpan=""><ml:nodoubleclickbutton id=m_submitBtn runat="server" Text="Change password" onclick="m_submitBtn_Click"></ml:nodoubleclickbutton></td></tr>
            </table>
          </td>
        </tr>
        <tr>
          <td valign="center" bgcolor="black" height="1%"><uc1:footer id=Footer1 runat="server"></uc1:footer></td>
        </tr>
      </table>

     </form>
	
  </body>
</html>
