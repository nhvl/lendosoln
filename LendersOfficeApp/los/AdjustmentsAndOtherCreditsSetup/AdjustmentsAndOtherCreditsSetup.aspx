﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdjustmentsAndOtherCreditsSetup.aspx.cs" Inherits="LendersOfficeApp.los.AdjustmentsAndOtherCreditsSetup.AdjustmentsAndOtherCreditsSetup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .adjustmentsDdl {
            display: block !important;
        }
        .fromDdl, .toDdl {
            display: block !important;
        }    
        #adjustmentsTable {
            color:black;
            margin: auto;
            text-align: center;
        }
        .width260 {
            width: 260px;
        }
        .width145{
            width: 145px;
        }
        .width50 {
            width: 50px;
        }
        #LQBPopupDiv {
            background-color: gainsboro;
        }
        #bodyClass {
            background-color: gainsboro;
            margin: 3px;
        }
        #saveCancelBtns {
            float: right;
            margin-right:30px;
        }
        .addBtn{
            float: left;
        }
        .deleteBtn{
            font-weight: bold;
            font-size: 13px;
            width: 30px;
        }
        .btn {
            width: 60px;
            margin: 10px;
        }
        .redBorder {
            border-color: red;
        }
        .closer-denied .hide-dflp{
            display: none;
        }
    </style>

    <script id="setupTemplate" type="text/x-jquery-tmpl">
        <table id="adjustmentsTable">
            <tr>
                <th class="width260">
                    Type
                </th>
                <th class="width260">
                    Description
                </th>
                <th class="width145">
                    From
                </th>
                <th class="width145">
                    To
                </th>
                <th class="width50">
                    POC
                </th>
                <th>
                    Include in 1003 Details
                </th>
                <th class="width50 hide-dflp">
                    DFLP
                </th>
                <th class="width50">
                </th>
            </tr>
            {{each(i) SerializedAdjustments}}
            <tr>
                <td>
                    <select class="adjustmentsDdl width260" onchange="adjustmentsDdlChange('${i}', false);" id="dropdownOptions">
                        {{each(i) SelectableAdjustments}}
                            <option value="${SelectableAdjustments[i].Item1}">${SelectableAdjustments[i].Item2}</option>
                        {{/each}}
                    </select>
                </td>
                <td>
                    <input type="text" class="description width260" maxlength="60" onchange="descriptionsChange('${i}');"/>
                </td>
                <td>
                    <select class="fromDdl" onchange="partyDdlChange('${i}');">
                        <option value="0"></option>
                        <option value="1">Borrower</option>
                        <option value="2">Seller</option>
                        <option value="3">Lender</option>
                        <option value="4">Employer</option>
                        <option value="5">Parent</option>
                        <option value="6">Relative</option>
                        <option value="11">Friend</option>
                        <option value="7">Federal Agency</option>
                        <option value="8">Builder or Developer</option>
                        <option value="9">Real Estate Agent</option>
                        <option value="10">Other</option>
                    </select>
                </td>
                <td>
                    <select class="toDdl" onchange="partyDdlChange('${i}');">
                        <option value="0"></option>
                        <option value="1">Borrower</option>
                        <option value="2">Seller</option>
                        <option value="3">Lender</option>
                        <option value="4">Employer</option>
                        <option value="5">Parent</option>
                        <option value="6">Relative</option>
                        <option value="11">Friend</option>
                        <option value="7">Federal Agency</option>
                        <option value="8">Builder or Developer</option>
                        <option value="9">Real Estate Agent</option>
                        <option value="10">Other</option>
                    </select>
                </td>
                <td>
                    <input type="checkbox" class="POC" onclick="pocChanged('${i}');"/>
                </td>
                <td>
                    <input type="checkbox" class="include1003" onchange="markDirty('${i}');"/>
                </td>
                <td>
                    <input type="checkbox" class="DFLP hide-dflp" onclick="dflpChanged('${i}');"/>
                </td>
                <td>
                    <input type="button" value="-" onclick="deleteRow('${i}')" class="deleteBtn"/>
                </td>
            </tr>            
            {{/each}}
            <tr>
                <td>
                    <div class="addBtn">
                        <input type="button" value="Add" onclick="onAdd();" class="btn"/>
                    </div>
                </td>
            </tr>
        </table>
        <div id="saveCancelBtns">
            <input type="button" value="Save" onclick="onSave();" class="btn"/>
            <input type="button" value="Cancel" onclick="onPopupCancel();" class="btn"/>
        </div>
    </script>

    <script type="text/javascript">
        $(function () {
            $("#setupTemplate").tmpl(model).appendTo("#setupDiv");
            renderData();
            $.each(model.SerializedAdjustments, function (index) {
                adjustmentsDdlChange(index, true);
                model.SerializedAdjustments[index].dirty = false; // don't set dirty on init
            });
        });
    </script>
</head>
<body id="bodyClass" runat="server">    
    <h4 class="page-header">Adjustments and Other Credits Setup</h4>
    <form id="form1" runat="server">
        <div id="setupDiv"></div>
    </form>
</body>
</html>
