﻿#region Auto-generated code
namespace LendersOfficeApp.los.AdjustmentsAndOtherCreditsSetup
#endregion
{
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.Security;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Service page for predefined adjustments setup page.
    /// </summary>
    public partial class AdjustmentsAndOtherCreditsSetupService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Runs the service method called from the page.
        /// </summary>
        /// <param name="methodName">The method to run.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(this.Save):
                    this.Save();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, $"Method not implemented {methodName}");
            }
        }

        /// <summary>
        /// Saves predefined adjustments returned from the setup page.
        /// </summary>
        private void Save()
        {
            List<PredefinedAdjustment> addedAdjustments = SerializationHelper.JsonNetDeserialize<List<PredefinedAdjustment>>(GetString("addedAdjustments"));
            List<PredefinedAdjustment> updatedAdjustments = SerializationHelper.JsonNetDeserialize<List<PredefinedAdjustment>>(GetString("updatedAdjustments"));
            List<int> deletedList = SerializationHelper.JsonNetDeserialize<List<int>>(GetString("deletedList"));

            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            bool isDflpEnabled = PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserWrite);

            PredefinedAdjustment.SavePredefinedAdjustments(addedAdjustments, updatedAdjustments, deletedList, brokerId, isDflpEnabled);
        }
    }
}