﻿#region Auto-generated code
namespace LendersOfficeApp.los.AdjustmentsAndOtherCreditsSetup
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Web.UI;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.Security;

    /// <summary>
    /// Page that lets you setup adjustments and other credits page information.
    /// </summary>
    public partial class AdjustmentsAndOtherCreditsSetup : LendersOffice.Common.BaseServicePage
    {
        /// <summary>
        /// The load function.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">EventArgs e.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            List<PredefinedAdjustment> adjList = PredefinedAdjustment.GetAllAdjustmentsForBroker(PrincipalFactory.CurrentPrincipal.BrokerId);
            ReadOnlyDictionary<E_AdjustmentT, PredefinedAdjustment> predefinedAdjustmentTypeMap = AdjustmentsAndOtherCreditsData.PredefinedAdjustmentTypeMap;
            List<Tuple<string, string>> selectableAdjustments = AdjustmentsAndOtherCreditsData.CreateAdjustmentSetupDrowndownList();

            AdjustmentsAndOtherCreditsSetupViewModel model = new AdjustmentsAndOtherCreditsSetupViewModel();
            model.SerializedAdjustments = adjList;
            model.SelectableAdjustments = selectableAdjustments;

            this.RegisterJsObjectWithJsonNetSerializer("model", model);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("predefinedAdjustmentTypeMap", predefinedAdjustmentTypeMap);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("predefinedAdjustmentStringMap", AdjustmentsAndOtherCreditsData.PredefinedAdjustmentsStringMap);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("adjustmentTypeEnums", AdjustmentsAndOtherCreditsData.AdjustmentTypeEnums);

            // We don't want users who don't have access to the funding folder
            // to see the DFLP checkbox. It was clarified that we do *not* need to
            // prevent users who lack the permission from ever being able to see
            // the value. It is sufficient to just hide the data. From Geoff
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowCloserRead))
            {
                bodyClass.Attributes["class"] = "closer-denied";
            }
        }

        /// <summary>
        /// The code that runs when the page is initialized.
        /// </summary>
        protected void Page_Init()
        {
            this.RegisterService("adjustments", "/los/AdjustmentsAndOtherCreditsSetup/AdjustmentsAndOtherCreditsSetupService.aspx");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("jquery.tmpl.js");
            this.RegisterJsScript("AdjustmentsAndOtherCreditsSetup.js");
            this.EnableJqueryMigrate = false;
        }
    }    
} 