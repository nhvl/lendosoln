using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using System.Collections.Generic;

namespace LendersOfficeApp.los
{
	public partial class LoanTemplateList : LendersOffice.Common.BasePage
	{
        protected bool m_displayNoTemplateMessage = false;

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal;}
		}
        private Guid RetrievePmlLoanTemplateID() 
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(BrokerUser.BrokerId);

            return brokerDB.PmlLoanTemplateID;
        }

        protected string Purpose
        {
            get { return RequestHelper.GetSafeQueryString("purpose"); }
        }

        protected bool IsTest
        {
            get { return RequestHelper.GetBool("test"); }
        }

        protected bool IsLead
        {
            get { return RequestHelper.GetBool("islead"); }
        }

        protected void AddRowAccountExecutiveTemplates(Guid sLId, DataTable table)
        {
            SqlParameter[] parameters = {
                                                new SqlParameter("@LoanId", sLId)
                                            };
            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "GetLoanNumberByLoanID", parameters))
            {
                if (sR.Read() == true)
                {
                    object[] values = { sLId, sR["sLNm"] };
                    table.Rows.Add(values);
                }
            }
        }

		protected void PageLoad( object sender , System.EventArgs a )
		{
            if (IsLead && !BrokerUser.BrokerDB.IsEditLeadsInFullLoanEditor)
            {
                throw new GenericUserErrorMessageException("Trying to create lead from template without lender setting IsEditLeadsInFullLoanEditor.");
            }

            #region OPM 2288 - If user with only AE role then it must create new loan via pml template
            if (BrokerUser.IsAccountExecutiveOnly)
            {
                //get all the pml template ids, and determine if they're unique or not
                BrokerDB brokerDB = BrokerDB.RetrieveById(BrokerUser.BrokerId);

                HashSet<Guid> set = new HashSet<Guid>();
                set.Add(brokerDB.PmlLoanTemplateID);
                set.Add(brokerDB.MiniCorrLoanTemplateID);
                set.Add(brokerDB.CorrLoanTemplateID);

                if (set.Count <= 1)
                {
                    Guid templateID = RetrievePmlLoanTemplateID();
                    Response.Redirect("~/newlos/LoanCreate.aspx?type=loan&templateid=" + templateID);
                }
                DataTable table = new DataTable();
                table.Columns.Add("sLId", typeof(Guid));
                table.Columns.Add("sLNm", typeof(string));

                foreach (Guid id in set)
                {
                    AddRowAccountExecutiveTemplates(id, table);
                }

                m_templateRepeater.DataSource = table.DefaultView;
                m_templateRepeater.DataBind();

                m_blankPanel.Visible = false;
            }

            #endregion
            else
            {
                if (!Page.IsPostBack)
                {
                    // Just as we now limit inidividual's loan assignment options to
                    // those who are employees of the branch, we keep users from
                    // using templates that were originated / designed in another
                    // branch.  This will help limit remote officers' choices and
                    // keep the new loans that they author to a broker-sanitized
                    // set.  This permission is mainly in place to facilitate the
                    // central-processing-hub-with-remote-officers model.

                    DataSet dS = new DataSet();

                    bool filterByBranch = true;

                    if (BrokerUser.HasPermission(Permission.AllowAccessToTemplatesOfAnyBranch))
                    {
                        filterByBranch = false;
                    }

                    SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerId", BrokerUser.BrokerId)
                                                    , new SqlParameter("@BranchId", BrokerUser.BranchId)
                                                    , new SqlParameter("@EmployeeId", BrokerUser.EmployeeId)
                                                    , new SqlParameter("@FilterByBranch", filterByBranch)
                                                };

                    DataSetHelper.Fill(dS, BrokerUser.BrokerId, "RetrieveLoanTemplateByBrokerID", parameters);

                    if (dS.Tables.Count > 0)
                    {
                        dS.Tables[0].DefaultView.Sort = "sLNm ASC"; // 10/16/06 OPM 7843 mf
                        var view = dS.Tables[0].DefaultView;

                        if (!string.IsNullOrEmpty(Purpose))
                        {
                            if (Purpose.ToLower() == "purchase")
                            {
                                view.RowFilter = "sLPurposeT = 0 AND sIsLineOfCredit=0";
                            }
                            else if (Purpose.ToLower() == "refinance")
                            {
                                view.RowFilter = "sIsLineOfCredit=0 AND (sLPurposeT = 1" // Refin
                                               + " OR sLPurposeT = 2" // RefinCashout
                                               + " OR sLPurposeT = 6" // FHA Streamline Refi
                                               + " OR sLPurposeT = 7" // VaIrrrl
                                               + " OR sLPurposeT = 8)"; // Home Equity
                            }
                            else if (Purpose.Equals("HELOC", StringComparison.OrdinalIgnoreCase))
                            {
                                view.RowFilter = "sIsLineOfCredit=1";
                            }
                            else if (Purpose.ToLower() == "construction")
                            {
                                view.RowFilter = "sIsLineOfCredit = 0 AND (sLPurposeT = 3 OR sLPurposeT = 4)"; // Construction OR Construction Perm
                            }
                        }

                        m_templateRepeater.DataSource = view;
                        m_templateRepeater.DataBind();
                    }

                    // 3/28/2005 kb - Hide the blank loan option if user's should
                    // not select it.
                    BrokerDB brokerDB = BrokerDB.RetrieveById(BrokerUser.BrokerId);

                    m_displayNoTemplateMessage = brokerDB.IsBlankTemplateInvisibleForFileCreation && m_templateRepeater.Items.Count == 0;
                    m_blankPanel.Visible = !brokerDB.IsBlankTemplateInvisibleForFileCreation;
                }
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion


	}
}
