<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeeService_ConditionGenerator.aspx.cs" Inherits="LendersOfficeApp.los.FeeService.FeeService_ConditionGenerator" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="CCTemplateGFE2010" Src="../Template/CCTemplateGFE2010.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>HUD Line Field Set and Condition Generator</title>
    <style type="text/css">
        body
        {
            background-color: gainsboro;
        }
        #AvailableFields, #SelectedFields
        {
            height: 100px;
            width: 250px;
            overflow: auto;
            border: 1px solid black;
            background-color: White;
        }
        #AvailableFields a.up, #AvailableFields a.down
        {
            display:none;
            height: 15px;
        }
        a.up, a.down
        {
            text-decoration: none;
        }
        a.up img, a.down img
        {
            height: 9px;
        }
        #AvailableFields ul, #SelectedFields  ul
        {
            list-style: none;
            padding: 0 0 0 10px;
        }

        div.field-section
        {
            display: inline-block;
            vertical-align: top;
        }

        div.paramDiv
        {
            width: 190px;
            display: inline-block;
        }

        div.field-section select
        {
            height: 100px;
            width: 175px;
            overflow:auto;
        }
        span.combination
        {
            vertical-align: 70px;
            margin-left: 10px;
        }
        .EscrowInfo > td
        {
            padding-left: 100px;
        }
        .EscrowInfo .ReserveMonthsLabel
        {
            padding-right: 10px;
        }
        .EscrowInfo .Cushion
        {
            margin-left: 5px;
        }
        .EscrowInfo .Center
        {
            text-align: center;
        }
        .EscrowInfo .DisbursementScheduleLabel
        {
            padding-left: 180px;
        }
        .ExpenseTable
        {
            margin: 1px 0px;
        }
        input.month
        {
            width: 30px;
        }
        input.money[preset='money']
        {
            width: 90px;
            text-align: right;
        }
        input.percent[preset='percent']
        {
            width: 70px;
        }

        #noFeeProvidersWarning {
            color: red;
            font-weight: bold;
        }

        #titleVendorRequired {
            display: none;
        }
    </style>
</head>
<body>
    <script type="text/javascript" >
        function genBtnCallback(tsv) {
            copyStringToClipboard(tsv);
            alert("HUD line's field set and conditions have been copied to your windows clipboard.");
        }

        function errorCallback(e) {
            if (e instanceof FeeServiceValidationError) {
                alert(e.message);
                return;
            }
            else {
                throw e;
            }
        }

        $(function() {
            var $fields = $('#trFields');
            var $parameters = $('#trParameters');
            var $availableFieldsUl = $('#AvailableFields ul'), $selectedFieldsUl = $('#SelectedFields ul');

            var $rows = $('table[data-fsid=outerTable] tr, h4.page-header');
            var $GfeLines = $('table[data-fsid=GfeLines] tr');

            var $cells = $GfeLines.find('td');
            $cells.attr('nowrap', true);

            var $SelectedRow;

            resize(1325, 550);
            oncreditupdate(document.getElementById(ctrlName + 'cPricingEngineCreditT'));

            enableDisableSelection();
            showHideParameterSelection();
            hideAllGfeRows();

            // Hide all parameter value selection divs
            $('.field-section').hide();

            $('#closeBtn').click(function() { onClosePopup(); });

            $('.hasConditions').click(function() { enableDisableSelection(); });

            $('#genBtn').click(function() {
                if (!validate())
                    return;

                var tsv;
                var gfeType = $(".gfeType").val();

                try {
                    if (gfeType == "OLD_GFE") {
                        var lineNumber = $('.hudLine').val();
                        OldGfe_GenerateTSV(lineNumber, $SelectedRow, false, genBtnCallback);
                    } else { // gfeType == "NEW_GFE"
                        NewGfe_GenerateTSV($SelectedRow, genBtnCallback, errorCallback);
                    }
                }
                catch (e) {
                    errorCallback(e);
                }
            });

            $('.hudLine').change(function() {
                hideAllGfeRows();
                $SelectedRow = null;

                var val = this.value;
                if (val) {
                    $SelectedRow = $GfeLines.filter('[data-fsid=' + val + ']').show();

                    if (val != 'Options') {
                        $GfeLines.filter('[data-fsid=Legend]').show();
                    } else {
                        $(".trWarning").toggle(ML.IsEnableNewFeeService);
                    }
                }
            });

            $('.gfeType').change(function() {
                hideAllGfeRows();

                var val = this.value;
                if (val == 'OLD_GFE') {
                    $('.oldGfe').show();
                    $('.newGfe').hide();

                    $(".hudLine").trigger("change");
                }
                else { // val == 'NEW_GFE'
                    $('.oldGfe').hide();
                    $('.newGfe').show();

                    $(".feeType").trigger("change");
                }
            });

            $('.feeType').change(function() {
                // Change remove message
                var msg = "Remove <fee> if it exists on the loan"
                var feeDesc = $(".feeType").children(':selected').text();
                $('.removeFeeNote').text(msg.replace("<fee>", feeDesc))

                // Set Fee Type defaults to template
                clearFeeTemplate();

                var val = this.value;
                var text = $(this).find('option:selected').text().toLowerCase();

                // if blank option selected, just clear template
                if (!val) {
                    return;
                }

                if (val == 'Options') {
                    $SelectedRow = $GfeLines.filter('[data-fsid=' + val + ']').show();
                    return;
                }

                if (val == "LENDER_CREDITS") {
                    $('.lenderCredits').show();
                    $('.sLenderCreditCalculationMethodT').trigger("change");
                    return;
                }

                if (ML.EnableEnhancedTitleQuotes && val == 'TITLE_SOURCE') {
                    // Set fee type span.
                    var titleFeeType;
                    var label = 'What is the source of <titleFeeType> Fees?'
                    switch (text) {
                        case "title fees source":
                            titleFeeType = 'Title';
                            break;
                        case 'closing fees source':
                            titleFeeType = 'Closing';
                            break;
                        case 'recording fees source':
                            titleFeeType = 'Recording';
                            break;
                    }

                    $('.TitleSourceLabel').text(label.replace("<titleFeeType>", titleFeeType));

                    // Show warning message if only option is blank.
                    $('#noFeeProvidersWarning').toggle($(".TitleVendor option").length == 0);

                    $('.titleSource').show();
                    clearTitleSource();
                    return;
                }

//                // Show Aggregate Escrow UI
//                if (val == ML.Hud1000AggregateAdjustmentFeeTypeId) {
//                    clearAggregateEscrow();
//                    $('.sAggEscrowCalcModeT').trigger("change");
//                    $('.aggregateEscrow').show();
//                    return;
//                }

                // Show Mortgage Insurance UI
                if (val == ML.Hud1000MortgageInsuranceReserveFeeTypeId) {
                    clearMortgageInsurance();
                    $('.mortgageInsurance').show();
                    return;
                }

                // Show Housing Expenses UI.
                if ($.inArray(val, HousingExpensesFeeTypeIds) != -1) {
                    $(".expName").text(feeDesc);
                    $(".expDesc").val(feeDesc);
                    $(".taxT option:first-child").prop("selected", true);
                    $(".annAmtCalcT option:first-child").prop("selected", true);

                    $('.annAmtCalcT').trigger("change");
                    $('.housingExpenses').show();

                    // Show Tax related fields for taxes.
                    if ($.inArray(val, TaxesFeeTypeIds) != -1) {
                        $('.trTaxes').show();
                    }

                    return;
                }

                // Fee Type selected, so show action drop down.
                $('.trAction').show();

                // Show Fee Template only if Action value is ADD
                if ($('.action').val() == "ADD") {
                    $('.feeTemplate').show();
                }

                // Search for fee type by id and load values into template.
                for (var i = 0; i < ClosingCostData.length; i++) {
                    var type = ClosingCostData[i];
                    if (type.typeid == val) {
                        $(".hudline").text(type.hudline);
                        $(".desc").val(type.desc);
                        $(".org_desc").text(type.desc); // Fee types leave type.org_desc blank.
                        $(".section").val(type.section);
                        $(".apr").prop('checked', type.apr);
                        $(".fha").prop('checked', type.fha);
                        $(".dflp").prop('checked', type.dflp);
                        $(".bene").val(type.bene);
                        $(".tp").prop('checked', type.tp);
                        $(".aff").prop('checked', type.aff);
                        $(".can_shop").prop('checked', type.can_shop);
                        //$(".p").val(type.f.p);
                        //$(".pt").val(type.f.pt);
                        //$(".base").val(type.f.base);
                        //$(".paid_by").val(type.pmts[0].paid_by);
                        //$(".pmt_at").val(type.pmts[0].pmt_at);

                        // Per-diem interest amount should be blank and read only.
                        if (val == ML.Hud900DailyInterestFeeTypeId) {
                            $(".p").val("");
                            $(".pt option:first-child").prop("selected", true);
                            $(".base").val("");
                        }

                        $(".p").prop('readOnly', val == ML.Hud900DailyInterestFeeTypeId);
                        $(".pt").prop('disabled', val == ML.Hud900DailyInterestFeeTypeId);
                        $(".base").prop('readOnly', val == ML.Hud900DailyInterestFeeTypeId);

                        // Originator Compensation Paid By should be borrower paid and read only.
                        if (val == ML.Hud800OriginatorCompensationFeeTypeId) {
                            $(".paid_by").val("1");
                        }

                        $(".paid_by").prop('disabled', val == ML.Hud800OriginatorCompensationFeeTypeId);

                        $('.ssp').toggle(type.disc_sect === <%=AspxTools.JsNumeric(E_IntegratedDisclosureSectionT.SectionC)%>);

                        return;
                    }
                }

                // TODO: Maybe throw an error/alert if type not found. Not really important as both generator and
                // parser can handle this case.
            });

            function clearFeeTemplate() {
                $(".hudline").text("");
                $(".desc").val("");
                $(".org_desc").text(""); // Fee types leave type.org_desc blank.
                $(".section option:first-child").prop("selected", true);
                $(".apr").prop('checked', false);
                $(".fha").prop('checked', false);
                $(".bene option:first-child").prop("selected", true);
                $(".dflp").prop('checked', false);
                $(".tp").prop('checked', false);
                $(".aff").prop('checked', false);
                $(".can_shop").prop('checked', false);
                $(".p").val("0.000%");
                $(".pt option:first-child").prop("selected", true);
                $(".base").val("$0.00");
                $(".paid_by").val("1");
                $(".pmt_at").val("1");
                $(".escrowed").prop('checked', false);

                // Clear description validation marker.
                $(".desc").css("border", "");

                // Hide Escrow Fields
                $(".trEscrowed").hide();
                $SelectedRow = null;
                hideAllGfeRows();

                // Hide Templates
                $('.feeTemplate').hide();
                $('.lenderCredits').hide();
                $('.housingExpenses').hide();
                $('.mortgageInsurance').hide();
                //$('.aggregateEscrow').hide();
                $('.trAction').hide();
                $('.trTaxes').hide();

                clearSspSelection();
                $('.ssp').hide();

                $('.titleSource').hide();
            }

            $('.action').change(function() {
                var val = this.value;
                if (val == 'ADD') {
                    $('.removeFeeNote').hide();
                }
                else { // val == 'REMOVE'
                    $('.removeFeeNote').show();
                }

                $(".feeType").trigger("change");
            });

            $('.sLenderCreditCalculationMethodT').change(function(e) {
                clearLenderCredits();

                var val = this.value;
                $('#FrontEndPanel').toggle(val == 1);
                $('#ManualCalculatePanel').toggle(val == 0);
            });

            function clearLenderCredits() {
                // Set Manually
                $(".sLDiscntPc").val("0.000%");
                $(".sLDiscntBaseT").val("0");
                $(".sLDiscntFMb").val("$0.00");
                $(".sGfeCreditLenderPaidItemT").val("0");
                $(".sLenderCustomCredit1Description2").val("");
                $(".sLenderCustomCredit1Amount2").val("$0.00");
                $(".sLenderCustomCredit2Description2").val("");
                $(".sLenderCustomCredit2Amount2").val("$0.00");
                //$(".sToleranceCureCalculationT").val("0");

                // Calculated from Front-end Rate Lock
                $(".sLDiscntBaseT2").val("0");
                $(".sLenderCreditMaxT").val("0");
                $(".sLenderPaidFeeDiscloseLocationT").val("0");
                $(".sLenderGeneralCreditDiscloseLocationT").val("0");
                $(".sLenderCustomCredit1DiscloseLocationT").val("0");
                $(".sLenderCustomCredit1Description").val("");
                $(".sLenderCustomCredit1Amount").val("$0.00");
                $(".sLenderCustomCredit2DiscloseLocationT").val("0");
                $(".sLenderCustomCredit2Description").val("");
                $(".sLenderCustomCredit2Amount").val("$0.00");
            }

            function clearTitleSource() {
                $(".TitleVendor").val("");
            }

            //            $('.sAggEscrowCalcModeT').change(function(e) {
            //                var val = this.value;
            //
            //                $(".trCustomaryEscrowImpoundsCalcMinT").toggle(val == <%= AspxTools.JsString(E_AggregateEscrowCalculationModeT.Customary) %>);
            //                $(".sCustomaryEscrowImpoundsCalcMinT").val("0");
            //            });

            //            function clearAggregateEscrow() {
            //                $(".sAggEscrowCalcModeT").val("0");
            //                $(".sCustomaryEscrowImpoundsCalcMinT").val("0");
            //            }

            $('.sMInsRsrvMonLckd').change(function(e) {
                var checked = this.checked;

                $(".sMInsRsrvMon").val("0");
                $(".sMInsRsrvMon").prop('readOnly', !checked);
            });

            function clearMortgageInsurance() {
                $(".sMInsRsrvEscrowedTri").prop('checked', false);
                $(".sMInsRsrvEscrowCushion").val("0");
                $(".sMInsRsrvMonLckd").prop('checked', false);
                //$(".sMInsRsrvMon").val("0");

                clearDisbursement($(".sMInsRsrvEscrowDisbursementSchedule"));

                // Set readonly fields.
                $(".sMInsRsrvMonLckd").trigger("change");
            }

            $('.isPrepaid').change(function(e) {
                var checked = this.checked;

                $(".prepMnth").val("0");
                $(".prepMnth").prop('readOnly', !checked);
            });

            $('.rsrvMonLckd').change(function(e) {
                var checked = this.checked;

                $(".rsrvMon").val("0");
                $(".rsrvMon").prop('readOnly', !checked);
            });

            $('.annAmtCalcT').change(function(e) {
                clearHousingExpenses();

                // Toggle readonly fields. Readonly if Disbursements.
                var val = this.value;
                $(".isPrepaid").prop('disabled', val == 1);
                $(".isEscrowed").prop('disabled', val == 1);
                $(".annAmtPerc").prop('readOnly', val == 1);
                $(".annAmtBase").prop('disabled', val == 1);
                $(".monAmtFixed").prop('readOnly', val == 1);
                //$(".prepMnth").prop('readOnly', val == 1);
                $(".Cush").prop('readOnly', val == 1);
                $(".rsrvMonLckd").prop('disabled', val == 1);
                //$(".rsrvMon").prop('readOnly', val == 1);
                $(".RepInterval").prop('disabled', val == 1);

                var $tr = $(".trDisbursements");
                $tr.find(".Jan").prop('readOnly', val == 1);
                $tr.find(".Feb").prop('readOnly', val == 1);
                $tr.find(".Mar").prop('readOnly', val == 1);
                $tr.find(".Apr").prop('readOnly', val == 1);
                $tr.find(".May").prop('readOnly', val == 1);
                $tr.find(".Jun").prop('readOnly', val == 1);
                $tr.find(".Jul").prop('readOnly', val == 1);
                $tr.find(".Aug").prop('readOnly', val == 1);
                $tr.find(".Sep").prop('readOnly', val == 1);
                $tr.find(".Oct").prop('readOnly', val == 1);
                $tr.find(".Nov").prop('readOnly', val == 1);
                $tr.find(".Dec").prop('readOnly', val == 1);
            });

            $('.RepInterval').change(function(e) {
                // Toggle readonly fields. Readonly if Monthly or Annually in Closing Month.
                var val = this.value;
                var $tr = $(".trDisbursements");
                var $monthInputs = $tr.find('.month');

                $monthInputs.prop('readOnly', val == 1 || val == 2);

                // Set monthly values to 1.
                if (val == 1) {
                    $monthInputs.val('1');
                } else if (val == 2) {
                    // When set to annually in closing month, we don't allow
                    // the user to modify the disbursements because the system
                    // will populate them automatically.
                    $monthInputs.val('');
                }
            });

            $('#sMIPaymentRepeat').change(function (e) {
                var val = this.value;
                var $tr = $(".sMInsRsrvEscrowDisbursementSchedule");
                var $monthInputs = $tr.find('.month');

                $monthInputs.prop('readOnly', val == 1 || val == 2);

                // Set monthly values to 1.
                if (val == 1) {
                    $monthInputs.val('1');
                } else if (val == 2) {
                    // When set to annually in closing month, we don't allow
                    // the user to modify the disbursements because the system
                    // will populate them automatically.
                    $monthInputs.val('');
                }
            });

            function clearHousingExpenses() {
                $(".isPrepaid").prop('checked', false);
                $(".isEscrowed").prop('checked', false);
                $(".annAmtPerc").val("0.000%");
                $(".annAmtBase option:first-child").prop("selected", true);
                $(".monAmtFixed").val("$0.00");
                $(".Cush").val("0");
                $(".rsrvMonLckd").prop('checked', false);
                $(".RepInterval option:first-child").prop("selected", true);

                clearDisbursement($(".trDisbursements"));

                // Set readonly fields.
                $(".isPrepaid").trigger("change");
                $(".rsrvMonLckd").trigger("change");
            }

            function clearDisbursement($row) {
                $row.find(".Jan").val("0");
                $row.find(".Feb").val("0");
                $row.find(".Mar").val("0");
                $row.find(".Apr").val("0");
                $row.find(".May").val("0");
                $row.find(".Jun").val("0");
                $row.find(".Jul").val("0");
                $row.find(".Aug").val("0");
                $row.find(".Sep").val("0");
                $row.find(".Oct").val("0");
                $row.find(".Nov").val("0");
                $row.find(".Dec").val("0");
            }

            $('.dollar').keydown(function(e) {
                // Prevents user from typing in decimal point
                if (e.keyCode == 110 || e.keyCode == 190)
                    e.preventDefault();
            });

            function validate() {
                // hide validation icons
                $("#HudLineR").hide();
                $("#FeeTypeR").hide();
                $('#titleVendorRequired').hide();
                $("#SelectedFieldsR").hide();
                $('.field-section img').hide();
                $('.rangeWarning').hide();
                $(".desc").css("border", "");
                $(".expDesc").css("border", "");

                var isValid = true;

                // Validate HUD Line or Fee Type based on GFE version.
                var gfeType = $(".gfeType").val();
                if (gfeType == "OLD_GFE") {
                    // Validate line selected
                    var lineNumber = $('.hudLine').val();
                    if (!lineNumber || !$SelectedRow) {
                        $("#HudLineR").show();
                        isValid = false;
                    }
                } else { // gfeType == "NEW_GFE"
                    // Validate fee type selected
                    var feeType = $('.feeType').val();
                    if (!feeType) {
                        $("#FeeTypeR").show();
                        isValid = false;
                    }

                    var isHousingExpense = $.inArray(feeType, HousingExpensesFeeTypeIds) != -1

                    if (feeType != "Options" && feeType != "LENDER_CREDITS" && feeType != "TITLE_SOURCE" &&
                        feeType != ML.Hud1000MortgageInsuranceReserveFeeTypeId /*&& feeType != ML.Hud1000AggregateAdjustmentFeeTypeId*/) {
                        // Validate fee description
                        var $desc;
                        if (isHousingExpense) {
                            $desc = $(".expDesc");
                        } else {
                            $desc = $(".desc");
                        }

                        if (!$desc.val()) {
                            $desc.css("border", "3px solid red");
                            isValid = false;
                        }
                    } else if (feeType == "TITLE_SOURCE" && !$(".TitleVendor").val()) {
                        $('#titleVendorRequired').show();
                        isValid = false;
                    }
                }

                // Only need to validate rest of page if ApplyConditionally is checked
                var $hasConditions = $('#ApplyConditionally');
                if ($hasConditions.prop('checked')) {
                    // Validate fields selected
                    $lis = $selectedFieldsUl.children('li');
                    if ($lis.length == 0) {
                        $("#SelectedFieldsR").show();
                        isValid = false;
                    }

                    // Validate values selected
                    $('.field-section:visible').each(function() {
                        $this = $(this);
                        switch ($this.attr('data-type')) {
                            case 'Enumeration':
                            case 'BrokerDefinedEnumeration':
                            case 'NestedEnumeration':
                                var $selectedValues = $this.find('select option:selected');
                                if ($selectedValues.length == 0) {
                                    $this.find("img").show();
                                    isValid = false;
                                }

                                break;
                            case 'Range':
                                var lowerBound = $this.find('input').first().val();
                                var upperBound = $this.find('input').last().val();

                                if (!lowerBound || !upperBound) {
                                    $this.find("img").show();
                                    isValid = false;
                                } else {
                                    // make sure lowerBound < upperBound
                                    var ilb = parseInt(lowerBound.replace(/[^0-9\.]+/g, ""), 10);
                                    var iub = parseInt(upperBound.replace(/[^0-9\.]+/g, ""), 10);
                                    if (ilb >= iub) {
                                        $this.find("img").show();
                                        $this.find('.rangeWarning').show();
                                        isValid = false;
                                    }
                                }

                                break;
                        }
                    });
                }

                return isValid;
            }

            function moveLI(anchor, $dest, anchorText) {
                var $this = $(anchor), $li = $this.parents('li');
                $this.text(anchorText);
                $li.clone().appendTo($dest);
                $li.remove();

                showHideParameterSelection()

                return false;
            }

            function hideValues(fieldID) {
                if (!fieldID)
                    return;

                var $div = $("div[data-id=" + fieldID + "]");
                if (!$div)
                    return;

                $div.hide();
            }

            function showValues(fieldID) {
                if (!fieldID)
                    return;

                var $div = $("div[data-id=" + fieldID + "]");
                if (!$div)
                    return;

                $div.show();
            }

            $availableFieldsUl.on('click', 'a.pick', function() {
                if (hasDisabledAttr($fields))
                    return;

                showValues($(this).attr('data-id'));

                return moveLI(this, $selectedFieldsUl, 'remove');
            });

            $selectedFieldsUl.on('click', 'a.pick', function() {
                if (hasDisabledAttr($fields))
                    return;

                hideValues($(this).attr('data-id'));

                return moveLI(this, $availableFieldsUl, 'include');
            });

            $selectedFieldsUl.on('click', 'a.up', function() {
                if (hasDisabledAttr($fields))
                    return;

                var $this = $(this), $lis = $selectedFieldsUl.children('li'), $li = $this.parents('li'),
                index = $lis.index($li), $removedNode = $li.remove();
                if (index == 0) {
                    $selectedFieldsUl.append($removedNode);
                }
                else {
                    $lis.eq(index - 1).before($removedNode);
                }
                return false;
            });

            $selectedFieldsUl.on('click', 'a.down', function() {
                if (hasDisabledAttr($fields))
                    return;

                var $this = $(this), $lis = $selectedFieldsUl.children('li'), $li = $this.parents('li'),
                index = $lis.index($li), $removedNode = $li.remove();
                if (index + 1 == $lis.length) {
                    $selectedFieldsUl.prepend($removedNode);
                }
                else {
                    $lis.eq(index + 1).after($removedNode);
                }
                return false;
            });

            $('div[data-type="NestedEnumeration"] select.listBox').on('change', function() {
                if (hasDisabledAttr($fields))
                    return;

                var $this = $(this);
                var $parent = $this.parents('.field-section');

                $parent.find('.sub-section').hide();
                $parent.find('div.sub-section[data-id=' + $this.val() + "]").show();
            });

            function enableDisableSelection() {
                var hasConditions = $('#ApplyConditionally').prop('checked');
                setDisabledAttr($($fields, $fields.find('a')), !hasConditions);
                setDisabledAttr($($parameters, $parameters.find('a')), !hasConditions);
            }

            function showHideParameterSelection() {
                var $lis = $selectedFieldsUl.children('li');
                if ($lis.length > 0) {
                    $parameters.show();
                }
                else {
                    $parameters.hide();
                }
            }

            function hideAllGfeRows() {
                $rows.hide();
                $rows.filter('[data-fsid=GfeRow]').show();
            }

            hideAllGfeRows();

            var gfeTypeDropdown = $('.gfeType');
            gfeTypeDropdown.val(ML.IsEnableNewFeeService ? 'NEW_GFE' : 'OLD_GFE').change();

            if (!ML.IsEnableNewFeeService) {
                $('.gfeTypeRow').hide();
            }

            $('.action').change();
            $('#ClearSspLink').hide();
        });

        function openSspModal() {
            var selectedType = $(".bene").val();
            showModal('/los/rolodex/rolodexlist.aspx?affdlg=Y&ssp=1&ssptype=' + selectedType, null, null, null, function(args){
                if (!args.OK) {
                    return;
                }

                $('#SelectedSsp').text(args.systemId + ' : ' + args.compName);

                $('#SetSspLink').hide();
                $('#ClearSspLink').show();
            });
        }

        function clearSspSelection() {
            $('#SelectedSsp').text('');
            $('#SetSspLink').show();
            $('#ClearSspLink').hide();
        }

        // Disable highlighting without having to
        // remove handlers from all those elements
        function highlightRow(obj) { return; }
        function unhighlightRow(obj) { return; }

        var ctrlName = 'CCTemplate1_';
        function oncreditupdate(el) {
          if (!el) { return; }
          var el2 = document.getElementById(ctrlName + 'cPricingEngineLimitCreditToT');
          if (el.value == '1') {
              el2.value = 0;
              el2.disabled = true;
          }
          else {
              el2.disabled = false;
          }
      }

      function _init() {
          toggleRecFFields();
      }
    </script>
    <h4 class="page-header">Fee Value and Condition Generator</h4>
    <form id="form2" runat="server">
        <table class=FormTable cellSpacing=2 cellPadding=4 width="100%"	border=0>
			<tr class="gfeTypeRow">
			    <td>
			        <ml:EncodedLabel AssociatedControlID="GfeType" runat="server" Text="Generate fee rule for:" Font-Bold="true" />
                    <asp:DropDownList ID="GfeType" runat="server" CssClass="gfeType" >
                        <asp:ListItem Text="New GFE/Loan Estimate" Value="NEW_GFE" />
                        <asp:ListItem Text="Old GFE" Value="OLD_GFE" />
                    </asp:DropDownList>
			    </td>
			</tr>
            <tr class="oldGfe">
                <td>
                    <ml:EncodedLabel AssociatedControlID="HudLine" runat="server" Text="Which HUD line's field set would you like to create a condition for?" Font-Bold="true" />
                    <asp:DropDownList ID="HudLine" runat="server" CssClass="hudLine" />
                    <img id="HudLineR" style="display:none" src="../../images/error_icon.gif" />
                </td>
            </tr>
            <tr class='newGfe'>
                <td>
                    <ml:EncodedLabel AssociatedControlID="FeeType" runat="server" Text="Which HUD line's field set would you like to create a condition for?" Font-Bold="true" />
                    <asp:DropDownList ID="FeeType" runat="server" CssClass="feeType" />
                    <img id="FeeTypeR" style="display:none" src="../../images/error_icon.gif" />
                </td>
            </tr>
            <tr class='newGfe trAction'>
                <td>
                    <ml:EncodedLabel AssociatedControlID="Action" runat="server" Text="What action do you want this condition to perform?" Font-Bold="true" />
                    <asp:DropDownList ID="Action" runat="server" CssClass="action" >
                        <asp:ListItem Text="Add/Update Fee" Value="ADD" />
                        <asp:ListItem Text="Remove Fee" Value="REMOVE" />
                    </asp:DropDownList>
                    <span id="removeFeeNote" class="removeFeeNote" style="color:Red;font-weight:bold" />
                </td>
            </tr>
            <tr class='newGfe'>
                <td>
                    <div class="lenderCredits">
                        <table cellspacing="2" cellpadding="0" border="0">
                            <tr>
                                <td colspan="2" class="FieldLabel">Credit/charge calculation method</td>
                                <td><asp:DropDownList runat="server" ID="sLenderCreditCalculationMethodT" class="sLenderCreditCalculationMethodT" /></td>
                            </tr>
                            <tr><td colspan="4">&nbsp;</td></tr>
                            <tbody id="FrontEndPanel">
                                <tr>
                                    <td class="FieldLabel" colspan="2">Credit/Charge is based on</td>
                                    <td><asp:DropDownList ID="sLDiscntBaseT2" runat="server" CssClass="sLDiscntBaseT2" /></td>
                                    <td></td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr>
                                    <td></td>
                                    <td class="FieldLabel">General lender credit limit</td>
                                    <td><asp:DropDownList runat="server" id="sLenderCreditMaxT" CssClass="sLenderCreditMaxT" /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">Disclose credit on</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><asp:DropDownList runat="server" id="sLenderPaidFeeDiscloseLocationT" CssClass="sLenderPaidFeeDiscloseLocationT" /></td>
                                    <td class="FieldLabel" colspan="2">Lender paid fees</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><asp:DropDownList ID="sLenderGeneralCreditDiscloseLocationT" runat="server" CssClass="sLenderGeneralCreditDiscloseLocationT" /></td>
                                    <td class="FieldLabel" colspan="2">General lender credit</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr><td colspan="4">&nbsp;</td></tr>
                                <tr>
                                    <td><asp:DropDownList ID="sLenderCustomCredit1DiscloseLocationT" runat="server" CssClass="sLenderCustomCredit1DiscloseLocationT" /></td>
                                    <td class="FieldLabel" colspan="2"><asp:TextBox runat="server" id="sLenderCustomCredit1Description" CssClass="sLenderCustomCredit1Description" /></td>
                                    <td><ml:MoneyTextBox ID="sLenderCustomCredit1Amount" runat="server" CssClass="sLenderCustomCredit1Amount" width="90" preset="money" /></td>
                                </tr>
                                <tr>
                                    <td><asp:DropDownList ID="sLenderCustomCredit2DiscloseLocationT" runat="server" CssClass="sLenderCustomCredit2DiscloseLocationT" /></td>
                                    <td colspan="2"><asp:TextBox ID="sLenderCustomCredit2Description" runat="server" CssClass="sLenderCustomCredit2Description" /></td>
                                    <td><ml:MoneyTextBox ID="sLenderCustomCredit2Amount" runat="server" CssClass="sLenderCustomCredit2Amount" width="90" preset="money" /></td>
                                </tr>
                            </tbody>
                            <tbody id="ManualCalculatePanel">
                                <tr>
                                    <td class="FieldLabel" colspan="2">Disclose credit on</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">Initial/GFE</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="2"><ml:PercentTextBox id="sLDiscntPc" runat="server" CssClass="sLDiscntPc" Width="70" preset="percent" /> of <asp:DropDownList id="sLDiscntBaseT" runat="server" CssClass="sLDiscntBaseT" /> + <ml:MoneyTextBox id="sLDiscntFMb" runat="server" CssClass="sLDiscntFMb" Width="90" preset="money" /></td>
                                </tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
                                <tr>
                                    <td class="FieldLabel">Initial/GFE</td>
                                    <td class="FieldLabel">Credit for lender paid fees </td>
                                    <td><asp:DropDownList ID="sGfeCreditLenderPaidItemT" runat="server" CssClass="sGfeCreditLenderPaidItemT" /></td>
                                </tr>
                                <tr><td colspan="3">&nbsp;</td></tr>
                                <tr>
                                    <td class="FieldLabel">Closing/HUD-1</td>
                                    <td><asp:TextBox runat="server" id="sLenderCustomCredit1Description2" CssClass="sLenderCustomCredit1Description2" /></td>
                                    <td align="right"><ml:MoneyTextBox ID="sLenderCustomCredit1Amount2" runat="server" CssClass="sLenderCustomCredit1Amount2" width="90" preset="money" /></td>
                                </tr>
                                <tr>
                                    <td class="FieldLabel">Closing/HUD-1</td>
                                    <td><asp:TextBox ID="sLenderCustomCredit2Description2" runat="server" CssClass="sLenderCustomCredit2Description2" /></td>
                                    <td align="right"><ml:MoneyTextBox ID="sLenderCustomCredit2Amount2" runat="server" CssClass="sLenderCustomCredit2Amount2" width="90" preset="money" /></td>
                                </tr>
<%--                                <tr><td colspan="3">&nbsp;</td></tr>
                                <tr>
                                    <td><asp:DropDownList runat="server" id="sToleranceCureCalculationT" CssClass="sToleranceCureCalculationT" /></td>
                                    <td colspan="2" class="FieldLabel">Tolerance cures in the total credit to closing</td>
                                </tr>--%>
                            </tbody>
                        </table>
                    </div>

                    <div class="titleSource">
                        <span class="FieldLabel TitleSourceLabel"></span>
                        <asp:DropDownList runat="server" CssClass="TitleVendor" ID="TitleVendor"></asp:DropDownList>
                        <img id="titleVendorRequired" src="../../images/error_icon.gif" />
                        <span id="noFeeProvidersWarning">No fee providers have been enabled in your system.</span>
                    </div>

                    <div class="feeTemplate">
                        <table border="0" style="white-space: nowrap; padding-left: 5px;" cellpadding="2" cellspacing="0" class="GfeTable" >
                            <thead>
                                <tr class="FormTableHeader " >
                                    <td>
                                        HUD<br/>Line
                                    </td>
                                    <td>
                                        Description/Memo
                                    </td>
                                    <td>
                                        GFE<br/>Box
                                    </td>
                                    <td>
                                        APR
                                    </td>
                                    <td>
                                        FHA
                                    </td>
                                    <td>
                                        Paid to
                                    </td>
                                    <td>
                                        DFLP
                                    </td>
                                    <td>
                                        TP
                                    </td>
                                    <td>
                                        AFF
                                    </td>
                                    <td>
                                        Can<br/>Shop
                                    </td>
                                    <td class="tdAmount">
                                        Amount
                                    </td>
                                    <td>
                                        Paid by
                                    </td>
                                    <td>
                                        Payable
                                    </td>
                                    <td class="trEscrowed">
                                        Escrowed
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="FieldLabel">
                                        <span class="hudline" />
                                    </td>
                                    <td>
                                        <input type="text" class="desc"/>
                                        <br />
                                        Type: <span class="org_desc" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="GfeBox" Enabled="false" runat="server" class="section" />
                                    </td>
                                    <td>
                                        <input type="checkbox" class="apr" />
                                    </td>
                                    <td>
                                       <input type="checkbox" class="fha" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="Beneficiary" runat="server" class="bene" />
                                    </td>
                                    <td>
                                        <input type="checkbox" class="dflp" />
                                    </td>
                                    <td>
                                        <input type="checkbox" class="tp" />
                                    </td>
                                    <td>
                                       <input type="checkbox" class="aff" />
                                    </td>
                                    <td>
                                        <input type="checkbox" class="can_shop" />
                                    </td>
                                    <td class="tdAmount">
                                        <input type="text" preset="percent" class="money mask p" value="0.000%" /> of
                                        <asp:DropDownList runat="server" id="PercentBase" class="pt" /> +
                                        <input type="text" preset="money" class="money mask base" value="$0.00" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" id="PaidBy" class="paid_by" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" id="Payable" class="pmt_at" />
                                    </td>
                                    <td class="trEscrowed">
                                        <input type="checkbox" class="escrowed" />
                                    </td>
                                </tr>
                                <tr class="ssp">
                                    <td colspan="5">&nbsp;</td>
                                    <td colspan="9">
                                        <span class="FieldLabel">Suggested Settlement Service Provider: </span>
                                        <a id="SetSspLink" onclick="openSspModal();">set</a>
                                        <span id="SelectedSsp"></span>
                                        <a id="ClearSspLink" onclick="clearSspSelection();">clear</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="housingExpenses">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%" nowrap >
                            <tr>
                                <td>
                                    <div class="ExpenseTable">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr class="FormTableHeader">
                                                <td align="left" colspan="2" >
                                                    <span class="expName" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="Summary">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                Description
                                                            </td>
                                                            <td colspan="2">
                                                                <input type="text" style="width: 180px;" class="expDesc" />
                                                            </td>
                                                        </tr>
                                                        <tr class="trTaxes">
                                                            <td>
                                                                Tax Type
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:DropDownList ID="TaxType" runat="server" class="taxT" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Calculation Source
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="annAmtCalcT" runat="server" class="annAmtCalcT" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Prepaid
                                                                <input type="checkbox" class="isPrepaid" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Escrowed
                                                                <input type="checkbox" class="isEscrowed" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" id="Calculator" >
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                Monthly Amount
                                                            </td>
                                                            <td colspan="3" nowrap >
                                                                <input type="text" class="mask percent annAmtPerc" preset="percent" value="0.000%" /> of
                                                                <asp:DropDownList runat="server" id="annAmtBase" class="annAmtBase" /> +
                                                                <input type="text" preset="money" class="money mask monAmtFixed" value="$0.00" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Prepaid Months
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <input type="text" class="mask month prepMnth" /> months
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Reserves Cushion
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <input type="text" class="mask month Cush" /> months
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Reserves Amount
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" class="rsrvMonLckd" />
                                                            </td>
                                                            <td>
                                                                <input type="text" class="mask month rsrvMon" /> months
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%">
                                                        <tr class="FormTableSubheader">
                                                            <td>
                                                                Disbursement schedule
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Payment repeat
                                                                <asp:DropDownList ID="RepInterval" runat="server" class="RepInterval" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            Jan
                                                                        </td>
                                                                        <td>
                                                                            Feb
                                                                        </td>
                                                                        <td>
                                                                            Mar
                                                                        </td>
                                                                        <td>
                                                                            Apr
                                                                        </td>
                                                                        <td>
                                                                            May
                                                                        </td>
                                                                        <td>
                                                                            Jun
                                                                        </td>
                                                                        <td>
                                                                            Jul
                                                                        </td>
                                                                        <td>
                                                                            Aug
                                                                        </td>
                                                                        <td>
                                                                            Sep
                                                                        </td>
                                                                        <td>
                                                                            Oct
                                                                        </td>
                                                                        <td>
                                                                            Nov
                                                                        </td>
                                                                        <td>
                                                                            Dec
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="trDisbursements">
                                                                        <td>
                                                                            Months
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Jan" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Feb" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Mar" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Apr" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month May" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Jun" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Jul" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Aug" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Sep" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Oct" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Nov" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="mask month Dec" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="mortgageInsurance">
                        <table style="width: 735px; margin-left: 9px;" cellpadding="0" border="0" cellspacing="0">
                            <tr class="FormTableHeader">
                                <td align="left">
                                    <span class="MIExpName" >Mortgage Insurance</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                Escrowed
                                                <input type="checkbox" class="sMInsRsrvEscrowedTri" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Reserves Cushion
                                            </td>
                                            <td>
                                            </td>
                                            <td align="right">
                                                <input type="text" class="mask month sMInsRsrvEscrowCushion" /> months
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Reserves Amount
                                            </td>
                                            <td>
                                                for
                                            </td>
                                            <td>
                                                <input type="checkbox" class="sMInsRsrvMonLckd" />
                                                <input type="text" class="mask month sMInsRsrvMon" /> months
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="FormTableSubheader">
                                <td>
                                    Payment Schedule
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Payment repeat
                                    <asp:DropDownList runat="server" ID="sMIPaymentRepeat"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                Jan
                                            </td>
                                            <td>
                                                Feb
                                            </td>
                                            <td>
                                                Mar
                                            </td>
                                            <td>
                                                Apr
                                            </td>
                                            <td>
                                                May
                                            </td>
                                            <td>
                                                Jun
                                            </td>
                                            <td>
                                                Jul
                                            </td>
                                            <td>
                                                Aug
                                            </td>
                                            <td>
                                                Sep
                                            </td>
                                            <td>
                                                Oct
                                            </td>
                                            <td>
                                                Nov
                                            </td>
                                            <td>
                                                Dec
                                            </td>
                                        </tr>
                                        <tr class="sMInsRsrvEscrowDisbursementSchedule">
                                            <td>
                                                Months
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Jan" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Feb" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Mar" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Apr" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month May" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Jun" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Jul" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Aug" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Sep" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Oct" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Nov" />
                                            </td>
                                            <td>
                                                <input type="text" class="mask month Dec" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
<%--                    <div class="aggregateEscrow">
                        <table cellspacing="2" cellpadding="0" border="0">
                            <tr>
                                <td class="FieldLabel">Aggregate Escrow Impounds Calculation Mode</td>
                                <td><asp:DropDownList runat="server" ID="sAggEscrowCalcModeT" class="sAggEscrowCalcModeT" /></td>
                            </tr>
                            <tr class="trCustomaryEscrowImpoundsCalcMinT">
                                <td class="FieldLabel">Customary Escrow Impounds Calculation Minimum</td>
                                <td><asp:DropDownList runat="server" ID="sCustomaryEscrowImpoundsCalcMinT" class="sCustomaryEscrowImpoundsCalcMinT" /></td>
                            </tr>
                        </table>
                    </div>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <uc1:CCTemplateGFE2010 id="CCTemplate1" runat="server" IsFeeService="true" />
                </td>
            </tr>
            <tr style="border-top-style:solid;border-top-width:10px;">
                <td>
                    <table cellpadding="2" cellspacing="4" width="100%" style="border-style:groove;border-width:2px;" >
					    <tr>
					        <td>
					            <ml:EncodedLabel runat="server" Text="On which parameters does the selected HUD line's field set depend?" Font-Bold="true" />
					        </td>
					    </tr>
					    <tr>
                            <td>
                                <input type="radio" id="ApplyConditionally" runat="server" name="HasConditions" value="Y" class="hasConditions" checked />
                                <ml:EncodedLabel runat="server" AssociatedControlID="ApplyConditionally" Text="The selected HUD line's field set is applied conditionally based on the parameters selected below." />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr id="trFields">
                                        <td nowrap>
                                            <ml:EncodedLabel runat="server" Font-Bold="true" Text="Available Fields" />
                                            <div id="AvailableFields" runat="server">
                                                <asp:Repeater runat="server" ID="FieldChoices" OnItemDataBound="FieldRepeater_OnItemDataBound">
                                                    <HeaderTemplate><ul></HeaderTemplate>
                                                    <FooterTemplate></ul></FooterTemplate>
                                                    <ItemTemplate>
                                                        <li>
                                                            <a href="#" class="up"><asp:Image runat="server" ImageUrl="~/images/up-blue.gif" /> </a>
                                                            <a href="#" class="down"><asp:Image runat="server" ImageUrl="~/images/down-blue.gif" /> </a>
                                                            <a href="#" runat="server" class="pick" id="IncludeUrl">include</a>
                                                            <ml:EncodedLiteral runat="server" ID="FieldName"></ml:EncodedLiteral>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </td>
                                        <td nowrap>
                                            <ml:EncodedLabel runat="server" Font-Bold="true" Text="Fields used to determine selected HUD line's conditions" />
                                            <img id="SelectedFieldsR" style="display:none" src="../../images/error_icon.gif" />
                                            <div id="SelectedFields" runat="server">
                                                <asp:Repeater runat="server" ID="IncludedFields" OnItemDataBound="FieldRepeater_OnItemDataBound">
                                                    <HeaderTemplate><ul></HeaderTemplate>
                                                    <FooterTemplate></ul></FooterTemplate>
                                                    <ItemTemplate>
                                                        <li>
                                                            <a href="#" class="up"><asp:Image runat="server" ImageUrl="~/images/up-blue.gif" /> </a>
                                                            <a href="#" class="down"><asp:Image runat="server" ImageUrl="~/images/down-blue.gif" /> </a>
                                                            <a href="#" runat="server" class="pick" id="IncludeUrl">remove</a>
                                                            <ml:EncodedLiteral runat="server" ID="FieldName"></ml:EncodedLiteral>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="radio" id="ApplyAlways" runat="server" name="HasConditions" value="N" class="hasConditions" />
                                <ml:EncodedLabel runat="server" AssociatedControlID="ApplyAlways" Text="The selected HUD line's field set is applied unconditionally; there is no parameter dependency." />
                            </td>
                        </tr>
				    </table>
                </td>
            </tr>
            <tr id="trParameters">
                <td>
                    <table cellpadding="2" cellspacing="4" width="100%" style="border-style:groove;border-width:2px;" >
					    <tr>
					        <td>
					            <ml:EncodedLabel runat="server" Text="Please select the parameter values which define the condition you wish to generate." Font-Bold="true" />
					            <br />
					            <ml:EncodedLabel runat="server" Text='Hold "Ctrl" to select more than one option for each field.' Font-Italic="true" />
					        </td>
					    </tr>
					    <tr>
					        <td>
					            <asp:Repeater runat="server" ID="PossibleValues" OnItemDataBound="Values_OnItemDataBound">
                                    <ItemTemplate>
                                        <div class="field-section" id="divField" runat="server">
                                            <div class="paramDiv">
                                                <ml:EncodedLiteral runat="server" ID="FieldName" />
                                                <img style="display:none" src="../../images/error_icon.gif" />
                                                <br />
                                                <div id="EnumField" runat="server">
                                                    <asp:ListBox runat="server" ID="Values" SelectionMode="Multiple" CssClass="Selectable listBox"></asp:ListBox>
                                                </div>

                                                <div id="RangeField" runat="server" >
                                                    <ml:EncodedLabel ID="Label1" runat="server" Text='Enter nearest whole dollar amount' Font-Italic="true" />
                                                    <br />
                                                    <ml:MoneyTextBox ID="LowerBound" runat="server" CssClass="dollar" preset="money" Width="100px" />
                                                    <ml:EncodedLabel AssociatedControlID="LowerBound" runat="server" Text="lower bound" />
                                                    <br />
                                                    <ml:MoneyTextBox ID="UpperBound" runat="server" CssClass="dollar" preset="money" Width="100px" />
                                                    <ml:EncodedLabel AssociatedControlID="UpperBound" runat="server" Text="upper bound"/>
                                                    <br />
                                                    <ml:EncodedLabel ID="rangeWarning" runat="server" CssClass="rangeWarning" style="color:Red;display:none;"
                                                    Font-Italic="true" text="lower bound must be less than upper bound" />
                                                </div>
                                            </div>

                                            <asp:Repeater runat="server" ID="SubValues" OnItemDataBound="SubValues_OnItemDataBound">
                                                <ItemTemplate>
                                                    <div class="field-section sub-section paramDiv" id="subField" runat="server">
                                                        <ml:EncodedLiteral runat="server" ID="SubFieldName" />
                                                        <img style="display:none" src="../../images/error_icon.gif" />
                                                        <br />
                                                        <div id="SubEnumField" runat="server" >
                                                            <asp:ListBox runat="server" ID="SubValues" SelectionMode="Multiple" CssClass="Selectable subListBox"></asp:ListBox>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                            <br />
                                            <br />
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
					        </td>
					    </tr>
					</table>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <input type="button" value="Generate HUD Line's Field Set and Condition" id="genBtn" />
                    <input type="button" value="Close" id="closeBtn" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
