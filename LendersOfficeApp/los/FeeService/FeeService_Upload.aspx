﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeeService_Upload.aspx.cs" Inherits="LendersOfficeApp.los.FeeService.FeeService_Upload" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Upload Fee Service File</title>
    <style type="text/css">
        body
        {
            background-color: gainsboro;
        }

        #dialog-wait
        {
            display: none;
            font-weight: bold;
        }

        #dialog-wait .ui-dialog-content
        {
            visibility: hidden;
        }
    </style>
</head>
<body>
    <script type="text/javascript" >
        $(function() {

            var errorRow = <%= AspxTools.JsGetElementById(trErrors) %>;
            resize(500, errorRow.style.display == 'none' ? 250 : 390);
            $('#closeBtn').click(function() {
                onClosePopup();
            });
        });

        function Click_UploadBtn() {
            <% if(!IsTest) {%>
                if (!confirm("Uploading this file to your production fee service may set fees on live loans. Continue?"))
                {
                    return false;
                }
            <% } %>

            // Open wait dialog.
            $('#dialog-wait').dialog({
                modal: true,
                closeOnEscape: false,
                width: "205",
                height: "85",
                draggable: false,
                dialogClass: "LQBDialogBox",
                resizable: false
            });

            // Remove dialog title bar.
            $('#dialog-wait').prev(".ui-dialog-titlebar").hide();

            return true;
        }
    </script>
    <h4 class="page-header">Upload and Validate New Fee Service File</h4>
    <form id="form1" enctype="multipart/form-data" runat="server">
        <div id="dialog-wait" title="Please wait">
            <table>
                <tbody>
                    <tr>
                        <td>
                            Please wait...
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src="../../images/status.gif" alt="Loading" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <table class=FormTable cellSpacing=2 cellPadding=4 width="100%"	border=0>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" Text="Please select your fee service file for upload and validation." Font-Bold="true" />

                </td>
            </tr>
	        <tr>
	            <td>
                    <input id="m_inputFile" style="Width: 95%" type="file" name="m_inputFile" runat="server" />
                    <asp:RequiredFieldValidator Runat="server" Display="Static" ErrorMessage="*" ControlToValidate="m_inputFile" />
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel AssociatedControlID="Comments" runat="server" Text="Comments:" Font-Bold="true" />
                    <br />
                    <asp:TextBox ID="Comments" runat="server" TextMode="MultiLine" Rows="5" Width="100%" MaxLength="1000" />
                </td>
            </tr>
            <tr id="trValidationMsg" runat="server" >
                <td>
                    <ml:EncodedLabel AssociatedControlID="ValidationMsg" runat="server" Text="Fee Service Upload Status: " Font-Bold="true" />
                    <ml:EncodedLabel ID="ValidationMsg" runat="server" Font-Bold="true" />
                </td>
            </tr>
            <tr id="trErrors" runat="server" >
                <td>
                    <ml:EncodedLabel ID="ErrorMsgs" runat="server" Font-Bold="true" Text="Error Messages"/>
		            <div style="overflow-y:auto;border-style:solid;height:100px;">
		                <ml:CommonDataGrid ID="m_grid" runat="server" AutoGenerateColumns="false" OnItemDataBound="m_grid_ItemDataBound" >
		                    <HeaderStyle />
                            <Columns>
                                <asp:TemplateColumn HeaderText="Error #" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <ml:EncodedLiteral ID="ErrorCount" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Error Description" ItemStyle-Width="70%">
                                    <ItemTemplate>
                                        <ml:EncodedLiteral ID="ErrorDesc" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="OffendingRows" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <ml:EncodedLiteral ID="ErrorRows" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </ml:CommonDataGrid>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="uploadBtn" runat="server" OnClientClick="return Click_UploadBtn();" OnClick="uploadBtn_Click" Text="Upload" />
                    <input type="button" value="Close" id="closeBtn" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
