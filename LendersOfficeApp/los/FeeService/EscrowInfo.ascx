﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EscrowInfo.ascx.cs" Inherits="LendersOfficeApp.los.FeeService.EscrowInfo" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<tr class="EscrowInfo" data-fsid="<%=AspxTools.HtmlString(this.FeeServiceID) %>">
    <td colspan="20">
        <table>
            <tr data-fsid="<%=AspxTools.HtmlString(this.FeeServiceID) %>" class="Center">
                <td></td>
                <td>Jan</td>
                <td>Feb</td>
                <td>Mar</td>
                <td>Apr</td>
                <td>May</td>
                <td>Jun</td>
                <td>Jul</td>
                <td>Aug</td>
                <td>Sep</td>
                <td>Oct</td>
                <td>Nov</td>
                <td>Dec</td>
            </tr>
            <tr data-fsid="<%=AspxTools.HtmlString(this.FeeServiceID) %>">
                <td><label class="ReserveMonthsLabel">Reserve Months Cushion<asp:TextBox runat="server" ID="Cushion" class="Cushion" MaxLength="2" Width="37px" /></label></td>
                <td><asp:TextBox runat="server" ID="Jan" class="Jan" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="Feb" class="Feb" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="Mar" class="Mar" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="Apr" class="Apr" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="May" class="May" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="Jun" class="Jun" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="Jul" class="Jul" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="Aug" class="Aug" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="Sep" class="Sep" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="Oct" class="Oct" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="Nov" class="Nov" MaxLength="2" Width="37px" /></td>
                <td><asp:TextBox runat="server" ID="Dec" class="Dec" MaxLength="2" Width="37px" /></td>
            </tr>
            <tr data-fsid="<%=AspxTools.HtmlString(this.FeeServiceID) %>" class="Center">
                <td colspan="13" class="DisbursementScheduleLabel">Escrow Disbursement Schedule</td>
            </tr>
        </table>
    </td>
</tr>