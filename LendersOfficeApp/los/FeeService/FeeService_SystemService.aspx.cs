﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using LendersOffice.Common;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;

using DataAccess.FeeService;

namespace LendersOfficeApp.los.FeeService
{
    public partial class FeeService_SystemService : BaseSimpleServiceXmlPage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGFEFeeService
                };
            }
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "GetFieldInfo":
                    GetFieldInfo();
                    break;
                case "ReleaseToProduction":
                    ReleaseToProduction();
                    break;
                default:
                    break;
            }
        }

        protected void GetFieldInfo()
        {
            string lineNumber = GetString("LineNumber");
            if (lineNumber == "Options")
                lineNumber = "Calc Option";

            string obj = "{";

            foreach ( FeeServiceFieldInfo fieldInfo in AvailableFeeServiceFields.GetFieldsByLineNumber(lineNumber) )
            {
                obj += "\"" + fieldInfo.LQBFieldName + "\": {\"ValidValueEntryOptions\": \"" + fieldInfo.ValidValueEntryOptions
                    + "\", \"DescriptionOfField\": \"" + fieldInfo.DescriptionOfField + "\"},";
            }

            // remove trailing comma
            obj = obj.Remove(obj.Length - 1);

            obj += "}";

            SetResult("obj", obj);
        }

        protected void ReleaseToProduction()
        {
            FeeServiceUtilities.ReleaseTestToProduction(PrincipalFactory.CurrentPrincipal);
        }
    }
}
