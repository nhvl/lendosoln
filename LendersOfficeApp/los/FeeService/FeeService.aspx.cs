﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.TitleProvider;
using LendersOffice.Security;
using DataAccess;
using DataAccess.FeeService;

namespace LendersOfficeApp.los.FeeService
{
    public partial class FeeService : BaseServicePage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGFEFeeService
                };
            }
        }

        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((BasePage)Page).IncludeStyleSheet("~/css/Tabs.css");

            EnableJqueryMigrate = false;
            RegisterJsScript("jquery.tablesorter.min.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");

            RegisterService("System", "/los/Template/ClosingCost/SystemService.aspx");
            RegisterService("FeeService_System", "/los/FeeService/FeeService_SystemService.aspx");

            RegisterJsScript("LQBPopup.js");
            RegisterJsGlobalVariables("EnableEnhancedTitleQuotes", PrincipalFactory.CurrentPrincipal.BrokerDB.EnableEnhancedTitleQuotes);

            if (!IsPostBack)
            {
                BindClosingCostDDL(ClosingCostTitleVendorId);
                PageNumber.Value = "0";
            }

            FeeServiceRevisionHistory revisionHistory = FeeServiceRevisionHistory.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId);

            if (PageNumber.Value == "0")
            {
                // Production Fee Service file revision history
                m_grid.DataSource = revisionHistory.FeeServiceRevisionsIncludingTitleVendorUpdates;
                m_grid.DataBind();
            }
            else
            {

                // OPM 219430 - Test Fee Service file revision history.
                m_grid.DataSource = revisionHistory.FeeServiceTestRevisions;
                m_grid.DataBind();
            }

            // Setup download latest production file link
            FeeServiceRevision currentRevision = revisionHistory.CurrentFeeServiceRevision;
            if (currentRevision == null)
            {
                // No current revision (no file uploaded yet). So hide link.
                divDownloadLatest.Visible = false;
            }
            else
            {
                // If current revision exists, set command parameters
                downloadLatestProd.CommandName = currentRevision.FileDbKey;
                downloadLatestProd.CommandArgument = currentRevision.UploadedD_rep;
            }

            // Set visibility of release to production button.
            FeeServiceRevision currentTestRevision = revisionHistory.CurrentFeeServiceTestRevision;
            releaseBtn.Visible = currentTestRevision != null;
        }

        private void BindClosingCostDDL(DropDownList ddl)
        {
            var associations = TitleProvider.GetAssociations(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);

            ddl.Items.Add(new ListItem("Fee Service Settings", "-1"));

            foreach (var association in associations)
            {
                ddl.Items.Add(new ListItem(association.VendorName, association.VendorId.ToString()));
            }

            if (db.ClosingCostTitleVendorId.HasValue)
            {
                Tools.SetDropDownListValue(ddl, db.ClosingCostTitleVendorId.Value);
            }
            else
            {
                ddl.Items[0].Selected = true;
            }
        }

        protected void m_grid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            LinkButton download = (LinkButton) e.Item.FindControl("download");
            download.CommandName = ((FeeServiceRevision)e.Item.DataItem).FileDbKey;
            download.CommandArgument = ((FeeServiceRevision)e.Item.DataItem).UploadedD_rep;

            download.Visible = !string.IsNullOrEmpty(download.CommandName);
        }

        protected void Download_Command(object sender, CommandEventArgs e)
        {
            try
            {
                DateTime date = DateTime.Parse((string)e.CommandArgument);
                string fileDBKey = e.CommandName;
                byte[] fileData = FileDBTools.ReadData(E_FileDB.Normal, fileDBKey);

                if (fileData != null)
                {
                    string fileName = "fee_service_" + date.ToString("MMddyyyyHHmm") + ".xlsx";

                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.AppendHeader("Content-Disposition", $"attachment; filename=\"{fileName}\"");
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.BinaryWrite(fileData);
                    Response.End();
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                // Response.End throws this by design
            }
        }
    }
}
