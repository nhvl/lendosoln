﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using DataAccess;
using DataAccess.FeeService;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.los.FeeService
{
    public partial class FeeService_Upload : BaseServicePage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGFEFeeService
                };
            }
        }

        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        protected bool IsTest
        {
            get
            {
                return RequestHelper.GetBool("test");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("jquery.tablesorter.min.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            EnableJqueryMigrate = false;

            trValidationMsg.Style.Add(HtmlTextWriterStyle.Display, "none");
            trErrors.Style.Add(HtmlTextWriterStyle.Display, "none");
        }

        protected void uploadBtn_Click(object sender, EventArgs e)
        {
            if (m_inputFile.PostedFile == null)
            {
                // m_inputFileValidator should catch this so we should technically never be here
                Tools.LogBug("Fee service uploader was unable to find the specified file: " + m_inputFile.Value);

                // Report Failure
                trValidationMsg.Style.Add(HtmlTextWriterStyle.Display, "");
                ValidationMsg.Text = "Upload failure! Uploader was unable to find the specified file..";
                ValidationMsg.Style.Add(HtmlTextWriterStyle.Color, "Red");

                return;
            }

            // Save to temp file.
            // We need to keep track of the extension, since that's how we differentiate between
            // different kinds of files.
            string ext = Path.GetExtension(m_inputFile.PostedFile.FileName);
            string tempFilename = Path.GetTempFileName() + ext;
            m_inputFile.PostedFile.SaveAs(tempFilename);

            bool passed;
            Dictionary<string, List<int>> errors;

            if (ext == ".xlsx")
            {
                // Save to fileDB
                try
                {
                    passed = FeeServiceUtilities.UploadRevision(PrincipalFactory.CurrentPrincipal, tempFilename, Comments.Text, IsTest, out errors);
                    if (errors.Count > 0)
                    {
                        try
                        {
                            var sb = new StringBuilder(2000, 40000); // 40K chars < 85KB.
                            foreach (KeyValuePair<string, List<int>> error in errors)
                            {
                                sb.AppendLine($"lines {string.Join(", ", error.Value)} have error: {error.Key}");
                            }
                            Tools.LogInfo(sb.ToString());
                        }
                        catch(ArgumentOutOfRangeException aoore)
                        {
                            // I expect that most of the time this won't happen, and I don't want to double check the log size for the majority of the time.
                            // Also, the duration of the upload is large compared to a try catch.
                            Tools.LogError(aoore);
                            foreach (KeyValuePair<string, List<int>> error in errors)
                            {
                                Tools.LogInfo($"lines {string.Join(", ", error.Value)} have error: {error.Key}");
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                    Tools.LogError("Unexpected error uploading fee service file.", exc);

                    passed = false;

                    string msg = "An unexpected exception occurred.";
                    errors = new Dictionary<string, List<int>>();
                    errors.Add(msg, new List<int>() { -1 });
                }
            }
            else
            {
                passed = false;

                string msg = "Cannot parse file with extension: " + ext + ". File must be a valid Excel spreadsheet with extension .xlsx.";

                errors = new Dictionary<string, List<int>>();
                errors.Add(msg, new List<int>() { -1 });
            }

            if (passed)
            {
                // Report Success
                trValidationMsg.Style.Add(HtmlTextWriterStyle.Display, "");
                ValidationMsg.Text = "Success! PML will now calculate closing costs as per your fee service file’s specifications.";
                ValidationMsg.Style.Add(HtmlTextWriterStyle.Color, "Green");

                // Clear Comments
                Comments.Text = "";
            }
            else
            {
                // Report Failure
                trValidationMsg.Style.Add(HtmlTextWriterStyle.Display, "");
                ValidationMsg.Text = "Upload failure! Validation errors detected. Please correct the errors and upload your file again.";
                ValidationMsg.Style.Add(HtmlTextWriterStyle.Color, "Red");

                // Display Errors
                trErrors.Style.Add(HtmlTextWriterStyle.Display, "");
                m_grid.DataSource = errors;
                m_grid.DataBind();
            }
        }

        protected void m_grid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            Literal ErrorCount = (Literal)e.Item.FindControl("ErrorCount");
            Literal ErrorDesc = (Literal)e.Item.FindControl("ErrorDesc");
            Literal ErrorRows = (Literal)e.Item.FindControl("ErrorRows");

            string errorString = ((KeyValuePair<string, List<int>>)e.Item.DataItem).Key;
            List<int> lineNumbers = ((KeyValuePair<string, List<int>>)e.Item.DataItem).Value;

            ErrorCount.Text = (e.Item.ItemIndex + 1).ToString();
            ErrorDesc.Text = errorString;
            if (errorString.EndsWith("Each LendingQB field setting must be mutually exclusive."))
            {
                
                ErrorRows.Text = "";
                if (lineNumbers.Count > 0)
                    ErrorRows.Text = lineNumbers[0].ToString();
                for (int i = 1; i < lineNumbers.Count; ++i)
                    ErrorRows.Text += (i % 2 == 1 ? "," : ";") + lineNumbers[i]; //Overlapping Condition Errors need to output pairs of row numbers
            }
            else
                ErrorRows.Text = string.Join(",", lineNumbers);
        }
    }
}