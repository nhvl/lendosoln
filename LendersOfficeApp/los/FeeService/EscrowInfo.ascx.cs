﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.ObjLib.Escrow;

namespace LendersOfficeApp.los.FeeService
{
    public partial class EscrowInfo : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets or sets the fee service ID to be applied to the table rows.
        /// </summary>
        public string FeeServiceID { get; set; }

        /// <summary>
        /// Gets or sets the escrow item this control will represent.
        /// </summary>
        public E_EscrowItemT EscrowItem { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                return;
            }

            var escrowItemDefaults = CPageBase.GetInitialEscrowAccountDefaults(this.EscrowItem);
            this.Cushion.Text = escrowItemDefaults[0].ToString();
            this.Jan.Text = escrowItemDefaults[1].ToString();
            this.Feb.Text = escrowItemDefaults[2].ToString();
            this.Mar.Text = escrowItemDefaults[3].ToString();
            this.Apr.Text = escrowItemDefaults[4].ToString();
            this.May.Text = escrowItemDefaults[5].ToString();
            this.Jun.Text = escrowItemDefaults[6].ToString();
            this.Jul.Text = escrowItemDefaults[7].ToString();
            this.Aug.Text = escrowItemDefaults[8].ToString();
            this.Sep.Text = escrowItemDefaults[9].ToString();
            this.Oct.Text = escrowItemDefaults[10].ToString();
            this.Nov.Text = escrowItemDefaults[11].ToString();
            this.Dec.Text = escrowItemDefaults[12].ToString();
        }
    }
}