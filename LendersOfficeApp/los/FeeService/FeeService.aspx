<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeeService.aspx.cs" Inherits="LendersOfficeApp.los.FeeService.FeeService" %>
<%@ Import Namespace="DataAccess.FeeService" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>GFE Fee Service</title>
    <style type="text/css">
        body
        {
            background-color: gainsboro;
        }

        #dialog-release
        {
            text-align: center;
        }

        .LQBDialogBox .ui-dialog-titlebar.ui-widget-header
        {
            background-color: rgb(0, 51, 102);
        }

        .warnTitle
        {
            color: red;
        }
    </style>
</head>
<body>
    <script type="text/javascript" >
        function _init() {
            var selectedIndex = $(<%= AspxTools.JsGetElementById(PageNumber) %>).val();

            $(".prodOnly").toggle(!ML.EnableEnhancedTitleQuotes && selectedIndex == 0);
            $(".testOnly").toggle(selectedIndex == 1);

            $("#tab").removeClass("selected");
            $("#tab" + selectedIndex).addClass("selected");
        }

        function onTabChange(selectedIndex) {
            $(<%= AspxTools.JsGetElementById(PageNumber) %>).val(selectedIndex);
        }

        function onUploadFileClick() {
            var selectedIndex = $(<%= AspxTools.JsGetElementById(PageNumber) %>).val();
            var url = '/los/FeeService/FeeService_Upload.aspx';

            // Add test flag if uploading from test tab.
            if (selectedIndex == 1) {
                url += '?test=1';
            }

            showModal(url, null, null, null, null, {hideCloseButton: true});
            return false;
        }

        function onConditionGeneratorClick() {
            return showModal('/los/FeeService/FeeService_ConditionGenerator.aspx', null, null, null, null, {width: 800, height: 700, hideCloseButton: true});
        }

        function releaseBtn_click() {
            $('#dialog-release').dialog({
                modal: true,
                buttons: {
                    "Yes": function () {
                        $(this).dialog("close");
                        var results = gService.FeeService_System.call('ReleaseToProduction');

                        if (results.error) {
                            alert('Failed to copy test file to production. \nError:' + results.UserMessage);
                            return;
                        }

                        // If successful, return to production.
                        onTabChange(0);
                        __doPostBack('', '');
                    },
                    "No": function () {
                        $(this).dialog("close");
                    }
                },
                closeOnEscape: false,
                width: "400",
                draggable: false,
                dialogClass: "LQBDialogBox",
                resizable: false
            });
        }

        function UpdateClosingCostVendorId()
        {
            var val = $('.ccvendor').val();
            var results = gService.System.call('UpdateClosingCostVendorId', { ClosingCostId: val, IsFeeService: true });

            if (results.error) {
                alert('Could not update title vendor. \nError:' + results.UserMessage);
                return false;
            }

            isDirty = false;
            toggleUi();

            return true;
        }

        var isDirty = false;
        function toggleUi()
        {
            $('#okBtn').prop("disabled", !isDirty);
            $('#applyBtn').prop("disabled", !isDirty);

            var val = $('.ccvendor').val();
            $('.warnTitle').toggle(val != -1);
        }

        $(function () {
            resize(800, 300);
            toggleUi();

            $('#closeBtn').click(function () { onClosePopup(); });

            $('#okBtn').click(function () { if (UpdateClosingCostVendorId()) onClosePopup(); });

            $('#applyBtn').click(function () { if (UpdateClosingCostVendorId()) __doPostBack('',''); });

            $('.ccvendor').change(function() {
                isDirty = true;
                toggleUi();
            });
        });
    </script>
    <h4 class="page-header">Fee Service</h4>
    <form runat="server">
        <asp:HiddenField ID="PageNumber" runat="server" />

        <div style="display:none;" id="dialog-release" title="Release to Production">
            <p style="width: 100%">Your currently active fee service test file will be released to production. Would you like to proceed?</p>
        </div>

        <table class=FormTable cellSpacing=2 cellPadding=4 width="100%"	border=0>
            <tr height="1%">
                <td>
                    <div class="Tabs">
                        <ul class="tabnav">
                            <li runat="server" id="tab0" onclick="onTabChange(0);"><asp:LinkButton ID="lnkTab0" runat="server">Production</asp:LinkButton></li>
                            <li runat="server" id="tab1" onclick="onTabChange(1);"><asp:LinkButton ID="lnkTab1" runat="server">Test</asp:LinkButton></li>
                        </ul>
                    </div>
                </td>
            </tr>
	        <tr>
	            <td nowrap>
	                <asp:Button UseSubmitBehavior="false" ID="UploadFile" runat="server" onClientClick="onUploadFileClick();return false;" text="Upload and Validate New Fee Service File" />
	                <input type="button" id="ConditionGenerator" runat="server" onclick="onConditionGeneratorClick();" value="Launch HUD Line Field Set Condition Generator" />
                </td>
            </tr>
            <tr class="prodOnly">
                <td>
                    <table cellpadding="2" cellspacing="4" width="100%" style="border-style:groove;border-width:2px;" >
					    <tr>
					        <td>
					            <ml:EncodedLabel runat="server" AssociatedControlID="ClosingCostTitleVendorId" Font-Bold=true>Obtain Escrow/Title/Recording/Transfer Tax from:</ml:EncodedLabel>
                                <asp:DropDownList runat="server" CssClass="ccvendor" ID="ClosingCostTitleVendorId" />
                                <br />
                                <label class="warnTitle">Note: Quotes from the selected vendor will be pulled/applied through pricing engine submissions only up until there is an initial disclosure on file.</label>
					        </td>
					    </tr>
				    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="2" cellspacing="4" width="100%" style="border-style:groove;border-width:2px;" >
					    <tr>
					        <td>
                                <div ID="divDownloadLatest" runat="server" class="testOnly" style="float:right;">
                                    <asp:LinkButton ID="downloadLatestProd" runat="server" OnCommand="Download_Command" Text="download latest production file" />
                                </div>
					            <ml:EncodedLabel runat="server" Font-Bold=true>Fee Service Change History</ml:EncodedLabel>
					            <div id="divHistory_Prod" style="overflow-y:auto;overflow-x:hidden;border-style:solid;height:100px;">
					                <ml:CommonDataGrid ID="m_grid" runat="server" AutoGenerateColumns="false" OnItemDataBound="m_grid_ItemDataBound" >
					                    <HeaderStyle />
                                        <Columns>
                                            <asp:BoundColumn ItemStyle-Width="50%" DataField="Comments" HeaderText="Comments" />
                                            <asp:BoundColumn ItemStyle-Width="125px" DataField="UploadedD_rep" HeaderText="Timestamp" />
                                            <asp:BoundColumn DataField="UploadedByUserName" HeaderText="Uploaded By" />
                                            <asp:TemplateColumn ItemStyle-Width="75px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="download" runat="server" OnCommand="Download_Command" Text="download" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </ml:CommonDataGrid>
                                </div>
					        </td>
					    </tr>
				    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="releaseBtn" runat="server" CssClass="testOnly" Text="Release to Production" OnClientClick="releaseBtn_click(); return false;" />

                    <input type="button" style="margin-left: 5px;" value="Apply" id="applyBtn" style="float:right;" />
                    <input type="button" style="margin-left: 5px;" value="Cancel" id="closeBtn" style="float:right;" />
                    <input type="button" style="margin-left: 5px;" value=" OK " id="okBtn" style="float:right;" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
