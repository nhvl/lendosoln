﻿namespace LendersOfficeApp.los.FeeService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.ClosingCostAutomation;
    using DataAccess.FeeService;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.TitleProvider;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;

    public partial class FeeService_ConditionGenerator : BaseServicePage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGFEFeeService
                };
            }
        }

        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.EmulateIE10;
        }

        protected int FieldCount { get; private set; }

        private ParameterReporting m_parameterReporting;

        private Guid[] feesToBeRemoved =
        {
            DefaultSystemClosingCostFee.Hud800DiscountPointsFeeTypeId,                      // 802
            DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId,   // 902
            DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId,     // 902
            DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId,                     // 903
            DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId,                           // 905
            DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId,                         // 907
            DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId,                           // 908
            DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId,                      // 909
            DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId,                           // 910
            DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId,                           // 911
            DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId,                           // 912
            DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId,                           // 913
            DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId,                  // 914
            DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId,                      // 915
            DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId,                             // 916
            DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId,                          // 917
            DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId,                           // 918
            DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId,                           // 919
            DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId,                           // 920
            DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId,                           // 921
            DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId,             // 1002
            DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId,           // 1003
            DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId,                         // 1004
            DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId,                          // 1005
            DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId,              // 1006
            DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId,                // 1007
            DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId,                            // 1008
            DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId,                            // 1009
            DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId,                            // 1010
            DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId,                            // 1011
            DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId,                 // 1012
            DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId,                  // 1013
            DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId,           // 1014
            DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId,                         // 1015
            DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId,                          // 1016
            DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId,                          // 1017
            DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId,                          // 1018
            DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId,                          // 1019
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("jquery.tablesorter.min.js");
            RegisterJsScript("FeeService_ConditionGenerator.js");
            this.RegisterJsScript("LQBPopup.js");
            RegisterService("System", "/los/FeeService/FeeService_SystemService.aspx");
            EnableJqueryMigrate = false;

            BindHudLines(HudLine);
            BindFeeTypes(FeeType);
            BindGfeBoxes(GfeBox);
            BindPaidBy(PaidBy);
            BindPayable(Payable);
            Tools.Bind_PercentBaseT(PercentBase);
            RolodexDB.PopulateAgentTypeDropDownList(Beneficiary);
            //Tools.Bind_AggregateEscrowCalcModeT(sAggEscrowCalcModeT, PrincipalFactory.CurrentPrincipal.BrokerDB.EnableCustomaryEscrowImpoundsCalculation);
            //Tools.Bind_CustomaryCalcMinT(sCustomaryEscrowImpoundsCalcMinT);

            // Lender Credits stuff
            Tools.Bind_sLenderCreditCalculationMethodT(sLenderCreditCalculationMethodT);
            Tools.Bind_sLenderCreditMaxT(sLenderCreditMaxT);
            Tools.Bind_LenderCreditDiscloseLocationT(sLenderPaidFeeDiscloseLocationT);
            Tools.Bind_LenderCreditDiscloseLocationT(sLenderGeneralCreditDiscloseLocationT);
            Tools.Bind_LenderCreditDiscloseLocationT(sLenderCustomCredit1DiscloseLocationT);
            Tools.Bind_LenderCreditDiscloseLocationT(sLenderCustomCredit2DiscloseLocationT);
            //Tools.Bind_sToleranceCureCalculationT(sToleranceCureCalculationT);
            Tools.Bind_PercentBaseLoanAmountsT(sLDiscntBaseT);
            Tools.Bind_PercentBaseLoanAmountsT(sLDiscntBaseT2);
            Tools.Bind_sGfeCreditLenderPaidItemT(sGfeCreditLenderPaidItemT);

            // Title Fees.
            BindTitleVendorDdl(TitleVendor);

            // Housing Expenses
            Tools.Bind_sTaxTableT(TaxType);
            Tools.Bind_AnnAmtCalcType(annAmtCalcT);
            Tools.Bind_PercentBaseT(annAmtBase);
            Tools.Bind_DisbursementReptIntervalT(RepInterval);
            Tools.Bind_DisbursementReptIntervalT(sMIPaymentRepeat);

            AvailableAutomationFields aaf = new AvailableAutomationFields(true, PrincipalFactory.CurrentPrincipal.BrokerId);

            FieldChoices.DataSource = aaf.GetFields();
            FieldChoices.DataBind();

            IncludedFields.DataSource = new List<AutomationField>();
            IncludedFields.DataBind();

            m_parameterReporting = new ParameterReporting(PrincipalFactory.CurrentPrincipal.BrokerId);

            AutomationField[] automationFields = aaf.GetFields().ToArray();
            FieldCount = automationFields.Length;
            PossibleValues.DataSource = automationFields;
            PossibleValues.DataBind();

            string HousingExpensesFeesScript = "var HousingExpensesFeeTypeIds = ["
                + "'" + DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId + "',"
                //+ "'" + DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId + "',"
                //+ "'" + DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId + "'"
                + "];";

            string TaxesFeesScript = "var TaxesFeeTypeIds = ["
                + "'" + DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId + "',"
                + "'" + DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId + "',"
                + "];";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "HOUSING_EXPENSES_FEE_ARRAY", HousingExpensesFeesScript, true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TAXES_FEE_ARRAY", TaxesFeesScript, true);
            RegisterJsObject("ClosingCostData", PrincipalFactory.CurrentPrincipal.BrokerDB.GetUnlinkedClosingCostSet().GetFees(null).ToList());
            RegisterJsGlobalVariables("IsEnableNewFeeService", PrincipalFactory.CurrentPrincipal.BrokerDB.IsEnableNewFeeService);
            RegisterJsGlobalVariables("Hud1000MortgageInsuranceReserveFeeTypeId", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId.ToString());
            RegisterJsGlobalVariables("Hud800OriginatorCompensationFeeTypeId", DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId.ToString());
            RegisterJsGlobalVariables("Hud900DailyInterestFeeTypeId", DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId.ToString());
            RegisterJsGlobalVariables("EnableEnhancedTitleQuotes", PrincipalFactory.CurrentPrincipal.BrokerDB.EnableEnhancedTitleQuotes);
        }

        protected void FieldRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            AutomationField item = (AutomationField)args.Item.DataItem;
            HtmlAnchor includeUrl = args.Item.FindControl("IncludeUrl") as HtmlAnchor;
            Literal fieldName = args.Item.FindControl("FieldName") as Literal;

            includeUrl.Attributes.Add("data-id", item.FieldId);
            fieldName.Text = item.Name;
        }

        protected void Values_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }
            AutomationField field = args.Item.DataItem as AutomationField;

            // Set div name
            HtmlGenericControl divField = args.Item.FindControl("divField") as HtmlGenericControl;
            divField.Attributes.Add("data-id", field.FieldId);
            divField.Attributes.Add("data-rep", field.Name);
            divField.Attributes.Add("data-type", field.Type.ToString());

            Literal fieldName = divField.FindControl("FieldName") as Literal;
            fieldName.Text = field.Name;

            HtmlGenericControl EnumField = args.Item.FindControl("EnumField") as HtmlGenericControl;
            HtmlGenericControl RangeField = args.Item.FindControl("RangeField") as HtmlGenericControl;

            Repeater SubValues = args.Item.FindControl("SubValues") as Repeater;

            switch (field.Type)
            {
                case AutomationFieldType.Enumeration:
                case AutomationFieldType.BrokerDefinedEnumeration:
                case AutomationFieldType.NestedEnumeration:
                    // Hide RangeField elements
                    RangeField.Style.Add(HtmlTextWriterStyle.Display, "none");

                    // Hide Subsets
                    if (field.Type != AutomationFieldType.NestedEnumeration)
                    {
                        SubValues.Visible = false;  // just remove from DOM.
                    }

                    // Set EnumField elements
                    ListBox valuesList = EnumField.FindControl("Values") as ListBox;
                    var parameter = m_parameterReporting[field.FieldId];

                    BindEnumValuesList(parameter, valuesList);

                    // Handle nested enumerations
                    if (field.Type == AutomationFieldType.NestedEnumeration)
                    {
                        valuesList.SelectionMode = ListSelectionMode.Single;

                        // Bind sub repeater.
                        SubValues.DataSource = ((SecurityParameter.NestedEnumeratedParmeter)parameter).SubParameters;
                        SubValues.DataBind();
                    }

                    break;
                case AutomationFieldType.Range:
                    // Hide EnumField elements
                    EnumField.Style.Add(HtmlTextWriterStyle.Display, "none");
                    break;
                default:
                    throw new ArgumentOutOfRangeException("AutomationField.Type", "Unknown automation field type value: " + field.Type.ToString());
            }
        }

        protected void SubValues_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }
            SecurityParameter.EnumeratedParmeter parameter = args.Item.DataItem as SecurityParameter.EnumeratedParmeter;

            // Set div name
            HtmlGenericControl subField = args.Item.FindControl("subField") as HtmlGenericControl;
            subField.Attributes.Add("data-id", parameter.Id);
            subField.Attributes.Add("data-rep", parameter.FriendlyName);
            subField.Attributes.Add("data-type", AutomationFieldType.Enumeration.ToString());  // Cheat for now. Just return Enumeration.

            Literal SubFieldName = subField.FindControl("SubFieldName") as Literal;
            SubFieldName.Text = parameter.FriendlyName;

            HtmlGenericControl SubEnumField = args.Item.FindControl("SubEnumField") as HtmlGenericControl;

            // Set EnumField elements
            ListBox valuesList = SubEnumField.FindControl("SubValues") as ListBox;
            BindEnumValuesList(parameter, valuesList);
        }

        private void BindEnumValuesList(SecurityParameter.Parameter parameter, ListBox valuesList)
        {
            AutomationFieldValue[] values;
            IList<SecurityParameter.EnumMapping> enumMapping;
            if (parameter is SecurityParameter.EnumeratedParmeter)
            {
                enumMapping = ((SecurityParameter.EnumeratedParmeter)parameter).EnumMapping;
            }
            else if (parameter.Type == SecurityParameter.SecurityParameterType.Boolean)
            {
                enumMapping = FeeServiceParser.BOOLEAN_ENUM_MAPPING;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Unhandled automation field type.");
            }

            values = new AutomationFieldValue[enumMapping.Count];

            for (int x = 0; x < values.Length; x++)
            {
                values[x] = new AutomationFieldValue()
                {
                    Rep = enumMapping[x].FriendlyValue.Replace(' ', '\u00A0'), // OPM 226337 - Convert spaces to &nbsp; to preserve multiple spaces.
                    Value = enumMapping[x].RepValue,
                    IsSelected = false
                };
            }

            valuesList.DataSource = values.OrderBy(val => val.Rep);
            valuesList.DataTextField = "Rep";
            valuesList.DataValueField = "Value";
            valuesList.DataBind();
        }

        private void BindHudLines(DropDownList ddlHudLine)
        {
            ddlHudLine.Items.Clear();
            ddlHudLine.Items.Add("");

            ddlHudLine.Items.Add(new ListItem("Calculation Options Outside HUD Lines", "Options"));

            // 800
            ddlHudLine.Items.Add(new ListItem("801 - Loan origination fee",         "801"));
            ddlHudLine.Items.Add(new ListItem("802 - Credit (-) or Charge (+)",     "802"));
            ddlHudLine.Items.Add(new ListItem("804 - Appraisal fee",                "804"));
            ddlHudLine.Items.Add(new ListItem("805 - Credit report",                "805"));
            ddlHudLine.Items.Add(new ListItem("806 - Tax service fee",              "806"));
            ddlHudLine.Items.Add(new ListItem("807 - Flood Certification",          "807"));
            ddlHudLine.Items.Add(new ListItem("808 - Mortgage broker fee",          "808"));
            ddlHudLine.Items.Add(new ListItem("809 - Lender's inspection fee",      "809"));
            ddlHudLine.Items.Add(new ListItem("810 - Processing fee",               "810"));
            ddlHudLine.Items.Add(new ListItem("811 - Underwriting fee",             "811"));
            ddlHudLine.Items.Add(new ListItem("812 - Wire transfer",                "812"));
            ddlHudLine.Items.Add(new ListItem("813 - Custom HUD Line",              "813"));
            ddlHudLine.Items.Add(new ListItem("814 - Custom HUD Line",              "814"));
            ddlHudLine.Items.Add(new ListItem("815 - Custom HUD Line",              "815"));
            ddlHudLine.Items.Add(new ListItem("816 - Custom HUD Line",              "816"));
            ddlHudLine.Items.Add(new ListItem("817 - Custom HUD Line",              "817"));

            // 900
            ddlHudLine.Items.Add(new ListItem("901 - Interest",                     "901"));
            ddlHudLine.Items.Add(new ListItem("902 - Mortgage Insurance Premium",   "902"));
            ddlHudLine.Items.Add(new ListItem("903 - Haz Ins.",                     "903"));
            ddlHudLine.Items.Add(new ListItem("904 - Custom HUD Line",              "904"));
            ddlHudLine.Items.Add(new ListItem("905 - VA Funding Fee",                "905"));
            ddlHudLine.Items.Add(new ListItem("906 - Custom HUD Line",              "906"));

            // 1000
            ddlHudLine.Items.Add(new ListItem("1002 - Haz ins. reserve",            "1002"));
            ddlHudLine.Items.Add(new ListItem("1003 - Mtg ins. reserve",            "1003"));
            ddlHudLine.Items.Add(new ListItem("1004 - Tax resrv ",                  "1004"));
            ddlHudLine.Items.Add(new ListItem("1005 - School taxes",                "1005"));
            ddlHudLine.Items.Add(new ListItem("1006 - Flood ins. reserve",          "1006"));
            ddlHudLine.Items.Add(new ListItem("1007 - Aggregate adjustment",        "1007"));
            ddlHudLine.Items.Add(new ListItem("1008 - Custom HUD Line",             "1008"));
            ddlHudLine.Items.Add(new ListItem("1009 - Custom HUD Line",             "1009"));
            if (BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).EnableAdditionalSection1000CustomFees)
            {
                ddlHudLine.Items.Add(new ListItem("1010 - Custom HUD Line",             "1010"));
                ddlHudLine.Items.Add(new ListItem("1011 - Custom HUD Line",             "1011"));
            }

            // 1100
            ddlHudLine.Items.Add(new ListItem("1102 - Closing/Escrow Fee",          "1102"));
            ddlHudLine.Items.Add(new ListItem("1103 - Owner's title Insurance",     "1103"));
            ddlHudLine.Items.Add(new ListItem("1104 - Lender's title Insurance",    "1104"));
            ddlHudLine.Items.Add(new ListItem("1109 - Doc preparation fee",         "1109"));
            ddlHudLine.Items.Add(new ListItem("1110 - Notary fees",                 "1110"));
            ddlHudLine.Items.Add(new ListItem("1111 - Attorney fees",               "1111"));
            ddlHudLine.Items.Add(new ListItem("1112 - Custom HUD Line",             "1112"));
            ddlHudLine.Items.Add(new ListItem("1113 - Custom HUD Line",             "1113"));
            ddlHudLine.Items.Add(new ListItem("1114 - Custom HUD Line",             "1114"));
            ddlHudLine.Items.Add(new ListItem("1115 - Custom HUD Line",             "1115"));

            // 1200
            ddlHudLine.Items.Add(new ListItem("1201 - Recording fees",              "1201"));
            ddlHudLine.Items.Add(new ListItem("1204 - City tax/stamps",             "1204"));
            ddlHudLine.Items.Add(new ListItem("1205 - State tax/stamps",            "1205"));
            ddlHudLine.Items.Add(new ListItem("1206 - Custom HUD Line",             "1206"));
            ddlHudLine.Items.Add(new ListItem("1207 - Custom HUD Line",             "1207"));
            ddlHudLine.Items.Add(new ListItem("1208 - Custom HUD Line",             "1208"));

            // 1300
            ddlHudLine.Items.Add(new ListItem("1302 - Pest Inspection",             "1302"));
            ddlHudLine.Items.Add(new ListItem("1303 - Custom HUD Line",             "1303"));
            ddlHudLine.Items.Add(new ListItem("1304 - Custom HUD Line",             "1304"));
            ddlHudLine.Items.Add(new ListItem("1305 - Custom HUD Line",             "1305"));
            ddlHudLine.Items.Add(new ListItem("1306 - Custom HUD Line",             "1306"));
            ddlHudLine.Items.Add(new ListItem("1307 - Custom HUD Line",             "1307"));
        }

        private void BindFeeTypes(DropDownList ddlFeeType)
        {
            BrokerDB broker = PrincipalFactory.CurrentPrincipal.BrokerDB;

            ddlFeeType.Items.Clear();
            ddlFeeType.Items.Add(new ListItem("", ""));
            ddlFeeType.Items.Add(new ListItem("Calculation Options Outside HUD Lines", "Options"));
            ddlFeeType.Items.Add(new ListItem("Lender Credits", "LENDER_CREDITS"));

            // OPM 243540 - Add ability to order different title fees from different vendors based on Fee Service.
            if (broker.EnableEnhancedTitleQuotes)
            {
                ddlFeeType.Items.Add(new ListItem("Title Fees Source", "TITLE_SOURCE"));
                ddlFeeType.Items.Add(new ListItem("Closing Fees Source", "TITLE_SOURCE"));
                ddlFeeType.Items.Add(new ListItem("Recording Fees Source", "TITLE_SOURCE"));
            }

            foreach (FeeSetupClosingCostFee fee in broker.GetUnlinkedClosingCostSet().GetFees(null).OrderBy(h => h.HudLine).ThenBy(d => d.Description))
            {
                // OPM 217028 - Remove Housing Expenses related fees.
                if (feesToBeRemoved.Contains(fee.ClosingCostFeeTypeId))
                {
                    continue;
                }
                ddlFeeType.Items.Add(new ListItem(fee.HudLine_rep + " - " + fee.Description, fee.ClosingCostFeeTypeId.ToString()));
            }

            // Add Housing Expenses
            ddlFeeType.Items.Add(new ListItem("Hazard Insurance", DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Flood Insurance", DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Windstorm Insurance", DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Condo HO-6 Insurance", DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Property Taxes", DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("School Taxes", DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Other Tax Expense 1", DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Other Tax Expense 2", DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Other Tax Expense 3", DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Other Tax Expense 4", DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Homeowner's Association Dues", DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Ground Rent", DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId.ToString()));
            //ddlFeeType.Items.Add(new ListItem("Aggregate Escrow Calculation", DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId.ToString()));
            ddlFeeType.Items.Add(new ListItem("Mortgage Insurance", DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId.ToString()));
        }

        private void BindGfeBoxes(DropDownList ddlGfeBox)
        {
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("A1", E_GfeSectionT.B1));
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("A2", E_GfeSectionT.B2));
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("B3", E_GfeSectionT.B3));
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("B4", E_GfeSectionT.B4));
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("B5", E_GfeSectionT.B5));
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("B6", E_GfeSectionT.B6));
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("B7", E_GfeSectionT.B7));
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("B8", E_GfeSectionT.B8));
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("B9", E_GfeSectionT.B9));
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("B10", E_GfeSectionT.B10));
            ddlGfeBox.Items.Add(Tools.CreateEnumListItem("B11", E_GfeSectionT.B11));
        }

        private void BindPaidBy(DropDownList ddlPaidBy)
        {
            ddlPaidBy.Items.Add(Tools.CreateEnumListItem("borr pd", E_ClosingCostFeePaymentPaidByT.Borrower));
            ddlPaidBy.Items.Add(Tools.CreateEnumListItem("seller", E_ClosingCostFeePaymentPaidByT.Seller));
            ddlPaidBy.Items.Add(Tools.CreateEnumListItem("borr fin", E_ClosingCostFeePaymentPaidByT.BorrowerFinance));
            ddlPaidBy.Items.Add(Tools.CreateEnumListItem("lender", E_ClosingCostFeePaymentPaidByT.Lender));
            ddlPaidBy.Items.Add(Tools.CreateEnumListItem("broker", E_ClosingCostFeePaymentPaidByT.Broker));
            ddlPaidBy.Items.Add(Tools.CreateEnumListItem("other", E_ClosingCostFeePaymentPaidByT.Other));
        }

        private void BindPayable(DropDownList ddlPayable)
        {
            ddlPayable.Items.Add(Tools.CreateEnumListItem("at closing", E_GfeClosingCostFeePaymentTimingT.AtClosing));
            ddlPayable.Items.Add(Tools.CreateEnumListItem("outside of closing", E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing));
        }

        private void BindTitleVendorDdl(DropDownList ddl)
        {
            var associations = TitleProvider.GetAssociations(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);

            // Only add blank option if other options exist.
            if (associations.Count > 0)
            {
                ddl.Items.Add(new ListItem("", ""));
            }

            foreach (var association in associations)
            {
                ddl.Items.Add(new ListItem(association.VendorName, association.VendorId.ToString()));
            }
        }
    }
}
