<%@ Page language="c#" Codebehind="RolodexList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RolodexList" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="AgentRecord" Src="~/newlos/Status/AgentRecord.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>Contacts</title>
        <style type="text/css">
            .btnSelect, .btnClose {
                width: 60px;
            }
            #searchControlsRow {
                height: 30px;
                width: 100%;
            }
            .uniqueEmailsCount {
                font-weight:bold;
                color: blue;
            }
            .rightPadding{
                padding-right: 10px;
            }
        </style>
	</HEAD>
	<body class="EditBackground" scroll="yes" MS_POSITIONING="FlowLayout">
<script type="text/javascript" src="../inc/common.js"></script>
<script type="text/javascript">
    var enableBatchSelEmails;
	function _init() {
	    enableBatchSelEmails = ML.ShowAssignedEmployees;
	    <% if ( !Page.IsPostBack ) { %>
	        resize( 800 , 400 );
	        if (enableBatchSelEmails)
            {
	            getEmailsFromParent();
	        }
	    <% } %>

	    if (enableBatchSelEmails)
	    {
	        $j('.oldBtnClose').hide();
	    }
	    else
	    {
	        $j(".btnSelect").hide();
	        $j('.newBtnClose').hide();
	    }


        if (<%= AspxTools.JsNumeric(selectedTabIndex) %> === 4)
        {
            document.getElementById("searchControlsRow").style.height = 0;
	    }

		if (enableBatchSelEmails)
		{
		    var emailList = getHiddenEmailList();
		    initCheckBoxes();
		    if (emailList.length > 0)
		    {
		        updateUniqueEmailsCount(emailList.length);
		        enableSelBtn(true);
		    }
		    else
		    {
		        enableSelBtn(false);
		    }
		}

        if (typeof _initControl === 'function') {
            _initControl();
        }
    }

    if (!String.prototype.trim){
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, "");
        };
    }

	function getHiddenEmailList(){
		if ($j('input#hiddenEmailList').val() !== "")
		{
		    return JSON.parse($j('input#hiddenEmailList').val());
		}
		else
		{
		    return new Array();
		}
	}

	function setHiddenEmailList(input){
	    $j('input#hiddenEmailList').val(JSON.stringify(input));
	}

	function getEmailsFromParent()
	{
	    if (!isLqbPopup(window))
	    {
	        return;
	    }
	    var parentEmails = parent.LQBPopup.GetArguments();
	    if (parentEmails !== null)
	    {
	        var emailArray = parentEmails.split(';');
	        var emailList = getHiddenEmailList();
	        for(var i = 0; i < emailArray.length; i++ )
	        {
	            if (isEmailValid(emailArray[i]))
	            {
	                emailList.push(emailArray[i].trim());
	            }
	        }
	        setHiddenEmailList(emailList);
	    }
	}

	function isEmailValid(email)
	{
	    var isValid = true;
	    if ( email == null || email == '' )
	    {
	        isValid = false;
	    }
	    else
	    {
	        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	        isValid = re.test(email.trim());
	    }
	    return isValid;
	}

	function onSelect()
	{
	    var returnObj =
            {
	            OK: true,
	            emailList: getHiddenEmailList()
	        }
	    parent.LQBPopup.Return(returnObj);
	}

	function includes(arr, value)
	{
		for (var i = 0; i < arr.length; i++)
		{
		    if (arr[i] === value)
		        return true;
		}

		return false;
	}

	function updateUniqueEmailsCount(length)
	{
	    if (length === 1)
	    {
	        $j(".uniqueEmailsCount").text(length + " unique email address selected ");
	    }
	    else
	    {
	        $j(".uniqueEmailsCount").text(length + " unique email addresses selected ");
	    }
	}

	function initCheckBoxes()
	{
	    var checkBoxList = document.getElementsByName("batchSel");
	    if (checkBoxList === undefined)
	    {
	        return;
	    }

	    var emailList = getHiddenEmailList();

	    for (var i = 0; i < checkBoxList.length; i++)
	    {
	        if (checkBoxList[i].value === "")
	        {
	            checkBoxList[i].disabled = true;
	            continue;
	        }
	        for (var j = 0; j < emailList.length; j++)
	        {
	            if (checkBoxList[i].value === emailList[j])
	            {
	                checkBoxList[i].checked = true;
	                break;
	            }
	        }
	    }
	}

	function enableSelBtn(input)
	{
	    $j(".btnSelect").prop('disabled', !input);
	    $j(".uniqueEmailsCount").toggle(input);
	}

      function onCheckRow(checkBox)
	  {
          var emailList = getHiddenEmailList();

          var checkBoxList = document.getElementsByName("batchSel");

          if (checkBox.checked)
          {
              if ($j(".btnSelect").prop("disabled"))
              {
                  enableSelBtn(true);
              }

              if (!includes(emailList,checkBox.value))
              {
                  emailList.push(checkBox.value);
                  setHiddenEmailList(emailList);
              }

              for (var i  = 0; i < checkBoxList.length; i++)
              {
                  if (checkBoxList[i].value === checkBox.value && checkBoxList[i].checked === false)
                  {
                      checkBoxList[i].checked = true;
                  }
              }
          }
          else
          {
              emailList.splice($j.inArray(checkBox.value, emailList), 1);
              setHiddenEmailList(emailList);

              for (var i  = 0; i < checkBoxList.length; i++)
              {
                  if (checkBoxList[i].value === checkBox.value && checkBoxList[i].checked === true)
                  {
                      checkBoxList[i].checked = false;
                  }
              }

              if (!$j(".btnSelect").prop("disabled") && emailList.length === 0)
              {
                  enableSelBtn(false);
              }
          }

          updateUniqueEmailsCount(emailList.length);
      }

      function onClose(args)
      {
          if(args == null)
          {
              args = {OK: false};
          }

          onClosePopup(args)
      }

      function ClearContact()
      {
        var args = {
            OK: true,
            Clear: true
        };

        onClosePopup(args);
      }

      function AddAgent()
      {
        var args = getAllFormValues();
        args.loanid = ML.sLId;

        if (typeof (postGetAllFormValues) == "function")
            postGetAllFormValues(args);

        var result = gService.rolodex_list.call("AddAgent", args);

        if (!result.error)
        {
            var args = {
                OK: true,
                RecordId : result.value.RecordId,
                AgentType : result.value.AgentType
            };

            onClosePopup(args);
        }
        else if (result.UserMessage)
        {
            alert(result.UserMessage);
        }
      }

      function select(name, type, typeOtherDescription, streetAddr, city, state, zip, altPhone, companyName, email, fax, phone, title, department, pager, agentLicenseNumber, companyLicenseNumber, phoneOfCompany, faxOfCompany, isNotifyWhenLoanStatusChange, addressOfBranch, cityOfBranch, stateOfBranch, zipOfBranch, phoneOfBranch, faxOfBranch, addressOfCompany, cityOfCompany, stateOfCompany, zipOfCompany, commissionPointOfLoanAmount, commissionPointOfGrossProfit, commissionMinBase, losIdentifier, companyLosIdentifier, licensesXml, companyLicensesXml, employeeId, companyId, employeeIDInCompany,
                      branchName, payToBankName, payToBankCityState, payToABANumber, payToAccountName, payToAccountNumber, furtherCreditToAccountName, furtherCreditToAccountNumber, brokerLevelAgentID, contactIsFromRolodex, chumsID, notes, recordId, overrideLicenses, isLender, isOriginator, county) {
          var arg = window.dialogArguments || {};
            arg.OK = true;
            arg.AgentName = name;
            arg.AgentType = type;
            arg.AgentTypeOtherDescription = typeOtherDescription;
            arg.AgentAltPhone = altPhone;
            arg.AgentCompanyName = companyName;
            arg.AgentStreetAddr = streetAddr;
		    arg.AgentCity = city;
		    arg.AgentState = state;
		    arg.AgentZip = zip;
		    arg.AgentCounty = county;
            arg.AgentEmail = email;
            arg.AgentFax = fax;
            arg.AgentPhone = phone;
            arg.AgentTitle = title;
            arg.AgentDepartmentName = department;
            arg.AgentPager = pager;
            arg.AgentLicenseNumber = agentLicenseNumber;
            arg.CompanyStreetAddr = addressOfCompany;
		    arg.CompanyCity = cityOfCompany;
		    arg.CompanyState = stateOfCompany;
		    arg.CompanyZip = zipOfCompany;
		    arg.PhoneOfCompany = phoneOfCompany;
		    arg.FaxOfCompany = faxOfCompany;
            arg.CompanyLicenseNumber = companyLicenseNumber;
            arg.IsNotifyWhenLoanStatusChange = isNotifyWhenLoanStatusChange;
            arg.CommissionPointOfLoanAmount = commissionPointOfLoanAmount;
            arg.CommissionPointOfGrossProfit = commissionPointOfGrossProfit;
            arg.CommissionMinBase = commissionMinBase;
            arg.LoanOriginatorIdentifier = losIdentifier;
            arg.CompanyLoanOriginatorIdentifier = companyLosIdentifier;
            arg.AgentLicensesPanel = licensesXml;
            arg.CompanyLicensesPanel = companyLicensesXml;
            arg.EmployeeIDInCompany = employeeIDInCompany;
            arg.BrokerLevelAgentID = brokerLevelAgentID;
            arg.ShouldMatchBrokerContact = contactIsFromRolodex === "1";
            if(arg.ShouldMatchBrokerContact)
            {
                arg.AgentSourceT = 2; // from contact list
            }

            if(<%= AspxTools.JsNumeric(selectedTabIndex) %> === 0 || <%= AspxTools.JsNumeric(selectedTabIndex) %> === 1)
            {
                arg.CompanyStreetAddr = streetAddr;
			    arg.CompanyCity = city;
			    arg.CompanyState = state;
			    arg.CompanyZip = zip;
			    arg.CompanyId = companyId;
            }
            arg.EmployeeId  = employeeId;

            // OPM 109299
            arg.BranchName = branchName;
            arg.PayToBankName = payToBankName;
            arg.PayToBankCityState = payToBankCityState;
            arg.PayToABANumber = payToABANumber;
            arg.PayToAccountName = payToAccountName;
            arg.PayToAccountNumber = payToAccountNumber;
            arg.FurtherCreditToAccountNumber = furtherCreditToAccountNumber;
            arg.FurtherCreditToAccountName = furtherCreditToAccountName;
            arg.ChumsId = chumsID;
            arg.AgentNote = notes;
            arg.RecordId = recordId;
            arg.OverrideLicenses = overrideLicenses === "1";
            arg.IsLender = isLender === "1";
            arg.IsOriginator = isOriginator === "1";

			onClosePopup(arg);
      }

<%-- 02/17/06 MF. OPM 4052. Now allow users to add and edit entries from here.
     Instead of standard refresh after, recreate search string so type filter does
     not revert back to original view. --%>

		    function onAddClick() {
		        showModal('/los/rolodex/rolodex.aspx', null, null, null, onAddClickCallback, {hideCloseButton: true} );
		    }

		    function onAddClickCallback(args)
		    {
		        if (args.OK) {
		            var search = '?type=' + <%= AspxTools.JsGetElementById(m_typeFilter) %>.value + '&tab=' + <%= AspxTools.JsString(selectedTabIndex) %>;
                      if (ML && typeof ML.sLId === 'string') {
                          search += '&loanid=' + ML.sLId;
                      }
		            window.location.search = search;
		        }
            }

		    function onEditClick(agentid) {
		        showModal('/los/rolodex/rolodex.aspx?cmd=edit&agentid=' + agentid, null, null, null, onEditClickReturn, {hideCloseButton: true} );
		    }

		    function onEditClickReturn(args)
		    {
		        if (args.OK) {
		            var search = '?type=' + <%= AspxTools.JsGetElementById(m_typeFilter) %>.value+ '&tab=' + <%= AspxTools.JsString(selectedTabIndex) %>;
              if (ML && typeof ML.sLId === 'string') {
                  search += '&loanid=' + ML.sLId;
              }
              window.location.search = search;
          }
      }
</script>
        <h4 class="page-header">Choose from Contacts</h4>
		<form id="RolodexList" method="post" runat="server">
            <input type="hidden" id="hiddenEmailList" value="" runat="server" />
			<table class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr id="TabsRow" runat="server">
					<td class="FormTableHeader" style="PADDING-TOP: 5px" colSpan="2">
						<div class="Tabs">
                            <div align="right">
                                <span class="uniqueEmailsCount"></span>
                                <input onclick="onSelect();" type="button" value="Select" class="btnSelect" />
                                <input onclick="onClose();" type="button" value="Close" class="btnClose newBtnClose" />
                            </div>
						    <ul class="tabnav">
						        <li id="tab0" runat="server"><a onclick="__doPostBack('changeTab', 0)"; href="#">Current Loan Agents</a></li>
						        <li id="tab1" runat="server"><a onclick="__doPostBack('changeTab', 1)"; href="#">Contact Entries</a></li>
						        <li id="tab2" runat="server"><a onclick="__doPostBack('changeTab', 2)"; href="#">Internal Employees</a></li>
						        <li id="tab3" runat="server"><a onclick="__doPostBack('changeTab', 3)"; href="#">PML Users</a></li>
						        <li id="tab4" runat="server"><a onclick="__doPostBack('changeTab', 4)"; href="#">Assigned/Official Contacts</a></li>
						        <% if (IsFeeContactPicker) { %>
						        <li id="tab5" runat="server"><a onclick="__doPostBack('changeTab', 5)"; href="#">Manual Entry</a></li>
						        <% } %>
						    </ul>
						</div>
					</td>
				</tr>
				<tr id="ContactInfoRow" runat="server" visible="false">
				    <td style="padding-left:10px;">
				        <uc:AgentRecord id="ContactInfo" runat="server" />
				        <br />
				        <div style="text-align:center" runat="server">
				            <input type="button" value="Add Agent" onclick="AddAgent();" />
				            <input type="button" value="Cancel" onclick="onClosePopup();" />
				        </div>
				    </td>
				</tr>
				<tr id="searchControlsRow" >
					<td class="FieldLabel">&nbsp;<asp:PlaceHolder runat="server" ID="SearchControls" >
					    <span style="MARGIN-LEFT: 4px; MARGIN-RIGHT: 4px">Search for:
							&nbsp;
							<asp:textbox id="m_searchFilter" runat="server" Width="100"></asp:textbox><asp:button id="m_searchButton" runat="server" Width="60" Text="Search"></asp:button>&nbsp;
							Type: &nbsp;
							<asp:dropdownlist id="m_typeFilter" runat="server" AutoPostBack="true" style="width:195px;"></asp:dropdownlist>&nbsp;
							<ml:EncodedLiteral ID="m_statusLiteral" Text="Status: " runat="server" />
							<asp:DropDownList ID="m_statusFilter" runat="server" AutoPostBack="true" />&nbsp;
						    </asp:PlaceHolder>
						</span>
					</td>
					<td align="right" class="rightPadding"><asp:panel id="m_addPanelTop" style="DISPLAY: inline" runat="server" Width="100%">
							<INPUT onclick="onAddClick();" type="button" value="Add new entry">
						</asp:panel><input onclick="onClose();" type="button" value="Close" class="btnClose oldBtnClose" />
					</td>
				</tr>
				<tr style="display:none">
					<asp:Panel ID="m_importFrom" runat="server" Visible="False">
						<TD>
							<TABLE class="FormTable">
								<TR>
									<TD class="FieldLabel" vAlign="middle">Populate Address/Phone Info from:
									</TD>
									<TD class="FieldLabel" vAlign="bottom">
										<asp:radiobuttonlist id="m_importFromRadio" Runat="server" RepeatDirection="Horizontal">
											<asp:ListItem Text="Employee" Value="E" Selected="True"></asp:ListItem>
											<asp:ListItem Text="Employee's Branch" Value="B" Selected="False"></asp:ListItem>
											<asp:ListItem Text="Company" Value="C" Selected="False"></asp:ListItem>
										</asp:radiobuttonlist></TD>
								</TR>
							</TABLE>
						</TD>
					</asp:Panel>
				</tr>
				<tr>
					<td align="center" colSpan="2"><ml:commondatagrid id="m_rolodexGrid" OnItemDataBound="RolodexItemBound" runat="server" DataKeyField="AgentID">
							<alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
							<itemstyle cssclass="GridItem"></itemstyle>
							<headerstyle cssclass="GridHeader"></headerstyle>
							<columns>
								<asp:TemplateColumn>
									<itemtemplate>
										<%# AspxTools.HtmlControl(GenerateSelectLink(Container.DataItem)) %>
									</itemtemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<a href="#" onclick='onEditClick("<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AgentID")) %>");'>
											edit </a>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="AgentTypeDesc" SortExpression="AgentTypeDesc" HeaderText="Type" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
								<asp:BoundColumn DataField="AgentNm" SortExpression="AgentNm" HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
								<asp:BoundColumn DataField="AgentTitle" SortExpression="AgentTitle" HeaderText="Title" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
								<asp:boundcolumn datafield="AgentPhone" HeaderText="Agent Phone" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:boundcolumn>
								<asp:BoundColumn DataField="AgentComNm" SortExpression="AgentComNm" HeaderText="Company Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
								<asp:TemplateColumn SortExpression="AgentComNmBranchNm" HeaderText="Company Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
								    <ItemTemplate>
								        <%# AspxTools.HtmlString(GetSafeCompanyNmBranchNm(Container.DataItem)) %>
								    </ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="NameOfBranch" HeaderText="Branch Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
								    <ItemTemplate>
								        <%# AspxTools.HtmlString(GetSafeBranchNm(Container.DataItem)) %>
								    </ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="PhoneOfCompany" HeaderText="Company Phone" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Email" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
								    <ItemTemplate>
								        <a runat="server"   id="EmailLink"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem,"AgentEmail")) %> </a>
								    </ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn SortExpression="AgentAddr,AgentCity,AgentState,AgentZip" HeaderText="Address" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
								    <ItemTemplate>
								        <%# AspxTools.HtmlString(GetFullAddress(Container.DataItem)) %>
								    </ItemTemplate>
								</asp:TemplateColumn>
							</columns>
						</ml:commondatagrid><asp:panel id="m_rolodexDenied" style="PADDING-RIGHT: 60px; PADDING-LEFT: 60px; PADDING-BOTTOM: 60px; FONT: 12px arial; COLOR: red; PADDING-TOP: 60px; TEXT-ALIGN: center" runat="server">Access denied.&nbsp; You do not have
      permission to view contact entries.&nbsp; Please consult your account
      administrator if you have any questions. </asp:panel><asp:panel id="m_chooseSearchTerm" style="PADDING-RIGHT: 60px; PADDING-LEFT: 60px; PADDING-BOTTOM: 60px; FONT: 12px arial; COLOR: red; PADDING-TOP: 60px; TEXT-ALIGN: center" runat="server">Please choose a search term above.
      </asp:panel></td>
				</tr>
				<tr height="30">
					<td align="right" colSpan="2" class="rightPadding"><asp:panel id="m_addPanelBottom" style="DISPLAY: inline" runat="server" Width="100%">
							<INPUT onclick="onAddClick();" type="button" value="Add new entry">
						</asp:panel>
                        <span class="uniqueEmailsCount"></span>
                        <input onclick="onSelect()" type="button" value="Select" class="btnSelect" />
                        <input onclick="onClose();" type="button" value="Close" class="btnClose" />

						<div style="text-align:center" runat="server" id="ClearContainer"><input type="button" value="Clear Agent" onclick="ClearContact();" /></div>

						<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
