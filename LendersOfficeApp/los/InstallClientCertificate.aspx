﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InstallClientCertificate.aspx.cs" Inherits="LendersOfficeApp.los.InstallClientCertificate" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body bgcolor="gainsboro">
<script type="text/javascript">
function _init() {
    resize(400, 200);
}
function f_close() {
    onClosePopup();
}

function f_getCertificate() {
    window.open('InstallClientCertificate.aspx?cmd=download&arg=' + ML.EncryptedString, '_self');
    return false;
}
function f_isValid() {

    if (<%= AspxTools.JsGetElementById(m_description) %>.value == '')
    {
        alert('Device name is required.');
        return false;
    }
    return true;
}
</script>
    <h4 class="page-header">Install Client Certificate</h4>
    <form id="form1" runat="server">
    <div id="Panel0" runat="server" style="padding:10px">
    Please specify a name to identify your device. (i.e: Work's laptop)
    <br /><br />
    Name: <asp:TextBox ID="m_description" runat="server" width="150px"/> 
    <br /><br />
    <asp:Button ID="m_continueBtn" runat="server" Text="Continue" OnClientClick="return f_isValid();" />&nbsp;<input type="button" value="Cancel" onclick="f_close();"/>
    </div>
    <div id="Panel1" runat="server" Visible="false" style="padding:10px;">
    The client certificate can be installed to authenticate a user and bypass the authentication code process. This should only be installed on a trusted machine. You will be required to enter the password below during installation.
        <br /><br />
<span style="font-weight:bold">Certificate Password:</span> <span style="font-size:16px"><ml:EncodedLiteral ID="m_certificatePassword" runat="server" /> </span>
        <br /><br />
        <input type="button" value="Get Certificate" onclick="return f_getCertificate();" />
    </div>
    </form>
</body>
</html>
