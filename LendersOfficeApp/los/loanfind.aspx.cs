using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.LoanSearch;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LendersOfficeApp.los.common;
using LendersOffice.Admin;

namespace LendersOfficeApp.los
{

    // 4/21/2006 mf OPM 4714 - We no longer allow the creation of loans from the search page.
    public partial class loanfind : LendersOffice.Common.BaseServicePage
    {
        protected string newSortExpression = "";
        protected string newSortDirection = "ASC";
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_managerChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_processorChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_telemarketerChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_agentChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_lenderAccountExecChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_realEstateAgentChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_underwriterChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_lockdeskChoice;
        protected LendersOfficeApp.los.common.EmployeeRoleChooserLink m_loanOpenerChoice;
        protected LendersOfficeApp.los.common.header m_header;


        List<EmployeeRoleChooserLink> m_linkList = null;
        // 7/18/2006 nw - OPM 6369 - Disallow users from running Advanced Search with no filter
        private bool m_bUsingAdvancedSearchFilter = false;

        // 7/26/2006 dl - OPM 6275 - Used to flag a blank search attempt
        private bool isBlankSearch = false;
        // 7/26/2006 dl - OPM 6275 - Decides whether partial search hint is displayed or not
        private bool showPartialHint = true;
        // 9/12/08 db - OPM 24639 - Create batch exporter for DataTrac
        private bool m_isDataTracIntegrationEnabled = false;

        protected Guid PMLDefaultLoanTemplateID = System.Guid.Empty; //OPM 18373 8/5/08 - Used to prevent deleting default loan Template 
        protected Guid QuickPricerTemplateId = System.Guid.Empty; //OPM 108979 7/2/13 - Used to prevent deleting QuickPricer Template 

        private void InitializeEmployeeRoleChooserLink(EmployeeRoleChooserLink link)
        {
            link.Data = "Any";
            link.Text = "Any";
            link.Mode = "Search";
            link.Width= "80px";
            link.Choices.Add(new FixedChoice("Any", "Any", "allow any"));
            link.Choices.Add(new FixedChoice("Not assigned", "Uny", "unassigned"));
            link.Choices.Add(new FixedChoice("assign", "assign", "pick user"));

        }
        private BrokerUserPrincipal CurrentUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected bool CanExportLoans
        {
            get { return CurrentUser.HasPermission(Permission.CanExportLoan); }
        }

        protected bool IsDataTracIntegrationEnabled
        {
            get { return m_isDataTracIntegrationEnabled; }
        }

        protected bool IsBecomePlusUser
        {
            get
            {
                bool ret = false;
                InternalUserPrincipal internalUserPrincipal = User as InternalUserPrincipal;

                if (internalUserPrincipal != null && internalUserPrincipal.IsInRole("IB"))
                    ret = true;

                return ret;
            }
        }
        protected bool SearchWasPerformed
        {
            get { return (m_bUsingAdvancedSearchFilter || isBlankSearch); }
        }
        protected string getExtraQueryString()
        {
            // 8/3/2005 kb - Removed all use of employees' ids in the request
            // string.  As of today, we don't support it and I don't want to
            // retro this code to work with the revised employee choosing.
            // If you need to support employee choices, just embed the ids
            // for each (no biggie).  The bigger change is to page loading.

            string query = "";

            query += "ssn=" + ssnLastFour.Value.TrimWhitespaceAndBOM();
            query += "&lastname=" + Server.HtmlEncode(m_lastNameTF.Text.TrimWhitespaceAndBOM());
            query += "&firstname=" + Server.HtmlEncode(m_firstNameTF.Text.TrimWhitespaceAndBOM());
            query += "&loannumber=" + m_loanNumberTF.Text.TrimWhitespaceAndBOM();
            query += "&loanreferencenumber=" + m_loanReferenceNumberTF.Text.TrimWhitespaceAndBOM();
            query += "&loanstatus=" + m_loanStatusDDL.SelectedItem.Value;
            query += "&leadstatus=" + m_leadStatusDDL.SelectedItem.Value;
            query += "&loantype=" + m_loanTypeDDL.SelectedItem.Value;
            query += "&loantemplate=" + m_searchForTempsRB.Checked;

            return Server.HtmlEncode(query);
        }
        private string TaskEditorUrl
        {
            get
            {
                BrokerDB brokerDB = BrokerDB.RetrieveById(CurrentUser.BrokerId);
                if (brokerDB.IsUseNewTaskSystem)
                {
                    return "/newlos/Tasks/TaskEditorV2.aspx";
                }
                else
                {
                    return "/los/reminders/TaskEditor.aspx";
                }
            }
        }
        protected void PageLoad(object sender, System.EventArgs a)
        {
            // 1/5/2005 kb - Get access control check credentials to
            // speed up lookup by removing overhead of retrieving this
            // info for each loan.  If we fail, then we won't be able
            // to read or edit any file.
            //
            // 8/3/2005 kb - Removed all use of employees' ids in the
            // request string.  As of today, we don't support it and I
            // don't want to retro this code to work with the revised
            // employee choosing.  If you need to support employee
            // choices, you need to retrieve the employee names by id
            // using a broker table data access object.  For each
            // employee bound through the query string, lookup the
            // full name and initialize the employee choice accordingly.


            BPSearch.Visible = CurrentUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
            ExternalPostCloserSearch.Visible = CurrentUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
            ExternalSecondarySearch.Visible = CurrentUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan);

            if (!Page.IsPostBack)
            {

                bool isSearch = false;                
                string qs = RequestHelper.GetSafeQueryString("quicksearch");
                bool isQuickSearch = (qs == null) ? false : (qs.TrimWhitespaceAndBOM() == "yes");
                bool allDates = false;
                if (RequestHelper.GetSafeQueryString("ssn") != null && RequestHelper.GetSafeQueryString("ssn").TrimWhitespaceAndBOM() != "")
                {
                    ssnLastFour.Value = RequestHelper.GetSafeQueryString("ssn");
                    if (ssnLastFour.Value.TrimWhitespaceAndBOM() != "*" && isQuickSearch)
                        allDates = true;
                    isSearch = true;
                }
                if (RequestHelper.GetSafeQueryString("lastname") != null && RequestHelper.GetSafeQueryString("lastname").TrimWhitespaceAndBOM() != "")
                {
                    m_lastNameTF.Text = RequestHelper.GetSafeQueryString("lastname");
                    if (m_lastNameTF.Text.TrimWhitespaceAndBOM() != "*" && isQuickSearch)
                        allDates = true;
                    isSearch = true;
                }
                if (RequestHelper.GetSafeQueryString("firstname") != null && RequestHelper.GetSafeQueryString("firstname").TrimWhitespaceAndBOM() != "")
                {
                    m_firstNameTF.Text = RequestHelper.GetSafeQueryString("firstname");
                    if (m_firstNameTF.Text.TrimWhitespaceAndBOM() != "*" && isQuickSearch)
                        allDates = true;
                    isSearch = true;
                }
                if (RequestHelper.GetSafeQueryString("loannumber") != null && RequestHelper.GetSafeQueryString("loannumber").TrimWhitespaceAndBOM() != "")
                {
                    m_loanNumberTF.Text = RequestHelper.GetSafeQueryString("loannumber");
                    if (m_loanNumberTF.Text.TrimWhitespaceAndBOM() != "*" && isQuickSearch)
                        allDates = true;
                    isSearch = true;
                }
                if (!string.IsNullOrWhiteSpace(RequestHelper.GetSafeQueryString("loanreferencenumber")))
                {
                    m_loanReferenceNumberTF.Text = RequestHelper.GetSafeQueryString("loanreferencenumber");
                    if (isQuickSearch)
                    {
                        allDates = true;
                    }

                    isSearch = true;
                }
                if (RequestHelper.GetSafeQueryString("loanstatus") != null && RequestHelper.GetSafeQueryString("loanstatus").TrimWhitespaceAndBOM() != "")
                {
                    Tools.SetDropDownListValue(m_loanStatusDDL, RequestHelper.GetSafeQueryString("loanstatus"));
                    isSearch = true;
                }
                if (RequestHelper.GetSafeQueryString("leadstatus") != null && RequestHelper.GetSafeQueryString("leadstatus").TrimWhitespaceAndBOM() != "")
                {
                    Tools.SetDropDownListValue(m_leadStatusDDL, RequestHelper.GetSafeQueryString("leadstatus"));
                    isSearch = true;
                }
                if (RequestHelper.GetSafeQueryString("loantype") != null && RequestHelper.GetSafeQueryString("loantype").TrimWhitespaceAndBOM() != "")
                {
                    Tools.SetDropDownListValue(m_loanTypeDDL, RequestHelper.GetSafeQueryString("loantype"));
                    isSearch = true;
                }
                if (RequestHelper.GetSafeQueryString("loantemplate") != null && RequestHelper.GetSafeQueryString("loantemplate").TrimWhitespaceAndBOM() != "")
                {
                    if (RequestHelper.GetSafeQueryString("loantemplate").TrimWhitespaceAndBOM().ToLower() == "true" || RequestHelper.GetSafeQueryString("loantemplate").TrimWhitespaceAndBOM() == "1")
                    {
                        m_searchForTempsRB.Checked = true;
                    }
                    else
                    {
                        m_searchForLoansRB.Checked = true;
                    }
                    isSearch = true;
                }
                if (RequestHelper.GetSafeQueryString("propertyaddress") != null && RequestHelper.GetSafeQueryString("propertyaddress").TrimWhitespaceAndBOM() != "")
                {
                    m_propertyAddressTF.Text = RequestHelper.GetSafeQueryString("propertyaddress");
                    if (m_propertyAddressTF.Text.TrimWhitespaceAndBOM() != "*" && isQuickSearch)
                        allDates = true;
                    isSearch = true;
                }
                if (RequestHelper.GetSafeQueryString("sStatusD") != null && RequestHelper.GetSafeQueryString("sStatusD").TrimWhitespaceAndBOM() != "")
                {
                    Tools.SetDropDownListValue(m_createdDateDDL, RequestHelper.GetSafeQueryString("sStatusD").TrimWhitespaceAndBOM());
                    isSearch = true;
                }

                if (allDates)
                {
                    m_createdDateDDL.SelectedIndex = 0;
                }

                if (isSearch)
                {
                    onFindLoanClick(null, null);
                }
                else
                {
                    // 2/10/2005 kb - Auto-select the branch on startup so
                    // we limit the search space for users with broker
                    // level access.
                    //
                    // 4/21/2005 kb - We now update the branch for broker level
                    // users when we don't auto-search.  If the user is not
                    // broker-level in access, then the branch ddl should be
                    // empty with one entry: n/a.
                    //
                    // 6/14/06 mf - OPM 5567 to be consistent with quick search,
                    // we now default to "Any Branch" for corporate users, instead
                    // of their branch.
                    /*

                    */
                    //
                    // 7/21/2006 nw - OPM 5567 - reverse Matthew's change on 6/14/2006

                    if (m_branchDDL.Enabled && m_branchDDL.Items.Count > 0)
                    {
                        m_branchDDL.SelectedIndex = 0;
                    }
                }
            }
            else
            {
                // AE 8/08 OPM 23440
                // Implemented custom paging for loan search results

                // CHECK IF SORT
                // IF SORT, CHANGE SORT AND DIR PARAM ON SQL QUERY


                if (String.Compare(Request.Form["__EVENTARGUMENT"], 0, "sort:", 0, "sort".Length) == 0)
                {
                    string[] args = Request.Form["__EVENTARGUMENT"].Split(':');
                    if (args.Length > 1)
                    {
                        args = args[1].Split(' ');
                        if (args.Length > 1)
                        {
                            newSortExpression = args[0];
                            newSortDirection = args[1];
                            m_loanDataGrid.CurrentPageIndex = 0;
                        }
                    }
                    BindGridAndUpdateCaps();
                }  // IF PAGE INDEX CHANGES, UPDATE PAGE AND TABLE
                else if (String.Compare(Request.Form["__EVENTTARGET"], 0, "m_loanDataGrid", 0, "m_loanDataGrid".Length) == 0)
                {
                    BindGridAndUpdateCaps();
                }
                else
                { // IF PERFORMING AN EMPTY SEARCH, RESET TABLE
                    ResetDataTable();
                }
            }

            BrokerDB broker = BrokerDB.RetrieveById(CurrentUser.BrokerId);
            PMLDefaultLoanTemplateID = broker.PmlLoanTemplateID;
            QuickPricerTemplateId = broker.QuickPricerTemplateId;
            m_isDataTracIntegrationEnabled = broker.IsDataTracIntegrationEnabled;
        }

        protected bool AllowToEdit(object dataItem)
        {
            // 11/3/2004 kb - In an effort to consolidate access control,
            // we now direct all load edit queries to a central helper.
            // This implementation will shift, soon, to one that checks
            // both native and override permissions.

            return (int)Eval("CanWrite") == 1;
        }

        protected bool AllowToAssign(object dataItem)
        {
            // 10/8/2004 kb - Added this to untie assigning and editing
            // to allow lender account execs the ability to bypass the
            // edit check.
            return AllowToEdit(dataItem);
        }

        private void BindCreatedDateDDL(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("<-- All dates -->", "0"));
            ddl.Items.Add(new ListItem("Today", "10"));
            ddl.Items.Add(new ListItem("Last 7 days", "1"));
            ddl.Items.Add(new ListItem("Last 90 days", "9"));
            ddl.Items.Add(new ListItem("This month", "2"));
            ddl.Items.Add(new ListItem("Previous month", "3"));
            ddl.Items.Add(new ListItem("Previous 2 months", "4"));
            ddl.Items.Add(new ListItem("Previous 3 months", "5"));
            ddl.Items.Add(new ListItem("Year to date", "6"));
            ddl.Items.Add(new ListItem("Previous year", "8"));
            ddl.Items.Add(new ListItem("Older than 3 months", "7"));

            if (RequestHelper.GetSafeQueryString("template") != null)
            {
                Tools.SetDropDownListValue(ddl, "0");
            }
            else
            {
                Tools.SetDropDownListValue(ddl, "9");
            }
        }
        protected void PageInit(object sender, System.EventArgs a)
        {
            m_linkList = new List<EmployeeRoleChooserLink>() 
			{
				m_managerChoice ,
				m_processorChoice ,
				m_agentChoice ,
				m_loanOpenerChoice ,
				m_telemarketerChoice ,
				m_realEstateAgentChoice ,
				m_lenderAccountExecChoice ,
				m_underwriterChoice,
				m_lockdeskChoice,
                m_closerChoice,
                m_shipperChoice,
                m_funderChoice,
                m_postCloserChoice,
                m_insuringChoice,
                m_collateralAgentChoice,
                m_docDrawerChoice,
                m_creditAuditorChoice,
                m_disclosureDeskChoice,
                m_juniorProcessorChoice,
                m_juniorUnderwriterChoice,
                m_legalAuditorChoice,
                m_loanOfficerAssistantChoice,
                m_purchaserChoice,
                m_qcComplianceChoice,
                m_secondaryChoice,
                m_servicingChoice
			};


            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            {
                m_linkList.Add(m_loanBPChoice);
                m_linkList.Add(m_externalSecondaryChoice);
                m_linkList.Add(m_externalPostCloserChoice);
            }
            foreach (EmployeeRoleChooserLink link in m_linkList)
            {
                InitializeEmployeeRoleChooserLink(link);
            }

            this.EnableJqueryMigrate = false;
            Tools.Bind_sStatusT_LoanFind(m_loanStatusDDL);
            Tools.Bind_sLoanStatusT(m_loanStatusDDL, CurrentUser.BrokerId, true);
            Tools.Bind_sLT_LoanFind(m_loanTypeDDL);
            BindCreatedDateDDL(m_createdDateDDL);
            m_loanStatusDDL.SelectedValue = "-1"; // <-- Any Status -->

            Tools.Bind_sStatusT_LeadFind(m_leadStatusDDL);
            Tools.Bind_sLeadStatusT(m_leadStatusDDL);
            m_leadStatusDDL.SelectedValue = "-1"; // <-- Any Status -->

            // OPM 4134 Ethan - required by gService call in f_chat (HeaderFrame.aspx)
            RegisterService("lookupRecord", "/common/RecordsLookupService.aspx");

            if (!CurrentUser.HasPermission(Permission.AllowCreatingTestLoans))
            {
                m_searchForTestLoansRB.Visible = false;
            }


            // OPM 3156 Ethan - if the url querystring passes a value for the 'template' variable, then
            //					the client is performing a loan template search, hence checking the Template radio button
            //					by default; otherwise the Loans radio button is checked by default
            if (RequestHelper.GetSafeQueryString("template") != null)
            {
                m_searchForTempsRB.Checked = true;
                m_searchForLoansRB.Checked = false;
            }

            // OPM 217783 - je - 07/29/2015 - Search for test loans if test parameter is set in query string.
            else if (RequestHelper.GetBool("test") && CurrentUser.HasPermission(Permission.AllowCreatingTestLoans))
            {
                m_searchForTestLoansRB.Checked = true;
                 m_searchForLoansRB.Checked = false;
            }

            m_header.ShowPipelineLink = true;
            m_header.ShowRecentWindowList = true;

            if (IsPostBack == false)
            {
                if (CurrentUser.HasPermission(Permission.BrokerLevelAccess) == true)
                {
                    // Only broker access level able to search by branch.
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerID", CurrentUser.BrokerId)
                                                };

                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(CurrentUser.BrokerId, "ListBranchByBrokerID", parameters ))
                    {
                        m_branchDDL.DataSource = reader;
                        m_branchDDL.DataValueField = "BranchID";
                        m_branchDDL.DataTextField = "Name";
                        m_branchDDL.DataBind();
                        m_branchDDL.Items.Insert(0, new ListItem("<-- Any branch -->", Guid.Empty.ToString()));
                    }
                }
                else
                {
                    m_branchDDL.Items.Insert(0, new ListItem("<-- N/A -->", Guid.Empty.ToString()));
                    m_branchDDL.Enabled = false;
                }
            }


            RegisterService("loanutils", "/newlos/LoanUtilitiesService.aspx");
            RegisterService("main", "/los/LoanFindService.aspx");
            RegisterJsGlobalVariables("TaskEditorUrl", TaskEditorUrl);
            RegisterJsScript("LQBPopup.js");
            RegisterJsScript("mask.js");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_loanDataGrid.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.m_loanDataGrid_PageIndexChanged); //
            this.m_loanDataGrid.ItemCreated += new DataGridItemEventHandler(m_loanDataGrid_ItemCreated);
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
        protected void m_loanDataGrid_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            m_loanDataGrid.CurrentPageIndex = e.NewPageIndex;

            BindGridAndUpdateCaps();
        }

        private void m_loanDataGrid_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            // AE 8/08 OPM 23440
            // Custom pager requires index link buttons

            if (e.Item.ItemType == ListItemType.Pager)
            {
                Label lblPagerText = new Label();
                lblPagerText.Text = "Page: ";

                if (e.Item.Controls[0].FindControl("lblPagerText") == null)
                {
                    e.Item.Controls[0].Controls.AddAt(0, lblPagerText);
                }
            }
        }


        protected void onFindLoanClick(object sender, System.EventArgs a)
        {
            // AE 8/08 Separated onFindLoanClick and BindGrid for better readability
            // OPM 23440
            m_loanDataGrid.CurrentPageIndex = 0;
            BindGridAndUpdateCaps();
        }

        private void BindGridAndUpdateCaps()
        {
            // AE 8/08 OPM 23440
            // Updated caps to reflect correct return numbers with new paging system

            BindLoanDataGrid();
            Int32 nL = m_loanDataGrid.VirtualItemCount;
            Int32 currentIndex = m_loanDataGrid.CurrentPageIndex;
            Int32 pageSize = m_loanDataGrid.PageSize;

            if (nL > pageSize)
            {
                Int32 maxResult = Math.Min(nL, (currentIndex + 1) * pageSize);
                Int32 minResult = (currentIndex * pageSize) + 1;

                m_resultCountLabel.Text = "Results <span style=\"COLOR: tomato;\">" + minResult + "-" + maxResult + "</span> of <span style=\"COLOR: tomato;\"> " + nL + "</span>";
            }
            else if (nL > 1)
            {
                m_resultCountLabel.Text = "Query returns " + nL + " results.";
            }
            else if (nL > 0)
            {
                m_resultCountLabel.Text = "Query returns " + nL + " result.";
            }
            else
            {
                if (m_bUsingAdvancedSearchFilter)
                {
                    m_resultCountLabel.Text = "Query returns " + nL + " results.";
                    if (nL == 0 && showPartialHint)
                        m_resultCountLabel.Text += "&nbsp; <span style=\"COLOR: tomato;\">** Hint:&nbsp; Try selecting the partial match checkboxes to return partial matches.</span>";
                }
                else
                {
                    m_resultCountLabel.Text = "<span style=\"COLOR: tomato;\">Please enter at least 1 search criterion to narrow down the search result</span>";
                    isBlankSearch = true;
                }
            }

            if (nL > 0)
            {
                m_resultCountCap.Text = m_resultCountLabel.Text;
            }
            else
            {
                m_resultCountCap.Text = "";
            }

            m_exportBtn.Enabled = CurrentUser.HasPermission(Permission.CanExportLoan);
            m_exportToDTBtn.Enabled = CurrentUser.HasPermission(Permission.CanExportLoan);
        }

        // AE 8/08 OPM 23440
        // New function for manual reset of datagrid inherited object
        // Custom paging requires viewstate to be enabled, must reset data/info manually sometimes

        private void ResetDataTable()
        {
            m_loanDataGrid.DataSource = null;
            m_loanDataGrid.DataBind();
            m_loanDataGrid.VirtualItemCount = 0;
        }
        private void BindLoanDataGrid()
        {
            string sortExpr;
            if (newSortExpression != "") // if there exists new sort, implement it
            {
                sortExpr = newSortExpression + " " + newSortDirection;
            }
            else if (string.IsNullOrEmpty(Request.Form["m_loanDataGrid_PreviousSort"]) == false)
            {
                sortExpr = Request.Form["m_loanDataGrid_PreviousSort"]; // otherwise take prior sort
            }
            else
            {
                // DEFAULT SORT EXPR

                sortExpr = "sStatusD DESC";
            }

            LendingQBSearchFilter searchFilter = new LendingQBSearchFilter();
            searchFilter.sLNm = m_loanNumberTF.Text.TrimWhitespaceAndBOM();
            searchFilter.sLRefNm = m_loanReferenceNumberTF.Text.TrimWhitespaceAndBOM();
            searchFilter.aBLastNm = m_lastNameTF.Text.TrimWhitespaceAndBOM();
            searchFilter.aBFirstNm = m_firstNameTF.Text.TrimWhitespaceAndBOM();
            searchFilter.SsnLastFour = ssnLastFour.Value.TrimWhitespaceAndBOM();
            searchFilter.sSpAddr = m_propertyAddressTF.Text.TrimWhitespaceAndBOM();
            try
            {
                searchFilter.sBranchId = new Guid(m_branchDDL.SelectedValue);
            }
            catch (FormatException e)
            {
                Tools.LogErrorWithCriticalTracking("Could not parse branch id from : " + m_branchDDL.SelectedValue, e);
                searchFilter.sBranchId = Guid.Empty; 
            }
            if(m_searchForLeadsRB.Checked)
                searchFilter.sStatusT = int.Parse(m_leadStatusDDL.SelectedValue);
            else
                searchFilter.sStatusT = int.Parse(m_loanStatusDDL.SelectedValue);
                

            searchFilter.sStatusDateT = int.Parse(m_createdDateDDL.SelectedValue);
            searchFilter.sLT = int.Parse(m_loanTypeDDL.SelectedValue);
            searchFilter.IsTemplate = m_searchForTempsRB.Checked;
            searchFilter.IncludeLeadAndLoanStatuses = m_searchForTempsRB.Checked; // opm 204098 include lead and loan statuses if searching for templates.
            searchFilter.IsLead = m_searchForLeadsRB.Checked;
            searchFilter.IsTest = m_searchForTestLoansRB.Checked;
            
            searchFilter.sMersMin = m_mersMinTF.Text.TrimWhitespaceAndBOM();
            searchFilter.sAgencyCaseNum = m_agencyCaseNumTF.Text.TrimWhitespaceAndBOM();
            searchFilter.sLenLNum = m_lenderCaseNumTF.Text.TrimWhitespaceAndBOM();
            searchFilter.sInvestLNum = m_investorLoanNumTF.Text.TrimWhitespaceAndBOM();
            searchFilter.sSubservicerLoanNm = m_subservicerLoanNumTF.Text.TrimWhitespaceAndBOM();

            // 06/30/06 mf - OPM 5836. For the fields that allow partial searching,
            // the user must now use an asterisk wildcard.  We only allow at the
            // end of the string.
            // 8/17/2006 nw - OPM 6424 - Partial searching will now be controlled by check boxes, 
            // and we will no longer strip out '*' and '%' from the search text
            if (searchFilter.sLNm != "")
            {
                if (searchFilter.sLNm.EndsWith("*"))
                {
                    m_loanNumberPartialMatchCB.Checked = true;
                    showPartialHint = false;
                }
                else if (m_loanNumberPartialMatchCB.Checked)
                {
                    showPartialHint = false;
                    searchFilter.sLNm += "*"; // 9/10/2010 dd - Append wild card at the end.
                }
            }

            if (searchFilter.aBLastNm != "")
            {
                if (searchFilter.aBLastNm.EndsWith("*"))
                {
                    m_lastNamePartialMatchCB.Checked = true;
                    showPartialHint = false;
                }
                else if (m_lastNamePartialMatchCB.Checked)
                {
                    showPartialHint = false;
                    searchFilter.aBLastNm += "*";
                }
            }

            if (searchFilter.aBFirstNm != "")
            {
                if (searchFilter.aBFirstNm.EndsWith("*"))
                {
                    m_firstNamePartialMatchCB.Checked = true;
                    showPartialHint = false;
                }
                else if (m_firstNamePartialMatchCB.Checked)
                {
                    showPartialHint = false;
                    searchFilter.aBFirstNm += "*";
                }
            }

            if (searchFilter.sSpAddr != "")
            {
                if (searchFilter.sSpAddr.EndsWith("*"))
                {
                    m_propertyAddressPartialMatchCB.Checked = true;
                    showPartialHint = false;
                }

                else if (m_propertyAddressPartialMatchCB.Checked)
                {
                    showPartialHint = false;
                    searchFilter.sSpAddr += "*";
                }
            }


            foreach (EmployeeRoleChooserLink cLink in m_linkList)
            {
                if (cLink.Data != "Any")
                {
                    Guid employeeId = Guid.Empty;
                    if (cLink.Data != "Uny")
                    {
                        employeeId = new Guid(cLink.Data);
                    }
                    if (cLink.RoleType.HasValue)
                    {
                        searchFilter.AddEmployeeFilter(cLink.RoleType.Value, employeeId);
                    }
                }
            }




            // 7/18/2006 nw - OPM 6369 - Disallow users from running Advanced Search with no filter
            m_bUsingAdvancedSearchFilter = searchFilter.IsEmptySearchFilter == false;
            if (!m_bUsingAdvancedSearchFilter)
            {						// 8/08 AE - OPM 23440
                ResetDataTable();	// view state now enabled for paging, must reset table on disallow
                return;
            }

            // CHECK IF NEW SEARCH PERFORMED AND CALCULATE NEW ROW NUMBERS

            if (Page.Request.Form["m_loanDataGrid_PreviousSearch"] != null && Page.Request.Form["m_loanDataGrid_PreviousSearch"] != searchFilter.ToString()) // check if new search
            {
                m_loanDataGrid.CurrentPageIndex = 0;		// if new search, reset page index to 0
            }

            int pageNumber = m_loanDataGrid.CurrentPageIndex;
            int pageSize = m_loanDataGrid.PageSize;
            int firstRow = (pageNumber) * pageSize + 1;
            int lastRow = (pageNumber + 1) * pageSize;

            //bool bDataBindSuccessful = false;
            //int nDataBindTries = 1;

            // 9/12/2006 nw - OPM 7282 - Stored Procedures will return phantom rows sometime, 
            // don't display error message right away, try calling the SP again
            ClientScript.RegisterHiddenField("m_loanDataGrid_PreviousSearch", searchFilter.ToString());
            int numberOfLoans;

            DataSet dS = LendingQBSearch.Search(CurrentUser, searchFilter, sortExpr, firstRow, lastRow, out  numberOfLoans);
            m_loanDataGrid.VirtualItemCount = numberOfLoans; // assign total entries to datagrid member   - av 1/3/11 ensure number of loans is correct still.

            m_loanDataGrid.DefaultSortExpression = sortExpr; // To utilize existing CommonDataGrid class
            // cause unneeded sort on page binding
            // set the sort to the desired expression
            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
            {
                // 10/20/2004 kb - We have need of showing lender account
                // execs in the result grid (and not loan officers) when
                // we have lenders looking at the results.  We assume that
                // all brokers with Price My Loan are lenders.
                //
                // 11/11/2004 kb - Previously, we alternated which column
                // to show depending on the broker's features.  Now, if
                // the broker is not a pml broker, then hide the lender
                // account executive column.  If pml-enabled, then show
                // both the exec and loan officer.

                m_loanDataGrid.Columns[8].Visible = false;
            }

            m_loanDataGrid.DataSource = dS.Tables[0].DefaultView;
            m_loanDataGrid.DataBind();
        }


        protected void m_exportBtn_Click(object sender, System.EventArgs e)
        {
            Export("BatchExport");
        }

        // OPM 24639 - Create batch exporter for DataTrac
        protected void m_exportToDTBtn_Click(object sender, System.EventArgs e)
        {
            Export("BatchDataTracExport");
        }

        private void Export(string method)
        {
            // 06/02/06 mf - OPM 2583.  We do not use server cache anymore.
            // We store the string list of loans in our custom cache.

            string id = Guid.NewGuid().ToString();
            string newkey = "BatchExport" + id;
            AutoExpiredTextCache.AddToCache(Request["cb"], TimeSpan.FromMinutes(5), newkey);

            this.AddInitScriptFunctionWithArgs(method, "'" + id + "'");
            onFindLoanClick(null, null);

        }

    }
}
