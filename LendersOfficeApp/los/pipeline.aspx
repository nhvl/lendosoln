<%@ Page language="c#" Codebehind="pipeline.aspx.cs"  ValidateRequest="false" AutoEventWireup="false" Inherits="LendersOfficeApp.los.pipeline" EnableSessionState="False" enableViewState="true" %>
<%@ Register TagPrefix="uc2" TagName="footer" Src="common/footer.ascx" %>
<%@ Register TagPrefix="uc2" TagName="header" Src="common/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SettingsPortlet" Src="Portlets/SettingsPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LoanTemplatePortlet" Src="Portlets/LoanTemplatePortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LoanPortlet" Src="Portlets/LoanPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="QuickSearchPortlet" Src="Portlets/QuickSearchPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ReportingPortlet" Src="Portlets/ReportingPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LeadPortlet" Src="Portlets/LeadPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UnassignLeadPortlet" Src="Portlets/UnassignLeadPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LoanPoolPortlet" Src="Portlets/LoanPoolPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TradeMasterPortlet" Src="Portlets/TradeMasterPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LoanProgramPortlet" Src="Portlets/LoanProgramPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="DiscTrackPortlet" Src="Portlets/DiscTrackPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MyLoanRemindersPortlet" Src="Portlets/MyLoanRemindersPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="RemindersPortlet" Src="Portlets/RemindersPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MyTasksPortlet" Src="Portlets/MyTasksPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UnassignLoanPortlet" Src="Portlets/UnassignLoanPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MyLeadPortlet" Src="Portlets/MyLeadPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LoanStatisticsPortlet" Src="Portlets/LoanStatisticsPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PipelinePortlet" Src="Portlets/PipelinePortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TestPortlet" Src="Portlets/TestPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PublishedReportsPortlet" Src="Portlets/PublishedReportsPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminPortlet" Src="Portlets/AdminPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="DocumentIndexingPortlet" Src="Portlets/DocumentIndexingPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PmlPortlet" Src="Portlets/PmlPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CapitalMarketsPortlet" Src="Portlets/CapitalMarketsPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="DocMagicLoginPortlet" Src="Portlets/DocMagicLoginPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="DisclosurePortlet" Src="Portlets/DisclosurePortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TestLoanPortlet" Src="Portlets/TestLoanPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="QuickPricerPortlet" Src="Portlets/QuickPricerPortlet.ascx" %>
<%@ Register TagPrefix="uc1" TagName="WorkflowPortlet" Src="~/los/Portlets/WorkflowPortlet.ascx" %>
<%@ Import Namespace="LendersOffice.Security" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html>
<html class="html-pipeline">
<head runat="server">
    <title>LendingQB</title>
    <LINK href="../css/stylesheet.css" type="text/css" rel="stylesheet">
    <style>

      .no-template-message {
          display: none;
      }

      .prevent-scroll  {
          overflow: hidden;
      }

      .hidden-but-accessible {
          width: 0;
          height: 0;
      }

      .loan-templates-dialog.no-templates .no-template-message {
          display: block;
      }

      .loan-templates-dialog {
          display: none;
      }

      div.ui-dialog .ui-dialog-content.loan-templates-dialog {
          padding: 0;
          overflow: hidden;
      }

      .loan-templates-dialog.show-templates {
          height: 500px !important;
      }

      .loan-templates-dialog .loading-container {
          margin: 0;
      }

      .loan-templates-dialog.show-templates .loading-container {
          margin-top: 45%;
      }

      .loading-container {
          text-align: center;
          height: auto !important;
          display: none;
      }

      .static-container {
          padding: .5em 1em;
      }

      .template-container {
          width: 100%;
          overflow:auto;
          height:420px;

      }

      .loading .blank-link, .loading .template-header, .loading .template-container {
          display: none;
      }

      .loading .loading-container {
          display: block;
      }

      .template-header, .blank-link {
          text-transform: capitalize;
      }

      .no-templates .template-header, .error .template-header {
          display: none;
      }

      .template-header {
          display: block;
          font-size: 13px;
          margin-top: 10px;
          margin-bottom: 5px;
      }

        div.ui-dialog-titlebar {
            background: #036;
            border-color: #036;
            text-transform: capitalize;
        }

        div.ui-widget-content {
            border: 1px solid black;
        }

        div.ui-widget-content a {
            color: blue;
            display: inline-block;
            margin-bottom: 3px;
            margin-left: 20px;
        }

        div.ui-widget-content a:hover {
            color: orange;
        }

		.PipelineLoad
		{
		    position: absolute;
		    z-index:200;
		    margin-top: -10px;
		}

		#PipelineLoadBG
		{
		    background-color: rgb(0,0,0);
		    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=75)";
		    filter: alpha(opacity=75);
		}

        .BlockMask
        {
            background-color: rgb(0,0,0);
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=75)";
		    filter: alpha(opacity=75);
            opacity: .75;
            z-index:201;
            position: absolute;
        }

		#PipelineLoadText p
		{
		    font-size: 2em;
		    font-weight: bold;
		    line-height: 300px;
		    width: 100%;
		    text-align: center;
		    vertical-align: middle;
		}
		.RefreshAlert { BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: bold; PADDING-BOTTOM: 5px; BORDER-LEFT: black 1px solid; COLOR: black; PADDING-TOP: 5px; BORDER-BOTTOM: black 1px solid; BACKGROUND-COLOR: yellow }
		.Div0 { PADDING-RIGHT: 0px; MARGIN-TOP: 2px; PADDING-LEFT: 0px; MARGIN-BOTTOM: 8px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px }
		.PL0 { PADDING-LEFT: 2px }
		.DivPopup {
		    border-right: black 3px solid; padding-right: 5px;
            border-top: black 3px solid; display: none; padding-left: 5px; padding-bottom: 5px;
            border-left: black 3px solid; width: 430px; padding-top: 5px; border-bottom: black 3px solid; color:Black;
            position: absolute; height: 50px; background-color: whitesmoke; color: Black !important;
		}

        ul#tabList li a
        {
          border-top: 3px solid #000;
          white-space: nowrap;
          position: relative;
          text-overflow: ellipsis;
          overflow: hidden;
          display: inline-block;
          top: 3px;
        }
        ul#tabList li.selected {
          border-bottom: none;
        }
        ul#tabList li.selected a {
          top: 4px !important;
        }

        ul#tabList li#addTabBtn a
		{
		    border-top: 1px solid #000;
		}
		ul#tabList li#addTabBtn a:hover
		{
		    background-color: gainsboro;
		    color: #000;
		}

        div[aria-describedby='LQBPopupDiv']
        {
            z-index:202;
        }

		</style>
	</HEAD>
	<BODY class="bgcolor1 body-pipeline" bottomMargin="0" onload="init();" scroll="yes">
		<script type="text/javascript">
      var gIntialTime = new Date();
      var gLoggingOut = false;
      var selectedTabIndex = <%=AspxTools.JsNumeric(selectedTabIndex) %>;
      var numPipelineTabs = <%= AspxTools.JsNumeric(numPipelineTabs) %>;
      var numTotalTabs = <%= AspxTools.JsNumeric(numTabs) %>;
      var ableToAddTabs = true;

      function refreshPipeline(bReallyRefresh) {
        if (bReallyRefresh) {
          var sortBy = "";
          if (null != document.getElementById("SortBy"))
             sortBy = document.getElementById("SortBy").value;
          self.location = self.location.pathname + '?page=' + selectedTabIndex +'&sortby=' + escape( sortBy );
        } else {
          <%-- dd 1/11/2006 - OPM 3382 - We no longer automatically refresh. Only explicit refresh is honor. --%>

          document.getElementById("RefreshAlertMsg").style.display="";

        }
      }

      function refreshIsNeeded()
      {
		return  document.getElementById("RefreshAlertMsg").style.display == "";
      }

        function createTask( loanId )
        {
            var queryString = '?loanId=' + loanId + '&IsTemplate=False';
            var options = 'width=750,height=400,center=yes,resizable=yes,scrollbars=yes,status=yes,help=no';
            window.open(VRoot + ML.TaskEditorUrl + queryString, '_blank', options);
        }

        function createSandbox(loanId)
        {
        return false;
        }

        function rateLock( loanId, action )
        {
            var queryString = '?loanId=' + loanId + '&action=' + action;
            var options = 'width=770,height=400,center=yes,resizable=yes,scrollbars=yes,status=yes,help=no';
            window.open(VRoot + '/los/RateLock/LockRate.aspx' + queryString, '_blank', options);
        }

      function getChecked() {
          // Return a comma delimited list of selected loans id in the main portlet.
          var ids = "";
          var collection = document.getElementsByName("loanIdCheckBox");
          if (typeof(collection) == "undefined") {
              return ids;
          }

          var length = collection.length;
          for (var i = 0; i < length; i++) {
              if (collection[i].checked == true) {
                  ids = ids + collection[i].value + ",";
              }
          }
          // This code is for if there is only one checkbox in datagrid.
          if (collection.length == null) {
              if (collection.checked == true) {
                  ids = collection.value;
              }
          }

          return ids;
      }

      function getStatus() {
          var hasLoan = $(".loan-select:checked + input:not(:checked)").length > 0;
          var hasLead = $(".loan-select:checked + input:checked").length > 0;

          if (hasLead && hasLoan)
              return "mixed";
          if (hasLead)
              return "lead";
          if (hasLoan)
              return "loan";
      }
      function getCheckList() {
        var list = new Array();

        var collection = document.getElementsByName("loanIdCheckBox");
        if (typeof(collection) == "undefined") {
          return list;
        }

        var length = collection.length;
        for (var i = 0; i < length; i++) {
          if (collection[i].checked)
            list.push(collection[i].value);
        }

        // This code is for if there only one checkbox in datagrid.
        if (collection.length == null) {
          if (collection.checked)
            list.push(collection.value);
        }

        return list;
      }
      function init()
      {
        var data =  $('#EmailConfirmation').val();
        if (data)
        {
            alert(data);
        }


        document.getElementById("PipelineLoadBG").style.display = "none";
        document.getElementById("PipelineLoadText").style.display = "none";
        var isAddNewLoanTemplate = document.getElementById("IsAddNewLoanTemplate");
        if (isAddNewLoanTemplate == null || isAddNewLoanTemplate.value != "T")
          refreshOpenWindowList();

        var errorMessage = document.getElementById("m_errorMessage");
        if( errorMessage != null && errorMessage.value != "" )
        {
			alert( errorMessage.value );
        }
        document.getElementById("tab" + selectedTabIndex).className = "selected";
      }
      function logOut() {
          gLoggingOut = true;
          var logOut = retrieveFrameProperty(parent, "frmCode", "logOut");
        if (logOut != null)
          gLoggingOut = logOut(true); // HACK: If user cancel out action, need to set gLogginOut = false
        else
          location.href = <%= AspxTools.JsString(VirtualRoot) %> + '/logout.aspx';
      }

    function viewLoan(id) {
        callFrameMethod(parent, "frmCode", "viewLoan", [id]);
    }

      function editLoan(id, name, url) {
          <%-- If this page embedded within a frame then open loan in new window --%>
          var editLoan = retrieveFrameProperty(parent, "frmCode", "editLoan");
          if (editLoan)
              editLoan(id, name, url);
          else
              window.open(ML.VirtualRoot + '/newlos/loanapp.aspx?loanid=' + id);
      }
      window.editLoan = editLoan;
		    function editLoanNewUI(id, name)
		    {
		        window.open(ML.VirtualRoot + '/LoanEditor/LoanEditor.aspx?loanid=' + id + "#/loanInfo", 'NewUI_' + removeDash(id));
		    }

		    function removeDash(value)
		    {
		        return value.replace(/-/g, "");
		    }

		    function viewStyleEditor() {
		        window.open(ML.VirtualRoot + '/newlos/Prototype/StyleEditor.aspx', name + '_NewUI');
		    }
      function editLoanOption(id, name, urlOption, winParam) {
          <%-- If this page embedded within a frame then open loan in new window --%>

          var editLoanOption = retrieveFrameProperty(parent, "frmCode", "editLoanOption");
          if (editLoanOption) {
              editLoanOption(id, name, urlOption, winParam);
          }
          else {
              window.open(ML.VirtualRoot + '/newlos/loanapp.aspx?loanid=' + id);
          }
      }

      function editLead(id, name) {
          var editLead = retrieveFrameProperty(parent, "frmCode", "editLead");
          if (editLead) {
              editLead(id, name);
          }
          else
          {
              window.open(ML.VirtualRoot + '/los/lead/leadmainframe.aspx?loanid=' + id);
          }
      }

      function exportLoan(id) {
        <% if ((BrokerUserPrincipal.CurrentPrincipal).HasPermission(Permission.CanExportLoan)) { %>
        showModal('/LegacyLink/LoanExportDlg.aspx?loanid=' + id);
        <% } else { %>
          alert(<%=AspxTools.JsString(JsMessages.NoExportPermission)%>);
        <% } %>
      }
        //Set cookie to set AssignedTo=Anybody. Once the cookie is set successfully open the task list
        function editTaskList(loanid)
        {
            var params =  {
                            'name': 'SelectedFilter',
                            'value' : 'ANYBODY'
                          };

            var strParams = JSON.stringify(params);

            callWebMethodAsync({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'PipelineService.aspx/SetCookie',
                data: strParams,
                dataType: 'json',
                async: false
            }).then(
                function(msg) {editLoan(loanid, 'Loan', 'Tasks/TaskList.aspx'); return false;},
                function(XMLHttpRequest, textStatus, errorThrown) { alert(textStatus + errorThrown); ret = false;},
                function(response) { alert("error - " + response); ret = false;}
            );

        }

		var queuedTimer = null;
		var tidInterval = null;



		function onTabClick(index)
		{
		    document.getElementById("PipelineLoadBG").style.display = "";
		    document.getElementById("PipelineLoadText").style.display = "";
		    document.getElementById("tab" + selectedTabIndex).className = "";
		    document.getElementById("tab" + index).className = "selected";
		    __doPostBack("changeTab", index);
		    selectedTabIndex = index;
		}

		function onAddBtnClick()
		{
		    if(ableToAddTabs)
		    {
		        showModal('/los/Portlets/AddTab.aspx', null, null, null, onAddBtnClickCallback, { hideCloseButton: true });
		    }
		    else
		    {
		        window.alert("Unable to add more tabs.");
		    }
		}

		function onAddBtnClickCallback(args)
		{
		    if(args.OK == true)
		    {
		        __doPostBack("addTab");
		    }
		}

		function onCloseBtnClick()
		{
		    if(numPipelineTabs == 1)
		    {
		        window.alert("Cannot close last pipeline tab");
		        return;
		    }
		    else
		    {
		        if(confirm("Do you wish to close this tab?"))
		        {
		            __doPostBack("closeTab", selectedTabIndex);
		        }
		    }

		}

		jQuery(function($) {
		    // Allows sortable via drag and drop.
		    $("#tabList").sortable({
		        items: "li:not(#addTabBtn)",
		        distance: 5,
		        helper: function(event, el) {
		            var helper = el.clone();
		            return helper;
		        },
		        start: function(event, ui) {
		            $("#previousTabIndex")[0].value = ui.item.index();
		        },
		        stop: function(event, ui) {
		            var oldPos = $("#previousTabIndex")[0].value;
		            var newPos = ui.item.index();
		            if(oldPos != newPos)
		            {
		                var positions = oldPos+","+newPos;
		                __doPostBack("reorderTab", positions);
		            }
		        }
		    });

		    // Resizes if too big
		    $(document).ready(function() {

    		        $(".loan-templates-dialog").on("click", "a", createLoanFromLink);
		            var width = 0;
		            var maxWidth = 1000;
		            $('.mainTabs').each(function() {
		                width += $(this).width();
		            });
		            if(width > maxWidth)
		            {
		                var characterSize = parseFloat($('li.mainTabs a').css('font-size'));
		                var reduceToPx = maxWidth / numTotalTabs;
		                var reduceToEm = reduceToPx / characterSize;

		                if(reduceToEm <= 4.5) // Less than 4.5 characters will show per tab
		                {
		                    ableToAddTabs = false;
		                    reduceToPx = 4.5 * characterSize;
		                }

	                    $('li.mainTabs a').each(function() {
	                        var tabLink = $(this);
	                        if(tabLink.width() > reduceToPx)
	                        {
	                            var truncReduce = reduceToPx.toFixed();
	                            tabLink.width(truncReduce);
	                            tabLink.css('top', 3);
	                        }
	                    });
	                }
		    });
		});
        </script>
		<FORM id="main" class="form-pipeline" method="post" runat="server">
			<TABLE id="Table2" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD bgcolor="black">
					    <uc2:header id="m_header" runat="server" />
					</TD>
				</TR>
				<TR vAlign="top" height="100%">
					<TD>
						<TABLE id="Table7" height="100%" cellSpacing="0" cellPadding="0" width="99%" border="0">
							<TR>
								<TD vAlign="top" width="1%">

									<uc1:quicksearchportlet id="QuickSearchPortlet1" runat="server" />
									<uc1:QuickPricerPortlet id="QuickPricerPortlet1" runat="server" />
								    <uc1:leadportlet id="LeadPortlet1" runat="server"></uc1:leadportlet>
									<uc1:loanportlet id="LoanPortlet1" runat="server"></uc1:loanportlet>
									<uc1:loantemplateportlet id="LoanTemplatePortlet1" runat="server"></uc1:loantemplateportlet>
									<uc1:adminportlet id="AdminPortlet1" runat="server"></uc1:adminportlet>
                                    <uc1:WorkflowPortlet id="WorkflowPortlet1" runat="server"></uc1:WorkflowPortlet>
                                    <uc1:DocumentIndexingPortlet id="DocIndexing" runat="server"></uc1:DocumentIndexingPortlet>
									<uc1:DocMagicLoginPortlet ID="DocMagicLoginPortlet1" runat="server" />
									<uc1:pmlportlet id="Pmlportlet1" runat="server"></uc1:pmlportlet>
									<uc1:settingsportlet id="SettingsPortlet1" runat="server"></uc1:settingsportlet>
									<uc1:reportingportlet id="ReportingPortlet1" runat="server"></uc1:reportingportlet>
									<uc1:loanprogramportlet id="LoanProgramPortlet1" runat="server"></uc1:loanprogramportlet>
									<uc1:CapitalMarketsPortlet ID="CapitalMarketsPortlet1" runat="server" />
                                    <uc1:TestLoanPortlet ID="TestLoanPortlet1" runat="server" />
								</TD>
								<TD style="PADDING-TOP: 10px" vAlign="top" width="98%" bgColor="#003366">
									<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD nowrap style="BORDER-RIGHT: 2px outset; PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset; PADDING-TOP: 0px;" valign="top" bgcolor="gainsboro">
											    <asp:HiddenField ID="previousTabIndex" runat="server" />
											    <asp:Panel ID="tabs" class="Tabs" runat="server">
											    </asp:Panel>
											    </TD>
										</TR>
										<TR>
											<TD style="BORDER-RIGHT: 2px outset; PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset; PADDING-TOP: 10px; BORDER-BOTTOM: 2px outset" valign="top" bgcolor="gainsboro" height="100%">
											    <div class="loading-pipeline">
											        <div class=PipelineLoad id=PipelineLoadBG>
                                                    </div>
							                        <div class=PipelineLoad id=PipelineLoadText>
							                            <p>Loading Pipeline...</p>
							                        </div>
							                        <div id="RefreshAlertMsg" class="RefreshAlert" style="DISPLAY:none;">The listing
									                    below may not reflect the latest changes.&nbsp; Click the "Refresh" button to
									                    update.
								                    </div>
											        <uc1:pipelineportlet id="m_pipelinePortlet" Visible="False" runat="server"></uc1:pipelineportlet>
											        <uc1:disclosureportlet id="m_disclosurePortlet" Visible="False" runat="server"></uc1:disclosureportlet>
											        <uc1:myleadportlet id="m_myLeadPortlet" Visible="False" runat="server"></uc1:myleadportlet>
											        <uc1:loanpoolportlet id="m_loanPoolPortlet" Visible="False" runat="server"></uc1:loanpoolportlet>
											        <uc1:trademasterportlet id="m_tradeMasterPortlet" Visible="False" runat="server"></uc1:trademasterportlet>
											        <uc1:unassignloanportlet id="m_unassignLoanPortlet" Visible="False" runat="server"></uc1:unassignloanportlet>
											        <uc1:unassignleadportlet id="m_unassignLeadPortlet" Visible="False" runat="server"></uc1:unassignleadportlet>
											        <uc1:remindersportlet id="m_remindersPortlet" Visible="False" runat="server"></uc1:remindersportlet>
											        <uc1:myloanremindersportlet id="m_myLoanRemindersPortlet" Visible="False" runat="server"></uc1:myloanremindersportlet>
											        <uc1:disctrackportlet id="m_discTrackPortlet" Visible="False" runat="server"></uc1:disctrackportlet>
											        <uc1:MyTasksPortlet id="m_MyTasksPortlet" Visible="False" runat="server"></uc1:MyTasksPortlet>
                                                    <uc1:TestPortlet id="m_testPortlet" Visible="False" runat="server"></uc1:TestPortlet>
											    </div>

											</TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD vAlign="middle" bgColor="black" height="1%">
						<uc2:footer id="Footer1" runat="server" />
					</TD>
				</TR>
			</TABLE>

            <div class="loan-templates-dialog">
                <div class="static-container">
                    <div class="no-template-message">
                        <span class="bold">No loan templates available.</span>
                        <span>You can create a new loan template or ask an administrator to enable our blank loan option.</span>
                    </div>
                    <div class="loading-container">
                        <img src="../images/loading.gif" />
                        <p>Please Wait...</p>
                    </div>
                    <a href="" class="blank-link"></a>
                    <span class="template-header"></span>
                </div>
                <div class="template-container"></div>
            </div>
		</FORM>
	</BODY>
</HTML>
