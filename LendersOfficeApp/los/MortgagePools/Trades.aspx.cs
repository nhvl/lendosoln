﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.CapitalMarkets;
using System.Web.Services;
using LendersOffice.Security;
using LendersOffice.ObjLib.MortgagePool;

namespace LendersOfficeApp.los.MortgagePools
{
    public partial class Trades : BaseMortgagePoolPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var trades = CurrentPool.GetTrades();
            ClientScript.RegisterHiddenField("PoolId", PoolId.ToString());
            TradeListing.DataSource = trades;
            TradeListing.DataBind();

            var tradeStats = new TradeStatisticsDTO(trades);

            DisableAllControls();
            BindStatistics(tradeStats);

        }

        private void BindStatistics(TradeStatisticsDTO t)
        {
            Type.Items.Add(t.Type.ToString());
            Direction.Items.Add(t.Direction.ToString());
            DealerInvestor.Value = t.DealerInvestor;

            Month.Text = t.Month;
            Security.Items.Add(t.Description);

            Coupon.Value = t.Coupon;
            LatestTradeDate.Text = t.LatestTradeDate;
            EarliestNotificationDate.Text = t.EarliestNotificationDate;
            EarliestSettlementDate.Text = t.EarliestSettlementDate;

            OpenAmt.Value = t.Amount;
            AvgPrice.Value = t.Price;
            TotalExpense.Value = t.TotalExpense;
            TotalGainLoss.Value = t.TotalGainLoss;


        }

        private void DisableAllControls()
        {
            //Why you gotta be this way, Asp.Net? Why can't you just pick one of .disabled or .enabled and use it everywhere?
            DealerInvestor.Disabled = true;
            
            Security.Enabled = false;
            Direction.Enabled = false;
            Type.Enabled = false;
            Month.Enabled = false;
            Coupon.Disabled = true;
            AvgPrice.Disabled = true;
            
            OpenAmt.Disabled = true;
            LatestTradeDate.Enabled = false;
            EarliestNotificationDate.Enabled = false;
            EarliestSettlementDate.Enabled = false;
            
            TotalGainLoss.Disabled = true;
            TotalExpense.Disabled = true;
        }

        [WebMethod]
        public static void RemoveTrade(Guid tradeId)
        {
            var trade = Trade.RetrieveById(tradeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            trade.AssociatedPoolId = null;
            trade.Save();
        }

        [WebMethod]
        public static void AddTrade(long poolId, Guid tradeId)
        {
            var trade = Trade.RetrieveById(tradeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            //Loading the pool makes sure that this user has access to that particular pool.
            var pool = new MortgagePool(poolId);

            trade.AssociatedPoolId = poolId;
            trade.Save();
        }
    }
}
