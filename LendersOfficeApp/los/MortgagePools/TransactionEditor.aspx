﻿<%@ Page Title="" Language="C#" MasterPageFile="~/los/MortgagePools/MasterPages/MortgagePoolPopup.Master" AutoEventWireup="true" CodeBehind="TransactionEditor.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.TransactionEditor" %>
<%@ Import Namespace="LendersOffice.ObjLib.CapitalMarkets" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ID="HtmlHead" ContentPlaceHolderID="HtmlHead" runat="server">
<script type="text/javascript" src="../../inc/calendar.js"></script>
<script type="text/javascript">

    jQuery(function($) {
        window.resizeTo($('#MasterPageMain').width() + 25, $('#MasterPageMain').height() + 70);

        var trade = JSON.parse(window.opener.GetTradeJSON());
        LQBMortgagePools.ApplyObject($('#TradeNum, #TradeInfo'), trade,
            function(name, $context) {
                return $('.trade_' + name, $context);
            }
        );

        var transaction = JSON.parse(window.opener.GetTransactionJSON());
        var baseTotal = fromRep(transaction["new-total"]) - fromRep(transaction["amount"]);

        var $gainLossAmt = $('#gain-loss input[type=text]');
        var $transactionAmt = $('#amount input[type=text]');

        var negateAmount = transaction["type"] != "Opening";
        if (negateAmount) {
            transaction["amount"] = -fromRep(transaction["amount"]);
            $transactionAmt.addClass('negated');
        }

        LQBMortgagePools.ApplyObject($('#TransactionInfo'), transaction,
            function(name, $context) {
                if (name == 'type-name') return $('#Type');
                var ret = $('#' + name + ' input', $context).not('input[type=checkbox]');
                if (ret.length == 0) ret = $('#' + name, $context);

                return ret;
            }
        );

        $('input[preset=money]').each(function() {
            //format_money is from mask.js
            format_money(this);
        });

        $('#amount input').on('change', function() {
            var neg = negateAmount ? -1 : 1;
            if (!isClosing) {
                format_money($('#new-total input').val(baseTotal + neg * fromRep($(this).val()))[0]);
            }
            else {
                format_money($('#new-total input').val('0.0')[0]);
            }
        });

        $('#linked-trade').on('change', function(e) {
            var value = $(this).val();
            var id = '';
            var name = '';
            var price;
            if (value != "none") {
                var parts = value.split(':');
                id = parts[0];
                name = 'Trade # ' + parts[1];
                price = parts[2];

                $('#price input').val(price).attr('disabled', 'disabled');
            }
            else {
                $('#price input').removeAttr('disabled');
            }

            $('#linked-name').val(name);
            $('#linked-id').val(id);
        });

        if (transaction["type-name"] == "Pair off") {
            setupTradeLinking(trade, transaction);
        }

        var isClosing = false;
        var originalTransactionAmt = trade["PreClosingAmount"];
        if (transaction["type-name"] == "Closing") {
            isClosing = true;
            setupClosingTransaction(trade, transaction);
        }

        var $transactionPrice = $('#price input[type=text]');
        var $gainLossLocked = $('#gain-loss-calculated');

        var tradePrice = fromRep($('.trade_Price').val());
        var tradeDirection = $('.trade_Direction').text();

        var gainLossOriginalVal = transaction["gain-loss-calculated-val"];
        $('#gain-loss-calculated').on('change', function() {
            var $this = $(this);
            var $textbox = $this.next('input[type=text]');
            if ($this.is(':checked')) {
                $textbox.val(gainLossOriginalVal).attr('disabled', 'disabled');
                updateGainLossAmount(fromRep($transactionAmt), fromRep($transactionPrice), tradeDirection, tradePrice);
            }
            else {
                $textbox.removeAttr('disabled');
            }
        }).trigger('change');

        $([$transactionAmt, $transactionPrice])
        .map(function() { return this.toArray(); })
        .on('change', function() {
            updateGainLossAmount(fromRep($transactionAmt), fromRep($transactionPrice), tradeDirection, tradePrice);
        });

        $('#OK').on('click', function() {
            var trans = LQBMortgagePools.ToObject($('#TransactionInfo'), ":input", function($e) {
                //This will pick the element's id if it has one, and then take the last thing after an underscore if any
                //(in order to handle ASP.net mangling client ids)
                //Otherwise it uses the name of the element's closest label.
                //We also don't want to include mirror elements.
                if ($e.is('.Mirror')) return false;

                var name = $e.attr('id');
                if (name) {
                    var name_parts = name.split('_');
                    return name_parts[name_parts.length - 1].replace('TransactionDate', 'date');
                }
                return $e.closest('label').attr('id');
            }, function($this, defaultSelector) {
                if ($this.is('.negated')) {
                    var value = defaultSelector($this);
                    //This is the easiest way to get the properly formatted value.
                    value = -fromRep(value);
                    $this.val(value);
                    format_money($this[0]);
                    return $this.val();
                }
                return defaultSelector($this);
            });

            var changes = JSON.stringify(trans);
            window.opener.ApplyTransactionJSON(changes);
            onClosePopup();
        });

        $('#Cancel').on('click', function() {
            onClosePopup();
        });

        function setupClosingTransaction(tradeDTO, transactionDTO) {
            $('#amount').addClass('closing');
            $('#amount input').trigger('change');
            $('#transaction-amt-calculated').on('change', function() {
                var $this = $(this);
                var $textbox = $this.next('input[type=text]');
                if ($this.is(':checked')) {
                    format_money($textbox.val(-fromRep(originalTransactionAmt)).attr('disabled', 'disabled')[0]);
                }
                else {
                    $textbox.removeAttr('disabled');
                }
            }).trigger('change');
        }

        function setupTradeLinking(tradeDTO, transactionDTO) {
            $('#link').show();
            $('#linked-trade').hide();

            $.each(tradeDTO, function(key, val) {
                if (val.txt) return tradeDTO[key] = val.txt;
                tradeDTO[key] = val;
            });

            var DTO = {
                linkTo: tradeDTO
            };

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'TransactionEditor.aspx/GetLinkableTrades',
                data: JSON.stringify(DTO),
                dataType: 'json',

                success: function(msg) {
                    var associatedId = (transactionDTO['linked-id'] || '').toLowerCase();
                    
                    var h = hypescriptDom;
                    $('#linked-trade').empty().append("<option value='none'>--- none ---</option>")
                        .append($.map(msg.d || [], function(val, key){
                            var id = val.TradeId.toLowerCase();
                            return h("option", {value:([id,val.TradeNum,val.Price].join(":")), selected:id == associatedId},
                                ["Trade #", val.TradeNum, "-", 
                                val.Amount, val.Direction_rep, val.Type_rep,
                                val.Description, val.Coupon, val.Month, "@", val.Price].join(' '));
                        }));
                    $('#linked-trade').show();
                    $('#linked-trade').trigger('change');
                },

                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("An unexpected error happened, please try again later");
                }
            });
        }

        swapCheckboxPolarity($('#gain-loss-calculated, #transaction-amt-calculated'));

        //This is necessary because we want to maintain positive "is something calculated" values,
        //but we want the box to be checked when the value is false and unchecked when the value is true.
        function swapCheckboxPolarity($cb) {
            $cb.each(function() {
                var $source = $(this);
                $('<input type="checkbox">').insertBefore($source)
                .prop('checked', !$source.is(':checked'))
                .on('change', function() {
                    $source.prop('checked', !$(this).is(':checked')).trigger('change');
                });
            }).hide();
        }

        function fromRep(input) {
            if (!input) return 0;
            if (input instanceof jQuery) input = input.val();

            var neg = 1;
            if (input.indexOf('(') != -1 || input.indexOf('-') != -1) neg = -1

            return +input.replace(/[^0-9.]/g, '') * neg;
        }

        function updateGainLossAmount(transactionAmount, transactionPrice, tradeDirection, tradePrice) {
            if (!$gainLossLocked.is(':checked')) return;

            var priceDiff;
            switch (tradeDirection) {
                case 'Sell':
                    priceDiff = transactionPrice - tradePrice;
                    break;
                case 'Buy':
                    priceDiff = tradePrice - transactionPrice;
                    break;
            }
            var neg = negateAmount ? -1 : 1;
            format_money($gainLossAmt.val((priceDiff / 100) * transactionAmount * neg)[0]);
        }

        $('.mask').css({ "text-align": "left" });
    });
</script>
<style type="text/css" >
img
{
    vertical-align: bottom;
}

label
{
    display: block;
    height: 25px;
}

label span
{
    display: inline-block;
    width: 100px;
}

#gain-loss span
{
    width: 80px;
}

#TransactionInfo
{
    margin-top: 10px;
}

#MasterPageMain
{
    width: 580px;
    height: 350px;
}

#price input
{
    width: 70px;
}

#memo input, #linked-trade
{
    width: 410px;
}

#bottom-controls
{
    margin-left: 425px;
}

#link
{
    display: none;
}

#amount input[type=checkbox]
{
    display: none;
}

#amount.closing input[type=checkbox]
{
    display: inline;
}

#amount.closing span
{
    width: 80px;
}
.w-90 { width: 90px; }
</style>
</asp:Content>
<asp:Content ID="Header" ContentPlaceHolderID="Header" runat="server">
<span id="Type"></span>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
<div id="TradeNum">Trade # <span class="trade_TradeNum"></span></div>
<div id="TradeInfo">
    <span class="trade_Direction"></span>
    <span class="trade_Type"></span>
    <span class="trade_Description"></span>
    <span class="trade_Coupon"></span>
    <span class="trade_Month"></span>
    <input type="hidden" class="trade_Price" />
</div>
<div id="TransactionInfo">
    <div id="link">
        <label><span>Linked trade</span> <select id="linked-trade"></select> <input type="hidden" value="" id="linked-name" /> <input type="hidden" value="" id="linked-id" /></label>
    </div>
    <div class="column left">
        <label id="date"><span>Date</span> <ml:datetextbox ID="TransactionDate" runat="server" width="75" preset="date" CssClass="mask" /></label>
        <label id="amount"><span>Transaction Amt</span> <input type="checkbox" id="transaction-amt-calculated" /><input type="text" class="mask" preset="money" /></label>
        <label id="new-total"><span>New Total</span> <input type="text" disabled /></label>
        <label id="price"><span>Price</span> <input type="text" preset="price" /></label>
    </div>
    
    <div class="column right">
        <label id="gain-loss"><span>Gain/Loss</span> <input type="checkbox" id="gain-loss-calculated" /><input type="text" disabled class="mask w-90" preset="money" /></label>
        <label id="expense"><span>Expense</span> <input type="text" class="mask w-90" preset="money" /></label>
    </div>
    
    <div class="clear">
        <label id="memo"><span>Memo</span> <input type="text" /></label>
    </div>
    
    <div id="bottom-controls">
        <input type="button" value="OK" id="OK" /> <input type="button" value="Cancel" id="Cancel" />
    </div>
</div>
</asp:Content>
