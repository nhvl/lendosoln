﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using LendersOffice.Common;
using DataAccess;
using System.Data.SqlClient;
using System.Collections;
using LendersOffice.Security;
using LendersOffice.ObjLib.MortgagePool;

namespace LendersOfficeApp.los.MortgagePools
{
    public partial class PoolDetails : BaseMortgagePoolPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (RequestHelper.GetBool("SaveError"))
            {
                ClientScript.RegisterHiddenField("SaveError", "true");
            }
            if (RequestHelper.GetBool("PoolNumberError"))
            {
                ClientScript.RegisterHiddenField("PoolNumberError", RequestHelper.GetSafeQueryString("PoolNumber"));
            }
            if (Page.IsPostBack) return;
            Tools.Bind_AmortizationT(AmortizationT);
            AmortizationT.SelectedValue = ((int)CurrentPool.AmortizationT).ToString();

            Tools.Bind_Generic(AgencyT, CurrentPool.AgencyT);
            AgencyT.Items.RemoveAt(0);
            Tools.Bind_Generic(AmortizationMethodT, CurrentPool.AmortizationMethodT);
            Tools.Bind_Generic(IndexT, CurrentPool.IndexT);
            Tools.Bind_Generic(GovernmentBondFinancingProgramType, CurrentPool.GovernmentBondFinancingProgramType);

            OpenedD.Text = ToDateTimeRep(CurrentPool.OpenedD);
            DeliveredD.Text = ToDateTimeRep(CurrentPool.DeliveredD);
            ClosedD.Text = ToDateTimeRep(CurrentPool.ClosedD);
            
            IssueD.Text = ToDateTimeRep(CurrentPool.IssueD);
            SettlementD.Text = ToDateTimeRep(CurrentPool.SettlementD);
            
            InitialPmtD.Text = ToDateTimeRep(CurrentPool.InitialPmtD);
            UnpaidBalanceD.Text = ToDateTimeRep(CurrentPool.UnpaidBalanceD);
            MaturityD.Text = ToDateTimeRep(CurrentPool.MaturityD);

            InternalId.Value = CurrentPool.InternalId;

            BasePoolNumber.Value = CurrentPool.BasePoolNumber.ToString();
            PoolPrefix.Value = CurrentPool.PoolPrefix;

            IssueTypeCode.Value = CurrentPool.IssueTypeCode.ToString();
            PoolTypeCode.Value = CurrentPool.PoolTypeCode.ToString();
            
            IssuerID.Value = CurrentPool.IssuerID.ToString();
            CustodianID.Value = CurrentPool.CustodianID.ToString();
            TaxID.Value = CurrentPool.TaxID.ToString();

            PIAccountNum.Value = CurrentPool.PIAccountNum.Value;
            PIRoutingNum.Value = CurrentPool.PIRoutingNum.Value;

            TIAccountNum.Value = CurrentPool.TIAccountNum.Value;
            TIRoutingNum.Value = CurrentPool.TIRoutingNum.Value;

            SettlementAccountNumber1.Value = CurrentPool.SettlementAccounts[0].AccountNumber.Value;
            SettlementRoutingNumber1.Value = CurrentPool.SettlementAccounts[0].RoutingNumber.Value;
            SettlementABAName1.Value = CurrentPool.SettlementAccounts[0].ABAName;
            SettlementThirdPartyAccountName1.Value = CurrentPool.SettlementAccounts[0].ThirdPartyAccountName;
            SecurityOriginalSubscriptionAmount1.Value = ToMoneyRep(CurrentPool.SettlementAccounts[0].SecurityInvestorOriginalSubscriptionAmount);

            SettlementAccountNumber2.Value = CurrentPool.SettlementAccounts[1].AccountNumber.Value;
            SettlementRoutingNumber2.Value = CurrentPool.SettlementAccounts[1].RoutingNumber.Value;
            SettlementABAName2.Value = CurrentPool.SettlementAccounts[1].ABAName;
            SettlementThirdPartyAccountName2.Value = CurrentPool.SettlementAccounts[1].ThirdPartyAccountName;
            SecurityOriginalSubscriptionAmount2.Value = ToMoneyRep(CurrentPool.SettlementAccounts[1].SecurityInvestorOriginalSubscriptionAmount);

            SecurityR.Text = ToRateRep(CurrentPool.SecurityR);
            Term.Value = CurrentPool.Term.ToString();
            
            TotalCurrentBalance.Text = ToMoneyRep(CurrentPool.TotalCurrentBalance);
            LowR.Text = ToRateRep(CurrentPool.LowR);
            HighR.Text = ToRateRep(CurrentPool.HighR);
            LoanCount.Value = CurrentPool.LoanCount.ToString();
            
            SecurityRMargin.Text = CurrentPool.SecurityRMargin.ToString();
            SecurityChangeD.Text = ToDateTimeRep(CurrentPool.SecurityChangeD);
            
            CertAndAgreementT.SelectedIndex = (int)CurrentPool.CertAndAgreementT;
            IsFormHUD11711ASentToDocumentCustodian.Checked = CurrentPool.IsFormHUD11711ASentToDocumentCustodian;

            //Fannie specific fields
            BookEntryD.Text = ToDateTimeRep(CurrentPool.BookEntryD);
            PoolSuffix.Value = CurrentPool.PoolSuffix;
            Tools.Bind_sLT(MortgageT);
            MortgageT.Items.RemoveAt(MortgageT.Items.Count - 1); //The last option is Other, remove it.
            MortgageT.SelectedValue = CurrentPool.MortgageT.ToString("D");

            SellerID.Value = CurrentPool.SellerID;
            ServicerID.Value = CurrentPool.ServicerID;
            DocumentCustodianID.Value = CurrentPool.DocumentCustodianID;

            RemittanceDay.Value = CurrentPool.RemittanceD.ToString();
            Tools.Bind_Generic(StructureT, CurrentPool.StructureT);

            FeatureJSON.Value = CurrentPool.FeatureCodesJSON;
            OwnershipPc.Value = ToRateRep(CurrentPool.OwnershipPc);

            IsInterestOnly.Checked = CurrentPool.IsInterestOnly;
            IsBalloon.Checked = CurrentPool.IsBalloon;
            IsAssumable.Checked = CurrentPool.IsAssumable;

            ARMPlanNum.Value = CurrentPool.ARMPlanNum;
            Tools.Bind_PoolRoundingType(RoundingT, CurrentPool.RoundingT);
            InterestRateChangeLookbackDays.Value = CurrentPool.InterestRateChangeLookbackDays.ToString();
            MBSMarginPc.Text = ToRateRep(CurrentPool.MBSMarginPc);
            Tools.Bind_Generic(AccrualRateStructureT, CurrentPool.AccrualRateStructureT);
            FixedServicingFeePc.Text = ToRateRep(CurrentPool.FixedServicingFeePc);
            MinAccrualR.Text = ToRateRep(CurrentPool.MinAccrualR);
            MaxAccrualR.Text = ToRateRep(CurrentPool.MaxAccrualR);

            GovernmentBondFinanceCheckbox.Checked = CurrentPool.GovernmentBondFinance;
            GovernmentBondFinanceAdditionalOptions.Style.Add("display", CurrentPool.GovernmentBondFinance ? "" : "none");
            GovernmentBondFinancingProgramName.Value = CurrentPool.GovernmentBondFinancingProgramName;

            ClientScript.RegisterHiddenField("OriginalPoolId", CurrentPool.InternalId);
        }

        private string ToDateTimeRep(CDateTime dt)
        {
            if (dt == null) return string.Empty;
            return converter.ToDateTimeString(dt);
        }

        LosConvert converter = new LosConvert();
        private string ToRateRep(decimal? rate)
        {
            if (!rate.HasValue) return "";
            return converter.ToRateString(rate.Value);
        }

        private string ToMoneyRep(decimal money)
        {
            return converter.ToMoneyString(money, FormatDirection.ToRep);
        }

        private CDateTime FromDateTimeRep(string from)
        {
            return CDateTime.Create(from, null);
        }

        private decimal FromRateRep(string from)
        {
            return converter.ToRate(from);
        }

        private decimal FromMoneyRep(string from)
        {
            return converter.ToMoney(from);
        }

        protected void SaveClicked(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                return;
            }

            CurrentPool.OpenedD = FromDateTimeRep(OpenedD.Text);
            CurrentPool.DeliveredD = FromDateTimeRep(DeliveredD.Text);
            CurrentPool.ClosedD = FromDateTimeRep(ClosedD.Text);
            CurrentPool.IssueD = FromDateTimeRep(IssueD.Text);
            CurrentPool.SettlementD = FromDateTimeRep(SettlementD.Text);
            CurrentPool.InitialPmtD = FromDateTimeRep(InitialPmtD.Text);
            CurrentPool.UnpaidBalanceD = FromDateTimeRep(UnpaidBalanceD.Text);
            CurrentPool.MaturityD = FromDateTimeRep(MaturityD.Text);
            CurrentPool.InternalId = InternalId.Value;
            CurrentPool.AgencyT = (E_MortgagePoolAgencyT)GetValue(AgencyT);
            CurrentPool.BasePoolNumber = BasePoolNumber.Value.ToUpper();
            CurrentPool.IssueTypeCode = IssueTypeCode.Value.ToUpper();
            CurrentPool.PoolTypeCode = PoolTypeCode.Value.ToUpper();
            CurrentPool.IssuerID = IssuerID.Value;
            CurrentPool.CustodianID = CustodianID.Value;
            CurrentPool.TaxID = TaxID.Value;
            CurrentPool.PIAccountNum = PIAccountNum.Value;
            CurrentPool.PIRoutingNum = PIRoutingNum.Value;

            CurrentPool.TIAccountNum = TIAccountNum.Value;
            CurrentPool.TIRoutingNum = TIRoutingNum.Value;
            
            if (CurrentPool.SettlementAccounts.Count >= 1)
            {
                CurrentPool.SettlementAccounts[0].AccountNumber = SettlementAccountNumber1.Value;
                CurrentPool.SettlementAccounts[0].RoutingNumber = SettlementRoutingNumber1.Value;
                CurrentPool.SettlementAccounts[0].ABAName = SettlementABAName1.Value;
                CurrentPool.SettlementAccounts[0].ThirdPartyAccountName = SettlementThirdPartyAccountName1.Value;
                CurrentPool.SettlementAccounts[0].SecurityInvestorOriginalSubscriptionAmount = FromMoneyRep(SecurityOriginalSubscriptionAmount1.Value);
            }
            else
            {
                CurrentPool.SettlementAccounts.Add(new MortgagePool.SettlementAccount(CurrentPool, 1,
                    SettlementAccountNumber1.Value,
                    SettlementRoutingNumber1.Value,
                    SettlementABAName1.Value,
                    SettlementThirdPartyAccountName1.Value,
                    FromMoneyRep(SecurityOriginalSubscriptionAmount1.Value)));
            }

            if (CurrentPool.SettlementAccounts.Count >= 2)
            {
                CurrentPool.SettlementAccounts[1].AccountNumber = SettlementAccountNumber2.Value;
                CurrentPool.SettlementAccounts[1].RoutingNumber = SettlementRoutingNumber2.Value;
                CurrentPool.SettlementAccounts[1].ABAName = SettlementABAName2.Value;
                CurrentPool.SettlementAccounts[1].ThirdPartyAccountName = SettlementThirdPartyAccountName2.Value;
                CurrentPool.SettlementAccounts[1].SecurityInvestorOriginalSubscriptionAmount = FromMoneyRep(SecurityOriginalSubscriptionAmount2.Value);
            }
            else
            {                
                CurrentPool.SettlementAccounts.Add(new MortgagePool.SettlementAccount(CurrentPool, 2,
                    SettlementAccountNumber2.Value,
                    SettlementRoutingNumber2.Value,
                    SettlementABAName2.Value,
                    SettlementThirdPartyAccountName2.Value,
                    FromMoneyRep(SecurityOriginalSubscriptionAmount2.Value)));
            }

            CurrentPool.SecurityR = FromRateRep(SecurityR.Text);
            int term;
            if(int.TryParse(Term.Value, out term)) CurrentPool.Term = term;

            CurrentPool.AmortizationMethodT = (E_MortgagePoolAmortMethT)GetValue(AmortizationMethodT);
            CurrentPool.AmortizationT = (E_sFinMethT)GetValue(AmortizationT);
            CurrentPool.SecurityRMargin = FromRateRep(SecurityRMargin.Text);
            CurrentPool.SecurityChangeD = FromDateTimeRep(SecurityChangeD.Text);
            CurrentPool.IndexT = (E_MortgagePoolIndexT)GetValue(IndexT);
            CurrentPool.CertAndAgreementT = (E_MortgagePoolCertAndAgreementT)CertAndAgreementT.SelectedIndex;
            CurrentPool.IsFormHUD11711ASentToDocumentCustodian = IsFormHUD11711ASentToDocumentCustodian.Checked;

            CurrentPool.BookEntryD = FromDateTimeRep(BookEntryD.Text);
            CurrentPool.PoolPrefix = PoolPrefix.Value;
            CurrentPool.PoolSuffix = PoolSuffix.Value;
            CurrentPool.MortgageT = (E_sLT)GetValue(MortgageT);
            CurrentPool.SellerID = SellerID.Value;
            CurrentPool.ServicerID = ServicerID.Value;
            CurrentPool.DocumentCustodianID = DocumentCustodianID.Value;

            CurrentPool.RemittanceD_rep = RemittanceDay.Value;

            CurrentPool.StructureT = (E_PoolStructureT)GetValue(StructureT);
            CurrentPool.FeatureCodesJSON = FeatureJSON.Value;
            CurrentPool.OwnershipPc_rep = OwnershipPc.Value;
            CurrentPool.IsInterestOnly = IsInterestOnly.Checked;
            CurrentPool.IsBalloon = IsBalloon.Checked;
            CurrentPool.IsAssumable = IsAssumable.Checked;
            CurrentPool.ARMPlanNum = ARMPlanNum.Value;
            CurrentPool.RoundingT = (E_PoolRoundingT)GetValue(RoundingT);

            CurrentPool.InterestRateChangeLookbackDays_rep = InterestRateChangeLookbackDays.Value;
            CurrentPool.MBSMarginPc = FromRateRep(MBSMarginPc.Text);
            CurrentPool.AccrualRateStructureT = (E_PoolAccrualRateStructT)GetValue(AccrualRateStructureT);
            CurrentPool.FixedServicingFeePc = FromRateRep(FixedServicingFeePc.Text);
            CurrentPool.MinAccrualR = FromRateRep(MinAccrualR.Text);
            CurrentPool.MaxAccrualR = FromRateRep(MaxAccrualR.Text);

            CurrentPool.GovernmentBondFinance = GovernmentBondFinanceCheckbox.Checked;
            CurrentPool.GovernmentBondFinancingProgramName = GovernmentBondFinancingProgramName.Value;
            CurrentPool.GovernmentBondFinancingProgramType = (E_GovernmentBondFinancingProgramType)GetValue(GovernmentBondFinancingProgramType);


            string poolNumberError = "";
            // If the resulting pool number is invalid, save the other fields and reset the pool number fields
            if (!MortgagePool.ValidPoolNumber(CurrentPool))
            {
                poolNumberError = "&PoolNumberError=t&PoolNumber=" + CurrentPool.PoolNumberByAgency;
                CurrentPool.BasePoolNumber = "";
                CurrentPool.PoolTypeCode = "";
                if (CurrentPool.AgencyT == E_MortgagePoolAgencyT.GinnieMae) CurrentPool.IssueTypeCode = "";
            }

            var saveError = CurrentPool.SaveData() ? "" : "&SaveError=t";

            Response.Redirect(Request.Url.ToString() + saveError + poolNumberError);
        }

        private int GetValue(DropDownList ddl)
        {
            var selected = ddl.SelectedValue;
            int parsing = 0;
            int.TryParse(selected, out parsing);
            return parsing;
        }

        [WebMethod]
        public static bool IsPoolIdUnique(string PoolId)
        {
            return MortgagePool.FindMortgagePoolsByInternalID(PoolId, false).Rows.Count == 0;
        }
    }
}
