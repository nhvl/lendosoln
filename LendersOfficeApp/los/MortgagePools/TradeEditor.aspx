﻿<%@ Page Title="" Language="C#" MasterPageFile="~/los/MortgagePools/MasterPages/StandAloneEditor.master" AutoEventWireup="true" CodeBehind="TradeEditor.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.TradeEditor" %>
<%@ Import Namespace="LendersOffice.ObjLib.CapitalMarkets" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript" src="../../inc/jquery.tablesorter.min.js"></script>
<link rel="Stylesheet" type="text/css" href="../../css/MortgagePoolTable.css" />

<style type="text/css">

#top-controls
{
    margin-bottom: 10px;
}

#right label span
{
    width: 130px;
}

#left select
{
    width: 130px;
}

#left
{
    width: 350px;
}

#Type select.direction
{
    width: 63px;
}

#Type select.type
{
 width: 64px;
}

#top-trade-controls
{
    margin-bottom: 20px;
}
</style>

<script type="text/javascript">
    jQuery(function($) {

        var errorMessage = null;

        $('select.autocomplete').each(function() {
            var $this = $(this);
            $this.hide();

            var options = [];
            $this.find('option').each(function() {
                options.push($(this).val());
            });

            if (options.length > 0) {
                $this.siblings('.autocomplete-value').autocomplete({
                    source: options,
                    minLength: 0
                }).on('click', function() {
                    $(this).autocomplete("search", "");
                });
            }
        });

        $('.TradeNum').on('blur', function() {
            VerifyTradeNumber();
        });

        $('#transaction-data table').on('click', 'td.edit', function() {
            $('#transaction-data table tbody tr').removeClass('selected');
            $(this).closest('tr').addClass('selected');

            editTransaction();
        });

        $('#transaction-data table').on('click', 'td.delete', function() {
            if (hasDisabledAttr($(this))) return;

            $selectedTransaction = $(this).closest('tr');

            var linkedId = $selectedTransaction.find('.linked-id').val();

            if (linkedId && linkedId != '') {
                if (!confirm("This action will also delete the corresponding transaction in the linked security. Proceed?")) return;
            }

            $selectedTransaction.find('.deleted').val('true');
            $selectedTransaction.hide();

            $('#transaction-data table').trigger("UpdateTableSorter");
            updateTransactionState();
        });

        $('#transaction-data table').on('click', 'td.linked-name', function() {
            var linkedTradeId = $(this).closest('tr').find('.linked-id').val();
            if (!linkedTradeId) return;

            var win = window.open("TradeEditor.aspx?TradeId=" + linkedTradeId, "LinkedTrade", "resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes");
            win.focus();
        });

        $('#Associate').on('click', function() {
            var win = window.open("PoolPicker.aspx", "PoolPicker", "resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes");
            win.focus();
            //PoolPicker.aspx calls TradeAssociated when it's done.
        });

        $('#Remove').on('click', function() {
            $('#PreAssociateInfo').html('This trade will be removed from the current pool ' +
                                    'when you save <span class="Action cancel-assign">cancel</span>');
            $('.ToAssociateId').val('remove');
        });

        if ($('#AssociatedWithPool').length > 0) {
            $('#Associate').hide();
            $('#Remove').show();
        }
        else {
            $('#Associate').show();
            $('#Remove').hide();
        }


        $('#top-trade-controls').on('click', '.cancel-assign', function() {
            $('#PreAssociateInfo').empty();
            $('.ToAssociateId').val('');
        });

        $('.new-transaction').on('click', function() {
            var $this = $(this);
            if ($this.is(':disabled')) return;

            var type = $this.attr('class').split(' ')[1];
            newTransaction(type);
        });

        var typeClassMap = {
            "PairOff": "pair-off",
            "Opening": "opening",
            "Closing": "closing"
        };

        function normalizeTransactionDTO(t) {
            if ($.isArray(t)) return $.map(t, normalizeTransactionDTO);

            var type = t.Type;
            t.TypeName = type == "PairOff" ? "Pair off" : type;
            t.TypeClass = typeClassMap[type] || "undefined-type";

            t.IsGainLossCalculated = t.IsGainLossCalculated || false;
            t.IsAmountCalculated = t.IsAmountCalculated || false;

            return t;
        }

        function newTransaction(type) {
            var $lastTransaction = $transactions.find('tr:visible').last();
            var date;
            if (type == "Opening") date = $('.trade-date').val();
            else date = getCurrentDateString();
            var transaction = {
                "Price": +$('.price').val(),
                "Type": type,
                "Date": date,
                "Expense": "",
                "RunningTotal": $lastTransaction.find('.new-total').val() || "$0.00",
                "GainLossAmount": "",
                "IsGainLossCalculated": true,
                "IsAmountCalculated": true, //only applies to new closing transactions
                "Memo": ""
            };
            //Create a new row for the new transaction and select it, but keep it hidden for now
            var $templateRowHtml = $(LQBMortgagePools.ApplyTemplate(normalizeTransactionDTO(transaction), transaction_row_template));
            $('#transaction-data table tbody tr').removeClass('selected')
            $templateRowHtml.addClass('selected').hide().find('.deleted').val('true'); ;

            $transactions.append($templateRowHtml);
            editTransaction();
        }

        function getCurrentDateString() {
            var d = new Date();
            return $.map([d.getMonth() + 1, d.getDate(), d.getFullYear()], function(elem) {
                return padDateElement(elem);
            }).join('/');
        }

        function padDateElement(i) {
            if (i < 10) return '0' + i;
            return '' + i;
        }

        function editTransaction(isNew, type) {
            var win = window.open("TransactionEditor.aspx", "TransactionEditor", "resizable=yes,scrollbars=no,menubar=no,toolbar=no,status=yes");
            win.focus();
        }

        function VerifyTradeNumber() {
            errorMessage = "Please wait while the trade number is checked";
            $('.TradeNum').siblings('.validation').hide();

            var tradeNum = $.trim($('.TradeNum').val());

            if (tradeNum == '') {
                errorMessage = "Please enter a trade number";
                return false;
            }

            if (tradeNum == $('#OriginalTradeNum').val()) {
                errorMessage = null;
                return true;
            }

            var DTO = {
                TradeNum: +tradeNum
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'TradeEditor.aspx/IsTradeNumUnique',
                data: JSON.stringify(DTO),
                dataType: 'json',

                success: function(msg) {
                    if (!msg.d) {
                        $('.TradeNum').siblings('.validation').show();
                        errorMessage = "Trade number is already in use, please choose another.";
                        
                        <% if(IsAutoGenerateTradeNums) { %>
                            errorMessage += "\nOr leave blank to auto-generate trade number.";
                        <% } %>
                    }
                    else {
                        errorMessage = null;
                    }
                },

                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("An unexpected error happened, please try again later");
                }
            });
        }

        var $openAmount = $(".open-amount");
        var $totalGainLoss = $('.total-gain-loss');
        var $totalExpense = $('.total-expense');
        function updateTransactionStatistics() {
            var openAmt = 0;
            var gainLoss = 0;
            var expense = 0;
            
            $('#transactions table tbody tr:visible').each(function() {
                openAmt += fromRep($('td.amount', this).text());
                gainLoss += fromRep($('td.gain-loss', this).text());
                expense += fromRep($('td.expense', this).text());
            });

            format_money($openAmount.val(openAmt)[0]);
            format_money($totalGainLoss.val(gainLoss)[0]);
            format_money($totalExpense.val(expense)[0]);
        }

        var $openBtn = $("#OpeningTransaction");
        var $pairBtn = $("#PairOffTransaction");
        var $closeBtn = $("#CloseTransaction");
        var $transactions = $('#transaction-data table tbody');
        var $transactionEditorButtons = $('.new-transaction');
        function updateTransactionState() {
            disableTransactionButtons();
            updateTransactionStatistics();

            //If we don't have any transactions, only allow creating an Opening transaction
            if ($transactions.is(".empty") || $transactions.find('tr:visible').length == 0) {
                $openBtn.removeAttr('disabled');
                return;
            }

            //Only allow creating more transactions if we don't have a closing transaction
            var $closing = $transactions.find('tr:visible').filter('.closing');
            if (!$closing.length) {
                $pairBtn.removeAttr('disabled');
                $closeBtn.removeAttr('disabled');
            }


            var $pairOff = $transactions.find('tr:visible').filter('.pair-off');
            var $opening = $transactions.find('tr:visible').filter('.opening');

            enableAllDeleteLinks();

            //If we have pair-off transactions, disable delete for opening
            //If we have a closing transaction, disable delete for pair-offs
            if ($pairOff.length > 0) disableDeleteLinks($opening);
            if ($closing.length > 0) disableDeleteLinks($pairOff);
        }

        function disableDeleteLinks($rows) {
            $rows.each(function() {
                setDisabledAttr($(this).find('td.delete'), true);
            });
        }

        function enableAllDeleteLinks() {
            setDisabledAttr($transactions.find('td.delete'), false);
        }

        function disableTransactionButtons() {
            $transactionEditorButtons.attr('disabled', 'disabled');
        }

        var transaction_row_template = '<tr id="trans_:TransactionId:" class=":TypeClass:">\
<td class="edit action"> edit \
<input type="hidden" value=":TransactionId:" class="transaction-id" /> \
<input type="hidden" value=":RunningTotal:" class="new-total" /> \
<input type="hidden" value="false" class="deleted" /> \
<input type="hidden" value=":Type:" class="type" /> \
<input type="hidden" value=":LinkedId:" class="linked-id" /> \
<input type="hidden" value=":IsGainLossCalculated:" class="gain-loss-calculated" /> \
<input type="hidden" value=":CalculatedGainLossAmount:" class="gain-loss-calculated-val" /> \
<input type="hidden" value=":IsAmountCalculated:" class="transaction-amt-calculated" /> \
</td> \
<td class="type-name">:TypeName:</td> \
<td class="date">:Date:</td> \
<td class="amount">:Amount: </td> \
<td class="price">:Price:</td> \
<td class="gain-loss">:GainLossAmount: </td> \
<td class="expense">:Expense:</td> \
<td class="linked-name action">:LinkedName: </td> \
<td class="memo">:Memo:</td> \
<td class="delete action">delete</td></tr>';

        //Transactions is snuck in by the codebehind.
        var templateRowHtml = LQBMortgagePools.ApplyTemplate(normalizeTransactionDTO(Transactions), transaction_row_template);
        $transactions.empty().append(templateRowHtml);

        $.tablesorter.addParser({
            id: 'transactionTypeSort',
            is: function(value, table, cell) {
                return $(cell).is('.type-name');
            },
            format: function(value, table, cell) {
                if (value == "Opening") return Number.NEGATIVE_INFINITY;
                if (value == "Closing") return Number.POSITIVE_INFINITY;

                return +new Date($(cell).next('.date').text());
            },
            type: 'numeric'
        });

        LQBMortgagePools.SetupTablesort($('#transaction-data table'), "No transactions in this trade", {
            sortList: [[1, 0]]
        });

        updateTransactionState();

        var IsAutoGenerateTradeNums = <%= AspxTools.JsBool(IsAutoGenerateTradeNums) %>;
        LQBMortgagePools.BeforePostback(function(target) {
            if (/Save$/.test(target)) {
                var currentTradeNum = $.trim($('.TradeNum').val());
                if (errorMessage || (currentTradeNum == '' && !IsAutoGenerateTradeNums )) {
                    alert(errorMessage || "Please enter a trade number");
                    return false;
                }
            }

            var transactions = GetAllTransactions();
            var transactionJSON = JSON.stringify(transactions);
            $('.TransactionJSON').val(transactionJSON);
        });

        var clientToServerMap = {
            "amount": "Amount",
            "date": "Date",
            "expense": "Expense",
            "gain-loss": "GainLossAmount",
            "gain-loss-calculated": "IsGainLossCalculated",
            "link": "Link",
            "memo": "Memo",
            "price": "Price",
            "transaction-id": "TransactionId",
            "type": "Type",
            "deleted": "Deleted",
            "linked-id": "LinkedId",
            "transaction-amt-calculated": "IsAmountCalculated"
        }

        function GetAllTransactions() {
            var ret = [];
            if ($("#transaction-table").is(".empty")) return ret;

            $("#transaction-table tbody tr").each(function() {
                ret.push(LQBMortgagePools.ToObject(this,
                    'td, input',
                    function($e) {
                        var name = $e.attr('class').split(' ')[0];
                        if (name == "delete" || name == "edit") return false;
                        return clientToServerMap[name];
                    },
                    function($e) {
                        var value;
                        if ($e.is(":input")) value = $e.val();
                        else value = $e.text();
                        return value;
                    }
                ));
            });
            return normalizeTransactionDTO(ret);
        }

        //Put these in the Window scope so our child windows can call them
        window.TradeAssociated = function TradeAssociated(poolId, poolName) {
            $('#PreAssociateInfo').html('This trade will be associated with ' + poolName +
                                        ' when you save <span class="Action cancel-assign">cancel</span>');
            $('.ToAssociateId').val(poolId);
        }


        window.GetTradeJSON = function GetTradeJSON() {
            var trade = LQBMortgagePools.ToObject($('#trade-info'));
            var ret = JSON.stringify(trade);
            return ret;
        }

        window.GetTransactionJSON = function GetTransactionJSON() {
            var transaction = LQBMortgagePools.ToObject($("#transaction-table tbody tr.selected"),
                'td, input',
                function($e) { //name selection / filtering
                    var name = $e.attr('class').split(' ')[0];
                    if (name == "delete" || name == "edit") return false;
                    return name;
                },
                function($e) { //value selection
                    var ret = $e.text();
                    if (ret == "") ret = $e.val();
                    return ret;
                }
            );
            return JSON.stringify(transaction);
        }

        window.ApplyTransactionJSON = function ApplyTransactionJSON(json) {
            var transaction = JSON.parse(json);
            LQBMortgagePools.ApplyObject($('#transaction-table tbody tr.selected'), transaction);

            $('#transaction-table tbody tr.selected').show().find('.deleted').val('false');
            $('#transaction-data table').trigger("UpdateTableSorter");

            updateTransactionState();
        }

        //This guy should be pulled into the common Pools object...
        function fromRep(input) {
            if (!input) return 0;
            if (input instanceof jQuery) input = input.val();

            var neg = 1;
            if (input.indexOf('(') != -1 || input.indexOf('-') != -1) neg = -1

            return +input.replace(/[^0-9.]/g, '') * neg;
        }
    });

</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<div id="top-trade-controls">
    <input type="button" id="Associate" value="Associate with pool..." /> 
    <input type="button" id="Remove" value="Remove from pool" />
    <span id="PreAssociateInfo"></span>
</div>
<div id="trade-info" class="stack-labels">
    <div class="column" id="left">
        <label><span>Trade #</span><asp:TextBox ID="TradeNum_rep" class="TradeNum" data-fieldname="TradeNum" runat="server"></asp:TextBox> <span class="validation">In use</span></label>
        <label><span>Security</span><asp:DropDownList ID="Description_rep" data-fieldname="Description" runat="server"></asp:DropDownList></label>
        <label id="Type"><span>Type</span><asp:DropDownList ID="Type_rep" data-fieldname="Type" class="type" runat="server" ></asp:DropDownList>
                                          <asp:DropDownList ID="Direction_rep" data-fieldname="Direction" class="direction" runat="server"></asp:DropDownList></label>
        <label><span>Month</span><asp:DropDownList ID="Month_rep" data-fieldname="Month" runat="server"></asp:DropDownList></label>
        <label><span>Coupon</span><asp:TextBox ID="Coupon_rep" data-fieldname="Coupon" preset="coupon" runat="server"></asp:TextBox></label>
        <label><span>Price</span><asp:TextBox ID="Price_rep" data-fieldname="Price" class="price" preset="price" runat="server"></asp:TextBox></label>
        <input type="hidden" class="ToAssociateId" id="PoolIdToAssociate" runat="server" />
        <input type="hidden" data-fieldname="PreClosingAmount" id="PreClosingAmount" runat="server" />
    </div>
    <div class="column" id="right">
        <label>
            <span>Dealer/Investor</span><input type="text" id="SelectedInvestor" data-fieldname="SelectedInvestor" class="autocomplete-value" runat="server" />
            <asp:DropDownList ID="DealerInvestor" class="autocomplete" runat="server"></asp:DropDownList> 
        </label>
        <label><span>Assigned to</span><asp:TextBox ID="AssignedTo" data-fieldname="AssignedTo" runat="server"></asp:TextBox></label>
        <label><span>Current Open Amount</span><asp:TextBox ID="CurrentOpenAmt" data-fieldname="CurrentOpenAmt" runat="server" class="open-amount" Enabled="false"></asp:TextBox></label>
        <label><span>Trade Date</span><ml:datetextbox ID="TradeDate" data-fieldname="TradeDate" runat="server" width="75" preset="date" CssClass="mask trade-date" /></label>
        <label><span>Notification Date</span><ml:datetextbox ID="NotificationDate" data-fieldname="NotificationDate" runat="server" width="75" preset="date" CssClass="mask" /></label>
        <label><span>Settlement Date</span><ml:datetextbox ID="SettlementDate" data-fieldname="SettlementDate" runat="server" width="75" preset="date" CssClass="mask" /></label>
    </div>
    <div class="clear"> </div>
</div>
<hr />
<div id="transactions">
    <input type="hidden" id="TransactionJSON" class="TransactionJSON" runat="server" />
    <div id="top-transaction-controls" class="column">
        <input type="button" value="Enter Opening Transaction..." id="OpeningTransaction" class="new-transaction Opening" />
        <input type="button" value="Pair off Trade..." id="PairOffTransaction" class="new-transaction PairOff" />
        <input type="button" value="Close Trade..." id="CloseTransaction" class="new-transaction Closing" />
    </div>
    <div id="top-transaction-info" class="column">
        <label><span>Total Gain/Loss </span><asp:TextBox ID="TotalGainLoss" runat="server" Enabled="false" class="total-gain-loss"></asp:TextBox></label>
        <label><span>Total Expense </span><asp:TextBox ID="TotalExpense" runat="server" Enabled="false" class="total-expense"></asp:TextBox></label>
    </div>
    <div class="clear"> </div>
    <div id="transaction-data">
        <table cellspacing="0px" id="transaction-table">
        <thead>
            <tr>
                <th></th>
                <th>Type</th>
                <th>Date</th>
                <th>Amount</th>
                <th>Price</th>
                <th>Gain/Loss</th>
                <th>Expense</th>
                <th>Linked security</th>
                <th>Memo</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <!-- gets filled in by javascript !-->
        </tbody>
        </table>
        <div id="bottom-transaction-controls">
            <input type="button" value="Save" runat="server" onserverclick="SaveClicked" id="Save" class="save" />
        </div>
    </div>
</div>
</asp:Content>

