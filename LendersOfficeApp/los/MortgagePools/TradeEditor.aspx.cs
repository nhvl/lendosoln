﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.CapitalMarkets;
using LendersOfficeApp.los.MortgagePools.MasterPages;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
using System.Web.Services;

namespace LendersOfficeApp.los.MortgagePools
{
    public partial class TradeEditor : BaseMortgagePoolPage
    {
        void Page_PreInit(object sender, EventArgs e)
        {
            if (EditingTrade.AssociatedPoolId.HasValue)
            {
                MasterPageFile = "./MasterPages/PoolEditor.master";
                ClientScript.RegisterHiddenField("AssociatedWithPool", "true");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var standAlone = Master as StandAloneEditor;
            if (standAlone != null)
            {
                standAlone.LinkName = "Trade #" + EditingTrade.TradeNum_rep;
            }
            if (!Page.IsPostBack)
            {
                BindData(EditingTrade);
            }

            ClientScript.RegisterHiddenField("TradeId", EditingTrade.TradeId.ToString());
            ClientScript.RegisterHiddenField("OriginalTradeNum", EditingTrade.TradeNum_rep);
        }

        private void BindData(Trade trade)
        {
            //DDLs
            //Association_unused.SelectedValue = trade.Association;

            var descriptions = Description.GetDescriptions();
            Tools.Bind_Generic(Description_rep, descriptions,
                a => a.DescriptionId == trade.DescriptionObj.DescriptionId,
                a => a.DescriptionName,
                a => a.DescriptionId.ToString());

            Tools.Bind_Generic(Type_rep, trade.Type, a => a.Replace("Option", ""));
            Tools.Bind_Generic(Direction_rep, trade.Direction);

            Tools.BindMonthNamesDropDown(Month_rep);
            Month_rep.SelectedIndex = trade.Month;

            //The drop default binding gives us an empty spot, which we don't want.
            //Note that if we did this before assigning the selected index we would have to offset it.
            Month_rep.Items.RemoveAt(0);

            //This one actually gets hidden and used as fodder for the autocomplete; the actual value is
            //kept in SelectedInvestor.
            Tools.Bind_Generic(DealerInvestor, Broker.DealerInvestors,
                a => {
                    if (string.IsNullOrEmpty(trade.DealerInvestor)) return false;
                    return trade.DealerInvestor.Equals(a, StringComparison.CurrentCultureIgnoreCase);
                });

            //Text
            SelectedInvestor.Value = trade.DealerInvestor;
 	        TradeNum_rep.Text = trade.TradeNum_rep;
            Coupon_rep.Text = trade.Coupon_rep;
            Price_rep.Text = trade.Price_rep;
            AssignedTo.Text = trade.AssignedTo;
            CurrentOpenAmt.Text = trade.Amount_rep;
            TotalGainLoss.Text = trade.TotalGainLoss_rep;
            TotalExpense.Text = trade.TotalExpense_rep;

            //Dates
            string tradeDate = trade.TradeDate_rep;
            if (string.IsNullOrEmpty(tradeDate)) tradeDate = DateTime.Today.ToShortDateString();

            TradeDate.Text = tradeDate;
            NotificationDate.Text = trade.NotificationDate_rep;
            SettlementDate.Text = trade.SettlementDate_rep;

            //Other
            PoolIdToAssociate.Value = trade.AssociatedPoolId.HasValue ? trade.AssociatedPoolId.ToString() : "";
            ClientScript.RegisterHiddenField("TradeId", trade.TradeId.ToString());

            PreClosingAmount.Value = trade.PreClosingAmount.ToString();

            RegisterJsStruct("Transactions", trade.GetTransactions().ToDTO());
        }

        protected void SaveClicked(object sender, EventArgs e)
        {
            SaveData(EditingTrade);
        }

        private void SaveData(Trade trade)
        {
            if (String.IsNullOrEmpty(TradeNum_rep.Text.TrimWhitespaceAndBOM()) && IsAutoGenerateTradeNums)
                trade.TradeNum_rep = Trade.GetTradeAutoNum(BrokerUserPrincipal.CurrentPrincipal.BrokerId).ToString();
            else
                trade.TradeNum_rep = TradeNum_rep.Text;

            trade.DescriptionObj = Description.GetDescriptionById(new Guid(Description_rep.SelectedValue));
            trade.Direction = (E_TradeDirection)Convert.ToInt32(Direction_rep.SelectedValue);
            trade.Type = (E_TradeType)Convert.ToInt32(Type_rep.SelectedValue);
            trade.Month = Convert.ToInt32(Month_rep.SelectedValue);

            trade.Coupon_rep = Coupon_rep.Text;
            trade.Price_rep = Price_rep.Text;

            trade.DealerInvestor = SelectedInvestor.Value;
            trade.AssignedTo = AssignedTo.Text;

            trade.TradeDate_rep = TradeDate.Text;
            trade.NotificationDate_rep = NotificationDate.Text;
            trade.SettlementDate_rep = SettlementDate.Text;

            long poolId;
            if (long.TryParse(PoolIdToAssociate.Value, out poolId))
            {
                trade.AssociatedPoolId = poolId;
            }
            else if (PoolIdToAssociate.Value.TrimWhitespaceAndBOM().Equals("remove", StringComparison.CurrentCultureIgnoreCase))
            {
                trade.AssociatedPoolId = null;
            }

            var transactions = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<TransactionDTO>>(TransactionJSON.Value);

            trade.Save();
            trade.ApplyTransactions(transactions);

            //Since this was a post, we should redirect back to the same URL
            //(keeps people refreshing the page from getting that nasty "you're gonna post again dude" warning)
            Response.Redirect(GetRedirectURL(trade));
        }

        private string GetRedirectURL(Trade t)
        {
            string poolPart = "";
            if (t.AssociatedPoolId.HasValue)
            {
                poolPart = "&PoolId=" + t.AssociatedPoolId.Value;
            }
            return Request.Url.GetLeftPart(UriPartial.Path) + "?TradeId=" + t.TradeId + poolPart;
        }

        protected static string GetLinked(Transaction t)
        {
            return "a link!";
        }

        protected BrokerDB Broker
        {
            get
            {
                return BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            }
        }

        protected bool IsNew
        {
            get
            {
                return RequestHelper.GetBool("new");
            }
        }

        // Only want to auto generate trade num if new
        // Throw Alert if user removed trade num and tried to save blank
        protected bool IsAutoGenerateTradeNums
        {
            get
            {
                return IsNew && Broker.IsAutoGenerateTradeNums;
            }
        }

        private Trade x_trade = null;
        protected Trade EditingTrade
        {
            get
            {

                if (x_trade != null) return x_trade;

                if (IsNew)
                {
                    x_trade = new Trade(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                }
                else
                {
                    x_trade = Trade.RetrieveById(RequestHelper.GetGuid("TradeId"), BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                }

                return x_trade;
            }
        }

        [WebMethod]
        public static bool IsTradeNumUnique(int TradeNum)
        {
            return Trade.GetTradesByBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId).All(a => a.TradeNum != TradeNum);
        }
    }
}
