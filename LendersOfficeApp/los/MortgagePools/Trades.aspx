﻿<%@ Page Title="" Language="C#" MasterPageFile="~/los/MortgagePools/MasterPages/PoolEditor.master"
    AutoEventWireup="true" CodeBehind="Trades.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.Trades" %>
<%@ Import Namespace="LendersOffice.ObjLib.CapitalMarkets" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
    .stack-labels label span
    {
        width: 150px;
    }
    
    .trade-info span
    {
        display: block;
    }
    
    .trade-info span.remove-trade, .trade-info span.trade-num
    {
        display: inline;
    }
    
    .trade-info.outlined.column
    {
        margin-right: -1px;
        margin-left: 0px;
        padding: 5px;
        padding-right: 30px;
    }
    
    div.info-section
    {
        margin-left: 10px;
    }
    
    #Totals
    {
        margin-left: 480px;
    }
    
    #Totals span
    {
        width: 80px;
    }
    
    label.type select
    {
        width: 60px;
    }
    
    label.type select.direction
    {
        width: 62px;
    }
    #TradeList
    {
        max-width: 800px;
        overflow-x: auto;
    }

</style>
<script type="text/javascript">
    jQuery(function($) {
        var updateWidthTimeout = null;
        $('.remove-trade').addClass('action').on('click', function() {
            var $thisTradeInfo = $(this);
            var tradeNumText = $thisTradeInfo.siblings('.trade-num').text();
            var tradeId = $thisTradeInfo.siblings('.trade-id').val();
            var DTO = {
                tradeId: tradeId
            };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'Trades.aspx/RemoveTrade',
                data: JSON.stringify(DTO),
                dataType: 'json',

                success: function(msg) {
                    $thisTradeInfo.closest('.trade-info').fadeOut();
                    $('.trade_' + tradeId).fadeOut();

                    window.clearTimeout(updateWidthTimeout);
                    updateWidthTimeout = window.setTimeout(function() {
                        updateTradeListWidth();
                    }, 1000);
                },

                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("An unexpected error happened, please try again later");
                }
            });
        });

        $('#AssociateTrade').on('click', function() {
            var win = window.open("TradePicker.aspx", "TradePicker", "resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes");
            win.focus();
        });

        function updateTradeListWidth() {
            var sum = 0;
            $('.trade-info').filter(':visible').each(function() { sum += $(this).width() + 50 });
            $('#TradeListContainer').css({ 'width': sum });
        }

        updateTradeListWidth();


        window.TradeAssociated = function TradeAssociated(tradeId) {
            var poolId = $('#PoolId').val();
            var DTO = {
                poolId: poolId,
                tradeId: tradeId
            };
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: 'Trades.aspx/AddTrade',
                data: JSON.stringify(DTO),
                dataType: 'json',

                success: function(msg) {
                    window.location.reload();
                },

                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("An unexpected error happened, please try again later");
                }
            });
        }
    });
</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
<div id="Trades">
    <div id="TradeControls">
        <div class="column">
            <input type="button" value="Associate Trade..." id="AssociateTrade"/>
        </div>
        <div id="TradeList" class="column">
            <div id="TradeListContainer">
            <asp:Repeater ID="TradeListing" runat="server">
                <ItemTemplate>
                    <div class="trade-info column outlined">
                        <span class="trade-header"> 
                            <span class="trade-num">Trade # <%#AspxTools.HtmlString(((Trade)Container.DataItem).TradeNum_rep)%> </span>
                            <span class="remove-trade"> remove </span>
                            <input type="hidden" class="trade-id" value="<%#AspxTools.HtmlString(((Trade)Container.DataItem).TradeId)%> " />
                        </span>
                        <div class="info-section">
                            <span class="types">
                                <%#AspxTools.HtmlString(((Trade)Container.DataItem).Types)%> 
                            </span>
                            <span class="stats">
                                <%#AspxTools.HtmlString(((Trade)Container.DataItem).Stats)%>                            
                            </span>
                            <span class="amount">
                                Amount: <%#AspxTools.HtmlString(((Trade)Container.DataItem).Amount_rep)%>
                            </span>
                            <span class="price">
                                Price: <%#AspxTools.HtmlString(((Trade)Container.DataItem).Price_rep)%>
                            </span>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            </div>
        </div>
    </div>
    <div class="clear">&nbsp;</div>
    <div id="TradeSummary" class="stack-labels">
        <div id="Settings" class="column left">
            <label>
                <span>Dealer / Investor</span>
                <input type="text" id="DealerInvestor" runat="server" />
            </label>
            <label>
                <span>Security</span>
                <asp:DropDownList ID="Security" runat="server"></asp:DropDownList>
            </label>
            <label class="type">
                <span>Type</span>
                <asp:DropDownList ID="Direction" CssClass="direction" runat="server"></asp:DropDownList>
                <asp:DropDownList ID="Type" runat="server"></asp:DropDownList>
            </label>
            <label>
                <span>Month</span>
                <asp:DropDownList ID="Month" runat="server"></asp:DropDownList>
            </label>
            <label>
                <span>Coupon</span>
                <input type="text" id="Coupon" runat="server" />
            </label>
            <label>
                <span>Weighted Avg Price</span>
                <input type="text" id="AvgPrice" runat="server" />
            </label>
        </div>
        <div id="Dates" class="column right">
            <label>
                <span>Current Open Amount</span>
                <input type="text" id="OpenAmt" runat="server" />
            </label>
            
            <label>
                <span>Latest Trade Date</span>
                <ml:DateTextBox ID="LatestTradeDate" runat="server"></ml:DateTextBox>
            </label>
            
            <label>
                <span>Earliest Notification Date</span>
                <ml:DateTextBox ID="EarliestNotificationDate" runat="server"></ml:DateTextBox>
            </label>
            
            <label>
                <span>Earliest Settlement Date</span>
                <ml:DateTextBox ID="EarliestSettlementDate" runat="server"></ml:DateTextBox>
            </label>
        </div>
    </div>
    <hr class="clear" />
    <div id="Totals" class="stack-labels">
        <label><span>Total Gain/Loss</span> <input type="text" id="TotalGainLoss" runat="server" /></label>
        <label><span>Total Expense</span> <input type="text" id="TotalExpense" runat="server" /></label>
    </div>
</div>
</asp:Content>
