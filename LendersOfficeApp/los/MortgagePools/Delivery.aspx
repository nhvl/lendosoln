﻿<%@ Page Title="" Language="C#" MasterPageFile="~/los/MortgagePools/MasterPages/PoolEditor.master" AutoEventWireup="true" CodeBehind="Delivery.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.Delivery" %>
<%@ Import Namespace="LendersOffice.ObjLib.CapitalMarkets" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<link rel="Stylesheet" type="text/css" href="../../css/MortgagePoolTable.css" />
<style type="text/css">
    .stack-labels .right span, .stack-labels .middle span
    {
        width: 125px;
    }
    .stack-labels .right span.has-checkbox, #BasePricing span.has-checkbox
    {
        width: 106px;
    }
    #BasePricing span
    {
        width: 125px;
    }
    select
    {
        width: 160px;
    }
    .right, .left
    {
        width: 300px;
    }
    .middle
    {
        width: 310px;
    }
    input.header
    {
        width: 145px;
    }
    #mid-spacer
    {
        height: 30px;
    }
    #FinalPriceSummary span
    {
        width: 56px;
    }
    #FinalPriceSummary input.price
    {
        width: 60px;
    }
    #BasePricingTable tr td.rate, #BasePricingTable tr td.price
    {
        height: 20px;
        width: 42%;
    }
    #BasePricingTable tr td.lastColumn
    {
        width: 16%;
        text-align: center;
    }
    #BasePricingTable input
    {
        margin: 0px;
    }
    #AddToBasePricingTable
    {
        margin-top: 10px;
        width: 100%;
    }
    #AddToBasePricingTable input[type=text]
    {
        width: 39%;
    }
    #AddToBasePricingTable input[type=button]
    {
        width: 15%;
    }
    .totals > label
    {
        padding-left: 15px;
    }
    #UnpaidBalance span, #Profitability span
    {
        width: 110px;
    }
    #BasePricingTableErrorMessage
    {
        color: Red;
        padding-top: 5px;
    }
    ul
    {
        list-style-type: none;
        margin-left: 0px;
        padding-left: 0px;
    }
    li
    {
        list-style-type: none;
        margin-left: 0px;
        padding-left: 0px;
    }
    #BasePricing span.w-145 { width: 145px; }
    .stack-labels .right span.w-106, .stack-labels .middle span.w-106 { width: 106px; }
    .w-312 { width: 312px; }
</style>

<script type="text/javascript">
    jQuery(function($) {

        //Total current balance; doesn't ever change on this page.
        var balance = $('#CurrentBalance').val();

        //Set up the radio button groups
        var radioGroups = {};
        $('input:radio, input:checkbox').each(function() {
            var name = $(this).prop('name');
            if (!radioGroups[name]) radioGroups[name] = [];
            radioGroups[name].push(this);
        })
        .each(function() {
            var $this = $(this);
            var name = $this.prop('name');
            var $group = $(radioGroups[name]);
            var $headerField = $this.closest('.outlined').find('input.header');
            var $linkedInput = $this.siblings('input[type=text]');
            var isPrice = $linkedInput.attr('preset') == 'price';

            $this.on('change', function() {
                //trigger a group update event whenever we change
                $group.trigger('update');
                //If we've been activated and we're a price, recalculate our group's linked field.
                if ($this.is(':checked') && isPrice) {
                    $headerField.val(balance * $linkedInput.val() / 100).trigger('blur.mask').trigger('change');
                }
            });

            if (isPrice) {
                $linkedInput.on('blur', function() {
                    $headerField.val(balance * $linkedInput.val() / 100).trigger('blur.mask').trigger('change');
                });
            }
        })
        //And then enable / disable our associated textbox on update if any.
        .each(function() {
            var $this = $(this);
            var enable = $this.data('enable');

            //The reason why we're binding in a $.each instead of doing $.on for the entire selector is here - 
            //we want to cache this selection, because it's expensive.
            var $toEnable = $('input[id$=' + enable + ']');
            if (!$toEnable.length) return;

            var disableValue = $($this.data('disable-value')).val();
            var enableValue = $toEnable.val();
            $this.on('update', function() {
                var isDisabled = $this.is(':disabled');
                $toEnable.prop('disabled', !$this.is(':checked') || isDisabled).trigger('update');

                if (!disableValue || isDisabled) return;

                if ($this.is(':checked')) {
                    $toEnable.val(enableValue).trigger('blur').trigger('change');
                }
                else {
                    enableValue = $toEnable.val();
                    $toEnable.val(disableValue).trigger('blur').trigger('change');
                }
            });
        }).trigger('update');

        var $tolerance = $('input.tolerance');
        var $commitmentBasis = $('input.commitment-basis');
        var $overUnder = $('input.over-under-tol');
        $('.tolerance, .commitment-basis').on('blur', function() {
            var basis = fromRep($commitmentBasis);
            var tolerance = fromRep($tolerance);
            var result = CalcOverUnderTolerance(+balance, +basis, +tolerance);
            $overUnder.val(result).trigger('blur.mask').trigger('change');
        });

        var $openBalance = $('input.open-balance');
        $commitmentBasis.on('change', function() {
            $openBalance.val(fromRep(this.value) - fromRep(balance)).trigger('blur.mask').trigger('change');
        });

        var $blendPairOffCalcType = $('select.blend-pairoff-calc-type');
        var $currentMarketPrice = $('input.current-market-price');
        var $basePricing = $('input.base-pricing');
        var $blendPairOffPrice = $('input.blend-pairoff-price');
        var $blendPairOffAmt = $('input.blend-pairoff-amt');

        $([$blendPairOffCalcType, $currentMarketPrice, $basePricing, $overUnder])
        .map(function() { return this.toArray(); })
        .on('change', function() {
            updateBlendPairOff();
        });

        function updateBlendPairOff() {
            var type = $blendPairOffCalcType.val();
            var base = fromRep($basePricing);
            var marketPrice = fromRep($currentMarketPrice);
            var overUnder = fromRep($overUnder);

            var blendPairOffPartial = GetBlendPairOffAmtPartial(base, +balance, marketPrice);
            var blendPairOffAmt = GetBlendPairOffAmt(blendPairOffPartial, overUnder, type);
            var blendPairOffPrice = GetBlendPairOffPrice(base, blendPairOffAmt, +balance, type);

            $blendPairOffAmt.val(blendPairOffAmt).trigger('blur.mask').trigger('change');
            $blendPairOffPrice.val(blendPairOffPrice).trigger('blur.mask').trigger('change');
        }

        var $summaryInputs = $('input.summary');
        var $finalPrice = $('input.final-price');
        var $finalAmt = $('input.final-amt');
        $summaryInputs.on('change', function() {
            var sum = 0;
            $summaryInputs.each(function() {
                sum += fromRep($(this).val());
            });

            $finalAmt.val(sum).trigger('blur.mask');
            if (balance != 0) {
                $finalPrice.val(sum / balance * 100).trigger('blur.mask').trigger('change');
            }
        });

        function CalcOverUnderTolerance(TotalCurrentBalance, CommitmentBasis, Tolerance) {
            if (TotalCurrentBalance >= CommitmentBasis) {
                return Math.max(0, TotalCurrentBalance - CommitmentBasis * (1 + Tolerance));
            }
            else {
                return Math.min(0, TotalCurrentBalance - CommitmentBasis * (1 - Tolerance));
            }
        }

        function GetBlendPairOffAmt(BlendPairOffPartial, ToleranceAmt, BlendPairOffType) {
            var currentMarket = ToleranceAmt * -BlendPairOffPartial;
            var worstCase = Math.min(currentMarket, 0);

            switch (+BlendPairOffType) {
                case (0/*none*/): return 0;
                case (1/*worst case*/): return worstCase;
                case (2/*current market*/):
                    if (ToleranceAmt < 0) return worstCase;
                    return currentMarket;
                default: throw "BlendPairOffType [" + BlendPairOffType + "] is not supported";
            }
        }

        function GetBlendPairOffPrice(BasePricing, BlendPairOffAmt, TotalCurrentBalance, BlendPairOffType) {
            if (BlendPairOffType == 0) return 0;
            return (BasePricing + BlendPairOffAmt) / TotalCurrentBalance * 100;
        }

        function GetBlendPairOffAmtPartial(BasePricing, TotalCurrentBalance, BlendPairOffMarketPrice) {
            return BasePricing / TotalCurrentBalance - BlendPairOffMarketPrice / 100;
        }

        var basePricingTableRowTemplate = "\
            <tr>\
                <td class='rate'>:rate:</td>\
                <td class='price'>:price:</td>\
                <td class='lastColumn'><a href='#' class='remove'>delete</a></td>\
            </tr>";

        $basePricingTable = $('#BasePricingTable tbody');
        $basePricingTablePrice = $('input.table-calc-val');
        $basePricingTableRb = $('input.base-pricing-table');
        $basePricingTableErrorMessageList = $('#BasePricingTableErrorMessage');
        $addRatePriceBtn = $('#AddRatePrice');

        NoteRateToLoanAmounts = ProcessNoteRate(NoteRateToLoanAmounts);
        SetupBasePricingTable(BasePricingTable, NoteRateToLoanAmounts, basePricingTableRowTemplate);

        $('#BasePricing').find('input:radio').on('change', function() {
            validateNoteRatePriceTable(NoteRateToLoanAmounts, getBasePricingTableValues());
        });

        function ProcessNoteRate(source) {
            //The dictionary gets serialized into JSON as a list of Key, Value pairs for some reason
            //Key is the loan note rate, Value is the list of total upb for loans with that rate.
            //We'll fix it up here so it makes sense in Javascript.
            var result = {};
            $.each(source, function(idx, elem) {
                result[elem.Key] = elem.Value;
            });

            return result;
        }

        function SetupBasePricingTable(storedBasePricing, noteRateToLoanAmt, template) {
            var basePricing = [];

            $.each(storedBasePricing, function(idx, elem) {
                basePricing.push({ rate: elem.key, price: elem.value });
            });

            basePricing.sort(function(a, b) { return b.rate - a.rate; });

            // In order to get the nice formatting without reinventing the wheel,
            // we're going to make use of the mask functionality.
            var $rateInput = $('<input type="text" preset="percent"></input>');
            var $priceInput = $('<input type="text" preset="price"></input>');

            _initMask($rateInput[0]);
            _initMask($priceInput[0]);

            var formattedBasePricing = $.map(basePricing, function(elem, index) {
                return {
                    rate: $rateInput.val(elem.rate).trigger('blur.mask').trigger('change').val(),
                    price: $priceInput.val(elem.price).trigger('blur.mask').trigger('change').val()
                };
            });

            var rows = LQBMortgagePools.ApplyTemplate(formattedBasePricing, template);
            $(rows).appendTo($basePricingTable);

            var options = {
                sortList: [[0, 0]],
                widgets: ['zebra']
            }

            LQBMortgagePools.SetupTablesort($('#BasePricingTable'), "No entries.", options);
            updateTradePrice(noteRateToLoanAmt, basePricing);
            validateNoteRatePriceTable(noteRateToLoanAmt, basePricing);

            $addRatePriceBtn.on('click', function() {
                var $rate = $(this).siblings('input.rate');
                var $price = $(this).siblings('input.price');

                var ratePrice = {
                    rate: $rate.val(),
                    price: $price.val()
                };

                if (fromRep(ratePrice.rate) === 0 || fromRep(ratePrice.price) === 0) {
                    alert('Please enter a valid note rate and price in order to add.');
                    return;
                }

                var row = LQBMortgagePools.ApplyTemplate(ratePrice, basePricingTableRowTemplate);
                $basePricingTable.append(row);

                $basePricingTable.trigger('UpdateTableSorter');

                $rate.val('');
                $price.val('');

                var basePricingTableValues = getBasePricingTableValues();
                updateTradePrice(noteRateToLoanAmt, basePricingTableValues);
                validateNoteRatePriceTable(noteRateToLoanAmt, basePricingTableValues);
            });

            $basePricingTable.on('click', '.remove', function() {
                $(this).closest('tr').hide();
                $basePricingTable.trigger('UpdateTableSorter');

                var basePricingTableValues = getBasePricingTableValues();
                updateTradePrice(noteRateToLoanAmt, basePricingTableValues);
                validateNoteRatePriceTable(noteRateToLoanAmt, basePricingTableValues);
            });
        }

        LQBMortgagePools.BeforePostback(function() {
            var baseTable = baseTableToObject();
            $('input.base-pricing-json').val(JSON.stringify(baseTable));
        });

        function baseTableToObject() {
            var ret = {};

            // Filter out hidden (deleted) rows
            $basePricingTable.find('tr')
                .filter(':visible')
                .each(function() {
                    var $this = $(this);
                    var rate = fromRep($this.find('td.rate').html());
                    var price = fromRep($this.find('td.price').html());

                    ret[rate] = price;
                });

            return ret;
        }

        function updateTradePrice(noteRateToLoanAmt, basePricingTableValues) {
            var total = 0;
            $.each(basePricingTableValues, function(index, value) {
                var noteRate = value.rate;
                var price = value.price;
                var amounts = noteRateToLoanAmt[noteRate];
                if (!amounts || !price) return;
                $.each(amounts, function(idx, amount) {
                    total += amount * price;
                });
            });
            var displayVal = Math.round((total / balance * 1000000)) / 1000000;
            $basePricingTablePrice.val(displayVal).trigger('blur.mask').trigger('change');
            if (!$basePricingTableRb.is(':checked')) return;

            $basePricing.val(total).trigger('blur.mask').trigger('change');
        }

        function validateNoteRatePriceTable(noteRateToLoanAmt, basePricingTableValues) {
            var i, noteRates = [], uniqueNoteRates = [], duplicateNoteRates = [],
                missingNoteRates = [], errorMessages = [];

            $basePricingTableErrorMessageList.empty();
            $basePricingTableErrorMessageList.hide();

            if (!$basePricingTableRb.is(':checked')) {
                toggleSaveButton(false);
                return;
            }

            for (i = 0; i < basePricingTableValues.length; i++) {
                noteRates.push(basePricingTableValues[i].rate);
            }

            $.each(noteRateToLoanAmt, function(noteRate, loanAmt) {
                var parsedNoteRate = parseFloat(noteRate);
                if ($.inArray(parsedNoteRate, noteRates) === -1 && $.inArray(parsedNoteRate, missingNoteRates) === -1) {
                    missingNoteRates.push(parsedNoteRate);
                }
            });

            if (missingNoteRates.length > 0) {
                errorMessages.push('Error: Missing prices for ' + missingNoteRates.join(', '));
            }

            $.each(noteRates, function(index, noteRate) {
                if ($.inArray(noteRate, uniqueNoteRates) === -1) {
                    uniqueNoteRates.push(noteRate);
                }
                else if ($.inArray(noteRate, duplicateNoteRates) === -1) {
                    duplicateNoteRates.push(noteRate);
                }
            });

            if (duplicateNoteRates.length > 0) {
                errorMessages.push('Error: Multiple prices for ' + duplicateNoteRates.join(', '));
            }

            if (errorMessages.length > 0) {
                $.each(errorMessages, function(index, errorMessage) {
                    $li = $('<li/>')
                        .text(errorMessage)
                        .appendTo($basePricingTableErrorMessageList);
                });

                $basePricingTableErrorMessageList.show();
            }

            toggleSaveButton(errorMessages.length > 0);
        }

        function toggleSaveButton(isDisable) {
            $('.save').prop('disabled', isDisable);
        }

        function getBasePricingTableValues() {
            var pricingTableValues = [];
            $basePricingTable.find('tr')
                .filter(':visible')
                .each(function() {
                    var $this = $(this);
                    var rate = fromRep($this.find('td.rate').html());
                    var price = fromRep($this.find('td.price').html());

                    pricingTableValues.push({ 'rate': rate, 'price': price });
                });

            return pricingTableValues;
        }

        function fromRep(input) {
            return LQBMortgagePools.FromRep(input);
        }

        LQBMortgagePools.InitMirror('summary');
    });
</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
<input type="hidden" id="BasePricingTableJSON" runat="server" class="base-pricing-json" />
<div id="DeliveryInfo" class="stack-labels">
    <div id="CommitmentInfo">
        <div id="Investment" class="column left w-312">
            <label><span>Investor</span><asp:DropDownList ID="InvestorName" runat="server"></asp:DropDownList></label>
            <label><span>Commitment type</span><asp:DropDownList ID="CommitmentType" runat="server"></asp:DropDownList></label>
            <label><span>Settlement type</span><asp:DropDownList ID="SettlementType" runat="server"></asp:DropDownList></label>
            <label><span>Delivery type</span><asp:DropDownList ID="DeliveryType" runat="server"></asp:DropDownList></label>
        </div>

        <div id="CommitmentDates" class="column middle">
            <label><span>Commitment date</span><ml:DateTextBox ID="CommitmentDate" runat="server"></ml:DateTextBox></label>
            <label><span>Early delivery date</span><ml:DateTextBox ID="EarlyDeliveryDate" runat="server"></ml:DateTextBox></label>
            <label><span>Commitment expires</span><ml:DateTextBox ID="CommitmentExpires" runat="server"></ml:DateTextBox></label>
            <label><span>Assign date</span><ml:DateTextBox ID="AssignDate" runat="server"></ml:DateTextBox></label>
            <label><span>Confirm date</span><ml:DateTextBox ID="ConfirmDate" runat="server"></ml:DateTextBox></label>
        </div>
            
        <div id="CommitmentBasisInfo" class="column right">
            <!-- For formatting reasons, there can't be a whitespace between the <input> and the <span> or between the <input>s. -->
            <label><span>Commitment number</span><input type="text" id="CommitmentNum" runat="server" /></label>
            <label><span class="has-checkbox">Commitment basis</span><input 
                type="checkbox" id="CommitmentBasisLckd" runat="server" data-enable="CommitmentBasis" data-disable-value="#CommitmentBasisCalc" /><input 
                type="text" id="CommitmentBasis" runat="server" disabled="disabled" preset="money" class="commitment-basis" />
            </label>
            <label><span>Open balance</span><input type="text" id="OpenBalance" runat="server" disabled="disabled" preset="money" class="open-balance" /></label>
            <label><span>Tolerance</span><input type="text" id="Tolerance" runat="server" preset="percent" class="tolerance" /></label>
            <label><span>Over/under tolerance</span><input type="text" id="OverUnderTolerance" runat="server" disabled="disabled" class="over-under-tol" preset="money" /></label>
        </div>
    </div>
    <div class="clear" id="mid-spacer"></div>
    <div id="Properties">
        <div id="BasePricing" class="column left outlined">
            <label>
                <span class="w-145">Base pricing</span>
                <input type="text" id="BasePricingManual" class="header base-pricing" runat="server" preset="money" disabled="disabled" />
            </label>
            
            <label>
                <input type="radio" name="BasePricing" runat="server" id="BasePricingFromManual" data-enable="BasePricingManual"/>
                <span>Manually</span>
            </label>
            <label>
                <input type="radio" name="BasePricing" runat="server" id="BasePricingFromBackend" />
                <span>From Back-end Lock</span>
                <input type="text" runat="server" id="BackendCalcValue" preset="price" disabled="disabled" />
            </label>
            <label>
                <input type="radio" name="BasePricing" runat="server" id="BasePricingFromPool" data-enable="BasePricingPoolLevelLckd" />
                <span class="has-checkbox">Pool-level</span><input type="checkbox" runat="server" id="BasePricingPoolLevelLckd" data-enable="BasePricingPoolLevelManual" data-disable-value="#PoolLevelPricingCalc" disabled="disabled" />
                <input type="text" runat="server" preset="price" id="BasePricingPoolLevelManual" disabled="disabled" /> 
            </label>
            <label>
                <input type="radio" name="BasePricing" runat="server" id="BasePricingFromTable" class="base-pricing-table" />
                <span>Table</span>
                <input type="text" runat="server" id="TableCalcValue" class="table-calc-val" preset="price" disabled="disabled" />
            </label>
            <table id="BasePricingTable" cellspacing="0px">
                <thead>
                    <tr>
                        <th class="rate">
                            Note Rate
                        </th>
                        <th class="price">
                            Price
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div id="AddToBasePricingTable">
                <input type="text" preset="percent" class="rate" />
                <input type="text" preset="price" class="price" />
                <input type="button" value="Add" id="AddRatePrice" />
            </div>
            <ul id="BasePricingTableErrorMessage">
            </ul>
        </div>
        <div id="LLPA_SRP" class="column middle">
            <div id="LLPA" class="outlined">
                <label>
                    <span>LLPA</span> 
                    <input type="text" id="LLPAManual" class="header llpa" disabled="disabled" preset="money" runat="server" />
                </label>
                
                <label>
                    <input type="radio" name="LLPA" runat="server" id="LLPAFromManual" data-enable="LLPAManual" />
                    <span>Manually</span>
                </label>
                <label>
                    <input type="radio" name="LLPA" runat="server" id="LLPAFromBackend" />
                    <span class="w-106">From Back-end Lock</span>
                    <input type="text" runat="server" id="LLPABackendRate" preset="price" disabled="disabled" />
                </label>
            </div>
            
            <div id="SRP" class="outlined">
                <label>
                    <span>SRP</span> 
                    <input type="text" id="SRPManual" class="header srp" runat="server" preset="money" disabled="disabled" />
                </label>
                
                <label>
                    <input type="radio" name="SRP" runat="server" id="SRPFromManual" data-enable="SRPManual" />
                    <span class="w-106">Manually</span>
                </label>
                <label>
                    <input type="radio" name="SRP" runat="server" id="SRPFromBackend" />
                    <span class="w-106">From Back-end Lock</span>
                    <input type="text" runat="server" id="SRPBackendRate" preset="price" disabled="disabled" />
                </label>
                <label>
                    <input type="radio" name="SRP" runat="server" id="SRPFromPool" data-enable="SRPPoolLevel" />
                    <span class="w-106">Pool-level</span>
                    <input type="text" runat="server" id="SRPPoolLevel" preset="price" disabled="disabled" />
                </label>
            </div>
            
            <div id="Blend" class="outlined">
                <label>
                    <span>Blend/Pair off calculation</span>
                    <asp:DropDownList ID="BlendPairOffCalcType" class="blend-pairoff-calc-type" runat="server"></asp:DropDownList>
                </label>
                <label>
                    <span>Current market price</span>
                    <input type="text" runat="server" id="BlendPairOffCurrPrice" class="current-market-price" preset="price" />
                </label>
                <label>
                    <span>Net Blend/Pair off price</span>
                    <input type="text" runat="server" id="BlendPairOffCalcNetPrice" class="blend-pairoff-price" preset="price" disabled="disabled" />
                </label>
                <label>
                    <span>Blend/Pair off gain/loss</span>
                    <input type="text" runat="server" id="BlendPairOffCalcNetAmount" class="blend-pairoff-amt" preset="money" disabled="disabled" />
                </label>
            </div>
        </div>
        <div id="Summary_Totals" class="column right">
            <div id="Summary" class="outlined">
                <label>
                    <span>Base pricing</span>
                    <input type="text" id="SummaryBasePricing" runat="server" disabled="disabled" data-mirrored="input.base-pricing" class="Mirror summary mirror-source" preset="money" />
                </label>
                <label>
                    <span>Blend/Pair off gain/loss</span>
                    <input type="text" id="SummaryBlendPairOffCalcNetAmount" runat="server" disabled="disabled" data-mirrored="input.blend-pairoff-amt" class="Mirror summary mirror-source" preset="money" />
                </label>
                <label>
                    <span>LLPA</span>
                    <input type="text" id="SummaryLLPA" runat="server" disabled="disabled" data-mirrored="input.llpa" class="Mirror summary mirror-source" preset="money" />
                </label>
                <label>
                    <span>SRP</span>
                    <input type="text" id="SummarySRP" runat="server" disabled="disabled" data-mirrored="input.srp" class="Mirror summary mirror-source" preset="money" />
                </label>
                <label id="FinalPriceSummary">
                    <span>Final price</span>
                    <input type="text" id="FinalPrice" class="price final-price" preset="price" runat="server" disabled="disabled" />
                    <input type="text" id="FinalAmt" runat="server" preset="money" disabled="disabled" class="final-amt" />
                </label>
            </div>
            <div id="Totals" class="outlined">
                <div id="UnpaidBalance" class="totals">
                    <span>Unpaid Balance</span>
                    <label>
                        <span>Amount allocated</span>
                        <input type="text" id="UnpaidBalanceAmountAllocated" preset="money" runat="server" disabled="disabled" />
                    </label>
                    <label>
                        <span>Amount shipped</span>
                        <input type="text" id="UnpaidBalanceAmountShipped" preset="money" runat="server" disabled="disabled" />
                    </label>
                    <label>
                        <span>Amount purchased</span>
                        <input type="text" id="UnpaidBalanceAmountPurchased" preset="money" runat="server" disabled="disabled" />
                    </label>
                </div>
                
                <div id="Profitability" class="totals">
                    <span>Profitability</span>
                    <label>
                        <span>Amount allocated</span>
                        <input type="text" id="ProfitabilityAmountAllocated" preset="money" runat="server" disabled="disabled" />
                    </label>
                    <label>
                        <span>Amount shipped</span>
                        <input type="text" id="ProfitabilityAmountShipped" preset="money" runat="server" disabled="disabled" />
                    </label>
                    <label>
                        <span>Amount purchased</span>
                        <input type="text" id="ProfitabilityAmountPurchased" preset="money" runat="server" disabled="disabled" />
                    </label>
                </div>
                
                <div>
                    <label>
                        <span>Total profit</span>
                        <input type="text" id="TotalProfit" preset="money" runat="server" disabled="disabled" />
                    </label>
                </div>
                
            </div>
        </div>
    </div>
    <div class="clear"><input type="button" id="Save" runat="server" onserverclick="SavePool" value="Save" class="save" /></div>
</div>
</asp:Content>
