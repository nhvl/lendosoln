﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.QueryProcessor;
using LendersOffice.Reports;
using LendersOffice.Security;
using System.Web.Services;
using LendersOffice.ObjLib.MortgagePool;
using LendersOfficeApp.los.admin;
using LendersOffice.ObjLib.BatchOperationError;

namespace LendersOfficeApp.los.MortgagePools
{
    public partial class AssignLoans : BaseMortgagePoolPage
    {
        protected override void OnInit(EventArgs e)
        {
            RegisterJsScript("mask.js");
            Tools.Bind_sLT(LoanTypes);
            Tools.Bind_AmortizationT(AmortTypes);
            Tools.Bind_sStatusT_AssignLoans(StatusTypes);

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (PrincipalFactory.CurrentPrincipal.BrokerDB.IsSetAllFieldsOnPoolAssignmentByDefault)
                {
                    IsSetBackEndBasePrice.Checked = true;
                    IsSetBackEndRateLock.Checked = true;
                    IsSetDeliveryFee.Checked = true;
                }

                DeliveryFee.Value = "0.000%";
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            if (!string.IsNullOrEmpty(SearchState.Value.TrimWhitespaceAndBOM()))
            {
                try
                {
                    var oldParams = ObsoleteSerializationHelper.JavascriptJsonDeserializerStruct<CandidateLoanQueryParameters>(SearchState.Value);
                    SetFromParams(oldParams);
                    BindLoans(oldParams);
                }
                catch (ArgumentException)
                {
                    //We don't really care if there's an exception deserializing the thing, just give the user a blank search area.
                }
                catch (InvalidOperationException)
                {
                }
            }
            else
            {
                Tools.SetDropDownListValue(StatusTypes, CurrentPool.SearchMinimumStatusT);
            }

            base.OnLoadComplete(e);
        }

        protected void SearchClick(Object sender, EventArgs e)
        {
            if (CurrentPool.SearchMinimumStatusT != (E_sStatusT)Tools.GetDropDownListValue(StatusTypes))
            {
                CurrentPool.SearchMinimumStatusT = (E_sStatusT)Tools.GetDropDownListValue(StatusTypes);
                CurrentPool.SaveData();
            }
            
            Loans.PageNum = 1;
            RunQuery();
        }

        private void RunQuery()
        {
            var p = GetQueryParams();

            BindLoans(p);

            SearchState.Value = ObsoleteSerializationHelper.JavascriptJsonSerializeStruct(p);
        }

        private void BindLoans(CandidateLoanQueryParameters p)
        {
            var results = MortgagePool.FetchLoanAssignmentCandidates(p, CandidateLoanQuery.DefaultQuery);
            Loans.DataSource = results.Candidates;
            Loans.DataBind();
        }

        private CandidateLoanQueryParameters GetQueryParams()
        {
            E_sLT LoanType = (E_sLT)int.Parse(this.LoanTypes.SelectedValue);
            E_sFinMethT AmortType = (E_sFinMethT)int.Parse(this.AmortTypes.SelectedValue);
            E_sStatusT MinStatus = (E_sStatusT)int.Parse(this.StatusTypes.SelectedValue);
            int? Term_val = null;
            decimal? MinRate_val = null;
            decimal? MaxRate_val = null;
            bool IncludeBackEndRateLockLoans_val;

            int Term_temp;
            if (int.TryParse(this.Term.Value, out Term_temp))
            {
                Term_val = Term_temp;
            }

            decimal Rate_temp;
            if (decimal.TryParse(this.MinRate.Value.Trim('%'), out Rate_temp))
            {
                MinRate_val = Rate_temp;
            }
            if (decimal.TryParse(this.MaxRate.Value.Trim('%'), out Rate_temp))
            {
                MaxRate_val = Rate_temp;
            }

            IncludeBackEndRateLockLoans_val = this.IncludeBackEndRateLock.Checked;

            return new CandidateLoanQueryParameters() 
            {
                LoanType = LoanType,
                AmortType = AmortType,
                MinStatus = MinStatus,
                Term = Term_val,
                MinRate = MinRate_val,
                MaxRate = MaxRate_val,
                IncludeBackEndRateLockLoans = IncludeBackEndRateLockLoans_val
            };
        }
        
        private void SetFromParams(CandidateLoanQueryParameters p)
        {
            LoanTypes.SelectedValue = ((int)p.LoanType).ToString();
            AmortTypes.SelectedValue = ((int)p.AmortType).ToString();
            StatusTypes.SelectedValue = ((int)p.MinStatus).ToString();

            Term.Value = ValueOrEmpty(p.Term);
            MinRate.Value = ValueOrEmpty(p.MinRate) + (p.MinRate.HasValue ? "%" : "");
            MaxRate.Value = ValueOrEmpty(p.MaxRate) + (p.MaxRate.HasValue ? "%" : "");

            IncludeBackEndRateLock.Checked = p.IncludeBackEndRateLockLoans;
        }

        private string ValueOrEmpty<T>(Nullable<T> val) where T: struct
        {
            return val.HasValue ? val.Value.ToString() : "";
        }

        [WebMethod]
        public static object Assign(List<Guid> LoanIds, long PoolId, LoanAssignmentSettings Settings)
        {
            BatchOperationError<LoanError> errorTracker = null;
            new MortgagePool(PoolId).AssignLoans(LoanIds, Settings, out errorTracker);

            if (errorTracker.HasErrors)
            {
                return new 
                {
                     ErrorCacheId = errorTracker.SaveToCache(),
                     ErrorLoanIds = errorTracker.Errors.Select(e => e.LoanId)
                };
            }
            else
            {
                return null;
            }
        }
    }
}