﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/los/MortgagePools/MasterPages/PoolEditor.Master" CodeBehind="ExportVerification.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.ExportVerification" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:Content runat="server" ContentPlaceHolderID="Content">
    <script type="text/javascript">
        function f_export() {
            window.open("Export.aspx?PoolId=<%= AspxTools.HtmlString(PoolId.ToString()) %>","_parent");   
        }
    </script>
    <div style="padding:5px;">
        <input runat='server' id="ExportButton" type='button' value='Export to File' disabled/>
        <br />
        <ml:PassthroughLabel runat="server" id="Message1" />
        <br />
        <br />
        <input runat='server' id="ExportButton2" type='button' value='Export to PDD File' visible="false" disabled/>
        <br />
        <ml:PassthroughLabel runat='server' id="Message2" />
    </div>
</asp:Content>