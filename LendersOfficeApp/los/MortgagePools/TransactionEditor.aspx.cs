﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.CapitalMarkets;
using LendersOffice.Security;
using LendersOffice.Common;
using System.Web.Services;
using DataAccess;

namespace LendersOfficeApp.los.MortgagePools
{
    public partial class TransactionEditor : BaseMortgagePoolPage
    {
        [WebMethod]
        public static IEnumerable<TradeDTO> GetLinkableTrades(TradeDTO linkTo)
        {
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            var trades = Trade.GetTradesByBroker(brokerId);

            return trades.Filter(new TradeDTOFilter(linkTo, brokerId)).ToDTO();
        }

        private class TradeDTOFilter : TradeFilter
        {
            TradeDTO source;
            Guid m_BrokerId;
            Guid descriptionId;
            public TradeDTOFilter(TradeDTO t, Guid brokerid)
            {
                //We want to get everything that's like our trade but in the opposite direction.
                if (t.Direction.HasValue && t.Direction == E_TradeDirection.Buy)
                {
                    t.Direction = E_TradeDirection.Sell;
                }
                else
                {
                    t.Direction = E_TradeDirection.Buy;
                }

                var match = Description.GetDescriptions().FirstOrDefault(a => a.DescriptionName.Equals(t.Description.TrimWhitespaceAndBOM(), StringComparison.InvariantCultureIgnoreCase));
                if (match != default(Description)) descriptionId = match.DescriptionId;

                source = t;
                m_BrokerId = brokerid;
            }

            public override Guid BrokerId
            {
                get { return m_BrokerId; }
            }

            public override TradeStatus? Status
            {
                get { return TradeStatus.Open; }
            }

            public override string TradeNum
            {
                get { return null; }
            }

            public override bool TradeNumPartialMatch
            {
                get { return true; }
            }

            public override E_TradeType? Type
            {
                get { return source.Type; }
            }

            public override E_TradeDirection? Direction
            {
                get { return source.Direction; }
            }

            public override int? Month
            {
                get { return Tools.GetIntFromMonth(source.Month); }
            }

            public override Guid? DescriptionId
            {
                get { return descriptionId; }
            }

            public override string Coupon
            {
                get { return source.Coupon; }
            }

            public override bool FilterTradesInPools
            {
                get { return false; }
            }

            public override DateTime? SettlementDateLowerBound
            {
                get { return null; }
            }

            public override DateTime? SettlementDateUpperBound
            {
                get { return null; }
            }
        }
    }
}
