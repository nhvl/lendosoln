﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;

namespace LendersOfficeApp.los.MortgagePools.MasterPages
{
    public partial class MortgagePoolPopup : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IncludeJavascript();
            IncludeCSS();

            //Make sure we always use the most recent mode in IE.
            //This always has to go first.
            HtmlGenericControl meta = new HtmlGenericControl("meta");
            meta.Attributes.Add("http-equiv", "X-UA-Compatible");
            meta.Attributes.Add("content", "IE=edge");
            this.Page.Header.Controls.AddAt(0, meta);
        }

        //Should refactor this to share most of its stuff with MortgagePoolLayout, but I'm not sure how much they'll differ yet.
        private void IncludeJavascript()
        {
            var CommonPath = Tools.VRoot + "/inc/MortgagePoolCommon.js";

            //Order does matter, jquery needs to come first.
            var scriptsToInclude = new[] { CommonPath };

            //Since we're adding them all at 0, reverse the order so that the first one in the list is added to the front last.
            foreach (var script in scriptsToInclude.Reverse())
            {
                HtmlGenericControl inc = new HtmlGenericControl("script");
                inc.Attributes.Add("type", "text/javascript");
                inc.Attributes.Add("src", script);
                this.Page.Header.Controls.AddAt(0, inc);
            }
        }

        private void IncludeCSS()
        {
            //Order matters for css too, later rules override earlier ones.
            //There should only be one css file referenced by Main, everything else should be 
            //@Import-ed in MortgagePoolCommon.css.
            var CommonCSSPath = Tools.VRoot + "/css/MortgagePoolCommon.css";
            HtmlGenericControl inc = new HtmlGenericControl("link");
            inc.Attributes.Add("rel", "stylesheet");
            inc.Attributes.Add("type", "text/css");
            inc.Attributes.Add("href", CommonCSSPath);
            this.Page.Header.Controls.AddAt(0, inc);
            
        }
    }
}
