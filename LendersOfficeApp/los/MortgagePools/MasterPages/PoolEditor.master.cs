﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.MortgagePool;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.ObjLib.CapitalMarkets;
using System.Text.RegularExpressions;
using LendersOffice.Common.TextImport;

namespace LendersOfficeApp.los.MortgagePools.MasterPages
{
    public partial class PoolEditor : System.Web.UI.MasterPage
    {
        public MortgagePool Pool { get; private set; }
        protected List<IEnumerable<HeaderInfo>> HeaderContents;

        public PoolEditor()
        {
            Pool = new MortgagePool(PoolId);
            HeaderContents = new List<IEnumerable<HeaderInfo>>() { 
                new[] {new HeaderInfo("Pool Number", Pool.PoolNumberByAgency),
                       new HeaderInfo("Pool ID", Pool.InternalId.ToString()),
                       new HeaderInfo("Pool Agency", Pool.AgencyT_rep)},

                new[] {new HeaderInfo("Amortization Type", Pool.AmortizationT_rep),
                       new HeaderInfo("Security Rate", Pool.SecurityR_rep)},

                new[] {new HeaderInfo("Number of Loans", Pool.LoanCount.ToString()),
                       new HeaderInfo("Total Current Balance", Pool.TotalCurrentBalance_rep),
                       new HeaderInfo("Total Open Balance", Pool.OpenBalance_rep)}
            };
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var basepage = Page as BasePage;
            if (basepage != null)
            {
                basepage.RegisterJsScript("MortgagePoolMainEditor.js");
            }

            Summary.DataSource = HeaderContents;
            Summary.DataBind();

            SetupLinks(RemoveTradeId(Request.Url.Query));
        }

        private void SetupLinks(string baseQuery)
        {
            string currentPage = Request.Url.Segments[Request.Url.Segments.Length - 1];
            if (PageUrlToInfo.ContainsKey(currentPage))
            {
                PageUrlToInfo[currentPage].IsCurrent = true;
            }

            var pagesList = PageUrlToInfo.Select(a => {
                a.Value.Url = a.Key + baseQuery;
                return a.Value;
            });

            pagesList = pagesList.Concat(CreateTradeLinks(Pool.GetTrades(), baseQuery));

            PageLinks.DataSource = pagesList;
            PageLinks.DataBind();
        }

        private string TradeFormat = "Trade # {0}";
        private IEnumerable<MiniPageInfo> CreateTradeLinks(List<Trade> trades, string baseQuery)
        {
            Guid currentTradeId = RequestHelper.GetGuid("TradeId", Guid.Empty);
            foreach (var t in trades)
            {
                yield return new MiniPageInfo() { 
                    Name = string.Format(TradeFormat, t.TradeNum_rep),
                    Url = "TradeEditor.aspx" + AddTradeId(baseQuery, t.TradeId),
                    IsCurrent = currentTradeId == t.TradeId,
                    CssClass = "trade_" + t.TradeId
                };
            }
        }

        private string RemoveTradeId(string query)
        {
            var parts = HttpUtility.ParseQueryString(query.ToLower());
            parts.Remove("tradeid");
            return "?" + parts.ToString();
        }

        private string AddTradeId(string query, Guid tradeId)
        {
            var parts = HttpUtility.ParseQueryString(query);
            parts.Add("TradeId", tradeId.ToString());
            return "?" + parts.ToString();
        }

        protected void SummaryBound(object sender, RepeaterItemEventArgs e)
        {
            var info = (IEnumerable<HeaderInfo>)e.Item.DataItem;
            var dataRepeater = (Repeater)e.Item.FindControl("SummaryData");
            var summaryRepeater = (Repeater)e.Item.FindControl("SummaryLabels");

            dataRepeater.DataSource = info;
            dataRepeater.DataBind();

            summaryRepeater.DataSource = info;
            summaryRepeater.DataBind();
        }

        public int PoolId
        {
            get
            {
                return RequestHelper.GetInt("PoolId");
            }
        }

        //Move this out to an external file if the editor gets more complex
        //Note that it currently doesn't highlight a link to something in a subfolder
        protected Dictionary<string, MiniPageInfo> PageUrlToInfo = new Dictionary<string, MiniPageInfo>()
        {
            {"PoolDetails.aspx", new MiniPageInfo() { Name = "Pool Details"}},
            {"LoansInPool.aspx", new MiniPageInfo() { Name = "Loans in Pool"}},
            {"AssignLoans.aspx", new MiniPageInfo() { Name = "Assign Loans"}},
            {"ExportVerification.aspx", new MiniPageInfo() { Name = "Export"}},
            {"Delivery.aspx", new MiniPageInfo() { Name = "Delivery"}},
            {"PoolNotes.aspx", new MiniPageInfo() { Name = "Notes"}},
            {"Trades.aspx", new MiniPageInfo() { Name = "Trades"}},
        };

        protected class HeaderInfo
        {
            public string Label;
            public string Value;
            public HeaderInfo(string label, string value)
            {
                Label = label;
                Value = value;

                if (string.IsNullOrEmpty(Label))
                {
                    Label = "Unknown";
                    Tools.LogError("Unknown label in Mortgage Pools header, value is: " + value);
                }

                if (string.IsNullOrEmpty(Value))
                {
                    Value = "Unknown";
                    Tools.LogError("Unknown value in Mortgage Pools header, label is: " + label);
                }
            }

            //For serialization
            public HeaderInfo() { }
        }

        protected class MiniPageInfo
        {
            public string Name;
            public string Url;
            public bool IsCurrent;
            public string CssClass;
        }
    }
}
