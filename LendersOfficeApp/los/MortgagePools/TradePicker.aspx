﻿<%@ Page Title="" Language="C#" MasterPageFile="~/los/MortgagePools/MasterPages/MortgagePoolPopup.Master" AutoEventWireup="true" CodeBehind="TradePicker.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.TradePicker" %>
<%@ Import Namespace="LendersOffice.ObjLib.CapitalMarkets" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc" TagName="TradePortlet" Src="~/los/Portlets/TradePortlet.ascx"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HtmlHead" runat="server">
<script type="text/javascript">
    jQuery(function($) {

        $(window).on('TradeSelected', function(event, tradeId, poolId) {
            if (poolId) {
                alert("This trade is already associated with a pool");
                return;
            }
            window.opener.TradeAssociated(tradeId);
            onClosePopup();
        });
    });
</script>
<style type="text/css">
#TradePortletMain table
{
    border-collapse: collapse;
    border: solid 0px black;
    background-color: black;
}

#TradePortletMain table td, #TradePortletMain table th
{
    border: solid 0px black;
}
</style>
<link rel="Stylesheet" type="text/css" href="../../css/MortgagePoolTable.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="server">
    Pick trade
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <uc:TradePortlet id="TradePortlet" runat="server" SelectTradeOnly="true" HideCreateTrade="true" HideDeleteLinks="true"></uc:TradePortlet>
</asp:Content>
