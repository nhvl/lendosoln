﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.QueryProcessor;
using LendersOffice.Reports;
using LendersOffice.Security;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;
using DataAccess;
using LendersOffice.ObjLib.MortgagePool;
using System.Web.Services;

namespace LendersOfficeApp.los.MortgagePools
{
    public partial class LoansInPool : BaseMortgagePoolPage
    {
        static string queryXml = @"
<Query xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <Label>
    <Title>FHA Loan Pool Summary</Title>
    <SavedBy>Rodrigo Noronha</SavedBy>
    <SavedOn>2012-08-01T10:11:23.861954-07:00</SavedOn>
    <SavedId>387395dc-017b-4644-ad29-d9339ffc81eb</SavedId>
    <Version>4</Version>
  </Label>
  <Columns>
    <Items>
      <Column Type=""Str"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>sLNm</Id>
        <Name>Loan Number</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Str"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>sAgencyCaseNum</Id>
        <Name>Agency Case Number</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Str"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>aBLastNm</Id>
        <Name>Borr Last Name</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Dec"" Kind=""Pct"" Show=""true"">
        <Format>N3</Format>
        <Id>sNoteIR</Id>
        <Name>Note Rate</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Enu"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>sFinMethT</Id>
        <Name>Amortization Type</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Str"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>sLpTemplateNm</Id>
        <Name>Loan Program Name</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Dec"" Kind=""Csh"" Show=""true"">
        <Format>C</Format>
        <Id>sFinalLAmt</Id>
        <Name>Total Loan Amount</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Enu"" Kind=""Txt"" Show=""true"">
        <Format />
        <Id>sStatusT</Id>
        <Name>Loan Status</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Dtm"" Kind=""Cal"" Show=""true"">
        <Format>M/d/yyyy</Format>
        <Id>sSchedFundD</Id>
        <Name>Scheduled Funding</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
      <Column Type=""Dtm"" Kind=""Cal"" Show=""true"">
        <Format>M/d/yyyy</Format>
        <Id>sFundD</Id>
        <Name>Funded Date</Name>
        <Functions>
          <Items />
        </Functions>
      </Column>
    </Items>
  </Columns>
  <Sorting>
    <Items />
  </Sorting>
  <GroupBy>
    <Items />
  </GroupBy>
  <Relates Type=""An"">
    <Items />
  </Relates>
  <Filters Type=""An"">
  </Filters>
  <Options>
    <Items />
  </Options>
  <Details>This report is empty.  Customize it by adding fields, conditions, status events with ranges, etc. and save it with a new name.</Details>
  <Id>fcca4a09-233f-46f9-a981-ab7d5733650b</Id>
  <DisplayTablesOnSeparatePages>false</DisplayTablesOnSeparatePages>
</Query>
";
        protected void Page_Load(object sender, EventArgs e)
        {
            Query poolInfo = queryXml;
            LoanReporting reportRunner = new LoanReporting();
            poolInfo.RestrictToLoanIds = CurrentPool.FetchLoansInPool();

            var report = reportRunner.RunReport(poolInfo, BrokerUserPrincipal.CurrentPrincipal, true, DataAccess.E_ReportExtentScopeT.SpecificLoans);
            Loans.DataSource = report;
            Loans.DataBind();
        }

        [WebMethod]
        public static void Remove(List<Guid> LoanIds, long PoolId)
        {
            new MortgagePool(PoolId).RemoveLoans(LoanIds);
        }
    }
}