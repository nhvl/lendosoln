﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TradeTable.ascx.cs" Inherits="LendersOfficeApp.los.Portlets.TradeTable" %>
<%@ Import Namespace="LendersOffice.ObjLib.CapitalMarkets" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<div id="TradeTableMain">
    <asp:Repeater runat="server" ID="TradeGroups" OnItemDataBound="TradeGroups_OnItemDataBound">
        <ItemTemplate>
            <div class="group-header"><ml:EncodedLabel runat="server" ID="Header"/></div>
            <table width="100%" border="0" cellSpacing="1" cellPadding="2" style="visibility: hidden;">
                <thead>
                    <tr>
                        <th class="action">Trade&nbsp;#</th>
                        <th class="action">Security</th>
                        <th class="action">Type</th>
                        <th class="action">Month</th>
                        <th class="action">Coupon</th>
                        <th class="action">Buy Amount</th>
                        <th class="action">Sell Amount</th>
                        <th class="action">Price</th>
                        <th class="action">Associated Loans</th>
                        <th class="action">Settlement Date</th>
                        <th class="action">Notification Date</th>
                        <th class="action">Dealer/Investor</th>
                        <th class="action">Trade Date</th>
                        <th class="action expanded">Gain/Loss</th>
                        <th class="action expanded">Expense</th>
                        <th class="delete"><!--Delete Column --></th>
                    </tr>
                </thead>
                <tbody>
            <asp:Repeater runat="server" ID="TradesForDealerInvestor">
                <ItemTemplate>
                    <tr class="GridItem">
                        <td class="trade-num action">
                            <%# AspxTools.HtmlString(((Trade)Container.DataItem).TradeNum_rep) %>
                            <input type="hidden" class="pool-id" value="<%# AspxTools.HtmlString(((Trade)Container.DataItem).AssociatedPoolId) %>" />
                            <input type="hidden" class="trade-id" value="<%# AspxTools.HtmlString(((Trade)Container.DataItem).TradeId) %>" />
                        </td>
                        <td class="security"><%# AspxTools.HtmlString(((Trade)Container.DataItem).Description_rep) %></td>
                        <td class="type"><%# AspxTools.HtmlString(((Trade)Container.DataItem).Type_rep) %></td>
                        <td class="month"><%# AspxTools.HtmlString(((Trade)Container.DataItem).Month_rep) %></td>
                        <td class="coupon"><%# AspxTools.HtmlString(((Trade)Container.DataItem).Coupon_rep) %></td>
                        <td class="buy-amount"><%# AspxTools.HtmlString(((Trade)Container.DataItem).BuyAmount_rep) %></td>
                        <td class="sell-amount"><%# AspxTools.HtmlString(((Trade)Container.DataItem).SellAmount_rep) %></td>
                        <td class="price"><%# AspxTools.HtmlString(((Trade)Container.DataItem).Price_rep) %></td>
                        <td class="associated-loan-value"><%# AspxTools.HtmlString(((Trade)Container.DataItem).AllocatedLoans_rep) %></td>
                        <td class="settlement-date"><%# AspxTools.HtmlString(((Trade)Container.DataItem).SettlementDate_rep) %></td>
                        <td class="notification-date"><%# AspxTools.HtmlString(((Trade)Container.DataItem).NotificationDate_rep) %></td>
                        <td class="dealer-investor"><%# AspxTools.HtmlString(((Trade)Container.DataItem).DealerInvestor) %></td>
                        <td class="trade-date"><%# AspxTools.HtmlString(((Trade)Container.DataItem).TradeDate_rep) %></td>
                        <td class="gain-loss expanded"><%# AspxTools.HtmlString(((Trade)Container.DataItem).TotalGainLoss_rep) %></td>
                        <td class="expense expanded"><%# AspxTools.HtmlString(((Trade)Container.DataItem).TotalExpense_rep) %></td>
                        <td class="delete">
                            <span class="action delete">delete</span>
                            <input type="hidden" class="tradeId" value="<%# AspxTools.HtmlString(((Trade)Container.DataItem).TradeId) %>" />
                            <input type="hidden" class="hasTransactions" value="<%# AspxTools.HtmlString(HasTransactions((Trade)Container.DataItem)) %>" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
                    <tr class="totals static expanded">
                        <td>Totals:</td>
                        <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                        <td class="gain-loss"><ml:EncodedLabel runat="server" ID="TotalGainLoss"/></td>
                        <td class="expense"><ml:EncodedLabel runat="server" ID="TotalExpense"/></td>
                        <td class="delete"></td>
                    </tr>
                </tbody>
            </table>
        </ItemTemplate>
    </asp:Repeater>
</div>
