﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.CapitalMarkets;
using DataAccess;
using System.Collections;

namespace LendersOfficeApp.los.Portlets
{
    public partial class TradeTable : System.Web.UI.UserControl
    {
        public bool SelectTradeOnly { get; set; }
        public bool HideDeleteLinks { get; set; }
        public bool DisplayExpanded { get; set; }
        public IEnumerable<TradeGroup> DataSource { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var basepage = Page as BasePage;
            if (basepage == null)
            {
                throw new Exception("Cannot use the Trade Table on something that is not a BasePage");
            }

            if (!this.Visible) return;
            basepage.EnableJqueryMigrate = false;
            basepage.RegisterCSS("TradeTable.css");
            basepage.RegisterJsScript("jquery.tablesorter.min.js");
            basepage.RegisterJsScript("jquery.tablesorter.staticrow.min.js");
            basepage.RegisterJsScript("TradeTable.js");

            if (SelectTradeOnly) Page.ClientScript.RegisterHiddenField("SelectTradeOnly", "true");
            if (HideDeleteLinks) Page.ClientScript.RegisterHiddenField("HideDeleteLinks", "true");
            if (DisplayExpanded) Page.ClientScript.RegisterHiddenField("DisplayExpanded", "true");
        }

        protected bool HasTransactions(Trade t)
        {
            return t.Transactions.Any();
        }

        public override void DataBind()
        {
            // If there aren't any matching tradegroups, we still  want to display
            // a single table with the no trades found message.
            if (!DataSource.Any())
            {
                DataSource = new[] { new TradeGroup("", new List<Trade>()) };
            }

            TradeGroups.DataSource = DataSource;
            TradeGroups.DataBind();

            base.DataBind();
        }

        protected void TradeGroups_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            var header = args.Item.FindControl("Header") as Label;
            var trades = args.Item.FindControl("TradesForDealerInvestor") as Repeater;
            var totalGainLoss = args.Item.FindControl("TotalGainLoss") as Label;
            var totalExpense = args.Item.FindControl("TotalExpense") as Label;

            var tradeGroup = args.Item.DataItem as TradeGroup;

            if (header == null || trades == null || tradeGroup == null ||
                totalGainLoss == null || totalExpense == null) return;

            header.Text = tradeGroup.Header;
            trades.DataSource = tradeGroup.Trades;
            trades.DataBind();

            var convert = new LosConvert();
            totalGainLoss.Text = convert.ToMoneyString(tradeGroup.Trades.Sum(t => t.TotalGainLoss), FormatDirection.ToRep);
            totalExpense.Text = convert.ToMoneyString(tradeGroup.Trades.Sum(t => t.TotalExpense), FormatDirection.ToRep);
        }
    }
}
