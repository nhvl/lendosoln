﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.QueryProcessor;
using LendersOffice.Common;
using LendersOffice.Reports;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Security;
using System.ComponentModel;

namespace LendersOfficeApp.los.MortgagePools.Controls
{
    public partial class LoanTable : System.Web.UI.UserControl
    {
        public Report DataSource;

        /// <summary>
        /// Alternative method for setting the data source, if you want to only use specific rows. DataSource takes priority if set.
        /// </summary>
        /// <param name="Rows"></param>
        /// <param name="ColumnNames"></param>
        public void SetRows(List<Row> Rows, IEnumerable<string> ColumnNames)
        {
            data = Rows;
            columnNames = ColumnNames;
        }

        private List<Row> data;
        private IEnumerable<string> columnNames;
        public bool ShowLoanEditor {get; set;}
        public bool ShowLoanViewer { get; set; }
        public bool ShowTaskEditor { get; set; }
        public bool ShowSelectionCol {get; set;}
        public bool EnableTablesorter { get; set; }
        public string NoLoansText {get; set;}
        public string ReportErrorText {get; set;}
        public string TableId { get; set; }
        protected string NoDataText {get; set;}
        public int RowsPerPage { get; set; }

        public LoanTable()
        {
            NoLoansText = "No loans";
            ReportErrorText = "There was an error, please try again";
            NoDataText = "An unexpected error occurred, please try again";
            RowsPerPage = 20;
        }

        protected override void OnInit(EventArgs e)
        {
            var basePage = Page as BasePage;
            if (basePage == null)
            {
                throw new NotImplementedException("This control was designed to be used on pages which extend LendersOffice.Common.BasePage");
            }

            if (ShowTaskEditor) basePage.RegisterJsGlobalVariables("TaskEditorUrl", GetTaskEditorUrl());
            if (ShowLoanEditor) basePage.RegisterJsGlobalVariables("LoanEditorUrl", GetLoanEditorUrl());
            if (ShowLoanViewer) basePage.RegisterJsGlobalVariables("LoanViewerUrl", GetLoanViewerUrl());
            basePage.RegisterJsGlobalVariables("ShowSelectionCol", ShowSelectionCol);
            basePage.RegisterJsGlobalVariables("EnableTablesorter", EnableTablesorter);

            basePage.RegisterJsScript("jquery.tablesorter.min.js");
            basePage.RegisterJsScript("MortgagePoolLoanTable.js");
            basePage.RegisterCSS("MortgagePoolLoanTable.css");

            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }

        int? _pageNum = null;
        public int PageNum
        {
            get 
            {
                if (_pageNum.HasValue) return _pageNum.Value;
                _pageNum = RequestHelper.GetInt("Page", 1);
                return _pageNum.Value;
            }
            set
            {
                _pageNum = value;
            }
        }

        protected string GetPageNumberClass(int num)
        {
            if (PageNum == num) return "";
            else return "Action";
        }

        public override void DataBind()
        {
            if (DataSource != null)
            {
                data = DataSource.Flatten();
                columnNames = from Column a in DataSource.Columns.Items select a.Name;
            }

            if (data == null)
            {
                Tools.LogWarning("Loan report was null when user " + BrokerUserPrincipal.CurrentPrincipal.LoginNm + " tried to look at a loan table. Request is: [" + Request.Url.ToString() + "]");
                NoData(NoDataText);
                return;
            }

            if (data.Count == 0)
            {
                NoData(NoLoansText);
                return;
            }

            int numPages = (int)Math.Ceiling(((double)data.Count / (double)RowsPerPage));
            int usePageNum = PageNum > numPages ? numPages : PageNum;

            PaginationPlaceholder.Visible = numPages > 1;

            PageLinks.DataSource = Enumerable.Range(1, numPages);
            PageLinks.DataBind();

            MainContent.Visible = true;
            NoLoansMessage.Visible = false;

            ColumnHeaders.DataSource = columnNames;
            ColumnHeaders.DataBind();

            Rows.DataSource = data.Skip((usePageNum - 1) * RowsPerPage).Take(RowsPerPage);
            Rows.DataBind();

            base.DataBind();
        }

        protected void NoData(string message)
        {
            NoDataText = message;
            MainContent.Visible = false;
            NoLoansMessage.Visible = true;
        }

        protected void RowsBound(object sender, RepeaterItemEventArgs e)
        {
            var row = (Row)e.Item.DataItem;
            var entryRepeater = (Repeater)e.Item.FindControl("RowEntries");
            var idHolder = (HtmlInputHidden)e.Item.FindControl("LoanId");

            idHolder.Value = row.Key.ToString();

            entryRepeater.DataSource = from Value a in row.Items select a.Text;
            entryRepeater.DataBind();
        }

        private string GetTaskEditorUrl()
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            if (brokerDB.IsUseNewTaskSystem)
            {
                return Tools.VRoot + "/newlos/Tasks/TaskEditorV2.aspx?IsTemplate=False&loanId=";
            }
            else
            {
                return Tools.VRoot + "/los/reminders/TaskEditor.aspx?IsTemplate=False&loanId=";
            }
        }

        private string GetLoanEditorUrl()
        {
            return Tools.VRoot + "/newlos/loanapp.aspx?loanid=";
        }

        private string GetLoanViewerUrl()
        {
            return Tools.VRoot + "/los/view/PrintPreviewFrame.aspx?loanid=";
        }
    }
}