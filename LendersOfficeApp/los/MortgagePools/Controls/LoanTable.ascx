﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoanTable.ascx.cs" Inherits="LendersOfficeApp.los.MortgagePools.Controls.LoanTable" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<asp:PlaceHolder ID="MainContent" runat="server">
    <div <%= TableId == null ? AspxTools.HtmlString("") : "id='" + AspxTools.HtmlString(TableId) +"_div'"%>>
    <asp:PlaceHolder ID="PaginationPlaceholder" runat="server">
    <span id="Pagination"> Page 
    <asp:Repeater ID="PageLinks" runat="server">
        <ItemTemplate>
            <span class="<%#AspxTools.HtmlString(GetPageNumberClass((int)Container.DataItem))%>"><%# AspxTools.HtmlString(Container.DataItem.ToString()) %></span>
        </ItemTemplate>
    </asp:Repeater>
    </asp:PlaceHolder>
    </span>
    <table class="Loans" <%= TableId == null ? AspxTools.HtmlString("") : "id='" + AspxTools.HtmlString(TableId) +"'"%> cellspacing="0px">
        <thead>
            <tr class="GridHeader">
                <th class="LoanSelectionCol"></th>
                <asp:Repeater runat="server" ID="ColumnHeaders">
                    <ItemTemplate>
                        <th>
                            <%# AspxTools.HtmlString((string)Container.DataItem) %>
                        </th>
                    </ItemTemplate>
                </asp:Repeater>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater runat="server" ID="Rows" OnItemDataBound="RowsBound">
                <ItemTemplate>
                    <tr>
                        <td class="LoanSelectionCol" valign="top">
                            <input type="hidden" runat="server" id="LoanId" class="LoanId" />
                            <input type="checkbox" class="LoanSelected" />
                        </td>
                        <asp:Repeater runat="server" ID="RowEntries">
                            <ItemTemplate>
                                <td>
                                    <span>
                                        <%# AspxTools.HtmlString((string)Container.DataItem) %></span>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="NoLoansMessage" runat="server" Visible="false">
    <div class="NoLoans">
        <span><%= AspxTools.HtmlString(NoDataText)%></span>
    </div>
</asp:PlaceHolder>
