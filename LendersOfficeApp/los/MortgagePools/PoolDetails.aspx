﻿<%@ Page Title="" Language="C#" MasterPageFile="~/los/MortgagePools/MasterPages/PoolEditor.Master" AutoEventWireup="true" CodeBehind="PoolDetails.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.PoolDetails" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        input {
            width: 80px;
        }

            input[type="checkbox"], input[type="radio"] {
                width: auto;
            }

            input.Date {
                width: 60px;
            }

        select {
            width: 85px;
        }

        div.Col {
            float: left;
            margin-right: 10px;
            width: 250px;
        }

        div.Section {
            overflow: auto;
            margin-bottom: 20px;
        }

        #MasterPageContent div label {
            width: 125px;
            display: inline-block;
        }

        #MasterPageContent div.Narrow label {
            width: 75px;
        }

        div.Col div, #PoolProperties div {
            margin-bottom: 3px;
        }

        #FannieStructure div label {
            width: 150px;
        }

        #PoolProperties div.Wide label {
            vertical-align: bottom;
            width: 500px;
        }

        #PoolProperties input {
            margin-left: 15px;
            vertical-align: top;
        }

        input.IssueTypeCode {
            width: 25px;
        }

        input.PoolTypeCode {
            width: 40px;
        }

        #PoolStats div.Middle input {
            width: 50px;
        }

        #AmortizationMethod select {
            width: 110px;
        }

        input.Term, input.TermMirror {
            width: 20px;
        }

        input.Wide {
            width: 100px;
        }

        .GinnieOnly, .FannieOnly,
        .ARMOnly, .WeightedOnly, .ARMAndWeightedOnly, .ARMAndStatedOnly {
            display: none;
        }


        #PoolDetails.Fannie div.FannieOnly,
        #PoolDetails.AccrualWeighted div.WeightedOnly,
        #PoolDetails.ARM div.ARMOnly,
        #PoolDetails.ARM.AccrualWeighted div.ARMAndWeightedOnly,
        #PoolDetails.ARM.AccrualStated div.ARMAndStatedOnly {
            display: block;
        }

        #PoolDetails.Fannie .NotFannie {
            display: none;
        }

        #PoolDetails.Ginnie .NotGinnie {
            display: none;
        }

        #PoolDetails.Fannie div.Col.Left, #PoolDetails.Fannie div.Col.Wide {
            width: 300px;
        }

        #PoolDetails.Ginnie div.GinnieOnly {
            display: block;
        }

        #Indicators div.radio-replacer {
            display: inline;
        }

            #Indicators div.radio-replacer label {
                width: 25px;
            }

        #PoolStructure select {
            width: 190px;
        }

        div#FannieSecurityInfo, div#FannieSecurityInfo div {
            width: 350px;
            margin-right: 10px;
        }

        div.Wide select {
            width: 130px;
        }

        input.Narrow {
            width: 25px;
        }
    </style>

    <script type="text/javascript">
        jQuery(function ($) {

            function empty(v) {
                return $.trim(v) == '';
            }

            function toNum(v) {
                return +v.replace(/[^0-9.]/, "");
            }

            function outOfBounds(num, min, max) {
                if (num > max) return max;
                if (num < min) return min;
                return false;
            }

            function offStep(num, stepSize) {
                var rem = num % stepSize;
                if (rem == 0) return false;

                return stepSize - rem + num;
            }

            function accrualValidator(name) {
                return function (val, $elem) {
                    if (empty(val)) return true;
                    var num = toNum(val);
                    var off = offStep(num, 0.125);
                    if (off) {
                        $elem.val(off + "%");
                        return name + " must be divisible by 0.125";
                    }
                }
            }

            function lessThan100Validator(name) {
                return function (val, $elem) {
                    if (empty(val)) return true;
                    var num = toNum(val);
                    var oob = outOfBounds(num, -100, 100);
                    if (oob) {
                        $elem.val(oob).blur();
                        return name + " must be less than 100%";
                    }
                }
            }

            var validators = [{
                elem: $('.Ownership'),
                valid: function (val, $elem) {
                    if (empty(val)) return true;
                    var num = toNum(val);
                    var oob = outOfBounds(num, 50, 100);

                    if (oob) {
                        $elem.val(oob + "%");
                        return "Ownership % must be between 50% and 100%";
                    }
                    var off = offStep(num, 5);

                    if (off) {
                        $elem.val(off + "%");
                        return "Ownership % must be divisible by 5";
                    }
                }
            }, {
                elem: $('.MaxAccrual'),
                valid: accrualValidator("Maximum Accrual Rate")
            }, {
                elem: $('.MinAccrual'),
                valid: accrualValidator("Minimum Accrual Rate")
            }, {
                elem: $('.RemittanceD'),
                valid: function (val, $elem) {
                    if (empty(val)) return true;
                    var num = toNum(val);
                    var oob = outOfBounds(num, 1, 31);

                    if (oob) {
                        $elem.val(oob);
                        return "Remittance Day must be between 1 and 31";
                    }
                }
            }, {
                elem: $('.FixedServicingFee'),
                valid: lessThan100Validator("Fixed Servicing Fee %")
            }, {
                elem: $('.MBSMargin'),
                valid: lessThan100Validator("MBS Margin")
            }, {
                elem: $('.SecurityRate'),
                valid: lessThan100Validator("Security Rate")
            }];

            $.each(validators, function (idx, v) {
                v.elem.on('blur', function () {
                    if (!v) return;
                    var result = v.valid(v.elem.val(), v.elem);
                    if (jQuery.type(result) == "string") alert(result);
                });
            });


            //These elements are logically checkboxes, but we want them to be radio buttons in the ui.
            $('input:checkbox').each(function () {
                var RadioY = $(MakeRadioButton(this.id, 'Yes'));
                var RadioN = $(MakeRadioButton(this.id, 'No'));
                if ($(this).is('.NY')) {
                    $(this).after(RadioY).after(RadioN);
                }
                else {
                    $(this).after(RadioN).after(RadioY);
                }

                var cb = this;
                (cb.checked ? RadioY.find('input') : RadioN.find('input')).attr('checked', 'checked');

                RadioN.change(function () {
                    $(cb).removeAttr('checked');
                });
                RadioY.change(function () {
                    $(cb).attr('checked', 'checked');
                });

                $(this).hide();
            });

            $('input[preset="date"]').css({ 'width': '' }).addClass('Date');
            $('#PoolStats div.Middle input').attr('readonly', 'true');
            $('#PoolProperties').delegate('label.Radio', 'click', function () {
                $(this).prev('input').click();
                $(this).prev('input').change();
            });

            var agencyMap = {
                1: "Fannie",
                3: "Ginnie"
            };

            $('.AgencyT').change(function () {
                $('#PoolDetails').removeClass('Ginnie Fannie');
                $('#PoolDetails').addClass(agencyMap[$(this).val()]);
            }).change();

            $('select.AmortizationT').change(function () {
                if ($(this).val() == 1 /*ARM*/) {
                    $('#PoolDetails').addClass("ARM");
                }
                else {
                    $('#PoolDetails').removeClass("ARM");
                }
            }).change();

            $('select.AccrualRateStructureT').change(function () {
                if ($(this).val() == 0 /*Stated*/) {
                    $('#PoolDetails').removeClass("AccrualWeighted").addClass("AccrualStated");
                }
                else { /*Weighted avg*/
                    $('#PoolDetails').removeClass("AccrualStated").addClass("AccrualWeighted");
                }
            }).change();

            $('.Clone').each(function () {
                var $dest = $(this);
                var $source = $($dest.data('cloned'));
                $dest.append($source.children().clone().removeClass().addClass('FannieOnly'));
            });

            $('#GovernmentBondFinance input').change(function () {
                if ($('#GovernmentBondFinance input:checked').val() == 'Yes') {
                    $(<%=AspxTools.JsGetElementById(GovernmentBondFinanceAdditionalOptions)%>).show();
                }
                else
                {
                    $(<%=AspxTools.JsGetElementById(GovernmentBondFinanceAdditionalOptions)%>).hide();
                }
            });

            function setupFeatureCodes() {
                var codes = JSON.parse($('.FeatureJSON').val() || '[]');
                if (!$.isArray(codes)) codes = [];
                while (codes.length < 3) codes.push('');

                var codesInput = ['<input class="Narrow Feature" type="text" value="'];
                var len = codes.length;
                for (var i = 0; i < len; i++) {
                    codesInput.push(codes[i]);
                    codesInput.push('" /> <input class="Narrow Feature" type="text" value="');
                }
                codesInput.pop();
                codesInput.push('" />');

                var inputs = $(codesInput.join('')).on('change', function () {
                    var features = [];
                    $('.Feature').each(function () {
                        features.push(this.value);
                    });
                    $('.FeatureJSON').val(JSON.stringify(features));
                });

                $('#FeatureCodes label').after(inputs);




                // OPM 145924 - Add Validation for PoolId Uniqueness
                var errorMessage = null;

                $('.PoolId').on('blur', function () {
                    VerifyPoolId();
                });

                function VerifyPoolId() {
                    errorMessage = "Please wait while Pool ID is checked";
                    $('.PoolId').siblings('.validation').hide();

                    var poolId = $.trim($('.PoolId').val());

                    if (poolId == '') {
                        errorMessage = "Please enter a Pool ID";
                        return false;
                    }

                    if (poolId == $('#OriginalPoolId').val()) {
                        errorMessage = null;
                        return true;
                    }

                    var DTO = {
                        PoolId: +poolId
                    }

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: 'PoolDetails.aspx/IsPoolIdUnique',
                        data: JSON.stringify(DTO),
                        dataType: 'json',

                        success: function (msg) {
                            if (!msg.d) {
                                $('.PoolId').siblings('.validation').show();
                                errorMessage = "PoolId is already in use, please choose another.";
                            }
                            else {
                                errorMessage = null;
                            }
                        },

                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("An unexpected error happened, please try again later");
                        }
                    });
                }

                LQBMortgagePools.BeforePostback(function (target) {
                    if (/SaveButton$/.test(target)) {
                        var currentPoolId = $.trim($('.PoolId').val());
                        if (errorMessage || currentPoolId == '') {
                            alert(errorMessage || "Please enter a Pool ID");
                            ReEnableSaveButton();
                            return false;
                        }
                    }
                });

            }

            setupFeatureCodes();

            // Do error checking down here to allow the UI to be fully updated when alerts display.
            var saveError = $("#SaveError").length > 0;
            var poolNumError = $("#PoolNumberError").length > 0;
            var loc = window.location.href;
            var errorString = "";

            if (saveError) {
                errorString += "There was an error saving your changes. ";
                loc = loc.replace("&SaveError=t", "");
            }
            if (poolNumError) {
                errorString += "Pool number " + $("#PoolNumberError").val() + " is already in use.";
                loc = loc.replace("&PoolNumberError=t&PoolNumber=" + encodeURIComponent($("#PoolNumberError").val()), "");
            }

            if (saveError || poolNumError) {
                alert(errorString);
                window.location.replace(loc);
            }

            function MakeRadioButton(uniqueId, val) {
                var retParts = [];
                retParts.push('<div class="radio-replacer"><input type="radio" name="');
                retParts.push(uniqueId);
                retParts.push('_radio" value="');
                retParts.push(val);
                retParts.push('"/><label class="Radio">');
                retParts.push(val);
                retParts.push('</label></div>');

                return retParts.join('');
            }

            LQBMortgagePools.InitMirror();
        });

        function DisableAndNotify() {
            jQuery('.SaveButton').val("Please wait...").attr('disabled', 'disabled');
        }

        function ReEnableSaveButton() {
            jQuery('.SaveButton').val("Save").removeAttr('disabled');
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div id="PoolDetails">
        <input type="hidden" class="FeatureJSON" id="FeatureJSON" runat="server" />
        <div class="Section" id="PoolStatusDates">
            <div class="Col Left">
                <div>
                    <label>Pool Opened Date</label>
                    <ml:DateTextBox ID="OpenedD" runat="server" Width="75" preset="date" CssClass="mask" /></div>
                <div>
                    <label>Pool Delivered Date</label>
                    <ml:DateTextBox ID="DeliveredD" runat="server" Width="75" preset="date" CssClass="mask" /></div>
                <div>
                    <label>Pool Closed Date</label>
                    <ml:DateTextBox ID="ClosedD" runat="server" Width="75" preset="date" CssClass="mask" /></div>
            </div>
            <div class="Col Middle">
                <div>
                    <label>Pool Issue Date</label>
                    <ml:DateTextBox ID="IssueD" runat="server" Width="75" preset="date" CssClass="mask" /></div>
                <div>
                    <label>Pool Settlement Date</label>
                    <ml:DateTextBox ID="SettlementD" runat="server" Width="75" preset="date" CssClass="mask" /></div>
                <div>
                    <label>Security Book-entry Date </label>
                    <ml:DateTextBox ID="BookEntryD" runat="server" Width="75" preset="date" CssClass="mask" /></div>
            </div>
            <div class="Col Right">
                <div>
                    <label>Initial Payment Date</label>
                    <ml:DateTextBox ID="InitialPmtD" runat="server" Width="75" preset="date" CssClass="mask" /></div>
                <div>
                    <label>Unpaid Balance Date</label>
                    <ml:DateTextBox ID="UnpaidBalanceD" runat="server" Width="75" preset="date" CssClass="mask" /></div>
                <div>
                    <label>Pool Maturity Date</label>
                    <ml:DateTextBox ID="MaturityD" runat="server" Width="75" preset="date" CssClass="mask" /></div>
            </div>
        </div>
        <div class="Section">
            <div class="Col Left">
                <div>
                    <label>Pool ID</label>
                    <input type="text" id="InternalId" runat="server" preset="alphanumeric" preset_len="36" class="PoolId" />
                    <span class="validation">In use</span></div>
                <div>
                    <label>Pool Agency</label>
                    <asp:DropDownList ID="AgencyT" runat="server" class="AgencyT"></asp:DropDownList></div>
            </div>
        </div>
        <div class="Section" id="PoolInfo">
            <div class="Col Left">
                <div>
                    <label>Base Pool Number</label>
                    <input type="text" id="BasePoolNumber" runat="server" preset="alphanumeric" preset_len="6" /></div>
                <div class="NotGinnie">
                    <div>
                        <label>Pool Prefix </label>
                        <input type="text" id="PoolPrefix" runat="server" /></div>
                </div>
                <div id="IssueType" class="GinnieOnly">
                    <label>Issue Type Code</label>
                    <input type="text" id="IssueTypeCode" runat="server" class="IssueTypeCode" preset="alphanumeric" preset_len="1" /></div>
                <div class="FannieOnly">
                    <div>
                        <label>Pool Suffix </label>
                        <input type="text" id="PoolSuffix" runat="server" /></div>
                    <div class="Wide">
                        <label>Mortgage Type </label>
                        <asp:DropDownList ID="MortgageT" runat="server"></asp:DropDownList></div>
                    <div>
                        <label>Seller ID </label>
                        <input type="text" id="SellerID" runat="server" /></div>
                    <div>
                        <label>Servicer ID </label>
                        <input type="text" id="ServicerID" runat="server" /></div>
                    <div>
                        <label>Document Custodian ID </label>
                        <input type="text" id="DocumentCustodianID" runat="server" /></div>
                </div>
                <div class="NotFannie">
                    <label>Pool Type Code</label>
                    <input type="text" id="PoolTypeCode" runat="server" class="PoolTypeCode" preset="alphanumeric" preset_len="2" /></div>
            </div>
            <div class="Col Middle Narrow GinnieOnly">
                <div>
                    <label>Issuer ID</label>
                    <input type="text" id="IssuerID" runat="server" preset="alphanumeric" preset_len="4" /></div>
                <div>
                    <label>Custodian ID</label>
                    <input type="text" id="CustodianID" runat="server" preset="alphanumeric" preset_len="6" /></div>
                <div>
                    <label>Tax ID</label>
                    <input type="text" id="TaxID" runat="server" preset="alphanumeric" preset_len="9" /></div>
            </div>
            <div class="Col Middle FannieOnly" id="FannieSecurityInfo">
                <div>
                    <label>Security Rate</label>
                    <input type="text" class="Mirror" data-mirrored=".SecurityRate" preset="percent" /></div>
                <div>
                    <label>Remittance Day </label>
                    <input type="text" id="RemittanceDay" runat="server" class="mask RemittanceD" mask="##" /></div>
                <div id="PoolStructure">
                    <label>Pool Structure Type </label>
                    <asp:DropDownList ID="StructureT" runat="server"></asp:DropDownList></div>
                <div id="FeatureCodes">
                    <label>Pool Feature Codes </label>
                </div>
                <div>
                    <label>Ownership % </label>
                    <input type="text" id="OwnershipPc" runat="server" class="mask Ownership" preset="percent" percent_no_decimal="true" /></div>
            </div>
            <div class="Col Right FannieOnly" id="Indicators">
                <div>
                    <label>Interest Only </label>
                    <input type="checkbox" id="IsInterestOnly" runat="server" class="NY" /></div>
                <div>
                    <label>Balloon Indicator </label>
                    <input type="checkbox" id="IsBalloon" runat="server" class="NY" /></div>
                <div>
                    <label>Assumability Indicator </label>
                    <input type="checkbox" id="IsAssumable" runat="server" class="NY" /></div>
                <div class="Clone" data-cloned="#LoanStatistics"></div>
            </div>
        </div>
        <div class="Section GinnieOnly" id="ACH_PITI">
            <div class="Col Left">
                <div class="Left">
                    <label>
                        Principal and Interest:
                    </label>
                </div>
                <div class="Right">
                    <div>
                        <label>Account Number</label>
                        <input type="text" id="PIAccountNum" preset="bankaccountnum" runat="server" class="Wide" />
                    </div>
                    <div>
                        <label>Routing Number</label>
                        <input type="text" id="PIRoutingNum" preset="bankroutingnum" runat="server" />
                    </div>
                </div>
            </div>
            <div class="Col Right">
                <div class="Left">
                    <label>
                        Taxes and Insurance:
                    </label>
                </div>
                <div class="Right">
                    <div>
                        <label>Account Number</label>
                        <input type="text" id="TIAccountNum" preset="bankaccountnum" runat="server" class="Wide" />
                    </div>
                    <div>
                        <label>Routing Number</label>
                        <input type="text" id="TIRoutingNum" preset="bankroutingnum" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <div class="Section GinnieOnly" id="ACH_Settlement">
            <div class="Col Left">
                <div class="Left">
                    <label>
                        Settlement Account # 1:
                    </label>
                </div>
                <div class="Right">
                    <div>
                        <label>Account Number</label>
                        <input type="text" id="SettlementAccountNumber1" preset="bankaccountnum" runat="server" class="Wide" />
                    </div>
                    <div>
                        <label>Routing Number</label>
                        <input type="text" id="SettlementRoutingNumber1" preset="bankroutingnum" runat="server" />
                    </div>
                    <div>
                        <label>American Bankers Association Bank Name</label>
                        <input type="text" id="SettlementABAName1" runat="server" />
                    </div>
                    <div>
                        <label>Third Party Sub-Account Name (e.g. CUST, TRUST, INV, 1010, 1020, etc.)</label>
                        <input type="text" id="SettlementThirdPartyAccountName1" runat="server" />
                    </div>
                    <div>
                        <label>Investor's Original Subscription Amount</label>
                        <input type="text" id="SecurityOriginalSubscriptionAmount1" preset="money" runat="server" />
                    </div>
                </div>
            </div>
            <div class="Col Right">
                <div class="Left">
                    <label>
                        Settlement Account # 2:
                    </label>
                </div>
                <div class="Right">
                    <div>
                        <label>Account Number</label>
                        <input type="text" id="SettlementAccountNumber2"  preset="bankaccountnum" runat="server" class="Wide"/>
                    </div>
                    <div>
                        <label>Routing Number</label>
                        <input type="text" id="SettlementRoutingNumber2" preset="bankroutingnum" runat="server"/>
                    </div>
                    <div>
                        <label>American Bankers Association Bank Name</label>
                        <input type="text" id="SettlementABAName2" runat="server" />
                    </div>
                    <div>
                        <label>Third Party Sub-Account Name (e.g. CUST, TRUST, INV, 1010, 1020, etc.)</label>
                        <input type="text" id="SettlementThirdPartyAccountName2" runat="server" />
                    </div>
                    <div>
                        <label>Investor's Original Subscription Amount</label>
                        <input type="text" id="SecurityOriginalSubscriptionAmount2" preset="money" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <div class="Section NotFannie" id="PoolStats">
            <div class="Col Left">
                <div>
                    <label>Security Rate</label>
                    <ml:PercentTextBox ID="SecurityR" runat="server" class="SecurityRate" preset="percent" Width="50"></ml:PercentTextBox></div>
                <div>
                    <label>Term</label>
                    <input type="text" id="Term" runat="server" class="Term mask" mask="####" maxlength="5" />
                    yrs</div>
                <div id="AmortizationMethod" class="GinnieOnly">
                    <label>Pool Amortization Method</label>
                    <asp:DropDownList ID="AmortizationMethodT" runat="server"></asp:DropDownList></div>
            </div>
            <div id="LoanStatistics" class="Col Middle">
                <div>
                    <label>Total Current Balance</label>
                    <ml:MoneyTextBox ID="TotalCurrentBalance" runat="server" preset="money"></ml:MoneyTextBox></div>
                <div>
                    <label>Low Rate</label>
                    <ml:PercentTextBox ID="LowR" runat="server" preset="percent" Width="50"></ml:PercentTextBox></div>
                <div>
                    <label>High Rate</label>
                    <ml:PercentTextBox ID="HighR" runat="server" preset="percent" Width="50"></ml:PercentTextBox></div>
                <div>
                    <label>Loan Count</label>
                    <input type="text" id="LoanCount" runat="server" /></div>
            </div>
            <div class="Col Right">
                <div>
                    <label>Amortization Type</label>
                    <asp:DropDownList ID="AmortizationT" class="AmortizationT" runat="server"></asp:DropDownList></div>
                <div class="ARMOnly">
                    <div>
                        <label>Security Rate Margin</label>
                        <ml:PercentTextBox ID="SecurityRMargin" runat="server" preset="percent" Width="50"></ml:PercentTextBox></div>
                    <div>
                        <label>Security Change Date</label>
                        <ml:DateTextBox ID="SecurityChangeD" runat="server" Width="75" preset="date" CssClass="mask" /></div>
                    <div>
                        <label>Index Type</label>
                        <asp:DropDownList ID="IndexT" runat="server" /></div>
                </div>
            </div>
        </div>
        <div class="Section FannieOnly">
            <div class="Col Left" id="FannieARMSettings">
                <div>
                    <label>Amortization Type</label>
                    <select class="Mirror" data-mirrored=".AmortizationT"></select></div>
                <div>
                    <label>Term</label>
                    <input type="text" class="Mirror TermMirror mask" data-mirrored=".Term" mask="####" maxlength="5" />
                    yrs</div>
                <div class="ARMOnly">
                    <label>ARM Plan Number </label>
                    <input type="text" id="ARMPlanNum" runat="server" /></div>
                <div class="Wide ARMOnly">
                    <label>Rounding Type </label>
                    <asp:DropDownList ID="RoundingT" runat="server"></asp:DropDownList></div>
                <div class="ARMOnly">
                    <label>Interest Rate Change Lookback Days </label>
                    <input type="text" id="InterestRateChangeLookbackDays" runat="server" /></div>
                <div class="ARMAndWeightedOnly">
                    <label>MBS Margin </label>
                    <ml:PercentTextBox ID="MBSMarginPc" runat="server" preset="percent" class="MBSMargin" Width="50"></ml:PercentTextBox></div>
            </div>
            <div class="Col Wide" id="FannieStructure">
                <div>
                    <label>Accrual Rate Structure </label>
                    <asp:DropDownList ID="AccrualRateStructureT" CssClass="AccrualRateStructureT" runat="server"></asp:DropDownList></div>
                <div class="WeightedOnly">
                    <label>Fixed Servicing Fee % </label>
                    <ml:PercentTextBox ID="FixedServicingFeePc" runat="server" preset="percent" Width="50" class="FixedServicingFee"></ml:PercentTextBox></div>
                <div class="ARMAndStatedOnly">
                    <label>Minimum Pool Accrual Rate </label>
                    <ml:PercentTextBox ID="MinAccrualR" runat="server" preset="percent" Width="50" class="MinAccrual"></ml:PercentTextBox></div>
                <div class="ARMAndStatedOnly">
                    <label>Maximum Pool Accrual Rate </label>
                    <ml:PercentTextBox ID="MaxAccrualR" runat="server" preset="percent" Width="50" class="MaxAccrual"></ml:PercentTextBox></div>
            </div>
        </div>
        <div class="Section GinnieOnly" id="PoolProperties">
            <div id="CertAndAgreement" class="Wide">
                <label>Certification & Agreement</label>
                <asp:RadioButtonList ID="CertAndAgreementT" runat="server">
                    <asp:ListItem>
            Any and all security agreements affecting the mortgages in the referenced pool or loan packages are limted by a duly executed Release of security interest
                    </asp:ListItem>
                    <asp:ListItem>
            No mortgage in the referenced pool or loan package is now subject to any security agreement
                    </asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="Wide">
                <label>Form HUD-11712A sent to document custodian</label>
                <input type="checkbox" id="IsFormHUD11711ASentToDocumentCustodian" runat="server" /></div>
            <div class="Wide" id="GovernmentBondFinance">
                <label>Government Bond Finance (Pool is to back securities for use as collateral for a state or local housing bond financing program)</label>
                <input id="GovernmentBondFinanceCheckbox" type="checkbox" runat="server" />
            </div>
            <div id="GovernmentBondFinanceAdditionalOptions" runat="server" >
                <div class="Col Left">
                    <label>Government Bond <br />Financing Program Type</label>
                    <asp:DropDownList id="GovernmentBondFinancingProgramType" runat="server"></asp:DropDownList>
                </div>
                <div class="Col Middle">
                    <label>Government Bond <br />Financing Program Name</label>
                    <input type="text" ID="GovernmentBondFinancingProgramName" runat="server" maxLength="144" style="vertical-align: bottom" />
                </div>
            </div>
        </div>

        <asp:Button ID="SaveButton" Text="Save" OnClick="SaveClicked" OnClientClick="DisableAndNotify()" runat="server" class="SaveButton" UseSubmitBehavior="false" />
    </div>
</asp:Content>
