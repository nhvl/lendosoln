﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;

namespace LendersOfficeApp.los.MortgagePools
{
    public partial class PoolNotes : BaseMortgagePoolPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Notes.Text = this.CurrentPool.Notes;
            }
        }

        protected void SavePool(object sender, EventArgs e)
        {
            CurrentPool.Notes = Notes.Text;
            CurrentPool.SaveData();
        }
    }
}
