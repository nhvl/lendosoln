﻿<%@ Page Title="" Language="C#" MasterPageFile="~/los/MortgagePools/MasterPages/PoolEditor.Master"
    AutoEventWireup="true" CodeBehind="LoansInPool.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.LoansInPool" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc" TagName="LoanTable" Src="Controls/LoanTable.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    jQuery(function($) {
        $('#RemoveButton').click(function() {
            var result = LQBMortgagePools.GetSelectedLoans();

            var loanIds = [];
            $(result).each(function() {
                loanIds.push(this.Id);
                $(this.Row).removeClass('Selected').hide();
            });

            RemoveFromPool(loanIds, this);

            $('#LoanTable').trigger('UpdateTableSorter');
            $('#RemoveButton').trigger('UpdateState');
            this.style.backgroundColor = "";
        });

        $('#RemoveButton').bind('UpdateState', function() {
            if ($('#LoanTable tr.Selected').length > 0) {
                $(this).removeAttr('disabled');
            }
            else {
                $(this).attr('disabled', 'disabled');
            }
        }).trigger('UpdateState');

        $('#LoanTable').delegate('input', 'click', function() {
            $('#RemoveButton').trigger('UpdateState');
        });

        function RemoveFromPool(loanIds, button) {
            var oldVal = $(button).val();
            $(button).val("Please wait...").attr('disabled', 'disabled');
            //Give the button a milisecond to update
            window.setTimeout(function() {
                var DTO = {
                    'LoanIds': loanIds,
                    'PoolId': ML.PoolId
                }
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: 'LoansInPool.aspx/Remove',
                    data: JSON.stringify(DTO),
                    dataType: 'json',
                    async: false,
                    error: function(XMLHttpRequest, textStatus, errorThrown) { alert("Error: " + textStatus); },
                    success: function(msg) {
                        alert("Loans removed from pool");
                    },
                    complete: function() { $(button).removeAttr('disabled').val(oldVal) }
                });
            }, 10);
        }
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
<input type="button" value="Remove from Pool" id="RemoveButton" NoHighlight />
<uc:LoanTable ID="Loans" runat="server" 
    ShowLoanEditor="true" ShowLoanViewer="true" ShowTaskEditor="true" ShowSelectionCol="true" 
    TableId="LoanTable" NoLoansText="There are no loans in this pool." RowsPerPage="30" />
</asp:Content>
