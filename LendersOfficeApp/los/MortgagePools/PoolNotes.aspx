﻿<%@ Page Language="C#" MasterPageFile="~/los/MortgagePools/MasterPages/PoolEditor.master" AutoEventWireup="true" CodeBehind="PoolNotes.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.PoolNotes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="Stylesheet" type="text/css" href="../../css/MortgagePoolTable.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
Pool notes
<br />
<asp:TextBox runat="server" ID="Notes" textmode="MultiLine" Columns="120" Rows="30"></asp:TextBox>
<br />
<input type="button" id="Save" runat="server" onserverclick="SavePool" value="Save" />
</asp:Content>
