﻿<%@ Page Title="" Language="C#" MasterPageFile="~/los/MortgagePools/MasterPages/PoolEditor.Master"
    AutoEventWireup="true" CodeBehind="AssignLoans.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.AssignLoans" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc" TagName="LoanTable" Src="Controls/LoanTable.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #FilterLeftCol
        {
            width: 30%;
            float: left;
        }
        #FilterLeftCol input[type=text]
        {
            width: 35px;
        }
        #FilterMiddleCol
        {
            width: 30%;
            float: left;
        }
        #FilterMiddleCol input
        {
            width: 75px;
        }
        #FilterRightCol
        {
            width: 30%;
            float: left;
            border: solid 1px black;
            padding: 3px;
        }
        #FilterRightCol input[type=text]
        {
            width: 75px;
        }
        #Controls
        {
            clear: both;
        }
        #Results
        {
            clear: both;
        }
        .Filters ul li label
        {
            display: inline-block;
            width: 100px;
        }
        .Filters ul li label.ExtraWide
        {
            width: 100%;
        }
        .Filters ul
        {
            list-style-type: none;
            padding: 0px;
            margin: 0px;
        }
        .Filters li
        {
            margin-bottom: 5px;
        }
        select[id*='StatusTypes']
        {
            width: 215px;
        }
    </style>
    <script type="text/javascript">
        jQuery(function($) {

            LQBMortgagePools.OnPageSelect = function(element, newUrl) {
                $('form').attr("action", newUrl);
                $('form').submit();
            }


            if ($('#LoanSearchResults tbody tr').length == 0) {
                $('#LoanSearchResults_div, #AssignButton').hide();
            }

            $('#AssignButton').click(function() {
                var result = LQBMortgagePools.GetSelectedLoans();
                if (!result) return;

                AssignToPool(result, this);

                $('#LoanSearchResults').trigger('UpdateTableSorter');
                $(this).trigger('UpdateState');
                this.style.backgroundColor = "";
            });

            $('#AssignButton').bind('UpdateState', function() {
                if ($('#LoanSearchResults tr.Selected').length > 0) {
                    $(this).removeAttr('disabled');
                }
                else {
                    $(this).attr('disabled', 'disabled');
                }
            }).trigger('UpdateState');

            $('#LoanSearchResults').delegate('input', 'click', function() {
                $('#AssignButton').trigger('UpdateState');
            });

            function AssignToPool(selectedLoans, button) {
                var oldVal = $(button).val();
                $(button).val("Please wait...").attr('disabled', 'disabled');

                var DTO = {
                    'LoanIds': $.map(selectedLoans, function(value, index) { return value.Id }),
                    'PoolId': ML.PoolId,
                    'Settings': {
                        'IsSetBackEndBasePrice': $('.IsSetBackEndBasePrice').prop('checked'),
                        'IsSetBackEndRateLock': $('.IsSetBackEndRateLock').prop('checked'),
                        'IsSetDeliveryFee': $('.IsSetDeliveryFee').prop('checked'),
                        'DeliveryFee': LQBMortgagePools.FromRep($('.DeliveryFee')) || 0
                    }
                }
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: 'AssignLoans.aspx/Assign',
                    data: JSON.stringify(DTO),
                    dataType: 'json',
                    async: false,
                    error: function(XMLHttpRequest, textStatus, errorThrown) { alert("Error: " + errorThrown); },
                    success: function(msg) {
                        if (msg && msg.d) {                            
                            var queryString = '?id=' + msg.d.ErrorCacheId;
                            showModal('/newlos/BatchOperationErrorSummary.aspx' + queryString, null, 'dialogWidth:600px;dialogHeight:300px;center:yes;resizable:yes;scroll:yes;status=yes;help=no;');
                            hideSuccessfullyAssignedLoanIds(selectedLoans, msg.d.ErrorLoanIds);
                        } else {
                            alert("Loans assigned to pool");
                            hideSuccessfullyAssignedLoanIds(selectedLoans, []);
                        }

                        function hideSuccessfullyAssignedLoanIds(selectedLoans, errorLoanIds) {
                            for (var i = 0; i < selectedLoans.length; i++) {
                                if ($.inArray(selectedLoans[i].Id, errorLoanIds) === -1) {
                                    $(selectedLoans[i].Row).removeClass('Selected').hide();
                                }
                            }
                        }
                    },
                    complete: function() { $(button).removeAttr('disabled').val(oldVal) }
                });
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <input type="hidden" runat="server" id="SearchState" />
    <div id="FilterLeftCol" class="Filters">
        <ul>
            <li>
                <label>
                    Loan Type
                </label>
                <asp:DropDownList runat="server" ID="LoanTypes">
                </asp:DropDownList>
            </li>
            <li>
                <label>
                    Amortization Type
                </label>
                <asp:DropDownList runat="server" ID="AmortTypes">
                </asp:DropDownList>
            </li>
            <li>
                <label>
                    Term
                </label>
                <input type="text" runat="server" id="Term" class="Term" />
            </li>
            <li>
                <label class="ExtraWide">
                    <input type="checkbox" runat="server" id="IncludeBackEndRateLock" />
                    Include loans with a back-end rate lock</label>
            </li>
        </ul>
    </div>
    <div id="FilterMiddleCol" class="Filters">
        <ul>
            <li>
                <label>
                    Min rate
                </label>
                <input type="text" runat="server" id="MinRate" preset="percent" />
            </li>
            <li>
                <label>
                    Max rate
                </label>
                <input type="text" runat="server" id="MaxRate" preset="percent" />
            </li>
            <li>
                <label>
                    Minimum Status
                </label>
                <asp:DropDownList runat="server" ID="StatusTypes">
                </asp:DropDownList>
            </li>
        </ul>
    </div>
    <div id="FilterRightCol" class="Filters">
        On assigning a loan to the pool, set
        <ul>
            <li>
                <label class="ExtraWide"><input type="checkbox" runat="server" id="IsSetBackEndBasePrice" class="IsSetBackEndBasePrice" />Back-end base price</label>
            </li>
            <li>
                <label class="ExtraWide"><input type="checkbox" runat="server" id="IsSetBackEndRateLock" class="IsSetBackEndRateLock" />Back-end rate lock</label>
            </li>
            <li>
                <label><input type="checkbox" runat="server" id="IsSetDeliveryFee" class="IsSetDeliveryFee" />Delivery fee</label>
                <input type="text" runat="server" id="DeliveryFee" class="DeliveryFee" preset="percent" />
            </li>
        </ul>
    </div>
    <div id="Controls">
        <input type="button" value="Search" runat="server" id="SearchButton" onserverclick="SearchClick" />
    </div>
    <div id="Results">
        <uc:LoanTable runat="server" ID="Loans" TableId="LoanSearchResults" ShowLoanEditor="true" ShowLoanViewer="true" ShowTaskEditor="true" ShowSelectionCol="true"/>
    </div>
    <input type="button" value="Assign Selected Loans to Pool" id="AssignButton" />
</asp:Content>
