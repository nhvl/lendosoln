﻿<%@ Page Title="" Language="C#" MasterPageFile="~/los/MortgagePools/MasterPages/MortgagePoolPopup.Master" AutoEventWireup="true" CodeBehind="PoolPicker.aspx.cs" Inherits="LendersOfficeApp.los.MortgagePools.PoolPicker" %>
<%@ Import Namespace="LendersOffice.ObjLib.CapitalMarkets" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc" TagName="LoanPoolPortlet" Src="~/los/Portlets/LoanPoolPortlet.ascx"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HtmlHead" runat="server">
<script type="text/javascript">
    jQuery(function($) {
        $('#PoolPortlet').on('PoolSelected', function(event, poolId, poolName) {
            window.opener.TradeAssociated(poolId, poolName);
            onClosePopup();
        });
    });
</script>
<link rel="Stylesheet" type="text/css" href="../../css/MortgagePoolTable.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Header" runat="server">
Pick loan pool
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Content" runat="server">
    <uc:LoanPoolPortlet id="LoanPools" runat="server" SelectPoolOnly="true" HideCreatePool="true" HideDeleteLinks="true"/>
</asp:Content>
