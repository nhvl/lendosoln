﻿using System;
using LendersOffice.Common;
using System.Text;
using DataAccess;
using LendersOffice.ObjLib.Conversions;
using LendersOffice.Security;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.MortgagePools
{
    public partial class ExportVerification : BaseMortgagePoolPage
    {
        protected void PageLoad(object sender, EventArgs e)
        {
            VerifyExport();
        }

        /// <summary>
        /// Creates the exporter corresponding to the pool's chosen agency type and runs the exporter to determine any export errors.
        /// </summary>
        protected void VerifyExport()
        {
            Message1.Text = String.Empty;
            Message2.Text = String.Empty;

            if (CurrentPool.AgencyT == E_MortgagePoolAgencyT.GinnieMae && BrokerUserPrincipal.CurrentPrincipal.BrokerDB.EnablePDDExport )
            {
                ExportButton.Value = "Export to GinnieNet File";
                ExportButton2.Value = "Export to PDD File";
                ExportButton2.Visible = true;
                
                var ginnieNetExporter = CurrentPool.GetExporter(false);
                TryExportAndDisplayErrors(ginnieNetExporter, ExportButton, Message1, "GinnieNet");

                var pddExporter = CurrentPool.GetExporter(true);
                TryExportAndDisplayErrors(pddExporter, ExportButton2, Message2, "PDD");
            }
            else
            {
                var exporter = CurrentPool.GetExporter(false);
                TryExportAndDisplayErrors(exporter, ExportButton, Message1);
            }
        }

        /// <summary>
        /// Does a "dry run" of an export to find any export errors. Does not use the output of the export. 
        /// If no errors are found, the given export button is enabled with the Export method.
        /// If errors are found, the errors are displayed in the page HTML.
        /// </summary>
        /// <param name="exporter">The exporter to run.</param>
        /// <param name="exportButton">The button to be enabled on a successful dry run export.</param>
        /// <param name="exportName">The name to display as the export name. Optional.</param>
        private void TryExportAndDisplayErrors(IPoolExporter exporter, System.Web.UI.HtmlControls.HtmlInputButton exportButton, System.Web.UI.WebControls.Label messageBox, string exportName = "")
        {
            exportName = AspxTools.HtmlString(string.IsNullOrEmpty(exportName) ? string.Empty : " " + exportName);

            byte[] buffer;

            bool exportWasSuccessful = exporter.TryExport(out buffer);

            if (exportWasSuccessful && exporter.Errors.Count == 0)
            {
                messageBox.Text += "No Errors with" + exportName + " Export.";
                exportButton.ServerClick += new EventHandler(Export);
                exportButton.Disabled = false;
            }
            else
            {
                exportButton.Disabled = true;
                StringBuilder errorMessage = new StringBuilder("<span style='color:red;'>The following errors are preventing" + exportName + " export:");
                foreach (string s in exporter.Errors)
                {
                    errorMessage.Append("<br/>").AppendLine(AspxTools.HtmlString(s));
                }
                errorMessage.Append("</span>");
                messageBox.Text += errorMessage.ToString();
            }

            if (exporter.Warnings.Count != 0)
            {
                StringBuilder warningMessage = new StringBuilder("<br/><br/><span style='color:darkorange;'>The following warnings occurred during" + exportName + " export:");
                foreach (string s in exporter.Warnings)
                {
                    warningMessage.Append("<br/>").AppendLine(AspxTools.HtmlString(s));
                }
                warningMessage.Append("</span>");
                messageBox.Text += warningMessage.ToString();
            }
        }

        /// <summary>
        /// Runs the exporter for the chosen pool's target agency and sends the output as a response to be downloaded as a file.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">Arguments to the event.</param>
        protected void Export(object sender, EventArgs e)
        {
            bool pdd = sender.Equals(ExportButton2);
            var exporter = CurrentPool.GetExporter(pdd);
            Response.Clear();
            Response.ContentType = "application/text";
            string ext = "";
            if (CurrentPool.AgencyT == DataAccess.E_MortgagePoolAgencyT.FannieMae || 
                CurrentPool.AgencyT == DataAccess.E_MortgagePoolAgencyT.FreddieMac ||
                (CurrentPool.AgencyT == E_MortgagePoolAgencyT.GinnieMae && pdd))
            {
                ext = ".xml";
            }

            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{exporter.OutputFileName}{ext}\"");

            byte[] buffer;
            exporter.TryExport(out buffer);

            Response.OutputStream.Write(buffer, 0, buffer.Length);
            Response.Flush();
            Response.End();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
