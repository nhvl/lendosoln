﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using DataAccess;
using System.Web.UI.HtmlControls;
using LendersOffice.ObjLib.MortgagePool;

namespace LendersOfficeApp.los.MortgagePools
{
    public partial class Delivery : BaseMortgagePoolPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("jquery.tablesorter.min.js");

            GroupRadioButtons();
            if (!Page.IsPostBack)
            {
                BindPool();
            }
        }
        
        private List<HtmlInputRadioButton> BasePricingSet;
        private List<HtmlInputRadioButton> LLPASet;

        private List<HtmlInputRadioButton> SRPSet;

        private void GroupRadioButtons()
        {
            BasePricingSet = new List<HtmlInputRadioButton>() 
            { 
                BasePricingFromManual, 
                BasePricingFromBackend, 
                BasePricingFromPool, 
                BasePricingFromTable, 
            };

            LLPASet = new List<HtmlInputRadioButton>()
            {
                LLPAFromManual, 
                LLPAFromBackend, 
            };

            SRPSet = new List<HtmlInputRadioButton>()
            {
                SRPFromManual,
                SRPFromBackend,
                SRPFromPool,
            };
        }
        private void BindPool()
        {
            BindInvestors(InvestorName, CurrentPool.InvestorName);
            Tools.Bind_Generic(CommitmentType, CurrentPool.CommitmentType);
            Tools.Bind_SettlementType(SettlementType, this.CurrentPool.SettlementType);
            Tools.Bind_DeliveryType(DeliveryType, this.CurrentPool.DeliveryType);

            CommitmentNum.Value = CurrentPool.CommitmentNum;
            CommitmentDate.Text = CurrentPool.CommitmentDate_rep;
            CommitmentExpires.Text = CurrentPool.CommitmentExpires_rep;

            CommitmentBasisLckd.Checked = CurrentPool.CommitmentBasisLckd;
            CommitmentBasis.Value = CurrentPool.CommitmentBasis_rep;
            Tolerance.Value = CurrentPool.Tolerance.ToString();
            OverUnderTolerance.Value = CurrentPool.OverUnderTolerance_rep;

            BindRadioButtons(BasePricingSet, CurrentPool.BasePricingCalcType);
            BasePricingPoolLevelLckd.Checked = CurrentPool.BasePricingPoolLevelLckd;
            BasePricingPoolLevelManual.Value = CurrentPool.BasePricingPoolLevelManual_rep;
            BasePricingManual.Value = CurrentPool.BasePricingAmt_rep;

            BindRadioButtons(LLPASet, CurrentPool.LLPAType);
            LLPAManual.Value = CurrentPool.LLPAAmt_rep;
            LLPABackendRate.Value = CurrentPool.BackendLLPA_rep;

            BindRadioButtons(SRPSet, CurrentPool.SRPType);
            SRPPoolLevel.Value = CurrentPool.SRPPoolLevel_rep;
            SRPManual.Value = CurrentPool.SRPAmt_rep;
            SRPBackendRate.Value = CurrentPool.BackendSRP_rep;

            BackendCalcValue.Value = CurrentPool.BackEndBasePricing_rep;

            BlendPairOffCalcNetAmount.Value = CurrentPool.BlendPairOffAmt_rep;
            BlendPairOffCalcNetPrice.Value = CurrentPool.BlendPairOffPrice_rep;

            EarlyDeliveryDate.Text = this.CurrentPool.EarlyDeliveryDate_rep;
            AssignDate.Text = this.CurrentPool.AssignDate_rep;
            ConfirmDate.Text = this.CurrentPool.ConfirmDate_rep;

            OpenBalance.Value = this.CurrentPool.OpenBalance_rep;

            UnpaidBalanceAmountAllocated.Value = this.CurrentPool.UnpaidBalanceAmountAllocated_rep;
            UnpaidBalanceAmountShipped.Value = this.CurrentPool.UnpaidBalanceAmountShipped_rep;
            UnpaidBalanceAmountPurchased.Value = this.CurrentPool.UnpaidBalanceAmountPurchased_rep;
            ProfitabilityAmountAllocated.Value = this.CurrentPool.ProfitabilityAmountAllocated_rep;
            ProfitabilityAmountShipped.Value = this.CurrentPool.ProfitabilityAmountShipped_rep;
            ProfitabilityAmountPurchased.Value = this.CurrentPool.ProfitabilityAmountPurchased_rep;
            TotalProfit.Value = this.CurrentPool.TotalProfit_rep;

            Tools.Bind_Generic(BlendPairOffCalcType, CurrentPool.BlendPairOffCalcType);
            BlendPairOffCurrPrice.Value = CurrentPool.BlendPairOffMarketPrice_rep;
            ClientScript.RegisterHiddenField("CurrentBalance", CurrentPool.TotalCurrentBalance.ToString());
            ClientScript.RegisterHiddenField("PoolLevelPricingCalc", CurrentPool.CalculatePoolLevelBasePricing().ToString());
            ClientScript.RegisterHiddenField("BasePricingManualDefault", CurrentPool.BasePricingManual.ToString());
            ClientScript.RegisterHiddenField("CommitmentBasisCalc", CurrentPool.CommitmentBasisCalc.ToString());
            RegisterJsObject("BasePricingTable", CurrentPool.BasePricingTable.ToList());
            RegisterJsObject("NoteRateToLoanAmounts", CurrentPool.NoteRateToLoanAmount);
        }

        protected void SavePool(object sender, EventArgs e)
        {
            if (InvestorName.SelectedItem != null)
            {
                CurrentPool.InvestorName = InvestorName.SelectedItem.Text;
            }

            CurrentPool.CommitmentType = (E_CommitmentType)GetEnum(CommitmentType.SelectedValue);
            CurrentPool.SettlementType = (E_SettlementType)GetEnum(SettlementType.SelectedValue);
            CurrentPool.DeliveryType = (E_DeliveryType)GetEnum(DeliveryType.SelectedValue);
            CurrentPool.CommitmentNum = CommitmentNum.Value;
            CurrentPool.CommitmentDate_rep = CommitmentDate.Text;
            CurrentPool.CommitmentExpires_rep = CommitmentExpires.Text;
            CurrentPool.CommitmentBasisLckd = CommitmentBasisLckd.Checked;
            CurrentPool.CommitmentBasisManual_rep = CommitmentBasis.Value;
            CurrentPool.Tolerance_rep = Tolerance.Value;
            CurrentPool.BasePricingCalcType = (E_BasePricingCalcType)GetSelectedIdx(BasePricingSet);
            CurrentPool.BasePricingPoolLevelLckd = BasePricingPoolLevelLckd.Checked;
            CurrentPool.BasePricingPoolLevelManual_rep = BasePricingPoolLevelManual.Value;
            CurrentPool.BasePricingManual_rep = BasePricingManual.Value;
            CurrentPool.LLPAType = (E_LLPAType)GetSelectedIdx(LLPASet);
            CurrentPool.LLPAManual_rep = LLPAManual.Value;
            CurrentPool.SRPType = (E_SRPType)GetSelectedIdx(SRPSet);
            CurrentPool.SRPManual_rep = SRPManual.Value;
            CurrentPool.SRPPoolLevel_rep = SRPManual.Value;
            CurrentPool.BlendPairOffCalcType = (E_BlendPairOffCalcType)GetEnum(BlendPairOffCalcType.SelectedValue);
            CurrentPool.BlendPairOffMarketPrice_rep = BlendPairOffCurrPrice.Value;
            CurrentPool.SRPPoolLevel_rep = SRPPoolLevel.Value;
            CurrentPool.BasePricingTableJSON = BasePricingTableJSON.Value;

            CurrentPool.EarlyDeliveryDate_rep = EarlyDeliveryDate.Text;
            CurrentPool.AssignDate_rep = AssignDate.Text;
            CurrentPool.ConfirmDate_rep = ConfirmDate.Text;

            CurrentPool.SaveData();
            Response.Redirect(Request.Url.ToString());
        }

        private int GetSelectedIdx(List<HtmlInputRadioButton> set)
        {
            for (var i = 0; i < set.Count; i++)
            {
                if (set[i] != null && set[i].Checked) return i;
            }

            return 0;
        }

        private int GetEnum(string p)
        {
            int val = 0;
            int.TryParse(p, out val);

            return val;
        }



        private void BindRadioButtons(IEnumerable<HtmlInputRadioButton> buttons, Enum value)
        {
            foreach (var b in buttons)
            {
                b.Checked = false;
            }

            buttons.ElementAt(Convert.ToInt32(value)).Checked = true;
        }

        private void BindInvestors(DropDownList InvestorName, string name)
        {
            var investors = InvestorRolodexEntry.GetAll(BrokerUserPrincipal.CurrentPrincipal.BrokerId, null);
            bool hasSelected = false;
            foreach (var i in investors)
            {
                var item = new ListItem(i.InvestorName, i.Id.Value.ToString());
                if (!string.IsNullOrEmpty(name) && name.Equals(i.InvestorName, StringComparison.CurrentCultureIgnoreCase) && !hasSelected)
                {
                    item.Selected = true;
                    hasSelected = true;
                }

                InvestorName.Items.Add(item);
            }
        }
    }
}
