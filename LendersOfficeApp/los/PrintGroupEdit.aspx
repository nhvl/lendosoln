<%@ Page language="c#" Codebehind="PrintGroupEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.PrintGroupEdit" EnableViewState="false"%>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrintGroupEdit</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
	</HEAD>
	<body style="MARGIN-LEFT: 5px" bgColor="gainsboro" MS_POSITIONING="FlowLayout" class="PaddingTopBottom5" >
	<script src=<%=AspxTools.SafeUrl(DataAccess.Tools.VRoot+ "/inc/mask.js")%> type="text/javascript"></script>
		<script language="javascript">
  <!--
  var oMainList = null;
  var oGroupList = null;
  
  function _init() {
    oMainList = document.getElementById("<%= AspxTools.ClientId(m_mainList) %>");
    oGroupList = document.getElementById("<%= AspxTools.ClientId(m_groupList) %>");
    document.getElementById("<%= AspxTools.ClientId(m_GroupName) %>").focus();
    setButtonStatus(); 
    if(getGroupID() ===  <%= AspxTools.JsString(Guid.Empty)%>)
		parent.list.unhighlightLast(); 
  }
  
  function createProperId(id)
  {	
	var proper = new String(id);
	proper = "_" + proper.replace(/-/g, "");
	return proper;
  }
  
  function getGroupID()
  {
	var groupIDelement = document.getElementById('<%= AspxTools.ClientId(m_GroupID) %>');
	if(groupIDelement.value)
		return groupIDelement.value;
	return <%= AspxTools.JsString(Guid.Empty)%>;
  }
  
  function setGroupID(val)
  {
	document.getElementById('<%= AspxTools.ClientId(m_GroupID) %>').value = val;
	document.getElementById("GroupID").value = val;
  }
  
  function duplicateGroup()
  {
		var id = getGroupID();
		var submitAndUnhighlightLinks = function(){
			document.getElementById('<%= AspxTools.ClientId(m_CommandToDo) %>').value = "Duplicate";
			var properId = createProperId(getGroupID());
			parent.list.unhighlightLink(properId);
			document.forms[0].submit();
		};
		
		if (isDirty() || (id == <%= AspxTools.JsString(Guid.Empty)%>)) {
			PolyConfirmSave(function(confirmed){
				if (!confirmed){
					parent.list.document.forms[0].submit();
					return;
				}

				if (save()){
					submitAndUnhighlightLinks();
				}
			}, true, function(){
				parent.list.document.forms[0].submit();
			});
    } else {
			submitAndUnhighlightLinks();
		}
	
  }
  
  function deleteGroup() {

    if (getGroupID() == <%= AspxTools.JsString(Guid.Empty)%> ) return;
    
    if (confirm(<%=AspxTools.JsString(JsMessages.PrintGroup_ConfirmDeleteGroup)%>)) {
      var args = new Object();
      args["GroupID"] = getGroupID();
      var result = gService.printgroupedit.call("DeleteGroup", args);   
      if (!result.error)
      {
		if( result.value.Error != null )
			alert( result.value.Error );

		parent.location = parent.location;
      }
	  else
		alert( <%=AspxTools.JsString(JsMessages.PrintGroup_DeleteGroupFailed)%> );
    }
  }
  
  function save() {
    var length = oGroupList.options.length;
    if (length == 0) {
      alert(<%=AspxTools.JsString(JsMessages.PrintGroup_Empty)%>);
      return false;
    }
    if (<%= AspxTools.JsGetElementById(m_GroupName) %>.value == "") {
      alert(<%=AspxTools.JsString(JsMessages.PrintGroup_BlankName)%>);
      return false;
    }
    
    var args = new Object();
    args["count"] = length + "";
    args["GroupName"] = document.getElementById("<%= AspxTools.ClientId(m_GroupName) %>").value;
    args["GroupID"] = getGroupID();
    
    for (var i = 0; i < length; i++) {
      var o = oGroupList.options[i];
      args["item_" + i] = o.value;
    }

    var result = gService.printgroupedit.call("Save", args);   

    if (!result.error)
    {
		if( getGroupID() != null)
		{
    		var command = document.getElementById('<%= AspxTools.ClientId(m_CommandToDo) %>').value;
    		setGroupID(result.value.GroupID);
    		if(command != "Duplicate")
				parent.list.updateGroupName(document.getElementById("<%= AspxTools.ClientId(m_GroupName) %>").value);
			clearDirty();
			alert(<%=AspxTools.JsString(JsMessages.PrintGroup_SaveSuccessful)%>);
			if(command == "Duplicate")
				parent.list.document.forms[0].submit();
            return true;
		}

		if( result.value.Error != null )
		{
			alert( result.value.Error );
			return false;
		}
    }
  }
  
  function closeMe() {
		
    if (isDirty()) {
      PolyConfirmSave(function(confirmed){
				if (confirmed ? save() : true) parent.closeMe();
			}, true);
	  } else {
			parent.closeMe();
		}
  } 
   
  function addToGroup() {
    var selectedIndex = oMainList.selectedIndex;
    if (selectedIndex < 0)
      return;

      
    var bottomIndex = oGroupList.options.length;      
    for (var i = oMainList.options.length - 1; i >= 0 ; i--) {
      var o = oMainList.options[i];
      if (!o.selected || o.value == '') continue;
      
      var o_clone = document.createElement("OPTION");
      oGroupList.options.add(o_clone, bottomIndex);  
    
      o_clone.text = o.text;
      o_clone.value = o.value;
      oMainList.remove(i);
    
    }
    
    if (selectedIndex == oMainList.length)
      oMainList.selectedIndex = selectedIndex - 1;
    else
      oMainList.selectedIndex = selectedIndex;
  
  }
  
  function removeFromGroup() {
    var selectedIndex = oGroupList.selectedIndex;
    if (selectedIndex < 0)
      return;  
    
    for (var i = oGroupList.options.length - 1; i >= 0; i--) {
      var o = oGroupList.options[i];
      if (!o.selected) continue;
      
      var o_clone = document.createElement("OPTION");
      oMainList.options.add(o_clone);
      o_clone.text = o.text;
      o_clone.value = o.value;
      oGroupList.remove(i);
    }
    
    if (selectedIndex == oGroupList.length)
      oGroupList.selectedIndex = selectedIndex - 1;
    else
      oGroupList.selectedIndex = selectedIndex;    
  }
  
  function moveUp() {
    var srcIndex = oGroupList.selectedIndex;
    if (srcIndex <= 0 || oGroupList.options.length == 1)
      return;
    swap(srcIndex, srcIndex - 1);
    
  }
  
  function moveDown() {
    var srcIndex = oGroupList.selectedIndex;
    if (srcIndex < 0 || oGroupList.options.length == 1 || srcIndex == oGroupList.options.length - 1)
      return;
      
    swap(srcIndex, srcIndex + 1);
    
  }
  
  function swap(srcIndex, destIndex) {
    swapNodePoly(oGroupList.options[srcIndex], oGroupList.options[destIndex]);
  }
  
  function setButtonStatus() {
	if (document.getElementById('<%= AspxTools.ClientId(m_GroupName) %>').readOnly == true)
		return;
	  
	var bMainSelection = document.getElementById('<%= AspxTools.ClientId(m_mainList) %>').selectedIndex >= 0;
	var bGroupSelection = document.getElementById('<%= AspxTools.ClientId(m_groupList) %>').selectedIndex >= 0;

    document.getElementById('AddToGroupButton').disabled = !bMainSelection;
    document.getElementById('RemoveFromGroupButton').disabled = !bGroupSelection;
    document.getElementById('MoveUpButton').disabled = !bGroupSelection;
    document.getElementById('MoveDownButton').disabled = !bGroupSelection;
  }
  
  window.onload     = handleLoad;
  window.onresize   = handleResize;
  
    function handleLoad()
  {
       resizeLists();
  }
  function resizeLists()
  {
        var winW = document.body.offsetWidth;
        var winH = document.body.offsetHeight;
        
        var MINW = 50;
        var MINH = 624;
        
        var MAXW = 75;
        var MAXH = 90;
        
        var gl = document.getElementById('m_groupList');
        var ml = document.getElementById('m_mainList');
        
        var gsty = gl.style;
        var msty = ml.style;
        
        var newW = Math.max(Math.round(winW/2-MAXW),MINW)+"px";
        var newH = Math.max(Math.round(winH-MAXH),MINH)+"px";

        gsty.width   = newW;
        msty.width   = newW;
        gsty.height  = newH;
        msty.height  = newH;
     
  }
  
  var resized = false;
  function handleResize()
  {
    resizeLists();
  }
  //-->
		</script>
		<form id="PrintGroupEdit" method="post" runat="server">
			<table id="Table1" cellSpacing="0" cellPadding="0" border="0">
				<input id="m_CommandToDo" runat="server" type="hidden" name="m_CommandToDo">
				<input id="m_GroupID" runat="server" type="hidden" name="m_GroupID" >
				<tr>
					<td class="FieldLabel" vAlign="top" colSpan="4">
						Group Name:
						<asp:textbox id="m_GroupName" runat="server" maxlength="200" CssClass="mask" preset="words"></asp:textbox>
						<asp:Panel id="m_saveItPanel" runat="server" style="DISPLAY: inline">
							<INPUT onclick="if (!hasDisabledAttr(parentElement)) save(true);" type="button" value="Save">
						</asp:Panel>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:Panel id="m_deletePanel" runat="server" style="DISPLAY: inline">
							<INPUT onclick="if (!hasDisabledAttr(parentElement)) deleteGroup();" type="button" value="Delete Group">
						</asp:Panel>
						<asp:Panel id="m_duplicatePanel" runat="server" style="DISPLAY: inline">
							<INPUT onclick="if (!hasDisabledAttr(parentElement)) duplicateGroup();" type="button" value="Duplicate Group">
						</asp:Panel>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td class="FieldLabel" vAlign="top"  >Available Forms</td>
					<td vAlign="middle" noWrap></td>
					<td class="FieldLabel" vAlign="top" >Forms in Group</td>
					<td></td>
				</tr>
				<tr>
					<td valign="top" >&nbsp;&nbsp;&nbsp;<i>(To select multiple items, 
							hold the "<b>CTRL</b>" key down while selecting)</i></td>
					<td></td>
				</tr>
				<tr>
					<td vAlign="top" noWrap><asp:listbox id="m_mainList" width="200px" height="100px" runat="server" selectionmode="Multiple" NoHighlight="true" AllowMouseWheel onchange="setButtonStatus();" ></asp:listbox></td>
					<td vAlign="middle" noWrap>
						<table id="Table2" cellSpacing="0" cellPadding="0" border="0" >
							<tr>
								<td noWrap>
									<asp:Panel id="m_addToGPanel" runat="server" style="DISPLAY: inline">
										<INPUT id="AddToGroupButton" style="WIDTH: 70px" onclick="if (!hasDisabledAttr(parentElement)) addToGroup();" type="button" value="Add >>">
									</asp:Panel>
								</td>
							</tr>
							<tr>
								<td noWrap>
									<asp:Panel id="m_removePanel" runat="server" style="DISPLAY: inline">
										<INPUT id="RemoveFromGroupButton" style="WIDTH: 70px" onclick="if (!hasDisabledAttr(parentElement)) removeFromGroup();" type="button" value="Remove <<">
									</asp:Panel>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><input style="WIDTH: 70px" onclick="closeMe();" type="button" value="Close"></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" noWrap><asp:listbox id="m_groupList"  height="300px" width="200px" runat="server" selectionmode="Multiple" NoHighlight="true" AllowMouseWheel onchange="setButtonStatus();"></asp:listbox></td>
					<td>
						<table cellSpacing="0" cellPadding="0" border="0">
							<tr>
								<td>
									<asp:Panel id="m_moveUpPanel" runat="server" style="DISPLAY: inline">
										<INPUT id="MoveUpButton" style="WIDTH: 50px" onclick="if (!hasDisabledAttr(parentElement)) moveUp();" type="button" value="Up">
									</asp:Panel>
								</td>
							</tr>
							<tr>
								<td>
									<asp:Panel id="m_moveDnPanel" runat="server" style="DISPLAY: inline">
										<INPUT id="MoveDownButton" style="WIDTH: 50px" onclick="if (!hasDisabledAttr(parentElement)) moveDown();" type="button" value="Down">
									</asp:Panel>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
