﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomPDFEditor.aspx.cs" Inherits="LendersOfficeApp.los.CustomPDFForms.CustomPDFEditor" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head runat="server">
	    <title>Custom PDF Editor</title>
	    <style type="text/css">
		    .Background {
			    background-color:Gray;
			    padding:20px;
		    }

		    .PdfPage {
			    background-color:White;
			    border: solid 1px black;
			    width: 400px;
			    height: 500px;
			    margin: 5px;
			    position: absolute;
		    }

		    .Text {
			    border: solid 1px black;
			    background-color: gainsboro;
			    z-index: 900;
		    }

		    .Checkbox {
			    border: solid 1px black;
			    background-color: gainsboro;
			    text-align:center;
			    z-index: 901;
		    }

		    .Signature {
			    border: solid 1px black;
			    background-color: #8DAAE2;
			    text-align:left;
			    z-index: 902;
		    }

		    .DataField {
			    width: 40px;
			    background-color: #CCC;
		    }

		    .Modal {
		        z-index: 900;
		        font-weight:normal;
		        border: black 1px solid;
		        padding: 2px;
		        display: none;
		        position: absolute;
		        background-color: whitesmoke;
		        width: 500px;
		    }

		    .Tooltip {
		        z-index: 900;
		        font-weight:normal;
		        padding: 2px;
		        display: none;
		        position: absolute;
		        background-color:yellow;
		        left:0px;
		        top:0px;
		        border: solid 1px black;
		    }

		    .Hidden {
		        display: none;
		    }
	    </style>
	</head>
    <body onunload="refreshOpener();" style="background:gainsboro">
        <script type="text/javascript">
          var confirmRemoveFieldMsg = "Remove the selected fields?";
            var confirmSaveOnExit = "Save changes before exiting?";
            var dirtyBit = false;
            window.onbeforeunload = unload;
            var hiddenList = [];

            function refreshOpener() {
	            if ((window.opener) && (!window.opener.closed))
                    window.opener.location.reload(true);
            }

            function setDirty() {
	            document.getElementById('save').disabled = false;
                dirtyBit = true;
            }

	        function resetDirty() {
	            document.getElementById('save').disabled = true;
                dirtyBit = false;
            }

            function defaultCategory() {
                var ddl = document.getElementById('m_Group');
                ddl.selectedIndex = 0;
	            document.getElementById('m_FieldList').scrollTop = 0;
            }

            function resetCategory() {
                var ddl = document.getElementById('m_Group');
                ddl.innerText = '';
                var opt = document.createElement("option");
                ddl.options.add(opt);
                opt.text = "All Categories";
                opt.value = "All Categories";
                ddl.selectedIndex = 0;

                var isCheckbox = g_oSelectedPdfField.cssClass.indexOf('pdf-field-checkbox') != -1;
	            var header = '';
	            var list = $('#m_FieldList div');
	            var headerHasVisibleChildren = false;

	            for (var i = 0; i < list.length;  i++) {
	                if (list[i].getAttribute('isHeader') == 'true') {
	                    if(headerHasVisibleChildren)
	                        addCategory(header);

	                    header = list[i].getAttribute('text');
	                    headerHasVisibleChildren = false;
	                    continue;
	                }

	                if (isCheckbox && list[i].getAttribute('isBool') == 'false')
	                    continue;

	                headerHasVisibleChildren = true;
	            }
	            if(headerHasVisibleChildren)
                    addCategory(header);
            }

            function addCategory(name) {
				var opt = document.createElement("option");
                opt.text = name;
                opt.value = name;
                var ddl = document.getElementById('m_Group');
                ddl.options.add(opt);
            }

            function saveOnExit() { return confirm(confirmSaveOnExit); }
            function confirmRemove() { return confirm(confirmRemoveFieldMsg); }
            function confirmClose() { return confirm(confirmSaveOnExit) ? 6 : 7;}

            function unload() {
                if (g_bIsPreviewClick) return; // 7/16/2010 dd - We do not want to ask user to Save when user click Preview.
                if (!dirtyBit) return;
                return UnloadMessage;
            }

            function onCloseClicked() {
                if (dirtyBit) {
                    choice = confirmClose();
                    if (choice == 2)
                        return;
                    else if (choice == 6)
                        Save();
                }
                resetDirty();
                onClosePopup();
            }

	        function showFieldText(text, event) {
	            if (Modal.activeModal != '')
	                return;

	            document.getElementById('FieldTextDiv').innerText = text;
	            Modal.ShowPopup('FieldTextDiv', null, event);
	        }

	        function hideFieldText() {
	            if (Modal.activeModal != 'FieldTextDiv')
	                return;

	            Modal.Hide();
	        }

	        function addNewField() {
	            var value = $('#searchCriteria').val();
	            if (value.length > 3 && !checkExistingList(value))
	            {
                    var args = { fieldName: value };
                    var result = gService.editor.call("AddField", args);

                    if (!result.error && result.value['Success'] == 'True') {
                        FieldList = eval(result.value['FieldList']);
                        populateFieldsForSearch();
                        resetCategory();
                    }
                }

                displaySearchResults(false);
	        }

	        function checkExistingList(fieldName) {
	            fieldName = fieldName.toLowerCase();

	            var list = $('#m_FieldList div');

	            for (var i = 0; i < list.length;  i++) {
	                var name = list[i].getAttribute('name');
	                if (name.indexOf(':') != -1)
	                    name = name.substring(0, name.indexOf(':'));
	                if (list[i].getAttribute('isHeader') != 'true' && name == fieldName)
	                    return true;
	            }

	            for(var i = 0; i < hiddenList.length; i++)
	            {
	                var name = hiddenList[i]['Name'].toLowerCase();

	                if (name.indexOf(':') != -1)
	                    name = name.substring(0, name.indexOf(':'));

	               if (name == fieldName)
	                    return true;
	            }

	            return false;
	        }

	        function populateFieldsForSearch() {
	            var i = 0;
	            var item = FieldList[i];
	            document.getElementById('m_FieldList').innerHTML = '';
	            while (item != null) {
	                if(item.IsHidden === 'True')
	                {
	                    hiddenList.push(item);

	                    i++;
	                    item = FieldList[i];

	                    continue;
	                }

	                var name = item.Name;
	                var des = item.Description;
	                var isBool = item.isBool == 'True';

                    var div = $('<div>');
                    div.attr({'text': name == 'Header' ? des : des.toLowerCase(), 'name': name.toLowerCase(), 'isBool': isBool, 'isHeader': name == 'Header'});
                    document.getElementById('m_FieldList').appendChild(div[0]);
                    var span = $('<span>');

                    if(name == 'Header') {
                        span.addClass('FieldLabel');
                        span.prop("innerText", des + ' ...');
                    }
                    else {
                        var a = $(document.createElement('a'));
                        a.attr({'href': '#', 'des': des, 'name': name});
                        a.prop("innerText", 'select');
                        a.click(function () { applyField(this.getAttribute('des'), this.getAttribute('name')); });
                        span.prop("innerText", '  ' + des + ' (' + name + ')');
                        div.append(a)
                    }

                    div.append(span);
	                i++;
	                item = FieldList[i];
	            }
	        }

	        function applyField(text, id) {
	            // 5/20/2010 dd - David Code Bellow
	            // TODO: Set the description.
	            g_oSelectedPdfField.Name = id;
	            g_oSelectedPdfField.Description = text;
	            if (g_oSelectedPdfField.cssClass == 'pdf-field-text') {
	              $(g_oSelectedPdfFieldElement).find('.pdf-field-content').text(text);
	            } 
	            else if (g_oSelectedPdfField.cssClass == 'pdf-field-signature' || g_oSelectedPdfField.cssClass == 'pdf-field-initial' || g_oSelectedPdfField.cssClass == 'pdf-field-signature-date') {
	                applyFieldText(g_oSelectedPdfFieldElement, text);
	            } 
	            
	            Modal.Hide();
	            setDirty();
	        }

	        function applyFieldText(field, text) {
	            $(field).find('.pdf-field-content-text').text(text);
	        }

	        function deleteSetField() {
	          if (f_removeFields()) {
	            Modal.Hide();
	          }
	        }

	        var g_bIsPreviewClick = false;
	        function onPreviewClick() {
            var pages = pdfEditor.GetPages();
            if (pages.length == 0) {
              alert('No page to preview');
              return;
            }

            var fields = pdfEditor.GetPdfFields();

            var jsonPages = JSON.stringify(pages);
            var jsonFields = JSON.stringify(fields);

            var args = {
              FormId: <%= AspxTools.JsString(FormId) %>,
              Pages: jsonPages,
              Fields: jsonFields
            };
            var result = gService.editor.call("GeneratePreviewPdf", args); // Step 3
            if (!result.error) {

              var previewId = result.value.PreviewId;
              g_bIsPreviewClick = true;
              window.open('ViewCustomPdf.aspx?mode=preview&previewid=' + previewId,  "_parent");
              g_bIsPreviewClick = false;
            } else {
              alert(result.error.UserMessage);
            }


	        }
	        function displaySearchResults(ignoreCriteria) {
	            var elemType;
	            var count = 0;
	            var elemClass = g_oSelectedPdfField.cssClass;
	            if(elemClass.indexOf('pdf-field-text') != -1)
	                elemType = 'text';
	            else if(elemClass.indexOf('pdf-field-checkbox') != -1)
	                elemType = 'checkbox';
	            else if(elemClass.indexOf('pdf-field-signature-date') != -1)
	                elemType = 'signatureDate';
	            else if(elemClass.indexOf('pdf-field-signature') != -1)
	                elemType = 'signature';
	            else
	                elemType = 'initial';

	            if (ignoreCriteria)
	                $('#searchCriteria').val('');
	            var search = $('#searchCriteria').val().toLowerCase();
	            var list = $('#m_FieldList div');

	            var previousHeader = -1;
	            var headerHasVisibleChildren = false;
	            for (var i = 0; i < list.length;  i++) {
	                if (list[i].getAttribute('isHeader') == 'true') {
	                    setVisibility(list, previousHeader, headerHasVisibleChildren);
	                    previousHeader = i;
	                    headerHasVisibleChildren = false;
	                    continue;
	                }

	                if (elemType == 'checkbox' && list[i].getAttribute('isBool') == 'false') {
	                    list[i].style.display = 'none';
                        continue;
	                }

	                var text = list[i].getAttribute('text');
	                var name = list[i].getAttribute('name');

	                if(name.indexOf('signaturedate') != -1) {
	                    list[i].style.display = elemType == 'signatureDate' ? '' : 'none';
	                    continue;
	                }
	                if (name.indexOf('signature') != -1) {
	                    list[i].style.display = elemType == 'signature' ? '' : 'none';
                        continue;
                    }
	                if (name.indexOf('initials') != -1) {
	                    list[i].style.display = elemType == 'initial' ? '' : 'none';
                        continue;
	                }

	                if ((text.indexOf(search) != -1 || name.indexOf(search) != -1) && elemType != 'initial' && elemType != 'signature' && elemType != 'signatureDate') {
	                    list[i].style.display = '';
	                    headerHasVisibleChildren = true;
	                    count++;
	                }
	                else
	                    list[i].style.display = 'none';
	            }
	            setVisibility(list, previousHeader, headerHasVisibleChildren);

	            document.getElementById('m_searchResultCount').innerText = search != '' ? 'Found ' + count + ' results.' : ' ';
	            document.getElementById('m_FieldList').scrollTop = 0;
	        }

	        function jumpToSelected() {
	            displaySearchResults(true);
	            var selection = document.getElementById('m_Group').value;
	            var list = $('#m_FieldList div');
	            for (var i = 0; i < list.length;  i++) {
	                if (list[i].getAttribute('isHeader') != 'true')
	                    continue;

	                if (list[i].getAttribute('text') == selection) {
	                    document.getElementById('m_FieldList').scrollTop = list[i].offsetTop;
	                    return;
	                }
	            }
	        }

	        function setVisibility(list, pos, visibility) {
	            if (pos > -1)
	                list[pos].style.display = visibility ? '' : 'none';
	        }

	        function movePopupLeft(pixels)
	        {
	            var div = $('#DivPopup');
	            div.css('left', div.position().left - pixels);
	        }

	        $(document).on('keyup', f_onkeyup);
            function f_onkeyup(event) {
                if (event.keyCode == 8 || event.keyCode == 46) {
                    f_removeFields();
                }
            }

          function f_removeFields() {
            var list = pdfEditor.GetSelectedPdfFields();
            var ret = false;
            if (list.length > 0) {
              if (confirmRemove()) {
                $.each(list, function(idx, o) { pdfEditor.RemovePdfField(o);});
                setDirty();
                ret = true;
              }
            }
            return ret;
          }
          function f_showProperties(event) {
            var list = pdfEditor.GetSelectedPdfFields();
            if (list.length == 1) {
              _showFieldSelector(event, list[0]);
            }

          }
	        function alignFields(type) {
	          var list = pdfEditor.GetSelectedPdfFields();

	          var anchorObj = $.grep(list, function(elem, idx){
	            return elem.is('.pdf-first-selected');
	          })[0];

	          $.each(list, function(idx, o) {
	            if (o.is('.pdf-first-selected')) {
	              return;
	            } else {
	              switch (type) {
	                case 0: // Align Top
	                  o.css('top', anchorObj.position().top + 'px');
	                  break;
	                case 1: // Align Left
	                  o.css('left', anchorObj.position().left + 'px');
	                  break;
	                case 2: // Align Right
	                  o.css('left', (anchorObj.position().left + anchorObj.width() - o.width()) + 'px');
	                  break;
	                case 3: // Align Bottom
	                  o.css('top', (anchorObj.position().top + anchorObj.height() - o.height()) + 'px');
	                  break;
	                case 4: // Same width
	                  if (o.hasClass('pdf-field-signature') || o.hasClass('pdf-field-initial') || o.hasClass('pdf-field-signature-date')) {
	                    break;
	                  }
	                  o.width(anchorObj.width());
	                  if (o.hasClass('pdf-field-checkbox')) {
	                    o.find("img").width(anchorObj.width());
	                  }
	                  break;
	                case 5: // Same height
	                    if (o.hasClass('pdf-field-signature') || o.hasClass('pdf-field-initial')  || o.hasClass('pdf-field-signature-date')) {
	                    break;
	                  }

	                  o.height(anchorObj.height());
	                  if (o.hasClass('pdf-field-checkbox')) {
	                    o.find("img").height(anchorObj.height());
	                  }
	                  break;
	                case 6: // Same size
	                    if (o.hasClass('pdf-field-signature') || o.hasClass('pdf-field-initial')  || o.hasClass('pdf-field-signature-date')) {
	                    break;
	                  }

	                  o.width(anchorObj.width()).height(anchorObj.height());
	                  if (o.hasClass('pdf-field-checkbox')) {
	                    o.find("img").width(anchorObj.width()).height(anchorObj.height());
	                  }
	                  break;

	              }
	            }
	          });
	          setDirty();
	        }

	        var g_oSelectedPdfField = null;
	        var g_oSelectedPdfFieldElement = null;
          function _onFieldDblClick(evt) {
            var o = evt.currentTarget;
            _showFieldSelector(evt, o);
          }
          function _onPdfFieldSelectedStop(count) {
            if (count <= 0) {
              $.each(['btnRemoveFields','btnProperties','alignTop','alignLeft','alignRight','alignBottom','btnSameWidth','btnSameHeight','btnSameSize'], function (idx, id) {
                document.getElementById(id).disabled = true;
              });
            } else if (count == 1) {
              $.each(['btnRemoveFields','btnProperties'], function (idx, id) {
                document.getElementById(id).disabled = false;
              });
              $.each(['alignTop','alignLeft','alignRight','alignBottom','btnSameWidth','btnSameHeight','btnSameSize'], function(idx, id) {
                document.getElementById(id).disabled = true;
              });
            } else {
              document.getElementById('btnRemoveFields').disabled = false;
              document.getElementById('btnProperties').disabled = true;
              $.each(['alignTop','alignLeft','alignRight','alignBottom','btnSameWidth','btnSameHeight','btnSameSize'], function(idx, id) {
                document.getElementById(id).disabled = false;
              });

            }
          }
          function _showFieldSelector(event, o, x, y) {
            g_oSelectedPdfFieldElement = o;
            g_oSelectedPdfField = $(o).data('pdfFieldData');
            Modal.ShowPopup('DivPopup', null, event, x, y);
            setTimeout("displaySearchResults(true)", 1);
            var isSign = g_oSelectedPdfField.cssClass == 'pdf-field-signature' ||
                g_oSelectedPdfField.cssClass == 'pdf-field-initial'  || g_oSelectedPdfField.cssClass == 'pdf-field-signature-date';
            document.getElementById('m_Group').disabled = isSign;
            document.getElementById('searchCriteria').disabled = isSign;
            document.getElementById('m_newFieldIdBtn').disabled = isSign;
            document.getElementById('m_enableSearch').disabled = isSign;
            document.getElementById('m_disableSearch').disabled = isSign;
            resetCategory();
            $('#m_enableSearch').prop('checked', true);
          }
          function _onFieldMouseOver(evt) {
            var o = evt.currentTarget;
            var data = $(o).data('pdfFieldData');
            if (data.Name != null) {
              <%-- Will try to align the tooltip either on top or bottom of the field. --%>
              $('#FieldTextDiv').text(data.Description + ' (' + data.Name + ')');

              var left = $(o).offset().left;
              var winHeight = $(window).height();
              var top = $(o).offset().top + $(o).height() + 10;
              if ((top + $('#FieldTextDiv').height() + 10) >= winHeight) {
                <%-- Without this check to put tooltip on top then IE will cause infinite resize event --%>
                top = $(o).offset().top - ($('#FieldTextDiv').height() + 10);
              }

              $('#FieldTextDiv').css({left:left,top:top}).show();
              }
          }
          function _onFieldMouseOut(evt) {
            $('#FieldTextDiv').hide();
          }
	        function addAttributes(o, attributes) {
	            for (var attr in attributes) {
	                if (attributes.hasOwnProperty(attr))
	                    o.data(attr, attributes[attr]);
	            }
	        }
          var pdfEditor = null;

          function Save() {
            // Step 1 - Get pages information - if we allow user to rearrange pages and rotate and delete.
            // Step 2- Get field layout.
            // Step 3 - Send to background service to save.
            // Step 4 - Refresh the page.

            var bRedirectoryToListing = false;
            var pages = pdfEditor.GetPages(); // Step 1
            if (pages.length == 0) {
              if (!confirm('The document has no pages. This will cause the document to be deleted. Do you want to continue with delete?')) {
                return false;
              }
              bRedirectoryToListing = true;
            }

            var fields = pdfEditor.GetPdfFields(); // Step 2
            var jsonPages = JSON.stringify(pages);
            var jsonFields = JSON.stringify(fields);

            var args = {
              FormId: <%= AspxTools.JsString(FormId) %>,
              Pages: jsonPages,
              Fields: jsonFields,
              Description: $('#Description').val()
            };
            var result = gService.editor.call("SaveData", args); // Step 3
            if (!result.error) {
              // TODO: Step 4 - Handle refresh screen here.
              resetDirty();
              self.location = self.location;
            }


          }

	        $(document).ready(function() {
	          $('#UploadXmlBtn').click(function(){
                  $( ".dialog-upload" ).css('display', '').dialog({
                      resizable: false,
                      height:240,
                      width: 400,
                      modal: true,
                      buttons: {
                        "Upload": function() {
                            var dto = {
                                Xml : $('#XmlContent').val(),
                                 FormId: <%= AspxTools.JsString(FormId) %>
                            };

                            var result = gService.editor.call('SaveXml', dto);
                            console.log(result.error);
                            if (result.error){
                                alert(result.UserMessage);
                                $( this ).dialog( "close" );

                            }
                            else if (result.value.Status === 'OK') {
                                self.location = self.location;
                            }
                        },
                        Cancel: function() {
                          $( this ).dialog( "close" );
                        }
                      }
                    });
	          });
	            $('input[type="text"], textarea').on('blur', function(){
	                $(document).on('keyup', f_onkeyup);
	            });
	            $('input[type="text"], textarea').on('focus', function(){
	                $(document).off('keyup', f_onkeyup)
	            });

	          resizeTo(1000, 800);
	          var uniqueKey = <%= AspxTools.JsString(Guid.NewGuid()) %>;
	          pdfEditor = new LendersOffice.PdfEditor('PdfEditor', PdfPages, {
          GenerateFullPageSrc : function(o) {  return 'ViewCustomPdf.aspx?formid=' + o.DocId + '&pg=' + o.Page + '&sessionid=' + uniqueKey;},
          OnPageStatusChanged : function() { /* TODO: alert('Implement Page Status Change'); */},
          OnPageModified : function() { setDirty();},
          OnError : function(evt, msg) { alert(msg);},
          OnPdfFieldDrop: function(evt, o, x, y) {_showFieldSelector(evt, o, x, y);},
	          SignatureFieldImgSrc : <%= AspxTools.JsString("Signature.aspx?name=Borrower+Signature") %>,
            InitialFieldImgSrc : <%= AspxTools.JsString("Signature.aspx?name=Coborrower+Initials&mode=initial&t=234") %>,
	          CheckboxFieldImgSrc : <%= AspxTools.JsString(VirtualRoot + "/images/checkbox_15x15_border.png") %>,
	          OnPdfFieldSelectedStop: function(evt, count) { _onPdfFieldSelectedStop(count);},
	          OnRenderPdfFieldContent: function(o) { return _renderPdfFieldContent(o);},
	          PageBottomPadding : 35,
	          EmptyImgSrc : gVirtualRoot + '/images/pixel.gif',
	          OverrideResizeForSignatureFields: true
	          });
	          pdfEditor.Initialize(function() {
	            // Fixed page number properties so it can deserialize correctly.
              $.each(PdfFields, function() {
                pdfEditor.AddPdfField(this);
              });
            });
	          $('.pdf-field-template').draggable({revert:'invalid', helper:'clone', zIndex: 1,
	          start : function(evt, ui) {
	                    ui.helper[0].className += this.className;
                        $('#PdfEditor').css('z-index', '-1' ); //needed for 6 and 7 so the draggable doesnt end in the back of the editor
                    },
                    stop : function() {
                        $('#Pdfditor').css('z-index', '' );
                    }
                });

	          $('#PdfEditor').on('dblclick', '.pdf-pdf-field', _onFieldDblClick)
	                          .on( 'mouseover', '.pdf-pdf-field',_onFieldMouseOver)
	                          .on('mouseout', '.pdf-pdf-field', _onFieldMouseOut);

	          $('#DivPopup').draggable({ handle: 'th'});
	          populateFieldsForSearch();

	          function ToggleVisibility() {
	              pdfEditor.GetFields().each(function() {
	                    var $this = $(this);
	                    var isESignTag = IsESignTag(this);
	                    var show = isESignTag ? ML.CanUseESignTags : true;
	                    $this.toggleClass('disable-selectable', !show);
	                    $this.data("__HIDDEN__", show ? false : true);
	                    $this.css('visibility', show ? '' : 'hidden');
	                    $this.removeClass('ui-selected pdf-first-selected');
	              });
	          }

	          function IsESignTag(item) {
	              var $item = $(item);
	              return $item.hasClass('pdf-field-signature') || $item.hasClass('pdf-field-initial') || $item.hasClass('pdf-field-signature-date');
	          }

	          ToggleVisibility();
	        });

            function CreateESignTag(text) {
                var pdfFieldTextContainer = $('<div>').addClass("pdf-field-content-text-container");
                var pdfFieldText = $('<div>').addClass("pdf-field-content-text").text(text);

                pdfFieldTextContainer.append(pdfFieldText);
                return pdfFieldTextContainer;
            }
	        function _renderPdfFieldContent(o) {
	            if (o.cssClass == 'pdf-field-signature' || o.cssClass == 'pdf-field-initial' || o.cssClass == 'pdf-field-signature-date') {
	                var displayText = o.Description;
	                return CreateESignTag(displayText);
	            } 
	            else if (o.cssClass == 'pdf-field-checkbox') {
	                return $("<img>", {"src":<%= AspxTools.JsString(VirtualRoot + "/images/checkbox_15x15_border.png") %>});
	            } 
	            else if (o.cssClass == 'pdf-field-text') {
	                return $("<div>").text(o.Description);
	            } 
	            else {
	                return null;
	            }
	        }
        </script>
        <form id="form" runat="server">
    	    <table>
    	        <tr>
    	            <td class="MainRightHeader FieldLabel" colspan="6">
    	                Edit PDF Document
    	            </td>
    	        </tr>
    	        <tr>
    	            <td colspan="4" style="padding-left:12px" class="FieldLabel">
    	                Form Description: &nbsp;<asp:TextBox ID="Description" onkeypress="setDirty();" runat="server" />
    	                <input type="button" id="btnPreview" value="Preview PDF" onclick="onPreviewClick();" NoHighlight />
    	            </td>
    	            <td align="right">
    	                <input type="button" value="Upload Xml" id="UploadXmlBtn"  runat="server" />
    	                <input type="button" value="Save" id="save" onclick="Save();" disabled="disabled" NoHighlight />
    	                <input type="button" value="Close" onclick="onCloseClicked();" NoHighlight />
    	            </td>
    	            <td style="width:100px"></td>
    	        </tr>
    	        <tr>
    	            <td colspan="5" style="padding:20px 0px 0px 12px">
    	                Drag and drop fields onto the document. Double-click a field to edit its properties.
    	            </td>
    	        </tr>
    	        <tr>
    	            <td colspan="5" style="padding-left:10px">
    	                <table cellpadding="0" cellpadding="0" border="0">
    	                    <tr align="center" valign="bottom">
    	                        <td>
					                <div id="GenericTextField" class="pdf-field-template pdf-field-text" style="width:100px;height:12px;">
					                  <div class="pdf-field-content">
					                </div>
					                </div>
    	                        </td>
    	                        <td style="width:50px"></td>
    	                        <td>
    	                        	<div id="GenericCheckbox" class="pdf-field-template pdf-field-checkbox" style="width:15px;height:15px;"><img src="~/images/checkbox_15x15_border.png" runat="server" border="0" class="pdf-field-content"/></div>
    	                        </td>
                                <asp:PlaceHolder ID="ESignSection" runat="server">
                                    <td style="width:50px"></td>
    	                            <td>
    	                                <div id="GenericSignature" class="pdf-field-template pdf-field-signature" style="width:200px;height:20px;">
                                            <div class="pdf-field-content pdf-field-content-text-container">
                                                <div class="pdf-field-content-text">Signature</div>
                                            </div>
    	                                </div>
    	                            </td>
    	                            <td style="width:50px"></td>
    	                            <td>
    	                                <div id="GenericInitial" class="pdf-field-template pdf-field-initial" style="width:130px;height:15px;">
                                            <div class="pdf-field-content pdf-field-content-text-container">
                                                <div class="pdf-field-content-text">Initials</div>
                                            </div>
    	                                </div>
    	                            </td>
                                    <td style="width:50px"></td>
                                    <td>
                                        <div id="GenericSignatureDate" class="pdf-field-template pdf-field-signature-date" style="width: 130px; height:15px;">
                                            <div class="pdf-field-content pdf-field-content-text-container">
                                                <div class="pdf-field-content-text">Sig Date</div>
                                            </div>
                                        </div>
                                    </td>
                                </asp:PlaceHolder>
    	                    </tr>
    	                    <tr class="FieldLabel" align="center">
    	                        <td>
    	                            Form Field
    	                        </td>
    	                        <td></td>
    	                        <td>
					                Checkbox
    	                        </td>
                                <asp:PlaceHolder ID="ESignLabels" runat="server">
                                    <td></td>
    	                            <td>
    	                                Signature
					                </td>
    	                            <td></td>
    	                            <td>
    	                                Initials
    	                            </td>
                                    <td></td>
                                    <td>Signed Date</td>
                                </asp:PlaceHolder>
    	                    </tr>
    	                </table>
    	            </td>
    	        </tr>
			    <tr>
			        <td style="padding-top:10px" align="center" class="FieldLabel" nowrap>
			            Align <label style="color:Blue">blue</label> field(s) to the <label style="color:Red">red</label> field
			        </td>
			        <td></td>
			        <td style="padding-top:10px" align="center" class="FieldLabel" nowrap>
			            Resize <label style="color:Blue">blue</label> field(s) to the <label style="color:Red">red</label> field
			        </td>
			        <td colspan="3"></td>
			    </tr>
			    <tr>
			        <td align="center">
			            <input type="button" value="Align top" id="alignTop" onclick="alignFields(0);"  NoHighlight  disabled="disabled"/>
		                <input type="button" value="Align left" id="alignLeft" onclick="alignFields(1);"  NoHighlight  disabled="disabled"/>
		                <input type="button" value="Align right" id="alignRight" onclick="alignFields(2);"  NoHighlight  disabled="disabled"/>
		                <input type="button" value="Align bottom" id="alignBottom" onclick="alignFields(3);"  NoHighlight  disabled="disabled"/>
		            </td>
		            <td></td>
		            <td align="center">
		                <input type="button" value="Same width" id="btnSameWidth" onclick="alignFields(4);"  NoHighlight  disabled="disabled"/>
		                <input type="button" value="Same height" id="btnSameHeight" onclick="alignFields(5);"  NoHighlight  disabled="disabled"/>
		                <input type="button" value="Same size" id="btnSameSize" onclick="alignFields(6);"  NoHighlight  disabled="disabled"/>
		            </td>
		            <td></td>
		            <td align="right">
		                <input type="button" value="Remove field(s)" id="btnRemoveFields" onclick="return f_removeFields();"  NoHighlight disabled="disabled"/>
		                <input type="button" value="Field properties ..." id="btnProperties" onclick="f_showProperties(event); movePopupLeft(300);" NoHighlight disabled="disabled"/>
		            </td>
		            <td></td>
			    </tr>
			    <tr>
			        <td colspan="6" style="padding-left:8px">
                        <div id="PdfEditor"></div>
			        </td>
			    </tr>
		    </table>

	        <div id="DivPopup" class="Modal">
	            <table width="100%" border="0" cellspacing="0" cellpadding="2">
                    <tr>
                        <th class="FormTableHeader" colspan="2">
                            Select Custom Field
                        </th>
                    </tr>
                    <tr>
                        <td class="FieldLabel" nowrap>
                            <input type="radio" id="m_enableSearch" name="searchOption" onclick="defaultCategory(); displaySearchResults(false);" checked />
                            Search by Field Name or ID
                            <input type="text" id="searchCriteria" onfocus="defaultCategory(); $('#m_enableSearch').prop('checked', true);"
                                onkeypress="if(event.keyCode==13) {addNewField();return false;} $('#m_enableSearch').prop('checked', true);" />
                            <input type="button" id="m_newFieldIdBtn" value="Search" onclick="defaultCategory(); $('#m_enableSearch').prop('checked', true); addNewField();"/>
                        </td>
                        <td class="FieldLabel" align="right" nowrap>
                            <a href="#" onclick="window.open('https://support.lendingqb.com/index.php?/Knowledgebase/Article/View/497', 'fieldIDtutorial',
                                'resizable=yes,menubar=no,status=no,scrollbars=yes,width=750px');">How to find a Field ID</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel" colspan="2" nowrap>
                            <input type="radio" id="m_disableSearch" name="searchOption" onclick="displaySearchResults(true);" />
                            Jump to category
                            <ASP:DropDownList id="m_Group" runat="server" onchange="$('#m_disableSearch').prop('checked', true); jumpToSelected();"
						        style="PADDING-LEFT: 4px; BORDER: 1px groove; FONT: 11px arial;">
						    </ASP:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="FieldLabel" align="center">
                            <label id="m_searchResultCount">&nbsp;</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="m_FieldList" style="height:200px; overflow:auto; padding:3px; border:solid 1px black">
		                    </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" class="ButtonStyle" value="Remove Field" onclick="deleteSetField();" />
                        </td>
                        <td align="right">
                            <input type="button" class="ButtonStyle" value="Close" onclick="Modal.Hide();" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="FieldTextDiv" class="Tooltip"></div>
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
        </form>

        <div class="dialog-upload" title="Upload PDF Xml" style="display:none">
            <p>
                <label>XML File
                    <textarea id="XmlContent" cols="50" rows="5" >
                    </textarea>
                </label>

            </p>
        </div>
    </body>
</html>
