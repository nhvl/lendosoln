﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Collections;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using iTextSharp.text;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOffice.PdfForm;
using LendersOffice.Common;
using LendersOffice.PdfLayout;
using EDocs;
using System.Web.UI.HtmlControls;
using LendersOffice.CustomFormField;
using System.Text;

namespace LendersOfficeApp.los.CustomPDFForms
{
    public partial class CustomPDFEditor : LendersOffice.Common.BaseServicePage
    {
        protected Guid FormId
        {
            get { return RequestHelper.GetGuid("id"); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            UploadXmlBtn.Visible = principal.HasPermission(Permission.CanModifyLoanPrograms);

            var docuSignSettings = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(principal.BrokerId);
            var canUseESignTags = principal.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes) && docuSignSettings != null && docuSignSettings.DocuSignEnabled && docuSignSettings.IsSetupComplete();
            this.ESignSection.Visible = canUseESignTags;
            this.ESignLabels.Visible = canUseESignTags;

            this.RegisterJsGlobalVariables("CanUseESignTags", canUseESignTags);

            PdfForm form = PdfForm.LoadById(FormId);
            PdfFormLayout layout = form.Layout;
            Description.Text = form.Description;

            List<PdfPageItem> pdfPages = form.GetPdfPageInfoList();
            List<PdfField> fieldList = new List<PdfField>();
            foreach (PdfField o in layout.FieldList)
            {
                fieldList.Add(o);
            }

            RegisterJsObject("PdfPages", pdfPages);
            RegisterJsObject("PdfFields", fieldList);

            string loanFileFields = string.Format(@"<script type=""text/javascript"">var FieldList = {0}</script>", getFieldList());
            ClientScript.RegisterStartupScript(this.GetType(), "loadList", loanFileFields);
        }

        private static List<CustomPDFFieldItem> GetESignFields()
        {
            List<CustomPDFFieldItem> fieldItems = new List<CustomPDFFieldItem>();

            var agentFormatString = "GetAgentOfRole[{0}].{1}";
            var borrowerFormatString = "a{0}{1}";
            var signatureString = "Signature";
            var initialString = "Initials";
            var signatureDateString = "SignatureDate";

            fieldItems.Add(new CustomPDFFieldItem("Borrower Sig Date", false, string.Format(borrowerFormatString, "B", signatureDateString), false));
            fieldItems.Add(new CustomPDFFieldItem("Coborrower Sig Date", false, string.Format(borrowerFormatString, "C", signatureDateString), false));

            foreach (E_AgentRoleT roleType in LendersOffice.Rolodex.RolodexDB.GetAgentTypeOptions())
            {
                var roleDescription = LendersOffice.Rolodex.RolodexDB.GetTypeDescription(roleType, useShortened: true);
                fieldItems.Add(new CustomPDFFieldItem($"{roleDescription} Signature", false, string.Format(agentFormatString, roleType.ToString("G"), signatureString), false));
                fieldItems.Add(new CustomPDFFieldItem($"{roleDescription} Initials", false, string.Format(agentFormatString, roleType.ToString("G"), initialString), false));
                fieldItems.Add(new CustomPDFFieldItem($"{roleDescription} Sig Date", false, string.Format(agentFormatString, roleType.ToString("G"), signatureDateString), false));
            }
            
            return fieldItems;
        }

        public static string getFieldList()
        {
            
            List<string> categories = new List<string>();
            foreach (FormFieldCategory ffc in FormFieldCategory.ListAll())
                categories.Add(ffc.Name);

            List<List<CustomPDFFieldItem>> data = new List<List<CustomPDFFieldItem>>();
            foreach (string s in categories)
                data.Add(new List<CustomPDFFieldItem>());

            foreach (FormField field in FormField.ListAll())
                data[categories.IndexOf(field.Category.Name)].Add(new CustomPDFFieldItem(field.FriendlyName, field.IsBoolean, field.Id, field.IsHidden));

            categories.Add("E-Sign");
            data.Add(GetESignFields());

StringBuilder jsData = new StringBuilder("[");
            for (int i = 0; i < data.Count; i++)
            {
                if (i != 0)
                {
                    jsData.Append(",");
                }
                jsData.Append("{\"Name\":\"Header\",\"Description\":\"" + categories[i] + "\",\"isBool\":\"True\"}");
                List<CustomPDFFieldItem> items = data[i];
                for (int j = 0; j < items.Count; j++)
                {
                    jsData.Append(", {\"Name\":\"" + items[j].Key + "\",\"Description\":\"" + items[j].Label + "\",\"IsHidden\":\"" + items[j].IsHidden + "\",\"isBool\":\"" + items[j].IsBool + "\"}");
                }
            }
            jsData.Append("];");
            return jsData.ToString();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (false == BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanCreateCustomForms))
            {
                throw new AccessDenied();
            }

            if (this.Header != null)
            {
                //fix jquery ui until we update


                HtmlGenericControl useMetaTagForNow = new HtmlGenericControl("meta");
                useMetaTagForNow.Attributes.Add("http-equiv", "X-UA-Compatible");
                useMetaTagForNow.Attributes.Add("content", "IE=EmulateIE8");
                this.Header.Controls.AddAt(0, useMetaTagForNow);

                EnableJqueryMigrate = false;

                RegisterJsScript("jquery-ui-1.11.4.min.js");
                RegisterCSS("jquery-ui-1.11.custom.css");
                IncludeStyleSheet("~/css/PdfEditor.css");
            }
            this.RegisterJsScript("json.js");
            this.RegisterJsScript("PdfEditor.js");
            this.RegisterJsScript("utilities.js");
            this.RegisterService("editor", "/los/CustomPdfForms/CustomPDFFormService.aspx");
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ClientScript.GetPostBackEventReference(this, "");
        }
    }

    public class CustomPDFFieldItem
    {
        public bool IsBool { get; set; }
        public string Label { get; set; }
        public string Key { get; set; }
        public bool IsHidden { get; set; }

        public CustomPDFFieldItem(string label, bool isBool, string key, bool isHidden)
        {
            IsBool = isBool;
            Label = label;
            Key = key;
            IsHidden = isHidden;
        }
        }
}