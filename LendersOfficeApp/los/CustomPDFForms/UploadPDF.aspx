﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadPDF.aspx.cs" Inherits="LendersOfficeApp.los.CustomPDFForms.UploadPDF" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Upload PDF</title>
	<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	<style type="text/css">
	    .fieldWidth { width: 300px; }
	</style>
	<script language="javascript">
    <!--
	    function refreshOpener() {
	        if ((window.opener) && (!window.opener.closed))
	            window.opener.location.reload(true);
	    }

	    function _init() {
	        resizeTo(450, 180);
	    }

	    function clearMsg() {
	        document.getElementById('m_errorMsg').innerText = ' ';
	    }
    //-->
    </script>
</head>
<body onunload="refreshOpener();">
    <form id="form1" runat="server">
    <table class="FieldLabel">
        <tr>
            <td nowrap style="padding-top:10px">
                Select a file (PDF only)
            </td>
            <td style="padding-top:10px">
                <asp:FileUpload ID="Pdf" runat="server" accept="application/pdf" CssClass="fieldWidth" onchange="clearMsg();" />                
            </td>
        </tr>
        <tr>
            <td>
                Description
            </td>
            <td>
                <asp:TextBox ID="Description" runat="server" CssClass="fieldWidth" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ml:EncodedLabel ID="m_errorMsg" runat="server" ForeColor="Red"></ml:EncodedLabel>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <asp:Button Text="Upload PDF" runat="server" OnClick="OnUploadClicked" />
                <input type="button" onclick="onClosePopup();" value="Close" />
            </td>
        </tr>        
    </table>
	<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
    </form>
</body>
</html>
