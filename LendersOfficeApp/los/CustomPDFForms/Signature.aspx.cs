﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.PdfLayout;
using LendersOffice.Common;

namespace LendersOfficeApp.los.CustomPDFForms
{
    public partial class Signature : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string text = RequestHelper.GetSafeQueryString("name");
            string mode = RequestHelper.GetSafeQueryString("mode");
            byte[] buffer = null;
            if (mode == "initial")
            {
                buffer = SignatureUtilities.ConvertToInitialImage(text, 130, 12);
            }
            else if (mode == "nolinesig")
            {
                buffer = SignatureUtilities.ConvertToInitialImage(text, 200, 20);
            }
            else
            {
                buffer = SignatureUtilities.ConvertToSignatureImage(text, 200, 20);
            }
            RenderPng(buffer);
        }
        private void RenderPng(byte[] buffer)
        {
            Response.Clear();
            Response.ContentType = "image/png";
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            Response.Cache.SetLastModified(DateTime.Now);

            Response.OutputStream.Write(buffer, 0, buffer.Length);
            Response.Flush();
            Response.End();
        }
    }
}
