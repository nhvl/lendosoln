﻿using System;
using LendersOffice.Common;
using LendersOffice.PdfForm;
using DataAccess;

namespace LendersOfficeApp.los.CustomPDFForms
{
    public partial class ViewCustomPdf : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int page = RequestHelper.GetInt("pg", 1);
            Guid formId = RequestHelper.GetGuid("formid", Guid.Empty);
            Guid sessionId = RequestHelper.GetGuid("sessionid", Guid.NewGuid());
            string previewId = RequestHelper.GetSafeQueryString("previewid");

            string mode = RequestHelper.GetSafeQueryString("mode");

            PdfForm pdfForm = null;

            if (formId != Guid.Empty)
            {
                pdfForm = PdfForm.LoadById(formId);
            }
            if (mode == "pdf")
            {
                RenderPdf(pdfForm.PdfContent, pdfForm.Description);
            }
            else if (mode == "preview")
            {
                byte[] content = AutoExpiredTextCache.GetBytesFromCache(previewId);
                RenderPdf(content, "Preview");
            }
            else
            {
                byte[] buffer = pdfForm.GetPng(page - 1, sessionId);

                RenderPng(buffer);
            }
        }
        private void RenderPng(byte[] buffer)
        {
            Response.Clear();
            Response.ContentType = "image/png";
            Response.Cache.SetExpires(DateTime.Now.AddDays(1));
            Response.Cache.SetLastModified(DateTime.Now);

            Response.OutputStream.Write(buffer, 0, buffer.Length);
            Response.Flush();
            Response.End();
        }
        private void RenderPdf(byte[] buffer, string fileName)
        {
            Response.Clear();
            Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
            Response.Cache.SetLastModified(DateTime.Now);
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{fileName}.pdf\"");
            Response.OutputStream.Write(buffer, 0, buffer.Length);
            Response.Flush();
            Response.End();

        }
    }
}
