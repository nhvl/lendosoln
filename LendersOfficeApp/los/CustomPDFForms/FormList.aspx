<%@ Page language="c#" Codebehind="FormList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.CustomPDFForms.FormList" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>FormList</title>
	</HEAD>
	<body  bgcolor="gainsboro">
		<script language="javascript">
        <!--

		var args = 'resizable=yes,menubar=no,status=no';
		var windowHandle = null;

		function checkHandle() {
		    if (windowHandle != null && !windowHandle.closed) {
		        alert('The action cannot be completed.  Please close the custom pdf editor first and try again.');
		        windowHandle.focus();
		        return true;
		    }

		    return false;
		}

		function uploadCustomPDF() {
		    if (checkHandle())
		        return false;

		    windowHandle = window.open('UploadPDF.aspx', 'CustomPDFEditor', args);
        }

        function duplicatePDF(id) {
            if (checkHandle())
                return false;

            document.getElementById('formOp').value = 'duplicate';
            document.getElementById('formId').value = id;
            __doPostBack('', '');
        }

        function replacePDF(id) {
            if (checkHandle())
                return false;

            windowHandle = window.open('UploadPDF.aspx?replace=' + id, 'CustomPDFEditor', args);
        }

        function editPDF(id) {
            if (checkHandle())
                return false;

            windowHandle = window.open('CustomPDFEditor.aspx?id=' + id, 'CustomPDFEditor', args);
        }

        function canDeletePdf(id) {
            return id !== ML.VerbalCreditReportAuthorizationCustomPdfId;
        }

        function deletePDF(id, des) {
            if (!canDeletePdf(id)) {
                alert(<%=AspxTools.JsString(LendersOffice.Common.ErrorMessages.CannotDeleteVerbalAuthorizationCustomPdf)%>);
                return;
            }

            if (checkHandle())
                return false;

            if (!confirm('Delete "' + des + '"?'))
                return;

            document.getElementById('formOp').value = 'delete';
            document.getElementById('formId').value = id;
            __doPostBack('', '');
        }
        //-->
        </script>
        <h4 class="page-header">Custom PDF List</h4>
		<form id="FormList" method="post" runat="server" >
		<asp:HiddenField runat="server" ID="formOp" />
		<asp:HiddenField runat="server" ID="formId" />
			<table id="Table1" cellspacing="0" cellpadding="0" border="0" class="FormTable" style="padding:0 5 0 5; width:100%">
				<tr>
				    <td style="padding:5 0 5 5">
						<input type="button" value="Upload Custom PDF Form ..." onclick="return uploadCustomPDF();" style="width:150px" />				        
				    </td>
				</tr>
				<tr>
				    <td>
				        <ml:CommonDataGrid id="m_CustomPdfGrid" runat="server" ItemStyle-Width="100%">
					        <alternatingitemstyle cssclass="GridAlternatingItem" HorizontalAlign="Center"></alternatingitemstyle>
					        <itemstyle cssclass="GridItem" HorizontalAlign="Center"></itemstyle>
					        <headerstyle cssclass="GridHeader"></headerstyle>
					        <columns>
                                <asp:TemplateColumn HeaderText="Description" ItemStyle-HorizontalAlign="Left">
						            <ItemTemplate>
						               <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString()) %>
						            </ItemTemplate>
						        </asp:TemplateColumn>
								<asp:TemplateColumn>
							        <itemtemplate>
								        <a href="#" onclick="editPDF(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FormId").ToString())%>);" style="width:25px">
								            edit
								        </a>
							        </itemtemplate>
						        </asp:TemplateColumn>
						        <asp:TemplateColumn>
							        <itemtemplate>
								        <a href="#" onclick="deletePDF(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FormId").ToString())%>, 
								            <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Description").ToString())%>);" style="width:38px">
								            delete
								        </a>
							        </itemtemplate>
						        </asp:TemplateColumn>
						        <asp:TemplateColumn>
							        <itemtemplate>
								        <a href="#" onclick="duplicatePDF(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FormId").ToString())%>);" style="width:50px">
								            duplicate
								        </a>
							        </itemtemplate>
						        </asp:TemplateColumn>
						        <asp:TemplateColumn>
							        <itemtemplate>
								        <a href="#" onclick="replacePDF(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FormId").ToString())%>);" style="width:64px">
								            replace PDF
								        </a>
							        </itemtemplate>
						        </asp:TemplateColumn>
					        </columns>
				        </ml:CommonDataGrid>
				    </td>
				</tr>				
				<tr>
					<td align="center" style="padding-top:6px">
					    <input type="button" value="Close" style="width:48px" onclick="onClosePopup();" />
					</td>
				</tr>
			</table>  
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
		</form>
	</body>
</HTML>
