using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Xml;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Linq.Expressions;
using System.Linq;
using System.Collections.Generic;
using LendersOffice.PdfForm;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.los.CustomPDFForms
{
	/// <summary>
	/// Summary description for FormList.
	/// </summary>
	public partial class FormList : LendersOffice.Common.BasePage
	{
		protected void PageLoad(object sender, System.EventArgs e)
		{
            var currentUser = BrokerUserPrincipal.CurrentPrincipal;
            if (!currentUser.HasPermission(Permission.CanCreateCustomForms))
            {
                throw new AccessDenied();
            }

            switch (formOp.Value)
            {
                case "duplicate":
                    duplicate();
                    break;
                case "delete":
                    delete();
                    break;
                default:
                    break;
            }

            formOp.Value = "";
            m_CustomPdfGrid.DataSource = PdfForm.RetrieveCustomFormsOfCurrentBroker();
            m_CustomPdfGrid.DataBind();

            Guid? brokerVerbalAuthorizationCustomPdfId = null;
            if (currentUser.BrokerDB.AllowIndicatingVerbalCreditReportAuthorization)
            {
                brokerVerbalAuthorizationCustomPdfId = currentUser.BrokerDB.VerbalCreditReportAuthorizationCustomPdfId;
            }

            this.RegisterJsGlobalVariables("VerbalCreditReportAuthorizationCustomPdfId", brokerVerbalAuthorizationCustomPdfId?.ToString());
		}

        private void delete()
        {
            var currentUser = BrokerUserPrincipal.CurrentPrincipal;
            if (currentUser.BrokerDB.AllowIndicatingVerbalCreditReportAuthorization &&
                currentUser.BrokerDB.VerbalCreditReportAuthorizationCustomPdfId == this.id)
            {
                var exception = new CBaseException(
                    ErrorMessages.CannotDeleteVerbalAuthorizationCustomPdf, 
                    "User attempted to delete verbal authorization custom form " + this.id);

                ErrorUtilities.DisplayErrorPage(
                    exception,
                    isSendEmail: false,
                    brokerID: currentUser.BrokerId,
                    employeeID: currentUser.EmployeeId);

                return;
            }

            PdfForm.DeleteById(id);
            formId.Value = "";
            Response.Redirect("FormList.aspx");
        }

        private void duplicate() 
        {
            PdfForm duplicate = PdfForm.CreateNew();
            PdfForm original = PdfForm.LoadById(id);
            duplicate.Description = original.Description + " (copy)";
            duplicate.Layout = original.Layout;
            duplicate.PdfContent = original.PdfContent;
            duplicate.Save();
            Response.Redirect("FormList.aspx");
        }

        private Guid id
        {
            get { return new Guid(formId.Value); }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);


		}
		#endregion
	}
}
