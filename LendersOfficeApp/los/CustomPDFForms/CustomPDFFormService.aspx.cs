﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.PdfForm;
using LendersOffice.PdfLayout;
using LendersOffice.Common;
using EDocs;
using LendersOffice.CustomFormField;
using System.Text;
using LendersOffice.Security;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Xml;

namespace LendersOfficeApp.los.CustomPDFForms
{
    public partial class CustomPDFFormService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            if (false == BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanCreateCustomForms))
            {
                throw new AccessDenied();
            }

            switch (methodName) 
            {
                case "SaveData":
                    SaveData();
                    break;
                case "AddField":
                    AddField();
                    break;
                case "GeneratePreviewPdf":
                    GeneratePreviewPdf();
                    break;
                case "SaveXml":
                    SaveXmlData();
                    break;
            }
        }

        private void SaveXmlData()
        {
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanModifyLoanPrograms))
            {
                throw new AccessDenied();
            }

            string xmlData = GetString("Xml");
            string formId = GetString("FormId");
            PdfForm pdfForm = PdfForm.LoadById(new Guid(formId));
            PdfFormLayout layout = new PdfFormLayout(xmlData);
            pdfForm.Layout = layout;
            pdfForm.Save();

            SetResult("Status", "OK");
        }
        private void GeneratePreviewPdf()
        {
            if (false == BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanCreateCustomForms))
            {
                throw new AccessDenied();
            }

            Guid formId = GetGuid("FormId");
            string jsonPages = GetString("Pages");
            string jsonFields = GetString("Fields");

            List<PdfField> fieldList = ObsoleteSerializationHelper.JsonDeserialize<List<PdfField>>(jsonFields);
            List<PdfPageItem> pageList = ObsoleteSerializationHelper.JsonDeserialize<List<PdfPageItem>>(jsonPages);

            Dictionary<int, int> pageOrders = new Dictionary<int, int>();
            bool isPageOrderModified = false;

            for (int i = 0; i < pageList.Count; i++)
            {
                // 5/28/2010 dd - The dictionary will help decipher where the old page numbering be in new location.
                // This is useful to map the field layout, because field layout still contain old page number.
                pageOrders.Add(pageList[i].Page, i + 1);
                if (pageList[i].Page != i + 1 || pageList[i].Rotation != 0)
                {
                    isPageOrderModified = true;
                }
            }

            PdfFormLayout layout = new PdfFormLayout();

            foreach (PdfField o in fieldList)
            {
                o.PageNumber = pageOrders[o.PageNumber];
                layout.Add(o);
            }

            PdfForm pdfForm = PdfForm.LoadById(formId);

            byte[] pdfContent = pdfForm.PdfContent;

            if (isPageOrderModified)
            {
                pdfContent = GetNewPdf(pdfContent, pageList);
            }
            using (MemoryStream stream = new MemoryStream())
            {
                Document document = new Document();
                PdfCopy writer = new PdfCopy(document, stream);
                document.Open();

                PdfFormContainer container = new PdfFormContainer(pdfContent, layout);
                container.IsTestMode = true;
                container.ShouldSkipESignTags = true;
                container.AppendTo(writer);
                document.Close();

                stream.Flush();
                byte[] content = stream.ToArray();

                string id = AutoExpiredTextCache.AddToCache(content, TimeSpan.FromMinutes(10));
                SetResult("PreviewId", id);
            }

        }

        public static byte[] GetNewPdf(byte[] currentContent, List<PdfPageItem> pages)
        {
            PdfReader reader = new PdfReader(currentContent);
            Document document = new Document();
            MemoryStream outputStream = new MemoryStream();
            PdfCopy pdfWriter = new PdfCopy(document, outputStream);
            document.Open();
            foreach (PdfPageItem item in pages)
            {
                if (item.Rotation != 0)
                {
                    int rotation = reader.GetPageRotation(item.Page) + item.Rotation;
                    reader.GetPageN(item.Page).Put(PdfName.ROTATE, new PdfNumber(rotation));
                }
                pdfWriter.AddPage(pdfWriter.GetImportedPage(reader, item.Page));
            }
            document.Close();

            return outputStream.ToArray();
        }
        private void AddField()
        {
            string fieldName = GetString("fieldName");
            bool success = false;
            if (PageDataUtilities.ValidFieldNameFormat(fieldName))
                success = FormField.AddNewFieldToDataBase(fieldName);
            SetResult("Success", success);
            if (success)
                SetResult("FieldList", CustomPDFEditor.getFieldList());
        }

        private void SaveData()
        {

            if (false == BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanCreateCustomForms))
            {
                throw new AccessDenied();
            }

            Guid formId = GetGuid("FormId");
            string jsonPages = GetString("Pages");
            string jsonFields = GetString("Fields");

            List<PdfField> fieldList = ObsoleteSerializationHelper.JsonDeserialize<List<PdfField>>(jsonFields);
            List<PdfPageItem> pageList = ObsoleteSerializationHelper.JsonDeserialize<List<PdfPageItem>>(jsonPages);

            Dictionary<int, int> pageOrders = new Dictionary<int, int>();
            bool isPageOrderModified = false;
            for (int i = 0; i < pageList.Count; i++)
            {
                // 5/28/2010 dd - The dictionary will help decipher where the old page numbering be in new location.
                // This is useful to map the field layout, because field layout still contain old page number.
                pageOrders.Add(pageList[i].Page, i + 1);
                if (pageList[i].Page != i + 1 || pageList[i].Rotation != 0)
                {
                    isPageOrderModified = true;
                }
            }
            if (isPageOrderModified)
            {
                PdfForm.SavePages(formId, pageList);
            }
            PdfFormLayout layout = new PdfFormLayout();
            foreach (PdfField o in fieldList)
            {
                o.PageNumber = pageOrders[o.PageNumber];
                layout.Add(o);
            }


            PdfForm pdfForm = PdfForm.LoadById(formId);

            pdfForm.Layout = layout;
            pdfForm.Description = GetString("Description");
            pdfForm.Save();
        }
    }
}
