﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.PdfForm;
using DataAccess;
using LendersOffice.Common;
using EDocs;
using LendersOffice.Security;

namespace LendersOfficeApp.los.CustomPDFForms
{
    public partial class UploadPDF : LendersOffice.Common.BasePage
    {
        private string id
        {
            get { return RequestHelper.GetSafeQueryString("replace"); }
        }

        protected void Page_Load(object sender, EventArgs args)
        {
            if (false == BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanCreateCustomForms))
            {
                throw new AccessDenied();
            }

            if (!String.IsNullOrEmpty(id) && !IsPostBack)
                Description.Text = PdfForm.LoadById(new Guid(id)).Description;
        }

        protected void OnUploadClicked(object sender, EventArgs args)
        {
            m_errorMsg.Text = "";
            if (!Pdf.HasFile)
                return;

            PdfForm form;
            if (!String.IsNullOrEmpty(id))
                form = PdfForm.LoadById(new Guid(id));
            else
                form = PdfForm.CreateNew();
            
            try
            {
                form.PdfContent = Tools.ReadFully(Pdf.PostedFile.InputStream, Pdf.PostedFile.ContentLength);
            }
            catch (InvalidPDFFileException)
            {
                m_errorMsg.Text = ErrorMessages.CantUploadInvalidPDF(System.IO.Path.GetFileName(Pdf.PostedFile.FileName));
            }
            catch (InsufficientPdfPermissionException)
            {
                m_errorMsg.Text = ErrorMessages.CantUploadProtectedPDF(System.IO.Path.GetFileName(Pdf.PostedFile.FileName));
            }
            catch (CBaseException e)
            {
                m_errorMsg.Text = ErrorMessages.CantUploadUnexpectedReason(System.IO.Path.GetFileName(Pdf.PostedFile.FileName));
                Tools.LogError("Unexpected exception from pdf reader", e);
            }

            if (!String.IsNullOrEmpty(m_errorMsg.Text))
                return;

            form.Description = Description.Text;
            
            form.Save();
            Response.Redirect("CustomPDFEditor.aspx?id=" + form.FormId);
        }

    }
}
