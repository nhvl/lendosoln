﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfigureUcdDelivery.aspx.cs" Inherits="LendersOfficeApp.los.admin.ConfigureUcdDelivery" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>UCD Delivery Configuration</title>
    <style type="text/css">
        .indented {
            margin-left: 2em;
        }
        .BottomButtons {
            width: 100%;
            position: fixed;
            bottom: 0;
            left: 0;
            text-align: center;
            padding-bottom: 2px;
        }
        .ContentBody {
            padding: 4px;
            line-height: 20px;
        }
    </style>
    <script type="text/javascript">
        jQuery(function ($) {
            if (!ML.HasDocMagicVendor) {
                $('.DocMagicDelivery').remove();
            }

            $('#SaveButton').click(
                function () {
                    var args = {
                        AllowLqbDeliveryToFannieMae: $('#AllowLqbDeliveryToFannieMae').prop('checked'),
                        IsFannieMaeTesting: $('#LqbToFannieMaeTesting').prop('checked'),
                        AllowLqbDeliveryToFreddieMac: $('#AllowLqbDeliveryToFreddieMac').prop('checked'),
                        IsFreddieMacTesting: $('#LqbToFreddieMacTesting').prop('checked'),
                        AllowDocMagicDeliveryToFannieMae: $('#AllowDocMagicDeliveryToFannieMae').prop('checked'),
                        AllowDocMagicDeliveryToFreddieMac: $('#AllowDocMagicDeliveryToFreddieMac').prop('checked'),
                        RelationshipWithFreddieMac: $('input[name=RelationshipWithFreddieMac]:checked').val()
                    };

                    gService.save.callAsyncSimple("SaveUcdDeliveryOptions", args, function (result) {
                        if (result.error) {
                            alert(result.UserMessage);
                        }
                        else if (result.value.Status !== "Success") {
                            alert(result.value.Message);
                        }
                        else {
                            parent.LQBPopup.Hide();
                        }
                    });                    
                });

            $('#AllowLqbDeliveryToFannieMae').change(function () {
                changeEnableIntegration($(this), $('#LqbToFannieMaeTestingLabel'), $('#LqbToFannieMaeTesting'));
            }).change();
            $('#AllowLqbDeliveryToFreddieMac').change(function () {
                changeEnableIntegration($(this), $('#LqbToFreddieMacTestingLabel'), $('#LqbToFreddieMacTesting'));
            }).change();

            function changeEnableIntegration($enableCheckbox, $useTestingLabel, $useTestingCheckbox) {
                var integrationEnabled = $enableCheckbox.prop('checked');
                $useTestingLabel.toggleClass('Hidden', !integrationEnabled);
                if (!integrationEnabled) {
                    $useTestingCheckbox.prop('checked', false);
                }
            }
        });
    </script>
</head>
<body>
    <h4 class="page-header">UCD Delivery Configuration</h4>
    <form id="aspform" runat="server">
        <div class="ContentBody">
            <span class="FieldLabel">Allow UCD Delivery...</span>
            <div class="indented">
                <span class="FieldLabel">To Fannie Mae:</span>
                <div class="indented">
                    <label title="LendingQB to Fannie Mae"><input type="checkbox" id="AllowLqbDeliveryToFannieMae" runat="server" />LendingQB direct integration</label>
                    <label id="LqbToFannieMaeTestingLabel" class="Hidden" title="Use Fannie Mae's UCD Test Environment"><input type="checkbox" id="LqbToFannieMaeTesting" runat="server" />Test Environment</label>
                </div>
                <div class="indented DocMagicDelivery">
                    <label title="DocMagic to Fannie Mae"><input type="checkbox" id="AllowDocMagicDeliveryToFannieMae" runat="server" />DocMagic UCD Data Service</label>
                </div>
            </div>
            <div class="indented">
                <span class="FieldLabel">To Freddie Mac:</span>
                <div class="indented" id="LqbFreddieMacContainer" runat="server">
                    <label title="LendingQB to Freddie Mac"><input type="checkbox" id="AllowLqbDeliveryToFreddieMac" runat="server" />LendingQB direct integration</label>
                    <label id="LqbToFreddieMacTestingLabel" class="Hidden" title="Use Freddie Mac's UCD Test Environment"><input type="checkbox" id="LqbToFreddieMacTesting" runat="server" />Test Environment</label>
                </div>
                <div class="indented DocMagicDelivery">
                    <label title="DocMagic to Freddie Mac"><input type="checkbox" id="AllowDocMagicDeliveryToFreddieMac" runat="server" />DocMagic UCD Data Service</label>
                </div>
                <div class="indented">
                    <span>Relationship with Freddie Mac:</span>
                    <label><input type="radio" name="RelationshipWithFreddieMac" id="RelationshipWithFreddieMac_Seller" value="Seller" runat="server" />Seller</label>
                    <label><input type="radio" name="RelationshipWithFreddieMac" id="RelationshipWithFreddieMac_Correspondent" value="Correspondent" runat="server" />Correspondent</label>
                </div>
            </div>
        </div>
        <div class="BottomButtons">
            <input type="button" value="Save" id="SaveButton"/>
            <input type="button" value="Cancel" onclick="parent.LQBPopup.Return()" />
        </div>
    </form>
</body>
</html>
