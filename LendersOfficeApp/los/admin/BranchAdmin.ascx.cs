namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.TextImport;
    using LendersOffice.Constants;
    using LendersOffice.CustomPmlFields;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ObjLib.ConsumerPortal;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    public partial  class BranchAdmin : System.Web.UI.UserControl
	{
		#region variables
		protected System.Web.UI.WebControls.Button                      m_Update;
		private BranchDescs												m_Branches = new BranchDescs();
		private Guid													m_BrokerId = Guid.Empty;
		private Guid													m_BranchId = Guid.Empty;
		protected System.Web.UI.WebControls.Button						m_ShowUsers;
		private Guid													m_EditorId = Guid.Empty;
		private Boolean													m_IsInternal = false;
        private List<BranchGroup>                                       m_BranchGroups = new List<BranchGroup>();
        private BrokerDB                                                m_broker;
        protected int selectedTabIndex;
        protected string tabDisplay = "none";
        protected string displayBranchInfo = "";

		#endregion

		private String ErrorMessage
		{
			set { Page.ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." ); }
		}

		public Boolean IsInternal
		{
			set { m_IsInternal = value; }	
			get { return m_IsInternal; }
		}

		private String CommandToDo
		{
			set { Page.ClientScript.RegisterHiddenField( "m_commandToDo" , value ); }
		}

		private Boolean IsDirty
		{
			set
			{
				if( value == true )
					Page.ClientScript.RegisterHiddenField( "m_isDirty" , "1" );
				else
					Page.ClientScript.RegisterHiddenField( "m_isDirty" , "0" );
			}
		}

		public Guid DataSource
		{
			get { return m_BranchId; }
			set { ViewState.Add( "BranchId" , m_BranchId = value ); }
		}

		public Guid DataBroker
		{
			get { return m_BrokerId; }
			set { ViewState.Add( "BrokerId" , m_BrokerId = value ); }
		}

		public Guid DataEditor
		{
			set { ViewState.Add( "EditorId" , m_EditorId = value ); }
		}
        private BrokerDB Broker
        {
            get
            {
                if (m_broker == null)
                {
                    m_broker = BrokerDB.RetrieveById(DataBroker);
                }
                return m_broker;
            }
        }
        protected bool ShowVendorLogins
        {
            get { return E_BrokerBillingVersion.PerTransaction == Broker.BillingVersion; }
        }

        private bool? m_docmagicEnabled = null;
        protected bool DocMagicEnabled
        {
            get
            {
                if (!m_docmagicEnabled.HasValue)
                {
                    m_docmagicEnabled = DocumentVendorFactory.CreateVendorsFor(m_BrokerId).Exists(v => v.Config.VendorId == Guid.Empty);
                }
                return m_docmagicEnabled.Value;
            }
        }

        protected bool IsEnabledMIVendorIntegration
        {
            get
            {
                return this.Broker.HasMIVendorIntegration;
            }
        }

		/// <summary>
		/// Initialize this control.
		/// </summary>
		protected void ControlInit( object sender , System.EventArgs a )
		{
			try
			{
                var page = this.Parent.Page as BasePage;
                page.RegisterJsScript("ServiceCredential.js");
                page.RegisterCSS("ServiceCredential.css");
                page.RegisterJsScript("LQBPopup.js");

                // Setup the auto zip locator.
                m_Zipcode.SmartZipcode( m_City , m_State );

                this.LegalEntityIdentifierValidator.ValidationExpression = LqbGrammar.DataTypes.RegularExpressionString.LegalEntityIdentifier.ToString();
                this.LegalEntityIdentifierValidator.ErrorMessage = ErrorMessages.InvalidLegalEntityIdentifier;
            }
            catch (Exception e)
			{
				Tools.LogError( ErrorMessage = "Failed to initialize web control." , e );
			}            
		}

		/// <summary>
		/// Load this control.
		/// </summary>

        protected void ControlLoad(object sender, System.EventArgs a)
        {
            ((BasePage)Page).RegisterCSS("Tabs.css");
            ((BasePage)Page).RegisterCSS("stylesheet.css");

            // Initialize this control.            
            try
			{
				// Get identifiers from the view state.  Also load the branches.

				if( ViewState[ "Branches" ] != null )
					m_Branches = ViewState[ "Branches" ] as BranchDescs;

				if( ViewState[ "EditorId" ] != null )
					m_EditorId = ( Guid ) ViewState[ "EditorId" ];

                if (ViewState["BrokerId"] != null)
                {
                    m_BrokerId = (Guid)ViewState["BrokerId"];
                    FhaLenderIdNineValidator.Enabled = Broker.UseFHATOTALProductionAccount;
                }

                ((BaseServicePage)Page).RegisterJsGlobalVariables("brokerLegalEntityIdentifier", this.Broker.LegalEntityIdentifier);

                if ( ViewState[ "BranchId" ] != null )
					m_BranchId = ( Guid ) ViewState[ "BranchId" ];

                if (ViewState["BranchGroups"] != null)
                    m_BranchGroups = (List<BranchGroup>) ViewState["BranchGroups"];

                this.tab3.Visible = this.Broker.EnableRetailTpoPortalMode;

                if (!Page.IsPostBack)
                {
                    m_DocumentVendorLogins.DataSource = DocumentVendorFactory.CreateVendorsFor(m_BrokerId);
                    m_DocumentVendorLogins.DataBind();
                }

                var docusignSettings = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(this.DataBroker);
                ((BasePage)this.Page).RegisterJsGlobalVariables(
                    "DocuSignEnabled",
                    docusignSettings != null && docusignSettings.DocuSignEnabled && docusignSettings.IsSetupComplete() && !string.IsNullOrEmpty(docusignSettings.DocuSignUserId));

                if (this.Broker.IsCustomPricingPolicyFieldEnabled)
                {
                    CustomLOPricingPolicyPlaceHolder.Visible = true;
                    this.BuildLoanOfficerPricingPolicyTable(this.Broker.BrokerID);
                }
                else
                {
                    CustomLOPricingPolicyPlaceHolder.Visible = false;
                }
			}
			catch( Exception e )
			{
				// Oops!
				Tools.LogError( ErrorMessage = "Failed to load web control." , e );
			}
		}
			
		/// <summary>
		/// Render this control.
		/// </summary>

		protected void ControlPreRender( object sender , System.EventArgs a )
		{
			// Render this control.

			try
			{
				// Bind the latest changes to this list.  Make note of
				// any dirty entries.

                BranchDesc branch = m_Branches[m_BranchId];

				ViewState.Add( "Branches" , m_Branches );
				m_Grid.DataSource = ViewGenerator.Generate( m_Branches.Items.Where(bD => bD.Status == BranchStatus.Active).ToList() );
                m_Grid.DataBind();

                m_InactiveGrid.DataSource = ViewGenerator.Generate(m_Branches.Items.Where(bD => bD.Status == BranchStatus.Inactive).ToList());
                m_InactiveGrid.DataBind();

                ViewState.Add("BranchGroups", m_BranchGroups);
                m_BranchGroupGrid.DataSource = ViewGenerator.Generate(m_BranchGroups);
                m_BranchGroupGrid.DataBind();

                if (branch != null)
                {
                    Page.ClientScript.RegisterHiddenField("BranchId", m_BranchId.ToString());
                    Page.ClientScript.RegisterHiddenField("DefaultDisplayName", branch.DisplayNm);
                    Page.ClientScript.RegisterHiddenField("DefaultStreet", branch.Street);
                    Page.ClientScript.RegisterHiddenField("DefaultCity", branch.City);
                    Page.ClientScript.RegisterHiddenField("DefaultAddrState", branch.State);
                    Page.ClientScript.RegisterHiddenField("DefaultZip", branch.Zipcode);
                    Page.ClientScript.RegisterHiddenField("DefaultPhone", branch.Phone);
                }

                if(m_DeleteSelectedBranch.Visible || m_SelectedBranch.Visible)
                {
                    tabDisplay = "";
                }

                if (this.Broker.IsCustomPricingPolicyFieldEnabled)
                {
                    this.PopulateHiddenFieldWithPricingPolicyInfo(this.Broker, branch);
                }
			}
            catch (Exception e)
            {
                Tools.LogError(ErrorMessage = "Failed to render web control.", e);
            }

            this.SetServiceCredentials(this.m_BranchId);
		}

        private void PopulateHiddenFieldWithPricingPolicyInfo(BrokerDB brokerDB, BranchDesc branch)
        {
            var customPricingPolicyFields = this.GetCustomPricingPolicyInfoForBranch(brokerDB, branch);
            this.CustomLOPPFieldSettings.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize<List<CustomPmlFieldInfo>>(customPricingPolicyFields);
        }

        private List<CustomPmlFieldInfo> GetCustomPricingPolicyInfoForBranch(BrokerDB brokerDB, BranchDesc branch)
        {
            var customPricingPolicyFields = new List<CustomPmlFieldInfo>();
            if (brokerDB.CustomPricingPolicyFieldList.IsFieldDefined(1))
            {
                customPricingPolicyFields.Add(new CustomPmlFieldInfo() { Id = "CustomPricingPolicyField1", Value = branch != null ? branch.CustomPricingPolicyField1 : "" });
            }
            if (brokerDB.CustomPricingPolicyFieldList.IsFieldDefined(2))
            {
                customPricingPolicyFields.Add(new CustomPmlFieldInfo() { Id = "CustomPricingPolicyField2", Value = branch != null ? branch.CustomPricingPolicyField2 : "" });
            }
            if (brokerDB.CustomPricingPolicyFieldList.IsFieldDefined(3))
            {
                customPricingPolicyFields.Add(new CustomPmlFieldInfo() { Id = "CustomPricingPolicyField3", Value = branch != null ? branch.CustomPricingPolicyField3 : "" });
            }
            if (brokerDB.CustomPricingPolicyFieldList.IsFieldDefined(4))
            {
                customPricingPolicyFields.Add(new CustomPmlFieldInfo() { Id = "CustomPricingPolicyField4", Value = branch != null ? branch.CustomPricingPolicyField4 : "" });
            }
            if (brokerDB.CustomPricingPolicyFieldList.IsFieldDefined(5))
            {
                customPricingPolicyFields.Add(new CustomPmlFieldInfo() { Id = "CustomPricingPolicyField5", Value = branch != null ? branch.CustomPricingPolicyField5 : "" });
            }
            return customPricingPolicyFields;
        }

        protected void DocumentVendorBind(object sender, RepeaterItemEventArgs e)
        {
            IDocumentVendor vendor = (IDocumentVendor)e.Item.DataItem;

            HiddenField VendorIdField = (HiddenField)e.Item.FindControl("VendorId");
            Label VendorName = (Label)e.Item.FindControl("VendorName");
            PlaceHolder UsernamePanel = (PlaceHolder)e.Item.FindControl("UsernamePanel");
            PlaceHolder PasswordPanel = (PlaceHolder)e.Item.FindControl("PasswordPanel");
            PlaceHolder CompanyIdPanel = (PlaceHolder)e.Item.FindControl("CompanyIdPanel");
            
            VendorIdField.Value = vendor.Config.VendorId.ToString();
            VendorName.Text = vendor.Skin.VendorName;
            UsernamePanel.Visible = vendor.Config.UsesUsername;
            PasswordPanel.Visible = vendor.Config.UsesPassword;
            CompanyIdPanel.Visible = vendor.Config.UsesAccountId;
        }

		/// <summary>
		/// Bind this control.
		/// </summary>

		public override void DataBind()
		{
			// Bind to the given details.

            // Load the list of branches from the database.  Each
            // branch is editable after we return from postback.
            SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", m_BrokerId)
                                            };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_BrokerId, "ListBranchByBrokerId", parameters))
            {
                m_Branches.Clear();

                while (reader.Read())
                {
                    BranchDesc bD = new BranchDesc();
                    bD.Contents = reader;
                    bD.IsSaved = true;
                    m_Branches.Add(bD);
                }
            }
            foreach (BranchDesc bD in m_Branches)
            {
                //bind logins
                SqlParameter[] documentVendorParameters = {
                                                                  new SqlParameter("@BranchId", bD.BranchId)
                                                              };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_BrokerId, "DOCUMENT_VENDOR_ListCredentialsByBranchId", documentVendorParameters))
                {
                    while (reader.Read())
                    {
                        Guid VendorId = (Guid)reader["VendorId"];
                        string Login = reader["Login"].ToString();
                        string Password = reader["Password"].ToString();
                        string AccountId = reader["AccountId"].ToString();

                        // We should not need to update passwords when binding 
                        // from the database, as there should not be duplicate
                        // vendor IDs.
                        bD.StoreDocumentVendorCredentials(VendorId, Login, Password, AccountId, updatePassword: false);
                    }
                }
            }
            if (!Page.IsPostBack)
            {
                Tools.Bind_sBranchChannelT(m_BranchChannelT);
                Tools.Bind_BranchStatus(m_BranchStatus);

                foreach (var division in Broker.BranchDivisions)
                {
                    m_Division.Items.Add(new ListItem(division, division));
                }
                m_Division.Items.Insert(0, "");

                if (this.Broker.EnableRetailTpoPortalMode)
                {
                    var configs = LendersOffice.ObjLib.TPO.TPOPortalConfig.RetrieveAllActiveAtBroker(PrincipalFactory.CurrentPrincipal.BrokerId);
                    Tools.Bind_TPOLandingPages(this.RetailTpoLandingPageList, configs);
                }
            }
            BindGroupings();

            // 8/4/2005 kb - Get all the enabled pricing groups for
            // this broker.  We autoselect the current one for this
            // user after we initialize.  Visibility depends on the
            // broker's feature set.  We re-bind the dropdown with
            // each branch so we don't have to show all disabled
            // groups -- only the current one if it's disabled.


            BrokerFeatures bFs = new BrokerFeatures(m_BrokerId);

            if (bFs.HasFeature(E_BrokerFeatureT.PricingEngine) || bFs.HasFeature(E_BrokerFeatureT.PriceMyLoan))
            {
                m_LpePanel.Visible = true;
            }
            else
            {
                m_LpePanel.Visible = false;
            }
		}

        protected void BuildLoanOfficerPricingPolicyTable(Guid brokerId)
        {
            var htmlTableRows = CustomPmlFieldUtilities.GetLoanOfficerPricingTableRows(brokerId);

            foreach (var row in htmlTableRows)
            {
                this.CustomLOPricingPolicyTable.Rows.Add(row);
            }
        }

        private void BindGroupings()
        {
            // Use the Group data layer to load up all branch groups of this lender.
            // Might want to redesign this interface to do this in batch if slows.

            Dictionary<Guid, string> branchGroupDict = new Dictionary<Guid, string>();
            Dictionary<Guid, List<Guid>> branchGroupMembership = new Dictionary<Guid, List<Guid>>();
            m_BranchGroups.Clear();
            foreach (var group in GroupDB.GetAllGroups(m_BrokerId, GroupType.Branch))
            {
                branchGroupDict.Add(group.GroupId, group.GroupName);
                m_BranchGroups.Add(new BranchGroup() { Id = group.GroupId, Description = group.Description, Name = group.GroupName });
            }

            // Setup the branch membership.
            foreach (Guid groupId in branchGroupDict.Keys)
            {
                foreach (var membership in GroupDB.GetBranchesWithGroupMembership(m_BrokerId, groupId))
                {
                    if (membership.IsInGroup == 1)
                    {
                        // Found one
                        Guid branchId = membership.BranchId;

                        if (!branchGroupMembership.ContainsKey(branchId))
                        {
                            branchGroupMembership.Add(branchId, new List<Guid>());
                        }
                        branchGroupMembership[branchId].Add(groupId);
                    }

                }
            }

            // For each branch, load up the members
            foreach (BranchDesc desc in m_Branches)
            {
                if (branchGroupMembership.ContainsKey(desc.BranchId))
                {
                    List<Guid> groups = branchGroupMembership[desc.BranchId];
                    if (groups != null)
                    {
                        string groupNameList = string.Empty;
                        string groupIdList = string.Empty;
                        foreach (Guid groupId in groups)
                        {
                            groupIdList += (groupIdList != string.Empty ? "," : "") + groupId;
                            groupNameList += (groupNameList != string.Empty ? "," : "") + branchGroupDict[groupId];
                        }
                        desc.BranchGroupList = groupIdList;
                        desc.BranchGroupNameList = groupNameList;
                    }
                }
            }
        }

		/// <summary>
		/// Check the current edit form and highlight missing/invalid
		/// entries.
		/// </summary>

		private void ValidateForm()
		{
			// Check the branch entry form and highlight errors.

			m_Name.BackColor    = Color.White;
			m_Street.BackColor  = Color.White;
			m_City.BackColor    = Color.White;
			m_State.BackColor   = Color.White;
			m_Zipcode.BackColor = Color.White;
			m_Phone.BackColor   = Color.White;

			if( m_Name.Text == "" )
			{
				m_Name.BackColor = Color.Pink;
				throw new ArgumentException( "Enter a valid branch name." );
			}

			if( m_Street.Text == "" )
			{
				m_Street.BackColor = Color.Pink;
				throw new ArgumentException( "Enter a valid street address." );
			}

			if( m_City.Text == "" )
			{
				m_City.BackColor = Color.Pink;
				throw new ArgumentException( "Enter a valid city." );
			}

			if( m_State.Value == "" )
			{
				m_State.BackColor = Color.Pink;
				throw new ArgumentException( "Enter a valid state." );
			}

			if( m_Zipcode.Text == "" )
			{
				m_Zipcode.BackColor = Color.Pink;
				throw new ArgumentException( "Enter a valid zipcode." );
			}

            if (m_Phone.Text == "")
            {
                m_Phone.BackColor = Color.Pink;
                throw new ArgumentException("Enter a valid phone number.");
            }

            if (m_txtDisplayNm.Text == "")
			{                
				throw new ArgumentException( "Enter a valid display name." );
			}

            if (m_BranchGroups.Count != 0 && BranchGroupsList.Value == string.Empty)
            {
                throw new ArgumentException("Choose at least one Branch Group.");
            }

            if (m_isFhaLenderIDModified.Checked)
            {
                var branchFhaLenderID = m_fhaLenderID.Text;
                if (branchFhaLenderID == "" || Regex.IsMatch(branchFhaLenderID, @"^\d{10}$"))
                {
                }
                else
                {
                    throw new ArgumentException("Lender ID must be 10 digits or blank.");
                }

                if (FhaLenderIdNineValidator.Enabled) // when Broker.UseFHATOTALProductionAccount
                {
                    if (m_fhaLenderID.Text.StartsWith("9"))
                    {
                        throw new ArgumentException("Lender ID must not start with 9.");
                    }
                }
            }

                //OPM 24668 jk 1-23-09 - Doesn't allow branches to be saved if the name already exists
            foreach (BranchDesc b in m_Branches)
            {
                if (m_Name.Text.TrimWhitespaceAndBOM().Equals(b.Name) && m_BranchId != b.BranchId)
                {
                    m_Name.BackColor = Color.Pink;
                    throw new ArgumentException("Branch name already exists.");
                }
            }
			
		}

		/// <summary>
		/// Commit the UI's state back to the listed branches.
		/// </summary>

		private void SaveData()
		{
            // Saved branches that are dirty just need to be updated.
            // Dirty non-saved branches need to be created.
            Guid? currentBranchDescKey = null;
			if( m_SelectedBranch.Visible == true )
			{
				
				// Locate the existing entry and overwrite.

				BranchDesc bD;
				
				ValidateForm();
				
                
				if( m_BranchId != Guid.Empty )
				{
					bD = m_Branches[ m_BranchId ];
					
                    if (bD == null)
                        throw new InvalidOperationException("Unable to find cached branch.");
                    
				}
				else
				{
					
					m_Branches.Add( bD = new BranchDesc() );
					
					bD.BranchId = Guid.NewGuid();
				}

                currentBranchDescKey = bD.BranchId;

				bD.Name       = m_Name.Text.TrimWhitespaceAndBOM();
				bD.Street     = m_Street.Text.TrimWhitespaceAndBOM();
				bD.City       = m_City.Text.TrimWhitespaceAndBOM();
				bD.State      = m_State.Value.TrimWhitespaceAndBOM();
				bD.Zipcode    = m_Zipcode.Text.TrimWhitespaceAndBOM();
				bD.Phone      = m_Phone.Text.TrimWhitespaceAndBOM();
				bD.Fax        = m_Fax.Text.TrimWhitespaceAndBOM();
				bD.BranchCode = m_BranchCode.Text.TrimWhitespaceAndBOM();
				bD.Division   = m_Division.Text.TrimWhitespaceAndBOM();
                bD.DisplayNm = m_txtDisplayNm.Text.TrimWhitespaceAndBOM();
                bD.IsDisplayNmModified = m_chkDisplayNmModified.Checked;
                bD.BranchGroupList = BranchGroupsList.Value;
                bD.BranchChannelT = (E_BranchChannelT)Convert.ToInt32(m_BranchChannelT.SelectedValue);
                bD.Status = (BranchStatus)Convert.ToInt32(m_BranchStatus.SelectedValue);

                if (this.Broker.EnableRetailTpoPortalMode)
                {
                    var selectedRetailLandingPageId = this.RetailTpoLandingPageList.SelectedValue;
                    if (selectedRetailLandingPageId == "none")
                    {
                        bD.RetailTpoLandingPageId = null;
                    }
                    else
                    {
                        bD.RetailTpoLandingPageId = Guid.Parse(selectedRetailLandingPageId);
                    }

                    bD.EnabledCustomPortalPages = SerializationHelper.JsonNetDeserialize<List<Guid>>(this.CustomPortalPageSettings.Value);
                }
				
                bD.IsUseBranchInfoForLoans = chkUseBranchInfoForLoans.Checked;
                bD.LosIdentifier = LicensesPanel.LosIdentifier;
                bD.LicenseInformationList = LicensesPanel.LicenseList;

                
                bD.IsFhaLenderIDModified = m_isFhaLenderIDModified.Checked;
                if (bD.IsFhaLenderIDModified)
                {
                    bD.FhaLenderID = m_fhaLenderID.Text.TrimWhitespaceAndBOM();
                }

                bD.PopulateFhaAddendumLines = m_PopulateFhaAddendumLines.Checked;

                bD.IsLegalEntityIdentifierModified = this.IsLegalEntityIdentifierModified.Checked;
                bD.LegalEntityIdentifier = this.LegalEntityIdentifier.Text;

                foreach (RepeaterItem item in m_DocumentVendorLogins.Items)
                {
                    HiddenField VendorIdField = (HiddenField)item.FindControl("VendorId");
                    Guid vendorId = new Guid(VendorIdField.Value);
                    TextBox UsernameField = (TextBox)item.FindControl("m_Username");
                    TextBox PasswordField = (TextBox)item.FindControl("m_Password");
                    TextBox CompanyIdField = (TextBox)item.FindControl("m_CompanyId");

                    var updatePassword = PasswordField.Text != ConstAppDavid.FakePasswordDisplay;

                    bD.StoreDocumentVendorCredentials(vendorId, UsernameField.Text, EncryptionHelper.Encrypt(PasswordField.Text), CompanyIdField.Text, updatePassword);

                    if (string.IsNullOrEmpty(PasswordField.Text)) PasswordField.Attributes.Add("value", "");
                    else PasswordField.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
                }

                LicensesPanel.Disabled = !chkUseBranchInfoForLoans.Checked;
				if( m_BranchLpePriceGroupIdDefault.SelectedIndex > 0 )
					bD.BranchLpePriceGroupIdDefault = new Guid( m_BranchLpePriceGroupIdDefault.SelectedItem.Value );
				else
					bD.BranchLpePriceGroupIdDefault = Guid.Empty;
				
                if (IsInternal)
                {
                    bD.IsAllowLOEditLoanUntilUnderwriting = m_IsAllowLOEditLoanUntilUnderwriting.Checked;
                }

                if (Broker.IsEnableNewConsumerPortal)
                {
                    Guid cid = Guid.Empty;
                    if (ConsumerPortalPicker.SelectedValue != "-1" && !string.IsNullOrEmpty(ConsumerPortalPicker.SelectedValue))
                    {
                        cid = new Guid(ConsumerPortalPicker.SelectedValue);
                    }
                    bD.ConsumerPortalId = cid;
                }

                if (this.Broker.IsCustomPricingPolicyFieldEnabled)
                {
                    SetCustomLOPricingPolicyFields(bD);
                }

				bD.IsDirty = true;
				
			}

            Guid? actualBranchId = null;
			using( CStoredProcedureExec spExec = new CStoredProcedureExec(m_BrokerId) )
			{
				
				spExec.BeginTransactionForWrite();
				
				try
				{
					
					foreach( BranchDesc bD in m_Branches )
					{
						
						if( bD.IsDirty == true )
						{
							
							// Load and save back.  We update our cache's dirty
							// bits after we have successfully committed.

							BranchDB branchD = new BranchDB();
							
							if( bD.IsSaved == true )
								branchD = new BranchDB( bD.BranchId , m_BrokerId );
							else
								branchD.BrokerID = m_BrokerId;
							
							branchD.Retrieve( spExec );
							
							branchD.Name				  = bD.Name;
							branchD.BranchCode			  = bD.BranchCode;
                            branchD.Division              = bD.Division;
							branchD.Address.StreetAddress = bD.Street;
							branchD.Address.City          = bD.City;
							branchD.Address.State         = bD.State;
							branchD.Address.Zipcode       = bD.Zipcode;
							branchD.Phone				  = bD.Phone;
							branchD.Fax					  = bD.Fax;
							branchD.BranchLpePriceGroupIdDefault = bD.BranchLpePriceGroupIdDefault;

                            branchD.DisplayNm = bD.DisplayNm;
                            branchD.IsDisplayNmModified = bD.IsDisplayNmModified;
                            branchD.IsUseBranchInfoForLoans = bD.IsUseBranchInfoForLoans;
                            branchD.LosIdentifier = bD.LosIdentifier;
                            branchD.LicenseXmlContent = bD.LicenseInformationList.ToString();
                            branchD.BranchChannelT = bD.BranchChannelT;
                            branchD.Status = bD.Status;

                            if (bD.ConsumerPortalId != Guid.Empty)
                            {
                                branchD.ConsumerPortalId = bD.ConsumerPortalId;
                            }
                            if (IsInternal)
                            {
                                branchD.IsAllowLOEditLoanUntilUnderwriting = bD.IsAllowLOEditLoanUntilUnderwriting;
                            }
                            branchD.FhaLenderID = bD.FhaLenderID;
                            branchD.IsFhaLenderIDModified = bD.IsFhaLenderIDModified;
                            branchD.PopulateFhaAddendumLines = bD.PopulateFhaAddendumLines;

                            branchD.IsLegalEntityIdentifierModified = bD.IsLegalEntityIdentifierModified;
                            branchD.LegalEntityIdentifier = bD.LegalEntityIdentifier;

                            branchD.CustomPricingPolicyField1 = bD.CustomPricingPolicyField1;
                            branchD.CustomPricingPolicyField2 = bD.CustomPricingPolicyField2;
                            branchD.CustomPricingPolicyField3 = bD.CustomPricingPolicyField3;
                            branchD.CustomPricingPolicyField4 = bD.CustomPricingPolicyField4;
                            branchD.CustomPricingPolicyField5 = bD.CustomPricingPolicyField5;

                            if (this.Broker.EnableRetailTpoPortalMode)
                            {
                                branchD.RetailTpoLandingPageId = bD.RetailTpoLandingPageId;
                            }

							branchD.Save( spExec );
                            
                            var credentials = new List<DocumentVendorLoginCredentials>(bD.DocumentVendorLoginCredentials.Values);
                            branchD.SaveDocumentVendorCredentials(spExec, credentials);

                            if (this.Broker.EnableRetailTpoPortalMode)
                            {
                                var branchCustomPortalPages = new BranchEnabledRetailPortalCustomPage(branchD.BrokerID, branchD.BranchID);
                                branchCustomPortalPages.SetEnabledPages(spExec, bD.EnabledCustomPortalPages);
                            }

                            if (currentBranchDescKey.HasValue && bD.BranchId == currentBranchDescKey.Value)
                            {
                                actualBranchId = branchD.BranchID;
                            }

                            if (bD.IsSaved == false )
                            {
                                bD.BranchId = branchD.BranchID;
                            }
						}
					}
				}
				catch
				{
					
					spExec.RollbackTransaction();
					throw;
				}
				
				spExec.CommitTransaction();
			}

			
			foreach( BranchDesc bD in m_Branches )
			{

                if (bD.IsDirty == true)
                {
                    if ( bD.BranchGroupList != string.Empty)
                    GroupDB.UpdateBranchGroups(m_BrokerId, bD.BranchId, bD.BranchGroupList, PrincipalFactory.CurrentPrincipal);

                    bD.IsDirty = false;
                }
				
				bD.IsSaved = true;
			}

            if (actualBranchId.HasValue)
            {
                this.m_BranchId = actualBranchId.Value;
            }
        }

        private void SetCustomLOPricingPolicyFields(BranchDesc branch)
        {
            var serializedFieldInfo = this.CustomLOPPFieldSettings.Value;

            if (string.IsNullOrEmpty(serializedFieldInfo))
            {
                return;
            }

            var fieldSettings = SerializationHelper.JsonNetDeserialize<List<CustomPmlFieldInfo>>(serializedFieldInfo);

            foreach (var field in fieldSettings)
            {
                switch (field.Id)
                {
                    case "CustomPricingPolicyField1": branch.CustomPricingPolicyField1 = field.Value; break;
                    case "CustomPricingPolicyField2": branch.CustomPricingPolicyField2 = field.Value; break;
                    case "CustomPricingPolicyField3": branch.CustomPricingPolicyField3 = field.Value; break;
                    case "CustomPricingPolicyField4": branch.CustomPricingPolicyField4 = field.Value; break;
                    case "CustomPricingPolicyField5": branch.CustomPricingPolicyField5 = field.Value; break;
                    default: throw new CBaseException(ErrorMessages.Generic, "Unhandled LO pricing policy field id.");
                }
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.ControlLoad);
			this.Init += new System.EventHandler(this.ControlInit);
            this.PreRender += new System.EventHandler(this.ControlPreRender);

		}
		#endregion

		/// <summary>
		/// Handle grid item bound.
		/// </summary>

		protected void BranchGridBound( object sender , System.Web.UI.WebControls.DataGridItemEventArgs a )
		{
			// Consider each branch row as we bind.
			
			try
			{
				// Highlight the ones that are selected.

				DataRowView dR = a.Item.DataItem as DataRowView;
				if( dR != null )
				{
                    if ((Guid)dR["BranchId"] == m_BranchId)
                    {
                        a.Item.BackColor = Color.LightBlue;
                    }
                }

                var hideAddEmployeeLink = !this.CurrentUser.HasRole(E_RoleT.Administrator) && !(this.CurrentUser is InternalUserPrincipal);
                if (hideAddEmployeeLink)
                {
                    a.Item.Cells.Remove(a.Item.Cells[8]); // add employee link gone.
                }

                if (a.Item.ItemType != ListItemType.Item && a.Item.ItemType != ListItemType.AlternatingItem)
                {
                    return;
                }

                var branchId = (Guid)dR["BranchId"];
                var name = (string)dR["Name"];
                var street = (string)dR["Street"];
                var city = (string)dR["City"];
                var state = (string)dR["State"];
                var zipcode = (string)dR["Zipcode"];
                LinkButton editLink = (LinkButton)a.Item.FindControl("Edit");
                LinkButton deleteLink = (LinkButton)a.Item.FindControl("Delete");
                LinkButton nameLink = (LinkButton)a.Item.FindControl("NameLink");
                EncodedLiteral nameLiteral = (EncodedLiteral)a.Item.FindControl("Name");
                EncodedLiteral streetLiteral = (EncodedLiteral)a.Item.FindControl("Street");
                EncodedLiteral cityStateZipLiteral = (EncodedLiteral)a.Item.FindControl("CityStateZip");
                EncodedLiteral status = (EncodedLiteral)a.Item.FindControl("Status");

                editLink.CommandArgument = branchId.ToString();
                deleteLink.CommandArgument = branchId.ToString();
                nameLink.CommandArgument = branchId.ToString();

                if (!hideAddEmployeeLink)
                {
                    LinkButton addLink = (LinkButton)a.Item.FindControl("Add");
                    addLink.CommandArgument = branchId.ToString();
                }

                nameLiteral.Text = name;
                streetLiteral.Text = street;
                cityStateZipLiteral.Text = $"{city}, {state} {zipcode}";
                status.Text = ((BranchStatus)Convert.ToInt32(dR["Status"])).ToString();
            }
			catch
			{
				// Oops!
				throw;
			}
		}

        protected AbstractUserPrincipal CurrentUser
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal;
            }
        }

		protected void RefreshDeletePanel( object sender , System.EventArgs a )
		{
			ShowDeletePanel(new Guid(m_hiddenBranchId.Value));
		}

		private void ShowDeletePanel(Guid branchID)
		{
            tabIndex.Text = "0";
            BranchDesc bD = m_Branches[ branchID ];

			if( bD == null )
				throw new ArgumentException( "Selected branch not found." );

			m_hiddenBranchId.Value = bD.BranchId.ToString();
			m_hiddenBranchName.Value = bD.Name;
			m_displayBranchName.Text = bD.Name;

			m_DeleteSelectedBranch.Visible = true;
			m_SelectedBranch.Visible = false;
            editBranchRow.Visible = true;
            branchTableRow.Visible = false;

			int numUsers;
			int numActiveUsers;
			int numInactiveUsers;
			int numLoanFiles;
			int numValidLoanFiles;
			int numDeletedLoanFiles;
			int numLoanTemplates;
            int numPmlBrokers;
            //ArrayList parameters = new ArrayList();

            //parameters.Add(new SqlParameter("@BrokerId", m_BrokerId));
            //parameters.Add(new SqlParameter("@BranchId", m_BranchId))

            SqlParameter[] parameters = {
                                            new SqlParameter("@BranchId", bD.BranchId)
                                      };

            using (DbDataReader sR = StoredProcedureHelper.ExecuteReader(m_BrokerId, "GetNumPmlBrokersWithBranchByBranchId", parameters))
            {
                numPmlBrokers = (!sR.Read()) ? 0 : Convert.ToInt32(sR["numPmlBrokersWithBranch"]);
            };

            PmlBrokersDependOnCount.InnerText = numPmlBrokers.ToString();
            PmlBrokersDependOnAction.Attributes.Add("class", numPmlBrokers > 0 ? "action action-required" : "action");
            PmlBrokersDependOnImg.ImageUrl = numPmlBrokers > 0 ? "../../images/error_icon.gif" : "";
            PmlBrokersDependOnAction.InnerText = numPmlBrokers > 0 ? "Re-assign the Originating Companies' branches" : "OK";

            parameters = new SqlParameter[]{
                                            new SqlParameter("@BranchId", bD.BranchId)
                                      };


            using (DbDataReader sR = StoredProcedureHelper.ExecuteReader(m_BrokerId, "GetNumUsersAssociatedWithBranchByBranchId", parameters))
            {
                numUsers = (!sR.Read())?0:Convert.ToInt32(sR["numUsers"]);
			}

            parameters = new SqlParameter[] {
                            new SqlParameter("@BranchId", bD.BranchId)
                          };
			using( DbDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "GetNumActiveUsersAssociatedWithBranchByBranchId" ,parameters ) )
			{
				numActiveUsers = (!sR.Read())?0:Convert.ToInt32(sR["numActiveUsers"]);
			}

			numInactiveUsers = numUsers - numActiveUsers;
			m_numActiveUsers.Text = "Number of active users associated with branch: " + numActiveUsers;
			m_numInactiveUsers.Text = "Number of inactive users associated with branch: " + numInactiveUsers;

            parameters = new SqlParameter[] {
                            new SqlParameter("@BranchId", bD.BranchId)
                          };

			using( DbDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "GetNumLoansAssociatedWithBranchByBranchId" , parameters ) )
			{
				numLoanFiles = (!sR.Read())?0:Convert.ToInt32(sR["numLoans"]);
			}

            parameters = new SqlParameter[] {
                            new SqlParameter("@BranchId", bD.BranchId)
                          };
			using( DbDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "GetNumDeletedLoansAssociatedWithBranchByBranchId" , parameters ) )
			{
				numDeletedLoanFiles = (!sR.Read())?0:Convert.ToInt32(sR["numDeletedLoans"]);
			}
			numValidLoanFiles = numLoanFiles - numDeletedLoanFiles;
			m_numValidLoanFiles.Text = "Number of valid loans associated with branch: " + numValidLoanFiles;
			m_numDeletedLoanFiles.Text = "Number of deleted loans associated with branch: " + numDeletedLoanFiles;

            parameters = new SqlParameter[] {
                            new SqlParameter("@BranchId", bD.BranchId)
                          };
			using ( DbDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "GetNumLoanTemplatesAssociatedWithBranchByBranchId" , parameters ) )
			{
				numLoanTemplates = (!sR.Read())?0:Convert.ToInt32(sR["numLoanTemplatesAssociatedWithBranchByBranchId"]);
			}

			m_numLoanTemplates.Text = "Number of loan templates that assign loans to this branch: " + numLoanTemplates;

			m_ConfirmDelete.Visible = ( (numActiveUsers == 0) && (numInactiveUsers == 0) && (numValidLoanFiles == 0) && (numDeletedLoanFiles == 0) && (numLoanTemplates == 0) )?true:false;

			if(numActiveUsers == 0)
			{
				m_numActiveUsersOK.ForeColor = System.Drawing.Color.Green;
				m_numActiveUsersImage.Visible = false;
				m_numActiveUsersOK.Text = "OK";
			}
			else
			{
				m_numActiveUsersOK.ForeColor = System.Drawing.Color.Red;
				m_numActiveUsersImage.ImageUrl = "../../images/error_icon.gif";
				m_numActiveUsersImage.Visible = true;
				m_numActiveUsersOK.Text = "Assign users to a different branch";
			}
			if(numInactiveUsers == 0)
			{
				m_numInactiveUsersOK.ForeColor = System.Drawing.Color.Green;
				m_numInactiveUsersImage.Visible = false;
				m_numInactiveUsersOK.Text = "OK";
			}
			else
			{
				m_numInactiveUsersOK.ForeColor = System.Drawing.Color.Red;
				m_numInactiveUsersImage.ImageUrl = "../../images/error_icon.gif";
				m_numInactiveUsersImage.Visible = true;
				m_numInactiveUsersOK.Text = "Assign users to a different branch";
			}
			if( numUsers == 0 )
			{
				m_displayShowUsers.Value = "none";
			}
			else
			{
				m_displayShowUsers.Value = "";
			}
						
			if(numValidLoanFiles == 0)
			{
				m_numValidLoanFilesOK.ForeColor = System.Drawing.Color.Green;
				m_numValidLoanFilesImage.Visible = false;
				m_numValidLoanFilesOK.Text = "OK";
			}
			else
			{
				m_numValidLoanFilesOK.ForeColor = System.Drawing.Color.Red;
				m_numValidLoanFilesImage.ImageUrl = "../../images/error_icon.gif";
				m_numValidLoanFilesImage.Visible = true;
				m_numValidLoanFilesOK.Text = "Assign loans to a different branch";
			}
			if(numDeletedLoanFiles == 0)
			{
				m_numDeletedLoanFilesOK.ForeColor = System.Drawing.Color.Green;
				m_numDeletedLoanFilesImage.Visible = false;
				m_numDeletedLoanFilesOK.Text = "OK";
			}
			else
			{
				m_numDeletedLoanFilesOK.ForeColor = System.Drawing.Color.Red;
				m_numDeletedLoanFilesImage.ImageUrl = "../../images/error_icon.gif";
				m_numDeletedLoanFilesImage.Visible = true;
				m_numDeletedLoanFilesOK.Text = "Assign loans to a different branch";
			}
			if( numLoanFiles == 0 )
			{
				m_displayShowLoans.Value = "none";
			}
			else
			{
				m_displayShowLoans.Value = "";
			}

			if ( numLoanTemplates == 0 )
			{
				m_numLoanTemplatesOK.ForeColor = System.Drawing.Color.Green;
				m_numLoanTemplatesImage.Visible = false;
				m_numLoanTemplatesOK.Text = "OK";
			}
			else
			{
				m_numLoanTemplatesOK.ForeColor = System.Drawing.Color.Red;
				m_numLoanTemplatesImage.ImageUrl = "../../images/error_icon.gif";
				m_numLoanTemplatesImage.Visible = true;
				m_numLoanTemplatesOK.Text = "Modify loan template branch settings";
			}
			if ( numLoanTemplates == 0 )
			{
				m_displayShowLoanTemplates.Value = "none";
			}
			else
			{
				m_displayShowLoanTemplates.Value = "";
			}

			DataSource = bD.BranchId;
		}

		/// <summary>
		/// Handle grid item click.
		/// </summary>

		protected void BranchGridClick( object sender , System.Web.UI.WebControls.DataGridCommandEventArgs a )
		{
            if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
            {
                Tools.ValidateInternalUserPermission(PrincipalFactory.CurrentPrincipal.Permissions, E_InternalUserPermissions.EditBroker);
            }

			// Handle edit click by loading entry in the edit textboxes.

			try
			{
				// Edit/delete existing entry.

				switch( a.CommandName.ToLower().TrimWhitespaceAndBOM() )
				{
						//OPM 7445
					case "delete":
					{
						ShowDeletePanel(new Guid( a.CommandArgument.ToString() ));
					}
						break;
					case "edit":
					{
                        if (requiresDataBind.Checked)
                            DataBind();
						BranchDesc bD = m_Branches[ new Guid( a.CommandArgument.ToString() ) ];

						if( bD == null )
						{
							throw new ArgumentException( "Selected branch not found." );
						}

						m_DeleteSelectedBranch.Visible = false;
                        m_SelectedBranch.Visible = true;
                        editBranchRow.Visible = true;
                        branchTableRow.Visible = false;

						m_Name.BackColor       = Color.White;
						m_Street.BackColor     = Color.White;
						m_City.BackColor       = Color.White;
						m_State.BackColor      = Color.White;
						m_Zipcode.BackColor    = Color.White;
						m_Phone.BackColor      = Color.White;
						m_BranchCode.BackColor = Color.White;
						m_Name.Text       = bD.Name;
						m_Street.Text     = bD.Street;
						m_City.Text       = bD.City;
						m_State.Value     = bD.State;
						m_Zipcode.Text    = bD.Zipcode;
						m_Phone.Text      = bD.Phone;
						m_Fax.Text        = bD.Fax;
						m_BranchCode.Text = bD.BranchCode;
						m_BranchIdNumber.Text = bD.BranchIdNumber == 0 ? "" : bD.BranchIdNumber.ToString();

                        Tools.SetDropDownListValue(m_Division, bD.Division);
                        Tools.SetDropDownListValue(m_BranchChannelT, bD.BranchChannelT);
                        Tools.SetDropDownListValue(m_BranchStatus, bD.Status);

                            if (this.Broker.EnableRetailTpoPortalMode)
                        {
                            Tools.SetDropDownListValue(this.RetailTpoLandingPageList, bD.RetailTpoLandingPageId?.ToString() ?? "none");
                        }

                        if (Broker.IsEnableNewConsumerPortal)
                        {
                            var portals = ConsumerPortalConfig.RetrievePortalsForBroker(Broker.BrokerID);
                            BindConsumerPortalPicker(ConsumerPortalPicker, portals);

                            if (bD.ConsumerPortalId != Guid.Empty)
                            {
                                Tools.SetDropDownListValue(ConsumerPortalPicker, bD.ConsumerPortalId.ToString());
                            }
                            else
                            {
                                ConsumerPortalPicker.Items.Insert(0, new ListItem("<-- Which consumer portal does this branch use? -->", "-1"));

                            }
                        }

                        m_IsAllowLOEditLoanUntilUnderwriting.Checked = bD.IsAllowLOEditLoanUntilUnderwriting;

                        chkUseBranchInfoForLoans.Checked = bD.IsUseBranchInfoForLoans;
                        LicensesPanel.Disabled = !chkUseBranchInfoForLoans.Checked;
                        LicensesPanel.LosIdentifier = bD.LosIdentifier;
                        LicenseInfoList licenseList = bD.LicenseInformationList;
                        
                        foreach (LicenseInfo licenseInfo in licenseList)
                        {
                            licenseInfo.DefaultDisplayName = bD.DisplayNm;
                            licenseInfo.DefaultPhone = bD.Phone;
                            licenseInfo.DefaultStreet = bD.Street;
                            licenseInfo.DefaultZip = bD.Zipcode;
                            licenseInfo.DefaultCity = bD.City;
                            licenseInfo.DefaultAddrState = bD.State;
                            licenseInfo.DefaultFax = bD.Fax;
                        }

                        LicensesPanel.LicenseList = bD.LicenseInformationList;

                        foreach (RepeaterItem item in m_DocumentVendorLogins.Items)
                        {
                            HiddenField VendorIdField = (HiddenField)item.FindControl("VendorId");
                            Guid vendorId = new Guid(VendorIdField.Value);
                            TextBox UsernameField = (TextBox)item.FindControl("m_Username");
                            TextBox PasswordField = (TextBox)item.FindControl("m_Password");
                            TextBox CompanyIdField = (TextBox)item.FindControl("m_CompanyId");

                            DocumentVendorLoginCredentials creds;
                            if (bD.DocumentVendorLoginCredentials.TryGetValue(vendorId, out creds))
                            {
                                UsernameField.Text = creds.Login;
                                InitPassword(PasswordField, creds.Password);
                                CompanyIdField.Text = creds.AccountId;
                            }
                            else
                            {
                                UsernameField.Text = string.Empty;
                                PasswordField.Text = string.Empty;
                                PasswordField.Attributes.Add("value", "");
                                CompanyIdField.Text = string.Empty;
                            }
                        }

                        SqlParameter[] parameters = {
                                                        new SqlParameter( "@BrokerId" , m_BrokerId )
                                                    };

						using( IDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "ListPricingGroupByBrokerId" , parameters ) )
						{
							m_BranchLpePriceGroupIdDefault.Items.Clear();

							while( sR.Read() == true )
							{
								String groupId = sR[ "LpePriceGroupId"   ].ToString();
								String groupNm = sR[ "LpePriceGroupName" ].ToString();

								if( ( bool ) sR[ "ExternalPriceGroupEnabled" ] == false )
								{
									groupNm += " (disabled)";

									if( ( Guid ) sR[ "LpePriceGroupId" ] != bD.BranchLpePriceGroupIdDefault )
									{
										continue;
									}
								}

								m_BranchLpePriceGroupIdDefault.Items.Add( new ListItem( groupNm  , groupId ) );
							}
						}

						m_BranchLpePriceGroupIdDefault.Items.Insert( 0 , new ListItem( "<-- Use default at corporate level -->" , Guid.Empty.ToString() ) );

						if( bD.BranchLpePriceGroupIdDefault != Guid.Empty )
							m_BranchLpePriceGroupIdDefault.SelectedIndex = m_BranchLpePriceGroupIdDefault.Items.IndexOf( m_BranchLpePriceGroupIdDefault.Items.FindByValue( bD.BranchLpePriceGroupIdDefault.ToString() ) );
						else
							m_BranchLpePriceGroupIdDefault.SelectedIndex = 0;

						DataSource = bD.BranchId;
                        BranchGroupsList.Value = bD.BranchGroupList;

                        m_chkDisplayNmModified.Checked = bD.IsDisplayNmModified;
                        this.m_branchDisplayNm.Value = bD.IsDisplayNmModified ? bD.DisplayNm : this.Broker.Name ;
                        this.m_brokerDisplayNm.Value = this.Broker.Name;

                        m_isFhaLenderIDModified.Checked = bD.IsFhaLenderIDModified;
                        this.m_branchFhaLenderID.Value = bD.IsFhaLenderIDModified ? bD.FhaLenderID : this.Broker.FhaLenderId;
                        this.m_brokerFhaLenderID.Value = this.Broker.FhaLenderId;

                        m_PopulateFhaAddendumLines.Checked = bD.PopulateFhaAddendumLines;

                        this.IsLegalEntityIdentifierModified.Checked = bD.IsLegalEntityIdentifierModified;
                        this.LegalEntityIdentifier.Text = bD.LegalEntityIdentifier; 


                        m_BranchGroupGrid.DataSource = ViewGenerator.Generate(m_BranchGroups);
                        m_BranchGroupGrid.DataBind();
                        }

                        if (this.Broker.EnableRetailTpoPortalMode)
                        {
                            var navigationItems = this.Broker.GetPmlNavigationConfiguration(PrincipalFactory.CurrentPrincipal).GetPageNodeNames();

                            var enabledBranchCustomPages = new BranchEnabledRetailPortalCustomPage(this.m_BrokerId, this.m_BranchId);
                            var enabledBranchCustomPageIds = enabledBranchCustomPages.RetrieveEnabledPages();

                            var customPages = navigationItems.Select(item => Tuple.Create(item.Item2/*Name*/, item.Item1/*Id*/, enabledBranchCustomPageIds.Contains(item.Item1)));
                            this.CustomPageRepeater.DataSource = customPages;
                            this.CustomPageRepeater.DataBind();
                        }

                        break;

					case "add":
					{
						m_DeleteSelectedBranch.Visible = false;
                        m_SelectedBranch.Visible = false;
                        editBranchRow.Visible = false;
                        branchTableRow.Visible = true;

                        Page.ClientScript.RegisterHiddenField( "m_addToBranch" , a.CommandArgument.ToString() );
						Page.ClientScript.RegisterHiddenField( "m_addToBroker" , m_BrokerId.ToString() );
					}
						break;
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to handle request." , e );
			}
		}

        private void SetServiceCredentials(Guid branchId)
        {
            if (branchId == Guid.Empty)
            {
                Page.ClientScript.RegisterHiddenField("ServiceCredentialsJson", "[]");
            }
            else
            {
                // OPM 450477.
                var serviceCredentials = ServiceCredential.GetAllBranchServiceCredentials(branchId, m_BrokerId, ServiceCredentialService.All);
                var serviceCredentialsJson = ObsoleteSerializationHelper.JsonSerialize(serviceCredentials);
                Page.ClientScript.RegisterHiddenField("ServiceCredentialsJson", serviceCredentialsJson);
            }

            Page.ClientScript.RegisterHiddenField("BranchIsNew", (branchId == Guid.Empty).ToTrueFalse());
        }

        private void InitPassword(TextBox pwd, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                pwd.Text = "";
                pwd.Attributes.Add("value", "");
            }
            else
            {
                pwd.Text = ConstAppDavid.FakePasswordDisplay;
                pwd.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
            }
        }

        protected void BindConsumerPortalPicker(DropDownList ddl, List<ConsumerPortalIdData> values)
        {
            ddl.Items.Clear();
            foreach(ConsumerPortalIdData portal in values)
            {
                ddl.Items.Add(new ListItem(portal.Name, portal.Id.ToString()));
            }
        }


		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void InsertClick( object sender , System.EventArgs a )
		{
			// Insert a new branch on update.

			try
			{
				// Setup the editor for a new branch.
				m_DeleteSelectedBranch.Visible = false;
                m_SelectedBranch.Visible = true;
                editBranchRow.Visible = true;
                branchTableRow.Visible = false;

				m_Name.BackColor       = Color.White;
				m_Street.BackColor     = Color.White;
				m_City.BackColor       = Color.White;
				m_State.BackColor      = Color.White;
				m_Zipcode.BackColor    = Color.White;
				m_Phone.BackColor      = Color.White;
				m_BranchCode.BackColor = Color.White;
				m_Name.Text       = String.Empty;
				m_Street.Text     = String.Empty;
				m_City.Text       = String.Empty;
				m_State.Value     = String.Empty;
				m_Zipcode.Text    = String.Empty;
				m_Phone.Text      = String.Empty;
				m_Fax.Text        = String.Empty;
				m_BranchCode.Text = String.Empty;
                m_Division.Text   = String.Empty;
                Tools.SetDropDownListValue(m_BranchChannelT, E_BranchChannelT.Blank);
                Tools.SetDropDownListValue(m_BranchStatus, BranchStatus.Active);
                Tools.SetDropDownListValue(this.RetailTpoLandingPageList, "none");
                m_IsAllowLOEditLoanUntilUnderwriting.Checked = false;
                chkUseBranchInfoForLoans.Checked = true;
                LicensesPanel.LosIdentifier = string.Empty;
                LicensesPanel.LicenseList = null;

                // Bind Consumer Portal Drop Down
                var portals = ConsumerPortalConfig.RetrievePortalsForBroker(Broker.BrokerID);
                BindConsumerPortalPicker(ConsumerPortalPicker, portals);
                ConsumerPortalPicker.Items.Insert(0, new ListItem("<-- Which consumer portal does this branch use? -->", "-1"));

                BrokerDB broker = BrokerDB.RetrieveById(m_BrokerId);

                m_txtDisplayNm.Text = broker.Name;
                m_chkDisplayNmModified.Checked = false;
                this.m_branchDisplayNm.Value = broker.Name;
                this.m_brokerDisplayNm.Value = broker.Name;

                this.m_isFhaLenderIDModified.Checked = false;
                this.m_branchFhaLenderID.Value = broker.FhaLenderId;
                this.m_brokerFhaLenderID.Value = broker.FhaLenderId;

                this.IsLegalEntityIdentifierModified.Checked = false;
                this.LegalEntityIdentifier.Text = broker.LegalEntityIdentifier;

                SqlParameter[] parameters = {
                                                new SqlParameter( "@BrokerId" , m_BrokerId ),
                                                new SqlParameter( "@ExternalPriceGroupEnabled" , 1 )
                                            };
				using( IDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "ListPricingGroupByBrokerId" ,  parameters) )
				{
					m_BranchLpePriceGroupIdDefault.DataTextField  = "LpePriceGroupName";
					m_BranchLpePriceGroupIdDefault.DataValueField = "LpePriceGroupId";
					m_BranchLpePriceGroupIdDefault.DataSource = sR;
					m_BranchLpePriceGroupIdDefault.DataBind();
				}

				m_BranchLpePriceGroupIdDefault.Items.Insert( 0 , new ListItem( "<-- Default -->" , Guid.Empty.ToString() ) );
				DataSource = Guid.Empty;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to handle request." , e );
			}
		}

		protected void DeleteClick( object sender , System.EventArgs a )
		{
			try
			{
                SqlParameter[] parameters = {
                                                new SqlParameter( "@BranchID" , m_BranchId )
                                            };
				StoredProcedureHelper.ExecuteNonQuery( m_BrokerId, "DeleteBranch" , 0, parameters );

                m_SelectedBranch.Visible = false;
                m_DeleteSelectedBranch.Visible = false;
                editBranchRow.Visible = false;
                branchTableRow.Visible = true;

				DataBind();
			}
			catch( Exception e)
			{
				Tools.LogError( ErrorMessage = "Failed to delete branch." , e );
            }
        }

		protected void CancelClick( object sender , System.EventArgs a )
		{
            m_SelectedBranch.Visible = false;
			m_DeleteSelectedBranch.Visible = false;
            editBranchRow.Visible = false;
            branchTableRow.Visible = true;
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void ApplyClick( object sender , System.EventArgs a )
		{
			// Commit all dirty branches to the database.
			
			try
			{
				// Saved branches that are dirty just need to be updated.
				// Dirty non-saved branches need to be created.
				
				if( m_SelectedBranch.Visible == true )
				{
					
					SaveData();
					
					DataBind();
				}
				
			}
			catch( ArgumentException e )
			{
				// D'oh!

				ErrorMessage = e.Message;

				IsDirty = true;
			}
			catch( Exception e )
			{
				// Oops!
				
				Tools.LogError( ErrorMessage = "Failed to save form." , e );
			}
		}

        protected void ExportClick(object sender, System.EventArgs a)
        {
            Response.Clear();
            //Tell them it's plain text
            Response.ContentType = "text/plain";
            //And that the client should download this file, instead of displaying it in the browser
            Response.AppendHeader("content-disposition", "attachment; filename=\"BranchExport.csv\"");
            var temp = m_Branches.Items.ToDataTable(new string[] 
            {
                "DisplayNm",
                "Name",
                "BranchCode",
                "BranchChannelT",
                "Street",
                "City",
                "State",
                "Zipcode",
                "Phone",
                "Fax",
                "BranchLpePriceGroupIdDefault",
                "LosIdentifier",
                "BranchGroupNameList",
                "LicenseInformationList",
                "Division",
                "BranchIdNumber",
                "Status"
            });

            //Cache the LPE price group ids and names for this broker
            var PriceGroupNamesById = new Dictionary<Guid, string>();
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", m_BrokerId),
            };

            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(m_BrokerId, "ListPricingGroupByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    PriceGroupNamesById.Add(
                        new Guid(reader["LpePriceGroupId"].ToString()),
                        (string)reader["LpePriceGroupName"]);
                }
            }
            
            //We need to translate some columns, since people don't like raw guids and enum values
            temp = temp.MapColumns(new Dictionary<string,Func<object,object>>()
            {
                {"BranchIdNumber", (branchIdNumber) => (int)branchIdNumber == 0 ? "" : branchIdNumber.ToString() },
                {"BranchChannelT", (chan) => Enum.GetName(typeof(E_BranchChannelT), chan)},
                {"Status", status => Enum.GetName(typeof(BranchStatus), status)},
                {"BranchLpePriceGroupIdDefault", (i) => PriceGroupNamesById.ContainsKey((Guid)i)? PriceGroupNamesById[(Guid)i] : "None"},
                {"LicenseInformationList", (l) =>
                    {
                        var licenses = (LicenseInfoList)l;
                        StringBuilder ret = new StringBuilder();
                        foreach(LicenseInfo license in licenses)
                        {
                            ret.Append(license.State + "; " + license.ExpD + "; " + license.License + " | ");
                        }

                        return ret.ToString().TrimEnd('|', ' ');
                    }},
                {"BranchGroupNameList", (l) =>
                    {
                        //Excel flips out about having commas in a csv field, even if those
                        //commas are delimited by quotes... so replace them with semi-colons.
                        string groups = (string)l;
                        if (!string.IsNullOrEmpty(groups))
                        {
                            string[] parts = groups.Split(',');
                            for (int i = 0; i < parts.Length; i++)
                            {
                                parts[i] = parts[i].TrimWhitespaceAndBOM();
                            }
                            //Since we're in here already, sort them to be friendlier
                            Array.Sort(parts);

                            return string.Join("; ", parts);
                        }
                        return "";
                        
                    }},
            });

            

            //Since the CSV header is generated from column names, rename some of them for nice.
            var renames = new Dictionary<string, string>() {
                {"LosIdentifier", "NMLS Identifier"},
                {"BranchCode", "Branch Code"},
                {"BranchGroupNameList", "List of branch groups"},
                {"DisplayNm", "Display Name"},
                {"BranchLpePriceGroupIdDefault", "Default price group"},
                {"LicenseInformationList", "Licensing information"},
                {"BranchChannelT", "Branch channel type"},
                {"BranchIdNumber", "Branch ID Number"},
                {"Status", "Branch Status" }
            };

            string csv = temp.RenameColumns(renames).ToCSVWithHeader();
            Response.Write(csv);
            Response.Flush();
            Response.End();
        }

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void OkClick( object sender , System.EventArgs a )
		{
			// Commit all dirty branches to the database.
			try
			{
				// Saved branches that are dirty just need to be updated.
				// Dirty non-saved branches need to be created.  Close
				// the window when done.

				// Saved branches that are dirty just need to be updated.
				// Dirty non-saved branches need to be created.
				
				if( m_SelectedBranch.Visible == true )
				{
					
					SaveData();
					
					DataBind();
                }

                m_SelectedBranch.Visible = false;
                m_DeleteSelectedBranch.Visible = false;
                editBranchRow.Visible = false;
                branchTableRow.Visible = true;
            }
			catch( ArgumentException e )
			{
				// D'oh!

				ErrorMessage = e.Message;

				IsDirty = true;
			}
			catch( Exception e )
			{
				// Oops!
				
				Tools.LogError( ErrorMessage = "Failed to save form." , e );
			}
		}

        protected void CustomPageRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // Page name, page ID, selected
            var customPage = (Tuple<string, Guid, bool>)e.Item.DataItem;

            var checkbox = e.Item.FindControl("CustomPageCheckbox") as HtmlInputCheckBox;
            checkbox.Attributes["data-page-id"] = customPage.Item2.ToString();
            checkbox.Attributes["class"] += " custom-page";
            checkbox.Checked = customPage.Item3;

            var nameSpan = e.Item.FindControl("CustomPageSpan") as HtmlGenericControl;
            nameSpan.InnerText = customPage.Item1;
        }
    }

    /// <summary>
    /// Track each branch of this broker.  We store a list of
    /// them in the view state.
    /// </summary>
    [ Serializable ]
	internal class BranchDesc
	{
		/// <summary>
		/// Track each branch of this broker.
		/// </summary>

		private string                       m_Name = String.Empty;
		private string				   m_BranchCode = String.Empty;
		private string                     m_Street = String.Empty;
		private string                       m_City = String.Empty;
		private string                      m_State = String.Empty;
		private string                    m_Zipcode = String.Empty;
		private string                      m_Phone = String.Empty;
		private string                        m_Fax = String.Empty;
		private Guid m_BranchLpePriceGroupIdDefault = Guid.Empty;
		private Guid                     m_BranchId = Guid.Empty;
		private bool                      m_IsSaved = false;
		private bool                      m_IsDirty = false;
        private bool m_IsAllowLOEditLoanUntilUnderwriting = false;
        private bool m_bIsUseBranchInfoForLoans = false;
        private string m_sDisplayNm = string.Empty;        
        private bool m_bDisplayNmModified = false;
        private bool m_bBranchTPO = false;
        private E_BranchChannelT m_BranchChannelT = E_BranchChannelT.Blank;
        private string m_FhaLenderID = string.Empty;
        private bool m_PopulateFhaAddendumLines = false;

        private string m_sLosIdentifier = string.Empty;
        //private string m_sLicenseXmlContent = string.Empty;
        private LicenseInfoList m_licenseInfoList;
        private Dictionary<Guid, DocumentVendorLoginCredentials> m_documentVendorCredentials = new Dictionary<Guid, DocumentVendorLoginCredentials>();

        private string m_Division;
        
        #region ( Descriptor properties )

		public IDataRecord Contents
		{
			// Set new values.

			set
            {
                Object o;

                o = value["Name"];

                if (o != null && o is DBNull == false)
                {
                    m_Name = (String)o;
                }
                else
                {
                    m_Name = String.Empty;
                }

                //OPM 28430, PA, 3/13/09
                o = value["BranchCode"];

                if (o != null && o is DBNull == false)
                {
                    m_BranchCode = (String)o;
                }
                else
                {
                    m_BranchCode = String.Empty;
                }

                o = value["BranchIdNumber"];
                if (o != null && o is DBNull == false)
                {
                    this.BranchIdNumber = (int)o;
                }
                else
                {
                    this.BranchIdNumber = 0;
                }

                o = value["Division"];

                if (o != null && o is DBNull == false)
                {
                    m_Division = (String)o;
                }
                else
                {
                    m_Division = String.Empty;
                }

                o = value["Street"];

                if (o != null && o is DBNull == false)
                {
                    m_Street = (String)o;
                }
                else
                {
                    m_Street = String.Empty;
                }

                o = value["City"];

                if (o != null && o is DBNull == false)
                {
                    m_City = (String)o;
                }
                else
                {
                    m_City = String.Empty;
                }

                o = value["State"];

                if (o != null && o is DBNull == false)
                {
                    m_State = (String)o;
                }
                else
                {
                    m_State = String.Empty;
                }

                o = value["Zipcode"];

                if (o != null && o is DBNull == false)
                {
                    m_Zipcode = (String)o;
                }
                else
                {
                    m_Zipcode = String.Empty;
                }

                o = value["Phone"];

                if (o != null && o is DBNull == false)
                {
                    m_Phone = (String)o;
                }
                else
                {
                    m_Phone = String.Empty;
                }

                o = value["Fax"];

                if (o != null && o is DBNull == false)
                {
                    m_Fax = (String)o;
                }
                else
                {
                    m_Fax = String.Empty;
                }

                o = value["BranchLpePriceGroupIdDefault"];

                if (o != null && o is DBNull == false)
                {
                    m_BranchLpePriceGroupIdDefault = (Guid)o;
                }
                else
                {
                    m_BranchLpePriceGroupIdDefault = Guid.Empty;
                }

                o = value["BranchId"];

                if (o != null && o is DBNull == false)
                {
                    m_BranchId = (Guid)o;
                }
                else
                {
                    m_BranchId = Guid.Empty;
                }

                o = value["DisplayNm"];
                if (o != null && o is DBNull == false)
                {

                    m_sDisplayNm = (string)o;
                }
                else
                {
                    m_sDisplayNm = string.Empty;
                }

                o = value["DisplayNmModified"];
                if (o != null && o is DBNull == false)
                {

                    m_bDisplayNmModified = (Boolean)o;
                }
                else
                {
                    m_bDisplayNmModified = false;
                }

                o = value["IsBranchTPO"];
                if (o != null && o is DBNull == false)
                {

                    m_bBranchTPO = (Boolean)o;
                }
                else
                {
                    m_bBranchTPO = false;
                }

                o = value["IsAllowLOEditLoanUntilUnderwriting"];

                if (o != null && o is DBNull == false)
                {
                    m_IsAllowLOEditLoanUntilUnderwriting = (bool)o;
                }
                else
                {
                    m_IsAllowLOEditLoanUntilUnderwriting = false;
                }

                o = value["UseBranchInfoForLoans"];

                if (o != null && o is DBNull == false)
                {
                    m_bIsUseBranchInfoForLoans = (bool)o;
                }
                else
                {
                    m_bIsUseBranchInfoForLoans = true;
                }

                o = value["NmlsIdentifier"];

                if (o != null && o is DBNull == false)
                {
                    m_sLosIdentifier = (String)o;
                }
                else
                {
                    m_sLosIdentifier = String.Empty;
                }

                o = value["LicenseXmlContent"];

                if (o != null && o is DBNull == false)
                {
                    m_licenseInfoList = LicenseInfoList.ToObject((string)o);
                }
                else
                {
                    m_licenseInfoList = null;
                }

                o = value["BranchChannelT"];
                if (o != null && o is DBNull == false)
                {
                    m_BranchChannelT = (E_BranchChannelT)o;
                }
                else
                {
                    m_BranchChannelT = E_BranchChannelT.Blank;
                }

                o = value["ConsumerPortalId"];
                if (o != null && o != DBNull.Value)
                {
                    ConsumerPortalId = (Guid)o;
                }
                else
                {
                    ConsumerPortalId = Guid.Empty;
                }

                o = value["IsFhaLenderIDModified"];
                if (o != null && o != DBNull.Value)
                {
                    IsFhaLenderIDModified = (bool)o;
                }
                else
                {
                    IsFhaLenderIDModified = false;
                }

                
                o = value["FhaLenderID"];
                if (o != null && o != DBNull.Value)
                {
                    FhaLenderID = (string)o;
                }
                else
                {
                    FhaLenderID = String.Empty;
                }

                o = value["PopulateFhaAddendumLines"];
                if (o != null && o != DBNull.Value)
                {
                    m_PopulateFhaAddendumLines = (bool)o;
                }
                else
                {
                    m_PopulateFhaAddendumLines = false;
                }

                o = value["IsLegalEntityIdentifierModified"];
                if (o != null && o != DBNull.Value)
                {
                    IsLegalEntityIdentifierModified = (bool)o;
                }
                else
                {
                    IsLegalEntityIdentifierModified = false;
                }

                o = value["LegalEntityIdentifier"];
                if (o != null && o != DBNull.Value)
                {
                    LegalEntityIdentifier = (string)o;
                }
                else
                {
                    LegalEntityIdentifier = String.Empty;
                }

                o = value["CustomPricingPolicyField1"];
                if (o != null && o != DBNull.Value)
                {
                    this.CustomPricingPolicyField1 = (string)o;
                }
                else
                {
                    this.CustomPricingPolicyField1 = "";
                }

                o = value["CustomPricingPolicyField2"];
                if (o != null && o != DBNull.Value)
                {
                    this.CustomPricingPolicyField2 = (string)o;
                }
                else
                {
                    this.CustomPricingPolicyField2 = "";
                }

                o = value["CustomPricingPolicyField3"];
                if (o != null && o != DBNull.Value)
                {
                    this.CustomPricingPolicyField3 = (string)o;
                }
                else
                {
                    this.CustomPricingPolicyField3 = "";
                }

                o = value["CustomPricingPolicyField4"];
                if (o != null && o != DBNull.Value)
                {
                    this.CustomPricingPolicyField4 = (string)o;
                }
                else
                {
                    this.CustomPricingPolicyField4 = "";
                }

                o = value["CustomPricingPolicyField5"];
                if (o != null && o != DBNull.Value)
                {
                    this.CustomPricingPolicyField5 = (string)o;
                }
                else
                {
                    this.CustomPricingPolicyField5 = "";
                }

                o = value["RetailTpoLandingPageId"];
                if (o != null && o != DBNull.Value)
                {
                    this.RetailTpoLandingPageId = (Guid)o;
                }
                else
                {
                    this.RetailTpoLandingPageId = null;
                }

                this.Status = (BranchStatus)Convert.ToInt32(value["Status"]);
            }
		}

		public string Name
		{
			set { m_Name = value; }
			get { return m_Name; }
		}

		// PA, OPM 28430, 3/13/09
		public string BranchCode
		{
			set { m_BranchCode = value; }
			get { return m_BranchCode; }
		}

        public int BranchIdNumber { get; private set; }

		public string Division
		{
			set { m_Division = value; }
			get { return m_Division; }
		}

        public E_BranchChannelT BranchChannelT
        {
            set { m_BranchChannelT = value; }
            get { return m_BranchChannelT; }
        }

		public string Street
		{
			set { m_Street = value; }
			get { return m_Street; }
		}

		public string City
		{
			set { m_City = value; }
			get { return m_City; }
		}

		public string State
		{
			set { m_State = value; }
			get { return m_State; }
		}

		public string Zipcode
		{
			set { m_Zipcode = value; }
			get { return m_Zipcode; }
		}

		public string Phone
		{
			set { m_Phone = value; }
			get { return m_Phone; }
		}

		public string Fax
		{
			set { m_Fax = value; }
			get { return m_Fax; }
		}

		public Guid BranchLpePriceGroupIdDefault
		{
			set { m_BranchLpePriceGroupIdDefault = value; }
			get { return m_BranchLpePriceGroupIdDefault; }
		}

		public Guid BranchId
		{
			set { m_BranchId = value; }
			get { return m_BranchId; }
		}

        public string DisplayNm
		{
            set { m_sDisplayNm = value; }
            get { return m_sDisplayNm; }
		}

        public bool IsDisplayNmModified
		{
			set { m_bDisplayNmModified = value; }
			get { return m_bDisplayNmModified; }
		}

		public bool IsSaved
		{
			set { m_IsSaved = value; }
			get { return m_IsSaved; }
		}

		public bool IsDirty
		{
			set { m_IsDirty = value; }
			get { return m_IsDirty; }
		}
        //12/14/09 fs - opm 42733
        public bool IsAllowLOEditLoanUntilUnderwriting
        {
            set { m_IsAllowLOEditLoanUntilUnderwriting = value; }
            get { return m_IsAllowLOEditLoanUntilUnderwriting; }
        }

        public bool IsUseBranchInfoForLoans
        {
            get { return m_bIsUseBranchInfoForLoans; }
            set { m_bIsUseBranchInfoForLoans = value; }
        }
        public string LosIdentifier
        {
            get { return m_sLosIdentifier; }
            set { m_sLosIdentifier = value; }
        }

        public LicenseInfoList LicenseInformationList
        {
            get { return m_licenseInfoList; }
            set { m_licenseInfoList = value; }
        }

        public string BranchGroupList { get; set; }
        public string BranchGroupNameList { get; set; }
        public Guid ConsumerPortalId { get; set; }

        public string FhaLenderID { get { return m_FhaLenderID; } set { m_FhaLenderID = value; } }
        public bool IsFhaLenderIDModified { get; set; }

        public bool IsLegalEntityIdentifierModified { get; set; }
        public string LegalEntityIdentifier { get; set; }

        public BranchStatus Status { get; set; } = BranchStatus.Active;

        public bool PopulateFhaAddendumLines
        {
            get { return m_PopulateFhaAddendumLines; }
            set { m_PopulateFhaAddendumLines = value; }
        }

        public Dictionary<Guid, DocumentVendorLoginCredentials> DocumentVendorLoginCredentials
        {
            get
            {
                return m_documentVendorCredentials;
            }
            set
            {
                m_documentVendorCredentials = value;
            }
        }

        public Guid? RetailTpoLandingPageId { get; set; }

        public IEnumerable<Guid> EnabledCustomPortalPages { get; set; }
        #endregion

        public void StoreDocumentVendorCredentials(Guid VendorId, string Login, string Password, string AccountId, bool updatePassword)
        {
            if (Password.Length > 150)
            {
                throw new CBaseException("Password too long.", $"Document vendor encoded password length of {Password.Length} exceeded max allowed length of 150.");
            }

            if (!m_documentVendorCredentials.ContainsKey(VendorId))
            {
                m_documentVendorCredentials.Add(VendorId, new DocumentVendorLoginCredentials(VendorId, Login, Password, AccountId));
            }
            else
            {
                var existing = m_documentVendorCredentials[VendorId];
                existing.Login = Login;
                if(updatePassword)
                {
                    existing.Password = Password;
                }
                existing.AccountId = AccountId;
            }
        }

        public string CustomPricingPolicyField1 { get; set; }
        public string CustomPricingPolicyField2 { get; set; }
        public string CustomPricingPolicyField3 { get; set; }
        public string CustomPricingPolicyField4 { get; set; }
        public string CustomPricingPolicyField5 { get; set; }
	}

	/// <summary>
	/// Track each branch of this broker.  We store a list of
	/// them in the view state.
	/// </summary>
	[ Serializable ]
	internal class BranchDescs
	{
		/// <summary>
		/// Track each branch of this broker.  We store a
		/// list of them in the view state.
		/// </summary>

        private List<BranchDesc> m_Set = new List<BranchDesc>();

		#region ( Set properties )

		public BranchDesc this[ Guid branchId ]
		{
			// Access element.

			get
			{
				foreach( BranchDesc bD in m_Set )
				{
					if( bD.BranchId == branchId )
					{
						return bD;
					}
				}

				return null;
			}
		}

        public List<BranchDesc> Items
		{
			set { m_Set = value; }
			get { return m_Set; }
		}

		public int Count
		{
			get { return m_Set.Count; }
		}

		#endregion

		/// <summary>
		/// Append a new branch descriptor onto the tail of this set.
		/// </summary>

		public void Add( BranchDesc bDesc )
		{
			// Append a new branch descriptor onto the tail of this
			// list.

			if( bDesc == null )
			{
				throw new ArgumentException( "Invalid branch descriptor." );
			}

			m_Set.Add( bDesc );
		}

		/// <summary>
		/// Get the looping interface for looping through for-eaches.
		/// </summary>

		public IEnumerator GetEnumerator()
		{
			// Return loop walking interface.
			return m_Set.GetEnumerator();
		}

		/// <summary>
		/// Get rid of the branch descriptors contained in our set.
		/// </summary>

		public void Clear()
		{
			// Drop all entries from the set.
			m_Set.Clear();
		}
	}
    
    // POD
    [Serializable]
    internal class BranchGroup
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
