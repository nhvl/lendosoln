<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BrokerInformationUserControl.ascx.cs" Inherits="LendersOfficeApp.los.admin.BrokerInformationUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@Import Namespace="LendersOffice.AntiXss"%>

<asp:PlaceHolder id="FhaLenderIdScript" runat="server">
<script type="text/javascript">
    function <%=AspxTools.HtmlString(ClientID) %>_loaded()
    {
        f_onKeyUpFhaLenderId();
    }
    
    function f_onKeyUpFhaLenderId() {
      var lenderIdTb = <%= AspxTools.JsGetElementById(FhaLenderId) %>;
      var hideCb = <%= AspxTools.JsGetElementById(IsHideFhaLenderIdInTotalScorecard) %>;
      
      if ( lenderIdTb.value.length != 10 )
      {
        hideCb.disabled = true;
        hideCb.checked = false;
      }
      else
      {
        hideCb.disabled = false;
      }
    }

    function f_closeLenderIdDetails() {
      document.getElementById("LenderIdDetails").style.display = "none";
    }
    function f_showLenderIdDetails(event) {
      var msg = document.getElementById("LenderIdDetails");
      msg.style.display = "";
      msg.style.top = (event.clientY - 80)+ "px";
      msg.style.left = (event.clientX - 110) + "px";
    }
</script>
			<div id="LenderIdDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:230px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION: absolute; HEIGHT:50px; BACKGROUND-COLOR:whitesmoke">
			<table width="100%"><tr><td>If entered, users will be required to use this ID when submitting to FHA TOTAL Scorecard.</td></tr>
			<tr><td align=center>[ <a href="#" onclick="f_closeLenderIdDetails();">Close</a> ]</td></tr>
			</table>
			</div>

</asp:PlaceHolder>
<div style="PADDING-RIGHT: 8px;PADDING-LEFT: 8px;PADDING-BOTTOM: 8px;PADDING-TOP: 8px">
	<TABLE class="FormTable" cellSpacing="0" cellPadding="0" border="0" width="97%">
  <TR>
    <TD class=FieldLabel noWrap width=180>Customer Code</TD>
    <TD noWrap><asp:TextBox id=CustomerCode runat="server" ReadOnly="True"></asp:TextBox></TD></TR>
		<tr>
			<td class="FieldLabel" width="180" noWrap>Company Name</td>
			<td noWrap><asp:TextBox id="Name" maxlength="100" runat="server" Width="338px"></asp:TextBox><img src="../../images/require_icon.gif">
                <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="Name">
                    <img runat="server" src="../../images/error_icon.gif">
                </asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td class="FieldLabel" noWrap>Address</td>
			<td noWrap><asp:TextBox id="Address" MaxLength="60" runat="server" Width="338px"></asp:TextBox><img src="../../images/require_icon.gif">
                <asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" Display="Dynamic" ControlToValidate="Address">
                    <img runat="server" src="../../images/error_icon.gif">
                </asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td class="FieldLabel" noWrap>City</td>
			<td noWrap><asp:TextBox id="City" runat="server" Width="240px"></asp:TextBox><img src="../../images/require_icon.gif">
                <asp:RequiredFieldValidator id="RequiredFieldValidator3" runat="server" Display="Dynamic" ControlToValidate="City">
                    <img runat="server" src="../../images/error_icon.gif">
                </asp:RequiredFieldValidator><ml:StateDropDownList id="State" runat="server"></ml:StateDropDownList><ml:ZipcodeTextBox id="Zipcode" runat="server" width="50" preset="zipcode" CssClass="mask"></ml:ZipcodeTextBox><img src="../../images/require_icon.gif">
                <asp:RequiredFieldValidator id="RequiredFieldValidator4" runat="server" Display="Dynamic" ControlToValidate="Zipcode">
                    <img runat="server" src="../../images/error_icon.gif">
                </asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td class="FieldLabel" noWrap>Phone</td>
			<td noWrap><ml:PhoneTextBox id="Phone" runat="server" CssClass="mask" preset="phone" width="120"></ml:PhoneTextBox><IMG src="../../images/require_icon.gif">
                <asp:RequiredFieldValidator id="RequiredFieldValidator5" runat="server" ControlToValidate="Phone" Display="Dynamic">
                    <img runat="server" src="../../images/error_icon.gif">
                </asp:RequiredFieldValidator></td>
		</tr>
		<TR>
			<TD class="FieldLabel" noWrap>Fax</TD>
			<td noWrap><ml:PhoneTextBox id="Fax" runat="server" CssClass="mask" preset="phone" width="120"></ml:PhoneTextBox></td>
		</TR>
		<TR>
			<TD class="FieldLabel" noWrap>Company Website</TD>
			<td noWrap><asp:TextBox id="Url" runat="server" width="338"></asp:TextBox></td>
		</TR>
        <tr>
            <td class="FieldLabel no-wrap">Legal Entity Identifier</td>
            <td class="no-wrap">
                <asp:TextBox ID="LegalEntityIdentifier" runat="server" MaxLength="20"></asp:TextBox>
                <asp:RegularExpressionValidator ID="LegalEntityIdentifierValidator" runat="server" ControlToValidate="LegalEntityIdentifier" Display="Dynamic" />
            </td>
        </tr>
		<asp:PlaceHolder id="FhaLenderIdSection" runat="server">
		<tr><td>&nbsp;</td></tr>
		<tr>
			<TD class="FieldLabel" noWrap>FHA Lender ID <a href='#"' onclick='f_showLenderIdDetails(event);'>?</a> </TD>
			<td noWrap><asp:TextBox id="FhaLenderId" runat="server" MaxLength="10" width="125px" onkeyup="f_onKeyUpFhaLenderId();"></asp:TextBox>&nbsp;
			<asp:CheckBox id="IsHideFhaLenderIdInTotalScorecard" runat="server" Text="Hide Lender ID and Sponsor ID in TOTAL" />
			</td>
		</tr>
		<tr>
		    <td></td>
		    <td><asp:RegularExpressionValidator id="FhaLenderIdLengthValidator"
		         runat="server"
		         ControlToValidate="FhaLenderId" 
		         ValidationExpression="\d{10}"
		         ErrorMessage="Lender ID must be 10 digits or blank"
		         Display="Dynamic"
		         /></td>
		</tr>
		<tr>
		    <td></td>
		    <td><asp:RegularExpressionValidator id="FhaLenderIdNineValidator"
		         runat="server"
		         ControlToValidate="FhaLenderId" 
		         ValidationExpression="^[^9].*"
		         ErrorMessage="Lender ID must not start with 9"
		         Display="Dynamic"
		         /></td>
		</tr>		
		</asp:PlaceHolder>
	</TABLE>
</div>
