﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Integration.MortgageInsurance;
using LendersOffice.Security;

namespace LendersOfficeApp.los.admin
{
    public partial class MIVendorBranchAccountDetails : LendersOffice.Common.BasePage
    {
        private Guid BranchId
        {
            get
            {
                return RequestHelper.GetGuid("branchid");
            }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingBranches
                };
            }
        }

        private IEnumerable<MortgageInsuranceVendorBrokerSettings> enabledVendors;
        private IEnumerable<MortgageInsuranceVendorBranchSettings> branchCredentials;

        /// <summary>
        /// Gets a list of enabled Mortgage Insurance Vendors for the given broker.
        /// </summary>
        /// <value>An IEnumerable of enabled Mortgage Insurance Vendors for the given broker.</value>
        private IEnumerable<MortgageInsuranceVendorBrokerSettings> EnabledVendors
        {
            get
            {
                if (this.enabledVendors == null)
                {
                    Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
                    this.enabledVendors = MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(brokerId);
                }

                return this.enabledVendors;
            }
        }

        /// <summary>
        /// Gets a list of Mortgage Insurance Vendor credentials for the given branch.
        /// </summary>
        /// <value>An IEnumerable of Mortgage Insurance Vendor credentials for the given branch.</value>
        private IEnumerable<MortgageInsuranceVendorBranchSettings> BranchCredentials
        {
            get
            {
                if (this.branchCredentials == null)
                {
                    Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
                    this.branchCredentials = MortgageInsuranceVendorBranchSettings.ListCredentialsByBranchId(brokerId, this.BranchId);
                }

                return this.branchCredentials;
            }
        }

        override protected void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            RegisterJsScript("../common/ModalDlg/CModalDlg.js");
            this.Load += new System.EventHandler(this.Page_Load);
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.RegisterJsObjectArray("CachedBrokerCrendentials", this.EnabledVendors.Select(brokerCredential =>
                            new CachedMIVendorCrential(brokerCredential)));
                this.RegisterJsObjectArray("CachedBranchCrendentials", this.BranchCredentials.Select(branchCredential =>
                            new CachedMIVendorCrential(branchCredential)));

                var allVendors = MortgageInsuranceVendorConfig.ListActiveVendors();
                var credentials = this.EnabledVendors.GroupJoin(this.BranchCredentials,
                                      broker => broker.VendorId,
                                      branch => branch.VendorId,
                                      (broker, branchCollection) =>
                                      {
                                          if (branchCollection.Any())
                                          {
                                              MortgageInsuranceVendorBranchSettings branch = branchCollection.Single();
                                              return new
                                              {
                                                  VendorId = broker.VendorId,
                                                  UseCorporateSettings = false,
                                                  UserName = branch.UserName,
                                                  Password = branch.Password,
                                                  AccountId = branch.AccountId,
                                                  PolicyId = branch.PolicyId,
                                                  IsDelegated = branch.IsDelegated
                                              };
                                          }
                                          return new
                                          {
                                              VendorId = broker.VendorId,
                                              UseCorporateSettings = true,
                                              UserName = broker.UserName,
                                              Password = broker.Password,
                                              AccountId = broker.AccountId,
                                              PolicyId = broker.PolicyId,
                                              IsDelegated = broker.IsDelegated
                                          };
                                      });

                this.VendorDetails.DataSource = credentials.Join(allVendors,
                                                                 credential => credential.VendorId,
                                                                 all => all.VendorId,
                                                                 (credential, all) =>
                                                                 new
                                                                 {
                                                                     m_VendorName = all.VendorTypeFriendlyDisplay,
                                                                     m_VendorId = all.VendorId,
                                                                     m_UseCorporateSettings = credential.UseCorporateSettings,
                                                                     m_IsAccountIdRequired = all.IsUseAccountId,
                                                                     m_Username = credential.UserName,
                                                                     m_Password = credential.Password,
                                                                     m_AccountId = credential.AccountId,
                                                                     m_PolicyId = credential.PolicyId,
                                                                     m_IsDelegated = credential.IsDelegated
                                                                 });
                   this.VendorDetails.DataBind();
            }
        }

        protected void VendorDetailsBind(object sender, RepeaterItemEventArgs e)
        {
            Label m_VendorName = (Label)e.Item.FindControl("m_VendorName");
            m_VendorName.Text = DataBinder.Eval(e.Item.DataItem, "m_VendorName") + " Account Details";

            HiddenField m_VendorId = (HiddenField)e.Item.FindControl("m_VendorId");
            m_VendorId.Value = AspxTools.HtmlString(DataBinder.Eval(e.Item.DataItem, "m_VendorId"));

            CheckBox m_UseCorporateSettings = (CheckBox)e.Item.FindControl("m_UseCorporateSettings");
            m_UseCorporateSettings.Checked = Boolean.Parse(DataBinder.Eval(e.Item.DataItem, "m_UseCorporateSettings").ToString());
            string onClickText = String.Format("oncheckboxClick(this,\"{0}\")", m_VendorId.Value);
            m_UseCorporateSettings.Attributes.Add("OnClick", onClickText);

            TextBox m_Username = (TextBox)e.Item.FindControl("m_Username");
            m_Username.CssClass = m_VendorId.Value + "_Username";
            m_Username.Text = DataBinder.Eval(e.Item.DataItem, "m_Username").ToString();
            m_Username.ReadOnly = m_UseCorporateSettings.Checked;

            TextBox m_Password = (TextBox)e.Item.FindControl("m_Password");
            m_Password.CssClass = m_VendorId.Value + "_Password";
            if (string.IsNullOrEmpty(DataBinder.Eval(e.Item.DataItem, "m_Password").ToString()))
            {
                m_Password.Text = string.Empty;
                m_Password.Attributes["value"] = string.Empty;
            }
            else
            {
                m_Password.Text = ConstAppDavid.FakePasswordDisplay;
                m_Password.Attributes["value"] = ConstAppDavid.FakePasswordDisplay;
            }
            m_Password.TextMode = TextBoxMode.Password;
            m_Password.ReadOnly = m_UseCorporateSettings.Checked;

            PlaceHolder m_AccountIdPanel = (PlaceHolder)e.Item.FindControl("AccountIdPanel");
            m_AccountIdPanel.Visible = Boolean.Parse(DataBinder.Eval(e.Item.DataItem, "m_IsAccountIdRequired").ToString());
            if (m_AccountIdPanel.Visible)
            {
                TextBox m_AccountId = (TextBox)e.Item.FindControl("m_AccountId");
                m_AccountId.CssClass = m_VendorId.Value + "_AccountId";
                m_AccountId.Text = DataBinder.Eval(e.Item.DataItem, "m_AccountId").ToString();
                m_AccountId.ReadOnly = m_UseCorporateSettings.Checked;
            }

            TextBox m_PolicyId = (TextBox)e.Item.FindControl("m_PolicyId");
            m_PolicyId.CssClass = m_VendorId.Value + "_PolicyId";
            m_PolicyId.Text = DataBinder.Eval(e.Item.DataItem, "m_PolicyId").ToString();
            m_PolicyId.ReadOnly = m_UseCorporateSettings.Checked;

            RadioButton m_delegatedUA = (RadioButton)e.Item.FindControl("m_delegatedUA");
            m_delegatedUA.CssClass = m_VendorId.Value + "_IsDelegated";
            m_delegatedUA.Checked = Boolean.Parse(DataBinder.Eval(e.Item.DataItem, "m_IsDelegated").ToString());
            RadioButton m_nondelegatedUA = (RadioButton)e.Item.FindControl("m_nondelegatedUA");
            m_nondelegatedUA.CssClass = m_VendorId.Value + "_IsNotDelegated";
            m_nondelegatedUA.Checked = !m_delegatedUA.Checked;
            if (m_UseCorporateSettings.Checked)
            {
                 m_delegatedUA.InputAttributes.Add("disabled", "disabled");
                 m_nondelegatedUA.InputAttributes.Add("disabled", "disabled");
            }

            RequiredFieldValidator m_RequiredFieldValidator1 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator1");
            RequiredFieldValidator m_RequiredFieldValidator2 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator2");
            RequiredFieldValidator m_RequiredFieldValidator3 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator3");
            RequiredFieldValidator m_RequiredFieldValidator4 = (RequiredFieldValidator)e.Item.FindControl("RequiredFieldValidator4");
            string requiredFieldCssClass = m_VendorId.Value + "_RequiredField";
            m_RequiredFieldValidator1.CssClass = requiredFieldCssClass;
            m_RequiredFieldValidator2.CssClass = requiredFieldCssClass;
            m_RequiredFieldValidator3.CssClass = requiredFieldCssClass;
            m_RequiredFieldValidator4.CssClass = requiredFieldCssClass;
            bool requiredFieldEnabled = !m_UseCorporateSettings.Checked;
            m_RequiredFieldValidator1.Enabled = requiredFieldEnabled;
            m_RequiredFieldValidator2.Enabled = requiredFieldEnabled;
            m_RequiredFieldValidator3.Enabled = requiredFieldEnabled;
            m_RequiredFieldValidator4.Enabled = requiredFieldEnabled;
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            foreach (RepeaterItem item in VendorDetails.Items)
            {
                HiddenField m_VendorId = (HiddenField)item.FindControl("m_VendorId");
                Guid vendorId = new Guid(m_VendorId.Value);
                CheckBox m_UseCorporateSettings = (CheckBox)item.FindControl("m_UseCorporateSettings");

                var selectedBranchCredential = this.BranchCredentials.FirstOrDefault(v => v.VendorId == vendorId);

                if (m_UseCorporateSettings.Checked == true)
                {
                    if (selectedBranchCredential == null)
                    {
                        continue;
                    }

                    selectedBranchCredential.ClearEntry(brokerId);
                }
                else
                {
                    if (selectedBranchCredential == null)
                    {
                        selectedBranchCredential = new MortgageInsuranceVendorBranchSettings(vendorId, this.BranchId);
                    }

                    TextBox m_Username = (TextBox)item.FindControl("m_Username");
                    TextBox m_Password = (TextBox)item.FindControl("m_Password");
                    TextBox m_AccountId = (TextBox)item.FindControl("m_AccountId");
                    TextBox m_PolicyId = (TextBox)item.FindControl("m_PolicyId");
                    RadioButton m_IsDelegated = (RadioButton)item.FindControl("m_delegatedUA");

                    selectedBranchCredential.UserName = m_Username.Text;
                    selectedBranchCredential.Password = m_Password.Text;
                    if (m_AccountId != null)
                    {
                        selectedBranchCredential.AccountId = m_AccountId.Text;
                    }
                    selectedBranchCredential.PolicyId = m_PolicyId.Text;
                    selectedBranchCredential.IsDelegated = m_IsDelegated.Checked;


                    selectedBranchCredential.SaveEntry(brokerId);
                }
            }
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "onClosePopup()", true);
        }
    }

    public class CachedMIVendorCrential
    {
        public CachedMIVendorCrential() { }
        public CachedMIVendorCrential(MortgageInsuranceVendorBrokerSettings mb)
        {
            VendorId = mb.VendorId.ToString();
            UserName = mb.UserName;
            Password = string.IsNullOrEmpty(mb.Password) ? string.Empty : ConstAppDavid.FakePasswordDisplay;
            AccountId = mb.AccountId;
            PolicyId = mb.PolicyId;
            IsDelegated = mb.IsDelegated;
        }
        public CachedMIVendorCrential(MortgageInsuranceVendorBranchSettings mb)
        {
            VendorId = mb.VendorId.ToString();
            UserName = mb.UserName;
            Password = string.IsNullOrEmpty(mb.Password) ? string.Empty : ConstAppDavid.FakePasswordDisplay;
            AccountId = mb.AccountId;
            PolicyId = mb.PolicyId;
            IsDelegated = mb.IsDelegated;
        }
        public string VendorId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AccountId { get; set; }
        public string PolicyId { get; set; }
        public bool IsDelegated { get; set; } 
    }
}
