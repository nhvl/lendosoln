<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BranchAdmin.ascx.cs" Inherits="LendersOfficeApp.los.admin.BranchAdmin"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="LendingLicenses" Src="../../los/BrokerAdmin/LendingLicensesPanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<style type="text/css">
    .CustomLOPPFieldLabel
    {
        display: inline-block;
        padding-right: 20px;
        font-weight: normal !important;
        font-size: 12px !important;
    }

    .action {
        color: green;
    }

    .action.action-required {
        color: red;
    }

    #tabContent3 table td {
        padding: 8px;
    }

    #tabsContainer, #branchTabsContainer {
        min-height: 500px;
    }

    .newheader {
        color: White;
        background-color: rgb(0,128,255);
        padding-left: 5px;
    }

    #LegalEntityIdentifierValidatorCell {
        width: 400px;
    }

    .LegalEntityIdentifier {
        width: 200px;
    }

    .align-top {
        vertical-align: top;
    }
    .TabContent {
        padding: 5px;
    }
    .ServicesSection {
        margin-bottom: 1.5em;
    }
</style>
<script>
    $(function() {
        var serializedCustomFields = $('#<%= AspxTools.ClientId(CustomLOPPFieldSettings) %>').val();
        if (serializedCustomFields !== '') {
            var customFieldInfo = $.parseJSON(serializedCustomFields);
            for (var i = 0; i < customFieldInfo.length; i++) {
                var field = customFieldInfo[i];
                $('.' + field.Id).val(field.Value);
                $('.' + field.Id).data('NonMangledId', field.Id);
            }
        }

        $(".IsLegalEntityIdentifierModified").click(setLegalEntityIdentifier);

        setLegalEntityIdentifier();
        var branchId = $("#BranchId").val();
        var existingBranch = $("#BranchIsNew").val() != 'true';
        $('#DocuSignSection').toggle(existingBranch && ML.DocuSignEnabled);
        if(branchId && existingBranch) {
            $('#ServiceCredentialsDiv').show();
            $("#ServiceCredentialsNewBranchDiv").hide();
            var serviceCredentialSettings = {
                ServiceCredentialJsonId: "ServiceCredentialsJson",
                ServiceCredentialTableId: "ServiceCredentialsTable",<% if (this.IsInternal) { %>
                BrokerId: <%= AspxTools.JsString(DataBroker) %>,<% } %>
                BranchId: branchId
            };

            ServiceCredential.Initialize(serviceCredentialSettings);
            ServiceCredential.RenderCredentials();
            $('#AddServiceCredentialBtn').click(function() {
                ServiceCredential.AddServiceCredential();
            });
            $('#ConfigureDocuSignButton').click(function() {
                LQBPopup.Show(
                    ML.VirtualRoot + '/los/admin/ConfigureDocuSignBranch.aspx?id=' + branchId, {
                        hideCloseButton: true,
                        width: 550,
                        height: 400
                    });
            });
        }
        else {
            $('#ServiceCredentialsDiv').hide();
            $("#ServiceCredentialsNewBranchDiv").show();
        }
    });

    function setLegalEntityIdentifier()
    {
        var isLegalEntityIdentifierModified = $(".IsLegalEntityIdentifierModified").prop("checked");
        var $LegalEntityIdentifier = $(".LegalEntityIdentifier");

       $LegalEntityIdentifier.prop("readonly", !isLegalEntityIdentifierModified);

        if (!isLegalEntityIdentifierModified)
        {
            $LegalEntityIdentifier.val(ML.brokerLegalEntityIdentifier);
        }
    }

    function prepareCustomLOPricingPolicyInfo() {
        // Iterate over the table, and get the field ids and values for each.
        var customFields = [];
        $('.CustomLOPricingPolicyTable :input').each(function() {
            $this = $(this);
            customFields.push({ Id: $this.data('NonMangledId'), Value: $(this).val() });
        });
        $('#<%= AspxTools.ClientId(CustomLOPPFieldSettings) %>').val(JSON.stringify(customFields));
    }

    function f_toggleDisplayNm(checkbox) {
        var textbox = <%= AspxTools.JsGetElementById(m_txtDisplayNm) %>;
        var branchDisplayNm = <%= AspxTools.JsGetElementById(m_branchDisplayNm) %>;
        var brokerDisplayNm = <%= AspxTools.JsGetElementById(m_brokerDisplayNm) %>;

        if (!checkbox || !textbox || !branchDisplayNm || !brokerDisplayNm) {
            return;
        }

        if (checkbox.checked) {
            textbox.value = branchDisplayNm.value;
        } else {
            textbox.value = brokerDisplayNm.value;
        }

        textbox.readOnly = !checkbox.checked;
    }

    function f_toggleFhaLenderId(checkbox) {
        var textbox = <%= AspxTools.JsGetElementById(m_fhaLenderID) %>;
        var branchFhaLenderID = <%= AspxTools.JsGetElementById(m_branchFhaLenderID) %>;
        var brokerFhaLenderID = <%= AspxTools.JsGetElementById(m_brokerFhaLenderID) %>;

        if (!checkbox || !textbox || !branchFhaLenderID || !brokerFhaLenderID) {
            return;
        }

        if (checkbox.checked) {
            textbox.value = branchFhaLenderID.value;
        } else {
            textbox.value = brokerFhaLenderID.value;
        }

        textbox.readOnly = !checkbox.checked;
    }

    function f_onUseBranchLicenseClick(cb) {
        LICENSES.fn.Disabled('Branch', !cb.checked);
        var o = document.getElementById(<%= AspxTools.JsGetClientIdString(LicensesPanel) %> + '_tbNmlsIdentifier');
        if (null != o) {
          o.readOnly = !cb.checked;
        }
    }

    function _init()
    {
        var showUsers = document.getElementById('m_ShowUsers');
        var showLoans = document.getElementById('m_ShowLoans');
        var showLoanTemplates = document.getElementById('m_ShowLoanTemplates');

        if(showUsers && showLoans && showLoanTemplates)
        {
            showUsers.style.display = <%= AspxTools.JsGetElementById(m_displayShowUsers) %>.value;
            showLoans.style.display = <%= AspxTools.JsGetElementById(m_displayShowLoans) %>.value;
            showLoanTemplates.style.display = <%= AspxTools.JsGetElementById(m_displayShowLoanTemplates) %>.value;
        }

        var $editBranchRow = <%= AspxTools.JQuery(editBranchRow) %>;
        if ($editBranchRow.length > 0)
        {
            f_onUseBranchLicenseClick(<%= AspxTools.JsGetElementById(chkUseBranchInfoForLoans) %>);
            changeTab(<%= AspxTools.JsGetElementById(tabIndex) %>.value);
        }
        else
        {
            changeBranchTab(<%= AspxTools.JsGetElementById(branchTabIndex) %>.value);
        }

        LoadGroupList();
        if(typeof(Page_ClientValidate) === 'function')
        {
            Page_ClientValidate();
        }

        f_toggleDisplayNm(<%= AspxTools.JsGetElementById(m_chkDisplayNmModified) %>);
        f_toggleFhaLenderId(<%= AspxTools.JsGetElementById(m_isFhaLenderIDModified) %>);
    }

    //OPM 8621
    function markDirty(phonebox)
    {
        if(phonebox.innerText != "")
            markAsDirty();
    }

    function checkDirty()
    {
        if( isDirty() == true || document.getElementById("m_IsDirty") != null && document.getElementById("m_IsDirty").value != "0" )
            return true;

        return false;
    }

    function onClose()
    {
        if( checkDirty() == true )
        {
            if( confirm( "You made changes.  Discard and close?" ) == true )
                onClosePopup();
        }
        else
            onClosePopup();
    }

    function onCancel()
    {
        if( checkDirty() == true )
        {
            return confirm( "You made changes.  Discard and close?" );
        }

        return true;
    }

    function onShowUsersClick()
    {
        var branchId = <%= AspxTools.JsGetElementById(m_hiddenBranchId) %>.value;
        var branchName = <%= AspxTools.JsGetElementById(m_hiddenBranchName) %>.value;
        <% if (IsInternal) {%>
            var brokerId = <%= AspxTools.JsString(DataBroker) %>;
            var url = "/LOAdmin/Broker/ShowUsersAssociatedWithBranch.aspx?branchId=" + branchId + "&branchName=" + branchName + "&brokerId=" + brokerId;
        <% } else { %>
            var url = "/los/admin/ShowUsersAssociatedWithBranch.aspx?branchId=" + branchId + "&branchName=" + branchName;
        <% } %>
        showModal(url, null, "Show Users Associated With Branch", null, function(arg){
            <%= AspxTools.JsGetElementById(m_SubmitBtn) %>.click();
        }, { hideCloseButton: true});
    }

    function showOriginatingCompanies() {
        var branchId = document.getElementById(<%=AspxTools.JsString(m_hiddenBranchId.ClientID)%>).value;
        var branchName = document.getElementById(<%=AspxTools.JsString(m_hiddenBranchName.ClientID)%>).value;

        showModal("/los/admin/ShowOriginatingCompaniesAssociatedWithBranch.aspx?branchId=" + branchId + "&branchName=" + branchName + "&brokerId=" + <%=AspxTools.JsString(DataBroker)%>, null, "Show Originating Companies Associated With Branch", null, function() {
            document.getElementById(<%=AspxTools.JsString(m_SubmitBtn.ClientID)%>).click();
        },{width:470});
    }

    function onShowLoansClick()
    {
        var branchId = <%= AspxTools.JsGetElementById(m_hiddenBranchId) %>.value;
        var branchName = <%= AspxTools.JsGetElementById(m_hiddenBranchName) %>.value;

        <% if (IsInternal) {%>
            var brokerId = <%= AspxTools.JsString(DataBroker) %>;
            var url = ("/loadmin/broker/ShowLoansAssociatedWithBranch.aspx?branchId=" + branchId + "&branchName=" + branchName + "&brokerId=" + brokerId);
        <% } else { %>
            var url = "/los/Admin/ShowLoansAssociatedWithBranch.aspx?branchId=" + branchId + "&branchName=" + branchName;
        <% } %>
        showModal(url, null, "Show Loans Associated With Branch", null, function(arg){
            <%= AspxTools.JsGetElementById(m_SubmitBtn) %>.click();
        });
    }
    function onApply()
    {
        if(!f_ValidatorCheckPriorToSubmit())
        {
            return;
        }
        document.getElementById('m_Apply_Seen').disabled = "disabled";

        var displayNm = <%= AspxTools.JsGetElementById(m_txtDisplayNm) %>;
        var branchDisplayNm = <%= AspxTools.JsGetElementById(m_branchDisplayNm) %>;
        var displayNmModifiedCheckbox = <%= AspxTools.JsGetElementById(m_chkDisplayNmModified) %>;

        if (displayNm && branchDisplayNm && displayNmModifiedCheckbox && displayNmModifiedCheckbox.checked) {
            branchDisplayNm.value = displayNm.value;
        }

        var fhaLenderID = <%= AspxTools.JsGetElementById(m_fhaLenderID) %>;
        var branchFhaLenderID = <%= AspxTools.JsGetElementById(m_branchFhaLenderID) %>;
        var fhaLenderIDModifiedCheckbox = <%= AspxTools.JsGetElementById(m_isFhaLenderIDModified) %>;

        if (fhaLenderID && branchFhaLenderID && fhaLenderIDModifiedCheckbox && fhaLenderIDModifiedCheckbox.checked) {
            branchFhaLenderID.value = fhaLenderID.value;
        }

        saveAllLicenses();
        prepareCustomLOPricingPolicyInfo();
        saveCustomPortalPageVisibilitySettings();
        var applyBtn = <%= AspxTools.JsGetElementById(m_Apply) %>;
        applyBtn.disabled = false;
        applyBtn.click();
    }
    function f_ValidatorCheckPriorToSubmit()
    {
        Page_ClientValidate();
        if(!Page_IsValid)
        {
            var msg = '';
            var numValidators = Page_Validators.length;
            for(var i = 0; i < numValidators; i++)
            {
                var v = Page_Validators[i];
                if(!v.isvalid)
                {
                    msg += v.errormessage + '\n';
                }
            }
            alert(msg);
            return false;
        }
        return true;
    }
    function onOk()
    {
        if(!f_ValidatorCheckPriorToSubmit())
        {
            return;
        }

        document.getElementById('m_Ok_Seen').disabled = "disabled";

        saveAllLicenses();
        prepareCustomLOPricingPolicyInfo();
        saveCustomPortalPageVisibilitySettings();
        var okBtn = <%= AspxTools.JsGetElementById(m_Ok) %>;
        okBtn.disabled = false;
        okBtn.click();
    }

    function saveCustomPortalPageVisibilitySettings() {
        var customPageIds = [];
        $('.custom-page:checked').each(function(index, checkbox) {
            var pageId = $(this).attr('data-page-id');
            customPageIds.push(pageId);
        });

        <%=AspxTools.JQuery(this.CustomPortalPageSettings)%>.val(JSON.stringify(customPageIds));
    }

    function onShowLoanTemplatesClick()
    {
        var branchId = <%= AspxTools.JsGetElementById(m_hiddenBranchId) %>.value;
        var branchName = <%= AspxTools.JsGetElementById(m_hiddenBranchName) %>.value;

        <% if (IsInternal) {%>
            var brokerId = <%= AspxTools.JsString(DataBroker) %>;
            var url = "/loadmin/broker/ShowLoanTemplatessAssociatedWithBranch.aspx?branchId=" + branchId + "&branchName=" + branchName + "&brokerId=" + brokerId;
        <% } else { %>
            var url = "/los/Admin/ShowLoanTemplatesAssociatedWithBranch.aspx?branchId=" + branchId + "&branchName=" + branchName;
        <% } %>
        showModal(url, null, "Show Loan Templates Associated With Branch", null, function(arg){
            <%= AspxTools.JsGetElementById(m_SubmitBtn) %>.click();
        }, { hideCloseButton: true });
    }

    <% if(ShowVendorLogins && DocMagicEnabled && IsInternal){ %>
        function onImportBranchDMLogins()
        {
            var brokerId = <%= AspxTools.JsString(DataBroker) %>;
            showModal( '/LOAdmin/Broker/ImportBranchDMLogins.aspx?brokerID=' + brokerId, null, null, null, function() {
                self.location = self.location;
            }, { hideCloseButton: true });
        }
    <%} %>

    <% if(IsInternal) { %>
        function onBatchUpdateBrancLicensesClick()
        {
            showModal('/LOAdmin/Broker/BatchUpdateBranchStateLicenses.aspx?brokerID=<%=AspxTools.JsStringUnquoted(DataBroker.ToString())%>', null, null, null, function() {
                self.location = self.location;
            });
        }
    <%} %>

    <% if ( IsEnabledMIVendorIntegration ) { %>
        function pmi_branch_config()
        {
            var MIVendorLink = "/los/admin/MIVendorBranchAccountDetails.aspx";
            var queryString = "?branchid="+<%= AspxTools.JsString(DataSource) %>;
            showModal(MIVendorLink + queryString, null, 'PMI Provider Configuration; dialogWidth: 500px;', null, null, { hideCloseButton: true });
        }
    <%} %>

    function UpdateGroupList()
    {
        var groupDiv = document.getElementById('BranchGroups');
        var checks = groupDiv.getElementsByTagName('INPUT');
        var list = '';
        for (var i=0; i < checks.length; i++)
        {
            var oBox = checks[i];
            if ( oBox.checked )
                list += ( list=='' ? '':',') + oBox.id;
        }
        <%= AspxTools.JsGetElementById(BranchGroupsList) %>.value = list;
    }

    function LoadGroupList()
    {
        var oGroupsList = <%= AspxTools.JsGetElementById(BranchGroupsList) %>;
        if ( oGroupsList == null ) return;
        var groupsList = oGroupsList.value.split(',');
        var groupDiv = document.getElementById('BranchGroups');
        var checks = groupDiv.getElementsByTagName('INPUT');
        var list = '';
        for (var i=0; i < checks.length; i++)
        {
            var oBox = checks[i];
            for( var j = 0; j < groupsList.length; j++)
            {
                if (groupsList[j] == oBox.id)
                {
                    oBox.checked = "checked";
                    break;
                }
            }
        }
    }

    function changeTab(index)
    {
        if(index == 0)
        {
            tab0.className = "selected";
            tab1.className = "";
            tab2.className = "";
            <%=AspxTools.JQuery(this.tab3)%>.removeClass("selected");
            tabContent0.style.display = "";
            tabContent1.style.display = "none";
            tabContent2.style.display = "none";
            tabContent3.style.display = "none";
            document.getElementById("m_Edit_tabIndex").value = "0";
        }
        else if (index == 1)
        {
            tab0.className = "";
            tab1.className = "selected";
            tab2.className = "";
            <%=AspxTools.JQuery(this.tab3)%>.removeClass("selected");
            tabContent0.style.display = "none";
            tabContent1.style.display = "";
            tabContent2.style.display = "none";
            tabContent3.style.display = "none";
            document.getElementById("m_Edit_tabIndex").value = "1";
        }
        else if (index == 2)
        {
            tab0.className = "";
            tab1.className = "";
            tab2.className = "selected";
            <%=AspxTools.JQuery(this.tab3)%>.removeClass("selected");
            tabContent0.style.display = "none";
            tabContent1.style.display = "none";
            tabContent2.style.display = "";
            tabContent3.style.display = "none";
            document.getElementById("m_Edit_tabIndex").value = "2";
        }
        else
        {
            tab0.className = "";
            tab1.className = "";
            tab2.className = "";
            <%=AspxTools.JQuery(this.tab3)%>.addClass("selected");
            tabContent0.style.display = "none";
            tabContent1.style.display = "none";
            tabContent2.style.display = "none";
            tabContent3.style.display = "";
            document.getElementById("m_Edit_tabIndex").value = "3";
        }

        $("#editButtonsDiv").toggle( $('#deleteTable').length == 0);
    }

    function changeBranchTab(index)
    {
        var $branchTab0 = $("#BranchTab0");
        var $branchTab1 = $("#BranchTab1");

        var $branchTabContent0 = $("#BranchTabContent0");
        var $branchTabContent1 = $("#BranchTabContent1");

        if(index == 0)
        {
            $branchTab0.addClass("selected");
            $branchTab1.removeClass("selected");

            $branchTabContent0.show();
            $branchTabContent1.hide();

            <%=AspxTools.JQuery(branchTabIndex)%>.val("0");
        }
        else if (index == 1)
        {
            $branchTab0.removeClass("selected");
            $branchTab1.addClass("selected");

            $branchTabContent0.hide();
            $branchTabContent1.show();

            <%=AspxTools.JQuery(branchTabIndex)%>.val("1");
        }
    }
</script>

<asp:HiddenField runat="server" ID="CustomLOPPFieldSettings" />
<asp:HiddenField runat="server" ID="CustomPortalPageSettings" />

<% if (ShowVendorLogins && DocMagicEnabled && IsInternal){ %>
    <input onclick="onImportBranchDMLogins()" type="button" value="Import Branch DocMagic Logins" />
<%} %>
<% if (IsInternal) {%>
    <input onclick="onBatchUpdateBrancLicensesClick();" type="button" value="Batch Update State Licenses" />
<%} %>
<asp:CheckBox runat="server" ID="requiresDataBind" Checked="false" style="display:none;" />

<table class="ie-only-full-height" cellSpacing="0" cellPadding="0">
    <tr id="editBranchRow" runat="server" visible="false"><td class="align-top">
        <div id="tabsContainer" style="height: 100%; display: <%= AspxTools.HtmlString(tabDisplay) %>; ">
            <asp:TextBox ID="tabIndex" runat="server" Text="0" style="display:none;"/>

            <div class="Tabs">
                <ul class="tabnav">
                    <li id="tab0" class="selected"><a href="#" onclick = "changeTab(0);">Branch Info</a></li>
                    <li id="tab1"><a href="#" onclick = "changeTab(1);">Branch Licenses</a></li>
                    <li id="tab2"><a href="#" onclick = "changeTab(2);">Services</a></li>
                    <li id="tab3" runat="server"><a href="#" onclick="changeTab(3);">Retail Portal Settings</a></li>
                </ul>
            </div>

            <div id="tabContent0">
                <table><tr><td noWrap>
                    <asp:panel id="m_DeleteSelectedBranch" style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 0px; BORDER-TOP: 2px groove; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; BORDER-LEFT: 2px groove; PADDING-TOP: 0px; BORDER-BOTTOM: 2px groove; BACKGROUND-COLOR:gainsboro" Visible="False" Runat="server">
                        <table id="deleteTable" cellSpacing="10" cellPadding="0" width="100%" border="0">
                            <INPUT id="m_hiddenBranchId" type="hidden" name="m_hiddenBranchId" runat="server">
                            <INPUT id="m_hiddenBranchName" type="hidden" name="m_hiddenBranchName" runat="server">
                            <INPUT id="m_displayShowUsers" type="hidden" name="m_displayShowUsers" runat="server">
                            <INPUT id="m_displayShowLoans" type="hidden" name="m_displayShowLoans" runat="server">
                            <INPUT id="m_displayShowLoanTemplates" type="hidden" name="m_displayLoanTemplates" runat="server">
                            <TR>
                                <TD class="FieldLabel">
                                    <ml:EncodedLabel ID="m_displayBranchName" Runat="server"/><bR>
                                    <ml:EncodedLabel id="m_numActiveUsers" Runat="server"/>
                                    <asp:Image id="m_numActiveUsersImage" Runat="server"/>
                                    &nbsp;<ml:EncodedLabel id="m_numActiveUsersOK" Runat="server"/><BR>
                                    <ml:EncodedLabel id="m_numInactiveUsers" Runat="server"/>
                                    <asp:Image id="m_numInactiveUsersImage" Runat="server"/>
                                    &nbsp;<ml:EncodedLabel id="m_numInactiveUsersOK" Runat="server"/><BR>
                                    <ml:EncodedLabel id="m_numValidLoanFiles" Runat="server"/>
                                    <asp:Image id="m_numValidLoanFilesImage" Runat="server"/>
                                    &nbsp;<ml:EncodedLabel id="m_numValidLoanFilesOK" Runat="server"/><br>
                                    <ml:EncodedLabel ID="m_numDeletedLoanFiles" Runat="server"/>
                                    <asp:Image ID="m_numDeletedLoanFilesImage" Runat="server"/>
                                    &nbsp;<ml:EncodedLabel ID="m_numDeletedLoanFilesOK" Runat="server"/><br>
                                    <ml:EncodedLabel ID="m_numLoanTemplates" Runat="server"/>
                                    <asp:Image ID="m_numLoanTemplatesImage" Runat="server"/>
                                    &nbsp;<ml:EncodedLabel ID="m_numLoanTemplatesOK" Runat="server"/>
                                    <br />
                                    <label runat="server" id="PmlBrokersDependOnLabel">Number of Originating Companies associated with the branch: </label>
                                    <label runat="server" id="PmlBrokersDependOnCount"> </label>
                                    <asp:Image id="PmlBrokersDependOnImg" Runat="server"/>
                                    <label runat="server" class="action" id="PmlBrokersDependOnAction">OK</label><bR>
                                </TD>
                            </TR>
                            <TR>
                                <TD>
                                    <INPUT id="m_ShowUsers" onclick="onShowUsersClick();" type="button" value="View Users">
                                    <INPUT id="m_ShowLoans" onclick="onShowLoansClick();" type="button" value="View Loans">
                                    <INPUT id="m_ShowLoanTemplates" onclick="onShowLoanTemplatesClick();" type="button" value="View Loan Templates" style="width: 132px;">
                                    <INPUT id="ViewOriginatingCompaniesBtn" onclick="showOriginatingCompanies()" type="button" value="View Originating Companies">
                                    <asp:Button id="m_ConfirmDelete" onclick="DeleteClick" runat="server" Text="Delete Branch"/>
                                    <asp:Button id="CancelDelete" onclick="CancelClick" runat="server" Text="Cancel" Width="60"/>
                                </TD>
                            </TR>
                        </table>
                    </asp:panel>
                    <asp:panel id="m_SelectedBranch" style="BACKGROUND-COLOR: gainsboro;" Visible="False" runat="server">
                        <asp:HiddenField runat="server" ID="m_branchDisplayNm" />
                        <asp:HiddenField runat="server" ID="m_brokerDisplayNm" />
                        <asp:HiddenField runat="server" ID="m_branchFhaLenderID" />
                        <asp:HiddenField runat="server" ID="m_brokerFhaLenderID" />
                        <TABLE cellSpacing="10" cellPadding="2" width="100%" border="0">
                            <TR>
                                <TD>
                                    <TABLE cellSpacing="0" cellPadding="2" border="0">
                                        <TR>
                                            <TD class="FieldLabel" noWrap width="70">Name</TD>
                                            <TD>
                                                <asp:TextBox id="m_Name" style="PADDING-LEFT: 4px" runat="server" Width="200" MaxLength="100"/><IMG src="../../images/require_icon.gif">
                                        </TR>
                                        <TR>
                                            <TD class="FieldLabel" noWrap width="70">Display&nbsp;Name</TD>
                                            <TD>
                                                <asp:TextBox id="m_txtDisplayNm" style="PADDING-LEFT: 4px" runat="server" Width="200"/><IMG src="../../images/require_icon.gif">
                                                &nbsp;
                                                <asp:CheckBox id="m_chkDisplayNmModified" runat="server" Text="Modify" associatedTextBoxID="<%=AspxTools.JsGetClientIdString(m_txtDisplayNm)%>" onclick="f_toggleDisplayNm(this)"/></TD>
                                        </TR>
                                        <TR>
                                            <TD class="FieldLabel" noWrap>
                                                Branch Code
                                            </TD>
                                            <TD>
                                                <asp:TextBox id="m_BranchCode" style="PADDING-LEFT: 4px" runat="server" Width="200"/>
                                            </TD>
                                        </TR>
                                        <tr>
                                            <td class="FieldLabel" noWrap>
                                                Branch ID Number
                                            </td>
                                            <td>
                                                <asp:TextBox id="m_BranchIdNumber" style="PADDING-LEFT: 4px" runat="server" Width="200" ReadOnly="true"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="FieldLabel" noWrap width="70">Division</td>
                                            <td><asp:DropDownList runat="server" ID="m_Division" Width="200" style="padding-left: 4px; background-color: White;"/></td>
                                        </tr>
                                        <TR>
                                            <TD class="FieldLabel" noWrap>
                                                Channel
                                            </TD>
                                            <TD>
                                                <asp:DropDownList ID="m_BranchChannelT" runat="server"/>
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="FieldLabel" noWrap>
                                                Branch Status
                                            </TD>
                                            <TD>
                                                <asp:DropDownList ID="m_BranchStatus" runat="server"/>
                                            </TD>
                                        </TR>
                                        <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                                        <TR>
                                            <TD class="FieldLabel" noWrap>Address</TD>
                                            <TD noWrap>
                                                <asp:TextBox id="m_Street" style="PADDING-LEFT: 4px" runat="server" Width="292"/><IMG src="../../images/require_icon.gif">
                                                <BR>
                                                <asp:TextBox id="m_City" style="PADDING-LEFT: 4px" runat="server" Width="150"/><IMG src="../../images/require_icon.gif">
                                                <ml:StateDropDownList id="m_State" style="PADDING-LEFT: 4px" runat="server" Width="50"/><IMG src="../../images/require_icon.gif">
                                                <ml:ZipcodeTextBox id="m_Zipcode" style="PADDING-LEFT: 4px" runat="server" Width="60" CssClass="mask" preset="zipcode"/><IMG src="../../images/require_icon.gif">
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="FieldLabel" noWrap>Phone</TD>
                                            <TD>
                                                <ml:PhoneTextBox id="m_Phone" style="PADDING-LEFT: 4px" runat="server" CssClass="mask" preset="phone" onchange="markDirty(this);"/><IMG src="../../images/require_icon.gif">
                                            </TD>
                                        </TR>
                                        <TR>
                                            <TD class="FieldLabel" noWrap>Fax</TD>
                                            <TD>
                                                <ml:PhoneTextBox id="m_Fax" style="PADDING-LEFT: 4px" runat="server" CssClass="mask" preset="phone" onchange="markDirty(this);"/></TD>
                                        </TR>
                                        <%if (ShowVendorLogins) { %>
                                            <asp:Repeater runat="server" ID="m_DocumentVendorLogins" OnItemDataBound="DocumentVendorBind">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="FieldLabel" noWrap>
                                                            <asp:HiddenField runat="server" ID="VendorId" />
                                                            <ml:EncodedLabel runat="server" ID="VendorName"/> Login
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <asp:PlaceHolder runat="server" ID="UsernamePanel">
                                                    <tr>
                                                        <td class="FieldLabel" noWrap>Username</td>
                                                        <td><asp:TextBox runat="server" ID="m_Username"/></td>
                                                    </tr>
                                                    </asp:PlaceHolder>
                                                    <asp:PlaceHolder runat="server" ID="PasswordPanel">
                                                        <tr>
                                                            <td class="FieldLabel" noWrap>Password</td>
                                                            <td><asp:TextBox ID="m_Password" runat="server" TextMode="Password"/></td>
                                                        </tr>
                                                    </asp:PlaceHolder>
                                                    <asp:PlaceHolder runat="server" ID="CompanyIdPanel">
                                                        <tr>
                                                            <td class="FieldLabel" noWrap>Company ID</td>
                                                            <td><asp:TextBox ID="m_CompanyId" runat="server"/></td>
                                                        </tr>
                                                    </asp:PlaceHolder>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        <%} %>
                                        <% if ( IsEnabledMIVendorIntegration ) { %>
                                            <tr>
                                                <td class="FieldLabel" noWrap="noWrap">PMI Providers</td>
                                                <td><a href="#" onclick="pmi_branch_config(); return false;">Edit Configuration</a></td>
                                            </tr>
                                        <% } %>
                                    </TABLE>
                                    <asp:PlaceHolder runat="server" id="NewConsumerPortalPanel">
                                            <TABLE cellSpacing="0" cellPadding="2" border="0">
                                            <TR>
                                                <TD class="FieldLabel" noWrap width="70">Consumer Portal</TD>
                                                <TD>
                                                    <asp:DropDownList id="ConsumerPortalPicker" runat="server"/></TD>
                                            </TR>
                                            </TABLE>
                                    </asp:PlaceHolder>
                                    <asp:Panel id="m_LpePanel" runat="server">
                                        <TABLE cellSpacing="0" cellPadding="2" border="0">
                                            <TR>
                                                <TD class="FieldLabel" noWrap width="70">Default Price Group</TD>
                                                <TD>
                                                    <asp:DropDownList id="m_BranchLpePriceGroupIdDefault" runat="server"/></TD>
                                            </TR>
                                        </TABLE>
                                    </asp:Panel>
                                    <TABLE cellSpacing="0" cellPadding="2" border="0">
                                        <TR>
                                            <TD class="FieldLabel" noWrap width="70">FHA Lender ID</TD>
                                            <TD>
                                                <asp:TextBox id="m_fhaLenderID" style="PADDING-LEFT: 4px" runat="server" Width="200"/>
                                                &nbsp;
                                                <asp:CheckBox id="m_isFhaLenderIDModified" runat="server" Text="Modify" associatedTextBoxID="<%=AspxTools.JsGetClientIdString(m_fhaLenderID)%>" onclick="f_toggleFhaLenderId(this)" />
                                                <asp:RegularExpressionValidator id="FhaLenderIdLengthValidator"
                                                 runat="server"
                                                 ControlToValidate="m_fhaLenderID"
                                                 ValidationExpression="\d{10}"
                                                 ErrorMessage="Lender ID must be 10 digits or blank"
                                                 Display="Dynamic"
                                                 />
                                                 <asp:RegularExpressionValidator id="FhaLenderIdNineValidator"
                                                 runat="server"
                                                 ControlToValidate="m_fhaLenderID"
                                                 ValidationExpression="^[^9].*"
                                                 ErrorMessage="Lender ID must not start with 9"
                                                 Display="Dynamic"
                                                 />
                                            </TD>
                                        </TR>
                                        <tr>
                                            <td class="FieldLabel no-wrap">Legal Entity Identifier</td>
                                            <td>
                                                <asp:TextBox ID="LegalEntityIdentifier" runat="server" MaxLength="20" CssClass="LegalEntityIdentifier"/>
                                                <input type="checkbox" id="IsLegalEntityIdentifierModified" class="IsLegalEntityIdentifierModified" runat="server" Text="Modify" associatedTextBoxID="<%=AspxTools.JsGetClientIdString(LegalEntityIdentifier)%>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" id="LegalEntityIdentifierValidatorCell">
                                                <asp:RegularExpressionValidator ID="LegalEntityIdentifierValidator" runat="server" ControlToValidate="LegalEntityIdentifier" Display="Dynamic" />
                                            </td>
                                        </tr>
                                    </TABLE>

                                    <table cellpadding="2" cellspacing="0" border="0">
                                        <tr>
                                                <td>
                                                <asp:CheckBox runat="server" ID="m_PopulateFhaAddendumLines" />
                                            </td>
                                            <td class="FieldLabel">Populate boxes 15 and 17 on the FHA Addendum with branch info</td>
                                        </tr>
                                    </table>

                                    <asp:PlaceHolder runat="server" ID="CustomLOPricingPolicyPlaceHolder">
                                        <div class="FieldLabel heading">Loan Officer Pricing Policy</div>
                                        <table runat="server" id="CustomLOPricingPolicyTable" class="CustomLOPricingPolicyTable"></table>
                                    </asp:PlaceHolder>

                                    <span class="FieldLabel">Branch Groups<img src="../../images/require_icon.gif"></span>
                                    <div id="BranchGroups" style='width: 100%; height: 140px; overflow-y: scroll'>
                                        <ml:CommonDataGrid ID="m_BranchGroupGrid" runat="server" EnableViewState="True" CellPadding="2" DefaultSortExpression="Name ASC">
                                            <Columns>
                                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <input type=checkbox id='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' onclick='UpdateGroupList();' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="Name" HeaderText="Name" SortExpression="Name" ItemStyle-Wrap="True"/>
                                                <asp:BoundColumn DataField="Description" HeaderText="Description" ItemStyle-Wrap="True"/>
                                            </Columns>
                                        </ml:CommonDataGrid>
                                        <INPUT id="BranchGroupsList" type="hidden" runat="server" />
                                    </div>
                                    <% if (IsInternal){ %>
                                        <table cellspacing="0" cellpadding="2" border="0">
                                            <tr><td>
                                                    <asp:CheckBox id="m_IsAllowLOEditLoanUntilUnderwriting" runat="server" Text="Loan Officers can make changes to their loans until loan status is set to Underwriting" />
                                            </td></tr>
                                        </table>
                                    <% } %>
                                </TD>
                            </TR>
                        </TABLE>
                    </asp:panel>
                </td></tr></table>
            </div>

            <div id="tabContent1" style="display:none;">
                <table height="70%" cellpadding="2" cellspacing="2">
                    <tr>
                        <td style="padding-top:0.5em">
                            <asp:CheckBox ID="chkUseBranchInfoForLoans" runat="server" Text="Use branch license and contact info for loans created by branch employees" onclick="f_onUseBranchLicenseClick(this);"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <uc:LendingLicenses id="LicensesPanel" IsCompanyLOID="True" NamingPrefix="Branch" runat="server"/>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="tabContent2" style="display:none;" class="TabContent">
                <div class="ServicesSection">
                    <div class="FieldLabel">Credentials</div>
                    <hr />
                    <div id="ServiceCredentialsDiv">
                        <div>
                            <table id="ServiceCredentialsTable" class="DataGrid"><tbody></tbody></table>
                        </div>
                        <br />
                        <input type="button" id="AddServiceCredentialBtn" value="Add Credential" />
                    </div>
                    <div id="ServiceCredentialsNewBranchDiv">
                        <span>Please save this branch before adding service credentials.</span>
                    </div>
                </div>
                <div class="ServicesSection" id="DocuSignSection">
                    <div class="FieldLabel">eSign</div>
                    <hr />
                    <input type="button" id="ConfigureDocuSignButton" value="Configure DocuSign Integration"/>
                </div>
            </div>

            <div id="tabContent3" style="display:none">
                <table>
                    <tr>
                        <td class="FieldLabel">Landing Page</td>
                        <td>
                            <asp:DropDownList ID="RetailTpoLandingPageList" runat="server"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel newheader" colspan="2">Custom Portal Page Visibility</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Branch can see which custom navigation pages? (Subject to role requirements for the page.)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Repeater ID="CustomPageRepeater" runat="server" OnItemDataBound="CustomPageRepeater_ItemDataBound">
                                <ItemTemplate>
                                    <input type="checkbox" runat="server" id="CustomPageCheckbox" />
                                    <span runat="server" id="CustomPageSpan">&nbsp;</span>
                                    <br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            </div>

            <div id="editButtonsDiv" align="right" style="padding-right:1em">
                <input type="button" id="m_Ok_Seen" onclick ='onOk();' value="OK" style="width:60px" >
                <asp:Button id="CancelEdit" OnClientClick="return onCancel();" onclick="CancelClick" runat="server" Text="Cancel" Width="60"/>
                <input type="button" id="m_Apply_Seen" onclick='onApply();' value="Apply" style="width:60px" />

                <asp:button id="m_Ok" style="display: none" Enabled="false" onclick="OkClick" runat="server" Text="OK" Width="60"/>
                <asp:button id="m_Apply" style="display: none" Enabled="false" onclick="ApplyClick" runat="server" Text="Apply" Width="60"/>
                <asp:button id="m_SubmitBtn" style="DISPLAY: none" Enabled="false" onclick="RefreshDeletePanel" runat="server"/>
            </div>
        </div>
    </td></tr>

    <tr vAlign="top" id="branchTableRow" runat="server">
        <td>
            <div style="padding:0.5em;">
                <asp:button id="m_Add" style="DISPLAY: none;" onclick="InsertClick" runat="server"/>
                <input style="WIDTH: 110px" onclick="if( checkDirty() == true ) if( confirm( 'Continue and ignore changes?' ) == false ) return; parentElement.children[ 0 ].click();" type="button" value="Add new branch">
                <asp:button id="m_Export" onclick="ExportClick" runat="server" Text="Export to CSV" />
                <input onclick="onClose();" type="button" value="Close" style="width:60px" />
            </div>

            <div id="branchTabsContainer">
                <asp:TextBox ID="branchTabIndex" runat="server" Text="0" style="display:none;"/>
                <div class="Tabs">
                    <ul class="tabnav">
                        <li id="BranchTab0" class="selected"><a onclick = "changeBranchTab(0);">Active</a></li>
                        <li id="BranchTab1"><a onclick = "changeBranchTab(1);">Inactive</a></li>
                    </ul>
                </div>

                <div id="BranchTabContent0" style="BORDER-RIGHT: 0px; BORDER-TOP: lightgrey 2px solid; PADDING-BOTTOM: 4px; BORDER-LEFT: lightgrey 2px solid; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid;">
                    <ml:commondatagrid id="m_Grid" runat="server" EnableViewState="True" OnItemDataBound="BranchGridBound" OnItemCommand="BranchGridClick" CellPadding="2">
                        <Columns>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton id="Edit" runat="server" style="DISPLAY: none;" CommandName="Edit"/>
                                    <a href="#" onclick="if( checkDirty() == true ) if( confirm( 'Continue and ignore changes?' ) == false ) return; parentElement.children[ 0 ].click();">edit</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton id="Delete" runat="server" style="DISPLAY: none;" CommandName="Delete"/>
                                    <a href="#" onclick="if( checkDirty() == true ) if( confirm( 'Continue and ignore changes?' ) == false ) return; parentElement.children[ 0 ].click();">delete</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="Branch Name">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" style="DISPLAY: none;" CommandName="Edit" ID="NameLink" NAME="NameLink"/>
                                    <div style="WIDTH: 100%; CURSOR: hand;" onclick="if( checkDirty() == true ) if( confirm( 'Continue and ignore changes?' ) == false ) return; parentElement.children[ 0 ].click();" onmouseover="style.color = 'lightblue';" onmouseout="style.color = '';">
                                    <ml:EncodedLiteral runat="server" id="Name"/>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BranchCode" HeaderText="Branch Code" ItemStyle-Wrap="false" SortExpression="BranchCode"/>
                            <asp:TemplateColumn HeaderText="Branch Status">
                                <ItemTemplate>
                                    <ml:EncodedLiteral id="Status" runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BranchGroupNameList" HeaderText="Branch Groups" ItemStyle-Wrap="false" SortExpression="BranchCode"/>
                            <asp:TemplateColumn HeaderText="Address">
                                <ItemTemplate>
                                    <ml:EncodedLiteral id="Street" runat="server"/>
                                    <br>
                                    <ml:EncodedLiteral id="CityStateZip" runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Phone" HeaderText="Phone" ItemStyle-Wrap="True"/>
                            <asp:BoundColumn DataField="Fax" HeaderText="Fax" ItemStyle-Wrap="False"/>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="False">
                                <ItemTemplate>
                                    <asp:LinkButton id="Add" runat="server" CommandName="Add">add new employee</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </ml:commondatagrid>
                </div>

                <div id="BranchTabContent1" style="BORDER-RIGHT: 0px; BORDER-TOP: lightgrey 2px solid; PADDING-BOTTOM: 4px; BORDER-LEFT: lightgrey 2px solid; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid;">
                    <ml:commondatagrid id="m_InactiveGrid" runat="server" EnableViewState="True" OnItemDataBound="BranchGridBound" OnItemCommand="BranchGridClick" CellPadding="2">
                        <Columns>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton id="Edit" runat="server" style="DISPLAY: none;" CommandName="Edit"/>
                                    <a href="#" onclick="if( checkDirty() == true ) if( confirm( 'Continue and ignore changes?' ) == false ) return; parentElement.children[ 0 ].click();">edit</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton id="Delete" runat="server" style="DISPLAY: none;" CommandName="Delete"/>
                                    <a href="#" onclick="if( checkDirty() == true ) if( confirm( 'Continue and ignore changes?' ) == false ) return; parentElement.children[ 0 ].click();">delete</a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="Branch Name">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" style="DISPLAY: none;" CommandName="Edit" ID="NameLink" NAME="NameLink"/>
                                    <div style="WIDTH: 100%; CURSOR: hand;" onclick="if( checkDirty() == true ) if( confirm( 'Continue and ignore changes?' ) == false ) return; parentElement.children[ 0 ].click();" onmouseover="style.color = 'lightblue';" onmouseout="style.color = '';">
                                        <ml:EncodedLiteral runat="server" id="Name"/>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BranchCode" HeaderText="Branch Code" ItemStyle-Wrap="false" SortExpression="BranchCode"/>
                            <asp:TemplateColumn HeaderText="Branch Status">
                                <ItemTemplate>
                                <ml:EncodedLiteral id="Status" runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BranchGroupNameList" HeaderText="Branch Groups" ItemStyle-Wrap="false" SortExpression="BranchCode"/>
                            <asp:TemplateColumn HeaderText="Address">
                                <ItemTemplate>
                                    <ml:EncodedLiteral id="Street" runat="server"/>
                                    <br>
                                    <ml:EncodedLiteral id="CityStateZip" runat="server"/>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Phone" HeaderText="Phone" ItemStyle-Wrap="True"/>
                            <asp:BoundColumn DataField="Fax" HeaderText="Fax" ItemStyle-Wrap="False"/>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="False">
                                <ItemTemplate>
                                    <asp:LinkButton id="Add" runat="server" CommandName="Add">add new employee</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </ml:commondatagrid>
                </div>
            </div>
        </td>
    </tr>
</table>
