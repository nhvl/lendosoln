<%@ Page language="c#" Codebehind="EditBranch.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.EditBranch" %>
<%@ Register TagPrefix="uc" TagName="BranchAdmin" Src="../../los/admin/BranchAdmin.ascx" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
<head runat="server">
	<title>Edit Branch</title>
	<style type="text/css">
		.ie-only-full-height {
			*height: 98%;
			min-height: 98%;
			min-width: 98%;
		}
	</style>

	<style type="text/css">
		body {
			background-color: red;
		}
	</style>
</head>
<script type="text/javascript">	
	function onInit()
	{
		var errorMessage = document.getElementById("m_errorMessage");
		var commandToDo = document.getElementById("m_commandToDo");
		var addToBranch = document.getElementById("m_addToBranch");

		if( errorMessage != null )
		{
			alert( errorMessage.value );
		}
		else
		if( commandToDo != null )
		{
			if( commandToDo.value == "Close" )
			{
				onClosePopup();
			}
		}
		else
		if( addToBranch != null )
		{
			showModal("/los/admin/EditEmployee.aspx?branchId=" + addToBranch.value + "&brokerId=" + document.getElementById("m_addToBroker").value,
				null,
				null,
				null,
				null,
				{ width: 1000, height: 800, hideCloseButton: true });
		}
		
		if( window.dialogArguments != null )
		{
			<% if( IsPostBack) { %>
				resize( 720 , 740 );
			<% } else{%>
				resize( 720 , 620 );
			<% }%>
		}
	}
</script>

<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto; BACKGROUND-COLOR: #003366" onload="onInit();">
	<form id="EditBranch" method="post" runat="server" class="ie-only-full-height expand-w-content">
		<h4 class="page-header">Edit Branches</h4>
		<div style="BORDER: 2px outset; BACKGROUND-COLOR: gainsboro; display: inline-block; margin: 5px;" class="ie-only-full-height">
			<uc:BranchAdmin id="m_Edit" runat="server"/>
		</div>
		<uc:CModalDlg runat="server"/>
	</form>
</body>
</html>
