using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using System.Xml;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{
	public partial class HiddenCRAListService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "Save":
                    Save();
                    break;
            }
        }

        private void Save() 
        {
            // 4/5/2006 dd - Save Hidden CRA for PML interface.
            BrokerDB broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);

            Hashtable hash = new Hashtable();

            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("list");
            doc.AppendChild(root);

            XmlElement pmlList = doc.CreateElement("pml");
            root.AppendChild(pmlList);

            string[] ids = GetString("Ids").Split(';');

            foreach (string s in ids) 
            {
                if (s.TrimWhitespaceAndBOM() != "") 
                {
                    XmlElement el = doc.CreateElement("item");
                    el.SetAttribute("id", s);
                    pmlList.AppendChild(el);
                }
            }

            broker.BlockedCRAsXmlContent = root.OuterXml;

            broker.Save();

        }

	}
}
