using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using System.Data.SqlClient;
using LendersOffice.Constants;
using LendersOfficeApp.los.RatePrice;
using System.Collections.Generic;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.admin
{
	/// <summary>
	/// Summary description for WarningMessages.
    /// OPM 20030 1-14-08 jk - Allows for each lender to edit and personalize their own ratesheet expiration message.
    /// 
	/// </summary>
    public class WarningMessagesNew : LendersOffice.Common.BasePage
	{
		protected string brokerId;
        protected Button m_SaveButton;
        protected Button m_CloseButton;
		protected TextBox m_newNormalUserExpiredMessage;
        protected TextBox m_newLockDeskExpiredMessage;
        protected TextBox m_newNormalUserCutOffMessage;
        protected TextBox m_newLockDeskCutOffMessage;
        protected TextBox m_newOutsideNormalHoursMessage;
        protected TextBox m_newOutsideClosureDayHourMessage;
        protected string m_newOutsideNormalHoursAndPassInvestorCutOffMessage;


		protected string m_defaultNormalUserExpiredMessage;
		protected string m_defaultLockDeskExpiredMessage;
		protected string m_defaultNormalUserCutOffMessage;
		protected string m_defaultLockDeskCutOffMessage;
		protected string m_defaultOutsideNormalHoursMessage;
		protected string m_defaultOutsideClosureDayHourMessage;
		protected string m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage;

        protected PassthroughLiteral m_lockDeskExpiredDetails;
        protected Literal m_lockDeskCutoffDetails;

        protected Boolean nothingLoaded;
        protected Guid PolicyId
        {
            get { return RequestHelper.GetGuid("policyid"); }
        }
        protected string PolicyName
        {
            get 
            {
                return LendersOffice.ObjLib.LockPolicies.LockPolicy.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId, PolicyId).PolicyNm; 
            }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }


		private void Page_Load(object sender, System.EventArgs e)
		{

			// Put user code to initialize the page here
			BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
			brokerId = principal.BrokerId.ToString();
              nothingLoaded = false;
              Boolean lock_desk = BrokerUserPrincipal.CurrentPrincipal.HasRole(E_RoleT.LockDesk);
           
            //The extra explanations for the 2 lock desk messages are only available if the user in the the role lock desk
              if (lock_desk)
              {
                  m_lockDeskExpiredDetails.Text = "<a href=\'#\"\' onclick=\'f_show(\"LockDeskExpiredDetails\", event);\'?>?</a>";
                  m_lockDeskCutoffDetails.Text = "<a href=\'#\"\' onclick=\'f_show(\"LockDeskCutOffDetails\", event);\'?>?</a>";
              }

            //Keeping the OutsideNormalHoursAndPassInvestorCutOffMessage in case it needs to be used in the future, but for now
            // it is set as a string and set equal to the OutsideNormalHours message.
              m_newOutsideNormalHoursAndPassInvestorCutOffMessage = m_newOutsideNormalHoursMessage.Text;
              SqlParameter[] parameters = {
                                              new SqlParameter("@Broker", principal.BrokerId),
                                              new SqlParameter("@PolicyId", PolicyId)
                                       };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "getRSWarningMessageByBrokerPolicyId", parameters))
                {
                    if (reader.Read())
                    {
                        if (!Page.IsPostBack)
                        {


                            m_newNormalUserExpiredMessage.Text = (string)reader["NormalUserExpiredCustomMsg"];
                            m_newLockDeskExpiredMessage.Text = (string)reader["LockDeskExpiredCustomMsg"];
                            m_newNormalUserCutOffMessage.Text = (string)reader["NormalUserCutOffCustomMsg"];
                            m_newOutsideNormalHoursMessage.Text = (string)reader["OutsideNormalHrCustomMsg"];
                            m_newLockDeskCutOffMessage.Text = (string)reader["LockDeskCutOffCustomMsg"];
                            m_newOutsideClosureDayHourMessage.Text = (string)reader["OutsideClosureDayHrCustomMsg"];
                            m_newOutsideNormalHoursAndPassInvestorCutOffMessage = (string)reader["OutsideNormalHrandPassInvestorCutOffCustomMsg"];
                        }
                    }
                    else 
                        nothingLoaded = true;

                }
            
                

            m_defaultNormalUserExpiredMessage = RateOptionExpirationResult.GetDefaultFriendlyRateLockSubmissionErrorMessage(E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage).Replace("\"", "\\\"").Replace("\'", "\\\'");
            m_defaultLockDeskExpiredMessage = RateOptionExpirationResult.GetDefaultFriendlyRateLockSubmissionErrorMessage(E_LpeRsExpirationByPassPermissionT.ByPassAll, ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage).Replace("\"", "\\\"").Replace("\'", "\\\'");
            m_defaultNormalUserCutOffMessage = RateOptionExpirationResult.GetDefaultFriendlyRateLockSubmissionErrorMessage(E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_CutOffMessage + "{0}").Replace("\"", "\\\"").Replace("\'", "\\\'");
            m_defaultLockDeskCutOffMessage = RateOptionExpirationResult.GetDefaultFriendlyRateLockSubmissionErrorMessage(E_LpeRsExpirationByPassPermissionT.ByPassAll, ConstAppDavid.BlockedRateLockSubmission_CutOffMessage + "{0}").Replace("\"", "\\\"").Replace("\'", "\\\'");
            m_defaultOutsideNormalHoursMessage = RateOptionExpirationResult.GetDefaultFriendlyRateLockSubmissionErrorMessage(E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHour + "{0}").Replace("\"", "\\\"").Replace("\'", "\\\'");
            m_defaultOutsideClosureDayHourMessage = RateOptionExpirationResult.GetDefaultFriendlyRateLockSubmissionErrorMessage(E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_OutsideClosureDayHour + "{0}").Replace("\"", "\\\"").Replace("\'", "\\\'");
            m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage = RateOptionExpirationResult.GetDefaultFriendlyRateLockSubmissionErrorMessage(E_LpeRsExpirationByPassPermissionT.NoByPass, ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff + "{0}" + "#" + "{1}").Replace("\"", "\\\"").Replace("\'", "\\\'");
           
            


            if (m_newNormalUserExpiredMessage.Text.Equals(""))
                m_newNormalUserExpiredMessage.Text = m_defaultNormalUserExpiredMessage;
            if (m_newLockDeskExpiredMessage.Text.Equals(""))
               m_newLockDeskExpiredMessage.Text = m_defaultLockDeskExpiredMessage;
            if (m_newNormalUserCutOffMessage.Text.Equals(""))
                m_newNormalUserCutOffMessage.Text = m_defaultNormalUserCutOffMessage;
            if (m_newOutsideNormalHoursMessage.Text.Equals(""))
                m_newOutsideNormalHoursMessage.Text = m_defaultOutsideNormalHoursMessage;
            if (m_newLockDeskCutOffMessage.Text.Equals(""))
                m_newLockDeskCutOffMessage.Text = m_defaultLockDeskCutOffMessage;
            if (m_newOutsideClosureDayHourMessage.Text.Equals(""))
                m_newOutsideClosureDayHourMessage.Text = m_defaultOutsideClosureDayHourMessage;
            if (m_newOutsideNormalHoursAndPassInvestorCutOffMessage.Equals(""))
                m_newOutsideNormalHoursAndPassInvestorCutOffMessage = m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage;

            m_SaveButton.Attributes.Add("onclick", "replace();");

            
		}

       

		protected void SaveClick(object sender, System.EventArgs e)
		{
			  String procedure = "";

            if (m_newNormalUserExpiredMessage.Text.Equals(m_defaultNormalUserExpiredMessage))
                m_newNormalUserExpiredMessage.Text = "";
            if (m_newLockDeskExpiredMessage.Text.Equals(m_defaultLockDeskExpiredMessage))
                m_newLockDeskExpiredMessage.Text = "";
            if (m_newNormalUserCutOffMessage.Text.Equals(m_defaultNormalUserCutOffMessage))
                m_newNormalUserCutOffMessage.Text = "";
            if (m_newLockDeskCutOffMessage.Text.Equals(m_defaultLockDeskCutOffMessage))
                m_newLockDeskCutOffMessage.Text = "";
            if (m_newOutsideNormalHoursMessage.Text.Equals(m_defaultOutsideNormalHoursMessage))
                m_newOutsideNormalHoursMessage.Text = "";
            if (m_newOutsideClosureDayHourMessage.Text.Equals(m_defaultOutsideClosureDayHourMessage))
                m_newOutsideClosureDayHourMessage.Text = "";
            if (m_newOutsideNormalHoursAndPassInvestorCutOffMessage.Equals(m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage))
                m_newOutsideNormalHoursAndPassInvestorCutOffMessage = "";



            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
                SqlParameter[] parameters = {
                    new SqlParameter("@Broker", BrokerUserPrincipal.CurrentPrincipal.BrokerId),
                    new SqlParameter("@NormalUserExpiredMsg", m_newNormalUserExpiredMessage.Text),
                    new SqlParameter("@LockDeskExpiredMsg", m_newLockDeskExpiredMessage.Text),
                    new SqlParameter("@NormalUserCutOffMsg", m_newNormalUserCutOffMessage.Text),
                    new SqlParameter("@LockDeskCutOffMsg", m_newLockDeskCutOffMessage.Text),
                    new SqlParameter("@OutsideNormalHrMsg", m_newOutsideNormalHoursMessage.Text),
                    new SqlParameter("@OutsideClosureDayHrMsg",  m_newOutsideClosureDayHourMessage.Text),
                    new SqlParameter("@OutsideNormalHrAndPassInvestorCutOffMsg", m_newOutsideNormalHoursMessage.Text),
                    new SqlParameter("@PolicyId", PolicyId)
                };

                //If nothingLoaded is true, then this broker doesn't have an entry in the database yet and a new entry
                // must be created. If it's false, the broker does have an entry and that entry must be updated.
                if (nothingLoaded)
                {
                    procedure = "CreateRSWarningMsgByBrokerPolicyId";
                }
                else
                {
                    procedure = "UpdateRSWarningMsgByBrokerPolicyId";
                }
                int result = 0;
                try
                {
                    result = StoredProcedureHelper.ExecuteNonQuery(brokerId, procedure, 1, parameters);

                }
                catch (SqlException)
                {

                    result = 1;
                }

                LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "OK", "true");
            
              
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
