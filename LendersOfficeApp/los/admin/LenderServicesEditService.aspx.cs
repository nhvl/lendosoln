﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Security;

    /// <summary>
    /// Service class for the lender service editor.
    /// </summary>
    public partial class LenderServicesEditService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets the permission required to load this page.
        /// </summary>
        /// <value>The permission required for page load.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Gets the service method to run.
        /// </summary>
        /// <param name="methodName">The service method name.</param>
        protected override void Process(string methodName)
        {
            if (!PrincipalFactory.CurrentPrincipal.BrokerDB.IsEnableVOXConfiguration)
            {
                return;
            }

            switch (methodName)
            {
                case nameof(this.CalculateVendorByServiceType):
                    this.CalculateVendorByServiceType();
                    break;
                case nameof(this.CalculateResellerbyVendorAndServiceType):
                    this.CalculateResellerbyVendorAndServiceType();
                    break;
                case nameof(this.SaveLenderService):
                    this.SaveLenderService();
                    break;
                case nameof(this.DeleteLenderService):
                    this.DeleteLenderService();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, $"Method not implemented {methodName}");
            }
        }

        /// <summary>
        /// Calculates the vendor by service type.
        /// </summary>
        private void CalculateVendorByServiceType()
        {
            VOXServiceT serviceT = GetEnum<VOXServiceT>("ServiceType");
            var vendorMap = VOXVendor.LoadLightVendorsWithServices();
            var vendorList = VOXLenderService.FilterValidVendorsByServiceT(vendorMap.Values, serviceT);

            List<VendorDDLOption> options = new List<VendorDDLOption>();
            foreach (var vendor in vendorList)
            {
                options.Add(new VendorDDLOption() { CanPayWithCreditCard = vendor.CanPayWithCreditCard, CompanyName = vendor.CompanyName, Id = vendor.VendorId });
            }

            SetResult("Vendors", SerializationHelper.JsonNetSerialize(options));
        }

        /// <summary>
        /// Calculates the reseller by vendor and service type.
        /// </summary>
        private void CalculateResellerbyVendorAndServiceType()
        {
            VOXServiceT serviceT = GetEnum<VOXServiceT>("ServiceType");
            int vendorId = GetInt("AssociatedVendorId");
            var vendorMap = VOXVendor.LoadLightVendorsWithServices();
            LightVOXVendor chosenVendor;
            if (!vendorMap.TryGetValue(vendorId, out chosenVendor))
            {
                SetResult("Success", false);
                SetResult("Errors", "Invalid vendor id");
            }
            else
            {
                bool isDirect;
                var resellerList = VOXLenderService.FilterValidResellersByVendorAndServiceT(vendorMap.Values, serviceT, chosenVendor, out isDirect);

                List<VendorDDLOption> options = new List<VendorDDLOption>();
                if (isDirect)
                {
                    options.Add(new VendorDDLOption() { CanPayWithCreditCard = false, CompanyName = "None/Direct Vendor", Id = VOXLenderService.NoneDirectVendorId });
                }

                foreach (var reseller in resellerList)
                {
                    options.Add(new VendorDDLOption() { CanPayWithCreditCard = reseller.CanPayWithCreditCard, CompanyName = reseller.CompanyName, Id = reseller.VendorId });
                }

                SetResult("Success", true);
                SetResult("Resellers", SerializationHelper.JsonNetSerialize(options));
            }
        }

        /// <summary>
        /// Saves the lender service.
        /// </summary>
        private void SaveLenderService()
        {
            string errors;
            VOXLenderServiceViewModel viewModel = SerializationHelper.JsonNetDeserialize<VOXLenderServiceViewModel>(GetString("ServiceViewModel"));
            VOXLenderService lenderService = VOXLenderService.CreateFromViewModel(viewModel, PrincipalFactory.CurrentPrincipal.BrokerId, out errors);

            if (lenderService == null)
            {
                this.SetResult("Errors", errors);
                this.SetResult("Success", false);
                return;
            }

            List<VOAOption> voaOptions = null;
            if (lenderService.ServiceType == VOXServiceT.VOA_VOD)
            {
                voaOptions = SerializationHelper.JsonNetDeserialize<List<VOAOption>>(GetString("VoaOptionJson"));
            }

            bool isNew = lenderService.IsNew;
            if (!lenderService.Save(out errors, voaOptions))
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", errors);
                return;
            }

            this.SetResult("Success", true);
            this.SetResult("ServiceName", Tools.VendorServiceTToString(lenderService.ServiceType));
            this.SetResult("LenderServiceId", lenderService.LenderServiceId);
            this.SetResult("IsNew", isNew);
        }

        /// <summary>
        /// Deletes the lender service.
        /// </summary>
        private void DeleteLenderService()
        {
            int lenderServiceId = GetInt("ServiceId");

            string errors;
            if (!VOXLenderService.DeleteService(PrincipalFactory.CurrentPrincipal.BrokerId, lenderServiceId, out errors))
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", errors);
            }
            else
            {
                this.SetResult("Success", true);
            }
        }

        /// <summary>
        /// Struct to hold options for vendor DDL.
        /// </summary>
        private struct VendorDDLOption
        {
            /// <summary>
            /// Gets or sets the company name.
            /// </summary>
            /// <value>The company name.</value>
            public string CompanyName { get; set; }

            /// <summary>
            /// Gets or sets the id.
            /// </summary>
            /// <value>The id for the vendor.</value>
            public int Id { get; set; }
            
            /// <summary>
            /// Gets or sets a value indicating whether this can pay with credit card.
            /// </summary>
            /// <value>Can pay with credit card.</value>
            public bool CanPayWithCreditCard { get; set; } 
        }
    }
}