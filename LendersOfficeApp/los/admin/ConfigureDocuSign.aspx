﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfigureDocuSign.aspx.cs" Inherits="LendersOfficeApp.los.admin.ConfigureDocuSign" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DocuSign Configuration</title>
    <style type="text/css">
        .indented {
            margin-left: 2em;
        }
        .BottomButtons {
            width: 100%;
            position: fixed;
            bottom: 0;
            left: 0;
            text-align: center;
            padding-bottom: 2px;
        }
        .ContentBody {
            padding: 4px;
            line-height: 20px;
            height: 94vh;
            overflow-y: auto;
        }
        .row
        {
            display: block;
        }
        .hidden
        {
            display: none;
        }
        .bold
        {
            font-weight: bold;
        }
        .ErrorMessage
        {
            color:red;
        }
        option[disabled], option:disabled {
            background-color: lightgray;
        }
        .sectionHeader {
            margin-top: 8px;
        }
        .Branding input.BrandId {
            font-family: Consolas, monospace;
            width:235px;
        }
        .InvalidInput {
            border: 1px solid red;
        }
    </style>
    <script type="text/javascript">
        jQuery(function ($) {
            var channels = {
                0: { description: 'Unspecified' },
                1: { description: 'Retail' },
                2: { description: 'Wholesale' },
                3: { description: 'Correspondent' },
                4: { description: 'Broker' }
            };
            function isGuid(value) {
                return /^[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12}$/i.test(value);
            }

            var mfaOptions = $.map(docuSignEnabledMfa, function (value, name) { if (name !== "NoSelection") return name; });
            var jqDocuSignOptionInputs;
            var brandIdInputs;
            function verifyPageState() {
                var noMfaEnabled = !$('[id|="Mfa"]').is(function (i, element) { return element.checked; });
                brandIdInputs = brandIdInputs || $('input.BrandId');
                jqDocuSignOptionInputs = jqDocuSignOptionInputs || $('input, select').not('#EnableDocuSign, #SaveButton, #CancelButton');
                brandIdInputs.each(function (i, el) {
                    $(el).toggleClass('InvalidInput', el.value !== '' && !isGuid(el.value));
                });
                var invalidBrandId = brandIdInputs.filter('.InvalidInput').length !== 0;
                $('.Branding .ErrorMessage').toggle(invalidBrandId);
                jqDocuSignOptionInputs.prop('disabled', !$('#EnableDocuSign').prop('checked'));
                $('#Mfa-InPerson').prop('checked', noMfaEnabled);
                $('#DocuSignIdCheckConfigContainer').toggle($('#Mfa-IdCheck').prop('checked'));
                $('#SaveButton').prop('disabled', !ML.docuSignUserId && $('#EnableDocuSign').prop('checked'));
                $('#EnabledMfaWarning').toggle($('#EnableDocuSign').prop('checked') && noMfaEnabled);
                if (!$('#EnableDocuSign').prop('checked')) return;

                $('#SaveButton').prop('disabled', noMfaEnabled || invalidBrandId);
                for (var i = 0; i < mfaOptions.length; i++) {
                    var mfaOptionEnabled = $('#Mfa-' + mfaOptions[i]).prop('checked');
                    var defaultOption = $('#MfaDefaultOption-' + mfaOptions[i]);
                    defaultOption.prop('disabled', !mfaOptionEnabled);
                    if (!mfaOptionEnabled && defaultOption.prop('selected')) {
                        $('#MfaDefaultOption-NoSelection').prop('selected', true);
                    }
                }
            }

            function initializeSelections() {
                $('#EnableDocuSign').prop('checked', ML.docuSignEnabled);

                $('#DocuSignUserName').text(ML.docuSignUserName);
                $('#DocuSignUserId').text(ML.docuSignUserId);
                if (docuSignEnabledMfa)
                {
                    for (var i = 0; i < mfaOptions.length; i++) {
                        $('#Mfa-' + mfaOptions[i]).prop('checked', docuSignEnabledMfa[mfaOptions[i]]);
                    }
                }

                $('#IdCheckConfigurationName').val(ML.docuSignIdCheckConfig);

                if (ML.docuSignDefaultMfa) {
                    $('#MfaDefault').val(ML.docuSignDefaultMfa);
                }
                else {
                    $('#MfaDefault').val('NoSelection');
                }

                if (ML.docuSignEnvironment) {
                    $('#DocuSignEnvironment-' + ML.docuSignEnvironment).prop('checked', true);
                }
                else {
                    $('#DocuSignEnvironment-Production').prop('checked', true);
                }

                $('#BrandsByChannel').append(
                    $('<tr>').append($('<th>').append('Channel'), $('<th>').append('Brand ID'), $('<th>').append('Brand Name')),
                    $.map(channels, function (channel) {
                        var idInputId = 'BrandId' + channel.description;
                        var nameInputId = 'BrandName' + channel.description;
                        var channelLabel = $('<label>').prop({ 'class': 'FieldLabel', 'for': idInputId }).text(channel.description);
                        var channelBrandIdInput = $('<input>').prop({ 'class': 'BrandId', type: 'text', name: idInputId, id: idInputId, maxLength: 36, value: ML['docuSign' + idInputId] || '' });
                        var channelBrandNameInput = $('<input>').prop({ 'class': 'BrandName', type: 'text', name: nameInputId, id: nameInputId, maxLength: 100, value: ML['docuSign' + nameInputId] || '' });
                        return $('<tr>').append($('<td>').append(channelLabel), $('<td>').append(channelBrandIdInput), $('<td>').append(channelBrandNameInput));
                    }));

                $('.userInfoAlreadyAuthenticated').toggle(!!ML.docuSignUserId);
                $('.userInfoNotAuthenticated').toggle(!ML.docuSignUserId);
                if (!!ML.docuSignUserId)
                    $('#AuthenticateInDocuSignButton').val('Re-Authenticate In DocuSign');
            }

            initializeSelections()
            verifyPageState();

            $('#Mfa-IdCheck').change(function (event) { $('#DocuSignIdCheckConfigContainer').toggle(event.target.checked); });
            $('#Mfa-InPerson').change(function (event) {
                if (event.target.checked)
                    $('[id|="Mfa"]').not('#Mfa-InPerson').prop('checked', false);
            });
            $('#EnableDocuSign, [id|="Mfa"], input.BrandId').change(verifyPageState);

            $('#SaveButton').click(
                function () {
                    if (!$('#EnableDocuSign').prop('checked')) {
                        var args = {
                            DocuSignEnabled: false
                        };
                    }
                    else
                    {
                        var enabledMfa = {};
                        for (var i = 0; i < mfaOptions.length; i++) {
                            enabledMfa[mfaOptions[i]] = $('#Mfa-' + mfaOptions[i]).prop('checked');
                        }

                        var args = {
                            DocuSignEnabled: true,
                            TargetEnvironment: $('input[name="docuSignEnvironment"]:checked').val(),
                            EnabledMfa: JSON.stringify(enabledMfa),
                            DefaultMfaOption: $('#MfaDefault').val(),
                            IdCheckConfigurationName: $('#IdCheckConfigurationName').val()
                        };

                        $('input.BrandId, input.BrandName').each(function (i, el) {
                            args[el.id] = el.value;
                        });
                    }

                    var result = gService.save.call("SaveDocuSignOptions", args);

                    if (result.error)
                    {
                        alert(result.UserMessage);
                    }
                    else if (result.value.Status !== "Success")
                    {
                        alert(result.value.Message);
                    }
                    else
                    {
                        parent.LQBPopup.Hide();
                    }
                });

            function GetDocuSignAuthUrl() {
                var args = {
                    useSandbox: $('input[name="docuSignEnvironment"]:checked').val() !== 'Production'
                };

                var result = gService.save.call('GetOAuthConsentUrl', args);

                if (!result.error && !result.value.Error)
                {
                    return result.value.oAuthConsentUrl;
                }
                else if (result.value.Error)
                {
                    alert("System Error: " + result.value.Error);
                }
                else
                {
                    alert("System Error. Please refresh the page and contact us if this happens again.");
                }
            }

            function RegisterAuthentication(authObj)
            {
                ML.docuSignUserName = authObj.docuSignUserName;
                ML.docuSignUserId = authObj.docuSignUserId;
            }

            $('#AuthenticateInDocuSignButton').click(function () {
                window.open(GetDocuSignAuthUrl(),
                    'DocuSignLogin',
                    'menubar=no,toolbar=no,personalbar=no,resizable=yes');
                gService.save.call("SaveDocuSignOptions", { DocuSignEnabled: true, TargetEnvironment: $('input[name="docuSignEnvironment"]:checked').val() });
            });
        });
    </script>
</head>
<body>
    <h4 class="page-header">DocuSign Configuration</h4>
    <form id="aspform" runat="server">
        <div class="ContentBody">
            <label class="FieldLabel"><input type="checkbox" id="EnableDocuSign" /> Enable DocuSign Integration</label>
            <div class="indented">
                <div class="FieldLabel sectionHeader">DocuSign Environment</div>
                <div class="indented">
                    <label><input type="radio" name="docuSignEnvironment" id="DocuSignEnvironment-Production" checked="checked" value="Production"/>Production</label>
                    <label><input type="radio" name="docuSignEnvironment" id="DocuSignEnvironment-Test" value="Test"/>Test</label>
                </div>
                <div class="FieldLabel sectionHeader">DocuSign User Information</div>
                <div class="indented userInfoAlreadyAuthenticated">
                    <span class="bold">DocuSign User Name: </span><span id="DocuSignUserName" style="-ms-user-select: text"></span><br />
                    <span class="bold">DocuSign User ID: </span><span style="font-family:Consolas, monospace; -ms-user-select: all" id="DocuSignUserId"></span><br />
                </div>
                <div class="indented userInfoNotAuthenticated">
                    <span>You have not authenticated LendingQB with DocuSign.</span>
                </div>
                <div class="indented" title="Make sure the DocuSign Environment is correct before logging in!">
                    <input type="button" value="Authenticate In DocuSign" id="AuthenticateInDocuSignButton" />
                </div>
                <div class="userInfoAlreadyAuthenticated">
                    <div class="FieldLabel sectionHeader">Multi-Factor Authentication Options</div>
                    <div class="indented">
                        <div id="EnabledMfaWarning" class="ErrorMessage">At least one option must be selected.</div>
                        <label class="row" title="MFA is required when a request for e-signatures is sent via email.">
                            <input type="checkbox" id="Mfa-NoMfa" disabled="disabled"/> No MFA (In-Person Only)</label>
                        <label class="row"><input type="checkbox" id="Mfa-AccessCode"/> Access Code</label>
                        <label class="row"><input type="checkbox" id="Mfa-IdCheck"/> ID Check</label>
                        <div id="DocuSignIdCheckConfigContainer" class="indented" style="display: none">
                            <label class="FieldLabel indented">Configuration Name <input type="text" id="IdCheckConfigurationName" /></label>
                        </div>
                        <label class="row"><input type="checkbox" id="Mfa-AccessCodeAndIdCheck"/> Access Code & ID Check</label>
                        <label class="row"><input type="checkbox" id="Mfa-Phone"/> Phone Authentication</label>
                        <label class="row"><input type="checkbox" id="Mfa-Sms"/> SMS Authentication</label>
                    </div>
                    <div class="indented">
                        <span class="FieldLabel">Default Multi-Factor Authentication Method</span>
                        <select id="MfaDefault">
                            <option id="MfaDefaultOption-NoSelection" value="NoSelection">No Default</option>
                            <option id="MfaDefaultOption-NoMfa" value="NoMfa">Select "No MFA"</option>
                            <option id="MfaDefaultOption-AccessCode" value="AccessCode">Access Code</option>
                            <option id="MfaDefaultOption-IdCheck" value="IdCheck">ID Check</option>
                            <option id="MfaDefaultOption-AccessCodeAndIdCheck" value="AccessCodeAndIdCheck">Access Code & ID Check</option>
                            <option id="MfaDefaultOption-Phone" value="IdCheck">Phone Authentication</option>
                            <option id="MfaDefaultOption-Sms" value="Sms">SMS Authentication</option>
                        </select>
                    </div>
                    <div class="Branding">
                        <div class="FieldLabel sectionHeader">Branding</div>
                        <div class="ErrorMessage">Brand ID must be a valid UUID</div>
                        <div class="indented">
                            <table id="BrandsByChannel"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="BottomButtons">
            <input type="button" value="Save" id="SaveButton"/>
            <input type="button" value="Cancel" id="CancelButton" onclick="parent.LQBPopup.Return();" />
        </div>
    </form>
</body>
</html>
