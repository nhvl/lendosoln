﻿namespace LendersOfficeApp.Los.Admin
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Admin.DocuSign;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// A page for configuring DocuSign options for a branch.
    /// </summary>
    public partial class ConfigureDocuSignBranch : BaseServicePage
    {
        /// <summary>
        /// Gets the identifier of the branch.
        /// </summary>
        public Guid BranchId => RequestHelper.GetGuid("id");

        /// <summary>
        /// Gets the permission required to load this page.
        /// </summary>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage => new[]
        {
            Permission.AllowViewingAndEditingBranches
        };

        /// <summary>
        /// Raises the control's Init event.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EnableJqueryMigrate = false;
            this.RegisterService("save", "/Los/Admin/ConfigureDocuSignBranchService.aspx");
        }

        /// <summary>
        /// Raises the control's Load event.
        /// </summary>
        /// <param name="e">The event arguments.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Note: If these settings end up containing sensitive data, add an extra load here to check that the lender
            // settings are properly enabled.  For the time being, I'm not too worried about someone loading/saving brand
            // information for a branch without DocuSign enabled.
            Guid branchId = this.BranchId;
            this.RegisterJsGlobalVariables("BranchId", branchId);
            var docuSignSettings = BranchDocuSignSettings.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId, branchId);
            if (docuSignSettings != null)
            {
                this.RegisterJsGlobalVariables("docuSignBrandIdUnspecified", docuSignSettings.BrandIdForBlank.ToString());
                this.RegisterJsGlobalVariables("docuSignBrandIdRetail", docuSignSettings.BrandIdForRetail.ToString());
                this.RegisterJsGlobalVariables("docuSignBrandIdWholesale", docuSignSettings.BrandIdForWholesale.ToString());
                this.RegisterJsGlobalVariables("docuSignBrandIdCorrespondent", docuSignSettings.BrandIdForCorrespondent.ToString());
                this.RegisterJsGlobalVariables("docuSignBrandIdBroker", docuSignSettings.BrandIdForBroker.ToString());
                this.RegisterJsGlobalVariables("docuSignBrandNameUnspecified", docuSignSettings.BrandNameForBlank);
                this.RegisterJsGlobalVariables("docuSignBrandNameRetail", docuSignSettings.BrandNameForRetail);
                this.RegisterJsGlobalVariables("docuSignBrandNameWholesale", docuSignSettings.BrandNameForWholesale);
                this.RegisterJsGlobalVariables("docuSignBrandNameCorrespondent", docuSignSettings.BrandNameForCorrespondent);
                this.RegisterJsGlobalVariables("docuSignBrandNameBroker", docuSignSettings.BrandNameForBroker);
            }
        }
    }
}