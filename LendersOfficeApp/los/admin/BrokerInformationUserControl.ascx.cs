using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;
//using LendersOffice.Features;
using LendersOffice.Common;

namespace LendersOfficeApp.los.admin
{
	public partial  class BrokerInformationUserControl : System.Web.UI.UserControl
	{
 // OPM 19976
        private BrokerDB m_bdb = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;

        private void InitializeComponent()
        {
        
        }

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        protected override void OnInit( System.EventArgs a ) 
        {
			// Initialize this web control.

			base.OnInit( a );
			
			Zipcode.SmartZipcode( City , State );
            FhaLenderIdNineValidator.Enabled = m_bdb.UseFHATOTALProductionAccount;

            this.LegalEntityIdentifierValidator.ValidationExpression = LqbGrammar.DataTypes.RegularExpressionString.LegalEntityIdentifier.ToString();
            this.LegalEntityIdentifierValidator.ErrorMessage = ErrorMessages.InvalidLegalEntityIdentifier;
        }

    }

}
