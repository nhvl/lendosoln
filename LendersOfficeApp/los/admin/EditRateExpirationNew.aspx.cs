using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using System.Data.SqlClient;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Collections.Generic;
using LendersOffice.ObjLib.LockPolicies;

namespace LendersOfficeApp.los.admin
{
	public partial class EditRateExpirationNew : BasePage
	{
		private DateTime										m_closeDateParam = SmallDateTime.MinValue;
		private string											m_action;
		private string											m_timezoneSetting;

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

		protected Guid PolicyId
		{
            get { return RequestHelper.GetGuid("policyid"); }
		}
        private Guid BrokerId
        {
            get { return PrincipalFactory.CurrentPrincipal.BrokerId; }
        }

        /// <summary>
        /// A backing field to contain the holiday closures.
        /// </summary>
        private LockDeskClosures holidayClosures;

        /// <summary>
        /// Gets the holiday closures being displayed on this page.
        /// </summary>
        /// <value>The holiday closures being displayed on this page.</value>
        protected LockDeskClosures HolidayClosures
        {
            get
            {
                if (this.holidayClosures == null)
                {
                    this.holidayClosures = new LockDeskClosures(this.BrokerId, this.PolicyId);
                }

                return this.holidayClosures;
            }
        }

        private bool IsNew
		{
			get { return m_closeDateParam == SmallDateTime.MinValue;}
		}

		public string TimezoneSetting
		{
			get { return m_timezoneSetting; }
			set { ViewState.Add( "TimezoneSetting" , m_timezoneSetting = value ); }
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			string closuredate = RequestHelper.GetSafeQueryString("closureDate");
			m_action = RequestHelper.GetSafeQueryString("cmd");

			if(!IsPostBack)
			{
				if(RequestHelper.GetSafeQueryString("timezone") == "")
					TimezoneSetting = "PST";
				else
					TimezoneSetting = RequestHelper.GetSafeQueryString("timezone");

				m_timezone.Text =  GetTimezoneLabel(TimezoneSetting);
                if (string.Equals(m_action, "add", StringComparison.OrdinalIgnoreCase))
                {
                    m_StartTime.Text = "12:00AM";
                    m_EndTime.Text = "11:59PM";
                    m_ClosedAllDay.SelectedIndex = 0;
                    m_ClosedForBusiness.SelectedIndex = 0;
                }
                else if (string.Equals(m_action, "edit", StringComparison.OrdinalIgnoreCase))
				{
                    m_ClosureDate.ReadOnly = true;
                    if (closuredate == null || !DateTime.TryParse(closuredate, out m_closeDateParam))
                    {
                        m_ErrorMessage.Text = "Invalid Holiday Closure.";
                    }
                    else
                    {
                        var holidayClosure = this.HolidayClosures.FindClosureOrDefault(m_closeDateParam);
                        if (holidayClosure == null)
                        {
                            m_ErrorMessage.Text = "Holiday Closure not found.";
                        }
                        else
                        {
                            m_ClosureDate.Text = holidayClosure.ClosureDate.ToShortDateString();
                            bool closedAllDay = holidayClosure.IsClosedForLocks;
                            bool closedForBusiness = holidayClosure.IsClosedForBusiness;
                            m_ClosedAllDay.SelectedIndex = closedAllDay ? 0 : 1;
                            m_ClosedForBusiness.SelectedIndex = closedForBusiness ? 0 : 1;
                            m_StartTime.Text = closedAllDay ? "12:00AM" : holidayClosure.LockDeskOpenTimeInLenderTimezone.ToString();
                            m_EndTime.Text = closedAllDay ? "11:59PM" : holidayClosure.LockDeskCloseTimeInLenderTimezone.ToString();
                        }
					}
				}
			} // end if !IsPostBack
			else
			{
				if( ViewState[ "TimezoneSetting" ] != null )
					m_timezoneSetting = ViewState[ "TimezoneSetting" ].ToString();
				else
					m_timezoneSetting = "PST";

                m_timezone.Text = GetTimezoneLabel(m_timezoneSetting);
			}
		}

        private string GetTimezoneLabel(string timezone)
        {
            switch (timezone.ToUpper().TrimWhitespaceAndBOM())
            {
                case "PST":
                    return "Pacific Time";
               
                case "CST":
                   return  "Central Time";
                case "MST":
                    return  "Mountain Time";
                case "EST":
                    return "Eastern Time";
                default:
                    return m_timezoneSetting;
            }
        }

        private string ValidateValues()
        {
            DateTime closureDate;
            if (!DateTime.TryParse(m_ClosureDate.Text, out closureDate))
            {
                return "Please enter a valid closure date.";
            }

			DateTime start = DateTime.Parse(m_StartTime.Text);
			DateTime end = DateTime.Parse(m_EndTime.Text);

			if(m_ClosedAllDay.SelectedIndex != 0)
			{
				if((start.TimeOfDay < TimeSpan.FromHours(3)) || (end.TimeOfDay < TimeSpan.FromHours(3)))
				{
					return "Please enter times between 3 AM and 11:59 PM";
				}

				if(start >= end)
				{
					return "Start time must come before end time.";
				}
			}

            var closure = this.HolidayClosures.FindClosureOrDefault(closureDate);
            if (string.Equals(m_action, "add", StringComparison.OrdinalIgnoreCase)
                && closure != null
                && closure.SourceHoliday == null)
            {
                return "Entry already exists.";
            }

            return string.Empty;
        }

		protected void OKClick(object sender, System.EventArgs e)
		{
            if(!Page.IsValid)
            {
                return;
            }

			string errorMsg = ValidateValues();
			if(!string.IsNullOrWhiteSpace(errorMsg))
			{
				m_ErrorMessage.Text = "Unable to save Holiday Closure - " + errorMsg;
				return;
			}

			if (!string.Equals(m_action, "add", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(m_action, "edit", StringComparison.OrdinalIgnoreCase))
			{
				LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "OK", "true") ;
				return;
			}

			DateTime closureDate = DateTime.Parse(m_ClosureDate.Text);
			bool closedAllDay = m_ClosedAllDay.SelectedIndex == 0;
			bool isClosedForBusiness = m_ClosedForBusiness.SelectedIndex == 0;

			if(closedAllDay)
			{
				m_StartTime.Text = DateTime.Parse("12:00AM").ToString();
				m_EndTime.Text = DateTime.Parse("11:59PM").ToString();
			}

			DateTime startTime = DateTime.Parse(m_StartTime.Text);
			DateTime endTime = DateTime.Parse(m_EndTime.Text);

            bool saveSuccessful = this.HolidayClosures.SaveClosure(new LockDeskClosureItem(
                closureDate,
                isClosedForBusiness,
                closedAllDay,
                startTime,
                endTime));

			if (saveSuccessful)
            {
                LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "OK", "true");
            }
			else
            {
                m_ErrorMessage.Text = "Unable to save Holiday Closure.";
            }
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
