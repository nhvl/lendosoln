﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Security;

namespace LendersOfficeApp.los.admin
{
    public partial class ViewOriginatorCompensationAuditEvent : LendersOffice.Common.BaseServicePage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                    {
                        Permission.CanAdministrateExternalUsers
                    };
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}
