<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="ShowLoansAssociatedWithBranch.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.ShowLoansAssociatedWithBranch" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>Loans Associated With Branch</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto; BACKGROUND-COLOR: #003366">
		<script language="javascript">
<!--
	function _init() 
    {
        resize( 700 , 800 );
        
        var checked = false;
        var table = document.getElementById("m_dg");
        
		for( var i=0; i<table.rows.length; i++ )
		{
			table.rows[i].cells[0].children[0].checked = checked;
			onClickCheckRow(table.rows[i].cells[0].children[0]);
		}        
	}

	function checkAllBoxes(obj)
	{
		var checked=false;
		var table = document.getElementById("m_dg");

		if( obj.checked )
		{
			checked=true;
		}
		for( var i=1; i<table.rows.length; i++ )
		{
			table.rows[i].cells[0].children[0].checked = checked;
			onClickCheckRow(table.rows[i].cells[0].children[0]);
		}
		
		checkboxChanged(obj);
	}
	
	function checkboxChanged(obj)
	{
		var enable = false;
		var table = document.getElementById("m_dg");
		var button = <%= AspxTools.JsGetElementById(m_MoveLoans) %>;
		var continueScanning = true;
		
		var numberOfRows = table.rows.length;
		var currentRowNumber = 1;
		
		// If this is true it means that only the header row exists.
		if ( currentRowNumber == numberOfRows )
		{
			continueScanning = false;
		}
		
		while ( continueScanning )
		{
			enable = table.rows[currentRowNumber].cells[0].children[0].checked;
			currentRowNumber++;

			if ( currentRowNumber == numberOfRows || enable )
			{
				continueScanning = false;
			}
			
		}

		button.disabled = !enable;
	}

	// 08/26/08 ck - OPM 23916. Highlight rows in salmon when checked.
	function onClickCheckRow( obj ) 
	{ 
		try
		{
			// Apply row-check effects to grid.
			if( obj.checked != null )
			{		
				highlightRowByCheckbox( obj );
			}
			
			checkboxChanged(obj);
		}
		catch( e )
		{
			window.status = e.message;
		}
	}	
	
	function onLoadFunction()
	{
		checkboxChanged(null);
	}
	
	window.onload = onLoadFunction;
	
    //-->
		</script>
		<h4 class="page-header">View Loans</h4>
		<form id="ShowLoans" method="post" runat="server">
			<table cellspacing="0" cellpadding="8" border="0" width="100%" height="100%">
				<tr>
					<td height="100%">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
							<tr>
								<td height="0%" style="BORDER-RIGHT: 2px outset; BORDER-TOP: 2px outset; BORDER-LEFT: 2px outset; BACKGROUND-COLOR: gainsboro">
									<table cellspacing="2" cellpadding="3" border="0" width="100%" height="100%">
										<tr>
											<td noWrap height="0%">
												<asp:Panel id="m_headerPanel" style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 0px; BORDER-TOP: 2px groove; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; BORDER-LEFT: 2px groove; PADDING-TOP: 0px; BORDER-BOTTOM: 2px groove; BACKGROUND-COLOR: gainsboro" Runat="server">
													<TABLE height="100" cellSpacing="10" cellPadding="0" width="100%" border="0">
														<TR>
															<TD>
																<ml:EncodedLabel id="m_branchNameLabel" style="FONT-SIZE: 13px" Runat="server" Font-Bold="True"></ml:EncodedLabel><BR>
																<ml:EncodedLabel id="m_tooManyLoans" style="FONT-SIZE: 12px" Runat="server" ForeColor="red" Font-Bold="True"></ml:EncodedLabel><BR>
																<ml:EncodedLabel id="m_invalidLoansLabel" style="FONT-SIZE: 13px" Runat="server" Font-Bold="True">(Please contact the LendingQB Support team for assistance in modifying these deleted loans.)</ml:EncodedLabel><BR>
															</TD>
														</TR>
														<TR>
															<TD style="FONT-WEIGHT: bolder; FONT-SIZE: 12px">
																<ml:EncodedLabel runat="server" style="FONT-SIZE: 12px" Font-Bold="True">Move Loans to Branch: </ml:EncodedLabel>
																<asp:DropDownList id="m_BranchDropdownList" runat="server"></asp:DropDownList>
																<asp:Button id="m_MoveLoans" onclick="MoveLoansClick" runat="server" Enabled="False" Text="Move Loans"></asp:Button>
															</TD>
														</TR>
													</TABLE>
												</asp:Panel>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr height=100%>
								<td>
									<table cellpadding="3" cellspacing="0" height="100%" width="100%">
										<td height="100%" style="BORDER-RIGHT: 2px outset; BORDER-LEFT: 2px outset; BORDER-BOTTOM: 2px outset; BACKGROUND-COLOR: gainsboro">
											<table cellspacing="0" cellpadding="0" height="100%" width="100%">
												<tr>
													<td colspan="2" vAlign="bottom">
														<div class="Tabs">
														    <ul class="tabnav">
														        <li runat="server" id="tab0"><a href="#" onclick="__doPostBack('changeTabs', 0);">Valid Loans</a></li>
														        <li runat="server" id="tab1"><a href="#" onclick="__doPostBack('changeTabs', 1);">Invalid (Deleted) Loans</a></li>
														    </ul>
												        </div>
													</td>
												</tr>
												<tr height=100%>
													<td>
														<div style="OVERFLOW-Y:scroll; BORDER-RIGHT:0px; PADDING-RIGHT:4px; PADDING-LEFT:4px; BORDER-LEFT:2px outset; PADDING-TOP:4px; BORDER-BOTTOM:lightgrey 2px solid; height : 563px" valign="top">
															<ml:CommonDataGrid id="m_dg" runat="server" OnItemDataBound="m_dg_ItemDataBound" onselectedindexchanged="m_dg_SelectedIndexChanged">
																<alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
																<itemstyle cssclass="GridItem"></itemstyle>
																<headerstyle cssclass="GridHeader"></headerstyle>
																<columns>
																	<asp:TemplateColumn ItemStyle-Width="20px">
																		<HeaderTemplate>
																			<asp:CheckBox Runat="server" ID="m_SelectAllBox" onclick="checkAllBoxes(this);"></asp:CheckBox>
																		</HeaderTemplate>
																		<itemtemplate>
																			<asp:CheckBox Runat="server" ID="m_CheckBox" onclick="onClickCheckRow(this)"></asp:CheckBox>
																			<ml:EncodedLabel ID="m_LID" Visible="false" Runat=server>
																			</ml:EncodedLabel>
																		</itemtemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn DataField="sLNm" ItemStyle-Width="250px" SortExpression="sLNm" HeaderText="Loan Number"></asp:BoundColumn>
																	<asp:BoundColumn DataField="aBLastNm" ItemStyle-Width="150px" SortExpression="aBLastNm" HeaderText="Last Name"></asp:BoundColumn>
																	<asp:BoundColumn DataField="aBFirstNm" ItemStyle-Width="150px" SortExpression="aBFirstNm" HeaderText="First Name"></asp:BoundColumn>
																	<asp:BoundColumn DataField="LoanStatus" ItemStyle-Width="200px" SortExpression="LoanStatus" HeaderText="Loan Status"></asp:BoundColumn>
																	<asp:BoundColumn DataField="sStatusD" ItemStyle-Width="200px" DataFormatString="{0:d}" SortExpression="sStatusD" HeaderText="Status Date"></asp:BoundColumn>
																</columns>
															</ml:CommonDataGrid>
														</div>
													</td>
												</tr>
												<tr>
													<td style="FONT-WEIGHT: bold; FONT-SIZE: 12px">
														<ml:EncodedLabel id="m_noneToShowLabel" ForeColor="green" Runat="server">There are no loans to display</ml:EncodedLabel>
													</td>
												</tr>
												<tr>
													<td align="right">
														<br>
														<input id="close" onclick="onClosePopup();" type="button" value="Close" name="close" Runat="server" style="width: 50px;">
													</td>
												</tr>
											</table>
										</td>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
	</body>
</HTML>
