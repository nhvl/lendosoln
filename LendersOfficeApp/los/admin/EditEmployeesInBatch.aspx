<%@ Page language="c#" Codebehind="EditEmployeesInBatch.aspx.cs" AutoEventWireup="false" Inherits="LendersOffice.Admin.EditEmployeesInBatch" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>
<%@ Register TagPrefix="uc" TagName="EmployeePermissionsList" Src="../../los/admin/EmployeePermissionsList.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
		<title>Edit Employees In Batch</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0" />
		<meta name="CODE_LANGUAGE" Content="C#" />
		<meta name=vs_defaultClientScript content="JavaScript" />
		<meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5" />
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet" />
		<style type="text/css">
		    .Action { color: Blue; text-decoration: underline; cursor: pointer;}

			.panel { PADDING-RIGHT: 8px; OVERFLOW-Y: hidden; DISPLAY: block; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; PADDING-TOP: 8px; HEIGHT: 420px }

			.willbeaffected { COLOR: tomato }

			.alreadyset { COLOR: lightgrey }

			.conflict { COLOR: red }

			.updated { COLOR: steelblue }

			.visible { DISPLAY: block }

			.hidden { DISPLAY: none }

            .section-header {
                PADDING-RIGHT: 4px; 
                PADDING-LEFT: 4px; 
                MARGIN-BOTTOM: 4px; 
                PADDING-BOTTOM: 4px; 
                PADDING-TOP: 4px; 
                BORDER-BOTTOM: lightgrey 2px solid;
            }
            
            .checkbox { width:5px;}
            .keepCurrent { width:80px; }
            .duAuthentication {width:60px; }
			</style>
</HEAD>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto; BACKGROUND-COLOR: #003366" onload="onInit();">
		<script language="javascript">
	
		function onInit()
		{
			<% if( IsPostBack == false ) { %>
			
			resize( 575 , 768 );
			
			<% } %>
			SetNextBtnStatus();
			
			var currentPanel = document.getElementById("m_CurrentPanel");
			var employees = document.getElementById("Employees");
			var settings = document.getElementById("Settings");
			var commit = document.getElementById("Commit");
			var errorMessage = document.getElementById("m_errorMessage");
			var feedBack = document.getElementById("m_feedBack");
			var commandToDo = document.getElementById("m_commandToDo");

			switch( currentPanel.value )
			{
				case "Employees":
					employees.className = "Visible";
					break;

				case "Settings":
					settings.className = "Visible";
					break;

				case "Commit":
					commit.className = "Visible";
					break;
					
				default:
					employees.className = "Visible";
					break;
			}

			if( errorMessage != null && errorMessage.value != "" )
				alert( errorMessage.value );
			else
			{
				if( feedBack != null && feedBack.value != "" )
					alert( feedBack.value );
				
				if( commandToDo != null )
				{
					if( commandToDo.value == "Close" )
						onClosePopup();
				}
			}
          
            document.getElementById("<%= AspxTools.ClientId(m_duLogin) %>").disabled = document.getElementById("<%=AspxTools.ClientId(m_keepCurrentDuLogin)%>").checked ? true : false;
            document.getElementById("<%= AspxTools.ClientId(m_duPw) %>").disabled = document.getElementById("<%=AspxTools.ClientId(m_keepCurrentDuPw)%>").checked ? true : false;
		}

		function showPanel( sPanel )
		{
		    var employees = document.getElementById("Employees");
		    var settings = document.getElementById("Settings");
		    var commit = document.getElementById("Commit");

			employees.className = "Hidden";
			settings.className  = "Hidden";
			commit.className    = "Hidden";

			if( sPanel != null )
			{			
				switch( sPanel )
				{
					case "Employees":
						employees.className = "Visible";
						break;

					case "Settings":
						settings.className = "Visible";
					    document.getElementById("<%= AspxTools.ClientId(m_duPw) %>").disabled = true; // make them retype the password between panel changes
					    document.getElementById("<%=AspxTools.ClientId(m_keepCurrentDuPw)%>").checked = true;
						break;

					case "Commit":
						commit.className = "Visible";
						break;
						
					default:
						employees.className = "Visible";
						break;
				}
			}
			
			document.getElementById("m_CurrentPanel").value = sPanel;

			if( sPanel == "Close" )
				onClosePopup();
		}
				
        jQuery(function($){
            var BrokerId = <%= AspxTools.JsString(BrokerUser.BrokerId) %>;
	        var $PrintGroupStorage = $('#' + <%=AspxTools.JsString(AssignedPrintGroupsJSON.ClientID) %>);
	        
	        var defaultSelections = $PrintGroupStorage.val();
	        if(defaultSelections == '') defaultSelections = '[]';
            var selectedIds = JSON.parse(defaultSelections);
	        
            $('#printSettings input').change(function(){
                if(!$(this).is(':checked')) return;
                
                var restricted = this.id.indexOf('m_restrictDocs') != -1;
                setDisabledAttr($('#SelectPrintGroups'), !restricted);                
            }).change();
            
            $('#SelectPrintGroups').click(function(){
                if (hasDisabledAttr(this)) {
                    $('.RestrictPrintingDocs input').attr('checked', 'checked').change();
                }
                var dialogArgs = {init: selectedIds};
                showModal("/los/Admin/PrintGroupPermAssignment.aspx?brokerId=" + BrokerId, dialogArgs, "Print Group Permissions", null, function(arg){ 
					if(arg.OK && arg.valuesJSON)
					{
						selectedIds = JSON.parse(arg.valuesJSON);
						$PrintGroupStorage.val(arg.valuesJSON);
					}
                },
                { hideCloseButton: true });
            });
        });
        
		function duLoginCheckBoxClick(duLoginCheckBox)
		{
		    document.getElementById("<%= AspxTools.ClientId(m_duLogin) %>").disabled = duLoginCheckBox.checked ? true : false;
		};

		function duPwCheckBoxClick(duPwCheckBox)
		{
            document.getElementById("<%= AspxTools.ClientId(m_duPw) %>").disabled = duPwCheckBox.checked ? true : false;
		};

		</script>
		<form id="EditEmployeesInBatch" method="post" runat="server">
			<div style="BORDER-RIGHT: 2px outset; BORDER-TOP: 2px outset; MARGIN: 8px; BORDER-LEFT: 2px outset; BORDER-BOTTOM: 2px outset; BACKGROUND-COLOR: gainsboro">
				<div style="PADDING-RIGHT: 1px; PADDING-LEFT: 1px; PADDING-BOTTOM: 1px; PADDING-TOP: 1px">
					<div class="FormTableHeader" style="PADDING-RIGHT: 4px;PADDING-LEFT: 4px;PADDING-BOTTOM: 4px;PADDING-TOP: 4px">
						Edit Employees
					</div>
					<input id="m_CurrentPanel" runat="server" type="hidden">
					<div id="Employees" class="Hidden">
						<div class="Panel" style="HEIGHT: 100%">
							<div class="FieldLabel section-header">
								Select individual employees
								<span style="FONT-WEIGHT: normal; COLOR: dimgray">
									(use header checkboxes to select entire branches)
								</span>
							</div>
							<div style="PADDING-RIGHT: 4px; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px; HEIGHT: 367px; BACKGROUND-COLOR: whitesmoke">
								<table cellspacing="2" cellpadding="2" border="0" width="100%">
								<tr class="FieldLabel" style="BACKGROUND-COLOR: wheat">
								<td>
									<input type="checkbox" kind="Broker" onclick="checkGroupBoxes( this , checked );">
								</td>
								<td>
									Name
								</td>
								<td align="center">
									Status
								</td>
								</tr>
								<asp:Repeater id="m_EmployeeList" runat="server" OnItemDataBound="BindEmployeeList">
									<SeparatorTemplate>
									<tr>
									<td colspan="4" height="8px">
									</td>
									</tr>
									</SeparatorTemplate>
									<ItemTemplate>
									<tr class="FieldLabel" style="BACKGROUND-COLOR: beige;">
									<td>
										<input type="checkbox" kind="Branch" onclick="checkGroupBoxes( this , checked );">
									</td>
									<td colspan="2" style="PADDING: 2px;">
                                        <ml:EncodedLiteral id="BranchNm" runat="server"></ml:EncodedLiteral>
									</td>
									</tr>
									<asp:Repeater id="List" runat="server" OnItemDataBound="List_ItemDataBound">
										<ItemTemplate>
										<tr>
										<td width="1px">
											<asp:CheckBox id="Selected" runat="server" onclick="SetNextBtnStatus();"></asp:CheckBox>
										</td>
										<td>
                                            <ml:EncodedLiteral id="Name" runat="server"></ml:EncodedLiteral>
										</td>
										<td align="center">
                                            <ml:EncodedLiteral id="EmployeeStatus" runat="server"></ml:EncodedLiteral>
										</td>
										</tr>
										</ItemTemplate>
									</asp:Repeater>
									</ItemTemplate>
								</asp:Repeater>
								</table>
								<script language="javascript">

									
									function checkGroupBoxes( oCheck , isSelected ) { try
									{
										if( oCheck == null || oCheck.getAttribute("kind") == null || isSelected == null )
											return;

										var i , n = 0 , r = oCheck;
										
										while( r.tagName != "TR" )
										{
											r = r.parentElement;
											if( r == null )
												return;
										}

										while( r != null )
										{
											//
											if( r.cells[ 0 ].children.length > 0 )
											{
												if( oCheck.getAttribute("kind") == "Broker" )
												{
													if( r.cells[ 0 ].children[ 0 ].getAttribute("kind") == null )
														r.cells[ 0 ].children[ 0 ].children[ 0 ].checked = isSelected;
													else
														r.cells[ 0 ].children[ 0 ].checked = isSelected;
												}
												else
												if( oCheck.getAttribute("kind") == "Branch" )
												{
													if( r.cells[ 0 ].children[ 0 ].getAttribute("kind") == null )
														r.cells[ 0 ].children[ 0 ].children[ 0 ].checked = isSelected;
													else
													if( n > 0 )
													{
														SetNextBtnStatus();
														return;
													}
												}
												++n;
											}
											r = r.nextElementSibling || r.nextSibling;
										}
										SetNextBtnStatus();
									}
									catch( e )
									{
									}}
									
									<%-- // 07/05/06 mf - OPM 2931. We disable the Next button when there
										 // are no employees or roles selected. --%>
									function SetNextBtnStatus() { try
									{
										var oPanel = $('Employees');
										var InputColl = oPanel.getElementsByTagName('INPUT');
										var bEmployeeSelected = false;
										for (var i = 0; i < InputColl.length; i++)
										{
											if ( InputColl[i].id && InputColl[i].id.indexOf('Selected') != -1 
											&& InputColl[i].checked && InputColl[i].checked == true )
											{
												bEmployeeSelected = true;
												break;
											}
										}
										$('<%= AspxTools.ClientId(m_Load) %>').disabled = ! bEmployeeSelected;
									}
									catch( e )
									{
									}}

									
									

								</script>
							</div>
							<div style="HEIGHT: 12px">
							</div>
							<div class="FieldLabel section-header">
								Select employees by role
								<span style="FONT-WEIGHT: normal; COLOR: dimgray">
									(specify roles to include and exclude with radio buttons)
								</span>
							</div>
							<div style="PADDING-RIGHT: 4px; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px; HEIGHT: 175px; BACKGROUND-COLOR: whitesmoke">
								<table cellspacing="2" cellpadding="2" border="0" width="100%">
								<tr class="FieldLabel" style="BACKGROUND-COLOR: wheat">
									<td>
									</td>
									<td>
										Role
									</td>
									<td align="center">
										Include | Exclude
									</td>
								</tr>
								<asp:Repeater id="m_RoleList" runat="server" OnItemDataBound="m_RoleList_ItemDataBound">
									<ItemTemplate>
									<tr>
										<td width="1px">
											<asp:CheckBox id="Selected" runat="server" onclick1="enableRole( this , checked);"></asp:CheckBox>
										</td>
										<td>
                                            <ml:EncodedLiteral id="ModifiableDesc" runat="server"></ml:EncodedLiteral>
										</td>
										<td align="center">
											<asp:Panel id="Panel" runat="server" style="DISPLAY: inline;" Enabled="False">
												<asp:RadioButton id="Include" runat="server" GroupName="Role">
												</asp:RadioButton>
												<asp:RadioButton id="Exclude" runat="server" GroupName="Role">
												</asp:RadioButton>
											</asp:Panel>
										</td>
									</tr>
									</ItemTemplate>
								</asp:Repeater>
								</table>
								<script type="text/javascript">
										function f_disableObject(oPanel, bDisabled) {
										  // 11/26/2008 dd - This function is need to disabled panel in .NET 3.5.
								        oPanel.disabled = bDisabled;
								        for (var i = 0; i < oPanel.children.length; i++)
								            f_disableObject(oPanel.children[i], bDisabled);
								    }
									function enableRole( oSelect , rowId ) { try
									{
										if( oSelect == null)
											return;

										var i , p = oSelect;
										if(oSelect.checked == false)
										{
										  document.getElementById(rowId + "_Include").checked = false;
										  document.getElementById(rowId + "_Exclude").checked = false;
										}
										else
										{
										  document.getElementById(rowId + "_Include").checked = true;
										}
										f_disableObject(document.getElementById(rowId + "_Panel"), !oSelect.checked);
										
										SetNextBtnStatus();
									}
									catch( e )
									{
									}}
									
									

								</script>
							</div>
						</div>
						<div style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; PADDING-TOP: 8px; TEXT-ALIGN: right">
							<input type="button" value="Cancel" style="WIDTH: 60px" onclick="showPanel( 'Close' );">
							<asp:Button id="m_Load" runat="server" Text="Next" Width="60px" OnClick="LoadClick">
							</asp:Button>
						</div>
					</div>
					<div id="Settings" class="Hidden">
						<div class="Panel" style="HEIGHT: auto">
							<div>
								<div class="FieldLabel section-header">
									Access level
								</div>
								<div style="MARGIN-BOTTOM: 8px; WIDTH: 100%">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
										<tr valign="top">
										<td>
											<asp:RadioButtonList id="m_AccessLevel" runat="server" CellPadding="0" CellSpacing="0" RepeatDirection="Vertical">
												<asp:ListItem Value="0" Selected="True">
													Current - Keep each user's currently assigned value
												</asp:ListItem>
												<asp:ListItem Value="1">
													Corporate - originated from any branch
												</asp:ListItem>
												<asp:ListItem Value="2">
													Branch - originated within own branch
												</asp:ListItem>
												<asp:ListItem Value="3">
													Individual - only if explicitly assigned
												</asp:ListItem>
											</asp:RadioButtonList>
										</td>
										</tr>
									</table>
								</div>
								<div class="FieldLabel section-header">Employee permissions</div>
								<div style="MARGIN-BOTTOM: 8px; WIDTH: 525px; BACKGROUND-COLOR: whitesmoke">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
									<tr valign="top">
									<td>
										<table cellspacing="2" cellpadding="2" border="0" width="100%">
											<tr class="FieldLabel" style="BACKGROUND-COLOR: lightblue">
												<td align="center">
													Current
												</td>
												<td align="center">
													On | Off
												</td>
												<td width="395px">
													Permission
												</td>
											</tr>
											<tr>
												<td colspan=3 >
													<div style="PADDING-RIGHT: 4px; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px; HEIGHT: 262px; BACKGROUND-COLOR: whitesmoke">				
														<uc:EmployeePermissionsList id=m_EmployeePermissionsList runat="server"></uc:EmployeePermissionsList>
													</div>
												</td>
											</tr>
										</table>
									</td>
									</tr>
									</table>
								</div>
								<div class="FieldLabel section-header">
									Loan event notification
								</div>
								<div style="MARGIN-BOTTOM: 8px; WIDTH: 100%">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
									<tr valign="top">
									<td>
										<asp:RadioButton id="m_KeepCurrentEvents" runat="server" Text="Current" GroupName="Notification" Checked="True">
										</asp:RadioButton>
										<asp:RadioButton id="m_SendOnEvents" runat="server" Text="Send email when loan events occur" GroupName="Notification">
										</asp:RadioButton>
										<asp:RadioButton id="m_DoNotSend" runat="server" Text="Do not send" GroupName="Notification">
										</asp:RadioButton>
									</td>
									</tr>
									</table>
								</div>
								<div class="FieldLabel section-header">
									Employment status
								</div>
								<div style="MARGIN-BOTTOM: 8px; WIDTH: 100%">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
									<tr valign="top">
									<td>
										<asp:RadioButton id="m_KeepCurrentStatus" runat="server" Text="Current" GroupName="Status" Checked="True">
										</asp:RadioButton>
										<asp:RadioButton id="m_CurrentEmployee" runat="server" Text="Active employee" GroupName="Status">
										</asp:RadioButton>
										<asp:RadioButton id="m_NotAnEmployee" runat="server" Text="Inactive employee" GroupName="Status">
										</asp:RadioButton>
									</td>
									</tr>
									</table>
								</div>
								<script type="text/javascript">
									function f_closeExpirationDetails() {
									    document.getElementById("ExpirationDetails").style.display = "none";
									}
									function f_showExpirationDetails() {
									    var msg = document.getElementById("ExpirationDetails");
										msg.style.display = "";
										msg.style.top = "267px";
										msg.style.left = "7px";
									}
								
								</script>
								<div id="ExpirationDetails" style="BORDER-RIGHT:black 2px solid; PADDING-RIGHT:0px; BORDER-TOP:black 2px solid; DISPLAY:none; PADDING-LEFT:0px; PADDING-BOTTOM:0px; BORDER-LEFT:black 2px solid; WIDTH:526px; PADDING-TOP:0px; BORDER-BOTTOM:black 2px solid; POSITION:absolute; BACKGROUND-COLOR:whitesmoke">
									<table bgcolor=black style="width:100%" cellspacing=1 cellpadding=2>
										<tr>
											<td bgcolor=gainsboro>&nbsp;</td>
											<td colspan=3 align=center bgcolor=gainsboro width="*"><span style="FONT-WEIGHT:bold">Rate lock request access<br>for <span style="FONT-STYLE:italic">possibly</span> expired rates</span></td>
										</tr>
										<tr>
											<td valign=top bgcolor=gainsboro style="width:260px"><span style="FONT-WEIGHT:bold">Scenario</span></td>
											<td align=center bgcolor=whitesmoke valign=top style="width:85px">"Never<br>allowed"</td>
											<td align=center bgcolor=whitesmoke style="width:85px">"Not allowed<br>after investor<br>cut-off time"</td>
											<td align=center bgcolor=whitesmoke valign=top style="width:85px">"Always<br>allowed"</td>
										</tr>
										<tr style="height:36px">
											<td bgcolor=whitesmoke valign=top>Pricing time is after investor cut-off</td>
											<td align=center bgcolor=ffcccc valign=top>blocked</td>
											<td align=center bgcolor=ffcccc valign=top>blocked</td>
											<td align=center bgcolor=ccffcc valign=top>allowed</td>
										</tr>
										<tr style="height:36px">
											<td bgcolor=whitesmoke nowrap valign=top>Pricing time is outside lender lock desk hours</td>
											<td align=center bgcolor=ffcccc valign=top>blocked</td>
											<td align=center bgcolor=ccffcc valign=top>allowed</td>
											<td align=center bgcolor=ccffcc valign=top>allowed</td>
										</tr>
										<tr>
											<td bgcolor=whitesmoke>New rate sheet is detected but updated<br><span style="width:10px">&nbsp;</span>pricing is not released to users yet</td>
											<td align=center bgcolor=ffcccc valign=top>blocked</td>
											<td align=center bgcolor=ccffcc valign=top>allowed</td>
											<td align=center bgcolor=ccffcc valign=top>allowed</td>
										</tr>
										<tr>
											<td bgcolor=whitesmoke>Technical difficulty is encountered when<br><span style="width:10px">&nbsp;</span>attempting to access or process rate sheet</td>
											<td align=center bgcolor=ffcccc valign=top>blocked</td>
											<td align=center bgcolor=ccffcc valign=top>allowed</td>
											<td align=center bgcolor=ccffcc valign=top>allowed</td>
										</tr>
										<tr>
											<td align=center bgcolor=whitesmoke colspan=4><br>[ <a href="#" onclick="f_closeExpirationDetails();">Close</a> ]</td>
										</tr>
									</table>
								</div>
								<% if ( IsPmlEnabled ) { %>
								<div class="FieldLabel section-header">
									Rate lock requests for <span style="FONT-STYLE:italic">possibly</span> expired rates <a href="#" onclick="f_showExpirationDetails();">?</a>
								</div>
								<div style="MARGIN-BOTTOM: 8px; WIDTH: 100%">
									<table cellspacing="0" cellpadding="0" border="0" width="100%">
									<tr valign="top">
									<td>
										<asp:RadioButton id="m_currentBypassSetting" runat="server" Text="Current" GroupName="RatesheetExpiration" Checked="True">
										</asp:RadioButton>
										<asp:RadioButton id="m_noBypass" runat="server" Text="Never allowed" GroupName="RatesheetExpiration">
										</asp:RadioButton>
										<asp:RadioButton id="m_bypassExcInvCutoff" runat="server" Text="Not allowed after investor cutoff time" GroupName="RatesheetExpiration">
										</asp:RadioButton>
										<asp:RadioButton id="m_bypassAll" runat="server" Text="Always allowed" GroupName="RatesheetExpiration">
										</asp:RadioButton>
									</td>
									</tr>
									</table>
								</div>
								<% } %>
	                            <div class="FieldLabel section-header">
									Document Printing
								</div>
								<div style="MARGIN-BOTTOM: 8px; WIDTH: 100%" id="printSettings">
	                                <asp:RadioButton ID="m_KeepCurrentPrintPermissions" runat="server" Text="Current" GroupName="PrintGroupSettings" Checked="true" />
	                                <asp:RadioButton ID="m_allowPrintingAllDocs" runat="server" Text="Allow printing all standard documents" GroupName="PrintGroupSettings" />
	                                <asp:RadioButton ID="m_restrictDocs" runat="server" Text="Restrict printing to approved print groups only" GroupName="PrintGroupSettings" class="RestrictPrintingDocs" />
	                                <a class="Action" id="SelectPrintGroups">edit</a>
	                                <asp:HiddenField ID="AssignedPrintGroupsJSON" runat="server" />
								</div>
								<div id="AuthenticatonContainer" runat="server">
								    <div class="FieldLabel section-header">
									    Authentication
								    </div>
								    <div style="MARGIN-BOTTOM: 8px; WIDTH: 100%" id="Div1">
								        <table cellspacing="0" cellpadding="0" border="0" style="width:525px; background-color:whitesmoke;">
									    <tr valign="top">
									    <td>
										    <table cellspacing="2" cellpadding="2" border="0" width="100%">
											    <tr class="FieldLabel" style="BACKGROUND-COLOR: lightblue">
												    <td align="center">
													    Current
												    </td>
												    <td align="center">
													    On | Off
												    </td>
												    <td width="395px">
													    Permission
												    </td>
											    </tr>
											    <tr runat="server" id="EnableMfaRow">
												    <td style="padding-left:15px;">
	                                                    <asp:RadioButton ID="CurrentMFA" runat="server" GroupName="MFASettings" Checked="true" />
												    </td>
												    <td>
	                                                    <asp:RadioButton ID="EnableMFA" runat="server" GroupName="MFASettings" />
	                                                    <asp:RadioButton ID="DisableMFA" runat="server" GroupName="MFASettings" class="RestrictPrintingDocs" />
												    </td>
												    <td>
												        Enable multi-factor auth for this user
												    </td>
											    </tr>
											    <tr runat="server" id="EnableCertRow">
												    <td style="padding-left:15px;">
	                                                    <asp:RadioButton ID="CurrentAllowCert" runat="server" GroupName="CertSettings" Checked="true" />
												    </td>
												    <td>
	                                                    <asp:RadioButton ID="AllowCert" runat="server" GroupName="CertSettings" />
	                                                    <asp:RadioButton ID="DisableCert" runat="server" GroupName="CertSettings" class="RestrictPrintingDocs" />
												    </td>
												    <td>
												        Allow user to install digital certificate
												    </td>
											    </tr>
											    <tr runat="server" id="EnableSmsAuthRow">
												    <td style="padding-left:15px;">
	                                                    <asp:RadioButton ID="CurrentSmsAuth" runat="server" GroupName="EnableSmsSettings" Checked="true" />
												    </td>
												    <td>
	                                                    <asp:RadioButton ID="EnableSmsAuth" runat="server" GroupName="EnableSmsSettings" />
	                                                    <asp:RadioButton ID="DisableEnableSms" runat="server" GroupName="EnableSmsSettings" class="RestrictPrintingDocs" />
												    </td>
												    <td>
												        Enable auth code transmission via SMS text
												    </td>
											    </tr>
											    
										    </table>
									    </td>
									    </tr>
									    </table>
	                                    
	                                </div>
	                            </div>
	                            <div>
	                                <div class="ip-header section-header FieldLabel">IP Address Restriction</div>
	                                <asp:RadioButton ID="CurrentAllowAnyIPAccess" runat="server" GroupName="AllowAnyIPAccessSetting" Checked="true" />
	                                <label>Current</label>
	                                <asp:RadioButton ID="DisableIpRestriction" runat="server" GroupName="AllowAnyIPAccessSetting" />
	                                <label>No IP address restriction</label>
	                                <asp:RadioButton ID="EnableIpRestriction" runat="server" GroupName="AllowAnyIPAccessSetting" class="RestrictPrintingDocs" />
	                                <label>Only allow whitelisted IPs or devices with a certificate</label>
	                            </div>
	                            <div>
	                                <table>
	                                    <tr></tr>
	                                    <tr><td colspan="3"><div class="ip-header section-header FieldLabel">DU Credentials</div></td></tr>
	                                    <tr>
	                                        <td class="checkbox"><asp:CheckBox id="m_keepCurrentDuLogin" runat="server" Checked="True" onclick="duLoginCheckBoxClick(this)"/></td>
	                                        <td class="keepCurrent">Keep Current</td>
	                                        <td class="duAuthentication">DU Login: </td>
	                                        <td><asp:textbox id="m_duLogin" runat="server" MaxLength="50"/></td>
	                                    </tr>
					                    <tr>
	                                        <td class="checkbox"><asp:CheckBox id="m_keepCurrentDuPw" runat="server" Checked="True" onclick="duPwCheckBoxClick(this)"/></td>
	                                        <td class="keepCurrent">Keep Current</td>
	                                        <td class="duAuthentication">Password: </td>
	                                        <td><asp:textbox id="m_duPw" runat="server" MaxLength="20" TextMode="Password"/></td>
					                    </tr>
	                                </table>
	                            </div>
								<%  if (!IsPmlEnabled) { %>
									<div style="Padding-Bottom:56px">
									</div>
								<% } %>
							</div>
						</div>
						<div style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; PADDING-TOP: 8px; TEXT-ALIGN: right">
							<input type="button" value="Cancel" style="WIDTH: 60px" onclick="showPanel( 'Close' );">
							<input type="button" value="Back" style="WIDTH: 60px" onclick="showPanel( 'Employees' );">
							<asp:Button id="m_Next" runat="server" Text="Next" Width="60px" OnClick="NextClick">
							</asp:Button>
						</div>
					</div>
					<div id="Commit" class="Hidden">
						<div class="Panel" style="HEIGHT:100%">
							<div class="FieldLabel section-header">
								Apply updates to selected employees
								<span style="FONT-WEIGHT: normal; COLOR: dimgray">
									(remove entries by unchecking the row)
								</span>
							</div>
							<div style="PADDING-RIGHT: 4px; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px; HEIGHT: 584px; BACKGROUND-COLOR: whitesmoke">
								<table cellspacing="2" cellpadding="2" border="0" width="100%">
								<tr class="FieldLabel" style="BACKGROUND-COLOR: lightsteelblue">
								<td>
								</td>
								<td>
									Name
								</td>
								<td>
									Branch
								</td>
								<td>
									Notes
								</td>
								<td align="center">
									Status
								</td>
								</tr>
								<asp:Repeater id="m_Working" runat="server" OnItemDataBound="m_Working_ItemDataBound">
									<ItemTemplate>
									<tr valign="top">
									<td width="1px">
										<asp:CheckBox id="Selected" runat="server" Checked="True"></asp:CheckBox>
									</td>
									<td>
                                        <ml:EncodedLiteral id="Name" runat="server"></ml:EncodedLiteral>
									</td>
									<td>
                                        <ml:EncodedLiteral id="Branch" runat="server"></ml:EncodedLiteral>
									</td>
									<td>
										<ml:PassthroughLabel id="Notes" runat="server">
										</ml:PassthroughLabel>
									</td>
									<td align="center">
                                        <ml:EncodedLiteral ID="EmployeeStatus" runat="server"></ml:EncodedLiteral>
									</td>
									</tr>
									</ItemTemplate>
								</asp:Repeater>
								</table>
								<asp:Panel id="m_Empty" runat="server" style="PADDING-RIGHT: 64px; PADDING-LEFT: 64px; PADDING-BOTTOM: 64px; COLOR: dimgray; PADDING-TOP: 64px; TEXT-ALIGN: center">
									Nothing loaded.  Choose individual employees or roles to select for update.
								</asp:Panel>
							</div>
						</div>
						<div style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; PADDING-TOP: 8px; TEXT-ALIGN: right">
							<input type="button" value="Cancel" style="WIDTH: 60px" onclick="showPanel( 'Close' );">
							<input type="button" value="Back" style="WIDTH: 60px" onclick="showPanel( 'Settings' );">
							<asp:Button id="m_Preview" runat="server" Text="Preview" Width="60px" OnClick="CheckClick">
							</asp:Button>
							<span>
								<input type="submit" value="Apply" style="WIDTH: 60px" onclick="disabled = true; parentElement.children[ 1 ].click();">
								<asp:Button id="m_Apply" runat="server" style="DISPLAY: none" Text="Apply" Width="60px" OnClick="ApplyClick">
								</asp:Button>
							</span>
						</div>
					</div>
				</div>
			</div>
			<uc:CModalDlg runat="server" id="Modal">
			</uc:CModalDlg>
		</form>
	</body>
</HTML>
