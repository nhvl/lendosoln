﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewOriginatorCompensationAuditEventCtrl.ascx.cs" Inherits="LendersOfficeApp.los.admin.ViewOriginatorCompensationAuditEventCtrl" %>
<style type="text/css">
	#auditTable td {font-size:small}
</style>
<h4 class="page-header">Audit history</h4>
<table id="auditTable" width="100%">
    <tr>
        <td>
            <b>Date: </b><ml:EncodedLiteral ID="Date" runat="server"></ml:EncodedLiteral>
        </td>
    </tr>
    <tr>
        <td>
            <b>By: </b><ml:EncodedLiteral ID="By" runat="server"></ml:EncodedLiteral>
        </td>
    </tr>
    <tr id="trOnlyFor1stLienOfCombo" runat="server">
        <td>
            <ml:EncodedLiteral ID="OnlyFor1stLienOfCombo" runat="server" ></ml:EncodedLiteral>
        </td>
    </tr>

    <tr id="trRate" runat="server">
        <td>
            <b>Rate: </b><ml:EncodedLiteral ID="Rate" runat="server"></ml:EncodedLiteral>
        </td>
    </tr>
    <tr id="trMin" runat="server">
        <td>
            <b>Minimum: </b><ml:EncodedLiteral ID="Minimum" runat="server"></ml:EncodedLiteral>
        </td>
    </tr>
    <tr id="trMax" runat="server">
        <td>
            <b>Maximum: </b><ml:EncodedLiteral ID="Maximum" runat="server"></ml:EncodedLiteral>
        </td>
    </tr>
    <tr id="trFixed" runat="server">
        <td>
            <b>Fixed Amount: </b><ml:EncodedLiteral ID="FixedAmount" runat="server"></ml:EncodedLiteral>
        </td>
    </tr>
    <tr>
        <td>
            <b>Notes: </b><ml:EncodedLiteral ID="Notes" runat="server"></ml:EncodedLiteral>
        </td>
    </tr>
</table>
