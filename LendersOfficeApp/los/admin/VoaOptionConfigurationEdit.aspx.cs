﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Allows the user to edit the VOA Option configuration.
    /// </summary>
    public partial class VoaOptionConfigurationEdit : BaseServicePage
    {
        /// <summary>
        /// The vendor providing the option set.
        /// </summary>
        private VOXVendor vendor;

        /// <summary>
        /// The lender service providing the current configuration, if applicable.
        /// </summary>
        private VOXLenderService lenderService;

        /// <summary>
        /// Gets the current configuration.
        /// </summary>
        private VOAOptionConfiguration configuration;

        /// <summary>
        /// Gets the permission required to load this page.
        /// </summary>
        /// <value>The permission required for page load.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Gets a value indicating whether the broker is configured to use VOX.
        /// </summary>
        /// <value>Whether the broker is configured to use VOX.</value>
        protected bool IsEnableVOXConfiguration
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerDB.IsEnableVOXConfiguration;
            }
        }

        /// <summary>
        /// Gets the broker ID of the current user.
        /// </summary>
        protected Guid BrokerId
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerId;
            }
        }

        /// <summary>
        /// Gets the vendor ID.
        /// </summary>
        protected int VendorId
        {
            get
            {
                return RequestHelper.GetInt("vendorId", 0);
            }
        }

        /// <summary>
        /// Gets the vendor providing the option set.
        /// </summary>
        protected VOXVendor Vendor
        {
            get
            {
                if (this.vendor == null)
                {
                    this.vendor = VOXVendor.LoadVendor(this.VendorId);
                }

                return this.vendor;
            }
        }

        /// <summary>
        /// Gets the lender service ID.
        /// </summary>
        protected int LenderServiceId
        {
            get
            {
                return RequestHelper.GetInt("lenderServiceId", 0);
            }
        }

        /// <summary>
        /// Gets the associated lender service.
        /// </summary>
        protected VOXLenderService LenderService
        {
            get
            {
                if (this.lenderService == null)
                {
                    var brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
                    this.lenderService = VOXLenderService.GetLenderServiceById(brokerId, this.LenderServiceId);
                }

                return this.lenderService;
            }
        }

        /// <summary>
        /// Gets the current VOA configuration.
        /// </summary>
        protected VOAOptionConfiguration Configuration
        {
            get
            {
                if (this.configuration == null)
                {
                    List<VOAOption> overrideOptions = null;

                    // Apply unsaved changes if any exist.
                    if (!string.IsNullOrEmpty(this.VoaOptionJsonOverride))
                    {
                        overrideOptions = SerializationHelper.JsonNetDeserialize<List<VOAOption>>(this.VoaOptionJsonOverride);
                        this.configuration = VOAOptionConfiguration.Create(this.BrokerId, this.LenderServiceId, this.VendorId, overrideOptions);
                    }
                    else
                    {
                        this.configuration = VOAOptionConfiguration.Load(this.BrokerId, this.LenderServiceId, this.VendorId);
                    }
                }

                return this.configuration;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the lender service is enabled for B-users.
        /// </summary>
        protected bool ServiceEnabledForLqbUsers
        {
            get
            {
                return RequestHelper.GetBool("enabledLqb");
            }
        }

        /// <summary>
        /// Gets a value indicating whether the lender service is enabled for P-users.
        /// </summary>
        protected bool ServiceEnabledForPmlUsers
        {
            get
            {
                return RequestHelper.GetBool("enabledPml");
            }
        }

        /// <summary>
        /// Gets the temporary VOA Option JSON.
        /// </summary>
        /// <remarks>This will be provided by the parent page if option configuration changes have been made but not yet saved.</remarks>
        protected string VoaOptionJsonOverride
        {
            get
            {
                return RequestHelper.GetQueryString("VoaOptionJsonOverride");
            }
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sending DOM object.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var vendorService = (VOAVendorService)this.Vendor.Services[VOXServiceT.VOA_VOD];

            this.AccountHistoryOptionRepeater.DataSource = vendorService.AccountHistoryOptions;
            this.AccountHistoryOptionRepeater.DataBind();

            this.RefreshPeriodOptionRepeater.DataSource = vendorService.RefreshPeriodOptions;
            this.RefreshPeriodOptionRepeater.DataBind();
        }

        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">The sending DOM element.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            if (!this.IsEnableVOXConfiguration || (!this.Vendor?.Services?.ContainsKey(VOXServiceT.VOA_VOD) ?? false))
            {
                return;
            }

            this.RegisterJsGlobalVariables("ServiceEnabledForLqbUsers", this.ServiceEnabledForLqbUsers);
            this.RegisterJsGlobalVariables("ServiceEnabledForPmlUsers", this.ServiceEnabledForPmlUsers);
        }

        /// <summary>
        /// Binds an account history option available from the vendor.
        /// </summary>
        /// <param name="sender">The sending DOM object.</param>
        /// <param name="args">Repeater event arguments.</param>
        protected void AccountHistoryOptionRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            this.BindOptionRow(args, VOAOptionType.AccountHistory);
        }

        /// <summary>
        /// Binds a refresh period option available from the vendor.
        /// </summary>
        /// <param name="sender">The sending DOM object.</param>
        /// <param name="args">Repeater event arguments.</param>
        protected void RefreshPeriodOptionRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            this.BindOptionRow(args, VOAOptionType.RefreshPeriod);
        }

        /// <summary>
        /// Binds a VOA option row to a repeater.
        /// </summary>
        /// <param name="args">Repeater event arguments.</param>
        /// <param name="type">The type of option being bound.</param>
        private void BindOptionRow(RepeaterItemEventArgs args, VOAOptionType type)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var boundValue = (int)args.Item.DataItem;

            var optionName = (EncodedLiteral)args.Item.FindControl("OptionName");
            optionName.Text = GetOptionDescription(boundValue);

            var optionValue = (HtmlInputHidden)args.Item.FindControl("OptionValue");
            optionValue.Value = boundValue.ToString();

            VOAOption configuredForLqb = null;
            if (type == VOAOptionType.AccountHistory)
            {
                configuredForLqb = this.Configuration.AccountHistoryOptions_LqbUsers.FirstOrDefault(o => o.OptionValue == boundValue);
            }
            else
            {
                configuredForLqb = this.Configuration.RefreshPeriodOptions_LqbUsers.FirstOrDefault(o => o.OptionValue == boundValue);
            }

            var enableForLqbUsers = (HtmlInputCheckBox)args.Item.FindControl("EnableForLqbUsers");
            enableForLqbUsers.Checked = configuredForLqb != null;

            // Note that ASP:Repeaters mangle radio button group names, so we have to inject the buttons via passthrough literals.
            var lqbDefaultRadioButton = new HtmlInputRadioButton();
            lqbDefaultRadioButton.ID = $"{type.ToString()}{boundValue}DefaultForLqbUsers";
            var lqbDefaultGroupName = $"{type.ToString()}DefaultForLqbUsers";
            lqbDefaultRadioButton.Name = lqbDefaultGroupName;
            lqbDefaultRadioButton.Attributes["class"] = "DefaultForLqbUsers LqbControl";

            var defaultForLqbUsers = (PassthroughLiteral)args.Item.FindControl("DefaultForLqbUsersLiteral");
            defaultForLqbUsers.Text = AspxTools.HtmlControl(lqbDefaultRadioButton);

            var isLqbDefaultSelected = configuredForLqb?.IsDefault ?? false;
            if (isLqbDefaultSelected)
            {
                this.RegisterJsGlobalVariables($"{lqbDefaultGroupName}SelectedId", lqbDefaultRadioButton.ID);
            }

            VOAOption configuredForPml = null;
            if (type == VOAOptionType.AccountHistory)
            {
                configuredForPml = this.Configuration.AccountHistoryOptions_PmlUsers.FirstOrDefault(o => o.OptionValue == boundValue);
            }
            else
            {
                configuredForPml = this.Configuration.RefreshPeriodOptions_PmlUsers.FirstOrDefault(o => o.OptionValue == boundValue);
            }

            var enableForPmlUsers = (HtmlInputCheckBox)args.Item.FindControl("EnableForPmlUsers");
            enableForPmlUsers.Checked = configuredForPml != null;

            var pmlDefaultRadioButton = new HtmlInputRadioButton();
            pmlDefaultRadioButton.ID = $"{type.ToString()}{boundValue}DefaultForPmlUsers";
            var pmlDefaultGroupName = $"{type.ToString()}DefaultForPmlUsers";
            pmlDefaultRadioButton.Name = pmlDefaultGroupName;
            pmlDefaultRadioButton.Attributes["class"] = "DefaultForPmlUsers PmlControl";

            var defaultForPmlUsers = (PassthroughLiteral)args.Item.FindControl("DefaultForPmlUsersLiteral");
            defaultForPmlUsers.Text = AspxTools.HtmlControl(pmlDefaultRadioButton);

            var isPmlDefaultSelected = configuredForPml?.IsDefault ?? false;
            if (isPmlDefaultSelected)
            {
                this.RegisterJsGlobalVariables($"{pmlDefaultGroupName}SelectedId", pmlDefaultRadioButton.ID);
            }
        }

        /// <summary>
        /// Gets the text description for a given option value.
        /// </summary>
        /// <param name="optionValue">The value to convert.</param>
        /// <returns>The string description.</returns>
        private string GetOptionDescription(int optionValue)
        {
            if (optionValue == VOAVendorService.MinOption)
            {
                return "None";
            }
            else if (optionValue == VOAVendorService.MaxOption)
            {
                return "Max Available";
            }
            else
            {
                return $"{optionValue} days";
            }
        }
    }
}