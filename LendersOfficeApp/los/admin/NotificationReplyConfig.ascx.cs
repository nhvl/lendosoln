namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.Data;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    /// <summary>
    ///	Summary description for NotificationReplyConfig.
    /// </summary>

    public partial  class NotificationReplyConfig : System.Web.UI.UserControl , INamingContainer
	{
 // 10/27/05 - added by Dominic to prevent more false alarm/email format errors

		private NotificationRules m_Rules = new NotificationRules();
		private ArrayList       m_Choices = new ArrayList();
		protected ArrayList       m_Listing = new ArrayList();

		public String EventLabel
		{
			// Access member.

			get
			{
				if( ViewState[ "EventLabel" ] != null )
				{
					return ViewState[ "EventLabel" ].ToString();
				}

				return "";
			}
			set
			{
				ViewState.Add( "EventLabel" , value );
			}
		}

		public String Mode
		{
			// Access member.

			get
			{
				if( ViewState[ "Mode" ] != null )
				{
					return ViewState[ "Mode" ].ToString().ToLower();
				}

				return "";
			}
			set
			{
				ViewState.Add( "Mode" , value );
			}
		}

		public NotificationRules DataSource
		{
			// Access member.

			set
			{
				m_Rules = value;
			}
		}

		/// <summary>
		/// Render this control.
		/// </summary>

		protected void ControlPreRender( object sender , System.EventArgs a )
		{
			// Draw controls according to selection settings.

			string initScript = String.Format(@"
				<script language='javascript'>
				<!--
					onInitNotifReplyConfig('{0}')
				// -->
				</script>
			", this.ClientID);

			Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "_Init", initScript);
			
			ViewState.Add( "Listing" , m_Listing );

			m_AddrList.DataSource = m_Listing;
			m_AddrList.DataBind();

			if( m_Listing.Count == 0)
				Page.ClientScript.RegisterHiddenField( this.ClientID + "_m_AddrNoneVisible" , "1");
			else
				Page.ClientScript.RegisterHiddenField( this.ClientID + "_m_AddrNoneVisible" , "0");
			
			ViewState.Add( "Choices" , m_Choices );

			m_RoleList.DataSource = m_Choices.Cast<ReplyToRoleDesc>().OrderBy(roleDesc => roleDesc.RoleModifiableDesc);
			m_RoleList.DataBind();
			if( Mode == "lite" )
			{
				m_ReplyPanel.Visible = false;
			}
			else
			{
				m_ReplyPanel.Visible = true;
			}
		}

		/// <summary>
		/// Initialize this control.
		/// </summary>

		protected void ControlLoad( object sender , System.EventArgs a )
		{
			// Initialize this control.

			// 10/27/05 - added by Dominic to prevent more false alarm/email format errors
            RegularExpressionValidator1.ValidationExpression = ConstApp.EmailValidationExpression;
			RegularExpressionValidator1.Text = ErrorMessages.InvalidEmailAddress;

			if( ViewState[ "Listing" ] != null )
			{
				m_Listing = ViewState[ "Listing" ] as ArrayList;
			}

			if( ViewState[ "Choices" ] != null )
			{
				m_Choices = ViewState[ "Choices" ] as ArrayList;
			}
		}

		/// <summary>
		/// Bind the source to this web control.
		/// </summary>

		public override void DataBind()
		{
			// Initialize the role list, even if it's not visible.
			//
			// Lookup the bound event's rule, and order the role
			// list according to the order found in the set.

			ListDictionary roleSet = new ListDictionary();
			ArrayList      listSet = new ArrayList();
			ArrayList      restSet = new ArrayList();

			// 3/17/2006 mf - OPM 4373 Admins cannot be assigned to loans, so
			// now we do not display that role in the lists.  For users who have
			// Admin in their reply-to list, they will be removed from the db upon
			// the next save.
			// 12/26/2006 db - OPM 8964 Also remove Accountant role
			foreach( Role role in Role.LendingQBRoles )
			{
				if ((role.RoleT != E_RoleT.Administrator) && (role.RoleT != E_RoleT.Accountant))		
				{
					ReplyToRoleDesc rD = new ReplyToRoleDesc();

					rD.RoleModifiableDesc = role.ModifiableDesc;
					rD.RoleDesc           = role.Desc;
					roleSet.Add( rD.RoleDesc , rD );
				}
			}

			if( m_Rules != null )
			{
				NotificationRule nCurr = new NotificationRule();

				foreach( NotificationRule nRule in m_Rules )
				{
					if( nRule.EventLabel == EventLabel )
					{
						nCurr = nRule;

						break;
					}
				}

				foreach( String rDesc in nCurr.ReplyToList )
				{
					// 3/17/2006 mf - OPM 4373 Admins cannot be assigned to loans
					// 12/26/2006 db - OPM 8964 Accountants cannot be assigned to loans
					if ((rDesc != "Administrator" ) && (rDesc != "Accountant")) 
					{
						listSet.Add( roleSet[ rDesc ] );

						roleSet.Remove( rDesc );
					}
				}

				m_CarbonCopyTo.Text = nCurr.CarbonCopyTo;
				m_DefaultReply.Text = nCurr.DefaultReply;
				m_DefaultLabel.Text = nCurr.DefaultLabel;

				m_AllowReply.Checked = !nCurr.DoNotReply;
			}

			if( m_CarbonCopyTo.Text.TrimWhitespaceAndBOM() == "" )
			{
				m_UseCopying.Checked = false;
			}
			else
			{
				m_UseCopying.Checked = true;
			}

			foreach( Object rDesc in roleSet.Values )
			{
				restSet.Add( rDesc );
			}

			m_Listing = listSet;
			m_Choices = restSet;

            if (EventLabel == "Admin")
            {
                EvenTypeLiteral.Text = "Administrative";
            }
            else
            {
                EvenTypeLiteral.Text = "Loan";
            }
		}

		/// <summary>
		/// Save this web control to the data.
		/// </summary>

		public void DataSave()
		{
			// Use the bound collection and overwrite the
			// matching rule configuration.

			if( m_Rules != null )
			{
				NotificationRule nRule = new NotificationRule();

				if( m_UseCopying.Checked == true )
				{
					nRule.CarbonCopyTo = m_CarbonCopyTo.Text;
				}

				if( m_AllowReply.Checked == true )
				{
					foreach( ReplyToRoleDesc rD in m_Listing )
					{
						nRule.ReplyToList.Add( rD.RoleDesc );
					}

					nRule.DefaultReply = m_DefaultReply.Text;
					nRule.DefaultLabel = m_DefaultLabel.Text;
				}

				nRule.DoNotReply = !m_AllowReply.Checked;

				nRule.EventLabel = EventLabel;

				m_Rules.Add( nRule );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Page.PreRender += new System.EventHandler( this.ControlPreRender );
			Page.Load += new System.EventHandler( this.ControlLoad );
		}
		#endregion

		/// <summary>
		/// Handle list bound.
		/// </summary>

		protected void AddrListBound( object sender , System.Web.UI.WebControls.RepeaterItemEventArgs a )
		{
			// Handle item binding.

			try
			{

                if (a.Item.ItemType != ListItemType.Item && a.Item.ItemType != ListItemType.AlternatingItem)
                {
                    return;
                }

				// Place a position specific label into each row so
				// we can quickly identify and ordering.

				Label lText = a.Item.FindControl( "Order" ) as Label;

				if( lText != null )
				{
					if( a.Item.ItemIndex == 0 )
					{
						lText.Text = "first";
					}
					else
					if( a.Item.ItemIndex == 1 )
					{
						lText.Text = "second";
					}
					else
					if( a.Item.ItemIndex == 2 )
					{
						lText.Text = "third";
					}
					else
					{
						lText.Text = "next";
					}
				}

                ReplyToRoleDesc rd = a.Item.DataItem as ReplyToRoleDesc;

                EncodedLiteral roleModifiableDesc = a.Item.FindControl("RoleModifiableDesc") as EncodedLiteral;
                roleModifiableDesc.Text = rd.RoleModifiableDesc;

                LinkButton linkbutton3 = a.Item.FindControl("Linkbutton3") as LinkButton;
                linkbutton3.CommandArgument = rd.ToString();
            }
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( e );
			}
		}

        protected void RoleListBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs a)
        {
            // Handle item binding.
            if (a.Item.ItemType != ListItemType.Item && a.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            ReplyToRoleDesc rd = a.Item.DataItem as ReplyToRoleDesc;

            EncodedLiteral roleModifiableDesc = a.Item.FindControl("RoleModifiableDesc") as EncodedLiteral;
            roleModifiableDesc.Text = rd.RoleModifiableDesc;

            LinkButton linkbutton4 = a.Item.FindControl("Linkbutton4") as LinkButton;
            linkbutton4.CommandArgument = rd.ToString();
        }

        /// <summary>
        /// Handle list click.
        /// </summary>

        protected void AddrListClick( object sender , System.Web.UI.WebControls.RepeaterCommandEventArgs a )
		{
			// Handle list command.

			try
			{
				// Move the role list item around according to the
				// command given.

				if( m_AllowReply.Checked == false )
				{
					return;
				}

				if( a.CommandName.ToLower() == "up" && a.Item.ItemIndex > 0 )
				{
					m_Listing.Reverse( a.Item.ItemIndex - 1 , 2 );
				}
				else
				if( a.CommandName.ToLower() == "dn" && a.Item.ItemIndex < m_Listing.Count - 1 )
				{
					m_Listing.Reverse( a.Item.ItemIndex , 2 );
				}

				if( a.CommandName.ToLower() == "drop" )
				{
					foreach( ReplyToRoleDesc rD in m_Listing )
					{
						if( rD.RoleDesc == a.CommandArgument.ToString() )
						{
							m_Choices.Insert( 0 , rD );
							m_Listing.Remove( rD );

							break;
						}
					}
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( e );
			}
		}

		/// <summary>
		/// Handle list click.
		/// </summary>

		protected void RoleListClick( object sender , System.Web.UI.WebControls.RepeaterCommandEventArgs a )
		{
			// Handle list command.

			try
			{
				// Move the role list item around according to the
				// command given.

				if( m_AllowReply.Checked == false )
				{
					return;
				}

				if( a.CommandName.ToLower() == "include" )
				{
					foreach( ReplyToRoleDesc rD in m_Choices )
					{
						if( rD.RoleDesc == a.CommandArgument.ToString() )
						{
							m_Listing.Add( rD);     // 3/17/2006 mf - OPM 4373 add to end instead of insert at beginning
							m_Choices.Remove( rD );

							break;
						}
					}
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( e );
			}
		}

	}

	/// <summary>
	/// Keep track of the current state of the role list.
	/// </summary>

	[ Serializable
	]
	public class ReplyToRoleDesc
	{
		/// <summary>
		/// Keep track of the current state of the role
		/// list.
		/// </summary>

		private String m_RoleModifiableDesc = String.Empty;
		private String           m_RoleDesc = String.Empty;

		#region ( Desc properties )

		public String RoleModifiableDesc
		{
			// Access member.

			set
			{
				m_RoleModifiableDesc = value;
			}
			get
			{
				return m_RoleModifiableDesc;
			}
		}

		public String RoleDesc
		{
			// Access member.

			set
			{
				m_RoleDesc = value;
			}
			get
			{
				return m_RoleDesc;
			}
		}

		#endregion

		/// <summary>
		/// Return id as default representation.
		/// </summary>

		public override String ToString()
		{
			// Return id.

			return m_RoleDesc.ToString();
		}
	}
}
