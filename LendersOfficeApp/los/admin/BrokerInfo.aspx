<%@ Register TagPrefix="uc1" TagName="NamingSchemeUserControl" Src="NamingSchemeUserControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PasswordOptionsUserControl" Src="PasswordOptionsUserControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BrokerECOAUserControl" Src="BrokerECOAUserControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BrokerLockPoliciesUserControl" Src="LockPoliciesUserControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BrokerInformationUserControl" Src="BrokerInformationUserControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BrokerOptionUserControl" Src="BrokerOptionUserControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BrokerCRASettings" Src="BrokerCRASettings.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LendingLicenses" Src="../../los/BrokerAdmin/LendingLicensesPanel.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BrokerGlobalWhiteListUserControl" Src="~/los/admin/BrokerGlobalWhiteListUserControl.ascx" %>
<%@ Register TagPrefix="uc1" TagName="LenderServices" Src="~/los/admin/LenderServices.ascx" %>
<%@ Page language="c#" Codebehind="BrokerInfo.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.BrokerInfo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>Broker Information</title>
    <link href="../../css/stylesheet.css" type=text/css rel=stylesheet >
    <meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
    <meta content=C# name=CODE_LANGUAGE>
    <meta content=JavaScript name=vs_defaultClientScript>
    <meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema>
    <script type="text/javascript" src="../../inc/utilities.js" > </script>
    <style type="text/css"  >
            
        	.modalbox { line-height: 1em; height: 100px; border:3px inset black; top: 150px;  margin-left: 200px; left: 0; right: 0;  width: 425px; display: none; position: absolute; background-color : whitesmoke;}
            .InvalidTextareaLength { color: #ff0000; font-weight: bold; }
            .ValidTextAreaLength {color: #000000; font-weight:normal; }
            .counter { font-weight:normal;font-size:xx-small;margin:0; padding:0; }
            .modalbox table { margin: 0 0 .5em .5em;  }
            .modalbox table tr td label { margin-top: 1em; }
            
            .tab-content { padding:8px;}
            #BrokerInformation_LegalEntityIdentifier { width: 200px; }
            .main-body { height: 100%; }
        form { margin-bottom: 0; }
    </style>
  </head>
  <body>
      <h4 class="page-header">General Settings</h4>
   <script language=javascript>
    <!--
       var oldTabIndex = 0;
       var tabsEnabled = true;
       var Tab_IsValid = true;
       function _init() {
           resizeForIE6And7(1200, 640);
           Tab_LoadData($("#tab" + oldTabIndex).attr("data-content"), function () {
               TextareaUtilities.LengthCheckInit();
           });
       }
       function SaveData(callback) {
           Tab_SaveData($("#tab" + oldTabIndex).attr("data-content"), callback);
       }
       function ApplyNow(callback) {
           Tab_ApplyNow($("#tab" + oldTabIndex).attr("data-content"), callback);
       }
       function onOKClick() {
           SaveData(function () {
               if (Tab_IsValid)
                   onClosePopup();
           });           
       }

       
       function onTabChange(selectedIndex) {
           if (tabsEnabled) {
               var oldTab = $("#tab" + oldTabIndex);
               var newTab = $("#tab" + selectedIndex);

               var tabChangeFinished = function(){ onTabChangeFinished(selectedIndex, oldTab, newTab) };
               if (isDirty() && (selectedIndex != oldTabIndex)) {
                    PolyConfirmSave(function(confirmed){
                        var dataContent = oldTab.attr("data-content");
                        if (confirmed){
                            Tab_SaveData(dataContent, tabChangeFinished);
                        } else {
                            Tab_LoadData(dataContent, function () {
                                clearDirty();
                                tabChangeFinished();
                            });
                        }
                    }, true, function(){
                        TextareaUtilities.CounterRefresh();
                    });
               }
               else {
                    tabChangeFinished();
               }
           }
       }

       function onTabChangeFinished(selectedIndex, oldTab, newTab) {
           if (selectedIndex == oldTabIndex) //This is true after cancel is pressed on "save changes" confirmation box
               return;

           if (Tab_IsValid) {
               $("#tabContent" + selectedIndex).css("display", "");
               $("#tabContent" + oldTabIndex).css("display", "none");
               oldTabIndex = selectedIndex;
               Tab_LoadData(newTab.attr("data-content"), function () {
                   oldTab[0].className = "";
                   newTab[0].className = "selected";

                   TextareaUtilities.CounterRefresh();
               });
           }
           else {
               TextareaUtilities.CounterRefresh();
           }
       }

       function Tab_LoadData(clientID, callback) {
           if (Tab_IsValid) {
               if (eval("typeof( " + clientID + "_init )") == "function") eval(clientID + "_init();");
               var args = new Object();               
               gService.main.callAsyncSimple(clientID + '_LoadData', args, function (result) {
                   if (!result.error) {
                       populateForm(result.value, clientID);
                       if (typeof executePostPopulateFormCallback === 'function') {
                           executePostPopulateFormCallback(result.value);
                       }
                   }
                   if (eval("typeof( " + clientID + "_loaded )") == "function") eval(clientID + "_loaded();");

                   if (typeof callback === "function") {
                       callback();
                   }
               });               
           }

       }

       function FieldsExceededCharLimit(fields) {
           var prefix = "The following fields have exceeded their character limit and will be truncated: \n";
           var suffix = "";
           for (var i = 0; i < fields.length; ++i) {
               suffix += "\n- " + fields[i];
           }
           return prefix + suffix;
       }

       function Tab_SaveData(clientID, callback) {
           if (typeof (Page_Validators) != null) {
               Tab_Validate(clientID);
           }
           if (Tab_IsValid) {
               try {                   
                   var args = getAllFormValues(clientID);                   
                   if (typeof executePostGetAllFormValuesCallback === 'function')
                       executePostGetAllFormValuesCallback(args);
                   
                   //OPM 8340
                   var fthb = args["FthbCustomDefinition"];
                   var submitAgreement = args["LpeSubmitAgreement"];
                   var quickPricerNRM = args["QuickPricerNoResultMessage"];
                   var quickPricerD = args["QuickPricerDisclaimerAtResult"];

                   var fields = new Array();
                   var count = 0;

                   if ((typeof (fthb) != 'undefined') && (fthb.length > 1000)) {
                       fields[count++] = "FTHB and Housing History Help Text";
                   }

                   if ((typeof (submitAgreement) != 'undefined') && (submitAgreement.length > 1500)) {
                       fields[count++] = "Loan Submission Agreement";
                   }

                   if ((typeof (quickPricerNRM) != 'undefined') && quickPricerNRM.length > 500) {
                       fields[count++] = "QuickPricer's No Result Message";
                   }

                   if ((typeof (quickPricerD) != 'undefined') && quickPricerD.length > 500) {
                       fields[count++] = "QuickPricer's Disclaimer";
                   }


                   if (count > 0)
                       alert(FieldsExceededCharLimit(fields));

                   gService.main.callAsyncSimple(clientID + '_SaveData', args, function (result) {
                       if (!result.error) {
                           clearDirty();
                           populateForm(result.value, clientID);
                           if (eval("typeof( " + clientID + "_loaded )") == "function") eval(clientID + "_loaded();");
                       }
                       else {
                           alert("Unable to save corporate configuration.");
                       }
                       document.activeElement.blur();

                       TextareaUtilities.CounterRefresh();

                       if (typeof callback === "function") {
                           callback();
                       }
                   });                   
               }
               catch (e) {
                   alert("Unable to save corporate configuration.");

               }
           }
       }

       function Tab_ApplyNow(clientID, callback) {
           if (typeof (Page_Validators) != null) {
               Tab_Validate(clientID);
           }
           if (Tab_IsValid) {
               var args = getAllFormValues(clientID);
               gService.main.callAsyncSimple(clientID + '_ApplyNow', args, function (result) {
                   if (!result.error) {
                       populateForm(result.value, clientID);
                       clearDirty();
                   }
                   if (eval("typeof( " + clientID + "_loaded )") == "function") eval(clientID + "_loaded();");

                   if (typeof callback === "function") {
                       callback();
                   }
               });               
           }
       }

       function Tab_Validate(clientID) {
           for (var i = 0; i < Page_Validators.length; i++) {
               if (Page_Validators[i].id.indexOf(clientID) == 0) {
                   ValidatorValidate(Page_Validators[i]);
               }
           }
           ValidatorUpdateIsValid();
           ValidationSummaryOnSubmit();
           Page_BlockSubmit = !Page_IsValid;
           Tab_IsValid = Page_IsValid;
           return Page_IsValid;
       }        
    //-->
   </script>
<form method=post runat="server">

<table id=Table3 height="100%" cellSpacing=0 cellPadding=0 width="100%" border=0>
<tr>
    <td>
            <div class="Tabs">
            <ul class="tabnav">
                <li runat="server" id="tab0" class="selected" onclick="onTabChange(0);" data-content = "BrokerInformation"><a >Company Info</a></li>
                <li runat="server" id="tab1" onclick="onTabChange(1);" data-content = "BrokerOption"><a >Options</a></li>
                <li runat="server" id="tab2" onclick="onTabChange(2);" data-content = "BrokerCRA"><a >Credit Reports</a></li>
                <li runat="server" id="tab9" onclick="onTabChange(9);" data-content = "Services"><a >Services</a></li>
                <li runat="server" id="tab3" onclick="onTabChange(3);" data-content = "NamingScheme"><a >Loan Naming</a></li>
                <li runat="server" id="tab4" onclick="onTabChange(4);" data-content = "PasswordOptions"><a >Password Options</a></li>
                <li runat="server" id="tab5" onclick="onTabChange(5);" data-content = "BrokerECOA"><a >Legal Addresses</a></li>
                <li runat="server" id="tab6" onclick="onTabChange(6);" data-content = "LendingLicense"><a >Company Licenses</a></li>
                <li runat="server" id="tab7" onclick="onTabChange(7);" data-content = "LockPolicies"><a >Lock Policies</a></li>
                <% if(CanViewIPRestrictionsTab){ %>
                <li runat="server" id="tab8" onclick="onTabChange(8);" data-content = "GlobalWhiteList"><a >IP Restrictions</a></li>
                <%} %>
            </ul>
        </div>
    </td>
</tr>
  <tr class="main-body">
    <td style="BORDER-RIGHT: 2px outset; PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset; PADDING-TOP: 5px; BORDER-BOTTOM: 2px outset" vAlign=top>
        <div id="tabContent0">
        <uc1:brokerinformationusercontrol id="BrokerInformation" runat="server"/></div>
        <div id="tabContent1" style="display: none">
        <uc1:brokeroptionusercontrol id="BrokerOption" runat="server" /></div>
        <div id="tabContent2" style="display: none">
        <uc1:brokercrasettings id="BrokerCRA" runat="server" /></div>
        <div id="tabContent3" style="display: none">
        <uc1:namingschemeusercontrol id="NamingScheme" runat="server" /></div>
        <div id="tabContent4" style="display: none">
        <uc1:passwordoptionsusercontrol id="PasswordOptions" runat="server" /></div>
        <div id="tabContent5" style="display: none">
        <uc1:brokerecoausercontrol id="BrokerECOA" runat="server" /></div>
        <div id="tabContent6" style="display: none">
        <uc1:LendingLicenses id="LendingLicense" NamingPrefix="lp0" IsCompanyLOID="True" runat="server" /> </div>   
        <div id="tabContent7" style="display: none">        
        <uc1:brokerlockpoliciesusercontrol id="LockPolicies" runat="server" /></div>

         <% if(CanViewIPRestrictionsTab){ %>
        <div id="tabContent8" style="display: none">        
        <uc1:brokerglobalwhitelistusercontrol id="GlobalWhiteList" runat="server" /></div>   
        <%} %>     
        <div id="tabContent9" style="display: none;" runat="server" >
            <uc1:LenderServices ID="Services" runat="server" />
        </div>
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%">
        <tr>
          <td align=middle width="100%">
			<INPUT type=button id="okaybutton" value="  OK  " onclick="onOKClick();">
			<INPUT type=button value=Cancel onclick="onClosePopup();">
			<INPUT type=button id="applybutton" value=Apply onclick="SaveData();">
		  </td>
          <td align=right>
			<INPUT onclick=displayHelp(); type=button value="Help (F1)">
		  </td>
		</tr>
	  </table>
	</td>
	</tr>
	</table>
	</form>

	<!--[if lte IE 6.5]><iframe id="selectmask" src="javascript:void(0)" ></iframe><![endif]-->
  </body>
 
</html>
