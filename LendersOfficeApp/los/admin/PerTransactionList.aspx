﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PerTransactionList.aspx.cs"
    Inherits="LendersOfficeApp.los.admin.PerTransactionList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Monthly Loan Billing</title>

    <script type="text/javascript" src="../../inc/jquery.tablesorter.min.js"></script>

    <style type="text/css">
        #Content
        {
            width: 100%;
            max-height: 100%;
        }
        #BillableLoansPanel
        {
            padding: 5px;
        }
        #TopControls
        {
            margin-top: 10px;
            margin-bottom: 5px;
            width: 100%;
        }
        #TopLeft
        {
            display: inline-block;
            width: 50%;
        }
        #Download
        {
            display:inline-block;
            text-align: right;
            width: 50%;
        }
        option.Empty
        {
            color: Gray;
        }
        #Close
        {
            display: inline-block;
            width: 100%;
            text-align: center;
            margin-top: 5px;
        }
        #BillableLoansContainer
        {
            max-height: 390px;
            overflow-y: auto;
            overflow-x: hidden;
            border: solid 1px black;
        }
        #TotalLoans
        {
            font-weight: bold;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <h4 class="page-header">Billing</h4>
    <script type="text/javascript">
        function onClose()
        {
            parent.LQBPopup.Return();
        }
        function onLoanClick(loanid)
        {
            var window = parent.LQBPopup.GetArguments();
            window.editLoan(loanid);
        }
    </script>
    <form id="aspxForm" runat="server">
    <div id="Content">
        
        <asp:ScriptManager ID="UpdatePanelScriptManager" runat="server" />
        <asp:UpdatePanel ID="BillableLoansPanel" UpdateMode="Conditional" runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="MonthPicker" EventName="TextChanged" />
            </Triggers>
            <ContentTemplate>
                <div id="TopControls">
                    <span id="TopLeft">
                        <asp:DropDownList ID="MonthPicker" AutoPostBack="true" OnSelectedIndexChanged="MonthPicker_OnSelectedIndexChanged" OnInit="MonthPicker_Init" runat="server">
                        </asp:DropDownList>
                        <label id="TotalLoans">
                            Total loans: <label id="TotalLoanCount" runat="server" />
                        </label>
                    </span><span id="Download">
                        <asp:Button ID="DownloadButton" Text="Download CSV" OnClick="DownloadClick" runat="server" />
                    </span>
                </div>
                <div id="BillableLoansContainer">
                    <asp:GridView ID="BillableLoans" OnRowDataBound="BillableLoans_RowDataBound" OnSorting="BillableLoans_Sorting" AutoGenerateColumns="false" AllowSorting="true" Width="100%" HeaderStyle-CssClass="header" RowStyle-CssClass="GridItem" AlternatingRowStyle-CssClass="GridAlternatingItem" runat="server" >
                        <Columns>
                            <asp:TemplateField HeaderText="Loan Number" SortExpression="LoanNumber">
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="LoanNumberLink"><%# AspxTools.HtmlString((string)Eval("LoanNumber")) %></asp:LinkButton>
                                    <ml:EncodedLabel runat="server" ID="LoanNumberLabel"></ml:EncodedLabel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Borrower" DataField="Borrower" SortExpression="Borrower" />
                            <asp:BoundField HeaderText="Loan Officer" DataField="LoanOfficer" SortExpression="LoanOfficer" />
                            <asp:BoundField HeaderText="Branch Name" DataField="BranchName" SortExpression="BranchName" />
                            <asp:BoundField HeaderText="Branch Code" DataField="BranchCode" SortExpression="BranchCode" />
                            <asp:TemplateField HeaderText ="Billed Date" SortExpression="BilledDate">
                                <ItemTemplate>
                                    <%# AspxTools.HtmlString(((DateTime)Eval("BilledDate")).ToString("d")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Billing Reason" SortExpression="BillingType">
                                <ItemTemplate>
                                    <%# AspxTools.HtmlString(GetBillingReason((DataAccess.E_BillingReason)Eval("BillingType"))) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <span id="Close">
                    <input type="button" value="Close" id="CloseButton" onclick="onClose()"/>
                </span>
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>