<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PasswordOptionsUserControl.ascx.cs" Inherits="LendersOfficeApp.los.admin.PasswordOptionsUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<script type="text/javascript">
    function PasswordOptions_loaded()
    {
        <%= AspxTools.JsGetElementById(PasswordExpiration) %>.readOnly = <%= AspxTools.JsBool(PasswordOption.SelectedValue != "Never") %>;

        <%= AspxTools.JsGetElementById(ErrorMessage) %>.style.display = "none";
        <%= AspxTools.JsGetElementById(Feedback) %>.style.display = "none";

        <%= AspxTools.JsGetElementById(m_ExpirationPeriod) %>.style.backgroundColor = "white";

        var errorMessageText = <%= AspxTools.JsGetElementById(ErrorMessageText) %>;
        var errorMessageTextbox = <%= AspxTools.JsGetElementById(ErrorMessage) %>;
        errorMessageText.innerText = errorMessageTextbox.value;

        var feedbackText = <%= AspxTools.JsGetElementById(FeedbackText) %>;
        var feedbackTextbox = <%= AspxTools.JsGetElementById(Feedback) %>;
        feedbackText.innerText = feedbackTextbox.value;
    }

    function getBasePanel( oControl )
    {
        while( oControl != null && oControl.id != null )
        {
            if( oControl.id.indexOf( "BasePanel" ) != -1 )
            {
                return oControl;
            }

            oControl = oControl.parentElement;
        }

        return null;
    }

    function getBaseName( oBase )
    {
        if( oBase.id.indexOf( "BasePanel" ) != -1 )
        {
            return oBase.id.substr( 0 , oBase.id.indexOf( "BasePanel" ) );
        }

        return "";
    }

    function onPasswordOptionClick( oControl )
    {
        var base , name;

        if( ( base = getBasePanel( oControl ) ) == null || ( name = getBaseName( base ) ) == "" )
        {
            return;
        }

        getElementByIdFromParent(base, name + "PasswordExpiration" ).readOnly = !getElementByIdFromParent(base, name + "PasswordOption_2" ).checked;

        if( getElementByIdFromParent(base, name + "PasswordOption_1" ).checked )
		{
			var ce = <%= AspxTools.JsGetElementById(m_CycleExpiration) %>;
			ce.disabled = true;
			ce.checked = false;
			<%= AspxTools.JsGetElementById(m_ExpirationPeriod) %>.disabled = true;
		}
		else
			<%= AspxTools.JsGetElementById(m_CycleExpiration) %>.disabled = false;
    }

    function onApplyPasswordOptions( oControl ) { try
    {
        if( typeof( ApplyNow ) == "function" )
        {
            ApplyNow();
        }
    }
    catch( e )
    {
    }}

    function onExpirationCycleClick(oCheck)
	{
		<%= AspxTools.JsGetElementById(m_ExpirationPeriod) %>.disabled = !oCheck.checked;
	}
</script>
<asp:Panel id="BasePanel" runat="server" style="PADDING-RIGHT: 8px;PADDING-LEFT: 8px;PADDING-BOTTOM: 8px;PADDING-TOP: 8px" Width="100%">
	<TABLE class="FormTable" cellSpacing="4" cellPadding="0" border="0">
		<TR>
			<TD noWrap><SPAN style="PADDING-RIGHT: 8px">Branch </SPAN>
				<asp:DropDownList id="BranchList" runat="server"></asp:DropDownList></TD>
		</TR>
		<TR>
			<TD noWrap>
				<asp:RadioButtonList id="PasswordOption" onclick="onPasswordOptionClick( this );" runat="server" CellSpacing="0" CellPadding="0">
					<asp:ListItem Value="NextL" Selected="True">
                Must change password at next logon
            </asp:ListItem>
					<asp:ListItem Value="Never">
                Password never expires
            </asp:ListItem>
					<asp:ListItem Value="Dated">
                Password expires on:
            </asp:ListItem>
				</asp:RadioButtonList>
				<DIV style="PADDING-RIGHT: 4px; PADDING-LEFT: 22px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px">
					<ml:DateTextBox id="PasswordExpiration" style="PADDING-LEFT: 4px" Width="100" runat="server" HelperAlign="absmiddle" preset="date" ReadOnly="True"></ml:DateTextBox></DIV>
				<asp:CheckBox id="m_CycleExpiration" onclick="onExpirationCycleClick( this );" runat="server" CssClass="FieldLabel" Text=""></asp:CheckBox>...and
				continue to expire passwords every&nbsp;
				<asp:DropDownList id="m_ExpirationPeriod" Runat="server" enabled="False">
					<asp:ListItem Value="15">15</asp:ListItem>
					<asp:ListItem Value="30">30</asp:ListItem>
					<asp:ListItem Value="45">45</asp:ListItem>
					<asp:ListItem Value="60">60</asp:ListItem>
				</asp:DropDownList>&nbsp;days following update
			</TD>
		</TR>
		<tr>
		    <td>
		        <span id="ActiveDirectoryMessage" runat="server" style="BORDER-RIGHT: medium none; BORDER-TOP: medium none; PADDING-LEFT: 0px; BACKGROUND: none transparent scroll repeat 0% 0%; BORDER-LEFT: medium none; COLOR: red; BORDER-BOTTOM: medium none">Password Options will not be applied to Active Directory users.</span>
		    </td>
		</tr>
		<TR>
			<TD noWrap>
				<INPUT id="ApplyOptions" onclick="onApplyPasswordOptions( this );" type="button" value="Apply option to branch" name="ApplyOptions" runat="server">
				<BR>
				<BR>
				<asp:TextBox id="ErrorMessage" width="400px" style="BORDER-RIGHT: medium none; BORDER-TOP: medium none; DISPLAY: none; PADDING-LEFT: 0px; BACKGROUND: none transparent scroll repeat 0% 0%; FONT: 12px arial; BORDER-LEFT: medium none; COLOR: red; BORDER-BOTTOM: medium none" runat="server"></asp:TextBox>
				<span id="ErrorMessageText" runat="server" style="BORDER-RIGHT: medium none; BORDER-TOP: medium none; PADDING-LEFT: 0px; BACKGROUND: none transparent scroll repeat 0% 0%; FONT: 12px arial; BORDER-LEFT: medium none; COLOR: red; BORDER-BOTTOM: medium none">debug 1</span>
				<br>
				<asp:TextBox id="Feedback" width="400px" style="BORDER-RIGHT: medium none; BORDER-TOP: medium none; DISPLAY: none; PADDING-LEFT: 0px; BACKGROUND: none transparent scroll repeat 0% 0%; FONT: 12px arial; BORDER-LEFT: medium none; BORDER-BOTTOM: medium none" runat="server"></asp:TextBox>
				<span id="FeedbackText" runat="server" style="BORDER-RIGHT: medium none; BORDER-TOP: medium none; PADDING-LEFT: 0px; BACKGROUND: none transparent scroll repeat 0% 0%; FONT: 12px arial; BORDER-LEFT: medium none; BORDER-BOTTOM: medium none">debug 2</span>
			</TD>
		</TR>
	</TABLE>
</asp:Panel>
