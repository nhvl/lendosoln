﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Integration.MortgageInsurance;
using LendersOffice.Security;

namespace LendersOfficeApp.los.admin
{
    public partial class MIVendorBrokerAccountDetails : LendersOffice.Common.BasePage
    {
        private IEnumerable<MortgageInsuranceVendorBrokerSettings> enabledVendors;

        /// <summary>
        /// Gets a list of enabled Mortgage Insurance Vendors for the given broker.
        /// </summary>
        /// <value>An IEnumerable of enabled Mortgage Insurance Vendors for the given broker.</value>
        private IEnumerable<MortgageInsuranceVendorBrokerSettings> EnabledVendors
        {
            get
            {
                if (this.enabledVendors == null)
                {
                    Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
                    this.enabledVendors = MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(brokerId);
                }

                return this.enabledVendors;
            }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        override protected void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.Page_Load);
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            if (!IsPostBack)
            {
                var allVendors = MortgageInsuranceVendorConfig.ListActiveVendors();
                this.VendorDetails.DataSource = this.EnabledVendors.Join(allVendors,
                                                                    enabled => enabled.VendorId,
                                                                    all => all.VendorId,
                                                                    (enabled,all) =>
                                                                    new
                                                                    {
                                                                        m_VendorName = all.VendorTypeFriendlyDisplay,
                                                                        m_VendorId = all.VendorId,
                                                                        m_IsAccountIdRequired = all.IsUseAccountId,
                                                                        m_Username = enabled.UserName,
                                                                        m_Password = enabled.Password,
                                                                        m_AccountId = enabled.AccountId,
                                                                        m_PolicyId = enabled.PolicyId,
                                                                        m_IsDelegated = enabled.IsDelegated
                                                                    });
                this.VendorDetails.DataBind();
                Page.Validate();
            }
        }

        protected void VendorDetailsBind(object sender, RepeaterItemEventArgs e)
        {
            Label m_VendorName = (Label)e.Item.FindControl("m_VendorName");
            m_VendorName.Text = DataBinder.Eval(e.Item.DataItem, "m_VendorName") + " Account Details";

            HiddenField m_VendorId = (HiddenField)e.Item.FindControl("m_VendorId");
            m_VendorId.Value = DataBinder.Eval(e.Item.DataItem, "m_VendorId").ToString();

            TextBox m_Username = (TextBox)e.Item.FindControl("m_Username");
            m_Username.Text = DataBinder.Eval(e.Item.DataItem, "m_Username").ToString();

            TextBox m_Password = (TextBox)e.Item.FindControl("m_Password");
            if (string.IsNullOrEmpty(DataBinder.Eval(e.Item.DataItem, "m_Password").ToString()))
            {
                m_Password.Text = string.Empty;
                m_Password.Attributes["value"] = string.Empty;
            }
            else
            {
                m_Password.Text = ConstAppDavid.FakePasswordDisplay;
                m_Password.Attributes["value"] = ConstAppDavid.FakePasswordDisplay;
            }
            m_Password.TextMode = TextBoxMode.Password;

            PlaceHolder m_AccountIdPanel = (PlaceHolder)e.Item.FindControl("AccountIdPanel");
            m_AccountIdPanel.Visible = Boolean.Parse(DataBinder.Eval(e.Item.DataItem, "m_IsAccountIdRequired").ToString());
            if (m_AccountIdPanel.Visible)
            {
                TextBox m_AccountId = (TextBox)e.Item.FindControl("m_AccountId");
                m_AccountId.Text = DataBinder.Eval(e.Item.DataItem, "m_AccountId").ToString();
            }

            TextBox m_PolicyId = (TextBox)e.Item.FindControl("m_PolicyId");
            m_PolicyId.Text = DataBinder.Eval(e.Item.DataItem, "m_PolicyId").ToString();

            RadioButton m_delegatedUA = (RadioButton)e.Item.FindControl("m_delegatedUA");
            m_delegatedUA.Checked = Boolean.Parse(DataBinder.Eval(e.Item.DataItem, "m_IsDelegated").ToString());
            RadioButton m_nondelegatedUA = (RadioButton)e.Item.FindControl("m_nondelegatedUA");
            m_nondelegatedUA.Checked = !m_delegatedUA.Checked;
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            foreach(RepeaterItem item in VendorDetails.Items)
            {
                HiddenField m_VendorId = (HiddenField)item.FindControl("m_VendorId");
                TextBox m_Username = (TextBox)item.FindControl("m_Username");
                TextBox m_Password = (TextBox)item.FindControl("m_Password");
                TextBox m_AccountId = (TextBox)item.FindControl("m_AccountId");
                TextBox m_PolicyId = (TextBox)item.FindControl("m_PolicyId");
                RadioButton m_IsDelegated = (RadioButton)item.FindControl("m_delegatedUA");

                var brokerSetting = this.EnabledVendors.First(v => v.VendorId == new Guid(m_VendorId.Value));
                brokerSetting.UserName = m_Username.Text;
                brokerSetting.Password = m_Password.Text;
                if (m_AccountId != null)
                {
                    brokerSetting.AccountId = m_AccountId.Text;
                }
                brokerSetting.PolicyId = m_PolicyId.Text;
                brokerSetting.IsDelegated = m_IsDelegated.Checked;
                brokerSetting.SaveLogin();
            }

            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, null) ;
        }
    }       
}
