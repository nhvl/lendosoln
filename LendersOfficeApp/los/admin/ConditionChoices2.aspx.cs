﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using LendersOffice.Security;
using System.Web.Services;
using LendersOffice.Common;
using DataAccess;
using System.Text;

namespace LendersOfficeApp.los.admin
{
    public partial class ConditionChoices2 : LendersOffice.Common.BasePage
    {
        protected bool showRequiredDocType
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal.BrokerDB.IsEDocsEnabled;
            }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingConditionChoices
                };
            }
        }

        protected string ConvertBoolToYes(bool val)
        {
            return val ? "Yes" : "No";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("LQBPopup.js");	
            
            if (Request.Form["CSV"] != null && string.IsNullOrEmpty(Request.Form["SortOrder"]) == false)
            {
                GenerateCSV();
                return;
            }
            if (IsPostBack == false)
            {
                IEnumerable<ConditionChoice> choices = ConditionChoice.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId);

                ConditionChoiceList.DataSource = choices;
                ConditionChoiceList.DataBind();

                if (ConditionChoiceList.Rows.Count > 0)
                {
                    ConditionChoiceList.HeaderRow.TableSection = TableRowSection.TableHeader;
                }
            }
       
        }

        private void GenerateCSV()
        {
            List<int> ids = ObsoleteSerializationHelper.JsonDeserialize<List<int>>(Request.Form["SortOrder"]);
            ConditionChoice[] choices = ConditionChoice.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId).ToArray();
            IEnumerable<ConditionChoice> orderedChoices = choices.
                Where(p => ids.Contains(p.ConditionChoiceId)).
                OrderBy(p => ids.IndexOf(p.ConditionChoiceId));

            WriteCsv(orderedChoices);
        }

        [WebMethod]
        public static string Save(int[] idsToDelete, int[] sortOrder)
        {

            try
            {
                ConditionChoice.SaveSortAndDelete(BrokerUserPrincipal.CurrentPrincipal.BrokerId, idsToDelete, sortOrder);
                return "OK";

            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking(e);
                throw;
            }
        }

        public static object RetrieveLatestConditionChoice(int id)
        {
            return ConditionChoice.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId, id);
        }

        private void WriteCsv(IEnumerable<ConditionChoice> choices)
        {
            Response.AddHeader("Content-Disposition", "attachment; filename=\"privileges.csv\"");
            Response.ContentType = "text/csv";
            StringBuilder sb = new StringBuilder();

            string csvFormat = "{0},{1},{2},{3},{4},{5},{6},{7}\n";
            sb.AppendFormat(csvFormat, "Category", "Condition Type", "Loan Type", "Condition Subject", "Hidden?", "Required Document Type", "To Be Assigned To", "To Be Owned By", "Due Date");

            foreach (ConditionChoice choice in choices)
            {
                sb.AppendFormat(csvFormat,
                    C(choice.Category.Category),
                    C(choice.ConditionType_rep),
                    C(choice.sLT_rep),
                    C(choice.ConditionSubject),
                    C(choice.IsHidden.ToString()),
                    C(choice.RequiredDocType_rep),
                    C(choice.ToBeAssignedRole),
                    C(choice.ToBeOwnedByRole),
                    C(choice.FriendlyDueDateDescription)); 
            }

            Response.Write(sb.ToString());
            Response.Flush();
            Response.End();

        }

        private string C(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return "";
            }
            data = data.Replace("\"", "\"\"");
            return "\"" + data + "\"";
        }

        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            base.OnInit(e);
        }
    }
}
