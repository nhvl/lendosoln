﻿namespace LendersOfficeApp.Los.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// Service credential editor.
    /// </summary>
    public partial class EditServiceCredential : LendersOffice.Common.BasePage
    {
        /// <summary>
        /// The context for editing the current service credential.
        /// </summary>
        private ServiceCredentialContext editingContext;

        /// <summary>
        /// A backing field for <see cref="BrokerId"/>.
        /// </summary>
        private Guid? brokerId;

        /// <summary>
        /// The principal.
        /// </summary>
        private AbstractUserPrincipal principal = null;

        /// <summary>
        /// The title quote lender service data.
        /// </summary>
        private Lazy<Dictionary<int, OptionInfo>> titleQuoteLenderServiceData = null;

        /// <summary>
        /// The principal for the user being edited.
        /// </summary>
        private AbstractUserPrincipal principalForUserBeingEdited = null;

        /// <summary>
        /// Gets the roles required to load the popup.
        /// </summary>
        /// <value>The roles required for loading the page.</value>
        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                if (this.EmployeeId.HasValue && !this.IsInternal && !this.IsFromUserProfile && 
                    !string.Equals(this.PrincipalForUserBeingEdited?.Type, "P", StringComparison.OrdinalIgnoreCase))
                {
                    // Need admin role if editing an employee and editer is not internal user and not editing from user profile page and not editing a P user.
                    return new E_RoleT[] { E_RoleT.Administrator };
                }

                return base.RequiredRolesForLoadingPage;
            }
        }

        /// <summary>
        /// Gets the permissions required to load the popup.
        /// </summary>
        /// <value>The permissions required to load the popup.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                if ((this.EmployeeId.HasValue && string.Equals(this.PrincipalForUserBeingEdited?.Type, "P", StringComparison.OrdinalIgnoreCase)) ||
                    this.OriginatingCompanyId.HasValue)
                {
                    // If we're editing a TPO user, the editer needs to have this permission.
                    return new Permission[] { Permission.CanAdministrateExternalUsers };
                }
                else if (this.EmployeeId.HasValue || this.IsInternal)
                {
                    // No need to check for permissions if its internal or if its for an employee.
                    return base.RequiredPermissionsForLoadingPage;
                }
                else if (this.BranchId.HasValue)
                {
                    // For branch editing, need branch view/edit permission.
                    return new Permission[] { Permission.AllowViewingAndEditingBranches };
                }
                else
                {
                    // For broker editing, need general settings view/edit permission.
                    return new Permission[] { Permission.AllowViewingAndEditingGeneralSettings };
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this page was openend from the Your Profile page.
        /// </summary>
        /// <value>Whether this page was openend from the Your Profile page.</value>
        protected bool IsFromUserProfile
        {
            get
            {
                return RequestHelper.GetBool("iup");
            }
        }

        /// <summary>
        /// Gets a value indicating whether the broker is configured to use VOX.
        /// </summary>
        /// <value>Whether the broker is configured to use VOX.</value>
        protected bool IsEnableVOXConfiguration
        {
            get
            {
                return BrokerDB.RetrieveById(this.BrokerId).IsEnableVOXConfiguration;
            }
        }

        /// <summary>
        /// Gets the <see cref="BrokerDB.BrokerID"/> for the current credential.
        /// </summary>
        private Guid BrokerId => this.brokerId ?? (this.brokerId = this.ComputedBrokerId).Value;

        /// <summary>
        /// Gets the computed value of <see cref="BrokerId"/>. 
        /// </summary>
        private Guid ComputedBrokerId
        {
            get
            {
                if (this.IsInternal)
                {
                    if (!this.BrokerIdForInternal.HasValue)
                    {
                        throw CBaseException.GenericException("Internal access requires a valid broker id");
                    }

                    return this.BrokerIdForInternal.Value;
                }

                return PrincipalFactory.CurrentPrincipal.BrokerId;
            }
        }

        /// <summary>
        /// Gets the principal for the current user or the principal for the user being edited in LOAdmin.  Be careful with this.
        /// </summary>
        private AbstractUserPrincipal Principal
        {
            get
            {
                if (this.principal != null)
                {
                    return this.principal;
                }

                if (this.IsInternal)
                {
                    // For the special case where this is being opened in the loadmin user editor.
                    // We'll let it break if these conditions aren't met.
                    if (!this.UserId.HasValue || !this.EmployeeId.HasValue || !Enum.IsDefined(typeof(UserType), this.EmployeeType))
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Required members for EditServiceCredential in LOAdmin not met");
                    }

                    // Retrieve this principal, but don't set the current thread's principal
                    this.principal = PrincipalFactory.RetrievePrincipalForUser(
                        this.BrokerId,
                        this.UserId.Value,
                        this.EmployeeType == LendersOffice.ObjLib.ServiceCredential.UserType.TPO ? "P" : "B");

                    if (this.principal.EmployeeId != this.EmployeeId.Value)
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Mismatch between principal employee id and passed in employee id.");
                    }
                }
                else
                {
                    this.principal = PrincipalFactory.CurrentPrincipal;
                }

                return this.principal;
            }
        }

        /// <summary>
        /// Gets the principal for the user being edited. Null if not editing a user.
        /// </summary>
        private AbstractUserPrincipal PrincipalForUserBeingEdited
        {
            get
            {
                if (!this.UserId.HasValue)
                {
                    return null;
                }

                if (this.principalForUserBeingEdited == null)
                {
                    this.principalForUserBeingEdited = PrincipalFactory.RetrievePrincipalForUser(this.BrokerId, this.UserId.Value, this.EmployeeType == LendersOffice.ObjLib.ServiceCredential.UserType.TPO ? "P" : "B");
                }

                return this.principalForUserBeingEdited;
            }
        }

        /// <summary>
        /// Gets the user id if this is opened in the internal use editor.
        /// </summary>
        /// <value>The user id if this opened in the internal user editor.</value>
        private Guid? UserId
        {
            get
            {
                var userId = RequestHelper.GetGuid("uid", Guid.Empty);
                return userId == Guid.Empty ? (Guid?)null : userId;
            }
        }

        /// <summary>
        /// Gets the broker id if this is opened in the internal user editor.
        /// </summary>
        /// <value>The broker id if this is opened in the internal user editor.</value>
        private Guid? BrokerIdForInternal
        {
            get
            {
                var brokerId = RequestHelper.GetGuid("bid", Guid.Empty);
                return brokerId == Guid.Empty ? (Guid?)null : brokerId;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this is openend in the internal user editor.
        /// </summary>
        /// <value>Whether opened in the internal user editor.</value>
        private bool IsInternal => PrincipalFactory.CurrentPrincipal is InternalUserPrincipal;

        /// <summary>
        /// Is it new.
        /// </summary>
        private bool IsNew => RequestHelper.GetSafeQueryString("cmd") == "add";

        /// <summary>
        /// Is it delete mode.
        /// </summary>
        private bool IsDelete => RequestHelper.GetSafeQueryString("cmd") == "delete";

        /// <summary>
        /// The target id.
        /// </summary>
        private int? Id => RequestHelper.GetInt("id");

        /// <summary>
        /// Gets the branch Id.
        /// </summary>
        private Guid? BranchId
        {
            get
            {
                var branch = RequestHelper.GetGuid("branch", Guid.Empty);
                return branch == Guid.Empty ? (Guid?)null : branch;
            }
        }

        /// <summary>
        /// Gets the originating company id.
        /// </summary>
        private Guid? OriginatingCompanyId
        {
            get
            {
                Guid oc = RequestHelper.GetGuid("oc", Guid.Empty);
                return oc == Guid.Empty ? (Guid?)null : oc;
            }
        }

        /// <summary>
        /// Gets the employee id.
        /// </summary>
        /// <value>The employee id.</value>
        private Guid? EmployeeId
        {
            get
            {
                Guid? employee = RequestHelper.GetGuid("employee", Guid.Empty);
                if (employee == Guid.Empty)
                {
                    employee = null;
                }

                return employee;
            }
        }

        /// <summary>
        /// Gets the employee type.
        /// </summary>
        /// <value>The employee type.</value>
        private UserType EmployeeType
        {
            get
            {
                return (UserType)RequestHelper.GetInt("eType", -1);
            }
        }

        /// <summary>
        /// Page init handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (this.titleQuoteLenderServiceData == null)
            {
                this.titleQuoteLenderServiceData = new Lazy<Dictionary<int, OptionInfo>>(this.GetTitleQuoteLenderServiceData);
            }

            if (this.IsFromUserProfile)
            {
                this.editingContext = ServiceCredentialContext.CreateEditFromProfileContext(this.Principal, isLqbEditor: true);
            }
            else if (this.EmployeeId.HasValue)
            {
                var employee = EmployeeDB.RetrieveByIdOrNull(this.BrokerId, this.EmployeeId.Value);
                this.RegisterJsGlobalVariables("IsOriginatingCompany", employee.UserType.Equals('P'));
                this.editingContext = ServiceCredentialContext.CreateEditEmployee(employee, isLqbEditor: true);
            }
            else if (this.OriginatingCompanyId.HasValue)
            {
                this.editingContext = new ServiceCredentialContext(this.BrokerId, branchId: null, originatingCompanyId: this.OriginatingCompanyId.Value, user: null, isLqbEditor: true);
                this.RegisterJsGlobalVariables("IsOriginatingCompany", this.OriginatingCompanyId.HasValue);
            }
            else if (this.BranchId.HasValue)
            {
                this.editingContext = new ServiceCredentialContext(this.BrokerId, branchId: this.BranchId.Value, originatingCompanyId: null, user: null, isLqbEditor: true);
            }
            else
            {
                this.editingContext = new ServiceCredentialContext(this.BrokerId, null, null, null, isLqbEditor: true);
            }

            this.BindCraList();
            this.BindVendorList();
            this.BindAusOptions();
            this.BindTitleQuoteLenderServices();
            this.BindUcdDeliveryOptions();
            this.BindDigitalMortgageOptions();
            this.EnableJqueryMigrate = false;

            if (!this.IsEnableVOXConfiguration)
            {
                this.IsForVerificationsOption.Style["display"] = "none";
                this.VerificationProviderRow.Style["display"] = "none";
            }

            if (this.IsFromUserProfile)
            {
                this.IsEnabledForNonSeamlessDuSection.Visible = false;
            }
        }

        /// <summary>
        /// Page load handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.EmployeeId.HasValue || this.OriginatingCompanyId.HasValue)
            {
                this.UserTypeRow.Visible = false;
            }            

            this.RegisterJsGlobalVariables("IsDelete", this.IsDelete); // always render this, since the page loads even from the postback, and some of the js will break if it doesn't know it's deleting
            if (!this.IsPostBack)
            {
                if (this.IsDelete)
                {
                    this.RenderDelete();
                }
                else if (!this.IsNew)
                {
                    var credential = ServiceCredential.Load(this.Id.Value, this.BrokerId);
                    this.AccountId.Text = credential.AccountId;
                    this.SetDropDownValue(this.CraList, credential.ServiceProviderId, credential.ServiceProviderName);
                    this.UserName.Text = credential.UserName;
                    this.UserPassword.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
                    this.ConfirmPassword.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
                    this.UserType.SelectedValue = ((int)credential.UserType).ToString();
                    this.IsForCreditReport.Checked = credential.IsForCreditReports;
                    this.IsForVerifications.Checked = credential.IsForVerifications;
                    this.IsForAusSubmission.Checked = credential.IsForAusSubmission;
                    this.IsForUcdDelivery.Checked = credential.IsForUcdDelivery;
                    this.IsForDigitalMortgage.Checked = credential.IsForDigitalMortgage;
                    this.SetDropDownValue(this.UcdDeliveryTarget, (int?)credential.UcdDeliveryTarget, credential.UcdDeliveryTarget?.ToString("G"));
                    this.SetDropDownValue(this.DigitalMortgageProviderDdl, (int?)credential.DigitalMortgageProviderTarget, credential.DigitalMortgageProviderTarget?.GetDescription());
                    this.SetDropDownValue(this.VendorList, credential.VoxVendorId, credential.VoxVendorName);
                    this.IsEnabledForNonSeamlessDu.Checked = credential.IsEnabledForNonSeamlessDu;
                    this.SetDropDownValue(this.AusOptions, (int?)credential.ChosenAusOption);
                    this.IsForTitleQuotes.Checked = credential.IsForTitleQuotes;
                    this.SetDropDownValue(this.TitleQuoteLenderServices, credential.TitleQuoteLenderServiceId, credential.TitleQuoteLenderServiceName);
                    this.IsForDocumentCapture.Checked = credential.IsForDocumentCapture;
                }
            }
        }

        /// <summary>
        /// Save click event handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void OnSaveClick(object sender, EventArgs e)
        {
            if (this.OriginatingCompanyId.HasValue && (this.IsForDigitalMortgage.Checked || this.DigitalMortgageProviderDdl.SelectedValue != "-1"))
            {
                throw new AccessDenied("Digital Mortgage is not allowed for Originator Portal.");
            }

            if (this.IsDelete)
            {
                ServiceCredential.DeleteServiceCredential(this.Id.Value, this.BrokerId);
            }
            else
            {
                ServiceCredential credential;
                if (this.IsNew)
                {
                    if (this.EmployeeId.HasValue)
                    {
                        credential = ServiceCredential.CreateNewEmployeeCredential(this.BrokerId, this.EmployeeId.Value);
                    }
                    else if (this.OriginatingCompanyId.HasValue)
                    {
                        credential = ServiceCredential.CreateNewOriginatingCompanyCredential(this.BrokerId, this.OriginatingCompanyId.Value);
                    }
                    else if (this.BranchId.HasValue)
                    {
                        credential = ServiceCredential.CreateNewBranchCredental(this.BrokerId, this.BranchId.Value);
                    }
                    else
                    {
                        credential = ServiceCredential.CreateNewBrokerCredental(this.BrokerId);
                    }
                }
                else
                {
                    credential = ServiceCredential.Load(this.Id.Value, this.BrokerId);
                }

                credential.IsForCreditReports = this.IsForCreditReport.Checked;
                credential.IsForVerifications = this.IsForVerifications.Checked;
                credential.IsForAusSubmission = this.IsForAusSubmission.Checked;
                credential.IsForTitleQuotes = this.IsForTitleQuotes.Checked;
                credential.IsForUcdDelivery = this.IsForUcdDelivery.Checked;
                credential.IsForDocumentCapture = this.IsForDocumentCapture.Checked;
                credential.IsForDigitalMortgage = this.IsForDigitalMortgage.Checked;

                if (credential.IsForCreditReports)
                {
                    credential.ServiceProviderName = this.ChosenCraName.Value;
                    credential.ServiceProviderId = Guid.Parse(this.ChosenCraId.Value);
                    if (credential.ServiceProviderId == Guid.Empty)
                    {
                        credential.ServiceProviderId = null;
                        credential.ServiceProviderName = null;
                    }
                }
                else
                {
                    credential.ServiceProviderName = null;
                    credential.ServiceProviderId = null;
                }

                if (credential.IsForVerifications)
                {
                    credential.VoxVendorName = this.ChosenVendorName.Value;
                    credential.VoxVendorId = int.Parse(this.ChosenVendorId.Value);
                    if (credential.VoxVendorId == -1)
                    {
                        credential.VoxVendorId = null;
                        credential.VoxVendorName = null;
                    }
                }
                else
                {
                    credential.VoxVendorName = null;
                    credential.VoxVendorId = null;
                }

                credential.AccountId = this.CredentialUsesAccountId(credential) ? this.AccountId.Text : string.Empty;

                AusOption chosenOption;
                if (credential.IsForAusSubmission && Enum.TryParse(this.ChosenAusOption.Value, out chosenOption) && Enum.IsDefined(typeof(AusOption), chosenOption))
                {
                    if (this.IsNew && this.IsFromUserProfile)
                    {
                        credential.IsEnabledForNonSeamlessDu = true;
                    }
                    else
                    {
                        credential.IsEnabledForNonSeamlessDu = this.IsEnabledForNonSeamlessDu.Checked;
                    }

                    credential.ChosenAusOption = chosenOption;
                }
                else
                {
                    credential.IsEnabledForNonSeamlessDu = false;
                    credential.ChosenAusOption = null;
                }

                if (credential.IsForTitleQuotes)
                {
                    credential.TitleQuoteLenderServiceName = this.ChosenTitleQuotesLenderServiceName.Value;
                    credential.TitleQuoteLenderServiceId = int.Parse(this.ChosenTitleQuotesLenderServiceId.Value);
                    if (credential.TitleQuoteLenderServiceId == -1)
                    {
                        credential.TitleQuoteLenderServiceId = null;
                        credential.TitleQuoteLenderServiceName = null;
                    }
                }
                else
                {
                    credential.TitleQuoteLenderServiceId = null;
                    credential.TitleQuoteLenderServiceName = null;
                }

                UcdDeliveryTargetOption chosenUcdTarget;
                if (credential.IsForUcdDelivery
                    && Enum.TryParse(this.UcdDeliveryTarget.SelectedValue, out chosenUcdTarget)
                    && Enum.IsDefined(typeof(UcdDeliveryTargetOption), chosenUcdTarget))
                {
                    credential.UcdDeliveryTarget = chosenUcdTarget;
                }
                else
                {
                    credential.UcdDeliveryTarget = null;
                }

                DigitalMortgageProvider chosenDigitalMortgageProvider;

                if (credential.IsForDigitalMortgage && DigitalMortgageProviderDdl.SelectedValue.TryParseDefine(out chosenDigitalMortgageProvider, true))
                {
                    credential.DigitalMortgageProviderTarget = chosenDigitalMortgageProvider;
                }
                else
                {
                    credential.DigitalMortgageProviderTarget = null;
                }

                credential.AccountId = this.CredentialUsesAccountId(credential) ? this.AccountId.Text : string.Empty;
                credential.UserName = this.UserName.Text;
                if (this.UserPassword.Text != ConstAppDavid.FakePasswordDisplay)
                {
                    credential.UserPassword = this.UserPassword.Text;
                }

                if (this.EmployeeId.HasValue)
                {
                    if (!Enum.IsDefined(typeof(UserType), this.EmployeeType))
                    {
                        throw new CBaseException(ErrorMessages.Generic, "eType query parameter is invalid.");
                    }

                    credential.UserType = this.EmployeeType;
                }
                else if (this.OriginatingCompanyId.HasValue)
                {
                    credential.UserType = LendersOffice.ObjLib.ServiceCredential.UserType.TPO;
                }
                else
                {
                    credential.UserType = (UserType)int.Parse(this.UserType.SelectedValue);
                }

                try
                {
                    credential.Save();
                }
                catch (CBaseException exc)
                {
                    Tools.LogWarning(exc);
                    this.UserMessage.Text = exc.UserMessage;
                    return;
                }
            }

            // Get latest state to pass to Listing UI.
            IEnumerable<ServiceCredential> currentList;
            if (this.EmployeeId.HasValue)
            {
                currentList = ServiceCredential.GetAllEmployeeServiceCredentials(this.BrokerId, this.EmployeeId.Value, ServiceCredentialService.All);
            }
            else if (this.OriginatingCompanyId.HasValue)
            {
                currentList = ServiceCredential.GetAllOriginatingCompanyServiceCredentials(this.BrokerId, this.OriginatingCompanyId.Value, ServiceCredentialService.All);
            }
            else if (this.BranchId.HasValue)
            {
                currentList = ServiceCredential.GetAllBranchServiceCredentials(this.BranchId.Value, this.BrokerId, ServiceCredentialService.All);
            }
            else
            {
                currentList = ServiceCredential.GetAllBrokerServiceCredentials(this.BrokerId, ServiceCredentialService.All);
            }

            ClientScript.RegisterHiddenField("credentialList", ObsoleteSerializationHelper.JsonSerialize(currentList));
            ClientScript.RegisterHiddenField("autoclose", "true");
        }

        /// <summary>
        /// Compiles a list of the CRAs which use account IDs for authentication.
        /// </summary>
        /// <param name="brokerId">The ID of the broker to retrieve available CRAs for.</param>
        /// <returns>A list of CRAs using account IDs.</returns>
        private List<Guid> GetAccountIdCras(Guid brokerId)
        {
            var accountIdCras = new List<Guid>();
            foreach (var cra in ServiceCredential.GetAvailableCreditVendorsForServiceCredentials(this.editingContext))
            {
                if (cra.Protocol == CreditReportProtocol.MISMO_2_1
                    || cra.Protocol == CreditReportProtocol.MISMO_2_3
                    || cra.Protocol == CreditReportProtocol.KrollFactualData
                    || cra.Protocol == CreditReportProtocol.InfoNetwork
                    || cra.Protocol == CreditReportProtocol.SharperLending
                    || cra.Protocol == CreditReportProtocol.CBC
                    || cra.Protocol == CreditReportProtocol.InformativeResearch)
                {
                    accountIdCras.Add(cra.ID);
                }
            }

            return accountIdCras;
        }

        /// <summary>
        /// Pulls the data needed for the title quote lender services.
        /// </summary>
        /// <returns>Dictionary mapping lender service to needed data. Item1 is lender service name. Item2 is uses account id. Item3 is requires account id.</returns>
        private Dictionary<int, OptionInfo> GetTitleQuoteLenderServiceData()
        {
            Dictionary<int, OptionInfo> titleQuoteLenderServiceData = new Dictionary<int, OptionInfo>();
            IEnumerable<TitleLenderService> lenderServices = TitleLenderService.GetLenderServicesByBrokerId(this.BrokerId);
            if (this.EmployeeId.HasValue)
            {
                lenderServices = TitleLenderService.FilterForPrincipal(lenderServices, this.Principal, false, true, true);
            }

            var vendors = TitleVendor.LoadTitleVendors_Light().ToDictionary(v => v.VendorId);
            foreach (var lenderService in lenderServices)
            {
                LightTitleVendor vendor;
                if (!vendors.TryGetValue(lenderService.VendorId, out vendor))
                {
                    continue;
                }

                var data = new OptionInfo()
                {
                    Name = lenderService.DisplayName,
                    UsesAcctId = vendor.QuotingPlatformUsesAccountId.Value,
                    RequiresAcctId = vendor.QuotingPlatformRequiresAccountId.Value
                };

                titleQuoteLenderServiceData.Add(lenderService.LenderServiceId, data);
            }

            return titleQuoteLenderServiceData;
        }

        /// <summary>
        /// Determines whether the given credential should use the account ID field.
        /// </summary>
        /// <param name="credential">The credential to check.</param>
        /// <returns>Whether or not the credential uses an account ID.</returns>
        private bool CredentialUsesAccountId(ServiceCredential credential)
        {
            var verificationsUsesAcctId = credential.IsForVerifications && ServiceCredential.GetAvailableVoxVendorsForServiceCredentials(this.editingContext)[credential.VoxVendorId.Value].UsesAccountId;
            var creditReportUsesAcctId = credential.IsForCreditReports && this.GetAccountIdCras(this.BrokerId).Contains(credential.ServiceProviderId ?? Guid.Empty);
            var titleQuoteUsesAcctId = credential.IsForTitleQuotes && credential.TitleQuoteLenderServiceId.HasValue
                && this.titleQuoteLenderServiceData.Value.ContainsKey(credential.TitleQuoteLenderServiceId.Value)
                && this.titleQuoteLenderServiceData.Value[credential.TitleQuoteLenderServiceId.Value].UsesAcctId;

            return verificationsUsesAcctId || creditReportUsesAcctId || titleQuoteUsesAcctId;
        }

        /// <summary>
        /// Sets the DropDownList value to a given value.
        /// </summary>
        /// <typeparam name="T">The type of backing value for the dropdown's option values.</typeparam>
        /// <param name="ddl">The dropdown to set the value of.</param>
        /// <param name="valueToSet">The value to set the dropdown value to. If null, this method does nothing.</param>
        /// <param name="nameToAdd">The name to add to the dropdown if no matching value already exists.</param>
        private void SetDropDownValue<T>(DropDownList ddl, T? valueToSet, string nameToAdd = null) where T : struct
        {
            if (valueToSet.HasValue)
            {
                ListItem foundDdlValue = ddl.Items.FindByValue(valueToSet.Value.ToString());
                if (foundDdlValue == null && nameToAdd != null)
                {
                    ddl.Items.Add(new ListItem(nameToAdd, valueToSet.ToString()));
                }
                else if (foundDdlValue == null && nameToAdd == null)
                {
                    return;
                }

                Tools.SetDropDownListValue(ddl, valueToSet.Value.ToString());
            }
        }

        /// <summary>
        /// Show the UI in the delete confirmation state.
        /// </summary>
        private void RenderDelete()
        {
            var credential = ServiceCredential.Load(this.Id.Value, this.BrokerId);
            this.MainEditTable.Visible = false;
            this.UserMessage.Text = $"Are you sure you want to delete {credential.ServiceProviderDisplayName}:{credential.UserName}?";
            this.SaveBtn.Text = "Yes";
            this.CancelBtn.Value = "No";
        }

        /// <summary>
        /// Bind the CRA list.
        /// </summary>
        private void BindCraList()
        {
            this.CraList.Items.Add(new ListItem(string.Empty, Guid.Empty.ToString()));
            
            foreach (var cra in ServiceCredential.GetAvailableCreditVendorsForServiceCredentials(this.editingContext))
            {
                if (!cra.IsInternalOnly)
                {
                    ListItem item = new ListItem(cra.VendorName, cra.ID.ToString());
                    item.Attributes.Add("data-protocol", cra.Protocol.ToString("d"));
                    if (cra.VoxVendorId.HasValue)
                    {
                        item.Attributes.Add("data-voxVendorId", cra.VoxVendorId.Value.ToString());
                    }

                    this.CraList.Items.Add(item);
                }
            }

            this.RegisterJsObject("accountIdCras", this.GetAccountIdCras(this.BrokerId));
        }

        /// <summary>
        /// Binds the vendors list.
        /// </summary>
        private void BindVendorList()
        {
            this.VendorList.Items.Add(new ListItem(string.Empty, (-1).ToString()));

            HashSet<int> addedVendors = new HashSet<int>();
            foreach (var lightVendor in ServiceCredential.GetAvailableVoxVendorsForServiceCredentials(this.editingContext))
            {
                if (!addedVendors.Contains(lightVendor.Key))
                {
                    ListItem item = new ListItem(lightVendor.Value.CompanyName, lightVendor.Key.ToString());
                    item.Attributes.Add("data-reqAcctId", lightVendor.Value.RequiresAccountId.ToString());
                    item.Attributes.Add("data-usesAcctId", lightVendor.Value.UsesAccountId.ToString());
                    this.VendorList.Items.Add(item);
                }
            }
        }

        /// <summary>
        /// Binds the title quote lender services DDL.
        /// </summary>
        private void BindTitleQuoteLenderServices()
        {
            this.TitleQuoteLenderServices.Items.Add(new ListItem(string.Empty, "-1"));
            foreach (var lenderService in this.titleQuoteLenderServiceData.Value)
            {
                var data = lenderService.Value;
                var id = lenderService.Key;

                ListItem item = new ListItem(data.Name, id.ToString());
                item.Attributes.Add("data-reqAcctId", data.RequiresAcctId.ToString());
                item.Attributes.Add("data-usesAcctId", data.UsesAcctId.ToString());

                this.TitleQuoteLenderServices.Items.Add(item);
            }
        }

        /// <summary>
        /// Binds the AUS options DDL.
        /// </summary>
        private void BindAusOptions()
        {
            this.AusOptions.Items.Add(new ListItem(string.Empty, (-1).ToString()));
            Tools.Bind_AusOption(this.AusOptions, this.EmployeeId.HasValue);
        }

        /// <summary>
        /// Binds the UCD delivery DDL.
        /// </summary>
        private void BindUcdDeliveryOptions()
        {
            this.UcdDeliveryTarget.Items.Add(new ListItem(string.Empty, (-1).ToString()));
            Tools.Bind_UcdDeliveryTargetOption(this.UcdDeliveryTarget, this.EmployeeId.HasValue);
        }

        /// <summary>
        /// Binds digital mortgage DDL.
        /// </summary>
        private void BindDigitalMortgageOptions()
        {
            this.DigitalMortgageProviderDdl.Items.Add(new ListItem(string.Empty, (-1).ToString()));
            Tools.Bind_DigitalMortgage(this.DigitalMortgageProviderDdl);
        }

        /// <summary>
        /// Info about service credential options.
        /// </summary>
        private class OptionInfo
        {
            /// <summary>
            /// Gets or sets the name of the option.
            /// </summary>
            /// <value>The name of the option.</value>
            public string Name
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the option uses an account id.
            /// </summary>
            /// <value>Whether the option uses an account id.</value>
            public bool UsesAcctId
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the option requires an account id.
            /// </summary>
            /// <value>Whether the option requires an account id.</value>
            public bool RequiresAcctId
            {
                get;
                set;
            }
        }
    }
}