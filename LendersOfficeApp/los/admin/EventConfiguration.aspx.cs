using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{
	/// <summary>
	/// Summary description for EventConfiguration.
	/// </summary>

	public partial class EventConfiguration : LendersOffice.Common.BasePage
	{
		protected LendersOfficeApp.los.admin.NotificationReplyConfig m_EventReply;
		protected LendersOfficeApp.los.admin.NotificationReplyConfig m_AdminReply;

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingEventNotifications
                };
            }
        }

		protected BrokerUserPrincipal BrokerUser
		{
			// Access member.
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		protected Boolean IsPmlEnabled
		{
            get { return BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan); }
		}

		private String ErrorMessage
		{
			// Access member.

			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		private String CommandToDo
		{
			// Access member.

			set
			{
				ClientScript.RegisterHiddenField( "m_commandToDo" , value );
			}
		}

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Initialize this page.
			try
			{
				// Bind to the user controls with our broker.

				if( IsPostBack == false )
				{
					// Current broker contains the rules for each event
					// as we initialize the view for the first time.

					BrokerDB brokerDb = BrokerUser.BrokerDB;


					m_EventReply.DataSource = brokerDb.NotifRules;
					m_EventReply.DataBind();

					m_AdminReply.DataSource = brokerDb.NotifRules;
					m_AdminReply.DataBind();

					// 12/21/06 db OPM 8940
					if(IsPmlEnabled)
						m_EmailAddrSendFromForNotif.Text = brokerDb.EmailAddrSendFromForNotif;

					m_isDirty.Value = "0";
				}
				else
				{
					// We're posting back, so mark as dirty.
					m_isDirty.Value = "Dirty";
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to load web form." , e );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

            m_RegularExpressionValidator1.ValidationExpression = ConstApp.EmailValidationExpression;
            m_RegularExpressionValidator1.Text = ErrorMessages.InvalidEmailAddress;

		}
		#endregion

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void ApplyClick( object sender , System.EventArgs a )
		{
			// Save the current state.
			try
			{
				// Bind each config and save.
				//
				// 8/16/2005 kb - Updated save to go direct to
				// the db.  Our current update is colliding with
				// view updating; large employee counts makes
				// this approach prohibitive.

				BrokerDB brokerDb = BrokerUser.BrokerDB;


				m_EventReply.DataSource = brokerDb.NotifRules;
				m_EventReply.DataSave();

				m_AdminReply.DataSource = brokerDb.NotifRules;
				m_AdminReply.DataSave();
				
				// 12/21/06 db OPM 8940
				if(IsPmlEnabled)
					brokerDb.EmailAddrSendFromForNotif = m_EmailAddrSendFromForNotif.Text;

                SqlParameter[] parameters = {
                                                new SqlParameter( "@BrokerId", BrokerUser.BrokerId)
					, new SqlParameter( "@NotifRuleXmlContent" , brokerDb.NotifRules.ToString() )
                                            };

				StoredProcedureHelper.ExecuteNonQuery(BrokerUser.BrokerId,  "UpdateBrokerNotifRule", 3, parameters);


                if (brokerDb.Save() == false)
                {
                    throw new CBaseException(ErrorMessages.FailedToSaveBroker, ErrorMessages.FailedToSaveBroker);
                }
                m_isDirty.Value = "0";
			}
			catch( Exception e )
			{
				// Oops!
				
				Tools.LogError( ErrorMessage = "Failed to save configuration." , e );
			}
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void OkClick( object sender , System.EventArgs a )
		{
			// Save the current state.

			try
			{
				// Apply the changes and close.

				ApplyClick( sender , a );

				CommandToDo = "Close";
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to save configuration." , e );
			}
		}

	}

}
