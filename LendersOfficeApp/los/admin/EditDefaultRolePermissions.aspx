<%@ Page language="c#" Codebehind="EditDefaultRolePermissions.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.EditDefaultRolePermissions" %>
<%@ Register TagPrefix="uc" TagName="DefaultRolePermissionsEdit" Src="../../los/admin/DefaultRolePermissionsEdit.ascx" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
	<title>Edit Default Role Permissions</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body MS_POSITIONING="FlowLayout" style="BACKGROUND-COLOR: #003366">
	<script>
		function _init()
		{
			var errorMessage = document.getElementById("m_errorMessage");
			if( errorMessage != null )
				alert( errorMessage.value );
		}
	</script>
	<form id="EditDefaultRolePermissions" method="post" runat="server" class="expand-w-content">
		<uc:DefaultRolePermissionsEdit id="m_Edit" runat="server"/>
		<uc:CModalDlg runat="server" ID="Cmodaldlg1"/>
	</form>
</body>
</html>
