﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Security;

    /// <summary>
    /// Edit for lender level service.
    /// </summary>
    public partial class LenderServicesEdit : LendersOffice.Common.BaseServicePage
    {
        /// <summary>
        /// Map of all vendor ids to the vendor.
        /// </summary>
        private Dictionary<int, LightVOXVendor> vendorsWithServices;

        /// <summary>
        /// Gets the permission required to load this page.
        /// </summary>
        /// <value>The permission required for page load.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Gets a value indicating whether the broker is configured to use VOX.
        /// </summary>
        /// <value>Whether the broker is configured to use VOX.</value>
        protected bool IsEnableVOXConfiguration
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerDB.IsEnableVOXConfiguration;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we're creating a new service in this editor.
        /// </summary>
        /// <value>Whether we're creating a new service in this editor.</value>
        private bool IsNewVendor
        {
            get
            {
                return this.ServiceId == 0;
            }
        }

        /// <summary>
        /// Gets the service id passed in the query string.
        /// </summary>
        /// <value>The service id passed in the query string.</value>
        private int ServiceId
        {
            get
            {
                return RequestHelper.GetInt("sId", 0);
            }
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsEnableVOXConfiguration)
            {
                return;
            }

            this.vendorsWithServices = VOXVendor.LoadLightVendorsWithServices();
            var serviceTypesEnabled = VOXLenderService.GetValidVendorServiceTypes(this.vendorsWithServices.Values) ?? (VOXServiceT[])Enum.GetValues(typeof(VOXServiceT));
            Tools.Bind_VendorServiceT(this.ServiceType, serviceTypesEnabled);
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (!this.IsNewVendor)
            {
                VOXLenderService service = VOXLenderService.GetLenderServiceById(principal.BrokerId, this.ServiceId);
                Tools.SetDropDownListValue(this.ServiceType, service.ServiceType);

                var vendorsForDDL = VOXLenderService.FilterValidVendorsByServiceT(this.vendorsWithServices.Values, service.ServiceType);
                this.BindVendors(vendorsForDDL);
                Tools.SetDropDownListValue(this.AssociatedVendorId, service.AssociatedVendorId);

                if (this.vendorsWithServices.ContainsKey(service.AssociatedVendorId))
                {
                    bool isDirect;
                    var resellersForDDL = VOXLenderService.FilterValidResellersByVendorAndServiceT(this.vendorsWithServices.Values, service.ServiceType, this.vendorsWithServices[service.AssociatedVendorId], out isDirect);
                    this.BindReseller(resellersForDDL, isDirect);
                    Tools.SetDropDownListValue(this.AssociatedResellerId, service.AssociatedResellerId);
                }

                this.DisplayName.Value = service.DisplayName;
                this.DisplayNameLckd.Checked = service.DisplayNameLckd;
                this.AllowPaymentByCreditCard.Checked = service.AllowPaymentByCreditCard;
                this.EnabledForLqbUsers.Checked = service.EnabledForLqbUsers;
                Tools.SetDropDownListValue(this.EnabledEmployeeGroupId, service.EnabledEmployeeGroupId.ToString());
                this.EnabledForTpoUsers.Checked = service.EnabledForTpoUsers;
                Tools.SetDropDownListValue(this.EnabledOCGroupId, service.EnabledOCGroupId.ToString());
                this.LenderServiceId.Value = service.LenderServiceId.ToString();
                this.VoeUseSpecificEmployerRecordSearch.Checked = service.VoeUseSpecificEmployerRecordSearch;
                this.VoeEnforceUseSpecificEmployerRecordSearch.SelectedValue = (service.VoeEnforceUseSpecificEmployerRecordSearch ? 1 : 0).ToString();
                this.VoaAllowImportingAssets.Checked = service.VoaAllowImportingAssets;
                this.VoaAllowRequestWithoutAssets.Checked = service.VoaAllowRequestWithoutAssets;
                this.VoaEnforceAllowRequestWithoutAssets.SelectedValue = (service.VoaEnforceAllowRequestWithoutAssets ? 1 : 0).ToString();
                this.VoeAllowRequestWithoutEmployment.Checked = service.VoeAllowRequestWithoutEmployment;
                this.VoeEnforceRequestsWithoutEmployment.SelectedValue = (service.VoeEnforceRequestsWithoutEmployment ? 1 : 0).ToString();
                this.CanBeUsedForCredentialsInTpo.Checked = service.CanBeUsedForCredentialsInTpo;
            }
            else
            {
                VOXServiceT? firstServiceT = serviceTypesEnabled?.FirstOrDefault();
                if (firstServiceT.HasValue)
                {
                    Tools.SetDropDownListValue(this.ServiceType, firstServiceT.Value);
                    var vendorsForDDL = VOXLenderService.FilterValidVendorsByServiceT(this.vendorsWithServices.Values, firstServiceT.Value);
                    this.BindVendors(vendorsForDDL);
                }
            }

            this.IsNew.Checked = this.IsNewVendor;
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            if (!this.IsEnableVOXConfiguration)
            {
                return;
            }

            this.RegisterService("edit", "/los/admin/LenderServicesEditService.aspx");

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            var employeeGroups = GroupDB.GetAllGroupIdsAndNames(principal.BrokerId, GroupType.Employee);
            this.EnabledEmployeeGroupId.Items.Add(new ListItem("Display for all employees", VOXLenderService.AllGroupsId.ToString()));
            foreach (var employeeGroup in employeeGroups)
            {
                this.EnabledEmployeeGroupId.Items.Add(new ListItem(employeeGroup.Item2, employeeGroup.Item1.ToString()));
            }

            var originatingCompGroups = GroupDB.GetAllGroupIdsAndNames(principal.BrokerId, GroupType.PmlBroker);
            this.EnabledOCGroupId.Items.Add(new ListItem("Display for all OCs", VOXLenderService.AllGroupsId.ToString()));
            foreach (var originatingCompGroup in originatingCompGroups)
            {
                this.EnabledOCGroupId.Items.Add(new ListItem(originatingCompGroup.Item2, originatingCompGroup.Item1.ToString()));
            }
        }

        /// <summary>
        /// Binds the vendors DDL based on the passed in list.
        /// </summary>
        /// <param name="vendorsToList">The vendors to populate.</param>
        private void BindVendors(IEnumerable<LightVOXVendor> vendorsToList)
        {
            this.AssociatedVendorId.Items.Add(new ListItem(string.Empty, string.Empty));
            foreach (var vendor in vendorsToList)
            {
                ListItem option = new ListItem(vendor.CompanyName, vendor.VendorId.ToString());
                option.Attributes.Add("data-canPayCC", vendor.CanPayWithCreditCard.ToString());
                this.AssociatedVendorId.Items.Add(option);
            }
        }

        /// <summary>
        /// Binds the reseller DDL.
        /// </summary>
        /// <param name="resellersToList">The resellers to populate.</param>
        /// <param name="isDirect">Whether this is a direct vendor for the service.</param>
        private void BindReseller(IEnumerable<LightVOXVendor> resellersToList, bool isDirect)
        {
            if (resellersToList == null)
            {
                return;
            }

            this.AssociatedResellerId.Items.Add(new ListItem(string.Empty, string.Empty));
            if (isDirect)
            {
                this.AssociatedResellerId.Items.Add(new ListItem("None/Direct Vendor", VOXLenderService.NoneDirectVendorId.ToString()));
            }

            foreach (var reseller in resellersToList)
            {
                ListItem option = new ListItem(reseller.CompanyName, reseller.VendorId.ToString());
                option.Attributes.Add("data-canPayCC", reseller.CanPayWithCreditCard.ToString());
                this.AssociatedResellerId.Items.Add(option);
            }
        }
    }
}