﻿#region Generated Code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Loads the data for the page.
    /// </summary>
    public partial class ShowOriginatingCompaniesAssociatedWithBranch : LendersOffice.Common.BasePage
    {
        /// <summary>
        /// Gets the required permissions for viewing the page.  If the user does not have those permissions,
        /// they will not have access to this page.
        /// </summary>
        /// <value>A list of required permissions.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingBranches
                };
            }
        }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        /// <value>The broker id.</value>
        private Guid BrokerID
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
        }

        /// <summary>
        /// Gets the Branch id.
        /// </summary>
        /// <value>The broker id.</value>
        private Guid BranchId
        {
            get { return RequestHelper.GetGuid("branchid"); }
        }

        /// <summary>
        /// Gets the branch name.
        /// </summary>
        /// <value>The branch name id.</value>
        private string BranchName
        {
            get { return RequestHelper.GetSafeQueryString("branchName"); }
        }

        /// <summary>
        /// Gets or sets the compatibility mode for this page.
        /// </summary>
        /// <returns>The compatibility mode.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindDataGrid();
            this.BranchNameContainer.InnerText = this.BranchName;
        }

        /// <summary>
        /// Binds the values to the correct controls.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arguments.</param>
        protected void OriginatingCompanyRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var companyName = e.Item.FindControl("CompanyName") as Label;
                var companyId = e.Item.FindControl("CompanyId") as Label;
                var data = e.Item.DataItem as DataRowView;

                companyId.Text = data.Row["CompanyId"] as string;
                companyName.Text = data.Row["Name"] as string;
            }
        }

        /// <summary>
        /// Retrieves the values from the database, and stores it in the repeater's datasource.
        /// </summary>
        private void BindDataGrid()
        {
            DataSet ds = new DataSet();
            SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerId", this.BrokerID),
                    new SqlParameter("@BranchId", this.BranchId)
                };

            DataSetHelper.Fill(ds, this.BrokerID, "ListOriginatingCompaniesAssociatedWithBranchId", parameters);
            OriginatingCompanyRepeater.DataSource = ds.Tables[0].DefaultView;
            OriginatingCompanyRepeater.DataBind();
        }
    }
}