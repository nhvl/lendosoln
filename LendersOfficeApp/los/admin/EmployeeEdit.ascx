<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EmployeeEdit.ascx.cs" Inherits="LendersOfficeApp.los.admin.EmployeeEdit" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.Security"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc" TagName="EmployeePermissionsList" Src="../../los/admin/EmployeePermissionsList.ascx" %>
<%@ Register TagPrefix="uc" TagName="LendingLicenses" Src="../../los/BrokerAdmin/LendingLicensesPanel.ascx" %>
<%@ Register TagPrefix="uc" TagName="DUDOSearch" Src="../BrokerAdmin/DoDuLoginNameSearch.ascx" %>
<%@ Register TagPrefix="uc" TagName="EmployeeTeams" Src="../../los/admin/EmployeeTeams.ascx" %>

<asp:HiddenField runat="server" ID="CustomLOPPFieldSettings" />
<asp:HiddenField runat="server" ID="SerializedEmployeeInfo" />
<asp:HiddenField runat="server" ID="OriginalSupportEmail" />

<style type="text/css" >
    #columnA {width:435px;}
	#columnB {margin-left: 5px;}
	.column0 { width:435px;}
	.column1 { margin-left: 5px;}
	.padleft1 { padding-left: 5px;}
	.padleft2 {padding-left: 25px;}
	.heading  {margin-top: 10px;padding-bottom: 5px;margin-bottom: 5px;BORDER-BOTTOM: lightgrey 2px solid; }
	.padbottom{padding-bottom: 7px;}
	.clear {clear: both;}
	.left {float: left;}
	.right {float:right;}
	.ipinput {width: 110px;}
	.modalmsg { border: black 3px solid; padding: 5px; display: none; position:absolute; background-color: whitesmoke; }
	.note {font-size: 8pt; color: dimgray;}
	.modalmsg { border: black 3px solid; padding: 5px; display: none; position:absolute; background-color: whitesmoke; }
	.modalbox { height: 100px; border:3px inset black; top: 200px;  margin-left: 32px; left: 0; right: 0;  width: 675px; display: none; position: absolute; background-color : whitesmoke;}
	.warning {background-color: white;border: 1px solid black;color : red; font-weight: bold; padding: 0 5px 0 5px; font-size: .9em;}

    .td-limiter{
        width: 650px;
    }

	span.Action
	{
	    text-decoration: underline;
	    color: Blue;
	    cursor: pointer;
	}
	#SelectPrintGroups
	{
	    position: relative;
	    left: 20px;
	}
	.CustomLOPPFieldLabel
	{
	    display: inline-block;
	    padding-right: 20px;
	    font-weight: normal !important;
	    font-size: 12px !important;
	}
	.tab_label { padding: 6px; border: 2px groove; color: whitesmoke; background-color:darkgrey;}
	.tab_selected { padding: 6px; border-bottom:0px; color:black; background-color:gainsboro;}
	td.relationship-td
	{
	    white-space: nowrap;
	    vertical-align: top;
	    text-align: left;
	    width:380px;
	}
</style>

<script type="text/javascript" src="../../inc/utilities.js"></script>

<script type="text/javascript">
    var IP = {
	    ip_input_id  : 'ip_input',
	    ip_sel_id    : '<%= AspxTools.ClientId(m_ip) %>',
	    ip_add_btn_id: 'ip_add',
	    ip_rem_btn_id: 'ip_remove',
	    ip_hid_id	 : '<%= AspxTools.ClientId(m_ip_parsed) %>',

	    Filter : function (IsOn)
	    {
		    if ( IsOn ) this.Clear();
		    document.getElementById(this.ip_input_id).disabled = IsOn;
		    document.getElementById(this.ip_input_id).className = IsOn ? "disabled ipinput" : "ipinput";
		    document.getElementById(this.ip_sel_id).disabled = IsOn;
		    document.getElementById(this.ip_sel_id).className = IsOn ? "disabled ipinput" : "ipinput";
		    document.getElementById(this.ip_add_btn_id).disabled = IsOn;
		    document.getElementById(this.ip_rem_btn_id).disabled = IsOn;
	    },
	    Remove  : function()
	    {
		    var from = document.getElementById(this.ip_sel_id);

		    for ( var i = (from.options.length-1); i >= 0; i-- )
		    {
			    var o = from.options[i];

			    if ( o.selected )
			    {
				    from.options[i] = null;
			    }
		    }
		    this.UpdateHiddenField();
	    },
	    CheckInputKey : function(event)
	    {

		    if ( event.keyCode == 13 )
		    {
			    document.getElementById(this.ip_add_btn_id).click();
			    return false;
		    }
		    return true;
	    },
	    Clear : function()
	    {
		    var from = document.getElementById(this.ip_sel_id);
		    for ( var i = (from.options.length-1); i >= 0; i-- )
		    {
			    from.options[i] = null;
		    }
		    this.UpdateHiddenField();
	    },
	    ShowWarning : function()
	    {
		    alert(<%=AspxTools.JsString(JsMessages.InvalidIpFormat)%>);
	    },
	    Add : function(id)
	    {
		    var val = document.getElementById(id);
		    var from = document.getElementById(this.ip_sel_id);
		    <%-- I tried Reg ex and could not figure out why 127..1.1.1.1 was considered an ip due to lack of time i did the following --%>
		    var segments = val.value.split('.');
		    if ( segments.length != 4 )
		    {
			    this.ShowWarning();
			    return false;
		    }
		    var num;
		    for( var x = 0; x < 4; x++ )
		    {
			    num = parseInt(segments[x]);
			    if (  isNaN(num) || num< 0 || num > 255  )
			    {
				    this.ShowWarning();
				    return false;
			    }
		    }

		    if ( document.getElementById(this.ip_hid_id).value.length  + val.value.length > 200 )
		    {
			    alert(<%= AspxTools.JsString(JsMessages.MaxIpCount) %> );
			    return false;
		    }

		    for ( var i = 0; i < from.options.length; i++ )
		    {
			    if ( from.options[i].text == val.value )
			    {
				    alert( <%= AspxTools.JsString(JsMessages.DuplicateIp) %>);
				    return false;
			    }
		    }
		    from.options[from.options.length] = new Option( val.value );
		    val.value = '';
		    this.UpdateHiddenField();
	    },
	    UpdateHiddenField : function ()
	    {
		    var from = document.getElementById(this.ip_sel_id);
		    var hid = document.getElementById(this.ip_hid_id);
		    hid.value = '';
		    for( var x = 0; x < from.options.length; x++ )
		    {
			    hid.value += from.options[x].text + ';';
		    }

	    }
    }

    function f_toggleAppraisalVendorPanel() {
        var bUserHasAppraisalVendorPermission = IsChecked(<%= AspxTools.JsString(Permission.AllowOrderingGlobalDMSAppraisals.ToString()) %>);
		$('#AppraisalVendorPanel').toggle(bUserHasAppraisalVendorPermission);
    }
    function f_toggleDocumentVendor(cb) {
        var inputFields = $(cb).closest('div.DocumentVendorEnable').next('div.DocumentVendorLogin').find('input');
        if(cb.checked)
        {
            inputFields.removeAttr('disabled');
            inputFields.css('background-color', '');
        }
        else
        {
            inputFields.attr('disabled', 'disabled');
            inputFields.css('background-color', 'gainsboro');
        }
    }

    function checkEnableCustomPricingPolicyControls() {
        var disabled = $(".CustomPricingPolicyFieldValueSource").val() === "0";

        $("input.CustomPMLField").attr("disabled",disabled).attr("readOnly",disabled);
        $("select.CustomPMLField").attr("disabled",disabled);
    }

    function onPricingPolicyLevelChange() {
        checkEnableCustomPricingPolicyControls();

        if($(".CustomPricingPolicyFieldValueSource").val() === "0") {
            populateBranchCustomPricingPolicy();
        } else {
            // Clear all of the field values.
            $('.CustomPMLField').val('');
        }
    }

    function populateBranchCustomPricingPolicy() {
        var sourceIsBranch = $(".CustomPricingPolicyFieldValueSource").val() === "0";
        var branchID = <%= AspxTools.JsGetElementById(m_Branch) %>.value;
        if (!sourceIsBranch || branchID === '00000000-0000-0000-0000-000000000000') {
            return;
        }

        var data = {
            'branchId': branchID,
            'brokerId': <%= AspxTools.JsString(DataBroker) %>
        };
        var result = gService.utils.call('GetBranchCustomPricingPolicyData', data);

        if (!result.error) {
            $('.CustomPricingPolicyField1').val(result.value['CustomPricingPolicyField1']);
            $('.CustomPricingPolicyField2').val(result.value['CustomPricingPolicyField2']);
            $('.CustomPricingPolicyField3').val(result.value['CustomPricingPolicyField3']);
            $('.CustomPricingPolicyField4').val(result.value['CustomPricingPolicyField4']);
            $('.CustomPricingPolicyField5').val(result.value['CustomPricingPolicyField5']);
        } else {
            var err = result.UserMessage || 'Error retrieving branch settings.';
            alert(err);
        }
    }

    function prepareCustomLOPricingPolicyInfo() {
        // Iterate over the table, and get the values field ids and values for each.
        var customFields = [];
        $('.CustomLOPricingPolicyTable :input').each(function() {
            $this = $(this);
            customFields.push({ Id: $this.data('NonMangledId'), Value: $(this).val() });
        });
        $('#<%= AspxTools.ClientId(CustomLOPPFieldSettings) %>').val(JSON.stringify(customFields));
    }

    function calcNameOnLoanDoc()
    {
        var nameOnLDocCheckbox = document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDocLock) %>);
        if(!nameOnLDocCheckbox.checked)
        {
            var fName = document.getElementById(<%= AspxTools.JsGetClientIdString(m_FirstName) %>).value + " ";

            var mName = document.getElementById(<%= AspxTools.JsGetClientIdString(m_MiddleName) %>).value;
            if(mName != "")
                mName += " ";

            var lName = document.getElementById(<%= AspxTools.JsGetClientIdString(m_LastName) %>).value;
            var suffix = document.getElementById(<%= AspxTools.JsGetClientIdString(m_Suffix) %>).value;
            if(suffix != "")
                lName += ", ";

            var nameOnLDoc = fName+mName+lName+suffix;

            document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDoc) %>).value = nameOnLDoc;
        }
    }

    function lockNameOnLoanDoc(el)
    {
        var nameOnLoanDoc = document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDoc) %>);
        nameOnLoanDoc.readOnly = !el.checked;

        if(!el.checked)
            calcNameOnLoanDoc();
    }

    $(document).ready(function () {
        IP.Filter(<%= AspxTools.JsBool(m_ipRestrictionOff.Checked) %>);
        f_toggleAppraisalVendorPanel();
        $('#RoleDescriptions tr').each(function(i, row) {
            var color = i % 2 == 0 ? 'aliceblue' : 'lightblue';
            $(this).css('background-color', color);
        });

        var serializedCustomFields = $('#<%= AspxTools.ClientId(CustomLOPPFieldSettings) %>').val();
        if (serializedCustomFields !== '') {
            var customFieldInfo = $.parseJSON(serializedCustomFields);
            for (var i = 0; i < customFieldInfo.length; i++) {
                var field = customFieldInfo[i];
                $('.' + field.Id).val(field.Value);
                $('.' + field.Id).data('NonMangledId', field.Id);
            }
        }

        var enableAuthCodeSmsCheckbox = (<%=AspxTools.JsGetElementById(EnableAuthCodeViaSms) %>);
		if(enableAuthCodeSmsCheckbox != null)
        {
		    toggleCellRequired(enableAuthCodeSmsCheckbox);

        $(enableAuthCodeSmsCheckbox).change(function(){
            toggleCellRequired(this);
        });
    }

		lockNameOnLoanDoc(document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDocLock) %>));

        var employeeId = $("#EmployeeId").val();
        if($('#IsNewEmployee').val() == 'true') {
            $('#ServiceCredentialsNewEmpDiv').show();
            $('#ServiceCredentialDiv').hide();
        }
        else {
            $('#ServiceCredentialsNewEmpDiv').hide();
            $('#ServiceCredentialDiv').show();
            var serviceCredentialData = {
                BranchId: null,
                EmployeeId: employeeId,
                EmployeeType: <%= AspxTools.JsString(userType.ToString("D")) %>,<% if (this.IsInternal) { %>
                UserId: <%= AspxTools.JsString(m_UserIdG) %>,
                BrokerId: <%= AspxTools.JsString(m_BrokerId) %>,<% } %>
                ServiceCredentialJsonId: "ServiceCredentialsJson",
                ServiceCredentialTableId: "ServiceCredentialTable"
            };
            ServiceCredential.Initialize(serviceCredentialData);
            ServiceCredential.RenderCredentials();
        }

        $('#AddServiceCredentialBtn').click(function() {
            ServiceCredential.AddServiceCredential();
        });
    });

    function toggleCellRequired(elem)
    {
        if(elem.checked)
		    {
		        $(<%=AspxTools.JsGetElementById(CellPhoneRequiredImg) %>).css("display", "");
		    }
		    else
		    {
		        $(<%=AspxTools.JsGetElementById(CellPhoneRequiredImg) %>).css("display", "none");
		    }
    }
</script>

<table height="99%" cellspacing="2" cellpadding="3" width="100%" border="0">
	<tr vAlign=top>

        <td style="PADDING-RIGHT: 6px; PADDING-LEFT: 6px; PADDING-BOTTOM: 0px; PADDING-TOP: 6px" height="100%">
			<table cellSpacing=0 cellPadding=0>
				<tr>
					<td class="FieldLabel tab_label" id="TabA" onclick="onTabClick( this );" nowrap="nowrap">Employee Information</td>
					<td style="BORDER-BOTTOM: 2px groove">&nbsp;</td>
					<td class="FieldLabel tab_label" id="TabA1" onclick="onTabClick( this );" nowrap="nowrap">Originator Compensation</td>
					<td style="BORDER-BOTTOM: 2px groove">&nbsp;</td>
					<td class="FieldLabel tab_label" id=TabA2  onclick="onTabClick( this );" nowrap="nowrap">Credentials</td>
                    <td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
                    <td class="FieldLabel tab_label" id="TabS" onclick="onTabClick( this );" nowrap="nowrap">Services</td>
					<td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
					<td class="FieldLabel tab_label" id=TabB  onclick="onTabClick( this );" nowrap="nowrap">Roles</td>
					<td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
					<td class="FieldLabel tab_label" id=TabC  onclick="onTabClick( this );" nowrap="nowrap">Relationships</td>
					<td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
					<td class="FieldLabel tab_label" id=TabD  onclick="onTabClick( this );" nowrap="nowrap">Permissions</td>
					<td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
					<td class="FieldLabel tab_label" id=TabE  onclick="onTabClick( this );" nowrap="nowrap">Licenses and IDs</td>
					<td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
					<td class="FieldLabel tab_label" id="TabF" onclick="onTabClick( this );" nowrap="nowrap">Teams</td>
					<td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
					<td class="FieldLabel tab_label" id="TabG" onclick="onTabClick( this );" nowrap="nowrap">System Access</td>
					<td style="BORDER-BOTTOM: 2px groove" width="100%" >&nbsp; </td>
					<% if( IsInternal ) { %>
						<td class="FieldLabel tab_label" id=TabI  onclick="onTabClick( this );" noWrap>Internal Administration </td>
					<% } %>
				</tr>
			</table>
			<div style="BORDER-RIGHT: 2px groove; BORDER-TOP: 0px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; BORDER-LEFT: 2px groove; PADDING-TOP: 8px; BORDER-BOTTOM: 2px groove; HEIGHT: auto; BACKGROUND-COLOR: gainsboro" >
			<div id=BaseA style="DISPLAY: none">
				<table cellSpacing=0 cellPadding=0 style="PADDING-BOTTOM: 3px">
					<tr vAlign=top>
						<td noWrap>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px" >Employee Details &nbsp;&nbsp;&nbsp;
							</div>
							<div style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 0px; WIDTH: auto; PADDING-TOP: 6px; HEIGHT: auto" >
								<table cellSpacing=0 cellPadding=0>
									<tr>
										<td style="WIDTH: 97px" noWrap width=131>First Name </td>
										<td noWrap width=527>
											<asp:textbox id=m_FirstName onchange="calcNameOnLoanDoc();" onkeyup="ClearErrorMessage('FirstName');" style="PADDING-LEFT: 4px" runat="server" MaxLength="21" />
											<IMG src="../../images/require_icon.gif" >
										</td>
									</tr>
									<tr>
									    <td>Middle Name</td>
									    <td noWrap>
									        <asp:TextBox ID=m_MiddleName onchange="calcNameOnLoanDoc();" style="padding-left: 4px;" runat="server" MaxLength="21" />
									    </td>
									</tr>
									<tr>
										<td>Last Name</td>
										<td noWrap>
											<asp:textbox id=m_LastName onchange="calcNameOnLoanDoc();" onkeyup="ClearErrorMessage('LastName');" style="PADDING-LEFT: 4px" runat="server" MaxLength="21" />
											<IMG src="../../images/require_icon.gif" >
										</td>
									</tr>
									<tr>
									    <td>Suffix</td>
									    <td noWrap>
									        <ml:ComboBox onchange="calcNameOnLoanDoc();" runat="server" ID=m_Suffix MaxLength="10" Width=50 />
									    </td>
									</tr>
									<% if (ShowPmlBroker){ %>
									<tr>
										<td>Company</td>
										<td noWrap>
											<asp:dropdownlist id="m_pmlBroker" style="PADDING-LEFT: 4px" runat="server" Width="145px"></asp:dropdownlist>
											<asp:RequiredFieldValidator id="m_pmlBrokerValidator" ErrorMessage="*" Display="Dynamic" ControlToValidate="m_pmlBroker" InitialValue="00000000-0000-0000-0000-000000000000" Runat="server"> </asp:RequiredFieldValidator>
										</td>
									</tr>
									<% } %>
									<tr>
									    <td style="width: 170px;">Name On Loan Documents</td>
									    <td noWrap>
									        <asp:TextBox ID="m_NameOnLoanDoc" runat="server" MaxLength="100" Width=250 />
									        <asp:CheckBox onclick="lockNameOnLoanDoc(this);" runat="server" ID=m_NameOnLoanDocLock Text="Lock" />
									    </td>
									</tr>
									<tr>
										<td>Branch</td>
										<td noWrap>
											<asp:dropdownlist id=m_Branch style="PADDING-LEFT: 4px" runat="server" Width="145px" onchange="onBranchChange();" />
											&nbsp;<input onclick=onBranchCopyClick(); type=button value="Copy Address">
										</td>
									</tr>
									<tr>
										<td>Address</td>
										<td noWrap>
											<asp:textbox id=m_Street style="PADDING-LEFT: 4px" runat="server" MaxLength="60" Width="260" />
											<br>
											<asp:textbox id=m_City style="PADDING-LEFT: 4px" runat="server" MaxLength="36" Width="150" />
											<ml:statedropdownlist id=m_State style="PADDING-LEFT: 4px" runat="server" Width="50" />
											<ml:zipcodetextbox id=m_Zipcode style="PADDING-LEFT: 4px" runat="server" Width="60" CssClass="mask" preset="zipcode" />
										</td>
									</tr>

									<tr>
										<td>LendingQB Notification Email</td>
										<td noWrap colspan=3 >
											<asp:textbox id=m_Email onkeyup="ClearErrorMessage('Email');" style="PADDING-LEFT: 4px" runat="server" MaxLength="80" Width="220" />
											<IMG src="../../images/require_icon.gif" >
											<asp:regularexpressionvalidator id=RegularExpressionValidator1 runat="server" ControlToValidate="m_Email" Display="Dynamic" ErrorMessage="RegularExpressionValidator"></asp:regularexpressionvalidator>
										</td>
									</tr>
									<tr runat="server" id="SupportCenterRow">
									    <td>Support Center Email</td>
									    <td noWrap colspan="3">
									        <asp:TextBox runat="server" ID="m_supportEmail" ReadOnly="true" style="PADDING-LEFT: 4px" runat="server" MaxLength="80" Width="220"></asp:TextBox>
									        <ml:EncodedLabel runat="server" ID="m_SupportEmailStatus"></ml:EncodedLabel>
									    </td>
									</tr>
									<tr>
                                        <td>Phone</td>
                                        <td noWrap>
											<ml:phonetextbox id=m_Phone onkeyup="ClearErrorMessage('Phone');" style="PADDING-LEFT: 4px" runat="server" CssClass="mask" preset="phone" />
											<IMG src="../../images/require_icon.gif" >
										</td>
									</tr>
									<tr>
									    <td>Cell Phone</td>
									    <td>
                                            <ml:PhoneTextBox onkeyup="ClearErrorMessage('CellPhone');" ID="m_Cell" runat="server" style="padding-left: 4px" CssClass="mask" preset="phone" />
									        <IMG runat="server" id="CellPhoneRequiredImg" src="../../images/require_icon.gif" >
                                            <asp:CheckBox runat="server" ID="IsCellphoneForMultiFactorOnly" Text="Private: For multi-factor authentication only" />
									    </td>
									</tr>
									<tr>
                                        <td>Fax</td>
										<td noWrap>
											<ml:phonetextbox id=m_Fax style="PADDING-LEFT: 4px" runat="server" CssClass="mask" preset="phone" />
										</td>
									</tr>
									<tr>
									    <td >Employee ID</td>
										<td noWrap>
											<asp:textbox id=m_EmployeeIDInCompany style="PADDING-LEFT: 4px" runat="server" MaxLength="30" />
										</td>
									</tr>
									<asp:Panel runat="server" ID="m_StartAndTerminationDatePanel" Visible=false>
									    <tr>
									        <td>Start Date</td>
									        <td>
									            <ml:DateTextBox ID=m_EmployeeStartD style="PADDING-LEFT: 4px" runat="server" MaxLength="30" />
									        </td>
									    </tr>
									    <tr>
									        <td>Termination</td>
									        <td>
									        <ml:DateTextBox ID=m_EmployeeTerminationD style="PADDING-LEFT: 4px" runat="server" MaxLength="30" />
									        <asp:CompareValidator ID="m_cvEmployeeStartAndEndDateComparer" ControlToCompare="m_EmployeeStartD"
                                            ControlToValidate="m_EmployeeTerminationD" Type="Date" Operator="GreaterThanEqual"
                                            ErrorMessage="*Termination must come after start date" runat="server" Display="Dynamic"></asp:CompareValidator>
									        </td>
									    </tr>
									</asp:Panel>
                                    <asp:PlaceHolder ID="SignaturePlaceHolder" runat="server" Visible="false">
									<tr><td>&nbsp;</td></tr>
									<tr>
									    <td style="WIDTH: 97px; vertical-align:middle" noWrap width=127 >Signature </td>
									    <td noWrap>
									        <asp:FileUpload ID="signatureUpload" Width="320" ToolTip="Select the image to upload" runat="server" />
									        <asp:Button UseSubmitBehavior="false" Runat="server" ID="btnSignatureUpload" Height="1.6em" Width="6em" Text="Upload" onclick="btnSignatureUpload_Click" OnClientClick="prePostbackClientSideSave();"></asp:Button><br />
									         <div id="divError" runat="server" visible="false" style="color:Red; font:8pt arial bold"></div>
									        <label style="font:italic 8pt arial">(The image should be a JPEG, PNG, GIF or BMP file, smaller than 100KB, and readable at 160x18 pixels.)</label><br/>
									    </td>
									</tr>
									<tr><td>&nbsp;</td></tr>
									<tr>
									    <td>&nbsp;</td>
									    <td>
									        <div style="padding-top:0.2em; font-size:0.9em;">
									            <asp:Image ID="imgSignature" Width="160" runat="server"/>&nbsp;
									            <asp:LinkButton ID="btnRemoveImage" runat="server" Text="Remove Signature" OnClick="OnRemoveSignature_Click" OnClientClick="prePostbackClientSideSave();"></asp:LinkButton>
									        </div>
									    </td>
									</tr>
                                    </asp:PlaceHolder>
									<tr >
										<td colSpan=2>
											<br>
											<div  id="CommisionLabel" class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px" colspan="2">Commissions </div>
											<div class=FieldLabel style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; WIDTH: 285px" colspan="2"></div>
										</td>
									</tr>

									<tr>
										<td colspan="2">
											<table>
												<tr id="CommisionLoanAmountRow">
													<td style="WIDTH: 187px">
														<ml:EncodedLabel id=ptLoanAmtLabel text="Percentage of Loan Amount" Runat="server"></ml:EncodedLabel>&nbsp;&nbsp;
													</td>
													<td>
														<ml:percenttextbox id=m_CommissionPointOfLoanAmount runat="server" CssClass="mask" preset="percent" width="90px"></ml:percenttextbox>
													</td>
												</tr>
												<tr>
													<td style="WIDTH: 187px">
														<ml:EncodedLabel id=ptGrossProfitLabel text="Percentage of Gross Profit" Runat="server"></ml:EncodedLabel>&nbsp;&nbsp;
													</td>
													<td>
														<ml:percenttextbox id=m_CommissionPointOfGrossProfit runat="server" CssClass="mask" preset="percent" width="90px"></ml:percenttextbox>
													</td>
												</tr>
												<tr id="CommisionMinBaseRow">
													<td style="WIDTH: 97px">
														<ml:EncodedLabel id=minBaseLabel text="Minimum Base" Runat="server"></ml:EncodedLabel>&nbsp;&nbsp;
													</td>
													<td>
														<ml:moneytextbox id=m_CommissionMinBase runat="server" CssClass="mask" preset="money" width="90px"></ml:moneytextbox>
													</td>
												</tr>
												<tr colspan="2">
													<td style="WIDTH: 97px" colSpan=2>
													</td>
												</tr>

											</table>
										</td>
									</tr>


									<tr>
										<td colSpan=2>
											<div class=FieldLabel style="PADDING-BOTTOM: 4px; PADDING-TOP: 12px" colspan="2">Notes </div>
											<div class=FieldLabel style="BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; WIDTH: 490px" colspan="2"></div>
										</td>
									</tr>
									<tr>
										<td>
											Administrator's Notes
										</td>
										<td colspan=2>
											<asp:textbox id=m_Notes style="PADDING-LEFT: 4px" runat="server" Width="350px" TextMode="MultiLine" Height="60px"> </asp:textbox>
										</td>
									</tr>
								</table>
							</div>
						</td>
						<td noWrap width=4>&nbsp; </td>
					</tr>
				</table>
			</div>
			<div id=BaseA1 style="DISPLAY: none">
				<table cellSpacing=0 cellPadding=0 style="PADDING-BOTTOM:0px">
                    <tr style="padding-bottom:4px">
				        <td>
                            <asp:CheckBox id="m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo" runat="server" Text="Compensation only paid for the 1st lien of a combo" />
				        </td>
				    </tr>
				    <tr style="padding-bottom:4px">
				        <td>
                            <ml:percenttextbox id="m_OriginatorCompensationPercent" runat="server" width="70" preset="percent"></ml:percenttextbox>
				            of the
				            <asp:DropDownList id="m_OriginatorCompensationBaseT" runat="server"></asp:DropDownList>
				        </td>
				    </tr>
				    <tr style="padding-bottom:4px">
				        <td style="padding-left:50px">
				            <asp:CheckBox ID="m_OriginatorCompensationMinAmountEnabled" runat="server" onclick="setAmountTextboxStatus(this);" />
				            Minimum
				            <ml:moneytextbox id="m_OriginatorCompensationMinAmount" runat="server" width="90" preset="money" style="position: absolute; left: 160px;"></ml:moneytextbox>
				            <asp:Image ID="m_OriginatorCompensationMinAmountR" runat="server" ImageUrl="~/images/require_icon.gif" style="position: absolute; left: 255px;" />
						</td>
				    </tr>
				    <tr style="padding-bottom:4px">
				        <td style="padding-left:50px">
				            <asp:CheckBox ID="m_OriginatorCompensationMaxAmountEnabled" runat="server" onclick="setAmountTextboxStatus(this);" />
				            Maximum
				            <ml:moneytextbox id="m_OriginatorCompensationMaxAmount" runat="server" width="90" preset="money" style="position: absolute; left: 160px;"></ml:moneytextbox>
				            <asp:Image ID="m_OriginatorCompensationMaxAmountR" runat="server" ImageUrl="~/images/require_icon.gif" style="position: absolute; left: 255px;" />
						</td>
				    </tr>
				    <tr>
				        <td>
				            Fixed amount
                            <ml:MoneyTextBox ID="m_OriginatorCompensationFixedAmount" runat="server" preset="money" Width="90" style="position: absolute; left: 260px;"></ml:MoneyTextBox>
				        </td>
				    </tr>
				    <tr>
				        <td style="padding-top:15px">
				            Notes
				        </td>
				    </tr>
				    <tr>
				        <td>
				            <asp:TextBox ID="m_OriginatorCompensationNotes" TextMode="MultiLine" Rows="5" Width="350px" runat="server"></asp:TextBox>
				        </td>
				    </tr>
				    <tr>
				        <td style="padding-top:15px">
				            Compensation plan change history
				        </td>
				    </tr>
				    <tr>
				        <td>
			            	<ml:CommonDataGrid id="m_OriginatorCompensationAuditGrid" runat="server" AllowPaging="true" OnPageIndexChanged="m_OriginatorCompensationAuditGrid_PageIndexChanged" OnItemCreated="m_OriginatorCompensationAuditGrid_ItemCreated" EnableViewState="true" CellPadding="3">
							    <PagerStyle VerticalAlign="Middle" Mode="NumericPages" horizontalalign="right" Position="Bottom"></PagerStyle>
							    <alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
								<itemstyle cssclass="GridItem"></itemstyle>
								<headerstyle cssclass="GridHeader"></headerstyle>
							    <columns>
							        <asp:BoundColumn DataField="Date_rep" HeaderText="Date"></asp:BoundColumn>
							        <asp:BoundColumn DataField="By_rep" HeaderText="By"></asp:BoundColumn>
							        <asp:BoundColumn DataField="Notes" HeaderText="Notes"></asp:BoundColumn>
							        <asp:TemplateColumn>
										<itemtemplate>
											<a href="#" onclick="showModal('<%# AspxTools.HtmlString(m_PopupPath.ToString()) %>ViewOriginatorCompensationAuditEvent.aspx?employeeId=<%# AspxTools.HtmlString(m_EmployeeId.ToString()) %>&brokerId=<%# AspxTools.HtmlString(DataBroker.ToString()) %>&eventId=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>');">view</a>
										</itemtemplate>
									</asp:TemplateColumn>
								</columns>
							</ml:CommonDataGrid>
							<ml:EncodedLiteral ID="m_OriginatorCompensationAuditDoesntExist" Text="No entries in change history" runat="server"></ml:EncodedLiteral>
				        </td>
				    </tr>
				</table>
                <asp:PlaceHolder runat="server" ID="CustomLOPricingPolicyPlaceHolder">
    				<div class="FieldLabel heading">Loan Officer Pricing Policy</div>
    				<label>The loan officer pricing policy is set at the
    				    <asp:DropDownList runat="server" ID="CustomPricingPolicyFieldValueSource" onchange="onPricingPolicyLevelChange();" class="CustomPricingPolicyFieldValueSource"></asp:DropDownList>
        				level.
    				</label>

                    <table runat="server" id="CustomLOPricingPolicyTable" class="CustomLOPricingPolicyTable">
                    </table>
                </asp:PlaceHolder>
			</div>
			<div id=BaseA2 style="DISPLAY: none">
				<table cellSpacing=0 cellPadding=0 style="PADDING-BOTTOM:0px">
					<tr>
						<% if( IsInternal ) { %>
						<td class=FieldLabel >
						<% } else { %>
						<td style="PADDING-BOTTOM: 3px; padding-left: 4px;" width="100%">
						<% } %>
						    <div class="column0 left">
						        <div class="FieldLabel heading">Employment Status</div>
						    </div>
						</td>
					</tr>
					<tr>
						<td colspan=2 class=FieldLabel  noWrap>
						    <div class="column0 left">
							<div style="PADDING-RIGHT: 6px; PADDING-BOTTOM: 2px; WIDTH: 220px">
								<table>
									<tr>
										<td style="PADDING-TOP: 2px">
											<asp:CheckBox ID=m_IsEmployee Runat=server Text="Active Employee" onclick="onEmploymentStatusClick();" />
											<asp:panel id=m_PreventLogin style="PADDING-LEFT: 20px" runat="server" Width=200px>
												<asp:CheckBox id=m_AllowMulti runat="server" Text="Shareable by multiple users" />
												<asp:CheckBox id=m_CanLogin onclick=onAllowLoginClick(); Runat="server" Text="Allow Login" />
											</asp:panel>
										</td>
									</tr>
								</table>
							</div>
							</div>

						</td>
					</tr>
					<tr vAlign=top style="PADDING-TOP: 12px">
						<td noWrap class="td-limiter">

							<% if( IsInternal ) { %>
							<div style="PADDING-RIGHT: 6px; PADDING-LEFT: 6px; PADDING-BOTTOM: 0px; PADDING-TOP: 2px; HEIGHT: auto" >
							<% } %>
							<% else {%>
							<div style="PADDING-RIGHT: 6px; PADDING-LEFT: 6px; PADDING-BOTTOM: 0px; PADDING-TOP: 2px; HEIGHT: auto" >
							<% } %>
								    <div id="columnA" class="left">
								        <div class="FieldLabel Heading">Login Info
							            </div>

										<asp:Panel style="PADDING-TOP: 4px" ID="m_UnlockAccountPanel" Runat=server>
										    <B><ml:EncodedLabel id=m_LockedAccount runat="server" Text="This account is locked" ForeColor="Red"></ml:EncodedLabel></B>&nbsp;&nbsp;
                                            <INPUT id=m_unlockAccount style="WIDTH: 100px" onclick=onUnlockAccountClick(); type=button value=Unlock />
										</asp:Panel>

										<div class="padleft1 padbottom">
											<asp:checkbox id=m_ChangeLogin onclick=onChangeLoginClick(); runat="server" Text="Change login and password?" Visible="True"></asp:checkbox>
											<asp:panel id=m_LoginNotes style="DISPLAY:inline; VISIBILITY:hidden; FONT:11px arial; COLOR:dimgray" runat="server">
											<% if ( m_HasLogin ) { %>
											    (Leave password blank if it is not being changed.)
											<% } %>
											</asp:panel>&nbsp;
										</div>

										<div class="padleft2">
										    <asp:checkbox ID=m_IsActiveDirectoryUser onclick=onADUserClick(); runat="server" Text="Active Directory User"></asp:checkbox>
										</div>

									    <div class="padleft2">
											<span id="m_LoginLabel" runat="server" style="width: 130px">
											    Login Name
											</span>
											<asp:textbox id="m_Login" style="PADDING-LEFT: 4px" onkeyup="ClearErrorMessage('Login');" runat="server" MaxLength="36"></asp:textbox>
											<asp:Image ID="m_loginR" style="DISPLAY:none" runat="server" ImageUrl="../../images/require_icon.gif"></asp:Image>
										</div>

										<div id=PasswordDiv class="padleft2">
											<span style="width: 130px">
											    Password
											    <span id="pwRules" style="font-size: .8em;">
												    (<a onclick="Modal.ShowPopup('m_PasswordRules', 'm_closepwrules' , event);" href='#"' ?>show rules</a>)
												</span>
											</span>
											<asp:textbox id="m_Password" style="PADDING-LEFT: 4px" runat="server" TextMode="Password" MaxLength="36"></asp:textbox>
											<asp:Image ID="m_passwordR" style="display:none" runat="server" ImageUrl="../../images/require_icon.gif"></asp:Image>
										</div>

										<div id=RetypePasswordDiv class="padleft2 padbottom">
											<span style="width: 130px" noWrap >
											    Retype
											</span>
											<asp:textbox id="m_Confirm" style="PADDING-LEFT: 4px" runat="server" TextMode="Password"  MaxLength="36"></asp:textbox>
										    <asp:Image ID="m_retypeR" style="DISPLAY:none" runat="server" ImageUrl="../../images/require_icon.gif"></asp:Image>
										</div>
									<div id=PasswordControls>
										<div>
											<span style="WIDTH: 250px" noWrap>
												<br>
												<asp:radiobutton id=m_MustChangePasswordAtNextLogin onclick=expirationPolicyClick(); runat="server" Text="Must change password at next login" GroupName="Password"></asp:radiobutton>
												<br>
												<asp:radiobutton id=m_PasswordNeverExpires onclick=expirationPolicyClick(); runat="server" Text="Password never expires" GroupName="Password"></asp:radiobutton>
												<br>
												<asp:radiobutton id=m_PasswordExpiresOn onclick=expirationPolicyClick(); runat="server" Text="Password expires on:" GroupName="Password"></asp:radiobutton>
												&nbsp;&nbsp;<ml:datetextbox id=m_ExpirationDate style="PADDING-LEFT: 4px" runat="server" preset="date" width="64" HelperAlign="AbsMiddle"></ml:datetextbox>
											</span>
										</div>
										<div height="40">
											<span style="PADDING-TOP: 15px" >
												<asp:checkbox id=m_CycleExpiration onclick=expirationCycleClick(); runat="server" CssClass="FieldLabel" Text="" />
												Expire passwords every &nbsp;
												<asp:dropdownlist id=m_ExpirationPeriod runat="server">
													<asp:ListItem Value="15">15</asp:ListItem>
													<asp:ListItem Value="30">30</asp:ListItem>
													<asp:ListItem Value="45">45</asp:ListItem>
													<asp:ListItem Value="60">60</asp:ListItem>
												</asp:dropdownlist> days following update
											</span>
										</div>
									</div>
								</div>
							    <div id="GenericFrameworkVendorPanel" class="clear">
                                    <asp:Repeater ID="m_genericFrameworkVendors" runat="server" OnItemDataBound="GenericFrameworkVendorBind">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="ProviderId" />
                                            <asp:HiddenField runat="server" ID="StoredUsername" />
                                            <div class="FieldLabel heading">
						                        <ml:EncodedLabel runat="server" ID="GenericFrameworkVendorName"></ml:EncodedLabel>
						                    </div>
						                    <div>
						                        <span style="width:250px;">Username:&nbsp;<asp:TextBox ID="Username" MaxLength="100" runat="server"></asp:TextBox></span>
						                    </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
						        </div>
								<div class="clear">
                                    <asp:Panel ID="m_PreventLicensing" runat="server">
                                        <div class="FieldLabel heading">
                                            Billing
                                        </div>
                                        <div>
                                            <span style="padding-left: 5px; padding-top: 8px" nowrap="nowrap" colspan="2">
                                                License #
                                                <asp:TextBox ID="m_LicenseNumber" runat="server" Size="10" ReadOnly="True"></asp:TextBox>&nbsp;&nbsp;
                                                <asp:HyperLink ID="m_AssignLicenseLink" runat="server" NavigateUrl="javascript:onAssignLicense();">assign</asp:HyperLink>&nbsp;
                                                <asp:HyperLink ID="m_UnassignLicenseLink" runat="server" NavigateUrl="javascript:onUnassignLicense();">remove</asp:HyperLink>
                                                <input type="hidden" value=<%= AspxTools.HtmlAttribute(m_LicenseId) %> id="LicenseId"/>
                                            </span>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel runat="server" ID="ComplianceEaglePanel">
                                        <div class="FieldLabel heading">Compliance Eagle</div>
                                        <div>
                                            <span nowrap="nowrap" colspan="2">
                                            <label style="width:250px;">Username:&nbsp;<asp:TextBox ID="ComplianceEagleUserName" runat="server" /></label>
                                            <label>Password:&nbsp;<asp:TextBox ID="ComplianceEaglePassword" runat="server" TextMode="Password" /></label>
                                            </span>
                                        </div>
                                    </asp:Panel>
                                    <div id="AppraisalVendorPanel">
							            <input type="hidden" runat="server" id="m_BrokerHasGDMSEnabled" />
                                        <asp:Repeater ID="m_appraisalVendors" runat="server" OnItemDataBound="AppraisalVendorBind">
                                            <ItemTemplate>
                                                <asp:HiddenField runat="server" ID="VendorId" />
                                                <div class="FieldLabel heading">
							                        <ml:EncodedLabel runat="server" ID="VendorName"></ml:EncodedLabel> Login
							                    </div>
							                    <div>
							                        <asp:PlaceHolder id="AccountIdField" runat="server">
							                            <span style="width:250px;">
							                                Account ID:&nbsp;
							                                <asp:TextBox ID="LoginAccountId" MaxLength="50" runat="server"></asp:TextBox>
							                            </span>
                                                    </asp:PlaceHolder>
							                        <span style="width:250px;">Username:&nbsp;<asp:TextBox ID="LoginUserName" MaxLength="50" runat="server"></asp:TextBox></span>
							                        <span>Password:&nbsp;<asp:TextBox ID="LoginPassword" TextMode="Password" runat="server"></asp:TextBox></span>
							                    </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
							        </div>
							        <div id="Irs4506TVendorPanel">
							            <asp:Repeater ID="m_irs4506TVendors" runat="server"  OnItemDataBound="Irs4506TVendorBind">
							                <ItemTemplate>
							                    <asp:HiddenField runat="server" ID="VendorId" />
							                    <div class="FieldLabel heading">
							                        <ml:EncodedLabel runat="server" ID="VendorName" /> Login
							                    </div>
							                    <div>
							                        <asp:PlaceHolder id="AccountIdField" runat="server">
							                            <span style="width:250px;">
							                                Account ID:&nbsp;
							                                <asp:TextBox ID="LoginAccountId" MaxLength="50" runat="server" />
							                            </span>
                                                    </asp:PlaceHolder>
							                        <span style="width:250px;">Username:&nbsp;<asp:TextBox ID="LoginUserName" MaxLength="50" runat="server" /></span>
							                        <span>Password:&nbsp;<asp:TextBox ID="LoginPassword" TextMode="Password" runat="server" /></span>
							                    </div>
							                </ItemTemplate>
							            </asp:Repeater>

							        </div>
							        <div class="FieldLabel heading">
                                        DO/DU Login Info
                                    </div>
                                    <div style="padding-top: 4px">
                                        <div>
                                            <div id="loginProcess" style="z-index: 900" class="modalmsg" style="width:300px">
										        During the automatic login process, it would technically be possible for a malicious user to employ 3rd-party web tools to attempt to obtain the login info while it is invisibly sent to DO/DU.
										        <div style="text-align:center"> [<a href="#" id='wmsgClose' onclick="Modal.Hide()"> Close </a>]</div>
									        </div>
								        </div>
							        </div>
							        <div>
                                        <div noWrap>
                                            <div class="warning" style="padding-top: 2px; padding-bottom: 2px; margin-bottom:8px;"  >
							                    Warnings: 1. DO/DU login info may not be completely hidden from users. <a href="#" onclick="Modal.ShowPopup('loginProcess', 'wmsgClose', event)">Details</a><br/>
							                    <span style="padding-left: 58px">
							                    2. Users with shared DO/DU logins can potentially access the same loan pipeline in DO/DU. </span>
						                    </div>
						                    DO Login: <asp:textbox id="m_doLogin" onkeyup=onDoLoginTextChange(); style="PADDING-LEFT: 4px" runat="server" MaxLength="50"></asp:textbox>
						                    <span style=" width: 30px" >
						                        <img id=doerror style="display:none" src="../../images/error_icon.gif" />
						                    </span>
						                    Password: <asp:textbox id="m_doPw" onkeyup=onDoLoginTextChange(); style="PADDING-LEFT: 4px" runat="server" MaxLength="20" TextMode="Password"></asp:textbox>
						                    <% if ( !IsNewEmp ) { %>
						                    <span class="note">(leave blank if not changing)</span>
						                    <% } else { %>
						                    <asp:Image id=m_doPasswordR style="DISPLAY: none" runat="server" ImageUrl="../../images/require_icon.gif"></asp:Image>
						                    <% } %>
				                            <br />
                                            <div id="DUSection" runat="server">
                                                DU Login:
                                                <asp:textbox id="m_duLogin" onkeyup=onDuLoginTextChange(); style="PADDING-LEFT: 4px" runat="server" MaxLength="50"></asp:textbox>
				                                <span style="width: 30px" >
				                                    <img id=duerror style="display:none" src="../../images/error_icon.gif" />
				                                </span>
				                                Password: <asp:textbox id="m_duPw" onkeyup=onDuLoginTextChange(); style="PADDING-LEFT: 4px" runat="server" MaxLength="20" TextMode="Password"></asp:textbox>
				                                <% if ( !IsNewEmp ) { %>
				                                <span class="note">(leave blank if not changing)</span>
				                                <% } else { %>
				                                <asp:Image id=m_duPasswordR style="DISPLAY: none" runat="server" ImageUrl="../../images/require_icon.gif"></asp:Image>
				                                <% } %>
                                            </div>

				                            <% if (PmlAutoLoginOption == "MustBeUnique") { %>
                                            <uc:DUDOSearch runat="server" id="m_dudoSearch" HideJS="Modal.Hide();" ZIndex="901" CssClass="modalbox"></uc:DUDOSearch>
                                            <% } %>
                                        </div>
                                    </div>
                                    <asp:Repeater runat="server" ID="m_DocumentVendors" OnItemDataBound="DocumentVendorBind">
                                        <ItemTemplate>
                                            <div class="FieldLabel heading DocumentVendorEnable">
                                                <asp:HiddenField runat="server" ID="m_VendorId" />
                                                <asp:CheckBox runat="server" ID="m_Enabled" onclick="f_toggleDocumentVendor(this);"/>
                                                <ml:EncodedLabel runat="server" AssociatedControlID="m_Enabled" ID="m_EnabledLabel"></ml:EncodedLabel>
                                            </div>
                                            <div class="DocumentVendorLogin">
                                                <div noWrap>
                                                    <asp:PlaceHolder runat="server" ID="UsernamePanel">
                                                        Username: <asp:textbox id="m_Username" style="PADDING-LEFT: 4px" runat="server" CssClass="DocumentVendorField"></asp:textbox>
                                                        <span style="width: 30px" ></span>
						                            </asp:PlaceHolder>

						                            <asp:PlaceHolder runat="server" ID="PasswordPanel">
                                                        Password: <asp:textbox id="m_Password" style="PADDING-LEFT: 4px" runat="server" TextMode="Password" CssClass="DocumentVendorField"></asp:textbox>
                                                        <span style="width: 30px" ></span>
                                                    </asp:PlaceHolder>

                                                    <asp:PlaceHolder runat="server" ID="CompanyIdPanel">
                                                        Company ID: <asp:textbox id="m_CompanyId" style="PADDING-LEFT: 4px" runat="server" CssClass="DocumentVendorField"></asp:textbox>
						                                </span>
						                            </asp:PlaceHolder>
						                        </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div id=BaseB style="DISPLAY: none">
				<table cellSpacing=0 cellPadding=0>
					<tr vAlign=top style="padding-bottom: 10px;">
						<td noWrap>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px" >Roles
							<IMG src="../../images/require_icon.gif" >
							</div>
							<div style="BORDER-RIGHT: lightgrey 2px solid; PADDING-RIGHT: 8px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 200px; PADDING-TOP: 8px; BORDER-BOTTOM: lightgrey 2px solid; HEIGHT: 525px; BACKGROUND-COLOR: white; overflow-y: scroll;" >
								<asp:checkboxlist id="m_EmployeeRoles" onclick="onRoleClick(true);" runat="server" cellspacing="0" cellpadding="0" name="m_EmployeeRoles"></asp:checkboxlist>
							</div>
						</td>
						<td noWrap width=15>&nbsp;</td>
						<td noWrap>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px" >Default Role Descriptions &amp; Features </div>
							<div style="BORDER-RIGHT: 0px; PADDING-RIGHT: 4px; BORDER-TOP: lightgrey 2px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 450px; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; HEIGHT: 525px; BACKGROUND-COLOR: whitesmoke" >
								<table id="RoleDescriptions" cellSpacing=0 cellPadding=4 width="100%">
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial" >Accountant </td>
										<td style="FONT: 11px arial">Can edit restricted trust account of the loan. <br />
										Can edit the loan information in the Finance folder.
										</td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial" >Administrator </td>
										<td style="FONT: 11px arial">
											<u >Admin</u> - Setup and configure user accounts.
											<br>
											Programs - Create loan programs, closing costs templates, and pricing policies.
										</td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Call Center Agent </td>
										<td style="FONT: 11px arial">
											<u >Leads</u> - Enter lead information into a simplified lead screen.
										</td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">Closer </td>
										<td style="FONT: 11px arial">Prepares documents for closing and funding the loan. </td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Collateral Agent </td>
										<td style="FONT: 11px arial">Gathers and transmits all required documents to the warehouse bank facilitating the advance of the monies necessary for closing. </td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">Credit Auditor</td>
										<td style="FONT: 11px arial">Performs QA/QC audits with a focus on analyzing and finding possible issues in the Credit File.</td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">Disclosure Desk</td>
										<td style="FONT: 11px arial">Responsible for accessing and sending disclosures.</td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Doc Drawer </td>
										<td style="FONT: 11px arial">Prepares and draws closing loan documents. </td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Funder </td>
										<td style="FONT: 11px arial">Reviews closing package, calculates funding figures and requests monies for disbursement to the appropriate closing agent. </td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Insuring </td>
										<td style="FONT: 11px arial">Prepares and ships case binders and other such documents required for the insuring of Government loans. </td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">Junior Processor</td>
										<td style="FONT: 11px arial">
											<u>Loans</u> - Open, edit, and view loan files.
										</td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">Junior Underwriter</td>
										<td style="FONT: 11px arial">Assesses risk and rates acceptability.</td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">Legal Auditor</td>
										<td style="FONT: 11px arial">Performs QA/QC audits with a focus on analyzing and finding possible issues in documentation.</td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Lender Account Executive </td>
										<td style="FONT: 11px arial">Liaison between lender and external broker agents. </td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Loan Officer </td>
										<td style="FONT: 11px arial">
											<u >Loans</u> - Open, edit, and view loan files.
										</td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">Loan Officer Assistant</td>
										<td style="FONT: 11px arial">
											<u>Loans</u> - Open, edit, and view loan files.
										</td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Loan Opener </td>
										<td style="FONT: 11px arial">
											<u >Loans</u> - Open, edit, and view loan files.
										</td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Lock Desk </td>
										<td style="FONT: 11px arial">Supports underwriting and servicing. </td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Manager </td>
										<td style="FONT: 11px arial">
											<u>Loan Stats</u> - View the # of loans in different categories (closed, open, denied,etc.)
											<br>
											<u>Loans</u> - Open, edit, and view loan files.
											<br>
											<u>Reports</u> - Generate, edit, publish custom reports.
										</td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Post Closer </td>
										<td style="FONT: 11px arial">Responsible for obtaining and forwarding to end investors all trailing documents. </td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Processor </td>
										<td style="FONT: 11px arial">
											<u >Loans</u> - Open, edit, and view loan files.
										</td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">Purchaser</td>
										<td style="FONT: 11px arial">Prepares files for purchase from correspondent lenders.</td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">QC Compliance</td>
										<td style="FONT: 11px arial">Performs QA/QC audits.</td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Real Estate Agent </td>
										<td style="FONT: 11px arial">Can only view loan files that have been assigned to them. </td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">Secondary</td>
										<td style="FONT: 11px arial">Works to get loans sold into the secondary markets.</td>
									</tr>
									<tr vAlign="baseline">
										<td style="FONT: bold 11px arial">Servicing</td>
										<td style="FONT: 11px arial">Performs interim servicing on loans before they are sold.</td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Shipper</td>
										<td style="FONT: 11px arial">Views and packs documents. </td>
									</tr>
									<tr vAlign=baseline>
										<td style="FONT: bold 11px arial">Underwriter </td>
										<td style="FONT: 11px arial">Assesses risk and rates acceptability. </td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<asp:Panel ID="EnableNewRolesPanel" runat="server">
    					<tr>
    					    <td colspan="3" style="color: Red; width: 675px;">* New employee roles are now available. Please contact your LendingQB system administrator to have these new roles enabled for employee assignment *</td>
    					</tr>
					</asp:Panel>
				</table>
			</div>
			<div id=BaseC style="DISPLAY: none">
			<asp:Panel Runat=server Height=477px ID="Panel1" NAME="Panel1">
      <TABLE id=RelationshipTable style="MARGIN-TOP: 5px; MARGIN-LEFT: 25px"
      cellSpacing=4 cellPadding=0 width=660 border=0>
        <TR>
          <TD colSpan=2>
<ml:EncodedLabel id=m_roleNote style="DISPLAY: none" runat="server" Font-Bold="True"></ml:EncodedLabel><BR></TD></TR>
        <TR vAlign=top>
          <TD class=FieldLabel style="BORDER-BOTTOM: lightgrey 2px solid" noWrap
          colSpan=2>Users to be assigned to all loans <SPAN
            style="TEXT-DECORATION: underline">created</SPAN> by this employee:
          </TD></TR>
        <TR vAlign=top>
          <TD noWrap><SPAN style="MARGIN-LEFT: 12px">Manager</SPAN> </TD>
          <TD class="relationship-td">
          <select id="m_Manager" style="width:190px" class="relationship"> </select>
          <span class="cross-branch-assignment-warning"></span>
			</TD></TR>
        <TR vAlign=top>
          <TD noWrap><SPAN style="MARGIN-LEFT: 12px">Processor</SPAN> </TD>
          <TD class="relationship-td">
          <select id="m_Processor" style="width:190px" class="relationship"></select>
          <span class="cross-branch-assignment-warning"></span>

</TD></TR>
        <tr vAlign=top>
          <td noWrap><span style="MARGIN-LEFT: 12px">Junior Processor</span></td>
          <td class="relationship-td">
            <select id="m_JuniorProcessor" style="width:190px" class="relationship"></select>
            <span class="cross-branch-assignment-warning"></span>
          </td>
        </tr>
        <tr vAlign=top>
          <td noWrap><span style="MARGIN-LEFT: 12px">Loan Officer Assistant</span></td>
          <td class="relationship-td">
            <select id="m_LoanOfficerAssistant" style="width:190px" class="relationship"></select>
            <span class="cross-branch-assignment-warning"></span>
          </td>
        </tr>
        <TR vAlign=top>
          <TD noWrap><SPAN style="MARGIN-LEFT: 12px">Lender Account
            Executive</SPAN> </TD>
          <TD class="relationship-td">
			<select id="m_LenderAcctExec" style="width:190px" class="relationship"></select>
            <span class="cross-branch-assignment-warning"></span>

			</TD></TR><% if ( IsPmlEnabled ) { %>
        <TR>
          <TD height=8>&nbsp;</TD></TR>
        <TR vAlign=top>
          <TD class=FieldLabel style="BORDER-BOTTOM: lightgrey 2px solid" noWrap colSpan=2>Users to be assigned to all loans
          <SPAN style="TEXT-DECORATION: underline">registered</SPAN> by this
            employee:
          </TD></TR>
          <TR vAlign=top>
          <TD noWrap><SPAN style="MARGIN-LEFT: 12px">Lock Desk</SPAN> </TD>
          <TD class="relationship-td">
			<select id="m_LockDesk"  style="width:190px" class="relationship"></select>
            <span class="cross-branch-assignment-warning"></span>
		   </TD>
		</TR>
        <TR vAlign=top>
          <TD noWrap><SPAN style="MARGIN-LEFT: 12px">Underwriter</SPAN> </TD>
          <TD class="relationship-td">
            <select id="m_Underwriter" style="width:190px" class="relationship"></select>
            <span class="cross-branch-assignment-warning"></span>

			</TD></TR>
        <tr vAlign=top>
          <td noWrap><span style="MARGIN-LEFT: 12px">Junior Underwriter</span></td>
          <td class="relationship-td">
            <select id="m_JuniorUnderwriter" style="width:190px" class="relationship"></select>
            <span class="cross-branch-assignment-warning"></span>
          </td>
        </tr>
        <TR>
          <TD height=8>&nbsp;</TD></TR>
        <TR vAlign=top>
          <TD><SPAN style="FONT-WEIGHT: bold">Price Group for this
            user.</SPAN> Applicable only if this person is a loan officer
            assigned to submitted loans. </TD>
          <TD><asp:dropdownlist id=m_LpePriceGroupId runat="server" Width="190px"></asp:dropdownlist></TD>
          </TR><% } %>
			</TABLE>

			</asp:Panel>


			</div>
			<div id=BaseD style="DISPLAY: none">
				<table cellSpacing=0 cellPadding=0 style="PADDING-BOTTOM: 10px">
					<tr vAlign=top>
						<td noWrap>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">Access Level </div>
							<div id=AccessLevelDiv style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
								<asp:radiobuttonlist id=m_AccessLevel runat="server" cellspacing="0" cellpadding="0" RepeatDirection="Vertical"></asp:radiobuttonlist>
							</div>
							<div style="MARGIN: 8px"></div>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">Notifications</div>
							<div style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
							    <asp:CheckBox ID="chkSendLoanEventsEmail" runat="server" Text="Send e-mail when loan events occur" />
							    <br />
							    <asp:CheckBox ID="chkSendTasksEmail" runat="server" Text="Send task-related e-mail" />
							</div>
							<% if ( IsPmlEnabled ) { %>
							<div id="ExpirationDetails" style=" BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:548px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; BACKGROUND-COLOR:whitesmoke">
								<table bgcolor=black style="WIDTH:100%" cellspacing=1 cellpadding=2>
								<tr>
									<td bgcolor=gainsboro>&nbsp;</td>
									<td colspan=3 align=center bgcolor=gainsboro width="*"><span style="FONT-WEIGHT:bold">Rate lock request access<br>for <span style="FONT-STYLE:italic">possibly</span> expired rates</span></td>
								</tr>
								<tr>
									<td valign=top bgcolor=gainsboro style="WIDTH:260px"><span style="FONT-WEIGHT:bold">Scenario</span></td>
									<td align=center bgcolor=whitesmoke valign=top style="WIDTH:85px">"Never<br>allowed"</td>
									<td align=center bgcolor=whitesmoke style="WIDTH:85px">"Not allowed<br>after investor<br>cut-off time"</td>
									<td align=center bgcolor=whitesmoke valign=top style="WIDTH:85px">"Always<br>allowed"</td>
								</tr>
								<tr style="HEIGHT:36px">
									<td bgcolor=whitesmoke valign=top>Pricing time is after investor cut-off</td>
									<td align=center bgcolor=#ffcccc valign=top>blocked</td>
									<td align=center bgcolor=#ffcccc valign=top>blocked</td>
									<td align=center bgcolor=#ccffcc valign=top>allowed</td>
								</tr>
								<tr style="HEIGHT:36px">
									<td bgcolor=whitesmoke nowrap valign=top>Pricing time is outside lender lock desk hours</td>
									<td align=center bgcolor=#ffcccc valign=top>blocked</td>
									<td align=center bgcolor=#ccffcc valign=top>allowed</td>
									<td align=center bgcolor=#ccffcc valign=top>allowed</td>
								</tr>
								<tr>
									<td bgcolor=whitesmoke>New rate sheet is detected but updated<br><span style="WIDTH:10px">&nbsp;</span>pricing is not released to users yet</td>
									<td align=center bgcolor=#ffcccc valign=top>blocked</td>
									<td align=center bgcolor=#ccffcc valign=top>allowed</td>
									<td align=center bgcolor=#ccffcc valign=top>allowed</td>
								</tr>
								<tr>
									<td bgcolor=whitesmoke>Technical difficulty is encountered when<br><span style="WIDTH:10px">&nbsp;</span>attempting to access or process rate sheet</td>
									<td align=center bgcolor=#ffcccc valign=top>blocked</td>
									<td align=center bgcolor=#ccffcc valign=top>allowed</td>
									<td align=center bgcolor=#ccffcc valign=top>allowed</td>
								</tr>
								<tr>
									<td align=center bgcolor=whitesmoke colspan=4><br>[ <a href="#" onclick="f_closeExpirationDetails();">Close</a> ]</td>
								</tr>
								</table>
							</div>
							<div style="MARGIN: 8px"></div>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">Rate lock requests for <span style="FONT-STYLE:italic">possibly</span> expired rates <a href="#" onclick="f_showExpirationDetails();">?</a></div>
							<div id=m_rateLockDiv style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
								<asp:radiobutton id="m_noBypass" runat="server" Text="Never allowed" GroupName="RatesheetExpiration"></asp:radiobutton>
								<br>
								<asp:radiobutton id="m_bypassExcInvCutoff" runat="server" Text="Not allowed after investor cut-off time" GroupName="RatesheetExpiration"></asp:radiobutton>
								<br>
								<asp:radiobutton id="m_bypassAll" runat="server" Text="Always allowed" GroupName="RatesheetExpiration"></asp:radiobutton>
							</div>
							<% } %>
							<div class="FieldLabel" style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">Document Printing</div>
							<div id="printSettings" style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
							    <asp:RadioButton class="AllowPrintingAllDocs" ID="AllowPrintingAllDocs" runat="server" Text="Allow printing all standard documents" GroupName="PrintPermissions"/>
							    <br />
							    <asp:RadioButton class="RestrictPrintingDocs" ID="RestrictPrintingDocs" runat="server" Text="Restrict printing to approved print groups only" GroupName="PrintPermissions"/>
							    <br />
							    <a class="Action" id="SelectPrintGroups">select approved print groups</a>
    							<asp:HiddenField ID="AssignedPrintGroupsJSON" runat="server" />
							</div>
						</td>
						<td noWrap width=24>&nbsp; </td>
						<td noWrap>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">
								<A title="Click here for help" style="CURSOR: help" href="https://support.lendingqb.com/index.php?/Knowledgebase/Article/View/288" target=_blank >Access Permissions</A>
								<span style="FONT: 11px arial; COLOR: dimgray">(scroll to see more options) </span>
							</div>
							<div id=m_PermissionsDiv style="BORDER-RIGHT: 0px; PADDING-RIGHT: 8px; BORDER-TOP: lightgrey 2px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 350px; PADDING-TOP: 8px; BORDER-BOTTOM: lightgrey 2px solid; HEIGHT: 300px; BACKGROUND-COLOR: white" align=left>
								<asp:panel id=m_PermissionsPanel onclick=onPermissionsClick(); Runat="server">
<uc:EmployeePermissionsList id=m_EmployeePermissionsList runat="server"></uc:EmployeePermissionsList>
								</asp:panel>
							</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<br>
							<input id=m_populateDefaultPermissions style="WIDTH: 180px" onclick=onPopulateDefaultPermissionsClick(); type=button value="Populate Default Permissions...">
							<input id=m_editDefaultPermissions style="WIDTH: 160px" onclick=onEditDefaultPermissionsClick(); type=button value="Edit Default Permissions">
						</td>
					</tr>
					<tr>
					    <td colspan="3">
                            <span class="FieldLabel">Employee Groups</span>
                            <div id="Groups" style='width: 100%; height: 140px; overflow-y: scroll'>
                                <table id="GroupsTable" class="DataGrid" cellspacing="0" cellpadding="2" rules="all" border="1" style="width:100%;border-collapse:collapse;">
		                        <thead>
		                            <tr class="GridHeader" style="color:black">
			                            <th>&nbsp;</th>
			                            <th align="left"><a href="#" onclick="f_sortGroupGrid();">Company Group</a><img id="GroupSortImg" style="display:none;"/></th>
			                            <th align="left">Description</th>
		                            </tr>
		                        </thead>
		                        <tbody id="GroupsBody">
		                        <asp:Repeater id="m_GroupTableBody" Runat="server" EnableViewState="true" OnItemDataBound="m_GroupTableBody_ItemDataBound">
		                            <ItemTemplate>
		                                <tr class="GridItem">
		                                    <td><input type=checkbox id='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' onclick='UpdateGroupList();' /></td>
		                                    <td>
                                                <ml:EncodedLiteral ID="Name" runat="server"></ml:EncodedLiteral>
		                                    </td>
		                                    <td>
                                                <ml:EncodedLiteral ID="Description" runat="server"></ml:EncodedLiteral>
		                                    </td>
		                                </tr>
		                            </ItemTemplate>
		                            <AlternatingItemTemplate>
		                                <tr class="GridAlternatingItem">
		                                    <td><input type=checkbox id='<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Id" )) %>' onclick='UpdateGroupList();' /></td>
		                                    <td>
                                                <ml:EncodedLiteral ID="Name" runat="server"></ml:EncodedLiteral>
		                                    </td>
		                                    <td>
                                                <ml:EncodedLiteral ID="Description" runat="server"></ml:EncodedLiteral>
		                                    </td>
		                                </tr>
		                            </AlternatingItemTemplate>
		                        </asp:Repeater>
		                        </tbody>
	                        </table>
                                <input id="GroupAssociationList" type="hidden" runat="server" />
							</div>
	                    </td>
					</tr>
				</table>
			</div>
			<div id="BaseE" style="DISPLAY: none">
			    <table style="WIDTH: 550px;">
					<tr>
						<td>
    						<uc:LendingLicenses id="LicensesPanel" NamingPrefix="lp0" runat="server"></uc:LendingLicenses>
						</td>
					</tr>
					<tr>
					    <td>
					     <b><label style="margin-right:10px;">DE Underwriter CHUMS ID</label></b><asp:TextBox ID="ChumsId" MaxLength="8" style="width:20em;" runat="server"></asp:TextBox>
					     </td>
					</tr>
				</table>
			</div>
			<div id="BaseF" style="display:none">
			    <uc:EmployeeTeams id="EmployeeTeams" runat="server"></uc:EmployeeTeams>
			</div>
			<div id="BaseG" style="DISPLAY: none">
				<table cellSpacing=0 cellPadding=0 style="PADDING-BOTTOM:0px">
					<tr>
						<% if( IsInternal ) { %>
						<td class=FieldLabel >
						<% } else { %>
						<td style="PADDING-BOTTOM: 3px; padding-left: 4px;" width="100%">
						<% } %>
						    <div class="column0 left">
						        <div class="FieldLabel heading">IP Address Restriction</div>
						    </div>
						    <div class="column1 left" id="MultiFactorSectionHeader" runat="server">
						        <div class="FieldLabel heading">Multi-Factor Authentication</div>
						    </div>
						</td>
					</tr>
					<tr>
						<td colspan=2 class=FieldLabel  noWrap>
						    <div class="column0 left">
						        <div >
						            <asp:RadioButton runat="server" ID="m_ipRestrictionOff" GroupName="ipRestriction" Text="No IP address restriction" onclick="IP.Filter(true);" /><br />
						            <asp:RadioButton runat="server" ID="m_ipRestrictionOn" GroupName="ipRestriction" Text="Only allow IPs listed below:" onclick="IP.Filter(false);" /><br />
							        <input type="checkbox" checked="checked" disabled="disabled" style="margin-left:15px" />IPs on global whitelist<br />
							        <input type="checkbox" checked="checked" disabled="disabled" style="margin-left:15px" />Devices with a certificate<br/>
						        </div>
						        <div class="padleft2 left">
							        <input type="text" class="ipinput" maxlength="15"  onkeydown="return IP.CheckInputKey(event)" name="ip_input" id="ip_input"/> <br/>
							        <asp:ListBox Runat="server" ID="m_ip" Rows="5"  CssClass="ipinput" EnableViewState="False"  SelectionMode="Multiple" />
							        <input type="hidden" runat="server" id="m_ip_parsed" name="m_ip_parsed" />
						        </div>
						        <div class="padleft1 left">
							        <input type="button" id="ip_add" name="ip_add" value="Add" onclick="IP.Add('ip_input')" /> <br/>
							        <input type="button" style="margin-top: 5px" id="ip_remove" name="ip_remove" value="Remove" onclick="IP.Remove()"/>
						        </div>
						    </div>
						    <div class="column1 left" id="MultiFactorSection" runat="server">
							    <div style="font-weight:normal">
                                    <input type="checkbox" runat="server" id="m_EnabledMultiFactorAuthentication" /> <label for="m_EnabledMultiFactorAuthentication">Enable multi-factor auth for this user.</label> <br />
							        <asp:CheckBox ID="m_EnabledClientDigitalCertificateInstall" runat="server" Text="Allow user to install digital certificate." /><br />
                                    <asp:CheckBox ID="EnableAuthCodeViaSms" runat="server" Text="Enable auth code transmission via SMS text" /> <br />
                                    <input type="checkbox" Id="EnableAuthCodeViaAuthenticator" runat="server" disabled="disabled" ToolTip="User must setup authenticator to enable this." /> <label for="EnableAuthCodeViaAuthenticator">Authenticator App Enabled</label>
                                    <asp:HiddenField runat="server" id="IsDisableAuthenticator" />
                                    <input type="button" id="DisableAuthenticator" runat="server" value="Disable authenticator app" />
							    </div>
							</div>
						</td>
					</tr>
					<div runat="server" id="CertificatesAndIPs">
					    <tr>
					        <% if( IsInternal ) { %>
						    <td class=FieldLabel >
						    <% } else { %>
						    <td style="PADDING-BOTTOM: 3px; padding-left: 4px;" width="100%">
						    <% } %>
                                <div class="FieldLabel heading">Client Certificate</div>
                            </td>
					    </tr>
					    <tr>
					        <td>
					           <asp:HiddenField ID="certIDsToDelete" runat="server" Value="" />
					           <ml:CommonDataGrid ID="m_clientCertificateGrid" runat="server" EnableViewState="true">
	                                <Columns>
	                                    <asp:BoundColumn DataField="Description" HeaderText="Device Name"/>
	                                    <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date"/>
	                                    <asp:TemplateColumn>
	                                        <ItemTemplate>
	                                        <a href="#" onclick="return revokeClientCertificate(this, '<%# AspxTools.HtmlString(Eval("CertificateId")) %>');">revoke</a>
	                                        </ItemTemplate>
	                                    </asp:TemplateColumn>
	                                </Columns>
			                    </ml:CommonDataGrid>
			                    <a href="#" onclick="f_installClientCertificate();" id="installCertificateLink" runat="server">Install client certificate</a>
			                </td>
			            </tr>
			            <tr>
			                <% if( IsInternal ) { %>
						    <td class=FieldLabel >
						    <% } else { %>
						    <td style="PADDING-BOTTOM: 3px; padding-left: 4px;" width="100%">
						    <% } %>
                                <div class="FieldLabel heading">IP Access</div>
                            </td>
			            </tr>
			            <tr>
			                <td>
			                <asp:HiddenField ID="IPsToDelete" runat=server Value="" />
			                    <ml:CommonDataGrid ID="m_registeredIpDataGrid" runat="server" EnableViewState="true">
			                        <Columns>
			                            <asp:BoundColumn DataField="IpAddress" HeaderText="IP Address" />
			                            <asp:BoundColumn DataField="IsRegistered" HeaderText="Registered IP" />
			                            <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" />
			                            <asp:TemplateColumn>
			                                <ItemTemplate>
			                                <a href="#" onclick="return deleteIp(this, '<%# AspxTools.HtmlString(Eval("Id")) %>');">remove</a>
			                                </ItemTemplate>
			                            </asp:TemplateColumn>
			                        </Columns>
			                    </ml:CommonDataGrid>
			                </td>
			            </tr>
			        </div>
				</table>
			</div>
			<div id="BaseS">
                <div id="ServiceCredentialDiv">
                    <div>
                        <table id="ServiceCredentialTable" class="DataGrid" ><tbody></tbody></table>
                    </div>
                    <br />
                    <input type="button" id="AddServiceCredentialBtn" value="Add Credential" />
                </div>
                <div id="ServiceCredentialsNewEmpDiv">
                    <span>Please create this employee before adding service credentials.</span>
                </div>
			</div>
			<% if( IsInternal  ) { %>
			<div id=BaseI style="DISPLAY: none">
				<table cellSpacing=0 cellPadding=0 style="PADDING-BOTTOM:145px" >
					<tr vAlign=top>
						<td noWrap>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px" >User Type </div>
							<div style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
								<asp:radiobuttonlist id=m_UserType runat="server">
									<asp:ListItem Value="B" Selected="True">Broker</asp:ListItem>
									<asp:ListItem Value="P">Price My Loan</asp:ListItem>
								</asp:radiobuttonlist>
							</div>
							<div style="MARGIN: 8px"></div>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px" >Allow Login From MCL </div>
							<div style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px" >
								<asp:radiobuttonlist id=m_AllowLoginFromMCL runat="server">
									<asp:ListItem Value="0" Selected="True">No</asp:ListItem>
									<asp:ListItem Value="1">Yes</asp:ListItem>
								</asp:radiobuttonlist>
								<table>
									<tr>
										<td width=90>External Site Id </td>
										<td>
											<asp:textbox id=m_ExternalSiteId style="PADDING-LEFT: 4px" onclick="focus(); select();" runat="server" Width="220px" ReadOnly="True" BorderWidth="0"></asp:textbox>
										</td>
									</tr>
								</table>
							</div>
							<div style="MARGIN: 8px"></div>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">User Identifiers </div>
							<div style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
								<table>
									<tr>
										<td width=90>Employee Id </td>
										<td>
											<asp:textbox id=m_Id  style="PADDING-LEFT: 4px" onclick="focus(); select();" runat="server" Width="220px" ReadOnly="True" BorderWidth="0"></asp:textbox>
										</td>
									</tr>
									<tr>
										<td width=90>User Id </td>
										<td>
											<asp:textbox id=m_UserId style="PADDING-LEFT: 4px" onclick="focus(); select();" runat="server" Width="220px" ReadOnly="True" BorderWidth="0"></asp:textbox>
										</td>
									</tr>
								</table>
							</div>

                                <div style="MARGIN: 8px"></div>
							    <div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">LendingQB Support Settings </div>
							    <div style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
                                <table>
                                    <tr>
                                        <td>
                                            <label for="m_SupportEmailInternal" style="margin-right: 5px;">Support Email</label>
                                            <asp:TextBox runat="server" ID="m_SupportEmailInternal"></asp:TextBox>
                                            <asp:regularexpressionvalidator id="SupportEmailValidator" runat="server" ControlToValidate="m_SupportEmailInternal" Display="Dynamic"></asp:regularexpressionvalidator>
									        <asp:CheckBox runat="server" ID="m_SupportEmailVerified"  Text="Verified?"/> Verified will reset if email is changed or removed.
									    </td>
									</tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox runat="server" ID="m_CanContactSupport" Text="Allow contacting LQB Support" />
                                        </td>
                                    </tr>
                                </table>

					</div>

						</td>
						<td noWrap width=24>&nbsp; </td>
						<td>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">Internal Permissions </div>
							<div style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; WIDTH: 262px; PADDING-TOP: 6px">
								<asp:panel id=m_IPermissionsPanel>
									<asp:checkboxlist id=m_InternalPermissions runat="server" cellspacing="0" cellpadding="0"></asp:checkboxlist>
								</asp:panel>
							</div>
							<div style="MARGIN: 8px"></div>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">Price My Loan </div>
							<div style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
								<asp:checkbox id=m_IsPmlManager runat="server" Text="Is Price My Loan manager?"></asp:checkbox>
								<div style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; WIDTH: 20px; PADDING-TOP: 8px; TEXT-ALIGN: center">or </div>
								<table>
									<tr>
										<td>Set manager as </td>
										<td>
											<asp:dropdownlist id=m_PmlExternalManagerEmployeeId runat="server"></asp:dropdownlist>
										</td>
									</tr>
								</table>
							</div>
							<div style="MARGIN: 8px"></div>
							<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">External Settings </div>
							<div style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
								<table>
								    <tr>
								        <td colspan="2" noWrap><asp:CheckBox ID="m_IsNewPmlUIEnabled" runat="server" Text="Enable New PML UI for this user." onclick="onEnableNewPmlUiClick();"/></td>
								    </tr>
								    <tr id="trShowQMStatusInPml2">
									  <td noWrap width="80">Show QM Info On PML 2.0 and Certificate?</td>
									  <td nowrap><asp:checkbox id="m_ShowQMStatusInPml2" runat="server" /></td>
									</tr>
									<tr>
									  <td noWrap width="80">Enable User Quick Pricer</td>
									  <td nowrap><asp:checkbox id="m_IsUserQuickPricerEnabled" runat="server" value="Yes" /></td>
									</tr>
									<tr>
									  <td colspan="2"><asp:CheckBox ID="m_IsPricingMultipleAppsSupported" runat="server" Text="Enable Pricing Multiple Apps" /></td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table></DIV>
			<% } %>
			<input id="m_CurrentTab" type="hidden" value="TabA" name="m_CurrentTab" runat="server"> </DIV>

		<script type="text/javascript">
        jQuery(function($){
            var BrokerId = <%= AspxTools.JsString(m_BrokerId) %>;
	        var UserId = <%=AspxTools.JsString(m_UserIdG) %>;
	        var $PrintGroupStorage = $('#' + <%=AspxTools.JsString(AssignedPrintGroupsJSON.ClientID) %>);
	        var $DirtyBit = $('#' + <%=AspxTools.JsString(m_Dirty.ClientID)%>);
	        var PrintGroupEditUrl = <%= AspxTools.JsString(PrintGroupEditUrl) %>;

            var selectedIds = JSON.parse($PrintGroupStorage.val());

            $('#printSettings input').change(function(){
                if(!$(this).is(':checked')) return;
                var restricted = this.id.indexOf('RestrictPrintingDocs') != -1;

                setDisabledAttr($('#SelectPrintGroups'), !restricted);
            }).change();

            $('#<%= AspxTools.ClientId(DisableAuthenticator) %>').click(function () {
                $('#<%= AspxTools.ClientId(IsDisableAuthenticator)%>').val('disabled');
                $('#<%= AspxTools.ClientId(EnableAuthCodeViaAuthenticator)%>').attr('checked', false);
                $DirtyBit.val("1");
                updateDirtyBit();
            });


            $('#SelectPrintGroups').click(function(){
                if (hasDisabledAttr(this))
                {
                    $('.RestrictPrintingDocs input').attr('checked', 'checked').change();
                }
                var dialogArgs = {init: selectedIds};
                showModal(PrintGroupEditUrl + "?brokerId=" + BrokerId + "&userid=" + UserId, dialogArgs, "Print Group Permissions", null, function(arg){ 
					if(arg.OK && arg.valuesJSON)
					{
						selectedIds = JSON.parse(arg.valuesJSON);
						var old = $PrintGroupStorage.val();
						if(arg.valuesJSON == old) return;

						$PrintGroupStorage.val(arg.valuesJSON);
						$DirtyBit.val('1');
						updateDirtyBit();
					}
                },
                { hideCloseButton: true });
            });

            var serializedInfo = $('#<%= AspxTools.ClientId(SerializedEmployeeInfo) %>').val();
            var employeeInfoDictionary = JSON.parse(serializedInfo);

            $('select.relationship').change(function() {
                var $this, $crossBranchAssignmentWarning, selectedEmployeeId, selectedUserIsInSameBranch, selectedEmployeeInfo;
                $this = $(this);
                $crossBranchAssignmentWarning = $this.siblings('.cross-branch-assignment-warning');
                selectedEmployeeId = this.value;
                selectedUserIsInSameBranch = isUserInCurrentSelectedBranch(selectedEmployeeId);
                selectedEmployeeInfo = employeeInfoDictionary[selectedEmployeeId];

                if (selectedEmployeeInfo && !selectedUserIsInSameBranch) {
                    $crossBranchAssignmentWarning.text('NOTE: Cross-branch assignment');
                } else {
                    $crossBranchAssignmentWarning.text('');
                }
            }).change();

        });
		function onEditDefaultPermissionsClick()
		{
		  var brokerId = <%= AspxTools.JsString(m_BrokerId) %>;
			<% if (this.IsInternal) {%>
				url = "/loadmin/broker/EditDefaultRolePermissions.aspx?brokerId=";
			<% } else { %>
				url = "/los/Admin/EditDefaultRolePermissions.aspx?brokerId=";
			<% } %>
		    showModal(url + brokerId, null, "Default Permissions", null, null, { hideCloseButton: true });
		}

		function onPermissionsClick()
		{
 			var isChecked = IsChecked(<%= AspxTools.JsString(Permission.AllowLoanAssignmentsToAnyBranch.ToString()) %>);

			var noteElement = <%= AspxTools.JsGetElementById(m_roleNote) %>;

			if(noteElement != null)
			{
				if(!isChecked)
				{
					noteElement.style.display = "";
					noteElement.innerText = <%= AspxTools.JsString(JsMessages.OnlyDisplayingUsersFromSameBranch) %> + '\n';
				}
				else
					noteElement.style.display = "none";
			}

			f_toggleAppraisalVendorPanel();
		}

		function onPopulateDefaultPermissionsClick()
		{
			dontResetAllowLogin = true;
			updateDirtyBit();
			var alertMessage = <%= AspxTools.JsGetElementById(m_AlertMessage) %>;
			var selectedRoles = new Array();
			var selectedValues = new Array();
			var selectedNames = new Array();
			var count = 0;
			var hasARole = false;
			if(window.roleIDArray)
			{
				for(var i = 0; i < roleIDArray.length; ++i)
				{
					var element = document.getElementById(roleIDArray[i]);
					if(element.checked)
					{
						hasARole = true;
						selectedRoles[count] = roleIDArray[i];
						selectedValues[count] = roleValArray[i];
						selectedNames[count] = roleNameArray[i];
						++count;
					}
				}

				if(!hasARole)
				{
					enableErrorMessages();
					alertMessage.innerText = <%= AspxTools.JsString(JsMessages.SpecifyRole)%>;
					return false;
				}
				else
				{
					var msg = "Populate the default permissions for this user's role(s)?";
					msg += "\n(After clicking OK, please review the selected permissions)";
					msg += "\n\nCurrent roles assigned to this user:";

					for(var i = 0; i < selectedNames.length; ++i)
						msg += "\n\t" + selectedNames[i];

					msg += "\n";
					if(confirm(msg))
					{
					    populateDefaultRolePermissionsAndUpdateUI(selectedRoles, selectedValues, true)
					}
				}
			}
		}

		// Applies the default permissions for the given roles and updates the UI.
		// If no roles are selected, it will default the non-bool permission to the
		// most limited option.
		function populateDefaultRolePermissionsAndUpdateUI(selectedRoles, selectedValues, bSetDefaultEnableDisable)
		{
		    var result = PopulateDefaultRolePermissions(selectedRoles, selectedValues);
		    if(!result.error)
		    {
			    var permissions = result.value;
			    for (perm in permissions)
			    {
				    SetPermissions(permissions[perm]);
				    break;
			    }

			    if (bSetDefaultEnableDisable)
			    {
					setDefaultDisableEnable();
				}
		    }
		    else
		    {
			    alert(<%= AspxTools.JsString(JsMessages.CannotPopulateDefaultRolePermissions) %>);
			    return;
		    }

    		if (selectedRoles.length === 0 || selectedValues.length === 0)
    		{
    		    setRSEPermission(0);
    		    return;
    		}

		    var resultNonBoolPerms = PopulateDefaultNonBoolRolePermissions(selectedRoles, selectedValues);
	        if(!resultNonBoolPerms.error)
		    {
			    setRSEPermission(resultNonBoolPerms.value["DefaultRSENonBoolPermission"])
		    }
		    else
		    {
			    alert(<%= AspxTools.JsString(JsMessages.CannotPopulateDefaultRolePermissions) %>);
			    return;
		    }

		    return;
		}

		function setRSEPermission(index)
		{
		    if(index == "0")
		    {
		        if(<%=AspxTools.JsGetElementById(m_noBypass)%>)
		        {
		            <%=AspxTools.JsGetElementById(m_noBypass)%>.checked = true;
		        }

		        if(<%=AspxTools.JsGetElementById(m_bypassExcInvCutoff)%>)
		        {
		            <%=AspxTools.JsGetElementById(m_bypassExcInvCutoff)%>.checked = false;
		        }

		        if(<%=AspxTools.JsGetElementById(m_bypassAll)%>)
		        {
		            <%=AspxTools.JsGetElementById(m_bypassAll)%>.checked = false;
		        }
		    }
		    else if(index == "1")
		    {
		        if(<%=AspxTools.JsGetElementById(m_noBypass)%>)
		        {
		            <%=AspxTools.JsGetElementById(m_noBypass)%>.checked = false;
		        }

		        if(<%=AspxTools.JsGetElementById(m_bypassExcInvCutoff)%>)
		        {
		            <%=AspxTools.JsGetElementById(m_bypassExcInvCutoff)%>.checked = true;
		        }

		        if(<%=AspxTools.JsGetElementById(m_bypassAll)%>)
		        {
		            <%=AspxTools.JsGetElementById(m_bypassAll)%>.checked = false;
		        }
		    }
		    else
		    {
		        if(<%=AspxTools.JsGetElementById(m_noBypass)%>)
		        {
		            <%=AspxTools.JsGetElementById(m_noBypass)%>.checked = false;
		        }

		        if(<%=AspxTools.JsGetElementById(m_bypassExcInvCutoff)%>)
		        {
		            <%=AspxTools.JsGetElementById(m_bypassExcInvCutoff)%>.checked = false;
		        }

		        if(<%=AspxTools.JsGetElementById(m_bypassAll)%>)
		        {
		            <%=AspxTools.JsGetElementById(m_bypassAll)%>.checked = true;
		        }
		    }
		}

		function setDefaultDisableEnable()
		{
			onRoleClick(false);

			if(window.dontResetAllowLogin && (dontResetAllowLogin == true))
				return;

			var loginNotes = <%= AspxTools.JsGetElementById(m_LoginNotes) %>;
			var password = <%= AspxTools.JsGetElementById(m_Password) %>;
			var confirm = <%= AspxTools.JsGetElementById(m_Confirm) %>;
			var login = <%= AspxTools.JsGetElementById(m_Login) %>;
			var changeLogin = <%= AspxTools.JsGetElementById(m_ChangeLogin) %>;
			var allowMulti = <%= AspxTools.JsGetElementById(m_AllowMulti) %>;
			var allowLogin = <%= AspxTools.JsGetElementById(m_CanLogin) %>;
			var passwordControls = document.getElementById('PasswordControls');
			var pwRules = document.getElementById('pwRules' );

			if ( <%= AspxTools.JsBool(m_setIsActive) %> )
			{
				if(allowMulti != null)
					allowMulti.disabled = false;
				allowLogin.disabled = false;
			}
			else
			{
				if(allowMulti != null)
					allowMulti.disabled = true;
				allowLogin.disabled = true;
			}

			var alertMessage = <%= AspxTools.JsGetElementById(m_AlertMessage) %>;
			changeLogin.checked = (alertMessage.innerText != "")?true:false;

			if ( <%= AspxTools.JsBool(m_setAllowLogin) %> )
			{
				changeLogin.disabled = false;

				if(!changeLogin.checked)
				{
					loginNotes.style.visibility = "hidden";
					pwRules.style.visibility = "hidden";

					login.readOnly    = true;
					password.readOnly = true;
					confirm.readOnly  = true;
					toggleActiveDirectoryUserCheckbox(false);
			    }
		        disablePasswordControls(false);
			}
			else
			{
				changeLogin.disabled = true;
				loginNotes.style.visibility = "hidden";
				pwRules.style.visibility = "hidden";
				login.readOnly    = true;
				password.readOnly = true;
				confirm.readOnly  = true;
				disablePasswordControls(true);
				toggleActiveDirectoryUserCheckbox(false);
			}

			toggleADFields();
		}

		function disablePasswordControls(disable) {
			setDisabledAttr(<%=AspxTools.JsGetElementById(m_MustChangePasswordAtNextLogin)%>, disable);
			setDisabledAttr(<%=AspxTools.JsGetElementById(m_PasswordNeverExpires)%>, disable);
			setDisabledAttr(<%=AspxTools.JsGetElementById(m_PasswordExpiresOn)%>, disable);
			setDisabledAttr(<%=AspxTools.JsGetElementById(m_ExpirationDate)%>, disable);
			setDisabledAttr(<%=AspxTools.JsGetElementById(m_CycleExpiration)%>, disable);
			setDisabledAttr(<%=AspxTools.JsGetElementById(m_ExpirationPeriod)%>, disable);
		}

        function toggleActiveDirectoryUserCheckbox(bShouldEnable)
        {
            <% if( IsActiveDirectoryAuthEnabledForLender  ) { %>
                var activeDirUser = <%= AspxTools.JsGetElementById(m_IsActiveDirectoryUser) %>;
                activeDirUser.disabled = !bShouldEnable;
            <% } %>
        }

        function toggleADFields()
        {
            <% if( IsActiveDirectoryAuthEnabledForLender  ) { %>
                var loginNotes = <%= AspxTools.JsGetElementById(m_LoginNotes) %>;
			    var passwordControls = document.getElementById('PasswordControls');
			    var passwordDiv = document.getElementById('PasswordDiv');
			    var retypePasswordDiv = document.getElementById('RetypePasswordDiv');
			    var activeDirUser = <%= AspxTools.JsGetElementById(m_IsActiveDirectoryUser) %>;
                var loginLabel = <%= AspxTools.JsGetElementById(m_LoginLabel) %>;

                if(activeDirUser.checked)
                {
                    loginNotes.style.visibility = "hidden";
                    loginLabel.innerHTML = "Active Directory Username&nbsp;&nbsp;"
                    passwordControls.style.display = 'none';
                    passwordDiv.style.display = 'none';
                    retypePasswordDiv.style.display = 'none';
                }
                else
                {
                    loginLabel.innerHTML = "Login Name";
                    passwordControls.style.display = 'block';
                    passwordDiv.style.display = 'block';
                    retypePasswordDiv.style.display = 'block';
                }
            <% } %>
        }

		function onAllowLoginClick()
		{
			<%= AspxTools.JsGetElementById(m_Dirty) %>.value = "1";
			var allowLogin = <%= AspxTools.JsGetElementById(m_CanLogin) %>;
			var changeLogin = <%= AspxTools.JsGetElementById(m_ChangeLogin) %>;
			var passwordControls = document.getElementById('PasswordControls');
			var expirationDate = <%= AspxTools.JsGetElementById(m_ExpirationDate) %>;
			var expirationPeriod = <%= AspxTools.JsGetElementById(m_ExpirationPeriod) %>;
			var assignLicenseLink = <%= AspxTools.JsGetElementById(m_AssignLicenseLink) %>;
			var unassignLicenseLink = <%= AspxTools.JsGetElementById(m_UnassignLicenseLink) %>;

			changeLogin.disabled = !allowLogin.checked;
			disablePasswordControls(!allowLogin.checked);
			expirationDate.disabled = !allowLogin.checked;
			expirationPeriod.disabled = !allowLogin.checked;

			var managerDDL = document.getElementById('m_Manager');
			var lenderaeDDL = document.getElementById('m_LenderAcctExec');
			var accessLevel = <%= AspxTools.JsGetElementById(m_AccessLevel) %>;
			var processorDDL = document.getElementById('m_Processor');
			var juniorProcessorDDL = document.getElementById('m_JuniorProcessor');
			var loanOfficerAssistantDDL = document.getElementById('m_LoanOfficerAssistant');

			<% if ( IsPmlEnabled ) { %>
				var underwriterDDL = document.getElementById('m_Underwriter');
			    var lockdeskDDL = document.getElementById('m_LockDesk');
    			var juniorUnderwriterDDL = document.getElementById('m_JuniorUnderwriter');
				var pricegroupDDL = <%= AspxTools.JsGetElementById(m_LpePriceGroupId) %>;
			<% } %>

			if(allowLogin.checked)
			{
				if(assignLicenseLink)
				{
				    setDisabledAttr(assignLicenseLink, false);
					assignLicenseLink.style.textDecorationNone = false;
					assignLicenseLink.style.cursor = "hand";
				}

				managerDDL.disabled = false;
				lenderaeDDL.disabled = false;
				processorDDL.disabled = false;
				<% if ( IsPmlEnabled ) { %>
					underwriterDDL.disabled = false;
					lockdeskDDL.disabled = false;
					pricegroupDDL.disabled = false;
				<% } %>

			    setAccessLevelDisabled(false);
			    if(document.getElementById('m_rateLockDiv'))
			        setRateLockDisabled(false);

				setDisabledAttr($(<%= AspxTools.JsGetElementById(m_PermissionsPanel) %>).find('input'), false);
				document.getElementById('m_populateDefaultPermissions').disabled = false;
				expirationPolicyClick();
				onRoleClick(true);
			}
			else
			{
				if(unassignLicenseLink)
					unassignLicenseLink.click();
				if(assignLicenseLink)
				{
				    setDisabledAttr(assignLicenseLink, true);
					assignLicenseLink.style.textDecorationNone = true;
					assignLicenseLink.style.cursor = "default";
				}

				expirationDate.style.backgroundColor="gainsboro";
			}

			if ( !(<%= AspxTools.JsBool(m_HasLogin) %>) || !allowLogin.checked)
				changeLogin.checked = allowLogin.checked;

			if ( !(<%= AspxTools.JsBool(m_HasLogin) %>) && !allowLogin.checked)
			{
				managerDDL.disabled = true;
				managerDDL.selectedIndex = 0;
				processorDDL.disabled = true;
				processorDDL.selectedIndex = 0;
				lenderaeDDL.disabled = true;
				lenderaeDDL.selectedIndex = 0;
				<% if ( IsPmlEnabled ) { %>
					underwriterDDL.disabled = true;
					underwriterDDL.selectedIndex = 0;
					lockdeskDDL.disabled = true;
				    lockdeskDDL.selectedIndex = 0;
					pricegroupDDL.disabled = true;
					pricegroupDDL.selectedIndex = 0;
				<% } %>

			    setAccessLevelDisabled(true);
			    if(document.getElementById('m_rateLockDiv'))
			        setRateLockDisabled(true);
				setDisabledAttr($(<%= AspxTools.JsGetElementById(m_PermissionsPanel) %>).find('input'), true);
			    document.getElementById('m_populateDefaultPermissions').disabled = true;
			    disablePasswordControls(true);
				expirationPeriod.disabled = true;
			}

			onChangeLoginClick();
			if ( typeof(Licensing) !== "undefined")  {
			    Licensing.UpdateDisplay();
			}
		}

		function setRateLockDisabled(disable) {
		    setDisabledAttr(<%= AspxTools.JsGetElementById(m_noBypass) %>, disable);
		    setDisabledAttr(<%= AspxTools.JsGetElementById(m_bypassExcInvCutoff) %>, disable);
		    setDisabledAttr(<%= AspxTools.JsGetElementById(m_bypassAll) %>, disable);
		}		

		function onChangeLoginClick()
		{
			var loginNotes = <%= AspxTools.JsGetElementById(m_LoginNotes) %>;
			var password = <%= AspxTools.JsGetElementById(m_Password) %>;
			var confirm = <%= AspxTools.JsGetElementById(m_Confirm) %>;
			var login = <%= AspxTools.JsGetElementById(m_Login) %>;
			var changeLogin = <%= AspxTools.JsGetElementById(m_ChangeLogin) %>;
			var loginR = <%= AspxTools.JsGetElementById(m_loginR) %>;
			var passwordR = <%= AspxTools.JsGetElementById(m_passwordR) %>;
			var retypeR = <%= AspxTools.JsGetElementById(m_retypeR) %>;
			var pwRules = document.getElementById('pwRules');
			password.innerText = "";
			confirm.innerText  = "";

			if( changeLogin.checked == true )
			{
				if(loginNotes != null)
				{
					loginNotes.style.visibility = "visible";
					    pwRules.style.visibility = "visible";
				}


				login.readOnly    = false;
				password.readOnly = false;
				confirm.readOnly  = false;

				loginR.style.display = "";
				passwordR.style.display = "";
				retypeR.style.display = "";
			}
			else
			{
				if(loginNotes != null)
				{
					loginNotes.style.visibility  = "hidden";
					pwRules.style.visibility = "hidden";
				}


				login.readOnly   = true;
				password.readOnly = true;
				confirm.readOnly  = true;

				loginR.style.display = "none";
				passwordR.style.display = "none";
				retypeR.style.display = "none";
			}

			toggleActiveDirectoryUserCheckbox(changeLogin.checked);
			toggleADFields();
		}

		function onADUserClick()
		{
		    toggleADFields();
		}

		function onEmploymentStatusClick()
		{
			<%= AspxTools.JsGetElementById(m_Dirty) %>.value = "1";
			var isCurrentEmployee = <%= AspxTools.JsGetElementById(m_IsEmployee) %>;
			var changeLogin = <%= AspxTools.JsGetElementById(m_ChangeLogin) %>;
			var allowMulti = <%= AspxTools.JsGetElementById(m_AllowMulti) %>;
			var allowLogin = <%= AspxTools.JsGetElementById(m_CanLogin) %>;
			var pwRules = document.getElementById('pwRules');

			if( isCurrentEmployee.checked == false )
			{
				changeLogin.disabled = true;
				changeLogin.checked = false;

				if(allowMulti != null)
				{
					allowMulti.disabled = true;
					allowMulti.checked = false;
				}

				allowLogin.disabled = true;
				allowLogin.checked = false;

				<%= AspxTools.JsGetElementById(m_Login) %>.readonly = true;
				<%= AspxTools.JsGetElementById(m_Password) %>.readonly = true;
				<%= AspxTools.JsGetElementById(m_Confirm) %>.readonly = true;
			}
			else
			{
				if( allowLogin.checked == true )
					changeLogin.disabled = false;
				if(allowMulti != null)
					allowMulti.disabled = false;
				allowLogin.disabled = false;
			}
			var loginNotes = <%= AspxTools.JsGetElementById(m_LoginNotes) %>;
			if(loginNotes != null)
			{
				loginNotes.style.visibility = "hidden";
				pwRules.style.visibility = "hidden";
			}
			onAllowLoginClick();
		}

		function onRoleClick(bPopulateDefaultRolePerms)
		{
			var alertMessage = <%= AspxTools.JsGetElementById(m_AlertMessage) %>;
			var index = alertMessage.innerText.indexOf(<%= AspxTools.JsString(JsMessages.SpecifyRole) %>);
			if(index != -1)
				disableErrorMessages(<%= AspxTools.JsString(JsMessages.SpecifyRole) %>);
			var selectedRoles = [];
			var selectedValues = [];
			var bIsNewEmployee = <%= AspxTools.JsBool(IsNewEmp) %>;

			if ( (<%= AspxTools.JsBool(m_HasLogin) %>) || (<%= AspxTools.JsGetElementById(m_CanLogin) %>.checked))
			{
				if(window.roleIDArray)
				{
					for(var i = 0; i < roleIDArray.length; ++i)
					{
						var element = document.getElementById(roleIDArray[i]);
						switch(element.parentElement.innerText)
						{
							case "Manager":
								var managerDD = document.getElementById('m_Manager');
								managerDD.disabled = element.checked;
								managerDD.title = (element.checked)? <%= AspxTools.JsString(JsMessages.UserAlreadyHasRole) %>:"";
								if(element.checked && managerDD.selectedIndex > 0)
								{
									alert(<%= AspxTools.JsString(JsMessages.ClearPreviousRoleSetting(s_Manager))%>);
									managerDD.selectedIndex = 0;
								}
							break;

							case "Lender Account Executive":
								var lenderaeDD = document.getElementById('m_LenderAcctExec');
								lenderaeDD.disabled = element.checked;
								lenderaeDD.title = (element.checked)?<%= AspxTools.JsString(JsMessages.UserAlreadyHasRole) %>:"";
								if(element.checked && lenderaeDD.selectedIndex > 0)
								{
									alert(<%= AspxTools.JsString(JsMessages.ClearPreviousRoleSetting(s_LenderAE)) %>);
									lenderaeDD.selectedIndex = 0;
								}
							break;
							case "Processor":
								var processorDD = document.getElementById('m_Processor');
								processorDD.disabled = element.checked;
								processorDD.title = (element.checked)? <%= AspxTools.JsString(JsMessages.UserAlreadyHasRole) %>:"";
								if(element.checked && processorDD.selectedIndex > 0)
								{
									alert(<%= AspxTools.JsString(JsMessages.ClearPreviousRoleSetting(s_Processor)) %>);
									processorDD.selectedIndex = 0;
								}
								break;
							case "Junior Processor":
								var juniorProcessorDD = document.getElementById('m_JuniorProcessor');
								juniorProcessorDD.disabled = element.checked;
								juniorProcessorDD.title = (element.checked) ? <%= AspxTools.JsString(JsMessages.UserAlreadyHasRole) %> : "";
								if(element.checked && juniorProcessorDD.selectedIndex > 0)
								{
									alert(<%= AspxTools.JsString(JsMessages.ClearPreviousRoleSetting(s_JuniorProcessor)) %>);
									juniorProcessorDD.selectedIndex = 0;
								}
								break;
							case "Loan Officer Assistant":
								var loanOfficerAssistantDD = document.getElementById('m_LoanOfficerAssistant');
								loanOfficerAssistantDD.disabled = element.checked;
								loanOfficerAssistantDD.title = (element.checked) ? <%= AspxTools.JsString(JsMessages.UserAlreadyHasRole) %> : "";
								if(element.checked && loanOfficerAssistantDD.selectedIndex > 0)
								{
									alert(<%= AspxTools.JsString(JsMessages.ClearPreviousRoleSetting(s_LoanOfficerAssistant)) %>);
									loanOfficerAssistantDD.selectedIndex = 0;
								}
								break;
							<% if ( IsPmlEnabled ) { %>
							case "Underwriter":
								var underwriterDD = document.getElementById('m_Underwriter');
								underwriterDD.disabled = element.checked;
								underwriterDD.title = (element.checked)? <%= AspxTools.JsString(JsMessages.UserAlreadyHasRole) %>:"";
								if(element.checked && underwriterDD.selectedIndex > 0)
								{
									alert(<%= AspxTools.JsString(JsMessages.ClearPreviousRoleSetting(s_Underwriter)) %>);
									underwriterDD.selectedIndex = 0;
								}
							break;
							case "Junior Underwriter":
								var juniorUnderwriterDD = document.getElementById('m_JuniorUnderwriter');
								juniorUnderwriterDD.disabled = element.checked;
								juniorUnderwriterDD.title = (element.checked) ? <%= AspxTools.JsString(JsMessages.UserAlreadyHasRole) %> : "";
								if(element.checked && juniorUnderwriterDD.selectedIndex > 0)
								{
									alert(<%= AspxTools.JsString(JsMessages.ClearPreviousRoleSetting(s_JuniorUnderwriter)) %>);
									juniorUnderwriterDD.selectedIndex = 0;
								}
							break;
							case "Lock Desk":
								var lockdeskDD = document.getElementById('m_LockDesk');
								lockdeskDD.disabled = element.checked;
								lockdeskDD.title = (element.checked)? <%= AspxTools.JsString(JsMessages.UserAlreadyHasRole) %>:"";
								if(element.checked && lockdeskDD.selectedIndex > 0)
								{
									alert(<%= AspxTools.JsString(JsMessages.ClearPreviousRoleSetting(s_LockDesk)) %>);
									lockdeskDD.selectedIndex = 0;
								}
							break;

								<% } %>
							break;
							default:
							break;
						}

						if (element.checked)
						{
						    selectedRoles.push(roleIDArray[i]);
						    selectedValues.push(roleValArray[i]);
						}
					}

					// OPM 112605 - If it is a new employee and the role is selected
					// populate the default permissions for the role.
					if (bIsNewEmployee && bPopulateDefaultRolePerms)
					{
                        populateDefaultRolePermissionsAndUpdateUI(selectedRoles, selectedValues, false);
					}
				}
			}
		}

		function isUserInCurrentSelectedBranch(elementValue)
		{
			var branchID = <%= AspxTools.JsGetElementById(m_Branch) %>.value;

			if (typeof(RoleIdList) != 'undefined')
			{
			    for(var i = 0; i < RoleIdList.length; ++i)
			    {
				    if((RoleIdList[i] == elementValue) && (RoleUsersInBranchList[i] == branchID))
					    return true;
			    }
			}
			return false;
		}

		function addElementToDDL(ddl, elementText, elementValue, savedUser)
		{
			if(ddl.options.length == 0)
			{
				var option = document.createElement("OPTION");
				option.text = "<-- None -->";
				option.value = <%= AspxTools.JsString(Guid.Empty)%>;
				ddl.options.add(option);
			}
			if(elementText == null)
				return;

			var option = document.createElement("OPTION");
			option.text = elementText;
			option.value = elementValue;
			ddl.options.add(option);
		}

		function removeElementsFromDDL(ddl)
		{
			if(ddl != null)
			{
				for(var i = ddl.options.length-1; i >= 0; i--)
				{
					ddl.options.remove(i);
				}
			}
		}

		function getUserNameFromID(userID)
		{
			for(var i = 0; i < RoleUserIDList.length; ++i)
			{
				if(RoleUserIDList[i] == userID)
					return RoleUserNameList[i];
			}
			return null;
		}

		function setSelectedIndex(ddl, userID)
		{
			if(ddl != null)
			{
				for(var i = ddl.options.length-1; i >= 0; i--)
				{
					if(ddl.options[i].value == userID)
					{
						ddl.selectedIndex = i;
						return true;
					}
				}
				return false;
			}
		}

		function bindRelationshipDropdowns()
		{
			var managerDDL = document.getElementById('m_Manager');
			var lenderaeDDL = document.getElementById('m_LenderAcctExec');
			var processorDDL = document.getElementById('m_Processor');
			var juniorProcessorDDL = document.getElementById('m_JuniorProcessor');
			var loanOfficerAssistantDDL = document.getElementById('m_LoanOfficerAssistant');
			var underwriterDDL, juniorUnderwriterDDL ,lockDeskDDL;
			<% if ( IsPmlEnabled ) { %>
			underwriterDDL = document.getElementById('m_Underwriter');
			juniorUnderwriterDDL = document.getElementById('m_JuniorUnderwriter');
			lockDeskDDL = document.getElementById('m_LockDesk');
			<% } %>
			if ( (typeof(RoleIdList) != 'undefined') && (typeof(RoleUsersInBranchList) != 'undefined') && (typeof(RoleNameList) != 'undefined') && (typeof(RoleNameIdList) != 'undefined'))
			{
				for( var i = 0; i < RoleNameList.length; ++i )
				{
					var idList = RoleNameIdList[i].split('|');
					switch(RoleNameList[i])
					{
						case "Manager":
							var currentSelectedManager = document.getElementById('m_Manager').value;
							removeElementsFromDDL(managerDDL);
							var userID;

							for(var y = 0; y < idList.length; ++y)
							{
								userID = idList[y];
								addElementToDDL(managerDDL, getUserNameFromID(userID), userID, savedUser);
							}
							var savedUser;
							if(m_loadInitialManager)
							{
								savedUser = <%= AspxTools.JsString(m_initialSelectedManager) %>;
								m_loadInitialManager = false;
							}
							else
							{
								savedUser = currentSelectedManager;
							}
							setSelectedIndex(managerDDL, savedUser);
							break;

						case "Processor":
							var currentSelectedProcessor = document.getElementById('m_Processor').value;
							removeElementsFromDDL(processorDDL);
							var userID;

							for(var y = 0; y < idList.length; ++y)
							{
								userID = idList[y];
								addElementToDDL(processorDDL, getUserNameFromID(userID), userID, savedUser);
							}
							var savedUser;
							if(m_loadInitialProcessor)
							{
								savedUser = <%= AspxTools.JsString(m_initialSelectedProcessor) %>;
								m_loadInitialProcessor = false;
							}
							else
							{
								savedUser = currentSelectedProcessor;
							}
							setSelectedIndex(processorDDL, savedUser);
							break;
						case "JuniorProcessor":
							var currentSelectedJuniorProcessor = document.getElementById('m_JuniorProcessor').value;
							removeElementsFromDDL(juniorProcessorDDL);
							var userID;

							for(var y = 0; y < idList.length; ++y)
							{
								userID = idList[y];
								addElementToDDL(juniorProcessorDDL, getUserNameFromID(userID), userID, savedUser);
							}
							var savedUser;
							if(m_loadInitialJuniorProcessor)
							{
								savedUser = <%= AspxTools.JsString(m_initialSelectedJuniorProcessor) %>;
								m_loadInitialJuniorProcessor = false;
							}
							else
							{
								savedUser =  currentSelectedJuniorProcessor ;
							}
							setSelectedIndex(juniorProcessorDDL, savedUser);
							break;
						case "LoanOfficerAssistant":
							var currentSelectedLoanOfficerAssistant = document.getElementById('m_LoanOfficerAssistant').value;
							removeElementsFromDDL(loanOfficerAssistantDDL);
							var userID;

							for(var y = 0; y < idList.length; ++y)
							{
								userID = idList[y];
								addElementToDDL(loanOfficerAssistantDDL, getUserNameFromID(userID), userID, savedUser);
							}
							var savedUser;
							if(m_loadInitialLoanOfficerAssistant)
							{
								savedUser = <%= AspxTools.JsString(m_initialSelectedLoanOfficerAssistant) %>;
								m_loadInitialLoanOfficerAssistant = false;
							}
							else
							{
								savedUser =  currentSelectedLoanOfficerAssistant ;
							}
							setSelectedIndex(loanOfficerAssistantDDL, savedUser);
							break;
						case "LenderAccountExec":
							var currentSelectedLenderAE = document.getElementById('m_LenderAcctExec').value;
							removeElementsFromDDL(lenderaeDDL);
							var userID;

							for(var y = 0; y < idList.length; ++y)
							{
								userID = idList[y];
								addElementToDDL(lenderaeDDL, getUserNameFromID(userID), userID, savedUser);
							}
							var savedUser;
							if(m_loadInitialLenderAE)
							{
								savedUser = <%= AspxTools.JsString(m_initialSelectedLenderAE) %>;
								m_loadInitialLenderAE = false;
							}
							else
							{
								savedUser = currentSelectedLenderAE;
							}
							setSelectedIndex(lenderaeDDL, savedUser);
							break;
						<% if ( IsPmlEnabled ) { %>
						case "Underwriter":
							var currentSelectedUnderwriter = document.getElementById('m_Underwriter').value;
							removeElementsFromDDL(underwriterDDL);
							var userID;

							for(var y = 0; y < idList.length; ++y)
							{
								userID = idList[y];
								addElementToDDL(underwriterDDL, getUserNameFromID(userID), userID, savedUser);
							}
							var savedUser;
							if(m_loadInitialUnderwriter)
							{
								savedUser = <%= AspxTools.JsString(m_initialSelectedUnderwriter) %>;
								m_loadInitialUnderwriter = false;
							}
							else
							{
								savedUser = currentSelectedUnderwriter;
							}
							setSelectedIndex(underwriterDDL, savedUser);
							break;
						case "JuniorUnderwriter":
							var currentSelectedJuniorUnderwriter = document.getElementById('m_JuniorUnderwriter').value;
							removeElementsFromDDL(juniorUnderwriterDDL);
							var userID;

							for(var y = 0; y < idList.length; ++y)
							{
								userID = idList[y];
								addElementToDDL(juniorUnderwriterDDL, getUserNameFromID(userID), userID, savedUser);
							}
							var savedUser;
							if(m_loadInitialJuniorUnderwriter)
							{
								savedUser = <%= AspxTools.JsString(m_initialSelectedJuniorUnderwriter) %>;
								m_loadInitialJuniorUnderwriter = false;
							}
							else
							{
								savedUser =  currentSelectedJuniorUnderwriter ;
							}
							setSelectedIndex(juniorUnderwriterDDL, savedUser);
							break;
						case "LockDesk":
							var currentSelectedLockDesk = document.getElementById('m_LockDesk').value;
							removeElementsFromDDL(lockDeskDDL);
							var userID;

							for(var y = 0; y < idList.length; ++y)
							{
								userID = idList[y];
								addElementToDDL(lockDeskDDL, getUserNameFromID(userID), userID, savedUser);
							}
							var savedUser;
							if(m_loadInitialLockDesk)
							{
								savedUser = <%= AspxTools.JsString(m_initialSelectedLockDesk) %>;
								m_loadInitialLockDesk = false;
							}
							else
							{
								savedUser = currentSelectedLockDesk;
							}
							setSelectedIndex(lockDeskDDL, savedUser);
							break;
						<% } %>
					default:
						break;
					}
				}
			}
			if(managerDDL.options.length == 0)
				addElementToDDL(managerDDL, null, null, null);
			if(lenderaeDDL.options.length == 0)
				addElementToDDL(lenderaeDDL, null, null, null);
			if(processorDDL.options.length == 0)
				addElementToDDL(processorDDL, null, null, null);
			<% if ( IsPmlEnabled ) { %>
			if(underwriterDDL.options.length == 0)
				addElementToDDL(underwriterDDL, null, null, null);
			if(lockDeskDDL.options.length == 0)
				addElementToDDL(lockDeskDDL, null, null, null);
			<% } %>
		}

		function isUserInCorrectBranch(userID)
		{
			var branchID = <%= AspxTools.JsGetElementById(m_Branch) %>.value;
			if (typeof(RoleIdList) != 'undefined')
			{
			    for( var i = 0; i < RoleIdList.length; ++i )
			    {
				    if ( userID == RoleIdList[i] )
				    {
					    var roleBranchID = RoleUsersInBranchList[i];
					    if(roleBranchID != branchID)
						    return false;
					    break;
				    }
			    }
			}
			return true;
		}

		function onBranchChange()
		{
    		validateRoleChoices();
    		populateBranchCustomPricingPolicy();
		}

        function validateRoleChoices(isInitialLoad)
        {
            if (isInitialLoad) {
                bindRelationshipDropdowns();
            }

            $('select.relationship').change();

            return true;
        }

		function revokeClientCertificate(el, certificateId)
        {
            if(confirm('Do you want to revoke the client certificate?'))
            {
                var parentRow = $(el).closest("TR")[0];
                parentRow.style.display = "none";

                var certIDs = $("input[id$='certIDsToDelete']")[0];

                if(certIDs.value.indexOf(certificateId) == -1)
                {
                    if(certIDs.value == "")
                        certIDs.value += certificateId;
                    else
                        certIDs.value += "," + certificateId;
                }

                updateDirtyBit();
            }
        }

        function f_installClientCertificate()
        {
            showModal('/los/InstallClientCertificate.aspx', null, null, null, null, { hideCloseButton: true });
        }

        function deleteIp(el, id)
        {
            if(confirm('Do you want to delete the IP?'))
            {
                var parentRow = $(el).closest("TR")[0];
                parentRow.style.display = "none";

                var IpIDs = $("input[id$='IPsToDelete']")[0];
                if(IpIDs.value.indexOf(id) == -1)
                {
                    if(IpIDs.value == "")
                        IpIDs.value += id;
                    else
                        IpIDs.value += ","+id;
                }

                updateDirtyBit();
            }
        }

		function onTabClick( oTab )
		{
			try
			{
				if( oTab == null || oTab.id.indexOf( "Tab" ) == -1 )
					return;
				$('#TabA').removeClass('tab_selected');
				$('#TabA1').removeClass('tab_selected');
				$('#TabA2').removeClass('tab_selected');
				$('#TabB').removeClass('tab_selected');
				$('#TabC').removeClass('tab_selected');
				$('#TabD').removeClass('tab_selected');
				$('#TabE').removeClass('tab_selected');
				$('#TabF').removeClass('tab_selected');
				$('#TabG').removeClass('tab_selected');
				$('#TabS').removeClass('tab_selected');

				var baseA = document.getElementById("BaseA");
				var baseA1 = document.getElementById("BaseA1");
				var baseA2 = document.getElementById("BaseA2");
				var baseB = document.getElementById("BaseB");
				var baseC = document.getElementById("BaseC");
				var baseD = document.getElementById("BaseD");
				var baseE = document.getElementById("BaseE");
				var baseF = document.getElementById("BaseF");
				var baseG = document.getElementById("BaseG");
				var baseI = document.getElementById("BaseI");
				var baseS = document.getElementById('BaseS');

				baseA.style.display = "none";
				baseA1.style.display = "none";
				baseA2.style.display = "none";
				baseB.style.display = "none";
				baseC.style.display = "none";
				baseD.style.display = "none";
				baseE.style.display = "none";
				baseF.style.display = "none";
				baseG.style.display = "none";
				baseS.style.display = 'none';

				<% if( IsInternal == true ) { %>
				    $('#TabI').removeClass('tab_selected');
				    baseI.style.display = "none";
				<% } %>

				$(oTab).addClass("tab_selected");
				if ( <%= AspxTools.JsGetElementById(m_CurrentTab) %>.value  != oTab.id  ) Modal.Hide();
				switch( oTab.id )
				{
					case "TabA":
						baseA.style.display = "block";
						break;
					case "TabA1":
						baseA1.style.display = "block";
						break;
					case "TabA2":
						baseA2.style.display = "block";
						break;
					case "TabB":
						baseB.style.display = "block";
						break;
					case "TabC":
						baseC.style.display = "block";
						break;
					case "TabD":
						baseD.style.display = "block";
						break;
					case "TabE":
						baseE.style.display = "block";
						break;
				    case "TabF":
						baseF.style.display = "block";
						break;
					case "TabG":
					    baseG.style.display = "block";
					    break;
                    case 'TabS':
                        baseS.style.display = 'block';
                        break;
					<% if( IsInternal == true ) { %>
					case "TabI":
						baseI.style.display = "block";
						break;
					<% } %>
				}
				document.getElementById('<%= AspxTools.ClientId(m_CurrentTab) %>').value = oTab.id;
			}
			catch( e )
			{
				alert( e.Message );
			}
		}

        onTabClick( document.getElementById( document.getElementById('<%= AspxTools.ClientId(m_CurrentTab) %>').value ) );

		function f_closeExpirationDetails() {
			document.getElementById("ExpirationDetails").style.display = "none";
		}
		function f_showExpirationDetails() {
			var msg = document.getElementById("ExpirationDetails");
			msg.style.display = "";
			msg.style.top = "297px";
			msg.style.left = "72px";
			msg.style.zIndex="999999";
		}

		function setAmountTextboxStatus(cb) {
		    var id = cb.id.substring(0, cb.id.length - 7);
		    if (!cb.checked) {
		        document.getElementById(id).value = "";
		    }
		    document.getElementById(id).readOnly = !cb.checked;
		    document.getElementById(id + 'R').style.display = cb.checked ? "" : "none";
		}
        </script>
    </TD></TR>
	<tr>
		<td height=0%>
			<table cellSpacing=0 cellPadding=2 width="100%">
				 <tr>
					<td width="60%">
						<div id=m_ErrorsDiv>
							<ml:EncodedLabel id=m_AlertMessageHeader Text="Please correct the following item(s):" style="FONT: 12px arial; COLOR: red" runat="server"> </ml:EncodedLabel>
							<br>
							<div id=m_ErrorMsgDiv style="OVERFLOW: auto; PADDING-TOP: 0px; HEIGHT: 45px" align=left>
								<ml:EncodedLabel id=m_AlertMessage style="FONT: 12px arial; COLOR: red" Height="45px" runat="server"> </ml:EncodedLabel>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td noWrap align=right height=0%>
			<div style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 2px">
				<hr>
					<input id=m_OKseen style="WIDTH: 60px" onclick=onClientOkClick(); type=button value=" OK ">
					<input style="WIDTH: 60px" onclick=onClose(); type=button value=Cancel>
					<input id=m_Applyseen style="WIDTH: 60px" onclick=onClientApplyClick(); type=button value=Apply>
					<ml:nodoubleclickbutton Enabled="false" id=m_Apply style="DISPLAY: none" onclick=ApplyClick runat="server" Width="60" Text="Apply"></ml:nodoubleclickbutton>
					<ml:nodoubleclickbutton Enabled="false" id=m_Ok style="DISPLAY: none" onclick=OkClick runat="server" Width="60" Text="OK"></ml:nodoubleclickbutton>
					<input id=submitButton style="DISPLAY: none" type=submit value="Submit Query">
			</div>
			<div id=m_PasswordRules style="z-index: 900; BORDER-RIGHT: black 3px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 3px solid; DISPLAY: none; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 3px solid; WIDTH: 325px; PADDING-TOP: 5px; BORDER-BOTTOM: black 3px solid; POSITION: absolute; BACKGROUND-COLOR: whitesmoke">
				<table width="100%">
					<tr>
						<td>
                            <ml:PassthroughLiteral ID="PasswordGuidelines" runat="server"></ml:PassthroughLiteral>
						</td>
					</tr>
					<tr>
						<td align=center>[ <A id='m_closepwrules' onclick="Modal.Hide()"; href="#" >Close</A> ]</td>
					</tr>
				</table>
			</div>
		</td>
	</tr></TBODY></TABLE>

<input id=m_LoginControlStatus type=hidden value=new runat="server">
<input id=m_DefaultPermissions type=hidden name=m_DefaultPermissions runat="server">
<input id=m_Dirty type=hidden value=0 name=m_Dirty runat="server">
<input id=m_SelectedManager type=hidden value=0 name=m_SelectedManager runat="server">
<input id=m_SelectedLockDesk type=hidden value=0 name=m_SelectedLockDesk runat="server">
<input id=m_SelectedLenderAE type=hidden value=0 name=m_SelectedLenderAE runat="server">
<input id=m_SelectedUnderwriter type=hidden value=0 name=m_SelectedUnderwriter runat="server">
<input id=m_SelectedJuniorUnderwriter type=hidden value=0 name=m_SelectedJuniorUnderwriter runat="server">
<input id=m_SelectedProcessor type=hidden value=0 name=m_SelectedProcessor runat="server">
<input id=m_SelectedJuniorProcessor type=hidden value=0 name=m_SelectedJuniorProcessor runat="server">
<input id="m_SelectedLoanOfficerAssistant" type="hidden" value="0" name="m_SelectedLoanOfficerAssistant" runat="server">
<input id=m_SendWelcomeEmail type=hidden value=false runat="server">
<input id=m_IsAccountLocked type=hidden value=false runat="server">
<script>
<% if( IsInternal ) { %>
    function onEnableNewPmlUiClick() {
        // If the New PML UI is enabled at the broker level, then do not disable the Quick Pricer,
        // disable the New PML checkbox.
        if (<%= AspxTools.JsBool(IsNewPmlUIEnabledForBroker) %>) {
            // Check and disable the new PML checkbox
            <%= AspxTools.JsGetElementById(m_IsNewPmlUIEnabled) %>.checked = true;
            <%= AspxTools.JsGetElementById(m_IsNewPmlUIEnabled) %>.disabled = true;
            // <%= AspxTools.JsGetElementById(m_IsUserQuickPricerEnabled) %>.checked = false;
            // <%= AspxTools.JsGetElementById(m_IsUserQuickPricerEnabled) %>.disabled = true;
        }
        //OPM 112346
        /*
        else if (<%= AspxTools.JsGetElementById(m_IsNewPmlUIEnabled) %>.checked) {
            <%= AspxTools.JsGetElementById(m_IsUserQuickPricerEnabled) %>.checked = false;
            <%= AspxTools.JsGetElementById(m_IsUserQuickPricerEnabled) %>.disabled = true;
        }
        else {
            <%= AspxTools.JsGetElementById(m_IsUserQuickPricerEnabled) %>.disabled = false;
        }
        */

        // OPM 147501
        showHideShowQMStatusInPml2();
    }

    function showHideShowQMStatusInPml2()
    {
        var IsSetBroker_ShowQMStatus = <%= AspxTools.JsBool(ShowQMStatusInPml2) %>;
        var IsSetBroker_NewPmlUI = <%= AspxTools.JsBool(IsNewPmlUIEnabledForBroker) %>;
        var IsNewPMLBoxChecked = <%= AspxTools.JsGetElementById(m_IsNewPmlUIEnabled) %>.checked;

        if (IsSetBroker_ShowQMStatus || (!IsSetBroker_NewPmlUI && !IsNewPMLBoxChecked)) {
            $('#trShowQMStatusInPml2').hide();
        } else {
            $('#trShowQMStatusInPml2').show();
        }
    }

<% } %>

	function onDoLoginTextChange() {
        var loginText = <%= AspxTools.JsGetElementById(m_doLogin) %>.value;
        var pwText = <%= AspxTools.JsGetElementById(m_doPw) %>.value;
        if (<%= AspxTools.JsBool(IsNewEmp) %>) {
            if (pwText == "" && loginText != "")
                <%= AspxTools.JsGetElementById(m_doPasswordR) %>.style.display = "";
            else {
                <%= AspxTools.JsGetElementById(m_doPasswordR) %>.style.display = "none";
                ClearErrorMessage('DOPassword');
            }
        }
    }

    function onDuLoginTextChange() {
        var loginText = <%= AspxTools.JsGetElementById(m_duLogin) %>.value;
        var pwText = <%= AspxTools.JsGetElementById(m_duPw) %>.value;
        if (<%= AspxTools.JsBool(IsNewEmp) %>) {
            if (pwText == "" && loginText != "")
                <%= AspxTools.JsGetElementById(m_duPasswordR) %>.style.display = "";
            else {
                <%= AspxTools.JsGetElementById(m_duPasswordR) %>.style.display = "none";
                ClearErrorMessage('DUPassword');
            }
        }
    }

	function disableErrorMessages(toReplace)
	{
		if(!toReplace)
		{
			<%= AspxTools.JsGetElementById(m_AlertMessage) %>.innerText = "";
			m_ErrorsDiv.style.display = "none";
			return;
		}
		var alertMsg = <%= AspxTools.JsGetElementById(m_AlertMessage) %>;

		var innerText = alertMsg.innerText;
		var index = innerText.indexOf(toReplace);
		if(index == 0)
			alertMsg.innerText = innerText.substring(toReplace.length + 2, innerText.length);
		else
		{
			var firstHalf = innerText.substring(0, innerText.indexOf(toReplace)-1);
			var secondHalf = innerText.substring(innerText.indexOf(toReplace) + toReplace.length +1, innerText.length);
			alertMsg.innerText = firstHalf + secondHalf;
		}

		if(alertMsg.innerText == "")
			m_ErrorsDiv.style.display = "none";
	}

	function enableErrorMessages()
	{
		m_ErrorsDiv.style.display = "";
	}

	function ClearErrorMessage(field)
	{
		var alertMessage = <%= AspxTools.JsGetElementById(m_AlertMessage) %>;
		switch(field)
		{
			case "FirstName":
				var index = alertMessage.innerText.indexOf(<%= AspxTools.JsString(JsMessages.SpecifyFirstName) %>);
				if(index != -1)
					disableErrorMessages(<%= AspxTools.JsString(JsMessages.SpecifyFirstName) %>);
				break;
			case "LastName":
				var index = alertMessage.innerText.indexOf(<%= AspxTools.JsString(JsMessages.SpecifyLastName) %>);
				if(index != -1)
					disableErrorMessages(<%= AspxTools.JsString(JsMessages.SpecifyLastName) %>);
				break;
			case "Email":
				var index = alertMessage.innerText.indexOf(<%= AspxTools.JsString(JsMessages.SpecifyValidEmail) %>);
				if(index != -1)
					disableErrorMessages(<%= AspxTools.JsString(JsMessages.SpecifyValidEmail) %>);
				break;
			case "Phone":
				var index = alertMessage.innerText.indexOf(<%= AspxTools.JsString(JsMessages.SpecifyValidContactPhoneNumber) %>);
				if(index != -1)
					disableErrorMessages(<%= AspxTools.JsString(JsMessages.SpecifyValidContactPhoneNumber) %>);
				break;
			case "CellPhone":
				var index = alertMessage.innerText.indexOf(<%= AspxTools.JsString(JsMessages.SpecifyValidCellPhoneNumber) %>);
				if(index != -1)
					disableErrorMessages(<%= AspxTools.JsString(JsMessages.SpecifyValidCellPhoneNumber) %>);
				break;
			case "Login":
				var index = alertMessage.innerText.indexOf(<%= AspxTools.JsString(JsMessages.SpecifyLoginName) %>);
				var index2 = alertMessage.innerText.indexOf(<%= AspxTools.JsString(JsMessages.EmployeeCanLoginButNoLoginSpecified) %>);
				if(index != -1)
					disableErrorMessages(<%= AspxTools.JsString(JsMessages.SpecifyLoginName) %>);
				if(index2 != -1)
					disableErrorMessages(<%= AspxTools.JsString(JsMessages.EmployeeCanLoginButNoLoginSpecified) %>);
				break;
			case "DOPassword":
				var index = alertMessage.innerText.indexOf(<%= AspxTools.JsString(JsMessages.SpecifyDOPassword) %>);
				if(index != -1)
					disableErrorMessages(<%= AspxTools.JsString(JsMessages.SpecifyDOPassword) %>);
				break;
			case "DUPassword":
				var index = alertMessage.innerText.indexOf(<%= AspxTools.JsString(JsMessages.SpecifyDUPassword) %>);
				if(index != -1)
					disableErrorMessages(<%= AspxTools.JsString(JsMessages.SpecifyDUPassword) %>);
				break;
			default:
				break;
		}
	}

	function onUnlockAccountClick()
	{
		if(confirm("Unlock this employee's account?"))
		{
			var args = new Object();
			args["login"] = <%= AspxTools.JsGetElementById(m_Login) %>.value;
			args["isPml"] = "false";

			var result = gService.passwordvalidate.call("UnlockAccount", args);
			if(result.error)
				alert("Unable to unlock account");
			else
				<%= AspxTools.JsGetElementById(m_UnlockAccountPanel) %>.style.display = "none";
		}
	}

	function ValidateForm()
	{
		var errorMsg = "";

		var isValid = true;
		var firstName = <%= AspxTools.JsGetElementById(m_FirstName) %>;
		if( firstName.value.replace(/^\s*|\s*$/g,"") == "")
		{
			firstName.style.backgroundColor = "lightyellow";
			errorMsg = '<br/>' + <%= AspxTools.JsString(JsMessages.SpecifyFirstName) %>;
			isValid = false;
		}

		var lastName = <%= AspxTools.JsGetElementById(m_LastName) %>;
		if( lastName.value.replace(/^\s*|\s*$/g,"") == "")
		{
			lastName.style.backgroundColor = "lightyellow";
			errorMsg += '<br/>' + <%= AspxTools.JsString(JsMessages.SpecifyLastName) %>;
			isValid = false;
		}


		var email = <%= AspxTools.JsGetElementById(m_Email) %>;
		if((email.value.replace(/^\s*|\s*$/g,"") == "") || (!<%= AspxTools.JsGetElementById(RegularExpressionValidator1) %>.isvalid))
		{
			email.style.backgroundColor = "lightyellow";
			errorMsg += '<br/>' + <%= AspxTools.JsString(JsMessages.SpecifyValidEmail)%>;
			isValid = false;
		}

		var phone = <%= AspxTools.JsGetElementById(m_Phone) %>;
		if( phone.value.replace(/^\s*|\s*$/g,"") == "")
		{
			phone.style.backgroundColor = "lightyellow";
			errorMsg += '<br/><%= AspxTools.JsStringUnquoted(JsMessages.SpecifyValidContactPhoneNumber) %>';
			isValid = false;
		}

		if(<%= AspxTools.JsBool(IsEnableMultiFactorAuthentication)%>)
		{
		    var enableAuthCodeSmsCheckbox = (<%=AspxTools.JsGetElementById(EnableAuthCodeViaSms) %>);

		    if(enableAuthCodeSmsCheckbox != null && enableAuthCodeSmsCheckbox.checked)
		    {
		        var cell = <%= AspxTools.JsGetElementById(m_Cell) %>;
		        if( cell.value.replace(/^\s*|\s*$/g,"") == "")
		        {
			        cell.style.backgroundColor = "lightyellow";
			        errorMsg += '<br/><%= AspxTools.JsStringUnquoted(JsMessages.SpecifyValidCellPhoneNumber) %>';
			        isValid = false;
		        }
		    }
		}

		var expiresOn = <%= AspxTools.JsGetElementById(m_PasswordExpiresOn) %>;
		var expirationDate = <%= AspxTools.JsGetElementById(m_ExpirationDate) %>;
		if(expiresOn.checked && (expirationDate.value.replace(/^\s*|\s*$/g,"") == ""))
		{
			errorMsg += '<br/><%= AspxTools.JsStringUnquoted(JsMessages.SpecifyValidPwExpirationDate) %>';
			isValid = false;
		}

		var changeLogin = <%= AspxTools.JsGetElementById(m_ChangeLogin) %>;
		if(changeLogin.checked)
		{
			var password = <%= AspxTools.JsGetElementById(m_Password) %>;
			var confirm = <%= AspxTools.JsGetElementById(m_Confirm) %>;

			if( (password.value.replace(/^\s*|\s*$/g,"") != "" || confirm.value.replace(/^\s*|\s*$/g,"") != "" ) && (password.value != confirm.value) )
			{
				password.style.backgroundColor = "lightyellow";
				confirm.style.backgroundColor = "lightyellow";
				errorMsg += '<br/><%= AspxTools.JsStringUnquoted(JsMessages.PasswordsDontMatch) %>';
				isValid = false;
			}

			var login = <%= AspxTools.JsGetElementById(m_Login) %>;
			if(login.value.replace(/^\s*|\s*$/g,"") == "")
			{
				login.style.backgroundColor = "lightyellow";
				errorMsg += '<br/><%= AspxTools.JsStringUnquoted(JsMessages.SpecifyLoginName) %>';
				isValid = false;
			}
		}
		else
		{
			var allowLogin = <%= AspxTools.JsGetElementById(m_CanLogin) %>;
			if ( !(<%= AspxTools.JsBool(m_HasLogin) %>) && allowLogin.checked)
			{
				errorMsg += '<br/><%= AspxTools.JsStringUnquoted(JsMessages.EmployeeCanLoginButNoLoginSpecified) %>';
				isValid = false;
			}
		}

		var hasARole = false;
		if(window.roleIDArray)
		{
			for(var i = 0; i < roleIDArray.length; ++i)
			{
				var element = document.getElementById(roleIDArray[i]);
				if(element.checked)
				{
					hasARole = true;
					break;
				}
			}
			if(!hasARole)
			{
				errorMsg += '<br/><%= AspxTools.JsStringUnquoted(JsMessages.SpecifyRole) %>';
				isValid = false;
			}
		}

		if(<%=AspxTools.JsGetElementById(m_doPasswordR)%> && <%=AspxTools.JsGetElementById(m_doPasswordR)%>.style.display == "")
		{
			errorMsg += '<br/>' + <%=AspxTools.JsString(JsMessages.SpecifyDOPassword)%>;
			isValid = false;
		}

		if(<%=AspxTools.JsGetElementById(m_duPasswordR)%> && <%=AspxTools.JsGetElementById(m_duPasswordR)%>.style.display == "")
		{
			errorMsg += '<br/>' + <%=AspxTools.JsString(JsMessages.SpecifyDUPassword)%>;
			isValid = false;
		}

		<% if (PmlAutoLoginOption == "MustBeUnique") { %>
		    checkDODU();
			isValid = isValid && passedDu && passedDo;


			if ( !passedDu	|| !passedDo )
			{
				if ( errorMsg != '' )
				{
					errorMsg += '<br/>';
				}
				var  needAnd = false;
				if ( !passedDo )
				{
					errorMsg += 'DO';
					needAnd = true;
				}
				if ( !passedDu )
				{
					if ( needAnd ) errorMsg += "/";
					errorMsg += 'DU';
				}

				errorMsg += " login" + ( needAnd  && !passedDu ? "s are "  : " is " ) + "already saved in different account" + ( needAnd  && !passedDu ? "s"  : "" ) + "." + "&nbsp;<a class='handcursor' style='text-decoration: underline; ' onclick=\"Modal.Show('<%= AspxTools.ClientId(m_dudoSearch) %>' )\">Show users with same login</a>";
			}
        <% } %>

        var supportEmail = <%= AspxTools.JsGetElementById(m_SupportEmailInternal) %>;

        // The support email will be null when an employee is being edited through the
        // regular pipeline link and not LOAdmin.
        if(supportEmail != null && supportEmail.value != "" && (!<%= AspxTools.JsGetElementById(SupportEmailValidator) %>.isvalid))
		{
		    supportEmail.style.backgroundColor = "lightyellow";
            errorMsg += '<br/>' + <%= AspxTools.JsString(JsMessages.SpecifyValidSupportEmail)%>;
            isValid = false;
        }

		if(isValid == true)
			disableErrorMessages();
		else
		{
			enableErrorMessages();
			var alertMessage = <%= AspxTools.JsGetElementById(m_AlertMessage) %>;
			alertMessage.innerHTML = errorMsg.replace(/\n/, "");
		}

		return isValid;
	}

	<% if (PmlAutoLoginOption == "MustBeUnique") { %>
	var passedDo = false;
	var passedDu = false;
	function checkDODU()
	{
	    var duEnabled = <%=AspxTools.JsBool(IsDUEnabled && UsingLegacyDUCredentials)%>;
        var doEnabled = <%=AspxTools.JsBool(IsDOEnabled)%>;
		var args = new Object();
		args['id'] = <%= AspxTools.JsString(ViewState["userid"].ToString()) %>;
		args['brokerId'] = <%= AspxTools.JsString(ViewState["brokerId"].ToString()) %>;
		args['checkDo'] = doEnabled && <%=AspxTools.JsGetElementById(m_doLogin)%> != null && <%=AspxTools.JsGetElementById(m_doLogin)%>.value.length != 0;
		args['checkDu'] = duEnabled && <%=AspxTools.JsGetElementById(m_duLogin)%> != null && <%=AspxTools.JsGetElementById(m_duLogin)%>.value.length != 0;

        if(duEnabled) {
            args['dulogin'] = <%=AspxTools.JsGetElementById(m_duLogin)%>.value;
        }

	    if(doEnabled) {
            args['dologin'] = <%=AspxTools.JsGetElementById(m_doLogin)%>.value;
	    }

		if (!args['checkDo'] && !args['checkDu'])
		{
			passedDo = true;
			passedDu = true;
			return;
		}

		var result = gService.utilities.call( "CheckDODULoginFields", args );
		if ( result.value && result.value["error"] )
		{
			alert(result.value["error"]);
			passedDo = false;
			passedDu = false;
			return;
		}
		passedDo = result.value["IsDoUnique"] == "True";
		passedDu = result.value["IsDuUnique"] == "True";

		if ( !passedDo || !passedDu )
		{
			DUDO.Activate(!passedDo ? <%=AspxTools.JsGetElementById(m_doLogin)%>.value : '',!passedDu ? <%=AspxTools.JsGetElementById(m_duLogin)%>.value : '' );
		}
	}

	<% } %>

	function ValidatePasswords(callback)
	{
	    if(!(<%= AspxTools.JsGetElementById(m_ChangeLogin) %>.checked)) {
	        callback(true);
	        return;
	    }
		var args = new Object();
		args["pw"] = <%= AspxTools.JsGetElementById(m_Password) %>.value;
		args["retype"] = <%= AspxTools.JsGetElementById(m_Confirm) %>.value;
		args["id"] = <%= AspxTools.JsString(ViewState[ "EmployeeId" ].ToString()) %>;
		<% if (this.IsInternal) {%>
		args["BrokerId"] = <%= AspxTools.JsString(DataBroker) %>;
		<% } %>

		args["isPml"] = "false";
		<% if( IsActiveDirectoryAuthEnabledForLender  ) { %>
		    args["isADUser"] = <%= AspxTools.JsGetElementById(m_IsActiveDirectoryUser) %>.checked;
        <% } %>

		var result = gService.passwordvalidate.call("ValidatePassword", args);
		if(result.value["ErrorMessage"])
		{
			if(result.value["ErrorMessage"] == <%= AspxTools.JsString(ErrorMessages.PasswordsShouldNotBeRecycled) %>)
			{
            <% if (this.IsInternal) {%>
                showModal("/loadmin/broker/ConfirmRepeatPasswordOverride.aspx", null, "Confirm Repeat Password Override", null, function(arg){
            <% } else { %>
                showModal("/los/BrokerAdmin/ConfirmRepeatPasswordOverride.aspx", null, "Confirm Repeat Password Override", null, function(arg){
            <% } %>
                    if(typeof(arg.choice) == 'undefined' || arg.choice == 1)
                    {
                        <%= AspxTools.JsGetElementById(m_AlertMessage) %>.innerText += result.value["ErrorMessage"];
                        <%= AspxTools.JsGetElementById(m_Password) %>.style.backgroundColor = "lightyellow";
                        <%= AspxTools.JsGetElementById(m_Confirm) %>.style.backgroundColor = "lightyellow";
                        enableErrorMessages();
                        callback(false);
                        return;
                    }

                    if(arg.choice == 0) {
                        callback(true);
                        return;
                    }
                },
                { hideCloseButton: true });	
            }
            else {
                <%= AspxTools.JsGetElementById(m_AlertMessage) %>.innerText += result.value["ErrorMessage"];
                <%= AspxTools.JsGetElementById(m_Password) %>.style.backgroundColor = "lightyellow";
                <%= AspxTools.JsGetElementById(m_Confirm) %>.style.backgroundColor = "lightyellow";
                enableErrorMessages();
                callback(false);
                return;
            }
        }
        else {
            callback(true);
            return;
        }
        
	}

	function ValidateFields()
	{
		var args = new Object();
		args["pw"] = <%= AspxTools.JsGetElementById(m_Password) %>.value;
		args["hasLogin"] = <%= AspxTools.JsBool(m_HasLogin) %>;
		args["id"] = <%= AspxTools.JsString(ViewState[ "EmployeeId" ].ToString()) %>;
		args["firstName"] = <%= AspxTools.JsGetElementById(m_FirstName) %>.value;
		args["lastName"] = <%= AspxTools.JsGetElementById(m_LastName) %>.value;
		args["login"] = <%= AspxTools.JsGetElementById(m_Login) %>.value;
		args["expirationDate"] = <%= AspxTools.JsGetElementById(m_ExpirationDate) %>.value;
		args["passwordExpiresOnChecked"] = <%= AspxTools.JsGetElementById(m_PasswordExpiresOn) %>.checked;
		args["changeLoginChecked"] = <%= AspxTools.JsGetElementById(m_ChangeLogin) %>.checked;
		args["isPml"] = "false";
		args["originatorCompensationMinAmount"] = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmount)%>.value;
		args["originatorCompensationMaxAmount"] = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmount)%>.value;
		args["originatorCompensationMinAmountEnabled"] = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmountEnabled)%>.checked;
		args["originatorCompensationMaxAmountEnabled"] = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmountEnabled)%>.checked;

        <% if (this.IsInternal) {%>
		args["BrokerId"] = <%= AspxTools.JsString(DataBroker) %>;
		<% } %>

		<% if( IsActiveDirectoryAuthEnabledForLender  ) { %>
		    args["isADUser"] = <%= AspxTools.JsGetElementById(m_IsActiveDirectoryUser) %>.checked;
        <% } %>

		var result = gService.passwordvalidate.call("ValidateFields", args);
		if(result.value["ErrorMessage"])
		{
			<%= AspxTools.JsGetElementById(m_AlertMessage) %>.innerText += result.value["ErrorMessage"];
			enableErrorMessages();
			return false;
		}
		return true;
	}

	function ValidateLicenses(){
	    if (typeof AreLicensesValid == 'undefined')
	        return true;
	    if (!AreLicensesValid()){
	        <%= AspxTools.JsGetElementById(m_AlertMessage) %>.innerText += 'The following fields are required: License #, Expiration Date.';
	        enableErrorMessages();
	        return false;
	    }
		return true;
	}

	function ValidateTeamRoles()
	{
	    var prompt = false;
	    var count = 0;
	    var roleLabels = "";
	    $(".RoleLabel").each(function(){
	        var idVal = $(this).attr("idVal");
	        var roleLabel  = $(this).text();
	        //get the checkbox
	        var checkbox = $("[id='m_Edit_m_EmployeeRoles_"+idVal+"']");
	        if(checkbox.length > 0 && !checkbox[0].checked)
	        {
	            count++;
	            prompt = true;
	            roleLabels += roleLabel + " ";
	        }
	    });

	    if(prompt)
	    {
	        if(count > 1)
	        {
	            return confirm("Employee is a member of one or more teams that require the types " + roleLabels +" for membership. Removing those roles will remove the employee from those teams. Do you still wish to proceed?");
	        }
	        else{
	            return confirm("Employee is a member of one or more teams that require the type " + roleLabels +" for membership. Removing that role will remove the employee from those teams. Do you still wish to proceed?");
	        }
	    }
	    else{
	        return true;
	    }

	}

	function onClientApplyClick()
	{
	    if(isUserEditorDirty()) {
            ValidatePasswords(function (isPasswordValid) {
                if ( isPasswordValid && isUserEditorDirty() && ValidateForm() && ValidateFields() && ValidateLicenses() && ValidateTeamRoles())
                {
                    <%= AspxTools.JsGetElementById(m_SelectedManager) %>.value = document.getElementById('m_Manager').value;
                    <%= AspxTools.JsGetElementById(m_SelectedLenderAE) %>.value = document.getElementById('m_LenderAcctExec').value;
                    <%= AspxTools.JsGetElementById(m_SelectedProcessor) %>.value = document.getElementById('m_Processor').value;
                    <%= AspxTools.JsGetElementById(m_SelectedJuniorProcessor) %>.value = document.getElementById('m_JuniorProcessor').value;
                    <%= AspxTools.JsGetElementById(m_SelectedLoanOfficerAssistant) %>.value = document.getElementById('m_LoanOfficerAssistant').value;
                    <% if ( IsPmlEnabled ) { %>
                    <%= AspxTools.JsGetElementById(m_SelectedUnderwriter) %>.value = document.getElementById('m_Underwriter').value;
                    <%= AspxTools.JsGetElementById(m_SelectedJuniorUnderwriter) %>.value = document.getElementById('m_JuniorUnderwriter').value;
                    <%= AspxTools.JsGetElementById(m_SelectedLockDesk) %>.value = document.getElementById('m_LockDesk').value;
                    <%}%>

                    prepareCustomLOPricingPolicyInfo();
                    confirmSendWelcomeEmail(function(){
                        saveAllLicenses();
                        saveTeamData();
                        var applyBtn = <%= AspxTools.JsGetElementById(m_Apply) %>;
                        applyBtn.disabled = false; 
                        applyBtn.click();
                    });
                }
            });
	    }
	}

    function confirmSendWelcomeEmail(callback)
	{
		var allowLogin = <%= AspxTools.JsGetElementById(m_CanLogin) %>;
		if ( !(<%= AspxTools.JsBool(m_HasLogin) %>) && allowLogin.checked)
		{
			var emailVal = <%= AspxTools.JsGetElementById(m_Email) %>.value;
			var arg;
			<% if (this.IsInternal) {%>
		    showModal("/loadmin/broker/ConfirmSendWelcomeEmail.aspx?email=" + emailVal, null, "Confirm Send Welcome Email", null, function(arg){
		        <% } else { %>
		        showModal("/los/BrokerAdmin/ConfirmSendWelcomeEmail.aspx?email=" + emailVal, null, "Confirm Send Welcome Email", null, function(arg){
		            <% } %>

		            if(typeof(arg.choice) == 'undefined' || arg.choice != 0)
		                <%= AspxTools.JsGetElementById(m_SendWelcomeEmail) %>.value = false;

		            if(arg.choice == 0)
		                <%= AspxTools.JsGetElementById(m_SendWelcomeEmail) %>.value = true;

		            callback();
		        },
            { hideCloseButton: true });	

		    }
		    else {
            callback();
		}
	}

	function onClose()
	{
		if ( isUserEditorDirty() )
		{
			if( confirm( "Close without saving?" ) == false )
				return;
		}
		onClosePopup();
	}

	function prePostbackClientSideSave()
	{
	    saveAllLicenses();
	}

	function onClientOkClick()
	{
		if ( isUserEditorDirty() )
		{
		    ValidatePasswords(function(isPasswordValid){
		        if( isPasswordValid && ValidateForm() && ValidateFields() && ValidateLicenses() && ValidateTeamRoles())
		        {
		            <%= AspxTools.JsGetElementById(m_SelectedManager) %>.value = document.getElementById('m_Manager').value;
		            <%= AspxTools.JsGetElementById(m_SelectedProcessor) %>.value = document.getElementById('m_Processor').value;
		            <%= AspxTools.JsGetElementById(m_SelectedJuniorProcessor) %>.value = document.getElementById('m_JuniorProcessor').value;
		            <%= AspxTools.JsGetElementById(m_SelectedLoanOfficerAssistant) %>.value = document.getElementById('m_LoanOfficerAssistant').value;
		            <%= AspxTools.JsGetElementById(m_SelectedLenderAE) %>.value = document.getElementById('m_LenderAcctExec').value;
		            <% if ( IsPmlEnabled ) { %>
		            <%= AspxTools.JsGetElementById(m_SelectedUnderwriter) %>.value = document.getElementById('m_Underwriter').value;
		            <%= AspxTools.JsGetElementById(m_SelectedJuniorUnderwriter) %>.value = document.getElementById('m_JuniorUnderwriter').value;
		            <%= AspxTools.JsGetElementById(m_SelectedLockDesk) %>.value = document.getElementById('m_LockDesk').value;
		            <%}%>

		            prepareCustomLOPricingPolicyInfo();
		            confirmSendWelcomeEmail(function() {
		                saveAllLicenses();
		                saveTeamData();
		                var okBtn = <%= AspxTools.JsGetElementById(m_Ok) %>;
		                okBtn.disabled = false;
		                okBtn.click(); 
		            });
		        }
		    });
		}
		else
		{

			onClosePopup();
        }
	}

	function _init()
	{
	    addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(RegularExpressionValidator1) %>);
        var supportEmailValidator = <%= AspxTools.JsGetElementById(SupportEmailValidator) %>;
	    if(supportEmailValidator) {
            addTrimmingToAspRegExValidator(supportEmailValidator);
	    }

	    if(<%=AspxTools.JsBool(!EnableTeamsUI) %>)
	    {
	        $("#TabF").css("display", "none");
	    }

		document.getElementById('m_Applyseen').disabled = ! isUserEditorDirty();
		<%= AspxTools.JsGetElementById(m_ExpirationPeriod) %>.disabled = <%= AspxTools.JsBool(m_expirationPeriodDisabled) %>;
		<%= AspxTools.JsGetElementById(m_CycleExpiration) %>.disabled = <%= AspxTools.JsBool(m_cycleExpirationDisabled) %>;
		<%= AspxTools.JsGetElementById(m_SendWelcomeEmail) %>.value = false;
		if(<%= AspxTools.JsGetElementById(m_AlertMessage) %>.innerText == "")
			disableErrorMessages();

		var unassignLicenseLink = <%= AspxTools.JsGetElementById(m_UnassignLicenseLink) %>;
		var assignLicenseLink = <%= AspxTools.JsGetElementById(m_AssignLicenseLink) %>;
		var licenseNumber = <%= AspxTools.JsGetElementById(m_LicenseNumber) %>;
		if(licenseNumber && unassignLicenseLink && licenseNumber.value == "")
		{
		    setDisabledAttr(unassignLicenseLink, true);
			unassignLicenseLink.style.textDecorationNone = true;
			unassignLicenseLink.style.cursor = "default";
		}

		var allowLogin = <%= AspxTools.JsGetElementById(m_CanLogin) %>;
		if ( !allowLogin.checked && assignLicenseLink)
		{
		    setDisabledAttr(assignLicenseLink, true);
			assignLicenseLink.style.textDecorationNone = true;
			assignLicenseLink.style.cursor = "default";
		}

		var expDate = <%= AspxTools.JsGetElementById(m_ExpirationDate) %>;
		if(<%= AspxTools.JsBool(m_expirationDateDisabled) %>)
		{
			expDate.disabled = true;
			expDate.style.backgroundColor = "gainsboro";
		}
		else
		{
			expDate.disabled = false;
			expDate.style.backgroundColor = "white";
		}

		setDefaultDisableEnable();

		//if the account is locked, display the account locked message and unlock button
		if(<%= AspxTools.JsGetElementById(m_IsAccountLocked) %>.value == "true")
			<%= AspxTools.JsGetElementById(m_UnlockAccountPanel) %>.style.display = "";
		else
			<%= AspxTools.JsGetElementById(m_UnlockAccountPanel) %>.style.display = "none";

		var element = <%= AspxTools.JsGetElementById(m_roleNote) %>;
		var isChecked = IsChecked(<%= AspxTools.JsString(Permission.AllowLoanAssignmentsToAnyBranch.ToString()) %>);

		if(element != null)
		{
			if(!isChecked)
			{
				element.style.display = "";
				element.innerText = <%= AspxTools.JsString(JsMessages.OnlyDisplayingUsersFromSameBranch) %> + "\n";
			}
			else
				element.style.display = "none";
		}

		m_loadInitialManager = true;
		m_loadInitialLockDesk = true;
		m_loadInitialLenderAE = true;
		m_loadInitialUnderwriter = true;
		m_loadInitialJuniorUnderwriter = true;
		m_loadInitialProcessor = true;
		m_loadInitialJuniorProcessor = true;
		m_loadInitialLoanOfficerAssistant = true;

		changeLoginWasChecked = false;
		validateRoleChoices(true);
		if ( typeof(Licensing) !== "undefined" ) {
		    Licensing.UpdateDisplay();
		}

	    <% if ((IsDOEnabled || IsDUEnabled) && PmlAutoLoginOption == "MustBeUnique") { %>
			DUDO.Init(<%= AspxTools.JsString(m_EmployeeId) %>) ;
		<% } %>

		LoadGroupList();

		var MinAmountEnabled = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmountEnabled)%>;
		var MaxAmountEnabled = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmountEnabled)%>;
		var MinAmountVal = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmount)%>.value;
		var MaxAmountVal = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmount)%>.value;
		var ZeroDollarStr = <%= AspxTools.JsString(m_ZeroDollarStr)%>
		MinAmountEnabled.checked = MinAmountVal.length > 0 && MinAmountVal != ZeroDollarStr;
		MaxAmountEnabled.checked = MaxAmountVal.length > 0 && MaxAmountVal != ZeroDollarStr;
		setAmountTextboxStatus(<%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmountEnabled)%> );
		setAmountTextboxStatus(<%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmountEnabled)%> );

		<% if( IsInternal ) { %>
		    onEnableNewPmlUiClick();
		<% } %>

		checkEnableCustomPricingPolicyControls();
	}

	function hasRole(friendlyRoleName){
	    if( typeof(window.roleIDArray) === 'undefined' || typeof(window.roleNameArray) === 'undefined' ) {
	        return false;
	    }

	    for( var i = 0; i < roleNameArray.length; i++) {
	        if( roleNameArray[i] === friendlyRoleName ) {
	            var element = document.getElementById(roleIDArray[i]);
	            return element.checked;
	        }
	    }
	    return false;
	}

	function expirationPolicyClick()
	{
		<%= AspxTools.JsGetElementById(m_Dirty) %>.value = "1";

		var expiresOn = <%= AspxTools.JsGetElementById(m_PasswordExpiresOn) %>;
		var mustChangePw = <%= AspxTools.JsGetElementById(m_MustChangePasswordAtNextLogin) %>;
		var cycleExpiration = <%= AspxTools.JsGetElementById(m_CycleExpiration) %>;
		var expirationPeriod = <%= AspxTools.JsGetElementById(m_ExpirationPeriod) %>;
		var expirationDate = <%= AspxTools.JsGetElementById(m_ExpirationDate) %>;

		if( expiresOn.checked == true || mustChangePw.checked == true )
		{
			expirationCycleClick();
			cycleExpiration.disabled = false;
		}
		else
		{
			expirationPeriod.disabled = true;
			cycleExpiration.disabled  = true;
			expirationDate.style.backgroundColor="gainsboro";
			cycleExpiration.checked = false;
		}

		if( expiresOn.checked == true )
		{
			expirationDate.disabled = false;
			expirationDate.style.backgroundColor="white";
		}
		else
		{
			expirationDate.disabled = true;
			expirationDate.style.backgroundColor="gainsboro";
		}
	}

	function expirationCycleClick()
	{
		<%= AspxTools.JsGetElementById(m_Dirty) %>.value = "1";
		<%= AspxTools.JsGetElementById(m_ExpirationPeriod) %>.disabled = (<%= AspxTools.JsGetElementById(m_CycleExpiration) %>.checked)?false:true;
	}


	function updateDirtyBit_callback()
	{
		// This functionality is based on the keyup event, which makes
		// it too sensitive.  In the future, we could filter out some
		// of the situations where the data would not change, such as
		// Control-C if someone wants to copy data with out marking as
		// dirty.  For now, we stay in sync with our system framework.
		//
		// 12/01/06 db - since there are no more postbacks for changing UI elements,
		// we check to see if the "change login and password" box was the only element changed,
		// and if so, keep the apply button disabled

		var dirtyBit = <%= AspxTools.JsGetElementById(m_Dirty) %>;
		var changeLogin = <%= AspxTools.JsGetElementById(m_ChangeLogin) %>;
		if( (changeLogin.checked != changeLoginWasChecked) && (dirtyBit.value == "0"))
		{
			changeLoginWasChecked = changeLogin.checked;
			return;
		}
		document.getElementById('m_Applyseen').disabled = false;
		dirtyBit.value = "1";
	}

	function isUserEditorDirty()
	{
		return <%= AspxTools.JsGetElementById(m_Dirty) %>.value == "1";
	}




	<%-- // 09/14/06 mf OPM 2650. --%>
	function onBranchCopyClick()
	{
		var branchId = <%= AspxTools.JsGetElementById(m_Branch) %>.value;
		if ( typeof(BranchIdList) != 'undefined'
			&& typeof(BranchInfoList) != 'undefined' )
		{
			var dataList;
			for( var i = 0; i < BranchIdList.length; i++ )
			{
				if ( branchId == BranchIdList[i] )
				{
					dataList = BranchInfoList[i].split('|');
					break;
				}
			}

			if ( dataList && dataList.length >= 4 )
			{
				<%= AspxTools.JsGetElementById(m_Street) %>.value = dataList[0];
				<%= AspxTools.JsGetElementById(m_City) %>.value = dataList[1];
				<%= AspxTools.JsGetElementById(m_State) %>.value = dataList[2];
				<%= AspxTools.JsGetElementById(m_Zipcode) %>.value = dataList[3];
				updateDirtyBit();
			}
		}
	}
	// 08/21/06 mf - OPM 7003. We disable controls that will not
	// be saved when the employee is "new".  To get around the fact
	// that disabled fields do not post and maintain viewstate, we
	// use hidden fields to refresh them to the default values.
	// If Allow Login: Yes is set, or a login already exists,
	// we enable the fields.
	function setUIStatus()
	{
		if ( ! <%= AspxTools.JsBool(m_HasLogin) %> )
		{
			var bDisableControls = ! <%= AspxTools.JsGetElementById(m_CanLogin) %>.checked;
			var previousStatus = <%= AspxTools.JsGetElementById(m_LoginControlStatus) %>;

			if ( previousStatus.value == 'disabled' || bDisableControls == true )
				LoadDefaultValues();

			setDefaultDisableEnable();

			if ( bDisableControls == true )
			{
				document.getElementById('m_Manager').disabled = true;
				document.getElementById('m_Processor').disabled = true;
				document.getElementById('m_LenderAcctExec').disabled = true;
				<% if ( IsPmlEnabled ) { %>
				document.getElementById('m_Underwriter').disabled = true;
				document.getElementById('m_LockDesk').disabled = true;
				<%= AspxTools.JsGetElementById(m_LpePriceGroupId) %>.disabled = true;
				<% } %>
			    setAccessLevelDisabled(true);
			    setDisabledAttr($(<%= AspxTools.JsGetElementById(m_PermissionsPanel) %>).find('input'), true);
                
			    document.getElementById('m_populateDefaultPermissions').disabled = true;
			    disablePasswordControls(true);
				<%= AspxTools.JsGetElementById(m_ExpirationPeriod) %>.disabled = true;
			    if(document.getElementById('m_rateLockDiv'))
			        setRateLockDisabled(true);
			}

			previousStatus.value = ( bDisableControls ) ? 'disabled' : 'enabled';
		}
	}

    function setAccessLevelDisabled(disable) {
        var accessLevelTable = <%= AspxTools.JsGetElementById(m_AccessLevel) %>;
        var inputs = $(accessLevelTable).find('input');
        setDisabledAttr(inputs, disable);
    }

	     // Since the default permissions are generated on bind, we save them in a
		 // hidden field and load it here when we have submitted when they were disabled.
		 // The default access level, relationships, and password settings are currently
		 // hard-coded, so we explicitly use those values here. 
	function LoadDefaultValues()
	{
		var defaultAccessIndex = 2;
		var defaultPasswordIndex = 0;
		var controls;

		controls = document.getElementById('PasswordControls').getElementsByTagName('INPUT');
		for ( var i = 0; i < controls.length; i++ )
			controls[i].checked = ( i == defaultPasswordIndex );

		controls = document.getElementById('RelationshipTable').getElementsByTagName('SELECT');
		for ( var i = 0; i < controls.length; i++ )
			controls[i].selectedIndex = 0;

		controls = document.getElementById('AccessLevelDiv').getElementsByTagName('INPUT');
		for ( var i = 0; i < controls.length; i++ )
		{
			if ( i == defaultAccessIndex )
			{
				controls[i].checked = true;
				break;
			}
		}
	}

	function UpdateGroupList()
	{
	    var groupBody = document.getElementById('GroupsBody');
	    var checks = groupBody.getElementsByTagName('INPUT');
	    var list = '';
	    for (var i=0; i < checks.length; i++)
	    {
	        var oBox = checks[i];
	        if ( oBox.checked )
	            list += ( list=='' ? '':',') + oBox.id;
	    }

	    <%= AspxTools.JsGetElementById(GroupAssociationList) %>.value = list;
	}

	var gCurrentGroupSort = "Group";
	function f_sortGroupGrid()
    {
        gCurrentGroupSort = ( gCurrentGroupSort == "Group" ) ?  "GroupDesc" : "Group";

        var tbody = document.getElementById('GroupsBody');
        var count = tbody.rows.length;
        var list = new Array(count);
        for (var i = 0; i < count; i++) {
          list.push(tbody.rows[i]);
        }

        list.reverse();

        for (var i = 0; i < count; i++)
        {
          tbody.appendChild(list[i]);
          list[i].className = ((i % 2) == 0) ? "GridItem" : "GridAlternatingItem";
        }

        var sortImage = document.getElementById('GroupSortImg');
        var asc = "../../images/Tri_ASC.gif";
        var desc = "../../images/Tri_DESC.gif";
        sortImage.src = ( gCurrentGroupSort == "Group" ) ? asc : desc;
        sortImage.style.display = "inline";
    }

	function LoadGroupList()
	{
	    var oGroupsList = <%= AspxTools.JsGetElementById(GroupAssociationList) %>;
	    if ( oGroupsList == null ) return;
	    var groupsList = oGroupsList.value.split(',');
	    var groupBody = document.getElementById('GroupsBody');
	    var checks = groupBody.getElementsByTagName('INPUT');
	    var list = '';
	    for (var i=0; i < checks.length; i++)
	    {
	        var oBox = checks[i];
	        var found = false;
	        for( var j = 0; j < groupsList.length; j++)
	        {
	            if (groupsList[j] == oBox.id)
	            {
	                oBox.checked = "checked";
	                found = true;
	                break;
	            }
	        }
	        if ( found == false )
	            oBox.checked = '';
	    }
	}
</script>
<asp:PlaceHolder runat="server" ID="FirstVersionLicenseScript">
<script type="text/javascript" >
	    function onAssignLicense()
	    {
		    var assignLicenseLink = <%= AspxTools.JsGetElementById(m_AssignLicenseLink) %>;
	        if(hasDisabledAttr(assignLicenseLink))
			    return;
		    var arg ;
		    var assignModalOnReturn = function(arg){
		        if (arg.LicenseId != null && arg.LicenseId.length > 0)
		        {
		            var licenseId = document.getElementById("LicenseId") ;
		            var licenseNumber = <%= AspxTools.JsGetElementById(m_LicenseNumber) %> ;

		            licenseId.value = arg.LicenseId ;
		            licenseNumber.value = arg.LicenseNumber ;

		            var unassignLicenseLink = <%= AspxTools.JsGetElementById(m_UnassignLicenseLink) %>;
		            setDisabledAttr(unassignLicenseLink, false);
		            unassignLicenseLink.style.textDecorationNone = false;
		            unassignLicenseLink.style.cursor = "hand";
		            updateDirtyBit();
		        }
		    };

			var url;
		    <% if (this.IsInternal) {%>

			  url = "/loadmin/broker/LicenseList.aspx?brokerId=<%= AspxTools.JsStringUnquoted(m_BrokerId.ToString()) %>&mode=select";
		    <% } else { %>
			  url = "/los/admin/LicenseList.aspx?mode=select";
		    <% } %>
			showModal(url, null, null, null, assignModalOnReturn, { hideCloseButton: true }) ;
	    }

	    function onUnassignLicense()
	    {
		    document.getElementById("LicenseId").value = "" ;
		    <%= AspxTools.JsGetElementById(m_LicenseNumber) %>.value = "" ;
	        var unassignLicenseLink = <%= AspxTools.JsGetElementById(m_UnassignLicenseLink) %>;
	        setDisabledAttr(unassignLicenseLink, true);
		    unassignLicenseLink.style.textDecorationNone = true;
		    unassignLicenseLink.style.cursor = "default";
		    updateDirtyBit();
	    }
	</script>
</asp:PlaceHolder>

		<!--[if lte IE 6.5]><iframe id="selectmask" style="display:none; position:absolute;top:0px;left:0px;filter:mask()"  src="javascript:void(0)" ></iframe><![endif]-->

