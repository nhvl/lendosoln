﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConditionChoiceEditor.aspx.cs"  Inherits="LendersOfficeApp.los.admin.ConditionChoiceEditor" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx"%>
<%@ Register TagPrefix="uc1" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Condition Choice Editor</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        td { padding: 3px; }
       
    </style>
</head>
<body>
    <h4 class="page-header"><ml:EncodedLiteral runat="server" ID="FormHeader"></ml:EncodedLiteral></h4>
    <form id="form1" runat="server">
	    <div class="InsetBorder">
	        <table cellSpacing="0" cellPadding="0" border="0" class="FormTable">

		        <tr >
		            <td class="FieldLabel" align="right">
		                Category
		            </td>
		            <td colspan="6" class="FieldLabel">
		                <asp:DropDownList ID="ConditionCategories" runat="server"></asp:DropDownList>
                    </td>
		        </tr>
		        <tr>
		            <td class="FieldLabel" align="right">
		                Condition Type
		            </td>
		            <td>
		                <asp:DropDownList runat="server" ID="ConditionTypes"></asp:DropDownList>
		            </td>
		        </tr>
		        <tr>
		            <td class="FieldLabel" align="right">
		                Loan Type
		            </td>
		            <td>
		                <asp:DropDownList runat="server" ID="sLT"></asp:DropDownList>
		            </td>
		        </tr>
		        <tr >
		            <td class="FieldLabel" valign="top" align="right">
		                Condition Subject
		            </td>
		            <td colspan="6">
		                <asp:TextBox ID="ConditionSubject" TextMode="MultiLine" Rows="6" Width="500px" runat="server"></asp:TextBox>
		            </td>
		        </tr>
		        
		        <tr id="trHidden" runat="server">
		            <td class="FieldLabel" align="right">
                        Hidden?
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="IsHidden"/> <span>Yes (Only visible to users with permission to see hidden information.)</span>
                    </td>
		        </tr>
		        
		        <tr id="trRequiredDocType" runat="server">
		            <td class="FieldLabel" align="right">
                        Required Document Type
                    </td>
                    <td>
                        <uc1:DocTypePicker runat="server" ID="DocTypePicker" Width="200px"></uc1:DocTypePicker>
                    </td>
		        </tr>
		        <tr>
		            <td class="FieldLabel" align="right">
		                To Be Assigned To
		            </td>
		            <td >
		                <asp:DropDownList runat="server" ID="ToBeAssignedToRole"></asp:DropDownList>
		            </td>
		        </tr>
		        <tr>
		            <td class="FieldLabel" align="right">
		                To Be Owned By
		            </td>
		            <td >
		                <asp:DropDownList runat="server" ID="ToBeOwnedByRole"></asp:DropDownList>
		            </td>
		        </tr>
		        <tr>
		            <td class="FieldLabel" align="right">
		                Due Date
		                <img id="DueDateR" src="~/images/error_icon_right.gif" alt="Required" visible="false" runat="server" />
                    </td>
                    <td >
                        <ml:EncodedLabel runat="server" ID="DueDateField"></ml:EncodedLabel>
                        <a href="#" id="DueDatePicker">choose</a>  
                        <asp:HiddenField runat="server" ID="DueDateFieldId" />
                        <asp:HiddenField runat="server" ID="DueDateBuisnessDayAddition"></asp:HiddenField>
                    </td>
		        </tr>
		        
		        <tr>
		            <td colspan="2" class="FieldLabel" align="center">
		                <ml:EncodedLabel ID="m_errorMessage" runat="server" ForeColor="Red"></ml:EncodedLabel>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="2" align="center">
		                <asp:Button ID="m_Save" Text="OK" runat="server" OnClick="OK_OnClick"  />
		                <input type="button" value="Cancel"  />
		            </td>
		        </tr>
		    </table>	    
	    </div>
        <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg> 
	    
        <script type="text/javascript">
            function SetAndClose(json){
                var args = window.dialogArguments || {}; 
                args.UpdatedChoice = json;
                args.OK = true;
                onClosePopup(args);
            }
        
            $(function(){
                resizeForIE6And7(650, 360);
                $('input[value=Cancel]').click(function(){
                    onClosePopup();
                });
                
                $('#DueDatePicker').click(function(){
                    var onDatePickerReturn = function (args) {
                        if (args.OK) {
                            $('#<%= AspxTools.ClientId(DueDateField) %>').text(args.dateDescription);
                            $('#<%= AspxTools.ClientId(DueDateFieldId) %>').val(args.id);
                            $('#<%= AspxTools.ClientId(DueDateBuisnessDayAddition) %>').val(args.offset);
                        }
                    };

                    showModal('/newlos/Tasks/DateCalculator.aspx', null, null, null, onDatePickerReturn, {hideCloseButton:true}); 
                });
            });
        </script>   
    </form>
</body>
</html>
