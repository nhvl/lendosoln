﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintGroupPermAssignment.aspx.cs" Inherits="LendersOfficeApp.los.admin.PrintGroupPermAssignment" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOfficeApp.los" %>
<%@ Register TagPrefix="uc" TagName="LRPicker" Src="../LeftRightPicker.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body>
    <h4 class="page-header">Edit Print Groups</h4>
    <form id="form1" runat="server">
    <div>
        <uc:LRPicker id="Picker" runat="server" AvailableHeader="Available Print Groups" AssignedHeader="Approved Print Groups"></uc:LRPicker>
    </div>
    </form>
</body>
</html>
