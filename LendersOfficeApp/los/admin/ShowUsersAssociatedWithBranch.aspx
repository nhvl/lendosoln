<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="ShowUsersAssociatedWithBranch.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.ShowUsersAssociatedWithBranch" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>View Users</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<style type="text/css">
			form{
				margin-bottom: 0px;
			}
		</style>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto; BACKGROUND-COLOR: #003366">
		<script language="javascript">
<!--
	function _init()
    {
        resize( 700 , 800 );
	}

    function onEditClick( employeeID, type )
	{
		var a;

		if(type == 'PML User')
			showModal( '/los/BrokerAdmin/EditPmlUser.aspx?cmd=edit&employeeId=' + employeeID , null, null, null, function(a){
				if( a != null && a.OK == true )
				self.location = self.location;
			}, { width:890, hideCloseButton:true});
		else
			showModal('/los/admin/EditEmployee.aspx?cmd=edit&employeeId=' + employeeID , null, null, null, function(a){
				if( a != null && a.OK == true )
				self.location = self.location;
			},{width:1000, hideCloseButton:true});


	}

	function checkAllBoxes(obj)
	{
		var checked=false;
		var table = document.getElementById("m_dg");

		if( obj.checked )
		{
			checked=true;
		}
		for( var i=1; i<table.rows.length; i++ )
		{
			table.rows[i].cells[0].childNodes[0].checked = checked;
			onClickCheckRow(table.rows[i].cells[0].childNodes[0]);
		}

		checkboxChanged(obj);
	}

    function checkboxChanged(obj)
	{
		var enable = false;
		var table = document.getElementById("m_dg");
		var button = <%= AspxTools.JsGetElementById(m_MoveUsers) %>;
		var continueScanning = true;

		var numberOfRows = table.rows.length;
		var currentRowNumber = 1;

		// If this is true it means that only the header row exists.
		if ( currentRowNumber == numberOfRows )
		{
			continueScanning = false;
		}

		while ( continueScanning )
		{
			enable = table.rows[currentRowNumber].cells[0].childNodes[0].checked;
			currentRowNumber++;

			if ( currentRowNumber == numberOfRows || enable )
			{
				continueScanning = false;
			}

		}

		button.disabled = !enable;
	}

	// 08/26/08 ck - OPM 23916. Highlight rows in salmon when checked.
	function onClickCheckRow( obj )
	{
		try
		{
			// Apply row-check effects to grid.
			if( obj.checked != null )
			{
				highlightRowByCheckbox( obj );
			}

			checkboxChanged(obj);
		}
		catch( e )
		{
			window.status = e.message;
		}
	}

    //-->
		</script>
		<h4 class="page-header">View Users</h4>
		<form id="ShowUsers" method="post" runat="server">
			<table cellspacing="0" cellpadding="8" border="0" width="100%" height="100%">
				<tr>
					<td height="100%">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
							<tr>
								<td height="0%" style="BORDER-RIGHT: 2px outset; BORDER-TOP: 2px outset; BORDER-LEFT: 2px outset; BACKGROUND-COLOR: gainsboro">
									<table cellspacing="2" cellpadding="3" border="0" width="100%" height="100%">
										<tr>
											<td noWrap height="0%">
												<asp:Panel id="m_headerPanel" style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 0px; BORDER-TOP: 2px groove; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; BORDER-LEFT: 2px groove; PADDING-TOP: 0px; BORDER-BOTTOM: 2px groove; BACKGROUND-COLOR: gainsboro" Runat="server">
													<TABLE height="100" cellSpacing="10" cellPadding="0" width="100%" border="0">
														<tr>
															<td style="FONT-WEIGHT: bold">
																<ml:EncodedLabel style="FONT-SIZE:13px" id="m_branchNameLabel" Runat="server"></ml:EncodedLabel>
																<br>
																<ml:EncodedLabel id="m_tooManyUsers" ForeColor="red" Runat="server"></ml:EncodedLabel>
																<br>
																<ml:EncodedLabel runat="server">Move Users to Branch: </ml:EncodedLabel>
																<asp:DropDownList id="m_BranchDropdownList" runat="server"></asp:DropDownList>
																<ml:nodoubleclickbutton id="m_MoveUsers" onclick="MoveUsersClick" runat="server" Text="Move Users" Enabled="False"></ml:nodoubleclickbutton>
															</td>
														</tr>
													</table>
												</asp:Panel>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr height="100%">
								<td>
									<table cellpadding="3" cellspacing="0" height="100%" width="100%">
										<td height="100%" style="BORDER-RIGHT:2px outset; BORDER-LEFT:2px outset; BORDER-BOTTOM:2px outset; BACKGROUND-COLOR:gainsboro">
											<table cellspacing="0" cellpadding="2" height="100%" width="100%">
												<tr height="100%">
													<td>
														<div style="OVERFLOW-Y:scroll; BORDER-RIGHT:0px; PADDING-RIGHT:4px; PADDING-LEFT:4px; BORDER-LEFT:lightgrey 2px solid; PADDING-TOP:4px; BORDER-BOTTOM:lightgrey 2px solid; BORDER-TOP:lightgrey 2px solid; height=100%" valign="top">
															<ml:CommonDataGrid id="m_dg" runat="server" valign="top" OnItemDataBound="m_dg_ItemDataBound">
																<alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
																<itemstyle cssclass="GridItem"></itemstyle>
																<headerstyle cssclass="GridHeader"></headerstyle>
																<columns>
																	<asp:TemplateColumn ItemStyle-Width="20px">
																		<HeaderTemplate>
																			<asp:CheckBox Runat="server" ID="m_SelectAllBox" onclick="checkAllBoxes(this);"></asp:CheckBox>
																		</HeaderTemplate>
																		<itemtemplate>
																			<asp:CheckBox Runat="server" ID="m_CheckBox" onclick="onClickCheckRow(this)"></asp:CheckBox>
                                                                            <ml:EncodedLabel ID="m_EID" Visible="false" Runat=server></ml:EncodedLabel>
																		</itemtemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn ItemStyle-Width="30px">
																		<itemtemplate>
                                                                            <asp:LinkButton ID="editLink" runat="server">edit</asp:LinkButton>
																		</itemtemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn DataField="LoginNm" ItemStyle-Width="150px" SortExpression="LoginNm" HeaderText="Login Name"></asp:BoundColumn>
																	<asp:BoundColumn DataField="UserName" ItemStyle-Width="200px" SortExpression="UserName" HeaderText="User Name"></asp:BoundColumn>
																	<asp:BoundColumn DataField="Type" ItemStyle-Width="100px" SortExpression="Type" HeaderText="Type"></asp:BoundColumn>
																	<asp:BoundColumn DataField="LicenseNumber" ItemStyle-Width="80px" SortExpression="LicenseNumber" HeaderText="Billing Lic #"></asp:BoundColumn>
																	<asp:BoundColumn DataField="IsActive" ItemStyle-Width="50px" SortExpression="IsActive" HeaderText="Active?"></asp:BoundColumn>
																</columns>
															</ml:CommonDataGrid>
														</div>
													</td>
												</tr>
												<tr>
													<td style="FONT-WEIGHT: bold; FONT-SIZE: 12px">
														<ml:EncodedLabel id="m_noneToShowLabel" ForeColor="green" Runat="server">There are no users to display</ml:EncodedLabel>
													</td>
												</tr>
												<tr>
													<td align="right">
														<br>
														<input id="close" onclick="onClosePopup();" type="button" value="Close" name="close" Runat="server" style="width: 50px;">
													</td>
												</tr>
											</table>
										</td>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
	</body>
</HTML>
