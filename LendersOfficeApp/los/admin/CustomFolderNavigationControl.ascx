﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomFolderNavigationControl.ascx.cs" Inherits="LendersOfficeApp.los.admin.CustomFolderNavigationControl" %>

<div class="main-display">
    <div id="PageTree"></div>

    <div class="footer">
        <div class="warning-message">
            *Subfolders will not be visible to users without permission to view the parent folder.
        </div>

        <div id="ButtonContainer">
            <button class="favorite-cancel" type="button">Cancel</button>
            <button class="favorite-ok" type="button">OK</button>
            <button class="favorite-apply" type="button">Apply</button>
        </div>
    </div>

    <div class="Hidden">
        <div class="folder-editor">
            <h4 class="page-header folder-editor-header">Add Subfolder</h4>
            <div class="form-row">
                <label for="">Folder name (in preview)</label>
                <input tabindex="300" class="internal-name" type="text" maxlength="50" />
                <img src="" id="InternalNameValidationImg" />
            </div>
            <div class="form-row">
                <label for="">Folder name (in loan editor)</label>
                <input tabindex="301" class="external-name" type="text" maxlength="50" />
            </div>
            <div class="form-row role-section">
                <label for="">Available to which user roles?</label>
                <div class="role-dropdown-checkbox"></div>
            </div>
            <div class="form-row group-section">
                <label for="">Available to which employee groups?</label>
                <div class="employee-group-dropdown-checkbox"></div>
            </div>
            <div class="form-row">
                <label for="">Is Lead/Loan</label>
                <select id="leadLoanStatus" class="lead-loan-status">
                    <option value="0">Not Applicable</option>
                    <option value="1">Loan</option>
                    <option value="2">Lead</option>
                </select>
            </div>
            <div class="form-row">
                <label for="">Available to which loan purposes?</label>
                <div class="loan-purpose-dropdown-checkbox"></div>
            </div>
            <div class="form-row">
                <label for="">Available to which loan types?</label>
                <div class="loan-type-dropdown-checkbox"></div>
            </div>
            <div class="folder-editor-buttons">
                <button class="cancel" type="button">Cancel</button>
                <button class="ok" type="button">OK</button>
            </div>
        </div>

        <div class="landing-page-selection-container">
            <div class="landing-page-selection"></div>
            <div class="landing-page-selection-buttons">
                <button class="cancel" type="button">Cancel</button>
                <button class="ok" type="button">OK</button>
            </div>
        </div>

        <div class="page-selection-container">
            <div class="page-selection"></div>
            <div class="page-selection-buttons">
                <button class="cancel" type="button">Cancel</button>
                <button class="ok" type="button">OK</button>
            </div>
        </div>
    </div>
</div>
