using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;
//using LendersOfficeApp.common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.ObjLib.Licensing;
using CommonLib;
using System.Collections.Generic;
namespace LendersOfficeApp.los.admin
{
	/// <summary>
	/// Summary description for LicenseList.
	/// </summary>
	public partial class LicenseList : LendersOffice.Common.BasePage
	{

		protected enum MODE { Manage, Select } ;
		protected MODE m_mode ;
		protected string m_AutoAssign ;

		const int USER_INDEX = 6 ;
		const int SELECT_INDEX = USER_INDEX + 1 ;

        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }

		private class LicenseDesc
		{
			private Guid m_licenseId ;
			private int m_licenseNumber ;
			private string m_description ;
			private DateTime m_purchaseDate ;
			private DateTime m_expirationDate ;
			private int m_seatCount ;
			private int m_openSeatCount ;

			public LicenseDesc(Guid licenseId, int licenseNumber, string description, DateTime purchaseDate, DateTime expirationDate, int seatCount, int openSeatCount)
			{
				m_licenseId = licenseId ;
				m_licenseNumber = licenseNumber ;
				m_description = description ;
				m_purchaseDate = purchaseDate ;
				m_expirationDate = expirationDate ;
				m_seatCount = seatCount ;
				m_openSeatCount = openSeatCount ;
			}

			public Guid LicenseId
			{
				get { return m_licenseId ; }
			}
			public int LicenseNumber
			{
				get { return m_licenseNumber ; }
			}
			public string Description
			{
				get { return m_description ; }
			}
			public DateTime PurchaseDate
			{
				get { return m_purchaseDate ; }
			}
			public DateTime ExpirationDate
			{
				get { return m_expirationDate ; }
			}
			public int SeatCount
			{
				get { return m_seatCount ; }
			}
			public int OpenSeatcount
			{
				get { return m_openSeatCount ; }
			}
		}
	
		protected void PageLoad(object sender, System.EventArgs e)
        {
            ((BasePage)Page).IncludeStyleSheet("~/css/Tabs.css");
			if (!IsPostBack)
			{
				switch(SafeConvert.ToString(Request["mode"]).ToLower())
				{
					case "select":
						m_mode = MODE.Select ;
						break ;
					default:
						m_mode = MODE.Manage ;
						break ;
				}

				ViewState["mode"] = m_mode ;
				ViewState["AutoAssign"] = m_AutoAssign = Request["AutoAssign"] ;
			}
			else
			{
				m_mode = (MODE)ViewState["mode"] ;
				m_AutoAssign = (string)ViewState["AutoAssign"] ;
			}
			BindDataGrid() ;
		}

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}
		private void BindDataGrid()
		{
			Licensee licensee = new Licensee(CurrentUser.BrokerId) ;
			DbDataReader sdr = null ;
			try
			{
                int selectedTabIndex = 0;
                if (Request.Params["__EVENTTARGET"] == "changeTab")
                    selectedTabIndex = Int32.Parse(Request.Params["__EVENTARGUMENT"]);
                else if (ViewState["selectedTabIndex"] != null)
                    selectedTabIndex = (Int32)ViewState["selectedTabIndex"];

                ViewState["selectedTabIndex"] = selectedTabIndex;
                tab0.Attributes.Remove("class");
                tab1.Attributes.Remove("class");
                tab2.Attributes.Remove("class");
				switch(selectedTabIndex)
				{
					case 0:
						sdr = licensee.OpenActiveLicenses ;
						dgLicenses.Columns[USER_INDEX].Visible = true ;
						dgLicenses.Columns[SELECT_INDEX].Visible = true ;
                        tab0.Attributes.Add("class", "selected");
						break ;
					case 1:
						sdr = licensee.AllActiveLicenses ;
						dgLicenses.Columns[USER_INDEX].Visible = true ;
						dgLicenses.Columns[SELECT_INDEX].Visible = false ;
                        tab1.Attributes.Add("class", "selected");
						break ;
					case 2:
						sdr = licensee.AllLicenses ;
						dgLicenses.Columns[USER_INDEX].Visible = false ;
						dgLicenses.Columns[SELECT_INDEX].Visible = false ;
                        tab2.Attributes.Add("class", "selected");
						break ;
				}

				if (m_mode == MODE.Manage)
					dgLicenses.Columns[SELECT_INDEX].Visible = false ;

				if (sdr != null)
					dgLicenses.DataSource = ToDataView(sdr) ;
			}
			finally
			{
				if (sdr != null)
					sdr.Close() ;
			}

			dgLicenses.DataBind() ;
		}
		private DataView ToDataView(DbDataReader sdr)
		{
			ArrayList list = new ArrayList() ;
			while (sdr.Read())
			{
				LicenseDesc desc = new LicenseDesc(
					new Guid(sdr["LicenseId"].ToString()),
					Convert.ToInt32(sdr["LicenseNumber"]),
					sdr["Description"].ToString(),
					Convert.ToDateTime(sdr["PurchaseDate"]),
					Convert.ToDateTime(sdr["ExpirationDate"]),
					Convert.ToInt32(sdr["SeatCount"]),
					Convert.ToInt32(sdr["OpenSeatCount"])) ;
				list.Add(desc) ;
			}

			return ViewGenerator.Generate(list) ;
		}
		private bool IsAutoAssign
		{
			get { return m_AutoAssign == "true" ; }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dgLicenses.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgLicenses_ItemCommand);
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		private void dgLicenses_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			Guid licenseId = new Guid(dgLicenses.DataKeys[e.Item.ItemIndex].ToString()) ;

			if (IsAutoAssign)
			{
				LendersOfficeApp.ObjLib.Licensing.BrokerUser brokerUser = new LendersOfficeApp.ObjLib.Licensing.BrokerUser(CurrentUser.BrokerId, CurrentUser.UserId, CurrentUser.DisplayName) ;
				LendersOfficeApp.ObjLib.Licensing.License license = new LendersOfficeApp.ObjLib.Licensing.License(CurrentUser.BrokerId, licenseId) ;
				brokerUser.AssignLicense(license) ;
			}

			string[] args = new string[2] ;
			args[0] = string.Format("LicenseId='{0}'", licenseId.ToString()) ;
			args[1] = string.Format("LicenseNumber='{0}'", e.Item.Cells[0].Text) ;
			LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, args) ;
		}

        protected void dgLicenses_OnItemDataBound(object source, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            Guid licenseId = new Guid(dgLicenses.DataKeys[e.Item.ItemIndex].ToString());
            LinkButton link = (LinkButton)e.Item.FindControl("lnkLicenseId");
            link.OnClientClick = "showUserList(" + LendersOffice.AntiXss.AspxTools.JsString(licenseId) + "); return false;";
        }
    }
}
