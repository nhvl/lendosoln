﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.Security;

    /// <summary>
    /// Service class for editing title service providers.
    /// </summary>
    public partial class EditTitleLenderServiceService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets the permission required to load this page.
        /// </summary>
        /// <value>The permission required for page load.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Chooses the method to run.
        /// </summary>
        /// <param name="methodName">The method to run.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "GetTitleVendorInfo":
                    this.GetTitleVendorInfo();
                    break;
                case "SaveTitleLenderService":
                    this.SaveTitleLenderService();
                    break;
                case "DeleteTitleLenderService":
                    this.DeleteTitleLenderService();
                    break;
                case "DeleteConfigurationFile":
                    this.DeleteConfigurationFile();
                    break;
                default:
                    throw new UnhandledCaseException(methodName, ErrorMessages.Generic);
            }
        }

        /// <summary>
        /// Gets title vendor info when switching title vendors.
        /// </summary>
        private void GetTitleVendorInfo()
        {
            int vendorId = this.GetInt("AssociatedVendorId");
            TitleVendor vendor = TitleVendor.LoadTitleVendor(vendorId);
            if (vendor == null)
            {
                this.SetResult("Success", false);
                this.SetResult("Error", "Unable to load Vendor.");
                return;
            }

            this.SetResult("IsQuickQuotingEnabled", vendor.IsQuickQuotingEnabled);
            this.SetResult("IsDetailedQuotingEnabled", vendor.IsDetailedQuotingEnabled);
            this.SetResult("IsPolicyOrderingEnabled", vendor.IsPolicyOrderingEnabled);
            this.SetResult("UsesProviderCodes", vendor.UsesProviderCodes);
            this.SetResult("Success", true);
        }

        /// <summary>
        /// Saves the service provider.
        /// </summary>
        private void SaveTitleLenderService()
        {
            TitleLenderServiceViewModel viewModel = SerializationHelper.JsonNetDeserialize<TitleLenderServiceViewModel>(this.GetString("LenderServiceViewModel"));

            string errors;
            TitleLenderService lenderService = TitleLenderService.CreateFromViewModel(viewModel, PrincipalFactory.CurrentPrincipal, out errors);
            if (lenderService != null)
            {
                // Capture value now, since Save() sets IsNew to false.
                bool isNew = lenderService.IsNew;
                if (lenderService.Save())
                {
                    this.SetResult("IsNew", isNew);
                    this.SetResult("ServiceId", lenderService.LenderServiceId);
                    this.SetResult("VendorAsString", lenderService.DisplayName);
                    this.SetResult("Success", true);
                }
                else
                {
                    this.SetResult("Success", false);
                    this.SetResult("Error", "Could not save service configuration.");
                }
            }
            else
            {
                Tools.LogWarning("Title Lender Service UI: " + errors);
                this.SetResult("Success", false);
                this.SetResult("Error", errors);
            }
        }

        /// <summary>
        /// Deletes a title service provider.
        /// </summary>
        private void DeleteTitleLenderService()
        {
            string errors;
            if (TitleLenderService.DeleteService(PrincipalFactory.CurrentPrincipal.BrokerId, this.GetInt("ServiceId"), out errors))
            {
                this.SetResult("Success", true);
            }
            else
            {
                this.SetResult("Success", false);
                this.SetResult("Errors", errors);
            }
        }

        /// <summary>
        /// Deletes the configuration file for the specified key.
        /// </summary>
        private void DeleteConfigurationFile()
        {
            string autoExpiredTextCacheKey = this.GetString("ConfigurationFileKeyTempKey");
            string data = AutoExpiredTextCache.GetUserString(PrincipalFactory.CurrentPrincipal, autoExpiredTextCacheKey);
            Guid fileKey;
            if (Guid.TryParse(data, out fileKey))
            {
                ErnstConfiguration.DeleteConfiguration(fileKey);
            }
        }
    }
}