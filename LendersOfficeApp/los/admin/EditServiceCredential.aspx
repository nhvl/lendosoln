﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditServiceCredential.aspx.cs" Inherits="LendersOfficeApp.Los.Admin.EditServiceCredential" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="~/common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Service Credential</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        #MainEditTable td:first-child {
            width: 30%;
        }

        .BottomButtons {
            width: 100%;
            position: fixed;
            bottom: 0;
            left: 0;
            text-align: center;
            padding-bottom: 16px;
        }

        .BottomButtons .ErrorMessage {
            color: red;
            background-color: gainsboro;
        }

        .hidden {
            display: none;
        }

        .ServicesList + .ServicesList {
            border-top: 1px solid #333333;
        }
    </style>
</head>
<body>
    <h4 class="page-header">Service Credential Editor</h4>
    <form id="EditServiceCredential" runat="server">
        <div class="FullWidthHeight">
            <table class="FullWidthHeight" id="MainEditTable" runat="server" border="0">
                <tr>
                    <td class="FieldLabel">Services</td>
                    <td>
                        <%--Each separate ul.ServicesList element represents a separate mutually exclusive group of services 
                            which may share credentials within the group. When adding to this list, 
                            also add the checkboxes to the js in serviceCheckboxGroups and serviceAssociatedElementMap.--%>
                        <ul class="UndecoratedList ServicesList">
                            <li>
                                <label>
                                    <input type="checkbox" class="ServiceCb" runat="server" id="IsForCreditReport" />Credit Reports</label>
                            </li>
                            <li id="IsForVerificationsOption" runat="server">
                                <label>
                                    <input type="checkbox" class="ServiceCb" runat="server" id="IsForVerifications" />Verifications (VOE/VOI, VOD/VOA, SSA-89)
                                </label>
                            </li>
                        </ul>
                        <ul class="UndecoratedList ServicesList">
                            <li>
                                <label>
                                    <input type="checkbox" class="ServiceCb" runat="server" id="IsForUcdDelivery" />UCD Delivery
                                </label>
                            </li>
                            <li>
                                <label>
                                    <input type="checkbox" class="ServiceCb" runat="server" id="IsForAusSubmission" />AUS Submission
                                </label>
                            </li>
                        </ul>
                        <ul class="UndecoratedList ServicesList">
                            <li>
                                <label><input type="checkbox" class="ServiceCb" runat="server" id="IsForTitleQuotes" />Title Quotes</label>
                            </li>
                        </ul>
                        <ul class="UndecoratedList ServicesList">
                            <li>
                                <label><input type="checkbox" class="ServiceCb" runat="server" id="IsForDocumentCapture" />Document Capture</label>
                            </li>
                        </ul>
                        <ul class="UndecoratedList ServicesList DigitalMortgageCheckBox">
                            <li>
                                <label><input type="checkbox" class="ServiceCb" runat="server" id="IsForDigitalMortgage" />Digital Mortgage</label>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr id="CraRow" class="hidden">
                    <td class="FieldLabel">Credit Reporting Agency</td>
                    <td>
                        <asp:DropDownList noteditable="true" ID="CraList" runat="server"></asp:DropDownList>
                        <input type="hidden" runat="server" id="ChosenCraId" />
                        <input type="hidden" runat="server" id="ChosenCraName" />
                        <img id="CraListRequiredImg" src="../../images/require_icon.gif" />
                    </td>
                </tr>
                <tr id="VerificationProviderRow" class="hidden">
                    <td class="FieldLabel">Verification Provider</td>
                    <td>
                        <asp:DropDownList noteditable="true" ID="VendorList" runat="server"></asp:DropDownList>
                        <input type="hidden" runat="server" id="ChosenVendorId" />
                        <input type="hidden" runat="server" id="ChosenVendorName" />
                        <img id="VendorListRequiredImg" src="../../images/require_icon.gif" />
                    </td>
                </tr>
                <tr id="UcdDeliveryRow" class="hidden">
                    <td class="FieldLabel">UCD Delivery Target</td>
                    <td>
                        <asp:DropDownList ID="UcdDeliveryTarget" runat="server"></asp:DropDownList>
                        <input type="hidden" runat="server" id="Hidden1" />
                        <img id="UcdDeliveryRequiredImg" src="../../images/require_icon.gif" />
                    </td>
                </tr>
                <tr id="AusOptionsRow" class="hidden">
                    <td class="FieldLabel">AUS</td>
                    <td>
                        <asp:DropDownList noteditable="true" ID="AusOptions" runat="server"></asp:DropDownList>
                        <input type="hidden" runat="server" id="ChosenAusOption" />
                        <img id="AusOptionsRequiredImg" src="../../images/require_icon.gif" />
                        <div id="IsEnabledForNonSeamlessDuSection" class="hidden" runat="server">
                            <input type="checkbox" runat="server" id="IsEnabledForNonSeamlessDu" />
                            <span>This credential can be used for non-seamless DU submissions</span>
                        </div>
                    </td>
                </tr>
                <tr id="TitleQuotesRow" class="hidden">
                    <td class="FieldLabel">Title Quote Service Provider</td>
                    <td>
                        <asp:DropDownList noteditable="true" ID="TitleQuoteLenderServices" runat="server"></asp:DropDownList>
                        <input type="hidden" runat="server" id="ChosenTitleQuotesLenderServiceId" />
                        <input type="hidden" runat="server" id="ChosenTitleQuotesLenderServiceName" />
                        <img id="TitleQuoteLenderServiceRequiredImg" src="../../images/require_icon.gif" />
                    </td>
                </tr>
                <tr id="DigitalMortgageRow" class="hidden">
                    <td class="FieldLabel">eMortgage Provider</td>
                    <td>
                        <asp:DropDownList noteditable="true" ID="DigitalMortgageProviderDdl" runat="server"></asp:DropDownList>
                        <input type="hidden" runat="server" id="ChosenDigitalMortgageId" />
                        <input type="hidden" runat="server" id="ChosenDigitalMortgageName" />
                        <img id="DigitalMortgageRequiredImg" src="../../images/require_icon.gif" />
                    </td>
                </tr>
                <tr id="accountIdRow" class="hidden">
                    <td class="FieldLabel">Account ID</td>
                    <td>
                        <asp:TextBox ID="AccountId" runat="server" MaxLength="100" Width="250"></asp:TextBox>
                        <img id="AccountIdRequiredImg" src="../../images/require_icon.gif" />
                        <asp:RequiredFieldValidator ID="AccountIdValidator" ControlToValidate="AccountId" runat="server" Display="Dynamic" ErrorMessage="*" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <span id="LoginLabel">Login</span>
                    </td>
                    <td>
                        <asp:TextBox ID="UserName" runat="server" MaxLength="100" Width="250"></asp:TextBox>
                        <img src="../../images/require_icon.gif" />
                        <asp:RequiredFieldValidator ID="UserNameValidator" ControlToValidate="UserName" runat="server" Display="Dynamic" ErrorMessage="*" />
                    </td>
                </tr>
                <tr class="PasswordRow">
                    <td class="FieldLabel" id="PasswordLabel">Password</td>
                    <td>
                        <asp:TextBox ID="UserPassword" runat="server" TextMode="Password" MaxLength="100" Width="250"></asp:TextBox>
                        <img src="../../images/require_icon.gif" />
                        <asp:RequiredFieldValidator ID="UserPasswordValidator" ControlToValidate="UserPassword" runat="server" Display="Dynamic" ErrorMessage="*" />
                    </td>
                </tr>
                <tr class="PasswordRow">
                    <td class="FieldLabel" id="ConfirmPasswordLabel">Confirm Password</td>
                    <td>
                        <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" MaxLength="100" Width="250"></asp:TextBox>
                        <img src="../../images/require_icon.gif" />
                        <asp:RequiredFieldValidator ID="ConfirmPasswordValidator" ControlToValidate="ConfirmPassword" runat="server" Display="Dynamic" ErrorMessage="*" />
                        <asp:CompareValidator ID="PasswordMatchValidator" runat="server" ControlToValidate="ConfirmPassword" ErrorMessage="Passwords do not match." ControlToCompare="UserPassword" Display="Dynamic"></asp:CompareValidator>
                    </td>
                </tr>
                <tr runat="server" id="UserTypeRow">
                    <td class="FieldLabel">LQB System User Type</td>
                    <td>
                        <asp:RadioButtonList ID="UserType" runat="server">
                            <asp:ListItem Text="LQB" Value="1" Selected="True" />
                            <asp:ListItem Text="TPO" Value="2" />
                            <asp:ListItem Text="Any" Value="0" />
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>

            <div class="BottomButtons">
                <div class="ErrorMessage">
                    <ml:EncodedLabel ID="UserMessage" runat="server"></ml:EncodedLabel>
                </div>
                <asp:Button runat="server" ID="SaveBtn" Text="Save" OnClick="OnSaveClick"></asp:Button>
                <input type="button" id="CancelBtn" runat="server" onclick="ClosePopup();" value="Cancel" />
            </div>
        </div>
        <uc1:cModalDlg ID="Cmodaldlg1" runat="server" />
    </form>
    <script type="text/javascript">
        <!--
    jQuery(function ($) {
        var Enums = {
            AusOption: {
                DesktopUnderwriter: "<%= AspxTools.JsNumeric(LendersOffice.ObjLib.ServiceCredential.AusOption.DesktopUnderwriter) %>",
                LpaUserLogin: "<%= AspxTools.JsNumeric(LendersOffice.ObjLib.ServiceCredential.AusOption.LpaUserLogin) %>",
                LpaSellerCredential: "<%= AspxTools.JsNumeric(LendersOffice.ObjLib.ServiceCredential.AusOption.LpaSellerCredential) %>"
            },
            UcdDeliveryTargetOption: {
                FannieMaeUcdCollectionSolution: "<%= AspxTools.JsNumeric(LendersOffice.ObjLib.ServiceCredential.UcdDeliveryTargetOption.FannieMaeUcdCollectionSolution) %>",
                LcaUserLogin: "<%= AspxTools.JsNumeric(LendersOffice.ObjLib.ServiceCredential.UcdDeliveryTargetOption.LcaUserLogin) %>",
                LcaSellerCredential: "<%= AspxTools.JsNumeric(LendersOffice.ObjLib.ServiceCredential.UcdDeliveryTargetOption.LcaSellerCredential) %>"
            }
        };

            // This is a list of the JQuery groups of mutually exclusive service checkboxes.
            // Ex. A credit credential can also be a verification credential, but not an AUS credential.
            var serviceCheckboxGroups = [
                $('#IsForCreditReport, #IsForVerifications'),
                $('#IsForAusSubmission, #IsForUcdDelivery'),
                $('#IsForTitleQuotes'),
                $('#IsForDocumentCapture'),
                $('#IsForDigitalMortgage')
            ];
            
            var serviceAssociatedElementMap = {
                'IsForUcdDelivery': {
                    row: $('#UcdDeliveryRow')
                },
                'IsForAusSubmission': {
                    row: $('#AusOptionsRow')
                },
                'IsForCreditReport': {
                    row: $('#CraRow')
                },
                'IsForVerifications': {
                    row: $('#VerificationProviderRow')
                },
                'IsForTitleQuotes': {
                    row: $('#TitleQuotesRow')
                },
                'IsForDigitalMortgage': {
                    row: $('#DigitalMortgageRow')
                }
            };

            $('#SaveBtn').click(function () {
                return PrepForSave();
            });

            $('.ServiceCb').change(function () {
                ToggleServiceExclusivity(this);
                ToggleServiceElements(this);
                UpdatePageState();
            });

            $('#CraList').change(function () {
                SetLinkedVendor();
                ToggleAccountIdVisibility();
            });

            $('#VendorList').change(function () {
                ToggleAccountIdVisibility();
            });

            $('#AusOptions').change(function () {
                ToggleAusOptions();
            });

            $('#AusOptions, #UcdDeliveryTarget').change(UpdateLoginLabels);

            $('#TitleQuoteLenderServices').change(function () {
                ToggleAccountIdVisibility();
            });

            $('#IsForAusSubmission, #IsForUcdDelivery, #UcdDeliveryTarget').change(function () {
                SynchronizeAusAndUcdOptions();
            });

            var ucdToAusAssociations = (function () {
                var o = {};
                o[Enums.UcdDeliveryTargetOption.FannieMaeUcdCollectionSolution] = Enums.AusOption.DesktopUnderwriter;
                o[Enums.UcdDeliveryTargetOption.LcaSellerCredential] = Enums.AusOption.LpaSellerCredential;
                o[Enums.UcdDeliveryTargetOption.LcaUserLogin] = Enums.AusOption.LpaUserLogin;
                return o;
            })();

            function SynchronizeAusAndUcdOptions() {
                var ausSelector = $('#AusOptions');
                var ausAndUcdSelected = $('#IsForAusSubmission').prop('checked') && $('#IsForUcdDelivery').prop('checked');
                var ucdDeliveryTargetValue = $('#UcdDeliveryTarget').val();
                if (ausAndUcdSelected && ucdToAusAssociations[ucdDeliveryTargetValue] !== undefined) {
                    ausSelector.val(ucdToAusAssociations[ucdDeliveryTargetValue]).prop('disabled', true).trigger('change');
                }
                else {
                    ausSelector.prop('disabled', false);
                }
            }

            function UpdateLoginLabels() {
                var loginLabelText = 'Login';
                var passwordLabelText = 'Password';
                var confirmPasswordLabelOverride = undefined;
                var hidePasswordFields = false;
                if ($('#IsForAusSubmission').prop('checked')) {
                  if ($('#AusOptions').val() === Enums.AusOption.LpaSellerCredential) {
                    loginLabelText = 'Seller/Servicer Number';
                    passwordLabelText = 'LP Password'; 
                  } else if ($('#AusOptions').val() === Enums.AusOption.LpaUserLogin) {
                    loginLabelText = 'User ID';
                    passwordLabelText ='Authentication Password';
                    confirmPasswordLabelOverride = 'Confirm Auth. Password';
                  }
                } else if ($('#IsForUcdDelivery').prop('checked')) {
                  if ($('#UcdDeliveryTarget').val() === Enums.UcdDeliveryTargetOption.LcaSellerCredential) {
                    loginLabelText = 'Seller/Servicer Number';
                    hidePasswordFields = true;
                  } else if ($('#UcdDeliveryTarget').val() === Enums.UcdDeliveryTargetOption.LcaUserLogin) {
                    loginLabelText = 'User ID';
                    passwordLabelText = 'Authentication Password';
                    confirmPasswordLabelOverride = 'Confirm Auth. Password';
                  }
                }

                $('#LoginLabel').text(loginLabelText);
                $('#PasswordLabel').text(passwordLabelText);
                $('#ConfirmPasswordLabel').text(confirmPasswordLabelOverride || ('Confirm ' + passwordLabelText));
                TogglePasswordRequired(!hidePasswordFields);
            }

            function PrepForSave() {
                var selectedVendor = $('#VendorList option:selected');
                var selectedCra = $('#CraList option:selected');
                var selectedAusOption = $('#AusOptions option:selected');
                var selectedTitleQuoteLenderService = $('#TitleQuoteLenderServices option:selected');
                var selectedDigitalMortgageService = $('#DigitalMortgageProviderDdl option:selected');

                $('#ChosenVendorId').val(selectedVendor.val());
                $('#ChosenVendorName').val(selectedVendor.text());
                $('#ChosenCraId').val(selectedCra.val());
                $('#ChosenCraName').val(selectedCra.text());
                $('#ChosenAusOption').val(selectedAusOption.val());
                $('#ChosenTitleQuotesLenderServiceId').val(selectedTitleQuoteLenderService.val());
                $('#ChosenTitleQuotesLenderServiceName').val(selectedTitleQuoteLenderService.text());
                $('#ChosenDigitalMortgageId').val(selectedDigitalMortgageService.val());
                $('#ChosenDigitalMortgageName').val(selectedDigitalMortgageService.text());

                if (typeof (ML.IsDelete) !== 'undefined' && ML.IsDelete) {
                    return true;
                }

                if (!Page_ClientValidate()) {
                    return false;
                }

                var checkedServiceBoxes = $('.ServiceCb:checked');
                if (checkedServiceBoxes.length == 0) {
                    alert("Please choose at least one Service.");
                    return false;
                }

                if (!ValidateExclusiveServiceGroups(checkedServiceBoxes)) {
                    alert('The combination of selected services is invalid. Please ensure the combination is correct.')
                    return false;
                }

                if ($('#IsForCreditReport').is(':checked') && $('#CraList').val() == '00000000-0000-0000-0000-000000000000') {
                    alert("Please choose a Credit Reporting Agency.")
                    return false;
                }

                if ($('#IsForVerifications').is(':checked') && $('#VendorList').val() == '-1') {
                    alert("Please choose a Verification Provider.");
                    return false;
                }

                if ($('#IsForAusSubmission').is(':checked') && $('#AusOptions').val() == '-1') {
                    alert("Please choose an AUS option.");
                    return false;
                }

                if ($('#IsForTitleQuotes').is(':checked') && $('#TitleQuoteLenderServices').val() == '-1') {
                    alert("Please choose a Title Quote Service Provider.");
                    return false;
                }

                return true;
            }

            function ValidateExclusiveServiceGroups(JQCheckedBoxes) {
                var representedGroup = -1;
                for (var checkedBox = 0; checkedBox < JQCheckedBoxes.length; checkedBox++) {
                    for (var group = 0; group < serviceCheckboxGroups.length; group++) {
                        if (group === representedGroup) {
                            continue;
                        }

                        if (serviceCheckboxGroups[group].is(JQCheckedBoxes[checkedBox])) {
                            if (representedGroup === -1)
                                representedGroup = group;
                            if (group !== representedGroup)
                                return false;
                        }
                    }
                }
                return true;
            }

            function UpdatePageState() {
                $('.ServiceCb').each(function () { ToggleServiceElements(this); });
                SetLinkedVendor();
                ToggleAccountIdVisibility();
                ToggleAusOptions();
                UpdateLoginLabels();
            }

            function ToggleServiceElements(checkbox) {
                var jqCheckbox = $(checkbox);
                var elementMap = serviceAssociatedElementMap[jqCheckbox.prop('id')];
                if (elementMap) {
                    elementMap.row.toggle(jqCheckbox[0].checked);
                }
            }

            function ToggleAusOptions() {
                var isDu = $('#AusOptions').val() === Enums.AusOption.DesktopUnderwriter;
                $('#IsEnabledForNonSeamlessDuSection').toggle(isDu);
                if (!isDu) {
                    $('#IsEnabledForNonSeamlessDu').prop('checked', false);
                }
            }

            /***
             * Unchecks all the service checkboxes that aren't included in the same service group as the passed in service checkbox.
             */
            function ToggleServiceExclusivity(serviceCheckbox) {
                var checkboxGroup = $('.ServiceCb');
                for (var i = 0; i < serviceCheckboxGroups.length; i++) {
                    if (serviceCheckboxGroups[i].is(serviceCheckbox)) {
                        checkboxGroup = serviceCheckboxGroups[i];
                    }
                }
                $('.ServiceCb').not(checkboxGroup).prop('checked', false);
            };

            function SetLinkedVendor() {
                var selectedCra = $('#CraList option:selected');
                if (!$('#IsForCreditReport').prop('checked') || selectedCra.val() === '00000000-0000-0000-0000-000000000000') {
                    $('#VendorList').prop('disabled', !$('#IsForVerifications').is(':checked'));
                    return;
                }

                var linkedVendor = selectedCra.attr('data-voxVendorId');
                if (typeof (linkedVendor) === 'string' &&
                    $('#VendorList option[value="' + linkedVendor + '"]').length !== 0) {
                    $('#VendorList').val(linkedVendor);
                    $('#VendorList').prop('disabled', true);
                }
                else {
                    $('#VendorList').prop('disabled', !$('#IsForVerifications').is(':checked'));
                    if ($('#VendorList').is(':disabled') || typeof (linkedVendor) === 'string') {
                        $('#VendorList').find('option:eq(0)').prop('selected', true);
                    }
                }
            }

            function TogglePasswordRequired(displayPassword) {
                $('.PasswordRow').toggle(displayPassword);
                $('#UserPasswordValidator').prop('enabled', displayPassword);
                $('#ConfirmPasswordValidator').prop('enabled', displayPassword);
                if (!displayPassword) {
                    $('#UserPassword').val('');
                    $('#ConfirmPassword').val('');
                }
            }

            function ToggleAccountIdVisibility() {
                var chosenCra = $('#CraList option:selected');
                var craEnabled = $('#IsForCreditReport').prop('checked');
                var craId = chosenCra.val();
                var isCbc = chosenCra.attr('data-protocol') === '15';

                var craUsesAcctId = false;
                if (craEnabled && accountIdCras != null) {
                    for (var i = 0; i < accountIdCras.length; i++) {
                        if (accountIdCras[i] === craId) {
                            craUsesAcctId = true;
                            break;
                        }
                    }
                }

                var craRequiresAcctId = craUsesAcctId && !isCbc;

                var chosenVendor = $('#VendorList option:selected');
                var verificationEnabled = $('#IsForVerifications').prop('checked');
                var dataUsesAcctId = chosenVendor.attr('data-usesAcctId');
                var dataReqAcctId = chosenVendor.attr('data-reqAcctId');

                var vendorUsesAcctId = verificationEnabled && typeof (dataUsesAcctId) === 'string' && dataUsesAcctId.toLowerCase() === 'true';
                var vendorReqAcctId = vendorUsesAcctId && typeof (dataReqAcctId) === 'string' && dataReqAcctId.toLowerCase() === 'true';

                var chosenTitleQuoteLS = $('#TitleQuoteLenderServices option:selected');
                var titleQuotesEnabled = $('#IsForTitleQuotes').prop('checked');
                var dataTitleQuotesUsesAcctId = chosenTitleQuoteLS.attr('data-usesAcctId');
                var dataTitleQuotesReqAcctId = chosenTitleQuoteLS.attr('data-reqAcctId');
                var titleQuotesUsesAcctId = titleQuotesEnabled && typeof(dataTitleQuotesUsesAcctId) === 'string' && dataTitleQuotesUsesAcctId.toLowerCase() === 'true';
                var titleQuotesReqAcctId = titleQuotesUsesAcctId && typeof (dataTitleQuotesReqAcctId) === 'string' && dataTitleQuotesReqAcctId.toLowerCase() === 'true';

                var usesAccountId = craUsesAcctId || vendorUsesAcctId;
                var requiresAccountId = usesAccountId && (craRequiresAcctId || vendorReqAcctId);

                $('#accountIdRow').toggle(usesAccountId);
                $('#AccountIdRequiredImg').toggle(requiresAccountId);
                $('#AccountIdValidator')[0].enabled = requiresAccountId;

                if (!usesAccountId) {
                    $('#AccountId').val('');
                }
            }

            SynchronizeAusAndUcdOptions();
            $(".DigitalMortgageCheckBox").toggle(!ML.IsOriginatingCompany);

            if (!(typeof (ML.IsDelete) !== 'undefined' && ML.IsDelete)) {
                UpdatePageState();
            }
        });

        function _init() {
            if (document.getElementById('autoclose') != null) {
                var results = {
                    OK: true,
                    CredentialList: document.getElementById('credentialList').value
                };

                parent.LQBPopup.Return(results);
            }
        }

        function ClosePopup() {
            parent.LQBPopup.Return(null);
        }
        // -->
    </script>
</body>
</html>
