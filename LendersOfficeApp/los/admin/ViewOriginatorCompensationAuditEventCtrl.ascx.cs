﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.admin
{
    public partial class ViewOriginatorCompensationAuditEventCtrl : System.Web.UI.UserControl
    {
        private Guid employeeId
        {
            get
            {
                return RequestHelper.GetGuid("employeeId", Guid.Empty);                
            }
        }

        private Guid pmlBrokerId
        {
            get
            {
                return RequestHelper.GetGuid("pmlBrokerId", Guid.Empty);
            }
        }

        private Guid brokerId
        {
            get
            {
                return RequestHelper.GetGuid("brokerId", Guid.Empty);
            }
        }

        private int eventId
        {
            get
            {
                return Convert.ToInt32(RequestHelper.GetSafeQueryString("eventId"));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            OriginatorCompensationAuditEvent auditEvent = null;
            bool  isBUser = false;
            if (employeeId != Guid.Empty)
            {
                EmployeeDB empDB = new EmployeeDB(employeeId, brokerId);
                empDB.Retrieve();
                isBUser = empDB.UserType == 'B';
                auditEvent = empDB.OriginatorCompensationAuditData.GetAuditEventById(eventId, brokerId);
            }
            else if(pmlBrokerId != Guid.Empty)
            {
                PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(pmlBrokerId, brokerId);
                auditEvent = pmlBroker.OriginatorCompensationAuditData.GetAuditEventById(eventId, brokerId);
            }
            else
            {
                throw(new InvalidOperationException("Invalid input to retrieve audit data."));
            }

            LosConvert m_losConvert = new LosConvert();
            Date.Text = auditEvent.Date_rep;
            By.Text = auditEvent.By_rep;
            if (auditEvent.Rate.TrimWhitespaceAndBOM().Length == 0 && isBUser)
            {
                Rate.Text = "Data Unavailable";
                Minimum.Text = "Data Unavailable";
                Maximum.Text = "Data Unavailable";
                FixedAmount.Text = "Data Unavailable";
            }
            else if (auditEvent.Rate.TrimWhitespaceAndBOM().Length > 0)
            {
                Rate.Text = string.Format("{0} of the {1}", auditEvent.Rate, auditEvent.BaseT_rep);
                Minimum.Text = m_losConvert.ToMoney(auditEvent.Minimum) > 0.0M ? auditEvent.Minimum : "";
                Maximum.Text = m_losConvert.ToMoney(auditEvent.Maximum) > 0.0M ? auditEvent.Maximum : "";
                FixedAmount.Text = auditEvent.FixedAmount;

                bool onlyFor1stLienOfCombo;
                if (bool.TryParse(auditEvent.OnlyFor1stLienOfCombo, out onlyFor1stLienOfCombo))
                {
                    OnlyFor1stLienOfCombo.Text = onlyFor1stLienOfCombo ?
                        "Compensation only paid for the 1st lien of a combo"
                        : "Compensation paid for both liens of a combo";
                }
                else
                {
                    OnlyFor1stLienOfCombo.Text = string.Empty;
                }
            }
            else
            {
                //this condition occurs when compensation plan is set to OC
                trRate.Visible = trMin.Visible = trMax.Visible = trFixed.Visible = trOnlyFor1stLienOfCombo.Visible = false;
            }            
            Notes.Text = auditEvent.Notes;
        }
    }
}