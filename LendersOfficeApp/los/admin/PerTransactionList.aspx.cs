﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.ObjLib.Billing;
using LendersOffice.Security;
using LendersOffice.Common.TextImport;
using DataAccess;
using LendersOffice.ObjLib.Loan.Security;
using MeridianLink.CommonControls;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.admin
{
    public partial class PerTransactionList : LendersOffice.Common.BasePage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EnableJqueryMigrate = false;
        }

        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }

        protected BrokerDB CurrentBroker
        {
            get
            {
                return BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            }
        }

        protected static Guid CurrentBrokerId
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            }
        }

        protected void MonthPicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            BillableLoans.DataSource = CreateBillableLoansDataTable().DefaultView;
            BillableLoans.DataBind();
            TotalLoanCount.InnerText = BillableLoansTable.Rows.Count.ToString();
            BillableLoansPanel.Update();
        }

        /// <summary>
        /// Initializes the <see cref="MonthPicker"/> control with the lender's billable months.
        /// </summary>
        /// <param name="sender">Event sender (<see cref="MonthPicker"/>).</param>
        /// <param name="e">Event arguments.</param>
        protected void MonthPicker_Init(object sender, EventArgs e)
        {
            var BilledMonths = PerTransactionBilling.GetBilledMonths(CurrentBrokerId);
            if (!BilledMonths.Any())
            {
                return;
            }

            DateTime firstMonth = BilledMonths.Keys.Min();
            DateTime lastMonth = BilledMonths.Keys.Max();
            int numOfMonths = (lastMonth.Year - firstMonth.Year) * 12 + lastMonth.Month - firstMonth.Month + 1;
            IEnumerable<DateTime> months = Enumerable.Range(0, numOfMonths).Select(monthIndex => firstMonth.AddMonths(monthIndex)).Reverse();

            foreach (DateTime month in months)
            {
                if (BilledMonths.Keys.Contains(month))
                {
                    // Month has billed loans
                    MonthPicker.Items.Add(MakeMonthListItem(month, BilledMonths[month]));
                }
                else
                {
                    // Month has no billed loans
                    ListItem empty = MakeMonthListItem(month, 0);
                    empty.Attributes["class"] = "Empty";
                    MonthPicker.Items.Add(empty);
                }

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            MonthPicker_OnSelectedIndexChanged(sender, e);
            UpdatePanelScriptManager.RegisterPostBackControl(this.DownloadButton);
        }

        private DataTable BillableLoansTable = null;

        /// <summary>
        /// Creates a <see cref="DataTable"/> representing the billing entries in the per-transaction billing for the currently selected month.
        /// </summary>
        /// <returns>A DataTable that can be easily rendered as a table in the UI.</returns>
        public DataTable CreateBillableLoansDataTable()
        {
            List<PerTransactionBilling.BillingEntry> billableLoans = PerTransactionBilling.GetBrokerBilledLoans(CurrentBrokerId, DateTime.Parse(MonthPicker.SelectedValue)).ToList();

            IEnumerable<Guid> billableIds = billableLoans.Select(billingentry => billingentry.LoanId);
            LoanReadPermissionResolver permissionResolver = new LoanReadPermissionResolver(CurrentBroker.BrokerID);
            HashSet<Guid> loanIdsWithPermission = permissionResolver.GetReadableLoans(PrincipalFactory.CurrentPrincipal, billableIds);

            this.BillableLoansTable = billableLoans.ToDataTable(new List<string> { "LoanNumber", "Borrower", "LoanOfficer", "BranchName", "BranchCode", "BilledDate", "BillingType", "LoanId" })
                .MapColumns(new Dictionary<string, Func<object, object>> {
                    { "LoanId", (id) => loanIdsWithPermission.Contains((Guid)id) ? id : Guid.Empty }
                });

            return this.BillableLoansTable;
        }

        /// <summary>
        /// Creates a ListItem to add to the Month list dropdown.
        /// </summary>
        /// <param name="month">The DateTime (Year and Month) to create a ListItem for.</param>
        /// <param name="NumberOfLoans">The number of loans billed in the month.</param>
        /// <returns>A ListItem for adding to a <see cref="DropDownList"/>.</returns>
        private static ListItem MakeMonthListItem(DateTime month, int NumberOfLoans)
        {
            ListItem item = new ListItem();
            item.Value = month.ToString("MM'/'yyyy");
            item.Text = $"{month:MMMM yyyy} ({NumberOfLoans})";
            return item;
        }

        protected void DownloadClick(object sender, EventArgs e)
        {
            var Month = DateTime.Parse(MonthPicker.SelectedValue);
            MonthPicker_OnSelectedIndexChanged(sender, e);
            var columnNameMap = new Dictionary<string, string> 
            { 
                {"LoanNumber", "Loan Number"},
                {"Borrower", "Borrower"},
                {"LoanOfficer", "Loan Officer"},
                {"BranchName", "Branch Name"},
                {"BranchCode", "Branch Code"},
                {"BilledDate", "Billed Date"},
                {"BillingType", "Billing Reason" }
            };

            var dt = this.BillableLoansTable.MapColumns(new Dictionary<string, Func<object, object>>
                                {
                                    //We don't want the user to think we know that they were all billed at 12:00 am exactly
                                    {"BilledDate", (date) => ((DateTime)date).ToShortDateString()},
                                    {"BillingType",  (billingType) => GetBillingReason((E_BillingReason)billingType)}
                                });

            dt.Columns.Remove("LoanId");

            foreach (var kvp in columnNameMap)
            {
                dt.Columns[kvp.Key].ColumnName = kvp.Value;
            }

            Response.Clear();
            Response.ContentType = "text/csv";
            Response.AppendHeader("Content-Disposition", $"attachment; filename=\"LOBilledLoans-{Month:yyyy'-'MM}.csv\"");
            Response.Write(dt.ToCSVWithHeader());
            Response.Flush();
            Response.End();
        }

        public static string GetBillingReason(E_BillingReason reason)
        {
            switch(reason)
            {
                case E_BillingReason.ClosingDocs:
                    return "Closing Docs Generated";
                case E_BillingReason.LoanPutInBillableStatus:
                    return "Billable Loan Status";
                default:
                    throw new UnhandledEnumException(reason);
            }
        }

        protected void BillableLoans_Sorting(object sender, GridViewSortEventArgs e)
        {
            var billableLoansSource = (DataView)BillableLoans.DataSource;

            if (BillableLoans.Attributes["SortExpression"] == e.SortExpression && BillableLoans.Attributes["SortDirection"] == SortDirection.Ascending.ToString())
            {
                e.SortDirection = SortDirection.Descending;
                BillableLoans.Attributes["SortDirection"] = SortDirection.Descending.ToString();
            }
            else
            {
                e.SortDirection = SortDirection.Ascending;
                BillableLoans.Attributes["SortDirection"] = SortDirection.Ascending.ToString();
            }

            BillableLoans.Attributes["SortExpression"] = e.SortExpression;
            billableLoansSource.Sort = $"{e.SortExpression} {(e.SortDirection == SortDirection.Ascending ? "asc" : "desc")}";
            BillableLoans.DataSource = billableLoansSource;
            BillableLoans.DataBind();
            BillableLoansPanel.Update();
        }

        protected void BillableLoans_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var label = e.Row.FindControl("LoanNumberLabel") as EncodedLabel;
            var link = e.Row.FindControl("LoanNumberLink") as LinkButton;
            var dataItem = e.Row.DataItem as DataRowView;

            if (label == null || link == null || dataItem == null)
            {
                return;
            }

            var loanId = (Guid)dataItem["LoanId"];
            label.Text = dataItem["LoanNumber"].ToString();
            label.Visible = loanId == Guid.Empty;
            link.Visible = loanId != Guid.Empty;
            link.OnClientClick = $"onLoanClick({AspxTools.JsString(loanId)});";
        }
    }
}