﻿// <copyright file="EmployeeTeamPicker.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: EduardoM
//    Date:   6/16/2014
// </summary>

namespace LendersOfficeApp.Los.Admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.Text;
    using System.Web;
    using System.Web.Script.Services;
    using System.Web.Services;
    using System.Web.SessionState;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Roles;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;

    /// <summary>
    /// Assigns or removes a team from a user.
    /// </summary>
    public partial class EmployeeTeamPicker : LendersOffice.Common.BaseServicePage
    {
        /// <summary>
        /// Gets the Employee ID.
        /// </summary>
        /// <value>A unique id.</value>
        protected Guid EmployeeId
        {
            get { return RequestHelper.GetGuid("employeeId", Guid.Empty); }
        }

        /// <summary>
        /// Gets the Role ID.
        /// </summary>
        /// <value>A unique id.</value>
        protected Guid RoleId
        {
            get { return RequestHelper.GetGuid("roleId"); }
        }

        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The Load Trigger.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterService("EditTeamsService", "/los/admin/EditTeamsService.aspx");
            Role role = Role.Get(this.RoleId);
            this.RegisterCSS("stylesheet.css");
            pageHeader.InnerText = "Select teams for " + role.ModifiableDesc + " role:";


            List<Team> assignedTeams = new List<Team>();
            List<Team> allTeams =  new List<Team>();
            List<Team> primaryTeams =  new List<Team>();
            if (this.EmployeeId != Guid.Empty)
            {
                EmployeeDB employee = new EmployeeDB(this.EmployeeId, PrincipalFactory.CurrentPrincipal.BrokerId);
                employee.Retrieve();
                CanBeMemberOfMultipleTeams.Checked = employee.CanBeMemberOfMultipleTeams;
                if (!RequestHelper.GetSafeQueryString("CanBeMemberOfMultipleTeams").Equals(string.Empty))
                {
                    CanBeMemberOfMultipleTeams.Checked = RequestHelper.GetBool("CanBeMemberOfMultipleTeams");
                }

                assignedTeams = (List<Team>)Team.ListTeamsByUserInRole(this.EmployeeId, this.RoleId);
                primaryTeams = (List<Team>)Team.ListPrimaryTeamByUserInRole(this.EmployeeId, this.RoleId);
            }

            bool hasPrimary = primaryTeams.Count > 0;

            foreach (Team team in (List<Team>)Team.ListTeamsAtLender(string.Empty))
            {
                if (team.RoleId == this.RoleId)
                {
                    allTeams.Add(team);
                }
            }

            DataTable m_data = new DataTable();
            m_data.Columns.Add("IsPrimary");
            m_data.Columns.Add("IsAssigned");
            m_data.Columns.Add("TeamName");
            m_data.Columns.Add("TeamId");

            bool firstTeam = true;
            foreach (Team team in allTeams)
            {
                var row = m_data.NewRow();
                row["TeamName"] = team.Name;
                row["TeamId"] = team.Id;
                foreach (Team primaryTeam in primaryTeams)
                {
                    if (team.Id.Equals(primaryTeam.Id))
                    {
                        row["IsPrimary"] = true;
                        break;
                    }
                }

                foreach (Team assignedTeam in assignedTeams)
                {
                    if (team.Id.Equals(assignedTeam.Id))
                    {
                        row["IsAssigned"] = true;
                        break;
                    }
                }

                m_data.Rows.Add(row);
                if (firstTeam && !hasPrimary)
                {
                    row["IsPrimary"] = true;
                    firstTeam = false;
                }
            }

            DataSet set = new DataSet();

            if (m_data.Rows.Count != 0)
            {
                set.Tables.Add(m_data);
                TeamsGrid.DataSource = set;
                TeamsGrid.DataBind();
            }
            else
            {
                m_data.Rows.Add(m_data.NewRow());
                set.Tables.Add(m_data);
                TeamsGrid.DataSource = set;
                TeamsGrid.DataBind();
                TeamsGrid.Rows[0].Visible = false;
            }
        }
    }
}
