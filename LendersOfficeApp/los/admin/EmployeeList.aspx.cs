using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.UI.HtmlControls;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.admin
{
	public partial class EmployeeList : LendersOffice.Common.BasePage
	{

        private BrokerDB m_brokerDB;

        protected BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}
        protected BrokerDB CurrentBrokerDB
        {
            get
            {
                if (m_brokerDB != null)
                {
                    return m_brokerDB;
                }
                return m_brokerDB = BrokerDB.RetrieveById(CurrentUser.BrokerId);
            }
        }

        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }

		private class EmployeeDesc
		{
			/// <summary>
			/// Keep track of each row's details.
			/// </summary>

			private String   m_EmployeeName = String.Empty;
			private String          m_Login = String.Empty;
			private String           m_Type = String.Empty;
			private String         m_Branch = String.Empty;
			private Boolean      m_IsAOwner = false;
			private Boolean      m_CanLogin = false;
			private Boolean      m_IsActive = false;
			private Guid       m_EmployeeId = Guid.Empty;
			private int		m_LicenseNumber = -1; // 05/10/06 mf.  OPM 4834.
            private string         m_GroupList = string.Empty;

			#region ( Desc properties )

			public String EmployeeStatus
			{
				// Access member.

				get
				{
					if( m_IsActive == true )
					{
						if( m_Type != "" )
						{
							if( m_CanLogin == true )
							{
								return "Active";
							}
							else
							{
								return "Disabled";
							}
						}
						else
						{
							return "New";
						}
					}
					else
					{
						return "Inactive";
					}
				}
			}

			public String EmployeeName
			{
				// Access member.

				set
				{
					m_EmployeeName = value;
				}
				get
				{
					return m_EmployeeName;
				}
			}

			public String Login
			{
				// Access member.

				set
				{
					m_Login = value;
				}
				get
				{
					return m_Login;
				}
			}

			public String Type
			{
				// Access member.

				set
				{
					m_Type = value;
				}
				get
				{
					return m_Type;
				}
			}

			public String Branch
			{
				// Access member.

				set
				{
					m_Branch = value;
				}
				get
				{
					return m_Branch;
				}
			}

			public Boolean IsAOwner
			{
				// Access member.

				set
				{
					m_IsAOwner = value;
				}
				get
				{
					return m_IsAOwner;
				}
			}

			public Boolean CanLogin
			{
				// Access member.

				set
				{
					m_CanLogin = value;
				}
				get
				{
					return m_CanLogin;
				}
			}

			public Boolean IsActive
			{
				// Access member.

				set
				{
					m_IsActive = value;
				}
				get
				{
					return m_IsActive;
				}
			}

			public Guid EmployeeId
			{
				// Access member.

				set
				{
					m_EmployeeId = value;
				}
				get
				{
					return m_EmployeeId;
				}
			}

			public int LicenseNumber
			{
				// Access member.

				set
				{
					m_LicenseNumber = value;
				}
				get
				{
					return m_LicenseNumber;
				}
			}

            public string GroupList
            {
                // Access member.

                set
                {
                    m_GroupList = value;
                }
                get
                {
                    return m_GroupList;
                }
            }


			#endregion

		}
        private string GetString(object o)
        {
            if (o == null || o is DBNull)
            {
                return string.Empty;
            }
            else
            {
                return (string)o;
            }
        }
        private bool GetBool(object o)
        {
            if (o == null || o is DBNull)
            {
                return false;
            }
            else
            {
                return (bool)o;
            }
        }
        private int GetInt(object o, int defaultValue)
        {
            if (o == null || o is DBNull)
            {
                return defaultValue;
            }
            else
            {
                return (int)o;
            }
        }
        private Guid GetGuid(object o)
        {
            if (o == null || o is DBNull)
            {
                return Guid.Empty;
            }
            else
            {
                return (Guid)o;
            }
        }

        private List<EmployeeDesc> RetrieveEmployeeList(bool isActiveFilter)
        {
            List<EmployeeDesc> list = new List<EmployeeDesc>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", CurrentUser.BrokerId),
                                            new SqlParameter("@IsActiveFilter", isActiveFilter)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(CurrentUser.BrokerId, "ListEmployeeByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    EmployeeDesc employee = new EmployeeDesc();
                    employee.EmployeeName = GetString(reader["UserLastNm"]) + ", " + GetString(reader["UserFirstNm"]);
                    employee.Type = GetString(reader["Type"]);
                    employee.Login = GetString(reader["LoginNm"]);
                    employee.IsAOwner = GetBool(reader["IsAccountOwner"]);
                    employee.CanLogin = GetBool(reader["CanLogin"]);
                    employee.IsActive = GetBool(reader["IsActive"]);
                    employee.EmployeeId = (Guid)reader["EmployeeId"];
                    employee.LicenseNumber = GetInt(reader["LicenseNumber"], -1);
                    employee.Branch = GetString(reader["BranchNm"]);
                    Guid userId = GetGuid(reader["EmployeeUserId"]);
                    if (CurrentBrokerDB.BillingVersion == E_BrokerBillingVersion.New)
                    {
                        throw new NotSupportedException();
                    }

                    EmployeePopulationMethodT? permissionPopulationMethod = null;

                    if (!Convert.IsDBNull(reader["PopulatePmlPermissionsT"]))
                    {
                        permissionPopulationMethod = (EmployeePopulationMethodT)reader["PopulatePmlPermissionsT"];
                    }

                    BrokerUserPermissions permission = new BrokerUserPermissions(
                        CurrentUser.BrokerId, 
                        employee.EmployeeId, 
                        GetString(reader["Permissions"]),
                        employee.Type,
                        reader["PmlBrokerId"] as Guid? ?? Guid.Empty,
                        permissionPopulationMethod);

                    if (!permission.IsInternalBrokerUser())
                    {
                        list.Add(employee);
                    }
                }
            }

            ILookup<Guid,GroupEmployeeRecord> groupInfo = GroupDB.GetBrokerEmployeeGroupMembershipByEmployeeId(this.CurrentUser.BrokerId);

            foreach (EmployeeDesc employee in list)
            {
                StringBuilder sb = new StringBuilder();

                if (groupInfo.Contains(employee.EmployeeId))
                {
                    foreach (GroupEmployeeRecord record in groupInfo[employee.EmployeeId])
                    {
                        sb.Append(record.GroupName);
                        sb.Append(",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                }

                employee.GroupList = sb.ToString();
            }

            return list;

        }
        private void BindActiveUsers() 
        {
			m_dg.DefaultSortExpression = "EmployeeStatus ASC, Login ASC";
			m_dg.DataSource = ViewGenerator.Generate( RetrieveEmployeeList(true) );
			m_dg.DataBind();

        }

		private void BindInactiveUsers() 
        {
			m_dg.Columns[ 5 ].Visible = false;
			m_dg.DataSource = ViewGenerator.Generate( RetrieveEmployeeList(false) );
			m_dg.DataBind();

        }

		protected void PageLoad( object sender , System.EventArgs e )
		{
            ((BasePage)Page).IncludeStyleSheet("~/css/Tabs.css");
			
			RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            if ( CurrentUser.BrokerId != Guid.Empty) 
            {
				// 8/30/2004 kb - We make special employees invisible in the grid,
				// so we need to fetch employee's permissions because we scrutinize
				// certain ones (test if on) to filter out employees.

                if (Page.Request.Params["__EVENTTARGET"] == "changeTab" && Page.Request.Params["__EVENTARGUMENT"] == "1"
                    || (ViewState["selectedTabIndex"] != null && (Int32)ViewState["selectedTabIndex"] == 1 && Page.Request.Params["__EVENTTARGET"] != "changeTab"))
                {
                    BindInactiveUsers();
                    tab1.Attributes.Add("class", "selected");
                    tab0.Attributes.Remove("class");
                    ViewState["selectedTabIndex"] = 1;
                }
                else
                {
                    BindActiveUsers();
                    tab0.Attributes.Add("class", "selected");
                    tab1.Attributes.Remove("class");
                    ViewState["selectedTabIndex"] = 0;
                }
                     
            }
        }

		protected string GetImages(object accountOwner) 
		{
			bool isAccountOwner = false;

			if (!(accountOwner is DBNull)) 
			{
				isAccountOwner = (bool) accountOwner;
			}

			if (isAccountOwner) 
				return "../../images/profile.gif";
			else 
				return "../../images/spacer.gif";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);

		}
        #endregion

        protected void m_dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != System.Web.UI.WebControls.ListItemType.Item && e.Item.ItemType != System.Web.UI.WebControls.ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView rowView = e.Item.DataItem as DataRowView;

            var employeeId = (Guid)rowView["EmployeeID"];
            HtmlAnchor editLink = (HtmlAnchor)e.Item.FindControl("EditLink");
            editLink.Attributes["data-empId"] = employeeId.ToString();

            var isOwner = rowView["IsAOwner"];
            HtmlImage ownerImg = (HtmlImage)e.Item.FindControl("OwnerImg");
            ownerImg.Src = GetImages(isOwner);

            var login = (string)rowView["Login"];
            EncodedLiteral loginLiteral = (EncodedLiteral)e.Item.FindControl("Login");
            loginLiteral.Text = login;

            var employeeName = (string)rowView["EmployeeName"];
            EncodedLiteral employeeNameLiteral = (EncodedLiteral)e.Item.FindControl("EmployeeName");
            employeeNameLiteral.Text = employeeName;

            var branch = (string)rowView["Branch"];
            EncodedLiteral branchLiteral = (EncodedLiteral)e.Item.FindControl("Branch");
            branchLiteral.Text = branch;

            var employeeStatus = (string)rowView["EmployeeStatus"];
            EncodedLiteral employeeStatusLiteral = (EncodedLiteral)e.Item.FindControl("EmployeeStatus");
            employeeStatusLiteral.Text = employeeStatus;

            var licenseNumber = (int)rowView["LicenseNumber"];
            EncodedLiteral licenseNumberLiteral = (EncodedLiteral)e.Item.FindControl("LicenseNumber");
            licenseNumberLiteral.Text = licenseNumber == -1 ? string.Empty : licenseNumber.ToString();
        }
    }

}
