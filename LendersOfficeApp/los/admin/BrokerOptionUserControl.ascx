<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BrokerOptionUserControl.ascx.cs" Inherits="LendersOfficeApp.los.admin.BrokerOptionUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ils" TagName="EmployeeRoleChooserLink" Src="../common/EmployeeRoleChooserLink.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<style type="text/css">
    .workflow-rules-spacer {
        width: 85px;
    }

    .telemarketer-option label {
        white-space: nowrap;
    }
    .modalbox{
        height: auto;
    }
</style>

<SCRIPT type="text/javascript">
	function BrokerOption_loaded() 
	{
		f_MakeIsAlwaysUseBestPriceInvisible();
		oncalccc();
	}
    
	function oncalccc(){
        var tr2 = document.getElementById('applyonsubrow'),
            cc1 = document.getElementById('<%= AspxTools.ClientId(m_CalculateclosingCostInPML) %>_0'),
            cc2 = document.getElementById('<%= AspxTools.ClientId(m_CalculateclosingCostInPML) %>_1'),
            ap1 = document.getElementById('<%= AspxTools.ClientId(m_ApplyClosingCostToGFE) %>_0'),
            ap2 = document.getElementById('<%= AspxTools.ClientId(m_ApplyClosingCostToGFE) %>_1');
        if (cc1 && cc1.checked){
            tr2.style.color = 'black';
            setDisabledAttr(ap1, false);
            setDisabledAttr(ap2, false);
        }
        else if( cc1){
            tr2.style.color = 'gray'; 
            ap2.checked = true;
            setDisabledAttr(ap1, true);
            setDisabledAttr(ap2, true);
        }
    }

    var FannieMaeEarlyCheckConfiguration = {
        TempFnmaEarlyCheckUserName:'',
        TempFnmaEarlyCheckInstutionId:'',
        TempFnmaEarlyCheckDecryptedPassword:'',
        Show : function() {
        
            this.TempFnmaEarlyCheckUserName = <%= AspxTools.JsGetElementById(FnmaEarlyCheckUserName) %>.value;
            this.TempFnmaEarlyCheckInstutionId = <%= AspxTools.JsGetElementById(FnmaEarlyCheckInstutionId) %>.value;
            this.TempFnmaEarlyCheckDecryptedPassword = <%= AspxTools.JsGetElementById(FnmaEarlyCheckDecryptedPassword) %>.value;
            
            Modal.Show("fannieMaeEarlyCheckConfigWin");
            return false;
        },      
        Hide : function(isRestore) {
        
            if (isRestore)
            {
                <%= AspxTools.JsGetElementById(FnmaEarlyCheckUserName) %>.value = this.TempFnmaEarlyCheckUserName;
                <%= AspxTools.JsGetElementById(FnmaEarlyCheckInstutionId) %>.value = this.TempFnmaEarlyCheckInstutionId;
                <%= AspxTools.JsGetElementById(FnmaEarlyCheckDecryptedPassword) %>.value = this.TempFnmaEarlyCheckDecryptedPassword;
            }
            Modal.Hide();
        }
        
    };	
    var QuickPricerConfiguration = 
    {
        isEnabled : '', 
        tempalteIdOption : '', 
        noResultMessege : '', 
        disclaimerResultMessege : '', 
        EnabledCheck : function(obj) {
             var img = document.getElementById('tempIdReq'); 
             img.src = '../../images/require_icon.gif'; 
             img.style.display = obj.checked ? "" : "none"; 
        },
        Show : function() {
           
            this.isEnabled = <%= AspxTools.JsGetElementById(IsQuickPricerEnabled) %>.checked; 
            this.tempalteIdOption = <%= AspxTools.JsGetElementById(QuickPricerTemplateId) %>.value;
             document.getElementById('tempIdReq').style.display = this.isEnabled ? "" : "none"; 
            this.noResultMessege = <%= AspxTools.JsGetElementById(QuickPricerNoResultMessage) %>.value; 
            this.disclaimerResultMessege = <%= AspxTools.JsGetElementById(QuickPricerDisclaimerAtResult) %>.value;
            Modal.Show("quickpricer");
        },
        Hide: function(restore) {
           this.CloseHelpText();
           var nrm = <%= AspxTools.JsGetElementById(QuickPricerNoResultMessage) %>;
           var dis = <%= AspxTools.JsGetElementById(QuickPricerDisclaimerAtResult) %>; 
           var templateId = <%= AspxTools.JsGetElementById(QuickPricerTemplateId) %>; 
           if ( restore ) 
           {
                <%= AspxTools.JsGetElementById(QuickPricerTemplateId) %>.value = this.tempalteIdOption;
                <%= AspxTools.JsGetElementById(IsQuickPricerEnabled) %>.checked = this.isEnabled; 
                nrm.value = this.noResultMessege; 
                dis.value = this.disclaimerResultMessege; 
                nrm.onkeyup();
                dis.onkeyup(); 
           }
           else 
           {
               if ( templateId.options[templateId.selectedIndex].value  == '00000000-0000-0000-0000-000000000000' && <%= AspxTools.JsGetElementById(IsQuickPricerEnabled) %>.checked  ) 
                {
                    alert('A valid template has to be selected when Quick Pricer is enabled.');
                    document.getElementById('tempIdReq').src = '../../images/error_icon.gif';
                    return; 
                }
                var isNrmOver = nrm.value.length > parseInt(nrm.getAttribute("maxcharactercount"));  
                var isDisOver = dis.value.length > parseInt(dis.getAttribute("maxcharactercount")); 
                
                if ( isNrmOver && isDisOver ) 
                {
                    var answer = confirm('The No Result Message and Disclaimer are too long. Would you like to automatically shorten the No Result Message and Disclaimer to 500 characters each?'); 
                    if ( answer ) 
                    { 
                        this.ShortenTextArea(nrm); 
                        this.ShortenTextArea(dis);
                    }
                    else  { return; }
                }
                else if ( isNrmOver || isDisOver  ) 
                {
                    var label = isNrmOver ? "No Result Message" : "Disclaimer"; 
                    var answer = confirm('The ' + label + ' is too long. Would you like to automatically shorten the '+ label + ' to 500 characters?'); 
                    if ( answer ) 
                    { 
                        if(isNrmOver) { this.ShortenTextArea(nrm); }
                        else { this.ShortenTextArea(dis); } 
                    }
                    else  { return; }
                }
           }
           
            document.getElementById("qpKeywords").style.display = "none"; 
            Modal.Hide();
        },
        ShortenTextArea : function(obj) 
        {
            obj.value = obj.value.substring(0, obj.getAttribute('maxcharactercount')); 
            obj.onkeyup(); 
        },
         ShowQPKeywordHelp : function() 
            {
              <%= AspxTools.JsGetElementById(QuickPricerTemplateId) %>.style.display = "none"; 
              
              var msg = document.getElementById("qpKeywords");
              msg.style.display = "";
              msg.style.top =  "130px";
              msg.style.left = "190px";
              
              //alert( msg.style.top   + " " +msg.style.left );  
   
            },
            CloseHelpText : function () 
            {
                 <%= AspxTools.JsGetElementById(QuickPricerTemplateId) %>.style.display = ""; 
                document.getElementById('qpKeywords').style.display = 'none';
            }
    };

    function BrokerOption_Data()
    {
        this.TelemarketerCanOnlyAssignToManager = '';
        this.TelemarketerCanRunPrequal = '';
        this.IsLOAllowedToEditProcessorAssignedFile = '';
        this.IsOnlyAccountantCanModifyTrustAccount = '';
        this.TemplateList = '';
    }
    function f_closeDetails(event) {
      document.getElementById("IsAEAsOfficialLoanOfficerDetails").style.display = "none";
    }
    function f_showDetails(event) {
      var msg = document.getElementById("IsAEAsOfficialLoanOfficerDetails");
      msg.style.display = "";
      msg.style.top = (event.clientY - 145 )+ "px";
      msg.style.left = (event.clientX - 200 ) + "px";
    }
    function f_closeFrontUserDetails() {
      document.getElementById("FrontUserDetails").style.display = "none";
    }
    function f_showFrontUserDetails(event) {
      var msg = document.getElementById("FrontUserDetails");
      msg.style.display = "";
      msg.style.top = (event.clientY - 80)+ "px";
      msg.style.left = (event.clientX - 110) + "px";
    }
    function f_closeLoanTempDetails() {
      document.getElementById("DefaultLoanTemplateDetails").style.display = "none";
    }
    function f_showLoanTempDetails(event) {
      var msg = document.getElementById("DefaultLoanTemplateDetails");
      msg.style.display = "";
      msg.style.top = (event.clientY - 110 )+ "px";
      msg.style.left = (event.clientX - 140 ) + "px";
    }
    
    function f_closeNewLoansDetails() {
      document.getElementById("AllowNewLoansDetails").style.display = "none";
    }
    function f_showNewLoansDetails(event)
    {
      var msg = document.getElementById("AllowNewLoansDetails");
      msg.style.display = "";
      msg.style.top = (event.clientY - 110 )+ "px";
      msg.style.left = (event.clientX - 140 ) + "px";
    }
    
    function f_isMaxLengh(obj, len, label)
    {
		if ( obj && len && label&& obj.value.length > len  )
		{
			var passedAnnount = obj.value.length - len; 
			obj.value = obj.value.substring(0,len);
			alert("Note: Your "  + label + " has been truncated by " + passedAnnount +  " characters to fit the " + len + " character limit." );  
		} 
			
    }
	
	<% if ( IsEnablePTMDocMagicSeamlessInterface ) { %>
    function dm_conf() 
	{	
		showModal( '/los/admin/DocMagicConfigurationEditor.aspx');
		return false;
	}
	<% } %>
	<% if ( IsEnablePTMComplianceEaseIndicator ) { %>
	function ce_conf()
	{
	    showModal( '/los/admin/ComplianceEaseConfigurationEditor.aspx');
	    return false;
	}
	<% } %>
	<% if (IsEnablePTMComplianceEagleAuditIndicator) { %>
	function ceagle_conf()
	{
	    showModal( '/los/admin/ComplianceEagleConfigurationEditor.aspx');
	    return false;
	}
	<% } %>
	<% if ( IsEnabledMIVendorIntegration ) { %>
    function pmi_config() 
	{	
        showModal( '/los/admin/MIVendorBrokerAccountDetails.aspx', null, 'dialogWidth: 500px', null, null, { hideCloseButton: true });
		return false;
	}
	<% } %>	
	
	function updateUrl()
	{
	    var $this = $("#BrokerOption_EmployeeResourcesURL");
	    if($this.val().indexOf("http:") < 0 && $this.val().indexOf("https:") < 0 &&  $this.val() != "")
	    {
	        var text = $this.val();
	        $this.val("http://" + text); 
	    }
	}
	
	function f_MakeIsAlwaysUseBestPriceInvisible()
	{	
		var IsBestPriceEnabledValue1 = document.getElementById(<%= AspxTools.JsString(IsBestPriceEnabled.ClientID) %> + '_0');
		var IsBestPriceEnabledValue2 = document.getElementById(<%= AspxTools.JsString(IsBestPriceEnabled.ClientID) %> + '_1');
		var IsAlwaysUseBestPricePanelVisible = document.getElementById("IsAlwaysUseBestPricePanel");
		if(IsBestPriceEnabledValue1.checked == true)
		{
			IsAlwaysUseBestPricePanelVisible.disabled = false;
		    document.getElementById("<%= AspxTools.ClientId(m_IsAlwaysUseBestPrice) %>_0").disabled = false
			document.getElementById("<%= AspxTools.ClientId(m_IsAlwaysUseBestPrice) %>_1").disabled = false
		}
		if(IsBestPriceEnabledValue2.checked == true)
		{
			IsAlwaysUseBestPricePanelVisible.disabled = true;
		    document.getElementById("<%= AspxTools.ClientId(m_IsAlwaysUseBestPrice) %>_0").disabled = true
			document.getElementById("<%= AspxTools.ClientId(m_IsAlwaysUseBestPrice) %>_1").disabled = true
			document.getElementById("<%= AspxTools.ClientId(m_IsAlwaysUseBestPrice) %>_0").checked = false;
			document.getElementById("<%= AspxTools.ClientId(m_IsAlwaysUseBestPrice) %>_1").checked = true;
		}
	}

    function clearWorkflowRulesController() {
        $(<%=AspxTools.JsGetElementById(this.WorkflowRulesControllerName)%>).text('None');
        $(<%=AspxTools.JsGetElementById(this.WorkflowRulesControllerId)%>).val(<%=AspxTools.JsString(Guid.Empty)%>);
    }

    function pickWorkflowRulesController() {
        showModal('/newlos/Tasks/RoleAndUserPicker.aspx?IsBatch=1&IsPipeline=1&NoPML=1&hideConditionsWarning=1', null, null, null, function(args) { 
            if (args.OK) {
                $(<%=AspxTools.JsGetElementById(this.WorkflowRulesControllerName)%>).text(args.name);
                $(<%=AspxTools.JsGetElementById(this.WorkflowRulesControllerId)%>).val(args.id)
            }
        }, { hideCloseButton: true });
    }
</SCRIPT>


<TABLE class="FormTable" cellspacing="0" cellPadding="0" border="0" style="MARGIN:8px">
    <% if ( IsPmlEnabled || IsLpeEnabled ) { %>
	<tr>
		<td class="FieldLabel" width="180" noWrap>
			Default Price Group
		</td>
		<td>
			<asp:DropDownList id="BrokerLpePriceGroupIdDefault" runat="server" Width="180"></asp:DropDownList>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
	<tr>
	    <td class="FieldLabel" width="180" noWrap>
	        Employee Resources URL
	    </td>
	    <td>
	        <asp:TextBox runat="server" style="width:100%" onchange="updateUrl();" id="EmployeeResourcesURL"></asp:TextBox>
	    </td>
	</tr>
	<TR style=display:none>
		<TD class=FieldLabel noWrap width=210>Display Fee (Point) in 100 basis format</TD>
		<TD>
			<table cellSpacing=0 cellPadding=0 border=0>
				<tr>
					<td>
						<asp:RadioButtonList id="m_displayPmlFeeIn100Format" runat="server" />
					</td>
					<td>&nbsp;(designed for Citi only)</td>
				</tr>
			</table>
		</TD>
	</TR>
	<tr>
		<td class="FieldLabel" width="180" noWrap>
			Loan Submission Agreement
			<div style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; FONT: 11px arial; COLOR: dimgray; PADDING-TOP: 4px">
				Specify your lending agreement notice when external brokers submit loans
				for underwriting (max 1500 characters). 
			</div>
		</td>
		<td>
			<asp:TextBox id="LpeSubmitAgreement" runat="server" style="PADDING-RIGHT: 4px;PADDING-LEFT: 4px;PADDING-BOTTOM: 4px;PADDING-TOP: 4px" Width="510px" TextMode="MultiLine" Rows="5" ></asp:TextBox>
		</td>
	</tr>
	<tr>
		<td class="FieldLabel" width="180" noWrap>
			FTHB and Housing History <br> Help Text <br>
			<div style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; FONT: 11px arial; COLOR: dimgray; PADDING-TOP: 4px">
				Specify the first time home buyer help text (max 1000 characters). 
			</div>
		</td>
		<td>
			<asp:TextBox id="FthbCustomDefinition" runat="server" style="PADDING-RIGHT: 4px;PADDING-LEFT: 4px;PADDING-BOTTOM: 4px;PADDING-TOP: 4px" Width="510px" TextMode="MultiLine" Rows="5" MaxLength="1000"></asp:TextBox>
		</td>
	</tr>
	<tr style=display:none>
		<TD class=FieldLabel noWrap width=180>Verify PML User Licenses <br>(FOR FUTURE USE)</TD>
		<td>
			<asp:RadioButtonList disabled=true id="m_RequireVerifyLicenseByState" runat="server" />
		</td>
	</tr>
	<% } %>
	<tr>
	<td class="FieldLabel" width="180" noWrap>
		Configuration Options
		<div style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; FONT: 11px arial; COLOR: dimgray; PADDING-TOP: 4px">
			Scroll to see more options.
		</div>
	</td>
	<td>
	    <% if ( IsPmlEnabled || IsLpeEnabled ) { %>
		<div style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 4px; BORDER-TOP: 2px groove; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 2px groove; WIDTH: 540px; PADDING-TOP: 4px; BORDER-BOTTOM: 2px groove; HEIGHT: 170px">
		<% } else { %>
		<div style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 4px; BORDER-TOP: 2px groove; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; BORDER-LEFT: 2px groove; WIDTH: 540px; PADDING-TOP: 4px; BORDER-BOTTOM: 2px groove; HEIGHT: 395px">
		<% } %>
			<table cellpadding="0" cellspacing="0">
			<% if( IsMarketingEnabled ) {%>
			<tr>
			<td noWrap>
				Allow call center agents to assign leads to:
			</td>
			<td noWrap>
				<asp:RadioButtonList id=TelemarketerCanOnlyAssignToManager runat="server" RepeatDirection="Horizontal" CssClass="telemarketer-option">
					<asp:ListItem Value="N">Anyone</asp:ListItem>
					<asp:ListItem Value="Y">Managers only</asp:ListItem>
				</asp:RadioButtonList>
			</td>
			</tr>
			<% } %>
			<tr id="AllowCallCenterAgentRunPrequalRow" runat="server">
			    <td noWrap>Allow call center agents to run prequal on leads?</td>
			    <td noWrap><asp:RadioButtonList id=TelemarketerCanRunPrequal runat="server" /></td>
			</tr>
			<tr>
			    <td noWrap>Allow <a href='#"' onclick='f_showFrontUserDetails(event);'?>front users</a> to edit loans assigned to processors?</td>
			    <td noWrap><asp:RadioButtonList id=IsLOAllowedToEditProcessorAssignedFile runat="server" /></td>
			</tr>
			<tr>
			    <td noWrap>Allow creating loans from our blank template?</td>
			    <td noWrap><asp:RadioButtonList id="AllowCreatingLoanFromBlankTemplate" runat="server" /></td>
			</tr>
			<tr>
			    <td nowrap>Allow non-accountant users to modify trust account info?</td>
			    <td nowrap><asp:RadioButtonList id=IsNonAccountantCanModifyTrustAccount runat="server" /></td>
			</tr>			
			<tr>
			    <td nowrap>Use AE as official loan officer for new loans?&nbsp; <a href='#"' onclick='f_showDetails(event);'?>details</a></td>
			    <td nowrap><asp:RadioButtonList ID="IsAEAsOfficialLoanOfficer" Runat=server/></td>
			</tr>
			
			<%-- 04/25/06 mf - OPM 4182 --%>
			<tr style="DISPLAY: none">
			<td nowrap>Automatically generate liability payoff conditions?</td>
			<td nowrap><asp:RadioButtonList ID="IsAutoGenerateLiabilityPayOffConditions" Runat=server/></td>
			</tr>
			
			<% if ( IsPmlEnabled ) { %>
			<tr>
			    <td nowrap>Allow 'Create new file' option for Originating Company users?&nbsp; <a href='#"' onclick='f_showNewLoansDetails(event);'?>details</a></td>
			    <td nowrap><asp:RadioButtonList ID="IsAllowExternalUserToCreateNewLoan" Runat="server" /></td>
			</tr>

			<tr>
			    <td nowrap>Display price group name to internal users?</td>
			    <td nowrap><asp:RadioButtonList ID="IsShowPriceGroupInEmbeddedPml" Runat="server" /></td>
			</tr>
			<tr>
			    <td nowrap>Allow users to submit loans for an exception?</td>
				<td nowrap><asp:RadioButtonList ID="IsLpeManualSubmissionAllowed" Runat=server /></td>
			</tr>
			<tr>
				<td nowrap>Allow users in the Originator Portal to upload Calyx Point files?</td>
				<td nowrap><asp:RadioButtonList ID="IsPmlPointImportAllowed" Runat=server /></td>
			</tr>
			<tr runat="server" id="AllowHelocsRow">
				<td nowrap>Allow creating HELOCs in the Originator Portal?</td>
				<td nowrap><asp:RadioButtonList ID="IsHelocsAllowedInPml" Runat=server /></td>
			</tr>
			<tr>
				<td nowrap>Allow creating Standalone 2nd Liens in the Originator Portal?</td>
				<td nowrap><asp:RadioButtonList ID="IsStandAloneSecondAllowedInPml" Runat=server /></td>
			</tr>
			<tr>
				<td nowrap>Allow combo loans in the pricing engine?</td>
				<td nowrap><asp:RadioButtonList ID="Is8020ComboAllowedInPml" Runat=server /></td>
			</tr>
			<tr>
				<td noWrap>Comply with State Prepay Laws?</td>
				<td noWrap><asp:RadioButtonList id="IsAllPrepaymentPenaltyAllowed" runat="server" /></td>
			</tr>
			<% } %>
			<tr>
				<td nowrap >Allow 'Best Prices per Program' in the pricing engine?</td>
				<td nowrap><asp:RadioButtonList ID="IsBestPriceEnabled" Runat="server" onclick="f_MakeIsAlwaysUseBestPriceInvisible()" /></td>
			</tr> 
			<tbody id="IsAlwaysUseBestPricePanel">
	  		<tr>
				<td nowrap >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:17">&bull;</span>&nbsp;
					Always use 'Best Prices per Program' in the pricing engine
				</td>
				<td nowrap><asp:RadioButtonList ID="m_IsAlwaysUseBestPrice" Runat="server" /></td>
			</tr>  
			</tbody>
			<tr>
				<td nowrap >
					Show Underwriter in Pricing Submission Certificate and Summary
				</td>
				<td nowrap><asp:RadioButtonList ID="m_ShowUnderwriterInPMLCertificate" Runat="server" /></td>
			</tr> 
			<tr>
				<td nowrap >
					Show Junior Underwriter in Pricing Submission Certificate and Summary
				</td>
				<td nowrap><asp:RadioButtonList ID="m_ShowJuniorUnderwriterInPMLCertificate" Runat="server" /></td>
			</tr> 
			<tr>
				<td nowrap >Show Processor in Pricing Submission Certificate and Summary</td>
				<td nowrap><asp:RadioButtonList ID="m_ShowProcessorInPMLCertificate" Runat="server" /></td>
			</tr>
			<tr>
				<td nowrap >Show Junior Processor in Pricing Submission Certificate and Summary</td>
				<td nowrap><asp:RadioButtonList ID="m_ShowJuniorProcessorInPMLCertificate" Runat="server" /></td>
			</tr>
			<tbody runat="server" id="CalculateClosingCostInPml">
			    <tr>
			        <td nowrap="nowrap">
			            Calculate closing costs in the pricing engine? 
			        </td>
			        <td>
			            <asp:RadioButtonList ID="m_CalculateclosingCostInPML" onclick="oncalccc()"  runat="server" />
			        </td>
			    </tr>
			    
			    <tr id="applyonsubrow">
			        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:17">&bull;</span>&nbsp;
			        Apply closing costs to loan file on submission</td>
			        <td>
			            <asp:RadioButtonList ID="m_ApplyClosingCostToGFE" runat="server" />
			        </td>
			    </tr>
			    
			</tbody>
			<tr>
			    <td nowrap>Trigger APR Redisclosure notification if APR goes down</td>
			    <td nowrap><asp:RadioButtonList ID="m_TriggerAprRediscNotifForAprDecrease" runat="server" /></td>
			</tr>
                <tr>
                    <td nowrap>Show Loan Product Identifier field</td>
                    <td>
                        <asp:RadioButtonList ID="ShowLoanProductIdentifier" runat="server"></asp:RadioButtonList>
                    </td>
                </tr>
			</table>
		</div></div>
	</td>
	</tr>

    <tr id="QuickPricerRow" runat="server">
        <td class="FieldLabel">QuickPricer</td>
        
        <td>
            <span id="quickpricershowlink"> <a href="javascript:void(0)" onclick="QuickPricerConfiguration.Show()" > Edit Configuration </a>  </span>
        </td>
    </tr>

	<% if ( IsEnabledMIVendorIntegration ) { %>    
        <tr id="MIVendorRow" runat="server">
        <td class="FieldLabel">PMI Providers</td>
        
        <td>
            <a href="#" onclick="return pmi_config()"> Edit Configuration </a>
        </td>
    </tr>
  	<% } %>
    
	<% if (IsEnablePTMDocMagicSeamlessInterface && HasVendors)
    { %>
        <tr>
			<td class="FieldLabel">
				<%=AspxTools.HtmlString(VendorName)%> 	
			</td>	
			<td>
				<a href="#" onclick="return dm_conf()"> Edit Configuration </a>
			</td>
		</tr>	
	<%} %>
	<% if (IsEnablePTMComplianceEaseIndicator)
    { %>
    	<tr>
			<td class="FieldLabel">
				ComplianceEase 	
			</td>	
			<td>
				<a href="#" onclick="return ce_conf()"> Edit Configuration </a>
			</td>
		</tr>	
	<%} %>
	<% if (IsEnablePTMComplianceEagleAuditIndicator)
    { %>
    	<tr>
			<td class="FieldLabel">
				ComplianceEagle
			</td>	
			<td>
				<a href="#" onclick="return ceagle_conf()"> Edit Configuration </a>
			</td>
		</tr>	
	<%} %>
	
	<tr id="FannieMaeEarlyCheckRow" runat="server">
	    <td class="FieldLabel">Fannie Mae EarlyCheck</td>
	    <td><a href="#" onclick="return FannieMaeEarlyCheckConfiguration.Show();">Edit Configuration</a></td>
	</tr>
    
    <tr>
	    <td class="FieldLabel" width="180" noWrap>Default Lock Desk</td>
	    <td>
	        <ils:EmployeeRoleChooserLink ID="m_lockDeskChoice" runat="server" Data="None" Text="None" Role="LockDesk" Desc="Lock Desk" Mode="Search" Width="80px" GenerateScripts="true">
							<FixedChoice Data="None" Text="None">
								None
							</FixedChoice><FixedChoice data="assign" Text="assign" >Pick Lock Desk</FixedChoice>
                    </ils:EmployeeRoleChooserLink>
	    </td>
	</tr>
    <tr runat="server" id="WorkflowRulesControllerRow">
        <td class="FieldLabel">Workflow Rules Controller</td>
        <td>
            <input type="hidden" runat="server" id="WorkflowRulesControllerId" />
            <span runat="server" id="WorkflowRulesControllerName" class="workflow-rules-spacer">None</span>

            [ 
            <a id="ClearWorkflowRulesContoller" onclick="clearWorkflowRulesController();">None</a> 
            | 
            <a id="PickWorkflowRulesContoller" onclick="pickWorkflowRulesController();">Pick Workflow Rules Controller</a> 
            ]
        </td>
    </tr>
	
	</TABLE></ASP:PANEL>
			<div id="IsAEAsOfficialLoanOfficerDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:230px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; HEIGHT:auto; BACKGROUND-COLOR:whitesmoke">
			<table style><tr><td>Enabling this option allows you to use the lender account executive's info for the loan officer record in the loan's "Official Contact List" (when the loan is originated by an AE or a PML user with an associated AE).</td></tr>
			<tr><td align=center>[ <a href="#" onclick="f_closeDetails(event);">Close</a> ]</td></tr>
			</table>
			</div>
			<div id="FrontUserDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:230px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION: absolute; HEIGHT:auto; BACKGROUND-COLOR:whitesmoke">
			<table width="100%"><tr><td>"<b>Front users</b>" include loan officers and lender account executives.</td></tr>
			<tr><td align=center>[ <a href="#" onclick="f_closeFrontUserDetails();">Close</a> ]</td></tr>
			</table>
			</div>
			<div id="DefaultLoanTemplateDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:230px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; HEIGHT:auto; BACKGROUND-COLOR:whitesmoke">
			<table width="100%"><tr><td>This template will be used for loans submitted through PML or created by your AE's.</td></tr>
			<tr><td align=center>[ <a href="#" onclick="f_closeLoanTempDetails();">Close</a> ]</td></tr>
			</table>
			</div>
			<div id="AllowNewLoansDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:230px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; HEIGHT:auto; BACKGROUND-COLOR:whitesmoke">
			<table width="100%"><tr><td>Disabling this option will restrict your Originating Company users to upload Fannie Mae or Point files only. &nbsp;Users will not be allowed to create blank new loans.</td></tr>
			<tr><td align=center>[ <a href="#" onclick="f_closeNewLoansDetails();">Close</a> ]</td></tr>
			</table>
			
			
			</div>
			
    <div class="modalbox" id="quickpricer">
        <h4 class="page-header">Quick Pricer Configuration</h4>
        <table  >
            <tr><td style="padding-bottom:5px"  ><ml:EncodedLabel runat="server"  AssociatedControlID="IsQuickPricerEnabled" CssClass="FieldLabel"  >Turn on Quick Pricer?</ml:EncodedLabel> &nbsp;&nbsp;&nbsp;
                <asp:CheckBox runat="server"  onclick="QuickPricerConfiguration.EnabledCheck(this)" style="font-size: 11px"  ID="IsQuickPricerEnabled" Text="Yes" /> 
              </td> </tr>
              <tr><td style="padding-bottom:5px" >  <ml:EncodedLabel runat="server" CssClass="FieldLabel"  AssociatedControlID="QuickPricerTemplateId">Template</ml:EncodedLabel>&nbsp;&nbsp;<asp:DropDownList runat="server" ID="QuickPricerTemplateId" />&nbsp;<img src="../../images/require_icon.gif" id="tempIdReq" alt="Required"/></td></tr>
              <tr><td style="padding-bottom:0px">  <ml:EncodedLabel runat="server" CssClass="FieldLabel"  AssociatedControlID="QuickPricerNoResultMessage">No Result Message</ml:EncodedLabel></td></tr>
              <tr><td>  <asp:TextBox runat="server"  id="QuickPricerNoResultMessage"  maxcharactercount="500" Width="400px" Height="70px" TextMode="MultiLine" /></asp:TextBox> </td> </tr>                  
              <tr><td style="padding-bottom:0px">   <ml:EncodedLabel runat="server" CssClass="FieldLabel"  AssociatedControlID="QuickPricerDisclaimerAtResult"> Disclaimer </ml:EncodedLabel> <a style="text-decoration: underline" onclick="QuickPricerConfiguration.ShowQPKeywordHelp()" >?</a></td> </tr>
              <tr><td>   <asp:TextBox runat="server" ID="QuickPricerDisclaimerAtResult"  maxcharactercount="500"   Width="400px" Height="70px"  TextMode="MultiLine" /> </td> </tr>
              <tr><td align="center"><input type="button" onclick="QuickPricerConfiguration.Hide(false)" value="  OK  " /> <input type="button" onclick="QuickPricerConfiguration.Hide(true);" value="Cancel" />   </td></tr>
        </table>                
        </div>
        

        <div id="qpKeywords" style="BORDER-RIGHT:black 3px solid;font-size: 1em; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:425px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; HEIGHT:auto; BACKGROUND-COLOR:whitesmoke;z-index: 9999">

<b>[LockPeriod]</b> - Displays rate lock period days.<br />
<b>[Impound]</b> - Displays either ?impound? or ?no impound?.<br />
<br />
<b>Example:</b><br />
"Quick Pricer assumes [LockPeriod] day pricing and [Impound]s."<br /><br />
If the QP template is set with a 30 day lock period and impounds, the result would be:<br /><br />
"Quick Pricer assumes 30 day pricing and impounds."<br /><br />
<div style="text-align:center" >[<a onclick="QuickPricerConfiguration.CloseHelpText();" >Close</a>]</div>

        </div>
    <div class="modalbox" id="fannieMaeEarlyCheckConfigWin">
        <h4 class="page-header">Fannie Mae EarlyCheck Configuration</h4>
        <table>
            <tr>
                <td>Login</td>
                <td><asp:TextBox ID="FnmaEarlyCheckUserName" runat="server" /></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><asp:TextBox ID="FnmaEarlyCheckDecryptedPassword" runat="server" TextMode="Password" /> (leave blank if not changing)</td>
            </tr>
            <tr>
                <td>Institution Id</td>
                <td><asp:TextBox ID="FnmaEarlyCheckInstutionId" runat="server" /></td>
            </tr>
            <tr>
                <td align="center">
                    <input type="button" value="Update"  onclick="FannieMaeEarlyCheckConfiguration.Hide(false);"/>&nbsp;<input type="button" value="Cancel" onclick="FannieMaeEarlyCheckConfiguration.Hide(true);"/>
                </td>
            </tr>
        </table>
    </div>
