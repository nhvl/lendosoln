<%@ Page language="c#" Codebehind="EmployeeListExport.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.EmployeeListExport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>EmployeeListExport</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body MS_POSITIONING="FlowLayout">
		<form id="EmployeeListExport" method="post" runat="server">
			<div style="WIDTH: 100%; TEXT-ALIGN: center;">
				<div style="WIDTH: 100%; HEIGHT: 80px; COLOR: red; FONT: 12px arial; BORDER: 2px groove; BACKGROUND-COLOR: whitesmoke; PADDING: 16px; MARGIN: 4px; MARGIN-BOTTOM: 10px; TEXT-ALIGN: center;">
					Failed to export.
				</div>
				<input type="button" value="Close" onclick="onClosePopup();">
			</div>
		</form>
	</body>
</html>
