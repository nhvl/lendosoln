﻿#region Generated Code 
namespace LendersOfficeApp.los.admin
#endregion
{
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.CustomFavoriteFolder;
    using LendersOffice.ObjLib.DynaTree;
    using LendersOffice.Security;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;

    /// <summary>
    /// This control creates the UI for editing a user or broker's Custom Folder Navigation.
    /// </summary>
    public partial class CustomFolderNavigationControl : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets or sets a value indicating whether the page is being displayed for Administrators.  If false, the user is modifying their personal favorites.
        /// </summary>
        public bool IsAdmin { get; set; } = false;

        /// <summary>
        /// Loads the User Control and registers the necessary scripts.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var page = (BasePage)this.Page;
            page.EnableJqueryMigrate = false;
            page.RegisterJsScript("jquery-ui-1.11.4.min.js");
            page.RegisterCSS("jquery-ui-1.11.custom.css");
            page.RegisterJsScript("CustomFolderNavigation.js");
            page.RegisterJsScript("dynatree/jquery.dynatree.js");
            page.RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");
            page.RegisterJsScript("DropdownCheckbox.js");
            page.RegisterCSS("CustomFolderNavigationControl.css");
            page.RegisterCSS("DropdownCheckbox.css");

            page.RegisterJsGlobalVariables(
                "rolesInfo",
                SerializationHelper.JsonNetAnonymousSerialize(Role.LendingQBRoles.Select(role => new KeyValuePair<string, string>(role.Id.ToString(), role.ModifiableDesc))));

            page.RegisterJsGlobalVariables("loanPurposes", SerializationHelper.JsonNetSerialize(Tools.ExtractDDLEntries(Tools.Bind_sLPurposeT)));
            page.RegisterJsGlobalVariables("loanTypes", SerializationHelper.JsonNetSerialize(Tools.ExtractDDLEntries(Tools.Bind_sLT)));

            var employeeGroups = GroupDB.GetAllGroups(PrincipalFactory.CurrentPrincipal.BrokerId, GroupType.Employee).Select(group => new KeyValuePair<string, string>(group.GroupId.ToString(), group.GroupName)).ToList();

            page.RegisterJsGlobalVariables("employeeGroups", SerializationHelper.JsonNetSerialize(employeeGroups));
            var children = CustomFavoriteFolderHelper.RetrieveCustomFavoriteFolderTreeByBrokerId(
                PrincipalFactory.CurrentPrincipal.BrokerId,
               this.IsAdmin ? (null as Guid?) : PrincipalFactory.CurrentPrincipal.EmployeeId, 
               false);
            CustomFavoriteFolderNode treeRoot;

            if (children.Count == 0)
            {
                treeRoot = new CustomFavoriteFolderNode();
                treeRoot.Folder = new CustomFavoriteFolder(PrincipalFactory.CurrentPrincipal.BrokerId, this.IsAdmin ? (null as Guid?) : PrincipalFactory.CurrentPrincipal.EmployeeId);
                treeRoot.Children = children;
            }
            else
            {
                treeRoot = children.First() as CustomFavoriteFolderNode;
            }

            // These dummy fields are required so we can extract the class type, which is required for deserialization.
            page.RegisterJsGlobalVariables("dummyPage", SerializationHelper.JsonNetSerializeWithTypeNameHandling(new CustomFavoritePageNode()));
            page.RegisterJsGlobalVariables("dummyFolder", SerializationHelper.JsonNetSerializeWithTypeNameHandling(new CustomFavoriteFolderNode()));
            page.RegisterJsGlobalVariables("isAdmin", this.IsAdmin);

            LendersOffice.Common.UI.Tree tree = new LendersOffice.Common.UI.Tree(page);

            if (this.IsAdmin)
            {
                tree.SpecialTypeNode += new LendersOffice.Common.UI.SpecialTypeNodeHandler(LendersOfficeApp.newlos.LeftTreeFrame.SpecialTreeNode_AdminConfig);
                tree.OnNodeCreate += new LendersOffice.Common.UI.NodeCreateHandler(LendersOfficeApp.newlos.LeftTreeFrame.TreeNode_Create_AdminConfig);
            }
            else
            {
                tree.SpecialTypeNode += new LendersOffice.Common.UI.SpecialTypeNodeHandler(LendersOfficeApp.newlos.LeftTreeFrame.SpecialTreeNode_UserConfig);
                tree.OnNodeCreate += new LendersOffice.Common.UI.NodeCreateHandler(LendersOfficeApp.newlos.LeftTreeFrame.TreeNode_Create_UserConfig);
            }

            tree.Load(Tools.GetServerMapPath((Page as BasePage).VirtualRoot + "/newlos/LoanNavigationNew.xml.config"));

            var dynaNode = tree.Render(false);
            var pageDictionary = this.PrepareTreeForSerialization(dynaNode);

            page.RegisterJsGlobalVariables("pageTreeModel", SerializationHelper.JsonNetSerialize(dynaNode));

            this.SetPageNames(treeRoot, pageDictionary);
            page.RegisterJsGlobalVariables("treeRoot", SerializationHelper.JsonNetSerializeWithTypeNameHandling(treeRoot));
            if (!this.IsAdmin)
            {
                var landingPagesRoot = tree.Render(true);
                this.PrepareTreeForSerialization(landingPagesRoot); 
                page.RegisterJsGlobalVariables("landingPageTreeModel", SerializationHelper.JsonNetSerialize(landingPagesRoot));
            }

            (Page as BaseServicePage).RegisterService("folderNavigation", "/los/admin/CustomFolderNavigationService.aspx");
        }

        /// <summary>
        /// Prepares the DynaTreeNode for serialization by updating a page's id and title for associated pages.
        /// </summary>
        /// <param name="node">A node for a tree root.</param>
        /// <returns>A flattned dictionary of page nodes.</returns>
        private Dictionary<string, List<DynaTreeNode>> PrepareTreeForSerialization(DynaTreeNode node)
        {
            var pageDictionary = CustomFavoriteFolderHelper.FlattenDynaNodePages(node, new Dictionary<string, List<DynaTreeNode>>());
            var associatedPagesDict = CustomFavoriteFolderHelper.AssociatedPages;

            foreach (var nodeList in pageDictionary.Values)
            {
                var firstPage = nodeList.FirstOrDefault();
                List<string> associatedPageIds;
                if (associatedPagesDict.TryGetValue(firstPage?.key, out associatedPageIds))
                {
                    foreach (var associatedPageId in associatedPageIds)
                    {
                        List<DynaTreeNode> associatedPages;
                        if (pageDictionary.TryGetValue(associatedPageId, out associatedPages))
                        {
                            foreach (var associatedNode in associatedPages)
                            {
                                associatedNode.key = firstPage.key;
                                associatedNode.customAttributes["pageid"] = firstPage.key;
                            }
                        }
                    }
                }

                string folderEditorName;
                nodeList.ForEach(childNode => childNode.title = childNode.customAttributes.TryGetValue("folderEditorName", out folderEditorName) ? folderEditorName : childNode.title);
            }

            return pageDictionary;
        }
        
        /// <summary>
        /// Sets a dynaNodes page name.  The names are not stored from in the DB, so we rely on the dynaTrees created from the 
        /// navigation config to provide the value.
        /// </summary>
        /// <param name="node">A CustomFavoriteNode who will have its own and its descendant's title populated. </param>
        /// <param name="pageDictionary">A dictionary of dynaNodes that represent the tree created from the navigation config.</param>
        private void SetPageNames(CustomFavoriteNode node, Dictionary<string, List<DynaTreeNode>> pageDictionary)
        {
            if (node is CustomFavoriteFolderNode)
            {
                var folder = node as CustomFavoriteFolderNode;
                foreach (var child in folder.Children)
                {
                    this.SetPageNames(child, pageDictionary);
                }

                folder.Children = folder.Children.Where(child =>
                child is CustomFavoriteFolderNode || 
                (child is CustomFavoritePageNode && !string.IsNullOrEmpty((child as CustomFavoritePageNode).LinkText))).ToList();
            }
            else
            {
                var page = node as CustomFavoritePageNode;
                List<DynaTreeNode> dynaNodeList;
                pageDictionary.TryGetValue(page.PageId, out dynaNodeList);
                var pageDynaNode = dynaNodeList?.FirstOrDefault();
                if (pageDynaNode != null)
                {
                    page.LinkText = pageDynaNode.title ?? string.Empty;
                }
            }
        }
    }
}