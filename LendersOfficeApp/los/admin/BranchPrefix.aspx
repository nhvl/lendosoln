<%@ Page language="c#" Codebehind="BranchPrefix.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.BranchPrefix" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD runat="server">
    <title>Branch Loan Naming Prefixes</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</HEAD>
<body MS_POSITIONING="FlowLayout" scroll="no">
    <script>
        function _init() {
            <% if ( !Page.IsPostBack ) { %>
                resize( 540 , 430 );
            <% } %>
        }

		function onCheckBoxClick(event) {try
		{
			var oSource = retrieveEventTarget(event);

			if (oSource.nextSibling.nextSibling.nextSibling.nextSibling)
			{
				// Set the readabliity of the textboxes
				if (oSource.checked == true)
				{
					oSource.nextSibling.nextSibling.readOnly = false;
					oSource.nextSibling.nextSibling.focus();
				}
				else
				{
					oSource.nextSibling.nextSibling.readOnly = true;
					oSource.nextSibling.nextSibling.value = "";
				}

				// Set the dirty field for this branch
				oSource.nextSibling.nextSibling.nextSibling.nextSibling.value = '1';
			}
		}
		catch (e)
		{
		}}

		function onPrefixChange(event) { try
		{
			// Dirty this branch
			var oSource = retrieveEventTarget(event);
			if (oSource.nextSibling.nextSibling)
				oSource.nextSibling.nextSibling.value = '1';
		}
		catch (e)
		{
		}}

		jQuery(function($){
			//because the apply/ok buttons are done in a post, when this runs it'll always have the most recent saved data.
			var branches = [];
			$('input.Override:checked').each(function(){
				var branchName = $(this).siblings('input.OverrideWith').val();
				branches.push(branchName);
			});

			var branchDTO = JSON.stringify(branches);

			if(window.opener && window.opener.BranchPrefixUpdate)
			{
				window.opener.BranchPrefixUpdate(branchDTO);
			}

			var args = getModalArgs();
			if(args && args.BranchPrefixUpdate)
			{
				args.BranchPrefixUpdate(branchDTO);
			}

			//this gets snuck in by the codebehind
			if($('#autoclose').length > 0)
			{
				onClosePopup();
			}
		});
	</script>
	<h4 class="page-header">Branch Prefixes</h4>
	<form id="BranchBrefix" method="post" runat="server">
		<table width="100%" height="370px" border="0" cellspacing="2" cellpadding="3">
		<tr valign=top>
			<td>
				<div> By default, each branch will use your corporate prefix. <br> Simply check a box below to override with a custom prefix. <br /> <hr />
					<div style="BORDER-RIGHT: 2px; PADDING-RIGHT: 4px; BORDER-LEFT: lightgrey 2px solid; PADDING-LEFT: 4px; BORDER-TOP: lightgrey 2px solid; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; PADDING-BOTTOM: 4px; OVERFLOW-Y: scroll; HEIGHT: 230px">
						<ml:CommonDataGrid id="m_branchDG" runat="server" OnItemDataBound="OnItemDataBound_BranchDG">
							<AlternatingItemStyle cssclass="GridAlternatingItem"></AlternatingItemStyle>
							<ItemStyle cssclass="GridItem"></ItemStyle>
							<HeaderStyle cssclass="GridHeader"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="Branch"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Override Corporate Prefix">
									<ItemTemplate>
										<input type="checkbox" class="Override" id="OverrideCB" runat="server" onClick="onCheckBoxClick(event);" />Override with:&nbsp;
										<input type="text" id="PrefixTB" class="OverrideWith" runat="server" MaxLength="10" onchange="onPrefixChange(event);" />
										<input type="hidden" runat="server" id="PrefixDirty">
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn Visible="false" DataField="BranchID"></asp:BoundColumn>
							</Columns>
						</ml:CommonDataGrid>
					</div>
				</div>
			</td>
		</tr>
		<tr valign=top>
			<td align="center" colSpan="1">
			<hr />
			<asp:Button runat="server" Text="  OK  " OnClick="OnOkClick"></asp:Button>
			<input type="button" onclick="onClosePopup();" value="Cancel">
			<asp:Button Runat="server" Text="Apply" OnClick="OnApplyClick"></asp:Button>
			</td>
		</tr>
	</table>
    </form>
  </body>
</HTML>
