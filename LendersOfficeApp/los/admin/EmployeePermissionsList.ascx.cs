namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using LendersOffice.Security;
    using LendersOffice.Events;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using DataAccess;
    using LendersOffice.Constants;
    using System.Threading;
    using System.Collections.Generic;
    using System.Web.UI;

    /// <summary>
    ///		Summary description for EmployeePermissions.
    /// </summary>
    public partial  class EmployeePermissionsList : System.Web.UI.UserControl
	{
		#region Variables
		protected System.Web.UI.HtmlControls.HtmlInputHidden        m_LastIndexSelected;
		protected System.Web.UI.HtmlControls.HtmlInputHidden        m_SelectedIndex;
		protected string											m_allowReadContacts = "";
		protected string											m_allowAccessCR = "";
		protected string											m_canSubmitWoCreditReport = "";
		protected string											m_canRunWoCreditReport = "";
		protected string											m_allowViewPriceGroups = "";
		protected Guid												m_BrokerId = Guid.Empty;
		protected Guid												m_DataId = Guid.Empty;
		public string												m_commandToDo = "";
		public string												m_Type = "";
		public bool													m_Disabled = false;
		public bool													m_BatchEdit = false;
		private BrokerDB											m_CurrentBrokerDb; 
		BrokerFeatures												m_bFs = new BrokerFeatures();
        protected bool m_bIsUnderwriterRole = false;
        private Dictionary<BrokerUserPermissionTable.ControlDescription, Tuple<CheckBoxList, Repeater>> controls; 

		#endregion 

		#region AccessMethods
        private bool IsFromLoAdmin
        {
            get
            {
                return System.Threading.Thread.CurrentPrincipal is InternalUserPrincipal; 
            }
        }
        private BrokerDB CurrentBrokerDb 
		{
			get 
			{ 
				if ( this.m_CurrentBrokerDb == null ) 
				{
					m_CurrentBrokerDb = BrokerDB.RetrieveById(m_BrokerId);
				}
				return m_CurrentBrokerDb;
			}
		}

		protected bool ShowAllowBypassAlwaysUseBestPrice
        {
            get
            {
                return CurrentBrokerDb.IsBestPriceEnabled && CurrentBrokerDb.IsAlwaysUseBestPrice;
            }
        }

        protected bool ShowDataTrac
		{
			get 
			{
				return CurrentBrokerDb.IsDataTracIntegrationEnabled; 
			}
		}

        protected bool ShowEDocs
        {
            get
            {
                return CurrentBrokerDb.IsEDocsEnabled;
            }
        }

        protected bool ShowTotalScorecard
        {
            get
            {
                return CurrentBrokerDb.IsTotalScorecardEnabled;
            }
        }


		public Guid DataSource
		{
			// cannot save the DataSource in the viewstate because in the case of EditDefaultRolePermissions, 
			// the DataSource needs to change at each post back
			set
			{
				if(m_Type.Equals("role"))
					m_DataId = value;
				else
					ViewState.Add( "DataId" , m_DataId = value );

                m_bIsUnderwriterRole = (value == CEmployeeFields.s_UnderwriterRoleId);
			}
			
			get { return m_DataId; }
		}

		public Guid DataBroker
		{
			set { ViewState.Add( "BrokerId" , m_BrokerId = value ); }
			get { return m_BrokerId; }
		}

		public bool Disabled
		{
			set { ViewState.Add( "Disabled" , m_Disabled = value ); }
			get { return m_Disabled; }
		}

		public bool BatchEdit
		{
			set { ViewState.Add( "BatchEdit" , m_BatchEdit = value ); }
			get { return m_BatchEdit; }
		}

		public string PermissionType
		{
			set { ViewState.Add( "PermissionType" , m_Type = value ); }
			get { return m_Type; }
		}
		#endregion

		protected void PageLoad(object sender, System.EventArgs e)
		{
            this.controls = new Dictionary<BrokerUserPermissionTable.ControlDescription, Tuple<CheckBoxList, Repeater>>()
            {
                { BrokerUserPermissionTable.ControlDescription.Contacts, Tuple.Create(m_EmployeePermissionsContacts, m_BatchPermissionsContacts) },
                { BrokerUserPermissionTable.ControlDescription.CustomReports, Tuple.Create(m_EmployeePermissionsCustomReports, m_BatchPermissionsCustomReports)},
                { BrokerUserPermissionTable.ControlDescription.CustomForms, Tuple.Create(m_EmployeePermissionsCustomForms,m_BatchPermissionsCustomForms)},
                { BrokerUserPermissionTable.ControlDescription.ExternalUsers, Tuple.Create(m_EmployeePermissionsExternalUsers, m_BatchPermissionsExternalUsers)},
                { BrokerUserPermissionTable.ControlDescription.Loans, Tuple.Create(m_EmployeePermissionsLoans, m_BatchPermissionsLoans)},
                { BrokerUserPermissionTable.ControlDescription.PricingEngine, Tuple.Create(m_EmployeePermissionsPricingEngine, m_BatchPermissionsPricingEngine)},
                { BrokerUserPermissionTable.ControlDescription.PrintGroups, Tuple.Create(m_EmployeePermissionsPrintGroups, m_BatchPermissionsPrintGroups)},
                { BrokerUserPermissionTable.ControlDescription.Templates, Tuple.Create(m_EmployeePermissionsTemplates, m_BatchPermissionsTemplates)},
                { BrokerUserPermissionTable.ControlDescription.CCTemplates, Tuple.Create(m_EmployeePermissionsCCTemplates, m_BatchPermissionsCCTemplates)},
                { BrokerUserPermissionTable.ControlDescription.PriceGroups, Tuple.Create(m_EmployeePermissionsPriceGroups, m_BatchPermissionsPriceGroups)},
                { BrokerUserPermissionTable.ControlDescription.EDocs, Tuple.Create(m_EmployeePermissionsEDocs,  m_BatchPermissionsEDocs)},
                { BrokerUserPermissionTable.ControlDescription.MortgageBanking, Tuple.Create(m_EmployeePermissionsMortgageBanking, m_BatchPermissionsMortgageBanking)},
                { BrokerUserPermissionTable.ControlDescription.TotalScorecard,  Tuple.Create(m_EmployeePermissionsTotalScorecard,  m_BatchPermissionsTotalScorecard)},
                { BrokerUserPermissionTable.ControlDescription.CapitalMarkets, Tuple.Create(m_EmployeePermissionsCapitalMarkets, m_BatchPermissionsCapitalMarkets)},
                { BrokerUserPermissionTable.ControlDescription.Disclosures, Tuple.Create(m_EmployeePermissionsDisclosures  , m_BatchPermissionsDisclosures)},
                { BrokerUserPermissionTable.ControlDescription.Administration, Tuple.Create(m_EmployeePermissionsAdministration, m_BatchPermissionsAdministration)},
                { BrokerUserPermissionTable.ControlDescription.TestLoans, Tuple.Create(m_EmployeePermissionsTestLoans, m_BatchPermissionsTestLoans)},
                { BrokerUserPermissionTable.ControlDescription.UserExperience, Tuple.Create(m_EmployeePermissionsUserExperience, m_BatchPermissionsUserExperience) }, 
                { BrokerUserPermissionTable.ControlDescription.DocumentReview, Tuple.Create(m_EmployeePermissionsDocumentReview, m_BatchPermissionsDocumentReview) },
                { BrokerUserPermissionTable.ControlDescription.Leads, Tuple.Create(m_EmployeePermissionsLeads, m_BatchPermissionsLeads)}
            };

            CapitalMarketCheckListContainer.Visible = IsFromLoAdmin;
            CapitalMarketsContainer.Visible = IsFromLoAdmin; 
			m_bFs.Retrieve(DataBroker);
			string initScript = String.Format(@"
				<script language='javascript'>
				<!--
					onInitEmployeePermissionsList();
				// -->
				</script>
			", this.ClientID);

			Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "_Init", initScript);

			if(!IsPostBack)
			{
				BindControls();
                if (!BatchEdit)
                {
                    PermissionsDataBind();
                }
			}

			//if this is an employee who has a status of "Current Employee" but has not been given a login yet,
			//they will not have permissions loaded in the db and so we must subsequently load the default permissions 
			//Or if we are changing roles in the default role permissions screen, we need to bind the permissions on postback
            if ((m_Type.Equals("employee") && (m_DefaultPermissions.Value.TrimWhitespaceAndBOM() == "")) || m_commandToDo.Equals("ChangeRole"))
            {
                PermissionsDataBind();
            }

			RegisterPermissionIdsArray();

            if (Disabled)
            {
                m_PermissionsPanel.BackColor = Color.Gainsboro;
            }
		}

		private ArrayList BuildPermissionsIdString(BrokerUserPermissionTable permissionsTable, string clientID)
		{
			ArrayList toReturn = new ArrayList();
			System.Text.StringBuilder permissionTitleList = new System.Text.StringBuilder();
			System.Text.StringBuilder permissionIdList = new System.Text.StringBuilder();
			
			int count = 0;
			string permissionId = "";
			string permissionTitle = "";

			foreach( BrokerUserPermissionTable.Desc dItem in permissionsTable)
			{
				permissionTitle = dItem.Code.ToString();
				permissionTitleList.Append(",'" + permissionTitle + "'");
                if (BatchEdit)
                {
                    permissionId = clientID + "_ctl" + count++.ToString("D2") + "_";
                }
                else
                {
                    permissionId = clientID + "_" + count++;
                }
                permissionIdList.Append(",'" + permissionId + "'");
			}	

			toReturn.Insert(0, permissionTitleList);
			toReturn.Insert(1, permissionIdList);
			return toReturn;
		}

        private Tuple<CheckBoxList, Repeater> GetControls(BrokerUserPermissionTable.ControlDescription cDesc)
        {
            Tuple<CheckBoxList, Repeater> control;

            if (!this.controls.TryGetValue(cDesc, out control))
            {
                throw new UnhandledEnumException(cDesc);
            }

            return control;
        }


        private string GetControlId(BrokerUserPermissionTable.ControlDescription cDesc)
		{
            var controls = this.GetControls(cDesc);

            if (this.BatchEdit)
            {
                return controls.Item2.ClientID;
            }
            else
            {
                return controls.Item1.ClientID;
            }
		}

		private Object GetListItems(BrokerUserPermissionTable.ControlDescription cDesc)
		{
            var controls = this.GetControls(cDesc); 

            if (BatchEdit)
            {
                return controls.Item2.Items;
            }
            else
            {
                return controls.Item1.Items;
            }
		}

		private void RegisterPermissionIdsArray()
		{
			System.Text.StringBuilder permissionTitleList = new System.Text.StringBuilder();
			System.Text.StringBuilder permissionIdList = new System.Text.StringBuilder();
			int numElementsInEnum = Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription)).Length;
			ArrayList values;
			BrokerUserPermissionTable.ControlDescription cd;
			for(int i = 0; i < numElementsInEnum; ++i)
			{
				cd = (BrokerUserPermissionTable.ControlDescription)Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription)).GetValue(i);
                values = BuildPermissionsIdString(BrokerUserPermissionTable.GetPermissionTable(cd, m_bFs.HasFeature(E_BrokerFeatureT.PricingEngine), ShowDataTrac, ShowEDocs, ShowTotalScorecard, CurrentBrokerDb.IsOCREnabled, m_bFs.HasFeature(E_BrokerFeatureT.MarketingTools)), GetControlId(cd));
                permissionTitleList.Append(values[0]);
				permissionIdList.Append(values[1]);
			}

			Page.ClientScript.RegisterArrayDeclaration( "PermissionTitleList", permissionTitleList.ToString().TrimStart(','));
			Page.ClientScript.RegisterArrayDeclaration( "PermissionIdList", permissionIdList.ToString().TrimStart(',') );
		}

		public void PermissionsDataBind()
		{		
			if(BatchEdit)
				return;
			BrokerUserPermissions bUp = new BrokerUserPermissions();

            if (!DataSource.Equals(Guid.Empty))
            {
                if (m_Type.Equals("role"))
                {
                    bUp.RetrieveRolePermissions(DataBroker, DataSource);
                }
                else
                {
                    bUp = new BrokerUserPermissions(DataBroker, DataSource);

                }
            }
            else
            {
                bUp.Retrieve(DataBroker);
            }


			//build the permissions string from the checkbox lists
			int numElementsInEnum = Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription)).Length;
			System.Text.StringBuilder permissions = new System.Text.StringBuilder();
			BrokerUserPermissionTable.ControlDescription cd;

			for(int i = 0; i < numElementsInEnum; ++i)
			{
				cd = (BrokerUserPermissionTable.ControlDescription)Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription)).GetValue(i);
				permissions.Append(BuildPermissionsString((ListItemCollection)GetListItems(cd), bUp));
			}

			m_DefaultPermissions.Value = permissions.ToString().TrimStart(',');

            chkEDocs_OnItemDataBound(m_EmployeePermissionsEDocs, null);
		}
		
		private bool SaveBatchPermissionGroup(BrokerUserPermissions bUp, System.Web.UI.WebControls.RepeaterItemCollection items)
		{
			bool isChanged = false;
			
			foreach( RepeaterItem pItem in items )
			{
				RadioButton keepCurrent = pItem.FindControl( "KeepCurrentPermission" ) as RadioButton;
				if(keepCurrent.Checked)
					continue;
				RadioButton rb = pItem.FindControl( "On" ) as RadioButton;
				if( rb != null )
				{
					if( bUp.HasPermission( int.Parse( rb.Attributes[ "code" ] ) ) != rb.Checked )
					{
						bUp.SetPermission( int.Parse( rb.Attributes[ "code" ] ) , rb.Checked );
						isChanged = true;
					}
				}
			}
			return isChanged;
		}

		public bool SaveBatchPermissions(BrokerUserPermissions bUp)
		{
			bool isChanged = false;
			
			int numElementsInEnum = Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription)).Length;
			BrokerUserPermissionTable.ControlDescription cd;

			for(int i = 0; i < numElementsInEnum; ++i)
			{
				cd = (BrokerUserPermissionTable.ControlDescription)Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription)).GetValue(i);
				isChanged = SaveBatchPermissionGroup(bUp, (RepeaterItemCollection)GetListItems(cd)) | isChanged;
			}
			
			return isChanged;
		}

        public void BindControls()
        {

            DocumentReviewSection.Visible = CurrentBrokerDb.IsOCREnabled;
            m_BatchPermissionsDocumentReview.Visible = CurrentBrokerDb.IsOCREnabled;

            if (IsPostBack)
                return;
            bool pmlEnabled = m_bFs.HasFeature(E_BrokerFeatureT.PricingEngine);
            bool marketingToolsEnabled = m_bFs.HasFeature(E_BrokerFeatureT.MarketingTools);
            this.LeadPlaceHolder.Visible = marketingToolsEnabled;
            this.BatchLeadPlaceHolder.Visible = marketingToolsEnabled;

            //bind the permissions
            if (!BatchEdit)
            {
                foreach (var control in this.controls)
                {
                    control.Value.Item1.DataTextField = "Label";
                    control.Value.Item1.DataValueField = "Value";
                    control.Value.Item1.DataSource = BrokerUserPermissionTable.GetPermissionTable(control.Key, pmlEnabled, ShowDataTrac, ShowEDocs, ShowTotalScorecard, CurrentBrokerDb.IsOCREnabled, marketingToolsEnabled);
                    control.Value.Item1.DataBind();
                }

                m_ExternalUsersLabel.Visible = pmlEnabled;
                m_pricingEngineLabel.Visible = pmlEnabled;
            }
            else
            {
                foreach (var control in this.controls)
                {
                    control.Value.Item2.DataSource = BrokerUserPermissionTable.GetPermissionTable(control.Key, pmlEnabled, ShowDataTrac, ShowEDocs, ShowTotalScorecard, CurrentBrokerDb.IsOCREnabled, marketingToolsEnabled);
                    control.Value.Item2.DataBind();
                }

                m_BatchExternalUsersLabel.Visible = pmlEnabled;
                m_BatchPricingEngineLabel.Visible = pmlEnabled;
            }
        }

        protected void ControlLoad( object sender , System.EventArgs a )
		{	
			if( ViewState[ "BrokerId" ] != null )
				m_BrokerId = ( Guid ) ViewState[ "BrokerId" ];

			if( ViewState[ "Disabled" ] != null )
				m_Disabled = ( bool ) ViewState[ "Disabled" ];

			if( ViewState[ "BatchEdit" ] != null )
				m_BatchEdit = ( bool ) ViewState[ "BatchEdit" ];

			if( ViewState[ "DataId" ] != null )
				m_DataId = ( Guid ) ViewState[ "DataId" ];

			if( ViewState[ "PermissionType" ] != null )
				m_Type = ViewState[ "PermissionType" ].ToString();
		}

		/// <summary>
		/// Store permissions and set permission checkbox state
		/// </summary>
		private System.Text.StringBuilder BuildPermissionsString(System.Web.UI.WebControls.ListItemCollection items, BrokerUserPermissions bUp )
		{	
			
			System.Text.StringBuilder permissions = new System.Text.StringBuilder();
			foreach( ListItem lItem in items )
			{
				lItem.Selected = bUp.HasPermission( int.Parse( lItem.Value ) );
				permissions.Append( "," + lItem.Selected.ToString());
			}
			return permissions;
		}

		private void SetPermissions(System.Web.UI.WebControls.ListItemCollection items, BrokerUserPermissions bUp)
		{
			foreach( ListItem pItem in items )
			{
				bUp.SetPermission( int.Parse( pItem.Value ) , pItem.Selected );
			}
		}

		public void Apply(string type)
		{
            using (CStoredProcedureExec spEx = new CStoredProcedureExec(DataBroker))
            {
                spEx.BeginTransactionForWrite();
                try
                {
                    BrokerUserPermissions bUp;

                    if (m_Type.Equals("role"))
                        bUp = new BrokerUserPermissions();
                    else
                        bUp = new BrokerUserPermissions(DataBroker, DataSource);


                    int numElementsInEnum = Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription)).Length;
                    BrokerUserPermissionTable.ControlDescription cd;

                    for (int i = 0; i < numElementsInEnum; ++i)
                    {
                        cd = (BrokerUserPermissionTable.ControlDescription)Enum.GetValues(typeof(BrokerUserPermissionTable.ControlDescription)).GetValue(i);
                        SetPermissions((ListItemCollection)GetListItems(cd), bUp);
                    }

                    if (m_Type.Equals("role"))
                        bUp.UpdateRolePermissions(spEx, DataBroker, DataSource);
                    else
                        bUp.Update(spEx, DataBroker, DataSource);
                    spEx.CommitTransaction();
                }
                catch (GenericUserErrorMessageException)
                {
                    spEx.RollbackTransaction();
                    throw;
                }
                catch (Exception)
                {
                    spEx.RollbackTransaction();
                    throw;
                }
            }

			PermissionsDataBind();
		}

        protected void chkEDocs_OnItemDataBound(object sender, EventArgs e) 
        {
          CheckBoxList chkeDocs = (CheckBoxList)sender;
          foreach (ListItem listitem in chkeDocs.Items)         
          {
              listitem.Attributes.Add("onclick", "onEDocsClick('" + listitem.Value + "');");         
          }
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.ControlLoad);
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
