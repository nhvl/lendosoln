﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmployeeTeamPicker.aspx.cs" Inherits="LendersOfficeApp.Los.Admin.EmployeeTeamPicker" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
            #TeamsGrid
            {
                width:100%;
            }
            
            .tableContainer
            {
                height:200px;
                overflow:auto;
                margin-bottom:10px;
            }
            
    </style>
</head>
<body onload="init();">
    <h4 id="pageHeader" runat="server" class="page-header"></h4>
    <form id="form1" runat="server">
    <div>
        
        <br />
        <div >
            <asp:CheckBox runat="server" ID="CanBeMemberOfMultipleTeams" onclick="onMultipleTeamsClick();" Text="User may belong to multiple teams" />
            <br />
            <div class="tableContainer">
            <asp:GridView style="width:100%" ID="TeamsGrid" runat="server" AutoGenerateColumns="false">
                <HeaderStyle cssclass="GridHeader AlignLeft" />
                        <AlternatingRowStyle cssclass="GridAlternatingItem HoverHighlight" />
                        <RowStyle cssclass="GridItem HoverHighlight" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>Primary <br /> Team</HeaderTemplate>
                        <ItemTemplate>
                            <input type="radio" class="<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamId").ToString()) %>" data-checked="<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IsPrimary").ToString()) %>" name="Primary" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>Additional <br /> Teams</HeaderTemplate>
                        <ItemTemplate>
                            <input type="checkbox" class="<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamId").ToString()) %>" data-checked="<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IsAssigned").ToString()) %>" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>Team</HeaderTemplate>
                        <ItemTemplate>
                            <span class="TeamName">
                                <%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamName").ToString()) %>
                            </span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
            </asp:GridView>
            </div>
            <div style="text-align:center">
                <input style="padding:2px 5px;" type="button" value="OK" onclick="Save();" />
                 &nbsp <input style="padding:2px 5px;" type="button" value="Cancel" onclick="onCancel();" />
            </div>
        </div>
    </div>
    </form>
    
        <script type="text/javascript">
            function init() {
                $("#TeamsGrid input").each(function() {
                    if ($(this).attr("data-checked") == "True") {
                        this.checked = true;
                    }
                });
                ///load the data
                var data = getModalArgs();
                
                if(data != null)
                {
                    var i = 0;
                    $("#TeamsGrid tr").each(function() {
                        if (i > 0) {
                            var IsPrimary = $(this).find("[type='radio']");
                            var IsAssigned = $(this).find("[type='checkbox']");
                           IsPrimary.prop("checked", data["IsPrimary" + i] == "true");
                           IsAssigned.prop("checked", data["IsAssigned" + i] == "true");
                        }
                        i++;
                        });
                    }
                
                onMultipleTeamsClick();
            }
            
            function onMultipleTeamsClick()
            {
                $("#TeamsGrid input[type='checkbox']").each(function(){
                    var checkbox = $(this);
                    
                if($("#CanBeMemberOfMultipleTeams").prop("checked"))
                {
                    checkbox.removeAttr("disabled");
                }
                else
                {
                    checkbox.attr("disabled", "true");
                    checkbox.removeAttr("checked");
                }
                });
                
            }

            function Save() {
                var data = {};
                var i = 0;
                $("#TeamsGrid tr").each(function() {
                    if (i > 0) {
                        var IsPrimary = $(this).find("[type='radio']");
                        var IsAssigned = $(this).find("[type='checkbox']");
                        var TeamName = $(this).find("span.TeamName").text();
                        
                        data["IsPrimary" + i] = "" + IsPrimary.prop("checked");
                        data["IsAssigned" + i] = "" + IsAssigned.prop("checked");
                        data["TeamId" + i] = IsAssigned.attr("class");
                        data["TeamName" + i] = TeamName;
                    }
                    i++;
                });

                data["numUpdates"] = "" + (i - 1);
                data["EmployeeId"] = <%=AspxTools.JsString(EmployeeId) %>;
                data["RoleId"] = <%=AspxTools.JsString(RoleId) %>;
                data["CanBeMemberOfMultipleTeams"] = "" + $("#CanBeMemberOfMultipleTeams").prop("checked");
                
                ///gService.EditTeamsService.call("UpdateAssignments", data);
                window.returnValue = data;

                onClosePopup(data);
            }
        function onCancel() {
            onClosePopup();
        }
    </script>
</body>
</html>
