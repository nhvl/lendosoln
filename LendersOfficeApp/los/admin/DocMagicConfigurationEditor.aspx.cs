﻿namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentCapture;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    public partial class DocMagicConfigurationEditor : LendersOffice.Common.BasePage
    {
        private BrokerDB m_bd = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;
        private bool m_automaticDocTypes = false;
        private bool m_supportBarcodes = false;

        /// <summary>
        /// The branch group IDs enabled for OCR.
        /// </summary>
        private HashSet<Guid> ocrEnabledBranchGroupIds = null;

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        protected string ErrorMessage1
        {
            set
            {
                m_ErrorMessage1.Visible = true;
                m_ErrorMessage1.Text = value;
            }
        }

        protected bool AutomaticDocTypes
        {
            get { return m_automaticDocTypes; }
            set { m_automaticDocTypes = value; }
        }

        protected bool SupportBarcodes
        {
            get { return m_supportBarcodes; }
            set { m_supportBarcodes = value; }
        }

        protected bool SupportCapture
        {
            get
            {
                return this.m_bd.EnableKtaIntegration;
            }
        }

        protected bool IsEnablePTMDocMagicSeamlessInterface
        {
            get { return E_BrokerBillingVersion.PerTransaction == m_bd.BillingVersion && m_bd.IsEnablePTMDocMagicSeamlessInterface; }
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            RegisterJsScript("LQBPopup.js");
            var vendors = DocumentVendorFactory.CreateVendorsFor(PrincipalFactory.CurrentPrincipal.BrokerId);
            // AutomaticDocTypes is set to true if the vendor is using our document framework.
            m_automaticDocTypes = vendors.Any(v => v.Skin.AutomaticDocTypes);
            m_supportBarcodes = vendors.Any(v => v.Config.SupportsBarcodes);
            if (AutomaticDocTypes)
            {
                ClientScript.RegisterHiddenField("AllowAutoDocTypes", "true");
            }

            if (!IsEnablePTMDocMagicSeamlessInterface)
            {
                ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.GenericAccessDenied, "Not enabled for this broker"), false, Guid.Empty, Guid.Empty);
            }

            if (!IsPostBack)
            {
                if (this.m_bd.IsOCREnabled)
                {
                    this.OCRSection.Visible = true;
                    this.OCRVendorName.Text = m_bd.OCRVendorName;
                    this.ocrUsername.Text = m_bd.OCRUsername;

                    if (m_bd.OCRHasConfiguredAccount)
                    {
                        this.ocrPassword.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
                    }
                }

                m_AutoSaveGeneratedDocs.Checked = m_bd.AutoSaveDocMagicGeneratedDocs;

                if (this.SupportCapture && this.m_bd.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.DocumentCapture)
                {
                    this.DocumentCapture.Checked = true;

                    if (m_bd.BackupDocumentSavingOption == E_DocMagicDocumentSavingOption.Single)
                    {
                        m_rbSaveSingle.Checked = true;
                    }
                    else if (m_bd.BackupDocumentSavingOption == E_DocMagicDocumentSavingOption.Split)
                    {
                        m_rbSaveSplit.Checked = true;
                    }
                }
                else if (m_bd.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.Single)
                {
                    m_rbSaveSingle.Checked = true;
                }
                else if (m_bd.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.Split)
                {
                    m_rbSaveSplit.Checked = true;
                }
                else
                {
                    ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.Generic,
                        string.Format("E_DocMagicDocumentSavingOption type {0} was not one of the known options.", m_bd.DocMagicDocumentSavingOption)),
                        false, Guid.Empty, Guid.Empty);
                }

                if (this.SupportCapture)
                {
                    if (this.m_bd.DocumentFrameworkCaptureEnabledBranchGroupT == BranchGroupSelection.All)
                    {
                        this.CaptureBranchGroupsAll.Checked = true;
                    }
                    else if (this.m_bd.DocumentFrameworkCaptureEnabledBranchGroupT == BranchGroupSelection.OnlySelected)
                    {
                        this.CaptureBranchGroupsSelected.Checked = true;
                    }

                    var availableGroups = GroupDB.GetAllGroups(this.m_bd.BrokerID, GroupType.Branch);
                    this.ocrEnabledBranchGroupIds = OcrEnabledBranchGroupManager.RetrieveEnabledBranchGroups(this.m_bd.BrokerID);

                    this.CaptureBranchGroupRepeater.DataSource = availableGroups;
                    this.CaptureBranchGroupRepeater.DataBind();

                    this.CaptureSelectedBranchGroups.Value = SerializationHelper.JsonNetSerialize(this.ocrEnabledBranchGroupIds);
                }

                // The Single Document option is only shown if at least one alternative is available.
                this.SingleDocumentOptionSection.Visible = this.SupportBarcodes || this.m_bd.EnableKtaIntegration;
                this.SplitDocumentsOptionSection.Visible = this.SupportBarcodes;
                this.DocumentCaptureOptionSection.Visible = this.m_bd.EnableKtaIntegration;

                DocMagicSplitESignedDocs.Checked = m_bd.DocMagicSplitESignedDocs;
                DocMagicSplitUnsignedDocs.Checked = m_bd.DocMagicSplitUnsignedDocs;

                var docTypeList = EDocumentDocType.GetDocTypesByBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId, E_EnforceFolderPermissions.True).ToList();

                if (m_bd.ESignedDocumentDocTypeId == null)
                {
                    this.ESignSelectedDocType.Text = $"{EDocumentFolder.GENERATED_FOLDERNAME}:{EDocumentDocType.ESIGNED_UPLOAD_DOCTYPE}";
                }
                else
                {
                    this.ESignSelectedDocType.Text = docTypeList
                        .First(d => d.DocTypeId == m_bd.ESignedDocumentDocTypeId.ToString())
                        .FolderAndDocTypeName;
                }

                if ((m_bd.DocMagicDefaultDocTypeID == null || m_bd.DocMagicDefaultDocTypeID <= 0) && !AutomaticDocTypes)
                {
                    m_bd.DocMagicDefaultDocTypeID = Int32.Parse((from d in docTypeList
                                                                 where d.FolderAndDocTypeName == "DOCMAGIC : GENERATEDDOCUMENTS"
                                                                 select d).First().DocTypeId);
                }
                if (AutomaticDocTypes && (!m_bd.DocMagicDefaultDocTypeID.HasValue || m_bd.DocMagicDefaultDocTypeID <= 0))
                {
                    m_lSelectedDocType.Text = "GENERATED DOCUMENTS:<PACKAGETYPE>";
                }
                else
                {
                    m_lSelectedDocType.Text = (from d in docTypeList
                                               where d.DocTypeId == m_bd.DocMagicDefaultDocTypeID.ToString()
                                               select d).First().FolderAndDocTypeName;
                }

                m_AutoSaveLeCdReceivedDate.Checked = m_bd.AutoSaveLeCdReceivedDate;
                RequireAllBorrowersToReceiveCD.Checked = m_bd.RequireAllBorrowersToReceiveCD;
                m_AutoSaveLeCdSignedDate.Checked = m_bd.AutoSaveLeCdSignedDate;
                AutoSaveLeSignedDateWhenAnyBorrowerSigns.Checked = m_bd.AutoSaveLeSignedDateWhenAnyBorrowerSigns;

                NumVendors.Value = m_bd.AllActiveDocumentVendors.Count.ToString();
                if (m_bd.DisableAutoPopulationOfCdReceivedDate)
                {
                    m_AutoSaveLeCdReceivedDate.Text = "Automatically save LE received date";
                }

                DefaultIssuedDateToToday.Checked = m_bd.DefaultIssuedDateToToday;

                VendorDetails.DataSource = m_bd.AllActiveDocumentVendors;
                VendorDetails.DataBind();
            }
        }

        protected void Update_Click(object sender, EventArgs args)
        {
            var vendors = m_bd.AllActiveDocumentVendors;
            foreach(RepeaterItem item in VendorDetails.Items)
            {
                HiddenField VendorId = (HiddenField)item.FindControl("VendorId");
                TextBox m_Username = (TextBox)item.FindControl("m_Username");
                TextBox m_Password = (TextBox)item.FindControl("m_Password");
                TextBox m_CompanyId = (TextBox)item.FindControl("m_CompanyId");
                CheckBox m_AllowUserCustomLogin = (CheckBox)item.FindControl("m_AllowUserCustomLogin");
                CheckBox m_AllowInvestorDocMagicPlanCodes = (CheckBox)item.FindControl("m_AllowInvestorDocMagicPlanCodes");
                CheckBox m_IsOnlyAllowApprovedDocMagicPlanCodes = (CheckBox)item.FindControl("m_IsOnlyAllowApprovedDocMagicPlanCodes");
                CheckBox enableDsiPrintAndDeliverOption = (CheckBox)item.FindControl("EnableDsiPrintAndDeliverOption");
                CheckBox blockManualFulfillment = (CheckBox)item.FindControl("BlockManualFulfillment");
                CheckBox enableAppraisalDelivery = item.FindControl("EnableAppraisalDelivery") as CheckBox;
                DocumentVendorBrokerSettings vendor = vendors.First(v => v.VendorId == new Guid(VendorId.Value));
                vendor.Login = m_Username.Text;
                if (m_Password.Text != ConstAppDavid.FakePasswordDisplay)
                {
                    vendor.Password = m_Password.Text;
                }
                vendor.AccountId = m_CompanyId.Text;
                vendor.BlockManualFulfillment = blockManualFulfillment.Checked;

                if (vendor.VendorId == Guid.Empty) //DocMagic vendor
                {
                    m_bd.DocMagicIsAllowuserCustomLogin = m_AllowUserCustomLogin.Checked;
                    m_bd.AllowInvestorDocMagicPlanCodes = m_AllowInvestorDocMagicPlanCodes.Checked;
                    m_bd.IsOnlyAllowApprovedDocMagicPlanCodes = m_IsOnlyAllowApprovedDocMagicPlanCodes.Checked;
                    this.m_bd.EnableDsiPrintAndDeliverOption = enableDsiPrintAndDeliverOption.Checked;
                }

                if (enableAppraisalDelivery != null)
                {
                    vendor.EnableAppraisalDelivery = enableAppraisalDelivery.Checked;
                }
            }

            m_bd.AllActiveDocumentVendors = vendors; //Mark as updated so it actually gets saved
            m_bd.AutoSaveDocMagicGeneratedDocs = m_AutoSaveGeneratedDocs.Checked;

            if (m_AutoSaveGeneratedDocs.Checked)
            {
                bool singleOrSplitMustBeBackup = false;
                if (this.DocumentCapture.Checked && this.SupportCapture)
                {
                    this.m_bd.DocMagicDocumentSavingOption = E_DocMagicDocumentSavingOption.DocumentCapture;
                    singleOrSplitMustBeBackup = true;

                    if (this.CaptureBranchGroupsAll.Checked)
                    {
                        this.m_bd.DocumentFrameworkCaptureEnabledBranchGroupT = BranchGroupSelection.All;
                    }
                    else if (this.CaptureBranchGroupsSelected.Checked)
                    {
                        this.m_bd.DocumentFrameworkCaptureEnabledBranchGroupT = BranchGroupSelection.OnlySelected;

                        var selectedBranchGroups = SerializationHelper.JsonNetDeserialize<List<Guid>>(this.CaptureSelectedBranchGroups.Value);
                        OcrEnabledBranchGroupManager.SetEnabledBranchGroups(this.m_bd.BrokerID, selectedBranchGroups);
                    }
                }

                if (m_rbSaveSingle.Checked || (!this.SupportBarcodes && !this.SupportCapture))
                {
                    if (singleOrSplitMustBeBackup)
                    {
                        m_bd.BackupDocumentSavingOption = E_DocMagicDocumentSavingOption.Single;
                    }
                    else
                    {
                        m_bd.DocMagicDocumentSavingOption = E_DocMagicDocumentSavingOption.Single;
                    }
                }
                else if (m_rbSaveSplit.Checked)
                {
                    if (singleOrSplitMustBeBackup)
                    {
                        m_bd.BackupDocumentSavingOption = E_DocMagicDocumentSavingOption.Split;
                    }
                    else
                    {
                        m_bd.DocMagicDocumentSavingOption = E_DocMagicDocumentSavingOption.Split;
                    }

                    m_bd.DocMagicSplitESignedDocs = DocMagicSplitESignedDocs.Checked;
                    m_bd.DocMagicSplitUnsignedDocs = DocMagicSplitUnsignedDocs.Checked;
                }
                else
                {
                    ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.Generic,
                             "User chose a bad saving option"),
                             false, Guid.Empty, Guid.Empty);
                }

                if (!string.IsNullOrEmpty(m_selectedDocType.Value))
                {
                    m_bd.DocMagicDefaultDocTypeID = Int32.Parse(m_selectedDocType.Value);
                }
            }

            // If a DocType value of 0 is returned, then clear the id.  This will either set the default to 'DocMagic : GenerateDocuments' for the
            // old DocMagic framework, and 'GenerateDocuments : <PackageType>' for our framework.
            if (m_selectedDocType.Value.Equals("0"))
            {
                m_bd.DocMagicDefaultDocTypeID = null;
            }

            string esignDocType = m_selectedESignDocType.Value;
            if (!string.IsNullOrEmpty(esignDocType))
            {
                if (esignDocType == "0")
                {
                    m_bd.ESignedDocumentDocTypeId = null;
                }
                else
                {
                    m_bd.ESignedDocumentDocTypeId = int.Parse(m_selectedESignDocType.Value);
                }
            }

            m_bd.AutoSaveLeCdReceivedDate = m_AutoSaveLeCdReceivedDate.Checked;
            m_bd.RequireAllBorrowersToReceiveCD = RequireAllBorrowersToReceiveCD.Checked;
            m_bd.AutoSaveLeCdSignedDate = m_AutoSaveLeCdSignedDate.Checked;
            m_bd.AutoSaveLeSignedDateWhenAnyBorrowerSigns = AutoSaveLeSignedDateWhenAnyBorrowerSigns.Checked;

            if (this.OCRSection.Visible && this.ocrPassword.Text != ConstAppDavid.FakePasswordDisplay)
            {
                m_bd.OCRUsername = this.ocrUsername.Text;
                m_bd.OCRPassword = this.ocrPassword.Text;
            }

            m_bd.DefaultIssuedDateToToday = DefaultIssuedDateToToday.Checked;

            m_bd.Save();
            
            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new string[] {"OK=true"});
        }

        protected void AccountDetailsBind(object sender, RepeaterItemEventArgs e)
        {
            DocumentVendorBrokerSettings settings = (DocumentVendorBrokerSettings)e.Item.DataItem;
            IDocumentVendor vendor = DocumentVendorFactory.Create(PrincipalFactory.CurrentPrincipal.BrokerId, settings.VendorId);

            Label VendorName = (Label)e.Item.FindControl("VendorName");
            HiddenField VendorId = (HiddenField)e.Item.FindControl("VendorId");
            TextBox m_Username = (TextBox)e.Item.FindControl("m_Username");
            TextBox m_Password = (TextBox)e.Item.FindControl("m_Password");
            TextBox m_CompanyId = (TextBox)e.Item.FindControl("m_CompanyId");
            CheckBox blockManualFulfillment = (CheckBox)e.Item.FindControl("BlockManualFulfillment");

            VendorName.Text = vendor.Config.VendorName;
            VendorId.Value = settings.VendorId.ToString();
            m_Username.Text = settings.Login;
            m_Password.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
            m_CompanyId.Text = settings.AccountId;
            blockManualFulfillment.Checked = settings.BlockManualFulfillment;

            e.Item.FindControl("UsernamePanel").Visible = vendor.Config.UsesUsername;
            e.Item.FindControl("PasswordPanel").Visible = vendor.Config.UsesPassword;
            e.Item.FindControl("CompanyIdPanel").Visible = vendor.Config.UsesAccountId;

            bool isLegacyDocMagic = vendor is DocMagicDocumentVendor;
            e.Item.FindControl("DocMagicExtraSettingsPanel").Visible = isLegacyDocMagic;
            if (isLegacyDocMagic)
            {
                CheckBox m_AllowUserCustomLogin = (CheckBox)e.Item.FindControl("m_AllowUserCustomLogin");
                CheckBox m_AllowInvestorDocMagicPlanCodes = (CheckBox)e.Item.FindControl("m_AllowInvestorDocMagicPlanCodes");
                CheckBox m_IsOnlyAllowApprovedDocMagicPlanCodes = (CheckBox)e.Item.FindControl("m_IsOnlyAllowApprovedDocMagicPlanCodes");
                CheckBox enableDsiPrintAndDeliverOption = (CheckBox)e.Item.FindControl("EnableDsiPrintAndDeliverOption");
                
                m_AllowUserCustomLogin.Checked = m_bd.DocMagicIsAllowuserCustomLogin;
                m_AllowInvestorDocMagicPlanCodes.Checked = m_bd.AllowInvestorDocMagicPlanCodes;
                m_IsOnlyAllowApprovedDocMagicPlanCodes.Checked = m_bd.IsOnlyAllowApprovedDocMagicPlanCodes;
                enableDsiPrintAndDeliverOption.Checked = this.m_bd.EnableDsiPrintAndDeliverOption;
            }

            if (vendor.Config.PlatformType == E_DocumentVendor.DocMagic && !isLegacyDocMagic)
            {
                CheckBox enableAppraisalDelivery = (CheckBox)e.Item.FindControl("EnableAppraisalDelivery");
                enableAppraisalDelivery.Checked = settings.EnableAppraisalDelivery;
            }
            else
            {
                PlaceHolder enableAppraisalDeliveryPanel = (PlaceHolder)e.Item.FindControl("EnableAppraisalDeliveryPanel");
                enableAppraisalDeliveryPanel.Visible = false;
            }
        }

        protected void CaptureBranchGroupRepeater_ItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                var group = (Group)args.Item.DataItem;

                var branchGroupSelected = (CheckBox)args.Item.FindControl("CaptureBranchGroupSelected");
                branchGroupSelected.Checked = this.ocrEnabledBranchGroupIds.Contains(group.Id);

                var branchGroupId = (HiddenField)args.Item.FindControl("CaptureBranchGroupId");
                branchGroupId.Value = group.Id.ToString();

                var branchGroupName = (EncodedLiteral)args.Item.FindControl("CaptureBranchGroupName");
                branchGroupName.Text = group.Name;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}
