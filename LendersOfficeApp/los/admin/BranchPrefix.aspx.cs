using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{
	public partial class BranchPrefix : LendersOffice.Common.BasePage
	{

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private void BindDataGrid()
		{
			DataSet ds = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerUser.BrokerId)
                                        };
			DataSetHelper.Fill(ds, BrokerUser.BrokerId, "ListBranchByBrokerID", parameters);

			m_branchDG.DataSource = ds.Tables[0].DefaultView;
			m_branchDG.DefaultSortExpression = "Name ASC";
			m_branchDG.DataBind();
		}
        
		protected void PageLoad( object sender , System.EventArgs a )
		{
			BindDataGrid();
            RegisterJsScript("json.js");
		}

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// On postback re-bind in PreRender so UI reflects 
			// the data changes from events
			if (Page.IsPostBack)	
				BindDataGrid();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            EnableJqueryMigrate = false;

            InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

		protected void OnItemDataBound_BranchDG(Object sender, DataGridItemEventArgs e)
		{
			// Setup the HTML controls based on status of branch prefix. If there is
			// a saved prefix for this branch, tick override checkbox and display
			// prefix in text box.  If not, untick and make box read-only.

			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				HtmlInputText branchPrefixTB = e.Item.FindControl("PrefixTB") as HtmlInputText;
				HtmlInputCheckBox overrideCB = e.Item.FindControl("OverrideCB") as HtmlInputCheckBox;
				HtmlInputHidden dirtyInput = e.Item.FindControl("PrefixDirty") as System.Web.UI.HtmlControls.HtmlInputHidden;
			
				if (branchPrefixTB != null && overrideCB != null && dirtyInput != null)
				{
					branchPrefixTB.Value = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BranchLNmPrefix")).TrimWhitespaceAndBOM();
					if (branchPrefixTB.Value == string.Empty)
					{
						overrideCB.Checked = false;
						branchPrefixTB.Attributes.Add("ReadOnly", "true");
					}
					else
						overrideCB.Checked = true;
					
					dirtyInput.Value = "0";
				}
			}
		}

		protected void OnOkClick(Object sender, EventArgs e)
		{
			SaveData();
            ClientScript.RegisterHiddenField("autoclose", "true");
		}
		
		protected void OnApplyClick(Object sender, EventArgs e)
		{
			SaveData();
		}

		private void SaveData()
		{
			// Determine which prefixes are updated and save to db.
			// We only write dirty entries that have a override checked
			// or are blank (ie. user unchecked and cleared a  prefix)
			
			foreach(DataGridItem branchRow in m_branchDG.Items)
			{
				HtmlInputHidden dirtyInput = branchRow.FindControl("PrefixDirty") as HtmlInputHidden;
				HtmlInputText branchPrefixTB = branchRow.FindControl("PrefixTB") as HtmlInputText;
				HtmlInputCheckBox overrideCB = branchRow.FindControl("OverrideCB") as HtmlInputCheckBox;
				
				if (dirtyInput != null && branchPrefixTB != null && overrideCB != null 
					&& dirtyInput.Value == "1" && (overrideCB.Checked == true || branchPrefixTB.Value == string.Empty) )
				{
					SqlParameter[] parameters = new SqlParameter[] {
																	   new SqlParameter("@BrokerID", BrokerUser.BrokerId),
																	   new SqlParameter("@BranchID", new Guid(branchRow.Cells[2].Text)),
																	   new SqlParameter("@NamingPrefix", branchPrefixTB.Value)
																   };
					try 
					{
						StoredProcedureHelper.ExecuteNonQuery(BrokerUser.BrokerId, "UpdateBranchNamingPrefix", 0, parameters);
					} 
					catch (Exception exc)
					{
						Tools.LogError("Failed to update branch prefix settings.", exc);
					}
				}
			}
		}

	}
}