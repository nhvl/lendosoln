﻿namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// This class serves as the container for the Custom Folder Navigation control.  This page is only intended for Admins to use.
    /// </summary>
    public partial class CustomFolderNavigation : BaseServicePage
    {
        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }
        
        /// <summary>
        /// Gets the list of permissions required for the page to load.
        /// </summary>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingCustomNavigation
                };
            }
        }

        /// <summary>
        /// Gets the list of roles required for the page to load.
        /// </summary>
        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}
