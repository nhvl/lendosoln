﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VoaOptionConfigurationEdit.aspx.cs" Inherits="LendersOfficeApp.los.admin.VoaOptionConfigurationEdit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Integration.VOXFramework" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit VOA Options</title>
    <style type="text/css">
        #VoaOptionsTable {
            width: 100%;
        }

        .SpacerRow {
            margin: 10px;
        }

        .Center {
            text-align: center;
        }

        .VerticalPadding {
            padding-bottom: 10px;
            padding-bottom: 10px;
        }

        .Width150Px {
            width: 150px;
        }

        .Right {
            text-align: right;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            $('.EnableForLqbUsers').each(function () {
                onOptionToggled($(this), '.DefaultForLqbUsers')
            });

            $('.EnableForPmlUsers').each(function () {
                onOptionToggled($(this), '.DefaultForPmlUsers')
            });

            // Set the selected defaults provided by the backend.
            $('#' + ML.AccountHistoryDefaultForLqbUsersSelectedId).prop('checked', true);
            $('#' + ML.AccountHistoryDefaultForPmlUsersSelectedId).prop('checked', true);
            $('#' + ML.RefreshPeriodDefaultForLqbUsersSelectedId).prop('checked', true);
            $('#' + ML.RefreshPeriodDefaultForPmlUsersSelectedId).prop('checked', true);

            disableControlsForInvalidUserTypes();

            function disableControlsForInvalidUserTypes() {
                if (!ML.ServiceEnabledForLqbUsers) {
                    $('.LqbControl').prop('checked', false);
                    $('.LqbControl').prop('disabled', true);
                }

                if (!ML.ServiceEnabledForPmlUsers) {
                    $('.PmlControl').prop('checked', false);
                    $('.PmlControl').prop('disabled', true);
                }
            }

            $('#OkBtn').on('click', function () {
                if (!validateForm()) {
                    return;
                }

                var optionSet = [];

                var accountHistoryType = <%= AspxTools.JsNumeric(VOAOptionType.AccountHistory) %>;
                $('.AccountHistoryOptionRow').each(function () {
                    serializeOptionRow($(this), accountHistoryType, optionSet);
                });

                var refreshPeriodType = <%= AspxTools.JsNumeric(VOAOptionType.RefreshPeriod) %>;
                $('.RefreshPeriodOptionRow').each(function () {
                    serializeOptionRow($(this), refreshPeriodType, optionSet);
                });

                var returnArgs = {
                    VendorId: <%= AspxTools.JsString(this.Vendor.VendorId) %>,
                    OptionSet: JSON.stringify(optionSet)
                };
                parent.LQBPopup.Return(returnArgs);
            });

            function serializeOptionRow($tr, optionType, returnArgs) {
                if ($tr.find('.EnableForLqbUsers').is(':checked') && ML.ServiceEnabledForLqbUsers) {
                    var optionValue = $tr.find('.OptionValue').val();
                    var userType = "B";
                    var isDefault = $tr.find('.DefaultForLqbUsers').is(':checked');

                    var option = serializeOption(optionType, optionValue, userType, isDefault);
                    returnArgs.push(option);
                }

                if ($tr.find('.EnableForPmlUsers').is(':checked') && ML.ServiceEnabledForPmlUsers) {
                    var optionValue = $tr.find('.OptionValue').val();
                    var userType = "P";
                    var isDefault = $tr.find('.DefaultForPmlUsers').is(':checked');

                    var option = serializeOption(optionType, optionValue, userType, isDefault);
                    returnArgs.push(option);
                }
            }

            function serializeOption(optionType, optionValue, userType, isDefault) {
                var option = {};
                option.OptionType = optionType;
                option.OptionValue = optionValue;
                option.userType = userType;
                option.isDefault = isDefault;

                return option;
            }

            function validateForm() {
                var valid = true;
                if (ML.ServiceEnabledForLqbUsers) {
                    if (!validateDefaultButtonGroup('AccountHistoryDefaultForLqbUsers')
                        || !validateDefaultButtonGroup('RefreshPeriodDefaultForLqbUsers')) {
                        valid = false;
                    }
                }

                if (ML.ServiceEnabledForPmlUsers) {
                    if (!validateDefaultButtonGroup('AccountHistoryDefaultForPmlUsers')
                     || !validateDefaultButtonGroup('RefreshPeriodDefaultForPmlUsers')) {
                        valid = false;
                    }
                }

                if (!valid) {
                    alert("At least one option and one default must be selected per category for enabled user types.");
                }

                return valid;
            }

            function validateDefaultButtonGroup(groupName) {
                var defaultSelected = $('input[name=' + groupName + ']:checked').val() !== undefined;
                return defaultSelected;
            }

            $('#CancelBtn').on('click', function () {
                var returnArgs = {
                    VendorId: <%= AspxTools.JsString(this.Vendor.VendorId) %>,
                    OptionSet: <%= AspxTools.JsString(this.VoaOptionJsonOverride) %>
                };

                parent.LQBPopup.Return(returnArgs);
            });

            $('.EnableForLqbUsers').on('click', function () {
                onOptionToggled($(this), '.DefaultForLqbUsers');
            });

            $('.EnableForPmlUsers').on('click', function () {
                onOptionToggled($(this), '.DefaultForPmlUsers');
            });

            function onOptionToggled($toggledCheckbox, defaultRbSelector) {
                var $defaultRb = $toggledCheckbox.closest('.AccountHistoryOptionRow, .RefreshPeriodOptionRow').find(defaultRbSelector);
                var optionEnabled = $toggledCheckbox.is(':checked');
                
                $defaultRb.prop('disabled', !optionEnabled);
                $defaultRb.prop('checked', false);
            }
        });
    </script>
    <form id="form1" runat="server">
    <div class="MainRightHeader">Edit VOA Options</div>
    <div>
        <table id="VoaOptionsTable">
            <tr>
                <td class="Width150Px"></td>
                <td class="FieldLabel Center" colspan="2">LQB users</td>
                <td class="FieldLabel Center" colspan="2">TPO users</td>
            </tr>
            <tr>
                <td class="FieldLabel Right">Account History Options</td>
                <td class="Center">Enabled</td>
                <td class="Center">Default</td>
                <td class="Center">Enabled</td>
                <td class="Center">Default</td>
            </tr>
            <tbody id="AccountHistoryOptions">
                <asp:Repeater runat="server" ID="AccountHistoryOptionRepeater" OnItemDataBound="AccountHistoryOptionRepeater_OnItemDataBound">
                <ItemTemplate>
                    <tr class="AccountHistoryOptionRow">
                        <td class="Right">
                            <ml:EncodedLiteral runat="server" ID="OptionName" />
                            <input type="hidden" runat="server" ID="OptionValue" class="OptionValue" />
                        </td>
                        <td class="Center">
                            <input type="checkbox" runat="server" ID="EnableForLqbUsers" class="EnableForLqbUsers LqbControl" />
                        </td>
                        <td class="Center">
                            <ml:PassthroughLiteral runat="server" ID="DefaultForLqbUsersLiteral" />
                        </td>
                        <td class="Center">
                            <input type="checkbox" runat="server" ID="EnableForPmlUsers" class="EnableForPmlUsers PmlControl" />
                        </td>
                        <td class="Center">
                            <ml:PassthroughLiteral runat="server" ID="DefaultForPmlUsersLiteral" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            </tbody>
            <tr class="SpacerRow">
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="Width150Px"></td>
                <td class="FieldLabel Center" colspan="2">LQB users</td>
                <td class="FieldLabel Center" colspan="2">TPO users</td>
            </tr>
            <tr>
                <td class="FieldLabel Right">Refresh Period Options</td>
                <td class="Center">Enabled</td>
                <td class="Center">Default</td>
                <td class="Center">Enabled</td>
                <td class="Center">Default</td>
            </tr>
            <tbody id="RefreshPeriodOptions">
                <asp:Repeater runat="server" ID="RefreshPeriodOptionRepeater" OnItemDataBound="RefreshPeriodOptionRepeater_OnItemDataBound">
                    <ItemTemplate>
                        <tr class="RefreshPeriodOptionRow">
                            <td class="Right">
                                <ml:EncodedLiteral runat="server" ID="OptionName" />
                                <input type="hidden" runat="server" ID="OptionValue" class="OptionValue" />
                            </td>
                            <td class="Center">
                                <input type="checkbox" runat="server" ID="EnableForLqbUsers" class="EnableForLqbUsers LqbControl" />
                            </td>
                            <td class="Center">
                                <ml:PassthroughLiteral runat="server" ID="DefaultForLqbUsersLiteral" />
                            </td>
                            <td class="Center">
                                <input type="checkbox" runat="server" ID="EnableForPmlUsers" class="EnableForPmlUsers PmlControl" />
                            </td>
                            <td class="Center">
                                <ml:PassthroughLiteral runat="server" ID="DefaultForPmlUsersLiteral" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
            <tr>
                <td colspan="5" class="Center VerticalPadding">
                    <input type="button" class="Width50Px" value="OK" id="OkBtn"/>
                    <input type="button" class="Width50Px" value="Cancel" id="CancelBtn"/>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>