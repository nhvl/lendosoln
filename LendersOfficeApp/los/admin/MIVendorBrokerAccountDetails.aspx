﻿
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="MIVendorBrokerAccountDetails.aspx.cs" Inherits="LendersOfficeApp.los.admin.MIVendorBrokerAccountDetails" %>
<%@ Import Namespace="LendersOffice.Common" %> 
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
  <head runat="server">
    <title>PMI Provider Account Details</title>
	<style type="text/css">
		.FormTableHeader
		{
		    padding: 0.25em 0.25em;
		    text-align: left;
		    text-indent: 5px;	    
		}
		
		body
		{
		    background-color: Gainsboro;
		    margin: 0 auto;
		    text-align: center;
		    line-height:2em;
		}
        form {
            padding: 1em;
        }
		table
		{
		    border: 1px solid gray;
            border-collapse: collapse;
		    margin-bottom: 1em;
		    text-align: left;
		    width: 90%;
		}
        table td {
            width: 150px;
        }
        #BtnHolder 
        {
            border: 0;
            padding: 1em;
            text-align: center; 
        }
	</style>
  </head>
<body>
    <h4 class="page-header">PMI Provider Account Details</h4>
    <form id="PMIBrokerConfigurationEditor" method="post" runat="server">
        <asp:Repeater runat="server" ID="VendorDetails" OnItemDataBound="VendorDetailsBind">
            <ItemTemplate>
                <table>
                    <thead class="FormTableHeader">
                        <tr>
                            <th colspan="2">
                                <ML:EncodedLabel ID="m_VendorName" runat="server" />
                                <asp:HiddenField ID="m_VendorId" runat="server" />
                            </th>
                        </tr>
                    </thead>
		            <tbody>
	                    <asp:PlaceHolder runat="server" ID="UsernamePanel">
	                        <tr>
	                            <td align="right" nowrap="nowrap" style="padding-right: 1em;">Username:</td>
                                <td ><asp:TextBox Width="90%" Runat="server" ID="m_Username" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="m_Username" Display="Static">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:RequiredFieldValidator>
	                            </td>
	                        </tr>
	                    </asp:PlaceHolder>
	                    <asp:PlaceHolder runat="server" ID="PasswordPanel">
	                        <tr>
	                            <td align="right" nowrap="nowrap" style="padding-right: 1em;">Password:</td>
	                            <td ><asp:TextBox Width="90%" Runat="server" ID="m_Password" />
	                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="m_Password" Display="Static">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:RequiredFieldValidator>
		                        </td>
	                        </tr>
		                </asp:PlaceHolder>
	                    <asp:PlaceHolder runat="server" ID="AccountIdPanel">
	                        <tr>
	                            <td align="right" nowrap="nowrap" style="padding-right: 1em;">Account ID:</td>
                                <td><asp:TextBox Width="90%" Runat="server" ID="m_AccountId" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="m_AccountId" Display="Static">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
	                    <asp:PlaceHolder runat="server" ID="PolicyIdPanel">
	                        <tr>
	                            <td align="right" nowrap="nowrap" style="padding-right: 1em;">Corporate Master Policy #</td>
                                <td><asp:TextBox Width="90%" Runat="server" ID="m_PolicyId" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="m_PolicyId" Display="Static">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="UnderwritingAuthorityPanel">
                            <tr>
                                <td align="right" nowrap="nowrap" style="padding-right: 1em;">Have Delegated Underwriting Authority?</td>
                                <td nowrap="nowrap" style="text-align:left">
                                <asp:RadioButton ID="m_delegatedUA" GroupName="isUADelegated" runat="server" Text="Yes" />
                                <asp:RadioButton ID="m_nondelegatedUA" GroupName="isUADelegated" runat="server" Text="No" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                    </tbody>
                </table>
            </ItemTemplate>
        </asp:Repeater>
        <div id="BtnHolder">
            <asp:Button ID="m_save" Text="Save" OnClick="Update_Click" runat="server" />
            <input type="button" value="Cancel" onclick="onClosePopup()" />
        </div>
    </form>

</body>
</html>
