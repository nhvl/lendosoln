using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.Security;

namespace LendersOfficeApp.los.admin
{
	public partial class HolidayListNew : BasePage
	{
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        protected Guid PolicyId
        {
            get { return RequestHelper.GetGuid("policyid"); }
        }

        private static Guid BrokerId
        {
            get { return PrincipalFactory.CurrentPrincipal.BrokerId; }
        }

        /// <summary>
        /// A backing field to contain the holiday closures.
        /// </summary>
        private LockDeskClosures holidayClosures;

        /// <summary>
        /// Gets the holiday closures being displayed on this page.
        /// </summary>
        /// <value>The holiday closures being displayed on this page.</value>
        protected LockDeskClosures HolidayClosures
        {
            get
            {
                if (this.holidayClosures == null)
                {
                    this.holidayClosures = new LockDeskClosures(BrokerId, this.PolicyId);
                }

                return this.holidayClosures;
            }
        }

        private Dictionary<string, Holiday> holidaysByEnumName = Holiday.AvailableLockPolicyHolidays.ToDictionary(h => h.EnumName);

        protected void Page_Load(object sender, EventArgs e)
        {
			if(!IsPostBack)
			{
                var holidayInitialStates = new Dictionary<string, bool>();
				// For each holiday, it will calculate the date and check to see if it's already
				// a closure date
				foreach(RadioButtonList rbl in loadHolidayRadios())
				{
					// Initializes all of the holidays to "No"
					rbl.SelectedIndex = 1;

                    Holiday holiday = holidaysByEnumName[rbl.ID];
                    DateTime nextDate = holiday.NextOccurrenceOnOrAfter(DateTime.Today);

                    changeInnerText(nextDate, rbl.ID);

                    bool isHolidayObserved = this.HolidayClosures.ObservedHolidays.Any(h => h.EnumName == holiday.EnumName);
                    holidayInitialStates.Add(rbl.ID, isHolidayObserved);
                    if (isHolidayObserved)
					{
						rbl.SelectedIndex = 0;
					}
				}

                this.RegisterJsObjectWithJavascriptSerializer("holidayRadioButtonListIds", holidayInitialStates.Keys); // Our Json.NET wrapper requires new(), so we can't use it.
                this.RegisterJsObjectWithJsonNetSerializer("holidayInitialStates", holidayInitialStates);
			}
		}

		// Uses the list of holidays to determine what dates to add/remove from
		// the Lock Desk Closure list.
		protected void SaveHolidays(object sender, System.EventArgs e)
		{
			foreach(RadioButtonList rbl in loadHolidayRadios())
			{
                Holiday holiday = holidaysByEnumName[rbl.ID];
                bool isHolidayObserved = this.HolidayClosures.ObservedHolidays.Any(h => h.EnumName == holiday.EnumName);
                bool userSelectedForTheHolidayToBeObserved = rbl.SelectedIndex == 0;

                if (!isHolidayObserved && userSelectedForTheHolidayToBeObserved)
                {
                    this.HolidayClosures.AddObservedHoliday(holiday);
                }
                else if (isHolidayObserved && !userSelectedForTheHolidayToBeObserved)
                {
                    this.HolidayClosures.RemoveFutureHolidayOccurrences(holiday);
                }
			}

			LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "OK", "true") ;
		}

		// Loads up the holiday list, based on their radio buttons
		private List<RadioButtonList> loadHolidayRadios()
		{
			var list = new List<RadioButtonList>();
			list.Add(NewYearsDay);
            list.Add(MartinLutherKingJrDay);
			list.Add(PresidentsDay);
			list.Add(GoodFriday);
			list.Add(Easter);
			list.Add(MemorialDay);
			list.Add(IndependenceDay);
            list.Add(LaborDay);
            list.Add(ColumbusDay);
			list.Add(VeteransDay);
			list.Add(ThanksgivingDay);
			list.Add(DayAfterThanksgiving);
			list.Add(ChristmasEve);
			list.Add(ChristmasDay);
			list.Add(NewYearsEve);

			return list;
		}

		// Loads up just the Strings for the holidays
		private static List<string> loadHolidays()
		{
			var list = new List<string>();
			list.Add("NewYearsDay");
            list.Add("MartinLutherKingJrDay");
			list.Add("PresidentsDay");
			list.Add("GoodFriday");
			list.Add("Easter");
			list.Add("MemorialDay");
			list.Add("IndependenceDay");
            list.Add("LaborDay");
            list.Add("ColumbusDay");
			list.Add("VeteransDay");
			list.Add("ThanksgivingDay");
			list.Add("DayAfterThanksgiving");
			list.Add("ChristmasEve");
			list.Add("ChristmasDay");
			list.Add("NewYearsEve");

			return list;
		}

        /// <summary>
        /// Sets the date in the UI for the given holiday.
        /// </summary>
        /// <param name="date">The next date the holiday falls on.</param>
        /// <param name="holiday">The holiday whose date we are setting.</param>
        /// <remarks>Moved into it's own function to keep the switch code from cluttering up the main logic.</remarks>
        private void changeInnerText(DateTime date, String holiday)
		{
			string s = date.ToShortDateString();
			switch (holiday)
			{
				case "NewYearsDay":
					m_NewYearsDayDate.InnerText = s;
					break;
                case "MartinLutherKingJrDay":
                    m_MartinLutherKingJrDate.InnerText = s;
                    break;
				case "PresidentsDay":
					m_PresidentsDayDate.InnerText = s;
					break;
				case "GoodFriday":
					m_GoodFridayDate.InnerText = s;
					break;
				case "Easter":
					m_EasterDate.InnerText = s;
					break;
				case "MemorialDay":
					m_MemorialDayDate.InnerText = s;
					break;
				case "IndependenceDay":
					m_IndependenceDayDate.InnerText = s;
					break;
                case "LaborDay":
                    m_LaborDayDate.InnerText = s;
                    break;
                case "ColumbusDay":
                    m_ColumbusDayDate.InnerText = s;
                    break;
				case "VeteransDay":
					m_VeteransDayDate.InnerText = s;
					break;
				case "ThanksgivingDay":
					m_ThanksgivingDayDate.InnerText = s;
					break;
				case "DayAfterThanksgiving":
					m_DayAfterThanksgivingDate.InnerText = s;
					break;
				case "ChristmasEve":
					m_ChristmasEveDate.InnerText = s;
					break;
				case "ChristmasDay":
					m_ChristmasDayDate.InnerText = s;
					break;
				case "NewYearsEve":
					m_NewYearsEveDate.InnerText = s;
					break;
			}
		}
	}
}
