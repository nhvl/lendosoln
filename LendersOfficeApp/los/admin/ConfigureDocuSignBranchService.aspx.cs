﻿namespace LendersOfficeApp.Los.Admin
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Admin.DocuSign;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Service page for the <see cref="ConfigureDocuSignBranch"/> page.
    /// </summary>
    public partial class ConfigureDocuSignBranchService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets the permission required to load this page.
        /// </summary>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage => new[]
        {
            Permission.AllowViewingAndEditingBranches
        };

        /// <summary>
        /// Process the service request.
        /// </summary>
        /// <param name="methodName">The method name to invoke server-side.</param>
        protected override void Process(string methodName)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            switch (methodName)
            {
                case nameof(this.SaveDocuSignBranchOptions):
                    this.SaveDocuSignBranchOptions(
                        principal.BrokerId,
                        this.GetGuid("BranchId"),
                        this.GetNullableGuid("BrandIdUnspecified"),
                        this.GetNullableGuid("BrandIdRetail"),
                        this.GetNullableGuid("BrandIdWholesale"),
                        this.GetNullableGuid("BrandIdCorrespondent"),
                        this.GetNullableGuid("BrandIdBroker"),
                        this.GetString("BrandNameUnspecified"),
                        this.GetString("BrandNameRetail"),
                        this.GetString("BrandNameWholesale"),
                        this.GetString("BrandNameCorrespondent"),
                        this.GetString("BrandNameBroker"));
                    return;
                default:
                    throw DataAccess.CBaseException.GenericException($"ConfigureDocuSignBranchService.aspx: Unknown service method \"{methodName}\"");
            }
        }

        /// <summary>
        /// Save the DocuSign configuration options from the Branch editor UI.
        /// </summary>
        /// <param name="brokerId">The identifier for the lender.</param>
        /// <param name="branchId">The identifier for the branch.</param>
        /// <param name="brandIdUnspecified">The brand ID to use for loans with a channel of <see cref="E_BranchChannelT.Blank"/>.</param>
        /// <param name="brandIdRetail">The brand ID to use for loans with a channel of <see cref="E_BranchChannelT.Retail"/>.</param>
        /// <param name="brandIdWholesale">The brand ID to use for loans with a channel of <see cref="E_BranchChannelT.Wholesale"/>.</param>
        /// <param name="brandIdCorrespondent">The brand ID to use for loans with a channel of <see cref="E_BranchChannelT.Correspondent"/>.</param>
        /// <param name="brandIdBroker">The brand ID to use for loans with a channel of <see cref="E_BranchChannelT.Broker"/>.</param>
        /// <param name="brandNameUnspecified">The name of the brand identified by <paramref name="brandIdUnspecified"/>.</param>
        /// <param name="brandNameRetail">The name of the brand identified by <paramref name="brandIdRetail"/>.</param>
        /// <param name="brandNameWholesale">The name of the brand identified by <paramref name="brandIdWholesale"/>.</param>
        /// <param name="brandNameCorrespondent">The name of the brand identified by <paramref name="brandIdCorrespondent"/>.</param>
        /// <param name="brandNameBroker">The name of the brand identified by <paramref name="brandIdBroker"/>.</param>
        private void SaveDocuSignBranchOptions(
            Guid brokerId,
            Guid branchId,
            Guid? brandIdUnspecified,
            Guid? brandIdRetail,
            Guid? brandIdWholesale,
            Guid? brandIdCorrespondent,
            Guid? brandIdBroker,
            string brandNameUnspecified,
            string brandNameRetail,
            string brandNameWholesale,
            string brandNameCorrespondent,
            string brandNameBroker)
        {
            var existingSettings = BranchDocuSignSettings.Retrieve(brokerId, branchId) ?? new BranchDocuSignSettings(brokerId, branchId);
            existingSettings.BrandIdForBlank = brandIdUnspecified;
            existingSettings.BrandIdForRetail = brandIdRetail;
            existingSettings.BrandIdForWholesale = brandIdWholesale;
            existingSettings.BrandIdForCorrespondent = brandIdCorrespondent;
            existingSettings.BrandIdForBroker = brandIdBroker;
            existingSettings.BrandNameForBlank = brandNameUnspecified;
            existingSettings.BrandNameForRetail = brandNameRetail;
            existingSettings.BrandNameForWholesale = brandNameWholesale;
            existingSettings.BrandNameForCorrespondent = brandNameCorrespondent;
            existingSettings.BrandNameForBroker = brandNameBroker;
            existingSettings.Save();
            this.SetResult("Status", "Success");
            this.SetResult("Message", "Saved settings.");
        }
    }
}