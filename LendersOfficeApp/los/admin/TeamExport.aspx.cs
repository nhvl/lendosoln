﻿// <copyright file="TeamExport.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: EduardoM
//    Date:   6/16/2014
// </summary>

namespace LendersOfficeApp.Los.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// This page exports both a list of teams and a list of a team's members.
    /// </summary>
    public partial class TeamExport : MinimalPage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingTeams
                };
            }
        }

        /// <summary>
        /// Exports the team members.
        /// </summary>
        public void ExportTeamMembers()
        {
            string nameFilter = RequestHelper.GetSafeQueryString("NameFilter");
            Guid teamId = RequestHelper.GetGuid("TeamId");

            List<Team.TeamUser> users = (List<Team.TeamUser>)Team.ListUsersInTeam(teamId);

            StringBuilder csvTable = new StringBuilder();
            csvTable.Append("Employee Id, Name , Branch");

            foreach (Team.TeamUser user in users)
            {
                string fullname = user.FirstName + " " + user.LastName;

                if (fullname.Contains(nameFilter))
                {
                    StringBuilder row = new StringBuilder();

                    row.Append(Environment.NewLine);
                    EmployeeDB employee = new EmployeeDB(user.EmployeeId, PrincipalFactory.CurrentPrincipal.BrokerId);
                    employee.Retrieve();
                    BranchDB branch = new BranchDB(employee.BranchID, PrincipalFactory.CurrentPrincipal.BrokerId);
                    branch.Retrieve();

                    row.Append(user.EmployeeId + ", " + fullname + ", " + branch.Name);
                    csvTable.Append(row);
                }
            }

            Response.Output.Write(csvTable.ToString());

            Response.AddHeader("Content-Disposition", "attachment; filename=\"TeamEmployees.csv\"");
            Response.ContentType = "application/csv";

            // Close up shop and return.
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Exports the list of teams.
        /// </summary>
        public void ExportTeams()
        {
            Response.BufferOutput = true;
            string nameFilter = RequestHelper.GetSafeQueryString("NameFilter");
            int teamRole = RequestHelper.GetInt("TeamRole");
            Role role = null;
            if (teamRole != -1)
            {
                role = Role.Get((E_RoleT)teamRole);
            }

            StringBuilder csvTable = new StringBuilder();
            csvTable.Append("Team Name, Team Role, Team Notes");

            List<Team> teams = (List<Team>)Team.ListTeamsAtLender(nameFilter);
            bool filterRole = role != null;
            foreach (Team team in teams)
            {
                StringBuilder row = new StringBuilder();
                row.Append(Environment.NewLine);
                if (filterRole)
                {
                    if (role.Id == team.RoleId)
                    {
                        if (team.Name.Contains(nameFilter))
                        {
                            csvTable.Append(row.Append(team.Name + ", " + role.ModifiableDesc + ", " + team.Notes));
                        }
                    }
                }
                else
                {
                    role = Role.Get(team.RoleId);
                    if (team.Name.Contains(nameFilter))
                    {
                        csvTable.Append(row.Append(team.Name + ", " + role.ModifiableDesc + ", " + team.Notes));
                    }
                }
            }

            Response.Output.Write(csvTable.ToString());

            Response.AddHeader("Content-Disposition", "attachment; filename=\"Teams.csv\"");
            Response.ContentType = "application/csv";

            // Close up shop and return.
            Response.Flush();
            Response.End();
        }

        /// <summary>
        /// Loads the page, and determines what it will export.
        /// </summary>
        /// <param name="sender">The trigger.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if(false == BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowViewingAndEditingTeams))
            {
                throw new AccessDenied("You do not have editing team access.", "User lacked editing team access.");
            }

            int exportType = RequestHelper.GetInt("ExportType");
            if (exportType == 0)
            {
                this.ExportTeams();
            }
            else
            {
                this.ExportTeamMembers();
            }
        }
    }
}
