﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowOriginatingCompaniesAssociatedWithBranch.aspx.cs" Inherits="LendersOfficeApp.los.admin.ShowOriginatingCompaniesAssociatedWithBranch" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            background-color: gainsboro;
            font-size: 11px;
        }

        div.FormTableHeader {
            font-size: 12px;
            padding: 2px;
        }

        .content {
            padding: 10px;
        }

        table {
            text-align: left;
            border-collapse: collapse;
        }

        td, th {
            border: 1px solid gray;
        }

        .explanation {
            margin-bottom: 20px;
        }

        #BranchNameContainer {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <h4 class="page-header">View Originating Companies</h4>
    <form id="form1" runat="server">
        <div class="content">
            <div class="explanation">
                The following originating companies have their Branch Id, Mini-Correspondent Branch Id, or Correspondent Branch Id set to the following branch:
            <span runat="server" id="BranchNameContainer"></span>
            </div>
            <div>
                <table>
                    <asp:Repeater ID="OriginatingCompanyRepeater" runat="server" OnItemDataBound="OriginatingCompanyRepeater_ItemDataBound">
                        <HeaderTemplate>
                            <tr class="GridHeader">
                                <th>Company Name</th>
                                <th>Company Id</th>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="<%# AspxTools.HtmlString(Container.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem") %>">
                                <td>
                                    <ml:EncodedLabel runat="server" ID="CompanyName"></ml:EncodedLabel>
                                </td>
                                <td>
                                    <ml:EncodedLabel runat="server" ID="CompanyId"></ml:EncodedLabel>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
