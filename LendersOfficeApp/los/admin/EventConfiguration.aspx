<%@ Page language="c#" Codebehind="EventConfiguration.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.EventConfiguration" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="UC" TagName="NotificationReplyConfig" Src="NotificationReplyConfig.ascx" %>
<%@ Register TagPrefix="ML" TagName="CModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
	<title>Event Configuration</title>
	<LINK href="../../css/stylesheet.css" rel="stylesheet">
	<style>
		form {
			margin-bottom: 0;
		}
	</style>
</HEAD>
<body onload="onInit();" MS_POSITIONING="FlowLayout">
	<script type="text/javascript">

		function onInit() { try
		{
            addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(m_RegularExpressionValidator1) %>);

		    var feedBack = document.getElementById("m_feedBack");
		    var errorMessage = document.getElementById("m_errorMessage");
		    var commandToDo = document.getElementById("m_commandToDo");

			if( errorMessage != null )
			{
				alert( errorMessage.value );
			}

			if( feedBack != null )
			{
				alert( feedBack.value );
			}

			if( commandToDo != null )
			{
				if( commandToDo.value == "Close" )
				{
				    parent.LQBPopup.Return();
				}
			}

			<% if( IsPostBack == false ) { %>
			
			resize( 800 , 600 );
			
			<% } %>
			
			var tb = <%= AspxTools.JsGetElementById(m_EmailAddrSendFromForNotif) %>;
			var allowVendor = <%= AspxTools.JsGetElementById(m_AllowVendorRequest) %>;
			
			if(tb.value == "")
			{
				allowVendor.checked = false;
				tb.readOnly = true;
			}
			else
			{
				allowVendor.checked = true;
				tb.readOnly = false;
			}
			
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function DisableEmailErrorMessage()
		{
			var validator = <%= AspxTools.JsGetElementById(m_RegularExpressionValidator1) %>;
			if(validator.style.display != "none")
				validator.style.display = "none";
		}

		function onClickVendorRequest(cb)
		{
			var tb = <%= AspxTools.JsGetElementById(m_EmailAddrSendFromForNotif) %>;
			if(cb.checked)
				tb.readOnly = false;
			else
			{
				tb.value = "";
				tb.readOnly = true;
				DisableEmailErrorMessage();
			}
		}

		function onClose()
		{
			var dirty = <%= AspxTools.JsGetElementById(m_isDirty) %>;
			if( isDirty() == true || dirty != null && dirty.value != "0" )
			{
				if( confirm( "Do you want to close without saving?" ) == false )
				{
					return;
				}
			}
			
			parent.LQBPopup.Return();
		}
	</script>
	<h4 class="page-header">Event Notification Address Configuration</h4>
	<form id="EventConfiguration" method="post" runat="server">
		<TABLE cellSpacing="2" cellPadding="3" border="0" style="height:100%; width:100%;">
			<TR>
				<TD vAlign="top" height="100%">
					<DIV style="WIDTH: 100%">
						<div class="FieldLabel" style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; MARGIN-BOTTOM: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 1px; BORDER-BOTTOM: lightgrey 2px solid">
							Loan Events
						</div>
						<div style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px"><uc:notificationreplyconfig id="m_EventReply" runat="server" Mode="Full" EventLabel="Event"/></div>
						<div style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px"></div>
					</DIV>
				</TD>
				<td style="PADDING-LEFT: 3px">&nbsp;</td>
				<td style="PADDING-RIGHT: 7px; BORDER-LEFT: lightgrey 2px solid">&nbsp;</td>
				<TD vAlign="top" height="100%">
					<div class="FieldLabel" style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; MARGIN-BOTTOM: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 1px; BORDER-BOTTOM: lightgrey 2px solid">
						Administrative Events
					</div>
					<div style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px"><uc:notificationreplyconfig id="m_AdminReply" runat="server" Mode="Lite" EventLabel="Admin"/></div>
					<br>
					<br>
					<%if(IsPmlEnabled) {%>
					<div class="FieldLabel" style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; MARGIN-BOTTOM: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid">
						New Credit Vendor Requests
					</div>

					<table>
						<tr>
							<td valign=top style="WIDTH: 25px">
								<asp:checkbox onclick="onClickVendorRequest(this);" id="m_AllowVendorRequest" runat="server"/>
							</td>
							<td valign=top style="WIDTH: 144px">
								Allow PML users to send new vendor requests to:
							</td>
							<td valign=top>
								<asp:textbox id="m_EmailAddrSendFromForNotif" onkeyup="DisableEmailErrorMessage();" runat="server" width="160px"/>
							</td>
						</tr>
						<tr>
							<td style="WIDTH: 269px" colSpan="3">
								<asp:regularexpressionvalidator id="m_RegularExpressionValidator1" runat="server" ControlToValidate="m_EmailAddrSendFromForNotif" Font-Bold="True" Display="Dynamic"/>
								<ml:EncodedLabel ID="m_EmailErrorLabel" runat="server" Font-Bold="True"/>
							</td>
						</tr>
					</table>
					<% } %>
				</TD>
			</TR>
			<TR>
				<TD colSpan="4">
					<HR>
					<DIV style="WIDTH: 100%; TEXT-ALIGN: center">
						<ASP:BUTTON id="m_Ok" onclick="OkClick" runat="server" Width="50px" Text="OK"/>
						<INPUT onclick="onClose();" type="button" value="Cancel" width="50px">
						<ASP:Button id="m_Apply" runat="server" Text="Apply" Width="50px" OnClick="ApplyClick"/>
					</DIV>
				</TD>
			</TR>
		</TABLE>
		<input id="m_isDirty" runat="server" type="hidden">
		<ML:CModalDlg id="m_ModalDlg" runat="server"/>
	</form>
</body>
</HTML>
