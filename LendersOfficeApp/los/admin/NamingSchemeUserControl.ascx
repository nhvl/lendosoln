<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NamingSchemeUserControl.ascx.cs" Inherits="LendersOfficeApp.los.admin.NamingSchemeUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>


<SCRIPT type="text/javascript">
    var mersUsesLoanNumber = false;
    function f_NamingScheme_refreshUI() {
        var useSequentialChecked = <%= AspxTools.JsGetElementById(UseSequentialRB) %>.checked;
        var bSequential = useSequentialChecked || mersUsesLoanNumber;

        if (useSequentialChecked == false){
          <%= AspxTools.JsGetElementById(IsAutoGenerateMersMinCB) %>.checked = true;
          <%= AspxTools.JsGetElementById(IsAutoGenerateMersMinCB) %>.disabled = true;
        } else {
            <%= AspxTools.JsGetElementById(IsAutoGenerateMersMinCB) %>.disabled = false;
        }
        f_enableSequentialScheme(bSequential);
        f_enableMersScheme();
        Tab_Validate(<%= AspxTools.JsString(this.ClientID) %>);

        if (<%= AspxTools.JsGetElementById(UseSequentialRB) %>.checked) {
            document.getElementById("currentSchemeBox").value = "Sequential";
        } else {
            document.getElementById("currentSchemeBox").value = "MERS";
        }

        jQuery('#' + <%=AspxTools.JsString(IsAutoGenerateMersMinCB.ClientID) %>).trigger('change');
        PrefixUpdate(BranchPrefixes);
        updateAvailableCounterOptions();

        var autoGenerateMersMin = <%= AspxTools.JsGetElementById(IsAutoGenerateMersMinCB) %>.checked;
        if(!useSequentialChecked || (autoGenerateMersMin && mersUsesLoanNumber))
        {
            ForceCurrentSchemeFor2ndLoans();
        }
        else
        {
            Reset2ndLoans();
        }

    }
    function f_enableSequentialScheme(bEnabled) {
      var cbMonth  = <%= AspxTools.JsGetElementById(MonthCB) %>;
      var cbYear   = <%= AspxTools.JsGetElementById(YearCB) %>;
      var cbDay    = <%= AspxTools.JsGetElementById(DayCB) %>;

      <%= AspxTools.JsGetElementById(PrefixTF) %>.readOnly = !bEnabled;

      setDisabledAttr(document.getElementById("branchPrefix"), !bEnabled);

      f_enableCheckbox(cbYear, bEnabled);
      f_enableYearChoice(cbYear.checked);

      f_enableCheckbox(cbMonth, bEnabled && cbYear.checked);
      f_enableMonthChoice(cbMonth.checked);

      f_enableCheckbox(cbDay, bEnabled && cbMonth.checked);

      <%= AspxTools.JsGetElementById(CounterDigits) %>.disabled = !bEnabled;

    }
    function f_enableCheckbox(o, bEnabled) {
      o.disabled = !bEnabled;
      o.checked = o.checked && bEnabled;
    }
    function f_enableMersScheme() {
      var bEnabled = <%= AspxTools.JsGetElementById(IsAutoGenerateMersMinCB) %>.checked;
      <%= AspxTools.JsGetElementById(MersOrganizationId) %>.readOnly = !bEnabled;
      <%= AspxTools.JsGetElementById(MersStartingCounter) %>.readOnly = !bEnabled;

      if (!bEnabled) {
        <%= AspxTools.JsGetElementById(MersOrganizationId) %>.value = "";
        <%= AspxTools.JsGetElementById(MersStartingCounter) %>.value = "";
        <%= AspxTools.JsGetElementById(MersStartingCounterValidator) %>.enabled = false;
        <%= AspxTools.JsGetElementById(MersStartingCounterRequiredValidator) %>.enabled = false;
      }
      <%= AspxTools.JsGetElementById(MersOrganizationIdValidator) %>.enabled = bEnabled;
      <%= AspxTools.JsGetElementById(MersOrganizationIdRequiredValidator) %>.enabled = bEnabled;
    }
    function NamingScheme_loaded()
    {
        jQuery('#' + <%=AspxTools.JsString(IsAutoGenerateMersMinCB.ClientID) %>).change();
        f_NamingScheme_refreshUI();
    }

    function f_enableYearChoice(bEnabled) {
      document.getElementById("<%= AspxTools.ClientId(ChooseYear) %>_0").disabled = !bEnabled;
      document.getElementById("<%= AspxTools.ClientId(ChooseYear) %>_1").disabled = !bEnabled;
      if (bEnabled) {
        if (!document.getElementById("<%= AspxTools.ClientId(ChooseYear) %>_0").checked && !document.getElementById("<%= AspxTools.ClientId(ChooseYear) %>_1").checked) {
          document.getElementById("<%= AspxTools.ClientId(ChooseYear) %>_1").checked = true;
        }
      }
      else{
        document.getElementById("<%= AspxTools.ClientId(ChooseYear) %>_0").checked = false;
        document.getElementById("<%= AspxTools.ClientId(ChooseYear) %>_1").checked = false;
      }
    }
    function f_enableMonthChoice(bEnabled) {
      document.getElementById("<%= AspxTools.ClientId(ChooseMonth) %>_0").disabled = !bEnabled;
      document.getElementById("<%= AspxTools.ClientId(ChooseMonth) %>_1").disabled = !bEnabled;
      if (bEnabled) {
        if (!document.getElementById("<%= AspxTools.ClientId(ChooseMonth) %>_0").checked && !document.getElementById("<%= AspxTools.ClientId(ChooseMonth) %>_1").checked) {
          document.getElementById("<%= AspxTools.ClientId(ChooseMonth) %>_1").checked = true;
        }
      }
      else{
        document.getElementById("<%= AspxTools.ClientId(ChooseMonth) %>_0").checked = false;
        document.getElementById("<%= AspxTools.ClientId(ChooseMonth) %>_1").checked = false;
      }
    }

    function validate(control)
    {
        //control.value = control.value.toUpperCase();
        if( control.value.search( '{' ) != -1 || control.value.search( '}' ) != -1 )
        {
            alert( 'Prefix cannot contain {, } character.' );
            control.focus();

            return;
        }
        f_NamingScheme_refreshUI();
    }
    function f_closeDefaultSchemeInfo() {
      document.getElementById("DefaultSchemeInfo").style.display = "none";
    }
    function f_showDefaultSchemeInfo() {
      var namingMsg = document.getElementById("DefaultSchemeInfo");
      namingMsg.style.display = "";
      namingMsg.style.top = "38px";
      namingMsg.style.left = "557px";
    }
    function f_displayBranchPrefix(o) {
      if (hasDisabledAttr(o))
        return false;

      var args = {
        BranchPrefixUpdate: BranchPrefixUpdate
      }

      showModal('/los/admin/BranchPrefix.aspx', args, null, null, null, {hideCloseButton:true});
      return false;
    }

    function setStartingValidator(bEnabled){
        <%= AspxTools.JsGetElementById(MersStartingCounterValidator) %>.enabled = bEnabled;
        <%= AspxTools.JsGetElementById(MersStartingCounterRequiredValidator) %>.enabled = bEnabled;
    }


    //Stubs for $ functions
    function updateAvailableCounterOptions() {}
    function BranchPrefixUpdate(branchPrefixJSON) {}
    function PrefixUpdate(branchPrefixes) {}

jQuery(function($){
    var $autoGenerateMERS = $('#' + <%=AspxTools.JsString(IsAutoGenerateMersMinCB.ClientID) %>);
    var $useSequential = $('#' + <%=AspxTools.JsString(UseSequentialRB.ClientID) %>);
    var $useMers = $('#' + <%=AspxTools.JsString(UseMersRB.ClientID) %>);
    var $IncludePrefixY = $('#' + <%=AspxTools.JsString(IncludePrefixY.ClientID) %>);
    var $IncludePrefixN = $('#' + <%=AspxTools.JsString(IncludePrefixN.ClientID) %>);
    var $ChooseMonth1Digit = $('#' + <%=AspxTools.JsString(ChooseMonth.ClientID) %> + '_0');

    //<%//put this here because the inline ASP up there messes up the Javascript intellisense%>
    var $allOptions = $('#MERSGenerationOptions').find('input[type=radio], label');
    var $useLoanNumberOptions = $('#MERSByLoanNumber').find('input[type=radio], label');

    $autoGenerateMERS.change(function(){
        if($(this).is(':checked'))
        {
            $allOptions.removeAttr('disabled');
            if($useMers.is(':checked'))
            {
                $useLoanNumberOptions.attr('disabled', 'disabled').removeAttr('checked');
            }
        }
        else
        {
            $allOptions.removeAttr('checked');
            $allOptions.attr('disabled', 'disabled');
        }

        $allOptions.trigger('update');
    }).change();

    var  $mersOptions = $('span.MERSGenType input');
    $mersOptions.change(function(){
        $mersOptions.trigger('update');
    }).on('update', function(){
        var $this = $(this);
        var $subOptions = $this.closest('label').siblings('ul').find('input, label');
        checkIfDefault($this, $mersOptions);

        if($this.is(':checked'))
        {
            $subOptions.removeAttr('disabled');
        }
        else
        {
            $subOptions.removeAttr('checked');
            $subOptions.attr('disabled', 'disabled');
        }

        $subOptions.trigger('update');

        if($this.is(':checked')){
            if($this.closest('span').is('.UseLoanNumber')){
                mersUsesLoanNumber = true;
                PrefixUpdate();
                $usedDigits.show();
                ForceCurrentSchemeFor2ndLoans();
            }
            else{
                mersUsesLoanNumber = false;
                HideInvalidPrefixNotice();
                $usedDigits.hide();
                Reset2ndLoans();
                //OPM 114547: For some reason, after being disabled and re-enabled this still looks disabled even though you can edit it.
                //Giving it focus clears that weird state out, so we'll have Javascript focus on it and then blur.
                $('.StartingCounter').focus();
                $('.StartingCounter').blur();
            }

            <%= AspxTools.JsGetElementById(MersStartingCounterValidator) %>.enabled = !mersUsesLoanNumber;
            <%= AspxTools.JsGetElementById(MersStartingCounterRequiredValidator) %>.enabled = !mersUsesLoanNumber;
            f_enableSequentialScheme(mersUsesLoanNumber || $useSequential.is(':checked'));

            if($this.closest('span').is('.UseLoanNumber')){
                if($IncludePrefixY.is('valid-prefix')) {
                    $IncludePrefixY.removeAttr('disabled').closest('label').removeAttr('disabled');
                }
                else{
                    $('.InvalidPrefixNotice').show();
                }
                $ChooseMonth1Digit.attr('disabled', 'disabled').removeAttr('checked').siblings('label').attr('disabled', 'disabled');
            }
            else
            {
                $ChooseMonth1Digit.removeAttr('disabled').siblings('label').removeAttr('disabled');
            }

        }
    });

    var $mersSubOptions = $('span.MERSByLoanNumOption input');
    $mersSubOptions.change(function(){
        $mersSubOptions.trigger('update');
    }).bind('update', function(){
        var $this = $(this);

        checkIfDefault($this, $mersSubOptions);
    });

    $('.StartingCounter').bind('update', function(){
        setStartingValidator(!$(this).is(":disabled"));
    });

    function checkIfDefault($elem, $context)
    {
        if($elem.is(':disabled')) return;
        if($elem.closest('span').is('.Default'))
        {
            var anyChecked = $context.filter(':checked').length > 0;
            if(!anyChecked)
            {
                $elem.attr('checked', 'checked');
            }
        }
    }

    var $digitAddingElements = $('#NamingSchemeTable select option, #NamingSchemeTable input[type="radio"], #NamingSchemeTable span.day-cb input, #NamingSchemeTable input.prefix_digits');

    var getDigits = /(\d+)/;
    //we'll use this to figure out how many digits are being used total, since the mers min is limited to 10 max.
    $digitAddingElements.each(function(){
        var $this = $(this);
        var digits = getDigits.exec($this.val())
        if(!digits || digits.length == 0) return;

        $this.addClass(digits[0] + "_digits adds-digit");
    }).change(function(){
        updateAvailableCounterOptions();
    });

    $('#NamingSchemeTable select').change(function(){

        updateAvailableCounterOptions();
    });

    //The day checkbox needs to be done as a special case
    $('#NamingSchemeTable span.day-cb input').addClass('2_digits adds-digit');

    var maxBranchPrefixLength = 0;
    var getClassDigit = /(.+)_digits/;
    function numDigitsInElement($elem)
    {
        var digit = getClassDigit.exec($elem.attr('class'));
        if(!digit || digit.length == 0) return null;

        if(digit[1].indexOf('prefix') != -1)
        {
            if($IncludePrefixY.is(':checked'))
            {
                return Math.max($elem.val().length, maxBranchPrefixLength);
            }

            return 0;
        }

        return digit[1];
    }

    var $saveBtns = $('#okaybutton, #applybutton');
    var $usedDigits = $('#DigitsUsed');
    function updateAvailableCounterOptions()
    {
        var hasError = $saveBtns.attr('disabled') == 'disabled';
        var numDigits = numUsedDigits();

        var availableDigits = 10 - numDigits;
        var msg = ' digit' + (availableDigits == 1 ? "" : "s") + ' available)';
        var digitDisplay = availableDigits;
        if(availableDigits < 0)
        {
            digitDisplay *= -1;
            msg = ' digit' + (digitDisplay == 1 ? "" : "s") + ' too many)';
        }

        $usedDigits.text('(' + digitDisplay + msg);

        if(availableDigits < 0)
        {
            if(!hasError)
            {
                $usedDigits.addClass('Error');
                alert($('.TooManyDigitsNotice').text());
                $('.TooManyDigitsNotice').show();
                $saveBtns.attr('disabled', 'disabled');
            }
            return;
        }

        $usedDigits.removeClass('Error');
        $saveBtns.removeAttr('disabled');
        $('.TooManyDigitsNotice').hide();
        $('#NamingSchemeTable select option').each(function(){
            var $this = $(this);
            var numDigits = numDigitsInElement($this);
            if(numDigits > availableDigits)
            {
                $this.hide();
            }
        });


    }

    function numUsedDigits()
    {
        var total = 0;
        $digitAddingElements.filter(':selected, :checked, .prefix_digits').each(function(){
            total += +numDigitsInElement($(this));
        });

        return total;
    }

    var notDigits = /[^0-9]/;
    function jq_BranchPrefixUpdateFromJSON(branchNameJSON)
    {
        var branchNames = JSON.parse(branchNameJSON);
        PrefixUpdate(branchNames);
    }

    function PrefixUpdate(prefixes)
    {
        prefixes = prefixes || window.BranchPrefixes;

        //Create a copy of the prefix list for the global state, so we can change this one without touching the global variable.
        window.BranchPrefixes = prefixes.slice();

        //This is the value of the corporate prefix, which won't otherwise be on the list.
        prefixes.push($('#NamingSchemeTable input.prefix_digits').val());
        maxBranchPrefixLength = prefixes[0].length;

        var validForPrefix = true;
        $.each(prefixes, function(idx, name){
            if(notDigits.test(name))
            {
                var includePrefixWasChecked = $IncludePrefixY.is(':checked');
                $IncludePrefixY.attr('disabled', 'disabled').removeAttr('checked').closest('label').attr('disabled', 'disabled');
                $IncludePrefixN.attr('checked', 'checked');
                maxBranchPrefixLength = 0;
                validForPrefix = false;
                if(includePrefixWasChecked){
                    alert($('.InvalidPrefixNotice').text());
                }
                return false;
            }

            maxBranchPrefixLength = Math.max(maxBranchPrefixLength, name.length);
        });

        $IncludePrefixY.addClass('valid-prefix', validForPrefix);

        if(validForPrefix)
        {
            HideInvalidPrefixNotice();
        }
    }

    function HideInvalidPrefixNotice()
    {
        $('.InvalidPrefixNotice').hide();
    }

    var $useCurrent2ndFinancing = $('#UseCurrent2ndFinancing');
    var $useDefault2ndFinancing = $('#UseDefault2ndFinancing');
    function ForceCurrentSchemeFor2ndLoans()
    {
        $useDefault2ndFinancing.removeAttr('checked').attr('disabled', 'disabled');
        $useCurrent2ndFinancing.attr('checked', 'checked');
    }

    function Reset2ndLoans()
    {
        $useCurrent2ndFinancing.removeAttr('disabled');
        $useDefault2ndFinancing.removeAttr('disabled');
    }

    window.ForceCurrentSchemeFor2ndLoans = ForceCurrentSchemeFor2ndLoans;
    window.Reset2ndLoans = Reset2ndLoans;
    window.PrefixUpdate = PrefixUpdate
    BranchPrefixUpdate = jq_BranchPrefixUpdateFromJSON;
    window.updateAvailableCounterOptions = updateAvailableCounterOptions;
});
</SCRIPT>

<style type="text/css">
#DigitsUsed
{
    display: none;
    font-weight: normal;
}

.Error
{
    color: Red;
}

.InvalidPrefixNotice, .TooManyDigitsNotice
{
    display: none;
    font-size: 11px;
    font-family: Arial, Helvetica, sans-serif;
}

#MERSSettings label
{
    display: block;
    font-size: 11px;
    font-family: Arial, Helvetica, sans-serif;
    font-weight: bold;
}

#MERSSettings span label
{
    display: inline;
}

#MERSSettings ul
{
    list-style-type: none;
    margin-top: 0px;
    margin-bottom: 5px;
    padding: 0px;
    font-size: 0px;
}

#MERSSettings li
{
    margin: 0px;
    padding: 0px;
    line-height: normal;
}

#MERSGenerationOptions
{
    margin-left: 10px;
}

</style>

<TABLE id="NamingSchemeTable" class=FormTable cellSpacing=0 cellPadding=0 border=0>
  <tr>
    <td noWrap>
      <table cellPadding=0 cellSpacing=1 class=InsetBorder width="100%">
        <tr bgColor=#999999>
          <td align=center colSpan=2><font color=white><b>Choose and configure your naming scheme</b></font></td>
        </tr>
        <tr bgColor=gainsboro>
          <td>
            <table class=InsetBorder>
              <TR>
                <TD class=FieldLabel noWrap colSpan=2><asp:RadioButton id=UseSequentialRB runat="server" Text="Sequential Naming" GroupName="NamingSchemeChoice" Checked="True" onclick="f_NamingScheme_refreshUI();" /></TD>
              </TR>
              <TR>
                <TD class=FieldLabel noWrap>Prefix
                  (optional)</TD>
                <TD noWrap><asp:TextBox id=PrefixTF onblur=validate(this); style="PADDING-LEFT: 4px" Width="148px" runat="server" MaxLength="10" class="adds-digits prefix_digits" /></TD>
              </TR>
              <TR>
                <TD class=FieldLabel noWrap></TD>
                <TD noWrap><a href='#' onclick='return f_displayBranchPrefix(this);' id='branchPrefix'>Setup branch-specific prefixes</a></TD></TR>
              <TR>
                <TD class=FieldLabel noWrap><asp:CheckBox id=YearCB onclick=f_NamingScheme_refreshUI(); runat="server" Text="Use year" class="MonthYearControl" /></TD>
                <TD noWrap><asp:RadioButtonList id=ChooseYear runat="server" RepeatDirection="Horizontal" CellPadding="0" CellSpacing="0"><asp:ListItem Value="2Y">2-digits</asp:ListItem><asp:ListItem Value="4Y">4-digits</asp:ListItem></asp:RadioButtonList></TD>
              </TR>
              <TR>
                <TD class=FieldLabel noWrap><asp:CheckBox id=MonthCB onclick=f_NamingScheme_refreshUI(); runat="server" Text="Use month" class="MonthYearControl"></asp:CheckBox></TD>
                <TD noWrap><asp:RadioButtonList id=ChooseMonth class="MonthList" runat="server" RepeatDirection="Horizontal" CellPadding="0" CellSpacing="0"><asp:ListItem Value="1M">1-digit</asp:ListItem><asp:ListItem Value="2M">2-digits</asp:ListItem></asp:RadioButtonList></TD>
              </TR>
              <TR>
                <TD class=FieldLabel noWrap><asp:CheckBox id=DayCB runat="server" Text="Use day" class="day-cb" /></TD>
                <TD noWrap>2-digits</TD>
              </TR>
              <TR>
                <TD class=FieldLabel noWrap>Counter </TD>
                <TD noWrap><asp:DropDownList id=CounterDigits Width="120px" runat="server">
        	      <asp:ListItem Value="{c:3}">3-digits</asp:ListItem>
            	  <asp:ListItem Value="{c:4}">4-digits</asp:ListItem>
             	  <asp:ListItem Value="{c:5}">5-digits</asp:ListItem>
               	  <asp:ListItem Value="{c:6}">6-digits</asp:ListItem>
            	  <asp:ListItem Value="{c:7}">7-digits</asp:ListItem>
            	  <asp:ListItem Value="{c:8}">8-digits</asp:ListItem>
            	  <asp:ListItem Value="{c:9}">9-digits</asp:ListItem>
        	  </asp:DropDownList></TD>
              </TR>
              <tr>
              <td class=FieldLabel noWrap colSpan=2><asp:RadioButton id=UseMersRB runat="server" Text="MERS Naming" GroupName="NamingSchemeChoice" onclick="f_NamingScheme_refreshUI();"/></td>
              </tr>
            </table>
          </td>
          <td vAlign=top>
            <TABLE class="InsetBorder" id="MERSSettings" width=280>
              <tr>
                <td>
                    <label><asp:CheckBox class="FieldLabel" ID="IsAutoGenerateMersMinCB" runat="server" onclick="f_NamingScheme_refreshUI();"/> Automatically generate MERS MIN</label>
                    <div id="MERSGenerationOptions">
                        <label>Organization ID <asp:TextBox id=MersOrganizationId Width="75px" runat="server" MaxLength="7" />&nbsp;<asp:RequiredFieldValidator id=MersOrganizationIdRequiredValidator runat="server" ControlToValidate="MersOrganizationId" ErrorMessage="* Required" Display="Dynamic"></asp:RequiredFieldValidator><asp:RegularExpressionValidator id=MersOrganizationIdValidator runat="server" ErrorMessage="Must be 7 digits." ValidationExpression="\d{7}" ControlToValidate="MersOrganizationId" Display="Dynamic"></asp:RegularExpressionValidator></label>

                        <ul id="MERSGenerationType">
                            <li id="MERSBySequence">
                                <label><asp:RadioButton runat="server" ID="UseSequentialMERS" GroupName="MERSGenType" class="MERSGenType Default" /> Use sequential naming</label>
                                <ul id="MERSBySequenceOptions">
                                    <li>
                                        <label>Starting Counter <asp:TextBox id=MersStartingCounter Width="75px" runat="server" MaxLength="10" class="StartingCounter" />&nbsp;<asp:RequiredFieldValidator id=MersStartingCounterRequiredValidator runat="server" ControlToValidate="MersStartingCounter" ErrorMessage="* Required" Display="Dynamic"></asp:RequiredFieldValidator><asp:RegularExpressionValidator id=MersStartingCounterValidator runat="server" ErrorMessage="Must be all digits." ValidationExpression="\d{0,10}" ControlToValidate="MersStartingCounter" Display="Dynamic"></asp:RegularExpressionValidator></label>
                                    </li>
                                </ul>
                            </li>
                            <li id="MERSByLoanNumber">
                                <label><asp:RadioButton runat="server" ID="UseLoanNumberMERS" GroupName="MERSGenType" class="MERSGenType UseLoanNumber" /> Use loan number <span id="DigitsUsed"></span></label>
                                <ul id="MERSByLoanNumberOptions">
                                    <li>
                                        <label><asp:RadioButton runat="server" ID="IncludePrefixY" class="MERSByLoanNumOption Default" GroupName="IncludePrefixType" /> Include prefix </label>
                                    </li>
                                    <li>
                                        <label><asp:RadioButton runat="server" ID="IncludePrefixN" class="MERSByLoanNumOption" GroupName="IncludePrefixType" /> Do not include prefix</label>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <span class="Error InvalidPrefixNotice FieldLabel">To generate a MERS MIN from the loan number including the prefix, the corporate prefix and all branch prefixes must contain only digits.</span> <br />
                    <span class="Error TooManyDigitsNotice FieldLabel">To generate a MERS MIN from the loan number, the loan number must be 10 digits or less.</span>
                </td>
              </tr>
            </TABLE>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <TR>
    <TD noWrap>
        <table bgcolor="#999999" cellpadding="0" cellspacing="1" class="InsetBorder" width="100%">
            <tr>
                <td align="center" nowrap colspan="3">
                    <font color="white"><b>Decide when to use your naming scheme</b></font>
                </td>
            </tr>
            <tr>
                <td bgcolor="gainsboro" nowrap>
                    <b>Loan type</b>
                </td>
                <td align="center" bgcolor="gainsboro">
                    Current scheme: &nbsp;<input id="currentSchemeBox" readonly size="6" style="text-align: center"
                        type="text">
                </td>
                <td align="center" bgcolor="gainsboro">
                    <a href='#"' onclick='f_showDefaultSchemeInfo();'>LendingQB default scheme</a>
                </td>
            </tr>
            <tr>
                <td bgcolor="gainsboro">
                    Loans created from templates
                </td>
                <td align="center" bgcolor="gainsboro">
                    <input checked disabled type="radio">
                </td>
                <td align="center" bgcolor="gainsboro">
                    N/A
                </td>
            </tr>
            <tr>
                <td bgcolor="gainsboro">
                    Duplicated loans
                </td>
                <td align="center" bgcolor="gainsboro">
                    <input name="<%= AspxTools.ClientId(this) %>$IsDuplicateLoanCreatedWAutonameBit" type="radio"
                        value="True">
                </td>
                <td align="center" bgcolor="gainsboro">
                    <input name="<%= AspxTools.ClientId(this) %>$IsDuplicateLoanCreatedWAutonameBit" type="radio"
                        value="False">
                </td>
            </tr>
            <tr>
                <td bgcolor="gainsboro">
                    Second (subfinancing) loans
                </td>
                <td align="center" bgcolor="gainsboro">
                    <input name="<%= AspxTools.ClientId(this) %>$Is2ndLoanCreatedWAutonameBit" type="radio" value="True" id="UseCurrent2ndFinancing">
                </td>
                <td align="center" bgcolor="gainsboro">
                    <input name="<%= AspxTools.ClientId(this) %>$Is2ndLoanCreatedWAutonameBit" type="radio" value="False" id="UseDefault2ndFinancing">
                </td>
            </tr>
        </table>
    </TD>
  </TR>
</TABLE>
<asp:TextBox id=PreviousNamingSchemeChoice runat="server" style="DISPLAY:none"></asp:TextBox>
			<div id="DefaultSchemeInfo" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:202px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; BACKGROUND-COLOR:whitesmoke">
			<table width="100%"><tr><td><b><u>How our default naming scheme works</u></b>:<br><br>Let's say the loan number for your original/1st loan is <font color=blue>123</font>.<br><br>If you were to <b>duplicate</b> this loan, our default naming scheme would take the original loan number (123) and add four random letters to end (e.g., <font color=blue>123-FJRH</font>).<br><br>If you were to create a <b>2nd/subfinancing</b> loan from the original (1st), our default naming scheme would take the original loan number (123) and add "x2ndx" and two random letters to the end (e.g., <font color=blue>123x2ndxQD</font>).<br><br></td></tr>
			<tr><td align=center>[ <a href="#" onclick="f_closeDefaultSchemeInfo();">Close</a> ]</td></tr>
			</table>
			</div>
