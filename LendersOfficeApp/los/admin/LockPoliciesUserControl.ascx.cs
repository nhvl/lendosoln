﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Common;

namespace LendersOfficeApp.los.admin
{
    public partial class LockPoliciesUserControl : System.Web.UI.UserControl
    {
        private const string JS_JSON_FILENAME = "json.js";
        private const string JS_LOCKPOLICIES_FILENAME = "lockpolicies.js";
        protected void PageLoad(object sender, EventArgs e)
        {
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), JS_JSON_FILENAME, string.Format("<script src=\"{0}/inc/{1}\" language=\"javascript\" type=\"text/javascript\"></script>", ((BasePage)Page).VirtualRoot, JS_JSON_FILENAME));
            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), JS_LOCKPOLICIES_FILENAME, string.Format("<script src=\"{0}/inc/{1}\" language=\"javascript\" type=\"text/javascript\"></script>", ((BasePage)Page).VirtualRoot, JS_LOCKPOLICIES_FILENAME));
            ((BasePage)Page).RegisterJsScript(JS_JSON_FILENAME);
            ((BasePage)Page).RegisterJsScript(JS_LOCKPOLICIES_FILENAME);
            
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
        }
        #endregion
    }
}