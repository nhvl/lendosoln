<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BrokerFairLendingUserControl.ascx.cs" Inherits="LendersOfficeApp.los.admin.BrokerFairLendingUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<asp:Panel id="BasePanel" runat="server" style="PADDING: 8px;" Width="100%">
    <TABLE class="FormTable" cellspacing="0" cellpadding="0" border="0" width="100%">
    <TR>
    <TD class="FieldLabel">
        Questions or complaints may be addressed to
    </TD>
    </TR>
    <TR>
    <TD>
    <TABLE cellPadding="0" cellSpacing="0" border="0">
    <TR>
    <TD>
        <asp:TextBox id=AddressLine1 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine2 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine3 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine4 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine5 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine6 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine7 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine8 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine9 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    </TABLE>
    </TD>
    </TR>
    </TABLE>
</asp:Panel>