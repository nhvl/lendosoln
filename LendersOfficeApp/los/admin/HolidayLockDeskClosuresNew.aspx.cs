using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using System.Data.SqlClient;
using LendersOffice.Security;
using System.Collections.Generic;
using LendersOffice.ObjLib.LockPolicies;

namespace LendersOfficeApp.los.admin
{
	public partial class HolidayLockDeskClosuresNew : BasePage
	{
		protected MeridianLink.CommonControls.TimeTextBox		m_StartTime;
		protected MeridianLink.CommonControls.TimeTextBox		m_EndTime;
		protected System.Web.UI.WebControls.TextBox				m_minReqToLock;
		private string											m_timezoneSetting;
		protected System.Web.UI.HtmlControls.HtmlInputHidden    m_timezone;
		protected bool											m_isDisabled = false;

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

		protected Guid PolicyId
		{
            get { return RequestHelper.GetGuid("policyid", Guid.Empty); }
		}
        private Guid BrokerId
        {
            get { return PrincipalFactory.CurrentPrincipal.BrokerId; }
        }

        /// <summary>
        /// A backing field to contain the holiday closures.
        /// </summary>
        private LockDeskClosures holidayClosures;

        /// <summary>
        /// Gets the holiday closures being displayed on this page.
        /// </summary>
        /// <value>The holiday closures being displayed on this page.</value>
        protected LockDeskClosures HolidayClosures
        {
            get
            {
                if (this.holidayClosures == null)
                {
                    this.holidayClosures = new LockDeskClosures(this.BrokerId, this.PolicyId);
                }

                return this.holidayClosures;
            }
        }

        public string TimezoneSetting
		{
			get { return m_timezoneSetting; }
			set { ViewState.Add( "TimezoneSetting" , m_timezoneSetting = value ); }
		}

		public bool IsDisabled
		{
			get { return m_isDisabled; }
			set { ViewState.Add( "IsDisabled" , m_isDisabled = value ); }
		}

        public bool ShowPastClosures
        {
            get { return ((bool?)this.ViewState["ShowPastClosures"]) ?? false; }
            set { this.ViewState["ShowPastClosures"] = value; }
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (RequestHelper.GetSafeQueryString("timezone") != "")
				TimezoneSetting = RequestHelper.GetSafeQueryString("timezone");
			else if( ViewState[ "TimezoneSetting" ] != null )
				m_timezoneSetting = ViewState[ "TimezoneSetting" ].ToString();
			else
				TimezoneSetting = "PST";

            // Disable the holiday lock closures frame if the policy ID is empty to prevent
            // the system from trying to look up the (empty) new policy ID when opening the add window 
            // and then blowing up since there is no policy that is all zeroes.
            if (PolicyId == Guid.Empty)
                IsDisabled = true;
			else if(RequestHelper.GetSafeQueryString("disabled") != "" && RequestHelper.GetSafeQueryString("disabled") != null)
				IsDisabled = bool.Parse(RequestHelper.GetSafeQueryString("disabled"));
			else if( ViewState[ "IsDisabled" ] != null )
				m_isDisabled = bool.Parse(ViewState[ "IsDisabled" ].ToString());
			else
				IsDisabled = false;

			BindDataGrid();
		}

		protected void m_btnRefresh_Click(object sender, System.EventArgs e)
		{
            // Let the refresh be handled by PageLoad
			////BindDataGrid();
		}

        protected void TogglePastClosuresBtnClick(object sender, EventArgs e)
        {
            this.ShowPastClosures = !this.ShowPastClosures;
            this.ShowPastBtn.Text = this.ShowPastClosures ? "Hide Past Closures" : "Show Past Closures";
            this.BindDataGrid();
        }

        protected void m_Grid_ItemCommand( object sender, DataGridCommandEventArgs e)
        {
            if (IsDisabled)
            {
                return;
            }

	        switch ( e.CommandName ) 
	        {
		        case "DropHolidayClosure":
                    var date = DateTime.Parse(e.Item.Cells[0].Text);
                    Holiday holidayToDelete = AdditionalObservedHolidayToDelete(date);
                    if (holidayToDelete == null)
                    {
                        this.HolidayClosures.RemoveClosure(date);
                    }
                    else
                    {
                        this.HolidayClosures.RemoveFutureHolidayOccurrences(holidayToDelete);
                    }

                    break;
	        }

	        BindDataGrid();
        }

        private Holiday AdditionalObservedHolidayToDelete(DateTime date)
        {
            date = date.Date;
            if (date < DateTime.Today)
            {
                return null;
            }

            return this.HolidayClosures.ObservedHolidays.FirstOrDefault(holiday => holiday.CalculateDate(date.Year) == date);
        }

		protected void m_Grid_ItemDataBound(object sender, DataGridItemEventArgs e) 
		{
			const int DropCol = 5;
			if ( e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer ) 
			{
				LinkButton butn = e.Item.Cells[DropCol].Controls[0] as LinkButton; 
				if(IsDisabled == false)
                {
                    var row = (DataRowView)e.Item.DataItem;
                    var closureDate = DateTime.Parse((string)row["ClosureDate"]);
                    butn.Attributes["onclick"] = $"return confirm({LendersOffice.AntiXss.AspxTools.JsString(GetDeleteConfirmationMessage(closureDate))})";
                }

				TableCell allDayCell = e.Item.Cells[1] as TableCell;
				bool allDayVal = bool.Parse(allDayCell.Text);
				allDayCell.Text = (allDayVal)?"Yes":"No";

                TableCell businessDayCell = e.Item.Cells[2] as TableCell;
                bool businessDay = bool.Parse(businessDayCell.Text);
                businessDayCell.Text = (businessDay) ? "Yes" : "No";

				TableCell dateCell = e.Item.Cells[0] as TableCell;
				dateCell.Text = DateTime.Parse(dateCell.Text).ToShortDateString();

				TableCell openTimeCell = e.Item.Cells[3] as TableCell;
				openTimeCell.Text = (allDayVal)?"n/a":DateTime.Parse(openTimeCell.Text).ToShortTimeString();

				TableCell closeTimeCell = e.Item.Cells[4] as TableCell;
				closeTimeCell.Text = (allDayVal)?"n/a":DateTime.Parse(closeTimeCell.Text).ToShortTimeString();
			}
		}

        /// <summary>
        /// Gets the confirmation message to display when deleting a closure date from the list.
        /// </summary>
        /// <param name="closureDate">The date to delete.</param>
        /// <returns>A confirmation message to display to the user.</returns>
        private string GetDeleteConfirmationMessage(DateTime closureDate)
        {
            string confirmationMessage = "Are you sure you want to delete this entry?";
            if (closureDate < DateTime.Today)
            {
                confirmationMessage = "Deleting past dates will change the values of loan data calculated using business days. " + confirmationMessage;
            }
            else
            {
                var additionalHolidayDeletion = AdditionalObservedHolidayToDelete(closureDate);
                if (additionalHolidayDeletion != null)
                {
                    confirmationMessage = $"Deleting this closure will mark this lock policy as open on {additionalHolidayDeletion.Name}. " + confirmationMessage;
                }
            }

            return confirmationMessage;
        }

		private void SetDisabled()
		{
			if(IsDisabled == true)
			{
				m_ButtonPanel.Enabled = false;
				m_Grid.Enabled = false;
			}
			else
			{
				m_ButtonPanel.Enabled = true;
				m_Grid.Enabled = true;
			}
		}

		private void BindDataGrid() 
		{
            m_Grid.DataSource = LockDeskClosures.GetDataTableOfClosures(this.HolidayClosures.GetUpcoming(this.ShowPastClosures)).DefaultView;
            m_Grid.DataBind();
            m_EmptyMessage.Visible = m_Grid.Items.Count == 0;
			SetDisabled();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
