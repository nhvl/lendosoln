﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;
using System.Runtime.Serialization;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.admin
{
    public partial class ConditionChoiceEditor : BasePage
    {

        private ConditionChoice m_xCurrentConditionChoice; 

        private BrokerUserPrincipal Principal
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal;
            }
        }

        private bool IsNew
        {
            get
            {
                return RequestHelper.GetInt("id", -1) == -1;
            }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingConditionChoices
                };
            }
        }

        private ConditionChoice CurrentConditionChoice
        {
            get
            {
                if (m_xCurrentConditionChoice == null)
                {

                    int id = RequestHelper.GetInt("id", -1);
                    if (id == -1)
                    {
                        m_xCurrentConditionChoice = new ConditionChoice(Principal.BrokerId);
                    }
                    else
                    {
                        m_xCurrentConditionChoice = ConditionChoice.Retrieve(Principal.BrokerId, id);
                    }
                }

                return m_xCurrentConditionChoice;
            }
        }

        protected void OK_OnClick(object sender, EventArgs args)
        {
            Guid toBeAssignedToId, toBeOwnedbyId;
            try {
                toBeAssignedToId = new Guid(ToBeAssignedToRole.SelectedValue);
                toBeOwnedbyId = new Guid(ToBeOwnedByRole.SelectedValue);
            }
            catch( FormatException )
            {
                ShowErrorMessage("To Be Assigned To/To Be Owned By is required");
                return;
            }

            if (string.IsNullOrEmpty(DueDateFieldId.Value))
            {
                ShowErrorMessage("Due Date is required.");
                return;
            }

            if (string.IsNullOrEmpty(ConditionSubject.Text))
            {
                ShowErrorMessage("Condition Subject is required.");
                return;
            }

            int buisnessDays;

            if (false == int.TryParse(DueDateBuisnessDayAddition.Value, out buisnessDays))
            {
                ShowErrorMessage("Business Days has to be a number.");
                return;
            }

            int conditionCategoryid;

            if( false == int.TryParse(ConditionCategories.SelectedValue, out conditionCategoryid))
            {
                ShowErrorMessage("Condition Categories is required.");
                return;
            }

            E_ConditionType choiceType = (E_ConditionType)Tools.GetDropDownListValue(ConditionTypes);
            E_ConditionChoice_sLT loanType = (E_ConditionChoice_sLT)Tools.GetDropDownListValue(sLT);

            CurrentConditionChoice.ToBeAssignedRoleId = toBeAssignedToId;
            CurrentConditionChoice.ToBeOwnedByRoleId = toBeOwnedbyId;
            CurrentConditionChoice.CategoryId = conditionCategoryid;
            CurrentConditionChoice.ConditionType = choiceType;
            CurrentConditionChoice.sLT = loanType;
            CurrentConditionChoice.ConditionSubject = ConditionSubject.Text;
            CurrentConditionChoice.DueDateAddition = buisnessDays;
            CurrentConditionChoice.DueDateFieldName = DueDateFieldId.Value;
            CurrentConditionChoice.IsHidden = IsHidden.Checked;

            CurrentConditionChoice.RequiredDocTypeId = null; // OPM 170314 - default required doc id to null
            if (Principal.BrokerDB.IsEDocsEnabled && DocTypePicker.Value != "-1")
            {
                CurrentConditionChoice.RequiredDocTypeId = int.Parse(DocTypePicker.Value);
            }

            if (IsNew)
            {
                CurrentConditionChoice.Rank = 999; //make it really high so it ends up in the bottom
            }
            CurrentConditionChoice.Save(Principal.BrokerId);
            
            ConditionChoice choice = ConditionChoice.Retrieve(Principal.BrokerId, CurrentConditionChoice.ConditionChoiceId );

            var choiceData = new
            {
                Category = choice.Category.Category,
                ConditionType = choice.ConditionType_rep,
                ConditionSubject = choice.ConditionSubject,
                RequiredDocType = choice.RequiredDocType_rep,
                ToBeAssigedRole = choice.ToBeAssignedRole,
                ToBeOwnedByRole = choice.ToBeOwnedByRole,
                DueDateDescription = Tools.GetTaskDueDateDescription(choice.DueDateAddition, choice.DueDateFieldName, true),
                Id = choice.ConditionChoiceId,
                sLT = choice.sLT_rep,
                IsHidden = choice.IsHidden ? "Yes" : "No"
            };

            string choiceJSon = ObsoleteSerializationHelper.JavascriptJsonSerialize(choiceData);

            AddInitScriptFunctionWithArgs("SetAndClose", choiceJSon);
            
        }

        private void ShowErrorMessage(string error)
        {
            m_errorMessage.Text = error;
            m_errorMessage.Visible = true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterJsScript("LQBPopup.js");	
            
            if (Page.IsPostBack)
            {
                return;
            }
            // OPM 108148 gf - include Shipper and Funder in roles
            KeyValuePair<Guid, string>[] roles = Tools.GetRolesExcluding("Consumer", "Administrator", "Accountant").OrderBy(p => p.Value).ToArray(); 
            ToBeAssignedToRole.DataSource = roles;
            ToBeAssignedToRole.DataValueField = "Key";
            ToBeAssignedToRole.DataTextField = "Value";
            ToBeAssignedToRole.DataBind();

            IsHidden.Checked = CurrentConditionChoice.IsHidden;

            ToBeOwnedByRole.DataSource = roles;
            ToBeOwnedByRole.DataValueField = "Key";
            ToBeOwnedByRole.DataTextField = "Value";
            ToBeOwnedByRole.DataBind();

            ConditionCategories.DataSource = ConditionCategory.GetCategories(Principal.BrokerId);
            ConditionCategories.DataValueField = "Id";
            ConditionCategories.DataTextField = "Category";
            ConditionCategories.DataBind();

            Tools.Bind_ConditionChoice_sLT(sLT);
            Tools.Bind_ConditionType(ConditionTypes);

            trRequiredDocType.Visible = Principal.BrokerDB.IsEDocsEnabled;
            DocTypePicker.EnableClearLink();
            DocTypePicker.SetDefault("None", "-1");

            string header = IsNew ? "Add New Condition Choice" : "Edit Condition Choice";
            FormHeader.Text = header;
            Header.Title = header;

            ParameterReporting a = ParameterReporting.RetrieveSystemParameters();
            if (false == IsNew)
            {
                Tools.SetDropDownListValue(ConditionCategories, CurrentConditionChoice.Category.Id);
                Tools.SetDropDownListValue(sLT, CurrentConditionChoice.sLT);
                Tools.SetDropDownListValue(ConditionTypes, CurrentConditionChoice.ConditionType);
                Tools.SetDropDownListValue(ToBeAssignedToRole, CurrentConditionChoice.ToBeAssignedRoleId.ToString());
                Tools.SetDropDownListValue(ToBeOwnedByRole, CurrentConditionChoice.ToBeOwnedByRoleId.ToString());

                
                ConditionSubject.Text = CurrentConditionChoice.ConditionSubject;
                DueDateField.Text = CurrentConditionChoice.FriendlyDueDateDescription;
                DueDateBuisnessDayAddition.Value = CurrentConditionChoice.DueDateAddition.ToString();
                DueDateFieldId.Value = CurrentConditionChoice.DueDateFieldName;
                DueDateBuisnessDayAddition.Value = CurrentConditionChoice.DueDateAddition.ToString();

                if (CurrentConditionChoice.RequiredDocTypeId.HasValue)
                {
                    DocTypePicker.Select(CurrentConditionChoice.RequiredDocTypeId.Value);
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            base.OnInit(e);
        }
    }
}
