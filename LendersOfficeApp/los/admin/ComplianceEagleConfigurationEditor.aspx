﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceEagleConfigurationEditor.aspx.cs" Inherits="LendersOfficeApp.los.admin.ComplianceEagleConfigurationEditor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ComplianceEagle Configuration</title>
    <style>
        #Main {
            border: solid 1px black;
            padding: 5px;
        }

        h1 {
            margin: 0;
            padding: 0;
            font-size: 12px;
            text-align: left;
        }

        body {
            margin: 0 auto;
            text-align: center;
            line-height: 2em;
            background-color: Gainsboro;
        }
        
        form{
            padding: 1em;
        }

        a {
            display: block;
            position: relative;
            bottom: 5px;
        }

        select.box {
            width: 400px;
            height: 100px;
        }

        .ErrorMessageSection {
            margin-top: 5px;
            font-weight: bold;
            color: red;
        }

        .Section div {
            text-align: left;
            margin-left: 5%;
        }

        #BeginRunningAuditsAt {
            text-align: left;
        }

        #BeginRunningAuditsAt h1 {
            display: inline;
            padding-right: 10px;
        }
    </style>
    <script>
        function _init() {
            validateBeginAuditsStatus();
        }

        function validateBeginAuditsStatus() {
            var beginAuditStatus = $('#BeginAuditStatus option:selected').text();
            var beginAuditStatusIsExcluded = false;

            $('#ExcludedStatuses option').each(function () {
                if ($(this).text() === beginAuditStatus) {
                    beginAuditStatusIsExcluded = true;
                }
            });

            if (beginAuditStatusIsExcluded) {
                $('#ErrorMessage').text("The " + beginAuditStatus + " status cannot be set as the Begin Audit "
                    + "status because it has been marked for exclusion. Please select another Begin Audit status "
                    + "or remove the selected status from the exclusion list.");
                $('#ErrorMessage').show();
                $('#Update').attr('disabled', true);
            }
            else {
                $('#ErrorMessage').val('');
                $('#ErrorMessage').hide();
                $('#Update').removeAttr('disabled');
            }
        }

        function resize(w, h) {
            window.dialogWidth = w + "px";
            window.dialogHeight = h + "px";
        }

        jQuery(function ($) {
            resize(500, 500);

            $('#EditExcludedStatuses').click(function () {
                var select = $(this).siblings('select').first();
                var args = { init: [] };
                var opts = select[0].options;
                var numInit = opts.length;
                for (var i = 0; i < numInit; i++) {
                    args.init.push(opts[i].value);
                }

                var beginAuditStatusQuery = beginAuditStatusQuery = '&beginAuditStatus=' + $('#BeginAuditStatus').val();
                var vendorQuery = '&vendor=complianceeagle';
                showModal("/los/admin/ComplianceOptionPicker.aspx?type=ExcludedStatuses" + beginAuditStatusQuery + vendorQuery, args, null, null, function(ret){ 
                    var results = ret.result;
                    var h = hypescriptDom;
    
                    if (results) {
                        select.empty().append($.map(results, function(res){
                            return h("option", {value:res.value}, res.text)
                        }));
    
                        var submit = $(this).siblings('input[type="hidden"]');
                        submit.val(ret.valuesString);
    
                        validateBeginAuditsStatus();
                    }
                }, {context: this, hideCloseButton: true});
            });
        });
    </script>
</head>
<body>
    <h4 class="page-header">ComplianceEagle Account Details</h4>
    <form id="form1" runat="server">
        <div id="Main">
            <div id="BeginRunningAuditsAt" class="Section">
                <h1>Begin Audits when loan reaches:
                </h1>
                <select id="BeginAuditStatus" runat="server" onchange="validateBeginAuditsStatus();"></select>
            </div>
            <div id="ExcludedStatusesDiv" class="Section">
                <h1>Exclude loans with loan status:
                </h1>
                <div>
                    <select class="box" multiple="true" id="ExcludedStatuses" runat="server"></select>
                    <a href="#" class="Edit" id="EditExcludedStatuses">edit excluded statuses</a>
                    <input type="hidden" id="ExcludedStatusesSubmit" runat="server" value="null" />
                </div>
            </div>
            <div class="ErrorMessageSection">
                <span runat="server" class="hidden" id="ErrorMessage"></span>
            </div>
            <div>
                <asp:Button runat="server" ID="Update" Text="Update" OnClick="Update_Click"></asp:Button>
            </div>
        </div>
    </form>
</body>
</html>
