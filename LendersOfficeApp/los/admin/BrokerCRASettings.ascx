<%@ Import namespace="LendersOfficeApp.los.admin"%>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BrokerCRASettings.ascx.cs" Inherits="LendersOfficeApp.los.admin.BrokerCRASettings" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>

  <HEAD>
</HEAD>
  <style>
    .HelpStyle { BORDER-RIGHT: black 3px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 3px solid; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 3px solid; WIDTH: 230px; PADDING-TOP: 5px; BORDER-BOTTOM: black 3px solid; POSITION: absolute; HEIGHT: 50px; BACKGROUND-COLOR: whitesmoke }
    </style>

<script type="text/javascript">
function BrokerCRA_loaded() 
{
    setSelectVerbalAuthorizationDocumentDisabled();

    for (var index = 0; index < 10; index++)
        onCreditReportProtocolChange(document.getElementById("BrokerCRA_CRA" + index), index);
}
function removeCRA(index) 
{
  document.getElementById("BrokerCRA_CRA" + index).selectedIndex = 0;
  document.getElementById("BrokerCRA_IsPdfViewNeeded" + index).checked = false;
  document.getElementById("BrokerCRA_IsPdfViewNeeded" + index).disabled = true;  
  updateDirtyBit();
}
function onCreditReportProtocolChange(ddl, index)
{
  if(invalidateDuplicateEntries(ddl, index))
  {
	alert(<%= AspxTools.JsString(JsMessages.CreditAgencyAlreadyInList) %>); 
	return;
  }
  
  var cbIsPdfViewNeeded = document.getElementById("BrokerCRA_IsPdfViewNeeded" + index);

  var bIsNonMcl = f_isNonMcl(ddl.value);
  cbIsPdfViewNeeded.disabled = bIsNonMcl;    
  
  if (bIsNonMcl) 
    cbIsPdfViewNeeded.checked = false;    
}

function invalidateDuplicateEntries(ddl, index)
{
	var craId = ddl.value;
	if (craId == <%= AspxTools.JsString(Guid.Empty.ToString()) %>) 
		return false;
	
	var element;
	for (var i = 0; i < 10; ++i)
	{
		if(i != index)
		{
			element = document.getElementById("BrokerCRA_CRA" + i);
			if(element.value == craId)
			{
				ddl.selectedIndex = 0;
				return true;
			}
		}
	}
	return false;
}

function f_isNonMcl(id) 
{
  for (var i = 0; i < g_aNonMclList.length; i++)
    if (id == g_aNonMclList[i]) return true;
  return false;
}

function f_hidePmlCra() 
{
  showModal('/los/admin/HiddenCRAList.aspx', null, null, null, null, {hideCloseButton: true});
  return false;
}

function setSelectVerbalAuthorizationDocumentDisabled() {
    var input = <%=AspxTools.JsGetElementById(this.AllowIndicatingVerbalCreditReportAuthorization)%>;
    var selectLink = document.getElementById('SelectVerbalAuthorizationDocument');
    setDisabledAttr(selectLink, !input.checked);

    var verbalAuthorizationFormName = $('#VerbalAuthorizationDocumentName');
    if (input.checked) {
        if (verbalAuthorizationFormName.text().length === 0) {
            verbalAuthorizationFormName.text('(Using default document)');
        }
    }
    else {
        verbalAuthorizationFormName.text('').removeAttr('data-doc-id');
    }
}

function selectVerbalAuthorizationDocument() {
    showModal('/newlos/ElectronicDocs/SelectCustomPdfForm.aspx?displayClearButton=1', null, null, null, function(result) {
        if (result) {
            if (result.ClearSelection) {
                $('#VerbalAuthorizationDocumentName').removeAttr('data-doc-id').text('(Using default document)');
            }
            else if (result.FormName && result.FormId) {
                $('#VerbalAuthorizationDocumentName').attr('data-doc-id', result.FormId).text('(' + result.FormName + ' selected)');
            }
        }
    });
}

registerPostGetAllFormValuesCallback(function (args) {
    args["VerbalCreditReportAuthorizationCustomPdfId"] = $('#VerbalAuthorizationDocumentName').attr('data-doc-id');
});

registerPostPopulateFormCallback(function (args) {
    if (typeof args.VerbalAuthorizationDocumentName !== 'undefined') {
        $('#VerbalAuthorizationDocumentName')
            .text('(' + args.VerbalAuthorizationDocumentName + ' selected)')
            .attr('data-doc-id', args.VerbalCreditReportAuthorizationCustomPdfId);
    }
});
</script>
<table class=FormTable style="MARGIN: 8px" cellSpacing=0 cellPadding=0 width="97%" border=0>
  <tr>
    <td vAlign=top>
      <table>
        <tr>
          <td style="FONT: 11px arial"><b>Preferred Credit Agencies</b><br></td></tr>
        <tr>
          <td style="FONT: 11px arial; COLOR: dimgray">These will appear at the top of the list when users go to order credit.</td></tr>
        <tr>
          <td><asp:dropdownlist id=CRA0 onchange="onCreditReportProtocolChange(this, 0);" runat="server" Width="310px"></asp:dropdownlist><asp:checkbox id=IsPdfViewNeeded0 runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick=removeCRA(0); type=button value=Clear></td></tr>
        <tr>
          <td><asp:dropdownlist id=CRA1 onchange="onCreditReportProtocolChange(this, 1);" runat="server" Width="310px"></asp:dropdownlist><asp:checkbox id=IsPdfViewNeeded1 runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick=removeCRA(1); type=button value=Clear></td></tr>
        <tr>
          <td><asp:dropdownlist id=CRA2 onchange="onCreditReportProtocolChange(this, 2);" runat="server" Width="310px"></asp:dropdownlist><asp:checkbox id=IsPdfViewNeeded2 runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick=removeCRA(2); type=button value=Clear></td></tr>
        <tr>
          <td><asp:dropdownlist id=CRA3 onchange="onCreditReportProtocolChange(this, 3);" runat="server" Width="310px"></asp:dropdownlist><asp:checkbox id=IsPdfViewNeeded3 runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick=removeCRA(3); type=button value=Clear></td></tr>
        <tr>
          <td><asp:dropdownlist id=CRA4 onchange="onCreditReportProtocolChange(this, 4);" runat="server" Width="310px"></asp:dropdownlist><asp:checkbox id=IsPdfViewNeeded4 runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick=removeCRA(4); type=button value=Clear></td></tr>
        <tr>
          <td><asp:dropdownlist id=CRA5 onchange="onCreditReportProtocolChange(this, 5);" runat="server" Width="310px"></asp:dropdownlist><asp:checkbox id=IsPdfViewNeeded5 runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick=removeCRA(5); type=button value=Clear></td></tr>
        <tr>
          <td><asp:dropdownlist id=CRA6 onchange="onCreditReportProtocolChange(this, 6);" runat="server" Width="310px"></asp:dropdownlist><asp:checkbox id=IsPdfViewNeeded6 runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick=removeCRA(6); type=button value=Clear></td></tr>
        <tr>
          <td><asp:dropdownlist id=CRA7 onchange="onCreditReportProtocolChange(this, 7);" runat="server" Width="310px"></asp:dropdownlist><asp:checkbox id=IsPdfViewNeeded7 runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick=removeCRA(7); type=button value=Clear></td></tr>
        <tr>
          <td><asp:dropdownlist id=CRA8 onchange="onCreditReportProtocolChange(this, 8);" runat="server" Width="310px"></asp:dropdownlist><asp:checkbox id=IsPdfViewNeeded8 runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick=removeCRA(8); type=button value=Clear></td></tr>
        <tr>
          <td><asp:dropdownlist id=CRA9 onchange="onCreditReportProtocolChange(this, 9);" runat="server" Width="310px"></asp:dropdownlist><asp:checkbox id=IsPdfViewNeeded9 runat="server" Text="Order PDF"></asp:checkbox>&nbsp;&nbsp;<input onclick=removeCRA(9); type=button value=Clear></td></tr></table>
          <br>
          <% if (IsLpeEnabled) { %>
          <input type="button" onclick="return f_hidePmlCra();" value="Hide credit agencies in PriceMyLoan ..." style="FONT-WEIGHT:bold; WIDTH:270px;">
          <br><br>
          <asp:CheckBox ID="m_AllowPmlOrderNewCreditReport" Text="Allow users to order new credit reports in PriceMyLoan" Runat="server"></asp:CheckBox>
		<% } %>
        <table border="0">
        <tr>
          <td class=FieldLabel colSpan=2> DU Login Information Fannie Mae Credit
          </td>
         </tr>
		<tr>
			<td class="FieldLabel">User ID</td>
			<td><asp:textbox id="CreditMornetPlusUserID" runat="server" NoUppercase="True"></asp:textbox></td>
		</tr>
		<tr>
			<td class="FieldLabel">Password</td>
			<td><asp:textbox id="CreditMornetPlusPassword" TextMode="Password" runat="server" NoUppercase="True"></asp:textbox></td>
		</tr>
         <tr> 
             <td colspan=2 width=250px align=left>
                 Don't have a DU account?
                <a href="https://support.lendingqb.com/index.php?/Knowledgebase/Article/View/221" target=_blank title="Help: How do I get a DU Account to access my credit provider?">Click here for help</a>.
             </td>
         </tr>
        <tr>
            <td colspan="2">
                <input type="checkbox" runat="server" id="AllowIndicatingVerbalCreditReportAuthorization" onclick="setSelectVerbalAuthorizationDocumentDisabled();" />
                Allow users to indicate that credit report authorization was received verbally.
                <a id="SelectVerbalAuthorizationDocument" onclick="selectVerbalAuthorizationDocument();">Select Verbal Authorization document</a>
                <span id="VerbalAuthorizationDocumentName">(Using default document)</span>
            </td>
        </tr>
          </table>
    </td>
    </tr>
</table>
