﻿<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="DocMagicConfigurationEditor.aspx.cs" Inherits="LendersOfficeApp.los.admin.DocMagicConfigurationEditor" %>
<%@ Import Namespace="LendersOffice.Common" %> 
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<HTML>
  <HEAD runat="server">
    <title>Document Generation Settings</title>
	<style type="text/css">
		.ConfigurationTable {
            display: block;
		}

		body {
            margin: 0 auto;
            text-align: center;
            line-height: 2em;
            background-color: gainsboro;
		}

        form {
            padding: 1em;
        }

		legend {
            font-weight: bold;
		}

		p {
            padding: 0;
            margin: 0;
            text-align: left;
            margin-left: 3em;
		}

	    span, label {
	        width: 80px;
	        text-align: right;
	    }

		.capture-label label {
            text-align: left;
		}

		.left {
            float: left;
            width: 0;
		}

		.center {
            margin: 0 auto;
            text-align: center;
		}

		.leg {
            padding: 10px;
		}

		.Header {
            font-weight: bold;
            font-size:  8px;
            font-family: Arial, Helvetica, sans-serif;
		}

		.ErrorMessage {
            margin-top: 5px;
            font-weight: bold;
            color: red;
		}

		.paddingleft {
            padding-left: 10px;
		}

		a.disabledLink:hover {
            color:Gray;
		}

		a.disabledLink:link {
            color:Gray;
		}

		td.alignSpansLeft span {
            text-align:left;
		}

        .singleIndent {
            padding-left: 20px;
        }

        .doubleIndent {
            padding-left: 40px;
        }

        .nowrap {
            white-space: nowrap;
        }
        
        .align-left {
            text-align: left;
        }

        .width-200 {
            width: 200px;
        }

        .width-350 {
            width: 350px;
        }

        #docOptionTable tr td {
            text-align:left;
        }

        #CaptureBranchGroupTable {
            width: 100%;
            height: 140px;
            overflow-y: scroll;
        }
	</style>
</head>
	<body>
	<script type="text/javascript"> 
        var docTypePickerLink = gVirtualRoot + "/newlos/ElectronicDocs/DocTypePicker.aspx";
        var selectedDocType;
        var scanHolder;
        var extraQuery = "";

	    function resize(w,h) 
		{
			window.dialogWidth = w + "px";
			window.dialogHeight = h + "px";
        }

        function selectDocType(hiddenFieldId, docTypeSpanId, isESignDocType) {
            var queryString = "?showSystemFolders=true" + extraQuery;
            if (isESignDocType) {
                queryString += '&showESignReset=1';
            }
            showModal(docTypePickerLink + queryString, null, null, null, function(result) {
                    if (result && result.docTypeId && result.docTypeName) { // New doc type selected
                        document.getElementById(hiddenFieldId).value = result.docTypeId;
                        document.getElementById(docTypeSpanId).innerText = result.docTypeName;
                    }
                },
                { hideCloseButton: true });
        }

        function onSplitSelected()
        {
            selectedDocType.style.display = 'none';
            scanHolder.style.display = 'inline';

            var link = document.getElementById('selectDocTypesLink');
            link.className += 'disabledLink';
            link.onclick = function () {
                return false;
            };
        }
        
        function onSingleSelected()
        {
            selectedDocType.style.display = 'inline';
            scanHolder.style.display = 'none';

            var link = document.getElementById('selectDocTypesLink');
            link.className = link.className.replace('disabledLink', '');
            link.onclick = function () {
                selectDocType('m_selectedDocType', 'lSelectedDocTypeSpan'); return false;
            };
        }

        function onAutoSaveOptionClicked()
        {
            var autoSaveSelected = $('#m_AutoSaveGeneratedDocs').is(':checked');
            $('.DisableWhenNotAutoSaving').toggle(autoSaveSelected);

            if (autoSaveSelected)
            {
                var documentCaptureSelected = $('#DocumentCapture').is(':checked');
                $('.CaptureSubOption').toggle(documentCaptureSelected);

                var documentSaveOptionDescriptorText = documentCaptureSelected ? 'backup' : 'default';
                $('#DocumentSaveOptionDescriptor').text(documentSaveOptionDescriptorText);

                var selectingBranchGroups = $('#CaptureBranchGroupsSelected').is(':checked');
                $('#CaptureBranchGroupTableRow').toggle(documentCaptureSelected && selectingBranchGroups);

                var singleDocumentSelected = $('#m_rbSaveSingle').is(':checked');
                if (singleDocumentSelected) {
                    onSingleSelected();
                }

                var splitDocumentsSelected = $('#m_rbSaveSplit').is(':checked');
                if (splitDocumentsSelected) {
                    onSplitSelected();
                }

                $('.DocTypeRow').toggle(singleDocumentSelected || splitDocumentsSelected);
                $('.SplitDocumentsSubOption').toggle(splitDocumentsSelected);
            }
        }
        
        $(document).ready(function($){
            selectedDocType = $('#lSelectedDocTypeSpan')[0];
            scanHolder = $('#scanHolder')[0];
            resize(400,500);
            onAutoSaveOptionClicked();
            ToggleRequireAllBorrowersToReceiveCD();
            ToggleAutoSaveLeSignedDateWhenAnyBorrowerSigns();

            if($('#AllowAutoDocTypes').length > 0)
            {
                // If we're editing a config on our framework, allow users to clear the doc type.
                extraQuery += "&showReset=true";
            }

            function ToggleRequireAllBorrowersToReceiveCD()
            {
                var hasVendors = $('#NumVendors').val() !== '0';
                if (!hasVendors)
                {
                    $('#RequireAllBorrowersToReceiveCD').prop('disabled', true).prop('checked', false);
                }

                var checked = $('#m_AutoSaveLeCdReceivedDate').is(':checked');
                if(checked)
                {
                    $('#RequireAllBorrowersToReceiveCD').prop('disabled', false);
                }
                else
                {
                    $('#RequireAllBorrowersToReceiveCD').prop('disabled', true).prop('checked', false);
                }
            }

            function ToggleAutoSaveLeSignedDateWhenAnyBorrowerSigns()
            {
                var checked = $('#m_AutoSaveLeCdSignedDate').is(':checked');
                if(checked)
                {
                    $('#AutoSaveLeSignedDateWhenAnyBorrowerSigns').prop('disabled', false);
                }
                else
                {
                    $('#AutoSaveLeSignedDateWhenAnyBorrowerSigns').prop('disabled', true).prop('checked', false);
                }
            }

            $('#m_AutoSaveLeCdReceivedDate').change(function()
            {
                ToggleRequireAllBorrowersToReceiveCD();
            });

            $('#m_AutoSaveLeCdSignedDate').change(function()
            {
                ToggleAutoSaveLeSignedDateWhenAnyBorrowerSigns();
            });

            $('.CaptureBranchGroupSelected').on('change', function () {
                var selectedGroupIds = JSON.parse($('#CaptureSelectedBranchGroups').val());

                var groupId = $(this).closest('td').find('input[type=hidden]').val();
                var groupIdSelected = $(this).find('input[type=checkbox]').is(':checked');

                var groupIdIndexInSelectedGroupIds = selectedGroupIds.indexOf(groupId);
                var groupIdAlreadyInArray = groupIdIndexInSelectedGroupIds > -1;

                if (groupIdSelected && !groupIdAlreadyInArray) {
                    selectedGroupIds.push(groupId);
                } else if (!groupIdSelected && groupIdAlreadyInArray) {
                    selectedGroupIds.splice(groupIdIndexInSelectedGroupIds, 1);
                }

                selectedBranchGroupJson = JSON.stringify(selectedGroupIds);
                $('#CaptureSelectedBranchGroups').val(selectedBranchGroupJson);
            });
        });
	</script>
	
    <h4 class="page-header">Document Generation Settings</h4>
    <form id="DocMagicConfigurationEditor" method="post" runat="server">
        <asp:HiddenField ID="m_selectedDocType" runat="server" /> 
        <asp:HiddenField ID="m_selectedESignDocType" runat="server" /> 
        <asp:Repeater runat="server" ID="VendorDetails" OnItemDataBound="AccountDetailsBind">
            <ItemTemplate>
                <div class="FormTableHeader"> 
                    <ML:EncodedLabel ID="VendorName" runat="server" /> Account Details
                    <asp:HiddenField runat="server" ID="VendorId" />
                </div>
                <div style="border: 0 1px ridge  gray 1px gray ridge  1px ridge  gray; padding-bottom: 1em; padding-top:1em;" class="width-350"> 
                <table>
		            <tbody align="center">
	                    <asp:PlaceHolder runat="server" ID="UsernamePanel">
	                        <tr>
	                            <td nowrap="nowrap" style="text-align:right">Username:</td>
                                <td><asp:TextBox Runat="server" ID="m_Username" Width="100px"> </asp:TextBox></td>
                                <td style="text-align:left" nowrap="nowrap">
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="m_Username"
	                                    ErrorMessage="* Required" Width="5px" Display="Dynamic">
	                                </asp:RequiredFieldValidator> 
	                            </td>
	                        </tr>
	                    </asp:PlaceHolder>
	                    <asp:PlaceHolder runat="server" ID="PasswordPanel">
	                        <tr>
	                            <td style="text-align:right">Password:</td>
	                            <td><asp:TextBox Runat="server" ID="m_Password" TextMode="Password"  Width="100px"> </asp:TextBox></td>
		                        <td style="text-align:left" nowrap="nowrap">
	                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="m_Password"
	                                    ErrorMessage="* Required" Width="5px" Display="Dynamic">
	                                </asp:RequiredFieldValidator> 
		                        </td>
	                        </tr>
		                </asp:PlaceHolder>
	                    <asp:PlaceHolder runat="server" ID="CompanyIdPanel">
	                        <tr>
	                            <td nowrap="nowrap" style="text-align:right">Company ID:</td>
                                <td><asp:TextBox Runat="server" ID="m_CompanyId" Width="100px"> </asp:TextBox></td>
                                <td style="text-align:left" nowrap="nowrap">
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="m_CompanyId"
	                                    ErrorMessage="* Required" Width="5px" Display="Dynamic">
	                                </asp:RequiredFieldValidator> 
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="BlockManualFulfillmentPanel">
                            <tr>
                                <td nowrap="nowrap" colspan="3" class="align-left">
                                    <asp:CheckBox runat="server" ID="BlockManualFulfillment" Text="Disable Manual Fulfillment Option" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="EnableAppraisalDeliveryPanel">
                            <tr>
                                <td nowrap="nowrap" colspan="3" class="align-left">
                                    <asp:CheckBox runat="server" ID="EnableAppraisalDelivery" Text="Enable Appraisal Delivery" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="DocMagicExtraSettingsPanel">
                            <tr>
                                <td nowrap="nowrap" colspan="3" class="align-left">
                                     <asp:CheckBox runat="server" ID="m_AllowUserCustomLogin" Text="Allow users to edit personal logins" />
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap" colspan="3" class="align-left">
                                     <asp:CheckBox runat="server" ID="m_AllowInvestorDocMagicPlanCodes" Text="Allow investor specific plan codes" />
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap" colspan="3" class="align-left">
                                     <asp:CheckBox runat="server" ID="m_IsOnlyAllowApprovedDocMagicPlanCodes" Text="Limit plan code mapping to approved codes" />
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap" colspan="3" class="align-left">
                                     <asp:CheckBox runat="server" ID="EnableDsiPrintAndDeliverOption" Text="Enable DSI Print and Deliver Option" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                    </tbody>
                </table>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        
        <asp:PlaceHolder runat="server" ID="OCRSection" Visible="false">

            <div class="FormTableHeader width-350" style="margin-top: 10px; text-align: center">
                OCR -
                <ml:EncodedLiteral runat="server" ID="OCRVendorName"></ml:EncodedLiteral>
            </div>
            <div style="border: 0 1px ridge  gray 1px gray ridge  1px ridge  gray; padding-bottom: 1em; padding-top: 1em; text-align: center" class="width-350">
                <table>
                    <tbody>
                        <tr>
                            <td>Username:</td>
                            <td><asp:TextBox runat="server" ID="ocrUsername" Width="100"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td><asp:TextBox runat="server" ID="ocrPassword" TextMode="Password" Width="100" /> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </asp:PlaceHolder>
	<div class="FormTableHeader" style="margin-top:10px; text-align:center"> 
        Document Options
    </div>
    <div style="border: 0 1px ridge  gray 1px gray ridge  1px ridge  gray; padding-bottom: 1em; padding-top:1em; width:360px; text-align:center" > 
        <table id="docOptionTable">
            <tr>
                <td class="nowrap">
                    <asp:CheckBox runat="server" ID="m_AutoSaveGeneratedDocs" Text="Automatically save generated documents" onclick="onAutoSaveOptionClicked();"/>
                </td>
            </tr>
            <tbody id="DocumentCaptureOptionSection" runat="server">
                <tr class="DisableWhenNotAutoSaving DocumentCaptureOption">
                    <td class="nowrap alignSpansLeft capture-label singleIndent">
                        <asp:CheckBox ID="DocumentCapture" runat="server" Text="Use Document Capture as default" class="width-200" onclick="onAutoSaveOptionClicked();" />
                    </td>
                </tr>
                <tr class="DisableWhenNotAutoSaving CaptureSubOption">
                    <td class="singleIndent FieldLabel">
                        Select branch groups permitted to use Document Capture:
                    </td>
                </tr>
                <tr class="DisableWhenNotAutoSaving CaptureSubOption">
                    <td class="singleIndent">
                        <asp:RadioButton ID="CaptureBranchGroupsAll" GroupName="DocumentCaptureBranchGroups" runat="server" Text="Enable for all branches" onclick="onAutoSaveOptionClicked();" />
                    </td>
                </tr>
                <tr class="DisableWhenNotAutoSaving CaptureSubOption">
                    <td class="singleIndent">
                        <asp:RadioButton ID="CaptureBranchGroupsSelected" GroupName="DocumentCaptureBranchGroups" runat="server" Text="Enable for selected branch groups" onclick="onAutoSaveOptionClicked();" />
                    </td>
                </tr>
                <tr id="CaptureBranchGroupTableRow">
                    <td>
                        <asp:HiddenField ID="CaptureSelectedBranchGroups" runat="server" />
                        <table id="CaptureBranchGroupTable" class="doubleIndent">
                            <asp:Repeater ID="CaptureBranchGroupRepeater" runat="server" OnItemDataBound="CaptureBranchGroupRepeater_ItemDataBound">
                                <HeaderTemplate>
                                    <tr class="GridHeader">
                                        <td>
                                            <!-- Checkbox column, no header content -->
                                        </td>
                                        <td>
                                            Branch Name
                                        </td>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="GridAutoItem">
                                        <td class="nowrap">
                                            <asp:CheckBox runat="server" ID="CaptureBranchGroupSelected" class="CaptureBranchGroupSelected" />
                                            <asp:HiddenField runat="server" ID="CaptureBranchGroupId" />
                                        </td>
                                        <td class="nowrap">
                                            <ML:EncodedLiteral runat="server" ID="CaptureBranchGroupName" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
            </tbody>
            <tbody id="SingleDocumentOptionSection" runat="server">
                <tr class="DisableWhenNotAutoSaving">
                    <td class="FieldLabel singleIndent">
                        Select <span id="DocumentSaveOptionDescriptor"></span> save option:
                    </td>
                </tr>
                <tr class="DisableWhenNotAutoSaving">
                    <td class="alignSpansLeft singleIndent nowrap">
                        <asp:RadioButton ID="m_rbSaveSingle" GroupName="SaveSplitOrSingle" runat="server" Text="Save to EDocs as single file" class="width-200" onclick="onAutoSaveOptionClicked();" />
                    </td>
                </tr>
            </tbody>
            <tbody id="SplitDocumentsOptionSection" runat="server">
                <tr class="DisableWhenNotAutoSaving">
                    <td class="alignSpansLeft singleIndent nowrap">
                        <asp:RadioButton ID="m_rbSaveSplit" GroupName="SaveSplitOrSingle" runat="server" Text="Split documents using barcodes" class="width-200" onclick="onAutoSaveOptionClicked();" />
                    </td>
                </tr>
                <tr class="DisableWhenNotAutoSaving SplitDocumentsSubOption">
                    <td class="doubleIndent">
                        <asp:CheckBox ID="DocMagicSplitESignedDocs" runat="server" Text="Split E-Signed Documents" />
                    </td>
                </tr>
                <tr class="DisableWhenNotAutoSaving SplitDocumentsSubOption">
                    <td class="doubleIndent">
                        <asp:CheckBox ID="DocMagicSplitUnsignedDocs" runat="server" Text="Split Unsigned Documents" />
                    </td>
                </tr>
            </tbody>
            <tr class="DisableWhenNotAutoSaving DocTypeRow">
                <td class="FieldLabel nowrap singleIndent">
                    Select Doc Type:
                </td>
            </tr>
            <tr class="DisableWhenNotAutoSaving DocTypeRow">
                <td class="nowrap singleIndent">
                    <span id="lSelectedDocTypeSpan"><ml:EncodedLiteral ID="m_lSelectedDocType" runat="server"></ml:EncodedLiteral></span> 
                    <span id="scanHolder" style="display:none">-Scan barcodes-</span>
                    <a href="#" onclick="selectDocType('m_selectedDocType', 'lSelectedDocTypeSpan'); return false;" id="selectDocTypesLink">(Select DocType)</a>
                </td>
            </tr>
            <tr class="DisableWhenNotAutoSaving DocTypeRow">
                <td class="nowrap singleIndent FieldLabel">
                    Select Doc Type for ESigned Documents:
                </td>
            </tr>
            <tr class="DisableWhenNotAutoSaving DocTypeRow">
                <td class="nowrap singleIndent">
                    <span id="ESignDocTypeSpan">
                        <ml:EncodedLiteral ID="ESignSelectedDocType" runat="server"></ml:EncodedLiteral>
                    </span>
                    <a href="#" onclick="selectDocType('m_selectedESignDocType', 'ESignDocTypeSpan', true); return false;" id="selectESignDocTypeLink">(Select DocType)</a>
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">
                    <asp:CheckBox runat="server" ID="m_AutoSaveLeCdReceivedDate" Text="Automatically save LE/CD received date" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">
                    <asp:HiddenField runat="server" ID="NumVendors" />
                    <asp:CheckBox runat="server" id="RequireAllBorrowersToReceiveCD" Text="Always require all borrowers to receive CD" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">
                    <asp:CheckBox runat="server" ID="m_AutoSaveLeCdSignedDate" Text="Automatically save LE/CD signed date" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap">
                    <asp:CheckBox runat="server" id="AutoSaveLeSignedDateWhenAnyBorrowerSigns" Text="Use first date any borrower signs an LE package as the signed date" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox runat="server" ID="DefaultIssuedDateToToday" Text="Default issued date to today's date and delivery method for LE/CD rows created through document generation" />
                </td>
            </tr>
	    </table>
	    <p class="ErrorMessage">
            <ml:EncodedLiteral Runat="server" Visible="False" ID="m_ErrorMessage1" EnableViewState="False" ></ml:EncodedLiteral>
        </p>
	</div>
	<p class="leg" style="text-align:center; margin-left:0em;" > 
		<asp:Button runat="server" ID="m_saveImp" Text="Update" OnClick="Update_Click" ></asp:Button>
	</p>
</form>
</body>
</HTML>