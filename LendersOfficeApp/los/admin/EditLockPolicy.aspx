﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditLockPolicy.aspx.cs" Inherits="LendersOfficeApp.los.admin.EditLockPolicy" %>
<%@ Register TagPrefix="ils" TagName="EmployeeRoleChooserLink" Src="../common/EmployeeRoleChooserLink.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="ml" TagName="LockExtensionTable" Src="~/los/admin/LockExtensionTable.ascx" %>
<%@ Register TagPrefix="ml" TagName="ReLockFeeTable" Src="~/los/admin/ReLockFeeTable.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Edit Lock Policy</title>
    <style type="text/css">
        .column1
        {
            width:400px;
        }
        .column2
        {
            width:600px;
        }
    </style>
</head>
<body style="background-color:gainsboro">
    <script type="text/javascript">
        function updateDirtyBit() {
            bIsDirty = true;
            document.getElementById('SaveBtnSeen').disabled = false;
        }
        function clearDirty() {
            bIsDirty = false;
            document.getElementById('SaveBtnSeen').disabled = true;
        }
        
//      **
//      UI CLICK ENABLE/DISABLE
//      **
        function f_onUsingRateExpirationClick() {
            if (!document.getElementById(<%=AspxTools.JsString(m_IsUsingRateSheetExpirationFeature.ClientID)%>))
                return;

            if (document.getElementById(<%=AspxTools.JsString(m_IsUsingRateSheetExpirationFeature.ClientID)%> + '_0').checked == true) {
                f_onUsingLockDeskHoursClick();
                document.getElementById(<%=AspxTools.JsString(m_minReqToLock.ClientID)%>).disabled = false;
                document.getElementById(<%=AspxTools.JsString(m_EnforceLockDeskHourForNormalUser.ClientID)%>).disabled = false;
                document.getElementById(<%=AspxTools.JsString(m_timezone.ClientID)%>).disabled = false;
                
                if(<%=AspxTools.JsBool(EditCommand == "new" || EditCommand == "dup") %>)
                {
                    setDisabledAttr(document.getElementById('m_warningMessages'), true);
                    document.getElementById('m_SaveForWarningMessages').style.display = 'inline';
                }
                else
                {
                    setDisabledAttr(document.getElementById('m_warningMessages'), false);
                    document.getElementById('m_SaveForWarningMessages').style.display = 'none';
                }
            }
            else {
                if (document.getElementById(<%=AspxTools.JsString(m_minReqToLock.ClientID)%>).value == "")
                    document.getElementById(<%=AspxTools.JsString(m_minReqToLock.ClientID)%>).value = 30;
                
                var timezone = document.getElementById(<%=AspxTools.JsString(m_timezone.ClientID)%>).value;
				window.frames[0].onSetDisabled(timezone, true);
				document.getElementById('m_SaveForHolidayWarning').style.display = "none";
				document.getElementById('m_SaveForWarningMessages').style.display = "none";
            }
        }
        function f_onUsingLockDeskHoursClick()
		{
			if(!document.getElementById(<%= AspxTools.JsString(m_EnforceLockDeskHourForNormalUser.ClientID) %>))
				return;
				
			var timezone = document.getElementById(<%=AspxTools.JsString(m_timezone.ClientID)%>).value;
			if(document.getElementById(<%= AspxTools.JsString(m_EnforceLockDeskHourForNormalUser.ClientID) %> + '_0').checked == true)
			{

				
				if(<%= AspxTools.JsBool(EditCommand == "new" || EditCommand == "dup") %>)
				{
				    document.getElementById('m_SaveForHolidayWarning').style.display = "inline";
				    window.frames[0].onSetDisabled(timezone, true);
				}
				else
				{
				    window.frames[0].onSetDisabled(timezone, false);
				}
				    
			}
			else
			{

			    
			    window.frames[0].onSetDisabled(timezone, true);
	            document.getElementById('m_SaveForHolidayWarning').style.display = "none";
			}
		}
		function f_onUsingLockExtensionsClick()
		{
		    if (!document.getElementById(<%=AspxTools.JsString(m_EnableAutoLockExtensions.ClientID)%>))
                return;

            var featureEnabled = document.getElementById(<%=AspxTools.JsString(m_EnableAutoLockExtensions.ClientID)%> + '_0').checked == true;
            document.getElementById(<%=AspxTools.JsString(m_MaxLockExtensions.ClientID)%>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_MaxTotalLockExtensionDays.ClientID)%>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_LockExtensionMarketWorsenPoints.ClientID)%>).disabled = !featureEnabled;
            
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(CompareValidator1.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(CompareValidator2.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(CompareValidator3.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator2.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator3.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator4.ClientID) %>), featureEnabled);
            if(featureEnabled)
                $('.LockExtensionTable').removeAttr('disabled');
            else
            {
                $('.LockExtensionTable').attr('disabled', 'disabled');
                $('.LockExtensionError').css('display', 'none');
            }
        }
		function f_onUsingFloatDownsClick()
		{
		    if (!document.getElementById(<%=AspxTools.JsString(m_EnableAutoFloatDowns.ClientID)%>))
                return;

            var featureEnabled = document.getElementById(<%=AspxTools.JsString(m_EnableAutoFloatDowns.ClientID)%> + '_0').checked == true;
            document.getElementById(<%=AspxTools.JsString(m_MaxFloatDowns.ClientID)%>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_FloatDownMinRateImprovement.ClientID)%>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_FloatDownMinPointImprovement.ClientID)%>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_FloatDownAllowRateOptionsRequiringAdditionalPoints.ClientID)%>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_FloatDownFeeIsPoints.ClientID) %>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_FloatDownFeeIsPercent.ClientID) %>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_FloatDownPointFee.ClientID) %>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_FloatDownPercentFee.ClientID) %>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_FloatDownAllowedAfterLockExtension.ClientID) %>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_FloatDownAllowedAfterReLock.ClientID) %>).disabled = !featureEnabled;
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(CompareValidator4.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(CompareValidator5.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(CompareValidator6.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator5.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator6.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator7.ClientID) %>), featureEnabled);            
            
            if(featureEnabled)
            {
                f_onFloatDownFeeClick();
            }
            else
            {
                document.getElementById(<%=AspxTools.JsString(m_FloatDownFeeIsPoints.ClientID) %>).checked = true;
                document.getElementById(<%=AspxTools.JsString(m_FloatDownFeeIsPercent.ClientID) %>).checked = false;
                ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator11.ClientID) %>), false);            
                ValidatorEnable(document.getElementById(<%=AspxTools.JsString(CompareValidator10.ClientID) %>), false);            
                ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator12.ClientID) %>), false);            
                ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RangeValidator1.ClientID) %>), false);            
            }
		}
		function f_onUsingReLocksClick()
		{
		    if (!document.getElementById(<%=AspxTools.JsString(m_EnableAutoReLocks.ClientID)%>))
                return;

            var featureEnabled = document.getElementById(<%=AspxTools.JsString(m_EnableAutoReLocks.ClientID)%> + '_0').checked == true;
            document.getElementById(<%=AspxTools.JsString(m_MaxReLocks.ClientID)%>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_ReLockWorstCasePricingMaxDays.ClientID)%>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_ReLockWorstCasePricingMaxDays_dup.ClientID)%>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_ReLockMarketPriceReLockFee.ClientID)%>).disabled = !featureEnabled;
            document.getElementById(<%=AspxTools.JsString(m_ReLockRequireSameInvestor.ClientID)%>).disabled = !featureEnabled;
            
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(CompareValidator7.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(CompareValidator8.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(CompareValidator9.ClientID) %>), featureEnabled);            
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator8.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator9.ClientID) %>), featureEnabled);
            ValidatorEnable(document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator10.ClientID) %>), featureEnabled);
            
            if(featureEnabled)
            {
                $('.ReLockFeeTable').removeAttr('disabled');
                organizeTable_RL();
            }
            else
            {
                $('.ReLockFeeTable').attr('disabled', 'disabled');
                $('.ReLockFeeError').css('display', 'none');
            }
		}
		function f_onChangeTimezoneClick()
		{
			var timezone = document.getElementById(<%=AspxTools.JsString(m_timezone.ClientID)%>); 
			document.getElementById(<%=AspxTools.JsString(m_holidayTimezoneLabel.ClientID)%>).innerText = timezone.options[timezone.selectedIndex].text;  
			var isDisabled = 
			    (document.getElementById(<%=AspxTools.JsString(m_IsUsingRateSheetExpirationFeature.ClientID)%> + '_0').checked == false)
			    || (document.getElementById(<%=AspxTools.JsString(m_EnforceLockDeskHourForNormalUser.ClientID)%> + '_0').checked == false)
			    || <%= AspxTools.JsBool(EditCommand == "dup" || EditCommand == "new") %>;
			window.frames[0].onChangeTimezone(timezone.value, isDisabled);
		}
		function f_onFloatDownFeeClick(radio)
		{
		    var radio1 = document.getElementById(<%=AspxTools.JsString(m_FloatDownFeeIsPoints.ClientID) %>);
		    var radio2 = document.getElementById(<%=AspxTools.JsString(m_FloatDownFeeIsPercent.ClientID) %>);
		    var text1 = document.getElementById(<%=AspxTools.JsString(m_FloatDownPointFee.ClientID) %>);
		    var text2 = document.getElementById(<%=AspxTools.JsString(m_FloatDownPercentFee.ClientID) %>);
		    var validator1req = document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator11.ClientID) %>);
		    var validator1format = document.getElementById(<%=AspxTools.JsString(CompareValidator10.ClientID) %>);
		    var validator2req = document.getElementById(<%=AspxTools.JsString(RequiredFieldValidator12.ClientID) %>);
		    var validator2format = document.getElementById(<%=AspxTools.JsString(RangeValidator1.ClientID) %>);
		    
		    if(radio==radio1)
		    {
		        radio1.checked = true;
		        radio2.checked = false;
		    }
		    else if(radio==radio2)
		    {
		        radio1.checked = false;
		        radio2.checked = true;
		    }
		    text1.disabled = !radio1.checked;
		    text2.disabled = !radio2.checked;
		    
		    ValidatorEnable(validator1req, radio1.checked);
		    ValidatorEnable(validator1format, radio1.checked);
		    ValidatorEnable(validator2req, radio2.checked);
		    ValidatorEnable(validator2format, radio2.checked);
		}
		
//      **
//		VALIDATION FUNCTIONS		
//		**
		function ValidateRateImprovement(source, args)
		{
		    var value = document.getElementById(<%= AspxTools.JsString(m_FloatDownMinRateImprovement.ClientID) %>).value;
		    if(value == '')
		    {
		        args.IsValid = true;
		        return;
		    }
		    var rateValue = parseFloat(value.replace('%', ''));
		    args.IsValid = !isNaN(rateValue) && rateValue >= 0;
		    return;
		}
	
        
        
 //     **
 //     HELP DETAILS SHOW/HIDE
 //     **
		function f_showTimezoneDetails(event) 
		{
			var msg = document.getElementById("TimezoneDetails");
			msg.style.display = "";
			msg.style.top = (event.clientY - 10)+ "px";
			msg.style.left = (event.clientX + 10 ) + "px";
		}
		function f_closeTimezoneDetails()
		{
			document.getElementById("TimezoneDetails").style.display = "none";
		}
        function f_showMinReqToLockDetails(event){
			var msg = document.getElementById("MinutesRequiredToLockDetails");
			msg.style.display = "";
			msg.style.top = (event.clientY - 10)+ "px";
			msg.style.left = (event.clientX + 10 ) + "px";
        }
        function f_closeMinReqToLockDetails() 
		{
			document.getElementById("MinutesRequiredToLockDetails").style.display = "none";
		}		
        function f_showWorstCasePricingDetails(event)
        {
			var msg = document.getElementById("WorstCasePricingDetails");
			msg.style.display = "";
			msg.style.top = (event.clientY - 10)+ "px";
			msg.style.left = (event.clientX + 10 ) + "px";
        }
		function f_closeWorstCasePricingDetails()
		{
		    document.getElementById("WorstCasePricingDetails").style.display = "none";
		}
		function onEditWarningMessages()
		{	
			if(document.getElementById(<%= AspxTools.JsString(m_IsUsingRateSheetExpirationFeature.ClientID) %> + '_0').checked == true)
			{
			    showModal('/los/admin/WarningMessagesNew.aspx?policyid='+<%= AspxTools.JsString(PolicyId.ToString()) %>, null, null, null, null, { width: 760, height: 400, hideCloseButton: true });
			}
		}
		
		
		function formatDecimal(control, precision) {
		    if(!precision) precision = 3;
            var pointvalue = parseFloat($(control).val());
            if (!isNaN(pointvalue)) {            
                $(control).val(pointvalue.toFixed(precision));
            }
        }
        
        function atLeastOneOFBBoxChecked()
        {
            var atLeastOneChecked = $("[id$='openForBuisness']:checked").length > 0;
            if(!atLeastOneChecked) 
            {
                $("[id$='openForBuisness']").each(function(i, el) 
                    {
                        el.parentNode.style.backgroundColor = 'red';
                    });
            } 
            $('#openForBusnsWarning').show(!atLeastOneChecked);
            return atLeastOneChecked ;; 
        }
        
        
		function page_save()
		{		    
		    var post = true;
		    var lockExtEnabled = document.getElementById(<%=AspxTools.JsString(m_EnableAutoLockExtensions.ClientID)%> + '_0').checked == true;
		    var reLockEnabled = document.getElementById(<%=AspxTools.JsString(m_EnableAutoReLocks.ClientID)%> + '_0').checked == true;
		    if(lockExtEnabled)
		    {
		        post &= LOCKEXTENSIONS.fn.saveTable();
		        var maxDays = document.getElementById(<%=AspxTools.JsString(m_MaxTotalLockExtensionDays.ClientID) %>).value;
		        post &= LOCKEXTENSIONS.fn.verifyMaxLockExtensionLength(maxDays);
		    }
		    if(reLockEnabled)
		        post &= RELOCKFEES.fn.saveTable();
		        
		    if(post) //No problems with lock extension table or re-lock fee table => try save
		    {
		        if(atLeastOneOFBBoxChecked() && Page_ClientValidate())
		        {
		            $('input').removeAttr("disabled");
		            var saveBtn = document.getElementById(<%= AspxTools.JsString(SaveBtn.ClientID) %>);
		            saveBtn.disabled = false;
		            var x = saveBtn.click();
		            page_load();
		        }
		        else
		        {
		            alert('Please fill out all required fields in order to save.');
		        }    
		    }
		}		
        function page_close() {
		    if ( bIsDirty )
		    {
			    if( confirm( "Close without saving?" ) == false )
				    return;
		    }
		    onClosePopup();
        }
        
        
        
        function page_load() {
            $('#<%= AspxTools.HtmlString(m_ReLockWorstCasePricingMaxDays.ClientID.ToString()) %>').blur(function() {
                $('#<%= AspxTools.HtmlString(m_ReLockWorstCasePricingMaxDays_dup.ClientID.ToString()) %>').val($(this).val());
            });
            
            //load lock extension table from json
            var lockExtensionJson = document.getElementById(<%= AspxTools.JsString(m_LockExtensionTableList.ClientID) %>).value;
            LOCKEXTENSIONS.fn.initTable(lockExtensionJson);
            updateTableSize_LX();
            
            //load re-lock fee table from json
            var reLockJson = document.getElementById(<%= AspxTools.JsString(m_ReLockFeeTableList.ClientID) %>).value;
            RELOCKFEES.fn.initTable(reLockJson);            
            updateTableSize_RL();
            
            f_onUsingRateExpirationClick();
            f_onUsingLockExtensionsClick();
            f_onUsingFloatDownsClick();
            f_onUsingReLocksClick();
	        f_onChangeTimezoneClick();
            
            $('#DaySettings span.lockCb input:not(:checked)').each(function(){
                $(this).closest('tr').find('td.locktimes input, td.locktimes select').attr('disabled', 'disabled');
            });
            
            $('span.lockCb input').click(function(){
                var checked = this.checked,
                    inputs = $(this).closest('tr').find('td.locktimes input, td.locktimes select');
                    
                if (checked) {
                    inputs.removeAttr('disabled');
                }
                else {
                    inputs.attr('disabled', 'disabled');
                }
            });
            
            $('input,select').change(updateDirtyBit);
            clearDirty();
        }
        
        $(window).on('load', page_load);
    </script>
    <h4 class="page-header">Edit Lock Policy</h4>
    <form id="form1" runat="server">
        <div style="padding:8px;">
        <table width="100%" border="0" cellspacing="2" cellpadding="3">
            <asp:PlaceHolder runat="server" ID="ErrorMsgPh"  Visible="false" EnableViewState="false">
                <tr>
                    <td colspan="2" class="FieldLabel" style="color:Red">
                        <ml:EncodedLiteral runat="server" ID="ErrorMsg" EnableViewState="false"></ml:EncodedLiteral>
                    </td>
                </tr>
            </asp:PlaceHolder>
			<tr>
			    <td class="FieldLabel column1">
			        Lock Policy Name
			    </td>
			    <td class="column2">
			        <asp:TextBox runat="server" ID="m_PolicyNm" MaxLength="100"></asp:TextBox>
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="m_PolicyNm" Display="Dynamic" ErrorMessage="Required field"></asp:RequiredFieldValidator>
			        <asp:PlaceHolder runat="server" ID="DuplicatePolicyNmError" Visible="false">
			            <br />
			            <label style="color:Red;font-weight:bold">***This lock policy name already exists. Lock policy name is required to be unique.***</label>
			        </asp:PlaceHolder>
			    </td>
			</tr>
		    <tr>
                <td class="FieldLabel column1">Default Lock Desk</td>
                <td class="column2">
                    <ils:EmployeeRoleChooserLink ID="m_lockDeskChoice" runat="server" Data="None" Text="Inherit From Corporate" Role="LockDesk" Desc="Lock Desk" Mode="Search" Width="80px" GenerateScripts="true">
						            <FixedChoice Data="None" Text="Inherit From Corporate">
							            Inherit From Corporate
						            </FixedChoice><FixedChoice data="assign" Text="assign" >Pick Lock Desk</FixedChoice>
                            </ils:EmployeeRoleChooserLink>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="checkbox" runat="server" id="IsLockDeskEmailDefaultFromForRateLockConfirmation" checked />
                    <span>Default rate lock confirmation 'from' address to default lock desk email</span>
                </td>
            </tr>
            <tr>
				<td class="FormTableHeader" colspan="2">
					Lock Desk Hours
				</td>
			</tr>
			<tr>
                <td class="FieldLabel">
                    Enable Rate Expiration Feature?
                </td>
                <td>
                    <asp:RadioButtonList ID="m_IsUsingRateSheetExpirationFeature" runat="server" RepeatDirection="Horizontal"
                        onclick="f_onUsingRateExpirationClick();">
                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel column1">
                    Enforce Lock Desk and Holiday hours?
                </td>
                <td class="column2">
                    <asp:RadioButtonList ID="m_EnforceLockDeskHourForNormalUser" runat="server" RepeatDirection="Horizontal"
                        onclick="f_onUsingLockDeskHoursClick();">
                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            
            <tr>
                <td class="FieldLabel">
                   Timezone of Corporate Office:
                </td>
                <td>
                    <asp:DropDownList ID="m_timezone" runat="server" onchange="f_onChangeTimezoneClick(); return false;">
                        <asp:ListItem Value="PST" Selected="True">Pacific Time</asp:ListItem>
                        <asp:ListItem Value="MST">Mountain Time</asp:ListItem>
                        <asp:ListItem Value="CST">Central Time</asp:ListItem>
                        <asp:ListItem Value="EST">Eastern Time</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; <a href='#' onclick='f_showTimezoneDetails(event);' />?</a>
                </td>
            </tr>
            <tr>
            
                <td colspan="2">
                    <asp:Repeater runat="server" ID="m_LockDeskHourTableRepeater" OnItemDataBound="LockDeskHourTableRepeater_OnItemDataBound">
                        <HeaderTemplate>
                <table cellpadding="1" cellspacing="1"  id="DaySettings" >
                                <thead>
                                    <tr class="GridHeader">
                                    <th>Day</th>
                                    <th>Open for<br /> Business</th>
                                    <th>Open for <br />Locks</th>
                                    <th>Lock Desk Hours</th>
                                    </tr>
                                </thead>
                                <tbody>
                       </HeaderTemplate>
                       <ItemTemplate>
                            <tr class='<%# AspxTools.HtmlString( Container.ItemIndex % 2 == 0 ? "GridItem" : "GridAlternatingItem")  %>'>
                                <td><ml:EncodedLiteral runat="server" ID="day"></ml:EncodedLiteral> </td>
                                <td align="center" ><asp:CheckBox runat="server" ID="openForBuisness" /></td>
                                <td align="center"><asp:CheckBox runat="server" ID="openForLocks" CssClass="lockCb"  /></td>
                                <td class="locktimes">
                                    <ml:TimeTextBox runat=server id="startTime" ></ml:TimeTextBox>    
                                    
                                    and 
                                    
                                    <ml:TimeTextBox runat="server" ID="endTime" ></ml:TimeTextBox>
                                </td>
                            </tr>
                        </ItemTemplate>
                            <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr id="openForBusnsWarning" style="display: none; FONT: 12px arial; COLOR: red">
                <td>
                    At least 1 day must be marked as open for business
                </td>
            </tr>
            <tr>
                <td class="FieldLabel column1">
                    Hasten investor cutoff by:
                </td>
                <td class="column2">
                    <asp:TextBox ID="m_minReqToLock" runat="server" Width="30px" MaxLength="3"></asp:TextBox>&nbsp;minutes&nbsp;<a
                        href='#"' onclick='f_showMinReqToLockDetails(event);' >?</a> &nbsp;
                    <br />
                    <asp:RangeValidator
                        ID="m_minReqVal" runat="server" Type="Integer" MaximumValue="999" MinimumValue="0"
                        ControlToValidate="m_minReqToLock" Display="Dynamic" ErrorMessage="Please enter a whole number between 0 and 999."></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" colspan="2">
                    <a id="m_warningMessages" href="javascript:void(0);" onclick="if(!hasDisabledAttr(this))onEditWarningMessages();">Warning Messages</a>&nbsp;<label id="m_SaveForWarningMessages" style="display:none;color:Red;font-weight:normal">Save lock policy before editing warning messages.</label>
                </td>
            </tr>
            <tr style="padding-top: 20px">
                <td class="FieldLabel" colspan="2">
                   Holiday Closures (<ml:EncodedLabel ID="m_holidayTimezoneLabel" runat="server">PT</ml:EncodedLabel>): <label id="m_SaveForHolidayWarning" style="display:none;color:Red;font-weight:normal">Save lock policy before editing holidays.</label>
                </td>
            </tr>
            <tr>
                <td colspan="2" height="200">
                    <iframe frameborder="0" scrolling="no" id="holidayClosureNewFrame"
                    src="HolidayLockDeskClosuresNew.aspx?policyid=<%=AspxTools.HtmlString(PolicyId.ToString()) %>&disabled=<%=AspxTools.JsBool(EditCommand=="dup" || EditCommand == "new") %>"
                        width="100%" height="100%" ></iframe>
                </td>
            </tr>
            <tr>
				<td class="FormTableHeader" colspan="2">
					Lock Extensions
				</td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
			        Automatically Approve Lock Extensions?
			    </td>
			    <td class="column2">
                    <asp:RadioButtonList ID="m_EnableAutoLockExtensions" runat="server" RepeatDirection="Horizontal"
                        onclick="f_onUsingLockExtensionsClick();">
                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                    </asp:RadioButtonList>
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
			        Maximum Number of Lock Extensions per File:
			    </td>
			    <td class="column2">
                    <asp:TextBox runat="server" ID="m_MaxLockExtensions" Width="30px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="m_MaxLockExtensions" Display="Dynamic" ErrorMessage="Required field"></asp:RequiredFieldValidator>
                    <asp:CompareValidator
                            ID="CompareValidator1" runat="server" Type="Integer" ValueToCompare="1" Operator="GreaterThanEqual"
                            ControlToValidate="m_MaxLockExtensions" Display="Dynamic" ErrorMessage="Must be a positive whole number"></asp:CompareValidator>
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
			        Maximum Total Extension From Original Lock:
			    </td>
			    <td class="column2">
                    <asp:TextBox runat="server" ID="m_MaxTotalLockExtensionDays" Width="30px"></asp:TextBox> days
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="m_MaxTotalLockExtensionDays" Display="Dynamic" ErrorMessage="Required field"></asp:RequiredFieldValidator>
                    <asp:CompareValidator
                            ID="CompareValidator2" runat="server" Type="Integer" ValueToCompare="1" Operator="GreaterThanEqual"
                            ControlToValidate="m_MaxTotalLockExtensionDays" Display="Dynamic" ErrorMessage="Must be a positive whole number"></asp:CompareValidator>
			    </td>
			</tr>
			<tr>
			    <td colspan="2">
			        <asp:HiddenField runat="server" ID="m_LockExtensionTableList" />    
			        <ml:LockExtensionTable runat="server" ID="m_LockExtensionTable" />
			    </td>
			</tr>
			<tr>
			    <td colspan="2" style="font-size:10px">
		            *Improving Market is when the market price at time of extension is better than the market price at original submission.<br />
		            **Static Market is when the market price at time of extension is equal to the market price at original submission.<br />
		            ***Worsening Market is when the market price at time of extension is worse than the market price at original submission.
		        </td>
			</tr>
			<tr>
			    <td colspan="2">
			        If market has worsened by more than <asp:TextBox runat="server" ID="m_LockExtensionMarketWorsenPoints" Width="50px" onblur="formatDecimal(this, 3);"></asp:TextBox>
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="m_LockExtensionMarketWorsenPoints" Display="Dynamic" ErrorMessage=" (Required field)"></asp:RequiredFieldValidator>
			        <asp:CompareValidator
                            ID="CompareValidator3" runat="server" Type="Double" ValueToCompare="0" Operator="GreaterThan"
                            ControlToValidate="m_LockExtensionMarketWorsenPoints" Display="Dynamic" ErrorMessage="(Must be a positive number)"></asp:CompareValidator>
                    points then re-lock at market price and add the remaining days from the maximum total extension days to the current lock expiration date.
			        
			    </td>
			</tr>
			<tr>
				<td class="FormTableHeader" colspan="2">
					Float Downs
				</td>
			</tr>
			<tr>
				<td class="FieldLabel column1">
					Automatically Approve Float Downs?
				</td>
				<td class="column2">
				    <asp:RadioButtonList ID="m_EnableAutoFloatDowns" runat="server" RepeatDirection="Horizontal"
                        onclick="f_onUsingFloatDownsClick();">
                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                    </asp:RadioButtonList>
				</td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
			        Allow Float Downs after a Lock Extension?
			    </td>
			    <td class="column2">
			        <asp:RadioButtonList ID="m_FloatDownAllowedAfterLockExtension" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                    </asp:RadioButtonList>
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
			        Allow Float Downs after a Rate Re-Lock?
			    </td>
			    <td class="column2">
			        <asp:RadioButtonList ID="m_FloatDownAllowedAfterReLock" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                    </asp:RadioButtonList>
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
			        Maximum Number of Float Downs Per Loan File:
			    </td>
			    <td class="column2">
			        <asp:TextBox runat="server" ID="m_MaxFloatDowns" Width="30px"></asp:TextBox>
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="m_MaxFloatDowns" Display="Dynamic" ErrorMessage="Required field"></asp:RequiredFieldValidator>
			        <asp:CompareValidator
                            ID="CompareValidator4" runat="server" Type="Integer" ValueToCompare="1" Operator="GreaterThanEqual"
                            ControlToValidate="m_MaxFloatDowns" Display="Dynamic" ErrorMessage="Must be a positive whole number"></asp:CompareValidator>
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
			        Float Down Fee:
			    </td>
			    <td class="column2">
			        <asp:RadioButton runat="server" ID="m_FloatDownFeeIsPoints" onclick="f_onFloatDownFeeClick(this);" /> &nbsp; <asp:TextBox runat="server" ID="m_FloatDownPointFee" onblur="formatDecimal(this, 3)" Width="50px"></asp:TextBox> points
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="m_FloatDownPointFee" Display="Dynamic" ErrorMessage="Required field"></asp:RequiredFieldValidator>
			        <asp:CompareValidator
                            ID="CompareValidator10" runat="server" Type="Double" ValueToCompare="0" Operator="GreaterThanEqual"
                            ControlToValidate="m_FloatDownPointFee" Display="Dynamic" ErrorMessage="Must be a nonnegative number"></asp:CompareValidator>
			        <br />
			        <asp:RadioButton runat="server" ID="m_FloatDownFeeIsPercent" onclick="f_onFloatDownFeeClick(this);" /> &nbsp; <asp:TextBox runat="server" ID="m_FloatDownPercentFee" onblur="formatDecimal(this, 1)" Width="50px"></asp:TextBox>% of market price improvement
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="m_FloatDownPercentFee" Display="Dynamic" ErrorMessage="Required field"></asp:RequiredFieldValidator>
			        <asp:RangeValidator
                        ID="RangeValidator1" runat="server" Type="Double" MaximumValue="100" MinimumValue="0"
                        ControlToValidate="m_FloatDownPercentFee" Display="Dynamic" ErrorMessage="Please enter a number between 0.0 and 100.0"></asp:RangeValidator>
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
			        Minimum Rate Improvement:
			    </td>
			    <td class="column2">
			        <ml:PercentTextBox runat="server" ID="m_FloatDownMinRateImprovement" Width="50px"></ml:PercentTextBox>
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="m_FloatDownMinRateImprovement" Display="Dynamic" ErrorMessage="Required field"></asp:RequiredFieldValidator>
			        <asp:CustomValidator
                            ID="CompareValidator5" runat="server" ClientValidationFunction="ValidateRateImprovement"
                            ControlToValidate="m_FloatDownMinRateImprovement" Display="Dynamic" ErrorMessage="Must be a nonnegative number"></asp:CustomValidator>
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
			        Minimum Market Price Improvement Per Rate Option:
			    </td>
			    <td class="column2">
			        <asp:TextBox runat="server" ID="m_FloatDownMinPointImprovement" Width="50px" onblur="formatDecimal(this, 3);"></asp:TextBox> points
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="m_FloatDownMinPointImprovement" Display="Dynamic" ErrorMessage="Required field"></asp:RequiredFieldValidator>
			        <asp:CompareValidator
                            ID="CompareValidator6" runat="server" Type="Double" ValueToCompare="0" Operator="GreaterThanEqual"
                            ControlToValidate="m_FloatDownMinPointImprovement" Display="Dynamic" ErrorMessage="Must be a nonnegative number"></asp:CompareValidator>
			    </td>
			</tr>
			<tr>
				<td class="FieldLabel column1">
					Allow Float Down to Rate Options Requiring Borrower to Pay Additional Points (or receive less credit)?
				</td>
				<td class="column2">
				    <asp:RadioButtonList ID="m_FloatDownAllowRateOptionsRequiringAdditionalPoints" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                    </asp:RadioButtonList>
				</td>
			</tr>
			<tr>
				<td class="FormTableHeader" colspan="2">
					Rate Re-Lock (for currently expired/canceled locks)
				</td>
			</tr>
			<tr>
				<td class="FieldLabel column1">
					Automatically Approve Rate Re-Locks?
				</td>
				<td class="column2">
				    <asp:RadioButtonList ID="m_EnableAutoReLocks" runat="server" RepeatDirection="Horizontal"
                        onclick="f_onUsingReLocksClick();">
                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                    </asp:RadioButtonList>
				</td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
			        Maximum Number of Rate Re-Locks Per Loan File:
			    </td>
			    <td class="column2">
			        <asp:TextBox runat="server" ID="m_MaxReLocks" Width="30px"></asp:TextBox>
			        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="m_MaxReLocks" Display="Dynamic" ErrorMessage="Required field"></asp:RequiredFieldValidator>
			        <asp:CompareValidator
                            ID="CompareValidator9" runat="server" Type="Integer" ValueToCompare="1" Operator="GreaterThanEqual"
                            ControlToValidate="m_MaxReLocks" Display="Dynamic" ErrorMessage="Must be a positive whole number"></asp:CompareValidator>
			    </td>
			</tr>
			<tr>
			    <td colspan="2">
			        For locks which have expired within the past <asp:TextBox runat="server" ID="m_ReLockWorstCasePricingMaxDays" Width="30px"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="m_ReLockWorstCasePricingMaxDays" Display="Dynamic" ErrorMessage=" (Required field)"></asp:RequiredFieldValidator>
			        <asp:CompareValidator
                            ID="CompareValidator7" runat="server" Type="Integer" ValueToCompare="1" Operator="GreaterThanEqual"
                            ControlToValidate="m_ReLockWorstCasePricingMaxDays" Display="Dynamic" ErrorMessage="(Must be a positive whole number)"></asp:CompareValidator> days, rate re-locks are eligible, subject to <a href="javascript:void(0);" onclick="f_showWorstCasePricingDetails(event)">worst case pricing</a>, and subject to the following fee table:
                </td>
			</tr>
			<tr>
			    <td colspan="2">
			        <asp:HiddenField runat="server" ID="m_ReLockFeeTableList" /> 
			        <ml:ReLockFeeTable runat="server" id="m_ReLockFeeTable"></ml:ReLockFeeTable>
			    </td>
			</tr>
			<tr>
			    <td colspan="2">
			        For locks which have been expired more than <asp:TextBox runat="server" ID="m_ReLockWorstCasePricingMaxDays_dup" Width="30px" ReadOnly></asp:TextBox> days, rate re-locks are eligible at the current market pricing, and are subject to a <asp:TextBox runat="server" ID="m_ReLockMarketPriceReLockFee" Width="50px" onblur="formatDecimal(this, 3);"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="m_ReLockMarketPriceReLockFee" Display="Dynamic" ErrorMessage=" (Required field)"></asp:RequiredFieldValidator>
			        <asp:CompareValidator
                            ID="CompareValidator8" runat="server" Type="Double" ValueToCompare="0" Operator="GreaterThanEqual"
                            ControlToValidate="m_ReLockMarketPriceReLockFee" Display="Dynamic" ErrorMessage="(Must be a nonnegative number)"></asp:CompareValidator> point re-lock fee.
			    </td>
			</tr>
			<tr>
			    <td class="FieldLabel column1">
					Require Re-Locks to be with same Investor as in original submission?
				</td>
				<td class="column2">
				    <asp:RadioButtonList ID="m_ReLockRequireSameInvestor" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                        <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                    </asp:RadioButtonList>
				</td>
			</tr>
			<tr>
			    <td colspan="2" class="FieldLabel" align="center">
			        <label style="color:Red;">***These policies do not apply to change-of-circumstance, or change of loan product type/term.***</label>
			    </td>
			</tr>
			<tr>
			    <td colspan="2" align="center">
			        <asp:Button ID="SaveBtn" runat="server" Text="" OnClick="OnSave" Enabled="false" style="display:none;" />
			        <input type="button" ID="SaveBtnSeen" value="Save" onclick="page_save();" />
			        <input type="button" ID="CloseBtn" value="Close" onclick="page_close();" />
			    </td>
			</tr>
		</table>
		</div>
		<div id="MinutesRequiredToLockDetails" style="border-right: black 3px solid; padding-right: 5px;
        border-top: black 3px solid; display: none; padding-left: 5px; padding-bottom: 5px;
        border-left: black 3px solid; width: 330px; padding-top: 5px; border-bottom: black 3px solid;
        position: fixed; height: 73px; background-color: whitesmoke">
        <table width="100%">
            <tr>
                <td>
                    Number of minutes to subtract from Investor cut-off time.&nbsp; Lock requests will
                    not be allowed if pricing time (pre-approval certificate time) is later than the
                    investor cut-off time minus this buffer.
                </td>
            </tr>
            <tr>
                <td align="center">
                    [ <a href="javascript:void(0);" onclick="f_closeMinReqToLockDetails();">Close</a> ]
                </td>
            </tr>
        </table>
    </div>
    <div id="TimezoneDetails" style="border-right: black 3px solid; padding-right: 5px;
        border-top: black 3px solid; display: none; padding-left: 5px; padding-bottom: 5px;
        border-left: black 3px solid; width: 330px; padding-top: 5px; border-bottom: black 3px solid;
        position: fixed; height: 170px; background-color: whitesmoke">
        <table width="100%">
            <tr>
                <td>
                    Select the timezone your corporate office is located in.
                    <br/>
                    <br/>
                    This is used to determine your lock desk hours.
                    <br/>
                    <br/>
                    This is also used to determine the investor cut-off time, if the investor's cut-off
                    is subject to seller timezone.<br/>
                    <br/>
                    Example: Investor A's cut-off is 5PM ET for sellers on the east coast, and 5PM
                    PT for sellers on the west coast.
                </td>
            </tr>
            <tr>
                <td align="center">
                    [ <a href="javascript:void(0);" onclick="f_closeTimezoneDetails();">Close</a> ]
                </td>
            </tr>
        </table>
    </div>
    <div id="WorstCasePricingDetails" style="border-right: black 3px solid; padding-right: 5px;
        border-top: black 3px solid; display: none; padding-left: 5px; padding-bottom: 5px;
        border-left: black 3px solid; width: 330px; padding-top: 5px; border-bottom: black 3px solid;
        position: fixed; height: 73px; background-color: whitesmoke">
        <table width="100%">
            <tr>
                <td>
                    Worse case pricing is calculated by comparing pricing from the original lock date to current pricing, for the same loan program, and then selecting the higher of the two.
                </td>
            </tr>
            <tr>
                <td align="center">
                    [ <a href="javascript:void(0);" onclick="f_closeWorstCasePricingDetails();">Close</a> ]
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
