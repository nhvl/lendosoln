﻿#region Generated Code
namespace LendersOfficeApp.los
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.CustomFavoriteFolder;
    using LendersOffice.Security;
    using DataAccess;

    /// <summary>
    /// The purpose of this service is to Save changes to both the User and Admin custom folder configurations.
    /// </summary>
    public partial class CustomFolderNavigationService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Save's the folder configuration and landing page if applicable.
        /// </summary>
        public void SaveTree()
        {
            string treeJSON = GetString("treeJSON");
            bool isAdmin = GetBool("isAdmin");
            string landingPageId = GetString("landingPageId");

            var treeRoot = SerializationHelper.JsonNetDeserializeWithTypeHandling<CustomFavoriteNode>(treeJSON);
            SaveTree(treeRoot, isAdmin, landingPageId);
        }

        /// <summary>
        /// Save's the folder configuration and landing page if applicable.
        /// </summary>
        /// <param name="treeRoot">The root node of the folder configuration.</param>
        /// <param name="isAdmin">Determines whether the user is modifying the page for the Broker configuration if true.  If false, it means the user is modifying his personal configuration.</param>
        /// <param name="landingPageId">The ID of the page that the loan will open to for the user.</param>
        public void SaveTree(CustomFavoriteNode treeRoot, bool isAdmin, string landingPageId)
        {
            if (isAdmin && !PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowViewingAndEditingCustomNavigation))
            {
                string developerMessage = "The user " + PrincipalFactory.CurrentPrincipal.LoginNm +
                    $" attempted to update the Custom Folder Navigation settings for broker {PrincipalFactory.CurrentPrincipal.BrokerId} without the permission 'AllowViewingAndEditingCustomNavigation'.";

                throw new CBaseException(
                    "You do not have permission to update the Custom Folder Navigation settings for this broker.",
                    developerMessage);
            }

            treeRoot.SortOrder = isAdmin ? 1 : 0;
            CustomFavoriteFolderHelper.SaveCustomFavoriteFolderTree(
                PrincipalFactory.CurrentPrincipal.BrokerId,
                isAdmin ? null as Guid? : PrincipalFactory.CurrentPrincipal.EmployeeId,
                treeRoot as CustomFavoriteFolderNode);

            if (!isAdmin)
            {
                LendersOffice.Admin.EmployeeDB.SaveLandingPageId(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId, landingPageId);
            }

            this.SetResult("OK", true);
        }

        /// <summary>
        /// Save's the user's page configuration for the top-level page items only.
        /// </summary>
        public void UpdateFavorites()
        {
            int folderId = GetInt("folderId");
            string pageId = GetString("pageId");
            string command = GetString("Command");

            var userFolder = CustomFavoriteFolderHelper.RetrieveUserFavoriteFolder(folderId, PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId);

            if (folderId == -1 && userFolder == null)
            {
                throw new CBaseException("Could not update the folder.  Please try again.", $"The employee {PrincipalFactory.CurrentPrincipal.EmployeeId} ");
            }
            else if (folderId == -1 && userFolder.Folder.FolderId == -1)
            {
                SaveTree(userFolder, false, string.Empty);
            }
            else
            {
                int index = 0;
                CustomFavoritePageNode page;
                switch (command)
                {
                    case "add":
                        index = userFolder.Children.FindIndex(child => child is CustomFavoriteFolderNode);
                        index = index == -1 ? userFolder.Children.Count : index;
                        page = new CustomFavoritePageNode(pageId, index);
                        userFolder.Children.Add(page);
                        break;
                    case "remove":
                        userFolder.Children = userFolder.Children.Where(child => child is CustomFavoriteFolderNode || (child as CustomFavoritePageNode).PageId != pageId).ToList();
                        userFolder.Children.ForEach(child => child.SortOrder = index++);
                        break;
                    case "drag":
                        string pagesJSON = GetString("pagesJSON");
                        var pages = SerializationHelper.JsonNetDeserialize<List<CustomFavoritePageNode>>(pagesJSON);

                        for (int i = 0; i < pages.Count; i++)
                        {
                            page = pages[i];
                            var childPage = userFolder.Children.FirstOrDefault(child => child is CustomFavoritePageNode && (child as CustomFavoritePageNode).PageId == page.PageId);
                            if (childPage != null)
                            {
                                childPage.SortOrder = i;
                            }
                        }

                        break;
                    default:
                        throw new CBaseException("Failed to update the favorites.", $"A user with employee id {PrincipalFactory.CurrentPrincipal.EmployeeId} attempted to save favorites with the unkown command '{command}'");
                }

                var childPages = userFolder.Children.Where(child => child is CustomFavoritePageNode).Cast<CustomFavoritePageNode>().ToList();
                CustomFavoriteFolderHelper.UpdatePagesInAFolder(PrincipalFactory.CurrentPrincipal.BrokerId, folderId == -1 ? userFolder.Folder.FolderId : folderId, childPages);
                this.SetResult("OK", true);
            }
        }

        /// <summary>
        /// Accepts a method name and calls the matching function.
        /// </summary>
        /// <param name="methodName">The name of the method to call.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SaveTree":
                    SaveTree();
                    break;
                case "UpdateFavorites":
                    UpdateFavorites();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "The method " + methodName + " is not a valid function for CustomFolderNavigationService.aspx.cs");
            }
        }
    }
}