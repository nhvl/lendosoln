﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LockExtensionTable.ascx.cs" Inherits="LendersOfficeApp.los.admin.LockExtensionTable" %>
<head>
    <script type="text/javascript">
        if (!this.LOCKEXTENSIONS) {
            this.LOCKEXTENSIONS = ({});

            LOCKEXTENSIONS.fn = new Object();
            LOCKEXTENSIONS.List = [];
            LOCKEXTENSIONS.Index = 0; // largest used ID + 1
            LOCKEXTENSIONS.Edit = []; // list of IDs for rows in EDIT mode
            LOCKEXTENSIONS.Deleted = new Object(); // list of IDs of all deleted rows

            LOCKEXTENSIONS.fn.addRow = function() {
                var oTbody = document.getElementById("m_LockExtensionTableBody");
                var nRowCount = oTbody.rows.length;
                var oRow = LOCKEXTENSIONS.fn.__CreateRow(oTbody, '', '', '', '');
                if (typeof updateDirtyBit === 'function') {
                    updateDirtyBit();
                }
                updateTableSize_LX();
            }
            LOCKEXTENSIONS.fn.modifyRow = function(id) {
                var link1 = document.getElementById('sModifyLink_LX' + id);
                var link2 = document.getElementById('sUpdateLink_LX' + id);
                if (hasDisabledAttr(link1))
                    return;
                for (i = 0; i < LOCKEXTENSIONS.Edit.length; i++) {
                    if (LOCKEXTENSIONS.Edit[i] == id) {
                        //edit mode cancel

                        LOCKEXTENSIONS.Edit.splice(i, 1); //remove from edit set

                        var sDays = document.getElementById('sDays_LX' + id);
                        var sDays_edit = document.getElementById('sDays_LX' + id + "_edit");
                        var sImproving = document.getElementById('sImproving_LX' + id);
                        var sImproving_edit = document.getElementById('sImproving_LX' + id + "_edit");
                        var sStatic = document.getElementById('sStatic_LX' + id);
                        var sStatic_edit = document.getElementById('sStatic_LX' + id + "_edit");
                        var sWorsening = document.getElementById('sWorsening_LX' + id);
                        var sWorsening_edit = document.getElementById('sWorsening_LX' + id + "_edit");

                        //cancelling new row, so skip to delete
                        if (sDays.innerText == '' && sImproving.innerText == '' && sStatic.innerText == '' && sWorsening.innerText == '')
                            break;

                        sDays.style.display = "inline";
                        sDays_edit.style.display = "none";
                        sDays_edit.value = sDays.innerText;

                        sImproving.style.display = "inline";
                        sImproving_edit.style.display = "none";
                        sImproving_edit.value = sImproving.innerText;

                        sStatic.style.display = "inline";
                        sStatic_edit.style.display = "none";
                        sStatic_edit.value = sStatic.innerText;

                        sWorsening.style.display = "inline";
                        sWorsening_edit.style.display = "none";
                        sWorsening_edit.value = sWorsening.innerText;

                        link1.innerText = 'delete';
                        link2.innerText = 'edit';

                        //Turn off error messages
                        document.getElementById('m_daysError_LX' + id).style.display = 'none';
                        document.getElementById('m_improvingError_LX' + id).style.display = 'none';
                        document.getElementById('m_staticError_LX' + id).style.display = 'none';
                        document.getElementById('m_worseningError_LX' + id).style.display = 'none';

                        return;
                    }
                }
                //row wasn't in edit mode; link = delete
                var oRow = document.getElementById('row_LX' + id);
                var iRowIndex = oRow.rowIndex;
                var oTbody = document.getElementById('m_LockExtensionTableBody');
                oTbody.deleteRow(iRowIndex - 1);
                for (var i = iRowIndex - 1; i < oTbody.rows.length; i++) {
                    oTbody.rows[i].className = ((i % 2) == 0) ? "GridItem" : "GridAlternatingItem";
                }
                if (typeof updateDirtyBit === 'function') {
                    updateDirtyBit();
                }
                LOCKEXTENSIONS.Deleted[id] = true;
                updateTableSize_LX();
            }

            LOCKEXTENSIONS.fn.updateRow = function(id) {

                var link1 = document.getElementById('sModifyLink_LX' + id);
                var link2 = document.getElementById('sUpdateLink_LX' + id);
                if (hasDisabledAttr(link2))
                    return;
                var sDays = document.getElementById('sDays_LX' + id);
                var sDays_edit = document.getElementById('sDays_LX' + id + "_edit");
                var sImproving = document.getElementById('sImproving_LX' + id);
                var sImproving_edit = document.getElementById('sImproving_LX' + id + "_edit");
                var sStatic = document.getElementById('sStatic_LX' + id);
                var sStatic_edit = document.getElementById('sStatic_LX' + id + "_edit");
                var sWorsening = document.getElementById('sWorsening_LX' + id);
                var sWorsening_edit = document.getElementById('sWorsening_LX' + id + "_edit");
                for (i = 0; i < LOCKEXTENSIONS.Edit.length; i++) {
                    if (LOCKEXTENSIONS.Edit[i] == id) {
                        //edit mode finish
                        if (validateRow_LX(id)) {
                            sDays.style.display = "inline";
                            sDays_edit.style.display = "none";
                            sDays.innerText = sDays_edit.value;

                            sImproving.style.display = "inline";
                            sImproving_edit.style.display = "none";
                            sImproving.innerText = sImproving_edit.value;

                            sStatic.style.display = "inline";
                            sStatic_edit.style.display = "none";
                            sStatic.innerText = sStatic_edit.value;

                            sWorsening.style.display = "inline";
                            sWorsening_edit.style.display = "none";
                            sWorsening.innerText = sWorsening_edit.value;

                            LOCKEXTENSIONS.Edit.splice(i, 1); //remove from edit set

                            if (typeof updateDirtyBit === 'function') {
                                updateDirtyBit();
                            }

                            link1.innerText = 'delete';
                            link2.innerText = 'edit';
                        }
                        return;
                    }
                }
                //row wasn't in edit mode; enter edit mode
                sDays.style.display = "none";
                sDays_edit.style.display = "inline";

                sImproving.style.display = "none";
                sImproving_edit.style.display = "inline";

                sStatic.style.display = "none";
                sStatic_edit.style.display = "inline";

                sWorsening.style.display = "none";
                sWorsening_edit.style.display = "inline";

                LOCKEXTENSIONS.Edit.push(id);

                link1.innerText = 'cancel';
                link2.innerText = 'update';
            }


            LOCKEXTENSIONS.fn.initTable = function(jsondata) {
                LOCKEXTENSIONS.fn.clearTable();
                try {
                    LOCKEXTENSIONS.List = JSON.parse(jsondata);

                    var oTbody = document.getElementById("m_LockExtensionTableBody");
                    var nRowCount = LOCKEXTENSIONS.List.length;
                    for (var i = 0; i < nRowCount; i++) {
                        var oEntry = LOCKEXTENSIONS.List[i];
                        LOCKEXTENSIONS.fn.__CreateRow(oTbody, oEntry[0], oEntry[1], oEntry[2], oEntry[3]);
                    }
                }
                catch (e) {  } //jsondata was empty, avoid parse error
            }

            LOCKEXTENSIONS.fn.clearTable = function() {
                var oTbody = document.getElementById("m_LockExtensionTableBody");
                var oHidden = document.getElementById('m_LockExtensionTableList');
                try {
                    if (oHidden != null) {
                        oHidden.value = '';
                    }
                    var nRowCount = oTbody.rows.length;
                    for (var i = 0; i < nRowCount; i++) {
                        oTbody.deleteRow(0);
                    }
                    LOCKEXTENSIONS.Index = 0;
                } catch (e) { }
            }

            LOCKEXTENSIONS.fn.__CreateRow = function(oParent, sDays, sImproving, sStatic, sWorsening) {
                var oRow = LOCKEXTENSIONS.fn.__CreateAndAppend("tr", oParent);
                var oCell = null;
                var o;
                var id = LOCKEXTENSIONS.Index++;
                oRow.id = 'row_LX' + id;
                var isNew = sDays == '' && sImproving == '' && sStatic == '' && sWorsening == '';
                if (isNew)
                    LOCKEXTENSIONS.Edit.push(id); //automatically enter edit mode for new rows
                oRow.className = ((oParent.rows.length % 2) == 0) ? "GridAlternatingItem" : "GridItem";

                //Extension (Days)
                oCell = LOCKEXTENSIONS.fn.__CreateAndAppend("td", oRow);
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("label", oCell);
                o.id = 'sDays_LX' + id;
                o.innerText = sDays;
                if (isNew) o.style.display = "none";
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("input", oCell);
                o.id = 'sDays_LX' + id + "_edit";
                o.className = "LockExtensionTable";
                o.type = 'text';
                o.style.width = '50px';
                o.maxLength = '5';
                o.value = sDays;
                addEvent(o, 'blur', function() {
                    return validateDays_LX(id);
                });
                if (!isNew) o.style.display = "none";
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("img", oCell);
                o.id = 'm_daysError_LX' + id;
                o.src = '../../images/error_icon.gif';
                o.className = 'LockExtensionError';
                o.style.display = 'none';

                //Fee (Points) in improving market
                oCell = LOCKEXTENSIONS.fn.__CreateAndAppend("td", oRow);
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("label", oCell);
                o.id = 'sImproving_LX' + id;
                o.innerText = sImproving;
                if (isNew) o.style.display = "none";
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("input", oCell);
                o.id = 'sImproving_LX' + id + "_edit";
                o.className = "LockExtensionTable";
                o.type = 'text';
                o.style.width = '50px';
                o.maxLength = '9';
                o.value = sImproving;
                addEvent(o, 'blur', function() {
                    return validateImproving_LX(id);
                });
                if (!isNew) o.style.display = "none";
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("img", oCell);
                o.id = 'm_improvingError_LX' + id;
                o.src = '../../images/error_icon.gif';
                o.className = 'LockExtensionError';
                o.style.display = 'none';

                //Fee (Points) in static market
                oCell = LOCKEXTENSIONS.fn.__CreateAndAppend("td", oRow);
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("label", oCell);
                o.id = 'sStatic_LX' + id;
                o.innerText = sStatic;
                if (isNew) o.style.display = "none";
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("input", oCell);
                o.id = 'sStatic_LX' + id + "_edit";
                o.className = "LockExtensionTable";
                o.type = 'text';
                o.style.width = '50px';
                o.maxLength = '9';
                o.value = sStatic;
                addEvent(o, 'blur', function() {
                    return validateStatic_LX(id);
                });
                if (!isNew) o.style.display = "none";
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("img", oCell);
                o.id = 'm_staticError_LX' + id;
                o.src = '../../images/error_icon.gif';
                o.className = 'LockExtensionError';
                o.style.display = 'none';

                //Fee (Points) in worsening market
                oCell = LOCKEXTENSIONS.fn.__CreateAndAppend("td", oRow);
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("label", oCell);
                o.id = 'sWorsening_LX' + id;
                o.innerText = sWorsening;
                if (isNew) o.style.display = "none";
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("input", oCell);
                o.id = 'sWorsening_LX' + id + "_edit";
                o.className = "LockExtensionTable";
                o.type = 'text';
                o.style.width = '50px';
                o.maxLength = '9';
                o.value = sWorsening;
                addEvent(o, 'blur', function() {
                    return validateWorsening_LX(id);
                });
                if (!isNew) o.style.display = "none";
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("img", oCell);
                o.id = 'm_worseningError_LX' + id;
                o.src = '../../images/error_icon.gif';
                o.className = 'LockExtensionError';
                o.style.display = 'none';

                //cancel/delete link
                oCell = LOCKEXTENSIONS.fn.__CreateAndAppend("td", oRow);
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("a", oCell);
                o.id = 'sModifyLink_LX' + id;
                o.className = "LockExtensionTable";
                o.href = 'javascript:void(0);';
                if (isNew)
                    o.innerText = 'cancel';
                else
                    o.innerText = 'delete';
                addEvent(o, 'click', function() {
                    return LOCKEXTENSIONS.fn.modifyRow(id);
                });

                //update/edit link
                oCell = LOCKEXTENSIONS.fn.__CreateAndAppend("td", oRow);
                o = LOCKEXTENSIONS.fn.__CreateAndAppend("a", oCell);
                o.id = 'sUpdateLink_LX' + id;
                o.className = "LockExtensionTable";
                o.href = 'javascript:void(0);';
                if (isNew)
                    o.innerText = 'update';
                else
                    o.innerText = 'edit';
                addEvent(o, 'click', function() {
                    return LOCKEXTENSIONS.fn.updateRow(id);
                });
                return oRow;
            }

            LOCKEXTENSIONS.fn.__CreateAndAppend = function(sTag, oParent) {
                var oItem = document.createElement(sTag);
                oParent.appendChild(oItem);
                return oItem;
            }

            LOCKEXTENSIONS.fn.saveTable = function() {
                if (LOCKEXTENSIONS.Edit.length > 0) {
                    alert("Please finish editing the Lock Extension table before saving.");
                    return false;
                }
                var oTbody = document.getElementById("m_LockExtensionTableBody");
                var oHidden = document.getElementById('m_LockExtensionTableList');

                if (oTbody.rows.length < 1) {
                    alert("Auto Lock Extensions requires at least one extension option.")
                    m_lockExtensionMinRowsError.style.display = 'inline';
                    return false;
                }

                var included = {};
                for (var i = 0; i < LOCKEXTENSIONS.Index; i++) {
                    if (LOCKEXTENSIONS.Deleted[i])
                        continue;
                    var sDays = document.getElementById('sDays_LX' + i);
                    if (sDays == null || sDays.innerText == '')
                        continue;
                    if (included.hasOwnProperty(sDays.innerText)) {
                        alert("Lock Extension Table rows must have unique days.");
                        return false;
                    }
                    included[sDays.innerText] = true;
                }

                try {
                    if (oHidden != null)
                        oHidden.value = JSON.stringify(LOCKEXTENSIONS.fn.__GetTableValues());
                } catch (e) { alert("Error saving lock extension table."); return false; }
                return true;
            }

            LOCKEXTENSIONS.fn.__GetTableValues = function() {
                var iCurrentIndex = 0;
                var oResults = [];
                for (var i = 0; i < LOCKEXTENSIONS.Index; i++) {
                    if (LOCKEXTENSIONS.Deleted[i])
                        continue;
                    var sDays = document.getElementById('sDays_LX' + i);
                    var sImproving = document.getElementById('sImproving_LX' + i);
                    var sStatic = document.getElementById('sStatic_LX' + i);
                    var sWorsening = document.getElementById('sWorsening_LX' + i);

                    if (null == sDays || null == sImproving || null == sStatic || null == sWorsening) {
                        continue;
                    }
                    if (sDays.innerText == '' && sImproving.innerText == '' && sStatic.innerText == '' && sWorsening.innerText == '') {
                        continue;
                    }
                    oResults.push([sDays.innerText, sImproving.innerText, sStatic.innerText, sWorsening.innerText]);
                }
                return oResults;
            }

            LOCKEXTENSIONS.fn.verifyMaxLockExtensionLength = function(maxDays) {
                for (var i = 0; i < LOCKEXTENSIONS.Index; i++) {
                    if (LOCKEXTENSIONS.Deleted[i])
                        continue;
                    var sDays = document.getElementById('sDays_LX' + i);
                    if (parseInt(sDays.innerText) > maxDays) {
                        alert('Lock extension options cannot be longer than "Maximum Total Extension From Original Lock".');
                        return false; //do not save
                    }
                }
                return true; //save OK
            }
        }
        function addEvent(obj, evType, fn, useCapture) {
                addEventHandler(obj, evType, fn, useCapture);
                return true;
        }
        
        function updateTableSize_LX() {
            var oTbody = document.getElementById("m_LockExtensionTableBody");
            document.getElementById('m_addRow_LX').className = ((oTbody.rows.length % 2) == 0) ? "GridItem" : "GridAlternatingItem";
            if(oTbody.rows.length>0)
                m_lockExtensionMinRowsError.style.display = 'none';
            else
                m_lockExtensionMinRowsError.style.display = 'inline';
        }
        
        function validateRow_LX(id) {
            var valid = validateDays_LX(id);
            valid &= validateImproving_LX(id);
            valid &= validateStatic_LX(id);
            valid &= validateWorsening_LX(id);
            return valid;
        }
        function validateDays_LX(id) {
            var sDays_edit = document.getElementById('sDays_LX' + id + "_edit");
            document.getElementById('m_daysError_LX' + id).style.display = 'none';
            var days = parseInt(sDays_edit.value);
            if (isNaN(days) || days <= 0) {
                document.getElementById('m_daysError_LX' + id).style.display = 'inline';
                return false;
            }
            sDays_edit.value = days;
            return true;
        }
        function validateImproving_LX(id) {

            var sImproving_edit = document.getElementById('sImproving_LX' + id + "_edit");
            document.getElementById('m_improvingError_LX' + id).style.display = 'none';
            formatDecimal(sImproving_edit, 3);
            if (isNaN(sImproving_edit.value) || sImproving_edit.value < 0) {
                document.getElementById('m_improvingError_LX' + id).style.display = 'inline';
                return false;
            }
            return true;
        }
        function validateStatic_LX(id)
        {
            var sStatic_edit = document.getElementById('sStatic_LX' + id + "_edit");
            document.getElementById('m_staticError_LX' + id).style.display = 'none';
            formatDecimal(sStatic_edit, 3);
            if (isNaN(sStatic_edit.value) || sStatic_edit.value < 0) {
                document.getElementById('m_staticError_LX' + id).style.display = 'inline';
                return false;
            }
            return true;
        }
        function validateWorsening_LX(id)
        {
            var sWorsening_edit = document.getElementById('sWorsening_LX' + id + "_edit");
            document.getElementById('m_worseningError_LX' + id).style.display = 'none';
            formatDecimal(sWorsening_edit, 3);
            if (isNaN(sWorsening_edit.value) || sWorsening_edit.value < 0) {
                document.getElementById('m_worseningError_LX' + id).style.display = 'inline';
                return false;
            }
            return true;
        }
    </script>
</head>
<div>
    <span id="m_lockExtensionMinRowsError" class="LockExtensionError" style="color:Red; display:none">Auto Lock Extensions requires at least one extension option.<br /></span>
    <div style="padding-right: 4px; height: 150px; overflow-y:scroll;">
        <table class="DataGrid LockExtensionTable" border="1" cellpadding="2" cellspacing="0" style="border-collapse: collapse;">
            <tr class="GridHeader" style="border: solid 1px white">
                <td style="width:200px">
                    Extension<br />(Days)
                </td>
                <td style="width:200px">
                    Fee (Points)<br />In Improving Market*
                </td>
                <td style="width:200px">
                    Fee (Points)<br />In Static Market**
                </td>
                <td style="width:200px">
                    Fee (Points)<br />In Worsening Market***
                </td>
                <td style="width:75px"></td>
                <td style="width:75px"></td>
            </tr>
            <tbody id='m_LockExtensionTableBody'></tbody>
            <tfoot>
                <tr id="m_addRow_LX">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="javascript:void(0);" onclick="if(!this.disabled && !hasDisabledAttr(this))LOCKEXTENSIONS.fn.addRow();" class="LockExtensionTable">add</a></td>
                    <td></td>
                </tr>
                <tr class="GridHeader">
                    <td colspan="6">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>