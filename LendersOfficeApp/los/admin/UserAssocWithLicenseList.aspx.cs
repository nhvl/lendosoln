using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{

	public partial class UserAssocWithLicenseList : LendersOffice.Common.BasePage
	{
        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }

		private class UserDesc
		{
			private Guid m_userId ;
			private string m_displayName ;
			private string m_loginNm ;
			private string m_lastLoginDate ;

			public UserDesc(Guid userId, string displayName, string loginNm, string lastLoginDate)
			{
				m_userId = userId ;
				m_displayName = displayName ;
				m_loginNm = loginNm ;
				m_lastLoginDate = lastLoginDate ;
			}
			public Guid UserId
			{
				get { return m_userId ; }
			}
			public string DisplayName
			{
				get { return m_displayName ; }
			}
			public string LoginNm
			{
				get { return m_loginNm ; }
			}
			public string LastLoginDate
			{
				get { return m_lastLoginDate ; }
			}
		}
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			string licenseId ;

			if (!IsPostBack)
			{
				licenseId = Request["licenseId"] ;
				ViewState["licenseId"] = licenseId ;
				ViewState["change"] = "F" ;

                Guid brokerId = Guid.Empty;

                if (BrokerUserPrincipal.CurrentPrincipal != null)
                {
                    brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
                }

                SqlParameter[] parameters = { new SqlParameter("@LicenseId", new Guid(licenseId)) };
				using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveLicenseByLicenseId", parameters))
				{
                    if (reader.Read()) 
                    {
                        licenseNumber.Text = Convert.ToString(reader["LicenseNumber"]);
                    }
				}
			}
			else
			{
				licenseId = (string)ViewState["licenseId"] ;

				string userId = Request["userid"] ;
                Guid brokerId = Guid.Empty;

                if (BrokerUserPrincipal.CurrentPrincipal != null)
                {
                    brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
                }
				if (userId != null && userId.Length > 0)
				{
					LendersOfficeApp.ObjLib.Licensing.BrokerUser brokerUser = new LendersOfficeApp.ObjLib.Licensing.BrokerUser(brokerId, new Guid(userId), "N/A") ;
					brokerUser.UnassignLicense() ;

					ViewState["change"] = "T" ;
				}
			}

			BindDataGrid(new Guid(licenseId)) ;
		}
		private void BindDataGrid(Guid licenseId)
		{
			ArrayList list = new ArrayList() ;

            SqlParameter[] parameters = { new SqlParameter("@LicenseId", licenseId) };
            Guid brokerId = Guid.Empty;

            if (BrokerUserPrincipal.CurrentPrincipal != null)
            {
                brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            }
			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListUsersByLicenseId", parameters))
			{
				while (reader.Read())
				{
					string datestring = "" ;
					if (reader["RecentLoginD"] != DBNull.Value)
						datestring = Convert.ToDateTime(reader["RecentLoginD"]).ToString() ;
					list.Add(new UserDesc(new Guid(reader["UserId"].ToString()), reader["DisplayName"].ToString(), reader["LoginNm"].ToString(), datestring)) ;
				}
			}

			m_dgUsers.DataSource = ViewGenerator.Generate(list) ;
			m_dgUsers.DataBind() ;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion


		protected void btnClose_Click(object sender, System.EventArgs e)
		{
			LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "change", "'" + (string)ViewState["change"] + "'") ;
		}

        protected void m_dgUsers_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dataItem = args.Item.DataItem as DataRowView;

            LinkButton UnassignLink = args.Item.FindControl("UnassignLink") as LinkButton;
            UnassignLink.OnClientClick = "unassignUser(" + LendersOffice.AntiXss.AspxTools.JsString(dataItem["userId"].ToString()) + "); return false;";
        }
    }
}
