﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LendersOfficeApp.los.admin {
    
    
    public partial class BrokerOptionUserControl {
        
        /// <summary>
        /// BrokerLpePriceGroupIdDefault control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList BrokerLpePriceGroupIdDefault;
        
        /// <summary>
        /// EmployeeResourcesURL control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox EmployeeResourcesURL;
        
        /// <summary>
        /// m_displayPmlFeeIn100Format control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList m_displayPmlFeeIn100Format;
        
        /// <summary>
        /// LpeSubmitAgreement control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LpeSubmitAgreement;
        
        /// <summary>
        /// FthbCustomDefinition control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox FthbCustomDefinition;
        
        /// <summary>
        /// m_RequireVerifyLicenseByState control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList m_RequireVerifyLicenseByState;
        
        /// <summary>
        /// TelemarketerCanOnlyAssignToManager control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList TelemarketerCanOnlyAssignToManager;
        
        /// <summary>
        /// AllowCallCenterAgentRunPrequalRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow AllowCallCenterAgentRunPrequalRow;
        
        /// <summary>
        /// TelemarketerCanRunPrequal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList TelemarketerCanRunPrequal;
        
        /// <summary>
        /// IsLOAllowedToEditProcessorAssignedFile control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsLOAllowedToEditProcessorAssignedFile;
        
        /// <summary>
        /// AllowCreatingLoanFromBlankTemplate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList AllowCreatingLoanFromBlankTemplate;
        
        /// <summary>
        /// IsNonAccountantCanModifyTrustAccount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsNonAccountantCanModifyTrustAccount;
        
        /// <summary>
        /// IsAEAsOfficialLoanOfficer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsAEAsOfficialLoanOfficer;
        
        /// <summary>
        /// IsAutoGenerateLiabilityPayOffConditions control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsAutoGenerateLiabilityPayOffConditions;
        
        /// <summary>
        /// IsAllowExternalUserToCreateNewLoan control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsAllowExternalUserToCreateNewLoan;
        
        /// <summary>
        /// IsShowPriceGroupInEmbeddedPml control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsShowPriceGroupInEmbeddedPml;
        
        /// <summary>
        /// IsLpeManualSubmissionAllowed control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsLpeManualSubmissionAllowed;
        
        /// <summary>
        /// IsPmlPointImportAllowed control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsPmlPointImportAllowed;
        
        /// <summary>
        /// AllowHelocsRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow AllowHelocsRow;
        
        /// <summary>
        /// IsHelocsAllowedInPml control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsHelocsAllowedInPml;
        
        /// <summary>
        /// IsStandAloneSecondAllowedInPml control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsStandAloneSecondAllowedInPml;
        
        /// <summary>
        /// Is8020ComboAllowedInPml control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList Is8020ComboAllowedInPml;
        
        /// <summary>
        /// IsAllPrepaymentPenaltyAllowed control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsAllPrepaymentPenaltyAllowed;
        
        /// <summary>
        /// IsBestPriceEnabled control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList IsBestPriceEnabled;
        
        /// <summary>
        /// m_IsAlwaysUseBestPrice control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList m_IsAlwaysUseBestPrice;
        
        /// <summary>
        /// m_ShowUnderwriterInPMLCertificate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList m_ShowUnderwriterInPMLCertificate;
        
        /// <summary>
        /// m_ShowJuniorUnderwriterInPMLCertificate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList m_ShowJuniorUnderwriterInPMLCertificate;
        
        /// <summary>
        /// m_ShowProcessorInPMLCertificate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList m_ShowProcessorInPMLCertificate;
        
        /// <summary>
        /// m_ShowJuniorProcessorInPMLCertificate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList m_ShowJuniorProcessorInPMLCertificate;
        
        /// <summary>
        /// CalculateClosingCostInPml control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl CalculateClosingCostInPml;
        
        /// <summary>
        /// m_CalculateclosingCostInPML control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList m_CalculateclosingCostInPML;
        
        /// <summary>
        /// m_ApplyClosingCostToGFE control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList m_ApplyClosingCostToGFE;
        
        /// <summary>
        /// m_TriggerAprRediscNotifForAprDecrease control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList m_TriggerAprRediscNotifForAprDecrease;
        
        /// <summary>
        /// ShowLoanProductIdentifier control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList ShowLoanProductIdentifier;
        
        /// <summary>
        /// QuickPricerRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow QuickPricerRow;
        
        /// <summary>
        /// MIVendorRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow MIVendorRow;
        
        /// <summary>
        /// FannieMaeEarlyCheckRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow FannieMaeEarlyCheckRow;
        
        /// <summary>
        /// m_lockDeskChoice control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LendersOfficeApp.los.common.EmployeeRoleChooserLink m_lockDeskChoice;
        
        /// <summary>
        /// WorkflowRulesControllerRow control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlTableRow WorkflowRulesControllerRow;
        
        /// <summary>
        /// WorkflowRulesControllerId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden WorkflowRulesControllerId;
        
        /// <summary>
        /// WorkflowRulesControllerName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl WorkflowRulesControllerName;
        
        /// <summary>
        /// IsQuickPricerEnabled control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox IsQuickPricerEnabled;
        
        /// <summary>
        /// QuickPricerTemplateId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList QuickPricerTemplateId;
        
        /// <summary>
        /// QuickPricerNoResultMessage control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox QuickPricerNoResultMessage;
        
        /// <summary>
        /// QuickPricerDisclaimerAtResult control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox QuickPricerDisclaimerAtResult;
        
        /// <summary>
        /// FnmaEarlyCheckUserName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox FnmaEarlyCheckUserName;
        
        /// <summary>
        /// FnmaEarlyCheckDecryptedPassword control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox FnmaEarlyCheckDecryptedPassword;
        
        /// <summary>
        /// FnmaEarlyCheckInstutionId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox FnmaEarlyCheckInstutionId;
    }
}
