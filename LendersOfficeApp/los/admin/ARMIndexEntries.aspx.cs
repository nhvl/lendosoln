using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.LoanPrograms;
using LendersOffice.Security;
using DataAccess;
using LendersOffice;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{
	public partial class ARMIndexEntries : LendersOffice.Common.BasePage
	{
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingArmIndexEntries
                };
            }
        }

		private ARMIndexDescriptorSet m_Set = new ARMIndexDescriptorSet();

		private Boolean m_DoNotSave = false;

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{
			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		private String CommandToDo
		{
			set
			{
				ClientScript.RegisterHiddenField( "m_commandToDo" , value );
			}
		}

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Initialize this view.

			try
			{
				// Load the broker to get the choices.

				if( IsPostBack == false )
				{
					// 12/17/2004 kb - Get the starting list from the
					// broker's set.

					m_Set.Retrieve( CurrentUser.BrokerId );

                    m_systemIndexes.DataSource = SystemArmIndex.ListSystemArmIndexes();
                    m_systemIndexes.DataBind();
				}
				else
				{
					// 12/17/2004 kb - Pull the entries from the grid,
					// which was seeded with the last posted set and
					// has any edits overlaid on top.

					m_Set.Items.Clear();

					foreach( DataGridItem dgItem in m_Grid.Items )
					{
						// Get the entry's id.  If empty, we know that this
						// row is yet to be saved.

						Guid idxId = Guid.Empty;

						try
						{
							idxId = new Guid( ( dgItem.FindControl( "Id" ) as Label ).Text );
						}
						catch
						{
							continue;
						}

						// Get the rest of the definition.  Each control
						// should be in place because we have 

						TextBox      namedAs = dgItem.FindControl( "NamedAs" ) as TextBox;
						DropDownList freddeT = dgItem.FindControl( "FreddeT" ) as DropDownList;
						DropDownList fannieT = dgItem.FindControl( "FannieT" ) as DropDownList;
						TextBox      effectD = dgItem.FindControl( "EffectD" ) as TextBox;
						TextBox      currVal = dgItem.FindControl( "CurrVal" ) as TextBox;
						TextBox      basedOn = dgItem.FindControl( "BasedOn" ) as TextBox;
						TextBox      foundAt = dgItem.FindControl( "FoundAt" ) as TextBox;

						if( namedAs != null && fannieT != null && freddeT != null && effectD != null && currVal != null && basedOn != null && foundAt != null )
						{
							ARMIndexDescriptor aDesc = new ARMIndexDescriptor();

							aDesc.IndexNameVstr       = namedAs.Text;
							aDesc.IndexBasedOnVstr    = basedOn.Text;
							aDesc.IndexCanBeFoundVstr = foundAt.Text;

							try
							{
								aDesc.FreddieArmIndexT = ( E_sFreddieArmIndexT ) Convert.ToInt32( freddeT.SelectedItem.Value );
							}
							catch
							{
								ErrorMessage = "\"" + namedAs.Text + "\" has invalid Freddie Mac ARM Index";

								m_DoNotSave = true;
							}

							try
							{
								aDesc.FannieMaeArmIndexT = ( E_sArmIndexT ) Convert.ToInt32( fannieT.SelectedItem.Value );
							}
							catch
							{
								ErrorMessage = "\"" + namedAs.Text + "\" has invalid Fannie Mae ARM Index";

								m_DoNotSave = true;
							}

							try
							{
								aDesc.IndexCurrentValueDecimal = Decimal.Parse( currVal.Text.TrimEnd( '%' ) );
							}
							catch
							{
								ErrorMessage = "\"" + namedAs.Text + "\" has invalid current rate value";

								m_DoNotSave = true;
							}

							try
							{
								aDesc.EffectiveD = DateTime.Parse( effectD.Text );
							}
							catch
							{
								ErrorMessage = "\"" + namedAs.Text + "\" has invalid effective date";

								m_DoNotSave = true;
							}

							aDesc.IndexIdGuid = idxId;

							m_Set.Items.Add( aDesc );
						}
					}
				}
			}
			catch( CBaseException e )
			{
				// Oops!

				Tools.LogError( "Failed to load page." , e );

				ErrorMessage = e.UserMessage;

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to load page." , e );

				ErrorMessage = "Failed to load page.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Overload framework event for loading.
		/// </summary>

		protected override void LoadViewState( object oState )
		{
			// Get the view state of a previous postback and restore
			// the data grid's state prior to loading form variables.

			try
			{
				base.LoadViewState( oState );

				if( ViewState[ "Cache" ] != null )
				{
					m_Set = ARMIndexDescriptorSet.ToObject( ViewState[ "Cache" ].ToString() );
				}

				m_Grid.DataSource = m_Set.Items;
				m_Grid.DataBind();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to load viewstate." , e );

				ErrorMessage = "Failed to load page.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Overload framework event for saving.
		/// </summary>

		protected override object SaveViewState()
		{
			// Store what we have as latest in the viewstate so
			// we can stay current between postbacks without
			// hitting the database.

			ViewState.Add( "Cache" , m_Set.ToString() );

			return base.SaveViewState();
		}

		/// <summary>
		/// Bind this page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Bind this view to our data.

			try
			{
				// Bind and let grid pull current choices.  We
				// apply the current sorting settings to the
				// list prior to binding.  On save, this is
				// the order we persist.

				if( m_Set.Items.Count == 0 )
				{
					m_EmptyMessage.Visible = true;
				}

				m_Grid.DataSource = m_Set.Items;
				m_Grid.DataBind();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to prerender page." , e );

				ErrorMessage = "Failed to render page.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);
		}
		#endregion

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void GridItemBound( object sender , System.Web.UI.WebControls.DataGridItemEventArgs a )
		{
			// 12/17/2004 kb - Bind the current choice item to
			// the grid.  We may get to the point where we keep
			// all the descriptions of the same category in
			// one binding object.  At that point, we'll bind
			// to a nested list here.

			ARMIndexDescriptor dItem = a.Item.DataItem as ARMIndexDescriptor;

			if( dItem != null )
			{
				// Get the entry's id.  If empty, we know that this
				// row is yet to be saved.

				Label idxId = a.Item.FindControl( "Id" ) as Label;

				if( idxId != null )
				{
					// Get the rest of the definition.  Each control
					// should be in place because we have 

					TextBox      namedAs = a.Item.FindControl( "NamedAs" ) as TextBox;
					DropDownList fannieT = a.Item.FindControl( "FannieT" ) as DropDownList;
					DropDownList freddeT = a.Item.FindControl( "FreddeT" ) as DropDownList;
					TextBox      effectD = a.Item.FindControl( "EffectD" ) as TextBox;
					TextBox      currVal = a.Item.FindControl( "CurrVal" ) as TextBox;
					TextBox      basedOn = a.Item.FindControl( "BasedOn" ) as TextBox;
					TextBox      foundAt = a.Item.FindControl( "FoundAt" ) as TextBox;

					if( namedAs != null )
					{
						namedAs.Text = dItem.IndexNameVstr;
					}

					if( currVal != null )
					{
						currVal.Text = dItem.IndexCurrentValueDecimal.ToString() + "%";
					}

					if( fannieT != null )
					{
						fannieT.SelectedIndex = fannieT.Items.IndexOf( fannieT.Items.FindByValue( dItem.FannieMaeArmIndexT.ToString( "D" ) ) );
					}

					if( freddeT != null )
					{
						freddeT.SelectedIndex = freddeT.Items.IndexOf( freddeT.Items.FindByValue( dItem.FreddieArmIndexT.ToString( "D" ) ) );
					}

					if( effectD != null )
					{
						effectD.Text = dItem.EffectiveD.ToShortDateString();
					}

					if( basedOn != null )
					{
						basedOn.Text = dItem.IndexBasedOnVstr;
					}

					if( foundAt != null )
					{
						foundAt.Text = dItem.IndexCanBeFoundVstr;
					}

					idxId.Text = dItem.IndexIdGuid.ToString();
				}
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void GridItemClick( object sender , System.Web.UI.WebControls.DataGridCommandEventArgs a )
		{
			// Handle delete command.  We use the position of the
			// row as our key.

			if( a.CommandName.ToLower() == "remove" )
			{
				try
				{
					ARMIndexDescriptor aDesc = m_Set.Items[ a.Item.ItemIndex ] as ARMIndexDescriptor;

					if( aDesc != null )
					{
						if( m_Set.IsARMIndexInUse( aDesc.IndexIdGuid ) == true )
						{
							ErrorMessage = "Index is in use. Remove request denied.";

							return;
						}

						m_Set.Items.RemoveAt( a.Item.ItemIndex );

						m_IsDirty.Value = "Dirty";
					}
				}
				catch( Exception e )
				{
					// Oops!

					Tools.LogError( "Failed to delete arm index entry." , e );

					ErrorMessage = "Failed to remove item.";

					m_Ok.Enabled    = false;
					m_Apply.Enabled = false;
					m_Add.Enabled   = false;
				}
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void ApplyClick( object sender , System.EventArgs a )
		{
			// Create new entry set and save it to the database.

			try
			{
				// Store the cached list directly in the broker
				// and save.

				if( m_DoNotSave == false )
				{
					m_Set.Commit( CurrentUser.BrokerId );

					m_IsDirty.Value = "";
				}
			}
			catch( CBaseException e )
			{
				// Oops!

				Tools.LogError( "Failed to save arm index entries." , e );

				ErrorMessage = e.UserMessage;

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to save arm index entries." , e );

				ErrorMessage = "Failed to save.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void OkClick( object sender , System.EventArgs a )
		{
			// Create new choice list and save it to the broker.

			try
			{
				// Store the cached list directly in the broker
				// and save.

				ApplyClick( sender , a );

				// We close on successful saving.

				CommandToDo = "Close";
			}
			catch( CBaseException e )
			{
				// Oops!

				Tools.LogError( "Failed to save arm index entries." , e );

				ErrorMessage = e.UserMessage;

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to save arm index entries." , e );

				ErrorMessage = "Failed to save.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void AddClick( object sender , System.EventArgs a )
		{
			// Add a new entry to our cached instance.

			try
			{
				// Create new entry only if the strings are valid.

				m_Set.Push( "" , 0 , E_sArmIndexT.LeaveBlank , DateTime.Today.AddDays( 1 - DateTime.Today.Day ) , "" , "" );

				m_IsDirty.Value = "Dirty";
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to add new entry." , e );

				ErrorMessage = "Failed to add.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

	}

}
