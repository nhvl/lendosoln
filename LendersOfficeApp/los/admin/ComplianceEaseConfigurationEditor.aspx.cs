﻿namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using ComplianceEase;
    using DataAccess;
    using DataAccess.Utilities;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Conversions;
    using LendersOffice.ObjLib.Conversions.ComplianceEase;
    using LendersOffice.Security;
    using HUDApproval = ComplianceEase.E_LenderDetailsLenderHudApprovalStatusType;

    public partial class ComplianceEaseConfigurationEditor : LendersOffice.Common.BasePage
    {

        private BrokerDB m_bd = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        protected bool IsEnabledForBroker
        {
            get
            {
                return m_bd.IsEnableComplianceEaseIntegration
                    && E_BrokerBillingVersion.PerTransaction == m_bd.BillingVersion
                    && m_bd.IsEnablePTMComplianceEaseIndicator;
            }
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            RegisterJsScript("LQBPopup.js");

            if (!IsEnabledForBroker)
            {
                ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.GenericAccessDenied, "Attempting to access broker ComplianceEase configuration without necessary permissions"), false, Guid.Empty, Guid.Empty);
            }

            if (!IsPostBack)
            {
                BindBeginAuditStatus(BeginAuditStatus);
                BindHUDApproval(Approval);
                Tools.Bind_RespaFeePaidToType(sLOrigFPaidTo);
                Tools.Bind_RespaFeePaidToType(sApprFPaidTo);
                Tools.Bind_RespaFeePaidToType(sCrFPaidTo);
                Tools.Bind_RespaFeePaidToType(sInspectFPaidTo);
                Tools.Bind_RespaFeePaidToType(sMBrokFPaidTo);
                Tools.Bind_RespaFeePaidToType(sProcFPaidTo);
                Tools.Bind_RespaFeePaidToType(sUwFPaidTo);
                Tools.Bind_RespaFeePaidToType(sWireFPaidTo);
                Tools.Bind_RespaFeePaidToType(sTxServFPaidTo);
                Tools.Bind_RespaFeePaidToType(sFloodCertificationFPaidTo);
                Tools.Bind_RespaFeePaidToType(sLDiscntPaidTo);
                Tools.Bind_RespaFeePaidToType(sGfeCreditLenderPaidItemFPaidTo);
                Tools.Bind_RespaFeePaidToType(sGfeLenderCreditFPaidTo);
                Tools.Bind_RespaFeePaidToType(sGfeDiscountPointFPaidTo);
                Tools.Bind_RespaFeePaidToType(s800RestPaidTo);
                Tools.Bind_RespaFeePaidToType(sIPiaPaidTo);
                Tools.Bind_RespaFeePaidToType(sHazInsPiaPaidTo);
                Tools.Bind_RespaFeePaidToType(sMipPiaPaidTo);
                Tools.Bind_RespaFeePaidToType(s900RestPaidTo);
                Tools.Bind_RespaFeePaidToType(sHazInsRsrvPaidTo);
                Tools.Bind_RespaFeePaidToType(sMInsRsrvPaidTo);
                Tools.Bind_RespaFeePaidToType(sRealETxRsrvPaidTo);
                Tools.Bind_RespaFeePaidToType(sSchoolTxRsrvPaidTo);
                Tools.Bind_RespaFeePaidToType(sFloodInsRsrvPaidTo);
                Tools.Bind_RespaFeePaidToType(s1000RestPaidTo);
                Tools.Bind_RespaFeePaidToType(sEscrowFPaidTo);
                Tools.Bind_RespaFeePaidToType(sDocPrepFPaidTo);
                Tools.Bind_RespaFeePaidToType(sOwnerTitleInsPaidTo);
                Tools.Bind_RespaFeePaidToType(sTitleInsFPaidTo);
                Tools.Bind_RespaFeePaidToType(sNotaryFPaidTo);
                Tools.Bind_RespaFeePaidToType(sAttorneyFPaidTo);
                Tools.Bind_RespaFeePaidToType(s1100RestPaidTo);
                Tools.Bind_RespaFeePaidToType(sCountyRtcPaidTo);
                Tools.Bind_RespaFeePaidToType(sStateRtcPaidTo);
                Tools.Bind_RespaFeePaidToType(sRecFPaidTo);
                Tools.Bind_RespaFeePaidToType(s1200RestPaidTo);
                Tools.Bind_RespaFeePaidToType(sPestInspectFPaidTo);
                Tools.Bind_RespaFeePaidToType(s1300RestPaidTo);

                var ceec = m_bd.ComplianceEaseExportConfig;
                BeginAuditStatus.Value = ceec.BeginRunningAuditsAt.ToString();
                Approval.Value = ceec.HUDApproval.ToString();
                Tools.SetDropDownListValue(sLOrigFPaidTo, ceec.sLOrigFPaidTo);
                Tools.SetDropDownListValue(sApprFPaidTo, ceec.sApprFPaidTo);
                Tools.SetDropDownListValue(sCrFPaidTo, ceec.sCrFPaidTo);
                Tools.SetDropDownListValue(sInspectFPaidTo, ceec.sInspectFPaidTo);
                Tools.SetDropDownListValue(sMBrokFPaidTo, ceec.sMBrokFPaidTo);
                Tools.SetDropDownListValue(sProcFPaidTo, ceec.sProcFPaidTo);
                Tools.SetDropDownListValue(sUwFPaidTo, ceec.sUwFPaidTo);
                Tools.SetDropDownListValue(sWireFPaidTo, ceec.sWireFPaidTo);
                Tools.SetDropDownListValue(sTxServFPaidTo, ceec.sTxServFPaidTo);
                Tools.SetDropDownListValue(sFloodCertificationFPaidTo, ceec.sFloodCertificationFPaidTo);
                Tools.SetDropDownListValue(sLDiscntPaidTo, ceec.sLDiscntPaidTo);
                Tools.SetDropDownListValue(sGfeCreditLenderPaidItemFPaidTo, ceec.sGfeCreditLenderPaidItemFPaidTo);
                Tools.SetDropDownListValue(sGfeLenderCreditFPaidTo, ceec.sGfeLenderCreditFPaidTo);
                Tools.SetDropDownListValue(sGfeDiscountPointFPaidTo, ceec.sGfeDiscountPointFPaidTo);
                Tools.SetDropDownListValue(s800RestPaidTo, ceec.s800RestPaidTo);
                Tools.SetDropDownListValue(sIPiaPaidTo, ceec.sIPiaPaidTo);
                Tools.SetDropDownListValue(sHazInsPiaPaidTo, ceec.sHazInsPiaPaidTo);
                Tools.SetDropDownListValue(sMipPiaPaidTo, ceec.sMipPiaPaidTo);
                Tools.SetDropDownListValue(s900RestPaidTo, ceec.s900RestPaidTo);
                Tools.SetDropDownListValue(sHazInsRsrvPaidTo, ceec.sHazInsRsrvPaidTo);
                Tools.SetDropDownListValue(sMInsRsrvPaidTo, ceec.sMInsRsrvPaidTo);
                Tools.SetDropDownListValue(sRealETxRsrvPaidTo, ceec.sRealETxRsrvPaidTo);
                Tools.SetDropDownListValue(sSchoolTxRsrvPaidTo, ceec.sSchoolTxRsrvPaidTo);
                Tools.SetDropDownListValue(sFloodInsRsrvPaidTo, ceec.sFloodInsRsrvPaidTo);
                Tools.SetDropDownListValue(s1000RestPaidTo, ceec.s1000RestPaidTo);
                Tools.SetDropDownListValue(sEscrowFPaidTo, ceec.sEscrowFPaidTo);
                Tools.SetDropDownListValue(sDocPrepFPaidTo, ceec.sDocPrepFPaidTo);
                Tools.SetDropDownListValue(sOwnerTitleInsPaidTo, ceec.sOwnerTitleInsPaidTo);
                Tools.SetDropDownListValue(sTitleInsFPaidTo, ceec.sTitleInsFPaidTo);
                Tools.SetDropDownListValue(sNotaryFPaidTo, ceec.sNotaryFPaidTo);
                Tools.SetDropDownListValue(sAttorneyFPaidTo, ceec.sAttorneyFPaidTo);
                Tools.SetDropDownListValue(s1100RestPaidTo, ceec.s1100RestPaidTo);
                Tools.SetDropDownListValue(sCountyRtcPaidTo, ceec.sCountyRtcPaidTo);
                Tools.SetDropDownListValue(sStateRtcPaidTo, ceec.sStateRtcPaidTo);
                Tools.SetDropDownListValue(sRecFPaidTo, ceec.sRecFPaidTo);
                Tools.SetDropDownListValue(s1200RestPaidTo, ceec.s1200RestPaidTo);
                Tools.SetDropDownListValue(sPestInspectFPaidTo, ceec.sPestInspectFPaidTo);
                Tools.SetDropDownListValue(s1300RestPaidTo, ceec.s1300RestPaidTo);

                var excludedStatuses = ceec.ExcludedStatuses.Select(s => LoanStatus.GetStatusDescription(s));
                ExcludedStatuses.DataSource = excludedStatuses;
                ExcludedStatuses.DataBind();

                Policies.DataSource = ceec.LendingPolicies;
                Policies.DataBind();

                Licenses.DataSource = ceec.LenderLicenses;
                Licenses.DataBind();

                Exemptions.DataSource = from a in ceec.DIDMCAExemptions
                                        select new
                                        {
                                            Text = ComplianceEaseExportConfiguration.States[a],
                                            Value = a
                                        };
                Exemptions.DataTextField = "Text";
                Exemptions.DataValueField = "Value";
                Exemptions.DataBind();
            }
        }

        protected void BindBeginAuditStatus(HtmlSelect ddl)
        {
            List<object> elems = new List<object>();
            var potentialBeginAuditStatuses = ComplianceExportStatusConfig.StatusesAvailableForAudit(m_bd.HasEnabledInAnyChannel)
                .Where(s => !Tools.IsInactiveStatus(s));

            foreach (var status in potentialBeginAuditStatuses)
            {
                elems.Add(new { Text = CPageBase.sStatusT_map_rep(status), Value = status.ToString() });
            }

            var currentBeginAuditStatus = m_bd.ComplianceEaseExportConfig.BeginRunningAuditsAt;
            if (!potentialBeginAuditStatuses.Contains(currentBeginAuditStatus))
            {
                // If the currently selected option is not valid, we still want to display it so 
                // the lender is aware what is configured and can change it.
                elems.Add(new { Text = CPageBase.sStatusT_map_rep(currentBeginAuditStatus), Value = currentBeginAuditStatus.ToString() });
            }

            ddl.DataSource = elems;
            ddl.DataTextField = "Text";
            ddl.DataValueField = "Value";
            ddl.DataBind();
        }

        protected void BindHUDApproval(HtmlSelect ddl)
        {
            Regex inflater = new Regex("([a-z])([A-Z])");
            string[] text = Enum.GetNames(typeof(HUDApproval));
            var vals = Enum.GetValues(typeof(HUDApproval));
            List<object> elems = new List<object>();
            for(int i = 0; i < text.Length; i++)
            {
                if (new[] { "Undefined", "Default" }.Contains(text[i]))
                {
                    continue;
                }
                elems.Add(new { Text = inflater.Replace(text[i], "$1 $2"), Value = vals.GetValue(i).ToString() });
            }

            ddl.DataSource = elems;
            ddl.DataTextField = "Text";
            ddl.DataValueField = "Value";
            ddl.DataBind();
        }

        protected void Update_Click(object sender, EventArgs args)
        {
            var broker = this.m_bd;
            var current = broker.ComplianceEaseExportConfig;

            var excludedStatuses = GetStatusValues(ExcludedStatusesSubmit) ?? current.ExcludedStatuses;
            var licenses = MapLicensesToStates(GetValues(LicensesSubmit)) ?? current.LenderLicensesByState;
            var policies = GetValues(PoliciesSubmit) ?? current.LendingPolicies;
            var exemptions = GetValues(ExemptionsSubmit) ?? current.DIDMCAExemptions;
            var approval = (HUDApproval)Enum.Parse(typeof(HUDApproval), Approval.Value);
            var beginRunningAuditsStatus = (E_sStatusT)Enum.Parse(typeof(E_sStatusT), BeginAuditStatus.Value);

            var newConfig = new ComplianceEaseExportConfiguration(excludedStatuses, licenses, policies, exemptions, approval, beginRunningAuditsStatus);
            newConfig.sLOrigFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sLOrigFPaidTo.SelectedValue);
            newConfig.sApprFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sApprFPaidTo.SelectedValue);
            newConfig.sCrFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sCrFPaidTo.SelectedValue);
            newConfig.sInspectFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sInspectFPaidTo.SelectedValue);
            newConfig.sMBrokFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sMBrokFPaidTo.SelectedValue);
            newConfig.sProcFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sProcFPaidTo.SelectedValue);
            newConfig.sUwFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sUwFPaidTo.SelectedValue);
            newConfig.sWireFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sWireFPaidTo.SelectedValue);
            newConfig.sTxServFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sTxServFPaidTo.SelectedValue);
            newConfig.sFloodCertificationFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sFloodCertificationFPaidTo.SelectedValue);
            newConfig.sLDiscntPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sLDiscntPaidTo.SelectedValue);
            newConfig.sGfeCreditLenderPaidItemFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sGfeCreditLenderPaidItemFPaidTo.SelectedValue);
            newConfig.sGfeLenderCreditFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sGfeLenderCreditFPaidTo.SelectedValue);
            newConfig.sGfeDiscountPointFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sGfeDiscountPointFPaidTo.SelectedValue);
            newConfig.s800RestPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), s800RestPaidTo.SelectedValue);
            newConfig.sIPiaPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sIPiaPaidTo.SelectedValue);
            newConfig.sHazInsPiaPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sHazInsPiaPaidTo.SelectedValue);
            newConfig.sMipPiaPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sMipPiaPaidTo.SelectedValue);
            newConfig.s900RestPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), s900RestPaidTo.SelectedValue);
            newConfig.sEscrowFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sEscrowFPaidTo.SelectedValue);
            newConfig.sDocPrepFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sDocPrepFPaidTo.SelectedValue);
            newConfig.sOwnerTitleInsPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sOwnerTitleInsPaidTo.SelectedValue);
            newConfig.sTitleInsFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sTitleInsFPaidTo.SelectedValue);
            newConfig.sNotaryFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sNotaryFPaidTo.SelectedValue);
            newConfig.sAttorneyFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sAttorneyFPaidTo.SelectedValue);
            newConfig.s1100RestPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), s1100RestPaidTo.SelectedValue);
            newConfig.sCountyRtcPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sCountyRtcPaidTo.SelectedValue);
            newConfig.sStateRtcPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sStateRtcPaidTo.SelectedValue);
            newConfig.sRecFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sRecFPaidTo.SelectedValue);
            newConfig.s1200RestPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), s1200RestPaidTo.SelectedValue);
            newConfig.sPestInspectFPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), sPestInspectFPaidTo.SelectedValue);
            newConfig.s1300RestPaidTo = (E_RespaFeePaidToType)Enum.Parse(typeof(E_RespaFeePaidToType), s1300RestPaidTo.SelectedValue);

            broker.ComplianceEaseExportConfig = newConfig;
            broker.Save();

            ClientScript.RegisterStartupScript(this.GetType(), "Autoclose", "onClosePopup()", true);
        }

        private ILookup<string, string> MapLicensesToStates(List<string> licenses)
        {
            if (licenses == null) return null;

            ILookup<string, string> ret;

            ret = (from a in licenses
                   select new
                   {
                       StateAbbrev = GetStateAbbreviationFromName(a),
                       License = a
                   }).ToLookup(a => a.StateAbbrev, a => a.License);


            return ret;
        }

        private string GetStateAbbreviationFromName(string License)
        {
            //Licenses always start with the state name, but some state names have a space in them
            var statesMap = ComplianceEaseExportConfiguration.States;
            var licenseParts = License.Split(' ');
            string state = licenseParts[0];
            
            //If "state" is currently any of these, it's from a two-word state name.
            if (new[] { "North",
                        "West",
                        "South",
                        "New",
                        "Rhode"}.Any(a => state == a))
            {
                state = state + " " + licenseParts[1];
            }

            //Handle "District of Columbia"
            if (state == "District")
            {
                state = state + " " + licenseParts[1] + " " + licenseParts[2];
            }

            return statesMap.Where(a => a.Value == state).First().Key;
        }

        private List<string> GetValues(HtmlInputHidden e)
        {
            if (string.IsNullOrEmpty(e.Value.TrimWhitespaceAndBOM()))
            {
                return new List<string>();
            }

            if (e.Value.TrimWhitespaceAndBOM().Equals("null", StringComparison.InvariantCultureIgnoreCase))
            {
                return null;
            }

            if (e.Value.Contains('|'))
            {
                return e.Value.Split('|').ToList();
            }
            else if (!(string.IsNullOrEmpty(e.Value)))
            {
                return new[] { e.Value }.ToList();
            }

            Tools.LogBug("In compliance ease config setup, user managed to select an invalid list of things. Value is: [" + e.Value + "]");
            return null;
        }

        private List<E_sStatusT> GetStatusValues(HtmlInputHidden input)
        {
            List<string> statuses = this.GetValues(input);

            if (statuses == null)
            {
                return null;
            }

            return statuses.Select(s => LoanStatus.GetStatusFromDescription(s)).OfType<E_sStatusT>().ToList();
        }

        protected void LoadCompleteHandler(object sender, EventArgs e)
        {
            // occurs on postback after all control raised events, but before prerendering.
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);

            this.EnableJqueryMigrate = false;
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.LoadComplete += new System.EventHandler(this.LoadCompleteHandler);
        }
        #endregion
    }
}
