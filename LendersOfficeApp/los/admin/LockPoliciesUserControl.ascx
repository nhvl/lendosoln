﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LockPoliciesUserControl.ascx.cs" Inherits="LendersOfficeApp.los.admin.LockPoliciesUserControl" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script>
    function f_editPolicy(policyid) {
        showPolicyEditor("edit", policyid);
    }
    function f_duplicatePolicy(policyid) {
        showPolicyEditor("dup", policyid);
    }
    function f_deletePolicy(policyid, policynm) {
        if (confirm("Delete policy " + policynm + "?") == "Y") {
            document.getElementById(<%= AspxTools.JsString(m_PolicyToDelete.ClientID) %>).value = policyid;
            ApplyNow(function () {
                Tab_LoadData($("#tab" + oldTabIndex).attr("data-content"));
            });
        }
    }
    function f_newPolicy() {
        showPolicyEditor("new");
    }
    function LockPolicies_loaded()
    {
        var errorMessage = document.getElementById(<%= AspxTools.JsString(ErrorMessage.ClientID) %>);
        if(errorMessage && errorMessage.value != '')
        {
            alert(errorMessage.value);
            errorMessage.value = '';
        }
        if(this.LOCKPOLICIES)
            LOCKPOLICIES.fn.ReloadTable();
    }
    function showPolicyEditor(command, policyid) {
        var url = "/los/admin/EditLockPolicy.aspx?cmd="+command;
        if(policyid)
            url += "&policyid="+policyid.toString();

        var settings = 'dialogWidth:1000px;dialogHeight:800px;center:yes;resizable:yes;scroll:yes;status=yes;help=no;';

        return showModal(url, null, settings, null, function () { Tab_LoadData($("#tab" + oldTabIndex).attr("data-content")); }, {hideCloseButton:true});
    }
    function f_viewHolidays(policyid) {
        var url = "/los/admin/HolidayLockDeskClosuresPopup.aspx?policyid="+policyid;
        showModal(url, null, 'status:false;dialogWidth:550px;dialogHeight:250px');
    }
    function f_onDisablePricingClick(policyid)
    {
        showModal('/los/Template/ListDisabledProductInvestor.aspx?policyid='+policyid, null, null, null, null, {hideCloseButton:true});
        return false;
    }
</script>

<asp:HiddenField ID="ErrorMessage" runat="server" />
<asp:HiddenField runat="server" ID="m_PolicyToDelete" />
<asp:HiddenField runat="server" ID="LockPolicyList" />
<asp:Panel ID="BasePanel" runat="server" Style="padding-right: 8px; padding-left: 8px; padding-bottom: 8px; padding-top: 8px; width:100%">
    <table class="DataGrid" border="1" cellpadding="1" cellspacing="0" style="border-collapse: collapse; text-align:center;">
        <tr class="GridHeader" style="border: solid 1px white">
            <td style="width:50px"></td>
            <td style="width:100px">Lock Policy Name</td>
            <td style="width:160px">Lock Desk Hours</td>
            <td style="width:160px">Lock Extensions</td>
            <td style="width:160px">Float Downs</td>
            <td style="width:160px">Rate Re-Locks<br />(Currently Expired/Canceled Rates)</td>
            <td style="width:50px"></td>
        </tr>
        <tbody id="m_LockPoliciesTable"></tbody>
    </table>
    <input type="button" style="margin:8px;" onclick="f_newPolicy()" value="Add New Lock Policy"/>
</asp:Panel>
