using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{
	public partial class EditDefaultRolePermissions : LendersOffice.Common.BaseServicePage
	{
		protected DefaultRolePermissionsEdit m_Edit;

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{
			set { ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." ); }
		}

        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }
        

		/// <summary>
		/// Initialize this form.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			m_Edit.BrokerId = BrokerUser.BrokerId;	
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}

}
