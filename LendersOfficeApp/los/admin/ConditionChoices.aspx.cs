using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
using System.Text;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{

	public partial class ConditionChoices : LendersOffice.Common.BasePage
	{

		private ChoiceList m_List = new ChoiceList();

		private BrokerUserPrincipal CurrentUser
		{
			// Fetch the current principal.
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{
			// Write out error message.

			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		private String CommandToDo
		{
			// Write out error message.

			set
			{
				ClientScript.RegisterHiddenField( "m_commandToDo" , value );
			}
		}

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingConditionChoices
                };
            }
        }

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Initialize this view.

			try
			{
				// Load the broker to get the choices.

				BrokerDB myBroker = BrokerDB.RetrieveById( CurrentUser.BrokerId );


				if( myBroker.HasLenderDefaultFeatures == false)
				{
					throw new CBaseException(ErrorMessages.GenericAccessDenied, "Attempt to load page without permission");
				}

				if( IsPostBack == false )
				{
					// 12/17/2004 kb - Get the starting list from the
					// broker's set.

					m_List = myBroker.ConditionChoices;
				}
				else
				{
					// 12/17/2004 kb - Pull the entries from the grid,
					// which was seeded with the last posted set and
					// has any edits overlaid on top.

					m_List.Choices.Clear();

					foreach( DataGridItem dgItem in m_Grid.Items )
					{
						TextBox condBox = dgItem.FindControl( "Condition" ) as TextBox;
						TextBox catgBox = dgItem.FindControl( "Category"  ) as TextBox;

						if( condBox != null && catgBox != null )
						{
							m_List.Add
								( catgBox.Text.TrimWhitespaceAndBOM()
								, condBox.Text.TrimWhitespaceAndBOM()
								);
						}
					}
				}
			}
			catch( CBaseException e )
			{
				// Oops!

				Tools.LogError( "Failed to load page." , e );

				ErrorMessage = e.UserMessage;

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to load page." , e );

				ErrorMessage = "Failed to load page.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Overload framework event for loading.
		/// </summary>

		protected override void LoadViewState( object oState )
		{
			// Get the view state of a previous postback and restore
			// the data grid's state prior to loading form variables.

			try
			{
				base.LoadViewState( oState );

				if( ViewState[ "Cache" ] != null )
				{
					m_List = ChoiceList.ToObject( ViewState[ "Cache" ].ToString() );
				}

				m_Grid.DataSource = m_List.Choices;
				m_Grid.DataBind();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to load viewstate." , e );

				ErrorMessage = "Failed to load page.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Overload framework event for saving.
		/// </summary>

		protected override object SaveViewState()
		{
			// Store what we have as latest in the viewstate so
			// we can stay current between postbacks without
			// hitting the database.
			ViewState.Add( "Cache" , m_List.ToString() );

			return base.SaveViewState();
		}

		/// <summary>
		/// Bind this page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Bind this view to our data.

			try
			{
				// Bind and let grid pull current choices.  We
				// apply the current sorting settings to the
				// list prior to binding.  On save, this is
				// the order we persist.

				if( m_List.Choices.Count == 0 )
				{
					m_EmptyMessage.Visible = true;
				}

				m_Grid.DataSource = m_List.Choices;
				m_Grid.DataBind();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to prerender page." , e );

				ErrorMessage = "Failed to render page.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void GridItemBound( object sender , System.Web.UI.WebControls.DataGridItemEventArgs a )
		{
			// 12/17/2004 kb - Bind the current choice item to
			// the grid.  We may get to the point where we keep
			// all the descriptions of the same category in
			// one binding object.  At that point, we'll bind
			// to a nested list here.

			ChoiceItem cItem = a.Item.DataItem as ChoiceItem;

			if( cItem != null )
			{
				TextBox condBox = a.Item.FindControl( "Condition" ) as TextBox;
				TextBox catgBox = a.Item.FindControl( "Category"  ) as TextBox;

				if( catgBox != null && condBox != null )
				{
					catgBox.Text = cItem.Category;
					condBox.Text = cItem.Description;
				}
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void GridItemClick( object sender , System.Web.UI.WebControls.DataGridCommandEventArgs a )
		{
			// Handle delete command.  We use the position of the
			// row as our key.

			if( a.CommandName.ToLower() == "remove" )
			{
				try
				{
					m_List.Choices.RemoveAt( a.Item.ItemIndex );

					m_IsDirty.Value = "Dirty";
				}
				catch( Exception e )
				{
					// Oops!

					Tools.LogError( "Failed to delete condition choice." , e );

					ErrorMessage = "Failed to remove item.";

					m_Ok.Enabled    = false;
					m_Apply.Enabled = false;
					m_Add.Enabled   = false;
				}
			}
		}

		protected void ApplyClick( object sender , System.EventArgs a )
		{
			// Create new choice list and save it to the broker.

			try
			{
				// Store the cached list directly in the broker
				// and save.
				//
				// 8/16/2005 kb - Updated save to go direct to
				// the db.  Our current update is colliding with
				// view updating; large employee counts makes
				// this approach prohibitive.

                BrokerDB brokerDb = BrokerDB.RetrieveById(CurrentUser.BrokerId);

                if (brokerDb.HasLenderDefaultFeatures == false)
                {
                    throw new CBaseException(ErrorMessages.GenericAccessDenied, "Attempt to save changes without permission");
                }

                brokerDb.ConditionChoices = m_List;

                Tools.LogInfo("Saving condition choices for " + brokerDb.CustomerCode + ": " + brokerDb.ConditionChoices.ToString());

                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", CurrentUser.BrokerId)
                    , new SqlParameter("@ConditionChoicesXmlContent", brokerDb.ConditionChoices.ToString())
                                            };
                StoredProcedureHelper.ExecuteNonQuery(brokerDb.BrokerID, "UpdateBrokerConditionChoices", 3, parameters);

				m_IsDirty.Value = "";
			}
			catch( CBaseException e )
			{
				// Oops!

				Tools.LogError( "Failed to save condition choices." , e );

				ErrorMessage = e.UserMessage;

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to save condition choices." , e );

				ErrorMessage = "Failed to save.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void OkClick( object sender , System.EventArgs a )
		{
			// Create new choice list and save it to the broker.

			try
			{
				// Store the cached list directly in the broker
				// and save.

				ApplyClick( sender , a );

				// We close on successful saving.

				CommandToDo = "Close";
			}
			catch( CBaseException e )
			{
				// Oops!

				Tools.LogError( "Failed to save condition choices." , e );

				ErrorMessage = e.UserMessage;

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to save condition choices." , e );

				ErrorMessage = "Failed to save.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void SortClick( object sender , System.EventArgs a )
		{
			// Add a new entry to our cached instance.

			try
			{
				// Create new entry only if the strings are valid.

				m_IsDirty.Value = "Dirty";

				if( m_ByCondition.Checked == true && m_ByCategory.Checked == true )
				{
					if( m_CatgFirst.Checked == true )
					{
						m_List.Sort( 0 , 1 );
					}
					else
					{
						m_List.Sort( 1 , 0 );
					}
				}
				else
					if( m_ByCondition.Checked == true )
				{
					m_List.Sort( 1 );
				}
				else
					if( m_ByCategory.Checked == true )
				{
					m_List.Sort( 0 );
				}

			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to add new choice." , e );

				ErrorMessage = "Failed to add.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void AddClick( object sender , System.EventArgs a )
		{
			// Add a new entry to our cached instance.

			try
			{
				// Create new entry only if the strings are valid.

				m_IsDirty.Value = "Dirty";

				m_List.Push( "" , "" );
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to add new choice." , e );

				ErrorMessage = "Failed to add.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}
		/// <summary>
		/// Handles Export Click Event.
		/// </summary>
		protected void ExportClick(object sender, System.EventArgs a)
		{
			try 
			{
				//CommonLib.CSVWriter changes double quotes to single quotes which is why I did not use it. -av
				StringBuilder CSVFormat = new StringBuilder(FormatCSVLine("Category","Condition"));

				foreach (ChoiceItem item in m_List.Choices) 
				{
					//if both fields are blank dont parse them. 
					if( ! ( item.Category.Length == 0 && item.Description.Length == 0 ) ) 
						CSVFormat.Append(FormatCSVLine(item.Category,item.Description));
				}
														 
				Response.AddHeader("Content-disposition","attachment; filename=\"condtionchoices.csv\"");
				Response.ContentType = "application/octet-stream";
				Response.Write(CSVFormat.ToString());
                Response.Flush();
				Response.End(); //needed so the file is no longer created. 
			} 
			//Response.End generates this exceptions its normal. 
			catch (System.Threading.ThreadAbortException tae) 
			{
				throw tae;
			}
			catch (Exception e) 
			{
				Tools.LogError( "Failed to Export CSV Data." , e);
				ErrorMessage = "Failed to Export CSV Data"; 
			}
		}
		
		/// <summary>
		/// Escapes double quotes by adding another double quote. Removes Carriage Returns (\r)
		/// which cause problems in excel ( displays a box instead of just a new line ).
		/// </summary>
		/// <param name="inString">String that may contain double quotes.</param>
		/// <returns>a_sIn with double quote followed by another double quote.</returns>
		private string PrepareCSVItem(string inString) 
		{
			string sSafeVal = inString.Replace("\"", "\"\"");
			return sSafeVal.Replace("\r","");
		}

	
		/// <summary>
		/// Composes a CSV Line from the given strings. Requires at least one string to be passed in.
		/// </summary>
		/// <param name="sFirstItem">The First Unescaped Raw String</param>
		/// <param name="remainingItems">The remaining Unescaped, Raw Strings</param>
		/// <returns>CSV Formatted Delimited String or </returns>
		private string FormatCSVLine(string sFirstItem, params string[] remainingItems) 
		{
			StringBuilder sb = new StringBuilder(); 
			
			//first item does not require a comma
			sb.Append("\"" + PrepareCSVItem(sFirstItem) + "\"");
			
			foreach ( string sItem in remainingItems ) 
			{
				sb.Append(",\"" + PrepareCSVItem(sItem) + "\"");
			}
			sb.Append("\n");
	
			return sb.ToString();
		}
	}

}
