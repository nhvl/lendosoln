namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.LockPolicies;
    using LendersOffice.PdfForm;
    using LendersOffice.Security;
    using LendersOfficeApp.los.BrokerAdmin;

    public partial class BrokerInfoService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private BrokerDB Broker
        {
            get { return this.BrokerUser.BrokerDB; }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "BrokerInformation_LoadData":
                    LoadCurrentBroker();
                    break;
                case "BrokerInformation_SaveData":
                    BrokerInformation_SaveData();
                    break;
                case "BrokerOption_LoadData":
                    LoadCurrentBroker();
                    break;
                case "BrokerOption_SaveData":
                    BrokerOption_SaveData();
                    break;
                case "NamingScheme_LoadData":
                    LoadCurrentBroker();
                    break;
                case "NamingScheme_SaveData":
                    NamingScheme_SaveData();
                    break;
                case "PasswordOptions_ApplyNow":
                    PasswordOptions_ApplyNow();
                    break;
                case "BrokerECOA_LoadData":
                    LoadCurrentBroker();
                    break;
                case "BrokerECOA_SaveData":
                    BrokerECOA_SaveData();
                    break;
                case "BrokerFairLending_LoadData":
                    LoadCurrentBroker();
                    break;
                case "BrokerFairLending_SaveData":
                    BrokerFairLending_SaveData();
                    break;
                case "BrokerCRA_LoadData":
                    LoadCurrentBroker();
                    BrokerCRA_LoadData();
                    break;
                case "BrokerCRA_SaveData":
                    BrokerCRA_SaveData();
                    break;
                case "LendingLicense_LoadData":
                    LendingLicense_LoadData();
                    break;
                case "LendingLicense_SaveData":
                    LendingLicense_SaveData();
                    break;
                case "LockPolicies_LoadData":
                    LockPolicies_LoadData();
                    break;
                case "LockPolicies_ApplyNow":
                    LockPolicies_ApplyNow();
                    break;
                case "GlobalWhiteList_EditItem":
                    GlobalWhiteList_EditItem();
                    break;
                case "GlobalWhiteList_LoadData":
                    GlobalWhiteList_LoadData();
                    break;
                case "GlobalWhiteList_Delete":
                    GlobalWhiteList_Delete();
                    break;
            }
        }

        private void GlobalWhiteList_EditItem()
        {
            int id = GetInt("Id", -1);
            string description = GetString("Description");
            string ipAddress = GetString("IpAddress");

            BrokerGlobalIpWhiteList item = null;

            if (id == -1)
            {
                item = new BrokerGlobalIpWhiteList();
            }
            else
            {
                item = BrokerGlobalIpWhiteList.RetrieveById(BrokerUser.BrokerId, id);
            }

            item.Description = description;
            item.IpAddress = ipAddress;
            item.Save(BrokerUser);
        }
        private void GlobalWhiteList_Delete()
        {

            int id = GetInt("Id", -1);
            if (id != -1)
            {
                BrokerGlobalIpWhiteList.Delete(BrokerUser.BrokerId, id);
            }
        }
        private void GlobalWhiteList_LoadData()
        {
            IEnumerable<BrokerGlobalIpWhiteList> list = BrokerGlobalIpWhiteList.ListByBrokerId(BrokerUser.BrokerId);

            string json = ObsoleteSerializationHelper.JsonSerialize(list);

            SetResult("WhiteListJson", json);
        }
        private void LendingLicense_LoadData()
        {
            BrokerDB broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);


            string prefix = "lp0";
            string json = LicenseInfoList.JsonSerialize(broker.LicenseInformationList);
            SetResult(prefix + "LendingLicenseList", json);

            SetResult("tbNmlsIdentifier", broker.NmLsIdentifier);
        }

        private void LendingLicense_SaveData()
        {
            BrokerDB broker = this.Broker;

            try
            {
                string prefix = "lp0";
                string json = GetString(prefix + "LendingLicenseList");
                broker.LicenseInformationList = LicenseInfoList.JsonDeserialize(json);

                broker.NmLsIdentifier = GetString("tbNmlsIdentifier");
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
            }

            bool isValid = broker.Save();
            if (!isValid)
                throw new CBaseException(ErrorMessages.Generic, "Unable to save broker licenses.");
        }

        private void LoadCurrentBroker()
        {
            BrokerDB broker = this.Broker;
            SetResult("IsQuickPricerEnabled", broker.IsQuickPricerEnable);
            SetResult("QuickPricerNoResultMessage", broker.QuickPricerNoResultMessage);
            SetResult("QuickPricerDisclaimerAtResult", broker.QuickPricerDisclaimerAtResult);
            SetResult("QuickPricerTemplateId", broker.QuickPricerTemplateId);
            SetResult("CustomerCode", broker.CustomerCode);
            SetResult("Name", broker.Name);
            SetResult("Address", broker.Address.StreetAddress);
            SetResult("City", broker.Address.City);
            SetResult("Zipcode", broker.Address.Zipcode.TrimWhitespaceAndBOM());
            SetResult("State", broker.Address.State);
            SetResult("Phone", broker.Phone);
            SetResult("Fax", broker.Fax);
            SetResult("Url", broker.Url);
            SetResult("FhaLenderId", broker.FhaLenderId);
            SetResult("IsHideFhaLenderIdInTotalScorecard", broker.IsHideFhaLenderIdInTotalScorecard);
            SetResult("BrokerLpePriceGroupIdDefault", broker.LpePriceGroupIdDefault.ToString());
            SetResult("LpeSubmitAgreement", broker.LpeSubmitAgreement);
            SetResult("FthbCustomDefinition", broker.FthbCustomDefinition);
            SetResult("ECOAAddress", broker.ECOAAddressDefault);
            SetResult("EmployeeResourcesUrl", broker.EmployeeResourcesUrl);
            if (broker.FairLendingNoticeAddress != null)
            {
                for (int i = 1; i <= broker.FairLendingNoticeAddress.Length; ++i)
                {
                    SetResult("AddressLine" + i, broker.FairLendingNoticeAddress[i - 1]);
                }
            }
            if (broker.AreAutomatedClosingCostPagesVisible && ((broker.IsNewPmlUIEnabled || EmployeeDB.RetrieveById(BrokerUser.BrokerId, BrokerUser.EmployeeId).IsNewPmlUIEnabled)))
            {
                SetResult("m_CalculateclosingCostInPML", broker.CalculateclosingCostInPML ? "Y" : "N");

                SetResult("m_ApplyClosingCostToGFE", broker.ApplyClosingCostToGFE ? "Y" : "N");
            }
            SetResult("TelemarketerCanOnlyAssignToManager", broker.OptionTelemarketerCanOnlyAssignToManager ? "Y" : "N");
            SetResult("TelemarketerCanRunPrequal", broker.OptionTelemarketerCanRunPrequal ? "Y" : "N");
            SetResult("IsLOAllowedToEditProcessorAssignedFile", broker.IsLOAllowedToEditProcessorAssignedFile ? "Y" : "N");
            SetResult("IsNonAccountantCanModifyTrustAccount", broker.IsOnlyAccountantCanModifyTrustAccount ? "N" : "Y");
            SetResult("AllowCreatingLoanFromBlankTemplate", broker.IsBlankTemplateInvisibleForFileCreation ? "N" : "Y");
            SetResult("IsAEAsOfficialLoanOfficer", broker.IsAEAsOfficialLoanOfficer ? "Y" : "N");
            //SetResult("IsAsk3rdPartyUwResultInPml", broker.IsAsk3rdPartyUwResultInPml ? "Y" : "N"); OPM 113409
            SetResult("IsAutoGenerateLiabilityPayOffConditions", broker.IsAutoGenerateLiabilityPayOffConditions ? "Y" : "N");  // 4/25/2006 mf - OPM 4182
            SetResult("IsAllowExternalUserToCreateNewLoan", broker.IsAllowExternalUserToCreateNewLoan ? "Y" : "N"); // 07/27/06 mf - OPM 6682
            SetResult("IsShowPriceGroupInEmbeddedPml", broker.IsShowPriceGroupInEmbeddedPml ? "Y" : "N"); //09/05/06 mf - OPM 7259
            SetResult("IsLpeManualSubmissionAllowed", broker.IsLpeManualSubmissionAllowed ? "Y" : "N"); //11/16/06 mf - OPM 5048
            SetResult("IsPmlPointImportAllowed", broker.IsPmlPointImportAllowed ? "Y" : "N"); //11/16/06 mf - OPM 4565
            SetResult("m_RequireVerifyLicenseByState", broker.RequireVerifyLicenseByState ? "Y" : "N"); //OPM 2703
            SetResult("IsAllPrepaymentPenaltyAllowed", broker.IsAllPrepaymentPenaltyAllowed ? "N" : "Y"); //5/17/07 db - OPM 12943
            SetResult("IsDuplicateLoanCreatedWAutonameBit", broker.IsDuplicateLoanCreatedWAutonameBit);
            SetResult("Is2ndLoanCreatedWAutonameBit", broker.Is2ndLoanCreatedWAutonameBit);
            SetResult("m_EnforceLockDeskHourForNormalUser", broker.LpeIsEnforceLockDeskHourForNormalUser ? "Y" : "N"); // 1/11/08 db - OPM 19717
            //SetResult("m_IsUsingRateSheetExpirationFeature", broker.UseRateSheetExpirationFeature ? "Y" : "N"); // 1/11/08 db - OPM 19717
            SetResult("IsStandAloneSecondAllowedInPml", broker.IsStandAloneSecondAllowedInPml ? "Y" : "N"); // 1/28/08 db - OPM 18430

            SetResult("IsHelocsAllowedInPml", broker.IsHelocsAllowedInPml ? "Y" : "N"); // OPM 187671

            SetResult("Is8020ComboAllowedInPml", broker.Is8020ComboAllowedInPml ? "Y" : "N"); // 1/28/08 db - OPM 19784
            SetResult("m_timezone", broker.TimezoneForRsExpiration); // OPM 20141
            SetResult("IsBestPriceEnabled", broker.IsBestPriceEnabled ? "Y" : "N"); //opm 22180 av 05 28 08
            //SetResult("m_displayPmlFeeIn100Format", broker.DisplayPmlFeeIn100Format ? "Y" : "N"); //1/4/08 db - OPM 19525
            SetResult("m_ShowUnderwriterInPMLCertificate", broker.ShowUnderwriterInPMLCertificate ? "Y" : "N"); //OPM 12711 7/16/08
            SetResult("m_ShowJuniorUnderwriterInPMLCertificate", broker.ShowJuniorUnderwriterInPMLCertificate ? "Y" : "N"); // 11/21/2013 gf - opm 145015
            SetResult("m_ShowProcessorInPMLCertificate", broker.ShowProcessorInPMLCertificate ? "Y" : "N"); //OPM 12711 7/16/08
            SetResult("m_ShowJuniorProcessorInPMLCertificate", broker.ShowJuniorProcessorInPMLCertificate ? "Y" : "N"); // 11/21/2013 gf - opm 145015
            SetResult("m_IsAlwaysUseBestPrice", broker.IsAlwaysUseBestPrice ? "Y" : "N"); //OPM 24214             
            SetResult("m_TriggerAprRediscNotifForAprDecrease", broker.TriggerAprRediscNotifForAprDecrease ? "Y" : "N"); // 8/30/2013 gf - OPM 118941
            SetResult("FnmaEarlyCheckUserName", broker.FnmaEarlyCheckUserName); // 9/24/2013 dd - OPM 106845
            SetResult("FnmaEarlyCheckInstutionId", broker.FnmaEarlyCheckInstutionId); // 9/24/2013 dd - OPM 106845
            this.SetResult(nameof(broker.LegalEntityIdentifier), broker.LegalEntityIdentifier);

            this.SetResult("ShowLoanProductIdentifier", broker.ShowLoanProductIdentifier ? "Y" : "N");
            if (broker.NamingPattern != null)
            {
                var prefix = GetCorporatePrefix(broker.NamingPattern);
                SetResult("PrefixTF", prefix);

                SetResult("IsAutoGenerateMersMinCB", broker.IsAutoGenerateMersMin);
                if (broker.IsAutoGenerateMersMin)
                {
                    if (string.IsNullOrEmpty(broker.MersFormat))
                    {
                        SetResult("MERSGenType", "UseSequentialMERS");
                    }
                    else
                    {
                        SetResult("MERSGenType", "UseLoanNumberMERS");
                        if (Tools.UsesPrefix(broker.MersFormat))
                        {
                            SetResult("IncludePrefixType", "IncludePrefixY");
                        }
                        else
                        {
                            SetResult("IncludePrefixType", "IncludePrefixN");
                        }
                    }

                }

                bool useLeadNaming = false;
                bool useTestNaming = false;
                bool checkNameOnly = true;

                var loanNamingData = new CLoanFileNamer.LoanNamingData(BrokerUser.BrokerId, BrokerUser.BranchId,
                                        useLeadNaming, useTestNaming, broker.IsAutoGenerateMersMin, checkNameOnly);

                if (broker.IsAutoGenerateMersMin)
                {
                    SetResult("MersOrganizationId", broker.MersOrganizationId);
                    SetResult("MersStartingCounter", broker.MersCounter + 1);
                    SetResult("NextMersMIN", MersUtilities.GenerateMersNumber(loanNamingData));
                }

                SetResult("NextLoanName", CLoanFileNamer.CreateLoanName(loanNamingData));
                var patternSetupActive = false;

                if (broker.NamingPattern.IndexOf("{mers") != -1)
                {
                    SetResult("NamingSchemeChoice", "UseMersRB");
                    SetResult("PreviousNamingSchemeChoice", "mers");
                    if (broker.NamingPattern.Contains("{mersID}")) patternSetupActive = true;
                }
                else
                {
                    SetResult("NamingSchemeChoice", "UseSequentialRB");
                    SetResult("PreviousNamingSchemeChoice", "sequential");
                    patternSetupActive = true;
                }

                if (patternSetupActive)
                {
                    SetNamingPattern(broker.NamingPattern);
                }
            }
        }

        private string GetCorporatePrefix(string pattern)
        {
            return Tools.GetCorporatePrefix(pattern);
        }

        private void SetNamingPattern(string pattern)
        {
            if (pattern.IndexOf("{mm}") != -1)
            {
                SetResult("MonthCB", true);
                SetResult("ChooseMonth", "2M");
            }
            else if (pattern.IndexOf("{m}") != -1)
            {
                SetResult("MonthCB", true);
                SetResult("ChooseMonth", "1M");
            }

            if (pattern.IndexOf("{yyyy}") != -1)
            {
                SetResult("YearCB", true);
                SetResult("ChooseYear", "4Y");
            }
            else if (pattern.IndexOf("{yy}") != -1)
            {
                SetResult("YearCB", true);
                SetResult("ChooseYear", "2Y");
            }
            else if (pattern.IndexOf("{y}") != -1)
            {
                SetResult("YearCB", true);
                SetResult("ChooseYear", "1Y");
            }

            if (pattern.IndexOf("{d}") != -1 || pattern.IndexOf("{dd}") != -1)
            {
                SetResult("DayCB", true);
            }

            if (pattern.IndexOf("{c:") != -1)
            {
                int _startIndex = pattern.IndexOf("{c:");
                int _endIndex = pattern.IndexOf("}", _startIndex);
                string str = pattern.Substring(_startIndex, _endIndex - _startIndex + 1);
                SetResult("CounterDigits", str);

            }
        }

        private void BrokerInformation_SaveData()
        {
            BrokerDB broker = this.Broker;
            CommonLib.Address address = new CommonLib.Address();
            address.StreetAddress = GetString("Address");
            address.City = GetString("City");
            address.State = GetString("State");
            address.Zipcode = GetString("Zipcode");
            broker.Address = address;
            broker.Name = GetString("Name");
            broker.Phone = GetString("Phone");
            broker.Fax = GetString("Fax");
            broker.Url = GetString("Url");
            broker.LegalEntityIdentifier = this.GetString(nameof(broker.LegalEntityIdentifier));

            if (broker.IsTotalScorecardEnabled)
            {
                broker.FhaLenderId = GetString("FhaLenderId");
                broker.IsHideFhaLenderIdInTotalScorecard = GetBool("IsHideFhaLenderIdInTotalScorecard");
            }

            bool isValid = broker.Save();
            if (!isValid)
                throw new CBaseException(ErrorMessages.Generic, "Unable to save broker information.");
        }

        private void BrokerOption_SaveData()
        {
            BrokerDB broker = this.Broker;

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.MarketingTools))
                broker.OptionTelemarketerCanOnlyAssignToManager = (GetString("TelemarketerCanOnlyAssignToManager") == "Y");

            if(  BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) 
                || BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine))
            {
                broker.EmployeeResourcesUrl = GetString("EmployeeResourcesUrl");
            }
            broker.IsLOAllowedToEditProcessorAssignedFile = (GetString("IsLOAllowedToEditProcessorAssignedFile") == "Y");
            broker.IsOnlyAccountantCanModifyTrustAccount = (GetString("IsNonAccountantCanModifyTrustAccount") == "N");
            broker.IsBlankTemplateInvisibleForFileCreation = (GetString("AllowCreatingLoanFromBlankTemplate") == "N");
            broker.IsAEAsOfficialLoanOfficer = (GetString("IsAEAsOfficialLoanOfficer") == "Y");
            broker.IsAutoGenerateLiabilityPayOffConditions = (GetString("IsAutoGenerateLiabilityPayOffConditions") == "Y");

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            {

                if (broker.AreAutomatedClosingCostPagesVisible && (broker.IsNewPmlUIEnabled || EmployeeDB.RetrieveById(BrokerUser.BrokerId, BrokerUser.EmployeeId).IsNewPmlUIEnabled))
                {
                    broker.CalculateclosingCostInPML = GetString("m_CalculateclosingCostInPML") == "Y";
                    broker.ApplyClosingCostToGFE = GetString("m_ApplyClosingCostToGFE") == "Y" && broker.CalculateclosingCostInPML;
                }

                broker.IsQuickPricerEnable = GetBool("IsQuickPricerEnabled");
                broker.QuickPricerTemplateId = GetGuid("QuickPricerTemplateId");
                string quickpricerNRM = GetString("QuickPricerNoResultMessage");
                if (quickpricerNRM.Length > 500)
                {
                    quickpricerNRM = quickpricerNRM.Substring(0, 500);
                }

                string quickpricerD = GetString("QuickPricerDisclaimerAtResult");
                if (quickpricerD.Length > 500)
                {
                    quickpricerD = quickpricerD.Substring(0, 500);
                }
                broker.QuickPricerNoResultMessage = quickpricerNRM;

                broker.QuickPricerDisclaimerAtResult = quickpricerD;
            }
            int fthbCustomDefinitionMaxLength = 1000;
            if (GetString("FthbCustomDefinition", "").Length > fthbCustomDefinitionMaxLength)
            {
                throw new CBaseException("The FTHB and Housing History Help Text has surpassed the " + fthbCustomDefinitionMaxLength + " character limit.", "FTHB and Housing Text is too long.");
            }
            else
            {
                broker.FthbCustomDefinition = GetString("FthbCustomDefinition", "");
            }


            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) || BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine))
            {
                int lpeSubmitAgreementLength = 1500;
                broker.LpePriceGroupIdDefault = GetGuid("BrokerLpePriceGroupIdDefault", Guid.Empty);
                if (GetString("LpeSubmitAgreement", "").Length > lpeSubmitAgreementLength)
                    throw new CBaseException("The Loan Submission Agreement has surpassed the " + lpeSubmitAgreementLength + " character limit.", "LpeSubmitAgreement is too long");
                else
                    broker.LpeSubmitAgreement = GetString("LpeSubmitAgreement", "");
                //broker.IsAsk3rdPartyUwResultInPml = (GetString("IsAsk3rdPartyUwResultInPml") == "Y"); OPM 113409
                broker.RequireVerifyLicenseByState = (GetString("m_RequireVerifyLicenseByState") == "Y");

                broker.IsHelocsAllowedInPml = (GetString("IsHelocsAllowedInPml") == "Y");

                broker.IsStandAloneSecondAllowedInPml = (GetString("IsStandAloneSecondAllowedInPml") == "Y");
                broker.Is8020ComboAllowedInPml = (GetString("Is8020ComboAllowedInPml") == "Y");
                broker.IsAllPrepaymentPenaltyAllowed = (GetString("IsAllPrepaymentPenaltyAllowed") == "N");
                if (GetString("IsBestPriceEnabled", string.Empty) != string.Empty)  //av 22180 
                {
                    broker.IsBestPriceEnabled = (GetString("IsBestPriceEnabled") == "Y");
                }
                //broker.DisplayPmlFeeIn100Format = (GetString("m_displayPmlFeeIn100Format") == "Y");
                broker.ShowUnderwriterInPMLCertificate = (GetString("m_ShowUnderwriterInPMLCertificate") == "Y"); //OPM 12711 7/16/08
                broker.ShowJuniorUnderwriterInPMLCertificate = (GetString("m_ShowJuniorUnderwriterInPMLCertificate") == "Y"); // 11/21/2013 gf - opm 145015
                broker.ShowProcessorInPMLCertificate = (GetString("m_ShowProcessorInPMLCertificate") == "Y"); //OPM 12711 7/16/08
                broker.ShowJuniorProcessorInPMLCertificate = (GetString("m_ShowJuniorProcessorInPMLCertificate") == "Y"); // 11/21/2013 gf - opm 145015
                broker.IsAlwaysUseBestPrice = (GetString("m_IsAlwaysUseBestPrice") == "Y"); //24214

            }

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            {
                broker.IsAllowExternalUserToCreateNewLoan = (GetString("IsAllowExternalUserToCreateNewLoan") == "Y");
                if (BrokerUser.HasFeatures(E_BrokerFeatureT.MarketingTools))
                {
                    broker.OptionTelemarketerCanRunPrequal = (GetString("TelemarketerCanRunPrequal") == "Y");
                }
                broker.IsShowPriceGroupInEmbeddedPml = (GetString("IsShowPriceGroupInEmbeddedPml") == "Y");
                broker.IsPmlPointImportAllowed = (GetString("IsPmlPointImportAllowed") == "Y");
                broker.IsLpeManualSubmissionAllowed = (GetString("IsLpeManualSubmissionAllowed") == "Y");
            }

            broker.DefaultLockDeskID = GetGuid("m_lockDeskChoice_m_Data", Guid.Empty);
            broker.TriggerAprRediscNotifForAprDecrease = GetBoolYN("m_TriggerAprRediscNotifForAprDecrease"); // 8/30/2013 gf - OPM 118941
            broker.FnmaEarlyCheckUserName = GetString("FnmaEarlyCheckUserName");
            broker.FnmaEarlyCheckInstutionId = GetString("FnmaEarlyCheckInstutionId");
            broker.FnmaEarlyCheckDecryptedPassword = GetString("FnmaEarlyCheckDecryptedPassword");

            if (broker.ExposeClientFacingWorkflowConfiguration)
            {
                broker.WorkflowRulesControllerId = this.GetGuid("WorkflowRulesControllerId", Guid.Empty);
            }

            broker.ShowLoanProductIdentifier = this.GetBoolYN("ShowLoanProductIdentifier");

            bool isValid = broker.Save();
            if (!isValid)
                throw new CBaseException(ErrorMessages.Generic, "Unable to save broker option.");
        }

        /// <summary>
        /// Return true if value of fieldId == Y else false.
        /// </summary>
        /// <param name="fieldId">Value of field</param>
        /// <returns></returns>
        private bool GetBoolYN(string fieldId)
        {
            return GetString(fieldId) == "Y";
        }
        private void NamingScheme_SaveData()
        {
            BrokerDB broker = this.Broker;
            string namingPattern = GetNamingPattern();

            broker.IsDuplicateLoanCreatedWAutonameBit = GetBool("IsDuplicateLoanCreatedWAutonameBit");
            broker.Is2ndLoanCreatedWAutonameBit = GetBool("Is2ndLoanCreatedWAutonameBit");

            bool isMersUsedForNewLoanName = GetString("NamingSchemeChoice") == "UseMersRB";
            var sequentialMers = GetString("MERSGenType", "UseSequentialMERS") == "UseSequentialMERS";

            bool isAutoGenerateMersMinCB = GetBool("IsAutoGenerateMersMinCB");
            broker.IsAutoGenerateMersMin = isAutoGenerateMersMinCB;

            if (isAutoGenerateMersMinCB)
            {
                broker.MersOrganizationId = GetString("MersOrganizationId");

                if (sequentialMers)
                {
                    broker.MersFormat = "";
                    broker.MersCounter = GetLong("MersStartingCounter") - 1;
                }
                else
                {
                    var mersPattern = namingPattern;
                    if (GetString("IncludePrefixType", "IncludePrefixY") != "IncludePrefixY")
                    {
                        var prefixIdx = namingPattern.IndexOf('{');
                        mersPattern = namingPattern.Substring(prefixIdx);
                    }

                    broker.MersFormat = "{mersID}" + mersPattern;
                }
            }

            if (isMersUsedForNewLoanName)
            {
                if (sequentialMers)
                {
                    broker.NamingPattern = "{mers:" + GetString("MersOrganizationId") + "}";
                    broker.NamingCounter = GetLong("MersStartingCounter") - 1;
                }
                else
                {
                    if (GetString("PreviousNamingSchemeChoice") == "mers")
                    {
                        broker.NamingCounter = 0;
                    }

                    broker.NamingPattern = broker.MersFormat;
                }
            }
            else
            {

                if (GetString("PreviousNamingSchemeChoice") == "mers")
                {
                    // Reset the counter if user switch to sequential scheme.
                    broker.NamingCounter = 0;
                }

                broker.NamingPattern = namingPattern;
            }

            // OPM 451012 - Copy the broker's loan naming pattern to their lead pattern
            // to keep the two in sync.
            broker.LeadNamingPattern = broker.NamingPattern;

            bool isValid = broker.Save();
            if (!isValid)
                throw new CBaseException(ErrorMessages.Generic, "Unable to save naming scheme.");
        }

        private string GetNamingPattern()
        {
            string namingPattern = "";

            namingPattern += GetString("PrefixTF").TrimWhitespaceAndBOM();

            if (GetBool("YearCB") == true)
            {
                switch (GetString("ChooseYear"))
                {
                    case "4Y": namingPattern += "{yyyy}";
                        break;
                    case "2Y": namingPattern += "{yy}";
                        break;
                }
            }
            if (GetBool("MonthCB") == true)
            {
                switch (GetString("ChooseMonth"))
                {
                    case "2M": namingPattern += "{mm}";
                        break;
                    case "1M": namingPattern += "{m}";
                        break;
                }
            }
            if (GetBool("DayCB") == true)
            {
                namingPattern += "{dd}";
            }
            if (GetString("CounterDigits") != null)
            {
                namingPattern += GetString("CounterDigits");
            }

            return namingPattern;
        }

        private void BrokerECOA_SaveData()
        {
            BrokerDB broker = this.Broker;
            broker.ECOAAddressDefault = GetString("ECOAAddress");

            string[] addresses = new string[9];
            addresses[0] = GetString("AddressLine1");
            addresses[1] = GetString("AddressLine2");
            addresses[2] = GetString("AddressLine3");
            addresses[3] = GetString("AddressLine4");
            addresses[4] = GetString("AddressLine5");
            addresses[5] = GetString("AddressLine6");
            addresses[6] = GetString("AddressLine7");
            addresses[7] = GetString("AddressLine8");
            addresses[8] = GetString("AddressLine9");

            broker.FairLendingNoticeAddress = addresses;

            bool isValid = broker.Save();
            if (!isValid)
                throw new CBaseException(ErrorMessages.Generic, "Unable to save Legal Addresses.");

        }

        private void BrokerFairLending_SaveData()
        {
            NameValueCollection ret = new NameValueCollection();
            BrokerDB broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
            string[] addresses = new string[9];
            addresses[0] = GetString("AddressLine1");
            addresses[1] = GetString("AddressLine2");
            addresses[2] = GetString("AddressLine3");
            addresses[3] = GetString("AddressLine4");
            addresses[4] = GetString("AddressLine5");
            addresses[5] = GetString("AddressLine6");
            addresses[6] = GetString("AddressLine7");
            addresses[7] = GetString("AddressLine8");
            addresses[8] = GetString("AddressLine9");

            broker.FairLendingNoticeAddress = addresses;

            bool isValid = broker.Save();
            if (!isValid)
                throw new CBaseException(ErrorMessages.Generic, "Unable to save Fair Lending Address.");

        }
        private void PasswordOptions_ApplyNow()
        {
            string branch, option, expire, cycleExpire, expirePeriod;
            ArrayList pa = new ArrayList();

            branch = GetString("BranchList");
            option = GetString("PasswordOption");
            expire = GetString("PasswordExpiration");

            cycleExpire = GetString("m_CycleExpiration");
            expirePeriod = GetString("m_ExpirationPeriod");

            if (branch == null || option == null || expire == null)
            {
                return;
            }

            if (branch == string.Empty)
            {
                SetResult("ErrorMessage", "Please select a valid branch.");
                SetResult("Feedback", string.Empty);
                return;
            }

            pa.Add(new SqlParameter("@BrokerId", BrokerUser.BrokerId));

            switch (option)
            {
                case "NextL":
                    {
                        pa.Add(new SqlParameter("@PasswordExpirationD", DateTime.Now.Date));
                    }
                    break;

                case "Dated":
                    {
                        if (expire != "")
                        {
                            try
                            {
                                DateTime dt = DateTime.Parse(expire).Date;

                                if (dt.CompareTo(DateTime.Now.Date) < 0 || dt.Year > 2070)
                                {
                                    throw new CBaseException(ErrorMessages.ExpirationDateOutOfRange, ErrorMessages.ExpirationDateOutOfRange);
                                }

                            }
                            catch
                            {
                                SetResult("ErrorMessage", "Invalid expiration date.  Please specify a date between " + DateTime.Now.Date.ToShortDateString() + " and 12/31/2070.");
                                SetResult("Feedback", "");

                                return;
                            }

                            pa.Add(new SqlParameter("@PasswordExpirationD", expire));
                        }
                        else
                        {
                            SetResult("ErrorMessage", "Please enter a valid expiration date");
                            SetResult("Feedback", "");

                            return;
                        }
                    }
                    break;
            }

            if ((cycleExpire != null) && (cycleExpire.Equals("True")))
                pa.Add(new SqlParameter("@PasswordExpirationPeriod", expirePeriod));

            if (branch != Guid.Empty.ToString())
            {
                pa.Add(new SqlParameter("@BranchId", branch));
            }

            try
            {
                BrokerEmployeeNameTable employees = new BrokerEmployeeNameTable();
                BrokerUserPermissionsSet permissions = new BrokerUserPermissionsSet();
                ArrayList paramList = new ArrayList();
                string customerCode = null;

                SqlParameter[] parameters = { 
                                                new SqlParameter("@BrokerId", BrokerUserPrincipal.CurrentPrincipal.BrokerId) 
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "GetBrokerCustomerCodeByBrokerId", parameters))
                {
                    if (reader.Read())
                    {
                        customerCode = reader["CustomerCode"].ToString().ToLower() + "test";
                    }
                }
                employees.Retrieve(BrokerUser.BrokerId);
                permissions.Retrieve(BrokerUser.BrokerId);

                using (CStoredProcedureExec spExec = new CStoredProcedureExec(BrokerUserPrincipal.CurrentPrincipal.BrokerId))
                {
                    spExec.BeginTransactionForWrite();
                    try
                    {
                        spExec.ExecuteNonQuery("BatchPasswordOptionsUpdate", 0, pa);

                        foreach (EmployeeDetails ed in employees.Values)
                        {
                            BrokerUserPermissions bup = permissions[ed.EmployeeId];
                            if ((bup != null && bup.IsInternalBrokerUser()) || (customerCode != null && ed.Type == "P" && ed.Login.ToLower() == customerCode))
                            {
                                paramList.Add(new SqlParameter("@UserId", ed.UserId));
                                spExec.ExecuteNonQuery("UpdatePasswordOptionForUser", paramList);
                                paramList.Clear();
                            }
                        }
                        spExec.CommitTransaction();
                        SetResult("Feedback", "Password options successfully applied.");
                        SetResult("ErrorMessage", "");
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        spExec.RollbackTransaction();
                        SetResult("Feedback", "");
                        SetResult("ErrorMessage", "Could not save password options.");
                    }

                }

            }
            catch
            {
                SetResult("ErrorMessage", "Failed to apply password settings.");
                SetResult("Feedback", "");
            }
        }


        // 5/11/2004 dd - Due to time constraint, I enforce limit of 10 available CRA for each broker. The architecture doesn't have a max limit.
        // However more time will be require to have UI that allow broker to have more than 10 CRA.
        private void BrokerCRA_LoadData()
        {
            BrokerDB broker = this.Broker;
            SetResult("m_AllowPmlOrderNewCreditReport", broker.AllowPmlUserOrderNewCreditReport);

            SqlParameter[] parameters = { new SqlParameter("@BrokerID", BrokerUser.BrokerId) };
            int index = 0;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "ListBrokerCRA", parameters))
            {
                while (reader.Read())
                {
                    SetResult("IsPdfViewNeeded" + index, reader["IsPdfViewNeeded"]);
                    SetResult("CRA" + index, reader["ServiceComId"]);
                    index++;
                }
            }

            // 5/11/2004 dd - Initialize the remainder with blank.
            for (int i = index; i < 10; i++)
            {
                SetResult("IsPdfViewNeeded" + i, false);
                SetResult("CRA" + i, Guid.Empty);
            }

            SetResult("CreditMornetPlusUserID", broker.CreditMornetPlusUserID);
            SetResult("CreditMornetPlusPassword", String.IsNullOrEmpty(broker.CreditMornetPlusPassword.Value) ? string.Empty : ConstAppDavid.FakePasswordDisplay );

            this.SetResult("AllowIndicatingVerbalCreditReportAuthorization", broker.AllowIndicatingVerbalCreditReportAuthorization);

            if (broker.AllowIndicatingVerbalCreditReportAuthorization &&
                broker.VerbalCreditReportAuthorizationCustomPdfId.HasValue)
            {
                var customFormId = broker.VerbalCreditReportAuthorizationCustomPdfId.Value;
                this.SetResult("VerbalCreditReportAuthorizationCustomPdfId", customFormId);
                this.SetResult("VerbalAuthorizationDocumentName", PdfForm.GetCustomPdfDescriptionById(broker.BrokerID, customFormId));
            }
        }

        private void BrokerCRA_SaveData()
        {
            BrokerDB broker = this.Broker;

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine))
                broker.AllowPmlUserOrderNewCreditReport = GetBool("m_AllowPmlOrderNewCreditReport");

            broker.CreditMornetPlusUserID = GetString("CreditMornetPlusUserID");
            var creditMornetPlusPassword = GetString("CreditMornetPlusPassword");

            if (creditMornetPlusPassword != ConstAppDavid.FakePasswordDisplay)
            {
                broker.CreditMornetPlusPassword = creditMornetPlusPassword;
            }

            broker.AllowIndicatingVerbalCreditReportAuthorization = this.GetBool("AllowIndicatingVerbalCreditReportAuthorization");
            broker.VerbalCreditReportAuthorizationCustomPdfId = this.GetGuid("VerbalCreditReportAuthorizationCustomPdfId", Guid.Empty);

            bool isValid = broker.Save();
            if (!isValid)
                throw new CBaseException(ErrorMessages.Generic, "Unable to save CRA.");

            SqlParameter[] brokerIdParameters = {
                                                    new SqlParameter("@BrokerID", BrokerUser.BrokerId)
                                                };

            StoredProcedureHelper.ExecuteNonQuery(BrokerUser.BrokerId, "DeleteAllBrokerCRA", 0, brokerIdParameters);

            int totalLength = 0;

            for (int index = 0; index < 10; index++)
            {
                if (Guid.Empty != GetGuid("CRA" + index))
                {
                    totalLength++;
                }
            }

            if (totalLength == 0)
            {
                return;
            }

            DateTime currentDateTime = DateTime.Now;
            // Set currentDateTime to milliseconds in the past so that we don't set dates into the future
            currentDateTime = currentDateTime.AddMilliseconds(-totalLength * 33);

            for (int index = 0; index < 10; index++)
            {
                Guid craID = GetGuid("CRA" + index);
                if (Guid.Empty != craID)
                {
                    bool isPdfViewNeeded = GetBool("IsPdfViewNeeded" + index);

                    SqlParameter[] parameters = {
                                                        new SqlParameter("@BrokerID", BrokerUser.BrokerId),
                                                        new SqlParameter("@CreatedD", currentDateTime),
                                                        new SqlParameter("@ModifiedD", currentDateTime),
                                                        new SqlParameter("@ServiceComId", craID),
                                                        new SqlParameter("@IsPdfViewNeeded", isPdfViewNeeded)
                                                    };

                    // opm 459804
                    // ensure that the datetime field is stored differently in the database by manually adding 33
                    // milliseconds that is consistently recognized by the sql server 2005 datetime's minimal precision.
                    currentDateTime = currentDateTime.AddMilliseconds(33);

                    StoredProcedureHelper.ExecuteNonQuery(BrokerUser.BrokerId, "CreateBrokerCRA", 0, parameters);
                }
            }
        }
        private void LockPolicies_LoadData()
        {
            var policies = LockPolicy.RetrieveAllForBroker(PrincipalFactory.CurrentPrincipal.BrokerId);

            List<List<string>> stringList = new List<List<string>>();
            if (policies != null)
            {
                foreach (LockPolicy o in policies)
                {
                    stringList.Add(new List<string>() {
                        o.LockPolicyId.ToString(),
                        o.PolicyNm,
                        o.LockDeskHoursDetails.Replace("\n", "<br/>"),
                        o.LockExtensionsDetails.Replace("\n", "<br/>"),
                        o.FloatDownsDetails.Replace("\n", "<br/>"),
                        o.ReLocksDetails.Replace("\n", "<br/>"),
                        o.LpeIsEnforceLockDeskHourForNormalUser ? "true" : "false",
                        o.IsUsingRateSheetExpirationFeature ? "true" : "false"
                    });
                }
            }

            var json = ObsoleteSerializationHelper.JsonSerialize(stringList);
            SetResult("LockPolicyList", json);
        }
        private void LockPolicies_ApplyNow()
        {
            string errorMessage = string.Empty;
            try
            {
                string policy = GetString("m_PolicyToDelete");
                if (!string.IsNullOrEmpty(policy))
                {
                    Guid policyId = new Guid(policy);
                    List<string> priceGroupNames = LockPolicy.GetSafePriceGroupNames(BrokerUser.BrokerId, policyId);
                    if (priceGroupNames.Count == 0)
                    {
                        LockPolicy.DeleteLockPolicy(BrokerUser.BrokerId, policyId);
                    }
                    else
                    {
                        string bullet = "\u2022 ";
                        errorMessage = string.Format("Cannot delete this lock policy because it is currently being used by the following active price groups:\n" + bullet + "{0}",
                            string.Join("\n" + bullet, priceGroupNames.ToArray()));
                    }
                    SetResult("m_PolicyToDelete", "");
                }
            }
            catch (SqlException)
            {
                errorMessage = "Failed to delete lock policy.";
            }
            SetResult("ErrorMessage", errorMessage);
            SetResult("Feedback", "");
        }
    }
}