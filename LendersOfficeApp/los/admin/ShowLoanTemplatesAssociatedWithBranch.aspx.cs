namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;
    /// <summary>
    /// Displays the loan templates that are associated with the branch the admin has selected for deletion
    /// </summary>
    public partial class ShowLoanTemplatesAssociatedWithBranch : LendersOffice.Common.BasePage
	{
		protected System.Web.UI.WebControls.Label					m_EID;
		private Hashtable											m_BranchHash = new Hashtable();
		private ArrayList											m_Branches = new ArrayList();
		private ArrayList											m_Selected = new ArrayList();

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingBranches
                };
            }
        }
	
		private Guid m_brokerID 
		{
			get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
		}

		private Guid m_BranchId 
		{
			get { return new Guid( Request[ "branchId" ] ); }
		}

		private string m_BranchName
		{
			get { return Request[ "branchName" ]; }
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			BindDataGrid();
			m_branchNameLabel.Text = "Loan templates that assign loans to branch:  " + m_BranchName;
			m_tooManyLoanTemplates.Text = "- Too many loan templates to display - displaying the first " + ConstAppDavid.MaxViewableRecordsInPipeline;
			m_noneToShowLabel.Visible = (m_dg.Items.Count == 0)?true:false;
			m_tooManyLoanTemplates.Visible = (m_dg.Items.Count >= ConstAppDavid.MaxViewableRecordsInPipeline)?true:false;
		}

		private void BindDataGrid()
		{
			DataSet dS = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BranchId", m_BranchId)
                                        };

			DataSetHelper.Fill( dS , m_brokerID, "ListLoanTemplatesAssociatedWithBranch", parameters);

			m_dg.DefaultSortExpression = "loanName ASC";

			m_dg.DataSource = dS.Tables[0].DefaultView;
			m_dg.DataBind();

            SqlParameter[] branchParameters = {
                                                      new SqlParameter( "@BrokerId" , m_brokerID )
                                                  };
            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(m_brokerID, "ListBranchByBrokerId", branchParameters))
            {
                string b_Name;
                string b_Id;
                m_BranchHash.Clear();
                m_Branches.Clear();
                while (sR.Read() == true)
                {
                    b_Name = sR["Name"].ToString();
                    b_Id = sR["BranchID"].ToString();
                    m_BranchHash.Add(b_Name, b_Id);
                    m_Branches.Add(b_Name);
                }
            }
			if( !Page.IsPostBack )
				InitBranchDropdownList();

		}

		// 08/26/08 ck - OPM 23916. Remove current branch from drop down list
		protected void InitBranchDropdownList()
		{
			m_BranchDropdownList.Items.Clear();
			foreach( String s in m_Branches )
			{
				if(!s.Equals(m_BranchName))
				{
					m_BranchDropdownList.Items.Add( s );	
				}
			}		
		}

		// 08/26/08 ck - OPM 23916. Instead of using the SelectedIndex of the 
		// drop down list, used the SelectedItem
		protected void AssignLoanTemplatesClick( object sender, System.EventArgs a )
		{
			string guid = (string)m_BranchHash[m_BranchDropdownList.SelectedItem.ToString()];

			foreach( System.Web.UI.WebControls.DataGridItem item in m_dg.Items )
			{				
				CheckBox cbox = item.FindControl( "m_CheckBox" ) as CheckBox;
				
				if( cbox.Checked )
				{
                    EncodedLabel lid = item.FindControl( "m_LID" ) as EncodedLabel;					
					m_Selected.Add( lid.Text );
					Tools.LogError( lid.Text );					
				}				
			}	

			Guid BranchMovingFrom = new Guid( m_BranchId.ToString() );
			Guid BranchMovingTo = new Guid( guid );			
			
			MoveLoanTemplatesFromBranch( BranchMovingFrom, BranchMovingTo );

			PageLoad(sender,a); //Refresh page.		
		}

		protected void MoveLoanTemplatesFromBranch( Guid BranchFromId, Guid BranchToId )
		{
			Guid loanTemplateId;

			foreach( string lid in m_Selected )
			{
				loanTemplateId = new Guid( lid );
				AssignBranchToLoanTemplates( BranchToId, loanTemplateId );
				Tools.LogError( BranchToId.ToString() + " : " + loanTemplateId.ToString() );
			}
		}

		protected void AssignBranchToLoanTemplates( Guid BranchToId, Guid LoanId )
		{
			SqlParameter[] parameters = {
											new SqlParameter("@LoanId", LoanId),
											new SqlParameter("@BranchId", BranchToId)
										};

            StoredProcedureHelper.ExecuteNonQuery(m_brokerID, "UpdateDefaultBranchForLoanTemplate", 0, parameters);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
        #endregion

        protected void m_dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dataItem = args.Item.DataItem as DataRowView;

            EncodedLabel sLId = args.Item.FindControl("m_LID") as EncodedLabel;
            sLId.Text = dataItem["ID"].ToString();
        }
    }
}
