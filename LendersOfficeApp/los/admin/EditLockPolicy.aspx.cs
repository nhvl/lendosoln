﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.Security;
using System.Data.SqlClient;
using DataAccess;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.admin
{
    public partial class EditLockPolicy : LendersOffice.Common.BaseServicePage 
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        protected bool m_isArizona;
        protected bool m_isDST;
        protected Guid BrokerId
        {
            get { return PrincipalFactory.CurrentPrincipal.BrokerId; }
        }

        protected bool m_disableHolidays = false;
        protected Guid PolicyId
        {
            get { return RequestHelper.GetGuid("policyid", Guid.Empty); }
        }
        protected string EditCommand
        {
            get { return RequestHelper.GetSafeQueryString("cmd"); }
        }

        private void PageInit(object sender, System.EventArgs args)
        {
            this.RegisterJsScript("LQBPopup.js");
        }
        protected void PageLoad(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            if (IsPostBack)
                return;
            BrokerDB m_brokerDB = PrincipalFactory.CurrentPrincipal.BrokerDB;
            m_isArizona = m_brokerDB.Address.State.ToUpper() == "AZ";
            m_isDST = TimeZone.CurrentTimeZone.IsDaylightSavingTime(DateTime.Now);

            if (EditCommand == "edit" || EditCommand == "dup")
            {
                LockPolicy p = LockPolicy.Retrieve(BrokerId, PolicyId);
                if(EditCommand == "edit")
                    m_PolicyNm.Text = p.PolicyNm;
                if (m_PolicyNm.Text == "Default")
                    m_PolicyNm.ReadOnly = true;

                if (p.DefaultLockDeskID != Guid.Empty)
                {
                    EmployeeDB empDB = new EmployeeDB(p.DefaultLockDeskID, BrokerId);
                    if (empDB.Retrieve())
                    {
                        m_lockDeskChoice.Data = p.DefaultLockDeskID.ToString();
                        m_lockDeskChoice.Text = empDB.FullName;
                    }
                }

                IsLockDeskEmailDefaultFromForRateLockConfirmation.Checked = p.IsLockDeskEmailDefaultFromForRateLockConfirmation;
                m_IsUsingRateSheetExpirationFeature.SelectedValue = p.IsUsingRateSheetExpirationFeature ? "Y" : "N";
                m_minReqToLock.Text = p.LpeMinutesNeededToLockLoan.ToString();
                m_EnforceLockDeskHourForNormalUser.SelectedValue = p.LpeIsEnforceLockDeskHourForNormalUser ? "Y" : "N";
                //m_StartTime.Text = p.LpeLockDeskWorkHourStartTime.ToShortTimeString();
                //m_EndTime.Text = p.LpeLockDeskWorkHourEndTime.ToShortTimeString();
                m_timezone.SelectedValue = p.TimezoneForRsExpiration;

                m_EnableAutoLockExtensions.SelectedValue = p.EnableAutoLockExtensions ? "Y" : "N";
                m_MaxLockExtensions.Text = p.MaxLockExtensions.ToString();
                m_MaxTotalLockExtensionDays.Text = p.MaxTotalLockExtensionDays.ToString();
                m_LockExtensionMarketWorsenPoints.Text = p.LockExtensionMarketWorsenPoints.ToString("F3");

                m_LockExtensionTableList.Value = SerializeLockExtensionTable(p.LockExtensionOptionTable);

                m_EnableAutoFloatDowns.SelectedValue = p.EnableAutoFloatDowns ? "Y" : "N";
                m_MaxFloatDowns.Text = p.MaxFloatDowns.ToString();
                m_FloatDownMinRateImprovement.Text = p.FloatDownMinRateImprovement.ToString("F3") + "%";
                m_FloatDownMinPointImprovement.Text = p.FloatDownMinPointImprovement.ToString("F3");
                m_FloatDownAllowRateOptionsRequiringAdditionalPoints.SelectedValue = p.FloatDownAllowRateOptionsRequiringAdditionalPoints ? "Y" : "N";
                m_FloatDownFeeIsPercent.Checked = p.FloatDownFeeIsPercent;
                m_FloatDownFeeIsPoints.Checked = !p.FloatDownFeeIsPercent;
                m_FloatDownPercentFee.Text = p.FloatDownPercentFee.ToString("F1");
                m_FloatDownPointFee.Text = p.FloatDownPointFee.ToString("F3");
                m_FloatDownAllowedAfterLockExtension.SelectedValue = p.FloatDownAllowedAfterLockExtension ? "Y" : "N";
                m_FloatDownAllowedAfterReLock.SelectedValue = p.FloatDownAllowedAfterReLock ? "Y" : "N";

                m_EnableAutoReLocks.SelectedValue = p.EnableAutoReLocks ? "Y" : "N";
                m_MaxReLocks.Text = p.MaxReLocks.ToString();
                m_ReLockWorstCasePricingMaxDays.Text = p.ReLockWorstCasePricingMaxDays.ToString();
                m_ReLockWorstCasePricingMaxDays_dup.Text = p.ReLockWorstCasePricingMaxDays.ToString();
                m_ReLockMarketPriceReLockFee.Text = p.ReLockMarketPriceReLockFee.ToString("F3");
                m_ReLockRequireSameInvestor.SelectedValue = p.ReLockRequireSameInvestor ? "Y" : "N";
                m_ReLockFeeTableList.Value = SerializeReLockFeeTable(p.ReLockFeeTable);
                m_LockDeskHourTableRepeater.DataSource = p.LpeLockDeskDaySettings;
                m_LockDeskHourTableRepeater.DataBind();

                if (p.IsCodeBased)
                {
                    m_EnableAutoLockExtensions.Enabled = false;
                    m_EnableAutoFloatDowns.Enabled = false;
                    m_EnableAutoReLocks.Enabled = false;
                }
            }
            else if (this.EditCommand == "new")
            {
                // OPM 242668, 4/27/2016, ML
                // We need to specify a data source so that the repeater has values
                // for the day settings.
                m_LockDeskHourTableRepeater.DataSource = new LockPolicy(this.BrokerId).LpeLockDeskDaySettings;
                m_LockDeskHourTableRepeater.DataBind();
            }
            else
            {
                throw new CBaseException("Invalid Edit Command", "EditLockPolicy.aspx: invalid edit command ("+EditCommand+")");
            }
        }
        protected void OnSave(object sender, EventArgs e)
        {
            LockPolicy p;
            if (EditCommand == "edit")
                p = LockPolicy.Retrieve(PrincipalFactory.CurrentPrincipal.BrokerId, PolicyId);
            else if (EditCommand == "new" || EditCommand == "dup")
                p = new LockPolicy(PrincipalFactory.CurrentPrincipal.BrokerId);
            else
                throw new CBaseException("Invalid Edit Command", "EditLockPolicy.aspx: invalid edit command (" + EditCommand + ")");

            BindValues(p);

            string lockExtensionJson = m_LockExtensionTableList.Value;
            if (!string.IsNullOrEmpty(lockExtensionJson))
            {
                string[][] extensionOptions = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[][]>(lockExtensionJson);
                p.LockExtensionOptionTable.Clear();
                foreach (string[] row in extensionOptions)
                {
                    p.LockExtensionOptionTable.Add(new LockExtensionOption()
                    {
                        ExtensionLength = int.Parse(row[0]),
                        FeeImproving = decimal.Parse(row[1]),
                        FeeStatic = decimal.Parse(row[2]),
                        FeeWorsening = decimal.Parse(row[3])
                    });
                }
                p.LockExtensionOptionTable.Sort();
            }

            string reLockFeeJson = m_ReLockFeeTableList.Value;
            if (!string.IsNullOrEmpty(reLockFeeJson))
            {
                string[][] feeOptions = ObsoleteSerializationHelper.JavascriptJsonDeserializer<string[][]>(reLockFeeJson);
                p.ReLockFeeTable.Clear();
                foreach (string[] row in feeOptions)
                {
                    p.ReLockFeeTable.Add(new ReLockFee()
                    { 
                        PeriodEndDay = int.Parse(row[0]),
                        Fee = decimal.Parse(row[1])
                    });
                }
                p.ReLockFeeTable.Sort();
            }

            if (p.PolicyNameIsAvailable())
            {
                DuplicatePolicyNmError.Visible = false;
                try
                {
                    p.Save();
                }
                catch (CBaseException ex)
                {
                    ErrorMsgPh.Visible = true;
                    ErrorMsg.Text = ex.UserMessage;
                    return;
                }
                if (EditCommand == "dup")
                {
                    p.DuplicateDisabledPricingFrom(PolicyId);
                    p.DuplicateHolidaysFrom(PrincipalFactory.CurrentPrincipal.BrokerId, PolicyId);
                    Response.Redirect("EditLockPolicy.aspx?cmd=edit&policyid=" + p.LockPolicyId, true);
                }
                else if (EditCommand == "new")
                {
                    Response.Redirect("EditLockPolicy.aspx?cmd=edit&policyid=" + p.LockPolicyId, true);
                }
            }
            else
            {
                //policy name already in use for broker
                DuplicatePolicyNmError.Visible = true;
            }
        }

        protected void LockDeskHourTableRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DaySetting ds = (DaySetting)args.Item.DataItem;

            EncodedLiteral day = (EncodedLiteral)args.Item.FindControl("Day");
            CheckBox openForBuisness = (CheckBox)args.Item.FindControl("openForBuisness");
            CheckBox openForLocks = (CheckBox)args.Item.FindControl("openForLocks");

            TimeTextBox startTime = (TimeTextBox)args.Item.FindControl("startTime");
            TimeTextBox endTime = (TimeTextBox)args.Item.FindControl("endTime");

            day.Text = ds.Day.ToString();

            openForBuisness.Checked = ds.IsOpenForBusiness;
            openForLocks.Checked = ds.IsOpenForLocks;

            startTime.Text = new DateTime(2000, 01, 01, ds.StartHour, ds.StartMinute, 0).ToShortTimeString();
            endTime.Text = new DateTime(2000, 01, 01, ds.EndHour, ds.EndMinute, 0).ToShortTimeString();

        }


        private void BindValues(LockPolicy p)
        {
            p.PolicyNm = m_PolicyNm.Text;
            try
            {
                p.DefaultLockDeskID = new Guid(m_lockDeskChoice.Data);
            }
            catch (FormatException)
            {
                p.DefaultLockDeskID = Guid.Empty;
            }

            p.IsLockDeskEmailDefaultFromForRateLockConfirmation = IsLockDeskEmailDefaultFromForRateLockConfirmation.Checked;
            p.IsUsingRateSheetExpirationFeature = m_IsUsingRateSheetExpirationFeature.SelectedValue == "Y";
            p.LpeMinutesNeededToLockLoan_rep = m_minReqToLock.Text;
            p.LpeIsEnforceLockDeskHourForNormalUser = m_EnforceLockDeskHourForNormalUser.SelectedValue == "Y";
            //p.LpeLockDeskWorkHourStartTime_rep = m_StartTime.Text;
            //p.LpeLockDeskWorkHourEndTime_rep = m_EndTime.Text;
            p.TimezoneForRsExpiration = m_timezone.SelectedValue;
            
            p.EnableAutoLockExtensions = m_EnableAutoLockExtensions.SelectedValue == "Y";
            p.MaxLockExtensions_rep = m_MaxLockExtensions.Text;
            p.MaxTotalLockExtensionDays_rep = m_MaxTotalLockExtensionDays.Text;
            p.LockExtensionMarketWorsenPoints_rep = m_LockExtensionMarketWorsenPoints.Text;

            p.EnableAutoFloatDowns = m_EnableAutoFloatDowns.SelectedValue == "Y";
            p.MaxFloatDowns_rep = m_MaxFloatDowns.Text;
            p.FloatDownMinRateImprovement_rep = m_FloatDownMinRateImprovement.Text.Replace("%", "");
            p.FloatDownMinPointImprovement_rep = m_FloatDownMinPointImprovement.Text;
            p.FloatDownAllowRateOptionsRequiringAdditionalPoints = m_FloatDownAllowRateOptionsRequiringAdditionalPoints.SelectedValue == "Y";
            p.FloatDownFeeIsPercent = !m_FloatDownFeeIsPoints.Checked;
            p.FloatDownPointFee_rep = m_FloatDownPointFee.Text;
            p.FloatDownPercentFee_rep = m_FloatDownPercentFee.Text.Replace("%", "");
            p.FloatDownAllowedAfterLockExtension = m_FloatDownAllowedAfterLockExtension.SelectedValue == "Y";
            p.FloatDownAllowedAfterReLock = m_FloatDownAllowedAfterReLock.SelectedValue == "Y";

            p.EnableAutoReLocks = m_EnableAutoReLocks.SelectedValue == "Y";
            p.MaxReLocks_rep = m_MaxReLocks.Text;
            p.ReLockWorstCasePricingMaxDays_rep = m_ReLockWorstCasePricingMaxDays.Text;
            p.ReLockMarketPriceReLockFee_rep = m_ReLockMarketPriceReLockFee.Text;
            p.ReLockRequireSameInvestor = m_ReLockRequireSameInvestor.SelectedValue == "Y";


            foreach (RepeaterItem item in m_LockDeskHourTableRepeater.Items)
            {
                Literal day = (Literal)item.FindControl("Day");
                CheckBox openForBuisness = (CheckBox)item.FindControl("openForBuisness");
                CheckBox openForLocks = (CheckBox)item.FindControl("openForLocks");
                TimeTextBox startTime = (TimeTextBox)item.FindControl("startTime");
                TimeTextBox endTime = (TimeTextBox)item.FindControl("endTime");

                DaySetting ds = p.GetDaySettingsFor((DayOfWeek)Enum.Parse(typeof(DayOfWeek), day.Text));
                ds.IsOpenForBusiness = openForBuisness.Checked;
                ds.IsOpenForLocks = openForLocks.Checked;
                ds.StartHour = startTime.Time.TimeOfDay.Hours;
                ds.EndHour = endTime.Time.TimeOfDay.Hours;
                ds.EndMinute = endTime.Time.TimeOfDay.Minutes;
                ds.StartMinute = startTime.Time.TimeOfDay.Minutes;

            }


        }
        private string SerializeLockExtensionTable(List<LockExtensionOption> table)
        {
            List<string[]> rows = new List<string[]>();
            foreach (LockExtensionOption option in table)
            {
                rows.Add(new string[]{
                        option.ExtensionLength.ToString(),
                        option.FeeImproving.ToString("F3"),
                        option.FeeStatic.ToString("F3"),
                        option.FeeWorsening.ToString("F3")
                    });
            }
            return ObsoleteSerializationHelper.JsonSerialize(rows.ToArray());
        }
        private string SerializeReLockFeeTable(List<ReLockFee> table)
        {
            List<string[]> rows = new List<string[]>();
            foreach (ReLockFee option in table)
            {
                rows.Add(new string[]{
                        option.PeriodEndDay.ToString(),
                        option.Fee.ToString("F3")
                    });
            }
            return ObsoleteSerializationHelper.JsonSerialize(rows.ToArray());
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion
    }
}
