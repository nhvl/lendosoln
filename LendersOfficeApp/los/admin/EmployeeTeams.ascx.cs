﻿// <copyright file="EmployeeTeams.ascx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: EduardoM
//    Date:   6/6/2014
// </summary>

namespace LendersOfficeApp.Los.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using LendersOfficeApp.ObjLib.Licensing;

    /// <summary>
    /// This control contains a list of all the teams that a user is assigned to.
    /// </summary>
    public partial class EmployeeTeams : System.Web.UI.UserControl
    {
        /// <summary>
        /// This loads the control.
        /// </summary>
        /// <param name="employeeId">The Employee's Id.</param>
        public void DataBind(Guid employeeId, Guid brokerId)
        {
            // Get the user's roles.
            EmployeeDB employee = new EmployeeDB(employeeId, brokerId);
            employee.Retrieve();

                TeamTable.Style.Add("display", "");
                NewUserMsg.Style.Add("display", "none");

                EmployeeRoles employeeRoles = new EmployeeRoles(brokerId, employeeId);

                List<Guid> assignedTeamRoles = new List<Guid>();
                List<Team> assignedTeams = (List<Team>)Team.ListTeamsByUser(brokerId, employeeId);

                foreach (Team team in assignedTeams)
                {
                    assignedTeamRoles.Add(team.RoleId);
                }

                int index = 0;
                // Create the table.
                foreach (Role role in Role.LendingQBRoles.OrderBy(role => role.ModifiableDesc))
                {
                    bool invalidAssignment = !employeeRoles.IsInRole(role.Id);
                    bool hideRow = !employeeRoles.IsInRole(role.Id) && !assignedTeamRoles.Contains(role.Id);
                    List<Team> teams = (List<Team>)Team.ListTeamsByUserInRole(brokerId, employeeId, role.Id);

                    HtmlGenericControl primaryP = new HtmlGenericControl("span");
                    Image expandImg = new Image();
                    expandImg.ImageUrl = ((BasePage) Page).VirtualRoot + "/images/tree/p.gif";
                    expandImg.AlternateText = "Expand";
                    expandImg.Attributes.Add("class", "show");

                    Image minimizeImg = new Image();
                    minimizeImg.ImageUrl = ((BasePage)Page).VirtualRoot + "/images/tree/m.gif";
                    minimizeImg.AlternateText = "Minimize";
                    minimizeImg.Attributes.Add("class", "hide");

                    HtmlGenericControl nonPrimaryTeams = new HtmlGenericControl("span");

                    Team primaryTeam = null;

                    List<Team> primaryTeams = (List<Team>)Team.ListPrimaryTeamByUserInRole(brokerId, employeeId, role.Id);

                    if (primaryTeams.Count != 0)
                    {
                        primaryTeam = primaryTeams[0];
                        primaryP.InnerText = primaryTeam.Name + " (primary)  ";
                        if (invalidAssignment)
                        {
                            primaryP.Attributes.Add("class", "invalidAssignment");

                            HyperLink a = new HyperLink();
                            a.Attributes.Add("class", "invalidTeamLink");
                            a.NavigateUrl = "#";
                            a.Text = "?";
                            primaryP.Controls.Add(a);
                        }
                    }
                    else
                    {
                        primaryP.InnerText = "No team assigned";
                    }

                    foreach (Team team in teams)
                    {
                        if (primaryTeam == null || !primaryTeam.Id.Equals(team.Id))
                        {
                            HtmlGenericControl teamP = new HtmlGenericControl("p");
                            teamP.InnerText = team.Name;
                            if (invalidAssignment)
                            {
                                teamP.Attributes.Add("class", "invalidAssignment");
                            }

                            nonPrimaryTeams.Controls.Add(teamP);
                        }
                    }

                    if (teams.Count == 0 || (teams.Count == 1 && primaryTeams.Count >= 1))
                    {
                        expandImg.Style.Add("display", "none");
                        minimizeImg.Style.Add("display", "none");
                    }

                    HyperLink link = new HyperLink();
                    link.Text = "assign";
                    link.NavigateUrl = "javascript:void(0)";
                    link.Attributes.Add("data-href", "/los/admin/EmployeeTeamPicker.aspx?employeeId=" + employeeId.ToString() + "&roleId=" + role.Id.ToString());
                    link.Attributes.Add("class", "TeamPickerLink");

                    // Generate the rows and cells.
                    HtmlTableRow row = new HtmlTableRow();
                    if (hideRow)
                    {
                        row.Style.Add("display", "none");
                    }
                    HtmlTableCell roleCell = new HtmlTableCell();
                    roleCell.InnerText = role.ModifiableDesc;
                    if (teams.Count == 0)
                    {
                        roleCell.Attributes.Add("class", "teamTD");
                    }
                    else
                    {
                        roleCell.Attributes.Add("class", "teamTD RoleLabel");
                    }

                    roleCell.Attributes.Add("idVal", string.Empty + index);
                    row.Controls.Add(roleCell);
                    index++;

                    HtmlTableCell imgCell = new HtmlTableCell();
                    imgCell.Controls.Add(expandImg);
                    imgCell.Controls.Add(minimizeImg);
                    row.Controls.Add(imgCell);

                    HtmlTableCell teamsCell = new HtmlTableCell();
                    teamsCell.Attributes.Add("class", "teamTD");
                    teamsCell.Controls.Add(primaryP);
                    row.Controls.Add(teamsCell);

                    HtmlTableCell linkCell = new HtmlTableCell();
                    link.Attributes.Add("roleIdVal", role.Id.ToString());
                    linkCell.Controls.Add(link);
                    linkCell.Attributes.Add("class", "teamTD");
                    row.Controls.Add(linkCell);

                    TeamTable.Controls.Add(row);

                    HtmlTableRow row2 = new HtmlTableRow();

                    HtmlTableCell roleCell2 = new HtmlTableCell();
                    roleCell2.Attributes.Add("class", "teamTD");
                    row2.Controls.Add(roleCell2);

                    HtmlTableCell imgCell2 = new HtmlTableCell();
                    row2.Controls.Add(imgCell2);

                    HtmlTableCell teamsCell2 = new HtmlTableCell();
                    teamsCell2.Controls.Add(nonPrimaryTeams);
                    teamsCell2.Attributes.Add("class", "teamTD");
                    row2.Controls.Add(teamsCell2);

                    HtmlTableCell linkCell2 = new HtmlTableCell();
                    row2.Controls.Add(linkCell2);
                    linkCell2.Attributes.Add("class", "teamTD");
                    row2.Attributes.Add("class", "nonPrimary");

                    TeamTable.Controls.Add(row2);
                }
        }

        public void Save(Guid employeeId, Guid brokerId)
        {
            string json = TeamData.Value;

            Dictionary<String, Dictionary<String, String>> dict = ObsoleteSerializationHelper.JavascriptJsonDeserializer<Dictionary<String, Dictionary<String, String>>>(json);
            foreach (String roleIdVal in dict.Keys)
            {
                Dictionary<string, string> data = dict[roleIdVal];
                int numUpdates = int.Parse(data["numUpdates"]);
                Guid roleId = new Guid(roleIdVal);
                List<Team> primaryTeam = (List<Team>)Team.ListPrimaryTeamByUserInRole(brokerId, employeeId, roleId);

                bool hasPrimaryForRole = false;
                for (int i = 0; i < numUpdates; i++)
                {
                    int j = i + 1;
                    Guid teamId = new Guid(data["TeamId" + j]);
                    bool isAssigned = bool.Parse(data["IsAssigned" + j]);
                    bool isPrimary = bool.Parse(data["IsPrimary" + j]);

                    // Another team has already been chosen as the primary team. Don't let this happen.
                    if (isPrimary && hasPrimaryForRole)
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Employee <" + employeeId.ToString() + "> had more than 1 team assigned as primary for role <" + roleId.ToString() + ">");
                    }

                    if (isPrimary)
                    {
                        // Mark this role as having a primary team already chosen.
                        hasPrimaryForRole = true;
                    }

                    Team.RemoveUserAssignment(brokerId, employeeId, teamId);

                    if (isAssigned || isPrimary)
                    {
                        Team.SetUserAssignment(brokerId, employeeId, teamId, isPrimary);
                    }
                }
            }

            //remove all of the teams the user doesn't have the roles for
            EmployeeRoles employeeRoles = new EmployeeRoles(brokerId, employeeId);

            List<Team> teams = (List<Team>)Team.ListTeamsByUser(brokerId, employeeId);
            foreach (Team team in teams)
            {
                if(!employeeRoles.IsInRole(team.RoleId))
                {
                    Team.RemoveUserAssignment(brokerId, employeeId, team.Id);
                }
            }

        }
    }
}