﻿// <copyright file="EditTeams.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: EduardoM
//    Date:   6/6/2014
// </summary>

namespace LendersOfficeApp.Los.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// This page allows you to create and edit teams.
    /// </summary>
    public partial class EditTeams : LendersOffice.Common.BaseServicePage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingTeams
                };
            }
        }

        /// <summary>
        /// Initializes the control.
        /// </summary>
        /// <param name="sender">The control that triggers this.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            RolesFilter.Items.Add(new ListItem("All roles", "-1"));
            TeamRole.Items.Add(new ListItem("<-- Select a role -->", "-1"));
            foreach (Role role in Role.LendingQBRoles.OrderBy(role => role.ModifiableDesc))
            {
                TeamRole.Items.Add(Tools.CreateEnumListItem(role.ModifiableDesc, role.RoleT));
                RolesFilter.Items.Add(Tools.CreateEnumListItem(role.ModifiableDesc, role.RoleT));
            }

            this.RegisterService("EditTeamsService", "/los/admin/EditTeamsService.aspx");
            this.RegisterVbsScript("common.vbs");
            RegisterJsScript("LQBPopup.js");
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The control that triggers this.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterJsScript("json.js");

            DataTable m_data = new DataTable();
            m_data.Columns.Add("TeamRoleDesc");
            m_data.Columns.Add("TeamRoleValue");
            m_data.Columns.Add("TeamName");
            m_data.Columns.Add("TeamId");

            List<Team> teams = (List<Team>)Team.ListTeamsAtLender(string.Empty);
            foreach (Team team in teams)
            {
                var row = m_data.NewRow();
                row["TeamName"] = team.Name;
                Guid roleGUID = team.RoleId;
                Role role = Role.Get(roleGUID);
                row["TeamRoleDesc"] = role.ModifiableDesc;
                row["TeamRoleValue"] = (int)role.RoleT;
                row["TeamId"] = team.Id;
                m_data.Rows.Add(row);
            }

            DataSet set = new DataSet();

            if (m_data.Rows.Count != 0)
            {
                set.Tables.Add(m_data);
                TeamsGrid.DataSource = set;
                TeamsGrid.DataBind();
            }
            else
            {
                m_data.Rows.Add(m_data.NewRow());
                set.Tables.Add(m_data);
                TeamsGrid.DataSource = set;
                TeamsGrid.DataBind();
                TeamsGrid.Rows[0].Visible = false;
            }

            DataSet emptyRows = new DataSet();
            DataTable emptyTable = new DataTable();
            emptyTable.Rows.Add(emptyTable.NewRow());
            emptyRows.Tables.Add(emptyTable);
            EmployeesGrid.DataSource = emptyRows;
            EmployeesGrid.DataBind();
            EmployeesGrid.Rows[0].Visible = false;
        }
    }
}
