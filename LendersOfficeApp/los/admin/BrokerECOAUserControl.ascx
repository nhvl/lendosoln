<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BrokerECOAUserControl.ascx.cs" Inherits="LendersOfficeApp.los.admin.BrokerECOAUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<asp:Panel id="BasePanel" runat="server" style="PADDING: 8px;" Width="100%">
    <TABLE class="FormTable" cellspacing="0" cellPadding="0" border="0" width="100%">
    <TR>
    <TD class=FieldLabel>
        Equal Credit Opportunity Act Information
    </TD>
    </TR>
    <TR>
    <TD>
        The Federal Agency that administers compliance with this law concerning this company is
    </TD>
    </TR>
    <TR>
    <TD >
        <asp:TextBox id="ECOAAddress" runat="server" style="PADDING-LEFT: 4px;" TextMode="MultiLine" Width="314px" Height="90px"/>
    </TD>
    </TR>
     <TR>
    <TD class="FieldLabel" style=padding-top:20px>
        Fair Lending questions or complaints may be addressed to
    </TD>
    </TR>
    <TR>
    <TD>
    <TABLE cellPadding="0" cellSpacing="0" border="0">
    <TR>
    <TD>
        <asp:TextBox id=AddressLine1 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine2 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine3 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine4 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine5 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine6 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine7 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine8 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    <TR>
    <TD>
        <asp:TextBox id=AddressLine9 runat="server" Width="408px" MaxLength="50">
        </asp:TextBox>
    </TD>
    </TR>
    </TABLE>
    </TD>
    </TR>
    </TABLE>
</asp:Panel>