<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.Security"%>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EmployeePermissionsList.ascx.cs" Inherits="LendersOfficeApp.los.admin.EmployeePermissionsList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Permissions List</title>
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </HEAD>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto" onload="onInitEmployeePermissionsList();">
	<script type="text/javascript">
		function onInitEmployeePermissionsList()
		{
			if('<%=AspxTools.JsStringUnquoted(BatchEdit)%>' == "False")
				LoadDefaults();
			SetDefaultUIStatus();
			if(typeof(makeClean) == 'function')
				makeClean();

			if('<%=AspxTools.JsStringUnquoted(BatchEdit)%>' == "True")
				createDefaultIndentsForBatchControls()
		}

		function createDefaultIndentsForBatchControls()
		{
			if('<%=AspxTools.JsStringUnquoted(BatchEdit)%>' == "True")
			{
				addDefaultIndentForSelectedBatchControl(<%=AspxTools.JsString(Permission.CanEditOthersCustomReports.ToString())%>);
				addDefaultIndentForSelectedBatchControl(<%=AspxTools.JsString(Permission.CanPublishCustomReports.ToString())%>);
				addDefaultIndentForSelectedBatchControl(<%=AspxTools.JsString(Permission.CanEditOthersCustomForms.ToString())%>);
				addDefaultIndentForSelectedBatchControl(<%=AspxTools.JsString(Permission.AllowWritingToRolodex.ToString())%>);
				addDefaultIndentForSelectedBatchControl(<%=AspxTools.JsString(Permission.CanEditEDocs.ToString())%>);
				addDefaultIndentForSelectedBatchControl(<%=AspxTools.JsString(Permission.CanSubmitWithoutCreditReport.ToString())%>);
				addDefaultIndentForSelectedBatchControl(<%=AspxTools.JsString(Permission.AllowLockDeskWrite.ToString())%>);
				addDefaultIndentForSelectedBatchControl(<%=AspxTools.JsString(Permission.AllowCloserWrite.ToString())%>);
				addDefaultIndentForSelectedBatchControl(<%=AspxTools.JsString(Permission.AllowAccountantWrite.ToString())%>);
				addDefaultIndentForSelectedBatchControl(<%=AspxTools.JsString(Permission.CanAccessTotalScorecard.ToString())%>);
			}
		}

		function addDefaultIndentForSelectedBatchControl(permission) {
			var id = getElementIdByPermission(permission);
			if(id)
			    document.getElementById(id + "Off").insertAdjacentHTML("afterEnd", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
		}

		function SetDefaultUIStatus()
		{
			onCustomReportsClick();
			onCustomFormsClick();
			onContactsClick();
			onEDocsClick();
			onPricingEngineClick();
			onMortgageBankingClick();
			onTemplatesClick();
			onCapitalMarketsClick();
			onAdministrationClick();
			onTestLoansClick();
			onUserExperienceClick();
			onPrintGroupsClick();
			if('<%=AspxTools.JsStringUnquoted(Disabled)%>' == "True")
				DisableAllPermissions(true);
		}

		function LoadDefaults()
		{
			if('<%=AspxTools.JsStringUnquoted(BatchEdit)%>' == "True")
				return;
			var defaultPermissions = <%= AspxTools.JsGetElementById(m_DefaultPermissions) %>;
			SetPermissions(defaultPermissions);
		}

		function IsCurrent(permissionDesc)
		{
			if('<%=AspxTools.JsStringUnquoted(BatchEdit)%>' == "False")
				return;
			if(window.PermissionTitleList && window.PermissionIdList)
			{
				var element;
				for(var i = 0; i < PermissionTitleList.length; ++i)
				{
					if(PermissionTitleList[i] == permissionDesc)
					{
						element = document.getElementById(PermissionIdList[i] + "KeepCurrentPermission");
						if(element)
							return element.checked;
					}
				}
			}
			return false;
		}

		function IsChecked(permissionDesc)
		{
			if(window.PermissionTitleList && window.PermissionIdList)
			{
				var element;
				for(var i = 0; i < PermissionTitleList.length; ++i)
				{
					if(PermissionTitleList[i] == permissionDesc)
					{
						if('<%=AspxTools.JsStringUnquoted(BatchEdit)%>' == "True")
							element = document.getElementById(PermissionIdList[i] + "On");
						else
							element = document.getElementById(PermissionIdList[i]);

						if(element)
							return element.checked;
					}
				}
			}
			return false;
		}


		function SelectPermission(permissionDesc, checkIt)
		{
			if(window.PermissionTitleList && window.PermissionIdList)
			{
				for(var i = 0; i < PermissionTitleList.length; ++i)
				{
					if(PermissionTitleList[i] == permissionDesc)
					{
						if('<%=AspxTools.JsStringUnquoted(BatchEdit)%>' == "True")
						{
							if(checkIt == true)
								document.getElementById(PermissionIdList[i] + "On").checked = true;
							else
								document.getElementById(PermissionIdList[i] + "Off").checked = true;
						}
						else
							document.getElementById(PermissionIdList[i]).checked = checkIt;
					}
				}
			}
		}

		function DisableAllPermissions(disableIt)
		{
			if('<%=AspxTools.JsStringUnquoted(BatchEdit)%>' == "True")
				return;
			if(window.PermissionTitleList && window.PermissionIdList)
			{
				for(var i = 0; i < PermissionTitleList.length; ++i)
				{
					document.getElementById(PermissionIdList[i]).disabled = disableIt;
				}
			}
		}

		function DisablePermission(permissionDesc, disableIt)
		{
			if(window.PermissionTitleList && window.PermissionIdList)
			{
				for(var i = 0; i < PermissionTitleList.length; ++i)
				{
					if(PermissionTitleList[i] == permissionDesc)
					{
						if('<%=AspxTools.JsStringUnquoted(BatchEdit)%>' == "True")
						{
							document.getElementById(PermissionIdList[i] + "KeepCurrentPermission").disabled = disableIt;
							document.getElementById(PermissionIdList[i] + "On").disabled = disableIt;
							document.getElementById(PermissionIdList[i] + "Off").disabled = disableIt;
						}
						else
							document.getElementById(PermissionIdList[i]).disabled = disableIt;
					}
				}
			}
		}

		function SetPermissions(permissions)
		{
			if('<%=AspxTools.JsStringUnquoted(BatchEdit)%>' == "True")
				return;
			var permissionSet;
			if(permissions.value)
				permissionSet = permissions.value.split(',');
			else
			{
				permissionSet = permissions.split(',');
			}

			var controls = document.getElementById('m_PermissionsListDiv').getElementsByTagName('INPUT');

			if(!permissionSet || !controls || controls.length != permissionSet.length)
			{
				alert('<%=AspxTools.JsStringUnquoted(JsMessages.PermissionSetsDontMatch)%>');
				return;
			}

			for ( var i = 0; i < controls.length; i++ )
			{
				controls[i].checked = ( permissionSet[i].toLowerCase() == 'true' );
			}


			SetDefaultUIStatus();
		}

		function onLeadsClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();
		}

		function onLoansClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();
		}

		function onCustomFormsClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();

			if('<%=AspxTools.JsStringUnquoted(Disabled)%>' == "True")
				return;

			if(IsChecked('<%=AspxTools.JsStringUnquoted(Permission.CanCreateCustomForms.ToString())%>'))
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditOthersCustomForms.ToString())%>', false);
			else if(IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanCreateCustomForms.ToString())%>'))
				setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanEditOthersCustomForms.ToString())%>');
			else
			{
				SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditOthersCustomForms.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditOthersCustomForms.ToString())%>', true);
			}
		}

		function onCCTemplatesClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();
		}

		function onTemplatesClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();


            if (IsChecked('<%=AspxTools.JsStringUnquoted(Permission.CanEditLoanTemplates.ToString())%>')) {
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanCreateLoanTemplate.ToString()) %>', false);
            }
            else if (IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanEditLoanTemplates.ToString())%>')){
                setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanCreateLoanTemplate.ToString())%>');
            }
            else {
                SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.CanCreateLoanTemplate.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanCreateLoanTemplate.ToString())%>', true);

            }
		}

		function getElementIdByPermission(permissionDesc)
		{
			if(window.PermissionTitleList && window.PermissionIdList)
			{
				for(var i = 0; i < PermissionTitleList.length; ++i)
				{
					if(PermissionTitleList[i] == permissionDesc)
						return PermissionIdList[i];
				}
			}
			return null;
		}

		function setBatchPermissionStateCurrent(permissionDesc)
		{
			var id = getElementIdByPermission(permissionDesc);
			if(document.getElementById(id + "On").checked )
				document.getElementById(id + "KeepCurrentPermission").checked = true;
			document.getElementById(id + "On").disabled = true;
			document.getElementById(id + "Off").disabled = false;
			document.getElementById(id + "KeepCurrentPermission").disabled = false;
		}

		function onCustomReportsClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();

			if('<%=AspxTools.JsStringUnquoted(Disabled)%>' == "True")
				return;

			if(IsChecked('<%=AspxTools.JsStringUnquoted(Permission.CanRunCustomReports.ToString())%>'))
			{
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditOthersCustomReports.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanPublishCustomReports.ToString())%>', false);
			}
			else if(IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanRunCustomReports.ToString())%>'))
			{
				setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanEditOthersCustomReports.ToString())%>');
				setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanPublishCustomReports.ToString())%>');
			}
			else
			{
				SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditOthersCustomReports.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditOthersCustomReports.ToString())%>', true);
				SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.CanPublishCustomReports.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanPublishCustomReports.ToString())%>', true);
			}
		}

		function disableEnablePermissionBasedOnFirst(permIDRequired, permIDToDisable)
		{
		    if(IsChecked(permIDRequired))
				DisablePermission(permIDToDisable, false);
			else if(IsCurrent(permIDRequired))
				setBatchPermissionStateCurrent(permIDToDisable);
			else
			{
				SelectPermission(permIDToDisable, false);
				DisablePermission(permIDToDisable, true);
			}
		}

		function onContactsClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();

			if('<%=AspxTools.JsStringUnquoted(Disabled)%>' == "True")
				return;

			if(IsChecked('<%=AspxTools.JsStringUnquoted(Permission.AllowReadingFromRolodex.ToString())%>'))
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowWritingToRolodex.ToString())%>', false);
			else if(IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowReadingFromRolodex.ToString())%>'))
				setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowWritingToRolodex.ToString())%>');
			else
			{
				SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.AllowWritingToRolodex.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowWritingToRolodex.ToString())%>', true);
			}

			if(IsChecked('<%=AspxTools.JsStringUnquoted(Permission.AllowWritingToRolodex.ToString())%>'))
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowManagingContactList.ToString())%>', false);
			else if(IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowWritingToRolodex.ToString())%>'))
				setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowManagingContactList.ToString())%>');
			else
			{
				SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.AllowManagingContactList.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowManagingContactList.ToString())%>', true);
			}
		}

		function onCommissionsClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();
		}

		function onPrintGroupsClick()
		{
			if(typeof(makeDirty) == 'function')
			{
				makeDirty();
			}

			disableEnablePermissionBasedOnFirst('<%=AspxTools.JsStringUnquoted(Permission.AllowViewingPrintGroups.ToString()) %>', '<%=AspxTools.JsStringUnquoted(Permission.CanEditPrintGroups.ToString()) %>');
		}

		function onExternalUsersClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();
		}
		function onPriceGroupsClick()
		{
			if ( typeof( makeDirty ) == 'function' )
				makeDirty();
        }
        function onEDocsClick(selValue) {
            if (typeof (makeDirty) == 'function')
                makeDirty();

            if (<%= AspxTools.JsBool(Disabled) %> == true)
                return;

            // Allow editing EDocs and Allow viewing EDocs Internal Notes depend
            // on allow viewing edocs.
            if (IsChecked('<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocs.ToString())%>')) {
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocs.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocsInternalNotes.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowCreatingDocumentSigningEnvelopes.ToString())%>', false);

                if(selValue == '<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocs) /* This will output the int value. */%>') {
                    if(<%=AspxTools.JsBool(m_bIsUnderwriterRole)%>) {
                        SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocsInternalNotes.ToString())%>', true);
                    }
                }
            }
            else if (IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocs.ToString())%>')) {
                setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocs.ToString())%>');
                setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocsInternalNotes.ToString())%>');
                setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowCreatingDocumentSigningEnvelopes.ToString())%>');
            }
            else {
                SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocs.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocs.ToString())%>', true);
                SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocsInternalNotes.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocsInternalNotes.ToString())%>', true);
                SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.AllowCreatingDocumentSigningEnvelopes.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowCreatingDocumentSigningEnvelopes.ToString())%>', true);
            }

            // Allow screening..., allow protecting/rejecting... and allow editing
            // protected all depend on status of allow editing edocs.
            if (IsChecked('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocs.ToString())%>')) {
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowScreeningEDocs.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowApprovingRejectingEDocs.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowEditingApprovedEDocs.ToString())%>', false);

                if(<%=AspxTools.JsBool(m_bIsUnderwriterRole)%>
                    && IsChecked('<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocsInternalNotes.ToString())%>')) {
                    SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocsInternalNotes.ToString())%>', true);
                }
            }
            else if (IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocs.ToString())%>')) {
                setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowScreeningEDocs.ToString())%>');
                setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowApprovingRejectingEDocs.ToString())%>');
                setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowEditingApprovedEDocs.ToString())%>');
            }
            else {
                SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.AllowScreeningEDocs.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowScreeningEDocs.ToString())%>', true);
                SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.AllowApprovingRejectingEDocs.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowApprovingRejectingEDocs.ToString())%>', true);
                SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.AllowEditingApprovedEDocs.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowEditingApprovedEDocs.ToString())%>', true);
            }

            // Allow editing edocs internal notes depends on allow viewing
            // edocs internal notes.
            if (IsChecked('<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocsInternalNotes.ToString())%>')) {
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocsInternalNotes.ToString())%>', false);
            }
            else if (IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanViewEDocsInternalNotes.ToString())%>')) {
                setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocsInternalNotes.ToString())%>');
            }
            else {
                SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocsInternalNotes.ToString())%>', false);
                DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanEditEDocsInternalNotes.ToString())%>', true);
            }
        }

		function onPricingEngineClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();

			if('<%=AspxTools.JsStringUnquoted(Disabled)%>' == "True")
				return;

			var disableAllowBypassAlwaysUseBestPrice = !<%=AspxTools.JsBool(ShowAllowBypassAlwaysUseBestPrice)%>
			var allowBypassAlwaysUseBestPrice = <%=AspxTools.JsString(Permission.AllowBypassAlwaysUseBestPrice.ToString())%>;
			if(disableAllowBypassAlwaysUseBestPrice)
			    SelectPermission(allowBypassAlwaysUseBestPrice, false);
			DisablePermission(allowBypassAlwaysUseBestPrice, disableAllowBypassAlwaysUseBestPrice);

			if(IsChecked('<%=AspxTools.JsStringUnquoted(Permission.CanRunPricingEngineWithoutCreditReport.ToString())%>'))
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanSubmitWithoutCreditReport.ToString())%>', false);
			else if(IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanRunPricingEngineWithoutCreditReport.ToString())%>'))
				setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.CanSubmitWithoutCreditReport.ToString())%>');
			else
			{
				SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.CanSubmitWithoutCreditReport.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.CanSubmitWithoutCreditReport.ToString())%>', true);
			}
		}
		function onMortgageBankingClick()
		{
			if(typeof(makeDirty) == 'function')
				makeDirty();

			if (<%= AspxTools.JsBool(Disabled) %> == true)
                return;

            if(IsChecked('<%=AspxTools.JsStringUnquoted(Permission.AllowViewingInvestorInformation.ToString())%>'))
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowEditingInvestorInformation.ToString())%>', false);
			else if(IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowViewingInvestorInformation.ToString())%>'))
				setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowEditingInvestorInformation.ToString())%>');
			else
			{
				SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.AllowEditingInvestorInformation.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowEditingInvestorInformation.ToString())%>', true);
			}

            if(IsChecked('<%=AspxTools.JsStringUnquoted(Permission.AllowLockDeskRead.ToString())%>'))
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowLockDeskWrite.ToString())%>', false);
			else if(IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowLockDeskRead.ToString())%>'))
				setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowLockDeskWrite.ToString())%>');
			else
			{
				SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.AllowLockDeskWrite.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowLockDeskWrite.ToString())%>', true);
			}

			if(IsChecked('<%=AspxTools.JsStringUnquoted(Permission.AllowCloserRead.ToString())%>'))
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowCloserWrite.ToString())%>', false);
			else if(IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowCloserRead.ToString())%>'))
				setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowCloserWrite.ToString())%>');
			else
			{
				SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.AllowCloserWrite.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowCloserWrite.ToString())%>', true);
			}

			if(IsChecked('<%=AspxTools.JsStringUnquoted(Permission.AllowAccountantRead.ToString())%>'))
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowAccountantWrite.ToString())%>', false);
			else if(IsCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowAccountantRead.ToString())%>'))
				setBatchPermissionStateCurrent('<%=AspxTools.JsStringUnquoted(Permission.AllowAccountantWrite.ToString())%>');
			else
			{
				SelectPermission('<%=AspxTools.JsStringUnquoted(Permission.AllowAccountantWrite.ToString())%>', false);
				DisablePermission('<%=AspxTools.JsStringUnquoted(Permission.AllowAccountantWrite.ToString())%>', true);
			}
		}

		function onDisclosuresClick()
		{
		    if (typeof(makeDirty) == 'function')
		        makeDirty();
		}

		function onCapitalMarketsClick()
		{
		    if (typeof(makeDirty) == 'function')
		        makeDirty();
		}
		function onAdministrationClick()
		{
		    if (typeof(makeDirty) == 'function')
		        makeDirty();

		    disableEnablePermissionBasedOnFirst('<%=AspxTools.JsStringUnquoted(Permission.AllowViewingAndEditingGeneralSettings.ToString()) %>', '<%=AspxTools.JsStringUnquoted(Permission.AllowViewingAndEditingGlobalIPSettings.ToString()) %>');
		}
		function onTestLoansClick()
		{
		    if (typeof(makeDirty) == 'function')
		        makeDirty();
		}
		function onUserExperienceClick()
		{
		    if (typeof(makeDirty) == 'function')
		        makeDirty();
		}

		function onDocumentReviewClick()
		{
		    if (typeof(makeDirty) == 'function')
		        makeDirty();
		}
    </script>
		<table border="0" width="100%" height="100%">
			<input type="hidden" runat="server" id="m_DefaultPermissions" NAME="m_DefaultPermissions">
			<tr>
				<td>
					<div id="m_PermissionsListDiv">
						<asp:Panel id="m_PermissionsPanel" Runat="server">
						<% if(!BatchEdit){ %>
							<asp:Panel ID="m_PermissionsCheckBoxes" Runat="server">
                            <B>
                            <asp:PlaceHolder runat="server" ID="LeadPlaceHolder">
                            Leads
							<asp:CheckBoxList id="m_EmployeePermissionsLeads" onclick="onLeadsClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<br />
                            </asp:PlaceHolder>
                            Loans
							<asp:CheckBoxList id="m_EmployeePermissionsLoans" onclick="onLoansClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<BR>Loan Templates
							<asp:CheckBoxList id="m_EmployeePermissionsTemplates" onclick="onTemplatesClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<BR>Closing Cost Templates
							<asp:CheckBoxList id="m_EmployeePermissionsCCTemplates" onclick="onCCTemplatesClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<BR>Custom Reports
							<asp:CheckBoxList id="m_EmployeePermissionsCustomReports" onclick="onCustomReportsClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<BR>Custom Forms
							<asp:CheckBoxList id="m_EmployeePermissionsCustomForms" onclick="onCustomFormsClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<BR>Contacts
							<asp:CheckBoxList id="m_EmployeePermissionsContacts" onclick="onContactsClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<% if (ShowDataTrac) { %>
								<br>Price Groups
								<asp:CheckBoxList id="m_EmployeePermissionsPriceGroups" onclick="onPriceGroupsClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<% } %>
							<% if (ShowEDocs) { %>
								<br>EDocs
								<asp:CheckBoxList id="m_EmployeePermissionsEDocs"  runat="server" cellpadding="0" cellspacing="0" OnDataBound="chkEDocs_OnItemDataBound"></asp:CheckBoxList>
							<% } %>
							<br />Disclosures
							<asp:CheckBoxList id="m_EmployeePermissionsDisclosures" onclick="onDisclosuresClick();"   runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<% if (ShowTotalScorecard) { %>
								<br>Total Scorecard
								<asp:CheckBoxList id="m_EmployeePermissionsTotalScorecard" onclick="onDisclosuresClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<% } %>
							<BR>Print Groups
							<asp:CheckBoxList id="m_EmployeePermissionsPrintGroups" onclick="onPrintGroupsClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<BR><ml:EncodedLabel id="m_ExternalUsersLabel" Runat="server">Originator Portal Administration</ml:EncodedLabel>
							<asp:CheckBoxList id="m_EmployeePermissionsExternalUsers" onclick="onExternalUsersClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<BR><ml:EncodedLabel id="m_pricingEngineLabel" Runat="server">PriceMyLoan</ml:EncodedLabel></B>
							<asp:CheckBoxList id="m_EmployeePermissionsPricingEngine" onclick="onPricingEngineClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<br /><b>Mortgage Banking</b>
							<asp:CheckBoxList id="m_EmployeePermissionsMortgageBanking" onclick="onMortgageBankingClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<br />
							<asp:PlaceHolder runat="server" ID="CapitalMarketCheckListContainer" Visible="false">
							<b>Capital Markets</b>
							<asp:CheckBoxList id="m_EmployeePermissionsCapitalMarkets" onclick="onCapitalMarketsClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<br />
							</asp:PlaceHolder>
							<b>Administration</b>
							<asp:CheckBoxList id="m_EmployeePermissionsAdministration" onclick="onAdministrationClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
							<br />
                            <b>Test Loans</b>
							<asp:CheckBoxList id="m_EmployeePermissionsTestLoans" onclick="onTestLoansClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
                            <br />
                            <b>User Experience</b>
							<asp:CheckBoxList id="m_EmployeePermissionsUserExperience" onclick="onUserExperienceClick();" runat="server" cellpadding="0" cellspacing="0"></asp:CheckBoxList>
                                <asp:PlaceHolder runat="server" ID="DocumentReviewSection">
                                    <br />
                                    <b>Document Review</b>
                                    <asp:CheckBoxList runat="server" ID="m_EmployeePermissionsDocumentReview" onclick="onDocumentReviewClick();" runat="server" cellpadding="0" cellspacing="0" ></asp:CheckBoxList>
                                </asp:PlaceHolder>

                            </asp:Panel>
						<% } else { %>
							<asp:Panel ID="m_BatchPermissionsPanel" Runat="server">
                            <asp:PlaceHolder runat="server" ID="BatchLeadPlaceHolder">
                            <div style="PADDING-LEFT:115px">
                            <b>Leads</b>
                            </div>
							<asp:Repeater id="m_BatchPermissionsLeads" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
                            <br />
                            </asp:PlaceHolder>
                            <div style="PADDING-LEFT:115px">
                            <b>Loans</b>
                            </div>
							<asp:Repeater id="m_BatchPermissionsLoans" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							<div style="PADDING-LEFT:115px">
							<br><B>Loan Templates</b>
							</div>
							<asp:Repeater id="m_BatchPermissionsTemplates" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onTemplatesClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel1" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onTemplatesClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onTemplatesClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							<div style="PADDING-LEFT:115px">
							<br><B>Closing Cost Templates</b>
							</div>
							<asp:Repeater id="m_BatchPermissionsCCTemplates" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel7" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							<div style="PADDING-LEFT:115px">
							<br><B>Custom Reports</b>
							</div>
							<asp:Repeater id="m_BatchPermissionsCustomReports" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onCustomReportsClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel2" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onCustomReportsClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onCustomReportsClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							<div style="PADDING-LEFT:115px">
							<br><B>Custom Forms</b>
							</div>
							<asp:Repeater id="m_BatchPermissionsCustomForms" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onCustomFormsClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel8" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onCustomFormsClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onCustomFormsClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							<div style="PADDING-LEFT:115px">
							<br><B>Contacts</b>
							</div>
							<asp:Repeater id="m_BatchPermissionsContacts" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onContactsClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel3" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onContactsClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onContactsClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							<% if ( ShowDataTrac ) {%>
							<div style="Padding-LEFT:115px">

								<br> <b> Price Groups </b>
							</div>
								<asp:Repeater id="m_BatchPermissionsPriceGroups" runat="server">
									<ItemTemplate>
										<asp:RadioButton id="KeepCurrentPermission" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
										</asp:RadioButton>
										<asp:Panel id="Panel9" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
											<asp:RadioButton id="On" runat="server" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
											</asp:RadioButton>
											<asp:RadioButton id="Off" runat="server" style="PADDING-RIGHT:12px" GroupName="Option">
											</asp:RadioButton>
											<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
										</asp:Panel>
										<br>
								</ItemTemplate>
							</asp:Repeater>
							<% } %>
							<% if ( ShowEDocs ) {%>
							<div style="Padding-LEFT:115px">
								<br> <b> EDocs </b>
							</div>
								<asp:Repeater id="m_BatchPermissionsEDocs" runat="server">
									<ItemTemplate>
										<asp:RadioButton id="KeepCurrentPermission" onclick="onEDocsClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
										</asp:RadioButton>
										<asp:Panel id="Panel9" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
											<asp:RadioButton id="On" runat="server" onclick="onEDocsClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
											</asp:RadioButton>
											<asp:RadioButton id="Off" runat="server" onclick="onEDocsClick();" style="PADDING-RIGHT:12px" GroupName="Option">
											</asp:RadioButton>
											<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
										</asp:Panel>
										<br>
								</ItemTemplate>
							</asp:Repeater>
							<% } %>
							<div style="Padding-LEFT:115px">
								<br> <b> Disclosures </b>
							</div>
							<asp:Repeater id="m_BatchPermissionsDisclosures" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel10" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel>
									<br>
								</ItemTemplate>
							</asp:Repeater>
							<% if ( ShowTotalScorecard ) {%>
							    <div style="Padding-LEFT:115px">
								<br> <b> Total Scorecard </b>
							</div>
								<asp:Repeater id="m_BatchPermissionsTotalScorecard" runat="server">
									<ItemTemplate>
										<asp:RadioButton id="KeepCurrentPermission" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
										</asp:RadioButton>
										<asp:Panel id="Panel9" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
											<asp:RadioButton id="On" runat="server" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
											</asp:RadioButton>
											<asp:RadioButton id="Off" runat="server" style="PADDING-RIGHT:12px" GroupName="Option">
											</asp:RadioButton>
											<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
										</asp:Panel>
										<br>
								</ItemTemplate>
							</asp:Repeater>
							<% } %>
							<div style="PADDING-LEFT:115px">
								<br><B>Print Groups</b>
							</div>
							<asp:Repeater id="m_BatchPermissionsPrintGroups" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" runat="server" onclick="onPrintGroupsClick();" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel4" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onPrintGroupsClick();"  code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onPrintGroupsClick();"  style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							<div style="PADDING-LEFT:115px">
							<br><B><ml:EncodedLabel id="m_BatchExternalUsersLabel" Runat="server">PriceMyLoan User Administration</ml:EncodedLabel></b>
							</div>
							<asp:Repeater id="m_BatchPermissionsExternalUsers" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel5" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							<div style="PADDING-LEFT:115px">
							<br><B><ml:EncodedLabel id="m_BatchPricingEngineLabel" Runat="server">PriceMyLoan</ml:EncodedLabel></B>
							</div>
							<asp:Repeater id="m_BatchPermissionsPricingEngine" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onPricingEngineClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel6" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onPricingEngineClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onPricingEngineClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							<div style="padding-left:115px">
								<br /><b>Mortgage Banking</b>
							</div>
							<asp:Repeater id="m_BatchPermissionsMortgageBanking" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onMortgageBankingClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel4" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onMortgageBankingClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onMortgageBankingClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							<div style="padding-left:115px">
								<br /><b>Mortgage Banking</b>
							</div>
							<asp:PlaceHolder runat="server" ID="CapitalMarketsContainer" Visible="false">
							<asp:Repeater id="m_BatchPermissionsCapitalMarkets" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onCapitalMarketsClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel8" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onCapitalMarketsClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onCapitalMarketsClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							</asp:PlaceHolder>
							<div style="PADDING-LEFT:115px">
								<B>Administration</b>
							</div>
							<asp:Repeater id="m_BatchPermissionsAdministration" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onAdministrationClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel9" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onAdministrationClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onAdministrationClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
							</asp:Panel>
                            <div style="padding-left:115px">
								<br /><b>Test Loans</b>
							</div>
							<asp:Repeater id="m_BatchPermissionsTestLoans" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onTestLoansClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel4" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onTestLoansClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onTestLoansClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
                                <div style="padding-left:115px">
								<br /><b>User Experience</b>
							</div>
                            <asp:Repeater id="m_BatchPermissionsUserExperience" runat="server">
								<ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onUserExperienceClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel4" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onUserExperienceClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onUserExperienceClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
							</asp:Repeater>
                                    <div style="padding-left:115px">
								<br /><b>Document Review</b>
							</div>
                            <asp:Repeater runat="server" ID="m_BatchPermissionsDocumentReview" runat="server">
                                <ItemTemplate>
									<asp:RadioButton id="KeepCurrentPermission" onclick="onDocumentReviewClick();" runat="server" style="PADDING-LEFT:15px" GroupName="Option" Checked="True">
									</asp:RadioButton>
									<asp:Panel id="Panel4" runat="server" style="DISPLAY: inline; PADDING-LEFT:20px">
										<asp:RadioButton id="On" runat="server" onclick="onDocumentReviewClick();" code=<%# AspxTools.HtmlString(Container.DataItem) %> GroupName="Option">
										</asp:RadioButton>
										<asp:RadioButton id="Off" runat="server" onclick="onDocumentReviewClick();" style="PADDING-RIGHT:12px" GroupName="Option">
										</asp:RadioButton>
										<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" )) %>
									</asp:Panel><br>
								</ItemTemplate>
                            </asp:Repeater>
						<% } %>
						</asp:Panel>
					</div>
				</td>
			</tr>
		</table>
		<uc:CModalDlg runat="server" ID="Cmodaldlg1"></uc:CModalDlg>
	</body>
</HTML>
