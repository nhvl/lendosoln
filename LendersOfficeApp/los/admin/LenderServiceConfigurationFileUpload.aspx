﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LenderServiceConfigurationFileUpload.aspx.cs" Inherits="LendersOfficeApp.los.admin.LenderServiceConfigurationFileUpload" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Configuration File Upload</title>
    <style type="text/css">
        #form1 {
            margin: 0 auto;
            width: 35em;
        }
        #form1 div {
            padding: 1em 0;
        }
        .center {
            text-align: center;
        }
        input[type=button], input[type=submit] {
            margin-left: .5em;
            margin-right: .5em;
        }
        input[type=file] {
            width: 30em;
        }
        #Spacer {
            margin-left: 1em;
        }
    </style>
</head>
<body>
    <h4 class="page-header">Configuration File Upload</h4>
    <form id="form1" runat="server">
        <div>
            <div>Please select a configuration file for this title quote provider</div>
            <input id="FileUpload" type="file" runat="server" />
        </div>
        <div>
            <a href="#" id="DownloadLink">download current file</a>
            <span id="Spacer"></span>
            <a href="#" id="DownloadTemplateLink">download template</a>
        </div>
        <div id="Message" runat="server">
        </div>
        <div class="center">
            <asp:Button Text="Upload" ID="UploadBtn" runat="server" />
            <input type="button" value="Close" id="CloseBtn" />
        </div>
    </form>
    <script type="text/javascript">
        function CloseWindow() {
            parent.LQBPopup.Return({
                FileKeyTempKey: (typeof uploadedFileKeyTempKey === 'undefined' ? undefined : uploadedFileKeyTempKey)
            });
        }
        function onFileUploadChange(e) {
            $('#UploadBtn').prop('disabled', !$('#FileUpload').val());
        }
        function AddQueryArgument(queryString, key, value) {
            var resultingValue = encodeURIComponent(key) + '=' + encodeURIComponent(value);
            if (!queryString) {
                return '?' + resultingValue;
            }

            var regex = new RegExp('(\\?|&)' + encodeURIComponent(key) + '=[^&]');
            if (regex.test(queryString)) {
                return queryString.replace(regex, '$1' + resultingValue);
            } else {
                return queryString + '&' + resultingValue;
            }
        }
        function Download() {
            if (noConfiguration) {
                return;
            }

            document.location.search = AddQueryArgument(document.location.search, 'download', 't');
        }
        function DownloadTemplate() {
            document.location.search = AddQueryArgument(document.location.search, 'downloadTemplate', 't');
        }


        $('#FileUpload').change(onFileUploadChange).change();
        $('#DownloadLink').click(Download).prop('disabled', noConfiguration).attr('title', noConfiguration ? 'No configuration has been uploaded' : '');
        $('#DownloadTemplateLink').click(DownloadTemplate);
        $('#CloseBtn').click(CloseWindow);
    </script>
</body>
</html>
