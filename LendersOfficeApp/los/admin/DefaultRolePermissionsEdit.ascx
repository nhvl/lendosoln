<%@ Register TagPrefix="uc" TagName="EmployeePermissionsList" Src="../../los/admin/EmployeePermissionsList.ascx" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DefaultRolePermissionsEdit.ascx.cs" Inherits="LendersOfficeApp.los.admin.DefaultRolePermissionsEdit" %>

<script type="text/javascript">
	function onInit()
	{
		<% if( IsPostBack == false ) { %>
			resize( 470 , 670 );
		<% } %>
		var alertMsg = <%= AspxTools.JsGetElementById(m_AlertMessage) %>;

		if(alertMsg != null && alertMsg.value != "")
			alert(alertMsg.value);

		var commandToDo = <%= AspxTools.JsGetElementById(m_commandToDo) %>;

		if( commandToDo != null )
		{
			if( commandToDo.value == "Close" )
			{
				var args = window.dialogArguments || {};
				args.OK = true;

				onClosePopup(args);
			}
		}

		makeClean();
	}

	function f_closeExpirationDetails() {
		document.getElementById("ExpirationDetails").style.display = "none";
	}
	function f_showExpirationDetails() {
		var msg = document.getElementById("ExpirationDetails");
		msg.style.display = "";
		msg.style.top = "297px";
		msg.style.left = "72px";
	}

	function isUserEditorDirty()
	{
		return <%= AspxTools.JsGetElementById(m_Dirty) %>.value == "1";
	}

	function onClientOkClick()
	{
	    if ( isUserEditorDirty() )
	    {
	        var okBtn = <%= AspxTools.JsGetElementById(m_Ok) %>;
	        okBtn.disabled = false;
	        okBtn.click();
        }
	    else
	        onClosePopup();
	}

	function onClose()
	{
		if ( isUserEditorDirty() )
		{
			if( confirm( "Close without saving?" ) == false )
				return;
		}
		onClosePopup();
	}

	function onRolesChange(ddl)
	{
		var commandToDo = <%= AspxTools.JsGetElementById(m_commandToDo) %>;
		if(isUserEditorDirty() && confirm("Save Changes?"))
		{
		    commandToDo.value = "SaveAndChangeRole";
		    var m_Apply = <%= AspxTools.JsGetElementById(m_Apply) %>;
		    m_Apply.disabled = false;
			m_Apply.click();
		}
		else
		{
		    commandToDo.value = "ChangeRole";
		    var m_submit = document.getElementById('m_submit');
		    m_submit.disabled = false;
			m_submit.click();
		}
	}

	function onRolesClick(ddl)
	{
		<%= AspxTools.JsGetElementById(m_LastIndexSelected) %>.value = ddl.selectedIndex;
	}

	function makeClean()
	{
		<%= AspxTools.JsGetElementById(m_Dirty) %>.value = "0";
		<%= AspxTools.JsGetElementById(m_Apply) %>.disabled = true;
	}

	function makeDirty()
	{
		<%= AspxTools.JsGetElementById(m_Dirty) %>.value = "1";
		<%= AspxTools.JsGetElementById(m_Apply) %>.disabled = false;
	}
</script>
<h4 class="page-header">Edit Default Role Permissions</h4>
<table cellspacing="2" cellpadding="5" border="0" width="100%">
	<tr>
		<td>
			<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
				<tr>
					<td style="BORDER-RIGHT: 2px outset; BORDER-TOP: 2px outset; BORDER-LEFT: 2px outset; BORDER-BOTTOM: 2px outset; BACKGROUND-COLOR: gainsboro">
						<div style="BORDER-RIGHT: 0px; PADDING-RIGHT: 2px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 0px; PADDING-BOTTOM: 4px; BORDER-LEFT: lightgrey 2px solid; PADDING-TOP: 0px; BORDER-BOTTOM: lightgrey 2px solid;">
							<table cellspacing="0" cellpadding="3" border="0" width="100%">
								<tr valign="top">
									<td style="WIDTH: 30px">
									</td>
									<td style="FONT-WEIGHT: bold" width="1500px" colspan="3">
										<br>
										Select a role and set the default permissions for that role
									</td>
								</tr>
								<tr>
									<td style="WIDTH: 30px">
									</td>
									<td>
										<asp:DropDownList ID="m_Roles" onclick="onRolesClick(this);" onchange="onRolesChange(this);" Runat="server"></asp:DropDownList>
									</td>
									<td>
									</td>
								</tr>
								<tr>
								<td style="WIDTH: 30px">
									</td>
									<td valign="top">
										<% if ( IsPmlEnabled ) { %>
										<div id="ExpirationDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:548px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; BACKGROUND-COLOR:whitesmoke">
											<table bgcolor=black style="WIDTH:100%" cellspacing=1 cellpadding=2>
											<tr>
												<td bgcolor=gainsboro>&nbsp;</td>
												<td colspan=3 align=center bgcolor=gainsboro width="*"><span style="FONT-WEIGHT:bold">Rate lock request access<br>for <span style="FONT-STYLE:italic">possibly</span> expired rates</span></td>
											</tr>
											<tr>
												<td valign=top bgcolor=gainsboro style="WIDTH:260px"><span style="FONT-WEIGHT:bold">Scenario</span></td>
												<td align=center bgcolor=whitesmoke valign=top style="WIDTH:85px">"Never<br>allowed"</td>
												<td align=center bgcolor=whitesmoke style="WIDTH:85px">"Not allowed<br>after investor<br>cut-off time"</td>
												<td align=center bgcolor=whitesmoke valign=top style="WIDTH:85px">"Always<br>allowed"</td>
											</tr>
											<tr style="HEIGHT:36px">
												<td bgcolor=whitesmoke valign=top>Pricing time is after investor cut-off</td>
												<td align=center bgcolor=#ffcccc valign=top>blocked</td>
												<td align=center bgcolor=#ffcccc valign=top>blocked</td>
												<td align=center bgcolor=#ccffcc valign=top>allowed</td>
											</tr>
											<tr style="HEIGHT:36px">
												<td bgcolor=whitesmoke nowrap valign=top>Pricing time is outside lender lock desk hours</td>
												<td align=center bgcolor=#ffcccc valign=top>blocked</td>
												<td align=center bgcolor=#ccffcc valign=top>allowed</td>
												<td align=center bgcolor=#ccffcc valign=top>allowed</td>
											</tr>
											<tr>
												<td bgcolor=whitesmoke>New rate sheet is detected but updated<br><span style="WIDTH:10px">&nbsp;</span>pricing is not released to users yet</td>
												<td align=center bgcolor=#ffcccc valign=top>blocked</td>
												<td align=center bgcolor=#ccffcc valign=top>allowed</td>
												<td align=center bgcolor=#ccffcc valign=top>allowed</td>
											</tr>
											<tr>
												<td bgcolor=whitesmoke>Technical difficulty is encountered when<br><span style="WIDTH:10px">&nbsp;</span>attempting to access or process rate sheet</td>
												<td align=center bgcolor=#ffcccc valign=top>blocked</td>
												<td align=center bgcolor=#ccffcc valign=top>allowed</td>
												<td align=center bgcolor=#ccffcc valign=top>allowed</td>
											</tr>
											<tr>
												<td align=center bgcolor=whitesmoke colspan=4><br>[ <a href="#" onclick="f_closeExpirationDetails();">Close</a> ]</td>
											</tr>
											</table>
										</div>
										<div style="MARGIN: 8px"></div>
										<div class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">Rate lock requests for <span style="FONT-STYLE:italic">possibly</span> expired rates <a href="#" style="display:none" onclick="f_showExpirationDetails();">?</a></div>
										<div id=m_rateLockDiv style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
											<asp:radiobutton id="m_noBypass" runat="server" Text="Never allowed" GroupName="RatesheetExpiration" onclick="makeDirty();"></asp:radiobutton>
											<br>
											<asp:radiobutton id="m_bypassExcInvCutoff" runat="server" Text="Not allowed after investor cut-off time" onclick="makeDirty();" GroupName="RatesheetExpiration"></asp:radiobutton>
											<br>
											<asp:radiobutton id="m_bypassAll" runat="server" Text="Always allowed" onclick="makeDirty();" GroupName="RatesheetExpiration"></asp:radiobutton>
										</div>
										<% } %>
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td></td>
									<td>
										<div id="m_PermissionsDiv" align="left" style="BORDER-RIGHT: 0px; PADDING-RIGHT: 8px; BORDER-TOP: lightgrey 2px solid; OVERFLOW-Y: scroll; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 350px; PADDING-TOP: 8px; BORDER-BOTTOM: lightgrey 2px solid; HEIGHT: 390px; BACKGROUND-COLOR: white">
											<uc:EmployeePermissionsList id="m_EmployeePermissionsList" runat="server"></uc:EmployeePermissionsList>
										</div>
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td style="WIDTH: 30px">
									</td>
									<td>
										<input type="button" id="m_OKseen" value=" OK " onclick="onClientOkClick();" style="WIDTH: 60px">
										<input type="button" style="WIDTH: 60px" value="Cancel" onclick="onClose();">
										<asp:button id="m_Apply" runat="server" Text="Apply" OnClick="ApplyClick" Width="60"></asp:button>
										<asp:button id="m_Ok" Enabled="false" runat="server" Text="OK" OnClick="OkClick" style="DISPLAY: none"></asp:button>
										<input type="submit" disabled id="m_submit" style="DISPLAY: none" value="Submit Query">
									</td>
									<td>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<uc:CModalDlg runat="server" ID="Cmodaldlg1" />
<input type="hidden" runat="server" id="m_Dirty" value="0" NAME="m_Dirty">
<input type="hidden" runat="server" id="m_LastIndexSelected" NAME="m_LastIndexSelected">
<input type="hidden" runat="server" id="m_commandToDo" NAME="m_commandToDo">
<input type="hidden" runat="server" id="m_AlertMessage" NAME="m_AlertMessage">
<script type="text/javascript">onInit();</script>
