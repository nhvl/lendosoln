<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="ShowLoanTemplatesAssociatedWithBranch.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.ShowLoanTemplatesAssociatedWithBranch" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>View Users</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto; BACKGROUND-COLOR: #003366">
		<script language="javascript">
<!--
	function _init() 
    {
        resize( 700 , 800 );
	}
	
	function checkAllBoxes(obj)
	{
		var checked=false;
		var table = document.getElementById("m_dg");

		if( obj.checked )
		{
			checked=true;
		}
		for( var i=1; i<table.rows.length; i++ )
		{
			table.rows[i].cells[0].childNodes[0].checked = checked;
			onClickCheckRow(table.rows[i].cells[0].childNodes[0]);
		}
		
		checkboxChanged(obj);
	}
	
	function checkboxChanged(obj)
	{
		var enable = false;
		var table = document.getElementById("m_dg");
		var button = <%= AspxTools.JsGetElementById(m_AssignLoanTemplates) %>;
		var continueScanning = true;
		
		var numberOfRows = table.rows.length;
		var currentRowNumber = 1;
		
		// If this is true it means that only the header row exists.
		if ( currentRowNumber == numberOfRows )
		{
			continueScanning = false;
		}
		
		while ( continueScanning )
		{
			enable = table.rows[currentRowNumber].cells[0].childNodes[0].checked;
			currentRowNumber++;

			if ( currentRowNumber == numberOfRows || enable )
			{
				continueScanning = false;
			}
			
		}

		button.disabled = !enable;
	}
	
	// 08/26/08 ck - OPM 23916. Highlight rows in salmon when checked.
	function onClickCheckRow( obj ) 
	{ 
		try
		{
			// Apply row-check effects to grid.
			if( obj.checked != null )
			{		
				highlightRowByCheckbox( obj );
			}
			
			checkboxChanged(obj);
		}
		catch( e )
		{
			window.status = e.message;
		}
	}
		
	//-->
	</script>
		<h4 class="page-header">View Loan Templates</h4>
		<form id="ShowLoanTemplates" method="post" runat="server">
			<table cellspacing="0" cellpadding="8" border="0" width="100%" height="100%">
				<tr>
					<td height="100%">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
							<tr>
								<td height="0%" style="BORDER-RIGHT: 2px outset; BORDER-TOP: 2px outset; BORDER-LEFT: 2px outset; BACKGROUND-COLOR: gainsboro">
									<table cellspacing="2" cellpadding="3" border="0" width="100%" height="100%">
										<tr>
											<td nowrap height="0%">
												<asp:Panel id="m_headerPanel" style="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 0px; BORDER-TOP: 2px groove; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; BORDER-LEFT: 2px groove; PADDING-TOP: 0px; BORDER-BOTTOM: 2px groove; BACKGROUND-COLOR: gainsboro" Runat="server">
													<table height="100" cellspacing="10" cellpadding="0" width="100%" border="0">
														<tr>
															<td style="FONT-WEIGHT: bold">
																<ml:EncodedLabel style="FONT-SIZE:13px" id="m_branchNameLabel" Runat="server"></ml:EncodedLabel><br>
																<ml:EncodedLabel id="m_tooManyLoanTemplates" Runat="server"></ml:EncodedLabel><br>
															</td>
														</tr>
														<tr>														
															<td>
																<ml:EncodedLabel runat="server">Selected loan templates will assign loans to branch: </ml:EncodedLabel>
																<asp:DropDownList id="m_BranchDropdownList" runat="server"></asp:DropDownList>
																<asp:Button id="m_AssignLoanTemplates" onclick="AssignLoanTemplatesClick" runat="server" Text="Modify Loan Templates" Enabled="False" style="width: 140px;"></asp:Button>
															</td>
														</tr>
													</table>
												</asp:Panel>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr height="100%">	
								<td>
									<table cellpadding="3" cellspacing="0" height="100%" width="100%">
										<td height="100%" style="BORDER-RIGHT:2px outset; BORDER-LEFT:2px outset; BORDER-BOTTOM:2px outset; BACKGROUND-COLOR:gainsboro">
											<table cellspacing="0" cellpadding="2" height="100%" width="100%">
												<tr height="100%">
													<td>
														<div style="OVERFLOW-Y:scroll; BORDER-RIGHT:0px; PADDING-RIGHT:4px; PADDING-LEFT:4px; BORDER-LEFT:lightgrey 2px solid; PADDING-TOP:4px; BORDER-BOTTOM:lightgrey 2px solid; BORDER-TOP:lightgrey 2px solid; height=100%" valign="top">
															<ml:CommonDataGrid id="m_dg" runat="server" valign="top" OnItemDataBound="m_dg_ItemDataBound">
																<ALTERNATINGITEMSTYLE cssclass="GridAlternatingItem"></ALTERNATINGITEMSTYLE>
																<ITEMSTYLE cssclass="GridItem"></ITEMSTYLE>
																<HEADERSTYLE cssclass="GridHeader"></HEADERSTYLE>
																<COLUMNS>
																	<ASP:TEMPLATECOLUMN ItemStyle-Width="4%">
																		<HEADERTEMPLATE>
																			<asp:CheckBox id=m_SelectAllBox onclick=checkAllBoxes(this); Runat="server"></asp:CheckBox>
																		</HEADERTEMPLATE>
																		<ITEMTEMPLATE>
																			<asp:CheckBox id=m_CheckBox Runat="server" onclick="onClickCheckRow(this)" ></asp:CheckBox>
                                                                            <ml:EncodedLabel ID="m_LID" Visible="false" runat=server></ml:EncodedLabel>
																		</ITEMTEMPLATE>
																	</ASP:TEMPLATECOLUMN>
																	<asp:BoundColumn DataField="loanName" ItemStyle-Width="96%" DataFormatString="{0:d}" SortExpression="loanName" HeaderText="Loan Template Name"></asp:BoundColumn>
																</COLUMNS>
															</ml:CommonDataGrid>
														</div>
													</td>
												</tr>
												<tr>
													<td style="FONT-WEIGHT: bold; FONT-SIZE: 12px">
														<ml:EncodedLabel id="m_noneToShowLabel" ForeColor="green" Runat="server">There are no loan templates to display</ml:EncodedLabel>
													</td>
												</tr>
												<tr>
													<td align="right">
														<br>
														<input id="close" onclick="onClosePopup();" type="button" value="Close" name="close" Runat="server" style="width: 50;">
													</td>
												</tr>
											</table>
										</td>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
	</body>
</HTML>
