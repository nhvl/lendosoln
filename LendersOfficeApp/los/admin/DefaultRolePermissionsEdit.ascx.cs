using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Events;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOfficeApp.los.admin
{
	/// <summary>
	/// This class allows broker admins to set default permissions for each role, per OPM 9022
	/// </summary>
	public partial class DefaultRolePermissionsEdit : System.Web.UI.UserControl
	{
		protected System.Web.UI.HtmlControls.HtmlInputButton		close;
		protected System.Web.UI.WebControls.Panel					m_PermissionsPanel;
		protected System.Web.UI.HtmlControls.HtmlInputHidden        m_SelectedIndex;
		protected Guid												m_BrokerId = Guid.Empty;
		protected EmployeePermissionsList							m_EmployeePermissionsList;
		
		public Guid BrokerId
		{
			set
			{
				ViewState.Add( "BrokerId" , m_BrokerId = value );
			}
			get
			{
				return m_BrokerId;
			}
		}

        public bool IsPmlEnabled
        {
            set { ViewState["IsPmlEnabled"] = value; }
            get
            {
                if (ViewState["IsPmlEnabled"] != null)
                    return (bool)ViewState["IsPmlEnabled"];
                else
                {
                    BrokerFeatures bFs = new BrokerFeatures(m_BrokerId);
                    return (bool)(ViewState["IsPmlEnabled"] = bFs.HasFeature(E_BrokerFeatureT.PriceMyLoan));
                }
            }
        }

		private Guid RoleId
		{
			get 
			{ 
				if(m_commandToDo.Value.Equals("SaveAndChangeRole"))
				{
                    int selectedIndex = (m_LastIndexSelected.Value.TrimWhitespaceAndBOM() == "")?0:int.Parse(m_LastIndexSelected.Value);
					return new Guid( m_Roles.Items[selectedIndex].Value); 
				}
				else
				{
					if(m_Roles.SelectedIndex == -1)
						return new Guid( m_Roles.Items[0].Value);
					return new Guid( m_Roles.Items[m_Roles.SelectedIndex].Value);
				}
			}
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
            m_EmployeePermissionsList.m_commandToDo = m_commandToDo.Value;
			if(!IsPostBack)
			{	
				BindControls();
				DataBind();
                BindNonBoolPermissions();
            }

			if(m_commandToDo.Value.Equals("ChangeRole"))
			{
				m_commandToDo.Value = "";
                DataBind();
                BindNonBoolPermissions();
            }

			m_EmployeePermissionsList.DataBroker = BrokerId;
			m_EmployeePermissionsList.DataSource = RoleId;
			m_EmployeePermissionsList.PermissionType = "role";
		}

        // OPM 19980
        private void BindNonBoolPermissions()
        {
            BrokerUserPermissionsNonBool bUp = new BrokerUserPermissionsNonBool();
            bUp.RetrieveRolePermissions(BrokerId, RoleId);

            E_RatesheetExpirationBypassType bypassT = (E_RatesheetExpirationBypassType)Enum.Parse(typeof(E_RatesheetExpirationBypassType), bUp.GetPermission(NonBoolPermission.RateLockRequestForExpiredRate).ToString());
            switch(bypassT)
            {
                case E_RatesheetExpirationBypassType.BypassAll:
                    m_bypassAll.Checked = true;
                    m_bypassExcInvCutoff.Checked = false;
                    m_noBypass.Checked = false;
                    break;
                case E_RatesheetExpirationBypassType.BypassAllExceptInvCutoff:
                    m_bypassExcInvCutoff.Checked = true;
                    m_bypassAll.Checked = false;
                    m_noBypass.Checked = false;
                    break;
                case E_RatesheetExpirationBypassType.NoBypass:
                    m_noBypass.Checked = true;
                    m_bypassExcInvCutoff.Checked = false;
                    m_bypassAll.Checked = false;
                    break;
                default:
                    m_bypassAll.Checked = true;
                    m_bypassExcInvCutoff.Checked = false;
                    m_noBypass.Checked = false;
                    Tools.LogBug("Unable to load non bool permissions - Unhandled E_RatesheetExpirationBypassType: " + bypassT);
                    break;
            }
        }

		public void BindControls()
		{
			//bind the roles
            m_Roles.DataSource = Role.LendingQBRoles;
			m_Roles.DataTextField  = "ModifiableDesc";
			m_Roles.DataValueField = "Id";
			m_Roles.DataBind();
		}
		
		protected void ControlLoad( object sender , System.EventArgs a )
		{	
			if( ViewState[ "BrokerId" ] != null )
				m_BrokerId = ( Guid ) ViewState[ "BrokerId" ];
		}

		/// <summary>
		/// Store permissions and set permission checkbox state
		/// </summary>
		private System.Text.StringBuilder BuildPermissionsString(System.Web.UI.WebControls.ListItemCollection items, BrokerUserPermissions bUp )
		{	
			System.Text.StringBuilder permissions = new System.Text.StringBuilder();
			foreach( ListItem lItem in items )
			{
				lItem.Selected = bUp.HasPermission( int.Parse( lItem.Value ) );
				permissions.Append( "," + lItem.Selected.ToString());
			}
			return permissions;
		}

		private void SetPermissions(System.Web.UI.WebControls.ListItemCollection items, BrokerUserPermissions bUp)
		{
			foreach( ListItem pItem in items )
			{
				bUp.SetPermission( int.Parse( pItem.Value ) , pItem.Selected );
			}
		}

		protected void ApplyClick( object sender , System.EventArgs a )
		{
            try
			{
				m_EmployeePermissionsList.Apply("role");
                int rsExpPermission = (int)E_RatesheetExpirationBypassType.BypassAll;

                if (m_noBypass.Checked)
                {
                    rsExpPermission = (int)E_RatesheetExpirationBypassType.NoBypass;
                }
                else if (m_bypassExcInvCutoff.Checked)
                {
                    rsExpPermission = (int)E_RatesheetExpirationBypassType.BypassAllExceptInvCutoff;
                }
                else
                {
                    rsExpPermission = (int)E_RatesheetExpirationBypassType.BypassAll;
                }

                BrokerUserPermissionsNonBool bUp = new BrokerUserPermissionsNonBool();
                bUp.RetrieveRolePermissions(BrokerId, RoleId);
                bUp.SetPermission(NonBoolPermission.RateLockRequestForExpiredRate, rsExpPermission);

                using (CStoredProcedureExec spEx = new CStoredProcedureExec(BrokerId))
                {
                    spEx.BeginTransactionForWrite();
                    bUp.UpdateRolePermissions(spEx, BrokerId, RoleId);
                    spEx.CommitTransaction();
                }

                if (m_commandToDo.Value.Equals("SaveAndChangeRole"))
                {
                    m_commandToDo.Value = "";
                }
                m_EmployeePermissionsList.DataSource = RoleId;
                m_EmployeePermissionsList.PermissionsDataBind();

                BindNonBoolPermissions();
			}
			catch( Exception e )
			{
				m_AlertMessage.Value = "Unable to save role permissions";
				Tools.LogError( "Unable to save role permissions: " + e );
			}
		}

		protected void OkClick( object sender , System.EventArgs a )
		{
			ApplyClick(sender, a);
			m_commandToDo.Value = "Close";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.ControlLoad);
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
