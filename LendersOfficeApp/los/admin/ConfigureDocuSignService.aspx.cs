﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using DataAccess;
    using LendersOffice.Admin.DocuSign;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentSigning;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Service page for the <see cref="ConfigureDocuSign"/> page.
    /// </summary>
    public partial class ConfigureDocuSignService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// The callback URL is a redirect from DocuSign, so it needs a full absolute URL, using the same authority as the user's original request.
        /// If the URL isn't registered with the integrator key in DocuSign, the authentication in their system will fail. 
        /// Make sure to register "/los/admin/DocuSignAuthCallback.aspx" for every environment where users might log in to DocuSign for consent.
        /// </summary>
        private Lazy<LqbAbsoluteUri> docuSignAuthCallbackUri = new Lazy<LqbAbsoluteUri>(() => LqbAbsoluteUri.Create($"{HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)}{Tools.VRoot}/los/admin/DocuSignAuthCallback.aspx").ForceValue());

        /// <summary>
        /// Gets the permission required to load this page.
        /// </summary>
        /// <value>The permission required for page load.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Process the service request.
        /// </summary>
        /// <param name="methodName">The method name to invoke server-side.</param>
        protected override void Process(string methodName)
        {
            AbstractUserPrincipal user = PrincipalFactory.CurrentPrincipal;
            switch (methodName)
            {
                case nameof(SaveDocuSignOptions):
                    SaveDocuSignOptions(
                        user,
                        this.GetNullableBool("DocuSignEnabled"),
                        this.GetString("TargetEnvironment", null).ToNullableEnum<DocuSignEnvironment>(ignoreCase: true),
                        this.GetString("EnabledMfa", null),
                        this.GetString("DefaultMfaOption", null).ToNullableEnum<MfaOptions>(ignoreCase: true),
                        this.GetString("IdCheckConfigurationName", null),
                        this.GetNullableGuid("BrandIdUnspecified", null),
                        this.GetNullableGuid("BrandIdRetail", null),
                        this.GetNullableGuid("BrandIdWholesale", null),
                        this.GetNullableGuid("BrandIdCorrespondent", null),
                        this.GetNullableGuid("BrandIdBroker", null),
                        this.GetString("BrandNameUnspecified", null),
                        this.GetString("BrandNameRetail", null),
                        this.GetString("BrandNameWholesale", null),
                        this.GetString("BrandNameCorrespondent", null),
                        this.GetString("BrandNameBroker", null));
                    return;
                case nameof(RegisterUser):
                    RegisterUser(
                        user,
                        this.GetString("code"),
                        this.GetString("state"));
                    return;
                case nameof(GetOAuthConsentUrl):
                    GetOAuthConsentUrl(
                        user,
                        this.GetBool("useSandbox"));
                    return;
                default:
                    throw new CBaseException(ErrorMessages.Generic, $"ConfigureDocuSignService.aspx: Unknown service method \"{methodName}\"");
            }
        }

        /// <summary>
        /// Save the DocuSign configuration options from the "General Settings" UI.
        /// </summary>
        /// <param name="user">The user making the request.</param>
        /// <param name="enabled">Whether DocuSign should be enabled.</param>
        /// <param name="environment">The DocuSign environment to use.</param>
        /// <param name="enabledMfaJsonString">A JSON string containing values indicating whether each of the MFA options is enabled.</param>
        /// <param name="mfaDefault">The default multi-factor authentication mode to set.</param>
        /// <param name="idcheckConfigurationName">The configuration name to send to DocuSign for ID Check MFA.</param>
        /// <param name="brandIdUnspecified">The brand ID to use for loans with a channel of <see cref="E_BranchChannelT.Blank"/>.</param>
        /// <param name="brandIdRetail">The brand ID to use for loans with a channel of <see cref="E_BranchChannelT.Retail"/>.</param>
        /// <param name="brandIdWholesale">The brand ID to use for loans with a channel of <see cref="E_BranchChannelT.Wholesale"/>.</param>
        /// <param name="brandIdCorrespondent">The brand ID to use for loans with a channel of <see cref="E_BranchChannelT.Correspondent"/>.</param>
        /// <param name="brandIdBroker">The brand ID to use for loans with a channel of <see cref="E_BranchChannelT.Broker"/>.</param>
        /// <param name="brandNameUnspecified">The name of the brand identified by <paramref name="brandIdUnspecified"/>.</param>
        /// <param name="brandNameRetail">The name of the brand identified by <paramref name="brandIdRetail"/>.</param>
        /// <param name="brandNameWholesale">The name of the brand identified by <paramref name="brandIdWholesale"/>.</param>
        /// <param name="brandNameCorrespondent">The name of the brand identified by <paramref name="brandIdCorrespondent"/>.</param>
        /// <param name="brandNameBroker">The name of the brand identified by <paramref name="brandIdBroker"/>.</param>
        private void SaveDocuSignOptions(
            AbstractUserPrincipal user,
            bool? enabled,
            DocuSignEnvironment? environment,
            string enabledMfaJsonString,
            MfaOptions? mfaDefault,
            string idcheckConfigurationName,
            Guid? brandIdUnspecified,
            Guid? brandIdRetail,
            Guid? brandIdWholesale,
            Guid? brandIdCorrespondent,
            Guid? brandIdBroker,
            string brandNameUnspecified,
            string brandNameRetail,
            string brandNameWholesale,
            string brandNameCorrespondent,
            string brandNameBroker)
        {
            var existingSettings = LenderDocuSignSettings.Retrieve(user.BrokerId);

            var enabledMfaOptions = SerializationHelper.JsonNetDeserialize<Dictionary<string, object>>(enabledMfaJsonString);

            if (!(enabled ?? false) && !existingSettings.DocuSignEnabled)
            {
                this.SetResult("Status", "Success");
                this.SetResult("Message", "Nothing to save.");
                return;
            }
            else
            {
                existingSettings.DocuSignEnabled = enabled ?? false;
                if (environment != null)
                {
                    existingSettings.TargetEnvironment = environment.Value;
                }

                // Note that this assumes all the enabledMfaOptions are mutually exclusive.
                if (enabledMfaOptions != null && enabledMfaOptions.Any(kvp => kvp.Value != null))
                {
                    existingSettings.EnabledMfaOptions = MfaOptions.NoSelection;
                    foreach (var mfaOption in enabledMfaOptions)
                    {
                        if ((bool)mfaOption.Value)
                        {
                            MfaOptions newMfaOption = mfaOption.Key.ToNullableEnum<MfaOptions>(ignoreCase: true) ?? MfaOptions.NoSelection;
                            existingSettings.EnabledMfaOptions |= newMfaOption;
                        }
                    }
                }

                if (mfaDefault != null && existingSettings.EnabledMfaOptions.HasFlag(mfaDefault.Value))
                {
                    existingSettings.DefaultMfaOption = mfaDefault.Value;
                }

                if (idcheckConfigurationName != null)
                {
                    existingSettings.IdCheckConfigurationName = idcheckConfigurationName;
                }

                existingSettings.BrandIdForBlank = brandIdUnspecified;
                existingSettings.BrandIdForRetail = brandIdRetail;
                existingSettings.BrandIdForWholesale = brandIdWholesale;
                existingSettings.BrandIdForCorrespondent = brandIdCorrespondent;
                existingSettings.BrandIdForBroker = brandIdBroker;
                existingSettings.BrandNameForBlank = brandNameUnspecified;
                existingSettings.BrandNameForRetail = brandNameRetail;
                existingSettings.BrandNameForWholesale = brandNameWholesale;
                existingSettings.BrandNameForCorrespondent = brandNameCorrespondent;
                existingSettings.BrandNameForBroker = brandNameBroker;

                existingSettings.Save();
                this.SetResult("Status", "Success");
                this.SetResult("Message", "Saved settings.");
            }
        }

        /// <summary>
        /// Loads user information from DocuSign and saves it in the lender config.
        /// </summary>
        /// <param name="user">The user making the request.</param>
        /// <param name="code">The auth code from the DocuSign auth grant.</param>
        /// <param name="state">The state hash that DocuSign sent back to us from their redirect on the login page.</param>
        private void RegisterUser(AbstractUserPrincipal user, string code, string state)
        {
            var docuSignSettings = LenderDocuSignSettings.Retrieve(user.BrokerId);
            bool useSandbox = docuSignSettings?.TargetEnvironment != DocuSignEnvironment.Production;
            string integratorKey = docuSignSettings?.IntegratorKey;
            string secretKey = docuSignSettings?.SecretKey;

            // Factory, Locator, Helper later
            DocuSignDriver driver = new DocuSignDriver();
            Result<DocuSignUserInfo> userInfoResult = driver.GetOAuthUserInfoFromCode(
                user,
                code,
                stateParameter: state,
                useSandbox: useSandbox,
                integratorKey: integratorKey,
                secretKey: secretKey);

            if (!userInfoResult.HasError)
            {
                var userInfo = userInfoResult.Value;
                if (userInfo != null)
                {
                    docuSignSettings.DocuSignUserId = userInfo.UserId;
                    docuSignSettings.DocuSignUserName = userInfo.Name;
                    docuSignSettings.DocuSignEmail = userInfo.Email;
                    docuSignSettings.Save();
                }
            }
            else
            {
                Tools.LogError("ConfigureDocuSignService.aspx::RegisterUser - DocuSign communication had an error.", userInfoResult.Error);
                string message;
                if (userInfoResult.Error is CBaseException)
                {
                    message = ((CBaseException)userInfoResult.Error).UserMessage;
                }
                else
                {
                    message = ErrorMessages.Generic;
                }

                this.SetResult("Error", message);
            }
        }

        /// <summary>
        /// Get the OAuth Consent URL to send the user for logging in.
        /// </summary>
        /// <param name="user">The user making the request.</param>
        /// <param name="useSandbox">Whether or not to use DocuSign's demo/sandbox environment.</param>
        private void GetOAuthConsentUrl(AbstractUserPrincipal user, bool useSandbox)
        {
            string integratorKey = LenderDocuSignSettings.Retrieve(user.BrokerId).IntegratorKey;
            SetResult("oAuthConsentUrl", new DocuSignDriver().GetOAuthConsentUrl(user, useSandbox, integratorKey, this.docuSignAuthCallbackUri.Value));
        }
    }
}