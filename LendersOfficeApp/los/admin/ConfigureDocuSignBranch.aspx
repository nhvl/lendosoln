﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfigureDocuSignBranch.aspx.cs" Inherits="LendersOfficeApp.Los.Admin.ConfigureDocuSignBranch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DocuSign Branch Configuration</title>
    <style>
        body {
            background-color: gainsboro;
        }
        .Indented {
            margin-left: 2em;
        }
        .BottomButtons {
            width: 100%;
            position: fixed;
            bottom: 0;
            left: 0;
            text-align: center;
            padding-bottom: 2px;
        }
        .ContentBody {
            padding: 4px;
            line-height: 20px;
            height: 94vh;
            overflow-y: auto;
        }
        .Branding input.BrandId {
            font-family: Consolas, monospace;
            width:235px;
        }
        .InvalidInput {
            border: 1px solid red;
        }
        .ErrorMessage
        {
            color:red;
        }
    </style>
    <script>
        $(function () {
            var channels = {
                0: { description: 'Unspecified' },
                1: { description: 'Retail' },
                2: { description: 'Wholesale' },
                3: { description: 'Correspondent' },
                4: { description: 'Broker' }
            };
            function isGuid(value) {
                return /^[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12}$/i.test(value);
            }

            var brandIdInputs;
            function validatePage() {
                brandIdInputs = brandIdInputs || $('input.BrandId');
                brandIdInputs.each(function (i, el) {
                    $(el).toggleClass('InvalidInput', el.value !== '' && !isGuid(el.value));
                });
                var invalidBrandId = brandIdInputs.filter('.InvalidInput').length !== 0;
                $('.Branding .ErrorMessage').toggle(invalidBrandId);

                $('#SaveButton').prop('disabled', invalidBrandId);
                return !invalidBrandId;
            }

            function savePage() {
                if (!validatePage()) {
                    return;
                }

                var args = {
                    BranchId: ML.BranchId
                };
                $('input.BrandId, input.BrandName').each(function (i, el) {
                    args[el.id] = el.value;
                });

                var handleResult = function(result) {
                    if (result.error) {
                        alert(result.UserMessage);
                    }
                    else {
                        parent.LQBPopup.Hide();
                    }
                };
                gService.save.callAsyncSimple('SaveDocuSignBranchOptions', args, handleResult, handleResult);
            }

            (function init() {
                $('#BrandsByChannel').append(
                    $('<tr>').append($('<th>').append('Channel'), $('<th>').append('Brand ID'), $('<th>').append('Brand Name')),
                    $.map(channels, function (channel) {
                        var idInputId = 'BrandId' + channel.description;
                        var nameInputId = 'BrandName' + channel.description;
                        var channelLabel = $('<label>').prop({ 'class': 'FieldLabel', 'for': idInputId }).text(channel.description);
                        var channelBrandIdInput = $('<input>').prop({ 'class': 'BrandId', type: 'text', name: idInputId, id: idInputId, maxLength: 36, value: ML['docuSign' + idInputId] || '' });
                        var channelBrandNameInput = $('<input>').prop({ 'class': 'BrandName', type: 'text', name: nameInputId, id: nameInputId, maxLength: 100, value: ML['docuSign' + nameInputId] || '' });
                        return $('<tr>').append($('<td>').append(channelLabel), $('<td>').append(channelBrandIdInput), $('<td>').append(channelBrandNameInput));
                    }));

                $('#SaveButton').on('click', savePage);
                $('input[type=text]').on('input propertychange', validatePage);
                validatePage();
            })();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="MainRightHeader">
            DocuSign Branch Configuration
        </div>
        <div class="ContentBody">
            <div class="Branding">
                <h5>Branding</h5>
                <div class="ErrorMessage">Brand ID must be a valid UUID</div>
                <div class="Indented">
                    <table id="BrandsByChannel"></table>
                </div>
            </div>
        </div>
        <div class="BottomButtons">
            <input type="button" value="Save" id="SaveButton"/>
            <input type="button" value="Cancel" id="CancelButton" onclick="parent.LQBPopup.Return();" />
        </div>
    </form>
</body>
</html>
