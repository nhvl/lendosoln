<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="ConfirmRelationshipSetting.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.ConfirmRelationshipSetting" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Confirm Relationship Setting</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
		<script language="javascript">
<!--
function _init() {
  resize(550, 280);
}
function f_choice(choice) {
  var args = window.dialogArguments || {};
  args.choice = choice;
  onClosePopup(args);
}
//-->
		</script>
		<form id="ConfirmRelationshipSetting" method="post" runat="server">
			<table width="100%">
				<tr>
					<td style="FONT-WEIGHT:bold;COLOR:black">
						<P>One or more of this user's Relationships will not be applied because the user 
							does not
							<BR>
							have permission to assign loans to other branches.&nbsp; What would you like to 
							do?</P>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="FONT-WEIGHT:bold;COLOR:black">
						<UL>
							<li>
								Enable the "Allow assigning loans to agents of any branch" permission for this user
								<BR>
							- or -
							<li>
								Clear Relationship settings that do not apply</li></UL>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><input type="button" value="Enable Permission" class="buttonstyle" onclick="f_choice(0);" style="WIDTH: 200px">
						&nbsp;<input type="button" value="Clear Relationship Settings" class="buttonstyle" onclick="f_choice(1);" style="WIDTH: 200px">
					</td>
				</tr>
			</table>
		</form>
		<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
	</body>
</HTML>
