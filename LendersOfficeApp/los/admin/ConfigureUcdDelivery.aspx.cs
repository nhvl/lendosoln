﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.UcdDelivery;
    using LendersOffice.Security;

    /// <summary>
    /// A page for configuring UCD delivery options for a lender.
    /// </summary>
    public partial class ConfigureUcdDelivery : LendersOffice.Common.BaseServicePage
    {
        /// <summary>
        /// Gets the required permissions for loading the page.
        /// </summary>
        /// <value>The required permissions for loading the page.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Page load event.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            AbstractUserPrincipal user = PrincipalFactory.CurrentPrincipal;
            bool brokerHasDocMagicVendor = DocumentVendorFactory.EnumerateVendorsFor(user.BrokerId).Any(vendor => vendor.Config.PlatformType == DataAccess.E_DocumentVendor.DocMagic);
            this.RegisterJsGlobalVariables("HasDocMagicVendor", brokerHasDocMagicVendor);
            this.RegisterService("save", "/los/admin/ConfigureUcdDeliveryService.aspx");
            var lenderService = new UcdLenderService(user.BrokerDB);

            this.AllowLqbDeliveryToFannieMae.Checked = lenderService.AllowLqbDeliveryToFannieMae;
            this.LqbToFannieMaeTesting.Checked = lenderService.IsFannieMaeTesting;
            this.AllowLqbDeliveryToFreddieMac.Checked = lenderService.AllowLqbDeliveryToFreddieMac;
            this.LqbToFreddieMacTesting.Checked = lenderService.IsFreddieMacTesting;
            this.AllowDocMagicDeliveryToFannieMae.Checked = lenderService.AllowDocMagicDeliveryToFannieMae;
            this.AllowDocMagicDeliveryToFreddieMac.Checked = lenderService.AllowDocMagicDeliveryToFreddieMac;
            this.RelationshipWithFreddieMac_Seller.Checked = lenderService.RelationshipWithFreddieMac == LendersOffice.Admin.LenderRelationshipWithFreddieMac.Seller;
            this.RelationshipWithFreddieMac_Correspondent.Checked = lenderService.RelationshipWithFreddieMac == LendersOffice.Admin.LenderRelationshipWithFreddieMac.Correspondent;
            this.LqbFreddieMacContainer.Visible = LendersOffice.Constants.ConstStage.EnableLoanClosingAdvisor;
        }
    }
}