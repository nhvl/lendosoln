<%@ Control Language="c#" AutoEventWireup="false" Codebehind="BrokerServicingDisclosureUserControl.ascx.cs" Inherits="LendersOfficeApp.los.admin.BrokerServicingDisclosureUserControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>



<TABLE id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0 class=FormTable>
  <TR>
    <TD>1. Estimate of mortgage loan</TD></TR>
  <TR>
    <TD>A. <asp:CheckBox id=CheckBox1 runat="server" Text="May assign, sell or transfer the servicing while loan is outstanding"></asp:CheckBox></TD></TR>
  <TR>
    <TD>&nbsp;&nbsp; We are able, and we <asp:DropDownList id=DropDownList1 runat="server">
<asp:ListItem Value="&lt;!-- Leave Blank --&gt;">&lt;!-- Leave Blank --&gt;</asp:ListItem>
<asp:ListItem Value="will">will</asp:ListItem>
<asp:ListItem Value="will not">will not</asp:ListItem>
<asp:ListItem Value="haven't decided to">haven't decided to</asp:ListItem>
</asp:DropDownList>&nbsp;service your loan</TD></TR>
  <TR>
    <TD>B. <asp:CheckBox id=CheckBox2 runat="server" Text="We do not service mortgage loans"></asp:CheckBox>&nbsp;<asp:CheckBox id=CheckBox3 runat="server" Text="and we have not serviced mortgage loans in 3 yrs."></asp:CheckBox></TD></TR>
  <TR>
    <TD>2. Estimate of percentage</TD></TR>
  <TR>
    <TD><asp:RadioButtonList id=RadioButtonList1 runat="server" RepeatDirection="Horizontal">
<asp:ListItem Value="0 to 25%">0 to 25%</asp:ListItem>
<asp:ListItem Value="26 to 50%">26 to 50%</asp:ListItem>
<asp:ListItem Value="51 to 75%">51 to 75%</asp:ListItem>
<asp:ListItem Value="76 to 100%">76 to 100%</asp:ListItem>
</asp:RadioButtonList></TD></TR>
  <TR>
    <TD>This estimate <asp:DropDownList id=DropDownList2 runat="server">
<asp:ListItem Value="&lt;!-- Leave Blank --&gt;">&lt;!-- Leave Blank --&gt;</asp:ListItem>
<asp:ListItem Value="does">does</asp:ListItem>
<asp:ListItem Value="does not">does not</asp:ListItem>
</asp:DropDownList>&nbsp;include assignments, sales or transfers</TD></TR>
  <TR>
    <TD>3. Record of transferring the servicing</TD></TR>
  <TR>
    <TD>A. <asp:CheckBox id=CheckBox4 runat="server" Text="We have previously assigned, sold, or transferred the servicing"></asp:CheckBox></TD></TR>
  <TR>
    <TD>B. <asp:CheckBox id=CheckBox5 runat="server" Text="This is our record of transferring the servicing of the first lien mortgage loans"></asp:CheckBox></TD></TR>
  <TR>
    <TD align=middle>
      <TABLE id=Table2 cellSpacing=0 cellPadding=0 border=0>
        <TR>
          <TD noWrap>Year</TD>
          <TD noWrap>Percentage of Transferred Loans</TD></TR>
        <TR>
          <TD noWrap><asp:TextBox id=TextBox1 runat="server" Width="61px" MaxLength="4"></asp:TextBox></TD>
          <TD noWrap align=middle><asp:DropDownList id=DropDownList4 runat="server">
<asp:ListItem Value="&lt;!-- Leave Blank --&gt;">&lt;!-- Leave Blank --&gt;</asp:ListItem>
<asp:ListItem Value="0 %">0 %</asp:ListItem>
<asp:ListItem Value="Nominal">Nominal</asp:ListItem>
<asp:ListItem Value="25%">25%</asp:ListItem>
<asp:ListItem Value="50%">50%</asp:ListItem>
<asp:ListItem Value="75%">75%</asp:ListItem>
<asp:ListItem Value="100%">100%</asp:ListItem>
</asp:DropDownList></TD></TR>
        <TR>
          <TD noWrap><asp:TextBox id=TextBox2 runat="server" Width="61px" MaxLength="4"></asp:TextBox></TD>
          <TD noWrap align=middle><asp:DropDownList id=DropDownList5 runat="server">
<asp:ListItem Value="&lt;!-- Leave Blank --&gt;">&lt;!-- Leave Blank --&gt;</asp:ListItem>
<asp:ListItem Value="0 %">0 %</asp:ListItem>
<asp:ListItem Value="Nominal">Nominal</asp:ListItem>
<asp:ListItem Value="25%">25%</asp:ListItem>
<asp:ListItem Value="50%">50%</asp:ListItem>
<asp:ListItem Value="75%">75%</asp:ListItem>
<asp:ListItem Value="100%">100%</asp:ListItem>
</asp:DropDownList></TD></TR>
        <TR>
          <TD noWrap><asp:TextBox id=TextBox3 runat="server" Width="61px" MaxLength="4"></asp:TextBox></TD>
          <TD noWrap align=middle><asp:DropDownList id=DropDownList6 runat="server">
<asp:ListItem Value="&lt;!-- Leave Blank --&gt;">&lt;!-- Leave Blank --&gt;</asp:ListItem>
<asp:ListItem Value="0 %">0 %</asp:ListItem>
<asp:ListItem Value="Nominal">Nominal</asp:ListItem>
<asp:ListItem Value="25%">25%</asp:ListItem>
<asp:ListItem Value="50%">50%</asp:ListItem>
<asp:ListItem Value="75%">75%</asp:ListItem>
<asp:ListItem Value="100%">100%</asp:ListItem>
</asp:DropDownList></TD></TR></TABLE></TD></TR>
  <TR>
    <TD>This information <asp:DropDownList id=DropDownList3 runat="server">
<asp:ListItem Value="&lt;!-- Leave Blank --&gt;">&lt;!-- Leave Blank --&gt;</asp:ListItem>
<asp:ListItem Value="does">does</asp:ListItem>
<asp:ListItem Value="does not">does not</asp:ListItem>
</asp:DropDownList>&nbsp;include assignments, sales or transfers</TD></TR>
  <TR>
    <TD></TD></TR></TABLE>
