using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
using System.Linq;
using System.Collections.Generic;
using ConfigSystem;
using LendersOffice.ConfigSystem;
using ConfigSystem.DataAccess;

namespace LendersOfficeApp.los.admin
{
	public partial class LeadSources : LendersOffice.Common.BasePage
	{
        private static readonly LeadSource systemDefaultBlankLeadSource = new LeadSource()
        {
            Name = ConstApp.SystemDefualtBlankLeadSourceName,
            Id = ConstApp.SystemDefaultBlankLeadSourceId
        };
        private bool duplicateLeadSourcesRemoved = false;
        private HashSet<string> restoredLeadSourceNames = new HashSet<string>();

		private LeadSourceList m_List = new LeadSourceList();

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingLeadSources
                };
            }
        }

		private String ErrorMessage
		{
			// Write out error message.

			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		private String CommandToDo
		{
			// Write out error message.

			set
			{
				ClientScript.RegisterHiddenField( "m_commandToDo" , value );
			}
		}

        private SystemConfig m_Config;
        private SystemConfig Config
        {
            get
            {
                if (this.m_Config == null)
                {
                    IConfigRepository repository = ConfigHandler.GetRepository(CurrentUser.BrokerId);
                    var activeRelease = repository.LoadActiveRelease(CurrentUser.BrokerId);
                    if (activeRelease == null || activeRelease.Configuration == null)
                    {
                        repository = ConfigHandler.GetRepository(ConstAppDavid.SystemBrokerGuid);
                        this.m_Config = repository.LoadActiveRelease(ConstAppDavid.SystemBrokerGuid).Configuration;
                    }
                    else
                    {
                        this.m_Config = activeRelease.Configuration;
                    }
                }

                return this.m_Config;
            }
        }

        private HashSet<string> m_leadSourceValuesInWorkflowUse = null;

        private IEnumerable<string> LeadSourceValuesInWorkflowUse
        {
            get
            {
                if (this.m_leadSourceValuesInWorkflowUse == null)
                {
                    this.m_leadSourceValuesInWorkflowUse = new HashSet<string>(this.Config.FindValuesInUse(E_TypeToRename.Value, "sLeadSrcIdEnum"));
                }

                return this.m_leadSourceValuesInWorkflowUse;
            }
        }

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Initialize this view.

            try
            {
                // Load the broker to get the choices.
            
                BrokerDB myBroker = BrokerDB.RetrieveById(CurrentUser.BrokerId);

                if (IsPostBack == false)
                {
                    // 12/17/2004 kb - Get the starting list from the
                    // broker's set.
                    myBroker.LeadSources.List.Sort();
                    m_List = myBroker.LeadSources;
                }
                else
                {
                    // 7/15/2004 kb - Pull the entries from the grid,
                    // which was seeded with the last posted set and
                    // has any edits overlaid on top.

                    m_List.Clear();

                    foreach (DataGridItem dgItem in m_Grid.Items)
                    {
                        TextBox itemBox = dgItem.FindControl("Item") as TextBox;
                        TextBox idBox = dgItem.FindControl("Id") as TextBox;

                        var idStr = idBox == null ? "0" : idBox.Text;
                        int id = 0;
                        int.TryParse(idStr, out id);
                        bool isSystemDefaultLeadSource = itemBox.Text == ConstApp.SystemDefualtBlankLeadSourceName 
                            && id == ConstApp.SystemDefaultBlankLeadSourceId;

                        // 5/7/2014 gf - Filter out the system default lead 
                        // source. We do not want it to be available for saving.
                        // It is added back in PagePreRender.
                        if (itemBox != null && idBox != null && !isSystemDefaultLeadSource)
                        {
                            m_List.Add(new LeadSource() { Name = itemBox.Text, Id = id, Deleted = !dgItem.Visible });
                        }
                    }

                    //If they're adding a source that we already have but is marked as deleted,
                    //don't make a new source - just undelete the old one.
                    var reAdded = m_List.List.GroupBy(l => l.Name.ToUpper()).Where(g => g.Count() > 1);
                    foreach (var g in reAdded)
                    {
                        duplicateLeadSourcesRemoved = true;
                        var byId = g.OrderBy(o => o.Id);
                        var oldest = g.First();
                        var others = g.Skip(1);
                        if (oldest.Deleted)
                        {
                            restoredLeadSourceNames.Add(oldest.Name);
                        }
                        oldest.Deleted = false;

                        m_List.List.RemoveAll(r => others.Any(m => m.Id == r.Id));
                    }
                }
            }
            catch (CBaseException e)
            {
                // Oops!

                Tools.LogError("Failed to load page.", e);

                ErrorMessage = e.UserMessage;

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
                m_Add.Enabled = false;
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError("Failed to load page.", e);

                ErrorMessage = "Failed to load page.";

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
                m_Add.Enabled = false;
            }
		}

		/// <summary>
		/// Overload framework event for loading.
		/// </summary>

		protected override void LoadViewState( object oState )
		{
			// Get the view state of a previous postback and restore
			// the data grid's state prior to loading form variables.

			try
			{
				base.LoadViewState( oState );

				if( ViewState[ "Cache" ] != null )
				{
					m_List = LeadSourceList.ToObject( ViewState[ "Cache" ].ToString() );
				}
				
				m_Grid.DataSource = m_List.List;
				m_Grid.DataBind();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to load viewstate." , e );

				ErrorMessage = "Failed to load page.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Overload framework event for saving.
		/// </summary>

		protected override object SaveViewState()
		{
			// Store what we have as latest in the viewstate so
			// we can stay current between postbacks without
			// hitting the database.

			ViewState.Add( "Cache" , m_List.ToString() );

			return base.SaveViewState();
		}

		/// <summary>
		/// Bind this page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Bind this view to our data.

			try
			{
				// Bind and let grid pull current choices.  We
				// apply the current sorting settings to the
				// list prior to binding.  On save, this is
				// the order we persist.

				if( m_List.List.Count == 0 )
				{
					m_EmptyMessage.Visible = true;
				}
				m_List.List.Sort();
                // 5/7/2014 gf - insert the default lead source here because
                // this will prevent it from being available when events are
                // handled.
                m_List.List.Insert(0, systemDefaultBlankLeadSource);
				m_Grid.DataSource = m_List.List;
				m_Grid.DataBind();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to prerender page." , e );

				ErrorMessage = "Failed to render page.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.PreRender += new System.EventHandler(this.PagePreRender);
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void GridItemBound( object sender , System.Web.UI.WebControls.DataGridItemEventArgs a )
		{
			// 12/17/2004 kb - Bind the current choice item to
			// the grid.  We may get to the point where we keep
			// all the descriptions of the same category in
			// one binding object.  At that point, we'll bind
			// to a nested list here.

            LeadSource sItem = a.Item.DataItem as LeadSource;

            if (sItem != null)
			{
                a.Item.Visible = !sItem.Deleted;
                TextBox itemBox = a.Item.FindControl("Item") as TextBox;
                TextBox idBox = a.Item.FindControl("Id") as TextBox;
                LinkButton removeLink = a.Item.FindControl("Remove") as LinkButton;

                bool isSystemDefaultLeadSource = (sItem.Id == ConstApp.SystemDefaultBlankLeadSourceId
                    && sItem.Name == ConstApp.SystemDefualtBlankLeadSourceName);

                bool isInUseInWorkflow = this.LeadSourceValuesInWorkflowUse.Contains(sItem.Id.ToString());

				if (itemBox != null && idBox != null)
				{
					itemBox.Text = sItem.Name;
                    idBox.Text = sItem.Id.ToString();
                    if (isSystemDefaultLeadSource)
                    {
                        itemBox.ReadOnly = true;
                        removeLink.Visible = false;
                    }
                    if (isInUseInWorkflow)
                    {
                        removeLink.Enabled = false;
                        removeLink.ToolTip = "This entry is in use in workflow and cannot be removed.";
                    }
				}
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void GridItemClick( object sender , System.Web.UI.WebControls.DataGridCommandEventArgs a )
		{
			// Handle delete command.  We use the position of the
			// row as our key.

			if( a.CommandName.ToLower() == "remove" )
			{
				try
				{
                    //m_List.List[a.Item.ItemIndex].Deleted = true;

                    // 5/7/2014 gf - Do not remove by index. The PageLoad handler
                    // removes duplicate items from the list, which can make the
                    // index invalid.
                    // If the user removed an item which caused a previously deleted item
                    // to be restored, we need to re-delete the restored item.


                    TextBox idBox = a.Item.FindControl("Id") as TextBox;

                    if (this.LeadSourceValuesInWorkflowUse.Contains(idBox.Text))
                    {
                        ErrorMessage = "This lead source is in use in workflow and cannot be removed.";
                        return;
                    }

                    var itemBox = a.Item.FindControl("Item") as TextBox;
                    var nameToRemove = itemBox.Text;
                    if (!duplicateLeadSourcesRemoved || restoredLeadSourceNames.Contains(nameToRemove))
                    {
                        m_List.GetByName(nameToRemove).Deleted = true;
                    }

					m_IsDirty.Value = "Dirty";
				}
				catch( Exception e )
				{
					// Oops!

					Tools.LogError( "Failed to delete lead source." , e );

					ErrorMessage = "Failed to remove item.";

					m_Ok.Enabled    = false;
					m_Apply.Enabled = false;
					m_Add.Enabled   = false;
				}
			}
		}

		protected void ApplyClick( object sender , System.EventArgs a )
		{
			// Create new lead list and save it to the broker.

			try
			{
				// Store the cached list directly in the broker
				// and save.

				BrokerDB brokerDb = BrokerDB.RetrieveById( CurrentUser.BrokerId );

                brokerDb.LeadSources = m_List;
                if (brokerDb.LeadSources.List.Any(leadSource => leadSource.Name == ConstApp.SystemDefualtBlankLeadSourceName
                    && leadSource.Id == ConstApp.SystemDefaultBlankLeadSourceId))
                {
                    throw new CBaseException(ErrorMessages.Generic, "Programming error. Should not try to save the default lead source to the broker.");
                }

                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", CurrentUser.BrokerId),
                                                new SqlParameter("@LeadSourcesXmlContent", brokerDb.LeadSources.ToString())
                                            };

                StoredProcedureHelper.ExecuteNonQuery(CurrentUser.BrokerId, "UpdateBrokerLeadSources", 3, parameters);

				m_IsDirty.Value = "";
			}
			catch( CBaseException e )
			{
				// Oops!

				Tools.LogError( "Failed to save lead sources." , e );

				ErrorMessage = e.UserMessage;

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to save lead sources." , e );

				ErrorMessage = "Failed to save.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void OkClick( object sender , System.EventArgs a )
		{
			// Create new lead list and save it to the broker.

			try
			{
				// Store the cached list directly in the broker
				// and save.

				ApplyClick( sender , a );

				// We close on successful saving.

				CommandToDo = "Close";
			}
			catch( CBaseException e )
			{
				// Oops!

				Tools.LogError( "Failed to save lead sources." , e );

				ErrorMessage = e.UserMessage;

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to save lead sources." , e );

				ErrorMessage = "Failed to save.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void AddClick( object sender , System.EventArgs a )
		{
			// Add a new entry to our cached instance.

			try
			{
				// Create new entry only if the strings are valid.

				m_IsDirty.Value = "Dirty";

				m_List.Add( new LeadSource() );
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to add new item." , e );

				ErrorMessage = "Failed to add.";

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

	}

}
