<%@ Page language="c#" Codebehind="HolidayLockDeskClosuresPopup.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.HolidayLockDeskClosuresPopup" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML>
<html>
<head>
	<title>Holiday Closures</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet" />
</head>
<body MS_POSITIONING="FlowLayout">
	<form id="HolidayLockDeskClosuresPopup" method="post" runat="server">
		<div style="padding: 8px;">
			<ML:EncodedLabel class="FieldLabel" runat="server" ID="m_title"/>
		</div>
		<div style="overflow-y:auto; max-height:200px; margin-left:8px; margin-right:8px;">
			<ml:CommonDataGrid id="m_Grid" runat="server" width="100%" cellpadding="2" OnItemDataBound="m_Grid_ItemDataBound">
				<Columns>
					<asp:BoundColumn DataField="ClosureDate" HeaderText=" Closure Date"/>
					<asp:BoundColumn DataField="IsClosedAllDay" HeaderText=" Closed All Day?"/>
					<asp:BoundColumn DataField="LockDeskOpenTime" HeaderText=" Lock Desk Open Time"/>
					<asp:BoundColumn DataField="LockDeskCloseTime" HeaderText=" Lock Desk Close Time"/>
				</Columns>
			</ml:CommonDataGrid>
			<asp:Panel id="m_EmptyMessage" runat="server" Visible="False" EnableViewState="False">
				<DIV style="PADDING-RIGHT: 40px; PADDING-LEFT: 40px; FONT: 11px arial; COLOR: dimgray; PADDING-TOP: 40px; TEXT-ALIGN: center">
					No holiday closures to display.
				</DIV>
			</asp:Panel>
		</div>
		<uc1:cModalDlg id="CModalDlg1" runat="server" />
	</form>
</body>
</html>
