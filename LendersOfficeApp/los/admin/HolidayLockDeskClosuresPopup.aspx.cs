using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using System.Collections;
using LendersOffice.Common;
using System.Data.SqlClient;
using LendersOffice.Security;
using LendersOffice.ObjLib.LockPolicies;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{
	public partial class HolidayLockDeskClosuresPopup : BasePage
	{
		protected Guid PolicyId
		{
            get { return RequestHelper.GetGuid("policyid", Guid.Empty); }
		}
        private Guid BrokerId
        {
            get { return PrincipalFactory.CurrentPrincipal.BrokerId; }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            LockPolicy p = LockPolicy.Retrieve(BrokerId, PolicyId);
            string policyName = p.PolicyNm;
            string timezone;
            switch(p.TimezoneForRsExpiration)
            {
                case "MST":
                    timezone = "Mountain Time";
                    break;
                case "CST":
                    timezone = "Central Time";
                    break;
                case "EST":
                    timezone = "Eastern Time";
                    break;
                case "PST":
                default:
                    timezone = "Pacific Time";
                    break;
            }

            m_title.Text = string.Format("{0} - Holiday Closures ({1}):", policyName, timezone);

			BindDataGrid();
		}

		protected void m_Grid_ItemDataBound(object sender, DataGridItemEventArgs e) 
		{
			if ( e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer ) 
			{
				TableCell allDayCell = e.Item.Cells[1] as TableCell;
				bool allDayVal = bool.Parse(allDayCell.Text);
				allDayCell.Text = (allDayVal)?"Yes":"No";

				TableCell dateCell = e.Item.Cells[0] as TableCell;
				dateCell.Text = DateTime.Parse(dateCell.Text).ToShortDateString();

				TableCell openTimeCell = e.Item.Cells[2] as TableCell;
				openTimeCell.Text = (allDayVal)?"n/a":DateTime.Parse(openTimeCell.Text).ToShortTimeString();

				TableCell closeTimeCell = e.Item.Cells[3] as TableCell;
				closeTimeCell.Text = (allDayVal)?"n/a":DateTime.Parse(closeTimeCell.Text).ToShortTimeString();
			}
		}

        private void BindDataGrid()
        {
            LockDeskClosures closures = new LockDeskClosures(this.BrokerId, this.PolicyId);
            m_Grid.DataSource = LockDeskClosures.GetDataTableOfClosures(closures.GetUpcoming()).DefaultView;
            m_Grid.DataBind();
            m_EmptyMessage.Visible = m_Grid.Items.Count == 0;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion
	}
}
