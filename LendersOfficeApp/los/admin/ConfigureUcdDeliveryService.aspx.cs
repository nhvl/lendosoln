﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.UcdDelivery;
    using LendersOffice.Security;

    /// <summary>
    /// A service page for the <see cref="ConfigureUcdDelivery"/> page.
    /// </summary>
    public partial class ConfigureUcdDeliveryService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets the permission required to load this page.
        /// </summary>
        /// <value>The permission required for page load.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Process the service request.
        /// </summary>
        /// <param name="methodName">The method name to invoke server-side.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(SaveUcdDeliveryOptions):
                    SaveUcdDeliveryOptions(
                        PrincipalFactory.CurrentPrincipal,
                        this.GetNullableBool("AllowLqbDeliveryToFannieMae"),
                        this.GetNullableBool("IsFannieMaeTesting"),
                        this.GetNullableBool("AllowLqbDeliveryToFreddieMac"),
                        this.GetNullableBool("IsFreddieMacTesting"),
                        this.GetNullableBool("AllowDocMagicDeliveryToFannieMae"),
                        this.GetNullableBool("AllowDocMagicDeliveryToFreddieMac"),
                        this.GetString("RelationshipWithFreddieMac", null));
                    return;
                default:
                    throw new DataAccess.CBaseException(ErrorMessages.Generic, $"ConfigureUcdDeliveryService.aspx: Unknown service method \"{methodName}\"");
            }
        }

        /// <summary>
        /// Save the UCD delivery options from the UI to the database.
        /// </summary>
        /// <param name="user">The user making the request.</param>
        /// <param name="allowLqbDeliveryToFannieMae">Whether to allow LQB to Fannie Mae delivery.</param>
        /// <param name="isFannieMaeTesting">Whether to use Fannie Mae's test environment.</param>
        /// <param name="allowLqbDeliveryToFreddieMac">Whether to allow LQB to Freddie Mac delivery.</param>
        /// <param name="isFreddieMacTesting">Whether to use Freddie Mac's test environment.</param>
        /// <param name="allowDocMagicDeliveryToFannieMae">Whether to allow DocMagic to Fannie Mae delivery.</param>
        /// <param name="allowDocMagicDeliveryToFreddieMac">Whether to allow DocMagic to Freddie Mac delivery.</param>
        /// <param name="relationshipWithFreddieMac">The lender's relationship to Freddie Mac.</param>
        private void SaveUcdDeliveryOptions(
            AbstractUserPrincipal user,
            bool? allowLqbDeliveryToFannieMae,
            bool? isFannieMaeTesting,
            bool? allowLqbDeliveryToFreddieMac,
            bool? isFreddieMacTesting,
            bool? allowDocMagicDeliveryToFannieMae,
            bool? allowDocMagicDeliveryToFreddieMac,
            string relationshipWithFreddieMac)
        {
            UcdLenderService lenderService = new UcdLenderService(user.BrokerDB)
            {
                AllowLqbDeliveryToFannieMae = allowLqbDeliveryToFannieMae ?? false,
                IsFannieMaeTesting = isFannieMaeTesting ?? false,
                AllowLqbDeliveryToFreddieMac = allowLqbDeliveryToFreddieMac ?? false,
                IsFreddieMacTesting = isFreddieMacTesting ?? false,
                AllowDocMagicDeliveryToFannieMae = allowDocMagicDeliveryToFannieMae ?? false,
                AllowDocMagicDeliveryToFreddieMac = allowDocMagicDeliveryToFreddieMac ?? false,
            };

            LenderRelationshipWithFreddieMac relationship;
            if (Enum.TryParse(relationshipWithFreddieMac, out relationship))
            {
                lenderService.RelationshipWithFreddieMac = relationship;
            }
            else
            {
                lenderService.RelationshipWithFreddieMac = LenderRelationshipWithFreddieMac.Undefined;
            }

            lenderService.Save();
            SetResult("Status", "Success");
        }
    }
}