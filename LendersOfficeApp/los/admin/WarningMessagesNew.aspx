<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="WarningMessagesNew.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.WarningMessagesNew" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>WarningMessages</title>
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">

    <script>
        function init()
        {
            resize(800, 500);
        }
        function f_show(showMsg, event)
        {
            var msg = document.getElementById("showDetails");
            var label = document.getElementById("detailLabel");
            if(showMsg == "LockDeskExpiredDetails")
                label.innerText = "This is only for LendingQB users, such as Lock Desks, with \"Rate lock requests for possibly expired rates\" permission set to \"Not allowed after investor cut-off time\" (see permissions tab of Employee editor).  This message will be displayed as a warning but will not prevent a rate lock.";
            else if(showMsg == "LockDeskCutOffDetails")
                label.innerText = "This is only for LendingQB users, such as Lock Desks, with \"Rate lock requests for possibly expired rates\" permission set to \"Not allowed after investor cut-off time\" (see permissions tab of Employee editor).";
            else
                label.innerText = "";

            msg.style.display = "";
                msg.style.top = (event.clientY - 10)+ "px";
                msg.style.left = (event.clientX + 10 ) + "px";

        }
        function f_close(closeMsg)
        {
            var msg = document.getElementById(closeMsg);
            msg.style.display="none";
        }
        function f_showDefault(showMsg, event)
        {
            var label = document.getElementById("defaultLabel");
            var label2 = document.getElementById("defaultLabel1");
            label.innerText="This is the default warning";
            var msg = document.getElementById("defaultView");

            if (showMsg == "NormalUserExpiredDetails") {
                label.innerText = <%= AspxTools.JsString(m_defaultNormalUserExpiredMessage) %>;
                document.getElementById("defaultLink").onclick = function() { f_setDefault("NormalUserExpiredDetails"); f_close("defaultView"); };
            }
            else if (showMsg == "LockDeskExpiredDetails") {
                label.innerText = <%= AspxTools.JsString(m_defaultLockDeskExpiredMessage) %>;
                document.getElementById("defaultLink").onclick = function() { f_setDefault("LockDeskExpiredDetails"); f_close("defaultView"); };
            }
            else if (showMsg == "NormalUserCutOffDetails") {
                label.innerText = <%= AspxTools.JsString(m_defaultNormalUserCutOffMessage) %> + "\nNote: {0} is used to include investor cut-off time";
                document.getElementById("defaultLink").onclick = function() { f_setDefault("NormalUserCutOffDetails"); f_close("defaultView"); };
            }
            else if (showMsg == "LockDeskCutOffDetails") {
                label.innerText = <%= AspxTools.JsString(m_defaultLockDeskCutOffMessage) %> + "\nNote: {0} is used to include investor cut-off time";
                document.getElementById("defaultLink").onclick = function() { f_setDefault("LockDeskCutOffDetails"); f_close("defaultView"); };
            }
            else if (showMsg == "OutsideNormalHoursDetails") {
                label.innerText = <%= AspxTools.JsString(m_defaultOutsideNormalHoursMessage) %> + "\nNote: {0} is used to include the regular lock desk hours";
                document.getElementById("defaultLink").onclick = function() { f_setDefault("OutsideNormalHoursDetails"); f_close("defaultView"); };
            }
            else if (showMsg == "OutsideClosureDayHourDetails") {
                label.innerText = <%= AspxTools.JsString(m_defaultOutsideClosureDayHourMessage) %>;
                document.getElementById("defaultLink").onclick = function() { f_setDefault("OutsideClosureDayHourDetails"); f_close("defaultView"); };
            }
            else if (showMsg == "OutsideNormalHoursAndPassInvestorCutOffDetails") {
                label.innerText = <%= AspxTools.JsString(m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage) %>;
                document.getElementById("defaultLink").onclick = function() { f_setDefault("OutsideNormalHoursAndPassInvestorCutOffDetails"); f_close("defaultView"); };
            }
            else
                label.innerText = "";

            msg.style.display = "";
            msg.style.top = (event.clientY - 10)+ "px";
            msg.style.left = (event.clientX + 10) + "px";
        }
        function f_setDefault(showMsg)
        {

            toDirty();

            if (showMsg == "NormalUserExpiredDetails")
                document.getElementById("m_newNormalUserExpiredMessage").value = <%= AspxTools.JsString(m_defaultNormalUserExpiredMessage) %>;
            else if(showMsg == "LockDeskExpiredDetails")
                document.getElementById("m_newLockDeskExpiredMessage").value = <%= AspxTools.JsString(m_defaultLockDeskExpiredMessage) %>;
            else if(showMsg == "NormalUserCutOffDetails")
                document.getElementById("m_newNormalUserCutOffMessage").value = <%= AspxTools.JsString(m_defaultNormalUserCutOffMessage) %>;
            else if(showMsg == "LockDeskCutOffDetails")
                document.getElementById("m_newLockDeskCutOffMessage").value = <%= AspxTools.JsString(m_defaultLockDeskCutOffMessage) %>;
            else if(showMsg == "OutsideNormalHoursDetails")
                document.getElementById("m_newOutsideNormalHoursMessage").value = <%= AspxTools.JsString(m_defaultOutsideNormalHoursMessage) %>;
            else if(showMsg == "OutsideClosureDayHourDetails")
                document.getElementById("m_newOutsideClosureDayHourMessage").value = <%= AspxTools.JsString(m_defaultOutsideClosureDayHourMessage) %>;
            else if(showMsg == "OutsideNormalHoursAndPassInvestorCutOffDetails")
                document.getElementById("m_newOutsideNormalHoursAndPassInvestorCutOffMessage").value = <%= AspxTools.JsString(m_defaultOutsideNormalHoursAndPassInvestorCutOffMessage) %>;
        }

        function replace() {
            if (document.getElementById("m_newNormalUserExpiredMessage").value.match("<") == "<" || document.getElementById("m_newNormalUserExpiredMessage").value.match(">") == ">" ||
                document.getElementById("m_newLockDeskExpiredMessage").value.match("<") == "<" || document.getElementById("m_newLockDeskExpiredMessage").value.match(">") == ">" ||
                document.getElementById("m_newNormalUserCutOffMessage").value.match("<") == "<" || document.getElementById("m_newNormalUserCutOffMessage").value.match(">") == ">" ||
                document.getElementById("m_newLockDeskCutOffMessage").value.match("<") == "<" || document.getElementById("m_newLockDeskCutOffMessage").value.match(">") == ">" ||
                document.getElementById("m_newOutsideNormalHoursMessage").value.match("<") == "<" || document.getElementById("m_newOutsideNormalHoursMessage").value.match(">") == ">" ||
                document.getElementById("m_newOutsideClosureDayHourMessage").value.match("<") == "<" || document.getElementById("m_newOutsideClosureDayHourMessage").value.match(">") == ">") {

                document.getElementById("m_newNormalUserExpiredMessage").value = document.getElementById("m_newNormalUserExpiredMessage").value.replace(/</g, "").replace(/>/g, "");
                document.getElementById("m_newLockDeskExpiredMessage").value = document.getElementById("m_newLockDeskExpiredMessage").value.replace(/</g, "").replace(/>/g, "");
                document.getElementById("m_newNormalUserCutOffMessage").value = document.getElementById("m_newNormalUserCutOffMessage").value.replace(/</g, "").replace(/>/g, "");
                document.getElementById("m_newLockDeskCutOffMessage").value = document.getElementById("m_newLockDeskCutOffMessage").value.replace(/</g, "").replace(/>/g, "");
                document.getElementById("m_newOutsideNormalHoursMessage").value = document.getElementById("m_newOutsideNormalHoursMessage").value.replace(/</g, "").replace(/>/g, "");
                document.getElementById("m_newOutsideClosureDayHourMessage").value = document.getElementById("m_newOutsideClosureDayHourMessage").value.replace(/</g, "").replace(/>/g, "");


                alert("The characters < and > are not permitted within the warning messages and have been removed. You may want to check your messages again now that the characters have been removed.");
            }
        }

        function toDirty() {
            document.getElementById("dirty").value = 1;
        }

        function close_click() {
            if (document.getElementById("dirty").value == 1) {
                if (confirm("Close without saving?")) {
                    onClosePopup();
                }
            }
            else {
                onClosePopup();
            }
        }

    </script>
</head>
<body MS_POSITIONING="FlowLayout" onload='init()'>
    <h4 class="page-header">Rate Expiration Warning Messages - <%= AspxTools.HtmlString(PolicyName) %></h4>
    <form id="WarningMessagesNew" method="post" runat="server">
        <table width=760 border=0 cellspacing=0>
            <tr class="GridAlternatingItem"><td class="FieldLabel"> Stale Pricing  <br /> <a href='#"' onclick='f_showDefault("NormalUserExpiredDetails", event);'?>View Default</a>  </td><td align=right><asp:TextBox id="m_newNormalUserExpiredMessage" TextMode=MultiLine rows="3" Columns=90 onchange="toDirty()" runat=server/></td></tr>
            <tr class="GridItem"><td class="FieldLabel"> Stale Pricing for Lock Desk Users   <ml:PassthroughLiteral ID="m_lockDeskExpiredDetails" runat=server/><br> <a href='#"' onclick='f_showDefault("LockDeskExpiredDetails", event);'?>View Default</a></td><td align="right"><asp:TextBox id="m_newLockDeskExpiredMessage" TextMode=MultiLine rows="3" Columns=90 onchange="toDirty()" runat=server/></td></tr>
            <tr class="GridAlternatingItem"><td class="FieldLabel"> Passed Investor cut-off  <br /> <a href='#"' onclick='f_showDefault("NormalUserCutOffDetails", event);'?>View Default</a> </td><td align="right"><asp:TextBox id="m_newNormalUserCutOffMessage" TextMode=MultiLine rows="3" Columns=90 onchange="toDirty()" runat=server/></td></tr>
            <tr class="GridItem"><td class="FieldLabel"> Passed Investor cut-off for Lock Desk Users <ml:PassthroughLiteral ID="m_lockDeskCutoffDetails" runat=server/><br> <a href='#"' onclick='f_showDefault("LockDeskCutOffDetails", event);'?>View Default</a> </td><td align="right"><asp:TextBox id="m_newLockDeskCutOffMessage" TextMode=MultiLine rows="3" Columns=90 onchange="toDirty()" runat=server/></td></tr>
            <tr class="GridAlternatingItem"><td class="FieldLabel"> Lock Desk is Closed <br /> <a href='#"' onclick='f_showDefault("OutsideNormalHoursDetails", event);'?>View Default</a> </td><td align="right"><asp:TextBox id="m_newOutsideNormalHoursMessage" TextMode=MultiLine rows="3" Columns=90 onchange="toDirty()" runat=server/></td></tr>
            <tr class="GridItem"><td class="FieldLabel"> Holiday Closure <br /> <a href='#"' onclick='f_showDefault("OutsideClosureDayHourDetails", event);'?>View Default</a> </td><td align="right"><asp:TextBox id="m_newOutsideClosureDayHourMessage" TextMode=MultiLine rows="3" Columns=90 onchange="toDirty()" runat=server/></td></tr>
        </table>
        <br /><br />
        <input type="hidden" value="0" id="dirty" />

        <center>
            <asp:Button ID="m_SaveButton" Runat="server" Text="  Ok  " OnClick="SaveClick"/>&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" onclick="close_click()" value="Cancel" />
        </center>

    </form>

    <div id="showDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:330px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; BACKGROUND-COLOR:whitesmoke">
        <table width="100%">
            <tr><td><label id="detailLabel"></label></td></tr>
            <tr><td align="center">[ <a href="#" onclick="f_close('showDetails');">Close</a> ]</td></tr>
        </table>
    </div>

    <div id="defaultView" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:330px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; BACKGROUND-COLOR:whitesmoke">
        <table width="100%">
            <tr><td><label id="defaultLabel"></label></td></tr>
			<tr><td align="center">[ <a href="#" onclick="f_close('defaultView');">Close</a> ]  [ <a id="defaultLink" href="#">Set to Default</a> ]</td></tr>
        </table>
    </div>
</body>
</html>
