﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Pdf;
using LendersOffice.Security;


namespace LendersOfficeApp.los.admin
{
    public partial class PrintGroupPermAssignment : BaseServicePage
    {
        protected Guid UserId
        {
            get
            {
                return RequestHelper.GetGuid("userid", default(Guid));
            }
        }

        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }

        protected Guid BrokerId
        {
            get
            {
                return RequestHelper.GetGuid("brokerid");
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var brokerGroups = PrintListGroup.ListAllGroups(BrokerId);

            IEnumerable<PrintListGroup> userGroups = new List<PrintListGroup>();
            if (UserId != default(Guid))
            {
                userGroups = EmployeeDB.RetrieveByUserId(BrokerId, UserId).AvailablePrintGroups;
            }

            var brokerGroupsMinusUserGroups = from g in brokerGroups
                                              where !userGroups.Any(a => a.Id == g.Id)
                                              select g;

            Picker.AvailableSrc = brokerGroupsMinusUserGroups.OrderBy(a => a.Name);
            Picker.AssignedSrc = userGroups.OrderBy(a => a.Name);

            Picker.TextField = "Name";
            Picker.ValueField = "Id";

            Picker.DataBind();
        }
    }
}
