using System;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{
	public partial class EmployeeListExport : BasePage
	{
		private BrokerUserPrincipal CurrentUser
		{
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }

		protected void PageLoad( object sender , System.EventArgs a )
		{
            bool includePermissions = false;
            try
            {
                includePermissions = RequestHelper.GetBool("permissions");

                var brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
                var exporter = includePermissions ?
                    new EmployeeExporterWithPermissions(brokerId) :
                    new EmployeeExporter(brokerId);

                Response.Output.Write(exporter.ExportCsv());
            }
            catch (System.Threading.ThreadAbortException e)
            {
                // Pass on the abort exception so export can finish
                // without writing the page.
                throw e;
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to load employee list export.", e);
            }

            string filename = includePermissions ? "EmployeesWithPermissions.csv" : "Employees.csv";
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{filename}\"");
            Response.ContentType = "application/csv";
            Response.Flush();
            Response.End();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}

}
