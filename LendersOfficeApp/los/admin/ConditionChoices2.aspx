﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConditionChoices2.aspx.cs" Inherits="LendersOfficeApp.los.admin.ConditionChoices2" %>
<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Condition Choices</title>
    	<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
    	<style type="text/css" >
 
    	    form {
    	        padding: 5px;
    	    }
    	    h1 { margin: 0; }
    	    .left { float: left; }
    	    .right { float:right; }
            table, .bottomActions  { clear: both; }
            .topActions,.bottomActions { overflow: hidden; }
            .topActions { margin-bottom: 5px; }
            .bottomMargin { margin-bottom: 5px; }
            .bottomActions input { float: right; margin-left: 5px; }
            .clickable { text-decoration: underline; color: Blue; cursor: pointer; }
            td { padding: 0 2px 0 2px;}
          textarea { background-color: #d3d3d3; }
    	</style>
</head>
<body>

    <% if (!showRequiredDocType) { %>
	<style type="text/css" >
	.RequiredDocType {
          display: none;
      }
	</style>
	<% } %>
    <h4 class="page-header">Condition Choices</h4>
    <script id="conditionTmpl" type="text/x-jquery-tmpl">
        <tr class="GridItem">
				<td>
                    <a href="#" class="editChoice" rowid='${Id}' >edit</a>
                </td><td class="CategoryRep">
                    ${Category}
                </td>
                <td class="ConditionTypeRep">${ConditionType}</td>
                <td class="LoanType">${sLT}</td>
                <td><textarea cols="40" readonly="readonly" class="ConditionSubject">${ConditionSubject}</textarea></td>
                <td class="IsHidden">${IsHidden}</td>
                <td class="RequiredDocType">${RequiredDocType}</td>
                <td class="ToBeAssignedRole">${ToBeAssigedRole}</td>
                <td class="ToBeOwnedByRole">${ToBeOwnedByRole}</td>
                <td class="DueDateDesc">${DueDateDescription} </td><td>
                    <a href="#" class="deleteChoice" rowid='${Id}' >delete</a>
                </td>
			</tr>
    </script>
    <form id="form1" runat="server">
    <div>
    <div class="topActions">
        <input type="button" value="Add" class="left" />
        <input type="button" value="Export to CSV" class="right" />
    </div>
    
    <asp:GridView runat="server" CssClass="bottomMargin mainTable" RowStyle-CssClass="GridItem" AlternatingRowStyle-CssClass="GridAlternatingItem" HeaderStyle-CssClass="GridHeader" ID="ConditionChoiceList" AutoGenerateColumns="false" UseAccessibleHeader="true" >
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <a href="#" class="editChoice" rowid='<%#  AspxTools.HtmlString(Eval("ConditionChoiceId").ToString()) %>' >edit</a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="CategoryRep" HeaderStyle-CssClass="clickable" HeaderText="Category">
                <ItemTemplate >
                    <%#  AspxTools.HtmlString(((LendersOffice.ObjLib.Task.ConditionChoice) (Container.DataItem)).Category.Category) %>
                
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderStyle-CssClass="clickable" HeaderText="Condition Type" ItemStyle-CssClass="ConditionTypeRep" DataField="ConditionType_rep"  />
            <asp:BoundField HeaderStyle-CssClass="clickable" ItemStyle-CssClass="LoanType" HeaderText="Loan Type" DataField="sLT_rep" />
            <asp:TemplateField HeaderStyle-CssClass="clickable" HeaderText="Condition Subject">
                <ItemTemplate>
                    <textarea  id="ConditionSubject" class="ConditionSubject" readonly="readonly" cols="40"><%#  AspxTools.HtmlString(((LendersOffice.ObjLib.Task.ConditionChoice) (Container.DataItem)).ConditionSubject) %></textarea>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-CssClass="RequiredDocType" HeaderStyle-CssClass="clickable" HeaderText="Hidden?">
                <ItemTemplate >
                    <%#  AspxTools.HtmlString( ConvertBoolToYes(((LendersOffice.ObjLib.Task.ConditionChoice) (Container.DataItem)).IsHidden)) %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="RequiredDocType_rep" HeaderStyle-CssClass="clickable RequiredDocType"  ItemStyle-CssClass="RequiredDocType" HeaderText="Required Document Type" />
            <asp:BoundField DataField="ToBeAssignedRole" HeaderStyle-CssClass="clickable"  ItemStyle-CssClass="ToBeAssignedRole" HeaderText="To Be Assigned To" />
            <asp:BoundField DataField="ToBeOwnedByRole" HeaderStyle-CssClass="clickable" ItemStyle-CssClass="ToBeOwnedByRole" HeaderText="To Be Owned By" />
            <asp:BoundField DataField="FriendlyDueDateDescription" ItemStyle-CssClass="DueDateDesc" HeaderText="Due Date" />
                        <asp:TemplateField>
                <ItemTemplate>
                    <a href="#" class="deleteChoice" rowid=' <%#  AspxTools.HtmlString(((LendersOffice.ObjLib.Task.ConditionChoice) (Container.DataItem)).ConditionChoiceId.ToString()) %>' >delete</a>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <div class="bottomActions">
        <input type="button" value="Apply" />
        <input type="button" value="Cancel" />
        <input type="button" value="Ok" />
    </div>
    </div>
    <ML:CMODALDLG id="m_ModalDlg" runat="server"></ML:CMODALDLG>
    <script type="text/javascript" src="../../inc/json.js"></script>
    <script type="text/javascript" src="../../inc/jquery.tmpl.js"></script>
    
    <script type="text/javascript">
        var dirty = false;
        
        $(function(){
            var $mainTable = $('.mainTable'), deletedChoices = [];
            
            $('input[value=Cancel]').click(function(){
                if (!dirty || confirm("Do you want to close without saving?")) {
                    dirty = false; // So the window.unload will not bother us
                    onClosePopup();
                }
            });
            
            $('input[value=Add]').click(function(){
                showModal("/los/admin/ConditionChoiceEditor.aspx", null, null, null, function(args){
                    if( args.OK ) {
                        if( $mainTable.length === 0 ) {
                            window.location.reload();
                        }
                        $('#conditionTmpl').tmpl(args.UpdatedChoice).appendTo($mainTable);
                        $mainTable.trigger('update');
                    }
	            },{hideCloseButton:true});
            });
            
            $(document).on('click', 'a.editChoice', function () {
	            showModal("/los/admin/ConditionChoiceEditor.aspx?id="+ $(this).attr('rowid'), null, null, null, function(args){
                    if( args.OK ) {
                        var conditionChoice = args.UpdatedChoice;
                        var row = $(this).parents('tr'); 
                        row.find('.ConditionTypeRep').text(conditionChoice.ConditionType);
                        row.find('.ConditionSubject').val(conditionChoice.ConditionSubject);
                        row.find('.RequiredDocType').text(conditionChoice.RequiredDocType);
                        row.find('.ToBeAssignedRole').text(conditionChoice.ToBeAssigedRole);
                        row.find('.ToBeOwnedByRole').text(conditionChoice.ToBeOwnedByRole);
                        row.find('.CategoryRep').text(conditionChoice.Category);
                        row.find('.DueDateDesc').text(conditionChoice.DueDateDescription);
                        row.find('.LoanType').text(conditionChoice.sLT);
                        $mainTable.trigger('update');
                    }
                },{hideCloseButton:true});
                
                return false;
            });
            
            $(document).on('click', 'a.deleteChoice', function(){
                deletedChoices.push($(this).attr('rowid')); 
                $(this).parents('tr').remove();
                dirty = true;
                $mainTable.trigger('update');
            });
            
            
            $('input[value=Ok],input[value=Apply]').click(function(){
                this.disabled = true;
                var sortOrder = getSortOrder();
                var input = this;
                dirty = false;
                save(input.value === "Ok", sortOrder, this);
            });
            
            $('input[value="Export to CSV"]').click(function(){
                var data = JSON.stringify(getSortOrder());
                $('#SortOrder').val(data);
                $('#GetCsv').submit();
            });
            
            window.onbeforeunload = function () {
                if (dirty) {
                    return UnloadMessage;
                }
            }
            
            function save(closeMe, sortOrder, btn) {
                var data = {
                    'idsToDelete' : deletedChoices,
                    'sortOrder' : sortOrder
                };
                
                data = JSON.stringify(data);
                
                callWebMethodAsync({
                    type: "POST",
                    url: "ConditionChoices2.aspx/Save",
                    data: data,
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    async : false,
                    success: function(msg){
                       if( msg.d === "OK" ){
                            if( closeMe ) {
                                onClosePopup();
                            }
                       }
                       deletedChoices = [];
              
                    },
                    error: function(msg){
                        var d = msg.responseText ;
                        if( d ) {
                            d = JSON.parse(d).Message;
                        }
                        if( console && console.log ){
                            console.log(msg.responseText);
                        }
                        alert('Could not update Error:' + d); 
                
                    }
                });
            }
            
            function getSortOrder() {
                var ids = [];
                $mainTable.children('tbody').children('tr').each(function(i,o){
                    ids.push($('a.editChoice',o).attr('rowid'));
                });            
                return ids;
            }
            
            function confirmExit() {
                if (confirm("Do you want to save the changes?")) {
                    save(true, getSortOrder(), null);
                }
                onClosePopup();
            }
            
            
            $mainTable.tablesorter({
                header : { 
                    0 : { sortable : false },
                    1 : { sortable : true }, //category
                    2 : { sortable : true }, //condition type
                    3 : { sortable : true }, //loan type
                    4 : { sortable : true }, // subject
                    5 : { sortable : true },
                    6 : { sortable : true},
                    7 : { sortable : false }
                }
            });
            
            
            
        });
    </script>
    <script type="text/javascript" src="../../inc/jquery.tablesorter.min.js"></script>
    <uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg> 
    </form>
    <form  id="GetCsv" target="submitFrame" action="ConditionChoices2.aspx" method="post" >
        <input type="hidden" id="SortOrder" name="SortOrder"/>
        <input type="hidden" id="CSV" name="CSV" value="1" />
    </form>
    
	<iframe style="display:none" id="submitFrame" name="submitFrame" src="javascript:false"> </iframe>
    
</body> 
</html>
