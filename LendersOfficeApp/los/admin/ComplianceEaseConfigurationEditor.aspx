﻿<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="ComplianceEaseConfigurationEditor.aspx.cs"
    Inherits="LendersOfficeApp.los.admin.ComplianceEaseConfigurationEditor" %>

<%@ Import Namespace="LendersOffice.Common" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>ComplianceEase Configuration</title>
    <style type="text/css">
        #Main {
            border: solid 1px black;
            padding: 5px;
        }

        select.box {
            width: 400px;
            height: 100px;
        }

        body {
            margin: 0 auto;
            text-align: center;
            line-height: 2em;
            background-color: Gainsboro;
        }
        form{
            padding: 1em;
        }

        p {
            padding: 0;
            margin: 0;
            text-align: left;
            margin-left: 3em;
        }

        .leg {
            padding: 10px;
            text-align: center;
            margin-left: 0;
        }

        .ErrorMessageSection {
            margin-top: 5px;
            font-weight: bold;
            color: red;
        }

        h1 {
            margin: 0;
            padding: 0;
            font-size: 12px;
            text-align: left;
        }

        a {
            display: block;
            position: relative;
            bottom: 5px;
        }

        .Section div {
            text-align: left;
            margin-left: 5%;
        }

        #BeginRunningAuditsAt, #HUDApproval {
            text-align: left;
        }

        #BeginRunningAuditsAt h1, #HUDApproval h1 {
            display: inline;
            padding-right: 10px;
        }

        .hidden {
            display: none;
        }
    </style>

    <script type="text/javascript">
        function _init() {
            validateBeginAuditsStatus();
        }

        function validateBeginAuditsStatus() {
            var beginAuditStatus = $('#BeginAuditStatus option:selected').text();
            var beginAuditStatusIsExcluded = false;

            $('#ExcludedStatuses option').each(function () {
                if ($(this).text() === beginAuditStatus) {
                    beginAuditStatusIsExcluded = true;
                }
            });

            if (beginAuditStatusIsExcluded) {
                $('#ErrorMessage').text("The " + beginAuditStatus + " status cannot be set as the Begin Audit "
                    + "status because it has been marked for exclusion. Please select another Begin Audit status "
                    + "or remove the selected status from the exclusion list.");
                $('#ErrorMessage').show();
                $('#m_saveImp').attr('disabled', true);
            }
            else {
                $('#ErrorMessage').val('');
                $('#ErrorMessage').hide();
                $('#m_saveImp').removeAttr('disabled');
            }
        }

        function resize(w, h) {
            window.dialogWidth = w + "px";
            window.dialogHeight = h + "px";
        }

        jQuery(function ($) {
            resize(500, 500); // this doesn't call the above resize function, but the LQBPopup's if LQBPopup exists.

            $('#ExcludedStatusesDiv a').data("type", "ExcludedStatuses");
            $('#StateExemptions a').data("type", "Exemptions");
            $('#LenderLicenses a').data("type", "Licenses");
            $('#LendingPolicies a').data("type", "Policies");

            $('a').click(function () {
                var type = $(this).data("type");
                if (!type) return;
                var select = $(this).siblings('select').first();
                var args = { init: [] };
                var opts = select[0].options;
                var numInit = opts.length;
                for (var i = 0; i < numInit; i++) {
                    args.init.push(opts[i].value);
                }

                var beginAuditStatusQuery = '';
                if (type === "ExcludedStatuses") {
                    beginAuditStatusQuery = '&beginAuditStatus=' + $('#BeginAuditStatus').val();
                }

                var vendorQuery = '&vendor=complianceease';
                showModal("/los/admin/ComplianceOptionPicker.aspx?type=" + type + beginAuditStatusQuery + vendorQuery, args, null, null, function(ret){
                    var results = ret.result;
    
                    if (results) {
                        select.empty().append($.map(results, function(res){
                            return hypescriptDom("option", {value:res.value}, res.text);
                        }));
    
                        var submit = $(this).siblings('input[type="hidden"]');
                        submit.val(ret.valuesString);
    
                        if (type === "ExcludedStatuses") {
                            validateBeginAuditsStatus();
                        }
                    }
                }, {context: this, hideCloseButton: true});
            })
        });
    </script>

</head>
<body>
    <h4 class="page-header">ComplianceEase Account Details</h4>
    <form id="ComplianceEaseConfigurationEditor" method="post" runat="server">
    <uc1:cModalDlg ID="CModalDlg1" runat="server"></uc1:cModalDlg>
    <div id="Main">
        <div id="BeginRunningAuditsAt" class="Section">
            <h1>
                Begin Audits when loan reaches:</h1>
            <select id="BeginAuditStatus" runat="server" onchange="validateBeginAuditsStatus();"></select>
        </div>
        <div id="ExcludedStatusesDiv" class="Section">
            <h1>
                Exclude loans with loan status:
            </h1>
            <div>
                <select class="box" multiple="true" id="ExcludedStatuses" runat="server"></select>
                <a href="#" class="Edit">edit excluded statuses</a>
                <input type="hidden" id="ExcludedStatusesSubmit" runat="server" value="null" />
            </div>
        </div>
        <div id="LenderLicenses" class="Section">
            <h1>
                Lender Licenses:</h1>
            <div>
                <select class="box" multiple="true" id="Licenses" runat="server"></select>
                <a href="#" class="Edit">edit licenses</a>
                <input type="hidden" id="LicensesSubmit" runat="server" value="null" />
            </div>
        </div>
        <div id="LendingPolicies" class="Section">
            <h1>
                Lending Policies:</h1>
            <div>
                <select class="box" multiple="true" id="Policies" runat="server"></select>
                <a href="#" class="Edit">edit lending policies</a>
                <input type="hidden" id="PoliciesSubmit" runat="server" value="null" />
            </div>
        </div>
        <div id="StateExemptions" class="Section">
            <h1>
                DIDMCA State Exemptions:</h1>
            <div>
                <select class="box" multiple="true" id="Exemptions" runat="server"></select>
                <a href="#" class="Edit">edit DIDMCA exemptions</a>
                <input type="hidden" id="ExemptionsSubmit" runat="server" value="null"/>
            </div>
        </div>
        <div id="HUDApproval" class="Section" style="padding-bottom:7px">
            <h1>
                HUD Approval Status:</h1>
            <select id="Approval" runat="server">
            </select>
        </div>
        <table cellspacing="0" class="FieldLabel">
            <tr>
                <td colspan="2" align="center" class="FormTableHeader">
                    Default Fee Profile
                </td>
            </tr>
            <tr><td></td></tr>
            <tr class="FormTableHeader">
                <td>
                    800 ITEMS PAYABLE IN CONNECTION WITH LOAN
                </td>
                <td align="center">
                    Paid To
                </td>
            </tr>
            <tr>
                <td>
                    801 Loan origination fee
                </td>
                <td>
                    <asp:DropDownList ID="sLOrigFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    802 Credit (-) or Charge (+)
                </td>
                <td>
                    <asp:DropDownList ID="sLDiscntPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;Credit for lender paid fees
                </td>
                <td>
                    <asp:DropDownList ID="sGfeCreditLenderPaidItemFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;General Lender credit
                </td>
                <td>
                    <asp:DropDownList ID="sGfeLenderCreditFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;Discount points
                </td>
                <td>
                    <asp:DropDownList ID="sGfeDiscountPointFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    804 Appraisal fee
                </td>
                <td>
                    <asp:DropDownList ID="sApprFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    805 Credit report
                </td>
                <td>
                    <asp:DropDownList ID="sCrFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    806 Tax service fee
                </td>
                <td>
                    <asp:DropDownList ID="sTxServFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    807 Flood Certification
                </td>
                <td>
                    <asp:DropDownList ID="sFloodCertificationFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    808 Mortgage broker fee
                </td>
                <td>
                    <asp:DropDownList ID="sMBrokFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    809 Lender's inspection fee
                </td>
                <td>
                    <asp:DropDownList ID="sInspectFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    810 Processing fee
                </td>
                <td>
                    <asp:DropDownList ID="sProcFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    811 Underwriting fee
                </td>
                <td>
                    <asp:DropDownList ID="sUwFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    812 Wire transfer
                </td>
                <td>
                    <asp:DropDownList ID="sWireFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Other 800 Section Fees
                </td>
                <td>
                    <asp:DropDownList ID="s800RestPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr class="FormTableHeader">
                <td>
                    900 ITEMS REQUIRED BY LENDER TO BE PAID IN ADVANCE
                </td>
                <td align="center">
                    Paid To
                </td>
            </tr>
            <tr>
                <td>
                    901 Interest for Lock                    
                </td>
                <td>
                    <asp:DropDownList ID="sIPiaPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    902 Mortgage Insurance Premium
                </td>
                <td>
                    <asp:DropDownList ID="sMipPiaPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    903 Haz Ins.
                </td>
                <td>
                    <asp:DropDownList ID="sHazInsPiaPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Other 900 Section Fees
                </td>
                <td>
                    <asp:DropDownList ID="s900RestPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr class="FormTableHeader">
                <td>
                    1000 RESERVES DEPOSITED WITH LENDER 
                </td>
                <td align="center">
                    Paid To
                </td>
            </tr>
            <tr>
                <td>
                    1002 Haz ins. reserve
                </td>
                <td>
                    <asp:DropDownList ID="sHazInsRsrvPaidTo" runat="server" Enabled="false"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1003 Mtg ins. reserve
                </td>
                <td>
                    <asp:DropDownList ID="sMInsRsrvPaidTo" runat="server" Enabled="false"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1004 Tax resrv
                </td>
                <td>
                    <asp:DropDownList ID="sRealETxRsrvPaidTo" runat="server" Enabled="false"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1005 School taxes
                </td>
                <td>
                    <asp:DropDownList ID="sSchoolTxRsrvPaidTo" runat="server" Enabled="false"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1006 Flood ins. reserve
                </td>
                <td>
                    <asp:DropDownList ID="sFloodInsRsrvPaidTo" runat="server" Enabled="false"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Other 1000 Section Fees
                </td>
                <td>
                    <asp:DropDownList ID="s1000RestPaidTo" runat="server" Enabled="false"></asp:DropDownList>
                </td>
            </tr>
            <tr class="FormTableHeader">
                <td>
                    1100 TITLE CHARGES
                </td>
                <td align="center">
                    Paid To
                </td>
            </tr>
            <tr>
                <td>
                    1102 Closing/Escrow Fee
                </td>
                <td>
                    <asp:DropDownList ID="sEscrowFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1103 Owner's title Insurance
                </td>
                <td>
                    <asp:DropDownList ID="sOwnerTitleInsPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1104 Lender's title Insurance
                </td>
                <td>
                    <asp:DropDownList ID="sTitleInsFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1109 Doc preparation fee
                </td>
                <td>
                    <asp:DropDownList ID="sDocPrepFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1110 Notary fees
                </td>
                <td>
                    <asp:DropDownList ID="sNotaryFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1111 Attorney fees
                </td>
                <td>
                    <asp:DropDownList ID="sAttorneyFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Other 1100 Section Fees                    
                </td>
                <td>
                    <asp:DropDownList ID="s1100RestPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr class="FormTableHeader">
                <td>
                    1200 GOVERNMENT RECORDING & TRANSFER CHARGES
                </td>
                <td align="center">
                    Paid To
                </td>
            </tr>
            <tr>
                <td>
                    1201 Recording fees
                </td>
                <td>
                    <asp:DropDownList ID="sRecFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1204 City/County tax stamps
                </td>
                <td>
                    <asp:DropDownList ID="sCountyRtcPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    1205 State tax/stamps
                </td>
                <td>
                    <asp:DropDownList ID="sStateRtcPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Other 1200 Section Fees                    
                </td>
                <td>
                    <asp:DropDownList ID="s1200RestPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr class="FormTableHeader">
                <td>
                    1300 ADDITIONAL SETTLEMENT CHARGES
                </td>
                <td align="center">
                    Paid To
                </td>
            </tr>
            <tr>
                <td>
                    1302 Pest Inspection
                </td>
                <td>
                    <asp:DropDownList ID="sPestInspectFPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Other 1300 Section Fees
                </td>
                <td>
                    <asp:DropDownList ID="s1300RestPaidTo" runat="server"></asp:DropDownList>
                </td>
            </tr>
        </table>
        <p class="ErrorMessageSection">
            <span runat="server" class="hidden" ID="ErrorMessage"></span>
        </p>
        <p class="leg">
            <asp:Button runat="server" ID="m_saveImp" Text="Update" OnClick="Update_Click"></asp:Button>
        </p>
    </div>
    </form>
</body>
</html>
