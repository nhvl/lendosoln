﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocuSignAuthCallback.aspx.cs" Inherits="LendersOfficeApp.los.admin.DocuSignAuthCallback" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>DocuSign Authentication Callback</title>
    <style type="text/css">
        body {
            width: 100%;
            height: 100%;
            text-align: center;
        }
        .centered
        {
            margin: 0;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
    </style>
    <script type="text/javascript">
        jQuery(function ($) {
            $.urlParam = function(name){
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                if (results==null){
                    return null;
                }
                else{
                    return decodeURIComponent(results[1]) || 0;
                }
            }
            gService.register(ML.VirtualRoot + '/los/admin/ConfigureDocuSignService.aspx', 'save');
            var args = { code: $.urlParam('code'), state: $.urlParam('state') };
            var successCallback = function (result) {
                window.opener.location.href = window.opener.location.href;
                window.opener.focus();
                window.close();
            };
            var errorCallback = function (result) {
                alert("Something went wrong. Please try again.");

                window.opener.location.href = window.opener.location.href;
                window.opener.focus();
                window.close();
            };
            gService['save'].callAsync('RegisterUser', args, true, false, true, true, successCallback, errorCallback)
        });
    </script>
</head>
<body>
    <form id="aspform" runat="server">
        <div class="centered">
            <span class="fa fa-spin fa-spinner"></span> LendingQB is authenticating with DocuSign...
        </div>
    </form>
</body>
</html>
