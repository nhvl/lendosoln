﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Admin.DocuSign;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;

    /// <summary>
    /// Tab for services.
    /// </summary>
    public partial class LenderServices : System.Web.UI.UserControl
    {
        /// <summary>
        /// Enum for service type.
        /// </summary>
        private enum ServiceType
        {
            /// <summary>
            /// VOX service type.
            /// </summary>
            Vox = 0,

            /// <summary>
            /// Title service type.
            /// </summary>
            Title = 1
        }

        /// <summary>
        /// Gets a value indicating whether the broker is configured to use VOX.
        /// </summary>
        /// <value>Whether the broker is configured to use VOX.</value>
        protected bool IsEnableVOXConfiguration
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerDB.IsEnableVOXConfiguration;
            }
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            Guid brokerId = principal.BrokerId;

            List<ServiceRowViewModel> viewModels = new List<ServiceRowViewModel>();
            if (IsEnableVOXConfiguration)
            {
                this.AddVoxService.Visible = true;
                IEnumerable<VOXLenderService> lenderServices = VOXLenderService.GetLenderServices(brokerId);
                Dictionary<int, LightVOXVendor> vendorsWithServices = VOXVendor.LoadLightVendorsWithServices();

                var voxServices = VOXLenderServiceViewModel.CreateViewModelFromVendorAndServices(lenderServices, vendorsWithServices);
                foreach (var service in voxServices)
                {
                    ServiceRowViewModel model = new ServiceRowViewModel()
                    {
                        AssociatedResellerName = service.AssociatedResellerName,
                        AssociatedVendorName = service.AssociatedVendorName,
                        DisplayName = service.DisplayName,
                        ServiceId = service.LenderServiceId.Value,
                        ServiceTypeName = service.ServiceTypeName,
                        Type = ServiceType.Vox.ToString("D")
                    };

                    viewModels.Add(model);
                }
            }

            if (!principal.BrokerDB.DisableNewTitleFramework)
            {
                this.AddTitleServiceBtn.Visible = true;
                viewModels.AddRange(TitleLenderService.GetLenderServicesByBrokerId(brokerId).Select(
                    service =>
                        new ServiceRowViewModel
                        {
                            AssociatedResellerName = string.Empty,
                            AssociatedVendorName = service.VendorName,
                            DisplayName = service.DisplayName,
                            ServiceId = service.LenderServiceId,
                            ServiceTypeName = "Title",
                            Type = ServiceType.Title.ToString("D")
                        }));
            }

            this.LenderServiceRepeater.DataSource = viewModels;
            this.LenderServiceRepeater.DataBind();

            LenderDocuSignSettings docuSignSettings = LenderDocuSignSettings.Retrieve(principal.BrokerId);
            if (docuSignSettings == null || !docuSignSettings.IsSetupComplete())
            {
                this.ConfigureDocuSignButton.Disabled = true;
                this.ConfigureDocuSignMessage.InnerText = "Please contact LendingQB support to start the process of setting up DocuSign API and Integrator Key Information.";
            }
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ((BaseServicePage)this.Page).RegisterService("lenderService", "/los/admin/LenderServicesEditService.aspx");
            ((BaseServicePage)this.Page).RegisterService("titleService", "/los/admin/EditTitleLenderServiceService.aspx");
            ((BaseServicePage)this.Page).RegisterJsScript("ServiceCredential.js");
            ((BaseServicePage)this.Page).RegisterCSS("ServiceCredential.css");
            ((BaseServicePage)this.Page).RegisterJsScript("LQBPopup.js");

            var serviceCredentials = ServiceCredential.GetAllBrokerServiceCredentials(PrincipalFactory.CurrentPrincipal.BrokerId, ServiceCredentialService.All);
            var serviceCredentialsJson = LendersOffice.Common.ObsoleteSerializationHelper.JsonSerialize(serviceCredentials);
            Page.ClientScript.RegisterHiddenField("ServiceCredentialsJson", serviceCredentialsJson);
        }

        /// <summary>
        /// Class to help populate the initial services table.
        /// </summary>
        private class ServiceRowViewModel
        {
            /// <summary>
            /// Gets or sets the lender service id.
            /// </summary>
            /// <value>The lender service id.</value>
            public int ServiceId
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the service type name.
            /// </summary>
            /// <value>The service type name.</value>
            public string ServiceTypeName
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the display name.
            /// </summary>
            /// <value>The display name.</value>
            public string DisplayName
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the associated vendor name.
            /// </summary>
            /// <value>The associated vendor name.</value>
            public string AssociatedVendorName
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the associated reseller name.
            /// </summary>
            /// <value>The associated reseller name.</value>
            public string AssociatedResellerName
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the type of service.
            /// </summary>
            /// <value>The type of service.</value>
            public string Type
            {
                get;
                set;
            }
        }
    }
}