﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LenderServicesEdit.aspx.cs" Inherits="LendersOfficeApp.los.admin.LenderServicesEdit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Integration.VOXFramework" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Service</title>
    <style type="text/css">
        .Center
        {
            text-align: center;
        }
        .LabelColumn
        {
            width: 40%;
        }
        .Hidden
        {
            display: none;
        }
        .InputText
        {
            width: 250px;
        }
        .PaddingBottom {
            padding-bottom: 10px;
        }
        .PaddingLeft10px {
            padding-left: 10px;
        }
        .PaddingLeft15px {
            padding-left: 15px;
        }
        .PopupHeight {
            height: 350px;
        }
        #ButtonDiv {
            position: absolute;
            bottom: 0;
            left: 50%;
            margin-left: -25px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            function onEnabledForTpoUsersClicked() {
                var checked = $('#EnabledForTpoUsers').prop('checked');
                $('#EnabledOCGroupId').prop('disabled', !checked);
                $('#CanBeUsedForCredentialsInTpo').prop('disabled', !checked);
                showVoaConfigurationWarning();
            }

            function onEnabledForLqbUsersClicked() {
                $('#EnabledEmployeeGroupId').prop('disabled', !$('#EnabledForLqbUsers').prop('checked'));
                showVoaConfigurationWarning();
            }

            var voaServiceValue = <%= AspxTools.JsString(VOXServiceT.VOA_VOD.ToString("D")) %>;
            var voeServiceValue = <%= AspxTools.JsString(VOXServiceT.VOE.ToString("D")) %>;
            var $ServiceTypeDDL = $('#ServiceType');
            var $voeDataRows = $('.VoeUseSpecificEmployerRecordSearchRow, .VOESection');

            $('#EnabledForLqbUsers').change(function () {
                onEnabledForLqbUsersClicked();
                if (this.checked) {
                    $('#EnabledEmployeeGroupId').find('option:first').prop('selected', true);
                }
            });

            $('#EnabledForTpoUsers').change(function () {
                onEnabledForTpoUsersClicked();
                if (this.checked) {
                    $('#EnabledOCGroupId').find('option:first').prop('selected', true);
                }
                else {
                    $('#CanBeUsedForCredentialsInTpo').prop('checked', false);
                }
            });
            
            $ServiceTypeDDL.change(ToggleServiceTypeUI);
            $ServiceTypeDDL.change(function () {
                var data = {
                    ServiceType: $(this).val()
                };

                var result = gService.edit.call('CalculateVendorByServiceType', data);
                if (!result.error) {
                    var vendors = JSON.parse(result.value["Vendors"]);
                    var associateVendorDDL = $('#AssociatedVendorId');
                    associateVendorDDL.find('option').remove();
                    associateVendorDDL.append($('<option>', { value: "" }).attr('data-canPayCC', false).text(""));
                    for (var i = 0; i < vendors.length; i++) {
                        var vendor = vendors[i];
                        var id = vendor.Id;
                        var name = vendor.CompanyName;
                        var canPayCC = vendor.CanPayWithCreditCard;
                        associateVendorDDL.append($('<option>', { value: id}).attr('data-canPayCC', canPayCC).text(name));
                    }
                    $('#AssociatedResellerId').find('option').remove();
                    CalculateDisplayName();
                    ToggleCreditCardCb();
                }
                else {
                    alert(result.UserMessage);
                }
            });

            $('#AssociatedVendorId').change(function () {
                if ($(this).val() === '') {
                    $('#AssociatedResellerId').val('');
                    return;
                }

                var data = {
                    ServiceType: $('#ServiceType').val(),
                    AssociatedVendorId: $(this).val()
                };

                var result = gService.edit.call('CalculateResellerbyVendorAndServiceType', data);
                if (!result.error) {
                    if (result.value["Success"] === 'True') {
                        var resellers = JSON.parse(result.value["Resellers"]);
                        var associatedResellerDDL = $('#AssociatedResellerId');
                        associatedResellerDDL.find('option').remove();
                        associatedResellerDDL.append($('<option>', { value: "" }).attr('data-canPayCC', false).text(""));
                        for (var i = 0; i < resellers.length; i++) {
                            var reseller = resellers[i];
                            var id = reseller.Id;
                            var name = reseller.CompanyName;
                            var canPayCC = reseller.CanPayWithCreditCard;
                            associatedResellerDDL.append($('<option>', { value: id }).attr('data-canPayCC', canPayCC).text(name));
                        }

                        CalculateDisplayName();
                        ToggleCreditCardCb();
                    }
                    else {
                        alert(result.value["Error"]);
                    }

                    if ($ServiceTypeDDL.val() === voaServiceValue) {
                        showVoaConfigurationWarning();
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            });

            function showVoaConfigurationWarning() {
                $('#VoaConfigurationWarning').show();
            }

            $('#AssociatedResellerId').change(function () {
                CalculateDisplayName();
                ToggleCreditCardCb();
            });

            $('#DisplayNameLckd').change(function () {
                CalculateDisplayName();
                $('#DisplayName').prop('disabled', !$('#DisplayNameLckd').is(':checked'));
            });

            $('#CancelBtn').click(function () {
                parent.LQBPopup.Return(null);
            });

            $('#OkBtn').click(function () {
                var service = {
                    LenderServiceId: $('#LenderServiceId').val(),
                    ServiceType: $('#ServiceType').val(),
                    AssociatedVendorId: $('#AssociatedVendorId').val(),
                    AssociatedResellerId: $('#AssociatedResellerId').val(),
                    DisplayName: $('#DisplayName').val(),
                    DisplayNameLckd: $('#DisplayNameLckd').is(':checked'),
                    EnabledForLqbUsers: $('#EnabledForLqbUsers').is(':checked'),
                    EnabledEmployeeGroupId: $('#EnabledEmployeeGroupId').val(),
                    EnabledForTpoUsers: $('#EnabledForTpoUsers').is(':checked'),
                    EnabledOCGroupId: $('#EnabledOCGroupId').val(),
                    AllowPaymentByCreditCard: $('#AllowPaymentByCreditCard').is(':checked'),
                    VoeUseSpecificEmployerRecordSearch: $('#VoeUseSpecificEmployerRecordSearch').is(':checked'),
                    VoeEnforceUseSpecificEmployerRecordSearch: $('#VoeEnforceUseSpecificEmployerRecordSearch_0').is(':checked'),
                    VoaAllowImportingAssets: $('#VoaAllowImportingAssets').is(':checked'),
                    VoaAllowRequestWithoutAssets: $('#VoaAllowRequestWithoutAssets').is(':checked'),
                    VoaEnforceAllowRequestWithoutAssets: $('#VoaEnforceAllowRequestWithoutAssets_0').is(':checked'),
                    <%-- VoeAllowImportingEmployment: $('#VoeAllowImportingEmployment').is(':checked'), // Uncomment when adding import --%>
                    VoeAllowRequestWithoutEmployment: $('#VoeAllowRequestWithoutEmployment').prop('checked'),
                    VoeEnforceRequestsWithoutEmployment: $('input[name=VoeEnforceRequestsWithoutEmployment]:checked').val() === '1',
                    CanBeUsedForCredentialsInTpo: $('#CanBeUsedForCredentialsInTpo').is(':checked')
                };

                var voaOptionJson = '';
                if ($ServiceTypeDDL.val() === voaServiceValue) {
                    voaOptionJson = $('#VoaOptionJson').val();
                }

                if (!VerifyService(service))
                {
                    return;
                }

                var data = {
                    ServiceViewModel: JSON.stringify(service),
                    VoaOptionJson: voaOptionJson
                };

                var result = gService.edit.call('SaveLenderService', data);
                if (!result.error) {
                    if (result.value["Success"] === 'True') {
                        var returnArgs = {
                            ServiceTypeName: result.value["ServiceName"],
                            ServiceId: result.value["LenderServiceId"],
                            IsNew: result.value["IsNew"],
                            DisplayName: $('#DisplayName').val(),
                            VendorName: $('#AssociatedVendorId').find(':selected').text(),
                            ResellerName: $('#AssociatedResellerId').find(':selected').text(),
                            Type: 0
                        };

                        parent.LQBPopup.Return(returnArgs);
                    }
                    else {
                        alert(result.value["Errors"]);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            });

            $('#VoaOptionSection').on('click', function() {
                var vendorId = $('#AssociatedVendorId').val();
                if (vendorId === '') {
                    alert("A vendor must be selected before configuring VOA options.");
                    return;
                }

                var lenderServiceId = $('#LenderServiceId').val();
                var enabledForLqbUsers = $('#EnabledForLqbUsers').is(':checked');
                var enabledForPmlUsers = $('#EnabledForTpoUsers').is(':checked');
                var voaOptionJsonOverride = vendorId === $('#VoaOptionJsonVendorId').val() ? $('#VoaOptionJson').val() : '';

                var url = gVirtualRoot + "/los/admin/VoaOptionConfigurationEdit.aspx"
                    + "?vendorId=" + encodeURIComponent(vendorId)
                    + "&lenderServiceId=" + encodeURIComponent(lenderServiceId)
                    + "&enabledLqb=" + encodeURIComponent(enabledForLqbUsers)
                    + "&enabledPml=" + encodeURIComponent(enabledForPmlUsers)
                    + "&VoaOptionJsonOverride=" + encodeURIComponent(voaOptionJsonOverride);

                LQBPopup.Show(url, {
                    hideCloseButton: true,
                    popupClasses: 'Popup',
                    width: 400,
                    height: 400,
                    onReturn: VoaOptionConfigurationReturn
                }, null);
            });

            function toggleVoaSectionVisibility() {
                $('.VOASection').toggle($ServiceTypeDDL.val() === voaServiceValue);
            }

            function ToggleVoeRows() {
                $voeDataRows.toggle($ServiceTypeDDL.val() === voeServiceValue);
            }
            
            function ToggleTPOEnabled() {
                var serviceType = $ServiceTypeDDL.val();
                var showTpoRows = serviceType === <%= AspxTools.JsString(LendersOffice.Integration.VOXFramework.VOXServiceT.VOA_VOD) %> ||
                                  serviceType === <%= AspxTools.JsString(LendersOffice.Integration.VOXFramework.VOXServiceT.VOE) %>;
                $('.TpoRow').toggle(showTpoRows);
                if(!showTpoRows) {
                    $('#EnabledForTpoUsers').prop('checked', false).change();
                }
            }
            
            function ToggleServiceTypeUI() {
                ToggleVoeRows();
                ToggleTPOEnabled();
                toggleVoaSectionVisibility();
            }

            ToggleServiceTypeUI();

            function VoaOptionConfigurationReturn(returnArgs) {
                $('#VoaOptionJsonVendorId').val(returnArgs.VendorId);
                $('#VoaOptionJson').val(returnArgs.OptionSet);
                $('#VoaConfigurationWarning').hide();
            }

            $('#DisplayName').prop('disabled', !$('#DisplayNameLckd').is(':checked'));
            onEnabledForTpoUsersClicked();
            onEnabledForLqbUsersClicked();
            ToggleCreditCardCb();

            function CalculateDisplayName()
            {
                if (!$('#DisplayNameLckd').is(':checked')) {
                    var resellerId = parseInt($('#AssociatedResellerId').val());
                    var vendorName = $('#AssociatedVendorId').find(':selected').text();
                    if (resellerId > 1) {
                        var resellerName = $('#AssociatedResellerId').find(':selected').text();
                        $('#DisplayName').val(vendorName + " by " + resellerName);
                    }
                    else
                    {
                        $('#DisplayName').val(vendorName);
                    }
                }
            }

            function ToggleCreditCardCb()
            {
                var selectedVendorCanPayCC = $('#AssociatedVendorId').find('option:selected').attr('data-canPayCC');
                var selectedResellerCanPayCC = $('#AssociatedResellerId').find('option:selected').attr('data-canPayCC');

                var shouldShow = false;
                if (typeof (selectedResellerCanPayCC) != 'undefined' && selectedResellerCanPayCC != null && $('#AssociatedResellerId').find('option:selected').val() !== '-1')
                {
                    shouldShow = selectedResellerCanPayCC.toLowerCase() == 'true';
                }
                else if (typeof (selectedVendorCanPayCC) != 'undefined' && selectedVendorCanPayCC != null)
                {
                    shouldShow = selectedVendorCanPayCC.toLowerCase() == 'true';
                }

                var wasVisible = $('#CreditCardRow').is(":visible");
                $('#CreditCardRow').toggle(shouldShow);

                if (!wasVisible && $('#CreditCardRow').is(':visible')) {
                    // Not visible to visible, we want to check the check box.
                    $('#AllowPaymentByCreditCard').prop('checked', true);
                }
            }

            function VerifyService(data)
            {
                if(data.DisplayName === '')
                {
                    alert("Please specify a display name.");
                    return false;
                }

                if(data.AssociatedVendorId === null || data.AssociatedVendorId === '')
                {
                    alert("Please specify a Vendor.");
                    return false;
                }

                if(data.AssociatedResellerId === null || data.AssociatedResellerId === '')
                {
                    alert("Please specify a Provider");
                    return false;
                }

                return true;
            }
        });
    </script>
    <h4 class="page-header">Edit Service</h4>
    <form id="form1" runat="server" class="PopupHeight">
    <div class="FullWidthHeight">
    <div>
        <input type="checkbox" runat="server" id="IsNew" disabled="disabled" class="Hidden" />
        <input type="hidden" runat="server" id="LenderServiceId" disabled="disabled" />
        <table>
            <tr>
                <td class="FieldLabel LabelColumn">Service Type</td>
                <td>
                    <asp:DropDownList runat="server" ID="ServiceType"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">Vendor</td>
                <td>
                    <asp:DropDownList ID="AssociatedVendorId" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">Provider</td>
                <td>
                    <asp:DropDownList ID="AssociatedResellerId" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">Display Name</td>
                <td>
                    <div>
                        <input type="text" id="DisplayName" runat="server" class="InputText" />
                        <label>
                            <input type="checkbox" id="DisplayNameLckd" runat="server" />lock
                        </label>
                    </div>
                    <div>
                        *This is the name which will be displayed to the user when they are asked to select a service provider
                    </div>
                </td>
            </tr>
            <tr id="CreditCardRow">
                <td class="FieldLabel" colspan="2">
                    <label>
                        <input type="checkbox" id="AllowPaymentByCreditCard" runat="server"/>
                        Allow payment by credit card?
                    </label>
                </td>
            </tr>
            
            <tr class="VoeUseSpecificEmployerRecordSearchRow">
                <td class="FieldLabel" colspan="2">
                    <label>
                        <input type="checkbox" id="VoeUseSpecificEmployerRecordSearch" runat="server"/>
                        Use Specific Employer Record Search?
                    </label>
                </td>
            </tr>
            <tr class="VoeUseSpecificEmployerRecordSearchRow">
                <td class="FieldLabel" style="padding-left: 50px;" colspan="2">
                    <asp:RadioButtonList ID="VoeEnforceUseSpecificEmployerRecordSearch" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Enforce this setting" Value="1" Selected="True"/>
                        <asp:ListItem Text="Allow user to select (apply setting by default)" Value="0"/>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="VOASection">
                <td class="FieldLabel" colspan="2">
                    <label>
                        <input type="checkbox" id="VoaAllowRequestWithoutAssets" runat="server" />Allow requesting report without specifying assets?
                    </label>
                </td>
            </tr>
            <tr class="VOASection">
                <td class="FieldLabel" style="padding-left: 50px;" colspan="2">
                    <asp:RadioButtonList ID="VoaEnforceAllowRequestWithoutAssets" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Enforce this setting" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Allow user to select (apply setting by default)" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr class="VOASection">
                <td class="FieldLabel" colspan="2">
                    <label>
                        <input type="checkbox" id="VoaAllowImportingAssets" runat="server" />Allow importing asset records?
                    </label>
                </td>
            </tr>
            <tr class="VOESection">
                <td class="FieldLabel" colspan="2">
                    <label>
                        <input type="checkbox" id="VoeAllowRequestWithoutEmployment" runat="server" />Allow requesting report for a borrower without specifying employment?
                    </label>
                </td>
            </tr>
            <tr class="VOESection">
                <td class="FieldLabel" style="padding-left: 50px;" colspan="2">
                    <asp:RadioButtonList ID="VoeEnforceRequestsWithoutEmployment" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Enforce this setting" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Allow user to select (apply setting by default)" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="FieldLabel">
                    <label>
                        <input type="checkbox" id="EnabledForLqbUsers" runat="server"/>Enable this service for LQB users?
                    </label>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel PaddingLeft15px">Only display to Employee Group:</td>
                <td>
                    <asp:DropDownList NotEditable="true" runat="server" ID="EnabledEmployeeGroupId"></asp:DropDownList>
                </td>
            </tr>
            <tr class="TpoRow">
                <td colspan="2" class="FieldLabel">
                    <label>
                        <input type="checkbox" id="EnabledForTpoUsers" runat="server" />Enable this service for TPO users?
                    </label>
                    <div class="PaddingLeft10px">
                        <label>
                            <input type="checkbox" id="CanBeUsedForCredentialsInTpo" runat="server" />Allow TPO users to set their own credentials?
                        </label>
                    </div>
                </td>
            </tr>
            <tr class="TpoRow">
                <td class="FieldLabel PaddingLeft15px">Only display to OC Group:</td>
                <td>
                    <asp:DropDownList NotEditable="true" runat="server" ID="EnabledOCGroupId"></asp:DropDownList>
                </td>
            </tr>
            <tr class="VOASection">
                <td colspan="2" class="FieldLabel">
                    <br />
                    <a id="VoaOptionSection">
                        Configure VOA Options
                    </a>
                    <img src="../../images/warn.png" alt="warning" id="VoaConfigurationWarning" class="Hidden" />
                    <input type="hidden" id="VoaOptionJson" />
                    <input type="hidden" id="VoaOptionJsonVendorId" />
                </td>
            </tr>
        </table>
        </div>
        <div>
            <div id="ButtonDiv" class="Center align-bottom PaddingBottom">
                <input type="button" class="Width50Px" value="OK" id="OkBtn"/>
                <input type="button" class="Width50Px" value="Cancel" id="CancelBtn"/>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
