<%@ Page language="c#" Codebehind="EditEmployee.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.EditEmployee" %>
<%@ Register TagPrefix="uc" TagName="EmployeeEdit" Src="../../los/admin/EmployeeEdit.ascx" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head runat="server">
		<title>Edit Employee</title>
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="../../inc/utilities.js" > </script>
	</head>
	<body class="expand-w-content" style="OVERFLOW-Y: auto; BACKGROUND-COLOR: #003366" onload="onInit();">
		<style type="text/css">
			form{
				margin-bottom:0px;
			}

            .containing-td {
                padding-left: 8px;
                padding-right: 8px;
            }
		</style>
        <script type="text/javascript" >
		function onInit()
		{
		    var errorMessage = document.getElementById("m_errorMessage");
		    var commandToDo = document.getElementById("m_commandToDo");

			if( errorMessage != null )
				alert( errorMessage.value );
			else
			if( commandToDo != null )
			{
				if( commandToDo.value == "Close" )
				{
					var args = window.dialogArguments || {};
					args.OK = true;
					onClosePopup(args);
				}
			}
			<%-- // 08/21/06 mf OPM 7003. --%>
			if ( typeof(setUIStatus) != 'undefined' )
				setUIStatus();

			if( window.dialogArguments != null )
			{			    
			    
				<% if( IsPostBack == false ) { %>				    
					resizeForIE6And7( 950 , 800 ); 
				<% } %>
			}
		}
		
		function PopulateDefaultRolePermissions(selectedRoles, selectedValues)
		{	
			var args = new Object();
			for(var i = 0; i < selectedRoles.length; ++i)
			{
				var name = selectedRoles[i];
				var value = selectedValues[i];
				args[name] = value;
			}
			return gService.main.call("PopulateDefaultRolePermissions", args);
		}
		
		function PopulateDefaultNonBoolRolePermissions(selectedRoles, selectedValues)
		{	
			var args = new Object();
			for(var i = 0; i < selectedRoles.length; ++i)
			{
				var name = selectedRoles[i];
				var value = selectedValues[i];
				args[name] = value;
			}
			return gService.main.call("PopulateRSENonBoolPermission", args);
		}
		</script>

		<form id="EditEmployee" method="post" runat="server" class="expand-w-content">
        <h4 class="page-header">Edit Employee</h4>
			<table cellspacing="0" border="0" width="100%" height="100%">
				<tr>
					<td class="containing-td" height="100%">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
							<tr>
								<td height="100%" style="BORDER: 2px outset; BACKGROUND-COLOR: gainsboro;">
									<uc:EmployeeEdit id="m_Edit" runat="server"></uc:EmployeeEdit>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<uc:CModalDlg runat="server"></uc:CModalDlg>
		</form>
	</body>
</html>
