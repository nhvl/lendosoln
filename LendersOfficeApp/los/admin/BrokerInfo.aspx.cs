using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.common;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.Common;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{
	public partial class BrokerInfo : LendersOffice.Common.BaseServicePage
    {

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        protected bool IsEnableVOXConfiguration
        {
            get
            {
                return this.BrokerUser.BrokerDB.IsEnableVOXConfiguration;
            }
        }

        protected bool CanViewIPRestrictionsTab
        {
            get { return BrokerUser.HasPermission(Permission.AllowViewingAndEditingGlobalIPSettings); }
        }

		public Boolean IsPmlEnabled
		{
			set { ViewState["IsPmlEnabled"] = value; }
			get 
			{ 
				if (ViewState["IsPmlEnabled"] != null)
					return (Boolean) ViewState["IsPmlEnabled"]; 
				else
				{
					BrokerFeatures bFs = new BrokerFeatures(BrokerUser.BrokerId);
					return (Boolean) ( ViewState["IsPmlEnabled"] = bFs.HasFeature( E_BrokerFeatureT.PriceMyLoan ) );
				}
			}
		}

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        protected void PageLoad(object sender, System.EventArgs e)
        {
            ((BasePage)Page).RegisterJsScript("LQBPopup.js");
            ((BasePage)Page).IncludeStyleSheet("~/css/Tabs.css");
			// Designate a help file in the base object so we can
			// direct users to the right page on query for more
			// info.

            HelpFile = "CorporateAdmin.aspx";
            //if pml is not enabled, then hide the Lock Policies tab
			if(!IsPmlEnabled)
				tab7.Style.Add("display", "none");

            EmployeeDB empDb = new EmployeeDB(BrokerUser.EmployeeId, BrokerUser.BrokerId);
            if (BrokerUser.EmployeeId != Guid.Empty)
            {
                if (empDb.Retrieve() == false)
                    throw new CBaseException(ErrorMessages.Generic, "Unable to load employee.");
            }

            LendingLicense.LicenseList = empDb.LicenseInformationList;
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
			// Register the background updater to avoid a full
			// postback on saving.
            RegisterService("main", "/los/admin/BrokerInfoService.aspx");
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.EnableJqueryMigrate = false;

            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

    }

}
