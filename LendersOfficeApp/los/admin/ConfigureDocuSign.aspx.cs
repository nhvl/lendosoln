﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using CommonProjectLib.Common;
    using LendersOffice.Admin.DocuSign;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// A page for configuring DocuSign options for a lender.
    /// </summary>
    public partial class ConfigureDocuSign : LendersOffice.Common.BaseServicePage
    {
        /// <summary>
        /// Gets the permission required to load this page.
        /// </summary>
        /// <value>The permission required for page load.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Page initialization event.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.RegisterService("save", "/los/admin/ConfigureDocuSignService.aspx");
        }

        /// <summary>
        /// Page load event.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnLoad(EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            AbstractUserPrincipal user = PrincipalFactory.CurrentPrincipal;
            LenderDocuSignSettings docuSignSettings = LenderDocuSignSettings.Retrieve(user.BrokerId);

            this.RegisterJsGlobalVariables("docuSignEnabled", (docuSignSettings != null && docuSignSettings.DocuSignEnabled) || RequestHelper.GetBool("enableDocuSign"));

            bool loadminSetupComplete = docuSignSettings != null && !string.IsNullOrEmpty(docuSignSettings.IntegratorKey) && !string.IsNullOrEmpty(docuSignSettings.SecretKey) && !string.IsNullOrEmpty(docuSignSettings.RsaPrivateKey);
            if (loadminSetupComplete)
            {
                var docuSignEnabledMfa = new Dictionary<string, object>();
                foreach (MfaOptions option in Enum.GetValues(typeof(MfaOptions)))
                {
                    docuSignEnabledMfa.Add(option.ToString(), docuSignSettings.EnabledMfaOptions.HasFlag(option));
                }

                this.RegisterJsObjectWithJsonNetSerializer("docuSignEnabledMfa", docuSignEnabledMfa);
                this.RegisterJsGlobalVariables("IntegratorKey", docuSignSettings.IntegratorKey);
                this.RegisterJsGlobalVariables("SecretKey", docuSignSettings.SecretKey);
                this.RegisterJsGlobalVariables("docuSignDefaultMfa", docuSignSettings.DefaultMfaOption.ToString());
                this.RegisterJsGlobalVariables("docuSignUserName", docuSignSettings.DisplayUserName);
                this.RegisterJsGlobalVariables("docuSignUserId", docuSignSettings.DocuSignUserId);
                this.RegisterJsGlobalVariables("docuSignEnvironment", docuSignSettings.TargetEnvironment.ToString());
                this.RegisterJsGlobalVariables("docuSignIdCheckConfig", docuSignSettings.IdCheckConfigurationName);
                this.RegisterJsGlobalVariables("docuSignBrandIdUnspecified", docuSignSettings.BrandIdForBlank.ToString());
                this.RegisterJsGlobalVariables("docuSignBrandIdRetail", docuSignSettings.BrandIdForRetail.ToString());
                this.RegisterJsGlobalVariables("docuSignBrandIdWholesale", docuSignSettings.BrandIdForWholesale.ToString());
                this.RegisterJsGlobalVariables("docuSignBrandIdCorrespondent", docuSignSettings.BrandIdForCorrespondent.ToString());
                this.RegisterJsGlobalVariables("docuSignBrandIdBroker", docuSignSettings.BrandIdForBroker.ToString());
                this.RegisterJsGlobalVariables("docuSignBrandNameUnspecified", docuSignSettings.BrandNameForBlank);
                this.RegisterJsGlobalVariables("docuSignBrandNameRetail", docuSignSettings.BrandNameForRetail);
                this.RegisterJsGlobalVariables("docuSignBrandNameWholesale", docuSignSettings.BrandNameForWholesale);
                this.RegisterJsGlobalVariables("docuSignBrandNameCorrespondent", docuSignSettings.BrandNameForCorrespondent);
                this.RegisterJsGlobalVariables("docuSignBrandNameBroker", docuSignSettings.BrandNameForBroker);
            }
            else
            {
                throw new CBaseException("Please have API and Integrator Key Information configured first.", "Can't configure DocuSign in general settings until LOAdmin configuration is complete.");
            }
        }
    }
}