﻿
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="MIVendorBranchAccountDetails.aspx.cs" Inherits="LendersOfficeApp.los.admin.MIVendorBranchAccountDetails" %>
<%@ Import Namespace="LendersOffice.Common" %> 
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head runat="server">
    <title>PMI Provider Account Details</title>
	<style type="text/css">
		.FormTableHeader
		{
		    padding: 0.25em 0.25em;
		    text-align: left;
		    text-indent: 5px;	    
		}
		
		body
		{
		    background-color: Gainsboro;
		    padding: 1em;
		    margin: 0 auto;
		    text-align: center;
		    line-height:2em;
		}
		table
		{
		    border: 1px solid gray;
            border-collapse: collapse;
		    margin-bottom: 1em;
		    text-align: left;
		    width: 90%;
		}
        #BtnHolder 
        {
            border: 0;
            padding: 1em;
            text-align: center; 
        }
	</style>
  </head>
<body>
<script type="text/javascript">
    function oncheckboxClick(cb, vendorId) {
        var o = null;
        if (cb.checked) {
            $("." + vendorId + "_Username").prop("readonly", true);
            $("." + vendorId + "_Password").prop("readonly", true);
            $("." + vendorId + "_AccountId").prop("readonly", true);
            $("." + vendorId + "_PolicyId").prop("readonly", true);
            $("." + vendorId + "_IsDelegated input[type=radio]").prop("disabled", true);
            $("." + vendorId + "_IsNotDelegated input[type=radio]").prop("disabled", true);

            $("." + vendorId + "_RequiredField").prop("enabled", false);

            o = getCredential(vendorId, CachedBrokerCrendentials);
        }
        else {
            $("." + vendorId + "_Username").prop("readonly", false);
            $("." + vendorId + "_Password").prop("readonly", false);
            $("." + vendorId + "_AccountId").prop("readonly", false);
            $("." + vendorId + "_PolicyId").prop("readonly", false);
            $("." + vendorId + "_IsDelegated input[type=radio]").prop("disabled", false);
            $("." + vendorId + "_IsNotDelegated input[type=radio]").prop("disabled", false);

            $("." + vendorId + "_RequiredField").prop("enabled", true);

            o = getCredential(vendorId, CachedBranchCrendentials);
        }

        if (o != null) {
            $("." + vendorId + "_Username").val(o.UserName);
            $("." + vendorId + "_Password").val(o.Password);
            $("." + vendorId + "_AccountId").val(o.AccountId);
            $("." + vendorId + "_PolicyId").val(o.PolicyId);
            $("." + vendorId + "_IsDelegated input[type=radio]").prop("checked", o.IsDelegated);
            $("." + vendorId + "_IsNotDelegated input[type=radio]").prop("checked", !o.IsDelegated);
        }
        else {
            $("." + vendorId + "_Username").val("");
            $("." + vendorId + "_Password").val("");
            $("." + vendorId + "_AccountId").val("");
            $("." + vendorId + "_PolicyId").val("");
            $("." + vendorId + "_IsDelegated input[type=radio]").prop("checked", false);
            $("." + vendorId + "_IsNotDelegated input[type=radio]").prop("checked", true);
        }
            
        Page_ClientValidate();
    }
    
    function getCredential(vendorId, cache) {
        for (var i = 0; i < cache.length; i++) {
            var obj = cache[i];
            if (obj.VendorId == vendorId.toLowerCase()) {
                return obj;
            }
        }
    }

</script>
    <form id="PMIBrokerConfigurationEditor" method="post" runat="server" style="text-align:center;">
        <asp:Repeater runat="server" ID="VendorDetails" OnItemDataBound="VendorDetailsBind">
            <ItemTemplate>
                <table>
                    <thead class="FormTableHeader">
                        <tr>
                            <td colspan="3">
                                <ML:EncodedLabel ID="m_VendorName" runat="server" />
                                <asp:HiddenField ID="m_VendorId" runat="server" />
                            </td>
                        </tr>
                    </thead>
		            <tbody>
                        <asp:PlaceHolder runat="server" ID="UseCorporteSettingsPanel">
                            <tr>
                                <td align="right" nowrap="nowrap" style="padding-right: 1em;">Use Corporate Settings:</td>
                                <td ><asp:CheckBox runat='server' ID="m_UseCorporateSettings" /> Yes</td>
                            </tr>
                        </asp:PlaceHolder>
	                    <asp:PlaceHolder runat="server" ID="UsernamePanel">
	                        <tr>
	                            <td align="right" nowrap="nowrap" style="padding-right: 1em;">Username:</td>
                                <td ><asp:TextBox EnableViewState="false" Width="90%" Runat="server" ID="m_Username" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="m_Username" Display="Static">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:RequiredFieldValidator>
	                            </td>
	                        </tr>
	                    </asp:PlaceHolder>
	                    <asp:PlaceHolder runat="server" ID="PasswordPanel">
	                        <tr>
	                            <td align="right" nowrap="nowrap" style="padding-right: 1em;">Password:</td>
	                            <td ><asp:TextBox EnableViewState="false" Width="90%" Runat="server" ID="m_Password" />
	                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="m_Password" Display="Static">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:RequiredFieldValidator>
		                        </td>
	                        </tr>
		                </asp:PlaceHolder>
	                    <asp:PlaceHolder runat="server" ID="AccountIdPanel">
	                        <tr>
	                            <td align="right" nowrap="nowrap" style="padding-right: 1em;">Account ID:</td>
                                <td><asp:TextBox EnableViewState="false" Width="90%" Runat="server" ID="m_AccountId" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="m_AccountId" Display="Static">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
	                    <asp:PlaceHolder runat="server" ID="PolicyIdPanel">
	                        <tr>
	                            <td align="right" nowrap="nowrap" style="padding-right: 1em;">Branch Master Policy #</td>
                                <td><asp:TextBox EnableViewState="false" Width="90%" Runat="server" ID="m_PolicyId" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="m_PolicyId" Display="Static">
                                        <img runat="server" src="../../images/error_icon.gif">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="UnderwritingAuthorityPanel">
                            <tr>
                                <td align="right" nowrap="nowrap" style="padding-right: 1em;">Have Delegated Underwriting Authority?</td>
                                <td nowrap="nowrap" style="text-align:left">
                                <asp:RadioButton EnableViewState="false" ID="m_delegatedUA" GroupName="isUADelegated" runat="server" Text="Yes" />
                                <asp:RadioButton EnableViewState="false" ID="m_nondelegatedUA" GroupName="isUADelegated" runat="server" Text="No" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>                   
                    </tbody>
                </table>
            </ItemTemplate>
        </asp:Repeater>
        <div id="BtnHolder">
            <asp:Button ID="m_save" Text="Save" OnClick="Update_Click" runat="server" />
            <input type="button" value="Cancel" onclick="onClosePopup()"; />
        </div>
    </form>

</body>
</html>
