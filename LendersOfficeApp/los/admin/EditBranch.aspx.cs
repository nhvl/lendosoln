using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.los.admin
{
	public partial class EditBranch : LendersOffice.Common.BaseServicePage
	{
		protected BranchAdmin m_Edit;

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingBranches
                };
            }
        }

        protected override E_JqueryVersion  GetJQueryVersion()
        {
            return E_JqueryVersion._1_11_2;
        }

		private BrokerUserPrincipal BrokerUser
		{
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{
			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Bind the passed in broker and branch to our control.

			try
			{
				m_Edit.DataBroker = BrokerUser.BrokerId;
				m_Edit.DataEditor = BrokerUser.UserId;

				if( IsPostBack == false )
				{
					m_Edit.DataSource = RequestHelper.GetGuid( "branchId" , Guid.Empty );

					m_Edit.DataBind();
				}
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( ErrorMessage = "Failed to load web form." , e );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            this.EnableJquery = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}

}
