﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.los.admin
{
    public partial class BrokerGlobalWhiteListEdit : BaseServicePage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGlobalIPSettings
                };
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            int id = RequestHelper.GetInt("id", -1);
            RegisterJsGlobalVariables("Id", id);
            if (id == -1)
            {
                saveBtn.Value = "Add";
            }
            else
            {
                saveBtn.Value = "Edit";


                BrokerGlobalIpWhiteList item = BrokerGlobalIpWhiteList.RetrieveById(principal.BrokerId, id);

                description.Text = item.Description;
                ipAddress.Text = item.IpAddress;
            }

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            RegisterService("main", "/los/admin/BrokerInfoService.aspx");
        }
    }
}
