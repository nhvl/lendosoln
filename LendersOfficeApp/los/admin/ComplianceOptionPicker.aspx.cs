﻿namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.Utilities;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Conversions;
    using LendersOffice.ObjLib.Conversions.ComplianceEase;
    using LendersOffice.Security;

    /// <summary>
    /// Allows the user to pick from sets of options for a Compliance export.
    /// </summary>
    public partial class ComplianceEaseOptionPicker : BaseServicePage 
    {
        /// <summary>
        /// Gets the required permissions to view the ComplianceEase option picker.
        /// </summary>
        /// <value>The required permissions to view the ComplianceEase option picker.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Loads the ComplianceEase option picker with the specified data.
        /// </summary>
        /// <param name="sender">The sending object.</param>
        /// <param name="e">A set of event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var vendor = RequestHelper.GetSafeQueryString("vendor");
            var optionType = RequestHelper.GetSafeQueryString("type");

            if (!this.AllowAccessingOptionsForVendor(vendor, optionType))
            {
                ErrorUtilities.DisplayErrorPage(
                    new CBaseException(
                        ErrorMessages.Generic,
                        "Unable to access vendor: " + vendor + ", option type: " + optionType),
                    false,
                    Guid.Empty,
                    Guid.Empty);
            }

            switch (optionType)
            {
                case "ExcludedStatuses":
                    LoadExcludedStatuses();
                    break;
                case "Licenses":
                    LoadLicenses();
                    break;
                case "Policies":
                    LoadPolicies();
                    break; 
                case "Exemptions":
                    LoadExemptions();
                    break;
                default:
                    throw new ArgumentException($"{optionType} is not a valid option group.");
            }

            OptionPicker.DataBind();
        }

        private bool AllowAccessingOptionsForVendor(string vendor, string optionType)
        {
            var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;

            if (vendor == "complianceease")
            {
                return broker.IsEnableComplianceEaseIntegration
                    && broker.BillingVersion == E_BrokerBillingVersion.PerTransaction
                    && broker.IsEnablePTMComplianceEaseIndicator;

            }
            else if (vendor == "complianceeagle")
            {
                return broker.IsEnableComplianceEagleIntegration
                    && broker.BillingVersion == E_BrokerBillingVersion.PerTransaction
                    && broker.IsEnablePTMComplianceEagleAuditIndicator
                    && optionType == "ExcludedStatuses";
            }
            else
            {
                Tools.LogWarning("Compliance option picker got unhandled vendor: " + vendor);
                return false;
            }
        }

        /// <summary>
        /// Allows the user to pick loan statuses that should be excluded from ComplianceEase audits.
        /// </summary>
        private void LoadExcludedStatuses()
        {
            var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
            this.IsLoanStatusPicker.Checked = true;
            var vendor = RequestHelper.GetSafeQueryString("vendor");
            IEnumerable<E_sStatusT> excludedStatuses = this.GetExcludedStatusesForVendor(broker, vendor);

            // The user may have selected a new beginning audit status that has not yet been saved, so we need to use
            // the current value of the dropdown over the value stored in the configuration.
            var beginAuditStatus = (E_sStatusT)Enum.Parse(typeof(E_sStatusT), RequestHelper.GetSafeQueryString("beginAuditStatus"));
            var allAvailableAuditStatusesForBroker = ComplianceExportStatusConfig.StatusesAvailableForAudit(broker.HasEnabledInAnyChannel);

            var priorStatuses = ComplianceExportStatusConfig.PriorLoanStatuses(beginAuditStatus)
                .Intersect(allAvailableAuditStatusesForBroker);

            this.OptionPicker.AvailableSrc = allAvailableAuditStatusesForBroker
                .Except(excludedStatuses)
                .Except( new E_sStatusT[] { beginAuditStatus })
                .Except(priorStatuses)
                .Select(s => LoanStatus.GetStatusDescription(s));

            this.OptionPicker.AssignedSrc = excludedStatuses
                .Select(s => LoanStatus.GetStatusDescription(s));

            this.OptionPicker.AvailableHeader = "Audits Occur in Status";
            this.OptionPicker.AssignedHeader = "Statuses Excluded from Audit";

            this.PriorStatuses.DataSource = priorStatuses.Select(s => LoanStatus.GetStatusDescription(s));
            this.PriorStatuses.DataBind();
        }

        private IEnumerable<E_sStatusT> GetExcludedStatusesForVendor(BrokerDB broker, string vendor)
        {
            if (vendor == "complianceease")
            {
                return broker.ComplianceEaseExportConfig.ExcludedStatuses;
            }
            else if (vendor == "complianceeagle")
            {
                return broker.ComplianceEagleAuditConfiguration.ExcludedStatuses;
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Unhandled vendor: " + vendor);
            }
        }

        /// <summary>
        /// Allows the user to pick from a collection of DIDMCA State Exemptions.
        /// </summary>
        private void LoadExemptions()
        {
            this.OptionPicker.AvailableSrc = ComplianceEaseExportConfiguration.States;
            this.OptionPicker.AssignedSrc = new Dictionary<string, string>();
            this.OptionPicker.TextField = "Value";
            this.OptionPicker.ValueField = "Key";
            this.OptionPicker.AvailableHeader = "Available States";
            this.OptionPicker.AssignedHeader = "Assigned States";

            this.PriorStatusDiv.Visible = false;
        }

        /// <summary>
        /// Allows the user to pick from a collection of lending policies.
        /// </summary>
        private void LoadPolicies()
        {
            this.OptionPicker.AvailableSrc = ComplianceEaseExportConfiguration.Policies;
            this.OptionPicker.AssignedSrc = new string[] { };
            this.OptionPicker.AvailableHeader = "Available Policies";
            this.OptionPicker.AssignedHeader = "Assigned Policies";

            this.PriorStatusDiv.Visible = false;
        }

        /// <summary>
        /// Allows the user to pick from a collection of lender licenses.
        /// </summary>
        private void LoadLicenses()
        {
            this.OptionPicker.AvailableSrc = ComplianceEaseExportConfiguration.Licenses;
            this.OptionPicker.AssignedSrc = new string[] { };
            this.OptionPicker.AvailableHeader = "Available Licenses";
            this.OptionPicker.AssignedHeader = "Assigned Licenses";

            this.PriorStatusDiv.Visible = false;
        }
    }
}
