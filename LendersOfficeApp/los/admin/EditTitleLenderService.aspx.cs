﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.Security;

    /// <summary>
    /// Edits a Title service provider.
    /// </summary>
    public partial class EditTitleLenderService : LendersOffice.Common.BaseServicePage
    {
        /// <summary>
        /// Gets the permission required to load this page.
        /// </summary>
        /// <value>The permission required for page load.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Gets a value indicating whether we're creating a new service in this editor.
        /// </summary>
        /// <value>Whether we're creating a new service in this editor.</value>
        protected bool IsNewLenderService
        {
            get
            {
                return this.ServiceId < 1;
            }
        }

        /// <summary>
        /// Gets the service id passed in the query string.
        /// </summary>
        /// <value>The service id passed in the query string.</value>
        protected int ServiceId
        {
            get
            {
                return RequestHelper.GetInt("sId", 0);
            }
        }

        /// <summary>
        /// Page load function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsNewLenderService)
            {
                TitleLenderService service = TitleLenderService.GetLenderServiceById(PrincipalFactory.CurrentPrincipal.BrokerId, ServiceId);

                if (service == null)
                {
                    throw new NotFoundException("The service vendor could not be found.");
                }

                this.IsNew.Checked = service.IsNew;
                this.LenderServiceId.Value = service.LenderServiceId.ToString();
                this.PublicId.Value = service.PublicId.ToString();
                this.IsNonInteractiveQuoteEnabled.Checked = service.IsNonInteractiveQuoteEnabled;
                this.IsInteractiveQuoteEnabled.Checked = service.IsInteractiveQuoteEnabled;
                this.IsPolicyEnabled.Checked = service.IsPolicyEnabled;
                this.IsEnabledForLqbUsers.Checked = service.IsEnabledForLqbUsers;
                this.IsEnabledForTpoUsers.Checked = service.IsEnabledForTpoUsers;
                this.UsesProviderCodes.Checked = service.UsesProviderCodes;

                Tools.SetDropDownListValue(this.EnabledOCGroupId, service.EnabledOcGroupId ?? Guid.Empty);
                Tools.SetDropDownListValue(this.EnabledEmployeeGroupId, service.EnabledEmployeeGroupId ?? Guid.Empty);
                Tools.SetDropDownListValue(this.AssociatedVendorId, service.VendorId);

                InitialInfo info = new InitialInfo()
                {
                    IsQuickQuotingEnabled = service.IsNonInteractiveQuoteEnabled,
                    IsDetailedQuotingEnabled = service.IsInteractiveQuoteEnabled,
                    IsPolicyOrderingEnabled = service.IsPolicyEnabled,
                    UsesProviderCodes = service.UsesProviderCodes
                };

                this.RegisterJsObjectWithJsonNetSerializer("InitialInfo", info);
            }
            else
            {
                this.IsNew.Checked = true;
            }
        }

        /// <summary>
        /// Page init function.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterService("edit", "/los/admin/EditTitleLenderServiceService.aspx");
            this.RegisterJsScript("LQBPopup.js");

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            var employeeGroups = GroupDB.GetAllGroupIdsAndNames(principal.BrokerId, GroupType.Employee);
            this.EnabledEmployeeGroupId.Items.Add(new ListItem("Display for all employees", Guid.Empty.ToString()));
            foreach (var employeeGroup in employeeGroups)
            {
                this.EnabledEmployeeGroupId.Items.Add(new ListItem(employeeGroup.Item2, employeeGroup.Item1.ToString()));
            }

            var originatingCompGroups = GroupDB.GetAllGroupIdsAndNames(principal.BrokerId, GroupType.PmlBroker);
            this.EnabledOCGroupId.Items.Add(new ListItem("Display for all employees", Guid.Empty.ToString()));
            foreach (var originatingCompGroup in originatingCompGroups)
            {
                this.EnabledOCGroupId.Items.Add(new ListItem(originatingCompGroup.Item2, originatingCompGroup.Item1.ToString()));
            }

            this.AssociatedVendorId.Items.Add(new ListItem(string.Empty, string.Empty));
            var vendorsAvailable = TitleVendor.LoadTitleVendors_Light().Where(vendor => vendor.IsEnabled);
            foreach (var vendor in vendorsAvailable)
            {
                this.AssociatedVendorId.Items.Add(new ListItem(vendor.CompanyName, vendor.VendorId.ToString()));
            }
        }

        /// <summary>
        /// Class to hold some initial info.
        /// </summary>
        private class InitialInfo
        {
            /// <summary>
            /// Gets or sets a value indicating whether quick quoting is enabled.
            /// </summary>
            /// <value>Whether quick quoting is enabled.</value>
            public bool IsQuickQuotingEnabled
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether detailed quick quoting is enabled.
            /// </summary>
            /// <value>Whether detailed quick quoting is enabled.</value>
            public bool IsDetailedQuotingEnabled
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether policy ordering is enabled.
            /// </summary>
            /// <value>Whether policy ordering is enabled.</value>
            public bool IsPolicyOrderingEnabled
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether provider codes are used.
            /// </summary>
            /// <value>Whether provider codes are used.</value>
            public bool UsesProviderCodes
            {
                get;
                set;
            }
        }
    }
}