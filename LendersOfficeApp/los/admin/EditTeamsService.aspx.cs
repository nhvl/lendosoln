﻿// <copyright file="EditTeamsService.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: EduardoM
//    Date:   6/6/2014
// </summary>

namespace LendersOfficeApp.Los.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using LendersOffice.Common;

    /// <summary>
    /// This service does all the loading, saving, and assigning for teams.
    /// </summary>
    public partial class EditTeamsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingTeams
                };
            }
        }

        /// <summary>
        /// This determines which method to use.
        /// </summary>
        /// <param name="methodName">The name of the method to use.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadTeam":
                    this.LoadTeam();
                    break;
                case "Save":
                    this.Save();
                    break;
                case "AddEmployee":
                    this.AddEmployee();
                    break;
                case "UpdateAssignments":
                    this.UpdateAssignments();
                    break;
                case "AssignLoan":
                    this.AssignLoan();
                    break;
                case "ClearLoanAssignment":
                    this.ClearLoanAssignment();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// This clear the current team assignment for that role.
        /// </summary>
        protected void ClearLoanAssignment()
        {
            Guid roleId = GetGuid("RoleId");
            Guid loanId = GetGuid("LoanId");
            Role role = Role.Get(roleId);
            foreach (Team team in Team.ListTeamsOnLoan(loanId))
            {
                if (role.Id == team.RoleId)
                {
                    Team.RemoveLoanAssignment(PrincipalFactory.CurrentPrincipal.BrokerId, team.Id, loanId);
                }
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(EditTeamsService));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            switch (role.RoleT)
            {
                case E_RoleT.CallCenterAgent:
                    dataLoan.sTeamCallCenterAgentId = Guid.Empty;
                    break;
                case E_RoleT.Closer:
                    dataLoan.sTeamCloserId = Guid.Empty;
                    break;
                case E_RoleT.CollateralAgent:
                    dataLoan.sTeamCollateralAgentId = Guid.Empty;
                    break;
                case E_RoleT.CreditAuditor:
                    dataLoan.sTeamCreditAuditorId = Guid.Empty;
                    break;
                case E_RoleT.DisclosureDesk:
                    dataLoan.sTeamDisclosureDeskId = Guid.Empty;
                    break;
                case E_RoleT.DocDrawer:
                    dataLoan.sTeamDocDrawerId = Guid.Empty;
                    break;
                case E_RoleT.Funder:
                    dataLoan.sTeamFunderId = Guid.Empty;
                    break;
                case E_RoleT.Insuring:
                    dataLoan.sTeamInsuringId = Guid.Empty;
                    break;
                case E_RoleT.JuniorProcessor:
                    dataLoan.sTeamJuniorProcessorId = Guid.Empty;
                    break;
                case E_RoleT.LegalAuditor:
                    dataLoan.sTeamLegalAuditorId = Guid.Empty;
                    break;
                case E_RoleT.LenderAccountExecutive:
                    dataLoan.sTeamLenderAccExecId = Guid.Empty;
                    break;
                case E_RoleT.LoanOfficer:
                    dataLoan.sTeamLoanRepId = Guid.Empty;
                    break;
                case E_RoleT.LoanOfficerAssistant:
                    dataLoan.sTeamLoanOfficerAssistantId = Guid.Empty;
                    break;
                case E_RoleT.LoanOpener:
                    dataLoan.sTeamLoanOpenerId = Guid.Empty;
                    break;
                case E_RoleT.LockDesk:
                    dataLoan.sTeamLockDeskId = Guid.Empty;
                    break;
                case E_RoleT.Manager:
                    dataLoan.sTeamManagerId = Guid.Empty;
                    break;
                case E_RoleT.PostCloser:
                    dataLoan.sTeamPostCloserId = Guid.Empty;
                    break;
                case E_RoleT.Processor:
                    dataLoan.sTeamProcessorId = Guid.Empty;
                    break;
                case E_RoleT.Purchaser:
                    dataLoan.sTeamPurchaserId = Guid.Empty;
                    break;
                case E_RoleT.QCCompliance:
                    dataLoan.sTeamQCComplianceId = Guid.Empty;
                    break;
                case E_RoleT.RealEstateAgent:
                    dataLoan.sTeamRealEstateAgentId = Guid.Empty;
                    break;
                case E_RoleT.Secondary:
                    dataLoan.sTeamSecondaryId = Guid.Empty;
                    break;
                case E_RoleT.Servicing:
                    dataLoan.sTeamServicingId = Guid.Empty;
                    break;
                case E_RoleT.Shipper:
                    dataLoan.sTeamShipperId = Guid.Empty;
                    break;
                case E_RoleT.Underwriter:
                    dataLoan.sTeamUnderwriterId = Guid.Empty;
                    break;
            }

            dataLoan.Save();
        }

        /// <summary>
        /// Assigns the team to a loan.
        /// </summary>
        private void AssignLoan()
        {
            Guid teamId = GetGuid("TeamId");
            Guid loanId = GetGuid("LoanId");

            Team team = Team.LoadTeam(teamId);
            Role role = Role.Get(team.RoleId);
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(EditTeamsService));
            dataLoan.InitLoad();

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            switch (role.RoleT)
            {
                case E_RoleT.CallCenterAgent:
                    dataLoan.sTeamCallCenterAgentId = team.Id;
                    break;
                case E_RoleT.Closer:
                    dataLoan.sTeamCloserId = team.Id;
                    break;
                case E_RoleT.CollateralAgent:
                    dataLoan.sTeamCollateralAgentId = team.Id;
                    break;
                case E_RoleT.CreditAuditor:
                    dataLoan.sTeamCreditAuditorId = team.Id;
                    break;
                case E_RoleT.DisclosureDesk:
                    dataLoan.sTeamDisclosureDeskId = team.Id;
                    break;
                case E_RoleT.DocDrawer:
                    dataLoan.sTeamDocDrawerId = team.Id;
                    break;
                case E_RoleT.Funder:
                    dataLoan.sTeamFunderId = team.Id;
                    break;
                case E_RoleT.Insuring:
                    dataLoan.sTeamInsuringId = team.Id;
                    break;
                case E_RoleT.JuniorProcessor:
                    dataLoan.sTeamJuniorProcessorId = team.Id;
                    break;
                case E_RoleT.LegalAuditor:
                    dataLoan.sTeamLegalAuditorId = team.Id;
                    break;
                case E_RoleT.LenderAccountExecutive:
                    dataLoan.sTeamLenderAccExecId = team.Id;
                    break;
                case E_RoleT.LoanOfficer:
                    dataLoan.sTeamLoanRepId = team.Id;
                    break;
                case E_RoleT.LoanOfficerAssistant:
                    dataLoan.sTeamLoanOfficerAssistantId = team.Id;
                    break;
                case E_RoleT.LoanOpener:
                    dataLoan.sTeamLoanOpenerId = team.Id;
                    break;
                case E_RoleT.LockDesk:
                    dataLoan.sTeamLockDeskId = team.Id;
                    break;
                case E_RoleT.Manager:
                    dataLoan.sTeamManagerId = team.Id;
                    break;
                case E_RoleT.PostCloser:
                    dataLoan.sTeamPostCloserId = team.Id;
                    break;
                case E_RoleT.Processor:
                    dataLoan.sTeamProcessorId = team.Id;
                    break;
                case E_RoleT.Purchaser:
                    dataLoan.sTeamPurchaserId = team.Id;
                    break;
                case E_RoleT.QCCompliance:
                    dataLoan.sTeamQCComplianceId = team.Id;
                    break;
                case E_RoleT.RealEstateAgent:
                    dataLoan.sTeamRealEstateAgentId = team.Id;
                    break;
                case E_RoleT.Secondary:
                    dataLoan.sTeamSecondaryId = team.Id;
                    break;
                case E_RoleT.Servicing:
                    dataLoan.sTeamServicingId = team.Id;
                    break;
                case E_RoleT.Shipper:
                    dataLoan.sTeamShipperId = team.Id;
                    break;
                case E_RoleT.Underwriter:
                    dataLoan.sTeamUnderwriterId = team.Id;
                    break;
            }

            dataLoan.Save();
        }

        /// <summary>
        /// Updates and sets a user's assignment to a team.
        /// </summary>
        private void UpdateAssignments()
        {
            int numUpdates = GetInt("numUpdates");
            Guid employeeId = GetGuid("EmployeeId");
            Guid roleId = GetGuid("RoleId");
            bool canBeMemberOfMultipleTeams = GetBool("CanBeMemberOfMultipleTeams");
            List<Team> primaryTeam = (List<Team>)Team.ListPrimaryTeamByUserInRole(employeeId, roleId);

            EmployeeDB employee = new EmployeeDB(employeeId, PrincipalFactory.CurrentPrincipal.BrokerId);
            employee.Retrieve();
            employee.CanBeMemberOfMultipleTeams = canBeMemberOfMultipleTeams;
            employee.Save(PrincipalFactory.CurrentPrincipal);

            for (int i = 0; i < numUpdates; i++)
            {
                int j = i + 1;
                Guid teamId = GetGuid("TeamId" + j);
                bool isAssigned = GetBool("IsAssigned" + j);
                bool isPrimary = GetBool("IsPrimary" + j);

                Team.RemoveUserAssignment(PrincipalFactory.CurrentPrincipal.BrokerId, employeeId, teamId);

                if (isAssigned || isPrimary)
                {
                    Team.SetUserAssignment(PrincipalFactory.CurrentPrincipal.BrokerId, employeeId, teamId, isPrimary); 
                }
            }
        }

        /// <summary>
        /// Adds an employee to a team.
        /// </summary>
        private void AddEmployee()
        {
            Guid teamId = GetGuid("TeamId", Guid.Empty);
            Guid employeeId = GetGuid("EmployeeId", Guid.Empty);
            int roleVal = GetInt("RoleId");
            Role role = Role.Get((E_RoleT)roleVal);

            EmployeeDB emp = new EmployeeDB(employeeId, PrincipalFactory.CurrentPrincipal.BrokerId);
            emp.Retrieve();
            bool hasCheckedEmployee = GetBool("hasCheckedEmployee", false);
            bool makePrimary = GetBool("MakePrimary", false);
            bool hasPrimary = false;
            this.SetResult("EmployeeIDInCompany", emp.EmployeeIDInCompany);
            List<Team> userPrimaryTeams = (List<Team>)Team.ListPrimaryTeamByUserInRole(emp.ID, role.Id);

            if (!hasCheckedEmployee)
            {
                if (userPrimaryTeams.Count > 0)
                {
                    this.SetResult("CanBeMemberOfMultipleTeams", emp.CanBeMemberOfMultipleTeams);
                    hasPrimary = true;
                    this.SetResult("HasPrimary", true);
                    this.SetResult("PrimaryTeamName", userPrimaryTeams[0].Name);
                }
                else
                {
                    makePrimary = true;
                }
            }

            if (hasCheckedEmployee || !hasPrimary)
            {
                if (!emp.CanBeMemberOfMultipleTeams)
                {
                    foreach (Team team in Team.ListTeamsByUserInRole(employeeId, role.Id))
                    {
                        //Team.RemoveUserAssignment(employeeId, team.Id);
                    }
                }
                else
                {
                    //Team.RemoveUserAssignment(employeeId, teamId);
                }

                //Team.SetUserAssignment(employeeId, teamId, makePrimary);
                if (makePrimary && emp.CanBeMemberOfMultipleTeams)
                {
                    foreach (Team team in userPrimaryTeams)
                    {
                        //Team.UpdateUserAssignment(employeeId, team.Id, false);
                    }
                }

                BranchDB branch = new BranchDB(emp.BranchID, PrincipalFactory.CurrentPrincipal.BrokerId);
                branch.Retrieve();
                this.SetResult("branchName", branch.Name);
            }
        }

        /// <summary>
        /// Saves the changes to a team.
        /// </summary>
        private void Save()
        {
            string removedEmployeesJSON = GetString("RemovedEmployees");
            string removedTeamsJSON = GetString("RemovedTeams");
            string addedEmployeesJSON = GetString("AddedEmployees");
            bool isEditingTeam = GetBool("IsEditingTeam");

            List<Guid> removedEmployeeIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Guid>>(removedEmployeesJSON);
            List<Guid> removedTeamIds = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Guid>>(removedTeamsJSON);
            List<List<String>> addedEmployees = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<List<String>>>(addedEmployeesJSON);

            if (isEditingTeam)
            {
                Guid teamId = GetGuid("TeamId", Guid.Empty);
                Team team;
                string teamName = GetString("TeamName");
                string teamNotes = GetString("TeamNotes");
                int teamRole = GetInt("TeamRole");
                Role role = Role.Get((E_RoleT)teamRole);
                bool hasChecked = GetBool("HasChecked", false);
                bool loseLoans = false;

                if (teamId != Guid.Empty)
                {
                    team = Team.LoadTeam(teamId);

                    loseLoans = !role.Id.Equals(team.RoleId);
                    this.SetResult("LoseLoans", loseLoans);
                    
                    team.Name = teamName;
                    if (role.Id != team.RoleId)
                    {
                        team.RoleId = role.Id;
                    }

                    team.Notes = teamNotes;
                }
                else
                {
                    team = Team.CreateTeam(teamName, role.Id, teamNotes);
                }

                if (hasChecked || !loseLoans)
                {                
                    // Remove the employees first, since a Team's role cannot change while it has employees assigned to it.
                    foreach (Guid id in removedEmployeeIds)
                    {
                        Team.RemoveUserAssignment(PrincipalFactory.CurrentPrincipal.BrokerId, id, teamId);
                    }

                    team.Save();

                    teamId = team.Id;

                    this.SetResult("teamId", teamId.ToString());
                }

                
                foreach (List<String> employeeData in addedEmployees)
                {
                    bool isPrimary = true;
                    if (employeeData.Count > 1)
                    {
                        isPrimary = employeeData[1].Equals("Primary");
                    }
                    Guid employeeId = new Guid(employeeData[0]);
                    Team.RemoveUserAssignment(PrincipalFactory.CurrentPrincipal.BrokerId, employeeId, team.Id);
                    Team.SetUserAssignment(PrincipalFactory.CurrentPrincipal.BrokerId, employeeId, team.Id, isPrimary);
                }
            }

            foreach (Guid id in removedTeamIds)
            {
                Team.DeleteTeam(id);
            }
        }

        /// <summary>
        /// Loads the team in the editor.
        /// </summary>
        private void LoadTeam()
        {
            //// I will need team Id.
            Guid teamId = GetGuid("teamId");
            Team team = Team.LoadTeam(teamId);

            Role role = Role.Get(team.RoleId);
            E_RoleT roleT = role.RoleT;
            this.SetResult("teamName", team.Name);
            this.SetResult("teamNotes", team.Notes);
            this.SetResult("roleT", (int)roleT);

            List<Team.TeamUser> users = (List<Team.TeamUser>)Team.ListUsersInTeam(teamId);
            for (int i = 0; i < users.Count; i++)
            {
                this.SetResult("employeeId" + i, users[i].EmployeeId);
                this.SetResult("employeeName" + i, users[i].FirstName + " " + users[i].LastName);
                EmployeeDB employee = new EmployeeDB(users[i].EmployeeId, PrincipalFactory.CurrentPrincipal.BrokerId);
                employee.Retrieve();
                BranchDB branch = new BranchDB(employee.BranchID, PrincipalFactory.CurrentPrincipal.BrokerId);
                branch.Retrieve();
                this.SetResult("branchName" + i, branch.Name);

                this.SetResult("EmployeeIDInCompany" + i, employee.EmployeeIDInCompany);

                EmployeeRoles employeeRole = new EmployeeRoles(PrincipalFactory.CurrentPrincipal.BrokerId, employee.ID);

                this.SetResult("HasRole" + i, employeeRole.IsInRole(role.Id));
            }

            this.SetResult("numberOfEmployees", users.Count);
        }
    }
}