﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewOriginatorCompensationAuditEvent.aspx.cs" Inherits="LendersOfficeApp.los.admin.ViewOriginatorCompensationAuditEvent" %>
<%@ Register TagPrefix="uc" TagName="AuditEventCtrl" Src="ViewOriginatorCompensationAuditEventCtrl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Audit history</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet" />
</head>
<body onload="resizeForIE6And7(270, 300);">
    <form id="form1" runat="server">
	    <uc:AuditEventCtrl id="m_Ctrl" runat="server"></uc:AuditEventCtrl>
    </form>
</body>
</html>
