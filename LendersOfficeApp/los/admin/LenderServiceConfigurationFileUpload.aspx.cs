﻿#region Auto-generated code
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// A page for uploading, validating, and downloading lender service configuration files.
    /// </summary>
    public partial class LenderServiceConfigurationFileUpload : BaseServicePage
    {
        /// <summary>
        /// Gets the required permissions for loading the page.
        /// </summary>
        /// <value>The required permissions for loading the page.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Handles the Page object's <c>Init</c> event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.UploadBtn.Click += UploadBtn_Click;
        }

        /// <summary>
        /// Handles the Page object's Load event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (RequestHelper.GetBool("downloadTemplate"))
            {
                LocalFilePath templateFile = ErnstConfigurationParser.WriteBlankConfigurationToTempFile();
                RequestHelper.SendFileToClient(this.Context, templateFile.Value, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Ernst_Configuration_Template.xlsx");
                this.Context.Response.SuppressContent = true;
                this.Context.ApplicationInstance.CompleteRequest();
                return;
            }

            int serviceId = RequestHelper.GetInt("sId", 0);

            var service = TitleLenderService.GetLenderServiceById(PrincipalFactory.CurrentPrincipal.BrokerId, serviceId);
            ErnstConfiguration configuration = ErnstConfiguration.LoadSavedConfiguration(service?.ConfigurationFileDbKey);
            if (configuration != null && RequestHelper.GetBool("download"))
            {
                string fileName = "Ernst_Configuration_" + configuration.UploadDate.ToString("yyyyMMddHHmm") + ".xlsx";

                configuration.UseSourceFile(fileInfo => RequestHelper.SendFileToClient(this.Context, fileInfo.FullName, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName));
                this.Context.Response.SuppressContent = true;
                this.Context.ApplicationInstance.CompleteRequest();
                return;
            }

            this.RegisterJsStruct("noConfiguration", configuration == null);
        }

        /// <summary>
        /// Handles the <c>UploadBtn</c> object's Click event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An object that contains no event data.</param>
        private void UploadBtn_Click(object sender, EventArgs e)
        {
            LocalFilePath sourceFilePath = LocalFilePath.Create(this.FileUpload.PostedFile.FileName).ForceValue();
            LocalFilePath tempFilePath = LqbGrammar.Utils.TempFileUtils.NewTempFilePath();
            this.FileUpload.PostedFile.SaveAs(tempFilePath.Value);

            this.Message.Controls.Clear();
            Result<ErnstConfiguration> result = ErnstConfigurationParser.UploadFile(tempFilePath, sourceFilePath);
            if (result.HasError && result.Error is ValidationException)
            {
                ValidationErrorContext context = ((ValidationException)result.Error).Context;
                var errorMessage = new HtmlGenericControl("p");
                errorMessage.InnerText = "One or more validation error(s) occurred:";
                var validationErrorList = new HtmlGenericControl("ul");
                foreach (ValidationErrorMessage validationErrorMessage in context.ValidationErrors)
                {
                    var listItem = new HtmlGenericControl("li");
                    listItem.InnerText = validationErrorMessage.Message;
                    validationErrorList.Controls.Add(listItem);
                }

                this.Message.Controls.Add(errorMessage);
                this.Message.Controls.Add(validationErrorList);
                this.Message.Style.Add("color", "red");
            }
            else if (result.HasError)
            {
                throw result.Error;
            }
            else
            {
                string autoExpiredKey = DataAccess.AutoExpiredTextCache.InsertUserString(LendersOffice.Security.PrincipalFactory.CurrentPrincipal, result.Value.FileKey.ToString(), TimeSpan.FromHours(20));
                this.RegisterJsStruct("uploadedFileKeyTempKey", autoExpiredKey);
                this.Message.InnerText = "Uploaded Successfully";
                this.Message.Style.Add("color", "green");
            }
        }
    }
}