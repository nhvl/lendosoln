﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReLockFeeTable.ascx.cs" Inherits="LendersOfficeApp.los.admin.ReLockFeeTable" %>
<head>
    <script type="text/javascript">
        if (!this.RELOCKFEES) {
            this.RELOCKFEES = ({});

            RELOCKFEES.fn = new Object();
            RELOCKFEES.List = [];
            RELOCKFEES.Index = 0; // largest used ID + 1
            RELOCKFEES.Edit = []; // list of IDs for rows in EDIT mode
            RELOCKFEES.Deleted = new Object(); // list of IDs of all deleted rows

            RELOCKFEES.fn.addRow = function() {
                var oTbody = document.getElementById("m_ReLockFeeTableBody");
                var nRowCount = oTbody.rows.length;
                var oRow = RELOCKFEES.fn.__CreateRow(oTbody, '', '');
                if (typeof updateDirtyBit === 'function') {
                    updateDirtyBit();
                }
                updateTableSize_RL();
            }
            RELOCKFEES.fn.modifyRow = function(id) {
                var link1 = document.getElementById('sModifyLink_RL' + id);
                var link2 = document.getElementById('sUpdateLink_RL' + id);
                if (hasDisabledAttr(link1))
                    return;
                for (i = 0; i < RELOCKFEES.Edit.length; i++) {
                    if (RELOCKFEES.Edit[i] == id) {
                        //edit mode cancel

                        RELOCKFEES.Edit.splice(i, 1); //remove from edit set

                        var sDays = document.getElementById('sDays_RL' + id);
                        var sDays_edit = document.getElementById('sDays_RL' + id + "_edit");
                        var sFee = document.getElementById('sFee_RL' + id);
                        var sFee_edit = document.getElementById('sFee_RL' + id + "_edit");
                        
                        //cancelling new row, so skip to delete
                        if (sDays.innerText == '' && sFee.innerText == '')
                            break;

                        sDays.style.display = "inline";
                        sDays_edit.style.display = "none";
                        sDays_edit.value = sDays.innerText;

                        sFee.style.display = "inline";
                        sFee_edit.style.display = "none";
                        sFee_edit.value = sFee.innerText;

                        link1.innerText = 'delete';
                        link2.innerText = 'edit';

                        //Turn off error messages
                        document.getElementById('m_daysError_RL' + id).style.display = 'none';
                        document.getElementById('m_feeError_RL' + id).style.display = 'none';

                        return;
                    }
                }
                //row wasn't in edit mode; link = delete
                var oRow = document.getElementById('row_RL' + id);
                var iRowIndex = oRow.rowIndex;
                var oTbody = document.getElementById('m_ReLockFeeTableBody');
                oTbody.deleteRow(iRowIndex - 1);
                if (typeof updateDirtyBit === 'function') {
                    updateDirtyBit();
                }
                RELOCKFEES.Deleted[id] = true;
                organizeTable_RL();
                updateTableSize_RL();
            }

            RELOCKFEES.fn.updateRow = function(id) {

                var link1 = document.getElementById('sModifyLink_RL' + id);
                var link2 = document.getElementById('sUpdateLink_RL' + id);
                if (hasDisabledAttr(link2))
                    return;
                var sDays = document.getElementById('sDays_RL' + id);
                var sDays_edit = document.getElementById('sDays_RL' + id + "_edit");
                var sFee = document.getElementById('sFee_RL' + id);
                var sFee_edit = document.getElementById('sFee_RL' + id + "_edit");
                for (i = 0; i < RELOCKFEES.Edit.length; i++) {
                    if (RELOCKFEES.Edit[i] == id) {
                        //edit mode finish
                        if (validateRow_RL(id)) {
                            sDays.style.display = "inline";
                            sDays_edit.style.display = "none";
                            sDays.innerText = sDays_edit.value;

                            sFee.style.display = "inline";
                            sFee_edit.style.display = "none";
                            sFee.innerText = sFee_edit.value;

                            RELOCKFEES.Edit.splice(i, 1); //remove from edit set

                            if (typeof updateDirtyBit === 'function') {
                                updateDirtyBit();
                            }

                            link1.innerText = 'delete';
                            link2.innerText = 'edit';
                        }
                        organizeTable_RL();
                        return;
                    }
                }
                //row wasn't in edit mode; enter edit mode
                sDays.style.display = "none";
                sDays_edit.style.display = "inline";

                sFee.style.display = "none";
                sFee_edit.style.display = "inline";

                RELOCKFEES.Edit.push(id);

                link1.innerText = 'cancel';
                link2.innerText = 'update';
            }


            RELOCKFEES.fn.initTable = function(jsondata) {
                RELOCKFEES.fn.clearTable();
                try {
                    RELOCKFEES.List = JSON.parse(jsondata);

                    var oTbody = document.getElementById("m_ReLockFeeTableBody");
                    var nRowCount = RELOCKFEES.List.length;
                    for (var i = 0; i < nRowCount; i++) {
                        var oEntry = RELOCKFEES.List[i];
                        RELOCKFEES.fn.__CreateRow(oTbody, oEntry[0], oEntry[1]);
                    }
                }
                catch (e) {  } //jsondata was empty, avoid parse error
            }

            RELOCKFEES.fn.clearTable = function() {
                var oTbody = document.getElementById("m_ReLockFeeTableBody");
                var oHidden = document.getElementById('m_ReLockFeeTableList');
                try {
                    if (oHidden != null) {
                        oHidden.value = '';
                    }
                    var nRowCount = oTbody.rows.length;
                    for (var i = 0; i < nRowCount; i++) {
                        oTbody.deleteRow(0);
                    }
                    RELOCKFEES.Index = 0;
                } catch (e) { }
            }

            RELOCKFEES.fn.__CreateRow = function(oParent, sDays, sFee) {
                var oRow = RELOCKFEES.fn.__CreateAndAppend("tr", oParent);
                var oCell = null;
                var o;
                var id = RELOCKFEES.Index++;
                oRow.id = 'row_RL' + id;
                var isNew = sDays == '' && sFee == '';
                if (isNew)
                    RELOCKFEES.Edit.push(id); //automatically enter edit mode for new rows
                oRow.className = ((oParent.rows.length % 2) == 0) ? "GridAlternatingItem" : "GridItem";

                //Re-Lock Period (days)
                oCell = RELOCKFEES.fn.__CreateAndAppend("td", oRow);
                o = RELOCKFEES.fn.__CreateAndAppend("label", oCell); //for displaying range info
                o = RELOCKFEES.fn.__CreateAndAppend("label", oCell);
                o.id = 'sDays_RL' + id;
                o.innerText = sDays;
                if (isNew) o.style.display = "none";
                o = RELOCKFEES.fn.__CreateAndAppend("input", oCell);
                o.id = 'sDays_RL' + id + "_edit";
                o.className = "ReLockFeeTable";
                o.type = 'text';
                o.style.width = '50px';
                o.maxLength = '5';
                o.value = sDays;
                addEvent(o, 'blur', function() {
                    return validateDays_RL(id);
                });
                if (!isNew) o.style.display = "none";
                o = RELOCKFEES.fn.__CreateAndAppend("img", oCell);
                o.id = 'm_daysError_RL' + id;
                o.src = '../../images/error_icon.gif';
                o.className = 'ReLockFeeError';
                o.style.display = 'none';

                //Fee (Points)
                oCell = RELOCKFEES.fn.__CreateAndAppend("td", oRow);
                o = RELOCKFEES.fn.__CreateAndAppend("label", oCell);
                o.id = 'sFee_RL' + id;
                o.innerText = sFee;
                if (isNew) o.style.display = "none";
                o = RELOCKFEES.fn.__CreateAndAppend("input", oCell);
                o.id = 'sFee_RL' + id + "_edit";
                o.className = "ReLockFeeTable";
                o.type = 'text';
                o.style.width = '50px';
                o.maxLength = '9';
                o.value = sFee;
                addEvent(o, 'blur', function() {
                    return validateFee_RL(id);
                });
                if (!isNew) o.style.display = "none";
                o = RELOCKFEES.fn.__CreateAndAppend("img", oCell);
                o.id = 'm_feeError_RL' + id;
                o.src = '../../images/error_icon.gif';
                o.className = 'ReLockFeeError';
                o.style.display = 'none';

                //cancel/delete link
                oCell = RELOCKFEES.fn.__CreateAndAppend("td", oRow);
                o = RELOCKFEES.fn.__CreateAndAppend("a", oCell);
                o.id = 'sModifyLink_RL' + id;
                o.className = "ReLockFeeTable";
                o.href = 'javascript:void(0);';
                if (isNew)
                    o.innerText = 'cancel';
                else
                    o.innerText = 'delete';
                addEvent(o, 'click', function() {
                    return RELOCKFEES.fn.modifyRow(id);
                });

                //update/edit link
                oCell = RELOCKFEES.fn.__CreateAndAppend("td", oRow);
                o = RELOCKFEES.fn.__CreateAndAppend("a", oCell);
                o.id = 'sUpdateLink_RL' + id;
                o.className = "ReLockFeeTable";
                o.href = 'javascript:void(0);';
                if (isNew)
                    o.innerText = 'update';
                else
                    o.innerText = 'edit';
                addEvent(o, 'click', function() {
                    return RELOCKFEES.fn.updateRow(id);
                });
                return oRow;
            }

            RELOCKFEES.fn.__CreateAndAppend = function(sTag, oParent) {
                var oItem = document.createElement(sTag);
                oParent.appendChild(oItem);
                return oItem;
            }

            RELOCKFEES.fn.saveTable = function() {
                if (RELOCKFEES.Edit.length > 0) {
                    alert("Please finish editing the Re-Lock Fee table before saving.");
                    return false;
                }
                var oTbody = document.getElementById("m_ReLockFeeTableBody");
                var oHidden = document.getElementById('m_ReLockFeeTableList');

                if (oTbody.rows.length < 1) {
                    alert("Auto Re-Locks requires at least one fee option.")
                    m_reLockMinRowsError.style.display = 'inline';
                    return false;
                }
                var lastDays = 0;
                for (var i = 0; i < oTbody.rows.length; i++) {
                    var sDays = oTbody.rows[i].childNodes[0].childNodes[1];
                    var daysValue = parseInt(sDays.innerText);
                    if (daysValue <= lastDays) {
                        alert("Re-Lock Fee Table contains an invalid range of days.");
                        return false;
                    }
                    lastDays = daysValue;
                }
                try {
                    if (oHidden != null)
                        oHidden.value = JSON.stringify(RELOCKFEES.fn.__GetTableValues());
                } catch (e) { alert("Error saving re-lock fee table."); return false; }
                return true;
            }

            RELOCKFEES.fn.__GetTableValues = function() {
                var iCurrentIndex = 0;
                var oResults = [];
                for (var i = 0; i < RELOCKFEES.Index; i++) {
                    if (RELOCKFEES.Deleted[i])
                        continue;
                    var sDays = document.getElementById('sDays_RL' + i);
                    var sFee = document.getElementById('sFee_RL' + i);

                    if (null == sDays || null == sFee) {
                        continue;
                    }
                    if (sDays.innerText == '' && sFee.innerText == '') {
                        continue;
                    }
                    oResults.push([sDays.innerText, sFee.innerText]);
                }
                return oResults;
            }
        }
        function addEvent(obj, evType, fn, useCapture) {
                addEventHandler(obj, evType, fn, useCapture);
                return true;
        }
        
        function updateTableSize_RL() {
            var oTbody = document.getElementById("m_ReLockFeeTableBody");
            document.getElementById('m_addRow_RL').className = ((oTbody.rows.length % 2) == 0) ? "GridItem" : "GridAlternatingItem";
            if(oTbody.rows.length>0)
                m_reLockMinRowsError.style.display = 'none';
            else
                m_reLockMinRowsError.style.display = 'inline';
        }

        function validateRow_RL(id) {
            var valid = validateDays_RL(id);
            valid &= validateFee_RL(id);
            return valid;
        }
        function validateDays_RL(id) {
            var sDays_edit = document.getElementById('sDays_RL' + id + "_edit");
            document.getElementById('m_daysError_RL' + id).style.display = 'none';
            var days = parseInt(sDays_edit.value);
            if (isNaN(days) || days <= 0) {
                document.getElementById('m_daysError_RL' + id).style.display = 'inline';
                return false;
            }
            sDays_edit.value = days;
            return true;
        }
        function validateFee_RL(id) {
            var sFee_edit = document.getElementById('sFee_RL' + id + "_edit");
            document.getElementById('m_feeError_RL' + id).style.display = 'none';
            formatDecimal(sFee_edit, 3);
            if (isNaN(sFee_edit.value) || sFee_edit.value < 0) {
                document.getElementById('m_feeError_RL' + id).style.display = 'inline';
                return false;
            }
            return true;
        }
        function organizeTable_RL() {
        
            var oTbody = document.getElementById("m_ReLockFeeTableBody");
            var rows = [];
            var nRows = oTbody.rows.length;
            for (i = 0; i < nRows; i++) {
                rows.push(oTbody.rows[i]);
            }
            
            //Sort rows by days (empty, new rows pushed to end)
            rows.sort(function(a, b) {
                if (a.childNodes[0].childNodes[1].innerText == '') {
                    if (b.childNodes[0].childNodes[1].innerText == '')
                        return 0;
                    return 1;
                }
                if (b.childNodes[0].childNodes[1].innerText == '')
                    return -1;

                var aVal = parseInt(a.childNodes[0].childNodes[1].innerText);
                var bVal = parseInt(b.childNodes[0].childNodes[1].innerText);
                return aVal - bVal;
            });
            while (oTbody.hasChildNodes())
                oTbody.removeChild(oTbody.firstChild);
                
            //Display table cell as "[prevDays+1] to [days]"
            var prevEnd = -1;
            for (i = 0; i < nRows; i++) {
                var row = rows[i];
                if (row.childNodes[0].childNodes[1].innerText == '')
                    row.childNodes[0].childNodes[0].innerText = '';
                else
                    row.childNodes[0].childNodes[0].innerText = (prevEnd + 1) + ' to ';
                prevEnd = parseInt(row.childNodes[0].childNodes[1].innerText);
                row.className = ((i % 2) == 0) ? "GridItem" : "GridAlternatingItem";
                oTbody.appendChild(row);
            }
        }
        
    </script>
</head>
<div>
    <span id="m_reLockMinRowsError" class="ReLockFeeError" style="color:Red; display:none">Auto Re-Locks requires at least one fee option.<br /></span>
    <div style="padding-right: 4px; height: 150px; overflow-y:scroll;">
        <table class="DataGrid ReLockFeeTable" border="1" cellpadding="2" cellspacing="0" style="border-collapse: collapse;">
            <tr class="GridHeader" style="border: solid 1px white">
                <td style="width:200px">
                    Re-Lock Period (days)
                </td>
                <td style="width:200px">
                    Fee (points)
                </td>
                <td style="width:75px"></td>
                <td style="width:75px"></td>
            </tr>
            <tbody id='m_ReLockFeeTableBody'></tbody>
            <tfoot>
                <tr id="m_addRow_RL">
                    <td></td>
                    <td></td>
                    <td><a href="javascript:void(0);" class="ReLockFeeTable" onclick="if(!this.disabled && !hasDisabledAttr(this))RELOCKFEES.fn.addRow();">add</a></td>
                    <td></td>
                </tr>
                <tr class="GridHeader">
                    <td colspan="4">&nbsp;</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>