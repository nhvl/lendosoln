﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployeeTeams.ascx.cs" Inherits="LendersOfficeApp.Los.Admin.EmployeeTeams" %>

<style>
    .teamTD{ padding:0px, 10px;}
    .teamTD p {margin: 5px;}
    .nonPrimary{display:none;}

    .invalidAssignment
    {
        color:#b83131;
    }
    
    #TeamTable
    {
        border-collapse:collapse;
    }
    
    .headerRow
    {
        border-bottom-color:Black;
        border-bottom-style:solid;
        border-bottom-width:3px;
    }
    
        
        .show{display:'';}
        .hide{display:none;}    
</style>
<div>
    <p id="NewUserMsg" runat="server" style="margin-top:20px; display:none;">User must be saved before teams may be assigned.</p>
    <table runat="server" id="TeamTable">
        <tr class="headerRow"  >
            <th>Role</th>
            <th></th>
            <th>Assigned Teams</th>
            <th></th>
        </tr>
    </table>
    <asp:HiddenField runat="server" ID="TeamData" />
    <asp:CheckBox runat="server" ID="CanBeMemberOfMultipleTeams" style="display:none;" />
</div>


<script>
    var data = new Object();
    var CanBeMemberOfMultipleTeams = "";

    $("input[id*='m_Edit_m_EmployeeRoles_']").click(function() {
        var id = $(this).attr("id");
        var index = id.replace("m_Edit_m_EmployeeRoles_", "");
        var cell = $("[idVal='" + index + "']");
        var row = cell.parent();
        var show = this.checked;

        if (show) {
            row.css("display", "");
        }
        else {
            if (cell.attr("class").indexOf("RoleLabel") < 0) {
                row.css("display", "none");
            }
        }
    });
    
    $(".show, .hide").click(function() {
        var $this = $(this);
        var $cell = $this.parent();

        var $currentShow = $cell.find(".show");
        var $currentHide = $cell.find(".hide");

        $currentShow.removeAttr("class");
        $currentHide.removeAttr("class");

        $currentShow.attr("class", "hide");
        $currentHide.attr("class", "show");

        var $nonPrimary = $cell.parent().next();

        if ($this.attr("alt") == "Expand") {
            $nonPrimary.removeAttr("class");
        }
        else {
            $nonPrimary.attr("class", "nonPrimary");
        }
    });
    $(".invalidTeamLink").click(function() {
        alert("The user does not have the role for this team.");
    });

    $(".TeamPickerLink").click(function() {
        showModal($(this).attr("data-href") + "&CanBeMemberOfMultipleTeams=" + CanBeMemberOfMultipleTeams, data[$(this).attr("roleIdVal")], "dialogWidth:400px; dialogHeight:300px; resizable:yes;", null, function(ret){
        if (ret != null) {
            
            data[ret["RoleId"]] = ret;
            CanBeMemberOfMultipleTeams = ret["CanBeMemberOfMultipleTeams"];


            var row = $(this).parent().parent();
            if (ret["numUpdates"] > 0) {
                row.find(".teamTD").attr("class", "teamTD RoleLabel");
                updateDirtyBit();


                var primaryText = "";
                var hasSecondary = false;
                var numUpdates = ret["numUpdates"];
                for (var i = 0; i < numUpdates; i++) {
                    var j = i + 1;
                    if (ret["IsAssigned" + j] == "true" || ret["IsPrimary" + j] == "true") {
                        if (ret["IsPrimary" + j] == "true") {
                            primaryText = ret["TeamName" + j] + "(primary)";
                        }
                        else {
                            var p = document.createElement("p");
                            $(p).text(ret["TeamName" + j]);
                            row.next().find("span").append($(p));
                            hasSecondary = true;
                        }
                    }
                }

                row.find("span").text(primaryText);
                if (hasSecondary) {
                    row.find("img").each(function() { $(this).css("display", ""); });
                    row.next().css("display", "");
                }
                else {
                    row.find("img").each(function() { $(this).css("display", "none"); });
                    row.next().css("display", "none");
                }
            }
            //update the displayed roles
        }
            
        }, { hideCloseButton: true, context: this });
        return false;
    });

    function saveTeamData() {
        $("[id*='TeamData']").val(JSON.stringify(data));
        $("[id*='CanBeMemberOfMultipleTeams']")[0].checked = CanBeMemberOfMultipleTeams == "true";
    }
</script>
