<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="HolidayLockDeskClosuresNew.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.HolidayLockDeskClosuresNew" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Holiday Closures</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<SCRIPT type="text/javascript">		
		function onNewHolidayClosure()
		{
			if(<%=AspxTools.JsString(ViewState["IsDisabled"].ToString())%> == 'True')
				return;
	
			onRunDialog('/los/admin/EditRateExpirationNew.aspx?cmd=add&timezone=' + '<%=AspxTools.HtmlString(TimezoneSetting)%>' + "&policyid=" + <%=AspxTools.JsString(PolicyId.ToString()) %>);
		}
		
		function onNewHolidayClosureByHoliday()
		{	
			if(<%=AspxTools.JsString(ViewState["IsDisabled"].ToString())%> == 'True')
				return;
				
			onRunDialog('/los/admin/HolidayListNew.aspx?policyid=' + <%=AspxTools.JsString(PolicyId.ToString()) %>);
		}
				
		function onEditHolidayClosure(closureDate)
		{
			if(<%=AspxTools.JsString(ViewState["IsDisabled"].ToString())%> == 'True')
				return;
				
			onRunDialog('/los/admin/EditRateExpirationNew.aspx?cmd=edit&closureDate=' + closureDate + '&timezone=' + <%=AspxTools.JsString(TimezoneSetting)%> + "&policyid=" + <%=AspxTools.JsString(PolicyId.ToString()) %>);
		}
		
		function onChangeTimezone(timezone, isDisabled)
		{
			isDisabled = 'false';
			self.location = "HolidayLockDeskClosuresNew.aspx?timezone=" + encodeURIComponent(timezone) + "&disabled=" + isDisabled + "&policyid=" + <%=AspxTools.JsString(PolicyId.ToString()) %>;
		}
		
		function onSetDisabled(timezone, isDisabled)
		{
			isDisabled = 'false';
			self.location = "HolidayLockDeskClosuresNew.aspx?timezone=" + encodeURIComponent(timezone) + "&disabled=" + isDisabled + "&policyid=" + <%=AspxTools.JsString(PolicyId.ToString()) %>;
		}
		
		function onRunDialog( url )
		{
			showModal(url, null, null, null, function(arg){
				if (arg.OK) 
				{
					document.getElementById(<%=AspxTools.JsString(m_btnRefresh.ClientID)%>).click();
				}
			},{hideCloseButton:true});
		}
		</SCRIPT>
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="HolidayLockDeskClosuresNew" method="post" runat="server">
			<table>
				<tr style="FONT-WEIGHT: bold; FONT-SIZE: 11px">
					<asp:Panel ID="m_ButtonPanel" Runat="server">
						Add new Closure: 
						<INPUT id="m_newHolidayClosure" onclick="onNewHolidayClosure();" type="button" value="By Date"> 
						<INPUT id="m_newHolidayClosureByHoliday" onclick="onNewHolidayClosureByHoliday();" type="button" value="By Holiday"> 
						<asp:button id="m_btnRefresh" runat="server" Text="Refresh" onclick="m_btnRefresh_Click"></asp:button>
						<asp:Button ID="ShowPastBtn" runat="server" Text="Show Past Closures" OnClick="TogglePastClosuresBtnClick" />
					</asp:Panel></TD>
				</tr>
				<tr>
					<td>
						<DIV style="OVERFLOW-Y:scroll; HEIGHT:170px">
							<ML:CommonDataGrid id="m_Grid" runat="server" width="100%" cellpadding="2" OnItemCommand="m_Grid_ItemCommand" OnItemDataBound="m_Grid_ItemDataBound">
								<Columns>
									<asp:BoundColumn DataField="ClosureDate" HeaderText=" Closure Date"></asp:BoundColumn>
									<asp:BoundColumn DataField="IsClosedAllDay" HeaderText="Closed for<br/>Locks?"></asp:BoundColumn>
									<asp:BoundColumn DataField="IsClosedForBusiness" HeaderText="Closed for<br/>Business?"></asp:BoundColumn>
									<asp:BoundColumn DataField="LockDeskOpenTime" HeaderText=" Lock Desk<br/>Open Time"></asp:BoundColumn>
									<asp:BoundColumn DataField="LockDeskCloseTime" HeaderText=" Lock Desk<br/>Close Time"></asp:BoundColumn>
									<asp:ButtonColumn Text="delete" CommandName="DropHolidayClosure"></asp:ButtonColumn>
									<asp:TemplateColumn>
										<ItemTemplate>
											<A href="#" onclick="onEditHolidayClosure(<%#AspxTools.JsString(DataBinder.Eval(Container.DataItem, "ClosureDate").ToString())%>); return false;">
												edit </A>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</ML:CommonDataGrid>
							<ASP:Panel id="m_EmptyMessage" runat="server" Width="100%" Visible="False" EnableViewState="False">
								<DIV style="PADDING-RIGHT: 40px; PADDING-LEFT: 40px; FONT: 11px arial; COLOR: dimgray; PADDING-TOP: 40px; TEXT-ALIGN: center">No 
									holiday closures to display.
								</DIV>
							</ASP:Panel>
							<BR>
						</DIV>
					</td>
				</tr>
			</table>
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
		</form>
	</body>
</HTML>
