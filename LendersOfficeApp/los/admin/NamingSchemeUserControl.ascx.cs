using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using System.Collections.Generic;
using DataAccess;
using System.Data.SqlClient;
using LendersOffice.Security;

namespace LendersOfficeApp.los.admin
{

	public partial  class NamingSchemeUserControl : System.Web.UI.UserControl
	{
        protected System.Web.UI.WebControls.RegularExpressionValidator RegularExpressionValidator1;

		protected void ControlLoad( object sender, System.EventArgs e )
		{
            var basePage = Page as BasePage;
            if (basePage == null) return;

            DataSet ds = new DataSet();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerUserPrincipal.CurrentPrincipal.BrokerId)
                                        };

            DataSetHelper.Fill(ds, BrokerUserPrincipal.CurrentPrincipal.BrokerId, "ListBranchByBrokerID", parameters);
            var branchDT = ds.Tables[0];

            List<string> branches = new List<string>();

            foreach (DataRow dr in branchDT.Rows)
            {
                branches.Add(dr["BranchLNmPrefix"].ToString());
            }

            basePage.EnableJqueryMigrate = false;
            basePage.RegisterJsScript("json.js");
            basePage.RegisterJsStruct<List<string>>("BranchPrefixes", branches);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.ControlLoad);

        }
		#endregion

	}

}
