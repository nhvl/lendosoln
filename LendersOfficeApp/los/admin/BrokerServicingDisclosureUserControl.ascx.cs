using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.common;
using LendersOffice.Common;
namespace LendersOfficeApp.los.admin
{


	/// <summary>
	///		Summary description for BrokerServicingDisclosureUserControl.
	/// </summary>
	public partial  class BrokerServicingDisclosureUserControl : System.Web.UI.UserControl, IAutoLoadUserControl
	{

        public void LoadData() 
        {
        }
        public void SaveData() 
        {
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
