<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="ARMIndexEntries.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.ARMIndexEntries" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
	<title>ARM Index Entries</title>
	<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
	<meta name="CODE_LANGUAGE" Content="C#">
	<meta name="vs_defaultClientScript" content="JavaScript">
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</HEAD>
<body MS_POSITIONING="FlowLayout" scroll="no" onload="onInit();">
	<script>
		function onInit() { try
		{
		    var feedBack = document.getElementById("m_feedBack");
		    var errorMessage = document.getElementById("m_errorMessage");
		    var commandToDo = document.getElementById("m_commandToDo");

			if( feedBack != null )
			{
				alert( feedBack.value );
			}

			if( errorMessage != null )
			{
				alert( errorMessage.value );
			}
			else if( commandToDo != null )
			{
				if( commandToDo.value == "Close" )
				{
					onClosePopup();
				}
			}

			<% if( IsPostBack == false ) { %>

			resize( 680 , 550 );

			<% } %>
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function markAsDirty()
		{
			document.getElementById("m_IsDirty").value = "Dirty";
		}

		function onClose()
		{
			if( document.getElementById("m_IsDirty").value == "Dirty" )
			{
				if( confirm( "Do you want to close without saving?" ) == false )
				{
					return;
				}
			}

			onClosePopup();
		}
	</script>
		<h4 class="page-header">ARM Index Entries</h4>
		<FORM id="ARMIndexEntries" method="post" runat="server">
			<INPUT type="hidden" id="m_IsDirty" runat="server" NAME="m_IsDirty">
			<TABLE width="100%" border="0" cellspacing="2" cellpadding="3">
			<TR>
			<TD>
				<DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 160px">
				<table width="100%">
				<tr><td class="FieldLabel">LQB System Indexes</td></tr>
						<asp:Repeater ID="m_systemIndexes" Runat="server" EnableViewState="true">
						<HeaderTemplate>
						<tr class="GridHeader">
						<td>Name</td>
						<td>Value</td>
						<td>Last Updated</td>
						</tr>
						</HeaderTemplate>
							<ItemTemplate>
								<tr class="GridItem">
									<td><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IndexNameVstr"))%></td>
									<td><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IndexCurrentValueDecimal", "{0}%"))%></td>
									<td><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EffectiveD", "{0:f}"))%></td>
								</tr>
							</ItemTemplate>
							<AlternatingItemTemplate>
								<tr class="GridAlternatingItem">
									<td><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IndexNameVstr"))%></td>
									<td><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "IndexCurrentValueDecimal", "{0}%"))%></td>
									<td><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EffectiveD", "{0:f}"))%></td>
								</tr>
							</AlternatingItemTemplate>
						</asp:Repeater>
    				</table>
				</DIV>
				<HR>
                 <span class="FieldLabel">Your Indexes</span>
				<DIV style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 250px">
					<ML:CommonDataGrid id="m_Grid" runat="server" cellpadding="2" width="100%" OnItemCommand="GridItemClick" OnItemDataBound="GridItemBound">
						<Columns>
							<asp:TemplateColumn HeaderText="Named As">
								<ItemTemplate>
									<div>
										<asp:TextBox id="NamedAs" runat="server" style="FONT: 12px arial; PADDING-LEFT: 4px;" Width="220px" onchange="markAsDirty();">
										</asp:TextBox>
										<a href="#" style="MARGIN-LEFT: 16px; DISPLAY: inline;" onclick="style.display = 'none'; var sibling = nextSibling; if(sibling.nodeType !== 1){sibling = sibling.nextSibling;} sibling.style.display = 'inline'; sibling = parentElement.nextSibling; if(sibling.nodeType !== 1){sibling = sibling.nextSibling;} sibling.style.display = 'block';">
											(more details)
										</a>
										<a href="#" style="MARGIN-LEFT: 16px; DISPLAY: none;" onclick="style.display = 'none'; var sibling = previousSibling; if(sibling.nodeType !== 1){sibling = sibling.previousSibling;}  sibling.style.display = 'inline'; sibling = parentElement.nextSibling; if(sibling.nodeType !== 1){sibling = sibling.nextSibling;} sibling.style.display = 'none';">
											(hide details)
										</a>
									</div>
									<div style="DISPLAY: none;">
										<div style="PADDING: 0px;">
											<table cellpadding="0" cellspacing="10" border="0">
											<tr>
											<td class="FieldLabel" nowrap>
												Fannie Mae ARM Index
											</td>
											<td>
												<asp:DropDownList id="FannieT" runat="server" style="FONT: 12px arial; PADDING-LEFT: 4px;" Width="280px" onchange="markAsDirty();">
													<asp:ListItem Value="0">
														Leave Blank
													</asp:ListItem>
													<asp:ListItem Value="1">
														Weekly Avg CMT
													</asp:ListItem>
													<asp:ListItem Value="2">
														Monthly Avg CMT
													</asp:ListItem>
													<asp:ListItem Value="3">
														Weekly Avg TAAI
													</asp:ListItem>
													<asp:ListItem Value="4">
														Weekly Avg TAABD
													</asp:ListItem>
													<asp:ListItem Value="5">
														Weekly Avg SMTI
													</asp:ListItem>
													<asp:ListItem Value="6">
														Daily CD Rate
													</asp:ListItem>
													<asp:ListItem Value="7">
														Weekly Avg CD Rate
													</asp:ListItem>
													<asp:ListItem Value="8">
														Weekly Avg Prime Rate
													</asp:ListItem>
													<asp:ListItem Value="9">
														TBill Daily Value
													</asp:ListItem>
													<asp:ListItem Value="10">
														Eleventh District COF
													</asp:ListItem>
													<asp:ListItem Value="11">
														National Monthly Median Cost Of Funds
													</asp:ListItem>
													<asp:ListItem Value="12">
														Wall Street Journal LIBOR
													</asp:ListItem>
													<asp:ListItem Value="13">
														Fannie Mae LIBOR
													</asp:ListItem>
												</asp:DropDownList>
											</td>
											</tr>
											<tr>
											<td class="FieldLabel" nowrap>
												Freddie Mac ARM Index
											</td>
											<td>
												<asp:DropDownList id="FreddeT" runat="server" style="FONT: 12px arial; PADDING-LEFT: 4px;" Width="280px" onchange="markAsDirty();">
													<asp:ListItem Value="0">
														Leave Blank
													</asp:ListItem>
													<asp:ListItem Value="1">
														One Year Treasury
													</asp:ListItem>
													<asp:ListItem Value="2">
														Three Year Treasury
													</asp:ListItem>
													<asp:ListItem Value="3">
														Six Month Treasury
													</asp:ListItem>
													<asp:ListItem Value="4">
														Eleventh District Cost Of Funds
													</asp:ListItem>
													<asp:ListItem Value="5">
														National Monthly Median Cost Of Funds
													</asp:ListItem>
													<asp:ListItem Value="6">
														LIBOR
													</asp:ListItem>
													<asp:ListItem Value="7">
														Other
													</asp:ListItem>
												</asp:DropDownList>
											</td>
											</tr>
											<tr>
											<td class="FieldLabel" nowrap>
												Effective date
											</td>
											<td>
												<ml:DateTextBox id="EffectD" runat="server" style="FONT: 12px arial; PADDING-LEFT: 4px;" preset="date" helperalign="absmiddle" Width="100px">
												</ml:DateTextBox>
											</td>
											</tr>
											<tr>
											<td class="FieldLabel" nowrap>
												Based on
											</td>
											<td>
												<asp:TextBox id="BasedOn" runat="server" style="FONT: 12px arial; PADDING-LEFT: 4px;" Wrap="True" Width="280px" TextMode="MultiLine" Rows="3" onchange="markAsDirty();">
												</asp:TextBox>
											</td>
											</tr>
											<tr>
											<td class="FieldLabel" nowrap>
												Found at
											</td>
											<td>
												<asp:TextBox id="FoundAt" runat="server" style="FONT: 12px arial; PADDING-LEFT: 4px;" Wrap="True" Width="280px" TextMode="MultiLine" Rows="3" onchange="markAsDirty();">
												</asp:TextBox>
											</td>
											</tr>
											</table>
										</div>
									</div>
									<ML:EncodedLabel id="Id" runat="server" style="DISPLAY: none;"></ML:EncodedLabel>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Current Value" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="120px">
								<ItemTemplate>
									<ml:PercentTextBox id="CurrVal" runat="server" style="FONT: 12px arial; TEXT-ALIGN: right; PADDING-RIGHT: 4px;" preset="percent" Width="90px" onchange="markAsDirty();">
									</ml:PercentTextBox>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="70px">
								<ItemTemplate>
									<asp:LinkButton runat="server" CommandName="Remove">
										remove
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
					</ML:CommonDataGrid>
					<ASP:Panel id="m_EmptyMessage" runat="server" Width="100%" Visible="False" EnableViewState="False">
      <DIV
      style="PADDING-RIGHT: 40px; PADDING-LEFT: 40px; PADDING-BOTTOM: 40px; FONT: 11px arial; COLOR: dimgray; PADDING-TOP: 40px; TEXT-ALIGN: center">No
      indexes to display. </DIV>
					</ASP:Panel>
				</DIV>
				<HR>
								<TABLE cellpadding="0" cellspacing="0" width="100%">
				<TR>
				<TD align="left" width="1%" nowrap>
					<ASP:Button id="m_Add" runat="server" Text="Add" OnClick="AddClick" Width="62">
					</ASP:Button>
				</TD>
				</TR>
				</TABLE>
				<hr />
				<DIV style="WIDTH: 100%; TEXT-ALIGN: right">
					<ASP:Button id="m_Ok" runat="server" Text="OK" Width="50px" OnClick="OkClick">
					</ASP:Button>
					<INPUT type="button" value="Cancel" width="50px" onclick="onClose();">
					<ASP:Button id="m_Apply" runat="server" Text="Apply" Width="50px" OnClick="ApplyClick">
					</ASP:Button>
				</DIV>
			</TD>
			</TR>
			</TABLE>
			<ML:CModalDlg id="m_ModalDlg" runat="server">
			</ML:CModalDlg>
		</FORM>
	</body>
</HTML>
