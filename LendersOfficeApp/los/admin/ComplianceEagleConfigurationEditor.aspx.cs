﻿#region Generated Code -- A lie to dupe the merciless StyleCop.
namespace LendersOfficeApp.los.admin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using DataAccess.Utilities;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Conversions.ComplianceEagleIntegration;
    using LendersOffice.ObjLib.Conversions;
    using LendersOffice.Security;

    /// <summary>
    /// ComplianceEagle configuration editor.
    /// </summary>
    public partial class ComplianceEagleConfigurationEditor : BasePage
    {
        /// <summary>
        /// The broker whose config is being edited.
        /// </summary>
        private BrokerDB broker = PrincipalFactory.CurrentPrincipal.BrokerDB;

        /// <summary>
        /// Gets the permissions that are required to load the page.
        /// </summary>
        /// <value>The permissions that are required to load the page.</value>
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }

        /// <summary>
        /// Gets a value indicating whether the async CEagle export is enabled for
        /// this lender.
        /// </summary>
        /// <value>True if the async audit is enabled for the broker. Otherwise, false.</value>
        protected bool IsEnabledForBroker
        {
            get
            {
                return this.broker.IsEnableComplianceEagleIntegration
                    && this.broker.BillingVersion == E_BrokerBillingVersion.PerTransaction
                    && this.broker.IsEnablePTMComplianceEagleAuditIndicator;
            }
        }

        /// <summary>
        /// Page load handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterJsScript("LQBPopup.js");
            
            if (!this.IsEnabledForBroker)
            {
                var exception = new CBaseException(
                    ErrorMessages.GenericAccessDenied,
                    "Attempting to access broker ComplianceEagle configuration without necessary permissions");

                ErrorUtilities.DisplayErrorPage(
                    exception,
                    false,
                    Guid.Empty,
                    Guid.Empty);
            }

            if (!this.IsPostBack)
            {
                var config = this.broker.ComplianceEagleAuditConfiguration;

                this.BindBeginAuditStatus(BeginAuditStatus, config);
                this.BeginAuditStatus.Value = config.BeginRunningAuditsStatus.ToString();

                var excludedStatuses = config.ExcludedStatuses.Select(s => LoanStatus.GetStatusDescription(s));
                this.ExcludedStatuses.DataSource = excludedStatuses;
                this.ExcludedStatuses.DataBind();
            }
        }

        /// <summary>
        /// Adds status options to beginning status dropdown.
        /// </summary>
        /// <param name="ddl">The dropdown.</param>
        /// <param name="config">The CEagle config.</param>
        protected void BindBeginAuditStatus(HtmlSelect ddl, ComplianceEagleAuditConfiguration config)
        {
            List<object> elems = new List<object>();
            var potentialBeginAuditStatuses = ComplianceExportStatusConfig.StatusesAvailableForAudit(this.broker.HasEnabledInAnyChannel)
                .Where(s => !Tools.IsInactiveStatus(s));

            foreach (var status in potentialBeginAuditStatuses)
            {
                elems.Add(new { Text = CPageBase.sStatusT_map_rep(status), Value = status.ToString() });
            }

            var currentBeginAuditStatus = config.BeginRunningAuditsStatus;
            if (!potentialBeginAuditStatuses.Contains(currentBeginAuditStatus))
            {
                // If the currently selected option is not valid, we still want to display it so 
                // the lender is aware what is configured and can change it.
                elems.Add(new { Text = CPageBase.sStatusT_map_rep(currentBeginAuditStatus), Value = currentBeginAuditStatus.ToString() });
            }

            ddl.DataSource = elems;
            ddl.DataTextField = "Text";
            ddl.DataValueField = "Value";
            ddl.DataBind();
        }

        /// <summary>
        /// Update click handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The event args.</param>
        protected void Update_Click(object sender, EventArgs args)
        {
            var currentConfig = this.broker.ComplianceEagleAuditConfiguration;

            var beginRunningAuditsStatus = (E_sStatusT)Enum.Parse(typeof(E_sStatusT), this.BeginAuditStatus.Value);
            var excludedStatuses = GetStatusValues(ExcludedStatusesSubmit) ?? currentConfig.ExcludedStatuses;

            this.broker.ComplianceEagleAuditConfiguration = new ComplianceEagleAuditConfiguration(beginRunningAuditsStatus, excludedStatuses);
            this.broker.Save();

            ClientScript.RegisterStartupScript(this.GetType(), "Autoclose", "onClosePopup()", true);
        }

        /// <summary>
        /// Code that runs on init.
        /// </summary>
        /// <param name="e">EventArgs object.</param>
        protected override void OnInit(EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            base.OnInit(e);
        }

        /// <summary>
        /// Retrieves status values from a hidden input.
        /// </summary>
        /// <param name="input">The hidden input.</param>
        /// <returns>The statuses.</returns>
        private List<E_sStatusT> GetStatusValues(HtmlInputHidden input)
        {
            List<string> statuses = this.GetValues(input);

            if (statuses == null)
            {
                return null;
            }

            return statuses.Select(s => LoanStatus.GetStatusFromDescription(s)).OfType<E_sStatusT>().ToList();
        }

        /// <summary>
        /// Gets string values from hidden input. Expects pipe-delimited values.
        /// </summary>
        /// <param name="e">The hidden input.</param>
        /// <returns>The string values.</returns>
        private List<string> GetValues(HtmlInputHidden e)
        {
            if (string.IsNullOrEmpty(e.Value.TrimWhitespaceAndBOM()))
            {
                return new List<string>();
            }

            if (e.Value.TrimWhitespaceAndBOM().Equals("null", StringComparison.InvariantCultureIgnoreCase))
            {
                return null;
            }

            if (e.Value.Contains('|'))
            {
                return e.Value.Split('|').ToList();
            }
            else if (!string.IsNullOrEmpty(e.Value))
            {
                return new[] { e.Value }.ToList();
            }

            Tools.LogBug("In compliance eagle config setup, user managed to select an invalid list of things. Value is: [" + e.Value + "]");
            return null;
        }
    }
}