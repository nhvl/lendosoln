﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditTitleLenderService.aspx.cs" Inherits="LendersOfficeApp.los.admin.EditTitleLenderService" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Title Service</title>
    <style type="text/css">
        .Center
        {
            text-align: center;
        }
        .LabelColumn
        {
            width: 40%;
        }
        .Hidden
        {
            display: none;
        }
        .InputText
        {
            width: 250px;
        }
        .PaddingBottom {
            padding-bottom: 10px;
        }
        .PopupHeight {
            height: 350px;
        }
        #ButtonDiv {
            position: absolute;
            bottom: 0;
            left: 48%;
            margin-left: -25px;
        }
        .UndecoratedList
        {
            list-style-type: none;
            padding-left: 0;
        }
        .Indent30Px{
            padding-left: 30px;
        }
        .FullWidth {
            width: 100%;
        }
        .Buttons {
            width: 50px;
            padding-left: 0px;
            padding-right: 0px;
        }
    </style>
</head>
<body>
    <script type="text/javascript">
        jQuery(function ($) {
            $('#IsEnabledForLqbUsers').change(function () {
                $('#EnabledEmployeeGroupId').prop('disabled', !this.checked);
                if (this.checked) {
                    $('#EnabledEmployeeGroupId').find('option:first').prop('selected', true);
                }
            });

            $('#IsEnabledForTpoUsers').change(function () {
                $('#EnabledOCGroupId').prop('disabled', !this.checked);
                if (this.checked) {
                    $('#EnabledOCGroupId').find('option:first').prop('selected', true);
                }
            });

            $('#CancelBtn').click(function () {
                if (configFileKeyTempKey) {
                    gService.edit.callAsyncSimple("DeleteConfigurationFile", {"ConfigurationFileKeyTempKey": configFileKeyTempKey});
                }
                parent.LQBPopup.Return(null);
            });

            $('#OkBtn').click(function () {
                var service = {
                    IsNew: $('#IsNew').is(':checked'),
                    LenderServiceId: $('#LenderServiceId').val(),
                    PublicId: $('#PublicId').val(),
                    ConfigurationFileKeyTempKey: configFileKeyTempKey,
                    AssociatedVendorId: $('#AssociatedVendorId').val(),
                    IsNonInteractiveQuoteEnabled: $('#IsNonInteractiveQuoteEnabled').is(':checked'),
                    IsInteractiveQuoteEnabled: $('#IsInteractiveQuoteEnabled').is(':checked'),
                    IsPolicyEnabled: $('#IsPolicyEnabled').is(':checked'),
                    IsEnabledForLqbUsers: $('#IsEnabledForLqbUsers').is(':checked'),
                    EnabledEmployeeGroupId: $('#EnabledEmployeeGroupId').val(),
                    IsEnabledForTpoUsers: $('#IsEnabledForTpoUsers').is(':checked'),
                    EnabledOCGroupId: $('#EnabledOCGroupId').val(),
                    UsesProviderCodes: $('#UsesProviderCodes').is(':checked')
                };

                if (!Verify(service)) {
                    return;
                }

                var data = {
                    LenderServiceViewModel: JSON.stringify(service)
                };

                gService.edit.callAsyncSimple('SaveTitleLenderService', data, function(result) {
                    if (!result.error) {
                        if (result.value["Success"].toLowerCase() === 'true') {
                            var returnArgs = {
                                IsNew: result.value["IsNew"],
                                ServiceId: result.value["ServiceId"],
                                ServiceTypeName: "Title",
                                DisplayName: result.value["VendorAsString"],
                                VendorName: result.value["VendorAsString"],
                                ResellerName: '',
                                Type: 1
                            };

                            parent.LQBPopup.Return(returnArgs);
                        }
                        else {
                            alert(result.value["Error"]);
                        }
                    }
                    else {
                        alert(result.UserMessage);
                    }
                });                
            });

            $('#AssociatedVendorId').change(function () {
                if ($(this).val() === '') {
                    $('#ServiceRow').hide();
                    $('#UsesProviderCodesRow').hide();
                    HideByVendorSettings(false, false, false, false, false);
                    ToggleConfigurationLink();
                    return;
                }

                var data = {
                    AssociatedVendorId: $(this).val()
                };

                gService.edit.callAsyncSimple("GetTitleVendorInfo", data, function(result) {
                    if (!result.error) {
                        if (result.value["Success"].toLowerCase() === 'true') {
                            var quickQuotingEnabled = result.value["IsQuickQuotingEnabled"].toLowerCase() === 'true';
                            var detailedQuotingEnabled = result.value["IsDetailedQuotingEnabled"].toLowerCase() === 'true';
                            var policyOrderingEnabled = result.value["IsPolicyOrderingEnabled"].toLowerCase() === 'true';
                            var usesProviderCodes = result.value["UsesProviderCodes"].toLowerCase() === 'true';
                        
                            $('#ServiceRow').show();
                            $('#UsesProviderCodesRow').show();
                            HideByVendorSettings(quickQuotingEnabled, detailedQuotingEnabled, policyOrderingEnabled, usesProviderCodes);
                            ToggleConfigurationLink();
                        }
                        else {
                            alert(result.value["Error"]);
                        }
                    }
                    else {
                        alert(result.UserMessage);
                    }
                });                
            });

            $('#IsNonInteractiveQuoteEnabled, #IsInteractiveQuoteEnabled').change(function () {
                ToggleConfigurationLink();
            });

            $('#ConfigurationFileLink').click(function () {
                LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/los/admin/LenderServiceConfigurationFileUpload.aspx") + (!this.IsNewLenderService ? "?sId=" + this.ServiceId : null)) %>, {
                    hideCloseButton: true,
                    popupClasses: 'Popup',
                    width: 450,
                    height: 375,
                    onReturn: OnConfigurationFileUploadReturn
                }, null);
            });

            var configFileKeyTempKey;
            function OnConfigurationFileUploadReturn(returnArgs) {
                if (returnArgs.FileKeyTempKey) {
                    configFileKeyTempKey = returnArgs.FileKeyTempKey;
                    $('#ConfigurationFileStatus').text('(file pending save)');
                }
            }

            function Verify(lenderService) {
                if (lenderService.AssociatedVendorId === '') {
                    alert("Please select a Vendor.");
                    return false;
                }

                return true;
            }

            function HideByVendorSettings(quickQuoteEnabled, detailedQuoteEnabled, policyOrderingEnabled, providerCodesUsed) {
                $('#IsNonInteractiveQuoteEnabledItem').toggle(quickQuoteEnabled);
                if (!quickQuoteEnabled && $('#IsNonInteractiveQuoteEnabled').is(':checked')) {
                    $('#IsNonInteractiveQuoteEnabled').prop('checked', false);
                }

                $('#IsInteractiveQuoteEnabledItem').toggle(detailedQuoteEnabled);
                if (!detailedQuoteEnabled && $('#IsInteractiveQuoteEnabled').is(':checked')) {
                    $('#IsInteractiveQuoteEnabled').prop('checked', false);
                }

                $('#IsPolicyEnabledItem').toggle(policyOrderingEnabled);
                if (!policyOrderingEnabled && $('#IsPolicyEnabled').is(':checked')) {
                    $('#IsPolicyEnabled').prop('checked', false);
                }

                
                $('#UsesProviderCodesRow').toggle(providerCodesUsed);
                if (!providerCodesUsed && $('#UsesProviderCodes').is(':checked')) {
                    $('#UsesProviderCodes').prop('checked', false);
                }
            }

            function ToggleConfigurationLink() {
                var quickQuoteEnabled = $('#IsNonInteractiveQuoteEnabled').is(':checked');
                var detailedQuoteEnabled = $('#IsInteractiveQuoteEnabled').is(':checked');
                $('#ConfigurationFileRow').toggle(quickQuoteEnabled || detailedQuoteEnabled);
            }

            $('#EnabledOCGroupId').prop('disabled', !$('#IsEnabledForTpoUsers').is(':checked'));
            $('#EnabledEmployeeGroupId').prop('disabled', !$('#IsEnabledForLqbUsers').is(':checked'));

            var hasInitial = typeof (InitialInfo) !== 'undefined';
            $('#ServiceRow').toggle(hasInitial);
            $('#ConfigurationFileRow').toggle(hasInitial);
            $('#UsesProviderCodesRow').toggle(hasInitial);
            if (typeof (InitialInfo) !== 'undefined') {
                HideByVendorSettings(InitialInfo.IsQuickQuotingEnabled, InitialInfo.IsDetailedQuotingEnabled, InitialInfo.IsPolicyOrderingEnabled, InitialInfo.UsesProviderCodes);
                ToggleConfigurationLink();
            }

            setTimeout(function() { // wait until the service page loads
                $('#AssociatedVendorId').change();
            })
        });
    </script>
    <h4 class="page-header">Edit Title Service</h4>
    <form id="form1" runat="server" class="PopupHeight">
    <div>
        <div>
            <input type="checkbox" runat="server" id="IsNew" disabled="disabled" class="Hidden" />
            <input type="hidden" runat="server" id="LenderServiceId" disabled="disabled" />
            <input type="hidden" runat="server" id="PublicId" disabled="disabled" />
            <table class="FullWidth">
                <tr>
                    <td class="FieldLabel LabelColumn">Vendor</td>
                    <td>
                        <asp:DropDownList runat="server" ID="AssociatedVendorId"></asp:DropDownList>
                    </td>
                </tr>
                <tr id="ServiceRow">
                    <td colspan="2">
                        <div class="FieldLabel">
                            Which services will this vendor be used for?
                        </div>
                        <div class="Indent30Px">
                            <ul class="UndecoratedList">
                                <li id="IsNonInteractiveQuoteEnabledItem">
                                    <label>
                                        <input type="checkbox" id="IsNonInteractiveQuoteEnabled" runat="server" />
                                        Non-interactive quote within PML
                                    </label>
                                </li>
                                <li id="IsInteractiveQuoteEnabledItem">
                                    <label>
                                        <input type="checkbox" id="IsInteractiveQuoteEnabled" runat="server" />
                                        Interactive quote outside of PML
                                    </label>
                                </li>
                                <li id="IsPolicyEnabledItem">
                                    <label>
                                        <input type="checkbox" id="IsPolicyEnabled" runat="server" />
                                        Ordering Policies
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr id="ConfigurationFileRow">
                    <td colspan="2">
                        <a id="ConfigurationFileLink">Configure default values for Quotes</a>
                        <span id="ConfigurationFileStatus"></span>
                    </td>
                </tr>
                <tr id="UsesProviderCodesRow">
                    <td colspan="2">
                        <label class="FieldLabel">
                            <input type="checkbox" runat="server" id="UsesProviderCodes" />
                            Uses Provider Codes?
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="FieldLabel">
                        <label>
                            <input type="checkbox" id="IsEnabledForLqbUsers" runat="server"/>Enable this service for LQB users?
                        </label>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">Only display to Employee Group:</td>
                    <td>
                        <asp:DropDownList NotEditable="true" runat="server" ID="EnabledEmployeeGroupId"></asp:DropDownList>
                    </td>
                </tr>
                <tr class="Hidden">
                    <td colspan="2" class="FieldLabel">
                        <label>
                            <input type="checkbox" id="IsEnabledForTpoUsers" runat="server" />Enable this service for Originator Portal users?
                        </label>
                    </td>
                </tr>
                <tr class="Hidden">
                    <td class="FieldLabel">Only display to OC Group:</td>
                    <td>
                        <asp:DropDownList NotEditable="true" runat="server" ID="EnabledOCGroupId"></asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <div id="ButtonDiv" class="Center align-bottom PaddingBottom">
                <input type="button" class="Buttons Center" value="OK" id="OkBtn"/>
                <input type="button" class="Buttons Center" value="Cancel" id="CancelBtn"/>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
