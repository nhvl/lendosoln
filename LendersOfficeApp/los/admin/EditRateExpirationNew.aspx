<%@ Page language="c#" Codebehind="EditRateExpirationNew.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.EditRateExpirationNew" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>EditRateExpirationNew</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </HEAD>
 <body onload="init();" MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto">
 <script type="text/javascript">
		function init()
		{
			resize( 540 , 340 );
			onClosedAllDayClick();
			ColorReadOnly();
		}
		
		function ColorReadOnly()
		{
			var closureDate = document.getElementById(<%=AspxTools.JsString(m_ClosureDate.ClientID)%>);
			if(closureDate && closureDate.readOnly == true)
				closureDate.style.color = "gray";
			else
				closureDate.style.color = "black";
		}
		
		function onClosedAllDayClick()
		{
			var startHour = document.getElementById(<%=AspxTools.JsString(m_StartTime.ClientID)%>);
			var startMinute = document.getElementById(<%=AspxTools.JsString(m_StartTime.ClientID)%> + '_minute');
			var startAM = document.getElementById(<%=AspxTools.JsString(m_StartTime.ClientID)%> + '_am');
			
			var endHour = document.getElementById(<%=AspxTools.JsString(m_EndTime.ClientID)%>);
			var endMinute = document.getElementById(<%=AspxTools.JsString(m_EndTime.ClientID)%> + '_minute');
			var endAM = document.getElementById(<%=AspxTools.JsString(m_EndTime.ClientID)%> + '_am');
			
			if(document.getElementById(<%=AspxTools.JsString(m_ClosedAllDay.ClientID)%> + '_0').checked == true)
			{
				startHour.disabled = true;
				startMinute.disabled = true;
				startAM.disabled = true;
				
				endHour.disabled = true;
				endMinute.disabled = true;
				endAM.disabled = true;
			}
			else
			{
				startHour.disabled = false;
				startMinute.disabled = false;
				startAM.disabled = false;
				
				endHour.disabled = false;
				endMinute.disabled = false;
				endAM.disabled = false;
			}
		}
		</script>
	
    <form id="EditRateExpirationNew" method="post" runat="server">
		<TABLE id="Table1" cellSpacing="1" cellPadding="5" border="0" width="100%" height=200>
			<tr valign=top>
				<td class=FormTableHeader noWrap height=0% colspan=2 style="HEIGHT: 25px">Edit Holiday Closure Information </td>
			</tr>
			<tr valign=top height="100%">
				<td width=163 style="WIDTH: 210px; HEIGHT: 17.97% ">
					Closure Date:
				</td>
				<td style="HEIGHT: 17.97%">
					<ml:DateTextBox id="m_ClosureDate" runat=server preset="date" width=75></ml:DateTextBox>
					<asp:requiredfieldvalidator id="m_ClosureDateValidator" runat="server" ErrorMessage="This field is required." Display="Dynamic" ControlToValidate="m_ClosureDate"></asp:requiredfieldvalidator>
				</td>
			</tr>
			<tr valign=top>
				<td width=163 style="WIDTH: 210px; HEIGHT: 26px">
					Closed for Business?
				</td>
				<td style="HEIGHT: 26px">
					<asp:RadioButtonList ID="m_ClosedForBusiness" Runat=server RepeatDirection=Horizontal >
						<asp:ListItem Value="Y">Yes</asp:ListItem>
						<asp:ListItem Value="N">No</asp:ListItem>
					</asp:RadioButtonList>
				</td>
			</tr>
			<tr valign=top>
				<td width=163 style="WIDTH: 210px; HEIGHT: 26px">
					Closed for Locks?
				</td>
				<td style="HEIGHT: 26px">
					<asp:RadioButtonList ID="m_ClosedAllDay" Runat=server RepeatDirection=Horizontal onclick="onClosedAllDayClick();">
						<asp:ListItem Value="Y">Yes</asp:ListItem>
						<asp:ListItem Value="N">No</asp:ListItem>
					</asp:RadioButtonList>
				</td>
			</tr>
			<tr valign=top id="timeRange">
				<td width=163 style="WIDTH: 210px; HEIGHT: 30px">
					Lock desk open between the hours of: 
				</td>
				<td style="HEIGHT: 30px" >
					<ml:TimeTextBox id="m_StartTime" runat=server></ml:TimeTextBox> and 
					<ml:TimeTextBox id="m_EndTime" runat=server></ml:TimeTextBox>
					&nbsp;<ml:EncodedLabel ID="m_timezone" Runat=server></ml:EncodedLabel>
				</td>
			</tr>
			<tr>
				<td>
				</td>
			</tr>
			</TABLE>
			<HR style="MARGIN-LEFT: 4px; MARGIN-RIGHT: 4px">
			
			<DIV style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-TOP: 4px" align=center><SPAN style="MARGIN-RIGHT: 8px">
					<asp:button id="m_OK" onclick="OKClick" runat="server" Text="  OK  "></asp:button>
					<INPUT onclick="onClosePopup();" type="button" value="Cancel"> 
					<br><ml:EncodedLabel id="m_ErrorMessage" runat="server" ForeColor="Red" Font-Size="12px"></ml:EncodedLabel></SPAN>
			</DIV>
<uc1:cmodaldlg id="Cmodaldlg1" runat="server"></uc1:cmodaldlg>
     </form>
	
  </body>
</HTML>
