﻿<%@ Control Language="c#" AutoEventWireup="false" Codebehind="NotificationReplyConfig.ascx.cs" Inherits="LendersOfficeApp.los.admin.NotificationReplyConfig" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<script type="text/javascript">
// Note - the reason these functions seem unnecessarily complicated in terms of checking to make sure the fields 
// are not null and using a precise prefix is because this control is used twice in the same page, with 2 separate
// name prefixes.  Using document.getElementById for some reason only grabs the child controls from one of the parent
// controls, instead of getting the correct one that is currently being clicked on.  Therefore, clicking a radio 
// button on one will affect the UI of the other, instead of only affecting the UI of the radio button that was clicked.
	function onInitNotifReplyConfig(prefix)
	{
		var copyTB = document.getElementById(prefix + '_m_UseCopying');
		var ccTo = document.getElementById(prefix + '_m_CarbonCopyTo');
		if(ccTo && copyTB)
			ccTo.readOnly = !copyTB.checked;
		
		var replyCB = document.getElementById(prefix + '_m_AllowReply');
		
		var rolesPanel = document.getElementById(prefix + '_m_RolesPanel');
		if (rolesPanel && replyCB) {
		    setDisabledAttr($(rolesPanel).find('input'), !replyCB.checked);
		    setDisabledAttr($(rolesPanel).find('a'), !replyCB.checked);
		}			
		
		var addrNoneVisible = document.getElementById(prefix + '_m_AddrNoneVisible');
		var addrNone = document.getElementById(prefix + '_m_AddrNone');
		var defaultReply = document.getElementById(prefix + '_m_DefaultReply');
		var defaultLabel = document.getElementById(prefix + '_m_DefaultLabel');
		
		if(replyCB && replyCB.checked == true)
		{
			if(defaultReply)
				defaultReply.readOnly = false;
			if(defaultLabel)
				defaultLabel.readOnly = false;
		}
		else
		{
			if(defaultReply)
				defaultReply.readOnly = true;
			if(defaultLabel)
				defaultLabel.readOnly = true;
		}
		
		if(addrNoneVisible.value == "1" && replyCB && replyCB.checked == true)
		{
			if(addrNone)
				addrNone.style.display = "";
		}
		else
		{
			if(addrNone)
				addrNone.style.display = "none";
		}
	}
	
	function onCopyClick(tb)
	{
		var index = tb.id.indexOf('m_UseCopying');
		if(index < 0)
			index = 0;
		var prefix = tb.id.substring(0,index);
		var ccTo = document.getElementById(prefix + 'm_CarbonCopyTo');
		if (ccTo) {
		    ccTo.readOnly = !tb.checked;
		    ccTo.style.background = tb.checked ? 'white' : 'lightgray';
		}
	}
	
	function onAllowClick(tb)
	{
		var index = tb.id.indexOf('m_AllowReply');
		if(index < 0)
			index = 0;
		
		var prefix = tb.id.substring(0,index);
		var addrNone = document.getElementById(prefix + 'm_AddrNone');
		var addrNoneVisible = document.getElementById(prefix + 'm_AddrNoneVisible');
		var rolesPanel = document.getElementById(prefix + 'm_RolesPanel');
		var defaultReply = document.getElementById(prefix + 'm_DefaultReply');
		var defaultLabel = document.getElementById(prefix + 'm_DefaultLabel');
			
		if(tb.checked)
		{
			if(addrNone)
			{
				if(addrNoneVisible && addrNoneVisible.value == "1")
					addrNone.style.display = "";
				else
					addrNone.style.display = "none";
			}
			
			if (rolesPanel) {
			    setDisabledAttr($(rolesPanel).find('input'), false);
			    setDisabledAttr($(rolesPanel).find('a'), false);
			}				
			
			if(defaultReply)
				defaultReply.readOnly = false;
			 
			if(defaultLabel)
				defaultLabel.readOnly = false;
		}
		else
		{	
			if(addrNone)
				addrNone.style.display = "none";
			
			if (rolesPanel) {
			    setDisabledAttr($(rolesPanel).find('input'), true);
			    setDisabledAttr($(rolesPanel).find('a'), true);
			}
			
			if(defaultReply)
				defaultReply.readOnly = true;
			 
			if(defaultLabel)
				defaultLabel.readOnly = true;
		}
	}

	$(document).ready(function () {
	    addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(RegularExpressionValidator1) %>);
	});
</script>

<table cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top">
			<asp:CheckBox id="m_UseCopying" runat="server" Checked="True" onclick="onCopyClick(this);"></asp:CheckBox>
		</td>
		<td width="4">
		</td>
		<td>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<div style="MARGIN-TOP: 3px; MARGIN-BOTTOM: 3px; WIDTH: 150px">
							Copy all notifications to
						</div>
					</td>
					<td>
						<asp:TextBox id="m_CarbonCopyTo" runat="server" style="PADDING-LEFT: 4px" Width="160"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td align="right" colspan="2"><asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ControlToValidate="m_CarbonCopyTo" Display="Dynamic" Font-Bold="True"></asp:RegularExpressionValidator></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div style="PADDING-RIGHT: 2px;PADDING-LEFT: 2px;PADDING-BOTTOM: 2px;PADDING-TOP: 2px">
</div>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top">
			<asp:CheckBox id="m_AllowReply" runat="server" onclick="onAllowClick(this);"></asp:CheckBox>
		</td>
		<td width="4">
		</td>
		<td>
			<div style="MARGIN-TOP: 3px; MARGIN-BOTTOM: 3px">
				Set from address for <ml:EncodedLiteral ID="EvenTypeLiteral" runat="server" /> Events
			</div>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top">
		</td>
		<td width="4">
		</td>
		<td>
			<asp:Panel id="m_ReplyPanel" runat="server">
      <DIV style="MARGIN-TOP: 3px; PADDING-LEFT: 30px; MARGIN-BOTTOM: 3px; WIDTH: 300px">Include from roles in order of preference 
		<SPAN style="COLOR: dimgray">(we use the address of the <I>first</I> assigned employee we find according to this role search order) </SPAN>
      </DIV>
      <asp:Panel id=m_RolesPanel style="PADDING-LEFT: 30px" runat="server" Width="333">
		<DIV style="BORDER-RIGHT: lightgrey 2px solid; BORDER-TOP: lightgrey 2px solid; MARGIN-TOP: 4px; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; MARGIN-BOTTOM: 4px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 100%; BORDER-BOTTOM: lightgrey 2px solid; HEIGHT: 95px; BACKGROUND-COLOR: whitesmoke">
		<TABLE id=m_AddrTable cellSpacing=2 cellPadding=0 width="100%"><asp:Repeater id=m_AddrList runat="server" OnItemCommand="AddrListClick" OnItemDataBound="AddrListBound">
								<ItemTemplate>
									<tr>
										<td width="1px">
											<asp:LinkButton runat="server" CommandName="Up" ID="Linkbutton1">▲</asp:LinkButton>
										</td>
										<td width="1px">
											<asp:LinkButton runat="server" CommandName="Dn" ID="Linkbutton2">▼</asp:LinkButton>
										</td>
										<td width="50px" align="right">
											<ml:EncodedLabel id="Order" runat="server"></ml:EncodedLabel>
										</td>
										<td width="4px">
										</td>
										<td style="COLOR: tomato;">
                                            <ml:EncodedLiteral ID="RoleModifiableDesc" runat="server" />
										</td>
										<td width="1px" align="right">
											<asp:LinkButton runat="server" CommandName="Drop" ID="Linkbutton3">
								drop
							</asp:LinkButton>
										</td>
										<td width="4px">
										</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater></TABLE><asp:Panel id=m_AddrNone style="PADDING-RIGHT: 20px; PADDING-LEFT: 30px; PADDING-BOTTOM: 20px; FONT: 11px arial; COLOR: dimgray; PADDING-TOP: 20px; TEXT-ALIGN: center" runat="server">No role selected. &nbsp;Using default from. 
      </asp:Panel></DIV>
      <DIV style="WIDTH: 100%; COLOR: whitesmoke; MARGIN-RIGHT: 25px; TEXT-ALIGN: right">&#x25b2; &#x25b2; &#x25b2; </DIV>
      <DIV style="BORDER-RIGHT: lightgrey 2px solid; BORDER-TOP: lightgrey 2px solid; MARGIN-TOP: 4px; OVERFLOW-Y: scroll; PADDING-LEFT: 4px; MARGIN-BOTTOM: 4px; BORDER-LEFT: lightgrey 2px solid; WIDTH: 100%; BORDER-BOTTOM: lightgrey 2px solid; HEIGHT: 100px; BACKGROUND-COLOR: whitesmoke">
      <TABLE cellSpacing=2 cellPadding=0 width="100%"><asp:Repeater id=m_RoleList runat="server" OnItemCommand="RoleListClick" OnItemDataBound="RoleListBound">
								<ItemTemplate>
									<tr>
										<td>
                                            <ml:EncodedLiteral ID="RoleModifiableDesc" runat="server" />
										</td>
										<td width="1px" align="right">
											<asp:LinkButton runat="server" CommandName="Include" ID="Linkbutton4">include</asp:LinkButton>
										</td>
										<td width="4px">
										</td>
									</tr>
								</ItemTemplate>
							</asp:Repeater></TABLE></DIV></asp:Panel>
      <DIV style="PADDING-RIGHT: 8px; PADDING-LEFT: 30px; PADDING-BOTTOM: 8px; PADDING-TOP: 8px">otherwise, 
      </DIV>
			</asp:Panel>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<div style="MARGIN-TOP: 3px; PADDING-LEFT: 30px; MARGIN-BOTTOM: 3px; WIDTH: 170px">
							Default from address
						</div>
					</td>
					<td>
						<asp:TextBox id="m_DefaultReply" runat="server" style="PADDING-LEFT: 4px" Width="160"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td>
						<div style="MARGIN-TOP: 3px; PADDING-LEFT: 30px; MARGIN-BOTTOM: 3px; WIDTH: 170px">
							Default from label
						</div>
					</td>
					<td>
						<asp:TextBox id="m_DefaultLabel" runat="server" style="PADDING-LEFT: 4px" Width="160"></asp:TextBox>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
