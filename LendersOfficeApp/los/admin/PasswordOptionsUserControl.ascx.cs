using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Admin;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.los.admin
{
	/// <summary>
	///	Summary description for PasswordOptionsUserControl.
	/// </summary>

	public partial  class PasswordOptionsUserControl : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.RadioButton					m_PasswordExpiresOn;
		protected MeridianLink.CommonControls.DateTextBox				   m_ExpirationDate;
		protected System.Web.UI.WebControls.RadioButton		m_MustChangePasswordAtNextLogin;

        private Guid BrokerId
        {
            // Access 

            get
            {
                BrokerUserPrincipal user = BrokerUserPrincipal.CurrentPrincipal; //Page.User as BrokerUserPrincipal;

                if( user != null )
                {
                    return user.BrokerId;
                }

                return Guid.Empty;
            }
        }

        private bool IsActiveDirectoryAuthEnabledForLender
        {
            get
            {
                if (BrokerId != Guid.Empty)
                {
                    BrokerDB lender = BrokerDB.RetrieveById(BrokerId);
                    return lender.IsActiveDirectoryAuthenticationEnabled;
                }
                return false;
            }
        }

        /// <summary>
        /// Initialize control.
        /// </summary>

		protected void ControlLoad( object sender , System.EventArgs e )
		{
            // Initialize the drop down list of branch labels
            // the first time we load.

            if( IsPostBack == false )
            {
                DataSet ds = new DataSet();

                SqlParameter[] parameters = {
                                                new SqlParameter( "@BrokerId" , BrokerId )
                                            };

                DataSetHelper.Fill(ds, BrokerId, "ListBranchByBrokerID", parameters);

                BranchList.DataSource     = ds.Tables[ 0 ].DefaultView;
                BranchList.DataValueField = "BranchID";
                BranchList.DataTextField  = "Name";
                BranchList.DataBind();

                BranchList.Items.Insert(0, new ListItem("<-- All branches -->", Guid.Empty.ToString()));
                BranchList.Items.Insert(0, new ListItem(string.Empty, string.Empty));

                if (!IsActiveDirectoryAuthEnabledForLender)
                {
                    ActiveDirectoryMessage.Visible = false;
                }
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.ControlLoad);

		}
		#endregion

	}

}
