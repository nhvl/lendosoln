<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="ConditionChoices.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.ConditionChoices" validateRequest="true" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Condition Choices</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body scroll="no" onload="onInit();" MS_POSITIONING="FlowLayout">
	<script>
		function onInit() { try
		{
		    var feedBack = document.getElementById("m_feedBack");
		    var errorMessage = document.getElementById("m_errorMessage");
		    var commandToDo = document.getElementById("m_commandToDo");

		    if (errorMessage != null)
			{
		        alert(errorMessage.value);
			}

		    if (feedBack != null)
			{
		        alert(feedBack.value);
			}

		    if (commandToDo != null)
			{
		        if (commandToDo.value == "Close")
				{
					onClosePopup();
				}
			}

			<% if( IsPostBack == false ) { %>

			resize( 640 , 580 );

			<% } %>
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function markAsDirty()
		{
		    document.getElementById("m_IsDirty").value = "Dirty";
		}

		function onClose()
		{
		    if (document.getElementById("m_IsDirty").value == "Dirty")
			{
				if( confirm( "Do you want to close without saving?" ) == false )
				{
					return;
				}
			}

			onClosePopup();
		}
	</script>
		<h4 class="page-header">Condition Choices</h4>
		<FORM id="ConditionChoices" method="post" runat="server">
			<INPUT id="m_IsDirty" type="hidden" runat="server">
			<TABLE cellSpacing="2" cellPadding="3" width="100%" border="0">
				<TR>
					<TD>
						<TABLE cellSpacing="0" cellPadding="0" width="100%">
							<TR>
								<TD noWrap align="left" width="1%"><ASP:BUTTON id="m_Add" onclick="AddClick" runat="server" Width="62" Text="Add"></ASP:BUTTON><ASP:BUTTON id="m_Sort" onclick="SortClick" runat="server" Width="62" Text="Sort"></ASP:BUTTON>&nbsp;
									&nbsp;
									<ASP:CHECKBOX id="m_ByCategory" runat="server" Text="by category"></ASP:CHECKBOX><ASP:RADIOBUTTON id="m_CatgFirst" runat="server" Text="first" GroupName="Sorting"></ASP:RADIOBUTTON>&nbsp;
									&nbsp;
									<ASP:CHECKBOX id="m_ByCondition" runat="server" Text="by condition"></ASP:CHECKBOX><ASP:RADIOBUTTON id="m_CondFirst" runat="server" Text="first" GroupName="Sorting"></ASP:RADIOBUTTON></TD>
								<TD align="right"><ASP:BUTTON id="m_Export" runat="server" Text="Export to CSV" onclick="ExportClick"></ASP:BUTTON></TD>
							</TR>
						</TABLE>
						<HR>
						<DIV style="PADDING-RIGHT: 4px; OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 410px"><ML:COMMONDATAGRID id="m_Grid" runat="server" cellpadding="2" width="100%" OnItemCommand="GridItemClick" OnItemDataBound="GridItemBound">
								<Columns>
									<asp:TemplateColumn HeaderText="Category">
										<ItemTemplate>
											<asp:TextBox id="Category" runat="server" style="PADDING-LEFT: 4px; WIDTH: 140px; FONT: 12px arial;" TextMode="SingleLine" onchange="markAsDirty();"></asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Condition" ItemStyle-Width="1%">
										<ItemTemplate>
											<asp:TextBox id="Condition" runat="server" style="PADDING-LEFT: 4px; WIDTH: 340px; FONT: 12px arial;" TextMode="MultiLine" Rows="2" onchange="markAsDirty();"></asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn ItemStyle-HorizontalAlign="center">
										<ItemTemplate>
											<asp:LinkButton runat="server" CommandName="Remove">
										remove
									</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</ML:COMMONDATAGRID><ASP:PANEL id="m_EmptyMessage" runat="server" Width="100%" Visible="False" EnableViewState="False">
								<DIV style="PADDING-RIGHT: 40px; PADDING-LEFT: 40px; PADDING-BOTTOM: 40px; FONT: 11px arial; COLOR: dimgray; PADDING-TOP: 40px; TEXT-ALIGN: center">No
									conditions to display.
								</DIV>
							</ASP:PANEL></DIV>
						<HR>
						<DIV style="WIDTH: 100%; TEXT-ALIGN: right"><ASP:BUTTON id="m_Ok" onclick="OkClick" runat="server" Width="50px" Text="OK"></ASP:BUTTON><INPUT onclick="onClose();" type="button" value="Cancel" width="50px">
							<ASP:BUTTON id="m_Apply" onclick="ApplyClick" runat="server" Width="50px" Text="Apply"></ASP:BUTTON></DIV>
					</TD>
				</TR>
			</TABLE>
			<ML:CMODALDLG id="m_ModalDlg" runat="server"></ML:CMODALDLG></FORM>
	</body>
</HTML>
