using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{
    public partial class FieldChoices : LendersOffice.Common.BasePage
    {

        private OptionSet m_Set = new OptionSet();

        private Boolean m_IsInError = false;

        private BrokerUserPrincipal CurrentUser
        {
            // Fetch the current principal.
            get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingCustomFieldChoices
                };
            }
        }

        private String ErrorMessage
        {
            // Write out error message.

            set
            {
                ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + ".");

                m_IsInError = true;
            }
        }

        private String CommandToDo
        {
            // Write out error message.

            set
            {
                ClientScript.RegisterHiddenField("m_commandToDo", value);
            }
        }

        private Boolean IsInError
        {
            // Check if we wrote one.

            get
            {
                return m_IsInError;
            }
        }

        /// <summary>
        /// Initialize this page.
        /// </summary>

        protected void PageLoad(object sender, System.EventArgs a)
        {
            // Initialize this view.

            try
            {
                // Load the broker to get the choices.

                BrokerDB myBroker = BrokerDB.RetrieveById(CurrentUser.BrokerId);


                if (myBroker.HasLenderDefaultFeatures == false)
                {
                    throw new CBaseException(ErrorMessages.GenericAccessDenied, "User tried to load a page without permission.");
                }

                if (IsPostBack == false)
                {
                    // 12/10/2004 kb - Initialize our set for editing
                    // from this broker's data.

                    m_Set = myBroker.FieldChoices;

                    //OPM 106900: We need to add something to the field choices, and it looks like there's no actual way to do that...
                    //            So when someone looks at this page we'll add the new one if it's not there.

                    if (m_Set["DealerInvestor"] == null)
                    {
                        var dealerInvestor = new FieldOptions();
                        dealerInvestor.FieldName = "DealerInvestor";
                        dealerInvestor.Description = "Capital Markets Trades Dealer/Investor";
                        m_Set.List.Add(dealerInvestor);
                    }

                    if (m_Set["BranchDivisions"] == null)
                    {
                        var branchDivisions = new FieldOptions();
                        branchDivisions.FieldName = "BranchDivisions";
                        branchDivisions.Description = "Branch Divisions";
                        m_Set.List.Add(branchDivisions);
                    }
                }
                else
                {
                    // 12/10/2004 kb - Read in the latest and greatest
                    // from the edit boxes contained in the grid.  We
                    // assume that the edits have already been loaded
                    // into the grid's ui elements.

                    foreach (DataGridItem dgItem in m_Grid.Items)
                    {
                        TextBox tbOpts = dgItem.FindControl("Options") as TextBox;
                        Label lbName = dgItem.FindControl("FieldName") as Label;

                        if (lbName != null)
                        {
                            // We have the row data.  Now, match it to
                            // our field options container item and bind
                            // back the ui changes to our cached image.
                            // Post back events will see this data in
                            // the collection (not the previous data).

                            FieldOptions fOpts = m_Set[lbName.Text];

                            if (fOpts == null)
                            {
                                continue;
                            }

                            fOpts.Options.Clear();

                            foreach (String sOpt in tbOpts.Text.Split('\n'))
                            {
                                if (sOpt.TrimWhitespaceAndBOM() == "")
                                {
                                    continue;
                                }

                                fOpts.Add(sOpt);
                            }
                        }
                    }
                }
            }
            catch (CBaseException e)
            {
                // Oops!

                Tools.LogError("Failed to load page.", e);

                ErrorMessage = e.UserMessage;

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError("Failed to load page.", e);

                ErrorMessage = "Failed to load page.";

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
            }
        }

        /// <summary>
        /// Overload framework event for loading.
        /// </summary>

        protected override void LoadViewState(object oState)
        {
            // Get the view state of a previous postback and restore
            // the data grid's state prior to loading form variables.

            try
            {
                base.LoadViewState(oState);

                if (ViewState["Cache"] != null)
                {
                    m_Set = OptionSet.ToObject(ViewState["Cache"].ToString());
                }

                m_Grid.DataSource = m_Set.List;
                m_Grid.DataBind();
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError("Failed to load viewstate.", e);

                ErrorMessage = "Failed to load page.";

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
            }
        }

        /// <summary>
        /// Overload framework event for saving.
        /// </summary>

        protected override object SaveViewState()
        {
            // Store what we have as latest in the viewstate so
            // we can stay current between postbacks without
            // hitting the database.

            ViewState.Add("Cache", m_Set.ToString());

            return base.SaveViewState();
        }

        /// <summary>
        /// Bind this page.
        /// </summary>

        protected void PagePreRender(object sender, System.EventArgs a)
        {
            // Bind this view to our data.

            try
            {
                // Bind and let grid pull current choices.

                if (m_Set.List.Count == 0)
                {
                    m_EmptyMessage.Visible = true;

                    m_Grid.Visible = false;
                }
                else
                {
                    m_EmptyMessage.Visible = false;

                    m_Grid.Visible = true;
                }

                m_Grid.DataSource = m_Set.List;
                m_Grid.DataBind();
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError("Failed to prerender page.", e);

                ErrorMessage = "Failed to render page.";

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.PreRender += new System.EventHandler(this.PagePreRender);
        }
        #endregion

        /// <summary>
        /// Handle UI event.
        /// </summary>

        protected void GridItemBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs a)
        {
            // Initialize each row with option info.

            try
            {
                // We load the options and the key for each row
                // so we can pull out user edits on post back.

                FieldOptions fOpts = a.Item.DataItem as FieldOptions;

                if (fOpts != null)
                {
                    // Bind the ui elements to the row's edit boxes.

                    TextBox tbOpts = a.Item.FindControl("Options") as TextBox;
                    Label lbName = a.Item.FindControl("FieldName") as Label;

                    if (tbOpts != null)
                    {
                        tbOpts.Text = "";

                        foreach (String sOpt in fOpts.Options)
                        {
                            tbOpts.Text += sOpt + "\n";
                        }
                    }

                    if (lbName != null)
                    {
                        lbName.Text = fOpts.FieldName;
                    }
                }
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError("Failed to save condition choices.", e);

                ErrorMessage = "Failed to save.";

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
            }
        }

        /// <summary>
        /// Handle UI event.
        /// </summary>

        protected void ApplyClick(object sender, System.EventArgs a)
        {
            // Create new option set and save it to the broker.

            try
            {
                // Store the cached set directly in the broker
                // and save.
                //
                // 8/16/2005 kb - Updated save to go direct to
                // the db.  Our current update is colliding with
                // view updating; large employee counts makes
                // this approach prohibitive.

                BrokerDB brokerDb = BrokerDB.RetrieveById(CurrentUser.BrokerId);

                // 2/2/2005 kb - Check access privelages and validate the
                // entries.  If any line items exceed our character limit,
                // then we tell them of the first and punt.

                if (brokerDb.HasLenderDefaultFeatures == false)
                {
                    throw new CBaseException(ErrorMessages.GenericAccessDenied, "User tried to save changes without permission.");
                }

                foreach (FieldOptions fOpts in m_Set)
                {
                    foreach (String sOpt in fOpts.Options)
                    {
                        if (sOpt.Length > 100)
                        {
                            ErrorMessage = String.Format
                                ("{0} has an entry longer than 100 characters ('{1}...'). Please trim and try again. Save aborted."
                                , fOpts.Description
                                , sOpt.Substring(0, 10)
                                );

                            return;
                        }
                    }
                }

                brokerDb.FieldChoices = m_Set;

                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", CurrentUser.BrokerId),
                                                new SqlParameter("@FieldChoicesXmlContent", brokerDb.FieldChoices.ToString())
                                            };

                StoredProcedureHelper.ExecuteNonQuery(CurrentUser.BrokerId, "UpdateBrokerFieldChoices", 3, parameters);

                m_IsDirty.Value = "";
            }
            catch (CBaseException e)
            {
                // Oops!

                Tools.LogError("Failed to apply page.", e);

                ErrorMessage = e.UserMessage;

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError("Failed to apply condition choices.", e);

                ErrorMessage = "Failed to apply.";

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
            }
        }

        /// <summary>
        /// Handle UI event.
        /// </summary>

        protected void OkClick(object sender, System.EventArgs a)
        {
            // Create new option set and save it to the broker.

            try
            {
                // Store the cached set directly in the broker
                // and save.

                ApplyClick(sender, a);

                if (IsInError == true)
                {
                    return;
                }

                // We close on successful saving.

                CommandToDo = "Close";
            }
            catch (CBaseException e)
            {
                // Oops!

                Tools.LogError("Failed to save page.", e);

                ErrorMessage = e.UserMessage;

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
            }
            catch (Exception e)
            {
                // Oops!

                Tools.LogError("Failed to save condition choices.", e);

                ErrorMessage = "Failed to save.";

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
            }
        }
    }

}
