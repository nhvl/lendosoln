using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Integration.DocumentVendor;
using LendersOffice.Integration.MortgageInsurance;
using LendersOffice.Security;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.admin
{
    public partial class BrokerOptionUserControl : System.Web.UI.UserControl
    {
        // 4/25/2006 mf - OPM 4182
        //OPM 2703
        //OPM 12943
        //OPM 18430
        //OPM 19784
        //protected System.Web.UI.WebControls.RadioButtonList m_displayPmlFeeIn100Format;  //OPM 19525
        private BrokerDB m_bdb = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;
        //OPM 12711
        //OPM 12711

        //OPM 24214

        private List<IDocumentVendor> vendors = null;
        protected string VendorName
        {
            get
            {
                if (vendors == null)
                {
                    vendors = DocumentVendorFactory.CurrentVendors();
                }
                
                if (vendors.Count > 1)
                    return "Document Generation";
                else if (vendors.Count == 1)
                    return vendors[0].Skin.VendorName;
                else
                    return string.Empty;
            }
        }

        protected bool HasVendors
        {
            get
            {
                return !string.IsNullOrEmpty(VendorName);
            }
        }

        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected Boolean IsLpeEnabled
        {
            get { return BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine); }
        }

        protected bool IsPmlEnabled
        {
            get { return BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan); }
        }
        protected Boolean IsNewPmlUIEnabled
        {
            get { return m_bdb.IsNewPmlUIEnabled; }
        }
        protected Boolean IsPerTransactionBilling
        {
            get { return E_BrokerBillingVersion.PerTransaction == m_bdb.BillingVersion; }
        }
        protected Boolean IsEnabledMIVendorIntegration
        {
            get { return m_bdb.HasMIVendorIntegration; }
        }
        protected Boolean IsEnablePTMComplianceEaseIndicator
        {
            get { return IsPerTransactionBilling && m_bdb.IsEnablePTMComplianceEaseIndicator; }
        }
        protected bool IsEnablePTMComplianceEagleAuditIndicator
        {
            get { return IsPerTransactionBilling && m_bdb.IsEnablePTMComplianceEagleAuditIndicator; }
        }
        protected Boolean IsEnablePTMDocMagicSeamlessInterface
        {
            get { return IsPerTransactionBilling && m_bdb.IsEnablePTMDocMagicSeamlessInterface; }
        }

        protected Boolean IsMarketingEnabled
        {
            get { return BrokerUser.HasFeatures(E_BrokerFeatureT.MarketingTools); }
        }

        protected override void OnLoad(System.EventArgs a)
        {
            // Initialize this web control.

            base.OnLoad(a);

            if (Page.IsPostBack == false)
            {
                InitUI();
                LpeSubmitAgreement.Attributes.Add("onkeyup", "f_isMaxLengh(this, 1500, 'Loan Submission Agreement')");
                FthbCustomDefinition.Attributes.Add("onkeyup", "f_isMaxLengh(this, 1000, 'FTHB and Housing History Help Text')");
                CalculateClosingCostInPml.Visible = false;

                if (IsPmlEnabled == true || IsLpeEnabled == true)
                {
                    // 8/5/2005 kb - Load the pricing groups for this broker.  We
                    // don't worry about selecting anything, because a background
                    // service will do the retrieve.  Note that we only insert
                    // the active/enabled groups, or the current one, if it is
                    // disabled (see case 2541).
                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@BrokerId" , BrokerUser.BrokerId )
                                                };

                    Boolean hasCurrent = false;
                    using (IDataReader sR = StoredProcedureHelper.ExecuteReader(BrokerUser.ConnectionInfo, "ListPricingGroupByBrokerId", parameters))
                    {
                        BrokerLpePriceGroupIdDefault.Items.Clear();

                        while (sR.Read() == true)
                        {
                            String groupId = sR["LpePriceGroupId"].ToString();
                            String groupNm = sR["LpePriceGroupName"].ToString();

                            if ((bool)sR["ExternalPriceGroupEnabled"] == false)
                            {
                                groupNm += " (disabled)";

                                if ((bool)sR["IsCurrentDefault"] == false)
                                {
                                    continue;
                                }
                            }

                            if ((bool)sR["IsCurrentDefault"] == true)
                            {
                                hasCurrent = true;
                            }

                            BrokerLpePriceGroupIdDefault.Items.Add(new ListItem(groupNm, groupId));
                        }

                        if (hasCurrent == false)
                        {
                            BrokerLpePriceGroupIdDefault.Items.Insert(0, new ListItem("<-- None -->", Guid.Empty.ToString()));
                        }
                    }
                }


                if (IsPmlEnabled == true)
                {
                    // 5/26/2005 kb - Per case 1884, we show templates according
                    // to their published level.  This won't work so well for a
                    // setting that affects the whole broker.  We bind to all
                    // templates within the broker, but we should really only
                    // let the admin select from those that are published as
                    // "corporate".  We'll try showing all for a while.  We
                    // need to evaluate using just what is "corporate".
                    CalculateClosingCostInPml.Visible = m_bdb.AreAutomatedClosingCostPagesVisible &&
                        (IsNewPmlUIEnabled || EmployeeDB.RetrieveById(BrokerUser.BrokerId, BrokerUser.EmployeeId).IsNewPmlUIEnabled);

                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@BrokerID" , BrokerUser.BrokerId ),
                                                    new SqlParameter( "@IsTemplate" , 1 ),
                                                    new SqlParameter( "@IsValid" , 1 )
                                                };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.ConnectionInfo, "ListLoansByBrokerId", parameters))
                    {
                        Dictionary<string, string> entries = new Dictionary<string, string>();
                        entries.Add("<-- None -->", Guid.Empty.ToString());
                        while (reader.Read())
                        {
                            entries.Add(reader["LoanNm"].ToString(), reader["LoanId"].ToString());
                        }
                        QuickPricerTemplateId.DataTextField = "Key";
                        QuickPricerTemplateId.DataValueField = "Value";

                        QuickPricerTemplateId.DataSource = entries;

                        QuickPricerTemplateId.DataBind();
                    }
                }

                if (m_bdb.DefaultLockDeskID != Guid.Empty)
                {
                    EmployeeDB empDB = new EmployeeDB(m_bdb.DefaultLockDeskID, BrokerUser.BrokerId);
                    if (empDB.Retrieve())
                    {
                        m_lockDeskChoice.Data = m_bdb.DefaultLockDeskID.ToString();
                        m_lockDeskChoice.Text = empDB.FullName;
                    }
                }

                if (this.m_bdb.ExposeClientFacingWorkflowConfiguration)
                {
                    if (this.m_bdb.WorkflowRulesControllerId.HasValue)
                    {
                        this.WorkflowRulesControllerId.Value = this.m_bdb.WorkflowRulesControllerId.Value.ToString();
                        this.WorkflowRulesControllerName.InnerText = EmployeeDB.RetrieveNameByUserId(this.m_bdb.BrokerID, this.m_bdb.WorkflowRulesControllerId.Value);
                    }
                }
                else
                {
                    this.WorkflowRulesControllerRow.Visible = false;
                }
            }

            EmployeeResourcesURL.Text = m_bdb.EmployeeResourcesUrl;
        }

        private void InitUI()
        {
            RadioButtonList[] yesNoList = {
                                              m_displayPmlFeeIn100Format,
                                              m_RequireVerifyLicenseByState,
                                              TelemarketerCanRunPrequal,
                                              IsLOAllowedToEditProcessorAssignedFile,
                                              AllowCreatingLoanFromBlankTemplate,
                                              IsNonAccountantCanModifyTrustAccount,
                                              IsAEAsOfficialLoanOfficer,
                                              IsAutoGenerateLiabilityPayOffConditions,
                                              IsAllowExternalUserToCreateNewLoan,
                                              IsShowPriceGroupInEmbeddedPml,
                                              IsLpeManualSubmissionAllowed,
                                              IsPmlPointImportAllowed,
                                              IsStandAloneSecondAllowedInPml,
                                              Is8020ComboAllowedInPml,
                                              IsAllPrepaymentPenaltyAllowed,
                                              IsBestPriceEnabled,
                                              m_IsAlwaysUseBestPrice,
                                              m_ShowUnderwriterInPMLCertificate,
                                              m_ShowJuniorUnderwriterInPMLCertificate,
                                              m_ShowProcessorInPMLCertificate,
                                              m_ShowJuniorProcessorInPMLCertificate,
                                              m_CalculateclosingCostInPML,
                                              m_ApplyClosingCostToGFE,
                                              m_TriggerAprRediscNotifForAprDecrease,
                                              IsHelocsAllowedInPml,
                                              ShowLoanProductIdentifier
                                          };

            foreach (var o in yesNoList)
            {
                BindYesNoRadioButtonList(o);
            }

            AllowCallCenterAgentRunPrequalRow.Visible = (IsPmlEnabled || IsLpeEnabled) && IsMarketingEnabled;

            QuickPricerRow.Visible = IsPmlEnabled;
            FannieMaeEarlyCheckRow.Visible = m_bdb.IsEnableFannieMaeEarlyCheck;
            AllowHelocsRow.Style.Add("display", m_bdb.IsEnableHELOC ? "" : "none");

        }

        private void BindYesNoRadioButtonList(RadioButtonList rbl)
        {
            rbl.RepeatDirection = RepeatDirection.Horizontal;
            rbl.Items.Add(new ListItem("Yes", "Y"));
            rbl.Items.Add(new ListItem("No", "N"));
        }

    }
}
