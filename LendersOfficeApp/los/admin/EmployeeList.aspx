<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="EmployeeList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.EmployeeList" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>UserAdmin</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body scroll="yes">
		<h4 class="page-header"> Employee List </h4>
		<script type="text/javascript">

	    <!--
        var rowToFilterBy;
	    function _init()
	    {
			  resize( 950 , 600 );
			  rowToFilterBy = DetermineRowToFilterBy('Employee Name');
		}
		function onAddNewClick()
		{
			showModal('/los/admin/EditEmployee.aspx?cmd=add', null, null, null, function(a){
				if( a != null && a.OK == true )
				{
					self.location = self.location;
				}
			},{width:1000, hideCloseButton:true});
		}

		function onEditClick(link)
		{
		    var employeeID = link.getAttribute('data-empId');
			showModal('/los/admin/EditEmployee.aspx?cmd=edit&employeeId=' + employeeID, null, null, null, function(a){
				if( a != null && a.OK == true )
				{
					self.location = self.location;
				}
			},{width:1000, hideCloseButton:true});
		}
		function onExportClick(includePermissions)
		{
		    var url = "EmployeeListExport.aspx";
		    if (includePermissions) {
		        url += '?permissions=t';
		    }

            window.open(url, "_self");
		}
		function onBatchClick()
		{
			window.open(gVirtualRoot + '/los/admin/EditEmployeesInBatch.aspx', "_blank", "width=1024, height=768, status=no, resizable=yes, scrollbars=yes");

			self.location = self.location;
		}
		
		function onEditDefaultPermissionsClick()
		{
			showModal("/los/Admin/EditDefaultRolePermissions.aspx?brokerId=<%= AspxTools.JsStringUnquoted(CurrentUser.BrokerId.ToString()) %>",
				null, "Default Permissions", null, null, {hideCloseButton:true});
		}

        function DetermineRowToFilterBy(columnHeader)
        {
            var table = <%= AspxTools.JsGetElementById(m_dg) %>,
                headerRow = table.tBodies[0].rows[0].cells;
            columnHeader = columnHeader.toLowerCase();
            for (var i = 0; i < headerRow.length; ++i)
            {
                if ((headerRow[i].textContent || headerRow[i].innerText).toLowerCase().indexOf(columnHeader) >= 0)
                {
                    return i;
                }
            }
            
            return -1;
        }
        
        function filterEmployeeList(searchTerm) {
            if (rowToFilterBy === -1) return;
            var table = <%= AspxTools.JsGetElementById(m_dg) %>,
                rows = table.tBodies[0].rows,
                isAlt = false;
            
            searchTerm = searchTerm.toLowerCase();
            for (var ir = 0; ir < rows.length; ir++)
            {
                var row = rows[ir];
                if (row.className !== 'GridItem' && row.className !== 'GridAlternatingItem')
                {
                    continue;
                }
                
                var cell = row.cells[rowToFilterBy];
                var cellText = cell.textContent || cell.innerText;
                if (cellText.toLowerCase().indexOf(searchTerm) >= 0)
                {
                    row.style.display = '';
                    row.className = isAlt ? 'GridAlternatingItem' : 'GridItem';     
                    isAlt = !isAlt;
                }
                else
                {
                    row.style.display = 'none';
                }
            }
        }

	    //-->

		</script>
		<form id="UserAdmin" method="post" runat="server">
			<table cellSpacing="2" cellPadding="3" width="100%" border="0">
				<tr>
					<td>
						<table id="Table2" cellSpacing="0" cellPadding="0" border="0" width="100%">
							<tr>
								<td colspan="2">
									<input type="button" onclick="onAddNewClick();" value="Add New"/>
									<asp:Panel id="m_BatchPanel" runat="server" style="DISPLAY: inline" Visible="True">
										<INPUT onclick="onBatchClick();" type="button" value="Edit Multiple"/>
									</asp:Panel>
									<input type="button" onclick="onExportClick();" value="Export without Permissions"/> 
									<input type="button" onclick="onExportClick(true);" value="Export with Permissions"/> 
									<input type="button" id="m_editDefaultPermissions" value="Edit Default Role Permissions" onclick="onEditDefaultPermissionsClick();"/>
									<input type="button" onclick="onClosePopup();" value="Close"/>
									<label>Search: <input type="text" placeholder="Filter by name..." onkeyup="filterEmployeeList(this.value);" /></label>
								</td>
							</tr>
							<tr>
								<td colspan="2"><hr>
								</td>
							</tr>
							<tr>
								<td colspan="2" vAlign="bottom" style = "BORDER-RIGHT: 2px outset; BORDER-LEFT: 2px outset; PADDING-RIGHT: 0px; PADDING-LEFT: 0px;">
									
									<div class="Tabs">
									    <ul class="tabnav">
									        <li runat="server" id="tab0"><a href = "#" onclick="__doPostBack('changeTab', '0');">Active Employees</a></li>
									        <li runat="server" id="tab1"><a href = "#" onclick="__doPostBack('changeTab', '1');">Inactive Employees</a></li>
									    </ul>
								    </div>
									</td>
									
									
							</tr>
							<tr>
								<td colSpan="2" style="BORDER-TOP-WIDTH: 2px; BORDER-RIGHT: 2px outset; PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: 2px outset; PADDING-TOP: 5px; BORDER-BOTTOM: 2px outset" vAlign="top">
									<ml:CommonDataGrid id="m_dg" runat="server" OnItemDataBound="m_dg_ItemDataBound">
										<alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
										<itemstyle cssclass="GridItem"></itemstyle>
										<headerstyle cssclass="GridHeader"></headerstyle>
										<columns>
											<asp:TemplateColumn>
												<itemtemplate>
													<a id="EditLink" runat="server" href="#" onclick="onEditClick(this); return false;">edit</a>
												</itemtemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<itemtemplate>
													<img id="OwnerImg" runat="server" alt='' />
												</itemtemplate>
											</asp:TemplateColumn>
											
											<asp:TemplateColumn HeaderText="Login Name" SortExpression="Login">
                                                  <itemtemplate>
                                                      <ml:EncodedLiteral ID="Login" runat="server"></ml:EncodedLiteral>  
                                                  </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Employee Name" SortExpression="EmployeeName">
                                                  <itemtemplate>  
                                                      <ml:EncodedLiteral ID="EmployeeName" runat="server"></ml:EncodedLiteral>
                                                  </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Branch" SortExpression="Branch">
                                                  <itemtemplate>  
                                                      <ml:EncodedLiteral ID="Branch" runat="server"></ml:EncodedLiteral>
                                                  </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="GroupList" HeaderText="Employee Groups" />
                                            <asp:TemplateColumn HeaderText="Status" SortExpression="EmployeeStatus">
                                                  <itemtemplate>  
                                                      <ml:EncodedLiteral ID="EmployeeStatus" runat="server"></ml:EncodedLiteral>
                                                  </ItemTemplate>
                                            </asp:TemplateColumn>
											<asp:TemplateColumn SortExpression="LicenseNumber" HeaderText="Billing Lic #">
												<itemtemplate>
                                                    <ml:EncodedLiteral ID="LicenseNumber" runat="server"></ml:EncodedLiteral>
												</itemtemplate>
											</asp:TemplateColumn>
										</columns>
									</ml:CommonDataGrid>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
		</form>
	</body>
</HTML>
