using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;
using LendersOffice.Admin;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Common;
using System.Collections.Generic;
using LendersOffice.Pdf;
using System.Linq;
using LendersOffice.AntiXss;
using MeridianLink.CommonControls;

namespace LendersOffice.Admin
{
	/// <summary>
	/// Summary description for EditEmployeesInBatch.
	/// </summary>

	public partial class EditEmployeesInBatch : LendersOffice.Common.BasePage
	{
		#region Variables
		private ArrayList											m_Employees = new ArrayList();
		private ArrayList											m_Includes  = new ArrayList();
		private ArrayList											m_Excludes  = new ArrayList();
		private ArrayList											m_Items     = new ArrayList();
		protected System.Web.UI.WebControls.Label					Notes;
		protected EmployeePermissionsList							m_EmployeePermissionsList;
        protected string typedPw
        {
            get
            {
                if (ViewState["typedPw"] != null)
                {
                    return Convert.ToString(ViewState["typedPw"]);
                }
                return null;
            }
            set
            {
                ViewState["typedPw"] = value;
            }
        }
	
		#endregion

        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }

		protected BrokerUserPrincipal BrokerUser
		{
			// Access member.
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		protected Boolean IsPmlEnabled
		{
			get{ return BrokerUser.HasFeatures( E_BrokerFeatureT.PriceMyLoan ); }
		}

		private String ErrorMessage
		{
			set
			{
				if( value != "" )
					ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
				else
					ClientScript.RegisterHiddenField( "m_errorMessage" , "" );
			}
		}

		private String FeedBack
		{
			set
			{
				if( value != "" )
					ClientScript.RegisterHiddenField( "m_feedBack" , value.TrimEnd( '.' ) + "." );
				else
					ClientScript.RegisterHiddenField( "m_feedBack" , "" );
			}
		}

		private String CommandToDo
		{
			set { ClientScript.RegisterHiddenField( "m_commandToDo" , value ); }
		}

		/// <summary>
		/// Keep track of employees by branch.
		/// </summary>

		private class BranchSet : ArrayList
		{
			private String m_BranchName = String.Empty;
			private Guid     m_BranchId = Guid.Empty;

			#region ( Set properties )

			public String BranchName
			{
				set { m_BranchName = value; }
				get { return m_BranchName; }
			}

			public Guid BranchId
			{
				set { m_BranchId = value; }
				get { return m_BranchId; }
			}

			#endregion

			#region ( Constructors )

			/// <summary>
			/// Construct default.
			/// </summary>

			public BranchSet( String sBranchName , Guid branchId )
			{
				// Initialize members.

				m_BranchName = sBranchName;
				m_BranchId   = branchId;
			}

			#endregion

		}

		/// <summary>
		/// Bind employee list by branch.
		/// </summary>

		protected void BindEmployeeList( object sender , System.Web.UI.WebControls.RepeaterItemEventArgs a )
		{
			// Get the grouped set and bind it to the inner list.

			BranchSet bSet = a.Item.DataItem as BranchSet;
			if( bSet != null )
			{
				Repeater eList = a.Item.FindControl( "List" ) as Repeater;

				if( eList != null )
				{
					eList.DataSource = bSet;
					eList.DataBind();
				}
			}

            if (a.Item.ItemType != ListItemType.Item && a.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var branchName = bSet.BranchName;
            EncodedLiteral branchNmSpan = (EncodedLiteral)a.Item.FindControl("BranchNm");
            branchNmSpan.Text = bSet.BranchName;
		}

        protected void List_ItemDataBound(object sender, RepeaterItemEventArgs a)
        {
            if (a.Item.ItemType != ListItemType.Item && a.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            EmployeeDetails details = a.Item.DataItem as EmployeeDetails;
            var key = details.ToString(); // Just a guid as a string.
            var name = $"{details.FirstName} {details.LastName}";
            var status = details.EmployeeStatus;

            CheckBox selected = (CheckBox)a.Item.FindControl("Selected");
            EncodedLiteral nameLiteral = (EncodedLiteral)a.Item.FindControl("Name");
            EncodedLiteral statusLiteral = (EncodedLiteral)a.Item.FindControl("EmployeeStatus");

            selected.Attributes["key"] = key;
            nameLiteral.Text = name;
            statusLiteral.Text = status;

        }

        protected void m_RoleList_ItemDataBound(object sender, RepeaterItemEventArgs a)
        {
            if (a.Item.ItemType != ListItemType.Item && a.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            Role role = a.Item.DataItem as Role;
            var desc = role.Desc;
            var modifiableDesc = role.ModifiableDesc;

            CheckBox selectedCb = (CheckBox)a.Item.FindControl("Selected");
            selectedCb.Attributes["key"] = AspxTools.HtmlString(desc);
            selectedCb.Attributes["onclick"] = GenerateEnableRoleScript(a.Item.ClientID);

            EncodedLiteral modifiableDescLiteral = (EncodedLiteral)a.Item.FindControl("ModifiableDesc");
            modifiableDescLiteral.Text = modifiableDesc;
        }

        protected void m_Working_ItemDataBound(object sender, RepeaterItemEventArgs a)
        {
            if (a.Item.ItemType != ListItemType.Item && a.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            WorkingItem workingItem = a.Item.DataItem as WorkingItem;
            var key = workingItem.ToString(); // This is just a guid as a string.
            var name = $"{workingItem.FirstName} {workingItem.LastName}";
            var branch = workingItem.Branch;
            var notes = workingItem.Notes;
            var status = workingItem.EmployeeStatus;

            CheckBox selectedCb = (CheckBox)a.Item.FindControl("Selected");
            selectedCb.Attributes["key"] = key;

            EncodedLiteral nameLiteral = (EncodedLiteral)a.Item.FindControl("Name");
            nameLiteral.Text = name;

            EncodedLiteral branchLiteral = (EncodedLiteral)a.Item.FindControl("Branch");
            branchLiteral.Text = branch;

            PassthroughLabel notesLabel = (PassthroughLabel)a.Item.FindControl("Notes");
            notesLabel.Text = notes;

            EncodedLiteral statusLiteral = (EncodedLiteral)a.Item.FindControl("EmployeeStatus");
            statusLiteral.Text = status;
        }

        /// <summary>
        /// Initialize page.
        /// </summary>

        protected void PageLoad( object sender , System.EventArgs a )
		{
            this.EnableJqueryMigrate = false;

            BrokerDB brokerDB = BrokerUser.BrokerDB;
            AuthenticatonContainer.Visible = brokerDB.IsEnableMultiFactorAuthentication;
            EnableMfaRow.Visible = brokerDB.IsEnableMultiFactorAuthentication && brokerDB.AllowUserMfaToBeDisabled;

            if (m_duPw.Text.Length > 0)
            {
                typedPw = m_duPw.Text;
            }

            // Initialize this web form.
            m_EmployeePermissionsList.BatchEdit = true;
			m_EmployeePermissionsList.DataBroker = BrokerUser.BrokerId;
			m_EmployeePermissionsList.PermissionType = "employee";
			try
			{
				// If for the first time, get all employees and roles
				// and cache the content.  If not, then apply the ui
				// state to our cached entities so they can be ready
				// for click event handling.

				if( IsPostBack == false )
				{
					// Get the broker's employees with their most important
					// role for quick identification.

					BrokerEmployeeNameTable eTable = new BrokerEmployeeNameTable();
					BrokerUserPermissionsSet pTable = new BrokerUserPermissionsSet();
					BrokerBranchTable       bTable = new BrokerBranchTable();
					ListDictionary       tBranches = new ListDictionary();
					ArrayList            aBranches = new ArrayList();

					eTable.Retrieve( BrokerUser.BrokerId );
					bTable.Retrieve( BrokerUser.BrokerId );
					pTable.Retrieve( BrokerUser.BrokerId );

					foreach( EmployeeDetails eD in eTable.Values )
					{	
						BrokerUserPermissions bUp = pTable[ eD.EmployeeId ];
						if( (eD.Type == "B" || eD.Type == "") && (bUp == null || bUp.IsInternalBrokerUser() == false))
						{
							BranchSet bSet = tBranches[ eD.BranchId ] as BranchSet;

							if( bSet == null )
							{
								BrokerBranchTable.Spec bS = bTable[ eD.BranchId ];

								if( bS != null )
									tBranches.Add( bS.BranchId , bSet = new BranchSet( bS.BranchName , bS.BranchId ) );
								else
								{
									tBranches.Add( eD.BranchId , bSet = new BranchSet ( "Unknown" , eD.BranchId ) );
									Tools.LogError( "Missing branch spec for " + eD.BranchId + "." );
								}
								aBranches.Add( bSet );
							}
							bSet.Add( eD );
						}
					}

					m_EmployeeList.DataSource = aBranches;
					m_EmployeeList.DataBind();

					// Get the current roles that are assignable to employees.

					m_RoleList.DataSource = Role.LendingQBRoles.OrderBy(role => role.ModifiableDesc);
					m_RoleList.DataBind();
				}
				else
				{
					// Bind the ui to our role selection list.  The posted-back
					// state is used when handling click events.

					foreach( RepeaterItem bItem in m_EmployeeList.Items )
					{
						Repeater eList = bItem.FindControl( "List" ) as Repeater;

						if( eList != null )
						{
							foreach( RepeaterItem eItem in eList.Items )
							{
								CheckBox cBox = eItem.FindControl( "Selected" ) as CheckBox;

								if( cBox != null && cBox.Checked == true )
								{
									try
									{
										m_Employees.Add( new Guid( cBox.Attributes[ "key" ] ) );
									}
									catch( Exception e )
									{
										Tools.LogError( "Selection checkbox key is invalid (" + cBox.Attributes[ "key" ] + ")." , e );
									}
								}
							}
						}
					}

					foreach( RepeaterItem rItem in m_RoleList.Items )
					{
						CheckBox rBut , cBox = rItem.FindControl( "Selected" ) as CheckBox;

						if( cBox == null || cBox.Checked == false )
							continue;

						rBut = rItem.FindControl( "Include" ) as CheckBox;

						if( rBut != null )
						{
							if( rBut.Checked == true )
								m_Includes.Add( cBox.Attributes[ "key" ] );
						}

						rBut = rItem.FindControl( "Exclude" ) as CheckBox;

						if( rBut != null )
						{
							if( rBut.Checked == true )
								m_Excludes.Add( cBox.Attributes[ "key" ] );
						}
					}

					foreach( RepeaterItem wItem in m_Working.Items )
					{
						CheckBox cBox = wItem.FindControl( "Selected" ) as CheckBox;

                        //  If a User was disabled for being unable to enable Sms Authorization, and if the batch setting
                        //  was changed so that Sms Authorization is no longer being turned on, re-enable the user's 
                        //  checkbox, set checked to true, and clear the notes.
                        if(cBox != null && cBox.Attributes["conflictingFields"] != null)
                        {
                            List<string> conflictingFields = SerializationHelper.JsonNetDeserialize<List<string>>(cBox.Attributes["conflictingFields"]);

                            bool isStillConflicting = conflictingFields.Exists(fieldName => (FindControl(fieldName) as CheckBox)?.Checked == true);

                            if(!isStillConflicting)
                            {
                                cBox.Enabled = true;
                                cBox.Checked = true;
                                cBox.Attributes.Remove("conflictingFields");

                                Label notes = wItem.FindControl("Notes") as Label;
                                notes.Text = "";
                            }
                        }

                        if ( cBox != null && cBox.Checked == true )
						{
							try
							{
								m_Items.Add( new Guid( cBox.Attributes[ "key" ] ) );
							}
							catch( Exception e )
							{
								Tools.LogError( "Selection checkbox key is invalid (" + cBox.Attributes[ "key" ] + ")." , e );
							}
						}
					}
				}
			}
			catch( CBaseException e )
			{
				if( e.UserMessage != "" )
					Tools.LogError( ErrorMessage = "Failed to load web form: " + e.UserMessage , e );
				else
					Tools.LogError( ErrorMessage = "Failed to load web form." , e );
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to load web form." , e );
			}
		}

		/// <summary>
		/// Render page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			try
			{
				// Disable and enable based on radio selection.
                var brokerDb = this.BrokerUser.BrokerDB;

				foreach( RepeaterItem rItem in m_RoleList.Items )
				{
					CheckBox cBox = rItem.FindControl( "Selected" ) as CheckBox;
					Panel    rPan = rItem.FindControl( "Panel" ) as Panel;

					if( cBox == null || rPan == null )
						continue;

                    var roleDesc = cBox.Attributes["key"];
                    var roleId = Role.Get(roleDesc).Id;

                    if (ConstApp.RolesAddedForOPM145015.Contains(roleId))
                    {
                        cBox.Enabled = brokerDb.HasMigratedToNewRoles_135241;
                    }

					rPan.Enabled = cBox.Checked;
				}
		
				m_Empty.Visible = (m_Working.Items.Count == 0)?true:false;
			}
			catch( CBaseException e )
			{
				if( e.UserMessage != "" )
					Tools.LogError( ErrorMessage = "Failed to render web form: " + e.UserMessage , e );
				else
					Tools.LogError( ErrorMessage = "Failed to render web form." , e );
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to render web form." , e );
			}
		}

        protected string GenerateEnableRoleScript(string rowId) 
        {
            return "enableRole(this, '" + rowId + "');";
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            RegisterJsScript("json.js");
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

		/// <summary>
		/// Provide binding descriptor for presenting the changes we're
		/// about to make.
		/// </summary>

		private class WorkingItem
		{
			/// <summary>
			/// Provide binding descriptor for presenting the changes
			/// we're about to make.
			/// </summary>

			private String m_FirstName = String.Empty;
			private String  m_LastName = String.Empty;
			private String    m_Branch = String.Empty;
			private String     m_Notes = String.Empty;
			private String     m_EmployeeStatus = String.Empty;
			private Guid  m_EmployeeId = Guid.Empty;

			#region ( Working Item Properties )

			public String EmployeeStatus
			{
				set { m_EmployeeStatus = value; }
				get { return m_EmployeeStatus; }
			}
			
			public String FirstName
			{
				set { m_FirstName = value; }
				get { return m_FirstName; }
			}

			public String LastName
			{
				set { m_LastName = value; }
				get { return m_LastName; }
			}

			public String Branch
			{
				set { m_Branch = value; }
				get { return m_Branch; }
			}

			public String Notes
			{
				set { m_Notes = value; }
				get { return m_Notes; }
			}

			public Guid EmployeeId
			{
				set { m_EmployeeId = value; }
				get { return m_EmployeeId; }
			}

			#endregion

			/// <summary>
			/// Return our identifier as a key for binding.
			/// </summary>

			public override string ToString()
			{
				// Return our identifier as a key for binding.
				return m_EmployeeId.ToString();
			}
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void LoadClick( object sender , System.EventArgs a )
		{
			// Load up a new employee set and bind to the working list for
			// committing on apply or ok click.

			try
			{
				// Choose how we're going to get the employees.  Both lookups
				// hit the db to get the records.

				BrokerEmployeeNameTable   eTable = new BrokerEmployeeNameTable();
				BrokerBranchTable         bTable = new BrokerBranchTable();
				BrokerLoanAssignmentTable rTable = new BrokerLoanAssignmentTable();
				BrokerUserPermissionsSet pTable = new BrokerUserPermissionsSet();
				ArrayList            conflictSet = new ArrayList();
				ArrayList             workingSet = new ArrayList();
				ArrayList              choiceSet = new ArrayList();

				pTable.Retrieve( BrokerUser.BrokerId );
				foreach( String rDesc in m_Includes )
				{
					if( rTable[ rDesc ] != null )
						continue;
					rTable.Retrieve( BrokerUser.BrokerId , rDesc );
				}

				foreach( String rDesc in m_Excludes )
				{
					if( rTable[ rDesc ] != null )
						continue;
					rTable.Retrieve( BrokerUser.BrokerId , rDesc );
				}

				foreach( String rDesc in m_Includes )
				{
					if( rTable[ rDesc ] == null )
						continue;

					foreach( BrokerLoanAssignmentTable.Spec eS in rTable[ rDesc ] )
					{
						if( choiceSet.Contains( eS.EmployeeId ) == false )
							choiceSet.Add( eS.EmployeeId );
					}
				}

				foreach( String rDesc in m_Excludes )
				{
					if( rTable[ rDesc ] == null )
						continue;

					foreach( BrokerLoanAssignmentTable.Spec eS in rTable[ rDesc ] )
					{
						if( m_Employees.Contains( eS.EmployeeId ) == true )
							conflictSet.Add( eS.EmployeeId );

						choiceSet.Remove( eS.EmployeeId );
					}
				}

				foreach( Guid employeeId in m_Employees )
				{
					if( choiceSet.Contains( employeeId ) == false )
						choiceSet.Add( employeeId );
				}

				eTable.Retrieve( BrokerUser.BrokerId );
				bTable.Retrieve( BrokerUser.BrokerId );

				foreach( Guid employeeId in choiceSet )
				{
					// Extract the details as a working set item.
					EmployeeDetails eD = eTable[ employeeId ];
					if( eD == null )
						continue;

					BrokerUserPermissions bUp = pTable[ eD.EmployeeId ];
					if( (eD.Type == "B" || eD.Type == "") && (bUp == null || bUp.IsInternalBrokerUser() == false))
					{
						WorkingItem wItem = new WorkingItem();
						wItem.FirstName  = eD.FirstName;
						wItem.LastName   = eD.LastName;
						wItem.EmployeeStatus = eD.EmployeeStatus;
						wItem.EmployeeId = eD.EmployeeId;

						if( bTable.Contains( eD.BranchId ) == true )
							wItem.Branch = bTable[ eD.BranchId ].BranchName;

						if( conflictSet.Contains( employeeId ) == true )
							wItem.Notes = "<span class=\"Conflict\">Individually selected, but excluded by role</span>";

						workingSet.Add( wItem );
					}
				}
				m_Working.DataSource = workingSet;
				m_Working.DataBind();

				// Cleanup selections by turning off all the conflicts.

				foreach( RepeaterItem rItem in m_Working.Items )
				{
					CheckBox cBox = rItem.FindControl( "Selected" ) as CheckBox;

					if( cBox != null && cBox.Checked == true )
					{
						if( conflictSet.Contains( new Guid( cBox.Attributes[ "key" ] ) ) == true )
							cBox.Checked = false;
					}
				}

				// Just point to the correct panel as we pass thru our
				// state machine.

				m_CurrentPanel.Value = "Settings";
                m_duPw.Enabled = false; // make them retype password between panel changes
                m_keepCurrentDuPw.Checked = true;
            }
			catch( CBaseException e )
			{
				if( e.UserMessage != "" )
					Tools.LogError( ErrorMessage = "Failed to load set of affected employees: " + e.UserMessage , e );
				else
					Tools.LogError( ErrorMessage = "Failed to load set of affected employees." , e );
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to load set of affected employees." , e );
			}
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void NextClick( object sender , System.EventArgs a )
		{
            // Handle transition from settings to committing.
            try
			{
                // Just point to the correct panel as we pass thru our
                // state machine.

				m_CurrentPanel.Value = "Commit";
			}
			catch( CBaseException e )
			{
				if( e.UserMessage != "" )
					Tools.LogError( ErrorMessage = "Failed to transition to preview: " + e.UserMessage , e );
				else
					Tools.LogError( ErrorMessage = "Failed to transition to preview." , e );
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to transition to preview." , e );
			}
		}

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void CheckClick( object sender , System.EventArgs a )
		{
			// Save the employee state changes.

			try
			{
				// Get the employee id set from our working list and check
				// each to be sure it's selected.  Each employee gets the
				// specified changes and then we save.  If any employee
				// save fails, then we rollback all the changes.
				//
				// 4/1/2005 kb - Added the preview to help with finding users
				// who have a particular state: case #1512.
				//
				// 8/17/2005 kb - Get all employee's permissions in one
				// shot (not one-by-one -- too slow).

				BrokerUserPermissionsSet bSet = new BrokerUserPermissionsSet();
				bSet.Retrieve( BrokerUser.BrokerId );
				foreach( RepeaterItem rItem in m_Working.Items )
				{
					CheckBox cBox = rItem.FindControl( "Selected" ) as CheckBox;
					Label lNotes = rItem.FindControl( "Notes" ) as Label;

					if( cBox != null && cBox.Attributes[ "key" ] != null )
					{
						// Get the current employee and apply the changes.  We
						// get the permissions in a separate container, but both
						// will be bound by the same transaction.

						EmployeeDB empDb = new EmployeeDB( new Guid( cBox.Attributes[ "key" ] ) , BrokerUser.BrokerId );

						if( empDb.Retrieve() == false )
							throw new CBaseException(ErrorMessages.FailedToSave, "Failed to load employee " + cBox.Attributes[ "key" ] + ".");

						// Apply the changes.  We don't change the employment
						// status of account owners.  Everyone else is fair
						// game -- even the user doing the operation.  We need
						// to work out the rules for updating self and admins.

						BrokerUserPermissions bUp;
						Boolean        willChange;
						Boolean        lacksLogin;
                        bool preventSave = false;
                        string nonFatalError = string.Empty;
                        List<string> conflictingFields = new List<string>();

						bUp = bSet[ empDb.ID ];

						willChange = false;
						lacksLogin = false;

						if(int.Parse(m_AccessLevel.SelectedItem.Value) > 0)
						{
							// Clear out all access levels and then set the selected
							// on.  If none is selected, we are going to punt.

							if( bUp != null )
							{
								if( bUp.HasPermission( int.Parse( m_AccessLevel.SelectedItem.Value ) ) == false )
								{
									bUp.ClearPermission( Permission.BrokerLevelAccess     );
									bUp.ClearPermission( Permission.BranchLevelAccess     );
									bUp.ClearPermission( Permission.IndividualLevelAccess );
                                    bUp.ClearPermission(Permission.TeamLevelAccess);
									bUp.SetPermission( int.Parse( m_AccessLevel.SelectedItem.Value ) );
									willChange = true;
								}
							}
							else
								lacksLogin = true;
						}

						if( bUp != null )
						{
							if(m_EmployeePermissionsList.SaveBatchPermissions(bUp) == true)
								willChange = true;
						}
						else
							lacksLogin = true;
						
						if( m_KeepCurrentEvents.Checked == false )
						{
							// Update the employee's notification settings.  Eventually,
							// this option will apply to all notifications.
							//
							// 3/10/2005 kb - We use this *old* enum to save the employee's
							// email send setting.  Note that only loan assignment changes
							// consider this option (we now have many other events that
							// assume the user *always* wants to receive it...).

							if( m_SendOnEvents.Checked == true && empDb.NewOnlineLoanEventNotifOptionT != E_NewOnlineLoanEventNotifOptionT.ReceiveEmail )
							{
								empDb.NewOnlineLoanEventNotifOptionT = E_NewOnlineLoanEventNotifOptionT.ReceiveEmail;
								willChange = true;
							}

							if( m_DoNotSend.Checked == true && empDb.NewOnlineLoanEventNotifOptionT != E_NewOnlineLoanEventNotifOptionT.DontReceiveEmail )
							{
								empDb.NewOnlineLoanEventNotifOptionT = E_NewOnlineLoanEventNotifOptionT.DontReceiveEmail;
								willChange = true;
							}
						}

						if( m_KeepCurrentStatus.Checked == false )
						{
							// 3/10/2005 kb - This is the most dangerous setting.  You
							// lock out users when you inactivate them.  Also, we assume
							// that all activated employees are coming from a state of
							// inactivation, which means they can't login.  Making an
							// employee active does not allow them to login.  Each user
							// has to be considered individually to update that setting.

							if( m_CurrentEmployee.Checked == true && empDb.IsActive != true )
							{
								empDb.IsActive = true;
								willChange = true;
							}

							if( m_NotAnEmployee.Checked == true && empDb.IsActive != false )
							{
								if( empDb.IsAccountOwner == false && empDb.ID != BrokerUser.EmployeeId )
								{
									empDb.AllowLogin = false;
									empDb.IsActive   = false;
									willChange = true;
								}
							}
						}

						if(IsPmlEnabled)
						{
							if( m_currentBypassSetting.Checked == false )
							{
								if( m_noBypass.Checked == true && empDb.RatesheetExpirationBypassType != E_RatesheetExpirationBypassType.NoBypass )
								{
									empDb.RatesheetExpirationBypassType = E_RatesheetExpirationBypassType.NoBypass;
									willChange = true;
								}

								if( m_bypassExcInvCutoff.Checked == true && empDb.RatesheetExpirationBypassType != E_RatesheetExpirationBypassType.BypassAllExceptInvCutoff )
								{
									empDb.RatesheetExpirationBypassType = E_RatesheetExpirationBypassType.BypassAllExceptInvCutoff;
									willChange = true;
								}

								if( m_bypassAll.Checked == true && empDb.RatesheetExpirationBypassType != E_RatesheetExpirationBypassType.BypassAll )
								{
									empDb.RatesheetExpirationBypassType = E_RatesheetExpirationBypassType.BypassAll;
									willChange = true;
								}
							}
						}

                        if (!m_KeepCurrentPrintPermissions.Checked)
                        {
                            if (empDb.RestrictPrintGroups && m_allowPrintingAllDocs.Checked)
                            {
                                empDb.AvailablePrintGroups = null;
                                willChange = true;
                            }
                            else if (m_restrictDocs.Checked)
                            {
                                willChange = empDb.AssignPrintGroupsById(AssignedPrintGroupsJSON.Value);
                            }
                        }

                        if (!CurrentAllowCert.Checked)
                        {
                            if (empDb.EnabledClientDigitalCertificateInstall != AllowCert.Checked)
                            {
                                empDb.EnabledClientDigitalCertificateInstall = AllowCert.Checked;
                                willChange = true;
                            }
                        }

                        if (!CurrentSmsAuth.Checked)
                        {
                            if (empDb.EnableAuthCodeViaSms != EnableSmsAuth.Checked)
                            {
                                if (EnableSmsAuth.Checked && !empDb.HasCellPhoneNumber)
                                {
                                    nonFatalError += String.Format
                                        ("\r\n"
                                        + "SMS cannot be enabled for this user because their cell phone number is blank."
                                        );
                                    
                                    preventSave = true;
                                    conflictingFields.Add(EnableSmsAuth.ClientID);
                                }
                                else
                                {
                                    empDb.EnableAuthCodeViaSms = EnableSmsAuth.Checked;
                                    willChange = true;
                                }
                            }
                        }

                        if (!CurrentMFA.Checked)
                        {
                            if (empDb.EnabledMultiFactorAuthentication != EnableMFA.Checked)
                            {
                                empDb.EnabledMultiFactorAuthentication = EnableMFA.Checked;
                                willChange = true;
                            }
                        }

                        if (!CurrentAllowAnyIPAccess.Checked)
                        {
                            if (empDb.EnabledIpRestriction != EnableIpRestriction.Checked)
                            {
                                empDb.EnabledIpRestriction = EnableIpRestriction.Checked;
                                willChange = true;
                            }
                        }                       

                        if (!m_keepCurrentDuLogin.Checked)
                        {
                            empDb.DUAutoLoginName = m_duLogin.Text;
                            willChange = true;
                        }
                        if (!m_keepCurrentDuPw.Checked)
                        {
                            empDb.DUAutoPassword = typedPw;
                            willChange = true;
                        }

                        if (conflictingFields.Count > 0)
                        {
                            cBox.Attributes.Add("conflictingFields", SerializationHelper.JsonNetSerialize(conflictingFields));
                        }

                        // Save out the changes (even if none made) with our lone
                        // transaction.  This *should* make this apply atomic.
                        // If nothing changed, then don't bother writing back.
                        if (preventSave)
                        {
                            cBox.Checked = false;
                            cBox.Enabled = false;
                            if ( lNotes != null )
								lNotes.Text = nonFatalError;
                        }
                        else if ( willChange == true && lacksLogin == false )
						{
							if( lNotes != null )
								lNotes.Text = "<span class=\"WillBeAffected\">Will be affected</span>";
						}
						else
						{
							if( lacksLogin == true )
							{
								if( lNotes != null )
									lNotes.Text = "<span class=\"Conflict\">Lacks login access</span>";
							}
							else
							{
								if( lNotes != null)
									lNotes.Text = "<span class=\"AlreadySet\">Already set</span>";
							}
						}
					}
				}

				// Just point to the correct panel as we pass thru our
				// state machine.

				m_CurrentPanel.Value = "Commit";
			}
			catch( CBaseException e )
			{
				if( e.UserMessage != "" )
					Tools.LogError( ErrorMessage = "Failed to check changes: " + e.UserMessage , e );
				else
					Tools.LogError( ErrorMessage = "Failed to check changes." , e );
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to check changes.  Nothing saved." , e );
			}
		}

		//For future use, doesn't work for all cases yet
		/*protected void ClearUnaffectedClick( object sender, System.EventArgs a )
		{
			CheckClick(sender, a);
			bool existsVisible = false;
			foreach( RepeaterItem rItem in m_Working.Items )
			{
				Label lNotes = rItem.FindControl( "Notes" ) as Label;
				if( lNotes.Text.IndexOf("AlreadySet") > -1 )
					rItem.Visible = false;
				else
				{
					rItem.Visible = true;
					existsVisible = true;
				}
			}
			
			if(!existsVisible)
			{
				m_Working.Visible = false;
				m_Empty.Visible = true;
			}
		}*/

		/// <summary>
		/// Handle UI click.
		/// </summary>

		protected void ApplyClick( object sender , System.EventArgs a )
		{
			// Save the employee state changes.

			try
			{
				// Get the employee id set from our working list and check
				// each to be sure it's selected.  Each employee gets the
				// specified changes and then we save.  If any employee
				// save fails, then we rollback all the changes.

				String sNotes = "";
				Int32  nUsers = 0;
				Int32  nSaved = 0;

                using (CStoredProcedureExec spExec = new CStoredProcedureExec(BrokerUser.BrokerId))
				{
					try
					{
						spExec.BeginTransactionForWrite();

						foreach( RepeaterItem rItem in m_Working.Items )
						{
							CheckBox cBox = rItem.FindControl( "Selected" ) as CheckBox;

							if( cBox != null && cBox.Checked == true )
							{
								// Get the current employee and apply the changes.  We
								// get the permissions in a separate container, but both
								// will be bound by the same transaction.

								BrokerUserPermissions bUp = new BrokerUserPermissions( BrokerUser.BrokerId , new Guid( cBox.Attributes[ "key" ] ) , spExec );
								EmployeeDB          empDb = new EmployeeDB( new Guid( cBox.Attributes[ "key" ] ) , BrokerUser.BrokerId );

								++nUsers;

								if( empDb.Retrieve( spExec ) == false )
									throw new CBaseException(ErrorMessages.FailedToSave, "Failed to load employee " + cBox.Attributes[ "key" ] + ".");

								// Apply the changes.  We don't change the employment
								// status of account owners.  Everyone else is fair
								// game -- even the user doing the operation.  We need
								// to work out the rules for updating self and admins.

								Boolean isChanged = false;
                                bool isPreventSave = false;
                                string nonFatalError = string.Empty;
                                List<string> conflictingFields = new List<string>();

								if( int.Parse(m_AccessLevel.SelectedItem.Value) > 0)
								{
									// Clear out all access levels and then set the selected
									// on.  If none is selected, we are going to punt.

									if( bUp.HasPermission( int.Parse( m_AccessLevel.SelectedItem.Value ) ) == false )
									{
										bUp.ClearPermission( Permission.BrokerLevelAccess     );
										bUp.ClearPermission( Permission.BranchLevelAccess     );
										bUp.ClearPermission( Permission.IndividualLevelAccess );
                                        bUp.ClearPermission(Permission.TeamLevelAccess);

										bUp.SetPermission( int.Parse( m_AccessLevel.SelectedItem.Value ) );

										isChanged = true;
									}
								}

								if(m_EmployeePermissionsList.SaveBatchPermissions(bUp) == true)
									isChanged = true;
								
								if( m_KeepCurrentEvents.Checked == false )
								{
									// Update the employee's notification settings.  Eventually,
									// this option will apply to all notifications.
									//
									// 3/10/2005 kb - We use this *old* enum to save the employee's
									// email send setting.  Note that only loan assignment changes
									// consider this option (we now have many other events that
									// assume the user *always* wants to receive it...).

									if( m_SendOnEvents.Checked == true && empDb.NewOnlineLoanEventNotifOptionT != E_NewOnlineLoanEventNotifOptionT.ReceiveEmail )
									{
										empDb.NewOnlineLoanEventNotifOptionT = E_NewOnlineLoanEventNotifOptionT.ReceiveEmail;
										isChanged = true;
									}

									if( m_DoNotSend.Checked == true && empDb.NewOnlineLoanEventNotifOptionT != E_NewOnlineLoanEventNotifOptionT.DontReceiveEmail )
									{
										empDb.NewOnlineLoanEventNotifOptionT = E_NewOnlineLoanEventNotifOptionT.DontReceiveEmail;
										isChanged = true;
									}
								}

								if( m_KeepCurrentStatus.Checked == false )
								{
									// 3/10/2005 kb - This is the most dangerous setting.  You
									// lock out users when you inactivate them.  Also, we assume
									// that all activated employees are coming from a state of
									// inactivation, which means they can't login.  Making an
									// employee active does not allow them to login.  Each user
									// has to be considered individually to update that setting.

									if( m_CurrentEmployee.Checked == true && empDb.IsActive != true )
									{
										empDb.IsActive = true;
										isChanged = true;
									}

									if( m_NotAnEmployee.Checked == true && empDb.IsActive != false )
									{
										if( empDb.IsAccountOwner == false && empDb.ID != BrokerUser.EmployeeId )
										{
											empDb.AllowLogin = false;
											empDb.IsActive   = false;
											isChanged = true;
										}
										else
											if( empDb.IsAccountOwner == true )
										{
											sNotes += String.Format
												( "\r\n"
												+ "{0} is the account owner, so employment status left unchanged"
												, empDb.FullName
												);
										}
										else
										{
											sNotes += String.Format
												( "\r\n"
												+ "Cannot deactivate yourself; employment status left unchanged"
												);
										}
									}
								}

								if(IsPmlEnabled)
								{
									if( m_currentBypassSetting.Checked == false )
									{
										if( m_noBypass.Checked == true && empDb.RatesheetExpirationBypassType != E_RatesheetExpirationBypassType.NoBypass )
										{
											empDb.RatesheetExpirationBypassType = E_RatesheetExpirationBypassType.NoBypass;
											isChanged = true;
										}

										if( m_bypassExcInvCutoff.Checked == true && empDb.RatesheetExpirationBypassType != E_RatesheetExpirationBypassType.BypassAllExceptInvCutoff )
										{
											empDb.RatesheetExpirationBypassType = E_RatesheetExpirationBypassType.BypassAllExceptInvCutoff;
											isChanged = true;
										}

										if( m_bypassAll.Checked == true && empDb.RatesheetExpirationBypassType != E_RatesheetExpirationBypassType.BypassAll )
										{
											empDb.RatesheetExpirationBypassType = E_RatesheetExpirationBypassType.BypassAll;
											isChanged = true;
										}
									}
								}

                                if (!m_KeepCurrentPrintPermissions.Checked)
                                {
                                    if (empDb.RestrictPrintGroups && m_allowPrintingAllDocs.Checked)
                                    {
                                        empDb.AvailablePrintGroups = null;
                                        isChanged = true;
                                    }
                                    else if (m_restrictDocs.Checked)
                                    {
                                        isChanged = empDb.AssignPrintGroupsById(AssignedPrintGroupsJSON.Value);
                                    }
                                }

                                if (!CurrentMFA.Checked)
                                {
                                    if (empDb.EnabledMultiFactorAuthentication != EnableMFA.Checked)
                                    {
                                        empDb.EnabledMultiFactorAuthentication = EnableMFA.Checked;
                                        isChanged = true;
                                    }
                                }

                                if (!CurrentAllowCert.Checked)
                                {
                                    if (empDb.EnabledClientDigitalCertificateInstall != AllowCert.Checked)
                                    {
                                        empDb.EnabledClientDigitalCertificateInstall = AllowCert.Checked;
                                        isChanged = true;
                                    }
                                }

                                if (!CurrentSmsAuth.Checked)
                                {
                                    if (empDb.EnableAuthCodeViaSms != EnableSmsAuth.Checked)
                                    {
                                        if(EnableSmsAuth.Checked && !empDb.HasCellPhoneNumber)
                                        {
                                            nonFatalError += String.Format
												( "\r\n"
												+ "SMS cannot be enabled for this user because their cell phone number is blank."
												);
                                            isPreventSave = true;
                                            conflictingFields.Add(EnableSmsAuth.ClientID);
                                        }
                                        else
                                        {
                                            empDb.EnableAuthCodeViaSms = EnableSmsAuth.Checked;
                                            isChanged = true;
                                        }
                                    }
                                }

                                if (!CurrentAllowAnyIPAccess.Checked)
                                {
                                    if (empDb.EnabledIpRestriction != EnableIpRestriction.Checked)
                                    {
                                        empDb.EnabledIpRestriction = EnableIpRestriction.Checked;
                                        isChanged = true;
                                    }
                                }

                                if (!m_keepCurrentDuLogin.Checked)
                                {
                                    empDb.DUAutoLoginName = m_duLogin.Text;
                                    isChanged = true;
                                }
                                if (!m_keepCurrentDuPw.Checked)
                                {
                                    empDb.DUAutoPassword = typedPw;
                                    isChanged = true;
                                }

                                if (conflictingFields.Count > 0)
                                {
                                    cBox.Attributes.Add("conflictingFields", SerializationHelper.JsonNetSerialize(conflictingFields));
                                }

                                // Save out the changes (even if none made) with our lone
                                // transaction.  This *should* make this apply atomic.
                                // If nothing changed, then don't bother writing back.
                                Label lNotes = rItem.FindControl( "Notes" ) as Label;
                                if (isPreventSave)
                                {
                                    if ( lNotes != null )
										lNotes.Text = nonFatalError;
                                    cBox.Checked = false;
                                    cBox.Enabled = false;
                                }
                                else if (isChanged == true)
                                {
                                    empDb.WhoDoneIt = BrokerUser.UserId;

									bUp.Update( spExec );
									empDb.Save( spExec, PrincipalFactory.CurrentPrincipal );

									if( lNotes != null )
										lNotes.Text = "<span class=\"Updated\">Updated</span>";

									++nSaved;
								}
								else
								{
									if( lNotes != null )
										lNotes.Text = "<span class=\"AlreadySet\">Already set</span>";
								}
							}
						}

						spExec.CommitTransaction();
					}
					catch
					{
						spExec.RollbackTransaction();
						throw;
					}
				}

				// Show user the success results (we wouldn't be here
				// otherwise).  If the saved and users counts don't match,
				// we make a note of it here (probably because changes were
				// already in place for one or more selected employees).

				String sFinal;

				if( nSaved != nUsers && nSaved > 0 )
				{
					sFinal = String.Format
						( "Updated {0} of {1} employees (some may not have been affected by the changes)"
						, nSaved
						, nUsers
						);
				}
				else
				if( nSaved > 0 )
				{
					sFinal = String.Format
						( "Updated {0} employees"
						, nSaved
						);
				}
				else
				{
					sFinal = String.Format
						( "Nothing to do (no employee selected or affected by the changes)"
						);
				}

				if( sNotes != "" )
					FeedBack = sFinal + "\r\n" + sNotes;
				else
					FeedBack = sFinal;

				// Just point to the correct panel as we pass thru our
				// state machine.

				m_CurrentPanel.Value = "Commit";
			}
			catch( CBaseException e )
			{
				if( e.UserMessage != "" )
					Tools.LogError( ErrorMessage = "Failed to apply changes: " + e.UserMessage , e );
				else
					Tools.LogError( ErrorMessage = "Failed to apply changes." , e );
			}
			catch( Exception e )
			{
				Tools.LogError( ErrorMessage = "Failed to apply changes.  Nothing saved." , e );
			}
		}
    }
}