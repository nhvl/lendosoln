﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomFolderNavigation.aspx.cs" Inherits="LendersOfficeApp.los.admin.CustomFolderNavigation" %>
<%@ Register TagPrefix="uc1" TagName="CustomFolderNavigationControl" Src="~/los/admin/CustomFolderNavigationControl.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
</head>
<body class="EditBackground">
    <h4 class="page-header">Edit Custom Navigation</h4>
    <form id="form1" runat="server">
        <uc1:CustomFolderNavigationControl IsAdmin="true" id="CustomFolderNavigationControl" runat="server"></uc1:CustomFolderNavigationControl>
    </form>
</body>
</html>
