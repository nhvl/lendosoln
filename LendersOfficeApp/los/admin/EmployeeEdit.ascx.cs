using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.CustomPmlFields;
using LendersOffice.Events;
using LendersOffice.Integration.Appraisals;
using LendersOffice.Integration.DocumentVendor;
using LendersOffice.Integration.GenericFramework;
using LendersOffice.Integration.Irs4506T;
using LendersOffice.ObjLib.ServiceCredential;
using LendersOffice.Security;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.admin
{
	public partial  class EmployeeEdit : System.Web.UI.UserControl
	{
		#region variables
        private const int MAX_IMG_SIZE = 100 * 1024; //100KB

        protected RadioButton                  m_CurrentEmployee;
        protected UserType userType;
        protected Guid                         m_EmployeeId = Guid.Empty;
        protected Guid                         m_BrokerId = Guid.Empty;
        protected Guid                         m_UserIdG = Guid.Empty;
        private Guid                           m_BranchId = Guid.Empty;
        private Guid                           m_WhoDoneIt = Guid.Empty;
        private Boolean                        m_IsDeveloper = false;
        private Boolean                        m_IsInternal = false;
        protected Boolean                      m_HasLogin = false;
        protected string                       m_LicenseId = "" ;
        protected Boolean                      m_expirationPeriodDisabled = false;
        protected Boolean                      m_cycleExpirationDisabled = false;
        protected Boolean                      m_expirationDateDisabled = false;
        protected Boolean                      m_setIsActive = false;
        protected Boolean                      m_setAllowLogin = false;
        protected string                       m_assignLoansToAnyBranchId = "";
        private System.Collections.Hashtable   m_BranchList;
        /// <summary>
        /// Hashtable from user id to branch id.
        /// </summary>
        private System.Collections.Hashtable   m_RoleUsers;
        /// <summary>
        /// Hash table from role name to pipe separated value of user ids with that role.
        /// </summary>
        private System.Collections.Hashtable   m_RoleUsersBranches;
        /// <summary>
        /// Hashtable from user id to user name.
        /// </summary>
        private System.Collections.Hashtable   m_RoleUserNames;
        protected string                       m_initialSelectedManager = "";
        protected string                       m_initialSelectedLockDesk = "";
        protected string                       m_initialSelectedLenderAE = "";
        protected string                       m_initialSelectedUnderwriter = "";
        protected string                       m_initialSelectedJuniorUnderwriter = "";
        protected string                       m_initialSelectedProcessor = "";
        protected string                       m_initialSelectedJuniorProcessor = "";
        protected string                       m_initialSelectedLoanOfficerAssistant = "";
        protected static string                s_Manager = "Manager";
        protected static string                s_LockDesk = "Lock Desk";
        protected static string                s_LenderAE = "Lender Account Executive";
        protected static string                s_Underwriter = "Underwriter";
        protected static string                s_JuniorUnderwriter = "Junior Underwriter";
        protected static string                s_Processor = "Processor";
        protected static string                s_JuniorProcessor = "Junior Processor";
        protected static string                s_LoanOfficerAssistant = "Loan Officer Assistant";
        protected EmployeePermissionsList      m_EmployeePermissionsList;
        private LosConvert                     m_losConvert = new LosConvert();
        private bool                           m_showPmlBroker = false;
        private BrokerDB                       m_currentBrokerDb;
        private List<UserGroup>                m_UserGroups = new List<UserGroup>();
        protected string                       m_ZeroDollarStr;
        private Dictionary<Guid, EmployeeInfo> employeeInfoByUserId;
        private BrokerUserPermissionsSet       userPermissions;
        #endregion

        protected string PrintGroupEditUrl
        {
            get
            {
                if (Request.Url.ToString().IndexOf("LOAdmin", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    return "/LOAdmin/Broker/LoAdminPrintGroupPermAssignment.aspx";
                }
                else
                {
                    return "/los/Admin/PrintGroupPermAssignment.aspx";
                }
            }
        }

        private BrokerDB CurrentBrokerDB
        {
            get
            {
                if (m_currentBrokerDb == null)
                {
                    m_currentBrokerDb = BrokerDB.RetrieveById(DataBroker);
                }
                return m_currentBrokerDb;
            }
        }

        protected bool UsingLegacyDUCredentials
        {
            get
            {
                return this.CurrentBrokerDB.UsingLegacyDUCredentials;
            }
        }

        public bool EnableTeamsUI
        {
            get { return CurrentBrokerDB.EnableTeamsUI; }
        }

        private AbstractUserPrincipal CurrentPrincipal
        {
            get { return System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal; }
        }


		private string ErrorMessage
		{
			set { Page.ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.', '\r', '\n' ) + "." ); }
		}

		private string CommandToDo
		{
			set { Page.ClientScript.RegisterHiddenField( "m_commandToDo" , value ); }
		}

		public bool IsDeveloper
		{
			set { m_IsDeveloper = value; }
		}

		public bool IsInternal
		{
			set { m_IsInternal = value; }
			get { return m_IsInternal; }
		}

		public bool IsPmlEnabled
		{
			set { ViewState["IsPmlEnabled"] = value; }
			get 
			{ 
				if (ViewState["IsPmlEnabled"] != null)
					return (bool) ViewState["IsPmlEnabled"]; 
				else
				{
					BrokerFeatures bFs = new BrokerFeatures(m_BrokerId);
					return (bool) ( ViewState["IsPmlEnabled"] = bFs.HasFeature( E_BrokerFeatureT.PriceMyLoan ) );
				}
			}
		}

        public bool IsNewPmlUIEnabledForBroker
        {
            get { return CurrentBrokerDB.IsNewPmlUIEnabled; }
        }

        public bool ShowQMStatusInPml2
        {
            get { return CurrentBrokerDB.ShowQMStatusInPml2; }
        }

        protected bool ShowPmlBroker
        {
            set
            {
                m_showPmlBroker = value;
            }
            get
            {
                return m_showPmlBroker;
            }
        }

        protected string PmlAutoLoginOption
        {
            get
            {
                return CurrentBrokerDB.PmlUserAutoLoginOption;
            }
        }

        protected bool IsDUEnabled
        {
            get
            {
                return PmlAutoLoginOption != "Off" ? CurrentBrokerDB.IsPmlDuEnabled : false;
            }
        }

        protected bool IsDOEnabled
        {
            get
            {
                return PmlAutoLoginOption != "Off" ? CurrentBrokerDB.IsPmlDoEnabled : false;
            }
        }
        protected bool IsEnableDocVendorLogin
        {
            get { return E_BrokerBillingVersion.PerTransaction == CurrentBrokerDB.BillingVersion; }
        }

        protected bool IsActiveDirectoryAuthEnabledForLender
        {
            get { return CurrentBrokerDB.IsActiveDirectoryAuthenticationEnabled; }
        }

        protected bool IsEnableMultiFactorAuthentication
        {
            get { return CurrentBrokerDB.IsEnableMultiFactorAuthentication; }
        }
        protected bool IsNewEmp
        {
            get;
            set;
        }

        public Guid DataSource
		{
			set { ViewState.Add( "EmployeeId" , m_EmployeeId = value ); }
		}

		/// <summary>
		/// UserId of the user who is doing the editing
		/// </summary>
		public Guid DataEditor
		{
			set { ViewState.Add( "WhoDoneIt" , m_WhoDoneIt = value ); }
		}

		public Guid DataBroker
		{
			get { return m_BrokerId; }
			set
			{
				ViewState.Add( "BrokerId" , m_BrokerId = value );
			}
		}

		public Guid DataBranch
		{
			set { ViewState.Add( "BranchId" , m_BranchId = value ); }
		}

        protected string m_PopupPath
        {
            get
            {
                if ((Thread.CurrentPrincipal as AbstractUserPrincipal).Type == "I")
                {
                    return "/LOAdmin/Broker/";
                }
                else
                {
                    return "/los/admin/";
                }
            }
        }

		/// <summary>
		/// Initialize this control.
		/// </summary>
		protected void ControlInit( object sender , System.EventArgs a )
		{
            var page = Page as BasePage;
            if (page != null)
            {
                page.RegisterJsScript("json.js");
                page.RegisterJsScript("ServiceCredential.js");
                page.RegisterCSS("ServiceCredential.css");
            }

            m_Zipcode.SmartZipcode( m_City , m_State );
            RegularExpressionValidator1.ValidationExpression = ConstApp.EmailValidationExpression;
			RegularExpressionValidator1.Text = ErrorMessages.InvalidEmailAddress;

            SupportEmailValidator.ValidationExpression = ConstApp.EmailValidationExpression;
            SupportEmailValidator.Text = JsMessages.SpecifyValidSupportEmail;

            var principal = PrincipalFactory.CurrentPrincipal;
            var brokerId = Guid.Empty;
            if (principal is InternalUserPrincipal)
            {
                brokerId = RequestHelper.GetGuid("brokerid");
            }
            else
            {
                brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            }
            var brokerDb = BrokerDB.RetrieveById(brokerId);
            SignaturePlaceHolder.Visible = brokerDb.EnableLqbNonCompliantDocumentSignatureStamp;
            if (brokerDb.IsCustomPricingPolicyFieldEnabled)
            {
                CustomLOPricingPolicyPlaceHolder.Visible = true;
                BuildLoanOfficerPricingPolicyTable(brokerDb.BrokerID);
                Tools.Bind_PricingPolicyFieldValueSourceForLqbUser(
                    this.CustomPricingPolicyFieldValueSource);
            }
            else
            {
                CustomLOPricingPolicyPlaceHolder.Visible = false;
            }
		}

		/// <summary>
		/// Initialize this control.
		/// </summary>

		protected void ControlLoad( object sender , System.EventArgs a )
		{
            // The guidelins contains HTML.
            PasswordGuidelines.Text = LendersOffice.Constants.ConstApp.PasswordGuidelines;

            m_EmployeePermissionsList.DataBroker = DataBroker;
			m_EmployeePermissionsList.DataSource = m_EmployeeId;
			m_EmployeePermissionsList.PermissionType = "employee";
			
			// Get identifiers from the view state.
			if( ViewState[ "HasLogin" ] != null )
				m_HasLogin = ( Boolean ) ViewState[ "HasLogin" ];

            if (ViewState["EmployeeId"] != null)
            {
                m_EmployeeId = (Guid)ViewState["EmployeeId"];
            }

			if( ViewState[ "WhoDoneIt" ] != null )
				m_WhoDoneIt = ( Guid ) ViewState[ "WhoDoneIt" ];

			if( ViewState[ "BrokerId" ] != null )
				m_BrokerId = ( Guid ) ViewState[ "BrokerId" ];

			if( ViewState[ "BranchId" ] != null )
				m_BranchId = ( Guid ) ViewState[ "BranchId" ];

			if (Request["licenseId"] != null)
				m_LicenseId = Request["licenseId"] ;

			if ( ViewState[ "BranchList" ] != null )
				m_BranchList = ( System.Collections.Hashtable ) ViewState[ "BranchList" ];

			if ( ViewState[ "RoleUserList" ] != null )
				m_RoleUsers = ( System.Collections.Hashtable ) ViewState[ "RoleUserList" ];

			if ( ViewState[ "RoleUserNames" ] != null )
				m_RoleUserNames = ( System.Collections.Hashtable ) ViewState[ "RoleUserNames" ];

			if ( ViewState[ "RoleUserBranchList" ] != null )
				m_RoleUsersBranches = ( System.Collections.Hashtable ) ViewState[ "RoleUserBranchList" ];

			String[] roleIDArray = new String[m_EmployeeRoles.Items.Count];
			String[] roleValArray = new String[m_EmployeeRoles.Items.Count];
			String[] roleNameArray = new String[m_EmployeeRoles.Items.Count];
			for(int i = 0; i < m_EmployeeRoles.Items.Count; ++i)
			{
				roleIDArray[i] = m_EmployeeRoles.ClientID + "_" + i;
				roleValArray[i] = m_EmployeeRoles.Items[i].Value;
				roleNameArray[i] = m_EmployeeRoles.Items[i].Text;
			}
			Page.ClientScript.RegisterArrayDeclaration("roleIDArray", "'" + String.Join("','", roleIDArray) + "'");
			Page.ClientScript.RegisterArrayDeclaration("roleValArray", "'" + String.Join("','", roleValArray) + "'");
			Page.ClientScript.RegisterArrayDeclaration("roleNameArray", "'" + String.Join("','", roleNameArray) + "'");

			EmployeeDB empDb = new EmployeeDB( m_EmployeeId , m_BrokerId );
			if( m_EmployeeId != Guid.Empty )
			{
				if ( empDb.Retrieve() == false )
					throw new CBaseException( ErrorMessages.Generic, "Unable to load employee." );
			}
            if (!empDb.IsNew)
            {
                m_UserIdG = empDb.UserID;
                userType = empDb.UserType == 'B' ? UserType.LQB : UserType.TPO;
            }
            IsNewEmp = empDb.IsNew;

            if (m_ip_parsed.Value != string.Empty)
            {
                string[] ips = m_ip_parsed.Value.Split(';');

                foreach (string ip in ips)
                {
                    if (ip == string.Empty) continue;
                    m_ip.Items.Add(new ListItem(ip));
                }
            }

            m_dudoSearch.IsDOEnabled = IsDOEnabled;
            m_dudoSearch.IsDUEnabled = IsDUEnabled;
            ViewState.Add("userid", empDb.UserID);
            ViewState.Add("brokerId", CurrentBrokerDB.BrokerID);
            RegisterRoleIDs();
            EnableNewRolesPanel.Visible = !CurrentBrokerDB.HasMigratedToNewRoles_135241;

            btnRemoveImage.Enabled = (empDb.SignatureKey != Guid.Empty);

            if (!Page.IsPostBack && CurrentBrokerDB.IsCustomPricingPolicyFieldEnabled)
            {
                PopulateHiddenFieldWithPricingPolicyInfo(CurrentBrokerDB, empDb);
                Tools.SetDropDownListValue(
                    this.CustomPricingPolicyFieldValueSource,
                    empDb.CustomPricingPolicyFieldValueSource);
            }

            if (!IsPostBack)
            {
                EmployeeTeams.DataBind(m_EmployeeId, m_BrokerId);
            }

            CellPhoneRequiredImg.Visible = IsEnableMultiFactorAuthentication;
            
        }

        private void CheckShowCertAndIPTable(EmployeeDB empDb)
        {
            if (IsEnableMultiFactorAuthentication &&
                    (empDb.EnabledMultiFactorAuthentication || empDb.EnabledClientDigitalCertificateInstall))
            {
                CertificatesAndIPs.Visible = true;

                m_clientCertificateGrid.DataSource = ClientCertificate.ListByUser(empDb.BrokerID, empDb.UserID);
                m_clientCertificateGrid.DataBind();

                if (empDb.EnabledMultiFactorAuthentication)
                {
                    m_registeredIpDataGrid.Visible = true;
                    m_registeredIpDataGrid.DataSource = UserRegisteredIp.ListByUserId(m_BrokerId, empDb.UserID);
                    m_registeredIpDataGrid.DataBind();
                }
                else
                {
                    m_registeredIpDataGrid.Visible = false;
                }

                this.installCertificateLink.Visible = empDb.EnabledClientDigitalCertificateInstall
                    && PrincipalFactory.CurrentPrincipal.UserId == empDb.UserID;
            }
            else
            {
                CertificatesAndIPs.Visible = false;
            }
        }

        private class EmployeeInfo
        {
            public Guid EmployeeId { get; set; }
            public Guid BranchId { get; set; }
            public bool HasCorporateLevelAccess { get; set; }

            public EmployeeInfo(BrokerLoanAssignmentTable.Spec spec, bool hasCorporateLevelAccess)
            {
                this.EmployeeId = spec.EmployeeId;
                this.BranchId = spec.BranchId;
                this.HasCorporateLevelAccess = hasCorporateLevelAccess;
            }
        }

		private void FillRoleUsersList(E_RoleT roleType , BrokerLoanAssignmentTable roleTable)
		{
			System.Text.StringBuilder usersWithRole = new System.Text.StringBuilder();
            if (roleTable[roleType] != null)
			{
                foreach (BrokerLoanAssignmentTable.Spec eSpec in roleTable[roleType])
				{
					if( eSpec.IsActive == false )
						continue;

					if(!(m_RoleUsers.ContainsKey(eSpec.EmployeeId)))
					{	
						m_RoleUsers.Add(eSpec.EmployeeId, eSpec.BranchId);
						m_RoleUserNames.Add(eSpec.EmployeeId, eSpec.EmployeeName);
					}
					usersWithRole.Append( ( usersWithRole.Length > 0 ?  "|" : "" ) +  eSpec.EmployeeId);

                    if (!this.employeeInfoByUserId.ContainsKey(eSpec.EmployeeId))
                    {
                        var permissions = this.userPermissions[eSpec.EmployeeId];
                        var hasCorporateAccess = false;
                        if (permissions != null)
                        {
                            hasCorporateAccess = permissions.HasPermission(
                                Permission.BrokerLevelAccess);
                        }
                        var info = new EmployeeInfo(eSpec, hasCorporateAccess);
                        this.employeeInfoByUserId.Add(info.EmployeeId, info);
                    }
				}
                string roleName = Role.Get(roleType).Desc;
				m_RoleUsersBranches.Add(roleName, usersWithRole.ToString());
			}
		}

		/// <summary>
		/// Bind this UI to the data.
		/// </summary>

		public override void DataBind()
		{
			// Load the specified employee.

			// 08/14/06 mf - OPM 6104. Modified the error handling here.  We would
			// previously render the form when an exception occurs, only logging it.
			// We now throw, so the user cannot postback the form when the UI controls
			// haven't been properly set.

			try
			{
				// 5/27/2005 kb - Load the employee from the database and bind.
				EmployeeDB empDb = new EmployeeDB( m_EmployeeId , m_BrokerId );
				if( m_EmployeeId != Guid.Empty )
				{
					if ( empDb.Retrieve() == false )
						throw new CBaseException( ErrorMessages.Generic, "Unable to load employee." );
				}    

                if ((Thread.CurrentPrincipal as AbstractUserPrincipal).Type != "I" && empDb.UserType == 'P')
                {
                    // 03/14/11 mp - OPM 63993
                    // LOS user should not be able to edit a PML user in the employeeEdit page
                    // Instead, they should go to the EditPmluser.aspx page
                    // Let internal "I" users through
                    throw new CBaseException(ErrorMessages.Generic, "An LOS user should not be able to edit a PML user in the employeeEdit page");
                }

                //OPM 111008: if the user currently does not have any restrictions, start out with no print groups assigned.
                if (empDb.RestrictPrintGroups)
                {
                    AssignedPrintGroupsJSON.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(from g in empDb.AvailablePrintGroups select g.Id);
                }
                else
                {
                    AssignedPrintGroupsJSON.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize(new List<Guid>());
                }
                AllowPrintingAllDocs.Checked = !empDb.RestrictPrintGroups;
                RestrictPrintingDocs.Checked = empDb.RestrictPrintGroups;

				if( empDb.LoginName != null && empDb.LoginName != "" )
					ViewState.Add( "HasLogin" , m_HasLogin = true );
                m_IsUserQuickPricerEnabled.Checked = empDb.IsQuickPricerEnabled; // 2/5/2009 dd - OPM 12621

                m_IsNewPmlUIEnabled.Checked = empDb.IsNewPmlUIEnabled;

                m_FirstName.Text        = empDb.FirstName;
                m_MiddleName.Text       = empDb.MiddleName;
				m_LastName.Text         = empDb.LastName;
                m_Suffix.Text           = empDb.Suffix;
                m_NameOnLoanDocLock.Checked = empDb.NmOnLoanDocsLckd;
                if (empDb.NmOnLoanDocsLckd)
                {
                    m_NameOnLoanDoc.ReadOnly = false;
                    m_NameOnLoanDoc.Text = empDb.NmOnLoanDocs;
                }
                Tools.BindSuffix(m_Suffix);

                m_Street.Text           = empDb.Address.StreetAddress;
				m_City.Text             = empDb.Address.City;
				m_State.Value           = empDb.Address.State;
				m_Zipcode.Text          = empDb.Address.Zipcode;
				m_Phone.Text            = empDb.Phone;
				m_Fax.Text              = empDb.Fax;
				m_Email.Text            = empDb.Email;
                m_Cell.Text             = empDb.PrivateCellPhone;
                this.IsCellphoneForMultiFactorOnly.Checked = empDb.IsCellphoneForMultiFactorOnly;
				m_Notes.Text            = empDb.NotesByEmployer;
                m_EmployeeIDInCompany.Text   = empDb.EmployeeIDInCompany;

                // OPM 220848
                string supportEmail = empDb.SupportEmail;
                                
                if (string.IsNullOrEmpty(supportEmail))
                {
                    m_supportEmail.Text = string.Empty;
                    m_SupportEmailInternal.Text = string.Empty;
                    OriginalSupportEmail.Value = string.Empty;

                    // If there is no email entered, support should not be able to mark
                    // the "email" as verified. However, KG asked that "Allow Contacting LQB Support"
                    // be enabled so that the user can be marked as allowed to contact support for
                    // reference by support.
                    m_SupportEmailVerified.Enabled = false;
                }
                else
                {
                    m_supportEmail.Text = supportEmail;
                    m_SupportEmailInternal.Text = supportEmail;
                    OriginalSupportEmail.Value = supportEmail;

                    m_SupportEmailVerified.Enabled = true;
                }

                m_SupportEmailVerified.Checked = empDb.SupportEmailVerified;

                if (empDb.SupportEmailVerified)
                {
                    m_SupportEmailStatus.Text = "Confirmed";
                    m_SupportEmailStatus.ForeColor = Color.Green;
                }
                else
                {
                    m_SupportEmailStatus.Text = "Unconfirmed";
                    m_SupportEmailStatus.ForeColor = Color.Red;
                }

                if(CurrentBrokerDB.IsEmployeeStartAndTerminationDEnabled)
                {
                    m_StartAndTerminationDatePanel.Visible = true;
                    m_EmployeeStartD.Text = empDb.EmployeeStartD.HasValue ? empDb.EmployeeStartD.Value.ToShortDateString() : "";
                    m_EmployeeTerminationD.Text = empDb.EmployeeTerminationD.HasValue ? empDb.EmployeeTerminationD.Value.ToShortDateString() : "";
                }

				m_CommissionPointOfLoanAmount.Text = m_losConvert.ToRateString(empDb.CommissionPointOfLoanAmount);
				m_CommissionPointOfGrossProfit.Text =  m_losConvert.ToRateString( empDb.CommissionPointOfGrossProfit );
				m_CommissionMinBase.Text =  m_losConvert.ToMoneyString( empDb.CommissionMinBase, FormatDirection.ToRep );
                m_IsPricingMultipleAppsSupported.Checked = empDb.IsPricingMultipleAppsSupported; // 4/30/2009 dd - OPM 30082
                m_ShowQMStatusInPml2.Checked = empDb.ShowQMStatusInPml2;    // 12/18/2013 dd - OPM 147501

                m_duLogin.Text = empDb.DUAutoLoginName;
                m_doLogin.Text = empDb.DOAutoLoginName;

                this.DUSection.Visible = this.UsingLegacyDUCredentials;
                                
                LicensesPanel.LosIdentifier = empDb.LosIdentifier;
                LicensesPanel.LicenseList = empDb.LicenseInformationList;

                ChumsId.Text = empDb.ChumsId;
                if (IsInternal && empDb.UserType.ToString() == "P")
                {
                    m_showPmlBroker = true;
                    List<PmlBroker> list = PmlBroker.ListAllPmlBrokerByBrokerId(empDb.BrokerID);
                    foreach (PmlBroker p in list)
                    {
                        ListItem li = new ListItem(p.Name, p.PmlBrokerId.ToString());
                        li.Attributes.Add("title", li.Text);
                        m_pmlBroker.Items.Add(li);
                    }

                    if (empDb.PmlBrokerId != Guid.Empty)
                    {
                        Tools.SetDropDownListValue(m_pmlBroker, empDb.PmlBrokerId.ToString());
                    }
                    else
                    {
                        m_pmlBroker.Items.Insert(0, new ListItem("<-- Select Company -->", Guid.Empty.ToString()));
                        m_pmlBroker.SelectedIndex = 0;
                    }
                }

				// 12/10/07 db - OPM 18661
				if(empDb.RatesheetExpirationBypassType == E_RatesheetExpirationBypassType.NoBypass)
					m_noBypass.Checked = true;
				else if(empDb.RatesheetExpirationBypassType == E_RatesheetExpirationBypassType.BypassAllExceptInvCutoff)
					m_bypassExcInvCutoff.Checked = true;
				else
					m_bypassAll.Checked = true;
				
				chkSendLoanEventsEmail.Checked = (empDb.NewOnlineLoanEventNotifOptionT == E_NewOnlineLoanEventNotifOptionT.ReceiveEmail);
                chkSendTasksEmail.Checked = (empDb.TaskRelatedEmailOptionT == E_TaskRelatedEmailOptionT.ReceiveEmail);

				m_UserType.SelectedIndex = m_UserType.Items.IndexOf( m_UserType.Items.FindByValue( empDb.UserType.ToString() ) );
                m_UserType.Enabled = false; // 03/24/14 BB - OPM 78765

				m_CanLogin.Checked = empDb.AllowLogin;
				m_AllowMulti.Checked = empDb.IsSharable;
				m_IsEmployee.Checked = empDb.IsActive;
				m_CanLogin.Checked = empDb.AllowLogin;
             
                // 5/27/2005 kb - Load up the branches for this broker and
                // select the employee's one.

                m_BranchList = new System.Collections.Hashtable();
				m_Branch.Items.Clear();

				using( IDataReader sR = BranchDB.GetBranches( m_BrokerId ) )
				{
					// 09/14/06 mf OPM 2650.  We now load up the branch address
					// information for the client so admins don't have to re-type.

					while ( sR.Read() == true )
					{
						string branchInfo =
							( (string) sR[ "Street" ]).Replace("|","")
							+ "|" + ( (string) sR[ "City" ]).Replace("|","")
							+ "|" + ( (string) sR[ "State" ]).Replace("|","")
							+ "|" + ( (string) sR[ "ZipCode" ]).Replace("|","");
						
						m_Branch.Items.Add ( new ListItem( (string) sR[ "Name" ], ((Guid) sR[ "BranchId" ]).ToString() ) );
						m_BranchList.Add( (Guid) sR[ "BranchID" ], branchInfo );
					}
				}
				ViewState.Add( "BranchList", m_BranchList );

				if( empDb.IsNew == false )
					m_Branch.SelectedIndex = m_Branch.Items.IndexOf( m_Branch.Items.FindByValue( empDb.BranchID.ToString() ) );
				else
				{
					BranchDB branchDb = new BranchDB( m_BranchId , m_BrokerId );
					m_Branch.SelectedIndex = m_Branch.Items.IndexOf( m_Branch.Items.FindByValue( m_BranchId.ToString() ) );
					
					if( branchDb.Retrieve() == true )
					{
						m_Street.Text  = branchDb.Address.StreetAddress;
						m_City.Text    = branchDb.Address.City;
						m_State.Value  = branchDb.Address.State;
						m_Zipcode.Text = branchDb.Address.Zipcode;
						m_Phone.Text = branchDb.Phone;
						m_Fax.Text   = branchDb.Fax;
					}
				}

				// 8/5/2005 kb - Get all the enabled pricing groups for
				// this broker.  We autoselect the current one for this
				// user after we initialize.
                SqlParameter[] parameters = {
                                                new SqlParameter( "@BrokerId" , m_BrokerId ),
                                                new SqlParameter( "@ExternalPriceGroupEnabled" , 1 )
                                            };
				using( IDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "ListPricingGroupByBrokerId" , parameters ) )
				{
					m_LpePriceGroupId.DataValueField = "LpePriceGroupId";
					m_LpePriceGroupId.DataTextField  = "LpePriceGroupName";
					m_LpePriceGroupId.DataSource = sR;
					m_LpePriceGroupId.DataBind();
				}

				m_LpePriceGroupId.Items.Insert( 0 , new ListItem( "<-- Use default at branch level -->" , Guid.Empty.ToString() ) );

				if( m_LpePriceGroupId.Items.FindByValue( empDb.LpePriceGroupID.ToString() ) == null )
					m_LpePriceGroupId.Items.Add( new ListItem( "Current (disabled)" , empDb.LpePriceGroupID.ToString() ) );

				m_LpePriceGroupId.SelectedIndex = m_LpePriceGroupId.Items.IndexOf( m_LpePriceGroupId.Items.FindByValue( empDb.LpePriceGroupID.ToString() ) );

				// 8/10/2004 kb - We added the lending account exec role
				// for price my loan integration.  Each broker user can
				// be associated to one or no lending account executive
				// employee.  Beware!  If you are related to an employee
				// that was a lending account exec, but has since lost
				// that role, the currently displayed employee will be
				// the 'none' employee.  On save, we will nix the existing
				// relationship.
				//
				// 10/21/2004 kb - We now do the same thing with a new
				// employee role: lock desk agent.
				//
				// 6/9/2005 kb - Added manager association as well.
				//
				// 7/13/2005 kb - Loading all the employees by role at
				// once (case 2372) so that we don't join so many times.
				// Note that we are not checking if they can login or
				// their user type.  If they have these roles, we assume
				// they're fine to use.
				//
				// 7/22/2005 kb - Reworked role loading to use new
				// view.  It seems that loading each role individually
				// is better than loading the whole broker when you
				// expect to have *many* loan officers because of pml.
				//
				// 11/15/06 db - OPM 8298 Moved the role binding to the client

				m_RoleUsers = new System.Collections.Hashtable();
				m_RoleUsersBranches = new System.Collections.Hashtable();
				m_RoleUserNames = new System.Collections.Hashtable();
                this.employeeInfoByUserId = new Dictionary<Guid, EmployeeInfo>();
                this.userPermissions = new BrokerUserPermissionsSet(this.m_BrokerId);

				BrokerLoanAssignmentTable roleTable;
				roleTable = new BrokerLoanAssignmentTable();
				roleTable.Retrieve( m_BrokerId , E_RoleT.LenderAccountExecutive );
				roleTable.Retrieve( m_BrokerId , E_RoleT.LockDesk );
				roleTable.Retrieve( m_BrokerId , E_RoleT.Underwriter );
				roleTable.Retrieve( m_BrokerId , E_RoleT.JuniorUnderwriter );
				roleTable.Retrieve( m_BrokerId , E_RoleT.Manager );
				roleTable.Retrieve( m_BrokerId , E_RoleT.Processor );
				roleTable.Retrieve( m_BrokerId , E_RoleT.JuniorProcessor );
				roleTable.Retrieve( m_BrokerId , E_RoleT.LoanOfficerAssistant );

				FillRoleUsersList(E_RoleT.Manager, roleTable);
				FillRoleUsersList(E_RoleT.LockDesk, roleTable);
				FillRoleUsersList(E_RoleT.LenderAccountExecutive, roleTable);
				FillRoleUsersList(E_RoleT.Underwriter, roleTable);
				FillRoleUsersList(E_RoleT.JuniorUnderwriter, roleTable);
				FillRoleUsersList(E_RoleT.Processor, roleTable);
				FillRoleUsersList(E_RoleT.JuniorProcessor, roleTable);
				FillRoleUsersList(E_RoleT.LoanOfficerAssistant, roleTable);

                ViewState.Add("RoleUserList", m_RoleUsers);
                ViewState.Add("RoleUserBranchList", m_RoleUsersBranches);
                ViewState.Add("RoleUserNames", m_RoleUserNames);

                var serializedInfo = SerializationHelper.JsonNetAnonymousSerialize(this.employeeInfoByUserId);
                this.SerializedEmployeeInfo.Value = serializedInfo;

				m_initialSelectedManager = empDb.ManagerEmployeeID.ToString();
				m_initialSelectedLockDesk = empDb.LockDeskEmployeeID.ToString();
				m_initialSelectedLenderAE = empDb.LenderAcctExecEmployeeID.ToString();
				m_initialSelectedUnderwriter = empDb.UnderwriterEmployeeID.ToString();
				m_initialSelectedJuniorUnderwriter = empDb.JuniorUnderwriterEmployeeID.ToString();
				m_initialSelectedProcessor = empDb.ProcessorEmployeeID.ToString();
				m_initialSelectedJuniorProcessor = empDb.JuniorProcessorEmployeeID.ToString();
				m_initialSelectedLoanOfficerAssistant = empDb.LoanOfficerAssistantEmployeeID.ToString();

				// 3/11/2005 kb - Set permissions for loan.  Note that we're
				// binding to a dynamic list.  The list is generated on-the-
				// fly using our current admin list.  We only check the boxes
				// that a user has.  Note that we don't need to update this
				// control anymore when we add a permission.  Nice.
				//
				// 5/27/2005 kb - Moved over to this control.
				//
				// 11/2006 dc - We have now grouped the permissions by similar topic, so if
				// a new permission is added to the overall BrokerUserPermissions() list, it 
				// needs to be added to the particular topic group as well in BranchDB.cs
				// For example, AdminPermissionTableLoans() now contains all permissions that relate
				// specifically to loans.

				BrokerUserPermissions bUp = new BrokerUserPermissions();
				BrokerFeatures        bFs = new BrokerFeatures(m_BrokerId);

				try
				{
					if( m_EmployeeId != Guid.Empty )
						bUp.Retrieve( m_BrokerId , m_EmployeeId );
					else
						bUp.Retrieve( m_BrokerId );
				}
				catch
				{
					bUp.Retrieve( m_BrokerId );
				}

				
				m_InternalPermissions.DataTextField  = "Label";
				m_InternalPermissions.DataValueField = "Value";

				if( m_IsInternal == true )
				{
					m_InternalPermissions.DataSource = m_IsDeveloper ? new DeveloperPermissionTable() : new InternalPermissionTable();
					m_InternalPermissions.DataBind();

					foreach( ListItem lItem in m_InternalPermissions.Items )
					{
						lItem.Selected = bUp.HasPermission( int.Parse( lItem.Value ) );
					}
				}

                if (bFs.HasFeature(E_BrokerFeatureT.PriceMyLoan))
                {
                    IsPmlEnabled = true;
                }

                m_AccessLevel.DataSource = new AccessLevelOptionTable(CurrentBrokerDB.EnableTeamsUI) ;
				m_AccessLevel.DataTextField  = "Label";
				m_AccessLevel.DataValueField = "Value";
				m_AccessLevel.DataBind();

				foreach( ListItem lItem in m_AccessLevel.Items )
				{
					lItem.Selected = bUp.HasPermission( int.Parse( lItem.Value ) );
				}

				// 10/7/2004 kb - I'm binding here to try and get some
				// filtering code taking place in the checkbox list.
				//
				// 5/27/2005 kb - Now move to this control.

				EmployeeRoles eRs = new EmployeeRoles(m_BrokerId, m_EmployeeId);



                m_EmployeeRoles.DataSource = Role.LendingQBRoles.OrderBy(role => role.ModifiableDesc);
				m_EmployeeRoles.DataTextField  = "ModifiableDesc";
				m_EmployeeRoles.DataValueField = "Id";
				m_EmployeeRoles.DataBind();

                foreach (ListItem listItem in m_EmployeeRoles.Items)
                {
                    if (ConstApp.RolesAddedForOPM145015.Contains(new Guid(listItem.Value)))
                    {
                        listItem.Enabled = CurrentBrokerDB.HasMigratedToNewRoles_135241;
                    }
                }

                if (bFs.HasFeature(E_BrokerFeatureT.MarketingTools) == false)
                {
                    m_EmployeeRoles.Items.Remove(m_EmployeeRoles.Items.FindByValue(Role.Get(E_RoleT.CallCenterAgent).Id.ToString()));
                }

				foreach( ListItem lItem in m_EmployeeRoles.Items )
				{
					lItem.Selected = eRs.IsInRole( new Guid( lItem.Value ) );
				}

				// 5/31/2005 kb - Update the password ui to better guide
				// the user in setting a new login or password.

				m_setIsActive = empDb.IsActive;
				m_setAllowLogin = empDb.AllowLogin;

				m_AllowLoginFromMCL.SelectedIndex = m_AllowLoginFromMCL.Items.IndexOf( m_AllowLoginFromMCL.Items.FindByValue( empDb.PmlSiteID != Guid.Empty ? "1" : "0" ) );
				
				// 08/14/06 mf. It is very wasteful make this SP call when these elements
				// will not be displayed so I have made the PML section Internal only
				// to agree with the conditional rendering.
				
				if ( IsInternal )
				{
					m_IsPmlManager.Checked = empDb.IsPmlManager;

                    m_CanContactSupport.Checked = empDb.CanContactSupport;

					try
					{
                        SqlParameter[] parameters1 = {
                                                        new SqlParameter( "@BrokerId" , m_BrokerId )
                                                    };

						using( IDataReader sR = StoredProcedureHelper.ExecuteReader( m_BrokerId, "ListPriceMyLoanManagerByBrokerId" , parameters1 ) )
						{
							m_PmlExternalManagerEmployeeId.DataTextField  = "UserFullName";
							m_PmlExternalManagerEmployeeId.DataValueField = "EmployeeId";
							m_PmlExternalManagerEmployeeId.DataSource = sR;
							m_PmlExternalManagerEmployeeId.DataBind();
						}

						if( m_PmlExternalManagerEmployeeId.Items.Count > 0 )
							m_PmlExternalManagerEmployeeId.Items.Remove( m_PmlExternalManagerEmployeeId.Items.FindByValue( m_EmployeeId.ToString() ) );

						m_PmlExternalManagerEmployeeId.SelectedIndex = m_PmlExternalManagerEmployeeId.Items.IndexOf( m_PmlExternalManagerEmployeeId.Items.FindByValue( empDb.PmlExternalManagerEmployeeId.ToString() ) );
						m_PmlExternalManagerEmployeeId.Items.Insert( 0 , new ListItem( "<-- None -->" , Guid.Empty.ToString() ) );
					}
					catch( Exception e )
					{
						Tools.LogError( "Unable to load pml manager list." , e );
					}
				}

				m_ExternalSiteId.Text = empDb.PmlSiteID.ToString();
				m_UserId.Text         = empDb.UserID.ToString();
                m_UserIdG = empDb.UserID;
                userType = empDb.UserType == 'B' ? UserType.LQB : UserType.TPO;
                m_Id.Text             = empDb.ID.ToString();

                this.ComplianceEaglePanel.Visible = this.CurrentBrokerDB.IsEnableComplianceEagleIntegration;
                var complianceEagleCredentials = ComplianceEagleUserCredentials.Retrieve(empDb.UserID, empDb.BrokerID);
                this.ComplianceEagleUserName.Text = complianceEagleCredentials.UserName;
				
				m_MustChangePasswordAtNextLogin.Checked = false;
				m_PasswordNeverExpires.Checked          = false;
				m_PasswordExpiresOn.Checked             = false;
				m_ExpirationDate.Text = "";

				if( empDb.PasswordExpirationD.CompareTo( SmallDateTime.MaxValue ) >= 0 )
				{
					m_PasswordNeverExpires.Checked = true;
					m_expirationPeriodDisabled = true;
					m_CycleExpiration.Checked = false;
					m_cycleExpirationDisabled = true;
					m_expirationDateDisabled = true;
				}
				else
				{
					if( empDb.PasswordExpirationD.CompareTo( SmallDateTime.MinValue ) <= 0 )
						m_MustChangePasswordAtNextLogin.Checked = true;
					else
					{
						m_ExpirationDate.Text = empDb.PasswordExpirationD.ToShortDateString();
						m_PasswordExpiresOn.Checked = true;
					}

					if( empDb.PasswordExpirationPeriod > 0 )
					{
						switch( empDb.PasswordExpirationPeriod )
						{
							case 15: m_ExpirationPeriod.SelectedIndex = 0;
								break;

							case 30: m_ExpirationPeriod.SelectedIndex = 1;
								break;

							case 45: m_ExpirationPeriod.SelectedIndex = 2;
								break;

							case 60: m_ExpirationPeriod.SelectedIndex = 3;
								break;
						}

						m_CycleExpiration.Checked = true;
					}
					else
						m_CycleExpiration.Checked = false;

					m_expirationPeriodDisabled = !m_CycleExpiration.Checked;
					m_expirationDateDisabled = !m_PasswordExpiresOn.Checked;
					m_ExpirationDate.BackColor = (m_PasswordExpiresOn.Checked)? Color.White:Color.Gainsboro;
					m_cycleExpirationDisabled = false;
				}

				if( empDb.IsNew == true )
					m_MustChangePasswordAtNextLogin.Checked = true;

				if( IsInternal == false )
					m_AllowMulti.Visible = false;
				else
					m_AllowMulti.Visible = true;

                m_ip_parsed.Value = empDb.MustLogInFromTheseIpAddresses;

                if (empDb.EnabledIpRestriction)
                {
                    m_ipRestrictionOn.Checked = true;
                }
                else
                {
                    m_ipRestrictionOff.Checked = true;
                }

				m_Login.Text = empDb.LoginName;

                if (IsActiveDirectoryAuthEnabledForLender)
                {
                    m_IsActiveDirectoryUser.Checked = empDb.IsActiveDirectoryUser;
                    m_IsActiveDirectoryUser.Enabled = empDb.UserType == 'B';
                }
                else
                {
                    m_IsActiveDirectoryUser.Visible = false;
                }

                MultiFactorSectionHeader.Visible = IsEnableMultiFactorAuthentication;
                MultiFactorSection.Visible = IsEnableMultiFactorAuthentication;

                if (IsEnableMultiFactorAuthentication)
                {
                    m_EnabledMultiFactorAuthentication.Disabled = this.CurrentBrokerDB.IsEnableMultiFactorAuthentication || !this.CurrentBrokerDB.AllowUserMfaToBeDisabled;
                    m_EnabledMultiFactorAuthentication.Checked = empDb.EnabledMultiFactorAuthentication;
                    m_EnabledClientDigitalCertificateInstall.Checked = empDb.EnabledClientDigitalCertificateInstall;
                    this.EnableAuthCodeViaSms.Checked = empDb.EnableAuthCodeViaSms;
                    this.EnableAuthCodeViaAuthenticator.Checked = empDb.EnableAuthCodeViaAuthenticator;

                    if (!empDb.EnableAuthCodeViaAuthenticator)
                    {
                        this.EnableAuthCodeViaAuthenticator.Attributes.Add("disabled", "disabled");
                        this.DisableAuthenticator.Disabled = true;
                    }

                }

                CheckShowCertAndIPTable(empDb);

				m_IsAccountLocked.Value = "false";
				SqlParameter[] parameters2 = {
												new SqlParameter("@LoginNm", m_Login.Text),
												new SqlParameter("@Type", "B")
											};
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(m_BrokerId, "GetLoginBlockExpirationDate", parameters2))
                {
                    if (reader.Read())
                    {
                        DateTime nextAvailableLoginTime = DateTime.Parse(reader["LoginBlockExpirationD"].ToString());
                        if (nextAvailableLoginTime.Equals(SmallDateTime.MaxValue))
                            m_IsAccountLocked.Value = "true";
                    }
                }

                // 03/16/2006 BD - licensing info

      

                BillingBind(empDb);
                BindGroupList();
                GroupAssociationList.Value = GetGroupMembership();

                if (m_OriginatorCompensationBaseT.Items.Count == 0)
                {
                    Tools.Bind_PercentBaseLoanAmountsT(m_OriginatorCompensationBaseT);
                }

                m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo.Checked = empDb.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo;
                m_OriginatorCompensationPercent.Text = m_losConvert.ToRateString(empDb.OriginatorCompensationPercent);
                m_OriginatorCompensationFixedAmount.Text = m_losConvert.ToMoneyString(empDb.OriginatorCompensationFixedAmount, FormatDirection.ToRep);
                m_OriginatorCompensationNotes.Text = empDb.OriginatorCompensationNotes;
                m_OriginatorCompensationMinAmount.Text = m_losConvert.ToMoneyString(empDb.OriginatorCompensationMinAmount, FormatDirection.ToRep);
                m_OriginatorCompensationMaxAmount.Text = m_losConvert.ToMoneyString(empDb.OriginatorCompensationMaxAmount, FormatDirection.ToRep);
                Tools.SetDropDownListValue(m_OriginatorCompensationBaseT, empDb.OriginatorCompensationBaseT);
                m_OriginatorCompensationAuditGrid.DataSource = empDb.OriginatorCompensationAuditData.ParseXml(empDb.BrokerID);
                m_OriginatorCompensationAuditGrid.AllowPaging = empDb.OriginatorCompensationAuditData.ParseXml(empDb.BrokerID).Count > 10;
                m_OriginatorCompensationAuditDoesntExist.Visible = empDb.OriginatorCompensationAuditData.ParseXml(empDb.BrokerID).Count == 0;
                m_OriginatorCompensationAuditGrid.DataBind();
                m_ZeroDollarStr = m_losConvert.ToMoneyString(0.0M, FormatDirection.ToRep);

                m_BrokerHasGDMSEnabled.Value = CurrentBrokerDB.IsAllowOrderingGlobalDmsAppraisals.ToString();

                if (CurrentBrokerDB.IsAllowOrderingGlobalDmsAppraisals)
                {
                    m_appraisalVendors.DataSource = AppraisalVendorFactory.GetAvailableVendorsForBroker(m_BrokerId);
                    m_appraisalVendors.DataBind();
                }

                if (IsEnableDocVendorLogin)
                {
                    m_DocumentVendors.DataSource = DocumentVendorFactory.CreateVendorsFor(m_BrokerId);
                    m_DocumentVendors.DataBind();
                }

                if (CurrentBrokerDB.Has4506TIntegration)
                {
                    m_irs4506TVendors.DataSource = Irs4506TVendorConfiguration.ListActiveVendorByBrokerId(m_BrokerId);
                    m_irs4506TVendors.DataBind();
                }

                m_genericFrameworkVendors.DataSource = GenericFrameworkVendor.LoadVendors(this.DataBroker).Where(vendor => vendor.IncludeUsername);
                m_genericFrameworkVendors.DataBind();
            }
			catch
			{
				throw;
			}

		}

        protected void GenericFrameworkVendorBind(object sender, RepeaterItemEventArgs args)
        {
            GenericFrameworkVendor vendor = (GenericFrameworkVendor)args.Item.DataItem;
            var credentials = new GenericFrameworkVendorCredentials(m_EmployeeId, vendor.ProviderID, m_BrokerId);

            ((Label)args.Item.FindControl("GenericFrameworkVendorName")).Text = vendor.Name;
            ((HiddenField)args.Item.FindControl("ProviderId")).Value = vendor.ProviderID;
            ((HiddenField)args.Item.FindControl("StoredUsername")).Value = credentials.Username;
            ((TextBox)args.Item.FindControl("Username")).Text = credentials.Username;
        }

        protected void AppraisalVendorBind(object sender, RepeaterItemEventArgs args)
        {
            AppraisalVendorConfig vendor = (AppraisalVendorConfig)args.Item.DataItem;
            ((Label)args.Item.FindControl("VendorName")).Text = vendor.VendorName;

            HiddenField vendorField = (HiddenField)args.Item.FindControl("VendorId");
            TextBox accountId = (TextBox)args.Item.FindControl("LoginAccountId");
            TextBox username = (TextBox)args.Item.FindControl("LoginUserName");
            TextBox password = (TextBox)args.Item.FindControl("LoginPassword");

            vendorField.Value = vendor.VendorId.ToString();

            AppraisalVendorCredentials login = new AppraisalVendorCredentials(m_BrokerId, m_EmployeeId, vendor.VendorId);
            accountId.Text = login.AccountId;
            username.Text = login.UserName;

            args.Item.FindControl("AccountIdField").Visible = vendor.UsesAccountId;
        }

        protected void Irs4506TVendorBind(object sender, RepeaterItemEventArgs args)
        {
            Irs4506TVendorConfiguration vendor = (Irs4506TVendorConfiguration)args.Item.DataItem;
            ((Label)args.Item.FindControl("VendorName")).Text = vendor.VendorName;

            HiddenField vendorField = (HiddenField)args.Item.FindControl("VendorId");
            TextBox accountId = (TextBox)args.Item.FindControl("LoginAccountId");
            TextBox username = (TextBox)args.Item.FindControl("LoginUserName");
            TextBox password = (TextBox)args.Item.FindControl("LoginPassword");

            vendorField.Value = vendor.VendorId.ToString();

            Irs4506TVendorCredential credential = Irs4506TVendorCredential.Retrieve(m_BrokerId, m_UserIdG, vendor.VendorId);
            accountId.Text = credential.AccountId;
            username.Text = credential.UserName;

            args.Item.FindControl("AccountIdField").Visible = vendor.IsUseAccountId;
        }

        protected void m_GroupTableBody_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView rowView = e.Item.DataItem as DataRowView;
            var name = (string)rowView["Name"];
            var description = (string)rowView["Description"];

            EncodedLiteral nameLiteral = (EncodedLiteral)e.Item.FindControl("Name");
            nameLiteral.Text = name;

            EncodedLiteral descriptionLiteral = (EncodedLiteral)e.Item.FindControl("Description");
            descriptionLiteral.Text = description;
        }

        private List<DocumentVendorBrokerSettings> x_documentVendorBrokerSettings = null;
        private List<DocumentVendorBrokerSettings> m_documentVendorBrokerSettings
        {
            get
            {
                if (x_documentVendorBrokerSettings == null)
                {
                    x_documentVendorBrokerSettings = DocumentVendorBrokerSettings.ListAllForBroker(m_BrokerId);
                }
                return x_documentVendorBrokerSettings;
            }
        }

        protected void DocumentVendorBind(object sender, RepeaterItemEventArgs args)
        {
            IDocumentVendor vendor = (IDocumentVendor)args.Item.DataItem;
            HiddenField m_VendorId = (HiddenField)args.Item.FindControl("m_VendorId");
            CheckBox enabled = (CheckBox)args.Item.FindControl("m_Enabled");
            TextBox companyId = (TextBox)args.Item.FindControl("m_CompanyId");
            TextBox password = (TextBox)args.Item.FindControl("m_Password");
            TextBox username = (TextBox)args.Item.FindControl("m_Username");
            Label m_EnabledLabel = (Label)args.Item.FindControl("m_EnabledLabel");

            m_VendorId.Value = vendor.Config.VendorId.ToString();
            var docVendorBrokerSettings = m_documentVendorBrokerSettings
                .Where((s) => s.VendorId == vendor.Config.VendorId)
                .FirstOrDefault();
            var servicesLinkNm = docVendorBrokerSettings == null ? "" : "(" + docVendorBrokerSettings.ServicesLinkNm + ")";
            m_EnabledLabel.Text = string.Format("Allow Accessing {0} {1}", 
                vendor.Skin.VendorName, servicesLinkNm);

            var login = vendor.CredentialsFor(m_UserIdG, m_BranchId);

            enabled.Checked = login.UserEnabled;

            if (!login.UserEnabled)
            {
                companyId.Attributes["disabled"] = "disabled";
                password.Attributes["disabled"] = "disabled";
                username.Attributes["disabled"] = "disabled";
                companyId.Style["background-color"] = "gainsboro";
                password.Style["background-color"] = "gainsboro";
                username.Style["background-color"] = "gainsboro";
            }
            else
            {
                BindCredentials(login, username, password, companyId);
            }

            args.Item.FindControl("UsernamePanel").Visible = vendor.Config.UsesUsername;
            args.Item.FindControl("PasswordPanel").Visible = vendor.Config.UsesPassword;
            args.Item.FindControl("CompanyIdPanel").Visible = vendor.Config.UsesAccountId;

        }


        private void BillingBind(EmployeeDB empDb)
        {
            // gf OPM 84666 - hide licensing info for PTM clients
            if (CurrentBrokerDB.BillingVersion == E_BrokerBillingVersion.PerTransaction)
            {
                m_PreventLicensing.Visible = false;
                FirstVersionLicenseScript.Visible = false;
                return;
            }

            if (empDb.AllowLogin && CurrentBrokerDB.BillingVersion != E_BrokerBillingVersion.New)
            {
                LendersOfficeApp.ObjLib.Licensing.BrokerUser brokerUser = new LendersOfficeApp.ObjLib.Licensing.BrokerUser(empDb.BrokerID, empDb.UserID, "N/A");
                if (brokerUser.HasValidLicense())
                {
                    m_LicenseId = brokerUser.License.LicenseId.ToString();
                    m_LicenseNumber.Text = brokerUser.License.LicenseNumber.ToString();
                }
            }
            else if (empDb.AllowLogin && CurrentBrokerDB.BillingVersion == E_BrokerBillingVersion.New)
            {
                // 4/29/2015 dd - No lender should be on this billing version.
                throw new NotSupportedException();
            }


            bool IsVersionTwoBillingOn = false;// CurrentBrokerDB.BillingVersion == E_BrokerBillingVersion.New;
            m_PreventLicensing.Visible = !IsVersionTwoBillingOn;
            FirstVersionLicenseScript.Visible = !IsVersionTwoBillingOn;
        }

        // POD
        [Serializable]
        internal class UserGroup : IComparable<UserGroup>
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }

            public int CompareTo(UserGroup group)
            {
                return Name.CompareTo(group.Name);
            }
        }

        private void BindGroupList()
        {
            
            m_UserGroups = new List<UserGroup>();

            var groupList = GroupDB.GetAllGroups(m_BrokerId, GroupType.Employee);
            foreach (var group in groupList)
            {
                m_UserGroups.Add(new UserGroup() { Id = group.GroupId, Description = group.Description, Name = group.GroupName });
            }

            m_UserGroups.Sort();
            m_GroupTableBody.DataSource = ViewGenerator.Generate(m_UserGroups);
            m_GroupTableBody.DataBind();

        }

        private string GetGroupMembership()
        {
            var membershipList = GroupDB.GetGroupMembershipsForEmployee(m_BrokerId, m_EmployeeId);
            string groupIds = string.Empty;
            foreach (var item in membershipList)
            {
                if (item.IsInGroup == 1)
                {
                    groupIds += (groupIds != string.Empty ? "," : "") + item.GroupId.ToString();
                }
            }

            return groupIds;
        }

		private void RegisterRoleIDs()
		{
			if ( m_RoleUsers.Count > 0 )
			{
                // Csv of all user ids.
				StringBuilder idList = new StringBuilder();
                // Csv of all branch ids. In the same order as idList, such 
                // that a user id at index 'i' in idList belongs to branch at
                // index 'i' in usersInBranchList.
				StringBuilder usersInBranchList = new StringBuilder();
                // Csv of role descriptions.
				StringBuilder roleBranchesList = new StringBuilder();
                // Csv of pipe separated user ids. In the same order as 
                // roleBranchesList, such that a role description at index 'i'
                // in roleBranchesList is associated with the user ids at index
                // 'i' in the roleIdList.
				StringBuilder roleIdList = new StringBuilder();
                // Csv of all user ids.
				StringBuilder roleUserIDs = new StringBuilder();
                // Csv of all user names. In the same order as roleIdList, such
                // that a user id at index 'i' in roleUserIDs is associated with
                // the name at index 'i' of roleUserNames.
				StringBuilder roleUserNames = new StringBuilder();

				foreach ( object key in m_RoleUsers.Keys )
				{
                    if (idList.Length != 0)
                    {
                        idList.Append(",");
                        usersInBranchList.Append(",");
                    }
					
                    idList.Append( AspxTools.JsString(key.ToString()) );
                    usersInBranchList.Append(AspxTools.JsString(m_RoleUsers[key].ToString()));
				}

				foreach( object key in m_RoleUsersBranches.Keys)
				{
                    if (roleBranchesList.Length != 0)
                    {
                        roleBranchesList.Append(",");
                        roleIdList.Append(",");
                    }

                    roleBranchesList.Append( AspxTools.JsString(key.ToString()) );
                    roleIdList.Append( AspxTools.JsString(m_RoleUsersBranches[key].ToString()));
				}

				foreach( object key in m_RoleUserNames.Keys)
				{
                    if (roleUserIDs.Length != 0)
                    {
                        roleUserIDs.Append(",");
                        roleUserNames.Append(",");
                    }

					roleUserIDs.Append( AspxTools.JsString(key.ToString()) );
                    roleUserNames.Append( AspxTools.JsString(m_RoleUserNames[key].ToString()) );
				}

				Page.ClientScript.RegisterArrayDeclaration( "RoleIdList", idList.ToString() );
				Page.ClientScript.RegisterArrayDeclaration( "RoleUsersInBranchList", usersInBranchList.ToString() );
				Page.ClientScript.RegisterArrayDeclaration( "RoleNameList", roleBranchesList.ToString() );
				Page.ClientScript.RegisterArrayDeclaration( "RoleNameIdList", roleIdList.ToString() );
				Page.ClientScript.RegisterArrayDeclaration( "RoleUserIDList", roleUserIDs.ToString() );
				Page.ClientScript.RegisterArrayDeclaration( "RoleUserNameList", roleUserNames.ToString() );
			}
		}

		/// <summary>
		/// Set variables that allow the UI to know which controls to disable/enable so that
		/// postbacks are not always necessary just for UI changes
		/// </summary>
		private void initPwUI()
		{
			m_cycleExpirationDisabled = (m_PasswordNeverExpires.Checked)?true:false;
			m_expirationPeriodDisabled = (m_CycleExpiration.Checked)?false:true;
			m_expirationDateDisabled = (m_PasswordExpiresOn.Checked)?false:true;
		}

        /// <summary>
        /// Commit the UI's state back to the specified employee.
        /// </summary>


        private void SaveData()
        {
            //Check for doc magic credentials; must have both login & password or neither
            foreach (RepeaterItem item in m_DocumentVendors.Items)
            {
                if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
                    continue;
                HiddenField vendorField = (HiddenField)item.FindControl("m_VendorId");
                TextBox username = (TextBox)item.FindControl("m_Username");
                TextBox password = (TextBox)item.FindControl("m_Password");
                CheckBox enabled = (CheckBox)item.FindControl("m_Enabled");
                Guid vendorId = new Guid(vendorField.Value);
                if (enabled.Checked && vendorId == Guid.Empty)
                {
                    if (string.IsNullOrEmpty(username.Text) && !string.IsNullOrEmpty(password.Text)
                        ||
                        !string.IsNullOrEmpty(username.Text) && string.IsNullOrEmpty(password.Text))
                    {
                        ErrorMessage = "DocMagic login must contain either both username and password or neither.";
                        return;
                    }
                    break;
                }
            }

            EmployeeDB empDb = new EmployeeDB(m_EmployeeId, m_BrokerId);
            empDb.WhoDoneIt = m_WhoDoneIt;

            StringBuilder sb = new StringBuilder();

            using (CStoredProcedureExec spExec = new CStoredProcedureExec(m_BrokerId))
            {
                spExec.BeginTransactionForWrite();
                try
                {
                    // 6/1/2005 kb - Get the employee's details so that we
                    // save back latest for everything we don't update here.

                    if (m_EmployeeId != Guid.Empty)
                    {
                        empDb.Retrieve(spExec);
                        sb.AppendLine("Other Employee Info: " + empDb.FullName + " (loginnm=" + empDb.LoginName + " userid=" + empDb.UserID + ") edited by id=" + m_WhoDoneIt);
                    }
                    else
                    {
                        sb.AppendLine("Other Employee Info: userid=" + empDb.UserID + " edited by id=" + m_WhoDoneIt);
                    }

                    empDb.FirstName = m_FirstName.Text;
                    empDb.MiddleName = m_MiddleName.Text;
                    empDb.LastName = m_LastName.Text;
                    empDb.Suffix = m_Suffix.Text;
                    empDb.NmOnLoanDocsLckd = m_NameOnLoanDocLock.Checked;
                    empDb.NmOnLoanDocs = m_NameOnLoanDoc.Text;

                    empDb.ChumsId = ChumsId.Text;

                    empDb.DOAutoLoginName = m_doLogin.Text;
                    if (m_doPw.Text != "")
                        empDb.DOAutoPassword = m_doPw.Text;

                    if (this.UsingLegacyDUCredentials)
                    {
                        empDb.DUAutoLoginName = m_duLogin.Text;
                        if (m_duPw.Text != "")
                            empDb.DUAutoPassword = m_duPw.Text;
                    }

                    empDb.BranchID = GetSafeGuid(m_Branch.SelectedItem.Value);

                    if (m_ChangeLogin.Checked == true)
                    {
                        // Updating the login and password.  We assume that
                        // these values were validated prior to saving.

                        // Allow changing of id without password change.  We already
                        // validated the login name change.
                        if (m_Password.Text.TrimWhitespaceAndBOM() != "" && m_Confirm.Text != "")
                            empDb.Password = m_Password.Text.TrimWhitespaceAndBOM();

                        empDb.LoginName = m_Login.Text.TrimWhitespaceAndBOM();
                        sb.AppendLine("Login updated");

                        // OPM 106855 - Active Directory user bit (LQB only)
                        if (IsActiveDirectoryAuthEnabledForLender)
                        {
                            empDb.IsActiveDirectoryUser = m_IsActiveDirectoryUser.Checked;

                            // This is a hack that avoids modifying the password db fields to allow null.
                            // Authentication will be done via the lender's Active Directory database.
                            // If the lender changes the LQB user from AD to regular LQB user, password validation will force them to reset the password at that time.
                            // - The net result is that the pw generated here will never be used.
                            if (empDb.IsActiveDirectoryUser)
                            {
                                if (empDb.IsNew)
                                {
                                    string sPwHash, sPwSalt;
                                    empDb.Password = EncryptionHelper.GetPasswordAndPBKDF2HashSalt(out sPwHash, out sPwSalt);
                                }

                                // Set pw to never expire so that any process that references pw expiration after login will not boot the user
                                m_PasswordNeverExpires.Checked = true;
                            }
                        }
                    }

                    // OPM 113855 - Don't clear these bits when not viewed/saved from LOAdmin.
                    if (IsInternal)
                    {
                        empDb.IsQuickPricerEnabled = m_IsUserQuickPricerEnabled.Checked; // 2/5/2009 dd - OPM 12621
                        empDb.IsNewPmlUIEnabled = m_IsNewPmlUIEnabled.Checked; // 11/8/12 gf - OPM 94411
                        empDb.IsPricingMultipleAppsSupported = m_IsPricingMultipleAppsSupported.Checked;
                        empDb.ShowQMStatusInPml2 = m_ShowQMStatusInPml2.Checked;

                        // OPM 220848
                        empDb.SetSupportEmail(m_SupportEmailInternal.Text, m_SupportEmailVerified.Checked);
                        empDb.CanContactSupport = m_CanContactSupport.Checked;
                    }

                    empDb.LosIdentifier = LicensesPanel.LosIdentifier;
                    empDb.LicenseInformationList = LicensesPanel.LicenseList;
                    // TODO: Would like to log licenses

                    empDb.Address.StreetAddress = m_Street.Text;
                    empDb.Address.City = m_City.Text;
                    empDb.Address.State = m_State.Value;
                    empDb.Address.Zipcode = m_Zipcode.Text;

                    empDb.Email = m_Email.Text.TrimWhitespaceAndBOM();
                    empDb.Phone = m_Phone.Text.TrimWhitespaceAndBOM();
                    empDb.PrivateCellPhone = m_Cell.Text.TrimWhitespaceAndBOM();
                    empDb.IsCellphoneForMultiFactorOnly = this.IsCellphoneForMultiFactorOnly.Checked;
                    empDb.Fax = m_Fax.Text.TrimWhitespaceAndBOM();
                    empDb.EmployeeIDInCompany = m_EmployeeIDInCompany.Text.TrimWhitespaceAndBOM();

                    if (CurrentBrokerDB.IsEmployeeStartAndTerminationDEnabled)
                    {
                        empDb.EmployeeStartD = null;
                        empDb.EmployeeTerminationD = null;

                        DateTime candidateStartD;
                        if (DateTime.TryParse(m_EmployeeStartD.Text.TrimWhitespaceAndBOM(), out candidateStartD))
                        {
                            empDb.EmployeeStartD = candidateStartD;
                        }

                        DateTime candidateTerminationD;
                        if (DateTime.TryParse(m_EmployeeTerminationD.Text.TrimWhitespaceAndBOM(), out candidateTerminationD))
                        {
                            empDb.EmployeeTerminationD = candidateTerminationD;
                        }
                    }

                    ListItem loanOfficerListItem = m_EmployeeRoles.Items.FindByText("Loan Officer");
                    if (loanOfficerListItem == null)
                    {
                        Tools.LogBug("Cannot find loan officer in list. It should be in the list.");
                    }

                    empDb.CommissionPointOfLoanAmount = m_losConvert.ToRate(m_CommissionPointOfLoanAmount.Text);
                    empDb.CommissionMinBase = m_losConvert.ToMoney(m_CommissionMinBase.Text);
                    empDb.CommissionPointOfGrossProfit = m_losConvert.ToRate(m_CommissionPointOfGrossProfit.Text);

                    if (IsInternal && empDb.UserType.ToString() == "P")
                    {
                        empDb.PmlBrokerId = GetSafeGuid(m_pmlBroker.SelectedItem.Value);
                    }

                    // 12/10/07 db - OPM 18661
                    if (m_noBypass.Checked)
                    {
                        empDb.RatesheetExpirationBypassType = E_RatesheetExpirationBypassType.NoBypass;
                    }
                    else if (m_bypassExcInvCutoff.Checked)
                    {
                        empDb.RatesheetExpirationBypassType = E_RatesheetExpirationBypassType.BypassAllExceptInvCutoff;
                    }
                    else
                    {
                        empDb.RatesheetExpirationBypassType = E_RatesheetExpirationBypassType.BypassAll;
                    }

                    if (chkSendLoanEventsEmail.Checked)
                    {
                        empDb.NewOnlineLoanEventNotifOptionT = E_NewOnlineLoanEventNotifOptionT.ReceiveEmail;
                    }
                    else
                    {
                        empDb.NewOnlineLoanEventNotifOptionT = E_NewOnlineLoanEventNotifOptionT.DontReceiveEmail;
                    }

                    if (chkSendTasksEmail.Checked)
                    {
                        empDb.TaskRelatedEmailOptionT = E_TaskRelatedEmailOptionT.ReceiveEmail;
                    }
                    else
                    {
                        empDb.TaskRelatedEmailOptionT = E_TaskRelatedEmailOptionT.DontReceiveEmail;
                    }

                    empDb.LpePriceGroupID = GetSafeGuid(m_LpePriceGroupId.SelectedItem.Value);
                    empDb.ManagerEmployeeID = GetSafeGuid(m_SelectedManager.Value);
                    empDb.LenderAcctExecEmployeeID = GetSafeGuid(m_SelectedLenderAE.Value);
                    empDb.LockDeskEmployeeID = GetSafeGuid(m_SelectedLockDesk.Value);
                    empDb.UnderwriterEmployeeID = GetSafeGuid(m_SelectedUnderwriter.Value);
                    empDb.JuniorUnderwriterEmployeeID = GetSafeGuid(m_SelectedJuniorUnderwriter.Value);
                    empDb.ProcessorEmployeeID = GetSafeGuid(m_SelectedProcessor.Value);
                    empDb.JuniorProcessorEmployeeID = GetSafeGuid(m_SelectedJuniorProcessor.Value);
                    empDb.LoanOfficerAssistantEmployeeID = GetSafeGuid(m_SelectedLoanOfficerAssistant.Value);

                    if (this.ComplianceEaglePanel.Visible)
                    {
                        var savedCredentials = ComplianceEagleUserCredentials.Retrieve(empDb.UserID, empDb.BrokerID);
                        savedCredentials.UserName = this.ComplianceEagleUserName.Text;
                        if (!string.IsNullOrEmpty(this.ComplianceEaglePassword.Text))
                        {
                            savedCredentials.Password = this.ComplianceEaglePassword.Text;
                        }

                        savedCredentials.Save();
                    }

                    if (m_PasswordNeverExpires.Checked == true)
                    {
                        empDb.PasswordExpirationD = SmallDateTime.MaxValue;
                        empDb.PasswordExpirationPeriod = 0;
                    }
                    else
                    {
                        if (m_MustChangePasswordAtNextLogin.Checked == true)
                            empDb.PasswordExpirationD = SmallDateTime.MinValue;

                        if (m_PasswordExpiresOn.Checked == true)
                            empDb.PasswordExpirationD = DateTime.Parse(m_ExpirationDate.Text);

                        if (m_CycleExpiration.Checked == true)
                            empDb.PasswordExpirationPeriod = Int32.Parse(m_ExpirationPeriod.SelectedItem.Value);
                        else
                            empDb.PasswordExpirationPeriod = 0;
                    }

                    if (m_AllowLoginFromMCL.SelectedItem.Value == "1")
                    {
                        if (empDb.PmlSiteID == Guid.Empty)
                            empDb.PmlSiteID = Guid.NewGuid();
                    }
                    else
                        empDb.PmlSiteID = Guid.Empty;

                    if (m_IsPmlManager.Checked == true)
                    {
                        empDb.PmlExternalManagerEmployeeId = Guid.Empty;
                        empDb.IsPmlManager = true;
                    }
                    else
                    {
                        if (m_PmlExternalManagerEmployeeId.SelectedItem != null)
                        {
                            empDb.PmlExternalManagerEmployeeId = GetSafeGuid(m_PmlExternalManagerEmployeeId.SelectedItem.Value);
                        }

                        empDb.IsPmlManager = false;
                    }

                    empDb.IsSharable = m_AllowMulti.Checked;
                    empDb.AllowLogin = m_CanLogin.Checked;
                    empDb.IsActive = m_IsEmployee.Checked;

                    String roleSet = "";
                    if (m_EmployeeRoles.SelectedItem != null)
                    {
                        // Build up the set of all selected roles.  On save, we
                        // clear out the existing roles and replace them with
                        // this set.

                        String roleNames = "";

                        foreach (ListItem rItem in m_EmployeeRoles.Items)
                        {
                            if (rItem.Selected == false)
                                continue;

                            roleNames += ":" + rItem.Text;

                            if (roleSet.Length > 0)
                            {
                                roleSet += "," + rItem.Value;
                            }
                            else
                            {
                                roleSet += rItem.Value;
                            }
                        }

                        empDb.Roles = roleSet;
                    }

                    if (m_UserType.SelectedItem != null)
                    {
                        empDb.UserType = m_UserType.SelectedItem.Value[0];
                    }

                    empDb.NotesByEmployer = m_Notes.Text;
                    empDb.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo.Checked;
                    empDb.OriginatorCompensationPercent = m_losConvert.ToRate(m_OriginatorCompensationPercent.Text);
                    empDb.OriginatorCompensationMinAmount = m_losConvert.ToMoney(m_OriginatorCompensationMinAmount.Text);
                    empDb.OriginatorCompensationMaxAmount = m_losConvert.ToMoney(m_OriginatorCompensationMaxAmount.Text);
                    empDb.OriginatorCompensationFixedAmount = m_losConvert.ToMoney(m_OriginatorCompensationFixedAmount.Text);
                    empDb.OriginatorCompensationNotes = m_OriginatorCompensationNotes.Text;
                    empDb.OriginatorCompensationBaseT = (E_PercentBaseT)Convert.ToInt32(m_OriginatorCompensationBaseT.SelectedValue);

                    if (IsEnableMultiFactorAuthentication)
                    {
                        empDb.EnabledMultiFactorAuthentication = m_EnabledMultiFactorAuthentication.Checked;
                        empDb.EnableAuthCodeViaSms = this.EnableAuthCodeViaSms.Checked;
                        empDb.EnabledClientDigitalCertificateInstall = m_EnabledClientDigitalCertificateInstall.Checked;
                       
                        if (this.IsDisableAuthenticator.Value == "disabled")
                        {
                            empDb.EnableAuthCodeViaAuthenticator = false;
                            this.IsDisableAuthenticator.Value = "";
                            this.DisableAuthenticator.Disabled = true;
                        }
                    }

                    // For saving cert and id deletions
                    string certIdsString = certIDsToDelete.Value;
                    string[] certIds = certIdsString.Split(',');

                    foreach (string certId in certIds)
                    {
                        if (!string.IsNullOrEmpty(certId))
                        {
                            Guid certGuid = new Guid(certId);
                            ClientCertificate.Delete(m_BrokerId, empDb.UserID, certGuid);
                        }
                    }

                    string IpIdsString = IPsToDelete.Value;
                    string[] IpIds = IpIdsString.Split(',');

                    foreach (string IpId in IpIds)
                    {
                        if (!string.IsNullOrEmpty(IpId))
                            UserRegisteredIp.Delete(m_BrokerId, empDb.UserID, int.Parse(IpId));
                    }

                    if (CurrentBrokerDB.IsCustomPricingPolicyFieldEnabled)
                    {
                        SetCustomLOPricingPolicyFields(empDb);
                        int selectedPolicySource = Convert.ToInt32(
                            this.CustomPricingPolicyFieldValueSource.SelectedValue);
                        empDb.CustomPricingPolicyFieldValueSource =
                            (E_PricingPolicyFieldValueSource)selectedPolicySource;
                    }

                    empDb.EnabledIpRestriction = m_ipRestrictionOn.Checked;
                    empDb.MustLogInFromTheseIpAddresses = m_ip_parsed.Value; // This value is used in a stored procedure, CheckIpForUser, in RequestHelper
                    empDb.CanBeMemberOfMultipleTeams = (EmployeeTeams.FindControl("CanBeMemberOfMultipleTeams") as CheckBox).Checked;

                    if (AllowPrintingAllDocs.Checked)
                    {
                        empDb.AvailablePrintGroups = null;
                    }
                    else
                    {
                        empDb.AssignPrintGroupsById(AssignedPrintGroupsJSON.Value);
                    }

                    try
                    {
                        // Save the employee now, then update the permissions.
                        // New employees' rows will be part of the transaction
                        // after this save.

                        empDb.Save(spExec, PrincipalFactory.CurrentPrincipal);
                    }
                    catch (CBaseException exec)
                    {
                        if (exec.DeveloperMessage.IndexOf("Cannot insert duplicate key in object", 0, StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            ErrorMessage = ErrorMessages.LoginNameAlreadyInUseFunction(empDb.LoginName);
                            return;
                        }
                        throw;
                    }


                    if (empDb.LoginName != null && empDb.LoginName != "")
                    {
                        // We must update the permissions outside of the save,
                        // as part of the transactions, because new employees
                        // lack the table entries until they are created.
                        //
                        // The user must have a valid login account to set
                        // permissions.

                        try
                        {
                            BrokerUserPermissions bUp = new BrokerUserPermissions(empDb.BrokerID, empDb.ID, spExec);

                            if (m_IsInternal == true)
                            {
                                foreach (ListItem pItem in m_InternalPermissions.Items)
                                {
                                    sb.AppendLine("Access level permission " + pItem.Text + ": " + pItem.Selected);
                                    bUp.SetPermission(int.Parse(pItem.Value), pItem.Selected);
                                }
                            }

                            foreach (ListItem aItem in m_AccessLevel.Items)
                            {
                                sb.AppendLine("Access level permission " + aItem.Text + ": " + aItem.Selected);
                                bUp.SetPermission(int.Parse(aItem.Value), aItem.Selected);
                            }
                            bUp.Update(spExec);
                        }
                        catch (Exception e)
                        {
                            Tools.LogError("Unable to save employee " + m_EmployeeId + " for broker " + m_BrokerId + ".", e);
                            throw;
                        }
                    }

                    try
                    {
                        spExec.CommitTransaction();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError("Failed to commit transaction of saving employee " + empDb.ID + ".", e);
                    }

                    //save the permissions
                    if (empDb.LoginName != null && empDb.LoginName != "")
                    {
                        try
                        {
                            m_EmployeePermissionsList.DataSource = empDb.ID;
                            m_EmployeePermissionsList.Apply("employee");

                            try
                            {
                                BrokerUserPermissions bUp = new BrokerUserPermissions(empDb.BrokerID, empDb.ID);
                                string[] names = Enum.GetNames(typeof(Permission));
                                var values = Enum.GetValues(typeof(Permission));
                                for (int i = 0; i < values.Length; i++)
                                {
                                    sb.AppendLine("Permission " + names[i] + "=" + bUp.HasPermission((int)values.GetValue(i)));
                                }
                            }
                            catch (Exception e)
                            {
                                Tools.LogError("Error when trying to log employee permissions.", e);
                            }
                        }
                        catch (Exception e)
                        {
                            Tools.LogError("Failed to commit transaction of saving employee " + empDb.ID + ".", e);
                        }
                    }

                    if (m_SendWelcomeEmail.Value.ToLower() == "true")
                    {
                        // 6/15/2005 kb - Per case 949, we now send out a welcome
                        // letter.  Notify this new employee that he was just added
                        // to lendingqb.  If we fail to send, that's ok.

                        try
                        {
                            NewEmployeeAdded empAdded = new NewEmployeeAdded();
                            empAdded.Initialize(empDb.ID);
                            empAdded.Send();
                        }
                        catch
                        {
                        }
                    }

                    DataSource = empDb.ID;
                    IsNewEmp = empDb.IsNew;
                    Guid userId = empDb.UserID;
                    if (userId == Guid.Empty)
                    {
                        SqlParameter[] parameters = {
                                                        new SqlParameter("@EmployeeId", empDb.ID)
                                                    };
                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(empDb.BrokerID, "GetUserByEmployeeId", parameters))
                        {
                            if (reader.Read())
                            {
                                try
                                {
                                    userId = new Guid(reader["UserId"].ToString());
                                }
                                catch
                                { }
                            }
                        }
                    }

                    SaveLicenseData(userId, empDb);
                    if (empDb.UserType == 'B')
                    {
                        SaveGroupData();
                        sb.AppendLine("Groups are now " + GroupAssociationList.Value);
                    }

                }
                catch (Exception e)
                {
                    Tools.LogError("Unable to save employee " + m_EmployeeId + " for broker " + m_BrokerId + ".", e);
                    spExec.RollbackTransaction();
                    throw;
                }

            }

            string invalidCredentialsErrors = null;
            foreach (RepeaterItem item in m_DocumentVendors.Items)
            {
                if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
                    continue;
                HiddenField vendorField = (HiddenField)item.FindControl("m_VendorId");
                TextBox accountId = (TextBox)item.FindControl("m_CompanyId");
                TextBox username = (TextBox)item.FindControl("m_Username");
                TextBox password = (TextBox)item.FindControl("m_Password");
                CheckBox enabled = (CheckBox)item.FindControl("m_Enabled");
                Guid vendorId = new Guid(vendorField.Value);
                if (enabled.Checked)
                {
                    var vendor = DocumentVendorFactory.Create(m_BrokerId, vendorId);
                    try
                    {
                        SaveCredentials(vendor.CredentialsFor(empDb.UserID, empDb.BranchID), username, password, accountId);
                    }
                    catch (ArgumentException ae) when (ae.Message == "Can't assign a password such that its encrypted form is longer than 150 characters.")
                    {
                        invalidCredentialsErrors += $"Password for {vendor.Skin.VendorName} is too long.{Environment.NewLine}";
                    }
                    catch (ArgumentException)
                    {
                        invalidCredentialsErrors += $"Please re-enter the credentials for {vendor.Skin.VendorName}.{Environment.NewLine}";
                    }
                }
                else
                {
                    VendorCredentials.DisableForUser(m_BrokerId, empDb.UserID, vendorId);
                }
            }

            if (invalidCredentialsErrors != null)
            {
                this.ErrorMessage = invalidCredentialsErrors;
            }

            //Save Appraisal Credentials using separate stored procedure
            foreach (RepeaterItem item in m_appraisalVendors.Items)
            {
                if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
                    continue;
                HiddenField vendorField = (HiddenField)item.FindControl("VendorId");
                TextBox accountId = (TextBox)item.FindControl("LoginAccountId");
                TextBox username = (TextBox)item.FindControl("LoginUserName");
                TextBox password = (TextBox)item.FindControl("LoginPassword");
                Guid vendorId = new Guid(vendorField.Value);
                AppraisalVendorCredentials login = new AppraisalVendorCredentials(m_BrokerId, m_EmployeeId, vendorId);
                login.AccountId = accountId.Text;
                login.UserName = username.Text;
                if(!string.IsNullOrEmpty(password.Text))
                    login.Password = password.Text;
                login.SaveCredentials();
            }

            // Save Generic Framework Credentials using separate stored procedure.
            foreach (RepeaterItem item in m_genericFrameworkVendors.Items)
            {
                if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
                {
                    continue;
                }

                HiddenField providerField = (HiddenField)item.FindControl("ProviderId");
                HiddenField storedUsername = (HiddenField)item.FindControl("StoredUsername");
                string ProviderId = providerField.Value;
                TextBox username = (TextBox)item.FindControl("Username");

                // Only hit the database if the username has been changed.
                if (!string.Equals(storedUsername.Value, username.Text))
                {
                    var login = new GenericFrameworkVendorCredentials(m_EmployeeId, ProviderId, username.Text, m_BrokerId);
                    login.SaveCredentials();
                }
            }

            foreach (RepeaterItem item in m_irs4506TVendors.Items)
            {
                if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
                    continue;
                HiddenField vendorField = (HiddenField)item.FindControl("VendorId");
                TextBox accountId = (TextBox)item.FindControl("LoginAccountId");
                TextBox username = (TextBox)item.FindControl("LoginUserName");
                TextBox password = (TextBox)item.FindControl("LoginPassword");
                Guid vendorId = new Guid(vendorField.Value);
                Irs4506TVendorCredential login = Irs4506TVendorCredential.Retrieve(m_BrokerId, m_UserIdG, vendorId);
                login.AccountId = accountId.Text;
                login.UserName = username.Text;
                if (!string.IsNullOrEmpty(password.Text))
                {
                    login.SetPassword(password.Text);
                }
                login.Save();

            }

            EmployeeTeams.Save(m_EmployeeId, m_BrokerId);
            EmployeeTeams.DataBind(m_EmployeeId, m_BrokerId);
            Tools.LogVerbose(sb.ToString());
		}

        private void RegisterEmployeeIdValues()
        {
            var serviceCredentials = ServiceCredential.GetAllEmployeeServiceCredentials(m_BrokerId, m_EmployeeId, ServiceCredentialService.All);
            var serviceCredentialsJson = LendersOffice.Common.ObsoleteSerializationHelper.JsonSerialize(serviceCredentials);
            Page.ClientScript.RegisterHiddenField("ServiceCredentialsJson", serviceCredentialsJson);
            Page.ClientScript.RegisterHiddenField("EmployeeId", m_EmployeeId.ToString());
            Page.ClientScript.RegisterHiddenField("IsNewEmployee", IsNewEmp.ToTrueFalse());
        }

        private void SaveCredentials(IDocumentVendorCredentials credentials, TextBox username, TextBox password, TextBox clientId)
        {
            credentials.UserName = username.Text;
            if (password.Text != ConstAppDavid.FakePasswordDisplay)
            {
                credentials.Password = password.Text;
            }
            if (clientId != null)
            {
                credentials.CustomerId = clientId.Text;
            }
            credentials.SaveUserCredentials();
        }


        private void SaveLicenseData(Guid userId, EmployeeDB empDb)
        {
            if (userId == Guid.Empty)
            {
                return;
            }
            if (CurrentBrokerDB.BillingVersion == E_BrokerBillingVersion.Classic)
            {
                // Update the LicenseId if it changed
                LendersOfficeApp.ObjLib.Licensing.BrokerUser brokerUser = new LendersOfficeApp.ObjLib.Licensing.BrokerUser(CurrentBrokerDB.BrokerID, userId, "N/A");
                Guid oldLicenseId = Guid.Empty;
                if (brokerUser.License != null)
                {
                    oldLicenseId = brokerUser.License.LicenseId;
                }
                Guid newLicenseId = Guid.Empty;
                if (empDb.AllowLogin && m_LicenseId != null && m_LicenseId.Length > 0)
                {
                    newLicenseId = new Guid(m_LicenseId);
                }
                if (oldLicenseId != newLicenseId)
                {
                    if (newLicenseId != Guid.Empty)
                    {
                        brokerUser.AssignLicense(new LendersOfficeApp.ObjLib.Licensing.License(CurrentBrokerDB.BrokerID, newLicenseId));
                    }
                    else
                    {
                        brokerUser.UnassignLicense();
                    }
                }
            }
            else if (CurrentBrokerDB.BillingVersion == E_BrokerBillingVersion.New)
            {
                // 4/29/2015 dd - Remove this billing version option.
                throw new NotSupportedException();
            }

          
        }

        /// <summary>
        /// Convert string to Guid. Return Guid.Empty if string is invalid.
        /// </summary>
        /// <param name="s">Input value.</param>
        /// <returns>A Guid representation. Guid.Empty if string is invalid.</returns>
        private Guid GetSafeGuid(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return Guid.Empty;
            }

            try
            {
                return new Guid(s);
            }
            catch (FormatException)
            {
            }

            return Guid.Empty;
        }

        private void SaveGroupData()
        {
            GroupDB.UpdateEmployeeGroups(m_BrokerId, m_EmployeeId , GroupAssociationList.Value, PrincipalFactory.CurrentPrincipal);
        }

        private void ShowAlertError(string error)
        {
            Page.ClientScript.RegisterStartupScript(typeof(EmployeeEdit), "error", @"<script type=""text/javascript"">alert("+ AspxTools.JsString(error) +"); </script>");
        }
		/// <summary>
		/// This function saves the currently selected relationship settings so that if the save fails and the page
		/// has to reload, the currently selected relationship settings will not disappear
		/// </summary>
		private void RecordCurrentRelationshipSettings()
		{
			m_initialSelectedManager = m_SelectedManager.Value;
			m_initialSelectedLockDesk = m_SelectedLockDesk.Value;
			m_initialSelectedLenderAE = m_SelectedLenderAE.Value;
			m_initialSelectedUnderwriter = m_SelectedUnderwriter.Value;
			m_initialSelectedJuniorUnderwriter = m_SelectedJuniorUnderwriter.Value;
			m_initialSelectedProcessor = m_SelectedProcessor.Value;
			m_initialSelectedJuniorProcessor = m_SelectedJuniorProcessor.Value;
			m_initialSelectedLoanOfficerAssistant = m_SelectedLoanOfficerAssistant.Value;
		}

        protected void m_OriginatorCompensationAuditGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            m_OriginatorCompensationAuditGrid.CurrentPageIndex = e.NewPageIndex;
            EmployeeDB empDb = new EmployeeDB(m_EmployeeId, m_BrokerId);
            empDb.Retrieve();
            m_OriginatorCompensationAuditGrid.DataSource = empDb.OriginatorCompensationAuditData.ParseXml(empDb.BrokerID);
            m_OriginatorCompensationAuditGrid.DataBind();
        }

        protected void m_OriginatorCompensationAuditGrid_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Pager)
            {
                Label lblPagerText = new Label();
                lblPagerText.Text = "Page: ";

                if (e.Item.Controls[0].FindControl("lblPagerText") == null)
                {
                    e.Item.Controls[0].Controls.AddAt(0, lblPagerText);
                }
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
        }
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.ControlLoad);
			this.Init += new System.EventHandler(this.ControlInit);

		}
		#endregion

		/// <summary>
		/// Handle UI click event.
		/// </summary>

		protected void ApplyClick( object sender , System.EventArgs a )
		{
            if (HasDuplicateSupportEmail())
            {
                m_AlertMessage.Text = JsMessages.SpecifyUniqueSupportEmail;

                return;
            }

            // Handle saving the employee's content.
            initPwUI();
            RecordCurrentRelationshipSettings();

            m_AlertMessage.Text = "";
            m_setIsActive = m_IsEmployee.Checked;
            m_setAllowLogin = m_CanLogin.Checked;
            SaveData();
            DataBind();
            m_Dirty.Value = "0";
	
		}

		/// <summary>
		/// Handle UI click event.
		/// </summary>

		protected void OkClick( object sender , System.EventArgs a )
		{
            if (HasDuplicateSupportEmail())
            {
                m_AlertMessage.Text = JsMessages.SpecifyUniqueSupportEmail;

                return;
            }

			// Handle saving the employee's content.
			initPwUI();
			RecordCurrentRelationshipSettings();
            m_AlertMessage.Text = "";
            m_setIsActive = m_IsEmployee.Checked;
            m_setAllowLogin = m_CanLogin.Checked;
            SaveData();
            DataBind();

            CommandToDo = "Close";
		}

		protected override void OnPreRender(System.EventArgs e)
		{
			if((m_UserType.SelectedItem != null) && (m_UserType.SelectedItem.Value != "B"))
			{
				m_AssignLicenseLink.Enabled = false;
				m_UnassignLicenseLink.Enabled = false;
			}

            m_OriginatorCompensationAuditGrid.PageIndexChanged += new DataGridPageChangedEventHandler(this.m_OriginatorCompensationAuditGrid_PageIndexChanged);

			// 09/16/06 mf - OPM 2650. We register the branch address info on the client
			// so admins can click to fill it.
			RegisterBranchInfo();
            imgSignature.ImageUrl = "UserSignature.aspx?eid=" + m_EmployeeId + "&brokerid=" + m_BrokerId + "&t=" + DateTime.Now.ToString();
            this.RegisterEmployeeIdValues();
		}

        private void BindCredentials(IDocumentVendorCredentials credentials, TextBox username, TextBox password, TextBox clientId)
        {
            username.Text = credentials.UserName;
            if (String.Empty != credentials.Password)
            {
                password.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
            }
            else
            {
                password.Attributes.Add("value", String.Empty);
            }
            clientId.Text = credentials.CustomerId;
        }
        
		private void RegisterBranchInfo()
		{
			if ( m_BranchList.Count > 0 )
			{
				System.Text.StringBuilder idList = new System.Text.StringBuilder();
				System.Text.StringBuilder addrList = new System.Text.StringBuilder();

				foreach ( object key in m_BranchList.Keys )
				{
					idList.Append( ( idList.Length > 0 ? ",'" : "'" ) + key.ToString() + "'" );
					addrList.Append( ( addrList.Length > 0 ?  ",'" : "'" ) +  Utilities.SafeJsString( m_BranchList[ key ].ToString() ) +  "'" );
				}

				Page.ClientScript.RegisterArrayDeclaration( "BranchIdList", idList.ToString() );
				Page.ClientScript.RegisterArrayDeclaration( "BranchInfoList", addrList.ToString() );
			}
		}
        private void DisplayUploadError(string msg)
        {
            divError.Visible = true;
            divError.InnerText = msg;
        }

        protected void btnSignatureUpload_Click(object sender, System.EventArgs e)
        {
            m_setIsActive = m_IsEmployee.Checked;
            m_setAllowLogin = m_CanLogin.Checked;

            if (!this.CurrentBrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                DisplayUploadError("Signature is not enabled.");
                return;
            }

            if (signatureUpload.HasFile)
            {
                try
                {
                    if (!ImageUtils.IsValidImageType(signatureUpload.PostedFile.ContentType))
                    {

                        DisplayUploadError("Incorrect file type, must be a JPEG, PNG, GIF or BMP file.");
                        return;
                    }
                    if (signatureUpload.PostedFile.ContentLength > MAX_IMG_SIZE)
                    {
                        DisplayUploadError("Selected file is too large. Images must be smaller than 100KB.");
                        return;
                    }                  
                    
                    
                    System.Drawing.Image im = System.Drawing.Image.FromStream(signatureUpload.PostedFile.InputStream);

                    //Do not resize the image. case 69393
                    //System.Drawing.Image im = ImageUtils.ResizeImage(signatureUpload.PostedFile.InputStream, 160, 18);

                    Guid signatureKey = Guid.NewGuid();
                    if (!ImageUtils.SaveImageToFileDB(signatureKey.ToString(), im))
                    {
                        DisplayUploadError("Unable to save file. Please try again.");
                        return;
                    }

                    //Save filename with employee's account
                    EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(m_BrokerId, m_EmployeeId, CurrentBrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);

                    if (signInfo.SignatureKey != Guid.Empty)
                    {
                        signInfo.DeleteSignature(); //this would delete from the fileDB too
                    }

                    signInfo.SaveSignatureKey(signatureKey);
                    var empDb = EmployeeDB.CreateEmployeeDBWithOnlyLoginNmAndUserIdByBrokerAndEmployeeId(m_BrokerId, m_EmployeeId);
                    Tools.LogVerbose("EMPLOYEE EDIT: " + empDb.LoginName + "(" + empDb.UserID + ") signature updated by " + m_WhoDoneIt);

                    RestoreAfterPostback();
                }
                catch (Exception ex)
                {
                    DisplayUploadError("The image file could not be uploaded.");
                    Tools.LogError(ex);
                }
            }
            else
            {
                DisplayUploadError("Please specify a valid file");
                return;
            }
            divError.Visible = false;
            btnRemoveImage.Enabled = true;            
        }
        
        protected void OnRemoveSignature_Click(object sender, System.EventArgs e)
        {
            if (!this.CurrentBrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(m_BrokerId, m_EmployeeId, CurrentBrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
            signInfo.DeleteSignature();
            btnRemoveImage.Enabled = false;
            
            m_setIsActive = m_IsEmployee.Checked;
            m_setAllowLogin = m_CanLogin.Checked;

            var empDb = EmployeeDB.CreateEmployeeDBWithOnlyLoginNmAndUserIdByBrokerAndEmployeeId(m_BrokerId, m_EmployeeId);
            Tools.LogVerbose("EMPLOYEE EDIT: " + empDb.LoginName + "(" + empDb.UserID + ") signature removed by " + m_WhoDoneIt);

            RestoreAfterPostback();
        }  
        
        private void RestoreAfterPostback()
        {
            LicensesPanel.LicenseList = LicensesPanel.LicenseList;
        }    

        protected void BuildLoanOfficerPricingPolicyTable(Guid brokerId)
        {
            var htmlTableRows = CustomPmlFieldUtilities.GetLoanOfficerPricingTableRows(brokerId);

            foreach (var row in htmlTableRows)
            {
                CustomLOPricingPolicyTable.Rows.Add(row);
            }
        }

        private void PopulateHiddenFieldWithPricingPolicyInfo(BrokerDB brokerDB, EmployeeDB empDB)
        {
            var customPricingPolicyFields = CustomPmlFieldUtilities.GetCustomPricingPolicyInfoForEmployee(brokerDB, empDB);
            CustomLOPPFieldSettings.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize<List<CustomPmlFieldInfo>>(customPricingPolicyFields);
        }

        private void SetCustomLOPricingPolicyFields(EmployeeDB empDB)
        {
            var serializedFieldInfo = CustomLOPPFieldSettings.Value;
            if (string.IsNullOrEmpty(serializedFieldInfo)) return;

            var fieldSettings = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<CustomPmlFieldInfo>>(serializedFieldInfo);
            foreach (var field in fieldSettings)
            {
                switch (field.Id)
                {
                    case "CustomPricingPolicyField1": empDB.CustomPricingPolicyField1Fixed = field.Value; break;
                    case "CustomPricingPolicyField2": empDB.CustomPricingPolicyField2Fixed = field.Value; break;
                    case "CustomPricingPolicyField3": empDB.CustomPricingPolicyField3Fixed = field.Value; break;
                    case "CustomPricingPolicyField4": empDB.CustomPricingPolicyField4Fixed = field.Value; break;
                    case "CustomPricingPolicyField5": empDB.CustomPricingPolicyField5Fixed = field.Value; break;
                    default: throw new CBaseException(ErrorMessages.Generic, "Unhandled LO pricing policy field id.");
                }
            }
        }

        private bool HasDuplicateSupportEmail()
        {
            // If no email is specified or the support email has not changed, 
            // there is no duplicate email.
            if (string.IsNullOrEmpty(m_SupportEmailInternal.Text) ||
                m_SupportEmailInternal.Text.Equals(OriginalSupportEmail.Value, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            foreach (var connection in DbConnectionInfo.ListAll())
            {

                SqlParameter[] parameters = { new SqlParameter("@Email", m_SupportEmailInternal.Text) };
                using (var reader = StoredProcedureHelper.ExecuteReader(connection, "BrokerUser_FindEmployeesWithGivenSupportEmail", parameters))
                {
                    while (reader.Read())
                    {
                        Guid? empId = reader["EmployeeId"] as Guid?;

                        if (empId.HasValue && empId.Value != m_EmployeeId)
                        {
                            return true;
                        }
                    }

                    return false;
                }
            }

            return false;
        }
    }
}