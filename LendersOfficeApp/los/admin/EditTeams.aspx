﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditTeams.aspx.cs" Inherits="LendersOfficeApp.Los.Admin.EditTeams" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">        
        .Header
        {
            background-color: #777;
            font-weight:bold;
            color:White;
            padding:3px;
        }
        
        .bottomDiv
        {
            text-align:center;
        }
        
        .NoRole
        {
            color:#b83131; 
        }
        
        td
        {
            vertical-align:top;
            white-space:nowrap;
        }
        
        body
        {
            background-color: Gainsboro;
        }
        
        span.label
        {
            display:inline-block;
            margin:5px, 10px;
            width:90px;
        }
        
        .Highlight, #TeamsGrid tr:hover
        {
            background-color:#7fc7ff !important;
        }
        
        .mainDiv .tableContainer
        {
            height:420px;
            overflow:auto;
        }
        
        #EmployeesGrid, #TeamsGrid
        {
            min-width:100%;
        }
        
    </style>    
    <script type="text/javascript">
        var currentTeamId = "";
        var removedTeams = new Array();
        var removedEmployees = new Array();
        var addedEmployees = new Array();
        var isEditingTeam = false;
        var IsDirty = false;

        function init() {
            disableUI();
            checkRequired();

            $("#EditDiv #TeamName, #EditDiv #TeamRole, #EditDiv #TeamNotes").change(function() {
                IsDirty = true;
            });
        }

        function loadTeam(teamId) {
            var  ToLoad = true;

            if (IsDirty) {
                if (confirm('Continue and ignore changes?') == false)
                    ToLoad = false;

                else {
                    if (currentTeamId == null || currentTeamId == "") {
                        removeTeam(currentTeamId);
                    }
                    removedEmployees = new Array();
                    addedEmployees = new Array();
                    IsDirty = false;
            }

            }
            if (ToLoad) {
                clearEmployeeTable();
                enableUI();
                isEditingTeam = true;

                currentTeamId = teamId;
                var args = new Object();
                args["teamId"] = teamId;
                gService.EditTeamsService.callAsyncSimple("LoadTeam", args, function (result) {
                    if (!result.error) {
                        document.getElementById("TeamName").value = result.value["teamName"];
                        document.getElementById("TeamRole").value = result.value["roleT"];
                        document.getElementById("TeamNotes").value = result.value["teamNotes"];

                        //generate the Employee Table
                        var EmployeesTable = $("#EmployeesGrid");
                        for (var i = 0; i < result.value["numberOfEmployees"]; i++) {
                            var row = document.createElement("tr");
                            var className = $("#EmployeesGrid tr").length % 2 == 1 ? "GridItem HoverHighlist" : "GridAlternatingItem HoverHighlight";
                            row.className = className;
                            row.id = result.value["employeeId" + i];

                            var editCell = document.createElement("td");
                            var editLink = document.createElement("a");
                            editLink.href = "#";
                            $(editLink).text("edit");
                            editCell.appendChild(editLink);
                            row.appendChild(editCell);

                            var deleteCell = document.createElement("td");
                            var deleteLink = document.createElement("a");
                            deleteLink.href = "#";
                            $(deleteLink).text("remove");
                            deleteCell.appendChild(deleteLink);
                            row.appendChild(deleteCell);

                            $(editLink).on("click", null, { EmployeeId: result.value["employeeId" + i] }, editEmployeeHandler);
                            $(deleteLink).on("click", null, { EmployeeId: result.value["employeeId" + i] }, removeEmployeeHandler);

                            var employeeIdCell = document.createElement("td");
                            var employeeIdSpan = document.createElement("span");
                            employeeIdSpan.className = result.value["employeeId" + i];
                            $(employeeIdSpan).text(result.value["EmployeeIDInCompany" + i]);
                            employeeIdCell.appendChild(employeeIdSpan);
                            row.appendChild(employeeIdCell);

                            var EmployeeNameCell = document.createElement("td");
                            var EmployeeNameSpan = document.createElement("span");
                            EmployeeNameSpan.className = "EmployeeName";
                            $(EmployeeNameSpan).text(result.value["employeeName" + i]);
                            EmployeeNameCell.appendChild(EmployeeNameSpan);
                            row.appendChild(EmployeeNameCell);

                            var BranchNameCell = document.createElement("td");
                            var BranchNameSpan = document.createElement("span");
                            BranchNameSpan.className = "BranchName";
                            $(BranchNameSpan).text(result.value["branchName" + i]);
                            BranchNameCell.appendChild(BranchNameSpan);
                            row.appendChild(BranchNameCell);

                            if (result.value["HasRole" + i] == "False") {
                                EmployeeNameCell.className = "NoRole";
                                BranchNameCell.className = "NoRole";
                                var ExpLink = document.createElement("a");
                                ExpLink.href = "#";
                                $(ExpLink).text("?");
                                $(ExpLink).on("click", null, { EmployeeName: result.value["employeeName" + i] }, showExplanation);
                                EmployeeNameCell.appendChild(ExpLink);
                            }

                            EmployeesTable.append(row);

                        }

                        UpdateTeamRoleDDL();
                    }
                });                
            }
        }

        function addTeam() {
        var  ToLoad = true;

            if (IsDirty) {
                if (confirm('Continue and ignore changes?') == false)
                    ToLoad = false;

                else {
                    if (currentTeamId == null || currentTeamId == "") {
                        removeTeam(currentTeamId);
                    }
                    IsDirty = false;
            }

            }
            if (ToLoad) {
                enableUI();
                currentTeamId = "";
                isEditingTeam = true;
                IsDirty = true;
                clearEmployeeTable();

                $(".Highlight").removeClass("Highlight");
                //to-do: create the arrays to hold the data for the new Teams
                var test = $("#TeamsGrid tr");
                var className = $("#TeamsGrid tr").length % 2 == 1 ? "GridItem HoverHighlist" : "GridAlternatingItem HoverHighlight";
                className += " Highlight";

                $("#TeamRole").val($("#RolesFilter").val());
                $("#TeamNotes").val("");
                $("#TeamName").val("");
                checkRequired();
                //create the table row
                var table = $("#TeamsGrid");
                var row = document.createElement("tr");
                row.className = className;

                var editCell = document.createElement("td");
                var editLink = document.createElement("a");
                editLink.href = "#";
                editLink.className = "editLink";
                $(editLink).text("edit");
                editCell.appendChild(editLink);
                row.appendChild(editCell);

                var deleteCell = document.createElement("td");
                var deleteLink = document.createElement("a");
                deleteLink.href = "#";
                deleteLink.className = "deleteLink";
                $(deleteLink).text("delete");
                $(deleteLink).click(function() { removeTeam(""); });
                deleteCell.appendChild(deleteLink);
                row.appendChild(deleteCell);

                var roleCell = document.createElement("td");
                var roleSpan = document.createElement("span");
                roleSpan.className = "TeamRole";
                $(roleSpan).css("display", "none");
                roleCell.appendChild(roleSpan);
                row.appendChild(roleCell);

                var nameCell = document.createElement("td");
                var nameSpan = document.createElement("span");
                nameSpan.className = "TeamName";
                nameCell.appendChild(nameSpan);
                row.appendChild(nameCell);


                table.append(row);

                UpdateTeamRoleDDL();
            }
        }

        function showExplanation(event) {
            alert(event.data.EmployeeName + " does not have the " + $("#TeamRole option:selected").text() + " role.");
        }

        function FilterTeams() {
            var skippedHeader = false;
            $("#TeamsGrid tr").each(function() {
                if (skippedHeader) {
                    $(this).css("display", "");
                }
                skippedHeader = true;
            });

            var RoleFilter = $("#RolesFilter").val();
            var TeamNameFilter = $("#TeamNameFilter").val();

            $(".TeamName").each(function() {
            if ($(this).text().toLowerCase().indexOf(TeamNameFilter.toLowerCase()) < 0) {
                    $(this).parent().parent().css("display", "none");
                }
            });

            $(".TeamRole").each(function() {
                if(RoleFilter != -1 && $(this).text() != "" + RoleFilter) {
                    $(this).parent().parent().css("display", "none");
                }
            });

        }
        
        function FilterEmployees(){
            var skippedHeader = false;
            $("#EmployeesGrid tr").each(function() {
                if (skippedHeader) {
                    $(this).css("display", "");
                }
                skippedHeader = true;
            });

            var EmployeeNameFilter = $("#TeamMemberName").val();
            
            $(".EmployeeName").each(function() {
            if ($(this).text().toLowerCase().indexOf(EmployeeNameFilter.toLowerCase()) < 0) {
                    $(this).parent().parent().css("display", "none");
                }
            });
        }

        function clearEmployeeTable() {
            var passedHeader = false;
            $("#EmployeesGrid tr").each(function() {
                if (passedHeader) {
                    $(this).remove();
                }
                passedHeader = true;
            });
        }

        function OnCancelClick() {
            onClosePopup();
        }

        function OnOKClick() {
            Save(onClosePopup);
        }

        function OnApplyClick() {
            Save();
        }

        function Save(callback) {
            var data = {};
            data["TeamId"] = currentTeamId;
            data["TeamNotes"] = $("#TeamNotes").val();
            data["TeamName"] = $("#TeamName").val();
            data["TeamRole"] = $("#TeamRole").val();
            data["RemovedEmployees"] = JSON.stringify(removedEmployees);
            data["RemovedTeams"] = JSON.stringify(removedTeams);
            data["AddedEmployees"] = JSON.stringify(addedEmployees);
            data["IsEditingTeam"] = isEditingTeam;

            //update the table
            gService.EditTeamsService.callAsyncSimple("Save", data, function (result) {
                if (!result.error) {
                    removedEmployees = new Array();
                    addedEmployees = new Array();
                    removedTeams = new Array();
                    if (result.value["LoseLoans"] == "True") {
                        var ret = confirm("Changing the role of an existing team may have unexpected effects and is not recommended. Do you want to proceed?");
                        if (ret) {
                            data["HasChecked"] = true;
                            gService.EditTeamsService.callAsyncSimple("Save", data, function (result) {
                                SaveFinished(result, callback);
                            });
                        }
                    }
                    else {
                        SaveFinished(result);
                        if (typeof callback === "function") {
                            callback();
                        }
                    }
                }
            });            
        }

        function SaveFinished(result, callback) {
            IsDirty = false;
            $("#btnOK").attr("disabled", true);
            $("#btnApply").attr("disabled", true);
            if (isEditingTeam) {
                var $row = getCurrentTeamRow(currentTeamId)

                if (currentTeamId == "") {
                    currentTeamId = result.value["teamId"];

                    $row.attr("id", "row" + currentTeamId);
                    var editLink = $row.find(".editLink");
                    editLink.bind("click", { teamId: currentTeamId }, editTeamHandler);

                    var deleteLink = $row.find(".deleteLink");
                    deleteLink.bind("click", { teamId: currentTeamId }, deleteTeamHandler);

                }

                //update the values
                var roleSpan = $row.find(".TeamRole").text($("#TeamRole").val());
                var roleCell = $row.find(".TeamRole").parent().text($("#TeamRole option:selected").text());
                roleCell.append(roleSpan);
                $row.find(".TeamName").text($("#TeamName").val());
            }            

            if (typeof callback === "function") {
                callback();
            }
        }

        function editTeamHandler(event) {
            loadTeam(event.data.teamId);
        }

        function deleteTeamHandler(event) {
            removeTeam(event.data.teamId);
        }

        function getCurrentTeamRow(teamId) {
            if (teamId != null && teamId != "") {

                $row = $("a[onclick*='" + teamId + "']").parent().parent();
                if ($row.length > 0)
                    return $row;
                else
                    return $row = $("#row" + teamId);
            }
            else {

                var table = $("#TeamsGrid tr");
               return $(table[table.length - 1]);
            }
        }

        function editEmployee(employeeId) {

            if (IsDirty) {
                if (confirm("Do you want to save your changes?")) {
                    Save(function () {
                        showModal('/los/admin/EditEmployee.aspx?cmd=edit&employeeId=' + employeeId, null, null, null, null, {
                            width: 1000,
                            height: 800,
                            hideCloseButton: true
                        });
                    });                    
                }
            }
            else {
                showModal('/los/admin/EditEmployee.aspx?cmd=edit&employeeId=' + employeeId, null, null, null, null, {
                        width: 1000,
                        height: 800,
                        hideCloseButton:true
                    });
            }
        }

        function removeTeam(teamId) {
            if (teamId == currentTeamId) {
                disableUI();
                clearEmployeeTable();
                $("#TeamName").val("") ;
                $("#TeamRole").val("-1");
                $("#TeamNotes").val("");
            }

            //if the current team is new, just remove it from the table

            getCurrentTeamRow(teamId).remove();
            if (teamId != null) {
                removedTeams.push(teamId);
            }

            if (teamId == "") {
                IsDirty = false;
            }


            checkRequired();

        }

        function editEmployeeHandler(event) {
            editEmployee(event.data.EmployeeId);
            checkRequired();
        }
        function removeEmployeeHandler(event) {
            removeEmployee(event.data.EmployeeId);
            checkRequired();
        }

        function AddEmployee() {
            var employeeData = new Array();
            
            showModal('/los/EmployeeRole.aspx?roleValue=' + $("#TeamRole").val() + '&roledesc=' + $("#TeamRole option:selected").text() + '&show=search&mode=team', null, null, null, function(arg){
                if (arg.OK) {
                    var className = $("#EmployeesGrid tr").length % 2 == 1 ? "GridItem HoverHighlist" : "GridAlternatingItem HoverHighlight";
                    var hasCheckedEmployee = false;
                    //arg.EmployeeId
                    var data = {};
                    data["EmployeeId"] = arg.EmployeeId;
                    data["TeamId"] = currentTeamId;
                    data["RoleId"] = $("#TeamRole").val();
                    data["hasCheckedEmployee"] = hasCheckedEmployee;
                    gService.EditTeamsService.callAsyncSimple("AddEmployee", data, function (result) {
                        employeeData.push(arg.EmployeeId);

                        if (!result.error) {
                            if (result.value["HasPrimary"] == "True") {
                                if (result.value["CanBeMemberOfMultipleTeams"] == "False") {
                                    var ret = confirm(arg.EmployeeName + " is currently assigned to " + result.value["PrimaryTeamName"] + ". Assign to " + $("#TeamName").val() + " instead?");
                                    if (ret) {
                                        data["hasCheckedEmployee"] = true;
                                        data["MakePrimary"] = true;
                                        gService.EditTeamsService.callAsyncSimple("AddEmployee", data, function (result) {
                                            employeeData.push("Primary");
                                            AddEmployeeFinished(result, employeeData, arg, className);
                                        });                                        
                                    }
                                }
                                else {
                                    //create the popup
                                    var ret = ConfirmAdd("Make " + $("#TeamName").val() + " the primary team for " + arg.EmployeeName + "?");
                                    if (ret == 6) {
                                        //yes is clicked
                                        data["hasCheckedEmployee"] = true;
                                        data["MakePrimary"] = true;
                                        gService.EditTeamsService.callAsyncSimple("AddEmployee", data, function (result) {
                                            employeeData.push("Primary");
                                            AddEmployeeFinished(result, employeeData, arg, className);
                                        });
                                    }
                                    else if (ret == 7) {

                                        data["hasCheckedEmployee"] = true;
                                        data["MakePrimary"] = false;
                                        gService.EditTeamsService.callAsyncSimple("AddEmployee", data, function (result) {
                                            employeeData.push("Secondary");
                                            AddEmployeeFinished(result, employeeData, arg, className);
                                        });
                                    }
                                }
                            }
                            else {
                                AddEmployeeFinished(result, employeeData, arg, className);
                            }
                        }
                    });                     
                }
            },
            { hideCloseButton: true });
            UpdateTeamRoleDDL();
        }

        function AddEmployeeFinished(result, employeeData, arg, className) {
            IsDirty = true;
            addedEmployees.push(employeeData);
            if ($("." + arg.EmployeeId).length == 0) {
                //generate the row
                var table = $("#EmployeesGrid");
                var row = document.createElement("tr");
                row.className = className;
                row.id = arg.EmployeeId;

                var editCell = document.createElement("td");
                var editLink = document.createElement("a");
                editLink.href = "#";
                editLink.className = "editLink";
                $(editLink).text("edit");
                editCell.appendChild(editLink);
                row.appendChild(editCell);

                var deleteCell = document.createElement("td");
                var deleteLink = document.createElement("a");
                deleteLink.href = "#";
                deleteLink.className = "deleteLink";
                $(deleteLink).text("remove");
                deleteCell.appendChild(deleteLink);
                row.appendChild(deleteCell);

                $(editLink).on("click", null, { EmployeeId: arg.EmployeeId }, editEmployeeHandler);
                $(deleteLink).on("click", null, { EmployeeId: arg.EmployeeId }, removeEmployeeHandler);

                var EmployeeIdCell = document.createElement("td");
                var EmployeeIdSpan = document.createElement("span");
                EmployeeIdSpan.className = "EmployeeId " + arg.EmployeeId;
                $(EmployeeIdSpan).text(result.value["EmployeeIDInCompany"]);
                EmployeeIdCell.appendChild(EmployeeIdSpan);
                row.appendChild(EmployeeIdCell);

                var EmployeeNameCell = document.createElement("td");
                var EmployeeNameSpan = document.createElement("span");
                $(EmployeeNameSpan).text(arg.EmployeeName);
                EmployeeNameCell.appendChild(EmployeeNameSpan);
                row.appendChild(EmployeeNameCell);

                var EmployeeBranchCell = document.createElement("td");
                var EmployeeBranchSpan = document.createElement("span");
                $(EmployeeBranchSpan).text(result.value["branchName"]);
                EmployeeBranchCell.appendChild(EmployeeBranchSpan);
                row.appendChild(EmployeeBranchCell);
                table.append(row);                
            }
        }

        function removeEmployee(EmployeeId) {
            IsDirty = true;
            //check to see if this was a newly added employee or if it was pre-existing
            var check = $(".EmployeeId." + EmployeeId);
            if ($(".EmployeeId." + EmployeeId).length > 0) {
                for (var i = 0; i < addedEmployees.length; i++) {
                    if (addedEmployees[i][0] == EmployeeId) {
                        addedEmployees.splice(i, 1);
                    }
                }
                
            }
            else {
                removedEmployees.push(EmployeeId);
            }

            $("#" + EmployeeId).remove();
            
            if ($("#EmployeesGrid tr").length <= 1) {
                $("#TeamRole").removeAttr("disabled");
            }
        }
        
        function disableUI()
        {
            $("#TeamMemberDiv input").attr("disabled", "true");
            $("#TeamNotes").attr("disabled", "true");
            $("#TeamRole").attr("disabled", "true");
            $("#TeamName").attr("disabled", "true");
            $("#SelectDiv input").removeAttr("disabled");
        }

        function enableUI() {
            $("input").removeAttr("disabled");
            $("#TeamNotes").removeAttr("disabled");
            $("#TeamRole").removeAttr("disabled");
        }

        

        function UpdateTeamRoleDDL() {
            if ($("#EmployeesGrid tr").length > 1) {
                $("#TeamRole").attr("disabled", "true");
            }
            else {
                $("#TeamRole").removeAttr("disabled");
            }
        }

        function Export(exportType) {
            var data = {};
            if (exportType == 0) {
                window.open(gVirtualRoot + "/los/admin/TeamExport.aspx?NameFilter=" + encodeURIComponent( $("#TeamNameFilter").val()) + "&TeamRole=" + $("#RolesFilter").val() + "&ExportType=" + exportType);
            }
            else {
                window.open(gVirtualRoot + "/los/admin/TeamExport.aspx?NameFilter=" + encodeURIComponent( $("#TeamMemberName").val()) + "&TeamId=" + currentTeamId + "&ExportType=" + exportType);
            }
        }

        function checkRequired() {
            if (isEditingTeam) {
                if ($("#TeamRole").val() != "-1" && $("#TeamName").val() != "") {
                    $("#btnOK").removeAttr("disabled");
                    $("#btnApply").removeAttr("disabled");
                }
                else {
                    $("#btnOK").attr("disabled", true);
                    $("#btnApply").attr("disabled", true);
                }
            }
            else {
                if (removedTeams.length != 0) {
                    $("#btnOK").removeAttr("disabled");
                    $("#btnApply").removeAttr("disabled");
                }
                else {
                    $("#btnOK").attr("disabled", true);
                    $("#btnApply").attr("disabled", true);
                }
            }
            if ($("#TeamRole").val() != "-1" && ($("#TeamName").val() != "" || currentTeamId != "")) {
                $("#AddMember").removeAttr("disabled");
            }
            else {
                $("#AddMember").attr("disabled", "true");
            }
        }

        function displayPopup(event) {
            var name = event.data.EmployeeName;
            var roleDesc = event.data.RoleDesc;

            alert(name + " does not have the " + roleDesc + " role.");
        }

        
    </script>
</head>
<body onload="init();" style="height:100%;">
    <h4 class="page-header">Teams</h4>
    <form id="form1" runat="server">
    <div>
    <table id="containingTable" style="width:100%;">
        <tr><td>
        <div nowrap class="mainDiv" id="SelectDiv">
        <div class="Header">Select Team</div>
        
        <span class="label">Role</span> <asp:DropDownList ID="RolesFilter" runat="server"></asp:DropDownList>
        <br />
        <span  class="label">Name</span> <asp:TextBox ID="TeamNameFilter" runat="server"></asp:TextBox>
        <br />
        <input type="button" value="Search"  onclick="FilterTeams();" id="TeamSearch"/>
        <br />
        <br />
        <div class="tableContainer">
        <asp:GridView ID="TeamsGrid" runat="server" Width="300" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">     

        <HeaderStyle cssclass="GridHeader AlignLeft" />
                    <AlternatingRowStyle cssclass="GridAlternatingItem HoverHighlight" />
                    <RowStyle cssclass="GridItem HoverHighlight" />
            <Columns>
            
                <asp:TemplateField>
                    <HeaderTemplate>
                        
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a href="#" onclick="loadTeam('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamId").ToString()) %>');">edit</a>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderTemplate>
                        
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a href="#" onclick="removeTeam('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamId").ToString()) %>');">delete</a>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderTemplate>Team Role</HeaderTemplate>
                    <ItemTemplate>
                        <%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamRoleDesc").ToString()) %>
                        <span style="display:none;" class="TeamRole"><%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamRoleValue").ToString()) %></span>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderTemplate>Team Name</HeaderTemplate>
                    <ItemTemplate>
                        <span class="TeamName"><%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamName").ToString()) %></span>
                    </ItemTemplate>
                </asp:TemplateField>
                
                
            </Columns>
            <emptydatarowstyle backcolor="LightBlue"
          forecolor="Red"/>
          
        </asp:GridView>
        </div>
        <br />

        
        
    </div>
    </td>
    
    
        <td><div nowrap id="EditDiv" class="mainDiv">
        <div class="Header">Edit Team Information</div>
        
        <span  class="label">Team Name</span> <asp:TextBox ID="TeamName" onchange="checkRequired();" runat="server"></asp:TextBox>
        <br />
        <span  class="label">Role</span> <asp:DropDownList onchange="checkRequired();" ID="TeamRole" runat="server"></asp:DropDownList>
        <br />
        <table><tr>
        <td><span style="float:left;"  class="label">Team Notes</span>
        </td>
        <td> <asp:TextBox TextMode="MultiLine" onchange="checkRequired();"  Rows="10" ID="TeamNotes" runat="server"></asp:TextBox></td>
        </tr></table>
        <br />
        </td>
    
    
        <td>
        <div nowrap id="TeamMemberDiv" class="mainDiv">
        <div class="Header">Team Membership</div>
        
        <span  class="label">Name</span> <asp:TextBox runat="server" ID="TeamMemberName"></asp:TextBox>
        <br />
        <input type="button" value="Search" id="MemberSearch" onclick="FilterEmployees()"/>
        <br />
        <div class="tableContainer">
        <asp:GridView ID="EmployeesGrid" runat="server">
             <HeaderStyle cssclass="GridHeader AlignLeft" />
                    <AlternatingRowStyle cssclass="GridAlternatingItem HoverHighlight" />
                    <RowStyle cssclass="GridItem HoverHighlight" />
            <Columns>
                <asp:TemplateField>
                </asp:TemplateField>
                <asp:TemplateField>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Employee ID</HeaderTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Name</HeaderTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Branch</HeaderTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        </div>
        <br />
    </div></td></tr>
    <tr>
        <td>        
        <div class="bottomDiv">
        <input type="button" id="AddTeam" onclick="addTeam();" value="Add new team" /> <input type="button" id="ExportTeams" onclick="Export(0)" value="Export to CSV"/>
        </div>
        </td>
        <td><div class="bottomDiv"><input type="button" id="btnOK" onclick="OnOKClick()" value="OK" /><input type="button" id="btnCancel" onclick="OnCancelClick();" value="Cancel" /><input type="button" id="btnApply" onclick="OnApplyClick();" value="Apply" NoHighlight="true"/></div>
    </td>
        <td>
        <div class="bottomDiv">
        <input type="button" id="AddMember" value="Add employee to team..." onclick="AddEmployee();" /> <input type="button" id="ExportMembers" onclick="Export(1);" value="Export to CSV" />
        </div>
        </td>
    </tr>
    </table>
    </div>
    
    </form>
</body>
</html>
