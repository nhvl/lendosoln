using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using DataAccess;
using System.Collections.Generic;

namespace LendersOfficeApp.los.admin
{
	public partial class EditEmployee : LendersOffice.Common.BaseServicePage
	{
		protected EmployeeEdit m_Edit;

        public override IEnumerable<E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new E_RoleT[]
                {
                    E_RoleT.Administrator
                };
            }
        }

		private BrokerUserPrincipal BrokerUser
		{
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{ 
			set { ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." ); }
		}

        private void PageInit(object sender, System.EventArgs args)
        {
            this.EnableJquery = true;
            this.RegisterJsScript("LQBPopup.js");
        }

        /// <summary>
        /// Initialize this form.
        /// </summary>

        protected void PageLoad( object sender , System.EventArgs a )
		{
			// Bind the passed in employee to our control.

			try
			{
                RegisterService("utilities", "/common/UtilitiesService.aspx");
				RegisterService("main", "/los/admin/DefaultRolePermissionsService.aspx");
				RegisterService("passwordvalidate", "/los/BrokerAdmin/PasswordValidationService.aspx");
                m_Edit.DataBroker = BrokerUser.BrokerId;
				m_Edit.DataEditor = BrokerUser.UserId;

				if( IsPostBack == false )
				{
					m_Edit.DataSource = RequestHelper.GetGuid( "employeeId" , Guid.Empty );
					m_Edit.DataBranch = RequestHelper.GetGuid( "branchId" , Guid.Empty );
					m_Edit.DataBind();
				}
			}
			catch( Exception e )
			{
				// Oops!

				// 08/14/06 mf - OPM 6104. We don't want the user to be able to
				// postback a page that failed to load, because we likely did
				// not populate the UI properly.

                ErrorUtilities.DisplayErrorPage( e, true, BrokerUser.BrokerId, BrokerUser.EmployeeId );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
		#endregion

	}
}
