namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Displays the users that are associated with whichever branch the admin has selected for deletion
    /// </summary>
    public partial class ShowUsersAssociatedWithBranch : LendersOffice.Common.BasePage
	{
		protected System.Web.UI.WebControls.Panel					CheckBox;		
		private ArrayList											m_Selected = new ArrayList();

        List<Pair> m_branches = new List<Pair>();

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingBranches
                };
            }
        }

		private Guid m_brokerID 
		{
			get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
		}

		private Guid m_BranchId 
		{
			get { return new Guid( Request[ "branchId" ] ); }
		}

		private string m_BranchName
		{
			get { return Request[ "branchName" ]; }
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			BindDataGrid();
			m_branchNameLabel.Text = "Users associated with branch:  " + m_BranchName;
			m_noneToShowLabel.Visible = (m_dg.Items.Count == 0)?true:false;

			if(m_dg.Items.Count >= 100)
				m_tooManyUsers.Text = "(Too many users to display - displaying the first 100)";
			else
				m_tooManyUsers.Text = "";
		}

		private void BindDataGrid()
		{
			DataSet ds = new DataSet();
			SqlParameter[] parameters = {
											new SqlParameter("@BrokerId", m_brokerID),
											new SqlParameter("@BranchId", m_BranchId)
										};
			DataSetHelper.Fill(ds, m_brokerID, "ListUsersAllAssociatedWithBranch", parameters );
			m_dg.DataSource = ds.Tables[0].DefaultView;
			m_dg.DataBind();

            SqlParameter[] branchParameters = {
                                                  new SqlParameter("@BrokerId", m_brokerID)
                                              };

            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(m_brokerID, "ListBranchByBrokerId", branchParameters))
            {
                string b_Name;
                string b_Id;
                while (sR.Read() == true)
                {
                    b_Name = sR["Name"].ToString();
                    b_Id = sR["BranchID"].ToString();
                    m_branches.Add(new Pair(b_Name, b_Id));
                }
            }
			if( !Page.IsPostBack )
				InitBranchDropdownList();
		}

		//OPM 18349 -jMorse
		//Initializes List of branches
		// 08/26/08 ck - OPM 23916. Remove current branch from drop down list
		protected void InitBranchDropdownList()
		{
			m_BranchDropdownList.Items.Clear();
			foreach( Pair bp in m_branches)
			{
                string sBranchName = bp.First.ToString();
                string sBranchID = bp.Second.ToString();

				if(!sBranchName.Equals(m_BranchName))
				{
					m_BranchDropdownList.Items.Add( new ListItem(sBranchName, sBranchID));	
				}
			}
		}

		// 08/26/08 ck - OPM 23916. Instead of using the SelectedIndex of the 
		// drop down list, used the SelectedItem
		protected void MoveUsersClick( object sender, System.EventArgs a )
		{		
			string guid = m_BranchDropdownList.SelectedValue.ToString();

			foreach( System.Web.UI.WebControls.DataGridItem item in m_dg.Items )
			{				
				CheckBox cbox = item.FindControl( "m_CheckBox" ) as CheckBox;
				
				if( cbox.Checked )
				{
                    EncodedLabel eid = item.FindControl( "m_EID" ) as EncodedLabel;					
					m_Selected.Add( eid.Text );					
				}				
			}			

			Guid BranchMovingFrom = new Guid( m_BranchId.ToString() );
			Guid BranchMovingTo = new Guid( guid );			
			
			MoveUsersFromBranch( BranchMovingFrom, BranchMovingTo );

			PageLoad(sender,a); //Refresh page.			
		}

		protected void MoveUsersFromBranch( Guid BranchFromId, Guid BranchToId )
		{
			Guid employeeId;
			
			foreach( string eid in m_Selected )
			{
				employeeId = new Guid( eid );
				EmployeeDB edb = new EmployeeDB( employeeId, m_brokerID );
				
				try
				{
					edb.Retrieve();
					edb.BranchID = BranchToId;
					edb.Save(PrincipalFactory.CurrentPrincipal);
				}
				catch
				{
					throw;
				}				
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
        #endregion

        protected void m_dg_ItemDataBound(object sender, DataGridItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dataItem = (DataRowView)args.Item.DataItem;
            Guid employeeId = (Guid)dataItem["EmployeeId"];
            string type = (string)dataItem["Type"];

            EncodedLabel m_EID = args.Item.FindControl("m_EID") as EncodedLabel;
            m_EID.Text = employeeId.ToString();

            LinkButton editLink = args.Item.FindControl("editLink") as LinkButton;
            editLink.OnClientClick = "onEditClick(" + AspxTools.JsString(employeeId) + "," + AspxTools.JsString(type) + "); return false;";
        }
    }
}
