<%@ Page language="c#" Codebehind="HiddenCRAList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.HiddenCRAList" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>HiddenCRAList</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema><link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
<style>.ScrollFrame {
	BORDER-RIGHT: 2px inset; BORDER-TOP: 2px inset; OVERFLOW: auto; BORDER-LEFT: 2px inset; BORDER-BOTTOM: 2px inset
}
</style>
</HEAD>
<body bgColor=gainsboro scroll=no MS_POSITIONING="FlowLayout">
<script language=javascript>
<!--
var g_bDirty = false;
function _init() {
  resize(600,400);
  updateScrollFrame();
}
function f_ok() {
  if (!g_bDirty) {
    onClosePopup();
    return;
  }

  var coll = document.getElementById("CraList");
  var len = coll.childNodes.length;
  var bFirst = true;
  var str = '';

  for (var i = 0; i < len; i++) {
    var o = coll.childNodes[i];
    if (!bFirst) str += ';';
    str += o.id;
    bFirst = false;
  }

  var args = new Object();
  args["Ids"] = str;

  var result = gService.main.call("Save", args);

  if (!result.error) {
    onClosePopup();
  } else {
    var errMsg = 'Unable to save data. Please try again.';

    if (null != result.UserMessage)
      errMsg = result.UserMessage;

      alert(errMsg);
  }
}

function f_cancel() {
  onClosePopup();
}
function updateScrollFrame() {
  var h = document.body.clientHeight;
  document.getElementById("ScrollFrame").style.height=(h - 130) + "px";
}
function f_addCra() {
  g_bDirty = true;
  var cra_ddl = document.getElementById("AvailableCRAs");
  var selectedIndex = cra_ddl.selectedIndex;
  var opt = cra_ddl.options[selectedIndex];
  
  var h = hypescriptDom;
  var coll = document.getElementById("CraList");
  coll.appendChild(h("li", {id:opt.value}, [
    opt.text, 
    " (", h("a", {href:'#', onclick:function(){f_removeCra(this)}}, "remove from hidden"), ")"]));

  <%-- dd 4/5/2006 - DO NOT call the f_removeCraFromDropDown without setTimeout. For some reason IE will not refresh correctly if you call the method without setTimeout --%>
  setTimeout('f_removeCraFromDropDown()', 50);
}
function f_removeCraFromDropDown() {
  var cra_ddl = document.getElementById("AvailableCRAs");
  var selectedIndex = cra_ddl.selectedIndex;

  cra_ddl.options.remove(selectedIndex);
  cra_ddl.selectedIndex = 0;

}
function f_removeCra(o) {

  g_bDirty = true;
  var id = o.parentNode.id;
  var name = o.parentNode.innerText.replace("(remove from hidden)", "");
  var ddl = document.getElementById("AvailableCRAs");
  var oOption = document.createElement("OPTION");
  oOption.value = id;
  oOption.innerText = name;
  ddl.add(oOption);


  var coll = document.getElementById("CraList");
  coll.removeChild(o.parentNode);

}
//-->
</script>
<h4 class="page-header">Hide Credit Report Agency</h4>
<form id=HiddenCRAList method=post runat="server">
<table width="100%" border=0>
  <tr>
    <td><asp:dropdownlist id=AvailableCRAs runat="server"></asp:DropDownList>&nbsp;
<input onclick=f_addCra(); type=button value="Add to hidden list"></TD></TR>
  <tr>
    <td>&nbsp;</TD></TR>
  <tr>
    <td><span class=FieldLabel>List of Hidden CRAs</span><span style="padding-left:10px;FONT:11px arial;COLOR:dimgray">(These will be hidden from the credit agency list in PriceMyLoan.)</span></TD></TR>
  <tr>
    <td>
      <div class=ScrollFrame id=ScrollFrame>
      <ul id=CraList style="MARGIN-LEFT: 18px" >
<ml:PassthroughLiteral id="ListHtml" runat="server" EnableViewState=False></ml:PassthroughLiteral>
      </UL>
      </DIV></TD></TR>
  <tr>
    <td align=center><input class=ButtonStyle id=btnOk style="WIDTH: 50px" onclick=f_ok(); type=button value=OK>&nbsp;&nbsp;
<input class=ButtonStyle onclick=f_cancel(); type=button value=Cancel></TD></TR></TABLE></FORM><uc1:cmodaldlg id=CModalDlg1 runat="server"></uc1:cModalDlg>

  </body>
</HTML>
