namespace LendersOfficeApp.los.admin
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    /// <summary>
    /// Displays the loans that are associated with whichever branch the admin has selected for deletion
    /// </summary>
    public partial class ShowLoansAssociatedWithBranch : LendersOffice.Common.BasePage
	{
		protected System.Web.UI.WebControls.Label ForMoreInfo;
		private ArrayList											m_Selected = new ArrayList();

        private List<KeyValuePair<Guid, string>> m_branchList = null;

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingBranches
                };
            }
        }

		private Guid m_brokerID 
		{
			get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
		}

		private Guid m_BranchId 
		{
			get { return RequestHelper.GetGuid("branchid"); }
		}

		private string m_BranchName
		{
			get { return RequestHelper.GetSafeQueryString("branchName"); }
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
            int tabIndex = -1;
            if (ViewState["tabIndex"] != null)
                tabIndex = (int)ViewState["tabIndex"];

            if ((Request.Params["__EVENTARGUMENT"] != "0" && tabIndex == 1 ) || Request.Params["__EVENTARGUMENT"] == "1")
			{

                BindDataGrid(0);
                m_invalidLoansLabel.Visible = true;
                tab1.Attributes.Add("class", "selected");
                tab0.Attributes.Remove("class");
                ViewState["tabIndex"] = 1;
			}
			else
			{
                BindDataGrid(1);
                m_invalidLoansLabel.Visible = false;
                tab0.Attributes.Add("class", "selected");
                tab1.Attributes.Remove("class");
                ViewState["tabIndex"] = 0;
			}

            

			m_branchNameLabel.Text = "Loans associated with branch:  " + m_BranchName;
			m_tooManyLoans.Text = "- Too many loans to display - displaying the first " + ConstAppDavid.MaxViewableRecordsInPipeline;
			m_noneToShowLabel.Visible = (m_dg.Items.Count == 0)?true:false;
			m_tooManyLoans.Visible = (m_dg.Items.Count >= ConstAppDavid.MaxViewableRecordsInPipeline)?true:false;
            IncludeStyleSheet("~/css/Tabs.css");
		}

		private void BindDataGrid(int validity)
		{
			DataSet                 dS = new DataSet();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_brokerID),
                                            new SqlParameter("@BranchId", m_BranchId),
                                            new SqlParameter("@Validity", validity)
                                        };

			DataSetHelper.Fill( dS , m_brokerID, "ListLoansAssociatedWithBranch", parameters);

			m_dg.DefaultSortExpression = "sLNm ASC";

			m_dg.DataSource = dS.Tables[0].DefaultView;
			m_dg.DataBind();

            SqlParameter[] branchParameters = {
                                                  new SqlParameter("@BrokerId", m_brokerID)
                                              };
            using (var reader = StoredProcedureHelper.ExecuteReader(m_brokerID, "ListBranchByBrokerId", branchParameters ))
            {
                string branchName;
                Guid branchId;
                m_branchList = new List<KeyValuePair<Guid, string>>();
                while (reader.Read() == true)
                {
                    branchName = reader["Name"].ToString();
                    branchId = (Guid)reader["BranchID"];
                    m_branchList.Add(new KeyValuePair<Guid, string>(branchId, branchName));
                }
            }
            if (!Page.IsPostBack)
            {
                InitBranchDropdownList();
            }

		}

		//OPM 18349 -jMorse
		//Initializes List of branches
		// 08/26/08 ck - OPM 23916. Remove current branch from drop down list
		protected void InitBranchDropdownList()
		{
            
			m_BranchDropdownList.Items.Clear();

            foreach (var o in m_branchList)
            {
                if (o.Key == m_BranchId)
                    continue;
                m_BranchDropdownList.Items.Add(new ListItem(o.Value, o.Key.ToString()));
            }
		}

		// 08/26/08 ck - OPM 23916. Instead of using the SelectedIndex of the 
		// drop down list, used the SelectedItem
		protected void MoveLoansClick( object sender, System.EventArgs a )
		{
            string guid = m_BranchDropdownList.SelectedItem.Value;

			foreach( System.Web.UI.WebControls.DataGridItem item in m_dg.Items )
			{				
				CheckBox cbox = item.FindControl( "m_CheckBox" ) as CheckBox;
				
				if( cbox.Checked )
				{
                    EncodedLabel lid = item.FindControl( "m_LID" ) as EncodedLabel;					
					m_Selected.Add( lid.Text );				
				}				
			}	

			Guid BranchMovingFrom = new Guid( m_BranchId.ToString() );
			Guid BranchMovingTo = new Guid( guid );			
			
			MoveLoansFromBranch( BranchMovingFrom, BranchMovingTo );

			PageLoad(sender,a); //Refresh page.			
		}
	
		protected void MoveLoansFromBranch( Guid BranchFromId, Guid BranchToId )
		{
			Guid loanId;

			foreach( string lid in m_Selected )
			{
				loanId = new Guid( lid );
				AssignBranchToLoans( BranchToId, loanId );
			}
		}

		protected void AssignBranchToLoans( Guid BranchToId, Guid LoanId )
		{
        	
			CPageData pg = GetPageWithBranchId( LoanId );
            pg.AllowSaveWhileQP2Sandboxed = true;
			pg.InitSave(ConstAppDavid.SkipVersionCheck);
            pg.AssignBranch(BranchToId);
			pg.Save();			
		}

		protected CPageData GetPageWithBranchId( Guid loanId )
		{
            CPageData pgData =  null;

            if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanModifyLoanPrograms))
            {
               pgData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(ShowLoansAssociatedWithBranch));
            }
            else
            {
                pgData = CPageData.CreateUsingSmartDependency(loanId, typeof(ShowLoansAssociatedWithBranch));
            }

            pgData.AllowLoadWhileQP2Sandboxed = true;
			pgData.InitLoad();
			return pgData;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void m_dg_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

        protected void m_dg_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            DataRowView dataItem = args.Item.DataItem as DataRowView;

            EncodedLabel sLId = args.Item.FindControl("m_LID") as EncodedLabel;
            sLId.Text = dataItem["sLId"].ToString();
        }
    }
}
