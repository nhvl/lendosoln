<%@ Page language="c#" Codebehind="FieldChoices.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.admin.FieldChoices" %>
<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>FieldChoices</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</head>
	<body MS_POSITIONING="FlowLayout" scroll="no" onload="onInit();">
		<script type="text/javascript">
	
		<!--

		function onInit() { try
		{
		    var feedBack = document.getElementById("m_feedBack");
		    var errorMessage = document.getElementById("m_errorMessage");
		    var commandToDo = document.getElementById("m_commandToDo");

			if( errorMessage != null )
			{
				alert( errorMessage.value );
			}

			if( feedBack != null )
			{
				alert( feedBack.value );
			}

			if( commandToDo != null )
			{
				if( commandToDo.value == "Close" )
				{
					onClosePopup();
				}
			}
			
			resize( 600 , 540 );
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function onClose()
		{
			if( document.getElementById("m_IsDirty").value == "Dirty" )
			{
				if( confirm( "Do you want to close without saving?" ) == false )
				{
					return;
				}
			}
			
			onClosePopup();
		}

		// -->

		</script>
		<h4 class="page-header">Custom Field Choices</h4>
		<FORM id="FieldChoices" method="post" runat="server">
			<INPUT type="hidden" id="m_IsDirty" runat="server" value="">
			<TABLE width="100%" border="0" cellspacing="2" cellpadding="3">
			<TR>
			<TD>
				<DIV style="WIDTH: 100%; HEIGHT: 90%; OVERFLOW-Y: scroll;">
					<ML:CommonDataGrid id="m_Grid" runat="server" cellpadding="2" width="100%" OnItemDataBound="GridItemBound">
						<Columns>
							<ASP:BoundColumn DataField="Description" HeaderText="Description">
							</ASP:BoundColumn>
							<ASP:TemplateColumn HeaderText="Options <span style='FONT-STYLE: italic; FONT-WEIGHT: normal; FONT: 11px arial;'>(enter new options on separate lines; limit: 100 characters)</span>" HeaderStyle-Width="1%" ItemStyle-Width="1%" HeaderStyle-Wrap="False">
								<ItemTemplate>
									<ASP:TextBox id="Options" runat="server" style="PADDING-LEFT: 4px; FONT: 12px arial; WIDTH: 330px;" Rows="4" Wrap="False" TextMode="MultiLine" onchange="document.getElementById('m_IsDirty').value = 'Dirty';">
									</ASP:TextBox>
									<ML:EncodedLabel id="FieldName" runat="server" style="DISPLAY: none;">
									</ML:EncodedLabel>
								</ItemTemplate>
							</ASP:TemplateColumn>
						</Columns>
					</ML:CommonDataGrid>
					<ASP:Panel id="m_EmptyMessage" runat="server" Width="100%" Visible="False" EnableViewState="False">
						<DIV style="PADDING: 24px; TEXT-ALIGN: center; COLOR: dimgray; FONT: 11px arial;">
							No options to display.
						</DIV>
					</ASP:Panel>
				</DIV>
				<HR>
				<DIV style="WIDTH: 100%; TEXT-ALIGN: right;">
					<ASP:Button id="m_Ok" runat="server" Text="OK" Width="50px" OnClick="OkClick">
					</ASP:Button>
					<INPUT type="button" value="Cancel" width="50px" onclick="onClose();">
					<ASP:Button id="m_Apply" runat="server" Text="Apply" Width="50px" OnClick="ApplyClick">
					</ASP:Button>
				</DIV>
			</TD>
			</TR>
			</TABLE>
			<ML:CModalDlg id="m_ModalDlg" runat="server">
			</ML:CModalDlg>
		</FORM>
	</body>
</html>
