using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using LendersOffice.Constants;
using LendersOffice.CreditReport;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.ObjLib.ServiceCredential;

namespace LendersOfficeApp.los.admin
{
	public partial  class BrokerCRASettings : System.Web.UI.UserControl
	{

        private BrokerUserPrincipal BrokerUser 
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }
        protected System.Web.UI.WebControls.DropDownList ShowCredcoOptionT;

        protected override void OnInit(System.EventArgs e) 
        {
            var currentPrincipal = BrokerUserPrincipal.CurrentPrincipal;
            var list = MasterCRAList.RetrieveAvailableCras(currentPrincipal.BrokerId);
            BindCRA(CRA0, list);
            BindCRA(CRA1, list);
            BindCRA(CRA2, list);
            BindCRA(CRA3, list);
            BindCRA(CRA4, list);
            BindCRA(CRA5, list);
            BindCRA(CRA6, list);
            BindCRA(CRA7, list);
            BindCRA(CRA8, list);
            BindCRA(CRA9, list);

            string str = "";
            foreach (CRA o in list) 
            {
                if (o.Protocol != CreditReportProtocol.Mcl) 
                {
                    str += "'" + o.ID + "',";
                }
            }
            str += "'" + Guid.Empty + "'";
            Page.ClientScript.RegisterArrayDeclaration("g_aNonMclList", str);

            var creditMornetPlusUserID = currentPrincipal.BrokerDB.CreditMornetPlusUserID;
            if (!String.IsNullOrEmpty(creditMornetPlusUserID))
            {
                CreditMornetPlusUserID.Text = currentPrincipal.BrokerDB.CreditMornetPlusUserID;
                CreditMornetPlusPassword.Attributes["value"] = ConstAppDavid.FakePasswordDisplay;
            }
        }
        protected Boolean IsLpeEnabled
        {
            get{ return BrokerUser.HasFeatures( E_BrokerFeatureT.PricingEngine ); }
        }
        private void InitializeComponent()
        {

		}

    
        private void BindCRA(DropDownList ddl, IEnumerable<CRA> list) 
        {
            ddl.Items.Add(new ListItem("", Guid.Empty.ToString()));
            foreach (CRA o in list) 
            {
                ddl.Items.Add(new ListItem(o.VendorName, o.ID.ToString()));
            }
        }
	}
}
