﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BrokerGlobalWhiteListUserControl.ascx.cs" Inherits="LendersOfficeApp.los.admin.BrokerGlobalWhiteListUserControl" %>
<script type="text/javascript">

function f_addGlobalWhiteList() {
    showModal('/los/admin/BrokerGlobalWhiteListEdit.aspx', null, null, null,
        function (args) {
            Tab_LoadData('GlobalWhiteList');
        }, { hideCloseButton: true });
}

function f_editGlobalWhiteList(id) {
    var onClosePopoup = function(){ Tab_LoadData('GlobalWhiteList'); };

  showModal('/los/admin/BrokerGlobalWhiteListEdit.aspx?id=' + id, null , null, null, onClosePopoup, { hideCloseButton: true });

    return false;
}
function f_deleteGlobalWhiteList(id, ipAddress) {

    if (confirm('Do you want to delete this ' + ipAddress + '?')) {
        var args = {
            Id: id
        }
        gService.main.callAsyncSimple('GlobalWhiteList_Delete', args, function () { Tab_LoadData('GlobalWhiteList'); });
    }

    return false;
}
function GlobalWhiteList_loaded() {
    var jsonData = document.getElementById("GlobalWhiteList_WhiteListJson").value;

    $('#GridData').empty();

    var list = JSON.parse(jsonData);

    for (var i = 0; i < list.length; i++) {
        var o = list[i];
        __CreateGlobalWhiteListRow(i, o.Id, o.IpAddress, o.Description, o.LastModifiedDateDisplay, o.LastModifiedLoginName);
    }
    
}

function __CreateGlobalWhiteListRow(row, id, ipAddress, desc, lastUpdated, lastUpdatedBy) {
    var h = hypescriptDom;
    $('#GridData').append(h("tr", {className:(row % 2 == 0 ? 'GridItem' : 'GridAlternatingItem')}, [
        h("td", {}, [
            h("a", {href:"#", onclick:function(){return f_editGlobalWhiteList(id)}}, "edit"),
            " ",
            h("a", {href:"#", onclick:function(){return f_deleteGlobalWhiteList(id, ipAddress)}}, "delete")
        ]),
        h("td", {}, ipAddress),
        h("td", {}, desc),
        h("td", {}, lastUpdated),
        h("td", {}, lastUpdatedBy),
    ]));
}
</script>
<asp:HiddenField ID="WhiteListJson" runat="server" />
<div class="tab-content">
<div class="FieldLabel">Global Whitelist</div>
<hr />
IP addresses on this list will not require SMS authentication for any user.
<br />
<div style="height: 460px; overflow-y:auto">
<table class="DataGrid" border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
    <tr class="GridHeader">
        <td style="width:50px"></td>
        <td style="width:100px">IP Address</td>
        <td style="width:400px">Description</td>
        <td style="width:100px">Last Updated</td>
        <td style="width:200px">Last Modified User</td>
    </tr>
    <tbody id="GridData">
    
    </tbody>
</table>
</div>
<br />
<input type="button" value="Add new" onclick="f_addGlobalWhiteList();"/>
</div>

