﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BrokerGlobalWhiteListEdit.aspx.cs" Inherits="LendersOfficeApp.los.admin.BrokerGlobalWhiteListEdit" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Edit Broker Global WhiteList</title>
</head>
<body bgcolor="gainsboro">
<script type="text/javascript">
function _init() {
    resizeForIE6And7(500, 300);
}
function f_cancel() {
    onClosePopup();
}
function f_save() {

    var ipAddress = <%= AspxTools.JsGetElementById(ipAddress) %>.value;
    
    var isIpPattern = /^((([0-9]{1,3})|\*)\.){3}(([0-9]{1,3})|\*)$/.test(ipAddress);
    
    if (isIpPattern == false)
    {
        alert('IP Address is invalid format.');
        return;
    }
    var args = {
        Id : ML.Id,
        Description : <%= AspxTools.JsGetElementById(description) %>.value,
        IpAddress : ipAddress
    };
    gService.main.callAsyncSimple('GlobalWhiteList_EditItem', args, onClosePopup);
}
</script>
    <h4 class="page-header">IP Restriction</h4>
    <form id="form1" runat="server">
    <div>
    <table>
        <tr>
            <td class="FieldLabel">IP Address</td>
            <td><asp:TextBox ID="ipAddress" runat="server" /></td>
        </tr>
        <tr>
            <td class="FieldLabel">Description</td>
            <td><asp:TextBox ID="description" runat="server"  width="200px"/></td>
        </tr>
        <tr>
            <td colspan="2">You can use a wildcard '*' in the IP address. Example: 128.201.200.* means to allow all users in the 128.201.200 subnet access.</td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="button" id="saveBtn" runat="server" value="Edit" onclick="f_save();"/>
                <input type="button" id="cancelBtn" value="Cancel" onclick="f_cancel();"/>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
