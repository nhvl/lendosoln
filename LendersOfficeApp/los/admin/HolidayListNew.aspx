<%@ Page language="c#" Codebehind="HolidayListNew.aspx.cs" AutoEventWireup="true" Inherits="LendersOfficeApp.los.admin.HolidayListNew" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Common Holidays</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body style="OVERFLOW-Y: auto" bottomMargin="0" onload="init();" MS_POSITIONING="FlowLayout">
		<script language="javascript">
		<!--
		
		function init()
		{
			resize(365,520);
		}
        function ConfirmHolidayChanges()
        {
            var holidayIds = holidayRadioButtonListIds; // Created in the code behind, an array of all the radiobuttonlist ids
            var initialHolidays = holidayInitialStates; // Created in the code Behind, an object mapping ids to initial values.
            var currentValues = {};
            for (var holidayIndex = 0; holidayIndex < holidayIds.length; ++holidayIndex)
            {
                var radioButtonList = document.getElementById(holidayIds[holidayIndex]);
                var listItems = radioButtonList.getElementsByTagName('input');
                for (var radioIndex = 0; radioIndex < listItems.length; ++radioIndex)
                {
                    if (listItems[radioIndex].checked)
                    {
                        currentValues[holidayIds[holidayIndex]] = listItems[radioIndex].value === "Y";
                    }
                }
            }

            var holidaysTurnedOff = [];
            for (var holidayIndex = 0; holidayIndex < holidayIds.length; ++holidayIndex)
            {
                var holidayId = holidayIds[holidayIndex];
                if (initialHolidays[holidayId] === true && currentValues[holidayId] === false)
                {
                    holidaysTurnedOff.push(holidayId);
                }
            }

            var confirmed = true;
            if (holidaysTurnedOff.length > 0)
            {
                confirmed = confirm('Toggling holidays from \'Yes\' to \'No\' will remove all future occurrences of those holidays from your list of closures. Do you wish to continue?');
            }

            return confirmed;
        }
     	//-->		
		</script>
		<form id="HolidayRadios" method="post" runat="server">
			<table>
				<tr align="center" style="FONT-WEIGHT: bolder">
					<td style="PADDING-TOP: 7px; padding-right:40;TEXT-DECORATION: underline">Holiday</td>
					<td style="PADDING-TOP: 7px; TEXT-DECORATION: underline">Next Date</td>
					<td style="PADDING-TOP: 7px; TEXT-DECORATION: underline">Closed?</td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">New Year's Day</td>
					<td id="m_NewYearsDayDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="NewYearsDay" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Martin Luther King, Jr. Day</td>
					<td id="m_MartinLutherKingJrDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="MartinLutherKingJrDay" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">President's Day</td>
					<td id="m_PresidentsDayDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="PresidentsDay" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Good Friday</td>
					<td id="m_GoodFridayDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="GoodFriday" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Easter</td>
					<td id="m_EasterDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="Easter" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Memorial Day</td>
					<td id="m_MemorialDayDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="MemorialDay" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Independence Day</td>
					<td id="m_IndependenceDayDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="IndependenceDay" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Labor Day</td>
					<td id="m_LaborDayDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="LaborDay" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Columbus Day</td>
					<td id="m_ColumbusDayDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="ColumbusDay" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Veteran's Day</td>
					<td id="m_VeteransDayDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="VeteransDay" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Thanksgiving Day</td>
					<td id="m_ThanksgivingDayDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="ThanksgivingDay" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Day After Thanksgiving</td>
					<td id="m_DayAfterThanksgivingDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="DayAfterThanksgiving" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Christmas Eve</td>
					<td id="m_ChristmasEveDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="ChristmasEve" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">Christmas Day</td>
					<td id="m_ChristmasDayDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="ChristmasDay" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
				<tr>
					<td style="WIDTH: 139px; padding-left=20">New Years Eve</td>
					<td id="m_NewYearsEveDate" runat="server" style="WIDTH: 80px" align="center"></td>
					<td style="HEIGHT: 26px"><asp:radiobuttonlist id="NewYearsEve" RepeatDirection="Horizontal" Runat="server">
							<asp:ListItem Value="Y">Yes</asp:ListItem>
							<asp:ListItem Value="N">No</asp:ListItem>
						</asp:radiobuttonlist></td>
				</tr>
			</table>
			<table style="WIDTH: 317px; HEIGHT: 27px">
				<tr align="center">
					<td align="center"><asp:Button id="m_SaveButton" Runat="server" OnClick="SaveHolidays" OnClientClick="return ConfirmHolidayChanges();" Text="  Save  "></asp:Button>
						<INPUT id="m_CloseHolidaysList" onclick="onClosePopup()" type="button" value=" Cancel "></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
