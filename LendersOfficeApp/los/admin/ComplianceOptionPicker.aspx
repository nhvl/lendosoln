﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComplianceOptionPicker.aspx.cs" Inherits="LendersOfficeApp.los.admin.ComplianceEaseOptionPicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOfficeApp.los" %>
<%@ Register TagPrefix="uc" TagName="LRPicker" Src="../LeftRightPicker.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
    body {
        background-color: Gainsboro;
    }

    #PriorStatusDiv {
        width: 44%;
        padding-top: 10px;
        position: relative;
        margin-right: 1%;
        margin-bottom: 1%;
    }

    #PriorStatuses {
        width: 100%;
        height: 100px;
    }

    #SpacerDiv {
        width: 54%;
    }

    #IsLoanStatusPicker { 
        display: none;
    }
    </style>
</head>
<body>
    <h4 class="page-header">Compliance Option Picker</h4>
    <form id="form1" runat="server">
    <asp:CheckBox runat="server" ID="IsLoanStatusPicker" checked="false" />
    <div id="PickerDiv">
        <uc:LRPicker id="OptionPicker" runat="server" EnableSaveOnUnload="false"></uc:LRPicker>
    </div>
    <div>
        <div id="SpacerDiv"></div>
        <div id="PriorStatusDiv" class="Column" runat="server">
            <span class="LRPHeader">
                Loan Statuses Prior to "Begin Audits" Status
            </span>
            <br />
            <select runat="server" id="PriorStatuses" class="box" multiple="true"></select>
        </div>
    </div>
    </form>
</body>
</html>
