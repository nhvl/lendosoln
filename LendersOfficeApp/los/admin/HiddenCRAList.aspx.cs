using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;

using DataAccess;
using LendersOffice.Admin;
using LendersOffice.CreditReport;
using LendersOffice.Security;
using System.Collections.Generic;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.admin
{

	public partial class HiddenCRAList : LendersOffice.Common.BaseServicePage
	{
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingGeneralSettings
                };
            }
        }
    
        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterService("main", "/los/admin/HiddenCRAListService.aspx");
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            NameValueCollection list = new NameValueCollection(100);
            // List Lender mapping cra
            foreach (var proxy in CreditReportAccountProxy.Helper.ListCreditProxyByBrokerID(brokerId))
            {
                list.Add(proxy.CrAccProxyId.ToString().ToLower(), proxy.CrAccProxyDesc);
            }

            var cralist = MasterCRAList.RetrieveAvailableCras(brokerId);

            foreach (CRA o in cralist) 
            {
                if (!o.IsInternalOnly) 
                {
                    list.Add(o.ID.ToString().ToLower(), o.VendorName);
                }
            }

            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);

            if (null != brokerDB.BlockedCRAsXmlContent && "" != brokerDB.BlockedCRAsXmlContent) 
            {
                try 
                {
                    XmlDocument doc = Tools.CreateXmlDoc(brokerDB.BlockedCRAsXmlContent);
                    XmlNodeList nodeList = doc.SelectNodes("//list/pml/item");
                    foreach (XmlElement el in nodeList) 
                    {

                        string id = el.GetAttribute("id").ToLower();
                        string name = list.Get(id);
                        if (id != "" && name != "") 
                        {
                            ListHtml.Text += string.Format("<li id=\"{0}\">{1} (<a href=\"#\" onclick=\"f_removeCra(this);\">remove from hidden</a>)", AspxTools.HtmlString(id), AspxTools.HtmlString(name));
                            list.Remove(id);
                        }
                    }
                } 
                catch{}
            }

            int count = list.Count;
            for (int i = 0; i < count; i++) 
            {
                AvailableCRAs.Items.Add(new ListItem(list.Get(i), list.GetKey(i)));
            }



            
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
