﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LenderServices.ascx.cs" Inherits="LendersOfficeApp.los.admin.LenderServices" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<style type="text/css">
    .BaseCopy
    {
        display: none;
    }
    .ServicesTable
    {
        border-collapse: collapse; 
        width: 100%;
        border: 1px solid gainsboro;
        border-spacing: 0;
    }
    .ServicesTable td
    {
        border: 1px solid gainsboro;
        padding: 2px;
    }
    .ServicesTableSection {
        overflow: auto;
        max-height: 200px;
    }
    .ServicesSection {
        margin-bottom: 1.5em;
    }
    .error {
        color: red;
    }
</style>
<script type="text/javascript">
    jQuery(function ($) {
        $(<%= AspxTools.JsGetElementById(AddVoxService) %>).click(function () {
            LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/los/admin/LenderServicesEdit.aspx")) %>, {
                hideCloseButton: true,
                popupClasses: 'Popup',
                width: 550,
                height: 375,
                onReturn: PopupReturn
            }, null);
        });

        $(<%= AspxTools.JsGetElementById(AddTitleServiceBtn) %>).click(function () {
            LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/los/admin/EditTitleLenderService.aspx")) %>, {
                hideCloseButton: true,
                popupClasses: 'Popup',
                width: 550,
                height: 375,
                onReturn: PopupReturn
            }, null);
        });

        $('#ServicesTableBody').on('click', '.ServiceEdit', function() {
            var type = $(this).data('type');
            var sId = $(this).closest('tr').data('serviceid');
            if (type === 0) {
                LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/los/admin/LenderServicesEdit.aspx")) %> + "?sId=" + sId, {
                    hideCloseButton: true,
                    popupClasses: 'Popup',
                    width: 550,
                    height: 500,
                    onReturn: PopupReturn
                }, null);
            }
            else if (type === 1) {
                LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/los/admin/EditTitleLenderService.aspx")) %> + "?sId=" + sId, {
                    hideCloseButton: true,
                    popupClasses: 'Popup',
                    width: 500,
                    height: 375,
                    onReturn: PopupReturn
                }, null);
            }
        });

        $('#ServicesTableBody').on('click', '.ServiceDelete', function() {
            var displayName = $(this).closest('tr').find('.DisplayName').text();
            var type = $(this).data('type');
            var parentRow = $(this).closest('tr');
            
            PolyConfirmShow("Are you sure you want to remove " + displayName + "?", function(confirmed) {
                if(!confirmed) {
                    return;
                }

                var data = {
                    ServiceId: parentRow.data('serviceid')
                };

                var result;
                if(type === 0) {
                    var result = gService.lenderService.call('DeleteLenderService', data)
                }
                else if(type === 1) {
                    var result = gService.titleService.call('DeleteTitleLenderService', data)
                }
                else {
                    return;
                }
                
                if (!result.error) {
                    if (result.value["Success"] === 'True') {
                        parentRow.remove();
                        AdjustAlternateItems();
                    }
                    else {
                        alert(result.value["Errors"]);
                    }
                }
                else {
                    alert(result.UserMessage);
                }
            }, false, null);
        });

        $('#ConfigureUcdDeliveryBtn').click(function() {
            LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/los/admin/ConfigureUcdDelivery.aspx")) %>, {
                hideCloseButton: true,
                popupClasses: 'Popup',
                width: 550,
                height: 350,
                onReturn: PopupReturn
            }, null);
        });

        $('#AddServiceCredentialBtn').click(function() {
            ServiceCredential.AddServiceCredential();
        });

        <%= AspxTools.JQuery(ConfigureDocuSignButton) %>.click(function() {
            LQBPopup.Show(<%= AspxTools.SafeUrl(Page.ResolveUrl("~/los/admin/ConfigureDocuSign.aspx")) %>, {
                hideCloseButton: true,
                popupClasses: 'Popup',
                width: 550,
                height: 550,
                onReturn: PopupReturn
            }, null);
        });

        function PopupReturn(returnArgs) {
            if(returnArgs != null) {
                var row;
                if(returnArgs.IsNew.toLowerCase() == 'true') {
                    var cloneRow = $('#ServicesTableBody').find('.BaseCopy').clone().removeClass('BaseCopy');
                    cloneRow.data('serviceid', returnArgs.ServiceId);
                    row = cloneRow;
                    $('#ServicesTableBody').append(row);
                    AdjustAlternateItems();
                }
                else {
                    row = $('#ServicesTableBody').find('tr').filter(function () { return $(this).data('serviceid') == returnArgs.ServiceId });
                }

                row.find('.ServiceTypeName').text(returnArgs.ServiceTypeName);
                row.find('.DisplayName').text(returnArgs.DisplayName);
                row.find('.AssociatedVendorName').text(returnArgs.VendorName);
                row.find('.AssociatedResellerName').text(returnArgs.ResellerName);
                row.find('.ServiceEdit').data('type', returnArgs.Type);
                row.find('.ServiceDelete').data('type', returnArgs.Type);
            }
        }

        function AdjustAlternateItems()
        {
            if($('#ServicesTableBody').length === 0) {
                return;
            }

            $('#ServicesTableBody').find('tr:not(.BaseCopy)').each(function(index, value) {
                $(this).removeClass('GridAlternatingItem GridItem');
                if(index % 2 === 0) {
                    $(this).addClass('GridItem');
                }
                else {
                    $(this).addClass('GridAlternatingItem');
                }
            });
        }

        AdjustAlternateItems();
        var serviceCredentialSettings = {
            ServiceCredentialJsonId: "ServiceCredentialsJson",
            ServiceCredentialTableId: "ServiceCredentialsTable"
        };

        ServiceCredential.Initialize(serviceCredentialSettings);
        ServiceCredential.RenderCredentials();
    });
</script>
<div class="tab-content">
    <div id="LenderServicesSection" class="ServicesSection" runat="server">
        <div class="FieldLabel">Services</div>
        <hr />
        <div class="ServicesTableSection">
            <table class="ServicesTable DataGrid">
                <tr class="GridHeader">
                    <td>Service</td>
                    <td>Display Name</td>
                    <td>Vendor</td>
                    <td>Provider</td>
                    <td></td>
                    <td></td>
                </tr>
                <tbody id="ServicesTableBody">
                    <tr class="BaseCopy" data-serviceid="">
                        <td><span class="ServiceTypeName"></span></td>
                        <td><span class="DisplayName"></span></td>
                        <td><span class="AssociatedVendorName"></span></td>
                        <td><span class="AssociatedResellerName"></span></td>
                        <td><a class="ServiceEdit" id="EditBtn" data-type="">edit</a></td>
                        <td><a class="ServiceDelete" id="DeleteBtn" data-type="">delete</a></td>
                    </tr>
                    <asp:Repeater runat="server" ID="LenderServiceRepeater">
                        <ItemTemplate>
                            <tr data-serviceid="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ServiceId").ToString()) %>">
                                <td>
                                    <span runat="server" class="ServiceType" id="ServiceTypeName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ServiceTypeName").ToString()) %></span>
                                </td>
                                <td>
                                    <span runat="server" class="DisplayName" id="DisplayName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "DisplayName").ToString()) %></span>
                                </td>
                                <td>
                                    <span runat="server" class="AssociatedVendorName" id="AssociatedVendorName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AssociatedVendorName").ToString()) %></span>
                                </td>
                                <td>
                                    <span runat="server" class="AssociatedResellerName" id="AssociatedResellerName"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AssociatedResellerName").ToString()) %></span>
                                </td>
                                <td>
                                    <a class="ServiceEdit" id="EditBtn" data-type="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Type").ToString()) %>">edit</a>
                                </td>
                                <td>
                                    <a class="ServiceDelete" id="DeleteBtn" data-type="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Type").ToString()) %>">delete</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
        <br />
        <input type="button" value="Add VOX Service" id="AddVoxService" runat="server" visible="false" />
        <input type="button" value="Add Title Service" id="AddTitleServiceBtn" runat="server" visible="false" />
    </div>
    <div class="ServicesSection">
        <div class="FieldLabel">Credentials</div>
        <hr />
        <div id="ServiceCredentialsDiv">
            <table id="ServiceCredentialsTable" class="DataGrid" ><tbody></tbody></table>
        </div>
        <br />
        <input type="button" id="AddServiceCredentialBtn" value="Add Credential" />
    </div>
    <div class="ServicesSection">
        <div class="FieldLabel">UCD Delivery</div>
        <hr />
        <input type="button" value="Configure UCD Delivery" id="ConfigureUcdDeliveryBtn" />
    </div>
    <div class="ServicesSection" id="DocuSignSection">
        <div class="FieldLabel">eSign</div>
        <hr />
        <input type="button" value="Configure DocuSign Integration" id="ConfigureDocuSignButton" runat="server" /> <span ID="ConfigureDocuSignMessage" class="error" runat="server"></span>
    </div>
</div>
