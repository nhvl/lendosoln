﻿// <copyright file="TeamPicker.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: EduardoM
//    Date:   6/16/2014
// </summary>

namespace LendersOfficeApp.Los
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.Text;
    using System.Web;
    using System.Web.Script.Services;
    using System.Web.Services;
    using System.Web.SessionState;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Roles;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;

    /// <summary>
    /// This class assigns a team to a loan.
    /// </summary>
    public partial class TeamPicker : BaseServicePage
    {
        /// <summary>
        /// Gets the Assign Type.  It's should currently always be 'loan'.
        /// </summary>
        /// <value>The type that the team should be assigned to.</value>
        public string AssignType
        {
            get { return RequestHelper.GetSafeQueryString("assignType"); }
        }

        /// <summary>
        /// Gets the loan id.
        /// </summary>
        /// <value>The current loan's id.</value>
        public Guid LoanId
        {
            get { return RequestHelper.GetGuid("loanid"); }
        }

        /// <summary>
        /// Gets the unfriendly role description. The role description is used to create the Role object.  It must exactly match the role description.
        /// </summary>
        /// <value>The unfriendly role description.</value>
        protected string RoleDesc
        {
            get { return RequestHelper.GetSafeQueryString("roledesc"); }
        }

        /// <summary>
        /// Gets the friendly role description.
        /// </summary>
        /// <value>The friendly role description.</value>
        protected string RoleString
        {
            get { return RequestHelper.GetSafeQueryString("role"); }
        }

        protected Guid RoleId = Guid.Empty;

        /// <summary>
        /// Loads the picker.
        /// </summary>
        /// <param name="sender">The trigger.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterCSS("stylesheet.css");
            RegisterService("EditTeamsService", "/los/admin/EditTeamsService.aspx");

            AssignedTeams.Text = "Assigned " + this.RoleDesc + " Teams";
            AllTeams.Text = "All " + this.RoleDesc + " Teams";
            Role role = Role.Get(this.RoleString);
            RoleId = role.Id;
            DataTable m_data = new DataTable();
            m_data.Columns.Add("TeamName");
            m_data.Columns.Add("TeamId");
            List<Team> teams = new List<Team>();

            if (AssignedTeams.Checked)
            {
                teams = (List<Team>)Team.ListTeamsByUserInRole(PrincipalFactory.CurrentPrincipal.EmployeeId, role.Id);
                if (!this.IsPostBack)
                {
                    if (teams.Count == 0)
                    {
                        AssignedTeams.Checked = false;
                        AllTeams.Checked = true;
                    }
                }
            }

            if (AllTeams.Checked)
            {
                List<Team> allTeams = (List<Team>)Team.ListTeamsAtLender(string.Empty);
                foreach (Team team in allTeams)
                {
                    if (role.Id == team.RoleId)
                    {
                        teams.Add(team);
                    }
                }
            }

            foreach (Team team in teams)
            {
                var row = m_data.NewRow();
                row["TeamName"] = team.Name;
                row["TeamId"] = team.Id;
                m_data.Rows.Add(row);
            }

            DataSet set = new DataSet();

            if (m_data.Rows.Count != 0)
            {
                set.Tables.Add(m_data);
                TeamsGrid.DataSource = set;
                TeamsGrid.DataBind();
            }
            else
            {
                m_data.Rows.Add(m_data.NewRow());
                set.Tables.Add(m_data);
                TeamsGrid.DataSource = set;
                TeamsGrid.DataBind();
                TeamsGrid.Rows[0].Visible = false;
            }
        }
    }
}
