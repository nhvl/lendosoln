namespace LendersOfficeApp.los
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Integration.Irs4506T;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using LendersOffice.ObjLib.CustomFavoriteFolder;

    public partial class myprofile : LendersOffice.Common.BaseServicePage
	{
        private const int MAX_IMG_SIZE = 100 * 1024; //100KB
        private BrokerDB m_broker;

        #region Protected member variables
        protected EmployeeDB m_Employee;
        protected System.Web.UI.WebControls.CustomValidator m_mclPasswordValidator;
        protected System.Web.UI.WebControls.TextBox m_mclLoginNameTF;
        protected System.Web.UI.WebControls.TextBox m_mclPasswordTF;
        protected System.Web.UI.WebControls.TextBox m_mclConfirmPasswordTF;
		protected LendersOffice.Common.RenderReadOnly readOnly;
		protected Guid m_brokerID;
		protected System.Web.UI.WebControls.Label				  m_pricingEngineLabel;
		protected EmployeePermissionsList							m_EmployeePermissionsList;
        protected Guid m_EmployeeId = Guid.Empty;
        
        #endregion

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        private Guid m_branchID 
        {
            get { return (Guid) ViewState["BranchID"]; }
            set { ViewState["BranchID"] = value; }
        }
        private bool IsPerTransactionBilling
        {
            get { return E_BrokerBillingVersion.PerTransaction == BrokerUser.BillingVersion; }
        }
        protected bool AllowCustomLogin
        {
            get
            {
                return IsPerTransactionBilling &&
                       Broker.IsEnablePTMDocMagicSeamlessInterface;
            }
        }

        protected bool CanCreateGlobalDMSLogin
        {
            get 
            {
                return (BrokerUser.HasPermission(Permission.AllowOrderingGlobalDMSAppraisals) &&
                        Broker.IsAllowOrderingGlobalDmsAppraisals);
          
            }
        }

        /// <summary>
        /// Check whether Broker has one or more 4506T Vendor enabled and user has permission to order 4506T
        /// </summary>
        /// 
        protected bool HasIrs4506TVendorEnabledAndCanOrder
        {
            get
            {
                return (BrokerUser.HasPermission(Permission.AllowOrder4506T) &&
                        Broker.Has4506TIntegration);
            }
        }
        
        private BrokerDB Broker
        {
            get
            {
                if (m_broker == null)
                {
                    m_broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                }
                return m_broker;
            }
        }

		protected Boolean IsPmlEnabled
		{
            get { return BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan); }
		}

        protected bool IsMFAEnabled
        {
            get { return Broker.IsEnableMultiFactorAuthentication; }
        }
    
		protected void PageLoad(object sender, EventArgs e)
		{
			// Put user code to initialize the page here
            if (Page.IsPostBack)
            {
                string target = this.Request.Form.Get("__EVENTTARGET");
                if (target == "ClientCertificateRevoke")
                {
                    Guid certificateId = new Guid(this.Request.Form["__EVENTARGUMENT"]);
                    RevokeClientCertificate(certificateId);
                }
                else if (target == "RegisteredIpDelete")
                {
                    int id = int.Parse(this.Request.Form["__EVENTARGUMENT"]);
                    DeleteRegisteredIp(id);
                }
            }

            RegisterService("Profile", "/los/myprofileservice.aspx");

			BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            m_brokerID = principal.BrokerId;
            m_Employee = new EmployeeDB(principal.EmployeeId, m_brokerID);
            m_EmployeeId = principal.EmployeeId;

            m_Employee.Retrieve();

            this.RegisterJsGlobalVariables("landingPageId", EmployeeDB.RetrieveLandingPageIdByEmployeeId(m_brokerID, m_EmployeeId));

            CellRequiredImg.Visible = m_Employee.EnableAuthCodeViaSms;
            m_RequireCell.Enabled = m_Employee.EnableAuthCodeViaSms;
            if (this.Broker.IsEnableMultiFactorAuthentication && (m_Employee.EnabledMultiFactorAuthentication || m_Employee.EnabledClientDigitalCertificateInstall))
            {
                tab5.Visible = true;
                _IPRestrictions.Visible = true;


                m_clientCertificateGrid.DataSource = ClientCertificate.ListByUser(m_Employee.BrokerID, m_Employee.UserID);
                m_clientCertificateGrid.DataBind();

                if (m_Employee.EnabledMultiFactorAuthentication)
                {
                    m_registeredIpDataGrid.DataSource = UserRegisteredIp.ListByUserId(m_brokerID, m_Employee.UserID);
                    m_registeredIpDataGrid.DataBind();
                }

                if (m_Employee.EnabledClientDigitalCertificateInstall == false)
                {
                    installCertificateLink.Visible = false;
                }
                else
                {
                    installCertificateLink.Visible = true;
                }
            }
            else
            {
                // 5/10/2014 dd - Hide the IP Restriction tab when user does not have Multi-Factor Authentication enable.
                tab5.Visible = false;
                _IPRestrictions.Visible = false;
            }

			if (!Page.IsPostBack) 
            {
                LoadEmployee();
            }
            m_FirstName.ReadOnly = Broker.DisableNameEditing;
            m_LastName.ReadOnly = Broker.DisableNameEditing;

			m_OldPassword.ReadOnly = m_Employee.IsNew;

            m_OldPasswordErrorMsg.Text = "";

			readOnly.Skip = m_FirstName;
			readOnly.Skip = m_LastName;
			readOnly.Skip = m_AddressTF;
			readOnly.Skip = m_CityTF;
			readOnly.Skip = m_StateTF;
			readOnly.Skip = m_ZipcodeTF;
			readOnly.Skip = m_PhoneTF;
            readOnly.Skip = m_CellTF;
            readOnly.Skip = IsCellphoneForMultiFactorOnly;
			readOnly.Skip = m_FaxTF;
			readOnly.Skip = m_EmailTF;
			readOnly.Skip = m_AgentLicenseNumber;
			readOnly.Skip = m_newOnlineLoanEventNotifOptionT;
            readOnly.Skip = m_OptsToUseNewLoanEditorUI;
            readOnly.Skip = m_TaskRelatedNotifOptionT;
			readOnly.Skip = m_OldPassword;
			readOnly.Skip = m_NewPassword;
			readOnly.Skip = m_ConfirmPassword;
			readOnly.Skip = m_Save;
            readOnly.Skip = btnSignatureUpload;
            readOnly.Skip = btnRemoveImage;
            readOnly.Skip = m_SupportEmail;
            readOnly.Skip = SecurityQuestions;
            
			m_EmployeePermissionsList.DataBroker = m_brokerID;
			m_EmployeePermissionsList.DataSource = principal.EmployeeId;
			m_EmployeePermissionsList.PermissionType = "employee";
			m_EmployeePermissionsList.Disabled = true;

            SignaturePlaceHolder.Visible = this.Broker.EnableLqbNonCompliantDocumentSignatureStamp;
            imgSignature.ImageUrl = "UserSignature.aspx?eid=" + BrokerUserPrincipal.CurrentPrincipal.EmployeeId + "&brokerid=" + m_brokerID + "&t=" + DateTime.Now.ToString();
            btnRemoveImage.Enabled = m_Employee.SignatureKey != Guid.Empty;

            if (m_broker.IsActiveDirectoryAuthenticationEnabled && m_Employee.IsActiveDirectoryUser)
            {
                m_loginLabel.InnerText = "Active Directory Username";
                HidePasswordFields();
            }

            SecurityQuestions.SetUserInfo(BrokerUser.BrokerId, m_Employee.UserID);
            tab7.Visible = BrokerUserPrincipal.CurrentPrincipal.GiveNewLoanEditorUIPreferenceOption;

            var serviceCredentials = ServiceCredential.GetAllEmployeeServiceCredentials(this.m_brokerID, m_EmployeeId, ServiceCredentialService.All);
            var serviceCredentialsJson = LendersOffice.Common.ObsoleteSerializationHelper.JsonSerialize(serviceCredentials);
            Page.ClientScript.RegisterHiddenField("ServiceCredentialsJson", serviceCredentialsJson);
        }

        private void DeleteRegisteredIp(int id)
        {
            UserRegisteredIp.Delete(BrokerUser.BrokerId, BrokerUser.UserId, id);
        }

        private void RevokeClientCertificate(Guid certificateId)
        {
            ClientCertificate.Delete(BrokerUser.BrokerId, BrokerUser.UserId, certificateId);
        }
        private void HidePasswordFields()
        {
            m_pwMessage.Visible = false;
            m_OldPassword.Visible = false;
            m_oldPWLabel.Visible = false;
            m_NewPassword.Visible = false;
            m_newPWLabel.Visible = false;
            m_ConfirmPassword.Visible = false;
            m_retypePWLabel.Visible = false;
            ValidatorRow.Visible = false;
        }

        protected void BindDocumentVendor(object sender, RepeaterItemEventArgs e)
        {
            var vendor = (IDocumentVendor)e.Item.DataItem;

            var login = vendor.CredentialsFor(PrincipalFactory.CurrentPrincipal.UserId, PrincipalFactory.CurrentPrincipal.BranchId);

            if (!login.UserEnabled)
            {
                e.Item.Visible = false;
            }
            else
            {

                TextBox username = (TextBox)e.Item.FindControl("Username");
                TextBox password = (TextBox)e.Item.FindControl("Password");
                TextBox clientId = (TextBox)e.Item.FindControl("ClientId");
                BindCredentials(login, username, password, clientId);

                Label VendorName = (Label)e.Item.FindControl("VendorName");
                VendorName.Text = vendor.Skin.VendorName;

                HiddenField VendorId = (HiddenField)e.Item.FindControl("VendorId");
                VendorId.Value = vendor.Config.VendorId.ToString();

                e.Item.FindControl("UsernamePanel").Visible = vendor.Config.UsesUsername;
                e.Item.FindControl("PasswordPanel").Visible = vendor.Config.UsesPassword;
                e.Item.FindControl("ClientIdPanel").Visible = vendor.Config.UsesAccountId;

                readOnly.Skip = username;
                readOnly.Skip = password;
                readOnly.Skip = clientId;
            }

        }

        /// <summary>
        /// 4506T ItemDataBound for m_irs4506TVendors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void IRS4506TVendorBind(object sender, RepeaterItemEventArgs e)
        {
            Irs4506TVendorConfiguration vendor = (Irs4506TVendorConfiguration)e.Item.DataItem;
            ((Label)e.Item.FindControl("VendorName")).Text = vendor.VendorName;

            HiddenField vendorField = (HiddenField)e.Item.FindControl("VendorId");
            TextBox accountId = (TextBox)e.Item.FindControl("LoginAccountId");
            TextBox username = (TextBox)e.Item.FindControl("LoginUserName");
            TextBox password = (TextBox)e.Item.FindControl("LoginPassword");

            vendorField.Value = vendor.VendorId.ToString();

            Irs4506TVendorCredential credential = Irs4506TVendorCredential.Retrieve(m_brokerID, m_Employee.UserID, vendor.VendorId);
            accountId.Text = credential.AccountId;
            username.Text = credential.UserName;
            if (credential.DecryptedPassword == String.Empty)
            {
                password.Text = String.Empty;
                password.Attributes.Add("value", String.Empty);
            }
            else
            {
                password.Text = ConstAppDavid.FakePasswordDisplay;
                password.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
            }

            e.Item.FindControl("LoginAccountIdPanel").Visible = vendor.IsUseAccountId;

            readOnly.Skip = username;
            readOnly.Skip = password;
            readOnly.Skip = accountId;
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            m_ZipcodeTF.SmartZipcode(m_CityTF, m_StateTF);
            Regularexpressionvalidator2.ValidationExpression = ConstApp.EmailValidationExpression;
            Regularexpressionvalidator2.Text = ErrorMessages.InvalidEmailAddress;

			readOnly.Enabled = true;
            this.RegisterJsScript("ServiceCredential.js");
            this.RegisterCSS("ServiceCredential.css");
            this.RegisterJsScript("LQBPopup.js");
        }

        private void BindCredentials(IDocumentVendorCredentials c, TextBox username, TextBox password, TextBox clientId)
        {
            if (c.Password == String.Empty)
            {
                password.Text = String.Empty;
                password.Attributes.Add("value", String.Empty);
            }
            else
            {
                password.Text = ConstAppDavid.FakePasswordDisplay;
                password.Attributes.Add("value", ConstAppDavid.FakePasswordDisplay);
            }
            username.Text = c.UserName;
            clientId.Text = c.CustomerId;
        }
        private bool sendEmailNotification = false;
        private void BindEmployee() 
        {
            CommonLib.Address addr = new CommonLib.Address();
            addr.StreetAddress = m_AddressTF.Text;
            addr.City = m_CityTF.Text;
            addr.State = m_StateTF.Value;
            addr.Zipcode = m_ZipcodeTF.Text;

            m_Employee.BranchID = m_branchID;
            m_Employee.FirstName = m_FirstName.Text;
            m_Employee.LastName = m_LastName.Text;
            m_Employee.Address = addr;
            m_Employee.Phone = m_PhoneTF.Text;
            m_Employee.PrivateCellPhone = m_CellTF.Text;
            m_Employee.IsCellphoneForMultiFactorOnly = IsCellphoneForMultiFactorOnly.Checked;
            m_Employee.Fax = m_FaxTF.Text;
            m_Employee.Email = m_EmailTF.Text;
			m_Employee.AgentLicenseNumber = m_AgentLicenseNumber.Text;
            m_Employee.IsActive = true;
            m_Employee.LoginName = m_Login.Text;
            m_Employee.Password = m_NewPassword.Text;

            if (!String.IsNullOrEmpty(m_SupportEmail.Text.TrimWhitespaceAndBOM()) && !m_Employee.HasSupportEmail)
            {
                m_Employee.SetSupportEmail(m_SupportEmail.Text, false);
                sendEmailNotification = true;
            }

            m_Employee.NewOnlineLoanEventNotifOptionT = (E_NewOnlineLoanEventNotifOptionT) m_newOnlineLoanEventNotifOptionT.SelectedIndex;
            m_Employee.TaskRelatedEmailOptionT = (E_TaskRelatedEmailOptionT)m_TaskRelatedNotifOptionT.SelectedIndex;
            m_Employee.OptsToUseNewLoanEditorUI = m_OptsToUseNewLoanEditorUI.Checked;

            if (IsEnableAuthCodeViaAuthenticator.Value == "disabled")
            {
                m_Employee.EnableAuthCodeViaAuthenticator = false;
            }
        }

        /// <summary>
        /// Keep track of supported roles.
        /// </summary>

        private class RoleDescriptor
		{
			#region ( Descriptor Properties )

			private String m_RoleDesc = String.Empty;
			private Guid     m_RoleId = Guid.Empty;
			private Boolean      m_On = false;

			public String RoleDesc
			{
				// Access member.

				set
				{
					m_RoleDesc = value;
				}
				get
				{
					return m_RoleDesc;
				}
			}

			public Boolean On
			{
				// Access member.

				set
				{
					m_On = value;
				}
				get
				{
					return m_On;
				}
			}

			public Guid RoleId
			{
				// Access member.

				set
				{
					m_RoleId = value;
				}
				get
				{
					return m_RoleId;
				}
			}

			#endregion

			/// <summary>
			/// Override default for ui binding of keys.
			/// </summary>

			public override string ToString()
			{
				// Override default for ui binding of keys.

				return m_RoleId.ToString();
			}

		}

        private void LoadEmployee() 
        {
            // 4/26/2005 kb - Revised this page and merged it with external
            // user controls to make it easier to maintain, per case #1346.
            EnableAuthCodeViaAuthenticator.Checked = m_Employee.EnableAuthCodeViaAuthenticator;

            m_branchID = m_Employee.BranchID ;
            m_FirstName.Text = m_Employee.FirstName ;
            m_LastName.Text = m_Employee.LastName ;

            CommonLib.Address addr = m_Employee.Address ;
            m_AddressTF.Text = addr.StreetAddress ;
            m_CityTF.Text = addr.City ;
            m_StateTF.Value = addr.State ;
            m_ZipcodeTF.Text = addr.Zipcode ;

            m_PhoneTF.Text = m_Employee.Phone ;
            m_CellTF.Text = m_Employee.PrivateCellPhone;
            this.IsCellphoneForMultiFactorOnly.Checked = m_Employee.IsCellphoneForMultiFactorOnly;
            m_FaxTF.Text = m_Employee.Fax ;
            m_EmailTF.Text = m_Employee.Email ;
			m_AgentLicenseNumber.Text = m_Employee.AgentLicenseNumber;
            m_Login.Text = m_Employee.LoginName ;

            m_SupportEmail.Text = m_Employee.SupportEmail ?? "";
            m_SupportEmail.ReadOnly = m_Employee.HasSupportEmail && m_Employee.SupportEmailVerified;

            UnconfirmedPlaceHolder.Visible = m_Employee.HasSupportEmail && !m_Employee.SupportEmailVerified;
            ConfirmedPlaceholder.Visible = m_Employee.HasSupportEmail && m_Employee.SupportEmailVerified;
    

			if(IsPmlEnabled)
			{
				if(m_Employee.RatesheetExpirationBypassType == E_RatesheetExpirationBypassType.NoBypass)
					m_rateLockExpLabel.Text = "Never allowed";
				else if(m_Employee.RatesheetExpirationBypassType == E_RatesheetExpirationBypassType.BypassAllExceptInvCutoff)
					m_rateLockExpLabel.Text = "Not allowed after investor cutoff time";
				else
					m_rateLockExpLabel.Text = "Always allowed";
			}

            m_newOnlineLoanEventNotifOptionT.SelectedIndex = (int) m_Employee.NewOnlineLoanEventNotifOptionT;
            m_TaskRelatedNotifOptionT.SelectedIndex = (int)m_Employee.TaskRelatedEmailOptionT;
            m_OptsToUseNewLoanEditorUI.Checked = m_Employee.OptsToUseNewLoanEditorUI;

            m_accountManagerLabel.Visible = m_Employee.IsAccountOwner;
            m_profileImage.Visible = m_Employee.IsAccountOwner;

            if (AllowCustomLogin)
            {
                IEnumerable<IDocumentVendor> filter = DocumentVendorFactory.CurrentVendors();
                if (!Broker.DocMagicIsAllowuserCustomLogin)
                {
                    // 7/9/2014 AV -  // 7/9/2014 AV - 186833 DocMagic: Allow editing personal login not working 
                    filter = filter.Where(p => p.Config.VendorId != Guid.Empty);
                }

                DocumentVendorCustomLogins.DataSource = filter;
                DocumentVendorCustomLogins.DataBind();
            } 
            
            if (CanCreateGlobalDMSLogin)
            {
                m_appraisalVendors.DataSource = AppraisalVendorFactory.GetAvailableVendorsForBroker(Broker.BrokerID);
                m_appraisalVendors.DataBind();
            }

            if (HasIrs4506TVendorEnabledAndCanOrder)
            {
                m_irs4506TVendors.DataSource = Irs4506TVendorConfiguration.ListActiveVendorByBrokerId(Broker.BrokerID);
                m_irs4506TVendors.DataBind();
            }
            // Fill out permissions and bind lists.  We want to populate the
			// repeater with all the entries possible.  We turn on the options
			// in the next section.

			AdminAccessLevelTable aTable = new AdminAccessLevelTable();
            if (!Broker.EnableTeamsUI)
            {
                aTable.RemoveAt(2);
            }
			m_employeeAccessLevel.DataSource = aTable;
			m_employeeAccessLevel.DataBind();

			// 3/11/2005 kb - Set permissions for loan.  Note that we're
			// binding to a dynamic list.  The list is generated on-the-
			// fly using our current admin list.  We only check the boxes
			// that a user has.  Note that we don't need to update this
			// control anymore when we add a permission.  Nice.

			BrokerUserPermissions bUp = new BrokerUserPermissions( BrokerUser.BrokerId , BrokerUser.EmployeeId );

			foreach( RepeaterItem rItem in m_employeeAccessLevel.Items )
			{
				CheckBox cBox = rItem.FindControl( "On" ) as CheckBox;

				if( cBox != null )
				{
					cBox.Checked = bUp.HasPermission( int.Parse( cBox.Attributes[ "code" ] ) );
				}
			}

			// 4/26/2005 kb - Get the roles that are possible and then
			// bind what we are to the list.

            EmployeeRoles empRoles = new EmployeeRoles(BrokerUser.BrokerId, BrokerUser.EmployeeId);
            List<RoleDescriptor> roleList = new List<RoleDescriptor>();
            Dictionary<Guid, RoleDescriptor> lookItUp = new Dictionary<Guid, RoleDescriptor>();

            foreach (var role in Role.LendingQBRoles)
			{
				RoleDescriptor rD = new RoleDescriptor();

				rD.RoleDesc = role.ModifiableDesc;
				rD.RoleId   = role.Id;

				lookItUp.Add( rD.RoleId , rD );

				roleList.Add( rD );
			}

			foreach( var role in empRoles.Items )
			{
				RoleDescriptor rD = lookItUp[ role.Id ];

				if( rD != null )
				{
					rD.On = true;
				}
			}

			m_employeeRoleListing.DataSource = roleList.OrderBy(role => role.RoleDesc);
			m_employeeRoleListing.DataBind();

			foreach( RepeaterItem rItem in m_employeeRoleListing.Items )
			{
				CheckBox cBox = rItem.FindControl( "On" ) as CheckBox;

				if( cBox != null )
				{
					RoleDescriptor rD = lookItUp[ new Guid( cBox.Attributes[ "code" ] ) ];

					if( rD != null )
					{
						cBox.Checked = rD.On;
					}
				}
			}
		}

        protected void AppraisalVendorBind(object sender, RepeaterItemEventArgs args)
        {
            AppraisalVendorConfig vendor = (AppraisalVendorConfig)args.Item.DataItem;
            ((Label)args.Item.FindControl("VendorName")).Text = vendor.VendorName;

            HiddenField vendorField = (HiddenField)args.Item.FindControl("VendorId");
            TextBox accountId = (TextBox)args.Item.FindControl("LoginAccountId");
            TextBox username = (TextBox)args.Item.FindControl("LoginUserName");
            TextBox password = (TextBox)args.Item.FindControl("LoginPassword");

            vendorField.Value = vendor.VendorId.ToString();

            AppraisalVendorCredentials login = new AppraisalVendorCredentials(BrokerUser.BrokerId, m_Employee.ID, vendor.VendorId);
            accountId.Text = login.AccountId;
            username.Text = login.UserName;

            args.Item.FindControl("AccountIdField").Visible = vendor.UsesAccountId;

            readOnly.Skip = accountId;
            readOnly.Skip = username;
            readOnly.Skip = password;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

        private void SetError(string msg)
        {
            SaveErrorMessage.Text = msg;
            ErrorMessagePlaceholder.Visible = true;
        }

        protected void m_Save_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //Check for doc magic credentials; must have both login & password or neither
                foreach (RepeaterItem item in DocumentVendorCustomLogins.Items)
                {
                    if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
                        continue;
                    if (!item.Visible)
                        continue;

                    HiddenField VendorIdTB = (HiddenField)item.FindControl("VendorId");
                    Guid vendorId = new Guid(VendorIdTB.Value);

                    TextBox username = (TextBox)item.FindControl("Username");
                    TextBox password = (TextBox)item.FindControl("Password");

                    if (vendorId == Guid.Empty)
                    {
                        if (string.IsNullOrEmpty(username.Text) && !string.IsNullOrEmpty(password.Text)
                            ||
                            !string.IsNullOrEmpty(username.Text) && string.IsNullOrEmpty(password.Text))
                        {
                            Literal m_dm_Error = (Literal)item.FindControl("m_dm_Error");
                            m_dm_Error.Text = "DocMagic login info must have either both username and password or neither.";
                            return;
                        }
                        break;
                    }
                }
                if( m_NewPassword.Text != "")
                {
                    if ( !m_Employee.IsCurrentPassword(m_OldPassword.Text) ) 
                    {
                        m_OldPasswordErrorMsg.Text = "Old password does not match current -- please retype.";
                        return;
                    } 
                    else 
                    {
                        // 4/7/2004 dd - Since password cannot contains firstname, last name. I need to check
                        // against new first name, last name NOT old first name, last name.
                        if (!Broker.DisableNameEditing)
                        {
                            m_Employee.FirstName = m_FirstName.Text;
                            m_Employee.LastName = m_LastName.Text;
                        }
                        StrongPasswordStatus ret = m_Employee.IsStrongPassword(m_NewPassword.Text);
                        m_OldPasswordErrorMsg.Text = EmployeeDB.GetStrongPasswordUserMessage(ret) ?? string.Empty;
                        if (ret != StrongPasswordStatus.OK)
                            return;
                    }

                }

                
                BindEmployee();

                // Try and save SecurityQuestions. Stop save if theres an error
                // (Should probably try and do validation in javascript).
                if (!SecurityQuestions.Save())
                {
                    ClientScript.RegisterStartupScript(typeof(myprofile), "SECURITY_QUESTIONS_SAVE_FAIL", "showSecurityInfoError('" + SecurityQuestions.StatusMessage + "');", true);
                    return;
                }

                try
                {
                    m_Employee.Save(PrincipalFactory.CurrentPrincipal);
                    if (this.sendEmailNotification)
                    {
                        ClientScript.RegisterHiddenField("ShowSupportEmailConfirmation", string.Format("An e-mail has be sent to {0}.  Please open the link in the e-mail in internet explorer and log in to verify your e-mail address", m_SupportEmail.Text));
                    }
                }

                catch (CBaseException ex)
                {
                    SetError(ex.UserMessage);
                    return;
                }

                string invalidCredentialsErrors = null;
                foreach (RepeaterItem item in DocumentVendorCustomLogins.Items)
                {
                    if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
                        continue;
                    if (!item.Visible)
                        continue;
                    
                    HiddenField VendorIdTB = (HiddenField)item.FindControl("VendorId");
                    Guid vendorId = new Guid(VendorIdTB.Value);

                    TextBox username = (TextBox)item.FindControl("Username");
                    TextBox password = (TextBox)item.FindControl("Password");
                    TextBox clientId = (TextBox)item.FindControl("ClientId");

                    var vendor = DocumentVendorFactory.Create(m_brokerID, vendorId);

                    try
                    {
                        saveCredentials(vendor.Credentials, vendor.Config, username, password, clientId);
                    }
                    catch (ArgumentException ae) when (ae.Message == "Can't assign a password such that its encrypted form is longer than 150 characters.")
                    {
                        invalidCredentialsErrors += $"Password for {vendor.Skin.VendorName} is too long.{Environment.NewLine}";
                    }
                    catch (ArgumentException)
                    {
                        invalidCredentialsErrors += $"Please re-enter the credentials for {vendor.Skin.VendorName}.{Environment.NewLine}";
                    }
                }

                if (invalidCredentialsErrors != null)
                {
                    this.SetError(invalidCredentialsErrors);
                    return;
                }

                //Save Appraisal Credentials using separate stored procedure
                foreach (RepeaterItem item in m_appraisalVendors.Items)
                {
                    if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
                        continue;
                    HiddenField vendorField = (HiddenField)item.FindControl("VendorId");
                    TextBox accountId = (TextBox)item.FindControl("LoginAccountId");
                    TextBox username = (TextBox)item.FindControl("LoginUserName");
                    TextBox password = (TextBox)item.FindControl("LoginPassword");
                    Guid vendorId = new Guid(vendorField.Value);
                    AppraisalVendorCredentials login = new AppraisalVendorCredentials(BrokerUser.BrokerId, m_Employee.ID, vendorId);
                    login.AccountId = accountId.Text;
                    login.UserName = username.Text;
                    if (!string.IsNullOrEmpty(password.Text))
                        login.Password = password.Text;
                    login.SaveCredentials();
                }

                foreach (RepeaterItem item in m_irs4506TVendors.Items)
                {
                    if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem)
                    {
                        continue;
                    }
                    HiddenField vendorField = (HiddenField)item.FindControl("VendorId");
                    TextBox accountId = (TextBox)item.FindControl("LoginAccountId");
                    TextBox username = (TextBox)item.FindControl("LoginUserName");
                    TextBox password = (TextBox)item.FindControl("LoginPassword");
                    Guid vendorId = new Guid(vendorField.Value);
                    Irs4506TVendorCredential login = Irs4506TVendorCredential.Retrieve(m_brokerID, m_Employee.UserID, vendorId);
                    login.UserName = username.Text;
                    login.AccountId = accountId.Text;
                    if (!string.IsNullOrEmpty(password.Text) && !password.Text.Equals(ConstAppDavid.FakePasswordDisplay))
                    {
                        login.SetPassword(password.Text);
                    }
                    login.Save();
                }

                LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this);
            }
        }

        private void saveCredentials(IDocumentVendorCredentials credentials, VendorConfig config, TextBox username, TextBox password, TextBox id)
        {
            if (config.UsesUsername) credentials.UserName = username.Text;
            if (config.UsesPassword && PasswordChanged(password)) credentials.Password = password.Text;
            //DocMagic has special considerations here, so we rely on something set previously instead of the vendor config
            //Users can't change their own account id for doc magic, but they can for other vendors.
            if (id != null) credentials.CustomerId = id.Text;
            credentials.SaveUserCredentials();
        }
        
        private void validatePassword(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;
            if (m_NewPassword.Text != m_ConfirmPassword.Text) 
            {
                args.IsValid = false;
            }
        }

        private bool PasswordChanged(TextBox pwd)
        {
            return pwd.Text != ConstAppDavid.FakePasswordDisplay;
        }

        private void DisplayUploadError(string msg)
        {
            divError.Visible = true;
            divError.InnerText = msg;
        }

        protected void btnSignatureUpload_Click(object sender, EventArgs e)
        {
            if (!this.Broker.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                DisplayUploadError("Signature is not enabled.");
                return;
            }

            if (signatureUpload.HasFile)
            {
                try
                {
                    if (!ImageUtils.IsValidImageType(signatureUpload.PostedFile.ContentType))
                    {

                        DisplayUploadError("Incorrect file type, must be a JPEG, PNG, GIF or BMP file.");
                        return;
                    }
                    if (signatureUpload.PostedFile.ContentLength > MAX_IMG_SIZE)
                    {
                        DisplayUploadError("Selected file is too large. Images must be smaller than 100KB.");
                        return;
                    }

                    System.Drawing.Image im = System.Drawing.Image.FromStream(signatureUpload.PostedFile.InputStream);

                    //Do not resize the image. case 69393
                    //System.Drawing.Image im = ImageUtils.ResizeImage(signatureUpload.PostedFile.InputStream, 160, 18);

                    Guid signatureKey = Guid.NewGuid();
                    if (!ImageUtils.SaveImageToFileDB(signatureKey.ToString(), im))
                    {
                        DisplayUploadError("Unable to save file. Please try again.");
                        return;
                    }

                    //Save filename with employee's account
                    EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(BrokerUser.BrokerId, BrokerUser.EmployeeId, Broker.EnableLqbNonCompliantDocumentSignatureStamp);

                    if (signInfo.SignatureKey != Guid.Empty)
                    {
                        signInfo.DeleteSignature(); //this would delete from the fileDB too
                    }

                    signInfo.SaveSignatureKey(signatureKey);
                }
                catch (Exception ex)
                {
                    DisplayUploadError("The image file could not be uploaded.");
                    Tools.LogError(ex);
                }
            }
            else
            {
                DisplayUploadError("Please specify a valid file");
                return;
            }

            divError.Visible = false;
            btnRemoveImage.Enabled = true;
        }

        protected void OnRemoveSignature_Click(object sender, EventArgs e)
        {
            if (!this.Broker.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(BrokerUser.BrokerId, BrokerUser.EmployeeId, Broker.EnableLqbNonCompliantDocumentSignatureStamp);
            signInfo.DeleteSignature();
            btnRemoveImage.Enabled = false;
        }      
	}
}
