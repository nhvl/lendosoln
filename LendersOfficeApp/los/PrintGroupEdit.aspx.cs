using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Xml;

using DataAccess;
using LendersOffice.Common;
using LendersOffice.Pdf;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LendersOffice.PdfForm;
using LendersOffice.Constants;
namespace LendersOfficeApp.los
{
	public partial class PrintGroupEdit : LendersOffice.Common.BaseServicePage
	{

        private List<ListItem> m_list = new List<ListItem>();
    
		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        private void LoadGroup() 
        {
			Guid gId;
			try
			{
				gId = new Guid(m_GroupID.Value);
			}
			catch(System.FormatException)
			{
				//new group that hasn't been saved yet
				return;
			}
			SqlParameter[] parameters = {
                                            new SqlParameter("@GroupID", gId),
                                            new SqlParameter("@BrokerID", BrokerUser.BrokerId)
                                        };
            string xml = "";
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "RetrievePrintGroup", parameters)) 
            {
				if (reader.Read()) 
                {
					if(!m_CommandToDo.Value.Equals("Duplicate"))
						m_GroupName.Text = (string) reader["GroupName"];
                    xml = (string) reader["GroupDefinition"];
                }
            }

            XmlDocument doc = Tools.CreateXmlDoc(xml);
            XmlNodeList xmlNodeList = doc.SelectNodes("//group/print");

            foreach (XmlElement el in xmlNodeList) 
            {
                ListItem item = null;
                string type = el.GetAttribute("type");

                // Skip the 2015 IFW 
                // And the 2015 Fuding Worksheet
                if (type == "LendersOffice.Pdf.CIFW2015" || type == "LendersOffice.Pdf.CFundingWorksheet2015")
                {
                    continue;
                }

                for (int i = 0; i < m_list.Count; i++) 
                {
                    // 5/27/2004 dd - Loop through the master list and remove ListItem from master list if found.
                    ListItem o = m_list[i];
                    if (string.Equals(o.Value, type, StringComparison.OrdinalIgnoreCase)) 
                    {
                        item = o;
                        m_list.RemoveAt(i);
                        break; 
                    }
                }

                if (null != item) 
                    m_groupList.Items.Add(item);
            }
        }

		private void LoadMasterList() 
        {
            foreach (var pdf in LendersOffice.Pdf.PdfPrintList.RetrieveOnlyPrintItem()) 
            {
                // 2/7/2005 dd - Print item such as Custom Forms should not show up in master list.
                if (pdf.IsExcludePrintGroup)
                    continue;

                string value = pdf.Type;

                // We'll skip the 2015 IFW and add it in manually if the user chose to add the original IFW in later.
                // Same with the Funding Worksheet
                if (pdf.Type == "LendersOffice.Pdf.CIFW2015" || pdf.Type == "LendersOffice.Pdf.CFundingWorksheet2015")
                {
                    continue;
                }

                ConstructorInfo c = PDFClassHashTable.GetConstructor(value);
                IPDFPrintItem o = (IPDFPrintItem) c.Invoke(new object[0]);

                string text = StripTags(o.Description);
                m_list.Add(new ListItem(text, value));
            }

            #region Add PDF Custom Forms to master list.
            m_list.Add(new ListItem("---------- PDF Custom Forms ----------", ""));
            foreach (var pdf in PdfForm.RetrieveCustomFormsOfCurrentBroker())
            {
                m_list.Add(new ListItem(pdf.Description, ConstAppDavid.CustomPdfProtocolPrefix + pdf.FormId));
            }
            #endregion

            #region Add Word Custom Forms to master list.
            if (PrincipalFactory.CurrentPrincipal.BrokerDB.IsCustomWordFormsEnabled)
            {
                m_list.Add(new ListItem("---------- Custom Forms ----------", ""));
                // 2/4/2005 dd - List individual custom forms here.
                SqlParameter[] parameters = { new SqlParameter("@BrokerID", BrokerUser.BrokerId) };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "ListCustomFormByBrokerID", parameters))
                {
                    while (reader.Read())
                    {
                        Guid customFormID = (Guid)reader["CustomLetterID"];
                        string title = (string)reader["Title"];
                        m_list.Add(new ListItem(title, "CustomForm:" + customFormID));
                    }
                }
            }
            #endregion
        }

		private void DuplicatePrintGroup()
		{
			ClientScript.RegisterHiddenField("GroupID", null);
			m_GroupID.Value = null;
			m_GroupName.Text = "";
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			if(!m_CommandToDo.Value.Equals("Duplicate"))
				m_GroupID.Value = RequestHelper.GetGuid("groupid", Guid.Empty).ToString();

            LoadMasterList();

            if (m_GroupID.Value != Guid.Empty.ToString()) 
				LoadGroup();

			foreach (ListItem item in m_list) 
			{
				m_mainList.Items.Add(item);
			}

			if(Page.IsPostBack && m_CommandToDo.Value.Equals("Duplicate"))
				DuplicatePrintGroup();
			else
				ClientScript.RegisterHiddenField("GroupID", m_GroupID.Value);


			// 3/24/2005 kb - We now block button clicking when permission
			// to edit print groups is denied.

			if( BrokerUser.HasPermission( Permission.CanEditPrintGroups ) == true )
			{
				m_saveItPanel.Enabled = true;
				m_saveItPanel.ToolTip = "";

				m_deletePanel.Enabled = true;
				m_deletePanel.ToolTip = "";

				m_duplicatePanel.Enabled = true;
				m_duplicatePanel.ToolTip = "";

				m_addToGPanel.Enabled = true;
				m_addToGPanel.ToolTip = "";

				m_removePanel.Enabled = true;
				m_removePanel.ToolTip = "";

				m_moveUpPanel.Enabled = true;
				m_moveUpPanel.ToolTip = "";

				m_moveDnPanel.Enabled = true;
				m_moveDnPanel.ToolTip = "";

				m_groupList.BackColor = Color.White;

				m_mainList.BackColor = Color.White;

				m_GroupName.ReadOnly = false;
			}
			else
			{
				m_saveItPanel.Enabled = false;
				m_saveItPanel.ToolTip = ErrorMessages.InsufficientPermissionToEditPrintGroups;

				m_deletePanel.Enabled = false;
				m_deletePanel.ToolTip = ErrorMessages.InsufficientPermissionToEditPrintGroups;
				
				m_duplicatePanel.Enabled = false;
				m_duplicatePanel.ToolTip = ErrorMessages.InsufficientPermissionToEditPrintGroups;

				m_addToGPanel.Enabled = false;
				m_addToGPanel.ToolTip = ErrorMessages.InsufficientPermissionToEditPrintGroups;

				m_removePanel.Enabled = false;
				m_removePanel.ToolTip = ErrorMessages.InsufficientPermissionToEditPrintGroups;

				m_moveUpPanel.Enabled = false;
				m_moveUpPanel.ToolTip = ErrorMessages.InsufficientPermissionToEditPrintGroups;

				m_moveDnPanel.Enabled = false;
				m_moveDnPanel.ToolTip = ErrorMessages.InsufficientPermissionToEditPrintGroups;

				m_groupList.BackColor = Color.LightGray;
				m_groupList.Attributes.Add("NotForEdit", "true");

				m_mainList.BackColor = Color.LightGray;
				m_mainList.Attributes.Add("NotForEdit", "true");

				m_GroupName.ReadOnly = true;
			}
		}
        
        private string StripTags(string sInput)
        { 
            return System.Text.RegularExpressions.Regex.Replace(sInput, "<[^>]*>", "");            
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            RegisterVbsScript("common.vbs");
            RegisterService("printgroupedit", "/los/PrintGroupEditService.aspx");
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}

}
