﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.los
{
    public partial class InstallClientCertificate : BasePage
    {

        private AbstractUserPrincipal CurrentPrincipal
        {
            get { return PrincipalFactory.CurrentPrincipal; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Page.IsPostBack == false)
            {
                string cmd = RequestHelper.GetSafeQueryString("cmd");
                if (cmd == "download")
                {
                    DownloadCertificate();
                    return;
                }
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            this.m_continueBtn.Click += new EventHandler(m_continueBtn_Click);
        }

        private void m_continueBtn_Click(object sender, EventArgs e)
        {
            Panel0.Visible = false;
            Panel1.Visible = true;

            m_certificatePassword.Text = MultiFactorAuthCodeUtilities.GenerateRandom(6);

            string str = EncryptionHelper.Encrypt(m_certificatePassword.Text + m_description.Text);

            str = str.Replace("+", ".").Replace("=", "-");

            RegisterJsGlobalVariables("EncryptedString", str);



        }
        private void DownloadCertificate()
        {

            string encryptedString = RequestHelper.GetSafeQueryString("arg");
            encryptedString = encryptedString.Replace(".", "+").Replace("-", "=");
            string str = EncryptionHelper.Decrypt(encryptedString);

            string password = str.Substring(0, 6);
            string description = str.Substring(6);

            byte[] bytes = ClientCertificate.CreateClientCertificate(CurrentPrincipal, description, password);

            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=\"certificate.p12\"");
            Response.ContentType = "application/octet-stream";

            Response.OutputStream.Write(bytes, 0, bytes.Length);
            Response.Flush();
            Response.End();
        }
    }
}
