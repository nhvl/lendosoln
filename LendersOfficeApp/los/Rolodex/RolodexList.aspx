<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc2" TagName="Tabs" Src="~/common/BaseTabPage.ascx"  %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="RolodexList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Rolodex.RolodexList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>Contacts</title>

        <style>
            .download-frame {
                visibility: hidden;
                height: 0;
            }
        </style>
		
	</HEAD>
	<script type="text/javascript">
        
        jQuery(function($){
            var tabs = $('#ExternalAgentList, #m_PageInvestorList, #InternalEmployee, #m_PageWarehouseLenderList');
            var tabParent = $('ul.tabnav li');
            var hash = $('#Hash'); 
            
            $('a.tabchange').click(function(){
                tabs.css('display', 'none');
                tabParent.removeClass('selected');
                var loc = $(this).attr('data-tab');
                $(loc).css('display', 'block');
                $(this).parent().addClass('selected');
                hash.val(loc);
            });
            
            tabs.hide();
            $(hash.val()).show();
            
            if (hash.val() == '#ExternalAgentList'){
                $('li.t_1').addClass('selected');
            }
            else if (hash.val() == '#m_PageInvestorList'){
                $('li.t_2').addClass('selected');
            }
            else if(hash.val() == '#m_PageWarehouseLenderList') {
                $('li.t_4').addClass('selected');
            }
            else {
                $('li.t_3').addClass('selected');
            }
            
        });
        
        <!--
     
        function _init()
        {
          <% if ( !Page.IsPostBack ) { %>
          resize( 858 , 598 );
          <% } %>
          
          var ddl = document.getElementById('InvestorStatus');
          onInvestorStatusChange.call(ddl);
          
          ddl = document.getElementById('WarehouseLenderStatus');
          onWarehouseLenderStatusChange.call(ddl);
          
          <% if( IsAffiliatesDialog ) { %>
          $("#title").text("Choose from Contacts");
          $("#tabsRow").hide();
          $("#addButtonsRow").hide();
          <% } %>
        }

        function onAddClick() {
          showModal('/los/rolodex/rolodex.aspx', null, null, null, function(args) {
                if (args != null && args.OK) {
                    __doPostBack('',''); //refresh
                }
            },{ hideCloseButton:true });
        }

        function onEditClick(agentid) {
          showModal('/los/rolodex/rolodex.aspx?cmd=edit&agentid=' + agentid, null, null, null, function(args){
                if (args != null && args.OK) {
                    __doPostBack('',''); //refresh
                }
            },{ hideCloseButton:true });
        }

		function onImportOutlookClick() {
            var args = new Object();
            args.isOutlook = true;
            showModal('/los/rolodex/RolodexImport.aspx', args, null, null, function(args){
                if (args != null && args.OK) {
                    __doPostBack('',''); //refresh
                }
		   },{ hideCloseButton:true });
        }

        function onImportOtherClick() {
            var args = new Object();
            args.isOutlook = false;
            showModal('/los/rolodex/RolodexImport.aspx', args, null, null, function(args){
                if (args != null && args.OK) {
                    __doPostBack('',''); //refresh
                }
            },{ hideCloseButton:true });
        }

        function onInvestorAddClick() {
          showModal('/los/Rolodex/InvestorEditor.aspx', null, null, null, function(){
				__doPostBack('',''); //refresh
			},{ hideCloseButton:true });
        }

		function onExportClick() {
			//needed to eliminate the undesired highlighting of the first "submit" type on the page
			<%= AspxTools.JsGetElementById(m_exportButton) %>.click();
		}
	
		function onSearchClick()
		{
			//needed to eliminate the undesired highlighting of the first "submit" type on the page
			<%= AspxTools.JsGetElementById(m_searchRolodexButton) %>.click();
		}
		
	    function investorEdit(anchor) {
			showModal('/los/Rolodex/InvestorEditor.aspx?id='+anchor.getAttribute('investorid'), null, null, null, function(){
				__doPostBack('',''); //refresh
			},{ hideCloseButton:true });
	    }

	    function investorExport() {
            $(".download-frame").prop("src", ML.VirtualRoot + "/los/rolodex/InvestorCsvExporter.aspx");
	    }
		
		function onWarehouseLenderAddClick() {
          showModal('/los/Rolodex/WarehouseLenderEditor.aspx', null, null, null, function(){
				__doPostBack('',''); //refresh
			},{ hideCloseButton:true });
		}
		
		function onWarehouseLenderEditClick(anchor) {
		    showModal('/los/Rolodex/WarehouseLenderEditor.aspx?id='+anchor.getAttribute('warehouselenderid'), null, null, null, function(){
				__doPostBack('',''); //refresh
			},{ hideCloseButton:true });
		}
		
		function onInvestorStatusChange() {
		    var id = this.value, table = document.getElementById('InvestorEntries');
		    
		    if (!table){
		        return;
		    }
		    
		    var rows = table.tBodies[0].rows, rowCount = rows.length, i = 0, row;
            
            for (i; i < rowCount; i++){
                row = rows[i]; 
                if (row.getAttribute('data-status') == id){
                    row.style.display = '';
                }
                else {
                    row.style.display = 'none';
                }
            }
		}
		function onWarehouseLenderStatusChange() {
		    var id = this.value, table = document.getElementById('WarehouseLenderEntries');
		    
		    if (!table){
		        return;
		    }
		    
		    var rows = table.tBodies[0].rows, rowCount = rows.length, i = 0, row;
            
            for (i; i < rowCount; i++){
                row = rows[i]; 
                if (row.getAttribute('data-status') == id){
                    row.style.display = '';
                }
                else {
                    row.style.display = 'none';
                }
            }
		}
		
		function approveDisableContact(item, isApproved) {
		    var message = (isApproved ? "Disable" : "Approve") + " this user?";
		    if(confirm(message)) {
		        $(item).siblings('.approveDisableClick')[0].click();
		    }
		}

		function duplicateClick(item) {
		    if(confirm( 'Duplicate this contact entry?' )) {
		        $(item).siblings('.duplicateClick')[0].click();
		    }
		}

		function deleteClick(item) {
		    if(confirm( 'Are you sure you want to delete?\nNote: Deleting this contact will remove any existing relationships.')) {
		        $(item).siblings('.deleteClick')[0].click();
		    }
		}

		$(document).on("click", ".select", function() {
		    var $this = $(this);
            var $tr = $($this.parents("tr")[0]);
            
		    // Set output
		    var args = window.dialogArguments || {};
            args.id = $this.attr("data-id");
            args.status = $tr.find(".status").text();
            args.type = $tr.find(".type").text();
            args.name = $tr.find(".name").text();
            args.compName = $tr.find(".compName").text();
            args.systemId = $tr.find(".systemId").text();
            args.OK = true;

            // close output
            onClosePopup(args);
		});
		
	    //-->
		</script>
	<body bgColor="gainsboro" scroll="no" MS_POSITIONING="FlowLayout">
        <h4 class="page-header" id="title">Contacts</h4>
		<form id="RolodexList" method="post" runat="server">
		    <asp:HiddenField runat="server" ID="Hash" />
			<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr id="tabsRow">
                    <td class="FormTableHeader">
                        <div class="Tabs">
                            <ul class="tabnav">
                                <li class="t_1"><a class="tabchange" href="#" data-tab="#ExternalAgentList">Contact Entries</a></li>
                                <li runat="server" id="m_InvestorTab" class="t_2" ><a  data-tab="#m_PageInvestorList" href="#" class="tabchange">Investor List</a></li>
                                <li class="t_4"><a class="tabchange" href="#" data-tab="#m_PageWarehouseLenderList">Warehouse Lenders</a></li>
                                <li class="t_3"><a href="#" class="tabchange" data-tab="#InternalEmployee" >Internal Employees</a></li>
                            </ul>
                        </div>
                    </td>
				</tr>
				<tr>
					<td style="PADDING-RIGHT: 4px;PADDING-LEFT: 4px;PADDING-BOTTOM: 4px;PADDING-TOP: 4px">
							<div id="ExternalAgentList">
								<table cellspacing="0" cellpadding="0" width="100%" border="0">
									<tr style="PADDING-BOTTOM: 4px;" id="addButtonsRow">
										<td align="left" nowrap>
											<asp:Panel id="m_exportPanel" runat="server" style="DISPLAY: inline;">
												<input type="button" onclick="onExportClick();" value="Export">
											</asp:Panel>
											<asp:Button id="m_exportButton" Runat="server" style="display:none"></asp:Button>
											<asp:Panel id="m_addPanel" runat="server" style="DISPLAY: inline;">
												<input type="button" onclick="onImportOutlookClick();" value="Import Outlook 2003/2007 Contacts" style="width:200px" >&nbsp;
												<input type="button" onclick="onImportOtherClick();" value="Import Other Contacts" style="width:130px">&nbsp;
												<input type="button" onclick="onAddClick();" value="Add new entry">
											</asp:Panel>
										</td>
										<td align="right">
											<input onclick="onClosePopup();" type="button" value="Close"></td>
									</tr>
									<tr style="PADDING-BOTTOM: 4px;">
										<td align="left" nowrap>
											<asp:Panel id="m_searchPanel" Runat="server">
											Search for: &nbsp;
											<asp:TextBox id="m_searchRolodexFilter" runat="server" onkeypress="if( event.keyCode == 13 ) { m_searchRolodexButton.click(); return false; }"></asp:TextBox>
											
											<input type="button" onclick="onSearchClick();" value="Search">
											<asp:Button id="m_searchRolodexButton" runat="server" style="display:none"></asp:Button>
											&nbsp;&nbsp;Type
											<asp:DropDownList ID="m_searchRolodexType" Runat="server" AutoPostBack="True"></asp:DropDownList>
										</asp:Panel>
										</td>
										<td></td>
									</tr>
								</table>
								<div id="m_results" style="PADDING-RIGHT: 4px; WIDTH: 780px; OVERFLOW-Y: scroll; HEIGHT: 437px;">
									<ml:CommonDataGrid id="m_rolodexDG" runat="server" DataKeyField="AgentID" OnItemCommand="RolodexItemCommand">
										<Columns>
											<asp:TemplateColumn>
												<ItemTemplate>
												    <% if (IsAffiliatesDialog) { %>
												    <a href="#" class="select" data-id=<%# AspxTools.HtmlAttribute(DataBinder.Eval(Container.DataItem, "AgentID").ToString())%>>select</a>
												    <% } else { %>
											        <a href="#" onclick="javascript:$('#actions_<%# AspxTools.JsStringUnquoted(DataBinder.Eval(Container.DataItem, "AgentID").ToString())%>').toggle()">actions</a>
											        <div id=<%#AspxTools.HtmlAttribute("actions_" + DataBinder.Eval(Container.DataItem, "AgentID"))%> style="display:none;padding-left:5px">
											            <a href="#" onclick="onEditClick(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "AgentID").ToString())%>);">edit </a><br />
												        <asp:LinkButton runat="server" CssClass="deleteClick" style="DISPLAY: none;" CommandName="Delete" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AgentID"))%>>
												        delete
											            </asp:LinkButton>
												        <a href="#" onclick="deleteClick(this);">
													        delete </a><br />
												        <asp:LinkButton Runat="server" style="DISPLAY: none;" CssClass="duplicateClick" CommandName="Duplicate" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AgentID"))%>>
									                    duplicate
									                    </asp:LinkButton>
												        <a href="#" onclick="duplicateClick(this);">
													        duplicate</a>
												        <%if (BrokerUser.HasPermission(LendersOffice.Security.Permission.AllowManagingContactList)) { %>
												            <br />
												            <asp:LinkButton runat="server" CssClass="approveDisableClick" style="display: none;" CommandName="ToggleApprovalStatusRolodex" CommandArgument=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "AgentID"))%>>
												            </asp:LinkButton>
												            <a href="#" onclick="approveDisableContact(this, <%# AspxTools.JsBool((bool)DataBinder.Eval(Container.DataItem, "AgentIsApproved"))%>);">
													            <%# AspxTools.HtmlString((bool)DataBinder.Eval(Container.DataItem, "AgentIsApproved")?"disable":"approve") %></a>
												        <%} %>
											        </div>
											        <% } %>
												</ItemTemplate>
											</asp:TemplateColumn>
                                            <asp:BoundColumn DataField="SystemId" HeaderText="System ID" ItemStyle-CssClass="systemId"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Status" SortExpression="AgentIsApproved">
											    <ItemTemplate>
											        <span class="status" style=<%#AspxTools.HtmlAttribute((bool)DataBinder.Eval(Container.DataItem, "AgentIsApproved") ? "" : "color:red")%>>
											        <%# AspxTools.HtmlString((bool)DataBinder.Eval(Container.DataItem, "AgentIsApproved")?"Approved":"Disabled") %>
											        </span>
											    </ItemTemplate>
											</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Affiliate" SortExpression="AgentIsAffiliated">
											    <ItemTemplate>
											        <%# AspxTools.HtmlString((bool)DataBinder.Eval(Container.DataItem, "AgentIsAffiliated") ? "Yes" : "No")%>
											    </ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="AgentTypeDesc" HeaderText="Type" SortExpression="AgentTypeDesc" ItemStyle-CssClass="type"></asp:BoundColumn>
											<asp:BoundColumn DataField="AgentNm" HeaderText="Name" SortExpression="AgentNm" ItemStyle-CssClass="name"></asp:BoundColumn>
											<asp:BoundColumn DataField="AgentTitle" HeaderText="Title" SortExpression="AgentTitle"></asp:BoundColumn>
											<asp:boundcolumn datafield="AgentPhone" headertext="Agent Phone"></asp:boundcolumn>
                                            <asp:TemplateColumn HeaderText="Company Name" SortExpression="AgentComNm" ItemStyle-CssClass="compName">
                                                <ItemTemplate>
                                                    <%# AspxTools.HtmlControl(GetCompanyNameDisplay((string)Eval("AgentComNm"), (string)Eval("AgentWebsiteUrl"))) %>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
											<asp:boundcolumn datafield="PhoneOfCompany" headertext="Company Phone"></asp:boundcolumn>
											<asp:HyperLinkColumn DataTextField="AgentEmail" DataNavigateUrlField="AgentEmail" DataNavigateUrlFormatString="mailto:{0}" HeaderText="Email" />
										</Columns>
									</ml:CommonDataGrid>
									<asp:Panel id="m_rolodexDenied" runat="server">
										<div style="PADDING: 80px; FONT: 12px arial; COLOR: red; TEXT-ALIGN: center;">
											Access denied.&nbsp; You do not have permission to view contact entries.&nbsp; 
											Please consult your account administrator if you have any questions.
										</div>
									</asp:Panel>
									<asp:Panel id="m_noFilter" runat="server">
										<div style="PADDING: 80px; FONT: 12px arial; COLOR: red; TEXT-ALIGN: center;">
											Please use the filters above to search for contacts.
										</div>
									</asp:Panel>
								</div>
							</div>
							<div ID="m_PageInvestorList" runat="server" >
							   <table cellpadding="0" cellspacing="0" width="100%">
							        <tr style="padding-bottom: 4px;" >
							            <td>
							                <input type="button" value="Add new entry" runat="server" id="AddnewInvestor" onclick="onInvestorAddClick();" />
							                <input type="button" value="Export Investors" runat="server" id="ExportInvestors" onclick="investorExport();" />
							                <asp:DropDownList runat="server" ID="InvestorStatus" onchange="onInvestorStatusChange.call(this);">
							                    <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
							                    <asp:ListItem Text="Inactive" Value="Inactive"></asp:ListItem>
							                </asp:DropDownList>
							            </td>
							            <td align="right">
											<input onclick="onClosePopup();" type="button" value="Close">
		
		
		
							            </td>
							        </tr>
							        <tr>
							            <td colspan="2">
							                <div id="m_resultsInvestors" style="PADDING-RIGHT: 4px; WIDTH: 780px; OVERFLOW-Y: scroll; HEIGHT: 462px;">
						                        <asp:Repeater runat="server" ID="m_investorList" OnItemDataBound="m_investorList_OnItemDataBound">
						                            <HeaderTemplate>
						                                <table cellpadding="0" cellspacing="0" width="100%" id="InvestorEntries">
						                                    <thead>
						                                    <tr class="GridHeader">
						                                        <td>&nbsp;</td>
						                                        <td>Investor Name</td>
						                                        <td>MERS Org Id</td>
						                                        <td>Main Contact</td>
						                                        <td>Main Phone</td>
						                                        <td>Main Email</td>
						                                        </tr>
						                                    </thead>
						                                    <tbody>
						                            </HeaderTemplate>
    						                        
						                            <FooterTemplate>
						                                </tbody>
						                                </table>
						                            </FooterTemplate>
                                                    <ItemTemplate>
                                                        <tr class="GridItem" data-status='<%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).Status.ToString()) %>'>
                                                            <td >
                                                                <a href="#" runat="server" id="EditLink" investorid='<%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).Id.ToString()) %>' onclick="return investorEdit(this);">edit</a>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).InvestorName) %>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).MERSOrganizationId) %>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).MainContactName) %>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).MainPhone) %>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).MainEmail) %>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    
                                                                                     <AlternatingItemTemplate>
                                                        <tr class="GridAlternatingItem" data-status='<%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).Status.ToString()) %>'>
                                                            <td>
                                                                <a href="#" runat="server" id="EditLink" investorid='<%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).Id.ToString()) %>' onclick="return investorEdit(this);">edit</a>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).InvestorName) %>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).MERSOrganizationId) %>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).MainContactName) %>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).MainPhone) %>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.InvestorRolodexEntry)Container.DataItem).MainEmail) %>
                                                            </td>
                                                        </tr>
                                                    </AlternatingItemTemplate>
						                        </asp:Repeater>
						                    </div>
							            </td>
							        </tr>
							   </table>
							</div>
							<div id="InternalEmployee">
								<table cellspacing="0" cellpadding="0" width="100%">
									<tr style="PADDING-BOTTOM: 4px;">
										<td align="left">
											<asp:Panel id="m_searchPanelInternal" Runat="server">
												Search for: &nbsp;
												<asp:TextBox id="m_searchInternalFilter" runat="server" onkeypress="if( event.keyCode == 13 ) m_searchRolodexButton.click();"></asp:TextBox>
												<input type="button" onclick="onSearchClick();" value="Search"> &nbsp; &nbsp; 
							                    Status: <asp:DropDownList ID="m_statusFilter" runat="server" AutoPostBack="true" /> &nbsp; &nbsp; 
												<img src="../../images/profile.gif">&nbsp; - Account Owner
											</asp:Panel>
										</td>
										<td align="right">
											<input onclick="onClosePopup();" type="button" value="Close">
										</td>
									</tr>
								</table>
								<div id="m_resultsInternal" style="PADDING-RIGHT: 4px; WIDTH: 780px; OVERFLOW-Y: scroll; HEIGHT: 462px;">
									<ml:CommonDataGrid id="m_internalEmployeeDG" runat="server">
										<Columns>
											<asp:TemplateColumn>
												<ItemTemplate>
													<img src=<%# AspxTools.HtmlAttribute(GetImages( DataBinder.Eval( Container.DataItem , "IsAOwner" ) )) %>>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="UserName" SortExpression="UserName" HeaderText="Employee Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="Branch" SortExpression="Branch" HeaderText="Branch"></asp:BoundColumn>
											<asp:BoundColumn DataField="Phone" HeaderText="Phone"></asp:BoundColumn>
											<asp:BoundColumn DataField="Email" HeaderText="Email" DataFormatString="<a href='mailto:{0}'>{0}<a/>"></asp:BoundColumn>
										</Columns>
									</ml:CommonDataGrid>
									<asp:Panel id="m_internalEmployeeDenied" runat="server">
										<div style="PADDING: 80px; FONT: 12px arial; COLOR: red; TEXT-ALIGN: center;">
											Access denied.&nbsp; You do not have permission to view contact entries.&nbsp; 
											Please consult your account administrator if you have any questions.
										</div>
									</asp:Panel>
								</div>
							</div>
							<div ID="m_PageWarehouseLenderList" runat="server" >
							   <table cellpadding="0" cellspacing="0" width="100%">
							        <tr style="padding-bottom: 4px;" >
							            <td>
							                <input type="button" value="Add new entry" runat="server" id="AddWarehouseLender" onclick="onWarehouseLenderAddClick();" />
							                <asp:DropDownList runat="server" ID="WarehouseLenderStatus" onchange="onWarehouseLenderStatusChange.call(this);">
							                    <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
							                    <asp:ListItem Text="Inactive" Value="Inactive"></asp:ListItem>
							                </asp:DropDownList>
							            </td>
							            <td align="right">
											<input onclick="onClosePopup();" type="button" value="Close">
							            </td>
							        </tr>
							        <tr>
							            <td colspan="2">
							                <div id="m_resultsWarehouseLenders" style="PADDING-RIGHT: 4px; WIDTH: 780px; OVERFLOW-Y: scroll; HEIGHT: 462px;">
						                        <asp:Repeater runat="server" ID="m_warehouseLenderList" OnItemDataBound="m_resultsWarehouseLenders_OnItemDataBound">
						                            <HeaderTemplate>
						                                <table cellpadding="0" cellspacing="0" width="100%" id="WarehouseLenderEntries">
						                                    <thead>
						                                    <tr class="GridHeader">
						                                        <td>&nbsp;</td>
						                                        <td>Warehouse Lender</td>
						                                        <td>Main Contact</td>
						                                        <td>Main Phone</td>
						                                        <td>Main Email</td>
						                                    </tr>
						                                    </thead>
						                                    <tbody>
						                            </HeaderTemplate>
    						                        
						                            <FooterTemplate>
						                                </tbody>
						                                </table>
						                            </FooterTemplate>
                                                    <ItemTemplate>
                                                        <tr class="GridItem" data-status='<%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).Status.ToString()) %>'>
                                                            <td >
                                                                <a href="#" runat="server" id="EditLink" warehouselenderid='<%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).Id.ToString()) %>' onclick="return onWarehouseLenderEditClick(this);">edit</a>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).WarehouseLenderName) %>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).MainContactName)%>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).MainPhone)%>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).MainEmail)%>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    
                                                                                     <AlternatingItemTemplate>
                                                        <tr class="GridAlternatingItem" data-status='<%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).Status.ToString()) %>'>
                                                            <td>
                                                                <a href="#" runat="server" id="EditLink" warehouselenderid='<%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).Id.ToString()) %>' onclick="return onWarehouseLenderEditClick(this);">edit</a>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).WarehouseLenderName)%>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).MainContactName)%>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).MainPhone)%>
                                                            </td>
                                                            <td>
                                                                <%# AspxTools.HtmlString(((LendersOffice.ObjLib.Rolodex.WarehouseLenderRolodexEntry)Container.DataItem).MainEmail)%>
                                                            </td>
                                                        </tr>
                                                    </AlternatingItemTemplate>
						                        </asp:Repeater>
						                    </div>
							            </td>
							        </tr>
							   </table>
							</div>
					</td>
				</tr>
				<tr>
					<td>
					</td>
				</tr>
				<tr>
					<td>
					</td>
				</tr>
			</table>
			<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></form>
        <iframe class="download-frame"></iframe>
	</body>
</HTML>
