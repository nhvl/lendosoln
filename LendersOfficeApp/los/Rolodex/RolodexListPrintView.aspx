<%@ Page language="c#" Codebehind="RolodexListPrintView.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Rolodex.RolodexListPrintView" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>RolodexListPrintView</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <style>
    TD { FONT-SIZE: 8pt; FONT-FAMILY: 'Times New Roman' }
    </style>
</HEAD>
  <body MS_POSITIONING="FlowLayout">
	
    <form id="RolodexListPrintView" method="post" runat="server">
    
    <asp:DataGrid id=m_rolodexDG runat="server" AutoGenerateColumns="False" EnableViewState="False" Width="100%">
<HeaderStyle Font-Bold="True">
</HeaderStyle>

<Columns>
<asp:BoundColumn DataField="AgentTypeDesc" SortExpression="AgentTypeDesc" HeaderText="Type"></asp:BoundColumn>
<asp:BoundColumn DataField="AgentNm" SortExpression="AgentNm" HeaderText="Name"></asp:BoundColumn>
<asp:BoundColumn DataField="AgentTitle" SortExpression="AgentTitle" HeaderText="Title"></asp:BoundColumn>
<asp:BoundColumn DataField="AgentPhone" HeaderText="Agent Phone"></asp:BoundColumn>
<asp:BoundColumn DataField="AgentComNm" SortExpression="AgentComNm" HeaderText="Company Name"></asp:BoundColumn>
<asp:BoundColumn DataField="PhoneOfCompany" HeaderText="Company Phone"></asp:BoundColumn>
<asp:BoundColumn DataField="AgentEmail" HeaderText="Email"></asp:BoundColumn>
</Columns>
    
    </asp:DataGrid><asp:DataGrid id=m_internalEmployeeDG runat="server" AutoGenerateColumns="False">
        <Columns>
        <asp:BoundColumn DataField="UserName" SortExpression="UserName" HeaderText="Employee Name">
        </asp:BoundColumn>
        <asp:BoundColumn DataField="Branch" SortExpression="Branch" HeaderText="Branch">
        </asp:BoundColumn>
        <asp:BoundColumn DataField="Phone" HeaderText="Phone">
        </asp:BoundColumn>
        <asp:BoundColumn DataField="Email" HeaderText="Email">
        </asp:BoundColumn>
    </Columns>
    </asp:DataGrid>

     </form>
	
  </body>
</HTML>
