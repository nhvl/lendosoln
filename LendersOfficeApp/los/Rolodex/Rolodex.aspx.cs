namespace LendersOfficeApp.los.Rolodex
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.TitleFramework;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    public partial class Rolodex : LendersOffice.Common.BaseServicePage
    {
        // OPM 3286
        protected RolodexDB m_rolodex;

        /// <summary>
        /// If user hit apply or create new rolodex then need to update list if user hit cancel.
        /// </summary>
		protected bool IsUpdateList 
        {
            get { return Page.IsPostBack || RequestHelper.GetBool("updatelist"); }
        }

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}
		
		private Guid m_agentID 
        {
            get { return RequestHelper.GetGuid("agentid", Guid.Empty); }
        }

        private FormAction m_action 
        {
            get 
            {
                string cmd = RequestHelper.GetSafeQueryString("cmd");
                if (cmd != null && cmd.ToLower() == "edit") 
                {
                    return FormAction.Edit;
                } 
                else 
                {
                    return FormAction.Add;
                }
            }
        }

        private void LoadData() 
        {
			if( BrokerUser.HasPermission( Permission.AllowReadingFromRolodex ) == false )
			{
				// 12/15/2004 kb - Block reading if access denied.

				return;
			}

            m_rolodex = new RolodexDB(m_agentID, BrokerUser.BrokerId);

            if (m_rolodex.Retrieve())
            {
                this.SystemId.Text = this.m_rolodex.SystemId;
				m_nameTF.Text = m_rolodex.Name;
                Tools.SetDropDownListValue(m_typeDDL, m_rolodex.Type);
                this.TypeOtherDescription.Text = m_rolodex.TypeOtherDescription;
                m_titleTF.Text = m_rolodex.Title;
                m_phoneTF.Text = m_rolodex.Phone;
                m_alternatePhoneTF.Text = m_rolodex.AlternatePhone;
                m_companyName.Text = m_rolodex.CompanyName;
                m_departmentNameTF.Text = m_rolodex.DepartmentName;
                m_emailTF.Text = m_rolodex.Email;
                m_faxTF.Text = m_rolodex.Fax;
                m_addressTF.Text = m_rolodex.Address.StreetAddress;
                m_cityTF.Text = m_rolodex.Address.City;
                m_stateDDL.Value = m_rolodex.Address.State;
                m_zipcodeTF.Text = m_rolodex.Address.Zipcode;
                this.BindCounty(m_rolodex.Address.State, m_rolodex.Address.County);
                m_noteTF.Text = m_rolodex.Note;
                m_pagerTF.Text = m_rolodex.Pager;
                m_licenseNumberTF.Text = m_rolodex.AgentLicenseNumber;
                m_companyLicenseNumberTF.Text = m_rolodex.CompanyLicenseNumber;
                m_phoneOfCompanyTF.Text = m_rolodex.PhoneOfCompany;
                m_faxOfCompanyTF.Text = m_rolodex.FaxOfCompany;

                this.TitleProviderCode.Text = m_rolodex.TitleProviderCode;

                this.TitleProviderRow.Visible = TitleLenderService.GetLenderServicesByBrokerId(this.BrokerUser.BrokerId).Any(ls => ls.UsesProviderCodes);

                m_websiteUrlTF.Text = m_rolodex.WebsiteUrl; // OPM 3286
                IsNotifyWhenLoanStatusChange.Checked = m_rolodex.IsNotifyWhenLoanStatusChange;
                m_companyID.Text = m_rolodex.CompanyID;
                this.IsSettlementServiceProvider.Checked = this.m_rolodex.IsSettlementServiceProvider;
                AgentIsAffiliated.Checked = m_rolodex.AgentIsAffiliated;
                OverrideLicenses.Checked = m_rolodex.OverrideLicenses;

                // OPM 109299
                m_branchName.Text = m_rolodex.BranchName;
                m_payToBankName.Text = m_rolodex.PayToBankName;
                m_payToBankCityState.Text = m_rolodex.PayToBankCityState;
                m_payToABANumber.Text = m_rolodex.PayToABANumber.Value;
                m_payToAccountName.Text = m_rolodex.PayToAccountName;
                m_payToAccountNum.Text = m_rolodex.PayToAccountNumber.Value;
                m_furtherCreditToAccountName.Text = m_rolodex.FurtherCreditToAccountName;
                m_furtherCreditToAccountNum.Text = m_rolodex.FurtherCreditToAccountNumber.Value;
            }
            else
            {
                Response.Redirect("~/common/AppError.aspx?errmsg=Invalid agent id");
            }
        }

        private void SaveData() 
        {
			if( BrokerUser.HasPermission( Permission.AllowWritingToRolodex ) == false )
			{
				// 12/15/2004 kb - Block reading if access denied.

				return;
			}

            if (m_action == FormAction.Add) 
            {
                m_rolodex = new RolodexDB();
                m_rolodex.BrokerID = BrokerUser.BrokerId;
            } 
            else 
            {
                m_rolodex = new RolodexDB(m_agentID, BrokerUser.BrokerId);
            }

            m_rolodex.Name = m_nameTF.Text;
            m_rolodex.Type = (E_AgentRoleT) Tools.GetDropDownListValue(m_typeDDL);
            m_rolodex.TypeOtherDescription = TypeOtherDescription.Text;
            m_rolodex.Title = m_titleTF.Text;
            m_rolodex.Phone = m_phoneTF.Text;
            m_rolodex.AlternatePhone = m_alternatePhoneTF.Text;
            m_rolodex.CompanyName = m_companyName.Text;
            m_rolodex.DepartmentName = m_departmentNameTF.Text;
            m_rolodex.Email = m_emailTF.Text;
            m_rolodex.Fax = m_faxTF.Text;
            m_rolodex.Address.StreetAddress = m_addressTF.Text;
            m_rolodex.Address.City = m_cityTF.Text;
            m_rolodex.Address.State = m_stateDDL.Value;
            m_rolodex.Address.Zipcode = m_zipcodeTF.Text;
            m_rolodex.Address.County = m_countyDdl_Value.Value;
            m_rolodex.Note = m_noteTF.Text;
            m_rolodex.Pager = m_pagerTF.Text;
            m_rolodex.AgentLicenseNumber = m_licenseNumberTF.Text;
			m_rolodex.CompanyLicenseNumber = m_companyLicenseNumberTF.Text;
            m_rolodex.PhoneOfCompany = m_phoneOfCompanyTF.Text ;
            m_rolodex.FaxOfCompany = m_faxOfCompanyTF.Text ;
            m_rolodex.TitleProviderCode = this.TitleProviderCode?.Text ?? string.Empty;
			m_rolodex.WebsiteUrl = m_websiteUrlTF.Text; // OPM 3286
            m_rolodex.IsNotifyWhenLoanStatusChange = IsNotifyWhenLoanStatusChange.Checked;
            m_rolodex.CompanyID = m_companyID.Text;
            this.m_rolodex.IsSettlementServiceProvider = this.IsSettlementServiceProvider.Checked;
            m_rolodex.AgentIsAffiliated = AgentIsAffiliated.Checked;
            m_rolodex.OverrideLicenses = OverrideLicenses.Checked;

            // OPM 109299
            m_rolodex.BranchName = m_branchName.Text;
            m_rolodex.PayToBankName = m_payToBankName.Text;
            m_rolodex.PayToBankCityState = m_payToBankCityState.Text;
            m_rolodex.PayToABANumber = m_payToABANumber.Text;
            m_rolodex.PayToAccountName = m_payToAccountName.Text;
            m_rolodex.PayToAccountNumber = m_payToAccountNum.Text;
            m_rolodex.FurtherCreditToAccountName = m_furtherCreditToAccountName.Text;
            m_rolodex.FurtherCreditToAccountNumber = m_furtherCreditToAccountNum.Text;
			
			if (!m_rolodex.Save()) 
            {
                Response.Redirect("~/common/AppError.aspx?errmsg=Unable to save agent");
            } 
        }

		protected void PageLoad( object sender , System.EventArgs a )
		{
            if (this.IsPostBack)
            {
                // The county needs to be rebound on postback. In this case we
                // can use the current values of the state and county controls.
                this.BindCounty(m_stateDDL.Value, m_countyDdl_Value.Value);
            }
            else
            {
                if (m_action == FormAction.Edit)
                {
                    if (m_agentID != Guid.Empty)
                    {
                        LoadData();
                    }
                    else
                    {
                        Response.Redirect("~/common/AppError.aspx?errmsg=Invalid agent id");
                    }
                }
                else
                {
                    this.NewSystemIdLabel.Visible = true;
                }
            }

			// 12/15/2004 kb - Hide all edit controls if permission
			// is denied.

			if( BrokerUser.HasPermission( Permission.AllowWritingToRolodex ) == false )
			{
				m_editingPanel.Visible = false;
				m_accessDenied.Visible = true;
			}
			else
			{
				m_editingPanel.Visible = true;
				m_accessDenied.Visible = false;
			}
		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            RolodexDB.PopulateAgentTypeDropDownList(m_typeDDL);
            m_zipcodeTF.SmartZipcode(m_cityTF, m_stateDDL, m_countyDdl);
            m_stateDDL.Attributes.Add("onchange", "javascript:UpdateCounties(this,document.getElementById('" + m_countyDdl.ClientID + "'), event);");

            RegularExpressionValidator1.ValidationExpression = ConstApp.EmailValidationExpression;
			RegularExpressionValidator1.Text = "(" + ErrorMessages.InvalidEmailAddress + ")";
            RegularExpressionValidator2.ValidationExpression = RegularExpressionString.RolodexCompanyID.ToString();
            RegularExpressionValidator2.Text = "(" + ErrorMessages.InvalidCompanyID + ")";
			m_websiteValidator.Text = "(" + ErrorMessages.InvalidUrl + ")"; // OPM 3286
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.EnableJqueryMigrate = false;
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

        protected void onOKClick(object sender, System.EventArgs e)
        {
            SaveData();
			LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, "OK", "true") ;
        }

        protected void onApplyClick(object sender, System.EventArgs e)
        {
            SaveData();
            if (m_action == FormAction.Add) 
            {
                Response.Redirect("Rolodex.aspx?cmd=edit&agentid=" + m_rolodex.ID + "&updatelist=t");
            }
        }

        /// <summary>
        /// Binds the county dropdown. This must be done during page load as it requires
        /// the currently selected state and county.
        /// </summary>
        /// <param name="state">The currently selected state.</param>
        /// <param name="county">The currently selected county.</param>
        private void BindCounty(string state, string county)
        {
            Tools.Bind_sSpCounty(state, m_countyDdl, true);
            Tools.SetDropDownListCaseInsensitive(m_countyDdl, county);
            this.m_countyDdl_Value.Value = county;
        }
    }
}
