<%@ Register TagPrefix="cc1" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="Rolodex.aspx.cs" EnableEventValidation="true" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Rolodex.Rolodex" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>Contacts</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<style type="text/css">
		    .alignRight { text-align: right; }
		    .verticalAlignHack { width: 60px; padding-bottom: .15em; }
            .wide90 { width: 90px; }
		    .wide120 { width: 120px; }
            .row-label { width: 242px; }
            .left-column { width: 50%; float: left; }
            .right-column { width: 50%; float: right; }
            #NewSystemIdLabel { color: #F00; }
            #SystemId { padding-right: 5px; }
		</style>
	</HEAD>
	<body bgColor="gainsboro" onload="init();" MS_POSITIONING="FlowLayout">
		<script language="javascript">
    <!--
	function requiresValidation() {
        return <%=AspxTools.JsGetElementById(this.IsSettlementServiceProvider)%>.checked;
	}
	function setRequiredImageDisplay() {
	    $('div.validation-icon').toggle(requiresValidation());
	}
    function init() {
        resize(550, 740); 

        addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(RegularExpressionValidator1) %>);

        setRequiredImageDisplay();
        onContactTypeChanged();
    }
    function validate() {
        var missingRequiredFields = 
            $.trim(<%=AspxTools.JsGetElementById(this.m_companyName)%>.value) === '' ||
            $.trim(<%=AspxTools.JsGetElementById(this.m_addressTF)%>.value) === '' ||
            $.trim(<%=AspxTools.JsGetElementById(this.m_cityTF)%>.value) === '' ||
            $(<%=AspxTools.JsGetElementById(this.m_stateDDL)%>).val() === '' ||
            $.trim(<%=AspxTools.JsGetElementById(this.m_zipcodeTF)%>.value) === '' ||
            $.trim(<%=AspxTools.JsGetElementById(this.m_phoneOfCompanyTF)%>.value) === '';

        if (missingRequiredFields) {
            alert('Please fill out all required fields.');
        }

		return !missingRequiredFields;
    }
    function onSave() {
        if (requiresValidation()) {
            return validate();
        }

        return true;
    }
    function onCancelClick() {
        var args;
        <% if (IsUpdateList){ %>
        args = {
            OK: true
        };
        
        <% } %>

        onClosePopup(args);       
    }

	function onCountyChanged() {
	    var selectedValue = $('#m_countyDdl').val();
	    $('#m_countyDdl_Value').val(selectedValue);
	}

	function onContactTypeChanged() {
	    var selectedValue = $('#m_typeDDL').val();
        var titleType = <%= AspxTools.JsString(E_AgentRoleT.Title) %>;
        var otherType = <%= AspxTools.JsString(E_AgentRoleT.Other) %>;

        $('#TypeOtherDescription').toggle(selectedValue == otherType);

        if (selectedValue === titleType) {
            $('#TitleProviderRow').show();
        }
	    else {
            $('#TitleProviderRow').hide();
            $('#TitleProviderCode').val('');
        }
	}
    //-->
        </script>
        <h4 class="page-header">Add / Edit Contact Entry</h4>
		<form id="Rolodex" method="post" runat="server">
			<asp:panel id="m_editingPanel" runat="server">
				<TABLE class="FormTable" id="Table2" style="MARGIN-TOP: 0px" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<TR style="PADDING-TOP: 5px">
						<TD class="FieldLabel" style="WIDTH: 242px">Type</TD>
						<TD>
							<asp:DropDownList id="m_typeDDL" runat="server" onchange="onContactTypeChanged();"></asp:DropDownList>
                            <asp:TextBox ID="TypeOtherDescription" runat="server"></asp:TextBox>
						</TD>
					</TR>
                    <tr>
                        <td class="FieldLabel row-label">System ID</td>
                        <td>
                            <asp:TextBox ID="SystemId" runat="server" ReadOnly="true" CssClass="wide90 alignRight"></asp:TextBox>
                            <ml:EncodedLabel ID="NewSystemIdLabel" runat="server" Visible="false">Will be assigned at contact creation.</ml:EncodedLabel>
                        </td>
                    </tr>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">Name</TD>
						<TD>
							<asp:TextBox id="m_nameTF" runat="server" Width="322px" MaxLength="200"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">Title</TD>
						<TD>
							<asp:TextBox id="m_titleTF" runat="server" Width="322px" MaxLength="21"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">Agent License #</TD>
						<TD>
							<asp:TextBox id="m_licenseNumberTF" runat="server" Width="215px" MaxLength="30" DESIGNTIMEDRAGDROP="28"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">Agent Phone</TD>
						<TD class="FieldLabel">
							<cc1:PhoneTextBox id="m_phoneTF" runat="server" CssClass="mask" preset="phone" width="120"></cc1:PhoneTextBox>&nbsp;Agent 
							Fax
							<cc1:PhoneTextBox id="m_faxTF" runat="server" CssClass="mask" preset="phone" width="120"></cc1:PhoneTextBox></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">Agent Cell&nbsp;Phone</TD>
						<TD class="FieldLabel">
							<cc1:PhoneTextBox id="m_alternatePhoneTF" runat="server" CssClass="mask" preset="phone" width="120"></cc1:PhoneTextBox>&nbsp;Agent 
							Pager
							<cc1:PhoneTextBox id="m_pagerTF" runat="server" CssClass="mask" preset="phone" width="120"></cc1:PhoneTextBox></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">Email</TD>
						<TD>
							<asp:TextBox id="m_emailTF" runat="server" Width="230px" MaxLength="80"></asp:TextBox>&nbsp;<A onclick="window.open('mailto:' + document.getElementById('m_emailTF').value);" href="#">send 
								email</A>
						</TD>
					</TR>
					<TR>
						<TD></TD>
						<TD>
							<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" Display="Dynamic" ControlToValidate="m_emailTF"></asp:RegularExpressionValidator></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">
                            <div class="left-column">Company Name</div>
                            <div class="alignRight right-column validation-icon"><img alt="Required" src="../../images/require_icon.gif" /></div>
						</TD>
						<TD>
							<asp:TextBox id="m_companyName" runat="server" Width="322px" MaxLength="100"></asp:TextBox>
						</TD>
					</TR>
					<tr>
					    <td class="FieldLabel" style="width: 242px">Branch Name</td>
					    <td>
					        <asp:TextBox id="m_branchName" runat="server" Width="322px" MaxLength="100"></asp:TextBox>
					    </td>
					</tr>
					<TR>
					    <TD class="FieldLabel" style="WIDTH: 242px">Company ID</TD>
					    <TD>
					        <asp:TextBox Id="m_companyID" runat="server" Width="215px" MaxLength="25"></asp:TextBox></TD>
					</TR>
					<TR>
					    <TD></TD>
					    <TD>
					        <asp:RegularExpressionValidator id="RegularExpressionValidator2" runat="server" Display="Dynamic" ControlToValidate="m_companyID"></asp:RegularExpressionValidator></TD>
					</TR>
					
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">Company License #</TD>
						<TD>
							<asp:TextBox id="m_companyLicenseNumberTF" runat="server" Width="215px" MaxLength="30"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">Department Name</TD>
						<TD>
							<asp:TextBox id="m_departmentNameTF" runat="server" Width="322px" MaxLength="100"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">
                            <div class="left-column">Address</div>
                            <div class="alignRight right-column validation-icon"><img alt="Required" src="../../images/require_icon.gif" /></div>
						</TD>
						<TD>
							<asp:TextBox id="m_addressTF" runat="server" Width="322" MaxLength="36"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">
                            <div class="left-column">City</div>
                            <div class="alignRight right-column validation-icon"><img alt="Required" src="../../images/require_icon.gif" /></div>
						</TD>
						<TD>
							<asp:TextBox id="m_cityTF" runat="server" Width="215px" MaxLength="36"></asp:TextBox>
							<cc1:StateDropDownList id="m_stateDDL" runat="server"></cc1:StateDropDownList>
							<cc1:ZipcodeTextBox id="m_zipcodeTF" runat="server" CssClass="mask" preset="zipcode" width="55" onchange="onCountyChanged();"></cc1:ZipcodeTextBox></TD>
					</TR>
                    <tr>
                        <td class="FieldLabel">County</td>
                        <td>
                            <select runat="server" width="100" ID="m_countyDdl" onchange="onCountyChanged();"></select>
                            <asp:HiddenField runat="server" ID="m_countyDdl_Value" />
                        </td>
                    </tr>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">
                            <div class="left-column">Company Phone</div>
                            <div class="alignRight right-column validation-icon"><img alt="Required" src="../../images/require_icon.gif" /></div>
						</TD>
						<TD>
							<cc1:PhoneTextBox id="m_phoneOfCompanyTF" runat="server" CssClass="mask" preset="phone" width="120"></cc1:PhoneTextBox></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">Company Fax</TD>
						<TD>
							<cc1:PhoneTextBox id="m_faxOfCompanyTF" runat="server" CssClass="mask" preset="phone" width="120"></cc1:PhoneTextBox></TD>
					</TR>
                    <tbody id="TitleProviderRow" runat="server">
                        <tr>
                            <td class="FieldLabel row-label">Title Provider Code</td>
                            <td>
                                <asp:TextBox ID="TitleProviderCode" runat="server" CssClass="mask" />
                            </td>
                        </tr>
                    </tbody>
					<tr>
					    <td class="FieldLabel">
					        <div style="width: 50%; float: left;">Pay To</div>
					        <div  class="alignRight" style="width: 50%; float: right;">Bank Name</div>
					    </td>
					    <td nowrap>
					        <asp:TextBox ID="m_payToBankName" class="wide120" runat="server"></asp:TextBox>
					        <span class="FieldLabel verticalAlignHack">City/State</span>
					        <asp:TextBox ID="m_payToBankCityState" class="wide120" runat="server"></asp:TextBox>
					    </td>
					</tr>
					<tr>
					    <td class="FieldLabel alignRight">
					        ABA Number
					    </td>
					    <td>
					        <asp:TextBox ID="m_payToABANumber" class="wide120" runat="server"></asp:TextBox>
					    </td>
					</tr>
					<tr>
					    <td class="FieldLabel alignRight">
					        Account Name
					    </td>
					    <td nowrap>
					        <asp:TextBox ID="m_payToAccountName" class="wide120" runat="server"></asp:TextBox>
					        <span class="FieldLabel verticalAlignHack">Account #</span>
					        <asp:TextBox ID="m_payToAccountNum" class="wide120" runat="server"></asp:TextBox>
					    </td>
					</tr>
					<tr>
					    <td class="FieldLabel">
					        <div style="width: 50%; float: left;">Further credit to</div>
					        <div class="alignRight" style="width: 50%; float:right;">Account Name</div>
					    </td>
					    <td nowrap>
					        <asp:TextBox ID="m_furtherCreditToAccountName" class="wide120" runat="server"></asp:TextBox>
					        <span class="FieldLabel verticalAlignHack">Account #</span>
					        <asp:TextBox ID="m_furtherCreditToAccountNum" class="wide120" runat="server"></asp:TextBox>
					    </td>
					</tr>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px">Company Website</TD>
						<TD>
							<asp:TextBox id="m_websiteUrlTF" runat="server" Width="230px" MaxLength="200"></asp:TextBox>&nbsp;<A onclick="if ( /^https?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&amp;=]*)?$/.test(document.getElementById('m_websiteUrlTF').value)) window.open(document.getElementById('m_websiteUrlTF').value);" href="#">go 
								to website</A>
						</TD>
					</TR>
					<TR>
						<TD></TD>
						<TD>
							<asp:RegularExpressionValidator id="m_websiteValidator" runat="server" Display="Dynamic" ValidationExpression="https?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?" ControlToValidate="m_websiteUrlTF"></asp:RegularExpressionValidator></TD>
					</TR>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px" vAlign="top">Notes</TD>
						<TD>
							<asp:TextBox id="m_noteTF" onkeyup="TextAreaMaxLength(this, 500);" runat="server" Width="322px" TextMode="MultiLine" Wrap="true" Rows="3"></asp:TextBox></TD>
					</TR>
                    <tr>
                        <td class="FieldLabel row-label"></td>
                        <td class="FieldLabel">
                            <asp:CheckBox ID="IsSettlementServiceProvider" runat="server" Text="This contact is a settlement service provider." onclick="setRequiredImageDisplay();" />
                        </td>
                    </tr>
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px"></TD>
						<TD class="FieldLabel">
							<asp:CheckBox id="AgentIsAffiliated" runat="server" Text="This provider is an affiliate of us"></asp:CheckBox></TD>
					</TR>					
					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px"></TD>
						<TD class="FieldLabel">
							<asp:CheckBox id="OverrideLicenses" runat="server" Text="Override agent and company licenses by default"></asp:CheckBox></TD>
					</TR>					

					<TR>
						<TD class="FieldLabel" style="WIDTH: 242px"></TD>
						<TD class="FieldLabel">
							<asp:CheckBox id="IsNotifyWhenLoanStatusChange" runat="server" Text="Send email when loan status changes"></asp:CheckBox></TD>
					</TR>
					<TR>
						<TD align="center" colSpan="2" rowSpan="1">
							<asp:Button id="m_okBtn" runat="server" Text="  OK  " onclick="onOKClick" OnClientClick="return onSave();"></asp:Button>
							<INPUT onclick="onCancelClick();" type="button" value="Cancel">
							<asp:Button id="m_applyBtn" runat="server" Text="Apply" onclick="onApplyClick" OnClientClick="return onSave();"></asp:Button></TD>
					</TR>
				</TABLE>
			</asp:panel><asp:panel id="m_accessDenied" runat="server">
				<DIV style="PADDING-RIGHT: 100px; PADDING-LEFT: 100px; PADDING-BOTTOM: 100px; FONT: 12px arial; WIDTH: 100%; COLOR: red; PADDING-TOP: 100px; TEXT-ALIGN: center">Access 
					denied.&nbsp; You do not have permission to edit contact entries.&nbsp; Please 
					consult your account administrator if you have any questions.
					<DIV style="PADDING-RIGHT: 20px; PADDING-LEFT: 20px; PADDING-BOTTOM: 20px; PADDING-TOP: 20px">
						<INPUT onclick="onCancelClick();" type="button" value="Close">
					</DIV>
				</DIV>
			</asp:panel></form>
		<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
	</body>
</HTML>
