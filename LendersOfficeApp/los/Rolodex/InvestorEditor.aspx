﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvestorEditor.aspx.cs"
    Inherits="LendersOfficeApp.los.Rolodex.InvestorEditor" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
    

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Investor Entry Editor</title>
    <style type="text/css">
        #form1 table, #form1 fieldset { width: 700px; }
        #form1 fieldset, #form1 table.last  { margin-top: 10px; display: block;}
        #form1 fieldset table { margin-left: 7px; margin-top: 5px;  }
        #form1 table.first { margin-left: 20px; }
        .MainRightHeader { margin-bottom: 10px; }
        body { font-weight: bold; background-color: gainsboro;}
        legend { font-weight: bolder;  }
        input.address, input.attention, input.company { width: 250px; }
        input.zip { width: 70px; }
        input.email { width: 150px; }
        input.contactname {width: 225px; }
        input.investor { width: 200px; }
        input.mers, input.seller { width: 95px; }
        span.usecb input { margin-left: -3px; position: relative;}
    </style>
</head>
<body >
    <script type="text/javascript">
        jQuery(function($) {
            var $mainAddress = $('#MainAddress'),
                $mainCity = $('#MainCity'),
                $mainState = $('#MainState'),
                $mainZip = $('#MainZip'),
                $mainPhone = $('#MainPhone'),
                $mainFax = $('#MainFax'),
                cbs = $('span.usecb input'),
                close = $('#Close').val();

            if (close == 'Close') {
                onClosePopup();
            }

            $mainAddress.change(updateCbs);
            $mainCity.change(updateCbs);
            $mainState.change(updateCbs);
            $mainZip.change(updateCbs);
            $mainPhone.change(updateCbs);
            $mainFax.change(updateCbs);


            resizeForIE6And7(800, 830);
            function updateCbs() {
                cbs.each(function() {
                    updateAddress.call(this);
                });
            }
            function updateAddress() {
                var $cb = $(this), $table = $cb.parents('table'),
                    $adddress = $table.find('input.address'),
                    $city = $table.find('input.city'),
                    $state = $table.find('select.state'),
                    $zip = $table.find('input.zip'),
                    $phone = $table.find('input.phone'),
                    $fax = $table.find('input.fax'),
                    checked = $cb.is(':checked');

                $city.prop('readonly', checked);
                $state.prop('disabled', checked);
                $zip.prop('readonly', checked);
                $adddress.prop('readonly', checked);
                $phone.prop('readonly', checked);
                $fax.prop('readonly', checked);

                if (checked) {
                    $city.val($mainCity.val());
                    $state.val($mainState.val());
                    $zip.val($mainZip.val());
                    $adddress.val($mainAddress.val());
                    $phone.val($mainPhone.val());
                    $fax.val($mainFax.val());
                }
            }

            cbs.click(updateAddress);
            updateCbs();
            $('#MainZip').change(function(event) {
                smartZipcode($('#MainZip')[0], $('#MainCity')[0], $('#MainState')[0], null, event);
            });
            $('#NoteShipToZip').change(function(event) {
                smartZipcode($('#NoteShipToZip')[0], $('#NoteShipToCity')[0], $('#NoteShipToState')[0], null, event);
            });
            $('#PaymentToZip').change(function(event) {
                smartZipcode($('#PaymentToZip')[0], $('#PaymentToCity')[0], $('#PaymentToState')[0], null, event);
            });
            $('#LossPayeeZip').change(function(event) {
                smartZipcode($('#LossPayeeZip')[0], $('#LossPayeeCity')[0], $('#LossPayeeState')[0], null, event);
            });

        });

        function ReLabel() {
            var label = document.getElementById("TypeLabel");
            var ddl = document.getElementById("InvestorRolodexType");
            if (ddl.value == 0)
                label.innerText = "Investor";
            else
                label.innerText = "Subservicer";
        }

        function displayTieToPmlInvestorHelp() {
             alert('At submission for a program with this investor, the contact information from this entry will populate to the investor name on the Back-End Rate Lock and the Investor Info pages. If you do not want this investor to populate at submission, please leave the Tie To PML Investor\'s drop down blank.');
           
             return false;
        }
    </script>
    <h4 class="page-header">Add Investor Entry</h4>
    <form id="form1" runat="server">
    <table cellpadding="0" cellspacing="0" class="first">
        <tr runat="server" id="RowContactType">
            <td>
                <ml:EncodedLabel ID="Label17" runat="server" AssociatedControlID="InvestorRolodexType">Type</ml:EncodedLabel>
            </td>
            <td>
                <asp:DropDownList ID="InvestorRolodexType" runat="server" onchange="ReLabel();" />
            </td>
            <td align="right" style="padding-right:5px">Rolodex ID</td>
            <td><ml:EncodedLiteral ID="RolodexInvestorId" runat="server" /></td>
        </tr>
        <tr>
            <td width="130px" >
                <ml:EncodedLabel ID="TypeLabel" runat="server" AssociatedControlID="InvestorName" Text="Investor" />
            </td>
            <td style="width:300px">
                <asp:TextBox runat="server" ID="InvestorName"  CssClass="investor"/>
            </td>
            <td width="130px" align="right" style="padding-right:5px">
                <ml:EncodedLabel runat="server" AssociatedControlID="Status" Text="Status" />
            </td>
            <td align="left">
                <asp:DropDownList runat="server" ID="Status" />
            </td>
        </tr>
        <tr>
            <td>Tie To PML Investor</td>
            <td colspan="3"><asp:DropDownList runat="server" ID="LpeInvestorId" width="200" /><a href="#" onclick="return displayTieToPmlInvestorHelp();">details</a></td>
        </tr>
        <tr>
            <td><ml:EncodedLabel runat="server" AssociatedControlID="SellerId" Text="Seller ID" /></td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="SellerId" CssClass="seller" />
            </td>
        </tr>
        <tr>
            <td>
                <ml:EncodedLabel runat="server" AssociatedControlID="MersOrganizationId" Text="MERS Organization ID" />
            </td>
            <td colspan="3">
                <asp:TextBox runat="server" ID="MersOrganizationId" preset="mersorgid" CssClass="mers" />
            </td>
        </tr>
        <tr>
            <td>
                <ml:EncodedLabel runat="server" AssociatedControlID="HmdaPurchaser2015T">HMDA Purchaser Type</ml:EncodedLabel>
            </td>
            <td colspan="3"> 
                <asp:DropDownList ID="HmdaPurchaser2015T" runat="server"></asp:DropDownList>
            </td>
        </tr>
    </table>
    <fieldset style="margin-left: 10px">
        <legend>Main Address</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="CompanyName">Company Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="CompanyName" CssClass="company"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="MainContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainContactName" CssClass="contactname"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="MainAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainAttention" CssClass="attention"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="MainEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainEmail" CssClass="email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="MainAddress">Address</ml:EncodedLabel>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="MainAddress" CssClass="address"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="MainCity" CssClass="city"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="MainState" CssClass="state" />
                    <asp:TextBox runat="server" ID="MainZip" preset="longzipcode" CssClass="zip"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="MainPhone">Phone</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="MainPhone" preset="phone"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="MainFax">Fax</ml:EncodedLabel>
                </td>
                <td>
                    <ml:PhoneTextBox runat="server" ID="MainFax" preset="phone"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Note Ship To</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="NoteShipToAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="NoteShipToAttention" CssClass="attention"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="NoteShipToContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="NoteShipToContactName" CssClass="contactname"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="NoteShipToUseNoteShipToAdddress" CssClass="usecb" />
                    <ml:EncodedLabel ID="Label1" runat="server" AssociatedControlID="NoteShipToUseNoteShipToAdddress">Same as Main Address</ml:EncodedLabel>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="NoteShipToEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="NoteShipToEmail" CssClass="email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="NoteShipToAddress">Address</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="NoteShipToAddress" CssClass="address"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="NoteShipToCity" CssClass="city"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="NoteShipToState"  CssClass="state"/>
                    <asp:TextBox runat="server" ID="NoteShipToZip" preset="longzipcode" CssClass="zip"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="NoteShipToPhone">Phone</ml:EncodedLabel>
                </td>
                <td>
                    <ml:PhoneTextBox runat="server" ID="NoteShipToPhone" CssClass="phone" preset="phone"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="NoteShipToFax">Fax</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="NoteShipToFax" CssClass="fax" preset="phone"> </ml:PhoneTextBox>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Payment To</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label3" runat="server" AssociatedControlID="PaymentToAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PaymentToAttention" CssClass="attention"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label4" runat="server" AssociatedControlID="PaymentToContactName" >Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PaymentToContactName" CssClass="contactname"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="PaymentToUseMainAdddress" CssClass="usecb" />
                    <ml:EncodedLabel ID="Label5" runat="server" AssociatedControlID="PaymentToUseMainAdddress">Same as Main Address</ml:EncodedLabel>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label6" runat="server" AssociatedControlID="PaymentToEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PaymentToEmail" CssClass="email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label7" runat="server" AssociatedControlID="PaymentToAddress">Address</ml:EncodedLabel>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="PaymentToAddress" CssClass="address"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="PaymentToCity" CssClass="city"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="PaymentToState" CssClass="state" />
                    <asp:TextBox runat="server" ID="PaymentToZip" preset="longzipcode" CssClass="zip"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label8" runat="server" AssociatedControlID="PaymentToPhone">Phone</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="PaymentToPhone" CssClass="phone" preset="phone"> </ml:PhoneTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label9" runat="server" AssociatedControlID="PaymentToFax">Fax</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="PaymentToFax" CssClass="fax" preset="phone"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Loss Payee</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label10" runat="server" AssociatedControlID="LossPayeeAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeAttention" CssClass="attention"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label11" runat="server" AssociatedControlID="LossPayeeContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeContactName" CssClass="contactname"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="LossPayeeUseMainAdddress" CssClass="usecb" />
                    <ml:EncodedLabel ID="Label12" runat="server" AssociatedControlID="LossPayeeUseMainAdddress">Same as Main Address</ml:EncodedLabel>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label13" runat="server" AssociatedControlID="LossPayeeEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeEmail" CssClass="email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label14" runat="server" AssociatedControlID="LossPayeeAddress">Address</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="LossPayeeAddress" CssClass="address"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="LossPayeeCity" CssClass="city"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="LossPayeeState"  CssClass="state"/>
                    <asp:TextBox runat="server" ID="LossPayeeZip" preset="longzipcode" CssClass="zip" ></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label15" runat="server" AssociatedControlID="LossPayeePhone">Phone</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="LossPayeePhone" CssClass="phone" preset="phone"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label16" runat="server" AssociatedControlID="LossPayeeFax">Fax</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="LossPayeeFax" CssClass="fax" preset="phone"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    
    <table cellpadding="0" cellspacing="0" class="last">
        <tr>
           <td align="center" style="color:Red;"><ml:EncodedLiteral runat="server" ID="ErrorMessage"></ml:EncodedLiteral></td> 
        </tr>
        <tr>
            <td align="center">
                <asp:Button runat="server" ID="OkayBtn" OnClick="OkBtn_Click" Text=" OK " />
                <input type="button" value="Cancel" onclick="onClosePopup();" />
                <asp:Button runat="server" ID="ApplyBtn" OnClick="ApplyBtn_Click" Text= "Apply" />
            </td>
        </tr>
    </table>
    
    </form>
</body>
</html>
