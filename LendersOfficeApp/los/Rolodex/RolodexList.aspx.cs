namespace LendersOfficeApp.los.Rolodex
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Data.SqlClient;
    using System.Threading;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Rolodex;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;
    using LendersOffice.Integration.TitleFramework;

    public partial class RolodexList : LendersOffice.Common.BaseServicePage
	{
        protected bool HasViewInvestorInfoPermission
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowViewingInvestorInformation);
            }
        }

        protected bool HasEditInvestorInfoPermission
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowEditingInvestorInformation);
            }
        }
		protected System.Web.UI.WebControls.Button			    m_exportBottomBtn;
		protected System.Web.UI.WebControls.Panel                      m_noFilterInternal;

        protected BrokerUserPrincipal BrokerUser
        {
			get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected bool IsAffiliatesDialog
        {
            get
            {
                return !string.IsNullOrEmpty(Request.QueryString["affdlg"]) && Request.QueryString["affdlg"].ToUpper() == "Y";
            }
        }

        private bool IsSettlementServiceProviderDialog
        {
            get { return RequestHelper.GetBool("ssp"); }
        }

        private string SettlementServiceProviderType
        {
            get { return RequestHelper.GetSafeQueryString("ssptype"); }
        }

        protected string GetImages(object accountOwner)
        {
            bool isAccountOwner = false;

            if (!(accountOwner is DBNull)) 
            {
                isAccountOwner = (bool) accountOwner;
            }

            if (isAccountOwner) 
                return "../../images/profile.gif";
            else 
                return "../../images/spacer.gif";
        }

		private void BindDataGrid() 
        {
			// Bind employees and entries to the rolodex.
			// If user is restricted, then skip.
			BindRolodex();
            BindInternalEmployee();
            BindInvestorList();
            BindWarehouseLenderList();
        }

        private void BindInvestorList()
        {
            if (BrokerUser.HasPermission(Permission.AllowViewingInvestorInformation))
            {
                m_investorList.DataSource = InvestorRolodexEntry.GetAll(PrincipalFactory.CurrentPrincipal.BrokerId, null);
                m_investorList.DataBind();
                m_investorList.Visible = true;
            }
    
        }
        private void BindWarehouseLenderList()
        {
            m_warehouseLenderList.DataSource = WarehouseLenderRolodexEntry.GetAll(PrincipalFactory.CurrentPrincipal.BrokerId, null);
            m_warehouseLenderList.DataBind();
            m_warehouseLenderList.Visible = true;
        }

        private void BindRolodex()
        {
			// Execute stored procedure and gather external agents
            // from the broker's records.

            int typeFilter = int.Parse(m_searchRolodexType.SelectedItem.Value);

            // 01/03/07 db OPM 9141 - only display contacts when the user has performed a filtered search
			if(!IsPostBack && (m_searchRolodexFilter.Text.TrimWhitespaceAndBOM().Equals(String.Empty)) && (typeFilter == -1))
			{
				m_noFilter.Visible = true;
				return;
			}
			else
			{
				m_noFilter.Visible = false;
			}
			
			SqlParameter[] parameters = { 
                                            new SqlParameter("@BrokerID", BrokerUser.BrokerId) , 
                                            new SqlParameter("@NameFilter", Utilities.SafeSQLString(m_searchRolodexFilter.Text)), 
                                            new SqlParameter("@TypeFilter", typeFilter == -1 ? (object) DBNull.Value : (object) typeFilter),
                                            new SqlParameter("@OnlySettlementServiceProviders", this.IsSettlementServiceProviderDialog)
                                        };
            DataSet ds = new DataSet();
            
            DataSetHelper.Fill(ds, BrokerUser.BrokerId, "ListRolodexByBrokerID", parameters);

            // Fixup the table with an extra column that contains the
            // text description of the agent type, which we can use
            // for sorting by description and not by enum.            

            DataTable table = ds.Tables[ 0 ];

            table.Columns.Add( "AgentTypeDesc" , typeof( string ) );

            foreach( DataRow row in table.Rows )
            {
                row[ "AgentTypeDesc" ] = RolodexDB.GetTypeDescription( ( E_AgentRoleT ) row[ "AgentType" ] );
            }

            // Bind the grid.

            m_rolodexDG.DefaultSortExpression = "AgentNm ASC";
            m_rolodexDG.DataSource = table.DefaultView;
            m_rolodexDG.DataBind();

            this.m_rolodexDG.Columns[1].Visible = this.IsSettlementServiceProviderDialog;
        }

		private class EmployeeDesc
		{
			/// <summary>
			/// Track individual rolodex roles for internal view.
			/// </summary>

			private String  m_UserName = String.Empty;
			private String    m_Branch = String.Empty;
			private String     m_Phone = String.Empty;
			private String     m_Email = String.Empty;
			private Boolean m_IsAOwner = false;

			#region ( Desc properties )

			public String UserName
			{
				// Access member.

				set
				{
					m_UserName = value;
				}
				get
				{
					return m_UserName;
				}
			}

			public String Branch
			{
				// Access member.

				set
				{
					m_Branch = value;
				}
				get
				{
					return m_Branch;
				}
			}

			public String Phone
			{
				// Access member.

				set
				{
					m_Phone = value;
				}
				get
				{
					return m_Phone;
				}
			}

			public String Email
			{
				// Access member.

				set
				{
					m_Email = value;
				}
				get
				{
					return m_Email;
				}
			}

			public Boolean IsAOwner
			{
				// Access member.

				set
				{
					m_IsAOwner = value;
				}
				get
				{
					return m_IsAOwner;
				}
			}

			#endregion

		}

        private void BindInternalEmployee()
        {
			BrokerUserPermissionsSet bupSet = new BrokerUserPermissionsSet();
			BrokerEmployeeNameTable  eTable = new BrokerEmployeeNameTable();
			BrokerBranchTable        bTable = new BrokerBranchTable();
			ArrayList                empSet = new ArrayList();
			
			if( m_searchInternalFilter.Text.TrimWhitespaceAndBOM() != String.Empty )
			{
				eTable.Retrieve( BrokerUser.BrokerId , m_searchInternalFilter.Text.TrimWhitespaceAndBOM() );
			}
			else
			{
				eTable.Retrieve( BrokerUser.BrokerId );
			}

			bTable.Retrieve( BrokerUser.BrokerId );

			bupSet.Retrieve( BrokerUser.BrokerId );

			foreach( EmployeeDetails eD in eTable.Values )
			{
				BrokerUserPermissions buP = bupSet[ eD.EmployeeId ];

				if( buP == null || buP.IsInternalBrokerUser() == false )
				{
					if( eD.Type == "" || eD.Type == "B" )
					{
                        if (eD.IsActive == Convert.ToBoolean(m_statusFilter.SelectedValue))
                        {
                            EmployeeDesc rD = new EmployeeDesc();

                            rD.UserName = eD.FullName;
                            rD.Phone = eD.Phone;
                            rD.Email = eD.Email;
                            rD.IsAOwner = eD.IsAOwner;

                            if (bTable.Contains(eD.BranchId) == true)
                            {
                                rD.Branch = bTable[eD.BranchId].BranchName;
                            }
                            else
                            {
                                rD.Branch = "?";
                            }

                            empSet.Add(rD);
                        }
					}
				}
			}
			
			m_internalEmployeeDG.DefaultSortExpression = "UserName ASC";
			
			m_internalEmployeeDG.DataSource = ViewGenerator.Generate( empSet );
			m_internalEmployeeDG.DataBind();
        }

        protected void PageInit(object sender, System.EventArgs arg) 
        {
            RolodexDB.PopulateAgentTypeDropDownList(m_searchRolodexType);
            m_searchRolodexType.Items.Insert(0, new ListItem("All", "-1"));

            if (!string.IsNullOrWhiteSpace(this.SettlementServiceProviderType))
            {
                Tools.SetDropDownListValue(this.m_searchRolodexType, this.SettlementServiceProviderType);
            }

            m_statusFilter.Items.Add(new ListItem("Active", "True"));
            m_statusFilter.Items.Add(new ListItem("Inactive", "False"));

            RegisterService("investorExport", "/los/Rolodex/InvestorExporter.aspx");
            RegisterJsScript("LQBPopup.js");
        }

		/// <summary>
		/// Initialize this page.
		/// </summary>

        protected void m_resultsWarehouseLenders_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {

            if (!BrokerUser.HasPermission(Permission.AllowWritingToRolodex))
            {
                HtmlAnchor anchor = args.Item.FindControl("EditLink") as HtmlAnchor;
                if (anchor != null)
                {
                    anchor.Visible = false;
                }
            }
        }

		protected void PageLoad( object sender , System.EventArgs a )
		{
            IncludeStyleSheet("~/css/Tabs.css");
            if (Hash.Value == "")
            {
                Hash.Value = "#ExternalAgentList";
            }
            m_InvestorTab.Visible = HasViewInvestorInfoPermission;
            AddnewInvestor.Visible = HasEditInvestorInfoPermission;
            AddWarehouseLender.Visible = BrokerUser.HasPermission(Permission.AllowWritingToRolodex);

			// 07/05/06 mf - Intercept Export button click here in Load
			// so we don't set the UI that will never be displayed.
			if( Request.Form["m_exportButton"] != null || Request.Form["m_exportBottomBtn"] != null )
			{
				ExportClick (sender, a);
                Response.End();
                return;
			}

			// Always display grids based on current specs.
			try
			{
				if( BrokerUser.HasPermission( Permission.AllowReadingFromRolodex ) == false )
				{
					m_internalEmployeeDenied.Visible = true;
					m_internalEmployeeDG.Visible     = false;

					m_rolodexDenied.Visible = true;
					m_rolodexDG.Visible     = false;

					m_noFilter.Visible = false;
					m_exportPanel.Visible       = false;
					m_searchPanel.Visible = false;
					m_searchPanelInternal.Visible = false;
				}
				else
				{
					m_internalEmployeeDenied.Visible = false;
					m_internalEmployeeDG.Visible     = true;
                    
					m_rolodexDenied.Visible = false;
					m_rolodexDG.Visible     = true;

					m_exportPanel.Visible       = true;
					m_searchPanel.Visible = true;
					m_searchPanelInternal.Visible = true;
				}

				if( BrokerUser.HasPermission( Permission.AllowWritingToRolodex ) == false )
				{
					m_rolodexDG.Columns[ 0 ].Visible = false; // Edit // it's now all one column. opm 109122
                    
                    m_addPanel.Visible = false;
				}
				else
				{
					m_rolodexDG.Columns[ 0 ].Visible = true;
                    
                    m_addPanel.Visible = true;
				}

				if( BrokerUser.HasPermission( Permission.AllowReadingFromRolodex ) == true )
					BindDataGrid();
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( "Failed to load web form." , e );
			}
		}

		/// <summary>
		/// Render this page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Re-bind to catch all events.

			try
			{
				if(BrokerUser.HasPermission( Permission.AllowReadingFromRolodex ) == true )
					BindDataGrid();
			}
			catch( Exception e )
			{
				Tools.LogError( "Failed to render web form." , e );
			}
		}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.EnableJqueryMigrate = false;
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{   
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

		// 07/05/06 mf - OPM 2262.  Allow user to export
		// current contacts view to CSV.

        protected void ExportClick(object sender, EventArgs e)
        {
            // We get run the SP to get the current view of the data,
            // then return the csv file in the response.  Note that
            // we use the status of the controls, not the actual output.

            E_AgentRoleT? typeFilter = null;
            int iTypeFilter = int.Parse(m_searchRolodexType.SelectedItem.Value);
            if (iTypeFilter != -1)
            {
                typeFilter = (E_AgentRoleT)iTypeFilter;
            }


            ContactsExporter exporter = new ContactsExporter(BrokerUser.BrokerId, m_searchRolodexFilter.Text, typeFilter);
            string file = exporter.Export();

            RequestHelper.SendFileToClient(Context , file, "text/csv", "contacts.csv");
        }

		/// <summary>
		/// Handle UI event for delete or duplicate command
		/// </summary>
		protected void RolodexItemCommand(object sender, DataGridCommandEventArgs a)
		{
			// Only process an item command if the user has rolodex writing permission
			if (BrokerUser.HasPermission(Permission.AllowWritingToRolodex))
			{
                var commandName = a.CommandName.ToLower().TrimWhitespaceAndBOM();
                var rolodexId = new Guid(a.CommandArgument.ToString());

				if (commandName == "delete" )
				{
                    this.DeleteRolodex(rolodexId);
				}
				else if (commandName == "duplicate" )
				{
                    this.DuplicateRolodex(rolodexId);
				}
                else if (commandName == "toggleapprovalstatusrolodex"
                    && BrokerUser.HasPermission(Permission.AllowManagingContactList)) // opm 109122
                {
                    this.ToggleApprovalStatusRolodex(rolodexId);
                }
			} 

			BindDataGrid();
		}

        /// <summary>
        /// Deletes an entry from the rolodex.
        /// </summary>
        /// <param name="rolodexId">The agent ID.</param>
        protected void DeleteRolodex(Guid rolodexId)
        {
            var brokerId = BrokerUser.BrokerId;

            using (var exec = new CStoredProcedureExec(brokerId))
            {

                try
                {
                    exec.BeginTransactionForWrite();

                    TitleProviderCodeDB.Delete(exec, brokerId, rolodexId);

                    SqlParameter[] parameters = {
                       new SqlParameter("@RolodexID", rolodexId)
                    };

                    exec.ExecuteNonQuery("DeleteRolodex", 0, parameters);

                    exec.CommitTransaction();
                }
                catch (ThreadAbortException)
                {
                    exec.RollbackTransaction();
                    throw;
                }
                catch (SystemException exc)
                {
                    exec.RollbackTransaction();
                    Tools.LogError("Failed to delete from Contacts.", exc);
                }
                catch (CBaseException exc)
                {
                    exec.RollbackTransaction();
                    Tools.LogError("Failed to delete from Contacts.", exc);
                }
            }
        }

        /// <summary>
        /// Duplicates a rolodex entry.
        /// </summary>
        /// <param name="rolodexId">The rolodex ID.</param>
        protected void DuplicateRolodex(Guid rolodexId)
        {
            var brokerId = BrokerUser.BrokerId;

            using (var exec = new CStoredProcedureExec(brokerId))
            {
                try
                {
                    var newRolodexId = Guid.NewGuid();

                    SqlParameter[] parameters = {
                        new SqlParameter("@RolodexID", rolodexId),
                        new SqlParameter("@NewRolodexID", newRolodexId)
                    };

                    exec.BeginTransactionForWrite();
                    exec.ExecuteNonQuery("DuplicateRolodex", 0, parameters);

                    var providerCodeToDuplicate = TitleProviderCodeDB.Retrieve(brokerId, rolodexId);
                    if (!string.IsNullOrEmpty(providerCodeToDuplicate))
                    {
                        TitleProviderCodeDB.Save(exec, brokerId, newRolodexId, providerCodeToDuplicate);
                    }

                    exec.CommitTransaction();
                }
                catch (ThreadAbortException)
                {
                    exec.RollbackTransaction();
                    throw;
                }
                catch (SystemException exc)
                {
                    exec.RollbackTransaction();
                    Tools.LogError("Failed to duplicate contact entry.", exc);
                }
                catch (CBaseException exc)
                {
                    exec.RollbackTransaction();
                    Tools.LogError("Failed to duplicate contact entry.", exc);
                }
            }
        }

        /// <summary>
        /// Toggles the approval status for a rolodex entry.
        /// </summary>
        /// <param name="rolodexId">The rolodex ID.</param>
        protected void ToggleApprovalStatusRolodex(Guid rolodexId)
        {
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@RolodexID", rolodexId)
                };

                StoredProcedureHelper.ExecuteNonQuery(BrokerUser.BrokerId, "ToggleApprovalStatusRolodex", 0, parameters);
            }
            catch (ThreadAbortException)
            {
                throw;
            }
            catch (SystemException exc)
            {
                Tools.LogError("Failed to toggle the approval status", exc);
            }
            catch (CBaseException exc)
            {
                Tools.LogError("Failed to toggle the approval status", exc);
            }
        }

        protected void m_investorList_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.AlternatingItem || args.Item.ItemType == ListItemType.Item)
            {
                var control = args.Item.FindControl("EditLink");
                control.Visible = HasEditInvestorInfoPermission;
            }
        }

        protected HtmlControl GetCompanyNameDisplay(string companyName, string websiteUrl)
        {
            // If the row has a website URL, display the company name as a link
            // to that URL.  If there is no company name, display the link as
            // the static text "[ website ]"
            if (string.IsNullOrEmpty(websiteUrl))
            {
                return new HtmlGenericControl("span") { InnerText = companyName };
            }

            var link = new HtmlAnchor();
            link.HRef = "#";
            link.Attributes.Add("onclick", "window.open(" + LendersOffice.AntiXss.AspxTools.JsString(websiteUrl) + ");");

            if (!string.IsNullOrEmpty(companyName))
            {
                link.InnerText = companyName;
                return link;
            }

            var wrapper = new HtmlGenericControl("span");
            wrapper.Controls.Add(new MeridianLink.CommonControls.EncodedLiteral() { Text = "[ " });
            link.InnerText = " website";
            wrapper.Controls.Add(link);
            wrapper.Controls.Add(new MeridianLink.CommonControls.EncodedLiteral() { Text = " ]" });
            return wrapper;
        }
    }
}
