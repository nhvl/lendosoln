﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WarehouseLenderEditor.aspx.cs" Inherits="LendersOfficeApp.los.Rolodex.WarehouseLenderEditor" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
    

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Warehouse Lender Entry Editor</title>
    <style type="text/css">
        #form1 table, #form1 fieldset { width: 700px; }
        #form1 fieldset, #form1 table.last  { margin-top: 10px; display: block;}
        #form1 fieldset table { margin-left: 7px; margin-top: 5px;  }
        #form1 table.first { margin-left: 20px; table-layout: fixed; }
        .MainRightHeader { margin-bottom: 10px; }
        body { font-weight: bold; background-color: gainsboro;}
        legend { font-weight: bolder;  }
        input.address, input.attention, input.company { width: 250px; }
        input.zip { width: 70px; }
        input.email { width: 150px; }
        input.contactname {width: 225px; }
        input.lendername { width: 200px; }
        input.mers, input.payee { width: 95px; }
        span.usecb input { margin-left: -3px; position: relative;}
        .CellTextBox { width:100%; margin:1px;}
    </style>
</head>
<body >
    <script type="text/javascript">
        jQuery(function($){
            var $mainAddress = $('#MainAddress'),
                $mainCity = $('#MainCity'),
                $mainState = $('#MainState'),
                $mainZip = $('#MainZip'),
                $mainPhone = $('#MainPhone'),
                $mainFax = $('#MainFax'),
                cbs = $('span.usecb input'),
                close = $('#Close').val();
                
            if (close == 'Close'){
                onClosePopup();
            }
                
            $mainAddress.change(updateCbs);
            $mainCity.change(updateCbs);
            $mainState.change(updateCbs);
            $mainZip.change(updateCbs);
            $mainPhone.change(updateCbs);
            $mainFax.change(updateCbs);

            
            Dialog.resizeForIE6And7(800, 800); 
            function updateCbs() {
                cbs.each(function(){
                    updateAddress.call(this);
                });
            }
            function updateAddress() {
                var $cb = $(this), $table = $cb.parents('table'),
                    $adddress = $table.find('input.address'),
                    $city = $table.find('input.city'),
                    $state = $table.find('select.state'),
                    $zip = $table.find('input.zip'),
                    $phone = $table.find('input.phone'),
                    $fax = $table.find('input.fax'),
                    checked = $cb.is(':checked');
                
                    $city.prop('readonly', checked);
                    $state.prop('disabled', checked);
                    $zip.prop('readonly', checked);
                    $adddress.prop('readonly', checked);
                    $phone.prop('readonly', checked);
                    $fax.prop('readonly', checked);
                    
                    if(checked){
                        $city.val($mainCity.val());
                        $state.val($mainState.val());
                        $zip.val($mainZip.val());
                        $adddress.val($mainAddress.val());
                        $phone.val($mainPhone.val());
                        $fax.val($mainFax.val());
                    }
            }           
            
            cbs.click(updateAddress);
            updateCbs();
            $('#MainZip').change(function(event){
                smartZipcode($('#MainZip')[0],$('#MainCity')[0],$('#MainState')[0], null, event);
            });
            $('#CollateralPackageToZip').change(function(event) {
                smartZipcode($('#CollateralPackageToZip')[0], $('#CollateralPackageToCity')[0], $('#CollateralPackageToState')[0], null, event);
            });
            $('#ClosingPackageToZip').change(function(event) {
                smartZipcode($('#ClosingPackageToZip')[0], $('#ClosingPackageToCity')[0], $('#ClosingPackageToState')[0], null, event);
            });

            $('.FundingPercent').blur(function() {
                formatPercent.call(this);
            });
            function formatPercent() {
                var $tb = $(this);
                var value = parseFloat($tb.val());
                if(isNaN(value))
                    $tb.val('');
                else if(value < 0 || value > 200)
                    $tb.val('0.000%');
                else
                    $tb.val(value.toFixed(3)+'%');      
            }
        });
    </script>
    <h4 class="page-header">Add Warehouse Lender Entry</h4>
    <form id="form1" runat="server">
    <table cellpadding="0" cellspacing="0" class="first">
        <tr>
            <td width="130px" >
                <ml:EncodedLabel ID="Label1" runat="server" AssociatedControlID="WarehouseLenderName">Warehouse Lender</ml:EncodedLabel>
            </td>
            <td width="230px">
                <asp:TextBox runat="server" ID="WarehouseLenderName" CssClass="lendername"></asp:TextBox>
            </td>
            <td width="130px">
                <ml:EncodedLabel ID="Label2" runat="server" AssociatedControlID="Status">Status</ml:EncodedLabel>
            </td>
            <td align="left">
                <asp:DropDownList runat="server" ID="Status">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <ml:EncodedLabel ID="Label3" runat="server" AssociatedControlID="FannieMaePayeeId">Fannie Mae Payee ID</ml:EncodedLabel>
            </td>
            <td>
                <asp:TextBox runat="server" ID="FannieMaePayeeId" CssClass="payee"></asp:TextBox>
            </td>
            <td colspan="2" rowspan="5">
                <label class="FieldLabel">Advance classifications</label>
                <table style="width:100%;table-layout:auto" cellpadding="0" cellspacing="0">
                    <tr class="GridHeader">
                        <td width="250px">Description</td>
                        <td>Max Funding %</td>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            <asp:TextBox ID="AdvanceClassificationDesc1" runat="server" CssClass="CellTextBox" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="AdvanceClassificationFunding1" runat="server" CssClass="CellTextBox FundingPercent"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            <asp:TextBox ID="AdvanceClassificationDesc2" runat="server" CssClass="CellTextBox" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="AdvanceClassificationFunding2" runat="server" CssClass="CellTextBox FundingPercent"></asp:TextBox>
                        </td>
                    </tr>                    
                    <tr class="GridItem">
                        <td>
                            <asp:TextBox ID="AdvanceClassificationDesc3" runat="server" CssClass="CellTextBox" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="AdvanceClassificationFunding3" runat="server" CssClass="CellTextBox FundingPercent"></asp:TextBox>
                        </td>
                    </tr>                    
                    <tr class="GridItem">
                        <td>
                            <asp:TextBox ID="AdvanceClassificationDesc4" runat="server" CssClass="CellTextBox" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="AdvanceClassificationFunding4" runat="server" CssClass="CellTextBox FundingPercent"></asp:TextBox>
                        </td>
                    </tr>                    
                    <tr class="GridItem">
                        <td>
                            <asp:TextBox ID="AdvanceClassificationDesc5" runat="server" CssClass="CellTextBox" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="AdvanceClassificationFunding5" runat="server" CssClass="CellTextBox FundingPercent"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            <asp:TextBox ID="AdvanceClassificationDesc6" runat="server" CssClass="CellTextBox" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="AdvanceClassificationFunding6" runat="server" CssClass="CellTextBox FundingPercent"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <label>Fannie Mae Warehouse Lender ID</label>
            </td>
            <td>
                <input type="text" runat="server" id="WarehouseLenderFannieMaeId" class="payee" />
            </td>
        </tr>
        <tr>
            <td>
                <label>Freddie Mac Payee ID</label>
            </td>
            <td>
                <input type="text" runat="server" id="FreddieMacPayeeId" class="payee" />
            </td>
        </tr>
        <tr>
            <td>
                <label>Freddie Mac Warehouse Lender ID</label>
            </td>
            <td>
                <input type="text" runat="server" id="WarehouseLenderFreddieMacId" class="payee" />
            </td>
        </tr>
        <tr>
            <td>
                <ml:EncodedLabel ID="Label4" runat="server" AssociatedControlID="MersOrganizationId">MERS Organization ID</ml:EncodedLabel>
            </td>
            <td>
                <asp:TextBox runat="server" ID="MersOrganizationId" preset="mersorgid" CssClass="mers" MaxLength="7"></asp:TextBox>
            </td>
        </tr>
        <tr><td style="height:110px"></td></tr>
    </table>
    <fieldset style="margin-left: 10px">
        <legend>Payment Instructions</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    Pay to
                </td>
                <td>
                    <ml:EncodedLabel ID="Label7" runat="server" AssociatedControlID="PayToBankName">Bank Name</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="PayToBankName"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <ml:EncodedLabel ID="Label6" runat="server" AssociatedControlID="PayToCity">City / State</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="PayToCity"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="PayToState"/>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <ml:EncodedLabel ID="Label8" runat="server" AssociatedControlID="PayToABANum">ABA Number</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="PayToABANum"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <ml:EncodedLabel ID="Label9" runat="server" AssociatedControlID="PayToAccountName">Account Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PayToAccountName"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label10" runat="server" AssociatedControlID="PayToAccountNum">Account #</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="PayToAccountNum"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Further credit to
                </td>
                <td >
                    <ml:EncodedLabel ID="Label11" runat="server" AssociatedControlID="FurtherCreditToAccountName">Account Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="FurtherCreditToAccountName"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label12" runat="server" AssociatedControlID="FurtherCreditToAccountNum">Account #</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="FurtherCreditToAccountNum"></asp:TextBox>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Main Address</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label13" runat="server" AssociatedControlID="MainCompanyName">Company Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainCompanyName" CssClass="company"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label14" runat="server" AssociatedControlID="MainContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainContactName" CssClass="contactname"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label15" runat="server" AssociatedControlID="MainAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainAttention" CssClass="attention"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label19" runat="server" AssociatedControlID="MainEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="MainEmail" CssClass="email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label16" runat="server" AssociatedControlID="MainAddress">Address</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="MainAddress" CssClass="address"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="MainCity" CssClass="city"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="MainState"  CssClass="state"/>
                    <asp:TextBox runat="server" ID="MainZip" preset="longzipcode" CssClass="zip"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label17" runat="server" AssociatedControlID="MainPhone">Phone</ml:EncodedLabel>
                </td>
                <td>
                    <ml:PhoneTextBox runat="server" ID="MainPhone" CssClass="phone" preset="phone"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label18" runat="server" AssociatedControlID="MainFax">Fax</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="MainFax" CssClass="fax" preset="phone"> </ml:PhoneTextBox>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Collateral package to</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="CollateralPackageToAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="CollateralPackageToAttention" CssClass="attention"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label5" runat="server" AssociatedControlID="CollateralPackageToContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="CollateralPackageToContactName" CssClass="contactname"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="CollateralPackageToUseMainAddress" CssClass="usecb" />
                    <ml:EncodedLabel ID="Label21" runat="server" AssociatedControlID="CollateralPackageToUseMainAddress">Same as Main Address</ml:EncodedLabel>
                </td>
                <td>
                    <ml:EncodedLabel runat="server" AssociatedControlID="CollateralPackageToEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="CollateralPackageToEmail" CssClass="email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label23" runat="server" AssociatedControlID="CollateralPackageToAddress">Address</ml:EncodedLabel>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="CollateralPackageToAddress" CssClass="address"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="CollateralPackageToCity" CssClass="city"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="CollateralPackageToState" CssClass="state" />
                    <asp:TextBox runat="server" ID="CollateralPackageToZip" preset="longzipcode" CssClass="zip"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label24" runat="server" AssociatedControlID="CollateralPackageToPhone">Phone</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="CollateralPackageToPhone" CssClass="phone" preset="phone"> </ml:PhoneTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label25" runat="server" AssociatedControlID="CollateralPackageToFax">Fax</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="CollateralPackageToFax" CssClass="fax" preset="phone"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset style="margin-left: 10px">
        <legend>Closing package to</legend>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label20" runat="server" AssociatedControlID="ClosingPackageToAttention">Attention</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="ClosingPackageToAttention" CssClass="attention"></asp:TextBox>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label22" runat="server" AssociatedControlID="ClosingPackageToContactName">Contact Name</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="ClosingPackageToContactName" CssClass="contactname"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="ClosingPackageToUseMainAddress" CssClass="usecb" />
                    <ml:EncodedLabel ID="Label26" runat="server" AssociatedControlID="ClosingPackageToUseMainAddress">Same as Main Address</ml:EncodedLabel>
                </td>
                <td>
                    <ml:EncodedLabel ID="Label27" runat="server" AssociatedControlID="ClosingPackageToEmail">Email</ml:EncodedLabel>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="ClosingPackageToEmail" CssClass="email"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label28" runat="server" AssociatedControlID="ClosingPackageToAddress">Address</ml:EncodedLabel>
                </td>
                <td >
                    <asp:TextBox runat="server" ID="ClosingPackageToAddress" CssClass="address"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td >
                    <asp:TextBox runat="server" ID="ClosingPackageToCity" CssClass="city"></asp:TextBox>
                    <ml:StateDropDownList runat="server" ID="ClosingPackageToState" CssClass="state" />
                    <asp:TextBox runat="server" ID="ClosingPackageToZip" preset="longzipcode" CssClass="zip"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label29" runat="server" AssociatedControlID="ClosingPackageToPhone">Phone</ml:EncodedLabel>
                </td>
                <td colspan="3">
                    <ml:PhoneTextBox runat="server" ID="ClosingPackageToPhone" CssClass="phone" preset="phone"> </ml:PhoneTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <ml:EncodedLabel ID="Label30" runat="server" AssociatedControlID="ClosingPackageToFax">Fax</ml:EncodedLabel>
                </td>
                <td >
                    <ml:PhoneTextBox runat="server" ID="ClosingPackageToFax" CssClass="fax" preset="phone"> </ml:PhoneTextBox>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </fieldset>
    
    <table cellpadding="0" cellspacing="0" class="last">
        <tr>
           <td align="center" style="color:Red;"><ml:EncodedLiteral runat="server" ID="ErrorMessage"></ml:EncodedLiteral></td> 
        </tr>
        <tr>
            <td align="center">
                <asp:Button runat="server" ID="OkayBtn" OnClick="OkBtn_Click" Text=" OK " />
                <input type="button" value="Cancel" onclick="onClosePopup();" />
                <asp:Button runat="server" ID="ApplyBtn" OnClick="ApplyBtn_Click" Text= "Apply" />
            </td>
        </tr>
    </table>
    
    </form>
</body>
</html>
