﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using DataAccess;
using System.Data.SqlClient;

namespace LendersOfficeApp.los.Rolodex
{
    public partial class InvestorEditor : BaseServicePage
    {

        private AbstractUserPrincipal Principal { get { return PrincipalFactory.CurrentPrincipal; } }
        private InvestorRolodexEntry m_entry = null;
        protected InvestorRolodexEntry CurrentEntry
        {
            get
            {
                if (m_entry == null)
                {
                    int id = RequestHelper.GetInt("Id", -1);
                    m_entry = InvestorRolodexEntry.Get(PrincipalFactory.CurrentPrincipal.BrokerId, id);
                }

                return m_entry;
            }
        }
        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowEditingInvestorInformation))
            {
                throw new AccessDenied();
            }
            ErrorMessage.Text = "";


            //RowContactType.Visible = LendersOfficeApp.los.admin.BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).ExpandSubservicerContacts;
                
            if (!Page.IsPostBack)
            {
                

                InvestorName.Text = CurrentEntry.InvestorName;
                if (!CurrentEntry.IsNew)
                {
                    Tools.SetDropDownListValue(Status, CurrentEntry.Status);
                    Tools.SetDropDownListValue(InvestorRolodexType, CurrentEntry.InvestorRolodexType);
                }

                if (CurrentEntry.Id.HasValue)
                {
                    RolodexInvestorId.Text = CurrentEntry.Id.Value.ToString();
                }
                SellerId.Text = CurrentEntry.SellerId;
                MersOrganizationId.Text = CurrentEntry.MERSOrganizationId;
                CompanyName.Text = CurrentEntry.CompanyName;
                MainContactName.Text = CurrentEntry.MainContactName;
                MainAttention.Text = CurrentEntry.MainAttention;
                MainEmail.Text = CurrentEntry.MainEmail;
                MainAddress.Text = CurrentEntry.MainAddress;
                MainCity.Text = CurrentEntry.MainCity;
                MainState.Value = CurrentEntry.MainState;
                MainZip.Text = CurrentEntry.MainZip;
                MainPhone.Text = CurrentEntry.MainPhone;
                MainFax.Text = CurrentEntry.MainFax;


                NoteShipToAttention.Text = CurrentEntry.NoteShipToAttention;
                NoteShipToContactName.Text = CurrentEntry.NoteShipToContactName;
                NoteShipToUseNoteShipToAdddress.Checked = CurrentEntry.NoteShipToUseMainAdddress;
                NoteShipToAddress.Text = CurrentEntry.NoteShipToAddress;
                NoteShipToCity.Text = CurrentEntry.NoteShipToCity;
                NoteShipToState.Value = CurrentEntry.NoteShipToState;
                NoteShipToZip.Text = CurrentEntry.NoteShipToZip;
                NoteShipToEmail.Text = CurrentEntry.NoteShipToEmail;
                NoteShipToPhone.Text = CurrentEntry.NoteShipToPhone;
                NoteShipToFax.Text = CurrentEntry.NoteShipToFax;


                PaymentToAttention.Text = CurrentEntry.PaymentToAttention;
                PaymentToContactName.Text = CurrentEntry.PaymentToContactName;
                PaymentToUseMainAdddress.Checked = CurrentEntry.PaymentToUseMainAdddress;
                PaymentToAddress.Text = CurrentEntry.PaymentToAddress;
                PaymentToCity.Text = CurrentEntry.PaymentToCity;
                PaymentToState.Value = CurrentEntry.PaymentToState;
                PaymentToZip.Text = CurrentEntry.PaymentToZip;
                PaymentToEmail.Text = CurrentEntry.PaymentToEmail;
                PaymentToPhone.Text = CurrentEntry.PaymentToPhone;
                PaymentToFax.Text = CurrentEntry.PaymentToFax;


                LossPayeeAttention.Text = CurrentEntry.LossPayeeAttention;
                LossPayeeContactName.Text = CurrentEntry.LossPayeeContactName;
                LossPayeeUseMainAdddress.Checked = CurrentEntry.LossPayeeUseMainAdddress;
                LossPayeeAddress.Text = CurrentEntry.LossPayeeAddress;
                LossPayeeCity.Text = CurrentEntry.LossPayeeCity;
                LossPayeeState.Value = CurrentEntry.LossPayeeState;
                LossPayeeZip.Text = CurrentEntry.LossPayeeZip;
                LossPayeeEmail.Text = CurrentEntry.LossPayeeEmail;
                LossPayeePhone.Text = CurrentEntry.LossPayeePhone;
                LossPayeeFax.Text = CurrentEntry.LossPayeeFax;

                Tools.SetDropDownListValue(LpeInvestorId, CurrentEntry.LpeInvestorId.ToString());
                Tools.SetDropDownListValue(HmdaPurchaser2015T, CurrentEntry.HmdaPurchaser2015T);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            EnableJqueryMigrate = false;
            RegisterJsScript("utilities.js");
            RegisterService("loanutils", "/newlos/LoanUtilitiesService.aspx");

            Status.Items.Add(new ListItem("Active", "1"));
            Status.Items.Add(new ListItem("Inactive", "0"));

            InvestorRolodexType.Items.Add(Tools.CreateEnumListItem("Investor", E_InvestorRolodexType.Investor));
            InvestorRolodexType.Items.Add(Tools.CreateEnumListItem("Subservicer", E_InvestorRolodexType.Subservicer));

            Tools.Bind_sHmdaPurchaser2015T(this.HmdaPurchaser2015T, CurrentEntry.HmdaPurchaser2015T);

            LpeInvestorId.DataSource = InvestorRolodexEntry.ListActiveLpeInvestor(this.Principal.BrokerDB);
            LpeInvestorId.DataTextField = "Value";
            LpeInvestorId.DataValueField = "Key";
            LpeInvestorId.DataBind();

            LpeInvestorId.Items.Insert(0, new ListItem("", "-1"));
        }

        protected void ApplyBtn_Click(object sender, EventArgs args)
        {
            if (Save())
            {
                Response.Redirect("InvestorEditor.aspx?id=" + CurrentEntry.Id);
            }
        }

        protected void OkBtn_Click(object sender, EventArgs args)
        {
            if (Save())
            {
                ClientScript.RegisterHiddenField("Close", "Close");
            }
        }

        private bool Save()
        {
            if (string.IsNullOrEmpty(InvestorName.Text))
            {
                ErrorMessage.Text = "Investor name is required.";
                return false;
            }

            CurrentEntry.InvestorRolodexType = (E_InvestorRolodexType)int.Parse(InvestorRolodexType.SelectedValue);

            CurrentEntry.InvestorName = InvestorName.Text;
            CurrentEntry.Status = (E_InvestorStatus)int.Parse(Status.SelectedValue);
            CurrentEntry.SellerId = SellerId.Text;
            CurrentEntry.MERSOrganizationId = MersOrganizationId.Text;
            CurrentEntry.InvestorRolodexType = (E_InvestorRolodexType) Int32.Parse(InvestorRolodexType.SelectedValue);

            CurrentEntry.CompanyName = CompanyName.Text;
            CurrentEntry.MainContactName = MainContactName.Text;
            CurrentEntry.MainAttention = MainAttention.Text;
            CurrentEntry.MainEmail = MainEmail.Text;
            CurrentEntry.MainAddress = MainAddress.Text;
            CurrentEntry.MainCity = MainCity.Text;
            CurrentEntry.MainState = MainState.Value;
            CurrentEntry.MainZip = MainZip.Text;
            CurrentEntry.MainPhone = MainPhone.Text;
            CurrentEntry.MainFax = MainFax.Text;

            CurrentEntry.NoteShipToAttention = NoteShipToAttention.Text;
            CurrentEntry.NoteShipToContactName = NoteShipToContactName.Text;
            CurrentEntry.NoteShipToUseMainAdddress = NoteShipToUseNoteShipToAdddress.Checked;
            CurrentEntry.NoteShipToAddress = NoteShipToAddress.Text;
            CurrentEntry.NoteShipToCity = NoteShipToCity.Text;
            CurrentEntry.NoteShipToState = NoteShipToState.Value;
            CurrentEntry.NoteShipToZip = NoteShipToZip.Text;
            CurrentEntry.NoteShipToEmail = NoteShipToEmail.Text;
            CurrentEntry.NoteShipToPhone = NoteShipToPhone.Text;
            CurrentEntry.NoteShipToFax = NoteShipToFax.Text;


            CurrentEntry.PaymentToAttention = PaymentToAttention.Text;
            CurrentEntry.PaymentToContactName = PaymentToContactName.Text;
            CurrentEntry.PaymentToUseMainAdddress = PaymentToUseMainAdddress.Checked;
            CurrentEntry.PaymentToAddress = PaymentToAddress.Text;
            CurrentEntry.PaymentToCity = PaymentToCity.Text;
            CurrentEntry.PaymentToState = PaymentToState.Value;
            CurrentEntry.PaymentToZip = PaymentToZip.Text;
            CurrentEntry.PaymentToEmail = PaymentToEmail.Text;
            CurrentEntry.PaymentToPhone = PaymentToPhone.Text;
            CurrentEntry.PaymentToFax = PaymentToFax.Text;


            CurrentEntry.LossPayeeAttention = LossPayeeAttention.Text;
            CurrentEntry.LossPayeeContactName = LossPayeeContactName.Text;
            CurrentEntry.LossPayeeUseMainAdddress = LossPayeeUseMainAdddress.Checked;
            CurrentEntry.LossPayeeAddress = LossPayeeAddress.Text;
            CurrentEntry.LossPayeeCity = LossPayeeCity.Text;
            CurrentEntry.LossPayeeState = LossPayeeState.Value;
            CurrentEntry.LossPayeeZip = LossPayeeZip.Text;
            CurrentEntry.LossPayeeEmail = LossPayeeEmail.Text;
            CurrentEntry.LossPayeePhone = LossPayeePhone.Text;
            CurrentEntry.LossPayeeFax = LossPayeeFax.Text;

            CurrentEntry.HmdaPurchaser2015T = (HmdaPurchaser2015T)Enum.Parse(typeof(HmdaPurchaser2015T), HmdaPurchaser2015T.SelectedValue);
            CurrentEntry.LpeInvestorId = int.Parse(LpeInvestorId.SelectedValue);
            try
            {
                CurrentEntry.Save();
                return true;
            }
            catch (DuplicateLpeInvestorAssignmentException exc)
            {
                ErrorMessage.Text = exc.UserMessage;
                return false;
            }
            catch (SqlException sqlEx)
            {
                if (sqlEx.Message.Contains("duplicate key"))
                {
                    ErrorMessage.Text = "An Investor with name '" + CurrentEntry.InvestorName + "' already exist";
                    return false;
                }
                throw new CBaseException(ErrorMessages.Generic, sqlEx);
            }
        }
    }
}
