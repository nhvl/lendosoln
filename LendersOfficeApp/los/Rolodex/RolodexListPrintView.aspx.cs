using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Rolodex;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Rolodex
{
	public partial class RolodexListPrintView : BasePage
	{
    
        private class EmployeeDesc
        {
            /// <summary>
            /// Track individual rolodex roles for internal view.
            /// </summary>

            private String  m_UserName = String.Empty;
            private String    m_Branch = String.Empty;
            private String     m_Phone = String.Empty;
            private String     m_Email = String.Empty;
            private Boolean m_IsAOwner = false;

			#region ( Desc properties )

            public String UserName
            {
                // Access member.

                set
                {
                    m_UserName = value;
                }
                get
                {
                    return m_UserName;
                }
            }

            public String Branch
            {
                // Access member.

                set
                {
                    m_Branch = value;
                }
                get
                {
                    return m_Branch;
                }
            }

            public String Phone
            {
                // Access member.

                set
                {
                    m_Phone = value;
                }
                get
                {
                    return m_Phone;
                }
            }

            public String Email
            {
                // Access member.

                set
                {
                    m_Email = value;
                }
                get
                {
                    return m_Email;
                }
            }

            public Boolean IsAOwner
            {
                // Access member.

                set
                {
                    m_IsAOwner = value;
                }
                get
                {
                    return m_IsAOwner;
                }
            }

			#endregion

        }
        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
			BindRolodex();
            BindInternalEmployee();
		}
        private void BindRolodex()
        {
            SqlParameter[] parameters = { 
                                            new SqlParameter("@BrokerID", BrokerUser.BrokerId) , 
                                            new SqlParameter("@NameFilter", RequestHelper.GetSafeQueryString("filter")) };
            DataSet ds = new DataSet();
            
            DataSetHelper.Fill(ds, BrokerUser.BrokerId, "ListRolodexByBrokerID", parameters);

            // Fixup the table with an extra column that contains the
            // text description of the agent type, which we can use
            // for sorting by description and not by enum.            

            DataTable table = ds.Tables[ 0 ];
            

            table.Columns.Add( "AgentTypeDesc" , typeof( string ) );

            foreach( DataRow row in table.Rows )
            {
                row[ "AgentTypeDesc" ] = RolodexDB.GetTypeDescription( ( E_AgentRoleT ) row[ "AgentType" ] );
            }


            m_rolodexDG.DataSource = table.DefaultView;
            ((DataView) m_rolodexDG.DataSource).Sort = "AgentNm ASC";
            m_rolodexDG.DataBind();
        }
        private void BindInternalEmployee()
        {
            BrokerUserPermissionsSet bupSet = new BrokerUserPermissionsSet();
            BrokerEmployeeNameTable  eTable = new BrokerEmployeeNameTable();
            BrokerBranchTable        bTable = new BrokerBranchTable();
            ArrayList                empSet = new ArrayList();

            string filter = RequestHelper.GetSafeQueryString("filter");
            if( filter != "" )
            {
                eTable.Retrieve( BrokerUser.BrokerId , filter );
            }
            else
            {
                eTable.Retrieve( BrokerUser.BrokerId );
            }

            bTable.Retrieve( BrokerUser.BrokerId );

            bupSet.Retrieve( BrokerUser.BrokerId );

            foreach( EmployeeDetails eD in eTable.Values )
            {
                BrokerUserPermissions buP = bupSet[ eD.EmployeeId ];

                if( buP == null || buP.IsInternalBrokerUser() == false )
                {
                    if( eD.Type == "" || eD.Type == "B" )
                    {
                        EmployeeDesc rD = new EmployeeDesc();

                        rD.UserName = eD.FullName;
                        rD.Phone    = eD.Phone;
                        rD.Email    = eD.Email;
                        rD.IsAOwner = eD.IsAOwner;

                        if( bTable.Contains( eD.BranchId ) == true )
                        {
                            rD.Branch = bTable[ eD.BranchId ].BranchName;
                        }
                        else
                        {
                            rD.Branch = "?";
                        }

                        empSet.Add( rD );
                    }
                }
            }
			
			
            m_internalEmployeeDG.DataSource = ViewGenerator.Generate( empSet );
            m_internalEmployeeDG.DataBind();
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
