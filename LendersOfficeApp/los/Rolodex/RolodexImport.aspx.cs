namespace LendersOfficeApp.los.Rolodex
{
    using System;
    using System.IO;
    using System.Web;
    using System.Web.UI;
    using DataAccess;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;

    /// <summary>
    /// Allow users to import contacts from external csv file.  
    /// </summary>

    // 3/13/2006 mf - OPM 2706.  Designed to accept CSV files generated
    // by Outlook from a contacts export.  Rejects all others.
    public partial class RolodexImport : LendersOffice.Common.BasePage
	{

		// For status of import
		protected System.String	m_importStatus = "NONE";
		protected System.Int32 m_sucessfulImports = 0;
		protected System.Int32 m_failedImports = 0;
		
		const int DUPLICATES_EXPIRATION_MINUTES = 20;

		protected void PageLoad( object sender , System.EventArgs e )
		{
		}

		protected void OnImportClick(Object sender, EventArgs e)
		{
            if (m_inputFile.PostedFile != null)
            {
                HttpPostedFile file = m_inputFile.PostedFile;
                //We need to keep track of the extension, since that's how we differentiate between
                //different kinds of files.
                string tempFilename = Path.GetTempFileName() + Path.GetExtension(m_inputFile.PostedFile.FileName);
                file.SaveAs(tempFilename);

                // Import file
                RolodexImporter importedList = new RolodexImporter(((BrokerUserPrincipal)Page.User).BrokerId);

                try
                {
                    if (!importedList.Import(tempFilename))
                    {
                        // Import process failed.
                        m_importStatus = "ERROR";
                        return;
                    }
                }
                catch (InvalidOperationException)
                {
                    m_importStatus = "ERROR";
                    return;
                }

                // Write to DB
                if (!importedList.WriteEntriesToRolodex())
                {
                    // Import failure occured in writing to contacts, set UI status
                    m_importStatus = "ERROR";
                    return;
                }

                if (importedList.Duplicates.Rows.Count == 0)
                {
                    ViewState.Remove("DuplicatesKey");
                    m_saveDuplicatesBtn.Enabled = false;
                }

                string csvDupes = importedList.CreateDuplicatesCSV();
                if (!string.IsNullOrEmpty(csvDupes))
                {
                    // Write the dupes to temporary cache
                    try
                    {
                        string cacheId = AutoExpiredTextCache.AddToCache(csvDupes, TimeSpan.FromMinutes(DUPLICATES_EXPIRATION_MINUTES));

                        ViewState.Add("DuplicatesKey", cacheId);
                        // Enable Buttons
                        m_saveDuplicatesBtn.Enabled = true;
                    }
                    catch (Exception exc)
                    {
                        Tools.LogInfo("Error in Rolodex Import" + exc);
                        m_importStatus = "ERROR";
                    }
                }

                // Set UI status information
                m_importStatus = "OK";
                m_sucessfulImports = importedList.LoTable.Rows.Count;
                m_failedImports = importedList.Duplicates.Rows.Count;
            }
		}

		// Load duplicates file from fileDB if it exists
		protected void OnSaveDuplicatesClick(Object sender, EventArgs e)
		{
			if (ViewState["DuplicatesKey"] != null)
			{
				string duplicatesKey = (string) ViewState["DuplicatesKey"];
				try
				{
					string csv = AutoExpiredTextCache.GetFromCache(duplicatesKey);
					if (csv != null)
					{
						Response.ClearHeaders();
						Response.Output.Write(csv);
						Response.AppendHeader("Content-Disposition", "attachment; filename=\"Duplicates.csv\"");
						Response.ContentType = "application/csv";
                        Response.Flush();
						Response.End();
					}
				}
				catch (System.Threading.ThreadAbortException)
				{
					// Response.End throws this by design
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}