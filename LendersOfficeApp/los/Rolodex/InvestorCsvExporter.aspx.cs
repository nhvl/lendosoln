﻿#region generated code
namespace LendersOfficeApp.los.Rolodex
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;

    using LendersOffice.ObjLib.Rolodex;
    using LendersOffice.Security;
    using LendersOffice.Common;

    /// <summary>
    /// A service page for exporting the investor list.
    /// </summary>
    public partial class InvestorCsvExporter : BasePage
    {
        /// <summary>
        /// Exports the list of investors.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            RequestHelper.SendFileToClient(Context, InvestorExporter.GenerateInvestorsCsv(PrincipalFactory.CurrentPrincipal), "text/csv", "Investors.csv");
        }
    }
}