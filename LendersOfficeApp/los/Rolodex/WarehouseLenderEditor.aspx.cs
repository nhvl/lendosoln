﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using LendersOffice.ObjLib.Rolodex;
using LendersOffice.Security;
using DataAccess;
using System.Data.SqlClient;

namespace LendersOfficeApp.los.Rolodex
{
    public partial class WarehouseLenderEditor : BaseServicePage
    {
        private WarehouseLenderRolodexEntry m_entry = null;
        protected WarehouseLenderRolodexEntry CurrentEntry
        {
            get
            {
                if (m_entry == null)
                {
                    int id = RequestHelper.GetInt("Id", -1);
                    m_entry = WarehouseLenderRolodexEntry.Get(PrincipalFactory.CurrentPrincipal.BrokerId, id);
                }

                return m_entry;
            }
        }
        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.EnableJqueryMigrate = false;
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ErrorMessage.Text = "";
            RegisterJsScript("utilities.js");
            RegisterService("loanutils", "/newlos/LoanUtilitiesService.aspx");

            Status.Items.Add(new ListItem("Active", "1"));
            Status.Items.Add(new ListItem("Inactive", "0"));

            if (!Page.IsPostBack)
            {
                if (!CurrentEntry.IsNew)
                {
                    Tools.SetDropDownListValue(Status, CurrentEntry.Status);
                }
                WarehouseLenderName.Text = CurrentEntry.WarehouseLenderName;
                FannieMaePayeeId.Text = CurrentEntry.FannieMaePayeeId;
                FreddieMacPayeeId.Value = CurrentEntry.FreddieMacPayeeId;
                this.WarehouseLenderFannieMaeId.Value = CurrentEntry.WarehouseLenderFannieMaeId;
                this.WarehouseLenderFreddieMacId.Value = CurrentEntry.WarehouseLenderFreddieMacId;
                MersOrganizationId.Text = CurrentEntry.MersOrganizationId;
                PayToBankName.Text = CurrentEntry.PayToBankName;
                PayToCity.Text = CurrentEntry.PayToCity;
                PayToState.Value = CurrentEntry.PayToState;
                PayToABANum.Text = CurrentEntry.PayToABANum.Value;
                PayToAccountName.Text = CurrentEntry.PayToAccountName;
                PayToAccountNum.Text = CurrentEntry.PayToAccountNum.Value;
                FurtherCreditToAccountName.Text = CurrentEntry.FurtherCreditToAccountName;
                FurtherCreditToAccountNum.Text = CurrentEntry.FurtherCreditToAccountNum.Value;
                MainCompanyName.Text = CurrentEntry.MainCompanyName;
                MainContactName.Text = CurrentEntry.MainContactName;
                MainAttention.Text = CurrentEntry.MainAttention;
                MainEmail.Text = CurrentEntry.MainEmail;
                MainAddress.Text = CurrentEntry.MainAddress;
                MainCity.Text = CurrentEntry.MainCity;
                MainState.Value = CurrentEntry.MainState;
                MainZip.Text = CurrentEntry.MainZip;
                MainPhone.Text = CurrentEntry.MainPhone;
                MainFax.Text = CurrentEntry.MainFax;
                CollateralPackageToAttention.Text = CurrentEntry.CollateralPackageToAttention;
                CollateralPackageToContactName.Text = CurrentEntry.CollateralPackageToContactName;
                CollateralPackageToUseMainAddress.Checked = CurrentEntry.CollateralPackageToUseMainAddress;
                CollateralPackageToEmail.Text = CurrentEntry.CollateralPackageToEmail;
                CollateralPackageToAddress.Text = CurrentEntry.CollateralPackageToAddress;
                CollateralPackageToCity.Text = CurrentEntry.CollateralPackageToCity;
                CollateralPackageToState.Value = CurrentEntry.CollateralPackageToState;
                CollateralPackageToZip.Text = CurrentEntry.CollateralPackageToZip;
                CollateralPackageToPhone.Text = CurrentEntry.CollateralPackageToPhone;
                CollateralPackageToFax.Text = CurrentEntry.CollateralPackageToFax;
                ClosingPackageToAttention.Text = CurrentEntry.ClosingPackageToAttention;
                ClosingPackageToContactName.Text = CurrentEntry.ClosingPackageToContactName;
                ClosingPackageToUseMainAddress.Checked = CurrentEntry.ClosingPackageToUseMainAddress;
                ClosingPackageToEmail.Text = CurrentEntry.ClosingPackageToEmail;
                ClosingPackageToAddress.Text = CurrentEntry.ClosingPackageToAddress;
                ClosingPackageToCity.Text = CurrentEntry.ClosingPackageToCity;
                ClosingPackageToState.Value = CurrentEntry.ClosingPackageToState;
                ClosingPackageToZip.Text = CurrentEntry.ClosingPackageToZip;
                ClosingPackageToPhone.Text = CurrentEntry.ClosingPackageToPhone;
                ClosingPackageToFax.Text = CurrentEntry.ClosingPackageToFax;
                try
                {
                    AdvanceClassificationDesc1.Text = CurrentEntry.AdvanceClassifications[0].Description;
                    AdvanceClassificationFunding1.Text = CurrentEntry.AdvanceClassifications[0].MaxFundingPercent_rep;
                    AdvanceClassificationDesc2.Text = CurrentEntry.AdvanceClassifications[1].Description;
                    AdvanceClassificationFunding2.Text = CurrentEntry.AdvanceClassifications[1].MaxFundingPercent_rep;
                    AdvanceClassificationDesc3.Text = CurrentEntry.AdvanceClassifications[2].Description;
                    AdvanceClassificationFunding3.Text = CurrentEntry.AdvanceClassifications[2].MaxFundingPercent_rep;
                    AdvanceClassificationDesc4.Text = CurrentEntry.AdvanceClassifications[3].Description;
                    AdvanceClassificationFunding4.Text = CurrentEntry.AdvanceClassifications[3].MaxFundingPercent_rep;
                    AdvanceClassificationDesc5.Text = CurrentEntry.AdvanceClassifications[4].Description;
                    AdvanceClassificationFunding5.Text = CurrentEntry.AdvanceClassifications[4].MaxFundingPercent_rep;
                    AdvanceClassificationDesc6.Text = CurrentEntry.AdvanceClassifications[5].Description;
                    AdvanceClassificationFunding6.Text = CurrentEntry.AdvanceClassifications[5].MaxFundingPercent_rep;
                }
                catch (ArgumentOutOfRangeException)
                {
                    //rows not required - do nothing
                }
            }
        }


        protected void ApplyBtn_Click(object sender, EventArgs args)
        {
            if (Save())
            {
                Response.Redirect("WarehouseLenderEditor.aspx?id=" + CurrentEntry.Id);
            }
        }

        protected void OkBtn_Click(object sender, EventArgs args)
        {
            if (Save())
            {
                ClientScript.RegisterHiddenField("Close", "Close");
            }
        }

        private bool Save()
        {
            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowWritingToRolodex))
            {
                ErrorMessage.Text = "You do not have the propper permissions to edit the rolodex.";
                return false;
            }
            if (string.IsNullOrEmpty(WarehouseLenderName.Text))
            {
                ErrorMessage.Text = "Warehouse lender name is required.";
                return false;
            }
            if (!ValidateAdvanceClassificationTable())
            {
                ErrorMessage.Text = "All Advance classification entries in use must have both a Description and a Max Funding %";
                return false;
            }

            CurrentEntry.Status = (E_WarehouseLenderStatus)int.Parse(Status.SelectedValue);
            CurrentEntry.WarehouseLenderName = WarehouseLenderName.Text;
            CurrentEntry.FannieMaePayeeId = FannieMaePayeeId.Text;
            CurrentEntry.FreddieMacPayeeId = FreddieMacPayeeId.Value;
            CurrentEntry.WarehouseLenderFannieMaeId = this.WarehouseLenderFannieMaeId.Value;
            CurrentEntry.WarehouseLenderFreddieMacId = this.WarehouseLenderFreddieMacId.Value;
            CurrentEntry.MersOrganizationId = MersOrganizationId.Text;
            CurrentEntry.PayToBankName = PayToBankName.Text;
            CurrentEntry.PayToCity = PayToCity.Text;
            CurrentEntry.PayToState = PayToState.Value;
            CurrentEntry.PayToABANum = PayToABANum.Text;
            CurrentEntry.PayToAccountName = PayToAccountName.Text;
            CurrentEntry.PayToAccountNum = PayToAccountNum.Text;
            CurrentEntry.FurtherCreditToAccountName = FurtherCreditToAccountName.Text;
            CurrentEntry.FurtherCreditToAccountNum = FurtherCreditToAccountNum.Text;
            CurrentEntry.MainCompanyName = MainCompanyName.Text;
            CurrentEntry.MainContactName = MainContactName.Text;
            CurrentEntry.MainAttention = MainAttention.Text;
            CurrentEntry.MainEmail = MainEmail.Text;
            CurrentEntry.MainAddress = MainAddress.Text;
            CurrentEntry.MainCity = MainCity.Text;
            CurrentEntry.MainState = MainState.Value;
            CurrentEntry.MainZip = MainZip.Text;
            CurrentEntry.MainPhone = MainPhone.Text;
            CurrentEntry.MainFax = MainFax.Text;
            CurrentEntry.CollateralPackageToAttention = CollateralPackageToAttention.Text;
            CurrentEntry.CollateralPackageToContactName = CollateralPackageToContactName.Text;
            CurrentEntry.CollateralPackageToUseMainAddress = CollateralPackageToUseMainAddress.Checked;
            CurrentEntry.CollateralPackageToEmail = CollateralPackageToEmail.Text;
            CurrentEntry.CollateralPackageToAddress = CollateralPackageToAddress.Text;
            CurrentEntry.CollateralPackageToCity = CollateralPackageToCity.Text;
            CurrentEntry.CollateralPackageToState = CollateralPackageToState.Value;
            CurrentEntry.CollateralPackageToZip = CollateralPackageToZip.Text;
            CurrentEntry.CollateralPackageToPhone = CollateralPackageToPhone.Text;
            CurrentEntry.CollateralPackageToFax = CollateralPackageToFax.Text;
            CurrentEntry.ClosingPackageToAttention = ClosingPackageToAttention.Text;
            CurrentEntry.ClosingPackageToContactName = ClosingPackageToContactName.Text;
            CurrentEntry.ClosingPackageToUseMainAddress = ClosingPackageToUseMainAddress.Checked;
            CurrentEntry.ClosingPackageToEmail = ClosingPackageToEmail.Text;
            CurrentEntry.ClosingPackageToAddress = ClosingPackageToAddress.Text;
            CurrentEntry.ClosingPackageToCity = ClosingPackageToCity.Text;
            CurrentEntry.ClosingPackageToState = ClosingPackageToState.Value;
            CurrentEntry.ClosingPackageToZip = ClosingPackageToZip.Text;
            CurrentEntry.ClosingPackageToPhone = ClosingPackageToPhone.Text;
            CurrentEntry.ClosingPackageToFax = ClosingPackageToFax.Text;

            var list = new List<WarehouseLenderAdvanceClassification>();
            if (!string.IsNullOrEmpty(AdvanceClassificationDesc1.Text.TrimWhitespaceAndBOM()) || !string.IsNullOrEmpty(AdvanceClassificationFunding1.Text.TrimWhitespaceAndBOM()))
            {
                var item = new WarehouseLenderAdvanceClassification();
                item.Description = AdvanceClassificationDesc1.Text.TrimWhitespaceAndBOM();
                item.MaxFundingPercent_rep = AdvanceClassificationFunding1.Text.TrimWhitespaceAndBOM();
                list.Add(item);
            }
            if (!string.IsNullOrEmpty(AdvanceClassificationDesc2.Text.TrimWhitespaceAndBOM()) || !string.IsNullOrEmpty(AdvanceClassificationFunding2.Text.TrimWhitespaceAndBOM()))
            {
                var item = new WarehouseLenderAdvanceClassification();
                item.Description = AdvanceClassificationDesc2.Text.TrimWhitespaceAndBOM();
                item.MaxFundingPercent_rep = AdvanceClassificationFunding2.Text.TrimWhitespaceAndBOM();
                list.Add(item);
            }
            if (!string.IsNullOrEmpty(AdvanceClassificationDesc3.Text.TrimWhitespaceAndBOM()) || !string.IsNullOrEmpty(AdvanceClassificationFunding3.Text.TrimWhitespaceAndBOM()))
            {
                var item = new WarehouseLenderAdvanceClassification();
                item.Description = AdvanceClassificationDesc3.Text.TrimWhitespaceAndBOM();
                item.MaxFundingPercent_rep = AdvanceClassificationFunding3.Text.TrimWhitespaceAndBOM();
                list.Add(item);
            }
            if (!string.IsNullOrEmpty(AdvanceClassificationDesc4.Text.TrimWhitespaceAndBOM()) || !string.IsNullOrEmpty(AdvanceClassificationFunding4.Text.TrimWhitespaceAndBOM()))
            {
                var item = new WarehouseLenderAdvanceClassification();
                item.Description = AdvanceClassificationDesc4.Text.TrimWhitespaceAndBOM();
                item.MaxFundingPercent_rep = AdvanceClassificationFunding4.Text.TrimWhitespaceAndBOM();
                list.Add(item);
            }
            if (!string.IsNullOrEmpty(AdvanceClassificationDesc5.Text.TrimWhitespaceAndBOM()) || !string.IsNullOrEmpty(AdvanceClassificationFunding5.Text.TrimWhitespaceAndBOM()))
            {
                var item = new WarehouseLenderAdvanceClassification();
                item.Description = AdvanceClassificationDesc5.Text.TrimWhitespaceAndBOM();
                item.MaxFundingPercent_rep = AdvanceClassificationFunding5.Text.TrimWhitespaceAndBOM();
                list.Add(item);
            }
            if (!string.IsNullOrEmpty(AdvanceClassificationDesc6.Text.TrimWhitespaceAndBOM()) || !string.IsNullOrEmpty(AdvanceClassificationFunding6.Text.TrimWhitespaceAndBOM()))
            {
                var item = new WarehouseLenderAdvanceClassification();
                item.Description = AdvanceClassificationDesc6.Text.TrimWhitespaceAndBOM();
                item.MaxFundingPercent_rep = AdvanceClassificationFunding6.Text.TrimWhitespaceAndBOM();
                list.Add(item);
            }
            CurrentEntry.AdvanceClassifications = list;

            try
            {
                CurrentEntry.Save();
                return true;
            }
            catch (SqlException sqlEx)
            {
                if (sqlEx.Message.Contains("duplicate key"))
                {
                    ErrorMessage.Text = "A Warehouse Lender with name '" + CurrentEntry.WarehouseLenderName + "' already exists.";
                    return false;
                }
                throw new CBaseException(ErrorMessages.Generic, sqlEx);
            }
        }
        private bool ValidateAdvanceClassificationTable()
        {
            //they must be both empty or non-empty to be valid
            if (string.IsNullOrEmpty(AdvanceClassificationDesc1.Text.TrimWhitespaceAndBOM()) != string.IsNullOrEmpty(AdvanceClassificationFunding1.Text.TrimWhitespaceAndBOM()))
                return false;
            if (string.IsNullOrEmpty(AdvanceClassificationDesc2.Text.TrimWhitespaceAndBOM()) != string.IsNullOrEmpty(AdvanceClassificationFunding2.Text.TrimWhitespaceAndBOM()))
                return false;
            if (string.IsNullOrEmpty(AdvanceClassificationDesc3.Text.TrimWhitespaceAndBOM()) != string.IsNullOrEmpty(AdvanceClassificationFunding3.Text.TrimWhitespaceAndBOM()))
                return false;
            if (string.IsNullOrEmpty(AdvanceClassificationDesc4.Text.TrimWhitespaceAndBOM()) != string.IsNullOrEmpty(AdvanceClassificationFunding4.Text.TrimWhitespaceAndBOM()))
                return false;
            if (string.IsNullOrEmpty(AdvanceClassificationDesc5.Text.TrimWhitespaceAndBOM()) != string.IsNullOrEmpty(AdvanceClassificationFunding5.Text.TrimWhitespaceAndBOM()))
                return false;
            if (string.IsNullOrEmpty(AdvanceClassificationDesc6.Text.TrimWhitespaceAndBOM()) != string.IsNullOrEmpty(AdvanceClassificationFunding6.Text.TrimWhitespaceAndBOM()))
                return false;
            return true;
        }
    }
}
