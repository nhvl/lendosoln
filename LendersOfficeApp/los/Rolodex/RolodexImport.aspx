<%@ Page language="c#" Codebehind="RolodexImport.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Rolodex.RolodexImport" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
        <title>Import To Contacts</title>
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </HEAD>
<body MS_POSITIONING="FlowLayout" scroll=no>
    <script>
        function _init() {
          <% if ( !Page.IsPostBack ) { %>
            resize( 480 , 180 );
          <% } %>
		    var importStatus = <%= AspxTools.JsString(m_importStatus)%>;

            if (importStatus == "OK") {
		      document.getElementById("StatusLabel").innerHTML = "<font color=green><%= AspxTools.HtmlString(m_sucessfulImports.ToString()) %> Entr<%= AspxTools.HtmlString((m_sucessfulImports == 1) ? "y" : "ies") %> Imported.</font><font color=red> <%= AspxTools.HtmlString(m_failedImports.ToString()) %> Duplicate<%= AspxTools.HtmlString((m_failedImports == 1) ? "" : "s") %> not imported.</font>";
		      if (<%= AspxTools.JsNumeric(m_sucessfulImports) %> > 0 )
				window.dialogArguments.OK = true;
			} else if (importStatus.indexOf("ERROR") != -1 ) {
		      document.getElementById("StatusLabel").innerHTML = "<font color=red>Unable to Import. Try again or select another file.</font>";
		    }

				var args = getModalArgs();
		    if(!args || !args.isOutlook)
		    {
		        document.getElementById("ImportContactsTitle").style.display = '';
		        document.getElementById("ImportOutlookTitle").style.display = 'none';
		        document.getElementById("OutlookHelpLink").style.display = 'none';
		    }

      }

		function onImportClick() { try
		{
			<%= AspxTools.JsGetElementById(m_importBtn) %>.click();

			<%-- Use the Client Validator to determine if this click will submit form --%>
			if (typeof(Page_ClientValidate) == 'function') Page_ClientValidate();
				if(!ValidatorOnSubmit()) return false;
			else
			{
					document.getElementById("StatusLabel").innerHTML = "File Submitted...";
			}
		}
		catch (e)
		{}}
    </script>
    <h4 class="page-header">
        <span id="ImportOutlookTitle">Import Outlook 2003/2007 Contacts</span>
        <span style="display:none" id="ImportContactsTitle">Import Contacts</span>
    </h4>
	<form id="RolodexImport" method="post" runat="server">
		<table width="100%" height="100%" border="0" cellspacing="2" cellpadding="3">
		<tr height="45" valign=top><td>
			<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0 >
				<TR vAlign=top>
				<TD nowrap class=FieldLabel width="1%">File: </TD>
				<TD align=left><INPUT id=m_inputFile style="WIDTH: 380px" type=file size=60
					name=m_inputFile runat="server">&nbsp;<asp:RequiredFieldValidator id=m_inputFileValidator Runat="server" Display="Static" ErrorMessage="*" ControlToValidate="m_inputFile" ></asp:RequiredFieldValidator>
					</TD></TR>
					<tr><td></td>
					<td>
						<ml:NoDoubleClickButton id="m_importBtn" runat="server" Text=" Import " OnClick="OnImportClick" style="DISPLAY: none"></ml:NoDoubleClickButton>
						<input type=button id='importbtn' onclick="onImportClick();" value=" Import " onfocus="blur()">&nbsp;
						<input type="button" onclick="onClosePopup();" value="Cancel">&nbsp;
						<asp:Button ID="m_saveDuplicatesBtn" Runat="server" Text="Download Duplicates CSV" OnClick="OnSaveDuplicatesClick" CausesValidation="False" Enabled=False />
						</td></tr>
					<tr><td valign=top class="FieldLabel">Status:</td>
					<td>
                        <span id="StatusLabel">Choose file.</span> <asp:RegularExpressionValidator id=m_inputFileTypeValidator Runat="server" Display="Dynamic" ControlToValidate="m_inputFile" ForeColor=red  ErrorMessage="&nbsp;Please choose a valid file (.csv)" ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.[cC][sS][vV])$"></asp:RegularExpressionValidator>
					</td></tr>
			</TABLE>
		</td></tr>
		<tr><td valign="top" id="OutlookHelpLink">
		For help, see our user guide:&nbsp; <a href="/help/userguides/How_To_Import_Outlook_200.aspx" style="cursor:help" target=_blank title="Click here for help">How to Import Your Outlook 2003/2007 Contacts</a>.
		</td></tr>
	</table>
	<uc1:cModalDlg runat="server" id=CModalDlg1></uc1:cModalDlg>
    </form>
  </body>
</HTML>
