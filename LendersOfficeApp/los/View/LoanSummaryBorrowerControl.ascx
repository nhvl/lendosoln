<%@ Control Language="c#" AutoEventWireup="false" Codebehind="LoanSummaryBorrowerControl.ascx.cs" Inherits="LendersOfficeApp.los.View.LoanSummaryBorrowerControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
    <tr>
        <td width="50%"><strong>Primary</strong></td>
        <td width="50%"><strong>Co-Borrower</strong></td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" vAlign="top">
            <TABLE id="Table4" cellSpacing="0" cellPadding="3" width="100%" border="0">
                <TR>
                    <TD>Name:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_borrowerNameLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>SSN:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_borrowerSSNLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Age:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_borrowerAgeLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Email:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_borrowerEmailLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Home Phone:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_borrowerHomePhoneLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                  <TD>Cell Phone:</TD>
                  <TD><strong><ml:EncodedLabel id=m_borrowerCellPhoneLabel runat="server"></ml:EncodedLabel></strong></TD></TR>
                <TR>
                  <TD>Work Phone:</TD>
                  <TD><strong><ml:EncodedLabel id=m_borrowerWorkPhoneLabel runat="server"></ml:EncodedLabel></strong></TD></TR>
                <TR>
                  <TD>Fax: </TD>
                  <TD><strong><ml:EncodedLabel id=m_borrowerFaxLabel runat="server" EnableViewState="False"></ml:EncodedLabel></strong></TD></TR>
                <TR>
                    <TD>Marital Status:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_borrowerMaritalStatusLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Present Address:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_borrowerPresentAddressLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD></TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_borrowerPresentAddress1Label" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Status:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_borrowerPresentStatusLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Years of Residence:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_borrowerPresentLengthLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Credit Score:</TD>
                    <TD>
                        <div id="m_borrowerCreditDiv" style="display:none" runat="server">
                            XP: <strong><ml:EncodedLabel id="m_borrowerCreditExperianLabel" runat="server"></ml:EncodedLabel></strong>
                            EQ: <strong><ml:EncodedLabel id="m_borrowerCreditEquifaxLabel" runat="server"></ml:EncodedLabel></strong>
                            TU:  <strong><ml:EncodedLabel id="m_borrowerCreditTransUnionLabel" runat="server"></ml:EncodedLabel></strong>
                        </div>
                    </TD>
                </TR>
            </TABLE>
        </td>
        <td bgcolor="#ffffff" vAlign="top">
            <TABLE id="Table5" cellSpacing="0" cellPadding="3" width="100%" border="0">
                <TR>
                    <TD>Name:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_coborrowerNameLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>SSN:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_coborrowerSSNLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Age:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_coborrowerAgeLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Email:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_coborrowerEmailLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Home Phone:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_coborrowerHomePhoneLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                  <TD>Cell Phone:</TD>
                  <TD><strong><ml:EncodedLabel id=m_coborrowerCellPhoneLabel runat="server"></ml:EncodedLabel></strong></TD></TR>
                <TR>
                  <TD>Work Phone:</TD>
                  <TD><strong><ml:EncodedLabel id=m_coborrowerWorkPhoneLabel runat="server"></ml:EncodedLabel></strong></TD></TR>
                <TR>
                  <TD>Fax:</TD>
                  <TD><strong><ml:EncodedLabel id=m_coborrowerFaxLabel runat="server" EnableViewState="False"></ml:EncodedLabel></strong></TD></TR>
                <TR>
                    <TD>Marital Status:</TD>
                    <TD>
                        <strong><ml:EncodedLabel id="m_coborrowerMaritalStatusLabel" runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Present Address:</TD>
                    <TD><strong><ml:EncodedLabel id=m_coborrowerPresentAddressLabel runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD></TD>
                    <TD><strong><ml:EncodedLabel id=m_coborrowerPresentAddress1Label runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Status:</TD>
                    <TD><strong><ml:EncodedLabel id=m_coborrowerPresentStatusLabel runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Years of Residence:</TD>
                    <TD><strong><ml:EncodedLabel id=m_coborrowerPresentLengthLabel runat="server"></ml:EncodedLabel></strong></TD>
                </TR>
                <TR>
                    <TD>Credit Score:</TD>
                    <TD>
                        <div id="m_coborrowerCreditDiv" style="display:none" runat="server">
                            XP: <strong><ml:EncodedLabel id="m_coborrowerCreditExperianLabel" runat="server"></ml:EncodedLabel></strong>
                            EQ: <strong><ml:EncodedLabel id="m_coborrowerCreditEquifaxLabel" runat="server"></ml:EncodedLabel></strong>
                            TU:  <strong><ml:EncodedLabel id="m_coborrowerCreditTransUnionLabel" runat="server"></ml:EncodedLabel></strong>
                        </div>
                    </TD>
                </TR>
            </TABLE>
        </td>
    </tr>
</table>
