using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using System.Web.UI;
using DataAccess.Utilities;
using LendersOffice.Security;

namespace LendersOfficeApp.los.View
{
    public partial class LoanStatusList : BasePage
    {
        // param = {
        //     src: "lead" | null,
        //     channel: E_BranchChannelT = null
        //     process: E_sCorrespondentProcessT = Blank
        // }

        protected bool m_hideLoanStatuses => RequestHelper.GetSafeQueryString("src") == "lead";

        protected E_BranchChannelT? BranchChannel
        {
            get
            {
                var channel = RequestHelper.GetInt("channel", -1);
                return channel == -1 ? null : (E_BranchChannelT?)channel;
            }
        }

        protected E_sCorrespondentProcessT CorrespondentProcess => (E_sCorrespondentProcessT) RequestHelper.GetInt("process", 0);

        protected void PageLoad(object sender, System.EventArgs e)
        {
            m_loanStatusesList.Visible = !m_hideLoanStatuses;
        }

        private void LinkButton_Command(object sender, CommandEventArgs e)
        {
            E_sStatusT sStatusT = (E_sStatusT)Enum.Parse(typeof(E_sStatusT), e.CommandArgument.ToString());

            CloseDialog(sStatusT);
        }

        void PageInit(object sender, EventArgs e)
        {
            AddLinkItems(m_leadLinks, LoanStatus.GetLeadStatusWithDesc());
            if (!m_hideLoanStatuses)
            {
                AddLinkItems(m_loanLinks, LoanStatus.GetLoanStatusWithDesc(BranchChannel.Value, CorrespondentProcess, PrincipalFactory.CurrentPrincipal.BrokerId));
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new EventHandler(this.PageInit);
        }
        #endregion

        private void CloseDialog(E_sStatusT sStatusT)
        {
            LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this, new[] {
                "StatusCode=" + sStatusT.ToString("D"),
                "Status=" + AspxTools.JsString(CPageBase.sStatusT_map_rep(sStatusT)),
                "OK=true"
            });
        }

        private void AddLinkItems(Control parent, IEnumerable<KeyValuePair<E_sStatusT, string>> statuses)
        {
            foreach (var status in statuses)
            {
                AddLinkItem(parent, status);
            }
        }

        private void AddLinkItem(Control parent, KeyValuePair<E_sStatusT, string> status)
        {
            var linkButton = new LinkButton
            {
                Text = status.Value,
                CommandArgument = status.Key.ToString("D"),
            };
            linkButton.Command += LinkButton_Command;

            var li = new HtmlGenericControl("li");
            li.Controls.Add(linkButton);

            parent.Controls.Add(li);
        }
    }
}
