<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="LoanStatusList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.View.LoanStatusList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<head runat="server">
  <title>LoanStatusList</title>
  <style>
  UL
{
    margin-left: 15px;
}

  </style>
</head>
<body MS_POSITIONING="FlowLayout" onload="init();" bgcolor="gainsboro">
		<script language="javascript">
    <!--
      function init() {
          resize(300, 550);
      }
    //-->
		</script>
    <h4 class="page-header">Change Status</h4>
		<form id="LoanStatusList" method="post" runat="server">
			<table width="100%" border="0">
				<tr>
					<td class="FieldLabel">Change file status to</td>
				</tr>
				<tr>
					<td>
            <ul>
              <li id="m_loanStatusesList" runat="server">Loan Statuses
                <ul id="m_loanLinks" runat="server" />
              </li>
              <li>Lead Statuses
                <ul id="m_leadLinks" runat="server" />
              </li>
            </ul>
          </td>
				</tr>
				<tr>
					<td align="center"><input type="button" value="Cancel" onclick="onClosePopup();"></td>
				</tr>
			</table>
								<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>			
		</form>
	</body>
</HTML>
