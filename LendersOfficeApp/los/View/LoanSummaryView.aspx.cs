using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;
using LendersOffice.CreditReport;
using LendersOffice.Reminders;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Admin;
using DataAccess;
using System.Collections.Generic;
using LendersOffice.ObjLib.Task;
using System.Linq;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.View
{
	public partial class LoanSummaryView : BasePage
	{
        #region Protected Member Variables
        #endregion

        private BrokerDB x_brokerDB = null;

        private BrokerDB CurrentBroker
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                }
                return x_brokerDB;
            }
        }
		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private Boolean UseUnderwritingConditions
		{
			get { return CurrentBroker.HasLenderDefaultFeatures; }
		}

		protected string GetAgentType(string code, string otherDesc) 
        {
            try 
            {
                E_AgentRoleT role = (E_AgentRoleT) int.Parse(code);
                if (role != E_AgentRoleT.Other || otherDesc.TrimWhitespaceAndBOM() == "") 
                {
                    return LendersOffice.Rolodex.RolodexDB.GetTypeDescription(role);
                } 
                else 
                {
                    return otherDesc;
                }
            } 
            catch {}
            return "";
        }

        // Display agent roles in alphabetical order
        private void BindAgentData(DataSet ds)
        {
            DataTable clone = ds.Tables[0].Clone();
            List<DataRow> agents = ds.Tables[0].Select().ToList();
            agents.Sort((r1, r2) =>
                {
                    return GetAgentType(r1["AgentRoleT"].ToString(), r1["OtherAgentRoleTDesc"].ToString())
                        .CompareTo(GetAgentType(r2["AgentRoleT"].ToString(), r2["OtherAgentRoleTDesc"].ToString()));
                });
            foreach (DataRow row in agents)
            {
                clone.Rows.Add(row.ItemArray);
            }
            m_agentRepeater.DataSource = clone.DefaultView;
            m_agentRepeater.DataBind();
        }

		protected void PageInit( object sender, System.EventArgs a )
		{
		}

		protected void PageLoad( object sender , System.EventArgs a )
		{

            CPageData dataLoan = new CLoanSummaryData(RequestHelper.LoanID);
            dataLoan.SetFormatTarget(FormatTarget.Webform);
            dataLoan.InitLoad();
            int appCount = dataLoan.nApps;

            m_salesPriceLabel.Text = string.Format("{0}", dataLoan.sPurchPrice_rep);
            m_downPaymentLabel.Text = string.Format("{0} / {1}", dataLoan.sDownPmtPc_rep, dataLoan.sEquityCalc_rep);
            m_appraisedValueLabel.Text = string.Format("{0}", dataLoan.sApprVal_rep);
            m_loanNumberLabel.Text = string.Format("{0}", Utilities.SafeHtmlString(dataLoan.sLNm));
            m_loanAmountLabel.Text = string.Format("{0}", dataLoan.sFinalLAmt_rep);
            m_loanPurposeLabel.Text = "" + dataLoan.sLPurposeT_rep + "";
            m_noteRateLabel.Text = string.Format("{0}", dataLoan.sNoteIR_rep);
            m_termLabel.Text = string.Format("{0}", dataLoan.sTerm_rep);
            m_dueLabel.Text = string.Format("{0}", dataLoan.sDue_rep);
            string interestOnlyLabel = dataLoan.sIOnlyMon_rep == "0" ? "" : " (* Interest Only)";
            m_monthlyLabel.Text           = string.Format("{0}{1}", dataLoan.sMonthlyPmt_rep, interestOnlyLabel);
            m_topRatioLabel.Text          = string.Format("{0}", dataLoan.sQualTopR_rep);
            m_bottomRatioLabel.Text       = string.Format("{0}", dataLoan.sQualBottomR_rep);
            m_ltvLabel.Text               = string.Format("{0}", dataLoan.sLtvR_rep);
            m_totalLTVLabel.Text          = string.Format("{0}", dataLoan.sCltvR_rep);
            m_totalIncomeLabel.Text       = string.Format("{0}", dataLoan.sLTotI_rep);
            m_totalLiabilitiesLabel.Text  = string.Format("{0}", dataLoan.sLiaMonLTot_rep);
            m_totalHouseExpenseLabel.Text = string.Format("{0}", dataLoan.sPresLTotPersistentHExp_rep);
            m_loanProgramLabel.Text       = string.Format("{0}", Utilities.SafeHtmlString( dataLoan.sLpTemplateNm));
            sLenderCaseNum.Text           = string.Format("{0}", Utilities.SafeHtmlString( dataLoan.sLenderCaseNum));
            sAgencyCaseNum.Text           = string.Format("{0}", Utilities.SafeHtmlString(dataLoan.sAgencyCaseNum));
            switch(dataLoan.sLienPosT) 
            {
                case E_sLienPosT.First:
                    m_lienPositionLabel.Text = "First"; break;
                case E_sLienPosT.Second:
                    m_lienPositionLabel.Text = "Second"; break;
            }
            m_loanStatusLabel.Text = string.Format("{0}", dataLoan.sStatusT_rep);
            m_notesLabel.Text = Utilities.SafeHtmlString(dataLoan.sTrNotes).Replace(Environment.NewLine, "<br>");
            for (int i = 0; i < appCount; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
                LoanSummaryBorrowerControl c = (LoanSummaryBorrowerControl) LoadControl("LoanSummaryBorrowerControl.ascx");
                if (c != null) 
                {
                    c.BorrowerName = Utilities.SafeHtmlString(d.aBNm);
                    c.BorrowerSSN = Utilities.SafeHtmlString(d.aBSsn);
                    c.BorrowerAge = d.aBAge_rep;
                    c.BorrowerEmail = Utilities.SafeHtmlString(d.aBEmail);
                    c.BorrowerHomePhone = Utilities.SafeHtmlString(d.aBHPhone);
                    c.BorrowerCellPhone = Utilities.SafeHtmlString(d.aBCellPhone);
                    c.BorrowerWorkPhone = Utilities.SafeHtmlString(d.aBBusPhone);
                    c.BorrowerFax = Utilities.SafeHtmlString(d.aBFax);
                    c.BorrowerMaritalStatus = d.aBMaritalStatT;
                    c.BorrowerScore = new CreditScore(d.aBEquifaxScore, d.aBExperianScore, d.aBTransUnionScore);
                    c.BorrowerPresentAddress = Utilities.SafeHtmlString(d.aBAddr);
                    c.BorrowerPresentAddress1 = Utilities.SafeHtmlString(Tools.CombineCityStateZip(d.aBCity, d.aBState, d.aBZip));
                    c.BorrowerPresentAddressLength = Utilities.SafeHtmlString(d.aBAddrYrs);
                    c.BorrowerPresentAddressStatus = d.aBAddrT;

                    c.EnableBorrowerScore = true;

                    string coborrowerName = Utilities.SafeHtmlString(d.aCNm);
                    c.CoborrowerName = coborrowerName;
                    if (coborrowerName.TrimWhitespaceAndBOM() != "") 
                    {
                        c.CoborrowerSSN = Utilities.SafeHtmlString(d.aCSsn);
                        c.CoborrowerAge = d.aCAge_rep;
                        c.CoborrowerEmail = Utilities.SafeHtmlString(d.aCEmail);
                        c.CoborrowerHomePhone = Utilities.SafeHtmlString(d.aCHPhone);
                        c.CoborrowerCellPhone = Utilities.SafeHtmlString(d.aCCellPhone);
                        c.CoborrowerWorkPhone = Utilities.SafeHtmlString(d.aCBusPhone);
                        c.CoborrowerFax = Utilities.SafeHtmlString(d.aCFax);
                        c.CoborrowerMaritalStatus = (E_aBMaritalStatT) d.aCMaritalStatT;
                        c.CoborrowerScore = new CreditScore(d.aCEquifaxScore, d.aCExperianScore, d.aCTransUnionScore);
                        c.CoborrowerPresentAddress = Utilities.SafeHtmlString(d.aCAddr);
                        c.CoborrowerPresentAddress1 = Utilities.SafeHtmlString(Tools.CombineCityStateZip(d.aCCity, d.aCState, d.aCZip));
                        c.CoborrowerPresentAddressLength = Utilities.SafeHtmlString(d.aCAddrYrs);
                        c.CoborrowerPresentAddressStatus = (E_aBAddrT) d.aCAddrT;

                        c.EnableCoborrowerScore = true;
                    } // end if (cobborrowerName.TrimWhitespaceAndBOM())
                    m_borrowerInformationPlaceHolder.Controls.Add(c);
                } 

            } // end for
            m_propertyAddress1Label.Text = Utilities.SafeHtmlString(dataLoan.sSpAddr);
            m_propertyAddress2Label.Text = Utilities.SafeHtmlString(Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            m_propertyAddress3CountyLabel.Text = Utilities.SafeHtmlString(dataLoan.sSpCounty);
            m_propertyAddress3YearLabel.Text = Utilities.SafeHtmlString(dataLoan.sYrBuilt);

            // Retrieve people working on loan info.
            DataTable table = null;
            table = CreatePeopleWorkingOnLoanTable();
            AddToPeopleWorkingOnLoanTable(table, "Call Center Agent", dataLoan.sEmployeeCallCenterAgent);
            AddToPeopleWorkingOnLoanTable(table, "Closer", dataLoan.sEmployeeCloser);
            AddToPeopleWorkingOnLoanTable(table, "Collateral Agent", dataLoan.sEmployeeCollateralAgent);
            AddToPeopleWorkingOnLoanTable(table, "Credit Auditor", dataLoan.sEmployeeCreditAuditor);
            AddToPeopleWorkingOnLoanTable(table, "Disclosure Desk", dataLoan.sEmployeeDisclosureDesk);
            AddToPeopleWorkingOnLoanTable(table, "Doc Drawer", dataLoan.sEmployeeDocDrawer);
            AddToPeopleWorkingOnLoanTable(table, "Funder", dataLoan.sEmployeeFunder);
            AddToPeopleWorkingOnLoanTable(table, "Insuring", dataLoan.sEmployeeInsuring);
            AddToPeopleWorkingOnLoanTable(table, "Junior Processor", dataLoan.sEmployeeJuniorProcessor);
            AddToPeopleWorkingOnLoanTable(table, "Junior Underwriter", dataLoan.sEmployeeJuniorUnderwriter);
            AddToPeopleWorkingOnLoanTable(table, "Legal Auditor", dataLoan.sEmployeeLegalAuditor);
            AddToPeopleWorkingOnLoanTable(table, "Lender Account Executive", dataLoan.sEmployeeLenderAccExec);
            AddToPeopleWorkingOnLoanTable(table, "Loan Officer", dataLoan.sEmployeeLoanRep);
            AddToPeopleWorkingOnLoanTable(table, "Loan Officer Assistant", dataLoan.sEmployeeLoanOfficerAssistant);
            AddToPeopleWorkingOnLoanTable(table, "Loan Opener", dataLoan.sEmployeeLoanOpener);
            AddToPeopleWorkingOnLoanTable(table, "Lock Desk", dataLoan.sEmployeeLockDesk);
            AddToPeopleWorkingOnLoanTable(table, "Manager", dataLoan.sEmployeeManager);
            AddToPeopleWorkingOnLoanTable(table, "Post-Closer", dataLoan.sEmployeePostCloser);
            AddToPeopleWorkingOnLoanTable(table, "Processor", dataLoan.sEmployeeProcessor);
            AddToPeopleWorkingOnLoanTable(table, "Purchaser", dataLoan.sEmployeePurchaser);
            AddToPeopleWorkingOnLoanTable(table, "QC Compliance", dataLoan.sEmployeeQCCompliance);
            AddToPeopleWorkingOnLoanTable(table, "Real Estate Agent", dataLoan.sEmployeeRealEstateAgent);
            AddToPeopleWorkingOnLoanTable(table, "Secondary", dataLoan.sEmployeeSecondary);
            AddToPeopleWorkingOnLoanTable(table, "Servicing", dataLoan.sEmployeeServicing);
            AddToPeopleWorkingOnLoanTable(table, "Shipper", dataLoan.sEmployeeShipper);
            AddToPeopleWorkingOnLoanTable(table, "Underwriter", dataLoan.sEmployeeUnderwriter);

            m_peopleRepeater.DataSource = table.DefaultView;
            m_peopleRepeater.DataBind();

            // Bind AgentList to list.
            DataSet ds = dataLoan.sAgentDataSet;
            BindAgentData(ds);

            if (dataLoan.BrokerDB.IsUseNewTaskSystem)
            {
                List<Task> conditions = Task.GetActiveConditionsByLoanId(dataLoan.BrokerDB.BrokerID, dataLoan.sLId, false, true);

                
                try
                {
                    var dataSource = from condition in conditions
                                     select new
                                     ConditionDesc()
                                     {
                                         Category = condition.CondCategoryId_rep,
                                         Description = condition.TaskSubject,
                                         IsHidden = condition.CondIsHidden,
                                         Id = Guid.Empty,
                                         IsDone = condition.TaskStatus == E_TaskStatus.Closed,
                                         DateDone = condition.TaskStatus == E_TaskStatus.Closed && condition.TaskClosedDate.HasValue ? condition.TaskClosedDate.Value.ToShortDateString() : ""
                                     };
           
                    m_underwritingDG.DataSource = dataSource;
                    m_underwritingDG.DataBind();
                    m_underwritingDG.Visible = true;
                    m_conditionDG.Visible = false;
                }
                catch (Exception e)
                {
                    Tools.LogError("Failed to load loan condition set.", e);
                }
                    
            }
            else
            {
                if (UseUnderwritingConditions == true)
                {
                    // 11/1/06 mf. Converted this from LoanConditionSet to 
                    // lighter CConditionSet because it is read-only.
                    try
                    {
                        CConditionSetObsolete lcSet = new CConditionSetObsolete(RequestHelper.LoanID);

                        m_underwritingDG.DataSource = lcSet.Digest;
                        m_underwritingDG.DataBind();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError("Failed to load loan condition set.", e);
                    }

                    m_underwritingDG.Visible = true;
                    m_conditionDG.Visible = false;

                }
                else
                {
                    ds = dataLoan.sCondDataSet.Value;
                    DataView dv = ds.Tables[0].DefaultView;
                    dv.RowFilter = "IsRequired='True'";
                    m_conditionDG.DataSource = dv;
                    m_conditionDG.DataBind();

                    m_underwritingDG.Visible = false;

                    m_conditionDG.Visible = true;
                }
            }

            sLeadSrcDesc.Text = dataLoan.sLeadSrcDesc;

            #region Bind Loan Status Table
            table = CreateLoanStatusTable();
            AddToLoanStatusTable(table, "Rate Lock", dataLoan.sRLckdD_rep, dataLoan.sRLckdN);
            AddToLoanStatusTable(table, "Rate Lock Expire", dataLoan.sRLckdExpiredD_rep, dataLoan.sRLckdExpiredN);
            AddToLoanStatusTable(table, "Lead New", dataLoan.sLeadD_rep, dataLoan.sLeadN);
            AddToLoanStatusTable(table, "Opened", dataLoan.sOpenedD_rep, dataLoan.sOpenedN);
            AddToLoanStatusTable(table, "Pre-qual", dataLoan.sPreQualD_rep, dataLoan.sPreQualN);
            AddToLoanStatusTable(table, "Preapproved", dataLoan.sPreApprovD_rep, dataLoan.sPreApprovN);
            AddToLoanStatusTable(table, "Submitted", dataLoan.sSubmitD_rep, dataLoan.sSubmitN);
            AddToLoanStatusTable(table, "Approved", dataLoan.sApprovD_rep, dataLoan.sApprovN);
            AddToLoanStatusTable(table, "Est Closing", dataLoan.sEstCloseD_rep, dataLoan.sEstCloseN);
            AddToLoanStatusTable(table, "Documents", dataLoan.sDocsD_rep, dataLoan.sDocsN);
            AddToLoanStatusTable(table, "Funded", dataLoan.sFundD_rep, dataLoan.sFundN);
            AddToLoanStatusTable(table, "Schedule Fund", dataLoan.sSchedFundD_rep, dataLoan.sSchedFundN);
            AddToLoanStatusTable(table, "Recorded", dataLoan.sRecordedD_rep, dataLoan.sRecordedN);
            AddToLoanStatusTable(table, "Loan On-hold", dataLoan.sOnHoldD_rep, dataLoan.sOnHoldN);
            AddToLoanStatusTable(table, "Loan Canceled", dataLoan.sCanceledD_rep, dataLoan.sCanceledN);
            AddToLoanStatusTable(table, "Loan Denied", dataLoan.sRejectD_rep, dataLoan.sRejectN);
            AddToLoanStatusTable(table, "Loan Suspended", dataLoan.sSuspendedD_rep, dataLoan.sSuspendedN);
            AddToLoanStatusTable(table, "Loan Closed", dataLoan.sClosedD_rep, dataLoan.sClosedN);
            AddToLoanStatusTable(table, dataLoan.sU1LStatDesc, dataLoan.sU1LStatD_rep, dataLoan.sU1LStatN);
            AddToLoanStatusTable(table, dataLoan.sU2LStatDesc, dataLoan.sU2LStatD_rep, dataLoan.sU2LStatN);
            AddToLoanStatusTable(table, dataLoan.sU3LStatDesc, dataLoan.sU3LStatD_rep, dataLoan.sU3LStatN);
            AddToLoanStatusTable(table, dataLoan.sU4LStatDesc, dataLoan.sU4LStatD_rep, dataLoan.sU4LStatN);
            m_loanStatusRepeater.DataSource = table.DefaultView;
            m_loanStatusRepeater.DataBind();
            #endregion


            #region Bind Basic Documents to repeater
            table = CreateBasicDocumentDataTable();
            AddToBasicDocumentTable(table, "Preliminary Report", dataLoan.sPrelimRprtOd_rep, dataLoan.sPrelimRprtRd_rep, dataLoan.sPrelimRprtN);
            AddToBasicDocumentTable(table, "Appraisal Report", dataLoan.sApprRprtOd_rep, dataLoan.sApprRprtRd_rep, dataLoan.sApprRprtN);
            AddToBasicDocumentTable(table, "TIL Disclosure / GFE", dataLoan.sTilGfeOd_rep, dataLoan.sTilGfeRd_rep, dataLoan.sTilGfeN);
            AddToBasicDocumentTable(table, dataLoan.sU1DocStatDesc, dataLoan.sU1DocStatOd_rep, dataLoan.sU1DocStatRd_rep, dataLoan.sU1DocStatN);
            AddToBasicDocumentTable(table, dataLoan.sU2DocStatDesc, dataLoan.sU2DocStatOd_rep, dataLoan.sU2DocStatRd_rep, dataLoan.sU2DocStatN);
            AddToBasicDocumentTable(table, dataLoan.sU3DocStatDesc, dataLoan.sU3DocStatOd_rep, dataLoan.sU3DocStatRd_rep, dataLoan.sU3DocStatN);
            AddToBasicDocumentTable(table, dataLoan.sU4DocStatDesc, dataLoan.sU4DocStatOd_rep, dataLoan.sU4DocStatRd_rep, dataLoan.sU4DocStatN);
            AddToBasicDocumentTable(table, dataLoan.sU5DocStatDesc, dataLoan.sU5DocStatOd_rep, dataLoan.sU5DocStatRd_rep, dataLoan.sU5DocStatN);

            for (int i = 0; i < appCount; i++) 
            {
                // I decide to use one letter variable name because I am too lazy
                // to think up a name, and also it will only be use in this for loop.
                CAppData d = dataLoan.GetAppData(i); 
                AddToBasicDocumentTable(table, "Credit Report", d.aCrOd_rep, d.aCrRd_rep, d.aCrN);
                AddToBasicDocumentTable(table, "Business Credit Report", d.aBusCrOd_rep, d.aBusCrRd_rep, d.aBusCrN);
                AddToBasicDocumentTable(table, d.aU1DocStatDesc, d.aU1DocStatOd_rep, d.aU1DocStatRd_rep, d.aU1DocStatN);
                AddToBasicDocumentTable(table, d.aU2DocStatDesc, d.aU2DocStatOd_rep, d.aU2DocStatRd_rep, d.aU2DocStatN);

            }
            m_basicDocumentRepeater.DataSource = table.DefaultView;
            m_basicDocumentRepeater.DataBind();
            #endregion

            #region Bind Verification of Employments to repeater
            table = CreateVerificationDataTable();
            for (int i = 0; i < appCount; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);

                IEmpCollection recordList = d.aBEmpCollection;

                // Get primary employment.
                IEmploymentRecord primaryEmployment = recordList.GetPrimaryEmp(false);
                if (null != primaryEmployment) 
                {
                    AddToVerificationTable(table, "(" + d.aBNm + ") " + primaryEmployment.EmplrNm, primaryEmployment.VerifSentD_rep, primaryEmployment.VerifReorderedD_rep, primaryEmployment.VerifRecvD_rep);
                }

                int cnt = recordList.CountRegular;
                for (int j = 0; j < cnt; j++) 
                {
                    IEmploymentRecord f = (IEmploymentRecord) recordList.GetRegularRecordAt(j);
                    AddToVerificationTable(table, "(" + d.aBNm + ") " + f.EmplrNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }
                // Get coborrower employment
                recordList = d.aCEmpCollection;
                primaryEmployment = recordList.GetPrimaryEmp(false);
                if (null != primaryEmployment) 
                {
                    AddToVerificationTable(table, "(" + d.aCNm + ") " + primaryEmployment.EmplrNm, primaryEmployment.VerifSentD_rep, primaryEmployment.VerifReorderedD_rep, primaryEmployment.VerifRecvD_rep);
                }
                cnt = recordList.CountRegular;
                for (int j = 0; j < cnt; j++) 
                {
                    IEmploymentRecord f = (IEmploymentRecord) recordList.GetRegularRecordAt(j);
                    AddToVerificationTable(table, "(" + d.aCNm + ") " + f.EmplrNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                }

            }
            m_voeRepeater.DataSource = table.DefaultView;
            m_voeRepeater.DataBind();
            #endregion


            #region Bind Verification of Deposit to repeater
            table.Clear();
            for (int i = 0; i < appCount; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);

				var subcoll = d.aAssetCollection.GetSubcollection( false, E_AssetGroupT.DontNeedVOD );
                Hashtable hash = new Hashtable();
				foreach( var item in subcoll )
				{
                    var f = (IAssetRegular)item;
                    if (!hash.ContainsKey(f.ComNm.ToLower())) 
                    {
                        hash.Add(f.ComNm.ToLower(), f.ComNm.ToLower());
                        AddToVerificationTable(table, f.ComNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
                    }
                }


            }
            m_vodRepeater.DataSource = table.DefaultView;
            m_vodRepeater.DataBind();
            #endregion

            #region Bind Verification of Mortgage & Loan to repeater
            DataTable vomTable = CreateVerificationDataTable();
            DataTable volTable = CreateVerificationDataTable();
			
            for (int i = 0; i < appCount; i++) 
            {
                CAppData d = dataLoan.GetAppData(i);
				ILiaCollection liaList = d.aLiaCollection;
				var liaSubcoll = liaList.GetSubcollection( true, E_DebtGroupT.Regular );
				foreach( ILiabilityRegular f in liaSubcoll )
				{
					switch( f.DebtT )
					{
						case E_DebtRegularT.Mortgage:
                        case E_DebtRegularT.Heloc:
							AddToVerificationTable(vomTable, f.ComNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
							break;
						case E_DebtRegularT.Installment:
						case E_DebtRegularT.Revolving:
							AddToVerificationTable(volTable, f.ComNm, f.VerifSentD_rep, f.VerifReorderedD_rep, f.VerifRecvD_rep);
							break;
					}
                }
            }
            m_vomRepeater.DataSource = vomTable.DefaultView;
            m_vomRepeater.DataBind();

            m_volRepeater.DataSource = volTable.DefaultView;
            m_volRepeater.DataBind();
            #endregion
			
		}
        private DataTable CreatePeopleWorkingOnLoanTable() 
        {
            DataTable table = new DataTable("PeopleWorkingOnLoan");
            table.Columns.Add(new DataColumn("Role", typeof(string)));
            table.Columns.Add(new DataColumn("FullName", typeof(string)));
            table.Columns.Add(new DataColumn("Phone", typeof(string)));
            table.Columns.Add(new DataColumn("Email", typeof(string)));
            return table;
        }
        private void AddToPeopleWorkingOnLoanTable(DataTable table, string role, CEmployeeFields employee) 
        {
            DataRow row = table.NewRow();
            row["Role"] = Utilities.SafeHtmlString(role);
            row["FullName"] = Utilities.SafeHtmlString(employee.FullName);
            row["Phone"] = Utilities.SafeHtmlString(employee.Phone);
            row["Email"] = Utilities.SafeHtmlString(employee.Email);

            table.Rows.Add(row);
        }

        protected HtmlImage DisplayIsDone(object value)
        {
            HtmlImage img = new HtmlImage();
            img.Src = (value != null && value.ToString() == "True") ? "../../images/checkmark.gif" : "../../images/checkbox.gif";
            return img;
        }

        protected HtmlImage DisplayIsRequired(object value)
        {
            HtmlImage img = new HtmlImage();
            img.Src = (value != null && value.ToString() == "True") ? "../../images/checkmark.gif" : "../../images/checkbox.gif";
            return img;
        }

        protected IEnumerable<HtmlControl> DisplayDoneInfo(object value)
        {
            ConditionDesc cDesc = value as ConditionDesc;
            List<HtmlControl> controls = null;

            if (cDesc != null)
            {
                if (UseUnderwritingConditions == false || cDesc.Category.ToLower().TrimWhitespaceAndBOM() != "notes")
                {
                    controls = new List<HtmlControl>();

                    HtmlImage img = new HtmlImage();
                    controls.Add(img);

                    if (cDesc.IsDone == true)
                    {
                        img.Src = "../../images/checkmark.gif";

                        var control = new HtmlGenericControl("span");
                        control.InnerText = cDesc.DateDone;
                        controls.Add(control);
                    }
                    else
                    {
                        img.Src = "../../images/checkbox.gif";
                    }
                }
            }

            return controls;
        }

        private DataTable CreateLoanStatusTable() 
        {
            DataTable table = new DataTable("LoanStatus");
            table.Columns.Add(new DataColumn("Description", typeof(string)));
            table.Columns.Add(new DataColumn("Date", typeof(string)));
            table.Columns.Add(new DataColumn("Comments", typeof(string)));
            return table;
        }

        private void AddToLoanStatusTable(DataTable table, string description, string date, string comments) 
        {
            if (date.TrimWhitespaceAndBOM() == "" && comments.TrimWhitespaceAndBOM() == "")
                return; // Skip empty entry;

            DataRow row = table.NewRow();
            row["Description"] = Utilities.SafeHtmlString(description);
            row["Date"] = date;
            row["Comments"] = Utilities.SafeHtmlString(comments);
            table.Rows.Add(row);

        }

        private DataTable CreateBasicDocumentDataTable() 
        {
            DataTable table = new DataTable("BasicDocument");
            table.Columns.Add(new DataColumn("Description", typeof(string)));
            table.Columns.Add(new DataColumn("Ordered", typeof(string)));
            table.Columns.Add(new DataColumn("Received", typeof(string)));
            table.Columns.Add(new DataColumn("Comments", typeof(string)));
            table.Columns.Add(new DataColumn("DaysOut", typeof(string)));
            table.Columns.Add(new DataColumn("DaysIn", typeof(string)));
            return table;

        }
        private void AddToBasicDocumentTable(DataTable table, string description, string ordered, string received, string comments) 
        {
            if (ordered.TrimWhitespaceAndBOM() == "" && received.TrimWhitespaceAndBOM() == "")
                return; // Skip empty entry.

            // Calculate daysOut & daysIn.
            string daysOut = "";
            string daysIn = "";
            try 
            {
                if (ordered.TrimWhitespaceAndBOM() != "") 
                {
                    DateTime dt = DateTime.Parse(ordered);
                    daysOut = DateTime.Now.Subtract(dt).Days.ToString();
                }
            } 
            catch {}

            try 
            {
                if (received.TrimWhitespaceAndBOM() != "") 
                {
                    DateTime dt = DateTime.Parse(received);
                    daysIn = DateTime.Now.Subtract(dt).Days.ToString();
                }
            } 
            catch {}

            DataRow row = table.NewRow();

            row["Description"] = Utilities.SafeHtmlString(description);
            row["Ordered"] = Utilities.SafeHtmlString(ordered);
            row["Received"] = Utilities.SafeHtmlString(received);
            row["Comments"] = Utilities.SafeHtmlString(comments);
            row["DaysOut"] = Utilities.SafeHtmlString(daysOut);
            row["DaysIn"] = Utilities.SafeHtmlString(daysIn);
            table.Rows.Add(row);
        }


        private DataTable CreateVerificationDataTable() 
        {
            DataTable table = new DataTable("BasicDocument");
            table.Columns.Add(new DataColumn("Description", typeof(string)));
            table.Columns.Add(new DataColumn("Ordered", typeof(string)));
            table.Columns.Add(new DataColumn("Reordered", typeof(string)));
            table.Columns.Add(new DataColumn("Received", typeof(string)));
            table.Columns.Add(new DataColumn("DaysOut", typeof(string)));
            table.Columns.Add(new DataColumn("DaysIn", typeof(string)));
            return table;
        }
        private void AddToVerificationTable(DataTable table, string description, string ordered, string reordered, string received) 
        {
            if (ordered.TrimWhitespaceAndBOM() == "" && received.TrimWhitespaceAndBOM() == "" && reordered.TrimWhitespaceAndBOM() == "")
                return; // Skip empty entry.

            // Calculate daysOut & daysIn.
            string daysOut = "";
            string daysIn = "";
            try 
            {
                if (ordered.TrimWhitespaceAndBOM() != "") 
                {
                    DateTime dt = DateTime.Parse(ordered);
                    daysOut = DateTime.Now.Subtract(dt).Days.ToString();
                }
            } 
            catch {}

            try 
            {
                if (received.TrimWhitespaceAndBOM() != "") 
                {
                    DateTime dt = DateTime.Parse(received);
                    daysIn = DateTime.Now.Subtract(dt).Days.ToString();
                }
            } 
            catch {}

            DataRow row = table.NewRow();

            row["Description"] = Utilities.SafeHtmlString(description);
            row["Ordered"] = Utilities.SafeHtmlString(ordered);
            row["Received"] = Utilities.SafeHtmlString(received);
            row["Reordered"] = Utilities.SafeHtmlString(reordered);
            row["DaysOut"] = Utilities.SafeHtmlString(daysOut);
            row["DaysIn"] = Utilities.SafeHtmlString(daysIn);
            table.Rows.Add(row);


        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}
}
