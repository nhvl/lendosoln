<%@ Page language="c#" Codebehind="LoanSummaryView.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.View.LoanSummaryView" enableViewState="False"%>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>LoanSummaryView</title>
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <style type="text/css">
TD { FONT-SIZE: 12pt; TEXT-INDENT: 2pt; FONT-FAMILY: 'Times New Roman', Arial, Helvetica, sans-serif }
.white { COLOR: #ffffff }
.FieldLabel { FONT-WEIGHT: bold }
</style>
</HEAD>
  <body MS_POSITIONING="FlowLayout">
    <script language="javascript">
    <!--
    function printMe() {
      self.focus();
      window.print();
    }
    function refreshMe() {
      self.location = self.location;
    }
    //-->
    </script>
    <form id="LoanSummaryView" method="post" runat="server">
      <div class="PaddingBottom">
        <table width="100%" border="0" cellpadding="5" cellspacing="0">
          <tr><td><font size="4"><strong>Loan Summary</strong></font></td></tr>
          <tr>
            <td bgcolor="#cccccc"><strong><font size="3">People Internally Assigned To This Loan</font></strong></td>
          </tr>
        </table>
        <table border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#cccccc" width="100%">
          <asp:Repeater id="m_peopleRepeater" runat="server">
            <itemtemplate>
              <tr bgcolor="#FFFFFF">
                <td width="33%" bgcolor="#FFFFFF">&#8226; <strong>
                    <%# AspxTools.HtmlString(Eval("Role").ToString())%>
                  </strong>-
                  <%# AspxTools.HtmlString(Eval("FullName").ToString())%>
                </td>
                <td width="33%" bgcolor="#FFFFFF"><strong> Phone</strong>:
                  <%# AspxTools.HtmlString(Eval("Phone").ToString())%>
                </td>
                <td width="33%" bgcolor="#FFFFFF"><strong> Email</strong>: <a href='mailto:<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Email"))%>'>
                    <%# AspxTools.HtmlString(Eval("Email").ToString())%>
                  </a>
                </td>
              </tr>
            </ItemTemplate>
          </asp:Repeater>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" style="MARGIN-TOP: 5px">
          <tr>
            <td bgcolor="#cccccc"><strong><font size="3">Agent List</font></strong></td>
          </tr>
        </table>
        <table border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#cccccc" width="100%">
          <asp:Repeater ID="m_agentRepeater" Runat="server">
            <itemtemplate>
              <tr bgcolor="#FFFFFF" width="100%">
                <td bgcolor="#FFFFFF"><strong><%# AspxTools.HtmlString(GetAgentType(Eval("AgentRoleT").ToString(), Eval("OtherAgentRoleTDesc").ToString())) %></strong> - 
                <%# AspxTools.HtmlString(Eval("AgentName").ToString())%></td>
                <td bgcolor="#FFFFFF"><strong>Company:</strong> <%# AspxTools.HtmlString(Eval("CompanyName").ToString())%></td>
                <td bgcolor="#FFFFFF"><strong>Phone:</strong> <%#AspxTools.HtmlString(Eval("Phone").ToString())%></td>
                <td bgcolor="#FFFFFF"><a href='mailto:<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EmailAddr"))%>'><%#AspxTools.HtmlString(Eval("EmailAddr").ToString())%></a></td>
              </tr>
            </ItemTemplate>
          </asp:Repeater>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" style="MARGIN-TOP: 5px">
          <tr><td bgcolor="#cccccc"><strong><font size="3">Borrower Information</font></strong></td></tr>
        </table>
        <asp:placeholder id="m_borrowerInformationPlaceHolder" runat="server"></asp:placeholder>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" style="MARGIN-TOP: 5px">
          <tr><td bgcolor="#cccccc"><strong><font size="3">Property Information</font></strong></td></tr>
        </table>
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr>
            <td>
              <table cellSpacing="0" cellPadding="5" border="0">
                <tr>
                  <td vAlign="top">Property Address</td>
                  <td vAlign="top">
                    <table cellSpacing="0" cellPadding="5" border="0">
                      <tr>
                        <td>
                          <b><ml:EncodedLabel id="m_propertyAddress1Label" runat="server"></ml:EncodedLabel></b>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>
                          <b><ml:EncodedLabel id="m_propertyAddress2Label" runat="server"></ml:EncodedLabel></b>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>
                          County <b><ml:EncodedLabel ID="m_propertyAddress3CountyLabel" runat="server"></ml:EncodedLabel></b>, Year built <b><ml:EncodedLabel id="m_propertyAddress3YearLabel" runat="server"></ml:EncodedLabel></b>&nbsp;
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>Sales Price</td>
                  <td><b><ml:EncodedLabel id="m_salesPriceLabel" runat="server"></ml:EncodedLabel></b></td>
                </tr>
                <tr>
                  <td>Down Payment/Equity</td>
                  <td><b><ml:EncodedLabel id="m_downPaymentLabel" runat="server"></ml:EncodedLabel></b></td>
                </tr>
                <tr>
                  <td>Appraised Value</td>
                  <td><b><ml:EncodedLabel id="m_appraisedValueLabel" runat="server"></ml:EncodedLabel></b></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" style="MARGIN-TOP: 5px">
          <tr>
            <td bgcolor="#cccccc"><strong><font size="3">Mortgage Information</font></strong></td>
          </tr>
        </table>
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr>
            <td>
              <table cellSpacing="0" cellPadding="3" border="0">
                <tr>
                  <td>Loan Number</td>
                  <td><b><ml:EncodedLabel id="m_loanNumberLabel" runat="server"></ml:EncodedLabel></b></td>
                  <td>Lien Position</td>
                  <td><b><ml:EncodedLabel id="m_lienPositionLabel" runat="server"></ml:EncodedLabel></b></td>
                </tr>
                <tr>
                  <td>Loan Amount</td>
                  <td><b><ml:EncodedLabel id="m_loanAmountLabel" runat="server"></ml:EncodedLabel></b></td>
                  <td>Loan Program</td>
                  <td><b><ml:EncodedLabel id="m_loanProgramLabel" runat="server"></ml:EncodedLabel></b></td>
                </tr>
                <tr>
                  <td>Loan Purpose</td>
                  <td><b><ml:EncodedLabel id="m_loanPurposeLabel" runat="server"></ml:EncodedLabel></b></td>
                  <td>Loan Status</td>
                  <td><b><ml:EncodedLabel id=m_loanStatusLabel runat="server"></ml:EncodedLabel></b></td>
                </tr>
                <tr>
                  <td>Note Rate</td>
                  <td><b><ml:EncodedLabel id="m_noteRateLabel" runat="server"></ml:EncodedLabel></b></td>
                  <td>Lender Case Number</td>
                  <td><b><ml:EncodedLabel id=sLenderCaseNum runat="server"></ml:EncodedLabel></b></td>
                </tr>
                <tr>
                  <td>Term (in months)</td>
                  <td><b><ml:EncodedLabel id="m_termLabel" runat="server"></ml:EncodedLabel></b></td>
                  <td>Agency Case Number</td>
                  <td><b><ml:EncodedLabel id=sAgencyCaseNum runat="server"></ml:EncodedLabel></b></td>
                </tr>
                <tr>
                  <td>Due (in months)</td>
                  <td><b><ml:EncodedLabel id="m_dueLabel" runat="server"></ml:EncodedLabel></b></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <td>Monthly Payment</td>
                  <td><b><ml:EncodedLabel id="m_monthlyLabel" runat="server"></ml:EncodedLabel></b></td>
                  <td></td>
                  <td></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" style="MARGIN-TOP: 5px">
          <tr>
            <td bgcolor="#cccccc"><strong><font size="3">Underwriting Information</font></strong></td>
          </tr>
        </table>
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr bgcolor="#ffffff">
            <td bgcolor="#ffffff"><table border="0" cellPadding="2" cellSpacing="0" id="Table5">
                <tr>
                  <td colSpan="2"><b>Qualifying Ratios</b></td>
                </tr>
                <tr>
                  <td>Housing:</td>
                  <td><span id="m_topRatioLabel1"><b><b><ml:EncodedLabel id="m_topRatioLabel" runat="server"></ml:EncodedLabel></b></b></span></td>
                </tr>
                <tr>
                  <td>Debt:</td>
                  <td><span id="m_bottomRatioLabel1"><b><b><ml:EncodedLabel id="m_bottomRatioLabel" runat="server"></ml:EncodedLabel></b></b></span></td>
                </tr>
              </table>
            </td>
            <td><table border="0" cellPadding="2" cellSpacing="0">
                <tr>
                  <td align="center" colSpan="2"><b>Loan-To-Value Ratios</b></td>
                </tr>
                <tr>
                  <td>LTV</td>
                  <td><span id="m_ltvLabel1"><b><b><ml:EncodedLabel id="m_ltvLabel" runat="server"></ml:EncodedLabel></b></b></span></td>
                </tr>
                <tr>
                  <td>Total LTV</td>
                  <td><span id="m_totalLTVLabel1"><b><b><ml:EncodedLabel id="m_totalLTVLabel" runat="server"></ml:EncodedLabel></b></b></span></td>
                </tr>
              </table>
              <center>
                <span id="m_loanNumberLabel1"></span>
              </center>
            </td>
            <td bgcolor="#ffffff"><table border="0" cellPadding="2" cellSpacing="0">
                <tr>
                  <td>Total Income :</td>
                  <td><span id="m_totalIncomeLabel1"><b><b><ml:EncodedLabel id="m_totalIncomeLabel" runat="server"></ml:EncodedLabel></b></b></span></td>
                </tr>
                <tr>
                  <td>Total Liabilities:</td>
                  <td><span id="m_totalLiabilitiesLabel1"><b><b><ml:EncodedLabel id="m_totalLiabilitiesLabel" runat="server"></ml:EncodedLabel></b></b></span></td>
                </tr>
                <tr>
                  <td>Present House Expense:&nbsp;
                  </td>
                  <td><span id="m_totalHouseExpenseLabel1"><b><b><ml:EncodedLabel id="m_totalHouseExpenseLabel" runat="server"></ml:EncodedLabel></b></b></span></td>
                </tr>
              </table>
              <center>
              </center>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="0" style="MARGIN-TOP: 5px">
          <tr>
            <td bgcolor="#cccccc"><strong><font size="3">Notes</font></strong></td>
          </tr>
        </table>
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr bgcolor="#ffffff">
            <td bgcolor="#ffffff"><b><ml:EncodedLabel id="m_notesLabel" runat="server"></ml:EncodedLabel></b></td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="3" cellspacing="0" style="MARGIN-TOP:5px">
          <tr><td bgcolor="#cccccc"><strong><font size="3">Conditions</font></strong></td></tr>
        </table>
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr bgcolor="#ffffff">
            <td>
              <asp:DataGrid id=m_conditionDG runat="server" Width="100%" AutoGenerateColumns="False" GridLines="Horizontal">
  				<headerstyle font-bold="True">
  				</HeaderStyle>
  				<columns>
  					<asp:TemplateColumn HeaderText="Done?">
  						<itemstyle horizontalalign="Center" width="40px">
  						</ItemStyle>
  						<itemtemplate>
  							<%# AspxTools.HtmlControl( DisplayIsDone(DataBinder.Eval(Container.DataItem, "IsDone")) )%>
  						</ItemTemplate>
  					</asp:TemplateColumn>
  					<asp:BoundColumn DataField="DoneDate" HeaderText="Date Done">
  						<headerstyle wrap="False">
  						</HeaderStyle>
  						<itemstyle width="70px">
  						</ItemStyle>
  					</asp:BoundColumn>
  					<asp:TemplateColumn HeaderText="Required?">
  						<itemstyle width="70px">
  						</ItemStyle>
  						<itemtemplate>
  							<%# AspxTools.HtmlControl( DisplayIsRequired(DataBinder.Eval(Container.DataItem, "IsRequired")) ) %>
  						</ItemTemplate>
  					</asp:TemplateColumn>
  					<asp:templatecolumn headertext="Prior To">
  					  <headerstyle wrap=false></headerstyle>
  					  <itemstyle wrap=False></itemstyle>
  					  <itemtemplate>
  					  <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PriorToEventDesc")) %>
  					  </itemtemplate>
  					</asp:templatecolumn>
  					<asp:templatecolumn headertext="Description">
  					  <itemstyle width="100%"></itemstyle>
  					  <itemtemplate>
  					    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "CondDesc")) %>
  					  </itemtemplate>
  					</asp:templatecolumn>
  				</Columns>
              </asp:DataGrid>
              <asp:DataGrid id="m_underwritingDG" runat="server" Width="100%" AutoGenerateColumns="False" GridLines="Horizontal" Visible="False">
  				<headerstyle font-bold="True">
  				</HeaderStyle>
  				<columns>
  					<asp:TemplateColumn HeaderText="Done?">
  						<itemstyle horizontalalign="Left" width="120px">
  						</ItemStyle>
  						<itemtemplate>
  							<%# AspxTools.HtmlControls( DisplayDoneInfo( Container.DataItem ) )%>
  						</ItemTemplate>
  					</asp:TemplateColumn>
  					<asp:templatecolumn headertext="Category">
  					  <itemstyle width="100px"></itemstyle>
  					  <itemtemplate>
  					    <%# AspxTools.HtmlString(Eval("Category").ToString()) %>
  					  </itemtemplate>
  					</asp:templatecolumn>
  					<asp:templatecolumn headertext="Condition">
  					  <itemtemplate>
  					    <%# AspxTools.HtmlString(Eval("Description").ToString())%>
  					  </itemtemplate>
  					</asp:templatecolumn>
  				</Columns>
              </asp:DataGrid>
            </td>
          </tr>
          
        </table>
        <table width="100%" border="0" cellpadding="3" cellspacing="0" style="MARGIN-TOP:5px">
          <tr><td bgcolor="#cccccc"><strong><font size="3">Status</font></strong></td></tr>
        </table>
        
        <table width="100%" cellpadding="3" cellspacing="0" border="1" bordercolor="#cccccc">
          <tr><td noWrap><strong>Lead Source</strong></td><td width="90%"><ml:EncodedLiteral id="sLeadSrcDesc" runat="server"></ml:EncodedLiteral></td></tr>
        </table>
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr><td>
          <table width="100%">
          <tr bgcolor="#ffffff">
            <td nowrap>&nbsp;</td>
            <td nowrap>&nbsp;&nbsp;</td>
            <td class="FieldLabel" nowrap>Date</td>
            <td nowrap>&nbsp;</td>
            <td class=FieldLabel width="100%">Comments</td>
          </tr>
            <asp:repeater id=m_loanStatusRepeater runat="server">
            <itemtemplate>
              <tr>
                <td nowrap><%# AspxTools.HtmlString(Eval("Description").ToString())%></td>
                <td></td>
                <td nowrap><%# AspxTools.HtmlString(Eval("Date").ToString())%></td>
                <td></td>
                <td><%# AspxTools.HtmlString(Eval("Comments").ToString())%></td>
              </tr>
            </ItemTemplate>
          </asp:repeater>     
          </table>   
          </td></tr>
        </table>
        <table width="100%" border="0" cellpadding="3" cellspacing="0" style="MARGIN-TOP:5px">
          <tr><td bgcolor="#cccccc"><strong><font size="3">Basic Documents</font></strong></td></tr>
        </table>     
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr><td>
          <table>
          <tr>
            <td noWrap></td>
            <td class=FieldLabel noWrap>Ordered</td>
            <td class=FieldLabel noWrap>Received</td>
            <td class=FieldLabel noWrap>Comments</td>
            <td class=FieldLabel noWrap>Days out</td>
            <td class=FieldLabel noWrap>Days in</td></tr><asp:repeater id=m_basicDocumentRepeater runat="server">
            <itemtemplate>
          <tr>
            <td nowrap><%# AspxTools.HtmlString(Eval("Description").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("Ordered").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("Received").ToString()) %></td>
            <td><%# AspxTools.HtmlString(Eval("Comments").ToString())%></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("DaysOut").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("DaysIn").ToString())%></td>
            </tr>        
            </ItemTemplate>
          </asp:repeater>        
          </table>
          </td></tr>
        </table> 
  <%-- Verify of Employment Information --%>      
        <table width="100%" border="0" cellpadding="3" cellspacing="0" style="MARGIN-TOP:5px">
          <tr><td bgcolor="#cccccc"><strong><font size="3">Verification of Employment</font></strong></td></tr>
        </table>     
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr><td>
          <table>
          <tr>
            <td class=FieldLabel noWrap>Employer</td>
            <td class=FieldLabel noWrap>Ordered</td>
            <td class=FieldLabel noWrap>Re-order</td>
            <td class=FieldLabel noWrap>Received</td>
            <td class=FieldLabel noWrap>Days out</td>
            <td class=FieldLabel noWrap>Days in</td></tr><asp:repeater id=m_voeRepeater runat="server">
            <itemtemplate>
          <tr>
            <td nowrap><%# AspxTools.HtmlString(Eval("Description").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("Ordered").ToString())%></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("Reordered").ToString()) %></td>
            <td><%# AspxTools.HtmlString(Eval("Received").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("DaysOut").ToString())%></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("DaysIn").ToString())%></td>
            </tr>        
            </ItemTemplate>
          </asp:repeater>
          </table>
          </td></tr>
        </table>      
  <%-- End Verify of Employment  --%>      
  <%-- Verify of Deposit Information --%>      
        <table width="100%" border="0" cellpadding="3" cellspacing="0" style="MARGIN-TOP:5px">
          <tr><td bgcolor="#cccccc"><strong><font size="3">Verification of Deposit</font></strong></td></tr>
        </table>     
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr><td>
          <table>
          <tr>
            <td class=FieldLabel noWrap>Depository</td>
            <td class=FieldLabel noWrap>Ordered</td>
            <td class=FieldLabel noWrap>Re-order</td>
            <td class=FieldLabel noWrap>Received</td>
            <td class=FieldLabel noWrap>Days out</td>
            <td class=FieldLabel noWrap>Days in</td></tr><asp:repeater id=m_vodRepeater runat="server">
            <itemtemplate>
          <tr>
            <td nowrap><%# AspxTools.HtmlString(Eval("Description").ToString())%></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("Ordered").ToString())%></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("Reordered").ToString())%></td>
            <td><%# AspxTools.HtmlString(Eval("Received").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("DaysOut").ToString())%></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("DaysIn").ToString()) %></td>
            </tr>        
            </ItemTemplate>
          </asp:repeater>        
          </table>

          </td></tr>
        </table>      
  <%-- End Verify of Deposit  --%>      
  <%-- Verify of Mortgage Information --%>      
        <table width="100%" border="0" cellpadding="3" cellspacing="0" style="MARGIN-TOP:5px">
          <tr><td bgcolor="#cccccc"><strong><font size="3">Verification of Mortgage</font></strong></td></tr>
        </table>     
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr><td>
          <table>
          <tr>
            <td class=FieldLabel noWrap>Creditor / 
            Landlord</td>
            <td class=FieldLabel noWrap>Ordered</td>
            <td class=FieldLabel noWrap>Re-order</td>
            <td class=FieldLabel noWrap>Received</td>
            <td class=FieldLabel noWrap>Days out</td>
            <td class=FieldLabel noWrap>Days in</td></tr><asp:repeater id=m_vomRepeater runat="server">
            <itemtemplate>
          <tr>
            <td nowrap><%# AspxTools.HtmlString(Eval("Description").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("Ordered").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("Reordered").ToString()) %></td>
            <td><%# AspxTools.HtmlString(Eval("Received").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("DaysOut").ToString())%></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("DaysIn").ToString())%></td>
            </tr>        
            </ItemTemplate>
          </asp:repeater>        
          </table>

          </td></tr>
        </table>      
  <%-- End Verify of Mortgage  --%>      
  <%-- Verify of Loan Information --%>      
        <table width="100%" border="0" cellpadding="3" cellspacing="0" style="MARGIN-TOP:5px">
          <tr><td bgcolor="#cccccc"><strong><font size="3">Verification of Loan</font></strong></td></tr>
        </table>     
        <table width="100%" border="1" cellpadding="3" cellspacing="0" bordercolor="#cccccc">
          <tr><td>
          <table>
          <tr>
            <td class=FieldLabel noWrap>Lender</td>
            <td class=FieldLabel noWrap>Ordered</td>
            <td class=FieldLabel noWrap>Re-order</td>
            <td class=FieldLabel noWrap>Received</td>
            <td class=FieldLabel noWrap>Days out</td>
            <td class=FieldLabel noWrap>Days in</td></tr><asp:repeater id=m_volRepeater runat="server">
            <itemtemplate>
          <tr>
            <td nowrap><%# AspxTools.HtmlString(Eval("Description").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("Ordered").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("Reordered").ToString())%></td>
            <td><%# AspxTools.HtmlString(Eval("Received").ToString()) %></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("DaysOut").ToString())%></td>
            <td nowrap><%# AspxTools.HtmlString(Eval("DaysIn").ToString())%></td>
            </tr>        
            </ItemTemplate>
          </asp:repeater>        
          </table>

          </td></tr>
        </table>      
  <%-- End Verify of Loan  --%>    
  <%-- End main table --%>      
        </TD></TR></TABLE>
      </div>
    </form>
  </body>
</HTML>
