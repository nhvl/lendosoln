<%@ Page language="c#" Codebehind="ClosingCostList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.View.ClosingCostList" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<head runat="server">
  <title>Closing Cost List</title>
</head>
<body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" scroll="yes">
    <script language="javascript">
	  <!--

		function _init()
		{
			resize(650, 540);
		}

		function onEditClick(ccID)
		{
			showModal('/los/Template/CCTemplate.aspx?ccid=' + ccID, null, null, null, function(){
        window.location = self.location;
      });
		}  

		//-->

    </script>	
    <form id="ClosingCostList" method="post" runat="server">
    <table class="FormTable" id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
      <tr>
        <td class="FormTableHeader">Closing Cost Template List</td>
      </tr>
      <tr>
        <td>
          <ml:CommonDataGrid ID="m_closingCostsDG" runat="server" DataKeyField="cCcTemplateId" OnItemCommand="OnApplyClosingCost" DefaultSortExpression="cCcTemplateNm ASC">
            <Columns>
              <asp:BoundColumn SortExpression="cCcTemplateNm" HeaderText="Closing Cost Template Name" DataField="cCcTemplateNm" />
              <asp:TemplateColumn HeaderText="Version" SortExpression="GfeVersion">
                <ItemTemplate>
                  <%# AspxTools.HtmlString(RenderGfeVersion()) %>
                </ItemTemplate>
              </asp:TemplateColumn>
              <asp:TemplateColumn>
                <ItemTemplate>
                  <asp:LinkButton ID="Apply" runat="server" Text="apply" CommandName="Apply" CommandArgument=<%# AspxTools.HtmlString(Eval("cCcTemplateId" ).ToString()) %> />
                </ItemTemplate>
              </asp:TemplateColumn>
            </Columns>
          </ml:CommonDataGrid></td>
      </tr>
      <tr><td>&nbsp;</td></tr>
      <tr>
        <td align="middle"><input type="button" value="Close" onclick="onClosePopup();"></td>
      </tr>
    </table>

    </form>
	
  </body>
</HTML>
