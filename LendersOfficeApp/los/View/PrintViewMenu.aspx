<%@ Page language="c#" Codebehind="PrintViewMenu.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.View.PrintViewMenu" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>PrintViewMenu</title>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema>
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
<body bottomMargin=0 leftMargin=0 topMargin=0 scroll=yes rightMargin=0 MS_POSITIONING="FlowLayout" bgcolor="gainsboro">
<script language=JavaScript>
	  function printView()
	  {
		  parent.body.printMe() ;
	  }
	  function refresh() 
	  {
	    parent.body.refreshMe(); 
	  }
	  function closeSelf()
	  {
		  parent.onClosePopup() ;
	  }
	  function displayCreditReport(applicationId)
	  {
    	var report_url = <%= AspxTools.JsString(DataAccess.Tools.VRoot) %> + '/newlos/services/ViewCreditFrame.aspx?loanid=' + <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("loanid")) %> + '&applicationid=' + applicationId;
		  
		  var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes") ;
		  win.focus() ;
		  //onClosePopup() ;	    
	  }
  function downloadFile(file, args) {
      var url = <%= AspxTools.JsString(DataAccess.Tools.VRoot) %> + '/pdf/' + file + '?loanid=' + <%= AspxTools.JsString(Request["loanid"]) %> + '&crack=' + new Date();
      if (typeof args !== 'undefined') {
          url += '&' + args;
      }

      LqbAsyncPdfDownloadHelper.SubmitSinglePdfDownload(url);
  }	  
    function getChecked() {	  
      return <%= AspxTools.JsString(RequestHelper.GetSafeQueryString("loanid")) %>;
    }
    </script>

<form id=PrintViewMenu method=post runat="server">
<table id=Table1 cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td>
      <ul style="margin-bottom:0px;padding-top:3px;padding-left:3px">
      <ml:PassthroughLiteral id="m_creditReportList" runat="server" EnableViewState="False"></ml:PassthroughLiteral>
      </ul>
      <table cellpadding=0 border=0>
        <tr>
        <td valign=top>
          <ul style="margin-bottom:0px">
            <li><A onclick="downloadFile('1003_10.aspx');" href="#" >1003 (4-page, Legal size printout)</A></li>
            <li><A onclick="downloadFile('1003_10Letter.aspx');" href="#" >1003 (5-page, Letter size printout)</A></li>
			<li><A onclick="downloadFile('BorrowerSignatureAuthorization.aspx');" href="#" >Borrower's Authorization Form</A></li>    
            <li><A onclick="downloadFile('BorrowerCertification.aspx');" href="#" >Borrower's Certification &amp; Authorization Form</A></li>         
          </ul>
        </td>
        <td valign=top>
          <ul>
            <li><A onclick="downloadFile('CreditDenial.aspx');" href="#" >Credit Denial Letter</A></li>      
            <li><A onclick="downloadFile('DisclosureNotices.aspx');" href="#" >Disclosure Notices</A></li>      
            <li><A onclick="downloadFile('EqualCredit.aspx');" href="#" >Equal Credit Opportunity Act</A></li>   
			<li><A onclick="downloadFile('FairLendingNotice.aspx');" href="#" >Fair Lending Notice</A></li>        
          </ul> 
          </td>
        <td valign=top>
          <ul>
            <li><A onclick="downloadFile('GoodFaithEstimate2010.aspx');" href="#" >GFE</A></li>
            <li><A onclick="downloadFile('TaskList.aspx');" href="#" >Tasks List</A></li>
            <li><A onclick="downloadFile('TruthInLending.aspx');" href="#" >Truth In Lending</A></li>            
          </ul>
        </td>
        </tr>
      </table>
</TD>
</TR>
  <tr>
    <td align=center><uc1:cmodaldlg id=CModalDlg1 runat="server"></uc1:cModalDlg><input onclick=printView(); type=button value="Print Loan Summary" NoHighlight class="ButtonStyle"> 
<input id=button2 onclick=closeSelf(); type=button value=Close name=button2 NoHighlight class="ButtonStyle"></TD>
    <td></TD></TR></TABLE></FORM>
  </body>
</HTML>
