<!DOCTYPE html> <%@ Page language="c#" Codebehind="PrintPreviewFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.View.PrintPreviewFrame" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html lang="en">
  <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <title>Loan Summary Print Preview</title>
  </head>
  <body class="body-iframe">
      <h4 class="page-header">Loan Summary Print Preview</h4>
    <form runat="server">
      <div class="top top-print-preview-frame">
        <iframe name="menu" src="PrintViewMenu.aspx?loanid=<%=AspxTools.HtmlStringFromQueryString("loanid")%>&cmd=<%=AspxTools.HtmlStringFromQueryString("cmd")%>&display=<%=AspxTools.HtmlStringFromQueryString("display")%>" scrolling="no"></iframe>
      </div>
      <div class="bottom bottom-print-preview-frame">
        <iframe name="body" src="LoanSummaryView.aspx?loanid=<%=AspxTools.HtmlStringFromQueryString("loanid")%>&cmd=<%=AspxTools.HtmlStringFromQueryString("cmd")%>&display=<%=AspxTools.HtmlStringFromQueryString("display")%>"></iframe>
      </div>
    </form>
  </body>
</html>
