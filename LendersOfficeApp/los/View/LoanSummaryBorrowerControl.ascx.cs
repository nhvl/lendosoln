using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.CreditReport;
namespace LendersOfficeApp.los.View
{

	public partial  class LoanSummaryBorrowerControl : System.Web.UI.UserControl
	{

        public string BorrowerName 
        {
            set { m_borrowerNameLabel.Text = string.Format("{0}", value); }
        }
        public string BorrowerSSN 
        {
            set { m_borrowerSSNLabel.Text = string.Format("{0}", value); }
        }
        public string BorrowerAge
        {
            set { m_borrowerAgeLabel.Text = string.Format("{0}", value); }
        }
        public string BorrowerEmail 
        {
            set { m_borrowerEmailLabel.Text = string.Format("{0}", value); }
        }
        public string BorrowerHomePhone
        {
            set { m_borrowerHomePhoneLabel.Text = string.Format("{0}", value); }
        }
        public string BorrowerCellPhone 
        {
            set { m_borrowerCellPhoneLabel.Text = string.Format("{0}", value); }
        }
        public string BorrowerWorkPhone 
        {
            set { m_borrowerWorkPhoneLabel.Text = string.Format("{0}", value); }
        }
        public CreditScore BorrowerScore 
        {
            set
            {
                m_borrowerCreditExperianLabel.Text = value.Experian.ToString();
                m_borrowerCreditEquifaxLabel.Text = value.Equifax.ToString();
                m_borrowerCreditTransUnionLabel.Text = value.TransUnion.ToString();
            }
        }
        public bool EnableBorrowerScore
        {
            set
            {
                if (value)
                {
                    m_borrowerCreditDiv.Attributes.Remove("style");
                }
            }
        }
        public string BorrowerFax 
        {
            set { m_borrowerFaxLabel.Text = string.Format("{0}", value); }
        }
        public string CoborrowerFax 
        {
            set { m_coborrowerFaxLabel.Text = string.Format("{0}", value); }
        }

        public E_aBMaritalStatT BorrowerMaritalStatus 
        {
            set 
            {
                switch (value) 
                {
                    case E_aBMaritalStatT.Married:
                        m_borrowerMaritalStatusLabel.Text = "Married"; break;
                    case E_aBMaritalStatT.NotMarried:
                        m_borrowerMaritalStatusLabel.Text = "Single"; break;
                    case E_aBMaritalStatT.Separated:
                        m_borrowerMaritalStatusLabel.Text = "Separated"; break;
                    case E_aBMaritalStatT.LeaveBlank:
                        m_borrowerMaritalStatusLabel.Text = ""; break;
                }
            }
        }
        public string CoborrowerName 
        {
            set { m_coborrowerNameLabel.Text = String.Format("{0}", value); }
        }
        public string CoborrowerSSN 
        {
            set { m_coborrowerSSNLabel.Text = String.Format("{0}", value); }
        }
        public string CoborrowerAge
        {
            set { m_coborrowerAgeLabel.Text = String.Format("{0}", value); }
        }
        public string CoborrowerEmail 
        {
            set { m_coborrowerEmailLabel.Text = String.Format("{0}", value); }
        }
        public string CoborrowerHomePhone
        {
            set { m_coborrowerHomePhoneLabel.Text = String.Format("{0}", value); }
        }
        public string CoborrowerCellPhone 
        {
            set { m_coborrowerCellPhoneLabel.Text = string.Format("{0}", value); }
        }
        public string CoborrowerWorkPhone 
        {
            set { m_coborrowerWorkPhoneLabel.Text = string.Format("{0}", value); }
        }
        public CreditScore CoborrowerScore 
        {
            set
            {
                m_coborrowerCreditExperianLabel.Text = value.Experian.ToString();
                m_coborrowerCreditEquifaxLabel.Text = value.Equifax.ToString();
                m_coborrowerCreditTransUnionLabel.Text = value.TransUnion.ToString();
            }
        }
        public bool EnableCoborrowerScore
        {
            set
            {
                if (value)
                {
                    m_coborrowerCreditDiv.Attributes.Remove("style");
                }
            }
        }

        public E_aBMaritalStatT CoborrowerMaritalStatus 
        {
            set 
            {
                switch (value) 
                {
                    case E_aBMaritalStatT.Married:
                        m_coborrowerMaritalStatusLabel.Text = "Married"; break;
                    case E_aBMaritalStatT.NotMarried:
                        m_coborrowerMaritalStatusLabel.Text = "Single"; break;
                    case E_aBMaritalStatT.Separated:
                        m_coborrowerMaritalStatusLabel.Text = "Separated"; break;
					case E_aBMaritalStatT.LeaveBlank:
						m_coborrowerMaritalStatusLabel.Text = ""; break;
                }
            }
        }
        public string BorrowerPresentAddress 
        {
            set { m_borrowerPresentAddressLabel.Text = String.Format("{0}", value); }
        }
        public string BorrowerPresentAddress1 
        {
            set { m_borrowerPresentAddress1Label.Text = String.Format("{0}", value); }
        }
        public E_aBAddrT BorrowerPresentAddressStatus 
        {
            set 
            {
                switch (value) 
                {
                    case E_aBAddrT.Own:
                        m_borrowerPresentStatusLabel.Text = "Own"; break;
                    case E_aBAddrT.Rent:
                        m_borrowerPresentStatusLabel.Text = "Rent"; break;
                }
            }
        }
        public string BorrowerPresentAddressLength 
        {
            set { m_borrowerPresentLengthLabel.Text = String.Format("{0}", value); }
        }

        public string CoborrowerPresentAddress 
        {
            set { m_coborrowerPresentAddressLabel.Text = String.Format("{0}", value); }
        }
        public string CoborrowerPresentAddress1 
        {
            set { m_coborrowerPresentAddress1Label.Text = String.Format("{0}", value); }
        }
        public E_aBAddrT CoborrowerPresentAddressStatus 
        {
            set 
            {
                switch (value) 
                {
                    case E_aBAddrT.Own:
                        m_coborrowerPresentStatusLabel.Text = "Own"; break;
                    case E_aBAddrT.Rent:
                        m_coborrowerPresentStatusLabel.Text = "Rent"; break;
                }
            }
        }
        public string CoborrowerPresentAddressLength 
        {
            set { m_coborrowerPresentLengthLabel.Text = String.Format("{0}", value); }
        }
		protected void PageLoad(object sender, System.EventArgs e)
		{
            // Put user code to initialize the page here
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
