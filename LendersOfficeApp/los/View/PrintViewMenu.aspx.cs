using System;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;

using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.View
{
	public partial class PrintViewMenu : LendersOffice.Common.BasePage
	{


		protected void PageLoad(object sender, System.EventArgs e)
		{
            var manager = new LendersOffice.Pdf.Async.AsyncPdfDownloadDependencyManager();
            manager.RegisterDependencies(this);

            ArrayList list = new ArrayList();

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(RequestHelper.LoanID, typeof(PrintViewMenu));
            dataLoan.InitLoad();

            int nApps = dataLoan.nApps;
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                SqlParameter[] parameters = {
                                                new SqlParameter("@ApplicationID", dataApp.aAppId)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(dataLoan.sBrokerId, "RetrieveCreditReport", parameters)) 
                {
                    if (reader.Read()) 
                    {
                        sb.AppendFormat(@"<li><a href='#' onclick='displayCreditReport(""{0}"");'>", dataApp.aAppId);
                        sb.AppendFormat(@"Credit Report of {0}</a>", Utilities.SafeHtmlString(dataApp.aBNm));
                    }
                }
            }

            m_creditReportList.Text = sb.ToString();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
