using System;
using DataAccess;
using DataAccess.Utilities;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOfficeApp.los.View
{
    public partial class ClosingCostList : BasePage
    {
        private Guid m_brokerID => BrokerUserPrincipal.CurrentPrincipal.BrokerId;

        protected void PageLoad(object sender, System.EventArgs e)
        {
            // Bind with each postback so sorting works.
            RegisterJsScript("LQBPopup.js");	
            this.EnableJqueryMigrate = false;
			BindDataGrid();
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);

        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
        }
		#endregion

		private void BindDataGrid()
        {
            // 11/3/2009 dd - Add support for GfeVersion. If GfeVersion is pass in through query string then
            // only retrieve that specific gfeversion. Otherwise retrieve everything.

            int version = RequestHelper.GetInt("GfeVersion", -1);
            var ds = CCcTemplateData.GetClosingCostDataSet(m_brokerID, version);
            m_closingCostsDG.DataSource = ds.Tables[0].DefaultView;
            m_closingCostsDG.DataBind();
        }
        protected string RenderGfeVersion()
        {
            int version = (int)Eval("GfeVersion");
            switch (version)
            {
                case 0:
                    return "GFE 2009";
                case 1:
                    return "GFE 2010";
                default:
                    throw new GenericUserErrorMessageException("Unhandle GfeVersion=" + version);
            }
        }
		protected void OnApplyClosingCost( object sender , System.Web.UI.WebControls.DataGridCommandEventArgs a )
		{
			// Check arguments.  Only handle apply case.
		    if (a.CommandName == "Apply" && a.CommandArgument != null)
		    {
		        var ccID = new Guid(a.CommandArgument.ToString());
		        ApplyClosingCost(ccID);
		    }
		}

        void ApplyClosingCost(Guid ccTemplateId)
        {
            try
            {
                ClosingCostTemplate.ApplyClosingCostTemplate(RequestHelper.LoanID, ccTemplateId);
                LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new[] { "OK=true" });
            }
            catch (PermissionException pe)
            {
                Response.Redirect(Page.ResolveClientUrl("~/newlos/LoanSaveDenial.aspx") + "?msg=" + Server.UrlEncode(pe.UserMessage));
            }
            catch (LoanFieldWritePermissionDenied e)
            {
                Response.Redirect(Page.ResolveClientUrl("~/newlos/LoanSaveDenial.aspx") + "?msg=" + Server.UrlEncode(e.UserMessage));
            }
        }
    }
}
