using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using DataAccess;

using LendersOffice.Common;

namespace LendersOfficeApp.los
{
	/// <summary>
	/// Summary description for PrintGroupList.
	/// </summary>

	public partial class PrintGroupList : LendersOffice.Common.BasePage
	{

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
            ClientScript.RegisterHiddenField("InitialID", "");
            Guid brokerID = ((LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal)).BrokerId;
            SqlParameter[] parameters = { new SqlParameter("@BrokerID", brokerID) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "ListPrintGroupByBrokerID", parameters)) 
            {
                m_printGroupRepeater.DataSource = reader;
                m_printGroupRepeater.DataBind();
            }

			if( BrokerUser.HasPermission( Permission.CanEditPrintGroups ) == true )
			{
				m_addNewGroupPanel.Enabled = true;
				m_addNewGroupPanel.ToolTip = "";
			}
			else
			{
				m_addNewGroupPanel.Enabled = false;
				m_addNewGroupPanel.ToolTip = ErrorMessages.InsufficientPermissionToEditPrintGroups;
			}
		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterVbsScript("common.vbs");
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion

	}

}
