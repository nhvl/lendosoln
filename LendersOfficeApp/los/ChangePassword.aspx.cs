using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Admin;
using LendersOfficeApp.common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;


namespace LendersOfficeApp.los
{
	/// <summary>
	/// Summary description for ChangePassword.
	/// </summary>
	public partial class ChangePassword : LendersOffice.Common.BasePage
	{

        private EmployeeDB m_employee;
    
		protected void PageLoad(object sender, System.EventArgs e)
		{
            
            if (Page.IsPostBack) 
            {
                // Retrieve employee information to use on postback.
                BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal; //(BrokerUserPrincipal) Page.User;
                m_employee = new EmployeeDB(principal.EmployeeId, principal.BrokerId);
                m_employee.Retrieve();
            } 
		}

        private void UpdatePassword() 
        {
            m_employee.Password = m_newPasswordTB.Text;
            // Reset password expiration date after user changed password.
            m_employee.ResetPasswordExpirationD();
            m_employee.Save(PrincipalFactory.CurrentPrincipal);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

        protected void m_submitBtn_Click(object sender, System.EventArgs e)
        {
            // Make sure all validation occurs.
            if (Page.IsValid) 
            {
                UpdatePassword();
                RequestHelper.DoNextPostLoginTask(PostLoginTask.ChangePassword);
            }
        }

        protected void m_compareOldPasswordValidator_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            args.IsValid = true;
            if (!m_employee.IsCurrentPassword(m_oldPasswordTB.Text)) 
            {
                m_compareOldPasswordValidator.Text = "Old password does not match.";
                args.IsValid = false;
            }
            if (m_newPasswordTB.Text.ToLower() == m_oldPasswordTB.Text.ToLower()) 
            {
                m_compareOldPasswordValidator.Text = "New password cannot be the same as old password.";
                args.IsValid = false;
            }

            StrongPasswordStatus ret = m_employee.IsStrongPassword(m_newPasswordTB.Text);
            if (ret != StrongPasswordStatus.OK)
            {
                m_compareOldPasswordValidator.Text = Utilities.SafeHtmlString(EmployeeDB.GetStrongPasswordUserMessage(ret));
                args.IsValid = false;
            }            
        }
	}
}
