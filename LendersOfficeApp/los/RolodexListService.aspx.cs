﻿namespace LendersOfficeApp.los
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    public partial class RolodexListService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "AddAgent":
                    AddAgent();
                    break;
            }
        }

        private void AddAgent()
        {
            Guid LoanID = GetGuid("loanid");

            CPageData dataLoan = new CPeopleData(LoanID);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields field = dataLoan.GetAgentFields(Guid.Empty);   // It's a new agent. Just pass in Guid.Empty, and let the code handle the rest.

            var brokerLevelAgentID = GetGuid("ContactInfo_BrokerLevelAgentID", Guid.Empty);
            if (GetBool("ContactInfo_ShouldMatchBrokerContact", false))
            {
                field.PopulateFromRolodex(brokerLevelAgentID, BrokerUserPrincipal.CurrentPrincipal);
            }
            field.BrokerLevelAgentID = brokerLevelAgentID;
            field.AgentSourceT = (E_AgentSourceT)GetInt("ContactInfo_AgentSourceT", 0); // order here is important.

            field.AgentName = GetString("ContactInfo_AgentName");
            field.AgentRoleT = (E_AgentRoleT)GetInt("ContactInfo_AgentRoleT");
            field.CaseNum = GetString("ContactInfo_CaseNum");
            field.CellPhone = GetString("ContactInfo_CellPhone");
            field.City = GetString("ContactInfo_City");
            field.CompanyName = GetString("ContactInfo_CompanyName");
            field.DepartmentName = GetString("ContactInfo_DepartmentName");
            field.EmailAddr = GetString("ContactInfo_EmailAddr");
            field.FaxNum = GetString("ContactInfo_FaxNum");
            field.Notes = GetString("ContactInfo_Notes");
            field.EmployeeIDInCompany = GetString("ContactInfo_EmployeeIDInCompany");
            field.LicenseNumOfAgent = GetString("ContactInfo_LicenseNum");
            field.LicenseNumOfCompany = GetString("ContactInfo_LicenseNumOfCompany");
            field.PagerNum = GetString("ContactInfo_PagerNum");
            field.Phone = GetString("ContactInfo_Phone");
            field.State = GetString("ContactInfo_State");
            field.StreetAddr = GetString("ContactInfo_StreetAddr");
            field.Zip = GetString("ContactInfo_Zip");
            field.County = GetString("ContactInfo_County");
            field.OtherAgentRoleTDesc = GetString("ContactInfo_OtherAgentRoleTDesc");
            field.InvestorBasisPoints_rep = GetString("ContactInfo_InvestorBasisPoints");
            field.InvestorSoldDate_rep = GetString("ContactInfo_InvestorSoldDate");
            field.TaxId = GetString("ContactInfo_TaxID");
            field.ChumsId = GetString("ContactInfo_ChumsID");
            field.IsLenderAssociation = GetBool("ContactInfo_IsLenderAssociation");

            if (dataLoan.BrokerDB.IsEnableGfe2015 && dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
            {
                field.IsLenderAffiliate = GetBool("ContactInfo_IsLenderAffiliate");
            }
            else
            {
                field.IsLenderAffiliate = GetBool("ContactInfo_IsLenderAffiliate_Relation");
            }

            field.OverrideLicenses = GetBool("ContactInfo_OverrideLicenses");

            field.IsLenderRelative = GetBool("ContactInfo_IsLenderRelative");
            field.HasLenderRelationship = GetBool("ContactInfo_HasLenderRelationship");
            field.HasLenderAccountLast12Months = GetBool("ContactInfo_HasLenderAccountLast12Months");
            field.IsUsedRepeatlyByLenderLast12Months = GetBool("ContactInfo_IsUsedRepeatlyByLenderLast12Months");
            field.IsApplyToFormsUponSave = GetBool("IsApplyToFormsUponSave");
            field.IsListedInGFEProviderForm = GetBool("ContactInfo_IsListedInGFEProviderForm");
            field.ProviderItemNumber = GetString("ContactInfo_ProviderItemNumber");
            field.PhoneOfCompany = GetString("ContactInfo_PhoneOfCompany");
            field.FaxOfCompany = GetString("ContactInfo_FaxOfCompany");
            field.IsNotifyWhenLoanStatusChange = GetBool("ContactInfo_IsNotifyWhenLoanStatusChange");
            field.CompanyLoanOriginatorIdentifier = GetString("ContactInfo_CompanyLoanOriginatorIdentifier");
            field.LoanOriginatorIdentifier = GetString("ContactInfo_LoanOriginatorIdentifier");
            field.EmployeeId = GetGuid("ContactInfo_EmployeeId", Guid.Empty);
            field.IsLender = GetBool("ContactInfo_IsLender");
            field.IsOriginator = GetBool("ContactInfo_IsOriginator");
            field.IsOriginatorAffiliate = GetBool("ContactInfo_IsOriginatorAffiliate");

            //av opm 48610  Agents record: Hide the commission information unless user has Finance read permission Just in case the user was JUST given the permission (while looking at the page)
            //dont try to save the values if the strings are not there there. If one of them is there the rest should be. 
            if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowAccountantRead) && GetString("CommissionMinBase", null) != null)
            {
                bool bCopyCommissionToAgent = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(field.EmployeeId, LoanID) == E_AgentRoleT.Other;
                if (bCopyCommissionToAgent)
                {
                    field.CommissionMinBase_rep = GetString("ContactInfo_CommissionMinBase");
                    field.CommissionPointOfLoanAmount_rep = GetString("ContactInfo_CommissionPointOfLoanAmount");
                    field.CommissionPointOfGrossProfit_rep = GetString("ContactInfo_CommissionPointOfGrossProfit");
                }
                else
                {
                    //else it retains the old values
                    field.CommissionMinBase = 0;
                    field.CommissionPointOfLoanAmount = 0;
                    field.CommissionPointOfGrossProfit = 0;
                }
            }

            // OPM 109299
            field.BranchName = GetString("ContactInfo_BranchName");
            field.PayToBankName = GetString("ContactInfo_PayToBankName");
            field.PayToBankCityState = GetString("ContactInfo_PayToBankCityState");
            field.PayToABANumber = GetString("ContactInfo_PayToABANumber");
            field.PayToAccountNumber = GetString("ContactInfo_PayToAccountNumber");
            field.PayToAccountName = GetString("ContactInfo_PayToAccountName");
            field.FurtherCreditToAccountNumber = GetString("ContactInfo_FurtherCreditToAccountNumber");
            field.FurtherCreditToAccountName = GetString("ContactInfo_FurtherCreditToAccountName");

            field.Update(false);

            SetResult("RecordId", field.RecordId.ToString());
            SetResult("AgentType", field.AgentRoleT);
            
            dataLoan.Save();
        }
    }
}
