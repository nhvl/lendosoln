///
/// Author: David Dao
/// 
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.DistributeUnderwriting;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.RatePrice;
using LendersOffice.Admin;

namespace LendersOfficeApp.los
{
	public partial class LoanFindService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "TakeResultSnapshot":
                    TakeResultSnapshot();
                    break;
            }
        }

        private void TakeResultSnapshot()
        {
            int count = GetInt("Count");
            ArrayList loanIds = new ArrayList( count );
            for (int i = 0; i < count; i++) 
            {
                Guid sLId = GetGuid("LoanID_" + i);
                loanIds.Add ( sLId );
            }

            Guid []arrayLoanIds = (Guid [])loanIds.ToArray( typeof( Guid ) );
            
            XmlDocument doc = TakeResultSnapshot( arrayLoanIds );

            string key = AutoExpiredTextCache.AddToCache(doc.OuterXml, TimeSpan.FromMinutes(5));

            SetResult("Key", key);


        }


        // ThienNguyen : will use this method for "unit test".
        static public XmlDocument TakeResultSnapshot(Guid []loanIds) 
        {
            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            long totalExecutionTicks = 0L;

            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("Snapshot");
            root.SetAttribute("Timestamp", DateTime.Now.ToString());
            root.SetAttribute("LoginName", principal.LoginNm);


            int count = loanIds.Length; // GetInt("Count");
            
            for (int i = 0; i < count; i++) 
            {
                Guid sLId = loanIds[i]; // GetGuid("LoanID_" + i);

                CPageData dataLoan = new CLpeSnapshotData(sLId);
                dataLoan.InitLoad();

                Guid sProdLpePriceGroupId = dataLoan.sProdLpePriceGroupId;

                LoanProgramByInvestorSet lpSet = RetrieveProductList(principal.BrokerId, sProdLpePriceGroupId);

                XmlElement loanEl = doc.CreateElement("Loan");
                root.AppendChild(loanEl);
                loanEl.SetAttribute("sLId", sLId.ToString());
                loanEl.SetAttribute("sLNm", dataLoan.sLNm);

                XmlElement resultEl = doc.CreateElement("Results");
                loanEl.AppendChild(resultEl);


                try 
                {

                    doc.AppendChild(root);
                    long start = DateTime.Now.Ticks;
                    var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
                    Guid requestId = DistributeUnderwritingEngine.SubmitToEngine(principal, sLId, lpSet, E_sLienQualifyModeT.ThisLoan, sProdLpePriceGroupId, "", E_RenderResultModeT.Regular, E_sPricingModeT.RegularPricing, Guid.Empty, 0, 0, 0, 0,
                        options);
                    UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestId);
                    long end = DateTime.Now.Ticks;
                    long loanExecutionTicks = end - start;
                    totalExecutionTicks += loanExecutionTicks;

                    if (resultItem.IsErrorMessage) 
                    {
                        resultEl.InnerText = resultItem.ErrorMessage;

                    } 
                    else 
                    {
                        ArrayList results = resultItem.Results;

                        if (null == results)
                            throw new CBaseException(ErrorMessages.Generic, "DistributeUnderwritingEngine.RetrieveResults return null for " + requestId);

                        foreach (object o in results) 
                        {

                            CApplicantPriceXml product = o as CApplicantPriceXml;

                            if (null == product) 
                            {
                                continue;
                            }

                            XmlElement el = CApplicantPriceXml.ToXmlElement(product, doc);
                            resultEl.AppendChild(el);
                        }
                    }

                    loanEl.SetAttribute("ExecutionTime", DisplayTime(loanExecutionTicks));


                } 
                catch (Exception exc) 
                {
                    resultEl.InnerText = exc.ToString();
                }
            }

            root.SetAttribute("ExecutionTime", DisplayTime(totalExecutionTicks));

            return doc;

        }

        static private string DisplayTime(long ticks) 
        {
            TimeSpan ts = new TimeSpan(ticks);
            return string.Format("[{0} hr {1} min {2} sec {3} ms] or [{4} ms.]", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds, ts.TotalMilliseconds);
        }

        static private LoanProgramByInvestorSet RetrieveProductList(Guid brokerID, Guid lpePriceGroupId) 
        {
            LoanProgramFilterParameters parameters = new LoanProgramFilterParameters()
            {
                Due10Years = true,
                Due15Years = true,
                Due20Years = true,
                Due25Years = true,
                Due30Years = true,
                DueOther = true,
                FinMethFixed = true,
                FinMeth3YearsArm = true,
                FinMeth5YearsArm = true,
                FinMeth7YearsArm = true,
                FinMeth10YearsArm = true,
                FinMethOther = true,
                PaymentTypePI = true,
                PaymentTypeIOnly = true,
                CanOnlyBeStandalone2nd = false,
                MatchingProductType = string.Empty,
                Include10And25YearTermsInOtherDue = false,
                ProductTypesToIgnore = null,
                LienOption = CLoanProgramsFromDb.E_Option.Only1stLien
            };

            var broker = BrokerDB.RetrieveById(brokerID);
            return CLoanProgramsFromDb.ListByBrokerIdToRunLpe(broker, lpePriceGroupId, Guid.Empty, parameters);

            //return CLoanProgramsFromDb.ListByBrokerIdToRunLpe(brokerID, CLoanProgramsFromDb.E_Option.Only1stLien, 
            //           lpePriceGroupId,
            //           true /* sProdFilterDue10Yrs*/, true /* sProdFilterDue15Yrs*/, true /* sProdFilterDue20Yrs*/, true /* sProdFilterDue25Yrs*/, true /* sProdFilterDue30Yrs */, true /* sProdFilterDueOther */, 
            //           true /* sProdFilterFinMethFixed */, true /* sProdFilterFinMeth3YrsArm */, true /*sProdFilterFinMeth5YrsArm*/, true /*sProdFilterFinMeth7YrsArm*/, true /*sProdFilterFinMeth10YrsArm*/,
            //           true /*sProdFilterFinMethOther */, false /* filterOnlyCanBeStandAlone2nd */, "", Guid.Empty, null, false);
        }
	}
}
