﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Web.Services;
using System.Web.Script.Services;

using LendersOffice.ConfigSystem;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.AntiXss;
using LendersOffice.Constants;
using LendersOffice.Audit;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOfficeApp.los
{
    public partial class PipelineService : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {            

        }

        private static List<Task> GetTasks(string sTaskIdsCsv)
        {
            List<Task> tasks = new List<Task>();
            foreach (string taskId in sTaskIdsCsv.Split(','))
            {
                tasks.Add(Task.Retrieve(BrokerUserPrincipal.CurrentPrincipal.BrokerId, taskId));
            }
            return tasks;
        }

        private static string PerformTaskOperations(string sTaskIdsCsv, Action<Task> Op)
        {
            List<string> failedTaskIds = new List<string>();

            List<Guid> uniqueLoanIdList = new List<Guid>();
            Guid brokerId = Guid.Empty;
            foreach (Task task in GetTasks(sTaskIdsCsv))
            {
                try
                {
                    if (brokerId == Guid.Empty)
                    {
                        brokerId = task.BrokerId;
                    }
                    if (uniqueLoanIdList.Contains(task.LoanId) == false)
                    {
                        uniqueLoanIdList.Add(task.LoanId);
                    }
                    Op(task);
                    task.Save(false);
                }
                catch (CBaseException)
                {
                    failedTaskIds.Add(task.TaskId);
                }
            }

            foreach (Guid sLId in uniqueLoanIdList)
            {
                TaskUtilities.EnqueueTasksDueDateUpdate(brokerId, sLId);
            }
            if (failedTaskIds.Count > 0)
            { 
                return "Operation failed for tasks " + string.Join(",", failedTaskIds.ToArray());
                //throw new CBaseException(sMessage, sMessage);
            }
            return string.Empty;
        }



        [WebMethod]
        public static object SetFollowupDate(string taskIds, DateTime date)
        {
            return Tools.LogAndThrowIfErrors<object>(() =>
                PerformTaskOperations(
                    taskIds,
                    (Task task) =>
                    {
                        task.TaskFollowUpDate = date;
                    }
                ));
        }

        [WebMethod]
        public static object SetDueDate(string taskIds, string dateString, string fieldId, int offset)
        {
            return Tools.LogAndThrowIfErrors<object>(() => 
                PerformTaskOperations(
                    taskIds,
                    (Task task) =>
                    {
                        task.TaskDueDateCalcDays = offset;
                        task.TaskDueDateCalcField = fieldId;
                        task.TaskDueDate_rep = dateString;
                    }
                ));
        }

        [WebMethod]
        public static object TakeTaskOwnership(string taskIds)
        {
            return Tools.LogAndThrowIfErrors<object>(() =>
                PerformTaskOperations(
                    taskIds,
                    (Task task) =>
                    {
                        task.TaskOwnerUserId = BrokerUserPrincipal.CurrentPrincipal.UserId;
                    }
                ));
        }


        [WebMethod]
        public static object SubscribeToTasks(string taskIds)
        {
            return Tools.LogAndThrowIfErrors<object>(() =>
                PerformTaskOperations(
                    taskIds,
                    (Task task) =>
                    {
                        TaskSubscription.Subscribe(BrokerUserPrincipal.CurrentPrincipal.BrokerId, task.TaskId, BrokerUserPrincipal.CurrentPrincipal.UserId);
                    }
                ));
        }

        [WebMethod]
        public static object UnsubscribeTasks(string taskIds)
        {
            return Tools.LogAndThrowIfErrors<object>(() =>
                PerformTaskOperations(
                    taskIds,
                    (Task task) =>
                    {
                        TaskSubscription.Unsubscribe(BrokerUserPrincipal.CurrentPrincipal.BrokerId, task.TaskId, BrokerUserPrincipal.CurrentPrincipal.UserId);
                    }
                ));
        }

        [WebMethod]
        public static object AssignTasks(string taskIds, Guid id, string type)
        {
            return Tools.LogAndThrowIfErrors<object>(() =>
                AssignTasksHelper(taskIds, id, type));
        }

        public static object AssignTasksHelper(string taskIds, Guid id, string type)
        {
            foreach (Task task in GetTasks(taskIds))
            {
                if (task.TaskStatus == E_TaskStatus.Resolved)
                {
                    string sMsg = "Resolved tasks may not be re-assigned.  Re-activate any resolved tasks that you wish to re-assign and try again.";
                    return sMsg;
                }
            }

            return PerformTaskOperations(
                taskIds,
                (Task task) =>
                {
                    if (type == "user")
                    {
                        task.IsBatchUpdate = true;
                        task.TaskAssignedUserId = id;
                        task.TaskToBeAssignedRoleId = Guid.Empty;
                    }
                    else
                    {
                        task.TaskAssignedUserId = Guid.Empty;
                        task.TaskToBeAssignedRoleId = id;
                    }
                });
        }
        [WebMethod]
        public static object GetLoanOptions(Guid loanId)
        {
            return Tools.LogAndThrowIfErrors<object>(() =>
                GetLoanOptionsHelper(loanId));
        }

        private static object GetLoanOptionsHelper(Guid loanId)
        {
            StringWriter stringWriter = new StringWriter();

            LoanValueEvaluator valueEvaluator;
            valueEvaluator = new LoanValueEvaluator(
                BrokerUserPrincipal.CurrentPrincipal.ConnectionInfo,
                BrokerUserPrincipal.CurrentPrincipal.BrokerId
                , loanId
                , WorkflowOperations.ReadLoanOrTemplate
                , WorkflowOperations.WriteLoanOrTemplate
                , WorkflowOperations.WriteField
                );
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerUserPrincipal.CurrentPrincipal));

            bool hasFieldWritePermission = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteField, valueEvaluator);

            bool canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);
            bool canEdit = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator) || hasFieldWritePermission;

            using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter))
            {
                // OPM 124901
                // We needs loan data to tell if these links should be displayed.
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(PipelineService));
                dataLoan.InitLoad();
                dataLoan.ByPassFieldSecurityCheck = true;


                // Edit link
                //if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.CanViewLoanInEditor) && canEdit)
                if (canEdit || canRead)
                {

                    //Make edit link always visible if not lead as there can be field level write privilege. So edit link is not necessary
                    bool isLead = Tools.IsStatusLead(dataLoan.sStatusT);

                    HyperLink editLink = new HyperLink();
                    editLink.Text = "edit";
                    if (isLead)
                    {
                        editLink.Attributes.Add("onclick", "editLead(" + AspxTools.JsString(loanId) + "); return false;");
                    }
                    else
                    {
                        editLink.Attributes.Add("onclick", "editLoan(" + AspxTools.JsString(loanId) + "); return false;");
                    }

                    editLink.Attributes.Add("href", "#"); writer.Write("</br>");
                    editLink.RenderControl(writer);

                    //Show view link
                    HyperLink viewSummaryLink = new HyperLink();
                    viewSummaryLink.Text = "view";
                    viewSummaryLink.Attributes.Add("onclick", "viewLoan(" + AspxTools.JsString(loanId) + "); return false;");
                    viewSummaryLink.Attributes.Add("href", "#"); writer.Write("</br>");
                    viewSummaryLink.RenderControl(writer);
                }

                HyperLink insertTaskLink = new HyperLink();
                insertTaskLink.Text = "new task";
                insertTaskLink.Attributes.Add("onclick", "createTask(" + AspxTools.JsString(loanId) + "); return false;");
                insertTaskLink.Attributes.Add("href", "#"); writer.Write("</br>");
                insertTaskLink.RenderControl(writer);

                if (dataLoan.sIsRateLockExtentionAllowed)
                {
                    HyperLink extendLink = new HyperLink();
                    extendLink.Text = "extend lock";
                    extendLink.Attributes.Add("onclick", "rateLock(" + AspxTools.JsString(loanId) + ", 'extend'); return false;");
                    extendLink.Attributes.Add("href", "#"); writer.Write("</br>");
                    extendLink.RenderControl(writer);
                }

                if (dataLoan.sIsRateReLockAllowed)
                {
                    HyperLink relockLink = new HyperLink();
                    relockLink.Text = "re-lock rate";
                    relockLink.Attributes.Add("onclick", "rateLock(" + AspxTools.JsString(loanId) + ", 'relock'); return false;");
                    relockLink.Attributes.Add("href", "#"); writer.Write("</br>");
                    relockLink.RenderControl(writer);
                }
                if (dataLoan.sIsRateLockFloatDownAllowed)
                {
                    HyperLink floatDownLink = new HyperLink();
                    floatDownLink.Text = "float down lock";
                    floatDownLink.Attributes.Add("onclick", "rateLock(" + AspxTools.JsString(loanId) + ", 'floatdown'); return false;");
                    floatDownLink.Attributes.Add("href", "#"); writer.Write("</br>");
                    floatDownLink.RenderControl(writer);
                }

            }
            string ret = stringWriter.ToString();
            return ret;
        }

        public static Pair GetTasksCount(bool bGetAssignedToAnybody)
        {
            return Tools.LogAndThrowIfErrors<Pair>(() =>
                GetTasksCountHelper(bGetAssignedToAnybody));
        }

        private static Pair GetTasksCountHelper(bool bGetAssignedToAnybody)
        {
            List<Task> tasks = Task.GetOpenTasksByAssignedUserId(bGetAssignedToAnybody ? Guid.Empty : BrokerUserPrincipal.CurrentPrincipal.UserId, BrokerUserPrincipal.CurrentPrincipal.BrokerId, "TaskDueDate", string.Empty);
            int nTasks = 0;
            int nDueTasks = 0;
            foreach (Task task in tasks)
            {
                nTasks++;
                if (task.TaskDueDate <= DateTime.Today)
                {
                    nDueTasks++;
                }
            }
            return new Pair(nTasks, nDueTasks);
        }

        [WebMethod]
        public static void SetCookie(string name, string value)
        {
            Tools.LogAndThrowIfErrors(() => RequestHelper.StoreToCookie(name, value, DateTime.MaxValue));
        }

        [WebMethod]
        public static int GetResolvedOrClosedTaskCount()
        {
            return Tools.LogAndThrowIfErrors<int>(() =>
                    TaskUtilities.CountRecentlyResolvedTasks(BrokerUserPrincipal.CurrentPrincipal, TaskUtilities.E_TaskConditionOption.IncludeBoth));
        }

        [WebMethod]
        public static void MarkLoanDisclosed(Guid sLId)
        {
            Tools.LogAndThrowIfErrors(() => ModifyDisclosure(sLId, false));
        }

        // OPM 173000, 8/13/2015, ML
        [WebMethod]
        public static void ClearLoanDisclosure(Guid sLId)
        {
            Tools.LogAndThrowIfErrors(() => ModifyDisclosure(sLId, true));
        }

        // OPM 173000, 8/13/2015, ML
        private static void ModifyDisclosure(Guid sLId, bool justClearDisclosure)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(PipelineService));

            //OPM 138391: Allow users with read-only access to mark disclosed
            // 12/9/2013 gf - opm 145170 Skip version check since we're no longer including file version.
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (justClearDisclosure)
            {
                dataLoan.ClearDisclosureWithoutMarking();
            }
            else
            {
                dataLoan.ProcessManualDisclosureTrigger();
            }

            dataLoan.Save();
        }
    }
}