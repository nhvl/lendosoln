<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="PrintGroupList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.PrintGroupList" enableViewState="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>PrintGroupList</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" class="LeftBackground PaddingTopBottom5" style="MARGIN-LEFT:5px">
	<script language=javascript>
  <!--
  var oPreviousLinkID = null;
  
  function highlightLink(id) {
      var link = document.getElementById(id);
      if (null != link) {
        link.style.backgroundColor = 'yellow';
        link.style.color = 'black';  
      }  
  }
  function unhighlightLink(id) {
      var link = document.getElementById(id);
      if (null != link) {
        link.style.backgroundColor = '';
        link.style.color = '';  
      }
  }
  
  function unhighlightLast()
  {
    var link = document.getElementById(oPreviousLinkID);
      if (null != link) {
        link.style.backgroundColor = '';
        link.style.color = '';  
      }
  }
  
  function _init() {
 
   var groupID = parent.edit.document.getElementById("GroupID");
   if (groupID != null) {
      //if groupID is empty guid, this page is in adding new group process. No need to highlight any link.
      if(groupID.value != <%= AspxTools.JsString(Guid.Empty)%>){
        var id = "_" + groupID.value.replace(/-/g, "");
        highlightLink(id);
        oPreviousLinkID = id;
      }
    } else {
      // Highlight first available link.
      var oColl = document.getElementsByTagName("A");
      if (null != oColl && null != oColl.length) {
        for (var i = 0; i < oColl.length; i++) {
          if (oColl[i].id.match(/^_/)) {
            editGroup(oColl[i], oColl[i].id.substring(1));
            break; 
          }
        }
      }
      
    }
  }

  <%-- Return false if user select cancel --%>
  function confirmSaveGroup(handler) {
    var shouldShowConfirm = typeof(parent.edit.isDirty) == 'function' && parent.edit.isDirty();
    parent.edit.PolyShouldShowConfirmSave(shouldShowConfirm, handler, parent.edit.save);
  }

  function updateGroupName(name) {
    if (null != oPreviousLinkID)
      document.getElementById(oPreviousLinkID).innerText = name;
    else
      self.location.href = getFullRef("PrintGroupList.aspx"); // Refresh when adding new group.
  }
  function editGroup(o, id) {
    confirmSaveGroup(function(){
      parent.edit.location = getFullRef('PrintGroupEdit.aspx?groupid=' + encodeURIComponent(id));
      //Wait until other functions finished to highlight link
      setTimeout(function() { 
        if (null != oPreviousLinkID) {
          unhighlightLink(oPreviousLinkID);
        }
        highlightLink(o.id);
        oPreviousLinkID = o.id;
      });
    });
    
    return false;
  }
  function addGroup() {
    confirmSaveGroup(function(){
      if (null != oPreviousLinkID) {
        unhighlightLink(oPreviousLinkID);    
      }

      oPreviousLinkID = null;
      parent.edit.location = getFullRef('PrintGroupEdit.aspx');    
    });
  }
  //-->
  </script>

	    <form id="PrintGroupList" method="post" runat="server">
			<table id=main cellspacing=0 cellpadding=0>
			<tr>
			<td>
				<asp:Panel id="m_addNewGroupPanel" runat="server" style="DISPLAY: inline;">
					<input type=button value="Add new group" onclick="if(!parentElement.disabled) addGroup();">
				</asp:Panel>
			</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
			<td class="fieldlabel" style="COLOR: #ffcc00" nowrap>
				Print Group
			</td>
			</tr>
			<asp:repeater id=m_printGroupRepeater runat="server">
			<itemtemplate>
				<tr>
				<td nowrap>
					<img src=<%=AspxTools.SafeUrl(VirtualRoot +"/images/bullet.gif")%> align="absMiddle">&nbsp;
					<a id=<%#AspxTools.HtmlAttribute("_" +((Guid)DataBinder.Eval(Container.DataItem, "GroupID")).ToString("N"))%> class="portletlink" href="#"
                        onclick='return editGroup(this, <%#AspxTools.HtmlAttribute(DataBinder.Eval(Container.DataItem, "GroupID").ToString())%>);'>
                        <%#AspxTools.HtmlString((string) DataBinder.Eval(Container.DataItem, "GroupName"))%>
					</a>
				</td>
				</tr>
			</itemtemplate>
			</asp:repeater>
			</table>
		</form>
	</body>
</html>
