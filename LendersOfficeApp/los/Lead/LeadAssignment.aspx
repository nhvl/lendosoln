<%@ Page language="c#" Codebehind="LeadAssignment.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Lead.LeadAssignment" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="status" TagName="OfficialContactList" Src="~/newlos/Status/OfficialContactList.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>LeadAssignment</title>
    <style type="text/css">
        .warning-icon { 
            display: none;
            height: 25px; 
            vertical-align: bottom; 
        }
    </style>
<meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
<meta content=C# name=CODE_LANGUAGE>
<meta content=JavaScript name=vs_defaultClientScript>
<meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema>
  </HEAD>
<body MS_POSITIONING="FlowLayout" class="EditBackground">
<script type="text/javascript">
function displayPeopleInRole(role) {
    var name;
    var roledesc;
    var show;
    if (role == 'Agent') {
        name = '<%= AspxTools.ClientId(sEmployeeLoanRep) %>';
        roledesc = 'Loan+Officer';
        show = 'full';
    } else if (role == 'Processor') {
        name = '<%= AspxTools.ClientId(sEmployeeProcessor) %>';
        roledesc = 'Processor';
        show = 'simple';
    } else if (role == 'Manager') {
        name = '<%= AspxTools.ClientId(sEmployeeManager) %>';
        roledesc = 'Manager';
        show = 'simple';
    } else if (role == 'RealEstateAgent') {
        name = '<%= AspxTools.ClientId(sEmployeeRealEstateAgent) %>';
        roledesc = 'Real+Estate+Agent';
        show = 'simple';
    } else if (role == 'Telemarketer') {
        name = '<%= AspxTools.ClientId(sEmployeeCallCenterAgent) %>';
        roledesc = 'Call+Center+Agent';
        show = 'simple';
    } else if (role == 'LenderAccountExec') {
        name = '<%= AspxTools.ClientId(sEmployeeLenderAccExec) %>';
        roledesc = 'Lender+Account+Executive';
        show = 'simple';
    } else if (role == 'LockDesk') {
        name = '<%= AspxTools.ClientId(sEmployeeLockDesk) %>';
        roledesc = 'Lock+Desk+Agent';
        show = 'simple';
    } else if (role == 'Underwriter') {
        name = '<%= AspxTools.ClientId(sEmployeeUnderwriter) %>';
        roledesc = 'Underwriter';
        show = 'simple';
    } else if (role == 'LoanOpener') {
        name = '<%= AspxTools.ClientId(sEmployeeLoanOpener) %>';
        roledesc = 'Loan+Opener';
        show = 'simple';
    } else if (role == 'Closer') {
        name = '<%= AspxTools.ClientId(sEmployeeCloser) %>';
        roledesc = 'Closer';
        show = 'simple';
    } else if (role == 'Shipper') {
        name = '<%= AspxTools.ClientId(sEmployeeShipper) %>';
        roledesc = 'Shipper';
        show = 'simple';
    } else if (role == 'Funder') {
        name = '<%= AspxTools.ClientId(sEmployeeFunder) %>';
        roledesc = 'Funder';
        show = 'simple';
    } else if (role == 'PostCloser') {
        name = '<%= AspxTools.ClientId(sEmployeePostCloser) %>';
        roledesc = 'Post-Closer';
        show = 'simple';
    } else if (role == 'Insuring') {
        name = '<%= AspxTools.ClientId(sEmployeeInsuring) %>';
        roledesc = 'Insuring';
        show = 'simple';
    } else if (role == 'CollateralAgent') {
        name = '<%= AspxTools.ClientId(sEmployeeCollateralAgent) %>';
        roledesc = 'Collateral+Agent';
        show = 'simple';
    } else if (role == 'DocDrawer') {
        name = '<%= AspxTools.ClientId(sEmployeeDocDrawer) %>';
        roledesc = 'Doc+Drawer';
        show = 'simple';
    } else if (role == 'CreditAuditor') {
        name = '<%= AspxTools.ClientId(sEmployeeCreditAuditor) %>';
        roledesc = 'Credit+Auditor';
        show = 'simple';
    } else if (role == 'DisclosureDesk') {
        name = '<%= AspxTools.ClientId(sEmployeeDisclosureDesk) %>';
        roledesc = 'Disclosure+Desk';
        show = 'simple';
    } else if (role == 'JuniorProcessor') {
        name = '<%= AspxTools.ClientId(sEmployeeJuniorProcessor) %>';
        roledesc = 'Junior+Processor';
        show = 'simple';
    } else if (role == 'JuniorUnderwriter') {
        name = '<%= AspxTools.ClientId(sEmployeeJuniorUnderwriter) %>';
        roledesc = 'Junior+Underwriter';
        show = 'simple';
    } else if (role == 'LegalAuditor') {
        name = '<%= AspxTools.ClientId(sEmployeeLegalAuditor) %>';
        roledesc = 'Legal+Auditor';
        show = 'simple';
    } else if (role == 'LoanOfficerAssistant') {
        name = '<%= AspxTools.ClientId(sEmployeeLoanOfficerAssistant) %>';
        roledesc = 'Loan+Officer+Assistant';
        show = 'simple';
    } else if (role == 'Purchaser') {
        name = '<%= AspxTools.ClientId(sEmployeePurchaser) %>';
        roledesc = 'Purchaser';
        show = 'simple';
    } else if (role == 'QCCompliance') {
        name = '<%= AspxTools.ClientId(sEmployeeQCCompliance) %>';
        roledesc = 'QC+Compliance';
        show = 'simple';
    } else if (role == 'Secondary') {
        name = '<%= AspxTools.ClientId(sEmployeeSecondary) %>';
        roledesc = 'Secondary';
        show = 'simple';
    } else if (role == 'Servicing') {
        name = '<%= AspxTools.ClientId(sEmployeeServicing) %>';
        roledesc = 'Servicing';
        show = 'simple';
    } else {
        return;
    }

    showModal( '/los/EmployeeRole.aspx?role=' + role + '&roledesc=' + roledesc + '&loanid=<%=AspxTools.JsStringUnquoted(LoanID.ToString())%>' + '&show=' + show , null, null, null, function(arg){ 
        if( arg.OK )
        {
            document.getElementById(name).innerText= arg.EmployeeName;
            document.getElementById(name).href = "mailto:" + arg.Email;
            window.location.href = window.location.href;
        }
    }, { hideCloseButton:true });
    
}
function displayStatusLink() {
    
    showModal('/los/View/LoanStatusList.aspx?src=lead', null, null, null, function(arg){ 
        if (arg.OK) {
          updateDirtyBit();
            document.getElementById("<%= AspxTools.ClientId(sStatusT_rep) %>").value = arg.Status;
            document.getElementById("sStatusT").value = arg.StatusCode;
        }
    });
  }
  function f_convertToLoan() {
    
      if(isDirty())
      {
          alert("Please save the changes before attempting to convert to loan.");
      }
      else
      {
          var option = "0";
          if (document.getElementById("ConvertOption1RB").checked)
              option = "1";
    
          var sLpTemplateId = f_getDDLValue('<%= AspxTools.ClientId(TemplateDDL) %>');
          if (option == "1") {
              // Alert user if template not selected.
              if (sLpTemplateId == <%= AspxTools.JsString(Guid.Empty.ToString()) %>) {
                  alert(<%=AspxTools.JsString(JsMessages.PickLoanTemplate)%>);
                  return;
              }

              // OPM 231555 - Alert user if lead has archives.
              if (<%= AspxTools.JsBool(HasArchives) %> ) {
                var msg;
                if (<%= AspxTools.JsBool(IsForceLeadToLoanTemplate) %> ) {
                  msg = <%= AspxTools.JsString(JsMessages.LeadHasArchives_ForceTemplate) %>;
                }
                else {
                  msg = <%= AspxTools.JsString(JsMessages.LeadHasArchives) %>;  
                }

                if(!confirm(msg)){
                    return;
                }
              }

          }
    
          var args = new Object();
          args["loanid"] = <%=AspxTools.JsString(LoanID.ToString())%>;
          args["option"] = option;
          args["sLpTemplateId"] = sLpTemplateId;
          args["RemoveLeadPrefix"] = document.getElementById("RemoveLeadPrefixRB").checked ? 'True' : 'False';
    
          <% if(phConsumerPortalVerbalSubmissionInfo.Visible){ %>
          args["sConsumerPortalUserMadeVerbalSubmission"] = $('#sConsumerPortalUserMadeVerbalSubmission').prop('checked');
          args["sConsumerPortalVerbalSubmissionD"] = $('#sConsumerPortalVerbalSubmissionD').val();
          args["sConsumerPortalVerbalSubmissionComments"] = $('#sConsumerPortalVerbalSubmissionComments').val();
          <%} %>
    
          var result = gService.loanedit.call("ConvertToLoan", args);
    
          if (!result.error) {
              if (result.value["MersErrors"]) {
                  alert('Unable to generate a MERS MIN for this loan:\n' + result.value["MersErrors"]);
              }

              if (result.value["Action"] == "Close") {

                  <% if ( Broker.IsEditLeadsInFullLoanEditor == false ) { %>
                    alert(<%=AspxTools.JsString(JsMessages.LeadConvertSuccessful)%>);
                  
                    if (typeof parent.bConvertedLeadToLoan == "boolean") 
                      {
                          parent.bConvertedLeadToLoan = true;
                      }

                      if ((typeof result.value["LoanId"] == "string") && 
                          (typeof parent.sConvertedLoanId == "string") && 
                          (typeof parent.bOpenConvertedLoan == "boolean")) 
                      {
                          parent.bOpenConvertedLoan = true;
                          parent.sConvertedLoanId = result.value["LoanId"];
                      }
                  
                      parent.header.closeMe();
                  <% } else { %>
                      var body_url = <%=AspxTools.JsNumeric(DataAccess.E_UrlOption.Page_StatusGeneral) %>;
                      var convertedLoanId = result.value["LoanId"];

                      if(typeof convertedLoanId == "string")
                      {
                          alert(<%=AspxTools.JsString(JsMessages.LeadConvertSuccessful)%>);
                          if(convertedLoanId == ML.sLId)
                          {
                              window.top.location = ML.VirtualRoot + '/newlos/LoanApp.aspx' + '?loanid=' + ML.sLId + "&appid=" + ML.aAppId + '&body_url=' + body_url;
                          }
                          else
                          {
                              // Don't know app id for this new loan.
                              window.top.location = ML.VirtualRoot + '/newlos/LoanApp.aspx' + '?loanid=' + encodeURIComponent(convertedLoanId) + '&body_url=' + body_url;
                          }
                      }
                      else
                      {
                          alert('You do not have permission to access the converted loan');
                          parent.header.closeMe();
                      }
                  <% } %>
              }
          } else {
              // OPM 243181 - If failed on CLoanNumberDuplicateException, offer user chance to convert with new loan number.
              if (<%= AspxTools.JsBool(IsForceLeadToUseLoanCounter) %> && result.UserMessage.indexOf("<%= AspxTools.HtmlString(JsMessages.LoanInfo_DuplicateLoanNumber) %>") > -1) {
                $('#dialog-convertNameError').dialog({
                    modal: true,
                    buttons: {
                        "Yes": function() {
                            $(this).dialog("close");
                            document.getElementById("GenerateNewLoanNumberRB").checked = true;
                            //document.getElementById("RemoveLeadPrefixRB").checked = true;
                            f_convertToLoan();
                        },
                        "No": function() {
                            $(this).dialog("close");
                        }
                    },
                    closeOnEscape: false,
                    width: "300",
                    draggable: false,
                    dialogClass: "LQBDialogBox",
                    resizable: false
                });
                
                return;
              }
              
              alert(result.UserMessage);
          }
      }
  }

  function f_getDDLValue(id) {
    var ddl = document.getElementById(id);
    var value = "";
    for (var i = 0; i < ddl.options.length; i++) {
      if (ddl.options[i].selected)
        value = ddl.options[i].value;
    }
    return value;
  }
  function f_displayConvert() {
    document.getElementById("ConvertLeadPanel").style.display = "";
    document.getElementById("btnDisplayConvert").style.display = "none";
    var alreadyHasTemplate = <%= AspxTools.JsBool(!IsLeadMadeFromBlankOrQPTemplate) %>;
    var IsForceLeadToLoanTemplate = <%= AspxTools.JsBool(IsForceLeadToLoanTemplate) %>;

    if(alreadyHasTemplate)
    {
        document.getElementById("ConvertOption0RB").style.display = "none";
        document.getElementById("ConvertOption0RBLabel").style.display = "none";
        document.getElementById("ConvertOption1RB").style.display = "none";
        document.getElementById("ConvertOption1RB").checked = false;
        document.getElementById("TemplateDDL").style.display = "none";
        document.getElementById("ConvertOption1RBLabel").style.display = "none";
  
        f_option0();
    }
    else if(IsForceLeadToLoanTemplate)
    {
        document.getElementById("ConvertOption0RB").style.display = "none";
        document.getElementById("ConvertOption0RBLabel").style.display = "none";
        document.getElementById("ConvertOption1RB").style.display = "none";
        document.getElementById("ConvertOption1RB").checked = true;
        f_option1();
    }

    if (<%= AspxTools.JsBool(IsForceLeadToLoanNewLoanNumber || IsForceLeadToLoanRemoveLeadPrefix || IsForceLeadToUseLoanCounter) %>) {
        document.getElementById("NumberingOptionsHeader").style.display = "none";
        document.getElementById("NumberingOptionsList").style.display = "none";
        if (<%= AspxTools.JsBool(IsForceLeadToLoanNewLoanNumber) %>) {
            document.getElementById("GenerateNewLoanNumberRB").checked = true;
        }
        else {
            document.getElementById("RemoveLeadPrefixRB").checked = true;
        }
    }
  }
  function f_hideConvert() {
    document.getElementById("ConvertLeadPanel").style.display = "none";
    document.getElementById("btnDisplayConvert").style.display = "";
  }

  function setConvertWarning() {
      if (!ML.UserHasLeadConversionPrivilege) {
          $('#WarningIcon').show().prop('title', ML.ConvertLeadToLoanBlockMessage);
          $('#btnDisplayConvert').prop('disabled', true);
      }
  }
  
  function f_option0() {
      <%= AspxTools.JsGetElementById(TemplateDDL) %>.disabled = true;
      //document.getElementById("RemoveLeadPrefixRB").disabled = false;
      //document.getElementById("GenerateNewLoanNumberRB").disabled = false;
  }
  
  function f_option1() {
    <%= AspxTools.JsGetElementById(TemplateDDL) %>.disabled = false;
    //document.getElementById("RemoveLeadPrefixRB").disabled = true;
    //document.getElementById("GenerateNewLoanNumberRB").disabled = true;
    
  }
  function _init() {
        
      <%= AspxTools.JsGetElementById(TemplateDDL) %>.disabled = true;
      document.getElementById("RemoveLeadPrefixRB").disabled = true;
      document.getElementById("GenerateNewLoanNumberRB").checked = true;
  }
  
  $(document).ready(function(){
        setConvertWarning();

        var $cbConsumerPortalUserMadeVerbalSubmission = $('#sConsumerPortalUserMadeVerbalSubmission');
        var $divConsumerPortalVerbalSubmissionInfo = $('#divConsumerPortalVerbalSubmissionInfo');
        var $sConsumerPortalVerbalSubmissionD = $('#sConsumerPortalVerbalSubmissionD');
        var $sConsumerPortalVerbalSubmissionComments = $('#sConsumerPortalVerbalSubmissionComments');
        var $bConvertToLoan = $('#bConvertToLoan');
        
        $cbConsumerPortalUserMadeVerbalSubmission.change(f_oncbConsumerPortalUserMadeVerbalSubmissionChange);
        $sConsumerPortalVerbalSubmissionD.change(f_onsConsumerPortalVerbalSubmissionDChange);
        
        function f_oncbConsumerPortalUserMadeVerbalSubmissionChange()
        {
            if(this.checked)
            {
                if($sConsumerPortalVerbalSubmissionD.val() === '')
                {
                    $sConsumerPortalVerbalSubmissionD.val(f_getTodaysDateString())
                }
                $bConvertToLoan.prop('disabled', false);
            }
            else
            {
                 $sConsumerPortalVerbalSubmissionD.val('');
                 $sConsumerPortalVerbalSubmissionComments.val('');
                 $bConvertToLoan.prop('disabled', true);
            }
            
            $divConsumerPortalVerbalSubmissionInfo.toggle(this.checked);
        }
        
        function f_onsConsumerPortalVerbalSubmissionDChange()
        {
            if($(this).val() === '')
            {
                $cbConsumerPortalUserMadeVerbalSubmission.prop('checked', false).trigger('change');
            }
        }
        
        function f_getTodaysDateString()
        {
            <%-- http://stackoverflow.com/questions/1531093/how-to-get-current-date-in-javascript --%>
            var today = new Date();
            var d = today.getDate();
            var m = today.getMonth() + 1;
            var y = today.getFullYear();
            
            if (d < 10)
            {
                d = '0' + d;
            }
            if (m < 10)
            {
                m = '0' + m;
            }
            
            return m + '/' + d + '/' + y;
        }
        
        $cbConsumerPortalUserMadeVerbalSubmission.trigger('change');
        
  });
</script>

<form id=LeadAssignment method=post runat="server">
<div style="display:none;" id="dialog-convertNameError" title="Duplicate Loan Number">
    <p style="width: 100%">An existing loan file has the same loan number. Converting this lead to a loan requires assigning a new loan number. Do you want to continue?</p>
</div>
<table cellSpacing=0 cellPadding=0 width="100%" border=0>
  <tr>
    <td class=MainRightHeader style="PADDING-LEFT: 5px" 
      >Lead Assignment</td></tr>
  <TR >
    <TD style="PADDING-LEFT: 5px">
      <TABLE class=InsetBorder id=Table1 cellSpacing=0 cellPadding=0 width="95%" 
      border=0>
        <TBODY>
        <TR>
          <TD noWrap>
              <INPUT class=ButtonStyle id="btnDisplayConvert" type=button value="Convert to loan file >>>" onclick="f_displayConvert();" NoHighlight>
              <img src="../../images/warning.svg" alt="Warning" id="WarningIcon" class="warning-icon" />
          </TD>
        </TR>
          <tbody id="ConvertLeadPanel" style="DISPLAY:none">
        <TR>
          <TD noWrap class="FieldLabel"><input type="radio" id=ConvertOption0RB name="ConvertOption" NotForEdit checked onclick="f_option0();"><label id="ConvertOption0RBLabel" for="ConvertOption0RB">Convert to loan</label></TD></TR>
        <TR>
          <TD noWrap class="FieldLabel"><input type="radio" id="ConvertOption1RB" name="ConvertOption" NotForEdit onclick="f_option1();"><label for="ConvertOption1RB" id="ConvertOption1RBLabel" >Convert to loan using template</label>&nbsp;<asp:DropDownList id=TemplateDDL runat="server" EnableViewState="False" NotForEdit="t">
<asp:ListItem Value="&lt;-- Choose a template --&gt;">&lt;-- Choose a template --&gt;</asp:ListItem>
</asp:DropDownList></TD></TR>
        <TR id="NumberingOptionsHeader">
          <TD class=FieldLabel noWrap>Loan numbering options:</TD></TR>
        <TR id="NumberingOptionsList">
          <TD class=FieldLabel style="PADDING-LEFT: 10px" noWrap><input type="radio" id=RemoveLeadPrefixRB NotForEdit  Name="NamingOption" checked><label for="RemoveLeadPrefixRB">Remove "Lead" prefix from loan number</label>&nbsp;&nbsp; 
<input type="radio" id=GenerateNewLoanNumberRB NotForEdit Name="NamingOption"><label for="GenerateNewLoanNumberRB">Auto-create new loan number</label></TD></TR>
        <asp:PlaceHolder ID="phConsumerPortalVerbalSubmissionInfo" runat="server" Visible="false">
        <TR>
            <td class="FieldLabel">
                <asp:CheckBox ID="sConsumerPortalUserMadeVerbalSubmission" runat="server" Text="Borrower provided verbal submission outside of Consumer Portal"/>
                <div id="divConsumerPortalVerbalSubmissionInfo">
                    <table style="margin-top:5px">
                    <tbody>
                        <tr>
                            <td width="8px"></td>
                            <td>
                                <span class="FieldLabel">Date of verbal submission</span>
                            </td>
                            <td>
                                <ml:DateTextBox runat="server" ID="sConsumerPortalVerbalSubmissionD"></ml:DateTextBox> 
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">
                                <span class="FieldLabel">Comments</span>
                                <asp:TextBox runat="server" ID="sConsumerPortalVerbalSubmissionComments" MaxLength="500" Width="250"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
            </td>
        </TR>
        </asp:PlaceHolder>
        <TR>
          <TD noWrap align=center><INPUT class=ButtonStyle type=button value=Convert id="bConvertToLoan" onclick="f_convertToLoan();">&nbsp; 
<INPUT class=ButtonStyle type=button value=Cancel onclick="f_hideConvert();"></TD></TR></tbody></TABLE></TD></TR>
  <tr>
    <td style="PADDING-LEFT: 5px">
      <table class=InsetBorder cellSpacing=0 cellPadding=0 border=0 
      width="95%">
        <tr>
          <td class=FieldLabel nowrap>Status</td>
          <td colSpan=2 width="100%"><asp:TextBox id=sStatusT_rep runat="server" ReadOnly="True"></asp:TextBox><input type=button value="Change Status" onclick="displayStatusLink();" class=ButtonStyle></td></tr>
        <tr>
          <td class="LoanFormHeader FieldLabel" nowrap align=center 
            colSpan=3>Assignment</td></tr>
        <tr><td width="32%">
        <table id="AssignmentCol1">
            <tr>
              <td class=FieldLabel noWrap>Call Center Agent</td>
              <td><a id=sEmployeeCallCenterAgent runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<A href="javascript:displayPeopleInRole('Telemarketer');" >(assign)</A><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Closer</td>
              <td><a id="sEmployeeCloser" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('Closer');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Collateral Agent</td>
              <td><a id="sEmployeeCollateralAgent" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('CollateralAgent');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Credit Auditor</td>
              <td><a id="sEmployeeCreditAuditor" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('CreditAuditor');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Disclosure Desk</td>
              <td><a id="sEmployeeDisclosureDesk" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('DisclosureDesk');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Doc Drawer</td>
              <td><a id="sEmployeeDocDrawer" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('DocDrawer');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Funder</td>
              <td><a id="sEmployeeFunder" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('Funder');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Insuring</td>
              <td><a id="sEmployeeInsuring" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('Insuring');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Junior Processor</td>
              <td><a id="sEmployeeJuniorProcessor" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('JuniorProcessor');" >(assign)</a><% } %></td></tr>
            </table></td>
        <td width="35%"><table id="AssignmentCol2">
            <tr>
              <td class="FieldLabel">Junior Underwriter</td>
              <td><a id="sEmployeeJuniorUnderwriter" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('JuniorUnderwriter');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Legal Auditor</td>
              <td><a id="sEmployeeLegalAuditor" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('LegalAuditor');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class=FieldLabel noWrap>Lender Account Executive&nbsp;&nbsp;&nbsp;</td>
              <td><a id="sEmployeeLenderAccExec" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<A href="javascript:displayPeopleInRole('LenderAccountExec');" >(assign)</A><% } %></td></tr>
            <tr>
              <td class=FieldLabel noWrap>Loan Officer</td>
              <td><a id=sEmployeeLoanRep runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<A href="javascript:displayPeopleInRole('Agent');" >(assign)</A><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Loan Officer Assistant</td>
              <td><a id="sEmployeeLoanOfficerAssistant" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('LoanOfficerAssistant');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class=FieldLabel noWrap>Loan Opener</td>
              <td><a id=sEmployeeLoanOpener runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<A href="javascript:displayPeopleInRole('LoanOpener');" >(assign)</A><% } %></td></tr>
            <tr>
              <td class=FieldLabel noWrap>Lock Desk</td>
              <td><a id="sEmployeeLockDesk" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<A href="javascript:displayPeopleInRole('LockDesk');" >(assign)</A><% } %></td></tr>
            <tr>
              <td class=FieldLabel noWrap>Manager</td>
              <td><a id=sEmployeeManager runat="server"></a>&nbsp;&nbsp;<A href="javascript:displayPeopleInRole('Manager');" >(assign)</A></td></tr>
            <tr>
              <td class="FieldLabel">Post-Closer</td>
              <td><a id="sEmployeePostCloser" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('PostCloser');" >(assign)</a><% } %></td></tr>
            </table></td>
        <td width="32%"><table id="AssignmentCol3">
            <tr>
              <td class=FieldLabel noWrap>Processor</td>
              <td><a id=sEmployeeProcessor runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<A href="javascript:displayPeopleInRole('Processor');" >(assign)</A><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Purchaser</td>
              <td><a id="sEmployeePurchaser" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('Purchaser');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">QC Compliance</td>
              <td><a id="sEmployeeQCCompliance" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('QCCompliance');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class=FieldLabel noWrap>Real Estate Agent</td>
              <td><a id=sEmployeeRealEstateAgent runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<A href="javascript:displayPeopleInRole('RealEstateAgent');" >(assign)</A><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Secondary</td>
              <td><a id="sEmployeeSecondary" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('Secondary');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Servicing</td>
              <td><a id="sEmployeeServicing" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('Servicing');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class="FieldLabel">Shipper</td>
              <td><a id="sEmployeeShipper" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<a href="javascript:displayPeopleInRole('Shipper');" >(assign)</a><% } %></td></tr>
            <tr>
              <td class=FieldLabel noWrap>Underwriter</td>
              <td><a id="sEmployeeUnderwriter" runat="server"></a><% if (!m_isAssignToManagerOnly) { %>&nbsp;&nbsp;<A href="javascript:displayPeopleInRole('Underwriter');" >(assign)</A><% } %></td></tr>
          </table></td></tr>
          
      <tr>
        <td colspan=3>
            <status:OfficialContactList ID="OfficialContactList1" runat="server" />
        </td>
      </tr>
      </table></td></tr>
</table>
</form>
<uc1:cmodaldlg id=CModalDlg1 runat="server"></uc1:cmodaldlg>
	<script type="text/javascript">
    var oldSaveMe = window.saveMe;
    window.saveMe = function(bRefreshScreen)
    {
        var resultSave = oldSaveMe(bRefreshScreen);
        if (resultSave)
        {
            window.parent.treeview.location = window.parent.treeview.location;
        }
        return resultSave;
    }
    </script>
  </body>
</HTML>
