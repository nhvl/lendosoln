using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Admin;
using LendersOffice.Constants;
namespace LendersOfficeApp.los.Lead
{
	public partial class LeadCreate : LendersOffice.Common.BasePage
	{
        protected string Purpose
        {
            get { return RequestHelper.GetSafeQueryString("purpose"); }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            if (Page.IsPostBack) 
                CreateLead();
		}

        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected bool IsNewPmlUIEnabled
        {
            get
            {
                var brokerDB = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                if (brokerDB.IsNewPmlUIEnabled)
                {
                    return true;
                }
                else
                {
                    var employeeDB = EmployeeDB.RetrieveById(BrokerUser.BrokerId, BrokerUser.EmployeeId);
                    if (employeeDB.IsNewPmlUIEnabled)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        private void CreateLead() 
        {
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            Guid loanID = Guid.Empty;
            try
            {
                CLoanFileCreator creator = CLoanFileCreator.GetCreator( principal, LendersOffice.Audit.E_LoanCreationSource.LeadCreate );
                loanID = creator.CreateLead(templateId: Guid.Empty);

                // We will need to set the loan purpose and set the default pml filters
                if (IsNewPmlUIEnabled)
                {
                    var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanID, typeof(LeadCreate));
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    if (!string.IsNullOrEmpty(Purpose))
                    {
                        if (Purpose.ToLower() == "purchase")
                        {
                            dataLoan.sLPurposeT = E_sLPurposeT.Purchase;
                        }
                        else if (Purpose.ToLower() == "refinance")
                        {
                            dataLoan.sLPurposeT = E_sLPurposeT.Refin;
                        }
                        else if (Purpose.Equals("HELOC", StringComparison.OrdinalIgnoreCase))
                        {
                            dataLoan.sIsLineOfCredit = true;
                        }
                    }

                    ApplyDefaultPmlFilters(dataLoan);

                    dataLoan.Save();
                }
            }
            catch( Exception exc )
            {
                ErrorUtilities.DisplayErrorPage(exc, true, principal.BrokerId, principal.UserId);
            }

            Response.Redirect("leadmainframe.aspx?loanid=" + loanID + "&isnew=t");
        }

        /// <summary>
        /// Apply the filter values of the defualt pml loan template.
        /// </summary>
        private void ApplyDefaultPmlFilters(CPageData dataLoan)
        {
            Guid brokerId = BrokerUser.BrokerId;
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);

            if (brokerDB.PmlLoanTemplateID == Guid.Empty) return;

            CPageData pmlTemplate = CPageData.CreateUsingSmartDependencyWithSecurityBypass(brokerDB.PmlLoanTemplateID,
                typeof(LeadCreate));
            pmlTemplate.InitLoad();

            dataLoan.sProdFilterDue10Yrs = pmlTemplate.sProdFilterDue10Yrs;
            dataLoan.sProdFilterDue15Yrs = pmlTemplate.sProdFilterDue15Yrs;
            dataLoan.sProdFilterDue20Yrs = pmlTemplate.sProdFilterDue20Yrs;
            dataLoan.sProdFilterDue25Yrs = pmlTemplate.sProdFilterDue25Yrs;
            dataLoan.sProdFilterDue30Yrs = pmlTemplate.sProdFilterDue30Yrs;
            dataLoan.sProdFilterDueOther = pmlTemplate.sProdFilterDueOther;
            dataLoan.sProdFilterFinMethFixed = pmlTemplate.sProdFilterFinMethFixed;
            dataLoan.sProdFilterFinMeth3YrsArm = pmlTemplate.sProdFilterFinMeth3YrsArm;
            dataLoan.sProdFilterFinMeth5YrsArm = pmlTemplate.sProdFilterFinMeth5YrsArm;
            dataLoan.sProdFilterFinMeth7YrsArm = pmlTemplate.sProdFilterFinMeth7YrsArm;
            dataLoan.sProdFilterFinMeth10YrsArm = pmlTemplate.sProdFilterFinMeth10YrsArm;
            dataLoan.sProdFilterFinMethOther = pmlTemplate.sProdFilterFinMethOther;
            dataLoan.sProdIncludeNormalProc = pmlTemplate.sProdIncludeNormalProc;
            dataLoan.sProdIncludeMyCommunityProc = pmlTemplate.sProdIncludeMyCommunityProc;
            dataLoan.sProdIncludeHomePossibleProc = pmlTemplate.sProdIncludeHomePossibleProc;
            dataLoan.sProdIncludeFHATotalProc = pmlTemplate.sProdIncludeFHATotalProc;
            dataLoan.sProdIncludeVAProc = pmlTemplate.sProdIncludeVAProc;
            dataLoan.sProdIncludeUSDARuralProc = pmlTemplate.sProdIncludeUSDARuralProc;
            dataLoan.sProdFilterPmtTPI = pmlTemplate.sProdFilterPmtTPI;
            dataLoan.sProdFilterPmtTIOnly = pmlTemplate.sProdFilterPmtTIOnly;
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
