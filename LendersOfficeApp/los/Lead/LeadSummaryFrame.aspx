<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %> 
<%@ Page language="c#" Codebehind="LeadSummaryFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Lead.LeadSummaryFrame" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>LeadSummaryFrame</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </HEAD>
  <body MS_POSITIONING="FlowLayout" class="HeaderBackground">
	<script language=javascript>
  <!--
  var gHistoryIndex = -1;
  var gHistory = new Array();
  var gHistoryTitle = new Array();
  var gMaxHistoryIndex = 0;
  var gIsTreeCollapse = false;
  var gaBLastNm = '';
  var currentClosingCostFeeVersionT = null;

  function f_reloadNavigationFrameIfClosingCostFeeVersionChanged(newClosingCostFeeVersionT) {
      if (newClosingCostFeeVersionT !== currentClosingCostFeeVersionT) {
          parent.treeview.f_refreshAfterTimeout(1000);
          currentClosingCostFeeVersionT = newClosingCostFeeVersionT;
      }
  }
  
  function _init() {
    <% if (!RequestHelper.GetBool("noinit")) { %>
        <%if(IsEnableBigLoanPage) { %>
      parent.body.location = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/BigLoanInfo.aspx?loanid=' + <%= AspxTools.JsString(LoanID.ToString()) %> + '&appid=' + encodeURIComponent(f_getCurrentApplicationID()) + '&isLead=t';
              <% } else { %>
      parent.body.location = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/LoanInfo.aspx?loanid=' + <%= AspxTools.JsString(LoanID.ToString()) %> + '&appid=' + encodeURIComponent(f_getCurrentApplicationID()) + '&isLead=t';
      <% } %> 
    <% } %>

    f_updateButtons();  
  }
  function updateFrameTitle(title) {
    parent.document.title = title;  
  }
  
  function f_pushHistory(href, title) {
    if (gHistory[gHistoryIndex] != href) {
      // Don't insert if previous location is the same with current.
      gHistory[++gHistoryIndex] = href;
      gHistoryTitle[gHistoryIndex] = title;
      gMaxHistoryIndex = gHistoryIndex;
    }
    f_updateButtons();    
  }
  
  function f_goBack() {
    if (gHistoryIndex > 0)
      f_goToLocation(gHistory[--gHistoryIndex]);
  }
  
  function f_goForward() {
    if (gHistoryIndex < gMaxHistoryIndex)
      f_goToLocation(gHistory[++gHistoryIndex]);
  }
  
  function f_goToLocation(href) {
    try{
        parent.body.f_loadFromHistory(href);
        f_updateButtons();    
    }catch(e){}
  }
  
  function f_updateButtons() {
    document.getElementById("btnBack").disabled = gHistoryIndex <= 0;
    document.getElementById("btnBack").style.backgroundColor = "";
    if (!document.getElementById("btnBack").disabled) {
      document.getElementById("btnBack").title = "Back to " + gHistoryTitle[gHistoryIndex - 1];
    } else {
      document.getElementById("btnBack").title = "";
    }
    document.getElementById("btnForward").disabled = gHistoryIndex == gMaxHistoryIndex || gHistoryIndex == -1;
    document.getElementById("btnForward").style.backgroundColor = "";
    if (!document.getElementById("btnForward").disabled) {
      document.getElementById("btnForward").title = "Forward to " + gHistoryTitle[gHistoryIndex + 1];
    } else {
      document.getElementById("btnForward").title = "";
    }
  }  
  function f_enableSave(b) {
    if (document.getElementById("btnSave") != null) {
      document.getElementById("btnSave").disabled = !b;
      document.getElementById("btnSave").style.backgroundColor = "";
    }
  }
  
  function f_save() {
    try{
        parent.body.f_saveMe();
    }catch(e){}
  }
  function f_getCurrentApplicationID() {
      return <%= AspxTools.JsGetElementById(m_applicantsDDL) %>.value;
  }
  
  function f_setCurrentApplicationID(id) {
      return <%= AspxTools.JsGetElementById(m_applicantsDDL) %>.value = id;
  }
  function f_updateApplicantDDL(name, id) {
      var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;
      var count = ddl.options.length;
      var op = null;
      for (var i = 0; i < count; i++) {
          if (ddl.options[i].value == id) {
              op = ddl.options[i];
              break;
          }
      }
      if (op == null) {
          op = document.createElement("OPTION");
          ddl.options.add(op);
      }
      op.text = name;
      op.value = id;
      f_setCurrentApplicationID(id);
  }
  function f_updateFieldValue(name, value) {
    document.getElementById(name).innerText = value == "" ? "N/A" : value;
  }
  function f_updateLoanNumber(sLNm) {
    parent.f_update_sLNm(sLNm + ' - ' + gaBLastNm);    
    document.getElementById("sLNm").innerText = sLNm;
  }      
  function f_refreshInfo() {
    f_displayLoanSummary(true);
    if (gService == null || gService.SummaryFrame == null) {
      return;
    }
    var args = new Object();
    args["loanid"] = <%=AspxTools.JsString(LoanID.ToString())%>;
    var result = gService.SummaryFrame.call("RefreshInfo", args);
    if (!result.error) {
      gaBLastNm = result.value["aBLastNm"];
      f_updateLoanNumber(result.value["sLNm"]);
      f_updateFieldValue("sQualTopR", result.value["sQualTopR"]);
      f_updateFieldValue("sQualBottomR", result.value["sQualBottomR"]);
      f_updateFieldValue("sCltvR", result.value["sCltvR"]);
      f_updateFieldValue("sLtvR", result.value["sLtvR"]);
      f_updateFieldValue("aBExperianScore", result.value["aBExperianScore"]);
      f_updateFieldValue("aBTransUnionScore", result.value["aBTransUnionScore"]);
      f_updateFieldValue("aBEquifaxScore", result.value["aBEquifaxScore"]);

      f_updateFieldValue("aCExperianScore", result.value["aCExperianScore"]);
      f_updateFieldValue("aCTransUnionScore", result.value["aCTransUnionScore"]);
      f_updateFieldValue("aCEquifaxScore", result.value["aCEquifaxScore"]);      

      var closingCostFeeVersionT = result.value['sClosingCostFeeVersionT'];
      if (currentClosingCostFeeVersionT === null) {
          currentClosingCostFeeVersionT = closingCostFeeVersionT;
      }
      else {
          f_reloadNavigationFrameIfClosingCostFeeVersionChanged(closingCostFeeVersionT);
      }
    }
  }  
  function f_updateRatioMannually(sLtvR, sCltvR, sQualTopR, sQualBottomR) {
      f_updateFieldValue("sQualTopR", sQualTopR);
      f_updateFieldValue("sQualBottomR", sQualBottomR);
      f_updateFieldValue("sCltvR", sCltvR);
      f_updateFieldValue("sLtvR", sLtvR);
  }
  function f_displayLoanSummary(bVisible, reload) {
    document.getElementById("LoanInfoTablePanel0").style.display = bVisible ? "" : "none";
    document.getElementById("LoanInfoTablePanel1").style.display = bVisible ? "" : "none";
    <%= AspxTools.JsGetElementById(m_applicantsDDL) %>.style.display = bVisible ? "" : "none";
    if (!g_bReload && typeof(reload) != 'undefined' && reload)
        g_bReload = true;
  }  
  function f_print() {
    parent.treeview.load('PrintList.aspx', true);
  }
  
  function f_swapPrimary(id) {
    var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;
    var count = ddl.options.length;
    var op = null;
    for (var i = 0; i < count; i++) {
      if (ddl.options[i].value == id) {
        op = ddl.options[i];
        ddl.options.remove(i);
        ddl.options.add(op, 0);
        break;
      }
    }

    f_setCurrentApplicationID(id);
    
  }
  
  function f_removeBorrower(id) {
    var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;
    var count = ddl.options.length;
    for (var i = 0; i < count; i++) {
      if (ddl.options[i].value == id) {
        
        ddl.options.remove(i);
        
        break;
      }
    }  
  } 

    function addNewAppToDropdown(appId) {
        var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;
        ddl.options.add(new Option(', ', appId));
    }

  function onApplicantsChange() {
    // Notify body frame that applicant changed. 
    if (typeof(parent.body.switchApplicant) == "function")
      parent.body.switchApplicant();
  }
  
  g_bReload = false;
  __bIsReloading = false;
  function f_AppReload()
  {
    if (__bIsReloading)
        return true;
    
    if (g_bReload)
    {
        g_bReload = false;
        var oApps = f_refreshAppList();
        if (oApps != null && !f_IsActiveAppRemoved(oApps) )
        {
            f_updateDDL(oApps);
            return false;
        }
            
        __bIsReloading = true;
        var newUrl = f_parseUrl(parent.info.location);
        parent.info.location.href = newUrl;
        return true;
    }
    return false;
  }
  
  function f_updateDDL(oApps)
  {
    var hash = new Object();
    var ddlHash = new Object();   
    var ddl = <%= AspxTools.JsGetElementById(m_applicantsDDL) %>;
    
    for (var i=0; i < oApps.values.length; i++)
    {
        hash[oApps.values[i].id] = oApps.values[i].name;
    }
    
    for (var i = (ddl.options.length-1) ; i >= 0; i--) {
      if (typeof(hash[ddl.options[i].value]) == 'undefined')
      {
        ddl.options.remove(i);
      }
    }
    
    for (var i=0; i < ddl.options.length; i++)
    {
        ddlHash[ddl.options[i].value] = ddl.options[i].text;
    }
    
    for (var i=0; i < oApps.values.length; i++)
    {
        if (typeof(ddlHash[oApps.values[i].id]) == 'undefined')
        {
            var op = document.createElement("OPTION");
            op.text = oApps.values[i].name;
            op.value = oApps.values[i].id;
            ddl.options.add(op);
        }
    }
  }
  
  function f_IsActiveAppRemoved(oApps, currentGuid)
  {
    if (typeof(oApps) != 'object' || oApps == null)
        return true;
    
    var currentGuid = f_getCurrentApplicationID();
    var found = false;
    for (var i=0; i < oApps.values.length; i++)
    {
        if (oApps.values[i].id == currentGuid)
        {
            found = true;
            break;
        }
    }
    if (found)
        return false;
        
    return true;
  }
  
  function f_refreshAppList()
  {
    var oApps = null;
    var args = new Object();
    args["loanid"] = <%=AspxTools.JsString(LoanID)%>;
    
    var result = gService.SummaryFrame.call("RefreshAppList", args);
    if (!result.error) {
      var data = result.value["appList"];
      try{
        oApps = eval(data);
      }
      catch(e){ oApps = null; }
    }
    return oApps;
  }
  
  function f_parseUrl(location)
  {
    var href = location.href;
    var baseUrl = location.pathname;
    var param = '';
    var newParam = '';            
    var limit = href.indexOf('?');
            
    if (limit != -1)
    {
        param = href.substring(limit+1);
        newParam = f_parseUrlParameters(param);
        return baseUrl + '?' + newParam;
    }
    return href;  
  }
  
  function f_parseUrlParameters(param)
  {
    var newParam = '';
    var limit = param.indexOf("appid=");
    if (limit != -1)
    {
        if (limit==0)
        {
            limit = param.indexOf("&");
            if (limit != -1)
            {
                newParam = param.substring(limit+1);
            }
        }
        else
        {
            newParam = param.substring(0, limit-1);
            param = param.substring(limit);
            limit = param.indexOf("&");
            if (limit != -1)
            {
                newParam += param.substring(limit);
            }
        }
        return newParam;
    }
    return param;
  }
  //-->
    </script>

    <form id="LeadSummaryFrame" method="post" runat="server">
      <table cellpadding=0 cellspacing=0 border=0>
        <tr>
          <td nowrap valign=bottom height=38>
            <input id=btnBack type=button value="Back" onclick="f_goBack();" tabindex=-1>
            <input id=btnForward type=button value="Forward" onclick="f_goForward();" tabindex=-1>
            <input id=btnSave type=button value="Save" onclick="f_save();" tabindex="-1" title="Save">
            <input id="btnPrint" type="button" value="Print ..." onclick="f_print();" tabindex="-1" title="Print ..." NoHighlight />
            <asp:DropDownList id="m_applicantsDDL" runat="server" onchange="onApplicantsChange();" tabIndex="-1" NotForEdit></asp:DropDownList>
          </td>
          <td height=38>
            <table cellpadding=0 cellspacing=0 border=0 class="LoanInfoTable" id="LoanInfoTablePanel0">
              <tr>
                <td class="LoanInfoFieldLabel">Loan Num:</td><td><b><div id="sLNm">N/A</div></b></td>
                <td class="LoanInfoFieldLabel">Top:</td><td><b><div id="sQualTopR">N/A</div></b></td>
                <td class="LoanInfoFieldLabel">LTV:</td><td><b><div id="sLtvR">N/A</div></b></td>
              </tr>
              <tr>
                <td class="LoanInfoFieldLabel"></td><td></td>
                <td class="LoanInfoFieldLabel">Bottom:</td><td><b><div id="sQualBottomR">N/A</div></b></td>
                <td class="LoanInfoFieldLabel">CLTV:</td><td><b><div id="sCltvR">N/A</div></b></td>
              </tr>
            </table>
          </td>
          <td height=38>&nbsp;</td>
          <td height=38>
            <table cellpadding=0 cellspacing=0 border=0 class="LoanInfoTable" id="LoanInfoTablePanel1">
              <tr>
                <td class="LoanInfoFieldLabel">Borr. Credit Score</td>
                <td class="LoanInfoFieldLabel">XP</td><td><b><div id="aBExperianScore">N/A</div></b></td>
                <td class="LoanInfoFieldLabel">TU</td><td><b><div id="aBTransUnionScore">N/A</div></b></td>
                <td class="LoanInfoFieldLabel">EF</td><td><b><div id="aBEquifaxScore">N/A</div></b></td>
              </tr>
              <tr>
                <td class="LoanInfoFieldLabel">Coborr. Credit Score</td>
                <td class="LoanInfoFieldLabel">XP</td><td><b><div id="aCExperianScore">N/A</div></b></td>
                <td class="LoanInfoFieldLabel">TU</td><td><b><div id="aCTransUnionScore">N/A</div></b></td>
                <td class="LoanInfoFieldLabel">EF</td><td><b><div id="aCEquifaxScore">N/A</div></b></td>
              </tr>
            </table>
          </td>          
        </tr>
      </table>
     </form>
	
  </body>
</HTML>
