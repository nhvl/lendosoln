<%@ Page language="c#" Codebehind="LeadMainFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Lead.LeadMainFrame" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8;" />
    <title>LeadMainFrame</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <script language=vbscript src=<%=AspxTools.HtmlAttribute(DataAccess.Tools.VRoot + "/inc/common.vbs")%>></script>
    <script language=javascript>
    <!--
    var bConvertedLeadToLoan = false;
    var bOpenConvertedLoan = false;
        var sConvertedLoanId = "";
        

        // Copied from common.js.  Cannot register script files due to framesets.
    function resizeToScreen() {
        var w = screen.availWidth;
        var h = screen.availHeight;
        window.moveTo(0, 0);
        window.resizeTo(w, h);
        window.focus();
    }

        if(<%=AspxTools.JsBool(IsResize) %>) {
            resizeToScreen();    
        }
    
    // Workaround implemented to preserve window management
    // when switching to and from the new PML UI
    var winOpener = parent.window.opener;
    if (null != winOpener && !winOpener.closed) {
        winOpener.gSkipWindowManagement = false;
    }
    window.onbeforeunload = _onbeforeunload;
    
    function _onbeforeunload() {

        var sLId = <%=AspxTools.JsString(RequestHelper.LoanID.ToString())%>;
        var winOpener = window.opener;

        // MULTI-EDIT
        if (null != winOpener && !winOpener.closed && typeof(winOpener.generateWindowName) != "undefined") {
            var windowName = winOpener.generateWindowName(sLId);
            if (typeof winOpener.getSkipWindowManagement === 'undefined' || winOpener.getSkipWindowManagement() !== true) {
                if (typeof(winOpener.removeFromCurrentLoanList) != "undefined" && !bConvertedLeadToLoan)
                    winOpener.removeFromCurrentLoanList(sLId);
                else if (typeof(winOpener.removeFromCurrentLoanListAndClose) != "undefined" && bConvertedLeadToLoan)
                    winOpener.removeFromCurrentLoanListAndClose(sLId, true);
        
                if (bOpenConvertedLoan && typeof(winOpener.openConvertedLoan) != "undefined")
                    winOpener.openConvertedLoan(sConvertedLoanId);
            }
        }

        if (body.isDirty()) {
            return "Leave site?  Changes you made may not be saved.";
        }
    }

    function f_insertWindowToOpenList(sLNm) {
      window.name = sLNm;
      var winOpener = window.opener;
      if (null != winOpener && !winOpener.closed) {
        if (typeof(winOpener.f_insertWindowToOpenList) != "undefined")
          winOpener.f_insertWindowToOpenList(<%=AspxTools.JsString(RequestHelper.LoanID.ToString())%>, sLNm, this);
      }
    }
    function f_update_sLNm(sLNm) {
      var winOpener = window.opener;
      if (null != winOpener && !winOpener.closed) {
        if (typeof(winOpener.f_insertWindowToOpenList) != "undefined")
          winOpener.f_update_sLNm_WindowToOpenList(<%=AspxTools.JsString(RequestHelper.LoanID.ToString())%>, sLNm);
      }
    }
    function f_isLoanWindowOpen(sLNm) {
      var winOpener = window.opener;
      if (null != winOpener && !winOpener.closed) {
        if (typeof(winOpener.f_isLoanWindowOpen) != "undefined")
          return winOpener.f_isLoanWindowOpen(sLNm);
      }
    }    
    <% if (RequestHelper.GetBool("isnew")) { %>
      var sLId = <%=AspxTools.JsString(RequestHelper.LoanID.ToString()) %>;
      var sLNm = <%=AspxTools.JsString(m_sLNm) %>;
      if (!f_isLoanWindowOpen(sLId)) {
        f_insertWindowToOpenList(sLNm);
      }
    <% } %>

    //-->
    </script>      
  </head>
  <% Guid tmp = RequestHelper.LoanID; %>  
    <frameset rows="25,40,*" frameborder="no" border=0 id="pageframeset">
        <frame name="header" src='HeaderFrame.aspx' scrolling="no" tabindex="-1">
        <frame name="info" src=<%=AspxTools.HtmlAttribute("LeadSummaryFrame.aspx?loanid=" + RequestHelper.LoanID)%> scrolling="no" tabindex=-1">
        <frameset cols="200, *" border=1 frameborder="yes" id="mainframeset">
            <frame name="treeview" src=<%=AspxTools.HtmlAttribute("LeadLeftFrame.aspx?loanid=" + RequestHelper.LoanID)%> tabindex=-1>
            <frame name="body" src=<%=AspxTools.HtmlAttribute(DataAccess.Tools.VRoot + "/newlos/Loading.aspx")%> tabindex=-1>
        </frameset>
    </frameset>
</html>
