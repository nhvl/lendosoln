using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;

namespace LendersOfficeApp.los.Lead
{
	public partial class LeadEditService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        private int sFileVersion
        {
            get { return GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck); }
        }
        private CPageData CreateDataLoan(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(LeadEditService));
        }
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "CalculateAge":
                    CalculateAge();
                    break;
                case "SaveData":
                    SaveData();
                    break;
                case "CalculateData":
                    CalculateData();
                    break;
            }
        }
        private void CalculateAge() 
        {
            try 
            {
                string dob = GetString("dob");

                SetResult("age", Tools.CalcAgeForToday(DateTime.Parse(dob)).ToString());
                
            } 
            catch 
            {
                // DOB is invalid. Use current age value.
                SetResult("age", GetString("age"));
            }
        }
        private void SaveData() 
        {
            Guid loanID = GetGuid("loanid");
            CPageData dataLoan =CreateDataLoan(loanID);
            dataLoan.InitSave(sFileVersion);

            CAppData dataApp = dataLoan.GetAppData(0);

            BindData(dataLoan, dataApp, true);

            dataLoan.Save();

            LoadData(dataLoan, dataApp, true);
        }

        private void CalculateData() 
        {
            Guid loanID = GetGuid("loanid");

            CPageData dataLoan = CreateDataLoan(loanID);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);

            BindData(dataLoan, dataApp, false);

            LoadData(dataLoan, dataApp, false);
        }

        private void BindData(CPageData dataLoan, CAppData dataApp, bool isForSave) 
        {
            dataApp.aBAddr           = GetString("aBAddr");
            dataApp.aBAge_rep        = GetString("aBAge");

            if (!dataLoan.sIsIncomeCollectionEnabled)
            {
                dataApp.aBBaseI_rep = GetString("aBBaseI");
            }

            dataApp.aBBusPhone       = GetString("aBBusPhone");
            dataApp.aBCellPhone      = GetString("aBCellPhone");
            dataApp.aBFax            = GetString("aBFax");
            dataApp.aBCity           = GetString("aBCity");
            dataApp.aBDob_rep        = GetString("aBDob");
            dataApp.aBEmail          = GetString("aBEmail");
            dataApp.aBFirstNm        = GetString("aBFirstNm");
            dataApp.aBHPhone         = GetString("aBHPhone");
            dataApp.aBLastNm         = GetString("aBLastNm");
            dataApp.aBMaritalStatT   = (E_aBMaritalStatT) GetInt("aBMaritalStatT");
            dataApp.aBMidNm          = GetString("aBMidNm");
            dataApp.aBSsn            = GetString("aBSsn");
            dataApp.aBState          = GetString("aBState");
            dataApp.aBSuffix         = GetString("aBSuffix");
            dataApp.aBZip            = GetString("aBZip");
            dataApp.aCAddr           = GetString("aCAddr");
            dataApp.aCAge_rep        = GetString("aCAge");

            if (!dataLoan.sIsIncomeCollectionEnabled)
            {
                dataApp.aCBaseI_rep = GetString("aCBaseI");
            }

            dataApp.aCBusPhone       = GetString("aCBusPhone");
            dataApp.aCCellPhone      = GetString("aCCellPhone");
            dataApp.aCFax            = GetString("aCFax");
            dataApp.aCCity           = GetString("aCCity");
            dataApp.aCDob_rep        = GetString("aCDob");
            dataApp.aCEmail          = GetString("aCEmail");
            dataApp.aCFirstNm        = GetString("aCFirstNm");
            dataApp.aCHPhone         = GetString("aCHPhone");
            dataApp.aCLastNm         = GetString("aCLastNm");
            dataApp.aCMaritalStatT   = (E_aCMaritalStatT) GetInt("aCMaritalStatT");
            dataApp.aCMidNm          = GetString("aCMidNm");
            dataApp.aCSsn            = GetString("aCSsn");
            dataApp.aCState          = GetString("aCState");
            dataApp.aCSuffix         = GetString("aCSuffix");
            dataApp.aCZip            = GetString("aCZip");
            dataLoan.sApprVal_rep    = GetString("sApprVal");
            dataLoan.sDue_rep        = GetString("sDue");
            dataLoan.sEquityCalc_rep = GetString("sEquityCalc");
            dataLoan.sLAmtCalc_rep   = GetString("sLAmtCalc");
            dataLoan.sLAmtLckd       = GetBool("sLAmtLckd");
			if ( isForSave ) // 09/27/06 mf OPM 7485 - Do not bind loan name for calculation
			{
				dataLoan.sLNm            = GetString("sLNm");
			}
            dataLoan.sLPurposeT      = (E_sLPurposeT) GetInt("sLPurposeT");
            dataLoan.sLT             = (E_sLT) GetInt("sLT");
            dataLoan.sLienPosT       = (E_sLienPosT) GetInt("sLienPosT");
            dataLoan.sLpTemplateNm   = GetString("sLpTemplateNm");
            dataLoan.sNoteIR_rep     = GetString("sNoteIR");
            dataLoan.sPurchPrice_rep = GetString("sPurchPrice");
            dataLoan.sSpAddr         = GetString("sSpAddr");
            dataLoan.sSpCity         = GetString("sSpCity");
            dataLoan.sSpState        = GetString("sSpState");
            dataLoan.sSpZip          = GetString("sSpZip");
            dataLoan.sTerm_rep       = GetString("sTerm");
            dataLoan.sTrNotes        = GetString("sTrNotes");
            dataLoan.sLeadSrcDesc    = GetString("sLeadSrcDesc");

        }

        private void LoadData(CPageData dataLoan, CAppData dataApp, bool isForSave) 
        {
            SetResult("sFileVersion", dataLoan.sFileVersion);
            SetResult("aCMaritalStatT", dataApp.aCMaritalStatT);
            SetResult("aBMaritalStatT", dataApp.aBMaritalStatT);
            SetResult("sLPurposeT", dataLoan.sLPurposeT);
            SetResult("sLT", dataLoan.sLT);
            SetResult("sLienPosT", dataLoan.sLienPosT);

            SetResult("aBAddr", dataApp.aBAddr);
            SetResult("aBAge", dataApp.aBAge_rep);
            SetResult("aBBaseI", dataApp.aBBaseI_rep);
            SetResult("aBBusPhone", dataApp.aBBusPhone);
            SetResult("aBCity", dataApp.aBCity);
            SetResult("aBDob", dataApp.aBDob_rep);
            SetResult("aBEmail", dataApp.aBEmail);
            SetResult("aBFirstNm", dataApp.aBFirstNm);
            SetResult("aBHPhone", dataApp.aBHPhone);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("aBMidNm", dataApp.aBMidNm);
            SetResult("aBSsn", dataApp.aBSsn);
            SetResult("aBState", dataApp.aBState);
            SetResult("aBSuffix", dataApp.aBSuffix);
            SetResult("aBZip", dataApp.aBZip);
            SetResult("aCAddr", dataApp.aCAddr);
            SetResult("aCAge", dataApp.aCAge_rep);
            SetResult("aCBaseI", dataApp.aCBaseI_rep);
            SetResult("aCBusPhone", dataApp.aCBusPhone);
            SetResult("aCCity", dataApp.aCCity);
            SetResult("aCDob", dataApp.aCDob_rep);
            SetResult("aCEmail", dataApp.aCEmail);
            SetResult("aCFirstNm", dataApp.aCFirstNm);
            SetResult("aCHPhone", dataApp.aCHPhone);
            SetResult("aCLastNm", dataApp.aCLastNm);
            SetResult("aCMidNm", dataApp.aCMidNm);
            SetResult("aCSsn", dataApp.aCSsn);
            SetResult("aCState", dataApp.aCState);
            SetResult("aCSuffix", dataApp.aCSuffix);
            SetResult("aCZip", dataApp.aCZip);
            SetResult("sApprVal", dataLoan.sApprVal_rep);
            SetResult("sDownPmtPc", dataLoan.sDownPmtPc_rep);
            SetResult("sDue", dataLoan.sDue_rep);
            SetResult("sEquityCalc", dataLoan.sEquityCalc_rep);
            SetResult("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            SetResult("sLAmtLckd", dataLoan.sLAmtLckd);
			if ( isForSave ) // 09/27/06 mf OPM 7485 - Only set loan name if save operation
			{
				SetResult("sLNm", dataLoan.sLNm);
			}
            SetResult("sLpTemplateNm", dataLoan.sLpTemplateNm);
            SetResult("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            SetResult("sNoteIR", dataLoan.sNoteIR_rep);
            SetResult("sPurchPrice", dataLoan.sPurchPrice_rep);
            SetResult("sSpAddr", dataLoan.sSpAddr);
            SetResult("sSpCity", dataLoan.sSpCity);
            SetResult("sSpState", dataLoan.sSpState);
            SetResult("sSpZip", dataLoan.sSpZip);
            SetResult("sTerm", dataLoan.sTerm_rep);
            SetResult("sTrNotes", dataLoan.sTrNotes);

            SetResult("sCltvR", dataLoan.sCltvR_rep);
            SetResult("sLtvR", dataLoan.sLtvR_rep);
            SetResult("sQualTopR", dataLoan.sQualTopR_rep);
            SetResult("sQualBottomR", dataLoan.sQualBottomR_rep);
        }

	}
}
