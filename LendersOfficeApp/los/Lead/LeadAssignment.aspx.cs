namespace LendersOfficeApp.los.Lead
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Security;

    public partial class LeadAssignment : LendersOfficeApp.newlos.BaseLoanPage
	{
        protected System.Web.UI.WebControls.RadioButton ConvertOption0RB;
        protected System.Web.UI.WebControls.RadioButton ConvertOption1RB;
        protected System.Web.UI.WebControls.RadioButton RemoveLeadPrefixRB;
        protected System.Web.UI.WebControls.RadioButton GenerateNewLoanNumberRB;

        /// <summary>
        /// Gets additional workflow operations that need checks for the page.
        /// </summary>
        /// <returns>
        /// The list of additional workflow operations that need checks for the page.
        /// </returns>
        protected override WorkflowOperation[] GetExtraOpsToCheck()
        {
            return new[] { WorkflowOperations.LeadToLoanConversion };
        }

        /// <summary>
        /// Gets the additional workflow operations that need checks for the page
        /// for subsequent message retrieval.
        /// </summary>
        /// <returns>
        /// The list of messages for additional workflow operations that need checks for the page.
        /// </returns>
        protected override WorkflowOperation[] GetReasonsForMissingPriv()
        {
            return new[] { WorkflowOperations.LeadToLoanConversion };
        }

        protected bool m_isAssignToManagerOnly = false;

		private bool IsAssignToManager() 
		{
			// If the user only has "Telemarketer" role and
			// the broker setting "OptionTelemarketerCanOnlyAssignToManager" is true
			// then user can only assign loan to manager.
			// Any other role can assign to other processor, agent and/or manager.

            BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;
			if (brokerUser.HasAtLeastOneRole(new E_RoleT[] { E_RoleT.Manager, E_RoleT.Processor, E_RoleT.LoanOfficer}))
			{
				return false;
			} 
			else if (brokerUser.HasRole(E_RoleT.CallCenterAgent))
			{
				BrokerDB broker = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                return broker.OptionTelemarketerCanOnlyAssignToManager;

			}
			return false;
		}

        protected bool IsEditLeadsInFullLoanEditor
        {
            get { return Broker.IsEditLeadsInFullLoanEditor; }
        }

        protected bool IsLeadMadeFromBlankOrQPTemplate
        {
            get;
            set;
        }

        protected bool IsForceLeadToLoanTemplate
        {
            get { return Broker.IsForceLeadToLoanTemplate; }
        }

        protected bool IsForceLeadToLoanNewLoanNumber
        {
            get { return Broker.IsForceLeadToLoanNewLoanNumber; }
        }

        protected bool IsForceLeadToLoanRemoveLeadPrefix
        {
            get { return Broker.IsForceLeadToLoanRemoveLeadPrefix; }
        }

        protected bool IsForceLeadToUseLoanCounter
        {
            get { return Broker.IsForceLeadToUseLoanCounter; }
        }

        protected bool HasArchives
        {
            get
            {
                if (Broker.IsGFEandCoCVersioningEnabled == false)
                {
                    return false;
                }

                CPageData dataLoan;
                if (m_dataLoan == null)
                {
                    dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LeadAssignment));
                    dataLoan.InitLoad();
                }
                else
                {
                    dataLoan = m_dataLoan;
                }

                if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    return dataLoan.GFEArchives.Any();
                }
                else
                {
                    return dataLoan.sClosingCostArchive.Any();
                }
            }
        }

        private CPageData m_dataLoan;

		protected void PageInit(object sender, System.EventArgs e) 
		{
			this.PageID = "LeadAssignment";
			this.PageTitle = "Lead Assignment";
            RegisterService("agentsService", "/newlos/Status/AgentsService.aspx");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("LQBPopup.js");
            EnableJqueryMigrate = false;
            bool filterByBranch = true;

            if (BrokerUser.HasPermission(Permission.AllowAccessToTemplatesOfAnyBranch))
                filterByBranch = false;

            SqlParameter[] parameters = {
                                            new SqlParameter( "@BrokerId"       , BrokerUser.BrokerId   )
                                            , new SqlParameter( "@BranchId"       , BrokerUser.BranchId   )
                                            , new SqlParameter( "@EmployeeId"     , BrokerUser.EmployeeId )
                                            , new SqlParameter( "@FilterByBranch" , filterByBranch         )
                                        };

			DataSet dS = new DataSet();
			DataSetHelper.Fill( dS, BrokerUser.BrokerId, "RetrieveLoanTemplateByBrokerID", parameters );

			// 10/16/06 OPM 7843 mf.  We now sort the list here instead of in query
			if( dS.Tables.Count > 0 )
			{
				dS.Tables[ 0 ].DefaultView.Sort = "sLNm ASC";
                var view = dS.Tables[0].DefaultView;

                m_dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LeadAssignment));
                m_dataLoan.AllowLoadWhileQP2Sandboxed = true;
                m_dataLoan.InitLoad();

                if (!m_dataLoan.sIsRefinancing)
                {
                    view.RowFilter = "sLPurposeT = 0";
                }
                else
                {
                    view.RowFilter = "sLPurposeT = 1" // Refin
                                   + " OR sLPurposeT = 2" // RefinCashout
                                   + " OR sLPurposeT = 6" // FHA Streamline Refi
                                   + " OR sLPurposeT = 7" // VaIrrrl
                                   + " OR sLPurposeT = 8"; // Home Equity
                }

                TemplateDDL.DataSource = view;
				TemplateDDL.DataTextField = "sLNm";
				TemplateDDL.DataValueField = "sLId";
				TemplateDDL.DataBind();
			}

            TemplateDDL.Items.Insert(0, new ListItem("<-- Choose a template -->", Guid.Empty.ToString()));
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			m_isAssignToManagerOnly = IsAssignToManager();
			// Put user code to initialize the page here
            
            CPageData dataLoan;
            if (m_dataLoan == null)
            {
                dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LeadAssignment));
                dataLoan.InitLoad();
            }
            else
            {
                dataLoan = m_dataLoan;
            }

            this.RegisterLeadConversionVariables();
            this.IsLeadMadeFromBlankOrQPTemplate = dataLoan.sSrcsLId == Guid.Empty || dataLoan.sSrcsLId == this.Broker.QuickPricerTemplateId;

			sStatusT_rep.Text = dataLoan.sStatusT_rep;
			ClientScript.RegisterHiddenField("sStatusT", dataLoan.sStatusT.ToString("D"));

            if (dataLoan.sIsConsumerPortalCreatedButNotSubmitted)
            {
                phConsumerPortalVerbalSubmissionInfo.Visible = true;
                sConsumerPortalUserMadeVerbalSubmission.Checked = dataLoan.sConsumerPortalVerbalSubmissionD.IsValid;
                sConsumerPortalVerbalSubmissionD.Text = dataLoan.sConsumerPortalVerbalSubmissionD_rep;
                sConsumerPortalVerbalSubmissionComments.Text = dataLoan.sConsumerPortalVerbalSubmissionComments;
            }

			BindAgentLink(sEmployeeLoanRep, dataLoan.sEmployeeLoanRep);
			BindAgentLink(sEmployeeProcessor, dataLoan.sEmployeeProcessor);
			BindAgentLink(sEmployeeLoanOpener, dataLoan.sEmployeeLoanOpener);
			BindAgentLink(sEmployeeCallCenterAgent, dataLoan.sEmployeeCallCenterAgent);
			BindAgentLink(sEmployeeRealEstateAgent, dataLoan.sEmployeeRealEstateAgent);
			BindAgentLink(sEmployeeLenderAccExec, dataLoan.sEmployeeLenderAccExec);
			BindAgentLink(sEmployeeLockDesk, dataLoan.sEmployeeLockDesk);
			BindAgentLink(sEmployeeUnderwriter, dataLoan.sEmployeeUnderwriter);
			BindAgentLink(sEmployeeManager, dataLoan.sEmployeeManager);
            BindAgentLink(sEmployeeCloser, dataLoan.sEmployeeCloser);
            BindAgentLink(sEmployeeShipper, dataLoan.sEmployeeShipper);
            BindAgentLink(sEmployeeFunder, dataLoan.sEmployeeFunder);
            BindAgentLink(sEmployeePostCloser, dataLoan.sEmployeePostCloser);
            BindAgentLink(sEmployeeInsuring, dataLoan.sEmployeeInsuring);
            BindAgentLink(sEmployeeCollateralAgent, dataLoan.sEmployeeCollateralAgent);
            BindAgentLink(sEmployeeDocDrawer, dataLoan.sEmployeeDocDrawer);
            BindAgentLink(sEmployeeCreditAuditor, dataLoan.sEmployeeCreditAuditor);
            BindAgentLink(sEmployeeDisclosureDesk, dataLoan.sEmployeeDisclosureDesk);
            BindAgentLink(sEmployeeJuniorProcessor, dataLoan.sEmployeeJuniorProcessor);
            BindAgentLink(sEmployeeJuniorUnderwriter, dataLoan.sEmployeeJuniorUnderwriter);
            BindAgentLink(sEmployeeLegalAuditor, dataLoan.sEmployeeLegalAuditor);
            BindAgentLink(sEmployeeLoanOfficerAssistant, dataLoan.sEmployeeLoanOfficerAssistant);
            BindAgentLink(sEmployeePurchaser, dataLoan.sEmployeePurchaser);
            BindAgentLink(sEmployeeQCCompliance, dataLoan.sEmployeeQCCompliance);
            BindAgentLink(sEmployeeSecondary, dataLoan.sEmployeeSecondary);
            BindAgentLink(sEmployeeServicing, dataLoan.sEmployeeServicing);
		}

        private void RegisterLeadConversionVariables()
        {
            this.RegisterJsGlobalVariables("UserHasLeadConversionPrivilege", this.UserHasWorkflowPrivilege(WorkflowOperations.LeadToLoanConversion));
            this.RegisterJsGlobalVariables("ConvertLeadToLoanBlockMessage", this.GetReasonUserCannotPerformPrivilege(WorkflowOperations.LeadToLoanConversion));
        }

        private void BindAgentLink(HtmlAnchor link, CEmployeeFields employee) 
        {
            if (!string.IsNullOrWhiteSpace(employee.FullName) || !string.IsNullOrWhiteSpace(employee.Email)) 
            {
                link.InnerText = employee.FullName;
                link.HRef = string.Format(@"mailto:{0}", AspxTools.HtmlString(employee.Email));
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            this.UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
