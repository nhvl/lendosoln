namespace LendersOfficeApp.los.Lead
{
    using System;
    using DataAccess;
    using LendersOffice.Constants;

    public partial class LeadAssignmentService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private int sFileVersion
        {
            get { return GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck); }
        }
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SaveData":
                    SaveData();
                    break;
                case "ConvertToLoan":
                    ConvertToLoan();
                    break;
            }
        }

        private void ConvertToLoan()
        {
            var parameters = new LeadConversionParameters();
            parameters.LeadId = GetGuid("LoanID");
            parameters.FileVersion = sFileVersion;
            parameters.TemplateId = GetGuid("sLpTemplateId");
            parameters.CreateFromTemplate = GetInt("option", 0) == 1;
            parameters.RemoveLeadPrefix = GetBool("removeLeadPrefix");
            parameters.ConsumerPortalUserMadeVerbalSubmission = GetBool("sConsumerPortalUserMadeVerbalSubmission", false);
            parameters.ConsumerPortalVerbalSubmissionDate = GetString("sConsumerPortalVerbalSubmissionD", string.Empty);
            parameters.ConsumerPortalVerbalSubmissionComments = GetString("sConsumerPortalVerbalSubmissionComments", string.Empty);
            parameters.IsNonQm = false;

            var results = LeadConversion.ConvertLeadToLoan_Lqb(LendersOffice.Security.PrincipalFactory.CurrentPrincipal, parameters);

            if (!string.IsNullOrEmpty(results.MersErrors))
            {
                SetResult("MersErrors", results.MersErrors);
            }

            if (!string.IsNullOrEmpty(results.Action))
            {
                SetResult("Action", results.Action);
            }

            if (results.ConvertedLoanId != Guid.Empty)
            {
                SetResult("LoanId", results.ConvertedLoanId);
            }
        }

        private void SaveData()
        {
            Guid loanID = GetGuid("loanid");

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanID, typeof(LeadAssignmentService));
            dataLoan.InitSave(sFileVersion);

            E_sStatusT sStatusT = (E_sStatusT)GetInt("sStatusT");
            if (dataLoan.sStatusT != sStatusT)
            {
                string dt = DateTime.Now.ToShortDateString();
                dataLoan.sStatusT = sStatusT;
                switch (sStatusT)
                {
                    case E_sStatusT.Loan_Open:
                        dataLoan.sOpenedD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Prequal:
                        dataLoan.sPreQualD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Docs:
                        dataLoan.sDocsD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Preapproval:
                        dataLoan.sPreApprovD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Registered:
                        dataLoan.sSubmitD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Approved:
                        dataLoan.sApprovD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Funded:
                        dataLoan.sFundD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Recorded:
                        dataLoan.sRecordedD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Closed:
                        dataLoan.sClosedD_rep = dt;
                        break;
                    case E_sStatusT.Loan_LoanSubmitted:
                        dataLoan.sLoanSubmittedD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Underwriting:
                        dataLoan.sUnderwritingD_rep = dt;
                        break;
                    case E_sStatusT.Loan_OnHold:
                        dataLoan.sOnHoldD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Suspended:
                        dataLoan.sSuspendedD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Canceled:
                        dataLoan.sCanceledD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Rejected:
                        dataLoan.sRejectD_rep = dt;
                        break;
                    case E_sStatusT.Lead_New:
                        dataLoan.sLeadD_rep = dt;
                        break;
                    case E_sStatusT.Lead_Canceled:
                        dataLoan.sLeadCanceledD_rep = dt;
                        break;
                    case E_sStatusT.Lead_Other:
                        dataLoan.sLeadOtherD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Shipped:
                        dataLoan.sShippedToInvestorD_rep = dt;
                        break;
                    case E_sStatusT.Lead_Declined:
                        dataLoan.sLeadDeclinedD_rep = dt;
                        break;
                    case E_sStatusT.Loan_PreProcessing:    // start sk 1/6/2014 opm 145251
                        dataLoan.sPreProcessingD_rep = dt;
                        break;
                    case E_sStatusT.Loan_DocumentCheck:
                        dataLoan.sDocumentCheckD_rep = dt;
                        break;
                    case E_sStatusT.Loan_DocumentCheckFailed:
                        dataLoan.sDocumentCheckFailedD_rep = dt;
                        break;
                    case E_sStatusT.Loan_PreUnderwriting:
                        dataLoan.sPreUnderwritingD_rep = dt;
                        break;
                    case E_sStatusT.Loan_ConditionReview:
                        dataLoan.sConditionReviewD_rep = dt;
                        break;
                    case E_sStatusT.Loan_PreDocQC:
                        dataLoan.sPreDocQCD_rep = dt;
                        break;
                    case E_sStatusT.Loan_DocsOrdered:
                        dataLoan.sDocsOrderedD_rep = dt;
                        break;
                    case E_sStatusT.Loan_DocsDrawn:
                        dataLoan.sDocsDrawnD_rep = dt;
                        break;
                    case E_sStatusT.Loan_InvestorConditions:       // this is "promoting the dt to a status". tied to sSuspendedByInvestorD
                        dataLoan.sSuspendedByInvestorD_rep = dt;
                        break;
                    case E_sStatusT.Loan_InvestorConditionsSent:   // this is "promoting the dt to a status". tied to sCondSentToInvestorD
                        dataLoan.sCondSentToInvestorD_rep = dt;
                        break;
                    case E_sStatusT.Loan_ReadyForSale:
                        dataLoan.sReadyForSaleD_rep = dt;
                        break;
                    case E_sStatusT.Loan_SubmittedForPurchaseReview:
                        dataLoan.sSubmittedForPurchaseReviewD_rep = dt;
                        break;
                    case E_sStatusT.Loan_InPurchaseReview:
                        dataLoan.sInPurchaseReviewD_rep = dt;
                        break;
                    case E_sStatusT.Loan_PrePurchaseConditions:
                        dataLoan.sPrePurchaseConditionsD_rep = dt;
                        break;
                    case E_sStatusT.Loan_SubmittedForFinalPurchaseReview:
                        dataLoan.sSubmittedForFinalPurchaseD_rep = dt;
                        break;
                    case E_sStatusT.Loan_InFinalPurchaseReview:
                        dataLoan.sInFinalPurchaseReviewD_rep = dt;
                        break;
                    case E_sStatusT.Loan_ClearToPurchase:
                        dataLoan.sClearToPurchaseD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Purchased:        // don't confuse with the old case E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
                        dataLoan.sPurchasedD_rep = dt;
                        break;
                    case E_sStatusT.Loan_CounterOffer:
                        dataLoan.sCounterOfferD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Withdrawn:
                        dataLoan.sWithdrawnD_rep = dt;
                        break;
                    case E_sStatusT.Loan_Archived:
                        dataLoan.sArchivedD_rep = dt;
                        break;


                }
            }

            dataLoan.sStatusT = (E_sStatusT)GetInt("sStatusT");

            dataLoan.sConsumerPortalVerbalSubmissionD_rep = string.Empty;
            dataLoan.sConsumerPortalVerbalSubmissionComments = string.Empty;

            if (dataLoan.sConsumerPortalCreationD.IsValid && false == dataLoan.sConsumerPortalSubmittedD.IsValid)
            {
                bool sConsumerPortalUserMadeVerbalSubmission = GetBool("sConsumerPortalUserMadeVerbalSubmission");
                if (sConsumerPortalUserMadeVerbalSubmission)
                {
                    dataLoan.sConsumerPortalVerbalSubmissionD_rep = GetString("sConsumerPortalVerbalSubmissionD");
                    dataLoan.sConsumerPortalVerbalSubmissionComments = GetString("sConsumerPortalVerbalSubmissionComments");
                }
            }

            dataLoan.Save();
        }
    }
}