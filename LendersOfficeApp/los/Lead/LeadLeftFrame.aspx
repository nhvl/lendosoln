﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import namespace="LendersOffice.Security" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Page language="c#" Codebehind="LeadLeftFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Lead.LeadLeftFrame" %>
<html>
	<head runat="server">
		<title>LeadLeftFrame</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

		    <style type="text/css">
        ul.ui-autocomplete { font-family: Arial, Helvetica, sans-serif; font-size: 10px;}
        div { white-space: nowrap; }
        body { overflow-x: hidden; }
        .ui-widget { font-size: 10px; font-family: Arial, Helvetica, sans-serif; }
        #searchlbl img {  border:none; } 
        #Search { margin: 4px 4px; }
        .loadsQuickReply { padding-left:5px; width:11px;}
        .loadsQuickReply:hover { cursor:pointer; width:12px; }
        #pageNavigation .dynatree-container { background-color: gray; color: white;}

        #pageNavigation .dynatree-container a {
            border: none;
            color: white;
        }

        #pageNavigation .dynatree-folder a {
            color: gold;
        }

        #pageNavigation span.dynatree-active a, #pageNavigation ul.dynatree-container a:hover, #pageNavigation span.dynatree-focused a:link {
            background-color: transparent;
            border: none;
        }
    </style>
    </head>
    
		
	<body class="LeftBackground" MS_POSITIONING="FlowLayout">
      <asp:PlaceHolder runat="server" ID="LoadsQuickReplyInjector">
      <script type="text/javascript">
        var QRI;
          $(document).ready( function () {
              QRI = QuickReplyInjector(
                  <%=AspxTools.JsString(Tools.VRoot)%>,
                  <%=AspxTools.JsString(RequestHelper.LoanID)%>,
                  <%=AspxTools.JsString(BasePage.ReadonlyIncludeVersion)%>,
                  $('.loadsQuickReply'), 
                  parent.frames['body']);

          });
      </script>
      </asp:PlaceHolder>
	  <div class="ui-widget" style="position: fixed; top: 0; background-color: gray;">
	    <input id="Search" name="Search"> 
        <a id="searchlbl" href="#"><img  src="../../images/edocs/magnify.png" title="Go To Page" style=" vertical-align: middle;" /></a>
    </div>
    <br />
    <br />
		<form id="LeadLeftFrame" method="post" runat="server">
       <asp:HiddenField runat="server" ID="DocListUrl" />
		
			<ml:PassthroughLiteral id="m_tree" runat="server" enableviewstate="False"></ml:PassthroughLiteral>
			<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></form>
			<script language="javascript">
    <!--

    <%-- When updating these mappings, also update the mappings in LeftTreeFrame.aspx --%>
    var uladPagesMappingsByPageName = {
        'Assets': { Legacy: 'BorrowerInfo.aspx?pg=5', Ulad: 'Ulad/Assets.aspx', CollectionVersion: 1 },
        'ApplicationManagement': { Legacy: 'BorrowerList.aspx', Ulad: 'Ulad/ApplicationManagement.aspx', CollectionVersion: 1 },
        'MonthlyIncome': { Legacy: 'BorrowerInfo.aspx?pg=3', Ulad: 'Ulad/IncomeSources.aspx', CollectionVersion: 2 },
        'REO': { Legacy: 'BorrowerInfo.aspx?pg=6', Ulad: 'Ulad/RealProperties.aspx', CollectionVersion: 1 },
        'Liabilities': { Legacy: 'BorrowerInfo.aspx?pg=4', Ulad: 'Ulad/Liabilities.aspx', CollectionVersion: 1 }
    };

    function loadUladPage(pageName, appId) {
        var pageMappings = uladPagesMappingsByPageName[pageName];
        if (pageMappings == null) {
            throw 'Unable to open page ' + pageName;
        }

        if (pageMappings.CollectionVersion && pageMappings.CollectionVersion <= ML.sBorrowerApplicationCollectionT) {
            return load(pageMappings.Ulad, true);
        }
        else {
            var appParam = '';
            if (appId != null) {
                appParam = pageMappings.Legacy.indexOf('?') == -1 ? '?' : '&';
                appParam += 'appid=' + appId;
            }

            return load(pageMappings.Legacy + appParam, true);
        }
    }


    function f_refreshAfterTimeout(timeoutInMilliseconds) {
        window.setTimeout(function() {
            this.location = this.location;
        }, timeoutInMilliseconds);
    }

    var edocWin;
    function f_viewDocList() {
    var body_url = document.getElementById('DocListUrl').value;
    if( edocWin == null || edocWin.closed) 
    {
        edocWin = openWindowWithArguments( body_url, "_blank", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=950,height=600");
    }
    else {
        edocWin.location.reload();
    }
    edocWin.focus();
    return false;
}
var edocWin2 = null;
function f_viewBatchDocList(){
    var body_url = <%=AspxTools.SafeUrl(Tools.GetEDocsLink(Tools.VRoot +"/newlos/ElectronicDocs/EditEdocV2.aspx?loanid="+RequestHelper.LoanID+"&key=all"))%>;
    if( edocWin2 == null || edocWin2.closed) 
    {
        edocWin2 = openWindowWithArguments( body_url, "_blank", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,scrollbars=yes,width=950,height=600");
    }
    else {
        edocWin2.location.reload();
    }
    edocWin2.focus();
    return false;
}
	// Search-n-replace addReminder
	function createNewTask()
    {
		showModal( '/los/reminders/TaskEditor.aspx?loanId=' + <%= AspxTools.JsString(RequestHelper.LoanID.ToString()) %> + '&hideViewEditLoan=t' );
	}

	function editReminder( notifId )
	{
		showModal( "/los/reminders/TaskEditor.aspx?notifId=" + notifId + "&hideViewEditLoan=t" );
	}
	    
      var gPreviousPageID = null;
      function selectPageID(id) {

        if (id == gPreviousPageID) return; 
  
        var coll = document.getElementsByTagName('a');
        var length = coll.length;
        for (var i = 0; i < length; i++) {
          var o = coll[i];
          if (o.getAttribute("pageid") == id) {
            o.style.backgroundColor = 'red';
            o.style.color='white';
            o.style.fontWeight='bold';
            __expandAndScrollVisible(o);
          }
          if (null != gPreviousPageID && o.getAttribute("pageid") == gPreviousPageID) {
            o.style.backgroundColor = '';
            o.style.color = '';
            o.style.fontWeight='';
          }
        }
      gPreviousPageID = id;
    }

    function findPageID(id) {
      var coll = document.getElementsByTagName("A");
      var length = coll.length;
      for (var i = 0; i < length; i++) {
        var o = coll[i];
        if (o.pageid == id)
          return o;
      }
  
      return null;
    }      
function load(href, dontUseLeadHref) {
    var ch = href.indexOf('?') > 0 ? '&' : '?';
    if (dontUseLeadHref == null) {
        var url = <%= AspxTools.JsString(VirtualRoot) %> + '/los/lead/' + href + ch + 'loanid=' + <%= AspxTools.JsString(RequestHelper.LoanID.ToString()) %> + '&printid=' + encodeURIComponent(parent.body.f_getPrintID()) + '&isLead=t';
    }
    else {
        var url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/' + href + ch + 'loanid=' + <%= AspxTools.JsString(RequestHelper.LoanID.ToString()) %> + '&printid=' + encodeURIComponent(parent.body.f_getPrintID()) + '&isLead=t';
    }
  if (typeof(parent.body.f_load) == 'function')
    parent.body.f_load(url);
  else parent.body.location = url + '&appid=' + encodeURIComponent(parent.info.f_getCurrentApplicationID()) + '&printid=' + encodeURIComponent(parent.body.f_getPrintID()) + '&isLead=t';

  loadConvLogIfNeeded();
  return false;
}

function loadConvLogIfNeeded()
{
	if(typeof QRI !== 'undefined')
	{
		if (typeof QRI.injectWhenIframeReady === 'function')
		{
			QRI.injectWhenIframeReady();
		}
	}        
}

function loadRaw(href) {

  var ch = href.indexOf('?') > 0 ? '&' : '?';
  var url = <%= AspxTools.JsString(VirtualRoot) %> + href + ch + 'loanid=' + <%= AspxTools.JsString(RequestHelper.LoanID.ToString()) %> + '&isLead=t';
  if (typeof(parent.body.f_load) == 'function')
    parent.body.f_load(url);
  else parent.body.location = url + '&appid=' + encodeURIComponent(parent.info.f_getCurrentApplicationID()) + '&isLead=t';

  loadConvLogIfNeeded();
  return false;

}
function displayCreditReport()
{
	var report_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/services/ViewCreditFrame.aspx?loanid=' + <%= AspxTools.JsString(RequestHelper.LoanID.ToString()) %> + '&applicationid=' + encodeURIComponent(parent.info.f_getCurrentApplicationID());	
	var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no") ;
	win.focus() ;
	
	return false;

}
function displayDUFindings() {
    var report_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/services/ViewDUFindingFrame.aspx?loanid=' + <%= AspxTools.JsString(RequestHelper.LoanID.ToString()) %>;
    var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600");
    win.focus();

    return false;

}
function displayLPFeedback() {
    var report_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/services/ViewLPFeedbackFrame.aspx?loanid=' + <%= AspxTools.JsString(RequestHelper.LoanID.ToString()) %>;
    var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600");
    win.focus();

    return false;

}
function displayFhaTotalFindings() {
    var report_url = <%= AspxTools.JsString(VirtualRoot) %> + '/newlos/services/FHATotalFindings.aspx?loanid=' + <%= AspxTools.JsString(RequestHelper.LoanID.ToString()) %>;
    var win = openWindowWithArguments(report_url, "report", "toolbar=no,menubar=no,location=no,status=no,resizable=yes,width=780,height=600");
    win.focus();
    return false;
}
function onResourcesClick() {
    window.open(document.getElementById("EmployeeResourcesUrl").value,
                'Resources', 
                "center=yes,resizable=yes,scrollbars=yes,status=yes,help=no");
    return false;       
}

function f_customEventKeyup(event) {
  if (event.ctrlKey && event.altKey && event.keyCode == 73) {
    <%-- Ctrl+Alt+I to display debug page for External Bug tool --%>
    if(<%=AspxTools.JsBool(IsInternalUser)%>)
    {
        f_displayDebugDu();
    }
  }
}

function f_displayDebugDu() {
    loadRaw('/newlos/test/testviewxisresponse.aspx');
}


      $(function() {

          $(document).keyup(f_customEventKeyup);

          var data = [];
          var dupeCheck = {};
          $('a.TreeNodeLink').each(function(i, o) {
              var $a = $(this), txt = $.trim($a.text()), $cDiv = $a.parents('.dynatree-folder + ul');
              $a.prop('title', txt);


              $cDiv.each(function() {
                  var $prev = $(this).prev();
                  txt = $prev.text() + '/' + txt;
              });

              if (dupeCheck.hasOwnProperty(txt)) {
                  return;
              }

              data.push({
                  'label': txt,
                  'anchor': $a
              });

              dupeCheck[txt] = true;
          });

          delete dupeCheck;

          $("#Search").autocomplete({
              source: data,
              select: function(e, u) {
                  u.item.anchor.click();
              },
              position: { my: 'left top', at: 'left bottom', of: '#Search' }
          });

          $('#searchlbl').click(function() {
              var el = $('#Search'), txt = el.val(), i = 0, items = data, count = items.length, obj;
              if (txt.length > 0) {
                  for (i; i < count; i++) {
                      obj = items[i];
                      if (obj.label === txt) {
                          obj.anchor.click();
                          return false;
                      }
                  }
              }
          });
          
          initFavorites(false);

      });
      
      
    //-->
            </script>
	</body>
</html>
