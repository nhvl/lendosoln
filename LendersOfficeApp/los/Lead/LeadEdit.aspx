<%@ Import Namespace="LendersOffice.Common"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="LeadEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Lead.LeadEdit" %>
<%@ Import Namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LeadEdit</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
	</HEAD>
	<body bgColor="gainsboro" MS_POSITIONING="FlowLayout">
		<script language="javascript">
    <!--
    var bFirstLoad = true;

    function doAfterDateFormat(object)
	{
		if(object.id == "aBDob")
		{
		calculateAge(object, "aBAge");
		}
		else if (object.id == "aCDob")
		{
			calculateAge(object, "aCAge");
		}

	}


    function calculateAge(dob, age) {
      var args = new Object();
      args["dob"] = dob.value;
      args["age"] = document.getElementById(age).value;
      var result = gService.loanedit.call("CalculateAge", args);
      if (!result.error) {
        document.getElementById(age).value = result.value["age"];
      }
    }
  function _onError(errMsg) {
    if (errMsg == <%=AspxTools.JsString(JsMessages.LoanInfo_DuplicateLoanNumber)%>) {
      alert(<%=AspxTools.JsString(JsMessages.LoanInfo_DuplicateLoanNumber)%>);

      var val = <%= AspxTools.JsGetElementById(sLNm_Validator) %>;
      val.isvalid = false;
      ValidatorUpdateDisplay(val);

      updateDirtyBit();

    }
  }
  function copyFieldTo(field1Id,field2Id)
  {
		document.getElementById(field2Id).value = document.getElementById(field1Id).value;

  }
  function onCopyBorrowrerInfo()
  {
		copyFieldTo("<%= AspxTools.ClientId(aBLastNm) %>","<%= AspxTools.ClientId(aCLastNm) %>");
		copyFieldTo("<%= AspxTools.ClientId(aBMaritalStatT) %>", "<%= AspxTools.ClientId(aCMaritalStatT) %>");
		copyFieldTo("<%= AspxTools.ClientId(aBAddr) %>","<%= AspxTools.ClientId(aCAddr) %>");
		copyFieldTo("<%= AspxTools.ClientId(aBCity) %>","<%= AspxTools.ClientId(aCCity) %>");
		copyFieldTo("<%= AspxTools.ClientId(aBState) %>","<%= AspxTools.ClientId(aCState) %>");
		copyFieldTo("<%= AspxTools.ClientId(aBZip) %>","<%= AspxTools.ClientId(aCZip) %>");
		copyFieldTo("<%= AspxTools.ClientId(aBHPhone) %>","<%= AspxTools.ClientId(aCHPhone) %>");
  }
  function _init() {
    if (bFirstLoad) {
      if (document.getElementById("FrameTitle") != null)
        parent.info.updateFrameTitle(document.getElementById("FrameTitle").value);
      bFirstLoad = false;
    }
    var bPurchase = <%= AspxTools.JsGetElementById(sLPurposeT) %>.value == <%=AspxTools.JsString(E_sLPurposeT.Purchase.ToString("D"))%>;
    <%= AspxTools.JsGetElementById(sPurchPrice) %>.readOnly = !bPurchase;
    <%= AspxTools.JsGetElementById(sLAmtLckd) %>.disabled = !bPurchase || ML.IsRenovationLoan;
	  if( <%= AspxTools.JsGetElementById(sLAmtLckd) %>.checked )
	  {
		  <%= AspxTools.JsGetElementById(sDownPmtPc) %>.readOnly = true;
		  <%= AspxTools.JsGetElementById(sEquityCalc) %>.readOnly = true;
		  <%= AspxTools.JsGetElementById(sLAmtCalc) %>.readOnly = false;
	  }
	  else
	  {
	  	<%= AspxTools.JsGetElementById(sDownPmtPc) %>.readOnly = false;
		  <%= AspxTools.JsGetElementById(sEquityCalc) %>.readOnly = false;
		  <%= AspxTools.JsGetElementById(sLAmtCalc) %>.readOnly = true;
	  }

  }
  function f_copyFromPresentAddress() {
    <%= AspxTools.JsGetElementById(sSpAddr) %>.value = <%= AspxTools.JsGetElementById(aBAddr) %>.value;
    <%= AspxTools.JsGetElementById(sSpCity) %>.value = <%= AspxTools.JsGetElementById(aBCity) %>.value;
    <%= AspxTools.JsGetElementById(sSpState) %>.value = <%= AspxTools.JsGetElementById(aBState) %>.value;
    <%= AspxTools.JsGetElementById(sSpZip) %>.value = <%= AspxTools.JsGetElementById(aBZip) %>.value;
    updateDirtyBit();
  }
  function _postRefreshCalculation(o) {
    parent.info.f_updateRatioMannually(o.sLtvR, o.sCltvR, o.sQualTopR, o.sQualBottomR);
  }
  function checkDuplicateLoan(source, args) {
  }
  function on_sDownPmtPc_change(event) {

    var p = parseMoneyFloat(document.getElementById("<%= AspxTools.ClientId(sPurchPrice) %>").value);
    var d = parseFloat(document.getElementById("<%= AspxTools.ClientId(sDownPmtPc) %>").value);
    var e = ((d / 100.0) * p) + 0.005;
    var ctl = document.getElementById("<%= AspxTools.ClientId(sEquityCalc) %>");
    ctl.value = e;
    format_money(ctl);
		updateDirtyBit(event);
		refreshCalculation();
  }
function onDateStampClick() {
  var dt = new Date();
  var hr = dt.getHours();
  var ampm = "AM";
  if (hr == 0) {
    hr = 12;
  } else if (hr == 12) {
    ampm = "PM";
  } else if (hr > 12) {
    hr = hr - 12;
    ampm = "PM";
  }

  var min = dt.getMinutes();
  var str_min = '';
  if (min == 0) {
    str_min = '00';
  } else if (min < 10) {
    str_min = '0' + min;
  } else {
    str_min = min;
  }

  var str = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear() + " " + hr + ":" + str_min + " " + ampm;
  <%= AspxTools.JsGetElementById(sTrNotes) %>.value = str + " - \n" + <%= AspxTools.JsGetElementById(sTrNotes) %>.value;
  updateDirtyBit();
}
<% if (m_optionTelemarketerCanRunPrequal) { %>
	<% if (m_isPricingEnabled) { %>
  function f_runLPE() {
    linkMe(<%= AspxTools.JsString(VirtualRoot) %> + '/newlos/Template/RunEmbeddedPML.aspx?isLead=t');
  }
  <% } %>
  function f_runNonLPE() {
    // sLienPostT =
    linkMe(<%= AspxTools.JsString(VirtualRoot) %> + '/newlos/Template/QualifiedLoanProgramsearchResultNew.aspx?islpe=f&isLead=t');

  }
  <% } %>
    //-->
		</script>
		<form id="LeadEdit" method="post" runat="server">
			<table class="insetborder" id="Table2" cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td class="MainRightHeader" style="PADDING-LEFT: 5px" noWrap colSpan="2">Lead
						Information</td>
				<TR>
					<TD class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Lead Source</TD>
					<TD noWrap><asp:DropDownList id="sLeadSrcDesc" runat="server"></asp:DropDownList></TD>
				</TR>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Loan Number</td>
					<td noWrap><asp:textbox id="sLNm" runat="server"></asp:textbox>&nbsp;<asp:CustomValidator id="sLNm_Validator" runat="server" ErrorMessage="Loan number already exists" ControlToValidate="sLNm" ClientValidationFunction="checkDuplicateLoan" font-bold="True" Display="Dynamic"></asp:CustomValidator><asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="sLNm" ErrorMessage="Loan number cannot be blank." Display="Dynamic" Font-Bold="True"></asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td class="LoanFormHeader FieldLabel" style="PADDING-LEFT: 5px" noWrap align="center" colSpan="2">Borrower
						Information</td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower First Name</td>
					<td noWrap><asp:textbox id="aBFirstNm" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower Middle Name</td>
					<td noWrap><asp:textbox id="aBMidNm" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower Last Name</td>
					<td noWrap><asp:textbox id="aBLastNm" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower Suffix</td>
					<td noWrap><ml:ComboBox id="aBSuffix" runat="server"></ml:ComboBox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower DOB</td>
					<td noWrap class="FieldLabel"><ml:datetextbox id="aBDob" runat="server" preset="date" width="75" ></ml:datetextbox>&nbsp;Age
						<asp:textbox id="aBAge" runat="server" Width="46px"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower SSN</td>
					<td noWrap><ml:ssntextbox id="aBSsn" runat="server" preset="ssn" width="75px"></ml:ssntextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower Marital Status</td>
					<td noWrap><asp:dropdownlist id="aBMaritalStatT" runat="server"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Present Address</td>
					<td noWrap><asp:textbox id="aBAddr" runat="server" Width="226px"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>&nbsp;</td>
					<td noWrap><asp:textbox id="aBCity" runat="server"></asp:textbox><ml:statedropdownlist id="aBState" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="aBZip" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower Email</td>
					<td noWrap>
						<asp:textbox id="aBEmail" runat="server"></asp:textbox>
						<a onclick="window.open('mailto:' + document.getElementById('<%= AspxTools.ClientId(aBEmail) %>').value);" href="#" tabindex=-1>send email</a>
					</td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower Home Phone</td>
					<td noWrap><ml:phonetextbox id="aBHPhone" runat="server" preset="phone" width="120"></ml:phonetextbox></td>
				</tr>
				<TR>
					<TD class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower Cell Phone</TD>
					<TD noWrap><ml:phonetextbox id="aBCellPhone" runat="server" width="120" preset="phone"></ml:phonetextbox></TD>
				</TR>
				<TR>
					<TD class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower Fax</TD>
					<TD noWrap><ml:phonetextbox id="aBFax" runat="server" width="120" preset="phone"></ml:phonetextbox></TD>
				</TR>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Borrower Business Phone</td>
					<td noWrap><ml:phonetextbox id="aBBusPhone" runat="server" preset="phone" width="120"></ml:phonetextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" nowrap>Borrower Monthly Income</td>
					<td nowrap><ml:moneytextbox id="aBBaseI" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></td>
				</tr>
				<tr>
					<td colspan="2">
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td class="LoanFormHeader FieldLabel"  width="90%" style="PADDING-LEFT: 5px" noWrap align="center">Coborrower
									Information</td>
								<td class="LoanFormHeader FieldLabel" style="PADDING-LEFT: 5px" nowrap align="right">
									<INPUT type="button" value="Copy from Borrower" onclick="onCopyBorrowrerInfo()">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower First Name</td>
					<td noWrap><asp:textbox id="aCFirstNm" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower Middle Name</td>
					<td noWrap><asp:textbox id="aCMidNm" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower Last Name</td>
					<td noWrap><asp:textbox id="aCLastNm" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px; HEIGHT: 24px" noWrap>Coborrower
						Suffix</td>
					<td noWrap style="HEIGHT: 24px"><ml:ComboBox  id="aCSuffix" runat="server"></ml:ComboBox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower DOB</td>
					<td noWrap class="FieldLabel"><ml:datetextbox id="aCDob" runat="server" preset="date" width="75" ></ml:datetextbox>&nbsp;Age
						<asp:textbox id="aCAge" runat="server" Width="56px"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower SSN</td>
					<td noWrap><ml:ssntextbox id="aCSsn" runat="server" preset="ssn" width="75px"></ml:ssntextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower Marital Status</td>
					<td noWrap><asp:dropdownlist id="aCMaritalStatT" runat="server"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Present Address</td>
					<td noWrap><asp:textbox id="aCAddr" runat="server" Width="227px"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>&nbsp;</td>
					<td noWrap><asp:textbox id="aCCity" runat="server"></asp:textbox><ml:statedropdownlist id="aCState" runat="server"></ml:statedropdownlist><ml:zipcodetextbox id="aCZip" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower Email</td>
					<td noWrap><asp:textbox id="aCEmail" runat="server"></asp:textbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower Home Phone</td>
					<td noWrap><ml:phonetextbox id="aCHPhone" runat="server" preset="phone" width="120"></ml:phonetextbox></td>
				</tr>
				<TR>
					<TD class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower Cell Phone</TD>
					<TD noWrap><ml:phonetextbox id="aCCellPhone" runat="server" width="120" preset="phone"></ml:phonetextbox></TD>
				</TR>
				<TR>
					<TD class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower Fax</TD>
					<TD noWrap><ml:phonetextbox id="aCFax" runat="server" width="120" preset="phone"></ml:phonetextbox></TD>
				</TR>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Coborrower Business Phone</td>
					<td noWrap><ml:phonetextbox id="aCBusPhone" runat="server" preset="phone" width="120"></ml:phonetextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" nowrap>Coborrower Monthly Income</td>
					<td nowrap><ml:moneytextbox id="aCBaseI" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></td>
				</tr>
				<tr>
					<td class="LoanFormHeader FieldLabel" style="PADDING-LEFT: 5px" noWrap align="center" colSpan="2">Loan
						Information</td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Loan Type</td>
					<td noWrap><asp:dropdownlist id="sLT" runat="server"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Loan Purpose</td>
					<td noWrap><asp:dropdownlist id="sLPurposeT" runat="server" onchange="refreshCalculation();"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Lien Position</td>
					<td noWrap><asp:dropdownlist id="sLienPosT" runat="server"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Loan Program</td>
					<td noWrap><asp:textbox id="sLpTemplateNm" runat="server"></asp:textbox>
						<% if (m_optionTelemarketerCanRunPrequal) { %>
						<% if (m_isPricingEnabled) {%>
						<input onclick="f_runLPE();" tabindex="1" type="button" value="Run PriceMyLoan">
						<% } %>
						<input id="btnSelectProgram" type="button" value="Select Program" onclick="f_runNonLPE();" style="WIDTH: 110px">
						<% } %>
					</td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Purchase Price</td>
					<td noWrap><ml:moneytextbox id="sPurchPrice" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" nowrap>Appraised Value</td>
					<td nowrap><ml:moneytextbox id="sApprVal" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" nowrap>Down Payment %</td>
					<td nowrap><ml:PercentTextBox id="sDownPmtPc" runat="server" width="70" preset="percent" onchange="on_sDownPmtPc_change(event);"></ml:PercentTextBox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Down Payment $</td>
					<td noWrap><ml:moneytextbox id="sEquityCalc" runat="server" preset="money" width="90" onchange="refreshCalculation();"></ml:moneytextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" nowrap>Loan Amount</td>
					<td class="FieldLabel" nowrap><asp:CheckBox id="sLAmtLckd" runat="server" Text="Lock" onclick="refreshCalculation();"></asp:CheckBox><ml:moneytextbox id="sLAmtCalc" runat="server" width="90" preset="money" onchange="refreshCalculation();"></ml:moneytextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" nowrap>Note Rate</td>
					<td nowrap><ml:percenttextbox id="sNoteIR" runat="server" width="70" preset="percent" onchange="refreshCalculation();"></ml:percenttextbox></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" nowrap>Term</td>
					<td nowrap><asp:textbox id="sTerm" runat="server" width="60px" onchange="refreshCalculation();"></asp:textbox>&nbsp;months</td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Due</td>
					<td noWrap><asp:textbox id="sDue" runat="server" width="59px" onchange="refreshCalculation();"></asp:textbox>&nbsp;months</td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Monthly Payment</td>
					<td noWrap><ml:moneytextbox id="sMonthlyPmt" runat="server" preset="money" width="90" readonly="True"></ml:moneytextbox></td>
				</tr>
				<tr>
					<td class="LoanFormHeader FieldLabel" style="PADDING-LEFT: 5px" noWrap align="center" colSpan="2">Subject
						Property</td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>Subject Property Address</td>
					<td noWrap><asp:textbox id="sSpAddr" runat="server" Width="226px"></asp:textbox><input type="button" value="Copy from Present Address" onclick="f_copyFromPresentAddress();"></td>
				</tr>
				<tr>
					<td class="FieldLabel" style="PADDING-LEFT: 5px" noWrap>&nbsp;</td>
					<td noWrap><asp:textbox id="sSpCity" runat="server"></asp:textbox><ml:statedropdownlist id="sSpState" runat="server" IncludeTerritories="false"></ml:statedropdownlist><ml:zipcodetextbox id="sSpZip" runat="server" preset="zipcode" width="50"></ml:zipcodetextbox></td>
				</tr>
				<tr>
					<td class="LoanFormHeader FieldLabel" style="PADDING-LEFT: 5px" noWrap align="center" colSpan="2" rowSpan="1">Notes</td>
				</tr>
				<tr>
					<td noWrap colSpan="2"><input type="button" value="Date & time stamp" onclick="onDateStampClick();"><br>
						<asp:textbox id="sTrNotes" runat="server" Width="442px" TextMode="MultiLine" Height="95px"></asp:textbox></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
