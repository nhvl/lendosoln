using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.Lead
{
	public partial class LeadEdit : LendersOfficeApp.newlos.BaseLoanPage
	{
        protected bool m_optionTelemarketerCanRunPrequal;
        protected bool m_isPricingEnabled;
		protected bool m_cannotModifyLoanName = false; // 08/24/06 mf - OPM 2862

        private void SetIsPricingEnable() 
        {
            m_isPricingEnabled = BrokerUser.HasFeatures(E_BrokerFeatureT.PricingEngine);
        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            this.PageID = "LeadInfo";
            this.PageTitle = "Lead Information";

            aBZip.SmartZipcode(aBCity, aBState);
            aCZip.SmartZipcode(aCCity, aCState);
            sSpZip.SmartZipcode(sSpCity, sSpState);
            Tools.Bind_aMaritalStatT(aBMaritalStatT);
            Tools.Bind_aMaritalStatT(aCMaritalStatT);
            Tools.Bind_sLT(sLT);
            Tools.Bind_sLPurposeT(sLPurposeT);
            Tools.Bind_sLienPosT(sLienPosT);
            SetIsPricingEnable();
			m_cannotModifyLoanName = !BrokerUser.HasPermission( Permission.CanModifyLoanName );
            BindLeadSource();

            Tools.BindSuffix(aBSuffix);
            Tools.BindSuffix(aCSuffix);

        }

        private void BindLeadSource() 
        {
            // 5/5/14 gf - opm 172380, always add a blank option for the default
            // value. We do not want the UI to display misleading data before the
            // page is saved because there are now pricing rules and fee service
            // rules which depend on this field.
            sLeadSrcDesc.Items.Add(new ListItem("", ""));

            foreach (LeadSource desc in this.Broker.LeadSources) 
            {
                sLeadSrcDesc.Items.Add(new ListItem(desc.Name, desc.Name));
            }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            UseNewFramework = true;
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

        protected override void LoadData() 
        {
            m_optionTelemarketerCanRunPrequal = this.Broker.OptionTelemarketerCanRunPrequal;

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(LeadEdit));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0); // 5/18/2004 dd - Only support primary application.

            Tools.SetDropDownListValue(aCMaritalStatT, dataApp.aCMaritalStatT);
            Tools.SetDropDownListValue(aBMaritalStatT, dataApp.aBMaritalStatT);
            Tools.SetDropDownListValue(sLPurposeT, dataLoan.sLPurposeT);
            Tools.SetDropDownListValue(sLT, dataLoan.sLT);
            Tools.SetDropDownListValue(sLienPosT, dataLoan.sLienPosT);

            this.aBBaseI.ReadOnly = dataLoan.sIsIncomeCollectionEnabled;
            this.aCBaseI.ReadOnly = dataLoan.sIsIncomeCollectionEnabled;

            aBAddr.Text        = dataApp.aBAddr;
            aBAge.Text         = dataApp.aBAge_rep;
            aBBaseI.Text       = dataApp.aBBaseI_rep;
            aBBusPhone.Text    = dataApp.aBBusPhone;
            aBCellPhone.Text   = dataApp.aBCellPhone;
            aBFax.Text         = dataApp.aBFax;
            aBCity.Text        = dataApp.aBCity;
            aBDob.Text         = dataApp.aBDob_rep;
            aBEmail.Text       = dataApp.aBEmail;
            aBFirstNm.Text     = dataApp.aBFirstNm;
            aBHPhone.Text      = dataApp.aBHPhone;
            aBLastNm.Text      = dataApp.aBLastNm;
            aBMidNm.Text       = dataApp.aBMidNm;
            aBSsn.Text         = dataApp.aBSsn;
            aBState.Value      = dataApp.aBState;
            aBSuffix.Text      = dataApp.aBSuffix;
            aBZip.Text         = dataApp.aBZip;
            aCAddr.Text        = dataApp.aCAddr;
            aCAge.Text         = dataApp.aCAge_rep;
            aCBaseI.Text       = dataApp.aCBaseI_rep;
            aCBusPhone.Text    = dataApp.aCBusPhone;

            aCCity.Text        = dataApp.aCCity;
            aCDob.Text         = dataApp.aCDob_rep;
            aCEmail.Text       = dataApp.aCEmail;
            aCFirstNm.Text     = dataApp.aCFirstNm;
            aCHPhone.Text      = dataApp.aCHPhone;
            aCBusPhone.Text    = dataApp.aCBusPhone;
            aCCellPhone.Text   = dataApp.aCCellPhone;
            aCFax.Text         = dataApp.aCFax;
            aCLastNm.Text      = dataApp.aCLastNm;
            aCMidNm.Text       = dataApp.aCMidNm;
            aCSsn.Text         = dataApp.aCSsn;
            aCState.Value      = dataApp.aCState;
            aCSuffix.Text      = dataApp.aCSuffix;
            aCZip.Text         = dataApp.aCZip;
            sApprVal.Text      = dataLoan.sApprVal_rep;
            sDownPmtPc.Text    = dataLoan.sDownPmtPc_rep;
            sDue.Text          = dataLoan.sDue_rep;
            sEquityCalc.Text   = dataLoan.sEquityCalc_rep;
            sLAmtCalc.Text     = dataLoan.sLAmtCalc_rep;
            sLAmtLckd.Checked  = dataLoan.sLAmtLckd;
            sLNm.Text          = dataLoan.sLNm;
            sLpTemplateNm.Text = dataLoan.sLpTemplateNm;
			// 5/29/07 mf. OPM 12061. There is now a corporate setting for editing
			// the loan program name.
            
            if(! BrokerUser.HasPermission(Permission.AllowEditingLoanProgramName))
				sLpTemplateNm.ReadOnly = true;

            sMonthlyPmt.Text   = dataLoan.sMonthlyPmt_rep;
            sNoteIR.Text       = dataLoan.sNoteIR_rep;
            sPurchPrice.Text   = dataLoan.sPurchPrice_rep;
            sSpAddr.Text       = dataLoan.sSpAddr;
            sSpCity.Text       = dataLoan.sSpCity;
            sSpState.Value     = dataLoan.sSpState;
            sSpZip.Text        = dataLoan.sSpZip;
            sTerm.Text         = dataLoan.sTerm_rep;
            sTrNotes.Text      = dataLoan.sTrNotes;

            // 9/3/2013 AV - 137485 Investigate Lead Source update problem
            if (dataLoan.sLeadSrcDesc.Length > 0 && sLeadSrcDesc.Items.FindByText(dataLoan.sLeadSrcDesc) == null)
            {
                // 7/15/2005 dd - If current description no longer in description, just add to the drop downlist instead of clear out.
                sLeadSrcDesc.Items.Add(new ListItem(dataLoan.sLeadSrcDesc, dataLoan.sLeadSrcDesc));
            }
         
            Tools.SetDropDownListValue(sLeadSrcDesc, dataLoan.sLeadSrcDesc);

			if ( m_cannotModifyLoanName )
			{
				sLNm.ReadOnly = true;
				sLNm.ToolTip = "You do not have permission to modify the loan number.";
			}

            string serverID = "";
            ServerLocation server = ConstAppDavid.CurrentServerLocation;
            switch (server) 
            {
                case ServerLocation.LocalHost:
                    serverID = "(LOCALHOST)  (LOCALHOST)  (LOCALHOST)";
                    break;
                case ServerLocation.Demo:
                    serverID = "(DEMO)  (DEMO)  (DEMO)";
                    break;
                case ServerLocation.Development:
                    serverID = "(DEVELOPMENT)  (DEVELOPMENT)";
                    break;
                case ServerLocation.DevCopy:
                    serverID = "(DEVCOPY)   (DEVCOPY)   (DEVCOPY)";
                    break;
            }
            ClientScript.RegisterHiddenField("FrameTitle", string.Format("{0}, {1} - {2} {3}", dataApp.aBLastNm, dataApp.aBFirstNm, dataLoan.sLNm, serverID));

            this.RegisterJsGlobalVariables("IsRenovationLoan", dataLoan.sIsRenovationLoan);
        }




	}
}
