using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.Security;

namespace LendersOfficeApp.los.Lead
{
	/// <summary>
	/// Summary description for LeadMainFrame.
	/// </summary>
	public partial class LeadMainFrame : BasePage
	{
        protected bool IsResize
        {
            get
            {
                return RequestHelper.GetBool("isResize");
            }
        }

        protected string m_sLNm = "";
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            if (RequestHelper.GetBool("isnew")) 
            {

                CPageData dataLoan = new CLoanNameOnlyData(RequestHelper.LoanID);
                dataLoan.InitLoad();
                m_sLNm = Utilities.SafeJsString(dataLoan.sLNm);

            }

            // OPM 228110 - Migrate lead if it's TRID 2015 but still in Legacy mode.
            if (!IsPostBack)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(RequestHelper.LoanID, typeof(LeadMainFrame));
                dataLoan.InitLoad();

                if (dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    E_sClosingCostFeeVersionT migrateToVersion = dataLoan.BrokerDB.MinimumDefaultClosingCostDataSet > E_sClosingCostFeeVersionT.LegacyButMigrated ?
                        dataLoan.BrokerDB.MinimumDefaultClosingCostDataSet : E_sClosingCostFeeVersionT.LegacyButMigrated;
                    ClosingCostFeeMigration.Migrate(PrincipalFactory.CurrentPrincipal, RequestHelper.LoanID, migrateToVersion);
                }
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
