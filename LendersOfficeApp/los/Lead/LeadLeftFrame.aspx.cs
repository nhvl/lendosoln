namespace LendersOfficeApp.los.Lead
{
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.CustomFavoriteFolder;
    using LendersOffice.ObjLib.DynaTree;
    using LendersOffice.Security;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web.UI.WebControls;

    public partial class LeadLeftFrame : LendersOffice.Common.BaseServicePage
    {

        private CPageData m_pageData;
        private BrokerDB x_brokerDB;
        private BrokerDB Broker
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                }

                return x_brokerDB;
            }
        }

        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected bool IsInternalUser
        {
            get { return this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms); } // Internal Admin user.
        }

		protected void PageLoad(object sender, System.EventArgs e)
        {
            EnableJqueryMigrate = false;
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("Favorites.js");
            RegisterCSS("Favorites.css");

            m_pageData = CPageData.CreateUsingSmartDependency(RequestHelper.LoanID, typeof(LeadLeftFrame));
            m_pageData.InitLoad();

            LendersOffice.Common.UI.Tree tree = new LendersOffice.Common.UI.Tree(this);
            tree.SpecialTypeNode += new LendersOffice.Common.UI.SpecialTypeNodeHandler(this.DisplayFindQualifiedLoanProgram);
            tree.OnNodeCreate += new LendersOffice.Common.UI.NodeCreateHandler(this.TreeNode_Create);

            EmployeeDB empDB = new EmployeeDB(PrincipalFactory.CurrentPrincipal.EmployeeId, PrincipalFactory.CurrentPrincipal.BrokerId);
            empDB.Retrieve();

            DocListUrl.Value = Tools.GetEDocsLink(Tools.VRoot + "/newlos/ElectronicDocs/DocList.aspx?loanid=" + RequestHelper.LoanID);

            var favorites = CustomFavoriteFolderHelper.RetrieveCustomFavoriteFolderTreeByBrokerId(
                PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId, true);

            CustomFavoriteFolderNode root;

            root = new CustomFavoriteFolderNode();
            root.Folder = new CustomFavoriteFolder(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId);
            root.Children = favorites;

            root.IsVisible = true;
            var groupIds = GroupDB.ListInclusiveGroupForEmployee(this.Broker.BrokerID, PrincipalFactory.CurrentPrincipal.EmployeeId).Select(group => group.Id);
            var roleIds = LendersOffice.Security.PrincipalFactory.CurrentPrincipal.GetRoles().Select(roleT => Role.Get(roleT).Id);
            
            var landingPageId = EmployeeDB.RetrieveLandingPageIdByEmployeeId(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId);
            var filter = new CustomFavoriteFolderFilter()
            {
                EmployeeId = PrincipalFactory.CurrentPrincipal.EmployeeId,
                LeadLoanStatus = Tools.IsStatusLead(m_pageData.sStatusT) ? LeadLoanStatusT.Lead : LeadLoanStatusT.Loan,
                GroupIds = groupIds,
                RoleIds = roleIds,
                LoanPurposes = new List<E_sLPurposeT>() { m_pageData.sLPurposeT },
                LoanTypes = new List<E_sLT>() { m_pageData.sLT }
            };

            CustomFavoriteFolderHelper.CheckCustomFavoriteFolderNodePermissionRecursive(root, filter);
            tree.Load(Tools.GetServerMapPath("LeadNavigation.xml.config"));

            var dynaNode = tree.Render(false);
            var flatDynaNodeDict = CustomFavoriteFolderHelper.FlattenDynaNodePages(dynaNode, new Dictionary<string, List<DynaTreeNode>>());
            List<DynaTreeNode> landingPageList;
            if (flatDynaNodeDict.TryGetValue(landingPageId, out landingPageList))
            {
                landingPageList.First().customAttributes.Add("landingpage", "true");
            }

            var dynaNodeRoot = LendersOfficeApp.newlos.LeftTreeFrame.CreateDynaNodesFromFavorites(root, flatDynaNodeDict, false, tree, true, CustomFavoriteFolderHelper.AssociatedPages);
            dynaNodeRoot.children = dynaNodeRoot.children.Concat(dynaNode.children).ToList();

            m_tree.Text = "<div id='pageNavigation'></div>";
            this.RegisterJsGlobalVariables("navigationModel", SerializationHelper.JsonNetSerialize(dynaNodeRoot));


            this.RegisterJsScript("jquery-ui-1.11.4.min.js");
            this.RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("dynatree/jquery.dynatree.js");
            this.RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");
            this.RegisterService("folderNavigation", "/los/admin/CustomFolderNavigationService.aspx");

            this.RegisterJsGlobalVariables("sBorrowerApplicationCollectionT", (int)this.m_pageData.sBorrowerApplicationCollectionT);

            if (PrincipalFactory.CurrentPrincipal.HasConversationLogQuickPostingEnabled)
            {
                this.RegisterJsScript("quickReplyInjector.js");
            }
            else
            {
                this.LoadsQuickReplyInjector.Visible = false;
            }
        }

        private void TreeNode_Create(LendersOffice.Common.UI.TreeNode item)
        {
            if (item.ID == "Page_NewTaskList")
            {
                item.IsVisible = Broker.IsUseNewTaskSystem;
            }
            else if (item.ID == "Page_CreateNewTask") // It should not be visible if the broker is using the new task system
            {
                item.IsVisible = !Broker.IsUseNewTaskSystem;
            }
            else if (item.ID == "Page_TaskList")
            {
                item.IsVisible = !Broker.IsUseNewTaskSystem;
            }
            else if (item.ID == "Page_LendingStaffNotes") // opm 202077 - 3/4/2015 ejm
            {
                item.IsVisible = BrokerUser.HasPermission(Permission.CanAccessLendingStaffNotes);
            }
            else if (item.ID == "Folder_InitialForms")
            {
                item.IsVisible = this.m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;
            }
            else if (item.ID == "Page_EmployeeResources")
            {
                item.IsVisible = !Broker.EmployeeResourcesUrl.Equals(string.Empty) && Broker.ShowEmployeeResourcesLinkInLeadEditor;
                ClientScript.RegisterHiddenField("EmployeeResourcesUrl", Broker.EmployeeResourcesUrl);
            }
            else if (item.ID == "Folder_LeadManagement")
            {
                item.Name = "Lead Info";
            }
            else if (item.ID == "Page_LeadInfo")
            {
                item.Name = "This Lead Info";
                if (Broker.IsEnableBigLoanPage)
                {
                    item.Attributes["onclick"] = "return load('BigLoanInfo.aspx', true);";
                }
                else
                {
                    item.Attributes["onclick"] = "return load('LoanInfo.aspx?pg=0', true);";
                }
                item.Attributes["pageid"] = "LoanInformation";
            }
            else if (item.ID == "Page_LeadLiabilities")
            {
                item.IsVisible = false;
            }
            else if (item.ID == "Page_LeadOrderCredit" || item.ID == "Page_LeadViewCredit")
            {
                // Want to hide the order/view credit pages in the Lead Management/Info folder.
                item.IsVisible = false;
            }
            else if (item.ID == "Page_ExportDu" || item.ID == "Page_ViewDUFindings")
            {
                item.IsVisible = Broker.IsExpandedLeadEditorDoEnabled ||
                    Broker.IsExpandedLeadEditorDuEnabled;
            }
            else if (item.ID == "Page_ExportFreddie" || item.ID == "Page_ViewLpFeedback")
            {
                item.IsVisible = Broker.IsExpandedLeadEditorLpEnabled;
            }
            else if (item.ID == "FHATotalAudit" || item.ID == "FHATOTALFindings")
            {
                item.IsVisible = Broker.IsExpandedLeadEditorTotalEnabled;
            }
            else if (item.ID == "Page_CustomFields" || item.ID == "Page_CustomFields2" || item.ID == "Page_CustomFields3")
            {
                item.IsVisible = Broker.IsExpandedLeadEditorCustomFieldsEnabled;
            }
            else if (item.ID == "Electronic_Docs")
            {

                item.IsVisible = Broker.IsEDocsEnabled && ((BrokerUserPrincipal)PrincipalFactory.CurrentPrincipal).HasPermission(Permission.CanViewEDocs);//BrokerUser.HasPermission(Permission.CanViewEDocs);
            }
            else if (item.ID == "Page_ConversationLog")
            {
                item.IsVisible = Broker.ActuallyEnableConversationLogForLoans;
            }
            else if (item.ID == "Page_InternalSupportPage")
            {
                item.IsVisible = ConstAppDavid.CurrentServerLocation == ServerLocation.Development ||
                    ConstAppDavid.CurrentServerLocation == ServerLocation.LocalHost ||
                    ConstAppDavid.CurrentServerLocation == ServerLocation.DevCopy ||
                    this.BrokerUser.HasPermission(Permission.CanModifyLoanPrograms); // Internal Admin user.
            }
            else if (item.ID == "Page_ManageNewFeatures" || item.ID == "Page_DataLayerMigration")
            {
                item.IsVisible = this.BrokerUser.HasPermission(Permission.AllowManageNewFeatures);
            }
            else if (item.ID == "Page_LoanTerms")
            {
                item.IsVisible = this.m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;
            }
            else if (item.ID == "Page_PropHousingExpenses")
            {
                item.IsVisible = this.m_pageData.sIsHousingExpenseMigrated;
            }
            else if (item.ID == "Page_2010GFE")
            {
                if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    item.IsVisible = true;
                }
                else if (m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID && m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated)
                {
                    item.IsVisible = true;
                    item.Name = "Legacy Initial Fees Worksheet";
                }
                else
                {
                    item.IsVisible = false;
                }
            }
            else if (item.ID == "Page_RActiveNewGfe")
            {
                if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    item.IsVisible = false;
                }
                else if (m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID && m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated)
                {
                    item.IsVisible = true;
                }
                else if (m_pageData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID && m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
                {
                    item.IsVisible = true;
                }
                else
                {
                    item.IsVisible = false;
                }
            }
            else if (item.ID == "Page_LoanEstimate")
            {
                if (m_pageData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                {
                    item.IsVisible = false;
                }
                else if (m_pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID && m_pageData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
                {
                    item.IsVisible = true;
                }
                else
                {
                    item.IsVisible = false;
                }
            }
            else if (item.ID == "Page_BorrowerClosingCosts")
            {
                if (m_pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
                {
                    item.IsVisible = true;
                }
                else
                {
                    item.IsVisible = false;
                }
            }
            else if (item.ID == "Page_AdjustmentsAndOtherCredits")
            {
                item.IsVisible = m_pageData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            }
            else if (item.ID == "Document_Capture")
            {
                item.IsVisible = this.Broker.EnableKtaIntegration;
            }
            else if (item.ID == "Capture_Audit")
            {
                item.IsVisible = this.Broker.EnableKtaIntegration && this.BrokerUser.HasPermission(Permission.AllowAccessingLoanEditorDocumentCaptureAuditPage);
            }
            else if (item.ID == "Capture_Review")
            {
                item.IsVisible = this.Broker.EnableKtaIntegration && this.BrokerUser.HasPermission(Permission.AllowAccessingLoanEditorDocumentCaptureReviewPage);
            }
        }

        private void DisplayFindQualifiedLoanProgram(LendersOffice.Common.UI.TreeNode node, string code, LendersOffice.Common.UI.Tree tree) 
        {
            if (code == "QualifiedLoan") 
            {
                // 5/11/2005 dd - Only display Run LPE in the menu if broker has PriceMyLoan Feature, and 
                // option allow call center to run LPE.
                if (BrokerUserPrincipal.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan)) 
                {
                    BrokerDB broker = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                    node.IsVisible = broker.OptionTelemarketerCanRunPrequal;
                } 
                else 
                {
                    node.IsVisible = false;
                }
                
            } 
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion


	}
}
