using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using System.Collections.Generic;
using System.Text;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.Lead
{
	public partial class LeadSummaryFrameService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "RefreshInfo":
                    RefreshInfo();
                    break;
                case "RefreshAppList":
                    RefreshAppList();
                    break;
            }
        }
        private void RefreshInfo() 
        {

            CPageData dataLoan = new CLeadSummaryData(GetGuid("loanid"));
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);

            SetResult("sLNm", dataLoan.sLNm);
            SetResult("aBLastNm", dataApp.aBLastNm);
            SetResult("sQualTopR", dataLoan.sQualTopR_rep);
            SetResult("sQualBottomR", dataLoan.sQualBottomR_rep);
            SetResult("sLtvR", dataLoan.sLtvR_rep);
            SetResult("sCltvR", dataLoan.sCltvR_rep);
            SetResult("sClosingCostFeeVersionT", dataLoan.sClosingCostFeeVersionT.ToString());
            SetResult("aAppId", dataApp.aAppId);
            SetResult("aBExperianScore", dataApp.aBExperianScore_rep);
            SetResult("aBTransUnionScore", dataApp.aBTransUnionScore_rep);
            SetResult("aBEquifaxScore", dataApp.aBEquifaxScore_rep);

            SetResult("aCExperianScore", dataApp.aCExperianScore_rep);
            SetResult("aCTransUnionScore", dataApp.aCTransUnionScore_rep);
            SetResult("aCEquifaxScore", dataApp.aCEquifaxScore_rep);        }

        private string ToJson(List<CAppData> list)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("({");
            sb.Append("'length':");
            sb.Append(list.Count);
            sb.Append(",");
            sb.Append("'values':");
            sb.Append("[");
            for (int i = 0; i < list.Count; i++)
            {
                if (i != 0)
                    sb.Append(",");
                sb.Append(ToJson(list[i]));
            }

            sb.Append("]");
            sb.Append("})");

            return sb.ToString();
        }

        private string ToJson(CAppData ap)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{");

            sb.Append("'id':");
            sb.Append(AspxTools.JsString(ap.aAppId));
            sb.Append(",");

            sb.Append("'name':");
            sb.Append(AspxTools.JsString(ap.aAppNm));
            sb.Append("}");

            return sb.ToString();
        }

        private void RefreshAppList()
        {
            CAppData dataApp = null;
            List<CAppData> list = new List<CAppData>();

            CPageData dataLoan = new CLeadSummaryData(GetGuid("loanid"));
            dataLoan.InitLoad();

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                dataApp = dataLoan.GetAppData(i);
                list.Add(dataApp);
            }
            string result = ToJson(list);
            SetResult("appList", result);
        }
	}
}
