using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.los.Lead
{
	public partial class LeadSummaryFrame : LendersOffice.Common.BaseServicePage
	{
        protected Guid LoanID 
        {
            get { return RequestHelper.LoanID; }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
            CPageData dataLoan = new CLeadSummaryData(LoanID);
            dataLoan.InitLoad();
            dataLoan.LoadAppNames(m_applicantsDDL);
		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            RegisterService("SummaryFrame", "/los/lead/LeadSummaryFrameService.aspx");
        }

        private LendersOffice.Admin.BrokerDB x_brokerDB;
        private LendersOffice.Admin.BrokerDB Broker
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = LendersOffice.Admin.BrokerDB.RetrieveById(LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                }

                return x_brokerDB;
            }
        }

        protected bool IsEnableBigLoanPage
        {
            get { return Broker.IsEnableBigLoanPage; }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}
