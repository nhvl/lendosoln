using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOfficeApp.los.admin;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.Lead
{
	public partial class HeaderFrame : LendersOffice.Common.BaseServicePage
	{
        protected string m_serverID = "";
    
        protected string m_userName = "";
        protected string m_brokerName = "";
		protected string m_userMail = "";

		protected void PageLoad(object sender, System.EventArgs e)
		{
            ServerLocation server = ConstAppDavid.CurrentServerLocation;
            switch (server) 
            {
                case ServerLocation.LocalHost:
                    m_serverID = "local_";
                    break;
                case ServerLocation.Demo:
                    m_serverID = "demo_";
                    break;
                case ServerLocation.Development:
                    m_serverID = "dev_";
                    break;
                case ServerLocation.DevCopy:
                    m_serverID = "devcopy_";
                    break;
            }

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            if (null != principal)
            {
                string pml_description = "";
                if (principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
                    pml_description = " (PML)";

                m_welcomeLabel.Text = string.Format("Welcome, {0}!", principal.DisplayName);
                m_userName = principal.DisplayName;
                m_brokerName = principal.BrokerName + pml_description;

                //opm 18633 fs 06/19/08
                EmployeeDB m_Employee = new EmployeeDB(principal.EmployeeId, principal.BrokerId);
                if (null != m_Employee)
                {
                    m_Employee.Retrieve();
                    m_userMail = m_Employee.Email;
                }
            }

			// Put user code to initialize the page here
            if (Page.IsPostBack)
            {
                if (Request.Form["IsLogout"] != null && Request.Form["IsLogout"] != "")
                {
                    signOut(PrincipalFactory.CurrentPrincipal);
                }
            }
            else
            {
                m_NewSupportLink.Title = m_NewSupportLink.InnerText = ConstStage.SupportCenterLinkText;
            }
            ClientScript.RegisterHiddenField("IsLogout", "");
            RegisterService("lookupRecord", "/common/RecordsLookupService.aspx");
		}

        private void signOut(AbstractUserPrincipal principal)
        {
            SecurityEventLogHelper.CreateLogoutLog(principal);

            RequestHelper.ClearAuthenticationCookies();
            //((BrokerUserPrincipal)Page.User).RemoveFromCache();

            string script = @"<script language='JavaScript1.2'>
            onDesktopClick();
            </script>";

            ClientScript.RegisterClientScriptBlock(this.GetType(), "init", script);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion
	}
}
