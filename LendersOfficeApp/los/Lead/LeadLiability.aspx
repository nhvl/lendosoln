<%@ Page language="c#" Codebehind="LeadLiability.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Lead.LeadLiability" %>
<%@ Register TagPrefix="uc1" TagName="BorrowerLiabilityFrame" Src="../../newlos/BorrowerLiabilityFrame.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
    <title>LeadLiability</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" scroll="yes">
	
    <form id="LeadLiability" method="post" runat="server">
    <table width="100%" border=0>
      <tr><td class="MainRightHeader" style="padding-left:5px">Liabilities</td></tr>
      <tr>
        <td>
    <uc1:borrowerliabilityframe id=BorrowerLiabilityFrame1 runat="server"></uc1:borrowerliabilityframe>
        </td>
      </tr>
    </table>


     </form>
	
  </body>
</html>
