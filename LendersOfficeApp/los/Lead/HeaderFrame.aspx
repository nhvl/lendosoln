<%@ Page language="c#" Codebehind="HeaderFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Lead.HeaderFrame" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
    <title>HeaderFrame</title>
    <meta content="Microsoft Visual Studio 7.0" name=GENERATOR>
    <meta content=C# name=CODE_LANGUAGE>
    <meta content=JavaScript name=vs_defaultClientScript>
    <meta content=http://schemas.microsoft.com/intellisense/ie5 name=vs_targetSchema>
    <link href="../../css/stylesheet.css" type=text/css rel=stylesheet >
      <script type="text/javascript">
      function onHelpClick()
      {
	      var handle = window.open("../../help/index.aspx", "", "resizable=yes,scrollbars=yes,menubar=yes,toolbar=yes,height=550pt,width=785pt") ;
	      handle.focus() ;      
      }
      function goBackPipeline() {
        if (null != parent.opener && parent.opener.closed) {
          // If parent window already close then open new one. Unfortunately that mean list of current loan
          // windows open will be lost.
          var preferedWidth = 1024;
          var preferedHeight = 768;
  
          var w = preferedWidth < screen.width ? preferedWidth : screen.width - 10;
          var h = preferedHeight < screen.height ? preferedHeight : screen.height - 60;
          var left = (screen.width - w) / 2;
          var top = (screen.height - h - 50) / 2;
  
          var options = 'height=' + h + ',width=' + w + ',left=' + left + ',top=' + top + ',location=no,menubar=no,resizable=yes,status=no,toolbar=no';

          window.open(<%= AspxTools.JsString(VirtualRoot) %> + '/los/main.aspx?showAlert=t', <%= AspxTools.JsString(m_serverID) %> + 'pipeline', options);      
          return;
  
        }
        else if (parent.opener != null && parent.opener.parent.frmCode != null) {
            /*
            If this loanapp is open new frame from /los/main.aspx then when click on pipeline,
            it should bring the parent browser to focus instead of redirect to los/main.aspx.
            */
          parent.opener.parent.focus(); // Focus parent.
          return;
        }
      }
      
      function closeMe(bCancel) {
        if (typeof(parent.body.f_closeMe) == "function") 
          return parent.body.f_closeMe(bCancel);
        else {
          if (null != parent.opener && !parent.opener.closed) parent.opener.focus();        
          window.parent.close();
        }
        return true;
      }     
      function f_chat() {
        var sUserName = <%=AspxTools.JsString(m_userName)%>;
        var sBrokerName = <%=AspxTools.JsString(m_brokerName)%>;
          var sUserMail = <%=AspxTools.JsString(m_userMail)%>;

        f_chatSupport(sUserName, sBrokerName, sUserMail);
      }       
      function f_disableCloseLink(bDisabled) {
          setDisabledAttr($j('#CloseLink'), bDisabled);
          setDisabledAttr($j('#MainLink'), bDisabled);
      }
      </script>
</HEAD>
  <body bottomMargin=0 leftMargin=0 topMargin=0 rightMargin=0 MS_POSITIONING="FlowLayout">
    <form id=HeaderFrame method=post runat="server">
      <table cellSpacing=0 cellPadding=3 width="100%">
        <tr bgColor=#000000>
          <td><span class=white><strong><font size=4><img height=20 src=<%=AspxTools.HtmlAttribute(VirtualRoot + "/images/spacer.gif")%> width=5 align=absMiddle >LendingQB</font></strong></span></td>
          <td class="white"><ml:EncodedLiteral id=m_welcomeLabel runat="server" EnableViewState="False"></ml:EncodedLiteral></td>
          <td align=right>
            <img height="12" src=<%=AspxTools.HtmlAttribute(VirtualRoot + "/images/homebox.gif")%> width="12" align="absMiddle">&nbsp;
            <A class=PortletLink onclick="if (!hasDisabledAttr(this)) return goBackPipeline();" href="#" title="Main Window" id="MainLink">Main Window</A>
            &nbsp;&nbsp; 
            <img src=<%=AspxTools.HtmlAttribute(VirtualRoot + "/images/chat.gif")%> align="absmiddle">&nbsp;
            <a runat="server" id="m_NewSupportLink" onclick="f_chat()" class="PortletLink" title="Instant Support">Instant Support New</a>
            &nbsp;&nbsp;
            <img height=12 src=<%=AspxTools.HtmlAttribute(VirtualRoot + "/images/xbox.gif")%> width=12 align="absMiddle">&nbsp;
            <a href="#" class="PortletLink" onclick="if (!hasDisabledAttr(this)) return closeMe();" id="CloseLink">Close</a>&nbsp;&nbsp;
          </td>
        </tr>

        <asp:PlaceHolder runat="server" ID="KayakoSupportMethod">
            <script type="text/javascript">
            function f_chatSupport(sUserName, sBrokerName, sUserMail) {
                var result =  gService.lookupRecord.call("SupportStatus");	
                if (result.value.error) {
                    alert(result.value.UserMessage);
                }
                else if (result.value.redirect) {
                    window.open(result.value.redirect,'_blank');
                }
                
                return false;
            }
        </script>
        </asp:PlaceHolder>
    
      </table>
    </form>
  </body>
</HTML>
