using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;

using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;

namespace LendersOfficeApp.los
{
	public partial class PrintGroupEditService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
		protected BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "Save":
                    Save();
                    break;
                case "DeleteGroup":
                    DeleteGroup();
                    break;
            }
        }
        private void DeleteGroup() 
        {
            Guid groupID = GetGuid("GroupID");
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerUserPrincipal.CurrentPrincipal.BrokerId),
                                            new SqlParameter("@GroupID", groupID)
                                        };

			if( BrokerUser.HasPermission( Permission.CanEditPrintGroups ) == true )
			{
				StoredProcedureHelper.ExecuteNonQuery(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "DeletePrintGroup", 0, parameters);
			}
			else
			{
				SetResult( "Error" , "Access denied. You lack permission to edit print groups." );
			}
        }
        private bool CheckExistingName(Guid groupID, string groupName) 
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerUserPrincipal.CurrentPrincipal.BrokerId),
                                            new SqlParameter("@GroupID", groupID),
                                            new SqlParameter("@GroupName", groupName)
                                        };
            bool ret = false;

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "IsPrintGroupNameExisting", parameters)) 
            {
                if (reader.Read())
                    ret = true;
            }
            return ret;
        }
        private void Save() 
        {
            string procedureName = "UpdatePrintGroup";

            int count = GetInt("count");
            string groupName = GetString("GroupName");
            Guid groupID = GetGuid("GroupID", Guid.Empty);

            // Check if group name is a duplicate.
            bool isDuplicate = CheckExistingName(groupID, groupName);

            if (isDuplicate) 
            {
                SetResult("Error", "Name already existed. Please choose a different group name.");
                return;
            }

            XmlDocument doc = new XmlDocument();

            XmlElement groupElement = doc.CreateElement("group");
            doc.AppendChild(groupElement);
            if (groupID == Guid.Empty) 
            {

                groupID = Guid.NewGuid();
                procedureName = "CreatePrintGroup";
            }
            groupElement.SetAttribute("id", groupID.ToString());
            groupElement.SetAttribute("name", groupName);

            for (int i = 0; i < count; i++) 
            {
                var type = GetString("item_" + i);
                
                // If the user added the Initial Fees worksheet, then also add in the 2015 version.
                if(type == "LendersOffice.Pdf.CInitialFeesWorksheetPDF")
                {
                    XmlElement ifw2015 = doc.CreateElement("print");
                    ifw2015.SetAttribute("type", "LendersOffice.Pdf.CIFW2015");
                    groupElement.AppendChild(ifw2015);
                }
                else if (type == "LendersOffice.Pdf.CStandardFundingWorksheetPDF")
                {
                    // Same for the Funding Worksheet
                    XmlElement fs2015 = doc.CreateElement("print");
                    fs2015.SetAttribute("type", "LendersOffice.Pdf.CFundingWorksheet2015");
                    groupElement.AppendChild(fs2015);
                }

                XmlElement printElement = doc.CreateElement("print");
                printElement.SetAttribute("type", type);
                groupElement.AppendChild(printElement);
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerUserPrincipal.CurrentPrincipal.BrokerId),
                                            new SqlParameter("@GroupID", groupID),
                                            new SqlParameter("@GroupName", groupName),
                                            new SqlParameter("@GroupDefinition", doc.InnerXml)
                                        };

			if( BrokerUser.HasPermission( Permission.CanEditPrintGroups ) == true )
			{
                StoredProcedureHelper.ExecuteNonQuery(BrokerUserPrincipal.CurrentPrincipal.BrokerId, procedureName, 0, parameters);
				SetResult("GroupID", groupID);
			}
			else
			{
				SetResult( "Error" , "Access denied. You lack permission to edit print groups." );
			}
        }
	}
}
