﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RateLockQuestions.aspx.cs" Inherits="LendersOfficeApp.los.RateLockQuestions.RateLockQuestions" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Rate Lock Questions</title>
    <link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
    <style>
        #BigContainer
        {
            width: 95%;	
        }
        .Questions
        {
        	width: 99%;
        }
        .Buttons
        {
            float:right;
        }
    </style>
</head>
<body onload="init();">
    <form id="form1" runat="server">
    
    <script type="text/javascript">
    <!--
        function m_CancelBtn_onclick() {
            onClosePopup();
        }
        
        function init() {
            window.resizeTo(600, 390);
        }
    -->
    </script>
    <div id="BigContainer">
    <asp:Repeater ID="m_Questions" runat="server" OnItemDataBound="m_Questions_OnItemDataBound">
        <HeaderTemplate>
            <p class="Header" style="margin-left: 5px;">
                Questions to ask when user submits a rate lock request
            </p>
            <ol>
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <%-- We don't use databinder.eval here because it requires aspxtools which will mess up the encoding --%>
                <asp:TextBox runat="server" ID="repeaterQuestion" CssClass="Questions">
                </asp:TextBox>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ol>
        </FooterTemplate>
    </asp:Repeater>
    
    <asp:Button cssClass="Buttons" runat="server" ID="m_CancelBtn" onclick="m_CancelBtn_onclick" OnClientClick="m_CancelBtn_onclick();" Text="Cancel"/>
    <asp:Button cssClass="Buttons" runat="server" ID="m_OKBtn" onclick="m_OKBtn_onclick" Text="OK"/>
    
    </div>
    </form>
</body>
</html>
