﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOfficeApp.newlos;
using LendersOffice.Security;
using LendersOffice.AntiXss;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RateLockQuestions
{
    public partial class RateLockQuestions : BasePage
    {
        private int numQuestions = 12;
        private BrokerDB broker;

        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private BrokerDB Broker
        {
            get
            {
                if (broker == null)
                {
                    broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
                }
                return broker;
            }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingLockDeskQuestions
                };
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var questions = Broker.RateLockQuestionsJson;
            if (questions.Count < numQuestions)
            {
                int remaining = numQuestions - questions.Count;
                for (int i = 0; i < remaining; i++)
                {
                    questions.Add("");
                }
            }

            m_Questions.DataSource = questions;
            if (!Page.IsPostBack)
            {
                m_Questions.DataBind();
            }
        }

        protected void m_Questions_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // Workaround so that we don't have to use AspxTools.HtmlString
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TextBox questionCtrl = (TextBox)e.Item.FindControl("repeaterQuestion");
                questionCtrl.MaxLength = ConstApp.RateLockQuestionMaxLength;
                string question = (string)e.Item.DataItem;
                questionCtrl.Text = question;
            }
        }

        protected void m_CancelBtn_onclick(object sender, EventArgs e)
        {
            
        }

        protected void m_OKBtn_onclick(object sender, EventArgs e)
        {
            // Get the questions
            var questions = new List<string>(numQuestions);
            foreach (RepeaterItem item in m_Questions.Items)
            {
                TextBox questionCtrl = (TextBox)item.FindControl("repeaterQuestion");
                string question = questionCtrl.Text;
                if (string.IsNullOrEmpty(question))
                {
                    continue;
                }

                // If a question is longer than X characters, truncate it. 
                if (question.Length > ConstApp.RateLockQuestionMaxLength)
                {
                    questions.Add(question.Substring(0, ConstApp.RateLockQuestionMaxLength));
                }
                else
                {
                    questions.Add(question);
                }
            }

            // Now store the questions in the datalayer
            Broker.RateLockQuestionsJson = questions;
            Broker.Save();

            ClientScript.RegisterStartupScript(this.GetType(), "close", "onClosePopup();", true);
        }
    }
}
