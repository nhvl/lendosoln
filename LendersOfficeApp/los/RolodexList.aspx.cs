namespace LendersOfficeApp.los
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using LendersOfficeApp.los.BrokerAdmin;

    public partial class RolodexList : LendersOffice.Common.BaseServicePage
	{
        private System.String m_value;
        private LosConvert m_losConvert = new LosConvert();
		private int m_tabIndex;
        private string m_sLicenseState;

        private const int CURRENT_LOAN_AGENTS = 0;
        private const int CONTACT_ENTRIES = 1;
        private const int INTERNAL_EMPLOYEES = 2;
        private const int PML_USERS = 3;
        private const int ASSIGNED_OFFICIAL_CONTACTS = 4;
        private const int MANUAL_ENTRY = 5;

		private BrokerUserPrincipal BrokerUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        protected bool ShowAssignedEmployees
        {
            get
            {
                return RequestHelper.GetBool("ass");
            }
        }

        protected bool IsFeeContactPicker
        {
            get
            {
                return RequestHelper.GetBool("isFeeContactPicker");
            }
        }

        protected bool IsCurrentAgentOnly
        {
            get
            {
                return RequestHelper.GetBool("isCurrentAgentOnly");
            }
        }

        protected bool OnlyTitleProviderCodes
        {
            get
            {
                return RequestHelper.GetBool("tpcodes");
            }
        }

        // If this is not the Contact entries tab, it will not contain
        // the Company Nm - Branch Nm column.
        protected string GetSafeCompanyNmBranchNm(object item)
        {
            return m_tabIndex == CONTACT_ENTRIES ? (string)DataBinder.Eval(item, "AgentComNmBranchNm") : string.Empty;
        }

        // If this is not the Internal Employees tab, it may not contain the NameOfBranch column.
        protected string GetSafeBranchNm(object item)
        {
            return m_tabIndex == INTERNAL_EMPLOYEES ? (string)DataBinder.Eval(item, "NameOfBranch") : string.Empty;
        }

        protected string GetFullAddress(object item)
        {
            if (m_tabIndex == CURRENT_LOAN_AGENTS || m_tabIndex == CONTACT_ENTRIES)
            {
                string companyAddress = SafeDataBinderEval(item, "AgentAddr");
                string companyCity = SafeDataBinderEval(item, "AgentCity");
                string companyState = SafeDataBinderEval(item, "AgentState");
                string companyZip = SafeDataBinderEval(item, "AgentZip");
                return Tools.FormatAddress(companyAddress, companyCity, companyState, companyZip);
            }
            else
            {
                return string.Empty;
            }
        }

        private string SafeDataBinderEval(object item, string expression)
        {
            object o = DataBinder.Eval(item, expression);

            if (Convert.IsDBNull(o))
            {
                return string.Empty;
            }
            else
            {
                return (string)o;
            }
        }

        private bool DisplayCurrentLoanAgentsTab
        {
            get { return RequestHelper.GetGuid("loanid", Guid.Empty) != Guid.Empty; }
        }

        protected int selectedTabIndex = 0;

        protected HtmlControl GenerateSelectLink( object item )
        {
            // Generate the selection link for each entry.  If we are
            // currently generating for internal employees, then fixup
            // the agent type with a role that best matches the employee's
            // type.
            
            DataRowView row = item as DataRowView;

            if (ShowAssignedEmployees)
            {
                //< input name = "batchSel" type = "checkbox" value = "t@yahoo.com" onclick = "onCheckRow(this);" >
                 var checkbox = new HtmlInputCheckBox();
                checkbox.Value = $"{row["AgentEmail"]}";
                checkbox.Attributes.Add("onclick", "onCheckRow(this);");
                checkbox.ID = "batchSel";
                checkbox.Name = checkbox.ID;

                return checkbox;
            }


            //OPM 6996 11/06 dc - if choosing an internal employee, choose phone/address info based on user selection: Employee, Branch, or Company			
            string typeOtherDescription = row["AgentTypeOtherDescription"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
            string branchAddress = "";
			string branchCity = "";
			string branchState = "";
			string branchZip = "";
			string branchPhone = "";
			string branchFax = "";
            string sAgentCompanyName = row["AgentComNm"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
            string companyId = "";
            string companyAddress = "";
            string companyCity = "";
            string companyState = "";
            string companyZip = "";
            string companyCounty = string.Empty;
            string employeeIDInCompany = "";
            string phoneOfCompany = row["PhoneOfCompany"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
            string faxOfCompany = row["FaxOfCompany"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
            string sLosIdentifier = string.Empty;
            string sCompanyLosIdentifier = string.Empty;
            string sAgentState = row["AgentState"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");            
            string sLicenseState = sAgentState;
            string sLicensesXml = row["LicenseXmlContent"].ToString().Replace("'", "\\'");//"<List><LicenseInfo><State>CA</State><ExpD>12/12/2020</ExpD><License>CA Employee License</License></LicenseInfo></List></LicenseInfoList>"; ;                        
            LicenseInfoList userLicenseInfoList = new LicenseInfoList();
            userLicenseInfoList.Retrieve(sLicensesXml);
            string notes = "";
            // OPM 109299
            string branchName = "";
            string payToBankName = "";
            string payToBankCityState = "";
            string payToABANumber = "";
            string payToAccountName = "";
            string payToAccountNumber = "";
            string furtherCreditToAccountNumber = "";
            string furtherCreditToAccountName = "";

            string cellphone = row["AgentAltPhone"].ToString();
            string agentAltPhone = cellphone;
            if (row.Row.Table.Columns.Contains("IsAgentAltPhoneForMultiFactorOnly"))
            {
                agentAltPhone = EmployeeDB.CalculatedCellphoneValue(cellphone, (bool)row["IsAgentAltPhoneForMultiFactorOnly"]);
            }

            string chumsId = "";
            string nameForPopulation = row["AgentNm"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");

            Guid brokerLevelAgentID = Guid.Empty;
            bool contactIsFromRolodex = false;

            Guid recordId = Guid.Empty;
            bool overrideLicenses = false;

            LicenseInfoList companyLicenseInfoList = null;
            Guid agentId = Guid.Empty;

            if (selectedTabIndex == CONTACT_ENTRIES || selectedTabIndex == CURRENT_LOAN_AGENTS) // From RolodexDB
            {
                // note agentid is passed to the 'select' JS method as the 'employeeid' parameter (even though they aren't an employee.)
                agentId = CommonFunctions.ToGuid_Safe(row["AgentID"].ToString().Replace("\'", "\\'").Replace("\"", "\\'")); 
                companyAddress = row["AgentAddr"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                companyCity = row["AgentCity"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                companyState = sAgentState;
                companyZip = row["AgentZip"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                companyCounty = row["AgentCounty"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");

                companyId = row["CompanyId"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");

                branchName = row["AgentBranchName"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                payToBankName = row["AgentPayToBankName"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                payToBankCityState = row["AgentPayToBankCityState"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                payToABANumber = row["AgentPayToABANumber"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                payToAccountName = row["AgentPayToAccountName"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                payToAccountNumber = row["AgentPayToAccountNumber"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                furtherCreditToAccountNumber = row["AgentFurtherCreditToAccountNumber"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                furtherCreditToAccountName = row["AgentFurtherCreditToAccountName"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                brokerLevelAgentID = agentId;
         

                if (selectedTabIndex == CONTACT_ENTRIES)
                {
                    contactIsFromRolodex = true;
                    notes = row["AgentN"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                    overrideLicenses = (bool)row["OverrideLicenses"];
                }
                else if (selectedTabIndex == CURRENT_LOAN_AGENTS)
                {
                    chumsId = AspxTools.JsString(row["ChumsID"].ToString()).Replace("'", string.Empty);
                    recordId = CommonFunctions.ToGuid_Safe(row["RecordId"].ToString().Replace("\'", "\\'").Replace("\"", "\\'"));
                    sLosIdentifier = row["LoanOriginatorIdentifier"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                    sCompanyLosIdentifier = row["CompanyLoanOriginatorIdentifier"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");

                    if (row["AgentSourceT"] != System.DBNull.Value)
                    {
                        contactIsFromRolodex = (E_AgentSourceT)row["AgentSourceT"] == E_AgentSourceT.CorporateList;
                    }
                }
            }
			else if( selectedTabIndex == INTERNAL_EMPLOYEES )  // Internal Employee
			{
                string agentnameonloandocs = row["AgentNmOnLoanDocs"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                if (!string.IsNullOrEmpty(agentnameonloandocs))
                    nameForPopulation = agentnameonloandocs;
                sAgentCompanyName = row["CompanyDisplayName"].ToString().Replace("\'", "\\'").Replace("\"", "\\'"); 
                agentId = CommonFunctions.ToGuid_Safe(row["AgentId"].ToString().Replace("\'", "\\'").Replace("\"", "\\'"));
                companyAddress = row["AddrOfCompany"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                companyCity = row["CityOfCompany"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                companyState = row["StateOfCompany"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                companyZip = row["ZipOfCompany"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");

                sLosIdentifier = row["LosIdentifier"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");                
                sCompanyLosIdentifier = row["BrokerLosIdentifier"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                string sCompanyLicensesXml = row["BrokerLicenseXml"].ToString().Replace("'", "\\'");
                companyLicenseInfoList = new LicenseInfoList();
                companyLicenseInfoList.Retrieve(sCompanyLicensesXml);
                employeeIDInCompany = row["EmployeeIDInCompany"].ToString().Replace("'", "\\'");
                bool bUseBranchInfoForLoans = (bool)row["UseBranchInfoForLoans"];
                chumsId = AspxTools.JsString(row["ChumsID"].ToString()).Replace("'", string.Empty);

                if (bUseBranchInfoForLoans)//override company info with the branch info
                {   
				    companyAddress = row[ "AddrOfBranch"            ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" );
				    companyCity = row[ "CityOfBranch"            ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" );
				    companyState = row[ "StateOfBranch"           ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" );
				    companyZip = row[ "ZipOfBranch"             ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" );
				    phoneOfCompany = row[ "PhoneOfBranch"       ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" );
				    faxOfCompany = row[ "FaxOfBranch"         ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" );                    
                    sCompanyLosIdentifier = row["BranchLosIdentifier"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                    
                    sCompanyLicensesXml = row["BranchLicenseXml"].ToString();
                    companyLicenseInfoList.Retrieve(sCompanyLicensesXml);
                }
            
			}
            else if (selectedTabIndex == PML_USERS)//PML user
            {
                try
                {
                    string agentnameonloandocs = row["AgentNmOnLoanDocs"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                    if (!string.IsNullOrEmpty(agentnameonloandocs))
                        nameForPopulation = agentnameonloandocs;
                    agentId = CommonFunctions.ToGuid_Safe(row["AgentId"].ToString().Replace("\'", "\\'").Replace("\"", "\\'"));
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerByName(sAgentCompanyName, BrokerUser.BrokerId);
                    sAgentCompanyName = pmlBroker.NmlsName;
                    companyAddress = pmlBroker.Addr;
                    companyCity = pmlBroker.City;
                    companyState = pmlBroker.State;
                    companyZip = pmlBroker.Zip;
                    
                    phoneOfCompany = pmlBroker.Phone;
                    faxOfCompany = pmlBroker.Fax;
                    sLosIdentifier = row["LosIdentifier"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
                    sCompanyLosIdentifier = pmlBroker.NmLsIdentifier;
                    companyLicenseInfoList = pmlBroker.LicenseInfoList;
                }
                catch{}
            }

            if (companyLicenseInfoList != null)
            {
                foreach (LicenseInfo licenseInfo in companyLicenseInfoList)
                {
                    licenseInfo.DefaultDisplayName = sAgentCompanyName;
                    licenseInfo.DefaultPhone = phoneOfCompany;
                    licenseInfo.DefaultStreet = companyAddress;
                    licenseInfo.DefaultZip = companyZip;
                    licenseInfo.DefaultCity = companyCity;
                    licenseInfo.DefaultAddrState = companyState;
                    licenseInfo.DefaultFax = faxOfCompany;
                }
            }
            
            string isNotifyWhenLoanStatusChange = row["IsNotifyWhenLoanStatusChange"].ToString() == "True" ? "1" : "0";

            string sAgentLicenseNumber = row["AgentLicenseNumber"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");
            string sCompanyLicenseNumber = row["CompanyLicenseNumber"].ToString().Replace("\'", "\\'").Replace("\"", "\\'");

            List<List<string>> sEmptyStringList = new List<List<string>>();
            string sJsonSerlializedAgentLicenses = ObsoleteSerializationHelper.JsonSerialize(sEmptyStringList);
            string sJsonSerlializedCompanyLicenses = ObsoleteSerializationHelper.JsonSerialize(sEmptyStringList);

            if (selectedTabIndex == INTERNAL_EMPLOYEES || selectedTabIndex == PML_USERS)
            {
                sJsonSerlializedAgentLicenses = LicenseInfoList.JsonSerialize(userLicenseInfoList);
                sJsonSerlializedCompanyLicenses = LicenseInfoList.JsonSerialize(companyLicenseInfoList);

                if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["loanid"]))
                {
                    sLicenseState = m_sLicenseState;
                }
                sAgentLicenseNumber = DataAccess.CAgentFields.GetLendingLicenseByState(sLicenseState, userLicenseInfoList);
                sCompanyLicenseNumber = DataAccess.CAgentFields.GetLendingLicenseByState(sLicenseState, companyLicenseInfoList);
            }

            if (companyLicenseInfoList != null)
            {
                foreach (LicenseInfo licenseInfo in companyLicenseInfoList)
                {
                    if (licenseInfo.License == sCompanyLicenseNumber)
                    {
                        if (licenseInfo.DisplayName != "")
                        {
                            sAgentCompanyName = licenseInfo.DisplayName;
                            companyAddress = licenseInfo.Street;
                            companyCity = licenseInfo.City;
                            companyState = licenseInfo.AddrState;
                            companyZip = licenseInfo.Zip;
                            phoneOfCompany = licenseInfo.Phone;
                            faxOfCompany = licenseInfo.Fax;
                        }
                    }
                }
            }    

            decimal commissionPointOfLoanAmount = row["CommissionPointOfLoanAmount"] == DBNull.Value ? 0 : (decimal)row["CommissionPointOfLoanAmount"];
            decimal commissionPointOfGrossProfit = row["CommissionPointOfGrossProfit"] == DBNull.Value ? 0 : (decimal)row["CommissionPointOfGrossProfit"];
            decimal commissionMinBase = row["CommissionMinBase"] == DBNull.Value ? 0 : (decimal)row["CommissionMinBase"];

            var agentType = row["AgentType"].ToString();

            HtmlAnchor link = new HtmlAnchor();
            link.InnerText = "select";
            link.Attributes["href"] = "javascript:void(0);";
            link.Attributes["tabIndex"] = m_rolodexGrid.TabIndex.ToString();

            link.Attributes["onclick"] =  String.Format
				( 
                 "select('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}','{28}','{29}','{30}','{31}', '{32}', '{33}', '{34}', '{35}', '{36}', '{37}', '{38}', '{39}', '{40}', '{41}', '{42}', '{43}', '{44}', '{45}', '{46}', '{47}', '{48}', '{49}', '{50}', '{51}', '{52}', '{53}', '{54}', '{55}', '{56}');"
				, nameForPopulation  // 0
                , agentType.Replace("\'", "\\'").Replace("\"", "\\'") // 1
				, row[ "AgentAddr"            ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 2
				, row[ "AgentCity"            ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 3
				, sAgentState  // 4
				, row[ "AgentZip"             ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 5
				, agentAltPhone.ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 6
                , sAgentCompanyName // 7
				, row[ "AgentEmail"           ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 8
				, row[ "AgentFax"             ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 9
				, row[ "AgentPhone"           ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 10
				, row[ "AgentTitle"           ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 11
				, row[ "AgentDepartmentName"  ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 12
				, row[ "AgentPager"           ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 13
                , sAgentLicenseNumber // 14
                , sCompanyLicenseNumber// 15
				, phoneOfCompany //row[ "PhoneOfCompany"       ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 16
				, faxOfCompany //frow[ "FaxOfCompany"         ].ToString().Replace( "\'", "\\'" ).Replace( "\"", "\\'" ) // 17
                , isNotifyWhenLoanStatusChange // 18
				, branchAddress //19
				, branchCity //20
				, branchState //21
				, branchZip //22
				, branchPhone //23
				, branchFax //24
				, companyAddress //25
				, companyCity //26
				, companyState //27
				, companyZip //28
                , m_losConvert.ToRateString(commissionPointOfLoanAmount) // 29
                , m_losConvert.ToRateString(commissionPointOfGrossProfit) // 30
                , m_losConvert.ToMoneyString(commissionMinBase, FormatDirection.ToRep) // 31
                , sLosIdentifier // 32
                , sCompanyLosIdentifier //33
                , "LICENSES.List[\\'User\\'] = " + sJsonSerlializedAgentLicenses.Replace("\'", "\\'").Replace("\"", "\\'") + ";" //sLicensesXml.Replace("\"","\\'")//34
                , "LICENSES.List[\\'Company\\'] = " + sJsonSerlializedCompanyLicenses.Replace("\'", "\\'").Replace("\"", "\\'") + ";" // Replace("\"", "\\\"") //35
                , agentId //36
                , companyId //37
                , employeeIDInCompany //38
                , branchName // 39
                , payToBankName // 40
                , payToBankCityState // 41
                , payToABANumber // 42
                , payToAccountName // 43
                , payToAccountNumber // 44
                , furtherCreditToAccountName // 45
                , furtherCreditToAccountNumber // 46
                , brokerLevelAgentID // 47
                , contactIsFromRolodex ? "1" : "0"// 48
                , chumsId //49
                , AspxTools.JsStringUnquoted(notes)
                , recordId //51
                , overrideLicenses ? "1" : "0" //52
                , agentType == E_AgentRoleT.Lender.ToString("D") ? "1" : "0" // 53
                , agentType == E_AgentRoleT.Broker.ToString("D") ? "1" : "0" // 54
                , companyCounty // 55
                , row["AgentTypeOtherDescription"].ToString().Replace("\'", "\\'").Replace("\"", "\\'") // 57
                );

            return link;
        }

        private void BindTypeFilter()
        {
            // Initialize the dropdown filter based on the currently
            // selected view mode.  We don't filter internal employees.

            int current = m_typeFilter.SelectedIndex;
            
            m_typeFilter.Items.Clear();
			m_typeFilter.Enabled = true;

            if (selectedTabIndex == CONTACT_ENTRIES || selectedTabIndex == CURRENT_LOAN_AGENTS)
                RolodexDB.PopulateAgentTypeDropDownList(m_typeFilter);

            if (selectedTabIndex == INTERNAL_EMPLOYEES)
            {
                int i = 0;

				foreach( Role role in Role.LendingQBRoles )
				{
					m_typeFilter.Items.Add( new ListItem( role.ModifiableDesc , i.ToString() ) );
					++i;
				}
			}

			// PML Users do not have a type, they are all Loan Officers,
			// so we disable the ddl.
			if (selectedTabIndex == PML_USERS)
				m_typeFilter.Enabled = false;

            // We always add all to the list so that filtering can
            // be turned off.
            
            m_typeFilter.Items.Insert(0, new ListItem("All", "-1"));

            if (current < m_typeFilter.Items.Count)
            {
                if (m_value != null && m_value != "") 
                    Tools.SetDropDownListValue(m_typeFilter, m_value); 
                else 
                    m_typeFilter.SelectedIndex = current;
            }
            else
                m_typeFilter.SelectedIndex = 0;
        }

        private void BindStatusFilter()
        {
            if (!Page.IsPostBack)
            {
                m_statusFilter.Items.Clear();
                m_statusFilter.Items.Add(new ListItem("Active", "1"));
                m_statusFilter.Items.Add(new ListItem("Inactive", "0"));
                m_statusFilter.SelectedIndex = 0;
                if (ShowAssignedEmployees)
                {
                    selectedTabIndex = ASSIGNED_OFFICIAL_CONTACTS;
                }
            }
            if (selectedTabIndex == INTERNAL_EMPLOYEES)
            {
                m_statusFilter.Visible = true;
                m_statusLiteral.Visible = true;
            }
            else
            {
                m_statusFilter.Visible = false;
                m_statusLiteral.Visible = false;
            }
        }

		private void BindDataGrid()
        {
            // Initialize the sql parameters and the actual procedure
            // to use based on the current view mode.  If the user is
			// restricted, then we do not bind.

			ListDictionary aD = new ListDictionary();

			DataSet        dS = new DataSet();

			if( BrokerUser.HasPermission( Permission.AllowReadingFromRolodex ) == false )
				return;

			aD.Add( "manager"             , E_AgentRoleT.Manager              );
			aD.Add( "lockdesk"            , E_AgentRoleT.Underwriter          );
			aD.Add( "underwriter"         , E_AgentRoleT.Underwriter          );
			aD.Add( "processor"           , E_AgentRoleT.Processor            );
			aD.Add( "loanopener"          , E_AgentRoleT.LoanOpener           );
			aD.Add( "agent"               , E_AgentRoleT.LoanOfficer          );
			aD.Add( "lenderaccountexec"   , E_AgentRoleT.Lender               );
			aD.Add( "realestateagent"     , E_AgentRoleT.SellingAgent         );
			aD.Add( "telemarketer"        , E_AgentRoleT.CallCenterAgent      );
			aD.Add( "accountant"          , E_AgentRoleT.Underwriter          );
            aD.Add( "funder"              , E_AgentRoleT.Funder               );
            aD.Add( "shipper"             , E_AgentRoleT.Shipper              );
            aD.Add( "postcloser"          , E_AgentRoleT.PostCloser           );
            aD.Add( "insuring"            , E_AgentRoleT.Insuring             );
            aD.Add( "collateralagent"     , E_AgentRoleT.CollateralAgent      );
            aD.Add( "docdrawer"           , E_AgentRoleT.DocDrawer            );
            aD.Add( "creditauditor"       , E_AgentRoleT.CreditAuditor        );
            aD.Add( "disclosuredesk"      , E_AgentRoleT.DisclosureDesk       );
            aD.Add( "juniorprocessor"     , E_AgentRoleT.JuniorProcessor      );
            aD.Add( "juniorunderwriter"   , E_AgentRoleT.JuniorUnderwriter    );
            aD.Add( "legalauditor"        , E_AgentRoleT.LegalAuditor         );
            aD.Add( "loanofficerassistant", E_AgentRoleT.LoanOfficerAssistant );
            aD.Add( "purchaser"           , E_AgentRoleT.Purchaser            );
            aD.Add( "qccompliance"        , E_AgentRoleT.QCCompliance         );
            aD.Add( "secondary"           , E_AgentRoleT.Secondary            );
            aD.Add( "servicing"           , E_AgentRoleT.Servicing            );
			
			m_tabIndex = selectedTabIndex;

            #region Current Loan Agents
            if (m_tabIndex == CURRENT_LOAN_AGENTS)
            {
                E_AgentRoleT? type = null;
                string searchText = null;

                if (m_typeFilter.SelectedItem.Value != "-1")
                {
                    type = (E_AgentRoleT)Enum.Parse(typeof(E_AgentRoleT), m_typeFilter.SelectedItem.Value);
                }
                if (m_searchFilter.Text.Length > 0)
                {
                    searchText = m_searchFilter.Text;
                }

                Guid sLId = RequestHelper.LoanID;
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(RolodexList));
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();
                dS = dataLoan.GetAgentDataSetForRolodex(type, searchText);

                m_rolodexGrid.Columns[4].Visible = false; // Agent Title
                m_rolodexGrid.Columns[7].Visible = false; // Company Name - Branch Name
                m_rolodexGrid.Columns[8].Visible = false; // Branch Name
            }
            #endregion

            #region Contact Entries Tab is selected
            // Contacts
			if( m_tabIndex == CONTACT_ENTRIES )
            {
                // Display external entries from rolodex table
                // when external view is selected.
                
				m_importFrom.Visible = false;
				
				Object typeFilter = DBNull.Value;
				Object nameFilter = DBNull.Value;

                if (m_typeFilter.SelectedItem.Value != "-1")
                {
                    typeFilter = int.Parse(m_typeFilter.SelectedItem.Value);
                }

                if (m_searchFilter.Text.Length > 0)
                {
                    nameFilter = m_searchFilter.Text;
                }

                SqlParameter[] parameters = {
                                                 new SqlParameter( "@BrokerID"   , BrokerUser.BrokerId )
					                            , new SqlParameter( "@NameFilter" , nameFilter          )
					                            , new SqlParameter( "@TypeFilter" , typeFilter          )
                                                , new SqlParameter( "@IsApprovedFilter", true             )
                                            };

                var spToUse = "ListRolodexByBrokerID";
                if (OnlyTitleProviderCodes)
                {
                    spToUse = "ListRolodexByBrokerIDWithTitleProviderCodes";
                }

                DataSetHelper.Fill( dS, BrokerUser.BrokerId, spToUse, parameters);

                // 2/1/13 gf - Hide the Company Name column and show
                // Company Name - Branch Name column in its place.
                m_rolodexGrid.Columns[6].Visible = false;
                m_rolodexGrid.Columns[8].Visible = false;   // Hide Branch Name Column
            }
            #endregion

            #region Internal Employees Tab is selected
            if ( m_tabIndex == INTERNAL_EMPLOYEES )
            {
                // Display internal employees from current broker
                // when internal view is selected.  We also turn
                // off two columns for internal employees: the
                // type and title are not needed.
                
				m_importFrom.Visible = true;

                Object typeFilter = DBNull.Value;
                Object nameFilter = DBNull.Value;
                Object statusFilter = DBNull.Value;

                if (m_typeFilter.SelectedItem.Value != "-1")
                    typeFilter = m_typeFilter.SelectedItem.Text;
                if (m_statusFilter.SelectedItem.Value != "-1")
                    statusFilter = m_statusFilter.SelectedItem.Value;
                if (m_searchFilter.Text.Length > 0)
                    nameFilter = m_searchFilter.Text;

                SqlParameter param = new SqlParameter("@UserType", SqlDbType.Char, 1);
                param.Value = 'B';

                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@BrokerID"   , BrokerUser.BrokerId));
                parameters.Add(param);
                parameters.Add(new SqlParameter("@NameFilter" , nameFilter));
                parameters.Add(new SqlParameter("@TypeFilter" , typeFilter));
                parameters.Add(new SqlParameter("@StatusFilter" , statusFilter));

                // OPM 55304 - If user cannot assign loans to agents of any branch and
                // does not have Corporate level access, then limit employees list to only those
                // employees from the same branch as user.
                if (BrokerUser.HasPermission(Permission.AllowLoanAssignmentsToAnyBranch) == false
                    && BrokerUser.HasPermission(Permission.BrokerLevelAccess) == false)
                {
                    parameters.Add(new SqlParameter("@BranchId", BrokerUser.BranchId));
                }

				DataSetHelper.Fill( dS, BrokerUser.BrokerId, "ListRolodexUsingEmployeeByBrokerID", parameters);

				if( m_typeFilter.SelectedIndex > 0 )
				{
					// Get the search filter's setting and map it to the
					// corresponding type.  We bind all employees to that
					// type because they're the only roles that should
					// show up.

					E_AgentRoleT aType = E_AgentRoleT.Other;

					foreach( Role role in Role.LendingQBRoles )
					{
						if( role.ModifiableDesc == m_typeFilter.SelectedItem.Text.TrimWhitespaceAndBOM() )
						{
							if( aD.Contains( role.Desc.ToLower() ) == true )
								aType = ( E_AgentRoleT ) aD[ role.Desc.ToLower() ];
						}
					}

					foreach( DataTable dT in dS.Tables )
					{
						foreach( DataRow dR in dT.Rows )
						{
							// 8/16/2005 kb - Fixup the rolodex binding to put
							// the equivalent agent type enum value in the type
							// column for internal employees.  Because each
							// employee can have multiple roles, we take the
							// currently selected role and use that.  If all
							// roles are shown, then we take the highest ranking
							// (non-administrator) role and map it to the agent
							// type equivalent.

							dR[ "AgentType" ] = aType;
						}
					}
				}
				else
				{
					// All employees are shown, so map each type to the
					// corresponding agent type.  We use the most important
					// role the employee maintains.

					BrokerLoanAssignmentTable brokerLoanAssignmentTable = new BrokerLoanAssignmentTable();

                    brokerLoanAssignmentTable.Retrieve(BrokerUser.BrokerId);

					foreach( DataTable dT in dS.Tables )
					{
						foreach( DataRow dR in dT.Rows )
						{
							// 8/16/2005 kb - Fixup the rolodex binding to put
							// the equivalent agent type enum value in the type
							// column for internal employees.  Because each
							// employee can have multiple roles, we take the
							// currently selected role and use that.  If all
							// roles are shown, then we take the highest ranking
							// (non-administrator) role and map it to the agent
							// type equivalent.

							String sRoleDesc = "";
							Int32  nRoleRank = -1;

                            foreach (var role in brokerLoanAssignmentTable[(Guid)dR["AgentId"]])
							{
                                if (role.ImportanceRank > nRoleRank && role.RoleT != E_RoleT.Administrator)
								{
                                    sRoleDesc = role.Desc;
                                    nRoleRank = role.ImportanceRank;
								}
							}

							if( aD.Contains( sRoleDesc.ToLower() ) == true )
								dR[ "AgentType" ] = ( E_AgentRoleT ) aD[ sRoleDesc.ToLower() ];
							else
								dR[ "AgentType" ] = E_AgentRoleT.Other;
						}
					}
				}

				// 8/16/2005 kb - We now show the type description, and
				// still hide the title for internal employees.
				// 2/28/06 mf - OPM 4052. Hide Company Phone in Internal Employees 
                // 2/1/13 gf - Hide Company Name - Branch Name column

                m_rolodexGrid.Columns[4].Visible = false; // Agent Title
				m_rolodexGrid.Columns[6].Visible = false; // Company Name
                m_rolodexGrid.Columns[7].Visible = false; // Company Name - Branch Name
				m_rolodexGrid.Columns[9].Visible = false; // Company Phone
                m_rolodexGrid.Columns[11].Visible = false; // Full Address
            }
            #endregion

            #region PML Users Tab is selected
            if (m_tabIndex == PML_USERS)
			{
				// Because this list may be large, we do not allow
				// displaying of PML users without a search term.

				m_importFrom.Visible = false;
				
				Object nameFilter = DBNull.Value;

				if( m_searchFilter.Text.TrimWhitespaceAndBOM().Length > 0 )
				{
					nameFilter = m_searchFilter.Text;

					SqlParameter param = new SqlParameter("@UserType", SqlDbType.Char, 1);
					param.Value = 'P';
                    SqlParameter[] parameters = {
                                                    new SqlParameter( "@BrokerID"   , BrokerUser.BrokerId )
						                            , param
						                            , new SqlParameter( "@NameFilter" , nameFilter          )
                                                };

					DataSetHelper.Fill( dS, BrokerUser.BrokerId, "ListRolodexUsingEmployeeByBrokerID", parameters);

					// We hide the following columns for the PML view:
					// Type, Title, Company Phone.
					m_rolodexGrid.Columns[2].Visible = false; // Agent Type Desc
					m_rolodexGrid.Columns[4].Visible = false; // Agent Title
                    m_rolodexGrid.Columns[7].Visible = false; // Company Name - Branch Name
                    m_rolodexGrid.Columns[8].Visible = false; // Hide Branch Name Column
                    m_rolodexGrid.Columns[9].Visible = false; // Company Phone
                    //m_rolodexGrid.Columns[6].Visible = false; // 6/30/2009 dd -Hide the company name because we do not retrieve correct value for "P" user.
                    m_rolodexGrid.Columns[11].Visible = false; // Full Address
                }

            }


            #endregion

            #region Official Contacts
            if (m_tabIndex == ASSIGNED_OFFICIAL_CONTACTS)
            {
                // 3/31/14 gf - opm 138296, changed this to use "loanid" instead of "sLId"
                // so it would match the naming scheme the rest of the tabs expect.
                Guid sLId = RequestHelper.LoanID;

                SqlParameter[] parameters = {
                                                new SqlParameter("@sLId", sLId), 
                                                new SqlParameter("@BrokerId", BrokerUser.BrokerId)
                                            };
                DataSetHelper.Fill(dS, BrokerUser.BrokerId, "ListRolodexByLoanId", parameters);
                CPageData data = CPageData.CreateUsingSmartDependency(sLId, typeof(RolodexList));
                data.AllowLoadWhileQP2Sandboxed = true;
                data.InitLoad();

                for(int i = 0; i < data.sAgentCollection.GetAgentRecordCount(); i++)
                {
                    CAgentFields agentData = data.sAgentCollection.GetAgentFields(i);
                    DataRow row = dS.Tables[0].NewRow();
                    row["AgentTypeDesc"] = agentData.AgentRoleDescription;
                    row["AgentNm"] = agentData.AgentName;
                    row["AgentPhone"] = agentData.Phone;
                    row["AgentComNm"] = agentData.CompanyName;
                    row["PhoneOfCompany"] = agentData.PhoneOfCompany;
                    row["AgentEmail"] = agentData.EmailAddr;
                    dS.Tables[0].Rows.Add(row);
                }

                m_rolodexGrid.Columns[7].Visible = false; // Company Name - Branch Name
                m_rolodexGrid.Columns[8].Visible = false; // Hide Branch Name Column
                m_rolodexGrid.Columns[11].Visible = false; // Full Address
            }
            #endregion

            // Given the query results, we bind them to the
            // current data grid.

            if( dS.Tables.Count > 0)
            {
                // We need to fixup the table with an extra column that we
                // use to display the returned agent type.  We need this so
                // that sorting on the column sorts according to text values,
                // not the enum integer-based representation.
                // 3/26/14 gf - opm 138296, we also want to add the type 
                // description for the current loan agents.
                if (m_tabIndex != ASSIGNED_OFFICIAL_CONTACTS)
                {
                    dS.Tables[0].Columns.Add("AgentTypeDesc", typeof(string));

                    foreach (DataRow row in dS.Tables[0].Rows)
                    {
                        row["AgentTypeDesc"] = RolodexDB.GetTypeDescription((E_AgentRoleT)row["AgentType"]);
                    }

                }
                // Connect the table to the layout.

                m_rolodexGrid.DefaultSortExpression = "AgentNm ASC";
				m_rolodexGrid.DataSource            = dS.Tables[ 0 ].DefaultView;
				m_rolodexGrid.DataBind();
            }
        }

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
        {
            IncludeStyleSheet("~/css/Tabs.css");

            if (DisplayCurrentLoanAgentsTab)
            {
                RegisterJsGlobalVariables("sLId", RequestHelper.LoanID);
            }

            RegisterJsGlobalVariables("ShowAssignedEmployees", ShowAssignedEmployees);

            if (ViewState["selectedTabIndex"] != null)
                ((HtmlGenericControl)Form.FindControl("tab" + (Int32)ViewState["selectedTabIndex"])).Attributes.Remove("class");

            if (Request.Params["__EVENTTARGET"] == "changeTab")
                selectedTabIndex = Int32.Parse(Request.Params["__EVENTARGUMENT"]);
            else if (ViewState["selectedTabIndex"] != null)
                selectedTabIndex = (Int32)ViewState["selectedTabIndex"];
            else if (RequestHelper.GetInt("tab", -1) != -1)
                selectedTabIndex = RequestHelper.GetInt("tab");

            TabsRow.Visible = !IsCurrentAgentOnly;

            ClearContainer.Visible = IsFeeContactPicker && selectedTabIndex != MANUAL_ENTRY;

            SearchControls.Visible =  selectedTabIndex != MANUAL_ENTRY;

            ContactInfoRow.Visible = selectedTabIndex == MANUAL_ENTRY;

            Guid loanId = RequestHelper.GetGuid("loanid");
            if (loanId != Guid.Empty)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(RolodexList));
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();
                m_sLicenseState  = dataLoan.sSpState;
            }

			// Show current rolodex and selection entries.  We
			// hide the grid based on permissions.
			try
			{
				if( !Page.IsPostBack )
				{
                    // 3/26/14 gf - opm 138296, If we don't have the loan id available,
                    // hide the current loan agents tab and select contact entries.
                    if (!DisplayCurrentLoanAgentsTab)
                    {
                        tab0.Visible = false;
                        if (selectedTabIndex == CURRENT_LOAN_AGENTS)
                        {
                            selectedTabIndex = CONTACT_ENTRIES;
                        }
                    }

                    if (!BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
					{
                        
						// Remove PML portion.
                        tab3.Visible = false;
					}

                    if (false == ShowAssignedEmployees)
                    {
                        tab4.Visible = false;
                    }

					// Initialize type filter with predefine rolodex type.
					m_value = RequestHelper.GetSafeQueryString("type");
					
				}

				// 02/16/06 mf OPM 4052 - Set visibility of add buttons and edit links
				if (!BrokerUser.HasPermission(Permission.AllowWritingToRolodex) || ! (selectedTabIndex == CONTACT_ENTRIES) )
				{
					m_addPanelTop.Visible = false;
					m_addPanelBottom.Visible = false;
					m_rolodexGrid.Columns[1].Visible = false;
				}
				else
				{
					m_addPanelTop.Visible = true;
					m_addPanelBottom.Visible = true;
					m_rolodexGrid.Columns[1].Visible = true;
				}

				// Set visibility of grid.  When we move to 2.0, the MultiView
				// control will allow us to avoid all this panel management.
				if( BrokerUser.HasPermission( Permission.AllowReadingFromRolodex ) == false )
				{
					m_rolodexDenied.Visible = true;
					m_rolodexGrid.Visible   = false;
					m_chooseSearchTerm.Visible = false;
				}
				else
				{
					if ( selectedTabIndex == PML_USERS && m_searchFilter.Text.TrimWhitespaceAndBOM().Length == 0 )
					{
						m_rolodexGrid.Visible = false;
						m_chooseSearchTerm.Visible = true;
						m_rolodexDenied.Visible = false;
					}
                    else if (selectedTabIndex == MANUAL_ENTRY)
                    {
                        m_rolodexGrid.Visible = false;
                        m_chooseSearchTerm.Visible = false;
                        m_rolodexDenied.Visible = false;
                    }
                    else
                    {
                        m_chooseSearchTerm.Visible = false;
                        m_rolodexDenied.Visible = false;
                        m_rolodexGrid.Visible = true;
                    }
				}                
			}
			catch( Exception e )
			{
				// Oops!
				Tools.LogError( e );
			}
		}

		/// <summary>
		/// Render this page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Bind grid and drop downs with latest.

			try
			{
				BindTypeFilter();
                BindStatusFilter();
				BindDataGrid();

                ((HtmlGenericControl)Form.FindControl("tab" + selectedTabIndex)).Attributes.Add("class", "selected");
                ViewState["selectedTabIndex"] = selectedTabIndex;
                if (selectedTabIndex == ASSIGNED_OFFICIAL_CONTACTS)
                {
                    SearchControls.Visible = false;
                    m_addPanelBottom.Visible = false;
                    m_addPanelTop.Visible = false;
                }
			}
			catch( Exception e )
			{
				// Oops!

				Tools.LogError( e );
			}
		}

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.EnableJqueryMigrate = false;
			InitializeComponent();
            RegisterService("rolodex_list", "/los/RolodexListService.aspx");
            RegisterJsScript("LQBPopup.js");
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);

		}
		#endregion

        private void onItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Header) 
            {
				e.Item.Attributes.Add("onmouseover", "highlightRow(this);");
                e.Item.Attributes.Add("onmouseout", "unhighlightRow(this);");
            }
        }

        protected void onRolodexSetChanged(object sender, System.EventArgs e)
        {
            // External/internal selection changed, so reset
            // the dropdown selection index.
			m_typeFilter.SelectedIndex = 0;
            m_statusFilter.SelectedIndex = 0;
            BindStatusFilter();
        }

		// 12/26/06 db - OPM 8938
		/// <summary>
		/// Handles the ItemDataBound event in the rolodex DataGrid
		/// </summary>
        protected void RolodexItemBound(object sender, DataGridItemEventArgs e)
        {
            //dont care about the rest
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            HtmlAnchor emailAnchor = (HtmlAnchor)e.Item.FindControl("EmailLink");

            if (ShowAssignedEmployees)
            {
                emailAnchor.Attributes.Add("onclick", string.Format("chooseEmail('{0}');", DataBinder.Eval(e.Item.DataItem, "AgentEmail")));
                emailAnchor.HRef = "#";
            }
            else
            {
                emailAnchor.HRef = string.Format("mailto:{0}", DataBinder.Eval(e.Item.DataItem, "AgentEmail"));
            }

            if (m_tabIndex != CONTACT_ENTRIES)
            {
                return;
            }
            // If the row has a website URL, display the company name as a link
            // to that URL.  If there is no company name, display the link as
            // the static text "[ website ]"

            // The column of company name is hard-coded to 6.  This may change
            // if future columns are added to the grid, but speed would be sacrificed
            // if this had to be recomputed every time a row was bound.
            string websiteUrl;
            if ((websiteUrl = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AgentWebsiteUrl"))) != String.Empty)
            {
                if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AgentComNm")) != String.Empty)
                    e.Item.Cells[6].Text = DataBinder.Eval(e.Item.DataItem, "AgentComNm", "<A onclick=\"window.open('" + websiteUrl + "');\" href=\"#\">{0}</A>");
                else
                    e.Item.Cells[6].Text = DataBinder.Eval(e.Item.DataItem, "AgentComNm", "[ <A onclick=\"window.open('" + websiteUrl + "');\" href=\"#\"> website</A> ]");
            }
        }
    }
}
