<%@ Page language="c#" Codebehind="PricingGroupList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.PricingGroup.PricingGroupList" EnableViewState ="false"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<html>
  <head runat="server">
    <title>PricingGroupList</title>
    <style>
      .NoItemMsg { PADDING-LEFT: 10px; FONT-WEIGHT: bold; COLOR: white }
      </style>
</head>
  <body class="LeftBackground">
	<script type="text/javascript">
<!--
var g_previousLinkID = null;
var g_initLinkID = '';

function f_highlightLink(id) {
  var link = document.getElementById(id);
  if (null != link) {
    link.style.backgroundColor = 'yellow';
    link.style.color = 'black';  
  }  
}
function f_unhighlightLink(id) {
  var link = document.getElementById(id);
  if (null != link) {
    link.style.backgroundColor = '';
    link.style.color = '';  
  }
}
function f_updateName(id, name) {
  var id = "_" + id.replace(/-/gi, '');
  var o = document.getElementById(id);
  if (null != o) o.innerText = name;
}
function _init() {
    g_initLinkID = '_' + ML.InitialPriceGroupId;

  f_highlightLink(g_initLinkID);
  g_previousLinkID = g_initLinkID;
  parent.edit.location = 'PricingGroupEdit.aspx?id=' + ML.InitialPriceGroupId; 
}

function goTo(id){
  window.location.href = window.location.pathname + "?id=" + encodeURIComponent(id);
}

function f_confirmSaveGroup(handler) {
  parent.edit.PolyShouldShowConfirmSave(typeof(parent.edit.isDirty) == 'function' && parent.edit.isDirty(), handler, parent.edit.f_save);
}

function f_editPricingGroup(id, o) {
  f_confirmSaveGroup(function() {
    parent.edit.goTo(id); 
    if (null != g_previousLinkID)
      f_unhighlightLink(g_previousLinkID);
    
    if (null != o) {
      f_highlightLink(o.id);
      g_previousLinkID = o.id;
    }
  });
  return false;
}

	    function f_addPricingGroup()
	    {
	        return f_editPricingGroup(ML.GuidEmpty);
	    }
//-->
</script>

    <form id="PricingGroupList" method="post" runat="server">
    <table class="Width100Pc">
      <tr><td><input type=button value="Add new price group" class="ButtonStyle" style="WIDTH: 168px" onclick="return f_addPricingGroup();"></td></tr>
      <tr><td class="FieldLabel" style="COLOR:#ffcc00">Active Price Groups</td></tr>
  <asp:Repeater id=m_activePricingGroup runat="server" EnableViewState="False" ItemType="LendersOffice.ObjLib.PriceGroups.PriceGroup">
      <ItemTemplate>
      <tr><td><img src="~/images/bullet.gif" align="absmiddle" runat="server" />&nbsp;&nbsp;
      <a id="_<%# AspxTools.HtmlString(Item.ID.ToString("N"))%>" href="#" class="PortletLink" onclick='return f_editPricingGroup("<%# AspxTools.JsStringUnquoted(Item.ID.ToString("N"))%>", this);'><%# AspxTools.HtmlString(Item.Name)%></a></td></tr>
      </ItemTemplate>
      
    </asp:Repeater>
        <tr>
            <td class="NoItemMsg"><ml:EncodedLiteral ID="m_noActiveMsg" runat="server" /></td>
        </tr>
      <tr><td><hr></td></tr>
      <tr><td class="FieldLabel" style="COLOR:#ffcc00">Inactive Price Groups</td></tr>
        <asp:Repeater ID="m_inactivePricingGroup" runat="server" ItemType="LendersOffice.ObjLib.PriceGroups.PriceGroup">
            <ItemTemplate>
                <tr>
                    <td><img src="~/images/bullet.gif" align="absmiddle" runat="server" />&nbsp;&nbsp;
      <a id="_<%# AspxTools.HtmlString(Item.ID.ToString("N"))%>" href="#" class="PortletLink" onclick='return f_editPricingGroup("<%# AspxTools.JsStringUnquoted(Item.ID.ToString("N"))%>", this);'><%# AspxTools.HtmlString(Item.Name)%></a></td>
                </tr>
            </ItemTemplate>

        </asp:Repeater>
  <tr>
    <td class=NoItemMsg><ml:EncodedLiteral id=m_noInactiveMsg runat="server" EnableViewState="False" /></td></tr>      

    </table>
     </form>
	
  </body>
</html>
