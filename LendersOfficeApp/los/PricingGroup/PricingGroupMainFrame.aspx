<%@ Page language="c#" Codebehind="PricingGroupMainFrame.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.PricingGroup.PricingGroupMainFrame" %>
<!DOCTYPE HTML>
<html>

<head runat="server">
    <title>Price Groups</title>
    <script type="text/javascript">
        var w = screen.availWidth;
        var h = screen.availHeight;
        window.moveTo(0, 0);
        window.resizeTo(w, h);
    </script>
</head>

<body class="body-iframe">
    <form runat="server">
        <div class="left">
            <iframe name="list" src="PricingGroupList.aspx"></iframe>
        </div>
        <div class="right">
            <iframe name="edit" src="PricingGroupEdit.aspx"></iframe>
        </div>
    </form>
</body>

</html>
