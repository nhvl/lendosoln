using System;
using System.Collections.Generic;
using System.Linq;
using LendersOffice.Common;
using LendersOffice.ObjLib.PriceGroups;
using LendersOffice.Security;

namespace LendersOfficeApp.los.PricingGroup
{
    public partial class PricingGroupList : LendersOffice.Common.BasePage
	{
	    /// <summary>
	    /// IE requires the meta tag to be FIRST in head. If this method returns a meta
	    /// the base page will add it on pre render. We should make it easier for dev to do this 
	    /// </summary>
	    /// <returns></returns>
	    protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
	    {
	        return E_XUAComaptibleValue.Edge;
	    }

        protected void PageLoad(object sender, System.EventArgs e)
		{
            Guid initialPricingGroup = Guid.Empty;

            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            Guid id = RequestHelper.GetGuid("id", Guid.Empty);
            if (id != Guid.Empty)
            {
                initialPricingGroup = id;
            }

            List<PriceGroup> activeList = new List<PriceGroup>();
            List<PriceGroup> inactiveList = new List<PriceGroup>();

            var mainList = PriceGroup.RetrieveAllFromBroker(principal.BrokerId).OrderByDescending(o => o.ExternalPriceGroupEnabled).ThenBy(o => o.Name);

            if (initialPricingGroup == Guid.Empty)
            {
                if (mainList.Any())
                {
                    initialPricingGroup = mainList.ElementAt(0).ID;
                }
            }

            foreach (var o in mainList)
            {
                if (o.ExternalPriceGroupEnabled)
                {
                    activeList.Add(o);
                }
                else
                {
                    inactiveList.Add(o);
                }
            }

            if (activeList.Count == 0)
            {
                m_noActiveMsg.Text = "No Active Price Groups.";
            }
            else
            {
                m_activePricingGroup.DataSource = activeList;
                m_activePricingGroup.DataBind();
            }

            if (inactiveList.Count == 0)
            {
                m_noInactiveMsg.Text = "No Inactive Price Groups.";
            }
            else
            {
                m_inactivePricingGroup.DataSource = inactiveList;
                m_inactivePricingGroup.DataBind();
            }

            this.RegisterJsGlobalVariables("InitialPriceGroupId", initialPricingGroup.ToString("N"));
        }

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterVbsScript("common.vbs");
            this.RegisterJsGlobalVariables("GuidEmpty", Guid.Empty.ToString());
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion
	}
}