using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.ObjLib.LockPolicies;
using LendersOffice.ObjLib.PriceGroups;
using LendersOffice.RatePrice.Helpers;
using LendersOffice.Security;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.PricingGroup
{

    public partial class PricingGroupEdit : LendersOffice.Common.BaseServicePage
	{
	    /// <summary>
	    /// IE requires the meta tag to be FIRST in head. If this method returns a meta
	    /// the base page will add it on pre render. We should make it easier for dev to do this 
	    /// </summary>
	    /// <returns></returns>
	    protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
	    {
	        return E_XUAComaptibleValue.Edge;
	    }

        protected Guid m_lpePriceGroupId = Guid.Empty;
        protected bool m_externalPriceGroupEnabled = true;
		protected bool m_useRateSheetExpirationFeature = false;

        // dd 3/9/2018 - No client is using DataTrac integration. DataTrac company is dead. However there are too many references in UI, so I will leave the property here.
        protected bool m_isDataTracEnabled = false;

        private HashSet<Guid> m_programsWithRates;
        protected int m_currentIndex = 0;
        protected List<string> m_lLpTemplateIdList = new List<string>();

        private BrokerUserPrincipal BrokerUser 
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

		protected bool m_showLpeEditLinks
		{
			get { return BrokerUser.HasPermission( Permission.CanModifyLoanPrograms); }
		}

		protected bool canEditDataTrac
		{
			get { return BrokerUser.HasPermission( Permission.CanEditDataTracLPNames); }
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
            RegisterJsScript("utilities.js");
			m_lpePriceGroupId = RequestHelper.GetGuid("id", Guid.Empty);
            Tools.Bind_LenderPaidOriginatorCompensationOptionT(m_LenderPaidOriginatorCompensationOptionT);
            Tools.Bind_LenderLockPolicy(m_LockPolicyID, BrokerUser.BrokerId);

            this.RegisterJsScript("LQBPopup.js");

            m_programsWithRates = new HashSet<Guid>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "PriceGroupRateOptionGet", new SqlParameter("@PriceGroupId", m_lpePriceGroupId)))
            {
                while (reader.Read())
                {
                    m_programsWithRates.Add((Guid)reader["lLpTemplateId"]);
                }
            }
			
			if (RequestHelper.GetSafeQueryString("cmd") == "csv") 
            {
                ExportToCsv();
                return;
			}

			BindPriceGroupLists();
			if (m_lpePriceGroupId != Guid.Empty)
			{
				LoadData();
			}
			else 
			{
				LoadProductAssocation(Guid.Empty);
                LoadDefaultsFromBroker();
                
			}

		}

		private void BindPriceGroupLists()
		{
            List<KeyValuePair<string, string>> priceGroupList = new List<KeyValuePair<string, string>>() ;

            SqlParameter[] parameters = {
                                            new SqlParameter( "@BrokerId" , BrokerUser.BrokerId )
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "ListPricingGroupByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    Guid key = (Guid)reader["LpePriceGroupId"];
                    string name = (string)reader["LpePriceGroupName"];

                    if (key != m_lpePriceGroupId)
                    {
                        priceGroupList.Add(new KeyValuePair<string, string>(key.ToString(), name));
                    }
                }
            }

            IncDDL.DataTextField = "Value";
            IncDDL.DataValueField = "Key";
            IncDDL.DataSource = priceGroupList;
            IncDDL.DataBind();


            RateDDL.DataTextField = "Value";
            RateDDL.DataValueField = "Key";
            RateDDL.DataSource = priceGroupList;
            RateDDL.DataBind();

            PointDDL.DataTextField = "Value";
            PointDDL.DataValueField = "Key";
            PointDDL.DataSource = priceGroupList;
            PointDDL.DataBind();


            MarginDDL.DataTextField = "Value";
            MarginDDL.DataValueField = "Key";
            MarginDDL.DataSource = priceGroupList;
            MarginDDL.DataBind();


            YspDDL.DataTextField = "Value";
            YspDDL.DataValueField = "Key";
            YspDDL.DataSource = priceGroupList;
            YspDDL.DataBind();

		}

        private void ExportToCsv() 
        {
			//opm 24638 fs 09/16/08
			BrokerDB broker = BrokerDB.RetrieveById( BrokerUser.BrokerId );


            bool isExportlLpTemplateId = BrokerUser.HasPermission(Permission.CanModifyLoanPrograms);

            IEnumerable<PricingGroupAssociationItem> pricingGroupAssociationItemList = PricingGroupAssociationItem.ListPricingGroupAssociation(broker, m_lpePriceGroupId);
            List<string> list = new List<string>();

            foreach (var item in pricingGroupAssociationItemList)
            {
                string visibility = "Hide";

                if (item.IsValid.HasValue && item.IsValid.Value)
                {
                    visibility = "Show";
                }

                string rate = item.LenderRateMargin.HasValue ? item.LenderRateMargin.Value.ToString() : string.Empty;
                string point = item.LenderProfitMargin.HasValue ? item.LenderProfitMargin.Value.ToString() : string.Empty;
                string investor = item.lLpInvestorNm;
                string prodCode = item.ProductCode;
                string templateName = item.lLpTemplateNm;
                string ysp = item.LenderMaxYspAdjust.HasValue ? item.LenderMaxYspAdjust.Value.ToString() : string.Empty;
                string lien = item.lLienPosT == E_sLienPosT.First ? "1st" : "2nd";
                string margin = item.LenderMarginMargin.HasValue ? item.LenderMarginMargin.ToString() : string.Empty;
                string dataTracName = item.lDataTracLpId;

                string line = string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\"",
                    visibility, investor.Replace("\"", "\"\""), prodCode.Replace("\"", "\"\""), lien, templateName, rate, point, margin, ysp);
                if (isExportlLpTemplateId)
                {
                    line += ",\"" + item.lLpTemplateId + "\"";
                }
                list.Add(line);

            }

            Response.Clear();
            Response.ContentType = "application/text";

			//OPM 8041
			System.Text.StringBuilder filename = new System.Text.StringBuilder();
			filename.Append("PriceGroup");
			
            var pg = PriceGroup.RetrieveByID(m_lpePriceGroupId, BrokerUser.BrokerId);
            if (pg != null)
            {
                filename.Append("_");
                string filenameToCheck = pg.Name;
                string filenameStripPath = Regex.Replace(filenameToCheck, @"^.*\\", "");
                string filenameStripInvalidChars = Regex.Replace(filenameStripPath, @"[?:\/*""<>|]", "");
                if (filenameStripInvalidChars.Length > 256)
                {
                    filenameStripInvalidChars = filenameStripInvalidChars.Substring(0, 255);
                }
                filename.Append(filenameStripInvalidChars);
            }
			
			filename.Append(".csv");
			Response.AddHeader("Content-Disposition", $"attachment; filename=\"{filename}\"");

            string headerRow = "\"Visibility\",\"Investor Name\",\"Prod Code\",\"Lien\",\"Loan Program\",\"Adj Rate By\",\"Adj Point By\",\"Adj Margin By\",\"Reduce Max YSP By\"";

            if (isExportlLpTemplateId)
            {
                headerRow += ",\"lLpTemplateId\"";
            }
            Response.Write(headerRow);
            foreach (string line in list) 
            {
				
                Response.Write(Environment.NewLine);
                Response.Write(line);
            }

            // fb 124877
            // NOTE: DON'T CALL BOTH FLUSH AND END.
            // End() can terminates the call to flush before it has finished, cutting off part of the file
            //Response.Flush();
            Response.End();
        }

        private void LoadDefaultsFromBroker()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerUser.BrokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "GetPriceGroupDefaultsByBrokerId", parameters))
            {
                if (reader.Read())
                {
                    Tools.SetDropDownListValue(m_LenderPaidOriginatorCompensationOptionT, (E_LenderPaidOriginatorCompensationOptionT)reader["LenderPaidOriginatorCompensationOptionT"]);
                    Tools.SetDropDownListValue(m_LockPolicyID, ((Guid)reader["DefaultLockPolicyID"]).ToString());
                    m_displayPmlFeeIn100Format.SelectedIndex = (bool)reader["DisplayPmlFeeIn100Format"] ? 1 : 0;
                    m_roundUpLpeFee.SelectedIndex = (bool)reader["IsRoundUpLpeFee"] ? 1 : 0;
                    m_roundUpLpeFeeInterval.Text = BrokerDB.ToDecimalString((decimal)reader["RoundUpLpeFeeToInterval"]);
                    m_alwaysDisplayExactParRateOption.SelectedIndex = (bool)reader["IsAlwaysDisplayExactParRateOption"] ? 1 : 0;
                }
            }
        }

	    private void LoadProductAssocation(Guid lpePriceGroupId) 
        {
            // Retrieve Loan program associatin.
            BrokerDB broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
            LockPolicy lockPolicy = null;

            if (lpePriceGroupId == Guid.Empty) 
            {
                lockPolicy = LockPolicy.Retrieve(broker.BrokerID, broker.DefaultLockPolicyID.Value);
                m_useRateSheetExpirationFeature = lockPolicy.IsUsingRateSheetExpirationFeature;
            } 
            else 
            {
                lockPolicy = LockPolicy.Retrieve(broker.BrokerID,
                                                    PriceGroup.RetrieveByID(
                                                          lpePriceGroupId
                                                        , BrokerUser.BrokerId
                                                    ).LockPolicyID.Value
                                                );
                m_useRateSheetExpirationFeature = lockPolicy.IsUsingRateSheetExpirationFeature;
            }

			// 03/14/07 mf. OPM 10722.  We want to add two additional columns
			// for the download start and end times.  We use the currently
			// released snapshot to ensure accuracy.
            IEnumerable<PricingGroupAssociationItem> programAssociationList = PricingGroupAssociationItem.ListPricingGroupAssociation(broker, lpePriceGroupId);

            if (m_useRateSheetExpirationFeature)
            {
                SetRateExpirationMsg(broker, programAssociationList, lockPolicy);
            }

            var downloadTimes = GetDownloadTimes(broker, lpePriceGroupId);

			// For batch selection of Investors
			List<string> investorNames = new List<string>();
			List<string> productCodes = new List<string>();
            foreach (var programRow in programAssociationList)
			{
                // Set the download times from our Snapshot source
                string date = string.Empty;
				if ( downloadTimes.TryGetValue( programRow.lLpTemplateId, out date ) )
				{
					programRow.lRateSheetDownloadStartD = date;
				}

				// Build investor and product code lists for selection in client UI
                if (!investorNames.Contains("'" + programRow.lLpInvestorNm + "'"))
                {
                    investorNames.Add("'" + programRow.lLpInvestorNm + "'");
                }

                if (!productCodes.Contains("'" + programRow.ProductCode + "'"))
                {
                    productCodes.Add("'" + programRow.ProductCode + "'");
                }
			}
			investorNames.Sort();
			productCodes.Sort();

			ClientScript.RegisterArrayDeclaration("m_investorNames", string.Join(",", investorNames));
			ClientScript.RegisterArrayDeclaration("m_productCodes", string.Join(",", productCodes));

            PricingGroupRepeater.DataSource = programAssociationList;
			PricingGroupRepeater.DataBind();

            ClientScript.RegisterArrayDeclaration("g_aIds", string.Join(",", m_lLpTemplateIdList));
        }

		// OPM 19887 mf. 01/30/08.  We evaluate if this program would
		// currently be blocked because of rate sheet expiration.
		private void SetRateExpirationMsg( BrokerDB broker, IEnumerable<PricingGroupAssociationItem> programList, LockPolicy lockPolicy)
		{
			LendersOfficeApp.los.RatePrice.RateOptionExpirationResult result;

            List<string> investorNamesForRateLockCutOff = new List<string>();
            List<string> rsFileIdsForFileInfo = new List<string>();
            List<Tuple<string, string>> investorProductCodePair = new List<Tuple<string, string>>();
            foreach (var program in programList)
            {
                investorNamesForRateLockCutOff.Add(program.lLpInvestorNm);
                rsFileIdsForFileInfo.Add(program.LpeAcceptableRsFileId);
                investorProductCodePair.Add(new Tuple<string, string>(program.lLpInvestorNm, program.ProductCode));
            }

            RateOptionExpirationDataLoader dataLoader = new RateOptionExpirationDataLoader(investorNamesForRateLockCutOff, rsFileIdsForFileInfo, investorProductCodePair, broker);
            foreach (PricingGroupAssociationItem program in programList)
			{
				result = LendersOfficeApp.los.RatePrice.CApplicantPriceOLD.DetermineRateOptionExpiration(
					lockPolicy
					, E_LpeRsExpirationByPassPermissionT.NoByPass
					, program.LpeAcceptableRsFileId
					, program.LpeAcceptableRsFileVersionNumber.HasValue ? program.LpeAcceptableRsFileVersionNumber.Value : 0
					, program.lLpInvestorNm
					, program.ProductCode
                    , false /* byPassManualDisabled */
					, dataLoader);

                if (result.IsHiddenFromResult)
                {
                    program.BlockMessage = AspxTools.HtmlString(string.Format("The {0} {1} product has been excluded from the pricing result via the Disable Pricing feature. It will not be displayed in the pricing result until it is removed from the disabled product list, even if it is marked as Visible (Show) within price groups.", 
                        program.lLpInvestorNm,
                        program.ProductCode )); 
					
                }
                else
                {
                    program.BlockMessage = AspxTools.HtmlString(result.GetFriendlyRateLockSubmissionErrorMessage(lockPolicy.LockPolicyId, E_LpeRsExpirationByPassPermissionT.NoByPass, BrokerUser.BrokerId));
                }

                program.IsHiddenFromResult = result.IsHiddenFromResult;

			}
		}

		// Create a hashtable to map Ids to download times
		private Dictionary<Guid, string> GetDownloadTimes(BrokerDB broker, Guid pricegroupid )
		{
            Dictionary<Guid, string> times = new Dictionary<Guid, string>();

            if (broker.ActualPricingBrokerId == Guid.Empty)
            {
                LendersOfficeApp.los.RatePrice.CLpeReleaseInfo info = LendersOfficeApp.los.RatePrice.CLpeReleaseInfo.GetLatestReleaseInfo();

                DataSet ds = new DataSet();
                SqlParameter[] parameters = {
                                            new SqlParameter( "@BrokerID", broker.BrokerID )
                                        };
                DataSetHelper.Fill(ds, info.DataSrcForSnapshot, "ListRateSheetDownloadDatesByBrokerID", parameters);

                DataTable downloadTimeTable = ds.Tables[0];

                foreach (DataRow rateDownloadRow in downloadTimeTable.Rows)
                {
                    string startD = rateDownloadRow["lRateSheetDownloadStartD"].ToString();
                    string endD = rateDownloadRow["lRateSheetDownloadEndD"].ToString();

                    // Cheap way to tell if we are looking at base datetime.
                    // Parsing is more expensive.

                    times[(Guid)rateDownloadRow["lLpTemplateId"]] = startD.StartsWith("1/1/2000") ? string.Empty : startD;
                }

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "PriceGroupRateOptionDlTimeGet", new SqlParameter("@PriceGroupId", pricegroupid)))
                {
                    while (reader.Read())
                    {
                        DateTime dtStart = (DateTime)reader["lRateSheetDownloadStartD"];
                        DateTime dtEnd = (DateTime)reader["lRateSheetDownloadEndD"];

                        times[(Guid)reader["lLpTemplateId"]] = dtStart.ToString();
                    }
                }
            }

            return times;
        }

        private void LoadData() 
        {
            var pg = PriceGroup.RetrieveByID(m_lpePriceGroupId, BrokerUser.BrokerId);
            if (pg != null)
            {
                LpePriceGroupId.Text = pg.ID.ToString();
                LpePriceGroupName.Text = pg.Name;
                ActualPriceGroupId.Text = pg.ActualPriceGroupId.ToString();
                m_externalPriceGroupEnabled = pg.ExternalPriceGroupEnabled;
                Tools.SetDropDownListValue(m_LenderPaidOriginatorCompensationOptionT, pg.LenderPaidOriginatorCompensationOptionT);
                Tools.SetDropDownListValue(m_LockPolicyID, (pg.LockPolicyID.Value).ToString());
                m_displayPmlFeeIn100Format.SelectedIndex = pg.DisplayPmlFeeIn100Format ? 1 : 0;
                m_roundUpLpeFee.SelectedIndex = pg.IsRoundUpLpeFee ? 1 : 0;
                m_alwaysDisplayExactParRateOption.SelectedIndex = pg.IsAlwaysDisplayExactParRateOption ? 1 : 0;
                m_roundUpLpeFeeInterval.Text = BrokerDB.ToDecimalString(pg.RoundUpLpeFeeToInterval);
            }
            LpePriceGroupName.Attributes.Add("oldValue", AspxTools.JsStringUnquoted(LpePriceGroupName.Text));
            LoadProductAssocation(m_lpePriceGroupId);
        }

        protected string GenerateIncludeText(object container) 
        {
            PricingGroupAssociationItem item = container as PricingGroupAssociationItem;

            string str = "Show";

            if (item != null)
            {
                if (item.HasPriceGroupInformation == false)
                {
                    str = "Hide";
                }
                else if (item.IsValid.HasValue && item.IsValid == false)
                {
                    str = "Hide";
                }
            }

            return str;
        }

        protected string GenerateRateMargin(object container) 
        {
            PricingGroupAssociationItem item = container as PricingGroupAssociationItem;

            if (item != null)
            {
                if (item.HasPriceGroupInformation)
                {
                    return item.LenderRateMargin.Value.ToString() + "%";
                }
            }
            return "0%";
        }

        protected string GenerateMarginMargin(object container) 
        {
            PricingGroupAssociationItem item = container as PricingGroupAssociationItem;

            if (item != null)
            {
                if (item.HasPriceGroupInformation)
                {
                    return item.LenderMarginMargin.Value.ToString() + "%";
                }
            }
            return "0%";
        }

        protected string GenerateProfitMargin(object container) 
        {
            PricingGroupAssociationItem item = container as PricingGroupAssociationItem;

            if (item != null)
            {
                if (item.HasPriceGroupInformation)
                {
                    return item.LenderProfitMargin.Value.ToString() + "%";
                }
            }
            return "0%";
        }

        protected string GenerateLienPosition(object container) 
        {
            PricingGroupAssociationItem item = container as PricingGroupAssociationItem;

            if (item != null)
            {
                    return item.lLienPosT == E_sLienPosT.First ? "1<sup>st</sup>" : "2<sup>nd</sup>";
            }

            return string.Empty;
        }

        protected string GenerateYSP(object container) 
        {
            PricingGroupAssociationItem item = container as PricingGroupAssociationItem;

            if (item != null)
            {
                if (item.HasPriceGroupInformation)
                {
                    return item.LenderMaxYspAdjust.Value.ToString() + "%";
                }
            }

            return "0%";
        }
		
		// 4/16/07 mf. We use style to determine if we are override or not
		protected string GenerateYSPStyle( object container )
		{
            PricingGroupAssociationItem item = container as PricingGroupAssociationItem;

            bool yspLock = false;
            if (item != null)
            {
                if (item.HasPriceGroupInformation)
                {
                    if (item.LenderMaxYspAdjustLckd.HasValue)
                    {
                        yspLock = item.LenderMaxYspAdjustLckd.Value;
                    }
                }
            }

            return yspLock ? "FONT-WEIGHT: bold;" : "";
		}

		// 01/29/08 OPM 19887 mf. We need the blocked status.  For performance,
		// better way is to cache the messages on the client since multiple programs
		// could point to the same message.
		protected string GenerateBlockStatus( object container )
		{
            if (!m_useRateSheetExpirationFeature)
            {
                return string.Empty;
            }

            PricingGroupAssociationItem item = container as PricingGroupAssociationItem;

            string blockMsg = string.Empty;

            if (item != null)
            {
                blockMsg = item.BlockMessage;

                if (string.IsNullOrEmpty(blockMsg) == false)
                {
                    return String.Format("Yes<br> (<a href='#' onclick='f_showBlockedStatus(\"{0}\",\"{1}\",\"{2}\", {3} , event );'>details</a>)"
                            , Utilities.SafeJsString(blockMsg)
                            , Utilities.SafeJsString(item.ProductCode)
                            , Utilities.SafeJsString(item.lLpInvestorNm)
                            , Utilities.SafeJsString(item.IsHiddenFromResult.ToString().ToLower())
                            );

                }
                else
                {
                    return "No";
                }

            }

            return "No";
		}

		protected string GenerateLoanProgramName( object container )
		{
            PricingGroupAssociationItem item = container as PricingGroupAssociationItem;

            string ret = string.Empty;

            if (item != null)
            {
                ret = Utilities.SafeHtmlString(item.lLpTemplateNm);
                string rates = "";
                Guid programId = item.lLpTemplateId;
                if (m_programsWithRates.Contains(programId))
                {
                    rates = string.Format(@"&nbsp;<a href=""#"" onclick=""return rateview('{0}', '{1}', event);"">rates</a>", m_lpePriceGroupId, programId);
                }
                if (BrokerUser.HasPermission(Permission.CanModifyLoanPrograms))
                {
                    ret +=
                        String.Format("&nbsp;-&nbsp;<a href=\"#\" onclick=\"f_onRuleEdit('{0}','{1}');\">rules</a>{2}"
                        , item.lLpTemplateId.ToString()
                        , Utilities.SafeHtmlString(Utilities.SafeJsString(item.lLpTemplateNm))
                        , rates
                        );
                }

            }

            return ret;
		}
		//opm 24638 fs 09/11/08
		protected string GenerateDataTracProgramName( object container )
		{
            PricingGroupAssociationItem item = container as PricingGroupAssociationItem;

            string ret = string.Empty;

            if (item != null)
            {
                ret = item.lDataTracLpId;
            }
			return ret;
		}

        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterJsScript("mask.js");
            this.RegisterVbsScript("common.vbs");
            this.RegisterService("main", "/los/PricingGroup/PricingGroupEditService.aspx");
            PricingGroupRepeater.ItemDataBound += new RepeaterItemEventHandler(this.PricingGroupRepeater_ItemDataBound);
        }

        private void PricingGroupRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e) 
        {
            m_currentIndex++;

            var dataItem = e.Item.DataItem;

            m_lLpTemplateIdList.Add("'" + DataBinder.Eval(dataItem, "lLpTemplateId").ToString() + "'");

            var lienPositionLbl = (PassthroughLabel)e.Item.FindControl("lienPosition");
            lienPositionLbl.Text = GenerateLienPosition(dataItem);

            var loanProgramNameLbl = (PassthroughLabel)e.Item.FindControl("loanProgramName");
            loanProgramNameLbl.Text = GenerateLoanProgramName(dataItem);

            var rateSheetBlockStatusLbl = (PassthroughLabel)e.Item.FindControl("rateSheetBlockStatus");
            rateSheetBlockStatusLbl.Text = GenerateBlockStatus(dataItem);
        }



        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
