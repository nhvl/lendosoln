using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Xml.Linq;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.ObjLib.PriceGroups;
using LendersOffice.ObjLib.PriceGroups.Model;
using LendersOffice.Security;

namespace LendersOfficeApp.los.PricingGroup
{

    public partial class PricingGroupEditService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "Save":
                    Save();
                    break;
                case "ValidateLpePriceGroupName":
                    ValidateLpePriceGroupName();
                    break;
                case "EnableDisablePricingGroup":
                    EnableDisablePricingGroup();
                    break;
				case "PrepareBatchEdit":
					PrepareBatchEdit();
					break;
				case "ImportValuesFromDB":
					ImportValuesFromDB();
					break;
                case "AdjustValues":
                    AdjustValues();
                    break;
                case "RateView":
                    GetRates();
                    break;
            }
        }
        public void GetRates()
        {
            Guid pricegroupId =GetGuid("PriceGroupId");
            Guid templateId = GetGuid("TemplateId");

            if (!PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanModifyLoanPrograms))
            {
                SetResult("Status","Denied");
                return;
            }
            XDocument doc = null;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "PriceGroupRateOptionGet",
                new SqlParameter("@PriceGroupId", pricegroupId), new SqlParameter("@TemplateId", templateId)))
            {
                if (reader.Read())
                {
                    doc = XDocument.Parse(reader["lRateSheetXmlContent"].ToString());
                }
            }

            if (doc == null)
            {
                SetResult("Status", "Not Found");
                return;
            }


            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("{0,15}{1,15}{2,15}{3,15}{4,15}{5}",
                "Rate", "Point", "Margin", "QRateBase", "TeaserRate", System.Environment.NewLine);

            foreach (XElement element in doc.Root.Elements("RateSheetXmlContent"))
            {
                builder.AppendFormat("{0,15}{1,15}{2,15}{3,15}{4,15}{5}",
                    GetSafeElement(element, "Rate"),
                    GetSafeElement(element, "Point"),
                    GetSafeElement(element, "Margin"),
                    GetSafeElement(element, "QRateBase"),
                    GetSafeElement(element, "TeaserRate"),
                    System.Environment.NewLine);
            }

            SetResult("Status", "OK");
            SetResult("Rates", builder.ToString());
        }

        private static string GetSafeElement(XElement element, string name)
        {
            XElement node = element.Element(name);

            if (node == null)
            {
                return "";
            }

            return node.Value;
        }
		private void ImportValuesFromDB()
		{
			Guid thisTPGId = GetGuid("thisTPGId");
			Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            BrokerDB broker = BrokerDB.RetrieveById(brokerId);

            IEnumerable<PricingGroupAssociationItem> pricingGroupAssociationList = PricingGroupAssociationItem.ListPricingGroupAssociation(broker, thisTPGId);

			bool IncCol = GetBool("IncCol");
			bool RateCol = GetBool("RateCol");
			bool DataTracCol = GetBool("DataTracCol");  //opm 24638 fs 09/12/08
			bool PointCol = GetBool("PointCol");
			bool MarginCol = GetBool("MarginCol");
			bool YspCol = GetBool("YspCol");

			//////////////////////////////////////////////////////////////////////////////////////////
			// - Check which columns to access and return only values from rows that are checked    //
			// - We speed up the checking process by indexing by columns first. There are only five //

			int numberOfCheckedPIds = GetInt("numOfCheckedPIds");
			bool prodNotFound = true;

			//foreach ( DataRow programRow in priceGroupProductTable.Rows )
            foreach (var programRow in pricingGroupAssociationList)
			{ 
				for(int o=0; o < numberOfCheckedPIds && (prodNotFound); o++)
				{
					Guid productId = GetGuid("checkedPId" + o);
					if(programRow.lLpTemplateId == productId)
					{
						prodNotFound = false;
						if(IncCol)
						{
							if(programRow.IsValid.HasValue && programRow.IsValid.Value)
							{
								SetResult("prod" + o + "IncCol", "Show");
							}
							else
							{
								SetResult("prod" + o + "IncCol", "Hide");
							}
						}

						if(RateCol)
						{
							string str = "0%";

                            if (programRow.LenderRateMargin.HasValue)
                            {
                                str = programRow.LenderRateMargin.ToString() + "%";
                            }

							SetResult("prod" + o + "RateCol", str);
						}

						if(DataTracCol) //opm 24638 fs 09/12/08
						{
							string str = programRow.lDataTracLpId;

							SetResult("prod" + o + "DataTracCol", str);
						}

						if(PointCol)
						{
							string str = "0%";

                            if (programRow.LenderProfitMargin.HasValue)
                            {
                                str = programRow.LenderProfitMargin.Value.ToString() + "%";
                            }

							SetResult("prod" + o + "PointCol", str);
						}

						if(MarginCol)
						{
							string str = "0%";

                            if (programRow.LenderMarginMargin.HasValue)
                            {
                                str = programRow.LenderMarginMargin.Value.ToString() + "%";
                            }

							SetResult("prod" + o + "MarginCol", str);
						}

						if(YspCol)
						{
							bool yspLock = false;

                            if (programRow.LenderMaxYspAdjustLckd.HasValue)
                            {
                                yspLock = programRow.LenderMaxYspAdjustLckd.Value;
                            }

							bool YspImportSetting = GetBool("YspImportSetting");
							if(!YspImportSetting)
							{
								yspLock = !YspImportSetting;
							}


							if(!yspLock) // UsePointAdj setting
							{
								string str = "POINT";
								SetResult("prod" + o + "YspCol", str);
								// RETURN POINT TO SIGNIFY IMPORT POINT ADJ SETTING
								
							}
							else // ELSE OVERRIDE WITH EXPLICIT
							{
								string str = "0%:o";

                                if (programRow.LenderMaxYspAdjust.HasValue)
                                {
                                    str = programRow.LenderMaxYspAdjust.Value + "%:o";
                                }

								SetResult("prod" + o + "YspCol", str);
							}
						}
					}
				}
				prodNotFound = true; // reset found flag, next line
			}
			return;
		}

        private void EnableDisablePricingGroup() 
        {
            Guid brokerId =  BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            Guid lpePriceGroupId = GetGuid("LpePriceGroupId");
            bool externalPriceGroupEnabled = GetBool("ExternalPriceGroupEnabled");

            var pricingGroup = PriceGroup.RetrieveByID(lpePriceGroupId, brokerId);
            pricingGroup.ExternalPriceGroupEnabled = externalPriceGroupEnabled;
            pricingGroup.Save(BrokerUserPrincipal.CurrentPrincipal, null);

            SetResult("LpePriceGroupId", lpePriceGroupId);
            SetResult("RefreshList", true);


        }
        private void ValidateLpePriceGroupName() 
        {
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            Guid lpePriceGroupId = GetGuid("LpePriceGroupId");
            string lpePriceGroupName = GetString("LpePriceGroupName");
            bool isValid = PriceGroup.IsLpePriceGroupNameAvailable(brokerId, lpePriceGroupId, lpePriceGroupName);

            SetResult("IsValid", isValid);
        }

        private void Save() 
        {
            SavePricingGroupInfo();
        }

        private void SavePricingGroupInfo() 
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            Guid brokerId = principal.BrokerId;

            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);

            bool isAllowEditDataTracLpName = broker.IsDataTracIntegrationEnabled && principal.HasPermission(Permission.CanEditDataTracLPNames);

            Guid lpePriceGroupId = GetGuid("LpePriceGroupId");
            string lpePriceGroupName = GetString("LpePriceGroupName");
            var LenderPaidOriginatorCompensationOptionT = (E_LenderPaidOriginatorCompensationOptionT) GetInt("LenderPaidOriginatorCompensationOptionT");
            Guid lockPolicyID = GetGuid("LockPolicyID");
            bool displayPmlFeeIn100Format = GetBool("DisplayPmlFeeIn100Format");
            bool isRoundUpLpeFee = GetBool("IsRoundUpLpeFee");
            decimal roundUpLpeFeeToInterval;
            if(!decimal.TryParse(GetString("RoundUpLpeFeeInterval"), out roundUpLpeFeeToInterval))
            {
                roundUpLpeFeeToInterval = 0.000M;
            }
            bool isAlwaysDisplayExactParRateOption = GetBool("IsAlwaysDisplayExactParRateOption");

            PriceGroup priceGroup = null;

            if (lpePriceGroupId == Guid.Empty) 
            {   
                priceGroup = PriceGroup.CreateNew(brokerId);
                
                SetResult("RefreshList", true);
            } 
            else 
            {
                priceGroup = PriceGroup.RetrieveByID(lpePriceGroupId, brokerId);
            }
            
            SetResult("LpePriceGroupId", priceGroup.ID);

            Guid actualPriceGroupId = GetGuid("ActualPriceGroupId", Guid.Empty);

            if (principal.HasPermission(Permission.CanModifyLoanPrograms))
            {
                priceGroup.ActualPriceGroupId = actualPriceGroupId;
            }

            priceGroup.Name = lpePriceGroupName;
            priceGroup.LenderPaidOriginatorCompensationOptionT = LenderPaidOriginatorCompensationOptionT;
            priceGroup.LockPolicyID = lockPolicyID;
            priceGroup.DisplayPmlFeeIn100Format = displayPmlFeeIn100Format;
            priceGroup.IsRoundUpLpeFee = isRoundUpLpeFee;
            priceGroup.RoundUpLpeFeeToInterval = roundUpLpeFeeToInterval;
            priceGroup.IsAlwaysDisplayExactParRateOption = isAlwaysDisplayExactParRateOption;

            var lpePriceGroupProductList = this.GetUpdatePriceGroupProductList();

            priceGroup.Save(principal, lpePriceGroupProductList);

            if (isAllowEditDataTracLpName)
            {
                var dataTracNameList = this.GetUpdateDataTracNameList();
                CLoanProductData.BatchUpdateDataTracLoanProgramName(dataTracNameList);
            }

        }

        private IEnumerable<LpePriceGroupProduct> GetUpdatePriceGroupProductList()
        {
            List<LpePriceGroupProduct> updatePriceGroupProductList = new List<LpePriceGroupProduct>();

            int count = GetInt("Count", 0);
            if (count == 0)
            {
                return updatePriceGroupProductList;
            }
            
            for (int i = 0; i < count; i++)
            {
                LpePriceGroupProduct item = new LpePriceGroupProduct();
                item.IsValid = GetBool("Include" + i);
                item.LoanProgramTemplateId = GetGuid("Id" + i);
                item.LenderProfitMargin = GetAndParseDecimal("Margin" + i);
                item.LenderMaxYspAdjustLckd = GetBool("YSPLock" + i, false);
                item.LenderMaxYspAdjust = GetAndParseDecimal("YSP" + i);
                item.LenderRateMargin = GetAndParseDecimal("RateMargin" + i);
                item.LenderMarginMargin = GetAndParseDecimal("MarginMargin" + i);
                updatePriceGroupProductList.Add(item);
            }

            return updatePriceGroupProductList;
        }

        private IEnumerable<Tuple<Guid, string>> GetUpdateDataTracNameList()
        {
            // 11/23/2016 - dd - Why do I use generic for holding DataTrac program name and id?
            //                   Because integration with datatrac is only available for 1 or 2 active lenders.
            //                   There is no plan to support this integration for future client since DataTrac is no longer in business.

            List<Tuple<Guid, string>> updateDataTracNameList = new List<Tuple<Guid, string>>();

            int count = GetInt("Count", 0);
            if (count == 0)
            {
                return updateDataTracNameList;
            }

            for (int i = 0; i < count; i++)
            {
                string name = GetString("DataTracName" + i, "");
                Guid lLpTemplateId = GetGuid("Id" + i);
                Tuple<Guid, string> item = new Tuple<Guid, string>(lLpTemplateId, name);
                updateDataTracNameList.Add(item);
            }

            return updateDataTracNameList;
        }

		private void PrepareBatchEdit()
		{
			// 05/19/08 mf - OPM 21028.  Set selected program list to cache.

			string selectedPrograms = GetString("SelectedPrograms");
			
			string key = AutoExpiredTextCache.AddToCache( selectedPrograms, TimeSpan.FromMinutes(5) );
			SetResult("Key", key);
		}

        private void AdjustValues()
        {
            // 04/07/10 -opm 48665 We now perform an adjustment calculation on this page.
            // It is very slow to do it row-by-row if there are many entries, and
            // we expect clients to be using this when there are broad market shifts, so 
            // adjusting many programs at once will be common. So we do it in batch.

            int count = GetInt("Count");
            decimal adjustment = GetAndParseDecimal("Adjustment");
            Dictionary<string, string> adjustmentMap = new Dictionary<string, string>();

            for (int i = 0; i < count; i++)
            {
                string originalStr = GetString("Value" + i).Replace("%", "");

                // Prepending with A for adjustment to make a legal xml identifier.
                if (!adjustmentMap.ContainsKey("a" + originalStr))
                {
                    decimal originalVal = ParseDecimal(originalStr);
                    string result = String.Format("{0:N3}%", originalVal + adjustment);
                    adjustmentMap.Add("a" + originalStr, result);
                }
            }
            
            foreach (KeyValuePair<string, string> adjustPair in adjustmentMap)
            {
                // Result is a mapping of original values to adjusted.
                SetResult(adjustPair.Key, adjustPair.Value);
            }
        }

        private decimal GetAndParseDecimal(string key)
        {
            return ParseDecimal( GetString(key, ""));
        }

        private decimal ParseDecimal(string valToParse) 
        {
            decimal ret = 0.0M;
            
            if (valToParse != "") 
            {
                try 
                {
                    ret = decimal.Parse(valToParse.Replace("%", ""));
                } 
                catch {}
            }
            return ret;
        }
	}
}
