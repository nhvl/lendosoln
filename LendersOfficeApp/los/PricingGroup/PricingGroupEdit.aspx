<%@ Page language="c#" Codebehind="PricingGroupEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.PricingGroup.PricingGroupEdit" %>
<%@ Import Namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<HTML>
	<HEAD runat="server">
		<title>PricingGroupEdit</title>
		<style type="text/css">
		
        .Modal
        {
            z-index: 900;
            display: none;
            border: black 3px solid;
            width: 325px;
            position: absolute;
            background-color: whitesmoke;
            text-align: center;
            top: 50px; 
            left: 137px; 
        }
		</style>
	</HEAD>
	<body bgcolor="gainsboro" margin="0" scroll="no" onresize="f_onresize();">
		<script type="text/javascript">
  var g_totalCount = <%= AspxTools.JsNumeric(m_currentIndex) %>;
  var g_currentSortColumn = -1;

  var AscImg = 'tri_asc.gif';
  var DescImg ='tri_desc.gif';
  var SpacerImg = 'spacer.gif';
  
  var dtrac = <%= AspxTools.JsNumeric(m_isDataTracEnabled ? 1 : 0) %> ;

  var g_dirtyList = new Array(g_totalCount);
  var selectCol = 0;  
  var includeCol = 1;
  var investorCol = 2;
  var prodCodeCol = 3;
  var lienCol = 4;
  var programCol = 5;
  <% if (m_isDataTracEnabled) { %>
  var DtracCol = 6;
  <% } %>
  var rateCol = 6 + dtrac;
  var pointCol = 7 + dtrac;
  var marginCol = 8 + dtrac;
  var yspCol = 9 + dtrac;
  <% if (m_useRateSheetExpirationFeature) { %>
  var blockCol = 10 + dtrac;
  var dlstartCol = 11 + dtrac;

  <% } else { %>
  var dlstartCol = 10 + dtrac;
  <% } %>
  var editMode = false;
  var nameDirty = false;
  var changeHighlightColor = "Bisque"
  var changes = new Array(g_totalCount);
  var undoRecord = new Array();
  
  function f_onresize() {
    var h = window.innerHeight;
    var spaceFromBottom = 500;
    if ( (h - spaceFromBottom) >= 0 )
      document.getElementById("MainTableDiv").style.height=(h - spaceFromBottom) + "px";
  }
 
    function f_onbefore_sort() {
    /* 8/5/05 dd - There is a bug in IE, when you move the element checkbox value doesnot retain. */
    var tbody = document.getElementById("MainTableBody");
    for(var i=0; i < tbody.rows.length; i++)
    {
      var cb = tbody.rows[i].children[selectCol].children[0];
      cb._checked = cb.checked;
    }
  }
  function f_onafter_sort() {
    var tbody = document.getElementById("MainTableBody");
    for(var i=0; i < tbody.rows.length; i++)
    {
      var cb = tbody.rows[i].children[selectCol].children[0];
      cb.checked = cb._checked;
    }
  }
  
  function rateview(id1,id2, event){
    var data = { 
        TemplateId : id2,
        PriceGroupId : id1
    };
    
    var results = gService.main.call("RateView", data);
    
    if (results.error) 
    {
        alert(results.UserMessage);
    }
    else if (results.value.Status == 'OK' ) 
    {
        document.getElementById('Rates').innerText = results.value.Rates; 
        Modal.ShowPopup('Modal', null, event);
    }
  }  

  var isReverseOrder = false;
    
  function f_sortTable(oCell) {
    var theader = document.getElementById("MainTableHeader");    
    if (g_currentSortColumn == oCell.cellIndex)
        isReverseOrder = !isReverseOrder;
    else {
      if (g_currentSortColumn > 0) {
        // Clear out asc/desc image from previous column.
        var prevhtml = theader.rows[0].cells[g_currentSortColumn].innerHTML;
        if (prevhtml.indexOf(AscImg) > 0)
          theader.rows[0].cells[g_currentSortColumn].innerHTML = prevhtml.replace(AscImg, SpacerImg);
        else if (prevhtml.indexOf(DescImg) > 0)
          theader.rows[0].cells[g_currentSortColumn].innerHTML = prevhtml.replace(DescImg, SpacerImg);
      }
      g_currentSortColumn = oCell.cellIndex;      
    }
      
    var html = oCell.innerHTML;

    if (html.indexOf(AscImg) > 0)
      oCell.innerHTML = html.replace(AscImg, DescImg);
    else if (html.indexOf(DescImg) > 0)
      oCell.innerHTML = html.replace(DescImg, AscImg);
    else
      oCell.innerHTML = html.replace(SpacerImg, AscImg);
      
    var tbody = document.getElementById("MainTableBody");
    var count = tbody.rows.length;
    
    f_onbefore_sort();
    
    var list = new Array(count);
    for (var i = 0; i < count; i++) {
      list[i] = tbody.rows[i];
    }

    
    if( g_currentSortColumn == 11 /*lRateSheetDownloadStartD*/ || g_currentSortColumn == 12 /*"lRateSheetDownloadEndD"*/ )
    {
        list.sort(f_sortDateAsc);
    }
    // Adjustment rate, adjustment point, adjustment ARM margin, or back-end max YSP
	else if(g_currentSortColumn === 6 || g_currentSortColumn === 7 || g_currentSortColumn === 8 || g_currentSortColumn === 9)
	{
	    list.sort(f_sortPercentageAsc);
	}
	else
	{
	    list.sort(f_sortAsc);
	}

    if (isReverseOrder)
    {
        list.reverse();
    }

    for (var i = 0; i < count; i++)
    {
      tbody.appendChild(list[i]);
      list[i].style.backgroundColor = ( i % 2 == 0 ) ? 'white' : '#CCCCCC';
    }
      
      f_onafter_sort()
  }
  function f_sortAsc(a, b) {
    var _a = a.cells[g_currentSortColumn].innerText;
    var _b = b.cells[g_currentSortColumn].innerText;
    if (_a < _b) return -1;
    else if (_a > _b) return 1;
    else return 0;
  }

  function f_sortPercentageAsc(a, b) {
      var _a = parseFloat(a.cells[g_currentSortColumn].innerText) / 100;
      var _b = parseFloat(b.cells[g_currentSortColumn].innerText) / 100;
      if (_a < _b) return -1;
      else if (_a > _b) return 1;
      else return 0;
  }
  
  //jMorse date sort fix
  //OPM 21323
  function f_sortDateAsc(a, b) {
    var _a;
    var _b;
    if( a.cells[g_currentSortColumn].innerText == "" )
      _a = 0;
    else 
      _a = Date.parse(a.cells[g_currentSortColumn].innerText);
	
    if( b.cells[g_currentSortColumn].innerText == "" )
      _b = 0;
    else
	  _b = Date.parse(b.cells[g_currentSortColumn].innerText);	
	
	if (_a < _b ) return -1;
    else if (_a > _b ) return 1;
    else return 0;
  }

  function f_setOnChanges(){
    var onChangeHandler = function(){return f_setNameDirty(true); };
    
    document.getElementById('<%= AspxTools.ClientId(m_displayPmlFeeIn100Format) %>'+'_0').onclick = 
    function(){ 
        document.getElementById('pmlFeePointsTxt').style.display = 'inline';
        document.getElementById('pmlFeePriceTxt').style.display = 'none';
        return f_setNameDirty(true);
    }
    document.getElementById('<%= AspxTools.ClientId(m_displayPmlFeeIn100Format) %>'+'_1').onclick = function(){ 
        document.getElementById('pmlFeePointsTxt').style.display = 'none';
        document.getElementById('pmlFeePriceTxt').style.display = 'inline';
        return f_setNameDirty(true);
    };
    document.getElementById('<%= AspxTools.ClientId(m_roundUpLpeFee) %>'+'_1').onclick = onChangeHandler;
    document.getElementById('<%= AspxTools.ClientId(m_roundUpLpeFee) %>'+'_0').onclick = onChangeHandler;
    document.getElementById('<%= AspxTools.ClientId(m_alwaysDisplayExactParRateOption) %>'+'_1').onclick = onChangeHandler;
    document.getElementById('<%= AspxTools.ClientId(m_alwaysDisplayExactParRateOption) %>'+'_0').onclick = onChangeHandler;
  }
  
  function f_setPmlFeeDisplayTxt()
  {
        if(document.getElementById('<%= AspxTools.ClientId(m_displayPmlFeeIn100Format) %>'+'_1').checked)
        {  
            document.getElementById('pmlFeePointsTxt').style.display = 'none';
            document.getElementById('pmlFeePriceTxt').style.display = 'inline';
        }
        else{
            document.getElementById('pmlFeePointsTxt').style.display = 'inline';
            document.getElementById('pmlFeePriceTxt').style.display = 'none';
        }  
  }
  
  
  function _init() {
    f_onresize();
    f_clearProgramDirty();
    f_clearEditArea();
    f_setEditAreaStatus( false );
    f_setCountText( 0 );
    f_setSaveBtnStatus( false );
    f_setEditMode( false, true );
    f_filterCategory();
    f_setOnChanges();
    f_setPmlFeeDisplayTxt();
    
    var bCurrentStateOn = <%= AspxTools.JsBool(m_externalPriceGroupEnabled) %>;
    document.getElementById("btnActivate").value = bCurrentStateOn ? "Disable Price Group" : "Enable Price Group";
    
	if (<%=AspxTools.JsString(m_lpePriceGroupId)%> == <%= AspxTools.JsString(Guid.Empty.ToString()) %> )
    {
      document.getElementById("btnActivate").disabled = true;
    }
}
  function f_save(bClick) {
    var bValid = true;
    if (typeof(Page_ClientValidate) == 'function' || typeof(Page_ClientValidate) == 'object')
      bValid = Page_ClientValidate();
  
    if (!bValid) {
      alert('Price group name is duplicate or missing. Please enter price group name and try again.');
      return;
    }
      
    var args = new Object();
    args["LpePriceGroupId"] = <%= AspxTools.JsString(m_lpePriceGroupId.ToString()) %>;

    <% if (m_showLpeEditLinks) { %>
    args["ActualPriceGroupId"] = <%= AspxTools.JsGetElementById(ActualPriceGroupId) %>.value;    
    <% } %>
    args["LpePriceGroupName"] = <%= AspxTools.JsGetElementById(LpePriceGroupName) %>.value;
    args["LenderPaidOriginatorCompensationOptionT"] = <%= AspxTools.JsGetElementById(m_LenderPaidOriginatorCompensationOptionT) %>.value;
    
    args["LockPolicyID"] = <%= AspxTools.JsGetElementById(m_LockPolicyID) %>.value;
    args["DisplayPmlFeeIn100Format"] = document.getElementById('<%= AspxTools.ClientId(m_displayPmlFeeIn100Format) %>'+'_1').checked;
    args["IsRoundUpLpeFee"] = document.getElementById('<%= AspxTools.ClientId(m_roundUpLpeFee) %>'+'_1').checked;
    args["RoundUpLpeFeeInterval"] = <%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>.value;
    args["IsAlwaysDisplayExactParRateOption"] = document.getElementById('<%= AspxTools.ClientId(m_alwaysDisplayExactParRateOption) %>' + '_1').checked;

    
    var editCount = 0;

    for (var i = 0; i < g_totalCount; i++) {
      if ( g_dirtyList[i] == 1 ) {
        args["Id" + editCount] = g_aIds[i];
        
        var row = document.getElementById('Row' + i);
        
        args["Include" + editCount] = row.children[includeCol].innerText.trim() == "Show" ? "True" : "False";

        args["RateMargin" + editCount] = row.children[rateCol].innerText;
        args["Margin" + editCount] = row.children[pointCol].innerText;
        args["MarginMargin" + editCount] =row.children[marginCol].innerText;
        args["YSP" + editCount] = row.children[yspCol].innerText;
        args["YSPLock" + editCount] = row.children[yspCol].style.fontWeight == 'bold';

        editCount++;

      }
    }
    args["Count"] = '' + editCount;
    
    var result = gService.main.call("Save", args);
    if (!result.error) {
      if (<%= AspxTools.JsGetElementById(LpePriceGroupName) %>.oldValue != null) {
        if (<%= AspxTools.JsGetElementById(LpePriceGroupName) %>.oldValue.toLowerCase() != <%= AspxTools.JsGetElementById(LpePriceGroupName) %>.value.toLowerCase()) {
          parent.list.f_updateName(result.value["LpePriceGroupId"], <%= AspxTools.JsGetElementById(LpePriceGroupName) %>.value);
        }    
      }
      clearDirty();
      f_clearProgramDirty();
      nameDirty = false;
      document.getElementById('UndoBtn').disabled = true;
      
      if (bClick != null) alert('Save successful.');
      if (result.value["RefreshList"] == "True")
        parent.list.goTo(result.value.LpePriceGroupId);
      f_setSaveBtnStatus( false );
    } else {
      alert(result.UserMessage || <%= AspxTools.JsString(JsMessages.PricingGroupEdit_UnableToSave) %>);
    }
  }
  function f_close() {
      PolyShouldShowConfirmSave(isDirty(), function(){ parent.onClosePopup(); }, f_save);
  }

  function f_validatePrintGroupName(source, arg) {
    
    var o = new Object();
    o["LpePriceGroupId"] = <%= AspxTools.JsString(m_lpePriceGroupId.ToString()) %>;
    o["LpePriceGroupName"] = <%= AspxTools.JsGetElementById(LpePriceGroupName) %>.value;
    
    var result = gService.main.call("ValidateLpePriceGroupName", o);
  
    if (!result.error) {
      arg.IsValid = result.value["IsValid"] == "True";
    } else {
      arg.IsValid = false;
    }
  }
  function f_toggleActivation() {
      PolyShouldShowConfirmSave(isDirty(), enableDisablePricingGroup, f_save);
  }

  function enableDisablePricingGroup(){
    var bCurrentStateOn = <%= AspxTools.JsBool(m_externalPriceGroupEnabled) %>;
    
    var confirmMsg = bCurrentStateOn ? <%= AspxTools.JsString(JsMessages.PricingGroupEdit_ConfirmDisable) %> : <%= AspxTools.JsString(JsMessages.PricingGroupEdit_ConfirmEnable) %>;
    
    if (confirm(confirmMsg)) {
    var args = new Object();
    args["LpePriceGroupId"] = <%= AspxTools.JsString(m_lpePriceGroupId.ToString()) %>;
    args["LpePriceGroupName"] = <%= AspxTools.JsGetElementById(LpePriceGroupName) %>.value;
    args["ExternalPriceGroupEnabled"] = bCurrentStateOn ? "False" : "True";
    args["LenderPaidOriginatorCompensationOptionT"] = <%= AspxTools.JsGetElementById(m_LenderPaidOriginatorCompensationOptionT) %>.value;
    
    args["LockPolicyID"] = <%= AspxTools.JsGetElementById(m_LockPolicyID) %>.value;
    args["DisplayPmlFeeIn100Format"] = document.getElementById('<%= AspxTools.ClientId(m_displayPmlFeeIn100Format) %>'+'_1').checked;
    args["IsRoundUpLpeFee"] = document.getElementById('<%= AspxTools.ClientId(m_roundUpLpeFee) %>'+'_1').checked;
    args["RoundUpLpeFeeInterval"] = <%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>.value;
    args["IsAlwaysDisplayExactParRateOption"] = document.getElementById('<%= AspxTools.ClientId(m_alwaysDisplayExactParRateOption) %>'+'_1').checked;
    
    var result = gService.main.call("EnableDisablePricingGroup", args);
    
    if (!result.error) {
      if (result.value["ErrorMessageForUserDisplay"] != null && result.value["ErrorMessageForUserDisplay"] != "") {
        alert(result.value["ErrorMessageForUserDisplay"]);
      }
      parent.list.goTo(result.value.LpePriceGroupId);
    } else {
      alert(result.UserMessage || <%= AspxTools.JsString(JsMessages.FailedToCompleteOperation) %>);
    }
    
    }
  }

  function f_export() {
    window.open('PricingGroupEdit.aspx?id=' + <%= AspxTools.JsString(m_lpePriceGroupId.ToString()) %> + '&cmd=csv', '_parent');
  }
  
  /*
  // 1/25/07 mf. OPM 8463.  This page was beginning to take a long time to
  // load, so it was refactored to use fewer input controls.  Basically, we
  // now only have one edit area for all programs, instead of one for each.
  // 
  // Since we can expect this page to get quite large when there are a lot of
  // programs active, I tried to reduce html generated by the repeater.  This
  // is why we use the DOM to access table cell content instead of ids.
  //
  // 2/26/07 mf. We now move to batch editing focus.  Checkboxes for selection.
  // We should be able to handle many.
  //
  // 4/18/07 mf. We now are supporting two modes of operation.  Edit mode and
  // preview mode.  Changes are accumulated and there is one level of Undo.

  // 4/18/07 mf. Use the undo record array to reverse last set of changes and
  // disable undo button. Delegate to f_applyChange() to make change and 
  // determine highlight/font weight.
  */
  function f_undoLastAction()
  {
	var tbody = document.getElementById("MainTableBody");    
    for (var i=0; i < undoRecord.length; i++)
      f_applyChange(undoRecord[i][0], undoRecord[i][1], undoRecord[i][2], true);

    document.getElementById('UndoBtn').disabled = true;
  }

  /*
  // 4/18/07 mf. Set the value, determine highlight/bold. We highlight if the value
  // is different than original setting.  Note that if later changes bring a
  // value back to what it is in DB (as of last load/save), we remove the
  // change highlight.
  */
  function f_applyChange( id, typeCol, newValue, bAsUndo )
  {
    if ( typeCol == yspCol )
       f_applyYspChange( id, typeCol, newValue, bAsUndo )
    else
    {
      var cell = document.getElementById('Row'+id).children[typeCol];
      if (newValue != cell.innerText)
      {
        var originalValue = cell.innerText;
        cell.innerText = newValue;
        var highlightCell = true;

        if ( changes[id] != null )
        {
          if (changes[id][typeCol] != null)
          {
            if ( changes[id][typeCol] == newValue )
            highlightCell = false;
          }
          else
            changes[id][typeCol] = originalValue;
        }
        else
        {
          var change = new Array();
          change[typeCol] = originalValue;
          changes[id] = change;
        }
        cell.style.backgroundColor = (highlightCell) ? changeHighlightColor : '';

        if ( !bAsUndo )
          undoRecord.push( new Array( id, typeCol, originalValue ) );
      }
    }
  }
    
  /*
  // 4/18/07 mf. It was suggested in OPM 11097 that we use formatting rather
  // than verbose text to identify Max YSP overrides.  We will use bold to represent
  // override, plain to represent point adjustment.
  // Rather than pollute applyChange() with a lot of special case code for this, we
  // handle YSP changes in this specialized method.  Basically, since we need
  // to represent formatting textually, in undo and change record, we use 
  // the format <value>':'('p'|'o'). p is point adj and o is override.
  */
  
  function f_applyYspChange( id, typeCol, newValue, bAsUndo )
  {
    var cell = document.getElementById('Row'+id).children[typeCol];
    var encodedCurrVal = cell.innerText + ':' + ( (cell.style.fontWeight == 'bold') ? 'o' : 'p');
    if (newValue != encodedCurrVal)
    {
      var originalValue = encodedCurrVal;
      cell.innerText = newValue.substr(0,newValue.indexOf(':'));
      cell.style.fontWeight = ( newValue.substr(newValue.indexOf(':')+1) == 'o' ) ? 'bold' : '';
      var highlightCell = true;

      if ( changes[id] != null )
      {
        if (changes[id][typeCol] != null)
        {
          if ( changes[id][typeCol] == newValue )
            highlightCell = false;
        }
        else
          changes[id][typeCol] = originalValue;
      }
      else
      {
        var change = new Array();
        change[typeCol] = originalValue;
        changes[id] = change;
      }
      cell.style.backgroundColor = (highlightCell) ? changeHighlightColor : '';
      if ( !bAsUndo )
          undoRecord.push( new Array( id, typeCol, originalValue ) );
    }
  }

  function mergeTPGIdIntoList(TPGId, TPGIdList, TPGArgsList, col)
  {
	var TPGFound = false;
	for(var k=0; k < TPGIdList.length; k++)
	{
		if(TPGId == TPGIdList[k])
		{
			// ALREADY EXISTS, UPDATE COL TO TRUE IN ARGS
			if(col == "Inc")
				TPGArgsList[k]["IncCol"] = true;
			else if(col == "Dtrac")
				TPGArgsList[k]["DataTracCol"] = true;
			else if(col == "Rate")
				TPGArgsList[k]["RateCol"] = true;
			else if(col == "Point")
				TPGArgsList[k]["PointCol"] = true;
			else if(col == "Margin")
				TPGArgsList[k]["MarginCol"] = true;
			else if(col == "Ysp")
			{
				TPGArgsList[k]["YspCol"] = true;
				var YspImportSettingVar = document.getElementById('YspSettingImport');
				TPGArgsList[k]["YspImportSetting"] = YspImportSettingVar.checked;
			}
			
			TPGFound = true;
		}
	}
	
	if(TPGFound == false)
	{
		// DOESNT EXIST IN SET, CREATE NEW TPGID IN LIST AND NEW TPGARGS IN LIST
		TPGIdList[TPGIdList.length] = TPGId;
		var tempObj = new Object();
		tempObj["thisTPGId"] = TPGId;
		
		tempObj["IncCol"] = false;
		tempObj["DataTracCol"] = false;
		tempObj["RateCol"] = false;
		tempObj["PointCol"] = false;
		tempObj["MarginCol"] = false;
		tempObj["YspCol"] = false;
		
		if(col == "Inc")
		{
			tempObj["IncCol"] = true;
		}
		else if(col == "Dtrac")
		{
			tempObj["DataTracCol"] = true;
		}
		else if(col == "Rate")
		{
			tempObj["RateCol"] = true;
		}
		else if(col == "Point")
		{
			tempObj["PointCol"] = true;
		}
		else if(col == "Margin")
		{
			tempObj["MarginCol"] = true;
		}
		else if(col == "Ysp")
		{
			tempObj["YspCol"] = true;
			var YspImportSettingVar = document.getElementById('YspSettingImport');
			tempObj["YspImportSetting"] = YspImportSettingVar.checked;

		}
		TPGArgsList[TPGArgsList.length] = tempObj;
	}
  }

  /*
  // 04/18/07 mf. For each program selected, we want to update the UI to
  // reflect the values for non-"Keep current" options.  Note the two special cases:
  // 1. Point is being changed and we are using point for max YSP -> Change YSP
  // 2. Max YSP goes from Override to Use point -> Take point value (after this change)
  */
  function f_onApplyChanges(event)
  {
    f_formatPercents();
    
    /*
    //	1) Make list of checkedProductIds
    //		a) get num of checkedProducts
    //	2) Traverse DropDownLists
    //		a) If new price group, add Id to TPGIdList
	//		b) Change args to true for column
	//	3) Args must include checkedProductId list
    //	4) One bg procedure per TPGId (max 5)	!-Possible consolidation into 1 function-!
    //		a) set checkedProductIdCol value	!-Ids are long, might require abbreviation-!
    //	5) Use returned values
    */
    
    var tbody = document.getElementById("MainTableBody");
    
    var numOfCheckedProducts = 0;
    var checkedProductIds = new Array();
    var TPGIdList = new Array();
    var TPGArgsList = new Array();
    var ImportedValuesListByTPG = new Array();
        
    //////////////////////////////////////////////////////////////////////////////////////////
    // - Check each DropDownList and create array of target price groups with no duplicates //
    // - Also update each corresponding args list with which columns are being accessed.    //
    
	if( document.getElementById('IncImport').checked )
	{
		var ddl = <%= AspxTools.JsGetElementById(IncDDL) %>
		var IncTPGId = ddl.options[ddl.selectedIndex].value; // TPG = TargetPriceGroup
		mergeTPGIdIntoList(IncTPGId, TPGIdList, TPGArgsList, "Inc");
	}
	if( document.getElementById('RateImport').checked )
	{
		var ddl = <%= AspxTools.JsGetElementById(RateDDL) %>
		var RateTPGId = ddl.options[ddl.selectedIndex].value; // TPG = TargetPriceGroup
		mergeTPGIdIntoList(RateTPGId, TPGIdList, TPGArgsList, "Rate");
	}

	if( document.getElementById('PointImport').checked )
	{
		var ddl = <%= AspxTools.JsGetElementById(PointDDL) %>
		var PointTPGId = ddl.options[ddl.selectedIndex].value; // TPG = TargetPriceGroup
		mergeTPGIdIntoList(PointTPGId, TPGIdList, TPGArgsList, "Point");
	}

	if( document.getElementById('MarginImport').checked )
	{
		var ddl = <%= AspxTools.JsGetElementById(MarginDDL) %>
		var MarginTPGId = ddl.options[ddl.selectedIndex].value; // TPG = TargetPriceGroup
		mergeTPGIdIntoList(MarginTPGId, TPGIdList, TPGArgsList, "Margin");
	}
	
	if( document.getElementById('YspImport').checked )
	{
		var ddl = <%= AspxTools.JsGetElementById(YspDDL) %>
		var YspTPGId = ddl.options[ddl.selectedIndex].value; // TPG = TargetPriceGroup
		mergeTPGIdIntoList(YspTPGId, TPGIdList, TPGArgsList, "Ysp");
	}
	
	//                                                                                      //
    //////////////////////////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////////////////////////
    // - Create list of all checked products to pass to db look up function                 //
    // - Also insert each of these products into each args variable so they can be passed   //

    for(var j=0; j < tbody.rows.length; j++)
    {
		if( tbody.rows[j].children[selectCol].children[0].checked )
		{
			var id = tbody.rows[j].id.substr(3);
			checkedProductIds[checkedProductIds.length] = g_aIds[id];
		}
	}

	for(var l=0; l < TPGArgsList.length; l++)
	{
		TPGArgsList[l]["numOfCheckedPIds"] = checkedProductIds.length;
		for(var m=0; m < checkedProductIds.length; m++)
		{
			TPGArgsList[l]["checkedPId" + m] = checkedProductIds[m];
		}
	}
	//                                                                                      //
    //////////////////////////////////////////////////////////////////////////////////////////

    /* 
    // 04/08/2010 mf. OPM 48665. If user wants to make a point adjustment, we need a server calculation.
    // For the purposes of avoiding many round trips, we do this once here instead of row-by-row.
    */
    var pointResult;
    if ( document.getElementById('PointAdjust').checked )
    {
        pointResult  = f_buildPointAdjustList();
    }
	
    //////////////////////////////////////////////////////////////////////////////////////////
    // - Call database access function on each TPGId with respective args list              //
    // - Returned values represent the "looked up" value and will be passed to f_applyChange//
	for(var n = 0; n < TPGIdList.length; n++)
	{
		var currentArgs = TPGArgsList[n];
		var currentResults = gService.main.call("ImportValuesFromDB", currentArgs);
		ImportedValuesListByTPG[n] = currentResults;
	}

	//                                                                                      //
    //////////////////////////////////////////////////////////////////////////////////////////
	var checkedIndex = -1;
    for(var i=0; i < tbody.rows.length; i++)
    {
      if( tbody.rows[i].children[selectCol].children[0].checked )
      {
        var row = tbody.rows[i];
        var id = tbody.rows[i].id.substr(3);
        
        var valueNotFound = true;

		checkedIndex++;

		var keyPrefix = "prod" + checkedIndex;
        
        if ( document.getElementById('IncShow').checked ) 
          f_applyChange(id, includeCol, 'Show', false);
        if ( document.getElementById('IncHide').checked ) 
          f_applyChange(id, includeCol, 'Hide', false);
		if( document.getElementById('IncImport').checked )
		{
			var importedValue;
			var valueNotFound = true;
			var key = keyPrefix + "IncCol";
			for(var p = 0; p < ImportedValuesListByTPG.length && valueNotFound; p++)
			{
				var TPGImportedValues = ImportedValuesListByTPG[p];
				if(TPGImportedValues.value[key] != null)
				{
					importedValue = TPGImportedValues.value[key];
					valueNotFound = false;
				}
			}
			f_applyChange(id, includeCol, importedValue, false);
		}
		


        if ( document.getElementById('RateUpdate').checked ) 
          f_applyChange(id, rateCol, document.getElementById('RateEdit').value == '0.000%'?'0%': document.getElementById('RateEdit').value, false);
		if ( document.getElementById('RateImport').checked )
		{
			var importedValue;
			var key = keyPrefix + "RateCol";
			var valueNotFound = true;
			for(var p = 0; p < ImportedValuesListByTPG.length && valueNotFound; p++)
			{
				var TPGImportedValues = ImportedValuesListByTPG[p];
				if(TPGImportedValues.value[key] != null)
				{
					importedValue = TPGImportedValues.value[key];
					valueNotFound = false;
				}
			}
			f_applyChange(id, rateCol, importedValue, false);
		}
		
        if ( document.getElementById('PointUpdate').checked )
          f_applyChange(id, pointCol, document.getElementById('PointEdit').value == '0.000%'?'0%': document.getElementById('PointEdit').value, false);

        if ( document.getElementById('PointAdjust').checked && pointResult && !pointResult.error)
        {
          var originalValue = f_getValue(id, pointCol);
          var newValue = pointResult.value['a' + originalValue.replace('%','')];
          
          f_applyChange(id, pointCol, newValue, false);
        }
          
   		if ( document.getElementById('PointImport').checked )
		{
			var importedValue;
			var key = keyPrefix + "PointCol";
			var valueNotFound = true;
			for(var p = 0; p < ImportedValuesListByTPG.length && valueNotFound; p++)
			{
				var TPGImportedValues = ImportedValuesListByTPG[p];
				if(TPGImportedValues.value[key] != null)
				{
					importedValue = TPGImportedValues.value[key];
					valueNotFound = false;
				}
			}
			
			f_applyChange(id, pointCol, importedValue, false);
		}

        if ( document.getElementById('MarginUpdate').checked )
          f_applyChange(id, marginCol, document.getElementById('MarginEdit').value == '0.000%'?'0%': document.getElementById('MarginEdit').value, false);
   		if ( document.getElementById('MarginImport').checked )
		{
			var importedValue;
			var key = keyPrefix + "MarginCol";
			var valueNotFound = true;
			for(var p = 0; p < ImportedValuesListByTPG.length && valueNotFound; p++)
			{
				var TPGImportedValues = ImportedValuesListByTPG[p];
				if(TPGImportedValues.value[key] != null)
				{
					importedValue = TPGImportedValues.value[key];
					valueNotFound = false;
				}
			}
			
			f_applyChange(id, marginCol, importedValue, false);
		}
        
        if ( document.getElementById('YspUpdate').checked )
        {
          f_applyChange(id, yspCol, ( document.getElementById('YspEdit').value == '0.000%'?'0%' : document.getElementById('YspEdit').value) + ':o', false);
        }
        else if ( document.getElementById('YspUsePoint').checked )
        {
          f_applyChange(id, yspCol, row.children[pointCol].innerText + ':p', false);
        }
        else if ( document.getElementById('YspImport').checked )
		{
			var importedValue;			
			var key = keyPrefix + "YspCol";
			var valueNotFound = true;
			for(var p = 0; p < ImportedValuesListByTPG.length && valueNotFound; p++)
			{
				var TPGImportedValues = ImportedValuesListByTPG[p];
				if(TPGImportedValues.value[key] != null)
				{
					importedValue = TPGImportedValues.value[key];
					valueNotFound = false;
				}
			}
				
			if(importedValue == "POINT")
			{
				importedValue = row.children[pointCol].innerText + ":p";
			}
			
			f_applyChange(id, yspCol, importedValue, false);
		}
        else if (!document.getElementById('PointKeep').checked )
        {          
          if ( row.children[yspCol].style.fontWeight != 'bold' )
            f_applyChange(id, yspCol, row.children[pointCol].innerText + ':p', false);
        }
        
        updateDirtyBit();
        g_dirtyList[id] = 1;
      }
    }

    f_setEditMode(false, false, event);
  }

  /*
  // 4/18/07 mf. Count all selected checkboxes. This should be used
  // sparingly, as we have to iterate through what could be a very large
  // number of rows.
  */
  function f_selectionCount()
  {
    var tbody = document.getElementById("MainTableBody");
    var selectionCount = 0;
    for(var i=0; i < tbody.rows.length; i++)
    {
      if( tbody.rows[i].children[selectCol].children[0].checked )
        selectionCount++;
    }
    return selectionCount;
  }
  
    /*
    // 04/08/2010 mf. OPM 48665. 
    // 1. Get all the selected point values, filtering out the duplicates
    // 2. Get the adjustment requested.
    // 3. Use background calc to have server do the adjusting
    */
    function f_buildPointAdjustList()
    {
    var pointValues = new Object();
    var count = 0;
    var tbody = document.getElementById("MainTableBody");
    for(var i=0; i < tbody.rows.length; i++)
    {
      if( tbody.rows[i].children[selectCol].children[0].checked )
      {
    	var id = tbody.rows[i].id.substr(3);
    	
    	var val = f_getValue( id, pointCol );
    	var isUnique = true;
    	for(var j=0; j < count; j++)
    	{
    	  if ( pointValues["Value"+ j ] == val )
    	  {
    	    isUnique = false;
    	    break;
    	  }
    	}
    	
    	if (isUnique)
            pointValues["Value"+ count++ ] = val;
      }
    }

        var result = new Object();
            pointValues["Count"] = count;
            pointValues["Adjustment"] = document.getElementById('PointAdjEdit').value;
            result = gService.main.call("AdjustValues", pointValues);

            if (result.error) 
            {
                alert("Could not apply adjustment.");
            }
        return result;
    }

  /* 
  // 04/18/07 mf. A single item selected or deselected.  Note that we
  // preload the rates only if they are selecting one program.  We
  // always the radios as "Keep current" for safest UI.  We 
  */
  function f_onSelectItem( index, bSelection )
  {
    var count = f_selectionCount();
    if ( count == 0 )
      f_clearEditArea();
    else if ( count == 1 )
      if ( bSelection )
        f_loadProgram( index );
    
    f_setEditAreaStatus( !count == 0 );
    
    if ( ! ( count == 0 ) )
      f_setChangesUI();
    
    f_setCountText( count );
  }
  
  function f_clearEditArea()
  {

    document.getElementById('RateEdit').value = "0.000%";
    document.getElementById('PointEdit').value = "0.000%";
    document.getElementById('PointAdjEdit').value = "0.000%"
    document.getElementById('MarginEdit').value = "0.000%";
    document.getElementById('YspEdit').value = "0.000%";
  }
  
  /*
  // 04/18/07 mf. Set the status of the rate edit text boxes and the preview
  // button based on the status of the Current/Keep radios.  Note that we
  // must also enable preview button when only the price group name has changed.
  */
  function f_setChangesUI()
  {

    document.getElementById('RateEdit').readOnly = !document.getElementById('RateUpdate').checked;
    document.getElementById('PointEdit').readOnly = !document.getElementById('PointUpdate').checked;
    document.getElementById('PointAdjEdit').readOnly = !document.getElementById('PointAdjust').checked;
    document.getElementById('MarginEdit').readOnly = !document.getElementById('MarginUpdate').checked;
    document.getElementById('YspEdit').readOnly = !document.getElementById('YspUpdate').checked;
    document.getElementById('IncDDL').disabled = !document.getElementById('IncImport').checked;
    document.getElementById('RateDDL').disabled = !document.getElementById('RateImport').checked;
    document.getElementById('PointDDL').disabled = !document.getElementById('PointImport').checked;
    document.getElementById('MarginDDL').disabled = !document.getElementById('MarginImport').checked;
    document.getElementById('YspDDL').disabled = !document.getElementById('YspImport').checked;
    document.getElementById('YspSettingImport').disabled = !document.getElementById('YspImport').checked;
	document.getElementById('YspExplicitImport').disabled = !document.getElementById('YspImport').checked;
	
    document.getElementById('ApplyBtn').disabled = !(document.getElementById('IncShow').checked || document.getElementById('RateUpdate').checked
    || document.getElementById('PointUpdate').checked || document.getElementById('PointAdjust').checked || document.getElementById('MarginUpdate').checked || document.getElementById('IncHide').checked
    || document.getElementById('YspUpdate').checked  || document.getElementById('YspUsePoint').checked || document.getElementById('IncImport').checked
    || document.getElementById('RateImport').checked || document.getElementById('PointImport').checked || document.getElementById('MarginImport').checked
    || document.getElementById('YspImport').checked) && !nameDirty;

  }
  
  /*
  // 04/18/07 mf. Enable or disable the entire edit area.  When we go from
  // enabled to disabled, we set all to Keep Current for safest UI.
  */
  function f_setEditAreaStatus( bSet )
  {
    if (!( bSet || document.getElementById('IncShow').disabled ) )
    {
      
      document.getElementById('IncKeep').checked = true; 
      document.getElementById('RateKeep').checked = true;
      document.getElementById('PointKeep').checked = true;
      document.getElementById('MarginKeep').checked = true;
      document.getElementById('YspKeep').checked = true;

    }
  
    document.getElementById('IncShow').disabled = ! bSet; 
    document.getElementById('IncHide').disabled = ! bSet;
    document.getElementById('IncImport').disabled = ! bSet;
    document.getElementById('RateImport').disabled = ! bSet;
    document.getElementById('PointImport').disabled = ! bSet;
    document.getElementById('MarginImport').disabled = ! bSet;
    document.getElementById('YspImport').disabled = ! bSet;
    document.getElementById('RateUpdate').disabled = ! bSet;
    document.getElementById('PointUpdate').disabled = ! bSet;
    document.getElementById('PointAdjust').disabled = ! bSet;
    document.getElementById('MarginUpdate').disabled = ! bSet;
    document.getElementById('YspUpdate').disabled = ! bSet;
    document.getElementById('IncKeep').disabled = ! bSet; 
    document.getElementById('RateKeep').disabled = ! bSet;
    document.getElementById('PointKeep').disabled = ! bSet;
    document.getElementById('MarginKeep').disabled = ! bSet;
    document.getElementById('YspKeep').disabled = ! bSet;
    document.getElementById('YspUsePoint').disabled = ! bSet;
    document.getElementById('ApplyBtn').disabled = ! bSet && !nameDirty;
    document.getElementById('RateEdit').readOnly = ! bSet;
    document.getElementById('PointEdit').readOnly = ! bSet;
    document.getElementById('PointAdjEdit').readOnly = ! bSet;
    document.getElementById('MarginEdit').readOnly = ! bSet;
    document.getElementById('YspEdit').readOnly = ! bSet;
    document.getElementById('IncDDL').disabled = ! bSet;
    document.getElementById('RateDDL').disabled = ! bSet;
    document.getElementById('MarginDDL').disabled = ! bSet;
    document.getElementById('PointDDL').disabled = ! bSet;
    document.getElementById('YspDDL').disabled = ! bSet;
    document.getElementById('YspSettingImport').disabled = ! bSet;
    document.getElementById('YspExplicitImport').disabled = ! bSet;

    
    <% if (m_showLpeEditLinks) { %>
    document.getElementById('EditRuleBtn').disabled = ! bSet;
    <% } %>
  }

  // 04/18/07 mf. Load rates from grid to edit UI.
  function f_loadProgram( index )
  {
    var row = document.getElementById('Row' + index);
    
    if ( row )
    {
      document.getElementById('RateEdit').value = row.children[rateCol].innerText;
      document.getElementById('PointEdit').value = row.children[pointCol].innerText;
      document.getElementById('MarginEdit').value = row.children[marginCol].innerText;
     
      document.getElementById('YspEdit').value = row.children[yspCol].innerText;
      
      f_formatPercents();
    }
  }
  function f_getValue(id, column)
  {
    var row = document.getElementById('Row' + id);
    return row.children[column].innerText;
  }

  /*
  // 04/18/07 We want to display 0% instead of 0.000%.  Takes up less speace
  // and is easier to read in grid.
  */
  function f_setSmartPercent( left, right )
  {
    
    left.innerText = right.value == '0.000%' ? '0%' : right.value;
  }

  function f_formatPercents()
  {
    format_percent(document.getElementById('RateEdit'));
    format_percent(document.getElementById('PointEdit'));
    format_percent(document.getElementById('MarginEdit'));
    format_percent(document.getElementById('YspEdit'));
  }
  
  function f_clearProgramDirty()
  {
    for (var i=0; i < g_dirtyList.length; i++)
      g_dirtyList[i] = 0;
  }
  
  function f_selectAll( select )
  {
    var tbody = document.getElementById("MainTableBody");
    for(var i=0; i < tbody.rows.length; i++)
      tbody.rows[i].children[selectCol].children[0].checked = select;

    if (!select) 
      f_clearEditArea();
    
    f_setEditAreaStatus( select );
    
    if ( select )
      f_setChangesUI();
      
    f_setCountText( select ? g_totalCount : 0 );
  }
  
  function f_setCountText( count )
  {
    countSpan = document.getElementById('batchCountSpan');
    if ( count > 0 )
    {
      countSpan.style.color = "red";
      countSpan.innerText = "(" + count + " program" + ( count == 1 ? "" : "s" ) + " selected)";
    }
    else
    {
      countSpan.style.color = "black";
      countSpan.innerText = "Please select at least one loan program.";
    }
  }
  function f_setSaveBtnStatus( bEnabled )
  {
    document.getElementById('SaveBtn').disabled = !bEnabled;
  }
  
  /*
  // 04/18/07 mf. Set UI if we are in edit or preview mode. Note that we
  // leave selection checkboxes alone. It might be a common situation that
  // after a preview, user would go back to perform another edit on the
  // same programs.
  */
  function f_setEditMode( bEdit, bInit, event )
  {
    editMode = bEdit;

    document.getElementById('GroupEditDiv').style.display = bEdit ? 'block' : 'none';
    document.getElementById('ApplyBtn').style.display = bEdit ? 'inline' : 'none';
    document.getElementById('CancelEditModeBtn').style.display = bEdit ? 'inline' : 'none';
    
    document.getElementById('EditModeBtn').style.display = bEdit ? 'none' : 'inline';
    document.getElementById('UndoBtn').style.display = bEdit ? 'none' : 'inline';
    document.getElementById('SaveBtn').style.display = bEdit ? 'none' : 'inline';
    document.getElementById('CloseBtn').style.display = bEdit ? 'none' : 'inline';
    
    f_toggleSelectionEnable( bEdit );
    <%= AspxTools.JsGetElementById(LpePriceGroupName) %>.disabled = !bEdit;
    <%= AspxTools.JsGetElementById(m_LenderPaidOriginatorCompensationOptionT) %>.disabled = !bEdit;
    <% if (m_showLpeEditLinks) { %>
      <%= AspxTools.JsGetElementById(ActualPriceGroupId) %>.disabled = !bEdit;
 <% } %>
    <%= AspxTools.JsGetElementById(m_LockPolicyID) %>.disabled = !bEdit;
    document.getElementById('<%= AspxTools.ClientId(m_displayPmlFeeIn100Format) %>'+'_0').disabled = !bEdit;
    document.getElementById('<%= AspxTools.ClientId(m_displayPmlFeeIn100Format) %>'+'_1').disabled = !bEdit;
    document.getElementById('<%= AspxTools.ClientId(m_roundUpLpeFee) %>'+'_0').disabled = !bEdit;
    document.getElementById('<%= AspxTools.ClientId(m_roundUpLpeFee) %>'+'_1').disabled = !bEdit;
    document.getElementById('<%= AspxTools.ClientId(m_alwaysDisplayExactParRateOption) %>'+'_0').disabled = !bEdit;
    document.getElementById('<%= AspxTools.ClientId(m_alwaysDisplayExactParRateOption) %>'+'_1').disabled = !bEdit;

    <%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>.readOnly = !bEdit || document.getElementById('<%= AspxTools.ClientId(m_roundUpLpeFee) %>'+'_0').checked;   
    
    if ( bEdit )
    {
      undoRecord = new Array();
      nameDirty = false;
    }
    else
    {
      document.getElementById('UndoBtn').disabled = undoRecord.length == 0;

      if ( ! bInit )
      {
          document.getElementById('ChooseCloseBtn').click();
          if(event) {
            var target = retrieveEventTarget(event);
              if (target && target.id == 'ApplyBtn')
                f_setSaveBtnStatus(true);  
          }
      }
    }
  }
  /*
  // 04/18/07 mf. There is no "fast" way to do this.  We have to hide or show 
  // all cells in the two date columns.
  */
  function f_toggleShowDates( bSet )
  {
    var headerBody = document.getElementById("MainTableHeader");
    <% if (m_useRateSheetExpirationFeature) { %>
    headerBody.rows[0].children[blockCol].style.display = bSet ? 'table-cell' : 'none';
    <% } %>
      headerBody.rows[0].children[dlstartCol].style.display = bSet ? 'table-cell' : 'none';


    var tbody = document.getElementById("MainTableBody");
    for(var i=0; i < tbody.rows.length; i++)
    {
      var row = tbody.rows[i];
      <% if (m_useRateSheetExpirationFeature) { %>
        row.children[blockCol].style.display = bSet ? 'table-cell' : 'none';      
      <% } %>
        row.children[dlstartCol].style.display = bSet ? 'table-cell' : 'none';

    }
  }
  
  
  /* // 04/18/07 Set if we can select programs or not. */
  function f_toggleSelectionEnable( bSet )
  {
    var headerBody = document.getElementById("MainTableHeader");
    headerBody.rows[0].children[selectCol].children[0].disabled = !bSet; // this is the batch select button

    var tbody = document.getElementById("MainTableBody");
    for(var i=0; i < tbody.rows.length; i++)
    {
      tbody.rows[i].children[selectCol].children[0].disabled = ! bSet; // this is each individual checkboxes
    }
    
    document.getElementById('ChooseBtn').disabled = ! bSet;    
    
  }

  /*
  // 04/18/07 mf. Use the client arrays to set the selection filter table.
  // 03/13/08 mf. Per OPM 20930 we will let users search by partial string in program name.
  */
  function f_filterCategory()
  {
    var ddl = <%= AspxTools.JsGetElementById(m_categoryDdl) %>;
    var column = ddl[ddl.selectedIndex].value;
    
    var batchSelectionDiv = document.getElementById('batchSelectionDiv');
    
    
    var srcArray = null;

    switch ( column )
    {
      case investorCol+'': srcArray = m_investorNames; break;
      case prodCodeCol+'': srcArray = m_productCodes; break;
      case includeCol+'': srcArray = ["Show", "Hide"]; break;
      case programCol+'': break;
      <% if (m_isDataTracEnabled) { %>
      case DtracCol+'': break;
      <% } %>
      default: return;
    }
    
    var sInnerHtml = null;
    var h = hypescriptDom;
    if (srcArray != null)
    {
      if (srcArray.length > 0 )
      {
        sInnerHtml = h("table", {cellspacing:2, cellpadding:1, border:0, width:"100%"}, srcArray.map(function(x) {
          return h("tr", {}, [
            h("td", {width:"60%"}, x),
            h("td", {}, h("a", {href:"#", onclick:function(){ f_selectGroup(x, column, true, true, true) }}, "Select All")),
            h("td", {}, h("a", {href:"#", onclick:function(){ f_selectGroup(x, column, false, true, true) }}, "Deselect All"))
          ]);
        }));
      }
    }
    else
    {
      sInnerHtml = h("table", {cellspacing:2, cellpadding:1, border:0, width:"100%"}, [
        h("tr", {}, [
          h("td", {}, "Contains:"),
          h("td", {}, h("input", {type:"text", id:"programMatch", onfocus:highlightField, onblur:unhighlightField})),
          h("td", {}, h("a", {href:"#", onclick:function(){ f_selectGroup(document.getElementById("programMatch").value, column,  true, true, false) }}, "Select All")),
          h("td", {}, h("a", {href:"#", onclick:function(){ f_selectGroup(document.getElementById("programMatch").value, column, false, true, false) }}, "Deselect All"))
        ]),
        h("tr", {}, [
          h("td", {}, "Does Not Contain:"),
          h("td", {}, h("input", {type:"text", id:"programNoMatch", onfocus:highlightField, onblur:unhighlightField})),
          h("td", {}, h("a", {href:"#", onclick:function(){ f_selectGroup(document.getElementById("programNoMatch").value, column,  true, false, false) }}, "Select All")),
          h("td", {}, h("a", {href:"#", onclick:function(){ f_selectGroup(document.getElementById("programNoMatch").value, column, false, false, false) }}, "Deselect All")),
        ])
      ]);
    }
    
    batchSelectionDiv.innerHTML = "";
    if (sInnerHtml) batchSelectionDiv.appendChild(sInnerHtml);
  }
  /*
  // 04/18/07 mf. Select/deselect a group of programs based on chosen settings.
  */
  function f_selectGroup( sFilter, column, bSelect, bShouldMatch, bExact )
  { 
    var tbody = document.getElementById("MainTableBody");
    for(var i=0; i < tbody.rows.length; i++)
    {
      if (bExact == true){ // NOT program name filter, must be exact!

         if (((bShouldMatch == true) &&  (tbody.rows[i].children[column].innerText.toLowerCase() == sFilter.toLowerCase()) )
          || ((bShouldMatch == false) && (tbody.rows[i].children[column].innerText.toLowerCase() != sFilter.toLowerCase()) ))
         {
           tbody.rows[i].children[selectCol].children[0].checked = bSelect;
         }
      } else {
         if (((bShouldMatch == true) &&  (tbody.rows[i].children[column].innerText.toLowerCase().indexOf( sFilter.toLowerCase() ) > -1) ) 
          || ((bShouldMatch == false) && (tbody.rows[i].children[column].innerText.toLowerCase().indexOf( sFilter.toLowerCase() ) == -1 ) ))
         {
           tbody.rows[i].children[selectCol].children[0].checked = bSelect;
         }
      }
    }
    
    var count = f_selectionCount();
    f_setCountText( count );
    f_setEditAreaStatus( count != 0 );
    f_setChangesUI();
  }
  
  function f_showSelectFilter(event)
  {
    if (document.getElementById('ChooseBtn').disabled) return;
    var filterPanel = document.getElementById('selectFilter');
    filterPanel.style.left = event.clientX + 25 + 'px';
    filterPanel.style.top = event.clientY + 10 + 'px';
    filterPanel.style.display = "block";
  }
  function f_setNameDirty( bSet )
  {
    nameDirty = bSet;
    f_setSaveBtnStatus(bSet);
    
    document.getElementById('ApplyBtn').disabled = !bSet;
  }
  <% if (m_useRateSheetExpirationFeature) { %>
  function f_showBlockedStatus(msg,prod,inv, dontShowHeader, event)
  {
    var blockedPanel = document.getElementById('blockStatus');
    var blockMsg = document.getElementById('blockMsg');
    var blockMsgProd = document.getElementById('blockMsgProd');
    var blockHeader = document.getElementById('headerMsg'); 
    blockHeader.style.display = dontShowHeader ? "none" : ""; 
    blockMsg.innerText = msg;
    blockMsgProd.innerText = inv + ' ' + prod;
    blockedPanel.style.left = event.clientX - 255 + 'px';
    blockedPanel.style.top = event.clientY + 10 + 'px';
    blockedPanel.style.display = "block";
  }
  <% } %>
  
  <% if ( m_showLpeEditLinks ) { %>
  function f_onRuleEdit( progId, progName )
  {
  if (<%=AspxTools.JsString(m_lpePriceGroupId)%> == <%= AspxTools.JsString(Guid.Empty.ToString()) %>)
  {
    alert("Cannot associate with unsaved price group.");
    return;
  }

    var priceId = <%=AspxTools.JsString(m_lpePriceGroupId)%>;
    var priceName = <%= AspxTools.JsGetElementById(LpePriceGroupName) %>.value;

    showModal('/los/RatePrice/PricePolicyListByProduct.aspx?FileId=' + progId + "&ProductName=" + escape( progName ) + "&PriceGroupId=" + priceId + "&PriceGroupName=" + escape(priceName),
        null, null, true);

  }
  
  function f_onGroupAssoc()
  {
    var priceId = <%=AspxTools.JsString(m_lpePriceGroupId)%>;
    var priceName = <%= AspxTools.JsGetElementById(LpePriceGroupName) %>.value;

    showModal('/los/RatePrice/PricePolicyListByProduct.aspx?PriceGroupId=' + priceId + "&PriceGroupName=" + escape(priceName),
        null, null, true);
  }
  
  /* // 05/19/08 mf - OPM 21028. Because of the size limits of querystring 
       // and dialog arguments, we use background calc to first store program id
       // list to cache, then let the attachment editor pull the list from cache. */
  function f_onRuleEditBatch()
  {
  if (<%=AspxTools.JsString(m_lpePriceGroupId)%> == <%= AspxTools.JsString(Guid.Empty.ToString()) %>)
  {
    alert("Cannot associate with unsaved price group.");
    return;
  }
  
    var args = new Object();
    args["SelectedPrograms"] = GetSelectedProgramIds();

    var result = gService.main.call("PrepareBatchEdit", args);
    if (!result.error) 
    {
      var key = result.value["Key"];
      var priceId = <%=AspxTools.JsString(m_lpePriceGroupId)%>;
      var priceName = <%= AspxTools.JsGetElementById(LpePriceGroupName) %>.value;
      showModal('/los/RatePrice/PricePolicyListByProduct.aspx?ProgramList=' + key + "&PriceGroupId=" + priceId + "&PriceGroupName=" + escape(priceName),
          null, null, true);
    }
    else
    {
      alert('Failed to prepare batch edit.');
    }
  }
  
  /* // Provide a comma delinated list of the IDs of the currently selected programs */  
  function GetSelectedProgramIds()
  {
    var tbody = document.getElementById("MainTableBody");
    var ret = '';
    for(var i=0; i < tbody.rows.length; i++)
    {
      if( tbody.rows[i].children[selectCol].children[0].checked )
        {
          var row = tbody.rows[i];
          var id = tbody.rows[i].id.substr(3);
          ret += ( (ret != '') ? ',' : '' ) +  g_aIds[id];
        }
	}
	return ret;
  }
  
  
  <% } %>
  
    function onRoundLpeFeeClick()
	{
		if(document.getElementById('<%= AspxTools.ClientId(m_roundUpLpeFee) %>' + '_0').checked == true)
		{
			var rangeValidator = <%= AspxTools.JsGetElementById(m_rangeValRoundLpeFeeInterval) %>;
			if (rangeValidator && !rangeValidator.isvalid)
			{
				<%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>.value = "0.050";
				if(typeof(ValidatorValidate) == 'function')
					ValidatorValidate(rangeValidator);
			}
			<%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>.readOnly = true;
		}
		else
			<%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>.readOnly = false;
	}
	
	function FormatRoundInterval()
	{
		var interval = <%= AspxTools.JsGetElementById(m_roundUpLpeFeeInterval) %>;
		var intervalVal = interval.value;
		var rangeValidator = <%= AspxTools.JsGetElementById(m_rangeValRoundLpeFeeInterval) %>;
		if( (intervalVal.replace(/^\s*|\s*$/g,"") == ""))
		{
			interval.value = "0.000";
			
			if (rangeValidator && typeof(ValidatorValidate) == 'function')
				ValidatorValidate(rangeValidator);
		}
		else
		{
			var index = intervalVal.indexOf('.');
			if(index >= 0)
			{
				if(intervalVal.substring(0, index) > 0 || intervalVal.indexOf('-') >= 0)
					return;
				var intervalLength = intervalVal.length;
				interval.value = ("0" + intervalVal.substring(index, intervalLength)).substring(0, 5);
			}
		}
  }
  function goTo(id){
    window.location.href = window.location.pathname + "?id=" + encodeURIComponent(id);
  }
		</script>
		<form id="PricingGroupEdit" method="post" runat="server">
			<table id="Table1" cellSpacing="0" cellPadding="5" width="100%" border="0">
				<tr>
					<td noWrap class="MainRightHeader">Price Group Edit</td>
					<td noWrap align="right" class="MainRightHeader" style="PADDING-RIGHT:10px">
						<input type="button" value="Export to CSV" onclick="f_export();" class="ButtonStyle" nohighlight>&nbsp;
						<INPUT class="ButtonStyle" type="button" value="Disable Price Group" style="WIDTH: 161px" id="btnActivate" onclick="f_toggleActivation();"></td>
				</tr>
				<tr>
					<td noWrap style="PADDING-LEFT:5px" class="FieldLabel">Price Group Name&nbsp;&nbsp;</td>
					<td noWrap width="100%"><asp:TextBox id="LpePriceGroupName" runat="server" onchange="f_setNameDirty( true )" Width="70%"></asp:TextBox><asp:CustomValidator id="LpePriceGroupNameValidator" runat="server" ErrorMessage="  * Duplicate name" Display="Dynamic" ClientValidationFunction="f_validatePrintGroupName" ControlToValidate="LpePriceGroupName" Font-Bold="True"></asp:CustomValidator><asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage=" * Required" Display="Dynamic" ControlToValidate="LpePriceGroupName" Font-Bold="True"></asp:RequiredFieldValidator>
					<% if (m_showLpeEditLinks) { %>
					<a href="javascript:f_onGroupAssoc()">PriceGroup Associate</a>
					<% } %>
					</td>
				</tr>
                <% if (m_showLpeEditLinks){ %>
                <tr>
                    <td class="FieldLabel">ActualPriceGroupId</td>
                    <td><asp:TextBox ID="ActualPriceGroupId" runat="server" Width="250px" onchange="f_setNameDirty( true )"/>   LpePriceGroupId: <asp:TextBox ID="LpePriceGroupId" runat="server" Width="250px" ReadOnly="true" /></td>
                </tr>
                <% } %>
			</table>
			<table cellspacing="0" cellpadding="0" border="0" width="100%" style="PADDING-RIGHT:5px; PADDING-LEFT:5px">
			    <tr>
			        <td class="FieldLabel">
			            Lender-paid Originator Compensation
			            <asp:DropDownList runat="server" onchange="f_setNameDirty( true )" ID="m_LenderPaidOriginatorCompensationOptionT" ></asp:DropDownList>
			        </td>
			        <td nowrap class="FieldLabel" align="right"> 
			            PML Results Display: 
			        </td>
			        <td class="FieldLabel">
			            <asp:RadioButtonList id="m_displayPmlFeeIn100Format" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" onchange="f_setNameDirty(true)">
							<asp:ListItem Value="Points">Points</asp:ListItem>
							<asp:ListItem Value="Price">Price</asp:ListItem> 
						</asp:RadioButtonList>	
					    <span id="pmlFeePointsTxt" style="display:none"><font style="font-weight:normal" >&nbsp;(Par is displayed as 0.000)</font></span>
					    <span id="pmlFeePriceTxt" style="display:none"><font style="font-weight:normal" >&nbsp;(Par is displayed as 100.000)</font></span>
			        </td>
			    </tr>
			    <tr>
			        <td class="FieldLabel">
			            Which lock policy does this group use?
			            <asp:DropDownList runat="server" ID="m_LockPolicyID" onchange="f_setNameDirty(true)"></asp:DropDownList>
			        </td>
			        
			        <td  class="FieldLabel" nowrap align="right">
			            PML Fee (Point) Rounding: 
			        </td>
			        <td>
			            <asp:RadioButtonList ID="m_roundUpLpeFee" onclick="onRoundLpeFeeClick();" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" onchange="f_setNameDirty(true)">
							<asp:ListItem Value="N">No rounding</asp:ListItem>
							<asp:ListItem Value="Y">Round up fees to nearest:</asp:ListItem>
						</asp:RadioButtonList>
						<asp:textbox id="m_roundUpLpeFeeInterval" onblur="FormatRoundInterval();" runat="server" Width="50px" MaxLength=5 onchange="f_setNameDirty(true)"></asp:textbox>
						<asp:rangevalidator id="m_rangeValRoundLpeFeeInterval" runat="server" Type=Double MaximumValue="1" MinimumValue=".002" ControlToValidate="m_roundUpLpeFeeInterval" Display="Dynamic" ErrorMessage="Please enter a valid value between .002 and 1."></asp:rangevalidator>
					</td>
			    </tr>
                <tr>
                    <td></td>
                    <td  class="FieldLabel" nowrap align="right">
			            Force an exact par rate in PML?
			        </td>
                    <td>
                        <asp:RadioButtonList ID="m_alwaysDisplayExactParRateOption" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" onchange="f_setNameDirty(true)">
							<asp:ListItem Value="N">No</asp:ListItem>
							<asp:ListItem Value="Y">Yes</asp:ListItem>
						</asp:RadioButtonList>
                        <span>(Requires at least one rate option offering a rebate)</span>
                    </td>
                </tr>
			</table>
			<table cellspacing="0" cellpadding="0" border="0" width="100%" style="PADDING-RIGHT:5px; PADDING-LEFT:5px">
				<tr>
					<td><input type="button" id="ChooseBtn" value="Batch-Select By Categories" onclick="f_showSelectFilter(event);" class="ButtonStyle" nohighlight style="WIDTH:210px"></td>
					<td></td>
					<td align="right">
					  <input type="checkbox" id="showDownloadDates" onclick="f_toggleShowDates( this.checked );" NotForEdit="true"><label for="showDownloadDates">Show Pricing Data Details</label>
					  <% if (m_isDataTracEnabled) { %>
					  &nbsp;&nbsp;<input type="checkbox" id="showDataTracName"  onclick="f_toggleShowDtracCol( this.checked );" NotForEdit="true"><label for="showDataTracName">Show DataTrac Program Name</label>
					  <% } %>
					</td>
				</tr>
			</table>
			<div id="MainTableDiv" style="BORDER-RIGHT:2px inset; BORDER-TOP:2px inset; MARGIN:5px 5px 0px; OVERFLOW:auto; BORDER-LEFT:2px inset; BORDER-BOTTOM:2px inset; HEIGHT:400px">
				<table cellspacing="1" cellpadding="1" border="0" width="100%">
					<tbody id="MainTableHeader">
						<tr>
							<td class="GridHeader" valign="top" width="40"><input type="checkbox" onclick="f_selectAll( this.checked );" NotForEdit="true"></td>
							<TD class="GridHeader" vAlign="top" width="55"><a href="#" onclick='f_sortTable(this.parentNode);'>Visibility</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></TD>
							<TD class="GridHeader" vAlign="top"><a href="#" onclick='f_sortTable(this.parentNode);'>Investor 
									Name</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></TD>
							<TD class="GridHeader" vAlign="top" noWrap width="90"><a href="#" onclick='f_sortTable(this.parentNode);'>Prod 
									Code</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></TD>
							<TD class="GridHeader" vAlign="top" width="45"><a href="#" onclick='f_sortTable(this.parentNode);'>Lien</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></TD>
							<td class="GridHeader" vAlign="top" noWrap><a href="#" onclick='f_sortTable(this.parentNode);'>Loan 
									Program</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></td>
							<% if (m_isDataTracEnabled) { //opm 24638 fs 09/11/08 %>
							<TD class="GridHeader" vAlign="top" style="DISPLAY: none"><a href="#" onclick='f_sortTable(this.parentNode);'>DataTrac 
									Program Name</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></TD>
							<% } %>
							<TD class="GridHeader" vAlign="top" width="60"><a href="#" onclick='f_sortTable(this.parentNode);'>Adj 
									Rate By</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></TD>
							<TD class="GridHeader" vAlign="top" width="60"><a href="#" onclick='f_sortTable(this.parentNode);'>Adj 
									Point By</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></TD>
							<TD class="GridHeader" vAlign="top" width="60"><a href="#" onclick='f_sortTable(this.parentNode);'>Adj 
									ARM Margin By</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></TD>
							<TD class="GridHeader" vAlign="top" width="80"><a href="#" onclick='f_sortTable(this.parentNode);'>Reduce 
									 Back-End Max YSP By</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></TD>
							<% if (m_useRateSheetExpirationFeature) { %>
							<TD class="GridHeader" vAlign="top" style="DISPLAY: none"><a href="#" onclick='f_sortTable(this.parentNode);'>Expired 
									for Normal Users?</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></TD>
							<% } %>
							<td class="GridHeader" valign="top" style="DISPLAY: none"><a href="#" onclick='f_sortTable(this.parentNode);'>Rate 
									Sheet Download Time</a>&nbsp;&nbsp;<img src=<%= AspxTools.SafeUrl(VirtualRoot + "/images/spacer.gif") %> ></td>

						</tr>
					</tbody>
					<tbody id="MainTableBody" style="CURSOR: default">
						<asp:Repeater ID="PricingGroupRepeater" Runat="server" EnableViewState="false">
							<ItemTemplate>
								<tr id="Row<%# AspxTools.HtmlString(m_currentIndex) %>" style="BACKGROUND: #<%# AspxTools.HtmlString(m_currentIndex % 2 == 0 ? "FFFFFF" : "CCCCCC" ) %>">
									<td>
										<input type="checkbox" onclick="f_onSelectItem( <%# AspxTools.HtmlString(m_currentIndex)%>, this.checked )" NotForEdit="true"></td>
									<td><%# AspxTools.HtmlString( GenerateIncludeText(Container.DataItem) )%></td>
									<td><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "lLpInvestorNm"))%></td>
									<td><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "ProductCode"))%></td>
									<td><ml:PassthroughLabel runat="server" ID="lienPosition"></ml:PassthroughLabel></td>
									<td><ml:PassthroughLabel runat="server" ID="loanProgramName"></ml:PassthroughLabel></td>
									<% if (m_isDataTracEnabled) { %> <%-- opm 24638 fs 09/11/08--%>
									<td style="DISPLAY: none"><%# AspxTools.HtmlString(GenerateDataTracProgramName(Container.DataItem))%></td>
									<% } %>
									<td><%# AspxTools.HtmlString(GenerateRateMargin(Container.DataItem))%></td>
									<td><%# AspxTools.HtmlString(GenerateProfitMargin(Container.DataItem)) %></td>
									<td><%# AspxTools.HtmlString(GenerateMarginMargin(Container.DataItem))%></td>
									<td style="<%# AspxTools.HtmlString(GenerateYSPStyle(Container.DataItem))%>"><%# AspxTools.HtmlString(GenerateYSP(Container.DataItem))%></td>
									<% if (m_useRateSheetExpirationFeature) { %>
									<td style="DISPLAY: none"><ml:PassthroughLabel runat="server" ID="rateSheetBlockStatus"></ml:PassthroughLabel></td>
									<% } %>
									<td style="DISPLAY: none"><%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "lRateSheetDownloadStartD"))%></td>

								</tr>
							</ItemTemplate>
						</asp:Repeater>
					</tbody>
				</table>
			</div>
			<div style="HEIGHT: 270px">
				<div id="GroupEditDiv" style="BORDER-RIGHT:2px inset; BORDER-TOP:2px inset; MARGIN:5px; OVERFLOW:auto; BORDER-LEFT:2px inset; BORDER-BOTTOM:2px inset; HEIGHT:240px">
					<table cellpadding="0" cellspacing="2" border="0">
						<tr>
							<td colspan="4">&nbsp;<span id="batchCountSpan"></span></td>
						</tr>
						<tr>
							<td class="FieldLabel" style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">Visibility</td>
							<td style="PADDING-RIGHT: 8px"><input id="IncKeep" checked type="radio" name="IncRB" onclick="f_setChangesUI();"><label for="IncKeep">Keep 
									current</label></td>
							<td><input id="IncShow" type="radio" name="IncRB" onclick="f_setChangesUI();"><label for="IncShow">Show</label></td>
							<td colspan="2"><input type="radio" name="IncRB" id="IncHide" onclick="f_setChangesUI();"><label for="IncHide">Hide</label></td>
							<td><input id="IncImport" type="radio" name="IncRB" onclick="f_setChangesUI();"><label for="IncImport">Import Visibility from Price Group: </label></td>
							<td><asp:DropDownList id="IncDDL" runat="server"></asp:DropDownList></td>
						</tr>
						<tr>
							<td class="FieldLabel" style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; WHITE-SPACE: nowrap">Rate 
								Adjustment</td>
							<td><input id="RateKeep" type="radio" name="RateRB" checked onclick="f_setChangesUI();"><label for="RateKeep">Keep 
									current</label></td>
							<td><input id="RateUpdate" type="radio" name="RateRB" onclick="f_setChangesUI();"><label for="RateUpdate">Change 
									to:</label></td>
							<td colspan="2"><input type="text" id="RateEdit" preset="percent" style="WIDTH: 60px" NotForEdit="true"></td>
							<td><input id="RateImport" type="radio" name="RateRB" onclick="f_setChangesUI();"><label for="RateImport">Import Rate Adjustment from Price Group: </label></td>
							<td><asp:DropDownList id="RateDDL" runat="server"></asp:DropDownList></td>
						</tr>
						<tr>
							<td class="FieldLabel" style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; WHITE-SPACE: nowrap">Point 
								Adjustment</td>
							<td><input id="PointKeep" type="radio" name="PointRB" checked onclick="f_setChangesUI();"><label for="PointKeep">Keep 
									current</label></td>
							<td><input id="PointUpdate" type="radio" name="PointRB" onclick="f_setChangesUI();"><label for="PointUpdate">Change 
									to:</label></td>
							<td colspan="2"><input type="text" id="PointEdit" preset="percent" style="WIDTH: 60px" NotForEdit="true"></td>
							<td><input id="PointImport" type="radio" name="PointRB" onclick="f_setChangesUI();"><label for="PointImport">Import Point Adjustment from Price Group: </label></td>
							<td><asp:DropDownList id="PointDDL" runat="server"></asp:DropDownList></td>
						</tr>
						<tr>
						    <td></td>
						    <td></td>
							<td><input id="PointAdjust" type="radio" name="PointRB" onclick="f_setChangesUI();"><label for="PointAdjust">Adjust 
									by:</label></td>
							<td colspan="2"><input type="text" id="PointAdjEdit" preset="percent" style="WIDTH: 60px" NotForEdit="true"></td>
						    <td></td>
						</tr>
						<tr>
							<td class="FieldLabel" style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; WHITE-SPACE: nowrap">ARM Margin 
								Adjustment</td>
							<td><input id="MarginKeep" type="radio" name="MarginRB" checked onclick="f_setChangesUI();"><label for="MarginKeep">Keep 
									current</label></td>
							<td><input id="MarginUpdate" type="radio" name="MarginRB" onclick="f_setChangesUI();"><label for="MarginUpdate">Change 
									to:</label></td>
							<td colspan="2"><input type="text" id="MarginEdit" preset="percent" style="WIDTH: 60px" NotForEdit="true"></td>
							<td><input id="MarginImport" type="radio" name="MarginRB" onclick="f_setChangesUI();"><label for="MarginImport">Import ARM Margin Adjustment from Price Group: </label></td>
							<td><asp:DropDownList id="MarginDDL" runat="server"></asp:DropDownList></td>
						</tr>
						<tr>
							<td class="FieldLabel" style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; WHITE-SPACE: nowrap">Back-End
								Max YSP Reduction</td>
							<td><input id="YspKeep" type="radio" name="YspRB" checked onclick="f_setChangesUI();"><label for="YspKeep">Keep 
									current setting</label></td>
							<td><input id="YspUsePoint" type="radio" name="YspRB" onclick="f_setChangesUI();"><label for="YspUsePoint">Use 
									point adjustment</label></td>
							<td><input id="YspUpdate" type="radio" name="YspRB" onclick="f_setChangesUI();"><label for="YspUpdate">Override 
									with:</label></td>
							<td><input type="text" id="YspEdit" preset="percent" style="WIDTH: 60px" NotForEdit="true"></td>
							<td><input id="YspImport" type="radio" name="YspRB" onclick="f_setChangesUI();"><label for="YspImport">Import Back-End Max YSP Reduction from Price Group: </label></td>
							<td><asp:DropDownList id="YspDDL" runat="server"></asp:DropDownList></td>
						</tr>

						<tr><td colspan="5"></td>
							<td colspan="2">
								<table cellpadding="0" cellspacing="0" border="0" >
									<tr>
										<td colspan="2" style="PADDING-RIGHT: 2px; PADDING-LEFT: 25px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px;"><input id="YspSettingImport" type="radio" name="YspSettingExpImport" checked><label for="YspSettingImport">Use point adjustment and override where applicable </label></td>
									</tr>
									<tr><td colspan="2" style="PADDING-RIGHT: 2px; PADDING-LEFT: 25px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px;"><input id="YspExplicitImport" type="radio" name="YspSettingExpImport"><label for="YspExplicitImport">Override all rows with explicit values only</label></td>
									</tr>
								</table>
							</td>
						</tr>
						<% if (m_showLpeEditLinks) { %>
						<tr>
							<td colspan=5 style="padding-left: 5px;">
								<input id="EditRuleBtn" type="button" class="ButtonStyle" value="Set Rule Attachments..." onclick="f_onRuleEditBatch();">
							</td>
						</tr>
						<% } %>
					</table>
				</div>
			</div>
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD noWrap align="center" id="TD1" runat="server">
						<input id="ApplyBtn" type="button" class="ButtonStyle" value="Preview these changes" onclick="f_onApplyChanges(event); return false;">
						<input id="CancelEditModeBtn" type="button" class="ButtonStyle" value="Cancel Edit" onclick="f_setEditMode(false, false, event);">
						<input id="EditModeBtn" type="button" class="ButtonStyle" value=" Edit " onclick="f_setEditMode(true, false, event);">
						<input id="UndoBtn" type="button" class="ButtonStyle" value=" Undo " onclick="f_undoLastAction(false);" onfocus="this.blur();">
						<INPUT class="ButtonStyle" type="button" id="SaveBtn" value="Commit All Changes" onclick="f_save(true);">&nbsp;&nbsp;&nbsp;
						<INPUT class="ButtonStyle" type="button" id="CloseBtn" value="Close" onclick="f_close();"></TD>
				</TR>
			</TABLE>
			<div id="selectFilter" runat="server" style="BORDER-RIGHT: gray 2px outset; BORDER-TOP: gray 2px outset; DISPLAY: none; BACKGROUND: gainsboro; BORDER-LEFT: gray 2px outset; WIDTH: 420px; BORDER-BOTTOM: gray 2px outset; POSITION: absolute" Width="420">
        <h4 class="page-header">Select Filter</h4>
				<DIV style="BORDER-RIGHT: medium none; PADDING-RIGHT: 2px; BORDER-TOP: 2px groove; OVERFLOW-Y: scroll; PADDING-LEFT: 2px; BACKGROUND: white; PADDING-BOTTOM: 2px; MARGIN: 8px; BORDER-LEFT: 2px groove; WIDTH: 395px; PADDING-TOP: 2px; BORDER-BOTTOM: 2px groove; HEIGHT: 315px">
					Category &nbsp;
					<asp:DropDownList id="m_categoryDdl" Runat="server" onchange="f_filterCategory();">
						<asp:ListItem Value="1">Visibility</asp:ListItem>
						<asp:ListItem Value="2">Investor</asp:ListItem>
						<asp:ListItem Value="3">Product Code</asp:ListItem>
						<asp:ListItem Value="5">Program Name</asp:ListItem>
					</asp:DropDownList>
					<hr>
					<div id="batchSelectionDiv">
					</div>
				</DIV>
				<DIV style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; WIDTH: 100%; PADDING-TOP: 0px; TEXT-ALIGN: center">
					<INPUT id="ChooseCloseBtn" onclick="this.parentElement.parentElement.style.display = 'none';" type="button" value="Close">
				</DIV>
			</div>
			<% if (m_useRateSheetExpirationFeature) { %>
			<%-- 01/29/08 mf. OPM 19887. Blocking details --%>
			<div id="blockStatus" runat="server" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:230px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; BACKGROUND-COLOR:whitesmoke">
                <span>
                    <span id="headerMsg">Lock requests by normal users for&nbsp; <span id="blockMsgProd"> </span>&nbsp;programs are currently blocked with the following message:
                    <br/>
                    <br/>
                    </span>
                    <span id="blockMsg"></span>
                </span>
				<div style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; WIDTH: 100%; PADDING-TOP: 0px; TEXT-ALIGN: center">
					[&nbsp;<a href="#" onclick="this.parentElement.parentElement.style.display = 'none';">Close</a>&nbsp;]
				</div>
			</div>
			<% } %>
		
		<div id="Modal" class="Modal">
		     <pre id="Rates">
		    
		    </pre>
		    <br />
		    <a href="#" class="close"  onclick="Modal.Hide();">close</a>
		</div>
		</form>
	</body>
</HTML>
