using System;
using LendersOffice.Common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Admin;
using System.Collections.Generic;

namespace LendersOfficeApp.los.PricingGroup
{
	public partial class PricingGroupMainFrame : BasePage
	{
	    /// <summary>
	    /// IE requires the meta tag to be FIRST in head. If this method returns a meta
	    /// the base page will add it on pre render. We should make it easier for dev to do this 
	    /// </summary>
	    /// <returns></returns>
	    protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
	    {
	        return E_XUAComaptibleValue.Edge;
	    }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingPriceGroups
                };
            }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;

            if (principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan)) 
            {
                return; 
            }
            throw new CBaseException(ErrorMessages.GenericAccessDenied, "Pricing Group requires PriceMyLoan feature");

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
