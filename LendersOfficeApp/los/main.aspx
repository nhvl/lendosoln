<%@ Page language="c#" Codebehind="main.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.main" %>
<%@ Import namespace="LendersOffice.Common"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html> 

<html>
  <head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <script type="text/javascript">
          function removeFromCurrentLoanList(id) {
            callFrameMethod(window, "frmCode", "removeFromCurrentLoanList", [id]);
          }
      </script>    
  </head>
  
  <frameset rows="0,*" frameborder="no" border="0" id="pageframeset" onunload="document.cookie = '.LENDINGQBUSER=0; path=/; expires=Fri, 31 Dec 1999 23:59:59 GMT;';">
    <frame name="frmCode" src="mainhelper.aspx" tabindex=-1>
    <frame name="frmMain" src=<%= AspxTools.SafeUrl(m_mainSrc) %>>
  </frameset>


</html>
