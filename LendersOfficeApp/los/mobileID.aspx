﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mobileID.aspx.cs" Inherits="LendersOfficeApp.los.mobileID" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Mobile ID</title>
    <LINK href="../css/stylesheet.css" type=text/css rel=stylesheet >
     <script language="javascript">
     function _init() {
        var windowheight = $(document).height();
        resize(400,250);        
     }
     </script>
</head>
<body>
    <h4 class="page-header">Mobile ID</h4>
    <form id="form1" runat="server">
	<div  style="margin-top: 10px;margin-left: 10px;margin-right: 10px; font-size:13px;">
        <div>
            <b>Mobile ID:</b>
            <ml:EncodedLabel id="mobilePassword" runat="server" />
        </div>
        <br />
        <br />
        <div>
            To Register:
        </div>
        <br />
        <div style="padding-left:2em">
	        1. Download and open mobile LendingQB app
        </div>
        <br />
        <div style="padding-left:2em">
	         2. Enter your credentials and the <b>Mobile ID</b> shown above where indicated
        </div>
        <br />
        <div>
            <font color="red">
                This <b>Mobile ID</b> is active for the next 10 minutes and may be used once on one device only.
            </font>
        </div>
        <br />
        <input  onclick="onClosePopup();" type="button" value="Close" style="float: right;">
        
    </div>
    </form>
</body>
</html>
