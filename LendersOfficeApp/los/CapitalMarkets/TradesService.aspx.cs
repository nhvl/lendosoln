﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using LendersOffice.ObjLib.CapitalMarkets;
using DataAccess;
using System.Web.Services;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOfficeApp.los.CapitalMarkets
{
    public partial class TradesService : BaseServicePage
    {
        protected static Guid BrokerID => BrokerUserPrincipal.CurrentPrincipal.BrokerId;

        [WebMethod]
        public static bool DeleteTrade(Guid tradeId)
        {
            Trade.RetrieveById(tradeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId).Delete();
            return true;
        }

        [WebMethod]
        public static object GetTransactionCalcGainLossAmount(string transactionPrice_rep, string transactionAmount_rep, int tradeTypeB, string tradePrice_rep)
        {
            LosConvert losConvert = new LosConvert();
            try
            {
                decimal transactionPrice = decimal.Parse(transactionPrice_rep);
                decimal transactionAmount = losConvert.ToMoney(transactionAmount_rep);
                decimal tradePrice = decimal.Parse(tradePrice_rep);

                decimal gainLossAmount = Transaction.CalculateGainLossAmount(transactionPrice, transactionAmount, (E_TradeDirection)tradeTypeB, tradePrice);
                return new { GainLossAmount = losConvert.ToMoneyString(gainLossAmount, FormatDirection.ToRep) };
            }
            catch (FormatException)
            {
                return new { Error = "Could not parse one of the fields required for Gain/Loss amount." }; // Invalid input. Don't show anything.
            }
        }

        [WebMethod]
        public static object SaveTrade(
            string TradeIdStr,
            string TradeNum, string DealerInvestor, string AssignedTo,
            int TypeA, int TypeB, int Month,
            string DescriptionDDLValue, string DescriptionDDLText,
            string Coupon,
            bool IsAllocatedLoansCalculated, string AllocatedLoans,
            string Price,
            string TradeDate, string NotificationDate, string SettlementDate)
        {
            Trade trade;
            Guid TradeId = new Guid(TradeIdStr);
            try
            {
                trade = Trade.RetrieveById(TradeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            }
            catch (NotFoundException)
            {
                trade = new Trade(BrokerID, TradeId);
            }
            trade.TradeNum_rep = TradeNum;
            trade.DealerInvestor = DealerInvestor;
            trade.AssignedTo = AssignedTo;
            trade.Type = (E_TradeType) TypeA;
            trade.Direction = (E_TradeDirection) TypeB;
            trade.Month = Month;
            trade.DescriptionObj = new Description(new Guid(DescriptionDDLValue), DescriptionDDLText);
            trade.Coupon_rep = Coupon;
            trade.IsAllocatedLoansCalculated = IsAllocatedLoansCalculated;
            trade.AllocatedLoans_rep = AllocatedLoans;
            trade.Price_rep = Price;
            trade.TradeDate_rep = TradeDate;
            trade.NotificationDate_rep = NotificationDate;
            trade.SettlementDate_rep = SettlementDate;
            trade.Save();
            return new { TradeId = trade.TradeId };
        }

        [WebMethod]
        public static object SaveTransaction(
            string TradeIdStr,
            string TransactionIdStr,
            string Date,
            string Amount,
            string Price,
            bool NotIsGainLossCalculated, string GainLoss,
            string Expense,
            string Memo)
        {
            Guid TradeId = new Guid(TradeIdStr);
            Guid TransactionId = new Guid(TransactionIdStr);
            // Load up the trade
            Trade trade;
            try
            {
                trade = Trade.RetrieveById(TradeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            }
            catch (NotFoundException)
            {
                return new { Error = "The trade could not be found." };
            }
            
            // Load up the transaction
            Transaction transaction = trade.GetTransactionById(TransactionId);
            if (transaction == null)
            {
                transaction = new Transaction(trade);
            }

            transaction.Date_rep = Date;
            transaction.Amount_rep = Amount;
            if (string.IsNullOrEmpty(Price))
            {
                transaction.Price_rep = trade.Price_rep;
            }
            else
            {
                transaction.Price_rep = Price;
            }
            transaction.IsGainLossCalculated = !NotIsGainLossCalculated;
            transaction.GainLossAmount_rep = GainLoss;
            transaction.Expense_rep = Expense;
            transaction.Memo = Memo;
            transaction.Save();
            return new { };
        }
    }
}
