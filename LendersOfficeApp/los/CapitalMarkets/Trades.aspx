﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="Trades.aspx.cs" Inherits="LendersOfficeApp.los.CapitalMarkets.Trades" EnableViewState="true" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MBS-TBA Trades</title>
    <style type="text/css">
        .clear
        {
            clear:both;
        }
        .error
        {
            color:red;
        }
        .DisplayNone
        {
            display: none;
        }
        #PageContainer
        {
            margin: 1em;
        }
        #TopSection
        {
            width: 100%;
        }
        #TradeContainer
        {
        	height: 15em;
        	overflow-y: scroll;
        	width: 100%;
        	border: 1px solid black;
        }
        #m_trades
        {
            width: 100%;
        }
        #TransactionContainer
        {
        	height: 10em;
        	overflow-y: scroll;
        	width: 100%;
        	border: 1px solid black;
        }
        #m_transactions
        {
        	width: 100%;
            overflow:scroll;
        }
        #ButtonSection
        {
        	margin-top: 1em;
        	margin-bottom: 1em;
        }
        #TradeSection
        {
            border: 1px solid black;
            overflow: auto;
            width: 98%;
            padding: 0.5em;
        }
        #TradeIdentInfo
        {
        }
        #TradeDetailsContainer
        {
            width: 100%;
            overflow: auto;
        }
        #AllocatedLoansOverride
        {
            width: 1em;
            float: left;
        }
        #TransactionSection
        {
        	width: 60%;
        }
        #TransactionDataTop
        {
            overflow: auto;
            width: 100%;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
        }
        #TransactionButtons
        {
            margin-top: 1em;
            margin-bottom: 1em;
        }
        .field_container1row
        {
        	margin-bottom: 0.5em;
        }
        .field_container1row label
        {
        	width: 6.5em;
        }
        .field_container2row
        {
            margin-bottom: 0.5em;
        }
        .field_container2row label
        {
        	display: block;
        }
        .OneColumn
        {
        	margin-left: 1%;
        	margin-right: 1%;
        	width: 98%;
        }
        .ThreeColumn
        {
        	margin-left: 1%;
        	margin-right: 1%;
        	width: 30%;
        	float: left;
        }
        .ThreeEighthColumn
        {
            margin-left: 1%;
            margin-right: 1%;
            width: 36%;
            float: left;
        }
        .FourColumn
        {
        	padding: 0;
        	margin-left: 1%;
        	margin-right: 1%;
        	width: 22%;
        	display: inline;
            float: left;
        }
        .FiveColumn
        {
        	padding: 0;
        	margin-left: 1%;
        	margin-right: 1%;
        	width: 18%;
        	display: inline;
            float: left;
        }
        .pct90
        {
        	padding: 0;
        	width:90%;
        	display:inline;
        	margin-left: 0%;
        	margin-right: 0%;
        	float:left;
        }
        .pct10
        {
        	padding: 0;
            width:10%;
            display:inline;
            margin-left: 0%;
        	margin-right: 0%;
        	float:left;
        }
        hr
        {
            color: black;
            background-color: black;
            height: 1px;
            border: 0px;
        }
    </style>
    <script src="../../inc/json.js" type="text/javascript"></script>
</head>
<body>
    <script>
        var tradeDirty = false;
        var transactionDirty = false;

        function ajax_calcTransactionGainLossAmount()
        {
            if (!$('#<%= AspxTools.HtmlString(TransactionGainLoss.ClientID) %>').prop('readonly')) {
                return; // Manual entry. Do not modify.
            }

            var args = {
                transactionPrice_rep : $('#<%= AspxTools.HtmlString(TransactionPrice.ClientID) %>').val(),
                transactionAmount_rep : $('#<%= AspxTools.HtmlString(TransactionAmount.ClientID) %>').val(),
                tradeTypeB : $('#<%= AspxTools.HtmlString(Direction.ClientID) %>').val(),
                tradePrice_rep : $('#<%= AspxTools.HtmlString(Price.ClientID) %>').val()
            };

            var data = JSON.stringify(args);

            callWebMethodAsync({
                type: "POST",
                url: "TradesService.aspx/GetTransactionCalcGainLossAmount",
                data: data,
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                success: function(msg) {
                    $('#<%= AspxTools.HtmlString(TransactionGainLoss.ClientID) %>').val(msg.d.GainLossAmount);
                },
                error: function(){ }
            });
        }

        function ajax_saveTrade()
        {
            var args = {
                TradeIdStr : null,
                TradeNum : $('#<%= AspxTools.HtmlString(TradeNum.ClientID) %>').val(),
                DealerInvestor : $('#<%= AspxTools.HtmlString(DealerInvestor.ClientID) %>').val(),
                AssignedTo : $('#<%= AspxTools.HtmlString(AssignedTo.ClientID) %>').val(),
                TypeA : $('#<%= AspxTools.HtmlString(Type.ClientID) %>').val(),
                TypeB : $('#<%= AspxTools.HtmlString(Direction.ClientID) %>').val(),
                Month : $('#<%= AspxTools.HtmlString(Month.ClientID) %>').val(),
                DescriptionDDLValue : $('#<%= AspxTools.HtmlString(DescriptionDDL.ClientID) %>').val(),
                DescriptionDDLText : $('#<%= AspxTools.HtmlString(DescriptionDDL.ClientID) %> option:selected').text(),
                Coupon : $('#<%= AspxTools.HtmlString(Coupon.ClientID) %>').val(),
                IsAllocatedLoansCalculated : $('#<%= AspxTools.HtmlString(IsAllocatedLoansCalculated.ClientID) %>').prop('checked') == true,
                AllocatedLoans : $('#<%= AspxTools.HtmlString(AllocatedLoans.ClientID) %>').val(),
                Price : $('#<%= AspxTools.HtmlString(Price.ClientID) %>').val(),
                TradeDate : $('#<%= AspxTools.HtmlString(TradeDate.ClientID) %>').val(),
                NotificationDate : $('#<%= AspxTools.HtmlString(NotificationDate.ClientID) %>').val(),
                SettlementDate : $('#<%= AspxTools.HtmlString(SettlementDate.ClientID) %>').val()
            };

            args.TradeIdStr = $('.SelectedTrade').find('.TradeIdField').text(); // there might not even BE a selectedTrade

            var data = JSON.stringify(args);

            callWebMethodAsync({
                async: false,
                type: "POST",
                url: "TradesService.aspx/SaveTrade",
                data: data,
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                success: function(data, textStatus, jqXHR) { }, // do nothing
                error: function(jqXHR, textStatus, errorThrown){
                    alert("Could not save! " + errorThrown);
                }
            });
        }

        function ajax_saveTransaction()
        {
            var args = {
                TradeIdStr : null,
                TransactionIdStr : null,
                Date : $('#<%= AspxTools.HtmlString(TransactionDate.ClientID) %>').val(),
                Amount : $('#<%= AspxTools.HtmlString(TransactionAmount.ClientID) %>').val(),
                Price : $('#<%= AspxTools.HtmlString(TransactionPrice.ClientID) %>').val(),
                NotIsGainLossCalculated : $('#<%= AspxTools.HtmlString(TransactionNotIsGainLossCalculated.ClientID) %>').prop('checked') == true,
                GainLoss : $('#<%= AspxTools.HtmlString(TransactionGainLoss.ClientID) %>').val(),
                Expense : $('#<%= AspxTools.HtmlString(TransactionExpense.ClientID) %>').val(),
                Memo : $('#<%= AspxTools.HtmlString(TransactionMemo.ClientID) %>').val()
            }

            args.TradeIdStr = $('.SelectedTrade').find('.TradeIdField').text();
            args.TransactionIdStr = $('.SelectedTransaction').find('.TransactionIdField').text();


            var data = JSON.stringify(args);

            callWebMethodAsync({
                async: false,
                type: "POST",
                url: "TradesService.aspx/SaveTransaction",
                data: data,
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                success: function(data, textStatus, jqXHR) { }, // do nothing
                error: function(jqXHR, textStatus, errorThrown){
                    alert("Could not save! " + errorThrown);
                }
            });
        }

        function TradeNum_Validate(sender, e) {
            var value = e.Value;
            value = $.trim(value);
            var isNumber = /^\d+$/.test(value); // is it at least a number?
            if (!isNumber) {
                e.IsValid = false;
                return;
            }

            // Ignore the selected trade
            var valid = true;
            $('.UnselectedTrade').each(function() {
                var probeNum = $(this).find('.TradeNumField').text();
                probeNum = $.trim(probeNum);
                if (value == probeNum) { // Duplicate trade num detected!
                    valid = false;
                    return;
                }
            });
            e.IsValid = valid;
            return;
        }

        function m_SaveTradeBtn_ClientClick() {
            tradeDirty = false;
        }

        function saveOnExit() { return confirm("Do you want to save changes?"); }

        function confirmSave() {
            var isConfirmedSave = false;
            if (tradeDirty || transactionDirty) {
                isConfirmedSave = saveOnExit();
            }
            if (isConfirmedSave) {
                if(transactionDirty) {
                    if (Page_ClientValidate("TransactionValidation")) {
                        ajax_saveTransaction();
                        transactionDirty = false;
                    } else {
                        alert("Could not save! Transaction failed validation.");
                        return false;
                    }
                }
                if(tradeDirty) {
                    if (Page_ClientValidate("TradeValidation")) {
                        ajax_saveTrade();
                        tradeDirty = false;
                    } else {
                        alert("Could not save! Trade failed validation.");
                        return false;
                    }
                }
            } else {
                transactionDirty = false;
                tradeDirty = false;
                return true; // Don't save changes
            }
        }

        function setTransactionGainLossState(state) {
            $TransactionGainLoss = $('#<%= AspxTools.HtmlString(TransactionGainLoss.ClientID) %>');
            if (state) {
                $TransactionGainLoss.prop('readonly', false);
            } else {
                $TransactionGainLoss.prop('readonly', true);
            }
        }

        function _init() {
            setTransactionGainLossState($('#<%= AspxTools.HtmlString(TransactionNotIsGainLossCalculated.ClientID) %>').prop('checked'));
            $('#<%= AspxTools.HtmlString(TransactionNotIsGainLossCalculated.ClientID) %>').click( function () {
                setTransactionGainLossState(this.checked);
                ajax_calcTransactionGainLossAmount();
            });
            $('#<%= AspxTools.HtmlString(TransactionPrice.ClientID) %>').change(function() {
                ajax_calcTransactionGainLossAmount();
            });
            $('#<%= AspxTools.HtmlString(TransactionAmount.ClientID) %>').change(function() {
                ajax_calcTransactionGainLossAmount();
            });
            $('#<%= AspxTools.HtmlString(Direction.ClientID) %>').change(function() {
                ajax_calcTransactionGainLossAmount();
            });
            $('#<%= AspxTools.HtmlString(Price.ClientID) %>').change(function() {
                ajax_calcTransactionGainLossAmount();
            });

            // align the datepickers
            $('.field_container1row img').css('vertical-align', 'bottom');
            $('.field_container1row img').removeAttr('valign');
            $('.field_container2row img').css('vertical-align', 'bottom');
            $('.field_container2row img').removeAttr('valign');

            // Page dirtyness
            $('.TradeInputClass').change(function() { tradeDirty = true; });
            $('.TransactionInputClass').change(function() { transactionDirty = true; });
            window.onbeforeunload = function() {
                if (tradeDirty || transactionDirty) {
                    return UnloadMessage;
                }
            };

        }
    -->
    </script>
    <form id="PageContainer" runat="server">
    <div id="TopSection">
        <asp:DropDownList runat="server" ID="m_filter" AutoPostBack="true" OnSelectedIndexChanged="m_filter_SelectedIndexChanged"></asp:DropDownList>
    </div>
    <div id="TradeContainer">
        <asp:GridView runat="server" ID="m_trades" AutoGenerateSelectButton="true" DataKeyNames="TradeId" OnSelectedIndexChanged="m_trades_SelectedIndexChanged" AutoGenerateColumns="false">
            <RowStyle CssClass="UnselectedTrade GridItem" />
            <AlternatingRowStyle CssClass="UnselectedTrade GridAlternatingItem" />
            <SelectedRowStyle CssClass="SelectedTrade GridSelectedItem" />
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Trade #
                    </HeaderTemplate>
                    <ItemStyle CssClass="TradeIdentifier" />
                    <ItemTemplate>
                        <span class='TradeIdField DisplayNone'>
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TradeId").ToString()) %>
                        </span>
                        <span class='TradeNumField'>
                            <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TradeNum_rep").ToString()) %>
                        </span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Type" DataField="Type" />
                <asp:BoundField HeaderText="Month" DataField="Month_rep" />
                <asp:BoundField HeaderText="Description" DataField="Description_rep" />
                <asp:BoundField HeaderText="Coupon" DataField="Coupon_rep" />
                <asp:BoundField HeaderText="Buy Amount" DataField="BuyAmount_rep" />
                <asp:BoundField HeaderText="Sell Amount" DataField="SellAmount_rep" />
                <asp:BoundField HeaderText="Price" DataField="Price_rep" />
                <asp:BoundField HeaderText="Allocated Loans" DataField="AllocatedLoans_rep" />
                <asp:BoundField HeaderText="Settlement Date" DataField="SettlementDate_rep" DataFormatString="{0:d}" />
                <asp:BoundField HeaderText="Notification Date" DataField="NotificationDate_rep" DataFormatString="{0:d}" />
                <asp:BoundField HeaderText="Dealer/Investor" DataField="DealerInvestor" />
                <asp:BoundField HeaderText="Trade Date" DataField="TradeDate_rep" DataFormatString="{0:d}" />
            </Columns>
            <EmptyDataTemplate>
                No trades found.
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
    <div id="ButtonSection">
        <asp:Button ID="m_PrevTradeBtn"   runat="server" Text="&lt; &lt; Prev" OnClientClick="confirmSave()" OnClick="m_PrevTradeBtn_Click" />
        <asp:Button ID="m_NextTradeBtn"   runat="server" Text="&gt; &gt; Next" OnClientClick="confirmSave()" OnClick="m_NextTradeBtn_Click" />
        <asp:Button ID="m_AddTradeBtn"    runat="server" Text="Add"            OnClientClick="confirmSave()" OnClick="m_AddTradeBtn_Click"  />
        <asp:Button ID="m_SaveTradeBtn"   runat="server" Text="Save"           OnClick="m_SaveTradeBtn_Click" OnClientClick="m_SaveTradeBtn_ClientClick();" ValidationGroup="TradeValidation"/>
        <asp:Button ID="m_DeleteTradeBtn" runat="server" Text="Delete"         OnClick="m_DeleteTradeBtn_Click" OnClientClick="tradeDirty = false;" />
    </div>
    <div id="TradeValidationSection">
        <asp:CustomValidator runat="server"
            id="TradeNumUniquenessValidator"
            ControlToValidate="TradeNum"
            ClientValidationFunction="TradeNum_Validate"
            Display="None"
            ValidationGroup="TradeValidation"
            ErrorMessage="Trade # must be a unique number." />

        <asp:RangeValidator runat="server"
            ID="CouponRangeValidator" ControlToValidate="Coupon"
            MinimumValue="0" MaximumValue="99.999"
            Type="Double"
            Display="None"
            ValidationGroup="TradeValidation"
            ErrorMessage='The "Coupon" value must be between 0 and 100.' />

        <asp:RequiredFieldValidator ID="TradeNumValidator" runat="server" ControlToValidate="TradeNum" Display="None" ErrorMessage="Trade # required." ValidationGroup="TradeValidation" />
        <asp:ValidationSummary runat="server" DisplayMode="BulletList" ValidationGroup="TradeValidation"/>
        <ml:EncodedLabel runat="server" id="m_TradeAdditionalErrors" CssClass="error" Text=""></ml:EncodedLabel>
    </div>
    <div id="TradeSection">
        <div id="TradeIdentInfo">
            <div class="FourColumn">
                <div class="field_container1row">
                    <label class="FieldLabel">Trade #</label>
                    <asp:TextBox ID="TradeNum" runat="server"></asp:TextBox>
                </div>
            </div>
            <div style="float:left; margin-left: 1%; margin-right: 1%;">
                <div class="field_container1row">
                    <label class="FieldLabel">Dealer/Investor</label>
                    <asp:TextBox ID="DealerInvestor" runat="server" Width="17em"></asp:TextBox>
                </div>
            </div>
            <div style="float:left; margin-left: 1%; margin-right: 1%;">
                <div class="field_container1row">
                    <label class="FieldLabel">Assigned To</label>
                    <asp:TextBox ID="AssignedTo" runat="server" Width="17em"></asp:TextBox>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <br />
        <div id="TradeDetailsContainer">
            <div id="TradeDetailsColumn0" class="FourColumn">
                <div class="field_container1row">
                    <label class="FieldLabel">Type</label>
                    <asp:DropDownList ID="Type" runat="server"></asp:DropDownList>
                    <asp:DropDownList ID="Direction" runat="server"></asp:DropDownList>
                </div>
                <div class="field_container1row">
                    <label class="FieldLabel">Month</label>
                    <asp:DropDownList ID="Month" runat="server"></asp:DropDownList>
                </div>
                <div class="field_container1row">
                    <label class="FieldLabel">Description</label>
                    <asp:DropDownList ID="DescriptionDDL" runat="server"></asp:DropDownList>
                </div>
                <div class="field_container1row">
                    <label class="FieldLabel">Coupon</label>
                    <asp:TextBox ID="Coupon" runat="server"></asp:TextBox>
                </div>
            </div>
            <div id="TradeDetailsColumn1" class="FourColumn">
                <div class="field_container1row">
                    <label class="FieldLabel">Amount</label>
                    <ml:MoneyTextBox ID="Amount" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                </div>
                <div class="field_container1row">
                    <label class="FieldLabel">Allocated Loans</label>
                    <asp:CheckBox ID="IsAllocatedLoansCalculated" runat="server" Visible="false"/>
                    <ml:MoneyTextBox ID="AllocatedLoans" runat="server"></ml:MoneyTextBox>
                    <div class="clear"></div>
                </div>
                <div class="field_container1row">
                    <label class="FieldLabel">Price</label>
                    <asp:TextBox ID="Price" runat="server" Width="90px"></asp:TextBox>
                </div>
            </div>
            <div id="TradeDetailsColumn2" class="FourColumn">
                <div class="field_container1row">
                    <label class="FieldLabel">Trade Date</label>
                    <ml:DateTextBox ID="TradeDate" runat="server"></ml:DateTextBox>
                </div>
                <div class="field_container1row">
                    <label class="FieldLabel">Notification Date</label>
                    <ml:DateTextBox ID="NotificationDate" runat="server"></ml:DateTextBox>
                </div>
                <div class="field_container1row">
                    <label class="FieldLabel">Settlement Date</label>
                    <ml:DateTextBox ID="SettlementDate" runat="server"></ml:DateTextBox>
                </div>
            </div>
            <div id="TradeDetailsColumn3" class="FourColumn">
                <div class="field_container1row">
                    <label class="FieldLabel">Total Gain/Loss</label>
                    <ml:MoneyTextBox ID="TotalGainLoss" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                </div>
                <div class="field_container1row">
                    <label class="FieldLabel">Total Expense</label>
                    <ml:MoneyTextBox ID="TotalExpense" runat="server" ReadOnly="true"></ml:MoneyTextBox>
                </div>
            </div>
        </div>
        <hr />
        <div id="TradeAssociatedItems">
            <div id="TransactionSection">
                <div class="FieldLabel OneColumn">History</div>
                <div id="TransactionContainer" class="OneColumn">
                    <asp:GridView id="m_transactions" EnableViewState="false" DataKeyNames="TransactionId" runat="server" AutoGenerateColumns="false" AutoGenerateSelectButton="true" OnSelectedIndexChanged="m_transaction_SelectedIndexChanged">
                        <RowStyle CssClass="UnselectedTransaction GridItem" />
                        <AlternatingRowStyle CssClass="UnselectedTransaction GridAlternatingItem" />
                        <SelectedRowStyle CssClass="SelectedTransaction GridSelectedItem" />
                        <Columns>
                            <asp:BoundField HeaderText="Date" DataField="Date_rep" />
                            <asp:BoundField HeaderText="Amount" DataField="Amount_rep" />
                            <asp:BoundField HeaderText="Price" DataField="Price_rep" />
                            <asp:BoundField HeaderText="Gain/Loss" DataField="GainLossAmount_rep" />
                            <asp:BoundField HeaderText="Expense" DataField="Expense_rep" />
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Memo
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <span class="TransactionIdField DisplayNone">
                                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TransactionId").ToString()) %>
                                    </span>
                                    <span class="TransactionMemoField">
                                        <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Memo").ToString()) %>
                                    </span>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataRowStyle CssClass="" />
                        <EmptyDataTemplate>
                            No transactions found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div id="TransactionButtons" class="OneColumn">
                    <asp:Button ID="m_AddTransactionBtn" runat="server" text="Add transaction" OnClick="m_AddTransactionBtn_Click" />
                    <asp:Button ID="m_SaveTransactionBtn" runat="server" text="Save transaction" OnClientClick="transactionDirty = false;" OnClick="m_SaveTransactionBtn_Click" ValidationGroup="TransactionValidation" />
                    <asp:Button ID="m_DeleteTransactionBtn" runat="server" text="Delete transaction" OnClientClick="transactionDirty = false;" OnClick="m_DeleteTransactionBtn_Click" />
                </div>
                <div id="TransactionValidationSection">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="TransactionDate" Display="None" ErrorMessage="Transaction Date required." ValidationGroup="TransactionValidation" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="TransactionAmount" Display="None" ErrorMessage="Amount required." ValidationGroup="TransactionValidation" />
                    <asp:ValidationSummary runat="server" DisplayMode="BulletList" ValidationGroup="TransactionValidation"/>
                    <ml:EncodedLabel runat="server" id="m_TransactionAdditionalErrors" CssClass="error" Text=""></ml:EncodedLabel>
                </div>
                <div id="TransactionData">
                    <div id="TransactionDataTop">
                        <div class="FiveColumn">
                            <div class="field_container2row">
                                <label class="FieldLabel">Date</label>
                                <ml:DateTextBox ID="TransactionDate" runat="server"></ml:DateTextBox>
                            </div>
                        </div>
                        <div class="FiveColumn">
                            <div class="field_container2row">
                                <label class="FieldLabel">Amount</label>
                                <ml:MoneyTextBox ID="TransactionAmount" preset="money" runat="server"></ml:MoneyTextBox>
                            </div>
                        </div>
                        <div class="FiveColumn">
                            <div class="field_container2row">
                                <label class="FieldLabel">Price</label>
                                <asp:TextBox ID="TransactionPrice" runat="server" Width="90px"></asp:TextBox>
                            </div>
                        </div>
                        <div class="FiveColumn">
                            <div class="field_container2row">
                                <label class="FieldLabel">Gain/Loss</label>
                                <asp:CheckBox ID="TransactionNotIsGainLossCalculated" runat="server" Width="13%" />
                                <ml:MoneyTextBox ID="TransactionGainLoss" runat="server" Width="73%"></ml:MoneyTextBox>
                            </div>
                        </div>
                        <div class="FiveColumn">
                            <div class="field_container2row">
                                <label class="FieldLabel">Expense</label>
                                <ml:MoneyTextBox ID="TransactionExpense" runat="server"></ml:MoneyTextBox>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="OneColumn">
                        <div class="field_container2row">
                            <label class="FieldLabel">Memo</label>
                            <asp:TextBox ID="TransactionMemo" runat="server" Width="100%"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>

