﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using LendersOffice.ObjLib.CapitalMarkets;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;

namespace LendersOfficeApp.los.CapitalMarkets
{
    public partial class Trades : LendersOffice.Common.BasePage
    {

        protected Guid BrokerID
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            }
        }

        private List<Trade> tradeList;
        private List<Transaction> transactionList;
        private Trade selectedTrade;
        
        private List<Trade> filteredTradeList;

        private enum E_Filter
        {
            Open,
            Closed
        }

        public string TradeInputClass = "TradeInputClass";
        public string TransactionInputClass = "TransactionInputClass";
        public override string StyleSheet
        {
            get { return VirtualRoot + "/css/stylesheet.css"; }
        }

        protected BrokerDB Broker
        {
            get
            {
                return BrokerDB.RetrieveById(BrokerID);
            }
        }

        protected bool IsNew
        {
            get
            {
                return ViewState["IsNewTrade"] != null && (bool) ViewState["IsNewTrade"];
            }
            set
            {
                ViewState["IsNewTrade"] = value;
            }
        }

        // Only want to auto generate trade num if new
        // Throw Alert if user removed trade num and tried to save blank
        protected bool IsAutoGenerateTradeNums
        {
            get
            {
                return IsNew && Broker.IsAutoGenerateTradeNums;
            }
        }

        protected void PageInit(object sender, EventArgs e)
        {
            selectedTrade = null;
            EnableJqueryMigrate = false;

            m_filter.Items.Add(new ListItem("Open Trades", ((int) E_Filter.Open).ToString()));
            m_filter.Items.Add(new ListItem("Closed Trades", ((int) E_Filter.Closed).ToString()));

            Tools.BindMonthNamesDropDown(Month);
            Tools.Bind_MBSTradeTypeA(Type);
            Tools.Bind_MBSTradeTypeB(Direction);

            var descriptions = Description.GetDescriptions();
            foreach (Description d in descriptions)
            {
                DescriptionDDL.Items.Add(new ListItem(d.DescriptionName, d.DescriptionId.ToString()));
            }

            TransactionFieldsReadOnly = true;

            // Set the fields to use a dirty bit
            // TODO: Refactoring item: put all these in a list
            TradeNum.CssClass += " " + TradeInputClass;
            DealerInvestor.CssClass += " " + TradeInputClass;
            AssignedTo.CssClass += " " + TradeInputClass;
            Type.CssClass += " " + TradeInputClass;
            Direction.CssClass += " " + TradeInputClass;
            Month.CssClass += " " + TradeInputClass;
            DescriptionDDL.CssClass += " " + TradeInputClass;
            Coupon.CssClass += " " + TradeInputClass;
            IsAllocatedLoansCalculated.CssClass += " " + TradeInputClass;
            AllocatedLoans.CssClass += " " + TradeInputClass;
            Price.CssClass += " " + TradeInputClass;
            TradeDate.CssClass += " " + TradeInputClass;
            NotificationDate.CssClass += " " + TradeInputClass;
            SettlementDate.CssClass += " " + TradeInputClass;

            TransactionDate.CssClass += " " + TransactionInputClass;
            TransactionAmount.CssClass += " " + TransactionInputClass;
            TransactionPrice.CssClass += " " + TransactionInputClass;
            TransactionNotIsGainLossCalculated.CssClass += " " + TransactionInputClass;
            TransactionGainLoss.CssClass += " " + TransactionInputClass;
            TransactionExpense.CssClass += " " + TransactionInputClass;
            TransactionMemo.CssClass += " " + TransactionInputClass;
        }

        private bool TradeFieldsReadOnly
        {
            get
            {
                return
                    TradeNum.ReadOnly
                    && DealerInvestor.ReadOnly
                    && AssignedTo.ReadOnly
                    && Type.Attributes["disabled"] != null
                    && Direction.Attributes["disabled"] != null
                    && Month.Attributes["disabled"] != null
                    && DescriptionDDL.Attributes["disabled"] != null
                    && Coupon.ReadOnly
                    //IsAllocatedLoansCalculated // TODO: when allocated loans are implemented
                    && AllocatedLoans.ReadOnly
                    && Price.ReadOnly
                    && TradeDate.ReadOnly
                    && NotificationDate.ReadOnly
                    && SettlementDate.ReadOnly;
            }
            set
            {
                TradeNum.ReadOnly = value;
                DealerInvestor.ReadOnly = value;
                AssignedTo.ReadOnly = value;
                if (value)
                {
                    Type.Attributes["disabled"] = "disabled";
                    Direction.Attributes["disabled"] = "disabled";
                    Month.Attributes["disabled"] = "disabled";
                    DescriptionDDL.Attributes["disabled"] = "disabled";
                }
                else
                {
                    Type.Attributes.Remove("disabled");
                    Direction.Attributes.Remove("disabled");
                    Month.Attributes.Remove("disabled");
                    DescriptionDDL.Attributes.Remove("disabled");
                }
                Coupon.ReadOnly = value;
                //IsAllocatedLoansCalculated; // TODO: when allocated loans are implemented
                AllocatedLoans.ReadOnly = value;
                Price.ReadOnly = value;
                TradeDate.ReadOnly = value;
                NotificationDate.ReadOnly = value;
                SettlementDate.ReadOnly = value;
            }
        }

        private bool TransactionFieldsReadOnly
        {
            get
            {
                return
                    TransactionDate.ReadOnly
                    && TransactionAmount.ReadOnly
                    && TransactionPrice.ReadOnly
                    && TransactionNotIsGainLossCalculated.Attributes["disabled"] != null
                    && TransactionGainLoss.ReadOnly
                    && TransactionExpense.ReadOnly
                    && TransactionMemo.ReadOnly;
            }
            set
            {
                TransactionDate.ReadOnly = value;
                TransactionAmount.ReadOnly = value;
                TransactionPrice.ReadOnly = value;
                if (value)
                {
                    TransactionNotIsGainLossCalculated.Attributes["disabled"] = "disabled";
                }
                else if (TransactionNotIsGainLossCalculated.Attributes["disabled"] != null)
                {
                    TransactionNotIsGainLossCalculated.Attributes.Remove("disabled");
                }
                TransactionGainLoss.ReadOnly = value;
                TransactionExpense.ReadOnly = value;
                TransactionMemo.ReadOnly = value;
            }
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Updated", "resizeTo(1048, 748);", true);
                TradeFieldsReadOnly = true;
            }
            tradeList = Trade.GetTradesByBroker(BrokerID);

            ApplyFilter((E_Filter)int.Parse(m_filter.SelectedValue));

            if (ViewState["SelectedTradeId"] != null)
            {
                selectedTrade = tradeList.Find((Trade t) => t.TradeId == (Guid)ViewState["SelectedTradeId"]);
            }

            m_TradeAdditionalErrors.Text = "";
            m_TransactionAdditionalErrors.Text = "";

            TransactionFieldsReadOnly = true;
        }

        protected void PagePreRender(object sender, EventArgs e)
        {
            // After viewstate has done its thing
            // After postbacks have done their thing
            if (selectedTrade != null && ViewState["SelectedTransactionId"] != null)
            {
                Guid selectedTransactionId = (Guid)ViewState["SelectedTransactionId"];
                int selectedTransactionIndex = selectedTrade.Transactions.FindIndex((Transaction tr) => tr.TransactionId == selectedTransactionId);
                Transaction selectedTransaction = selectedTrade.Transactions[selectedTransactionIndex];
                BindTrade(selectedTrade);
                m_transactions.SelectedIndex = selectedTransactionIndex;
                ViewState["SelectedTransactionId"] = selectedTransactionId;
                TransactionFieldsReadOnly = false;
            }

            TradeNumValidator.Enabled = !IsAutoGenerateTradeNums;
        }

        protected override void OnInit(EventArgs e)
        {
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);
            this.PreRender += new System.EventHandler(this.PagePreRender);
            base.OnInit(e);
        }

        // Is this necessary? Since we're applying the filter on PageLoad, it may be called multiple times
        protected void m_filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyFilter((E_Filter) int.Parse(m_filter.SelectedValue));
        }

        private void ApplyFilter(E_Filter filterType)
        {
            switch (filterType)
            {
                case E_Filter.Open:
                    filteredTradeList = tradeList.Where((Trade t) => t.Status == TradeStatus.Open).ToList();
                    break;
                case E_Filter.Closed:
                    filteredTradeList = tradeList.Where((Trade t) => t.Status == TradeStatus.Closed).ToList();
                    break;
                default:
                    throw new UnhandledEnumException(filterType);
            }
            m_trades.DataSource = filteredTradeList;
            m_trades.DataBind();
            ReconcileSelectedIndex();
        }

        protected void m_trades_SelectedIndexChanged(object sender, EventArgs e)
        {
            IsNew = false;
            SelectedIndexChangedHelper();
        }

        protected void SelectedIndexChangedHelper()
        {
            try
            {
                selectedTrade = filteredTradeList[Convert.ToInt32(m_trades.SelectedIndex)];
                ViewState["SelectedTradeId"] = selectedTrade.TradeId;
            }
            catch (ArgumentOutOfRangeException)
            {
                selectedTrade = null;
                return;
            }

            BindTrade(selectedTrade);
            ClearTransactionInfo();
        }

        private void BindTrade(Trade trade)
        {
            if (trade == null)
            {
                ClearTrade();
                return;
            }

            TradeNum.Text = trade.TradeNum_rep;
            DealerInvestor.Text = trade.DealerInvestor;
            AssignedTo.Text = trade.AssignedTo;

            Type.SelectedIndex = (int)trade.Type;
            Direction.SelectedIndex = (int)trade.Direction;
            Month.SelectedValue = trade.Month.ToString();

            for (int i = 0; i < DescriptionDDL.Items.Count; i++)
            {
                ListItem item = DescriptionDDL.Items[i];
                // This definitely violates some OOP principle
                if (item.Value == trade.DescriptionObj.DescriptionId.ToString())
                {
                    DescriptionDDL.SelectedIndex = i;
                    break;
                }
            }
            
            Coupon.Text = trade.Coupon_rep;
            Amount.Text = trade.Amount_rep;
            IsAllocatedLoansCalculated.Checked = trade.IsAllocatedLoansCalculated;
            AllocatedLoans.Text = trade.AllocatedLoans_rep;
            Price.Text = trade.Price_rep;
            TradeDate.Text = trade.TradeDate_rep;
            NotificationDate.Text = trade.NotificationDate_rep;
            SettlementDate.Text = trade.SettlementDate_rep;

            TotalGainLoss.Text = trade.TotalGainLoss_rep;
            TotalExpense.Text = trade.TotalExpense_rep;

            TradeFieldsReadOnly = false;

            BindTransactionsFromTrade(trade);
        }

        private void BindTransactionsFromTrade(Trade trade)
        {
            transactionList = trade.Transactions;
            m_transactions.DataSource = transactionList;
            m_transactions.DataBind();
            m_transactions.SelectedIndex = -1;
            TransactionFieldsReadOnly = true;
        }

        private void ClearTrade()
        {
            if (m_trades != null)
            {
                m_trades.SelectedIndex = -1;
            }
            
            selectedTrade = null;
            ViewState["SelectedTradeId"] = null;

            TradeNum.Text = "";
            DealerInvestor.Text = "";
            AssignedTo.Text = "";

            Type.SelectedIndex = 0;
            Direction.SelectedIndex = 0;
            Month.SelectedIndex = 0;

            DescriptionDDL.SelectedIndex = 0;
            Coupon.Text = "";
            Amount.Text = "";
            IsAllocatedLoansCalculated.Checked = false;
            AllocatedLoans.Text = "";
            Price.Text = "";
            TradeDate.Text = "";
            NotificationDate.Text = "";
            SettlementDate.Text = "";

            TotalGainLoss.Text = "";
            TotalExpense.Text = "";

            transactionList = new List<Transaction>();
            m_transactions.DataSource = transactionList;
            m_transactions.DataBind();
            m_transactions.SelectedIndex = -1;

            TradeFieldsReadOnly = true;
            TransactionFieldsReadOnly = true;
        }

        protected void m_PrevTradeBtn_Click(object sender, EventArgs e)
        {
            if (m_trades == null)
            {
                return;
            }

            IsNew = false;
            var newIndex = m_trades.SelectedIndex - 1;
            SetTradeIndex(newIndex);
        }

        protected void m_NextTradeBtn_Click(object sender, EventArgs e)
        {
            if (m_trades == null)
            {
                return;
            }

            IsNew = false;
            var newIndex = m_trades.SelectedIndex + 1;
            SetTradeIndex(newIndex);

        }

        private void SetTradeIndex(int index)
        {
            if (tradeList.Count == 0)
            {
                m_trades.SelectedIndex = -1;
            }
            else if (index <= -1) // wrap backward
            {
                m_trades.SelectedIndex = filteredTradeList.Count - 1;
            }
            else if (index >= filteredTradeList.Count) // wrap forward
            {
                m_trades.SelectedIndex = 0;
            }
            else
            {
                m_trades.SelectedIndex = index;
            }
            SelectedIndexChangedHelper();
        }

        protected void m_AddTradeBtn_Click(object sender, EventArgs e)
        {
            ClearTrade();
            
            // Create a new empty trade
            IsNew = true;
            selectedTrade = new Trade(BrokerID);
            ViewState["SelectedTradeId"] = selectedTrade.TradeId;
            tradeList.Add(selectedTrade);
            // Switch filter to open trades
            ApplyFilter(E_Filter.Open);
            m_filter.SelectedValue = ((int)E_Filter.Open).ToString();
            // Select it
            SetTradeIndex(filteredTradeList.Count - 1);
        }

        protected void m_SaveTradeBtn_Click(object sender, EventArgs e)
        {
            if (!this.IsValid) // Only save if we pass validation
            {
                return;
            }
            Trade t;
            if (selectedTrade != null)
            {
                t = selectedTrade;   
            }
            else if (ViewState["SelectedTradeId"] != null)
            {
                t = new Trade(BrokerID, (Guid) ViewState["SelectedTradeId"]);
            }
            else
            {
                t = new Trade(BrokerID);
            }

            try
            {
                // make sure this is unique
                if (String.IsNullOrEmpty(TradeNum.Text.TrimWhitespaceAndBOM()) && IsAutoGenerateTradeNums)
                    t.TradeNum_rep = Trade.GetTradeAutoNum(BrokerUserPrincipal.CurrentPrincipal.BrokerId).ToString();
                else
                    t.TradeNum_rep = TradeNum.Text; 

                t.DealerInvestor = DealerInvestor.Text;
                t.AssignedTo = AssignedTo.Text;
                t.Type = (E_TradeType)Convert.ToInt32(Type.SelectedValue);
                t.Direction = (E_TradeDirection)Convert.ToInt32(Direction.SelectedValue);
                t.Month = Convert.ToInt32(Month.SelectedValue); // make sure it's not null
                t.DescriptionObj = new Description(new Guid(DescriptionDDL.SelectedItem.Value), DescriptionDDL.SelectedItem.Text);
                t.Coupon_rep = Coupon.Text; // make sure coupon is between 0 and 100
                t.TradeDate_rep = TradeDate.Text;
                t.NotificationDate_rep = NotificationDate.Text;
                t.SettlementDate_rep = SettlementDate.Text;
                t.Price_rep = Price.Text;
                t.IsAllocatedLoansCalculated = IsAllocatedLoansCalculated.Checked;
                t.AllocatedLoans_rep = AllocatedLoans.Text;
            }
            catch (FormatException)
            {
                m_TradeAdditionalErrors.Text = "Could not parse.";
                return;
            }
            t.Save();

            IsNew = false;
            Refresh(true); // Perhaps a new trade was added to the list, so hit the DB again

            // BIND TRADE
            int tradeIndex = filteredTradeList.FindIndex((Trade tradeCandidate) => tradeCandidate.TradeId == t.TradeId);
            m_trades.SelectedIndex = tradeIndex;
            BindTrade(t);
            ViewState["SelectedTradeId"] = t.TradeId;

            // BIND TRANSACTION IF NECESSARY
            if (ViewState["SelectedTransactionId"] != null)
            {
                int index = transactionList.FindIndex((Transaction tr) => tr.TransactionId == (Guid) ViewState["SelectedTransactionId"]);
                m_transactions.SelectedIndex = index;
                BindTransaction(transactionList[index]);
                ViewState["SelectedTransactionId"] = transactionList[index].TransactionId;
            }
        }
        protected void m_DeleteTradeBtn_Click(object sender, EventArgs e)
        {
            if (selectedTrade != null)
            {
                selectedTrade.Delete();
                tradeList.Remove(selectedTrade);
            }

            IsNew = false;
            Refresh(false);
            ClearTransactionInfo();
            ClearTrade();
        }

        protected void m_transaction_SelectedIndexChanged(object sender, EventArgs e)
        {
            Transaction selectedTransaction;
            try
            {
                selectedTransaction = selectedTrade.Transactions[m_transactions.SelectedIndex];
                ViewState["SelectedTransactionId"] = selectedTransaction.TransactionId;
            }
            catch (ArgumentOutOfRangeException)
            {
                selectedTransaction = null;
                return;
            }
            BindTransaction(selectedTransaction);
        }

        private void BindTransaction(Transaction transaction)
        {
            TransactionDate.Text = transaction.Date_rep;
            TransactionAmount.Text = transaction.Amount_rep;
            TransactionPrice.Text = transaction.Price_rep;
            TransactionNotIsGainLossCalculated.Checked = ! transaction.IsGainLossCalculated;
            TransactionGainLoss.Text = transaction.GainLossAmount_rep;
            TransactionExpense.Text = transaction.Expense_rep;
            TransactionMemo.Text = transaction.Memo;

            TransactionFieldsReadOnly = false;
        }

        protected void m_AddTransactionBtn_Click(object sender, EventArgs e)
        {
            if (selectedTrade == null)
            {
                m_TransactionAdditionalErrors.Text = "A trade must be selected and saved before adding a transaction!";
                return;
            }

            // Create a new empty transaction. This is a ghost transaction. You will not see it after the next postback.
            Transaction transaction = new Transaction(selectedTrade);
            ViewState["SelectedTransactionId"] = transaction.TransactionId;

            selectedTrade.Transactions.Add(transaction); // should be trade.AddTransaction
            Refresh(false);
            ClearTransactionInfo();
            TransactionFieldsReadOnly = false;

            m_transactions.SelectedIndex = selectedTrade.Transactions.Count - 1;
        }

        protected void m_SaveTransactionBtn_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            if (selectedTrade == null)
            {
                m_TransactionAdditionalErrors.Text = "A trade must be selected and saved before deleting a transaction!";
                return;
            }
            Transaction transaction;
            if (m_transactions != null && m_transactions.SelectedIndex != -1 && m_transactions.SelectedIndex < selectedTrade.Transactions.Count) // Transaction is old
            {
                transaction = selectedTrade.Transactions[m_transactions.SelectedIndex];
            }
            else // Transaction is new
            {
                transaction = new Transaction(selectedTrade);
            }

            try
            {
                transaction.Date_rep = TransactionDate.Text;
                transaction.Amount_rep = TransactionAmount.Text;
                if (string.IsNullOrEmpty(TransactionPrice.Text))
                {
                    transaction.Price_rep = selectedTrade.Price_rep;
                }
                else
                {
                    transaction.Price_rep = TransactionPrice.Text;
                }
                transaction.IsGainLossCalculated = !TransactionNotIsGainLossCalculated.Checked;
                transaction.GainLossAmount_rep = TransactionGainLoss.Text;
                transaction.Expense_rep = TransactionExpense.Text;
                transaction.Memo = TransactionMemo.Text;
            }
            catch (FormatException)
            {
                m_TransactionAdditionalErrors.Text = "Could not parse.";
                return;
            }
            transaction.Save();

            Refresh(true);

            int index = transactionList.FindIndex((Transaction tr) => tr.TransactionId == transaction.TransactionId);
            m_transactions.SelectedIndex = index;
            BindTransaction(transaction);
            ViewState["SelectedTransactionId"] = transaction.TransactionId;
        }

        protected void m_DeleteTransactionBtn_Click(object sender, EventArgs e)
        {
            if (selectedTrade == null)
            {
                m_TransactionAdditionalErrors.Text = "A trade must be selected before deleting a transaction!";
                return;
            }

            if (m_transactions == null || m_transactions.SelectedIndex == -1)
            {
                m_TransactionAdditionalErrors.Text = "A transaction must be selected!";
                return;
            }

            selectedTrade.DeleteTransaction((Guid) m_transactions.SelectedDataKey.Value);

            Refresh(false);
            ClearTransactionInfo();
            TransactionFieldsReadOnly = true;
        }

        /// <summary>
        /// Rebind all the data.
        /// </summary>
        /// <param name="hardRefresh">Query the database again</param>
        private void Refresh(bool hardRefresh)
        {
            if (hardRefresh)
            {
                tradeList = Trade.GetTradesByBroker(BrokerID);
                selectedTrade = tradeList.Find((Trade t) => t.TradeId == (Guid)ViewState["SelectedTradeId"]);
            }
            ApplyFilter((E_Filter)int.Parse(m_filter.SelectedValue));
            BindTrade(selectedTrade);
        }

        private void ClearTransactionInfo()
        {
            TransactionDate.Text = "";
            TransactionAmount.Text = "";
            TransactionPrice.Text = "";
            TransactionNotIsGainLossCalculated.Checked = false;
            TransactionGainLoss.Text = "";
            TransactionExpense.Text = "";
            TransactionMemo.Text = "";
            ViewState["SelectedTransactionId"] = null;
        }

        /// <summary>
        /// This is needed when the user selects a trade, then performs an action
        /// that causes it to be filtered out.
        /// </summary>
        private void ReconcileSelectedIndex()
        {
            if (selectedTrade == null)
            {
                return;
            }
            for (int i = 0; i < filteredTradeList.Count; i++)
            {
                if (selectedTrade.TradeId == filteredTradeList[i].TradeId)
                {
                    m_trades.SelectedIndex = i;
                    return;
                }
            }
            m_trades.SelectedIndex = -1;
        }
    }
}
