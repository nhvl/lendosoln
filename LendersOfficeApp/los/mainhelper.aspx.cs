using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
namespace LendersOfficeApp.los
{
	/// <summary>
	/// Summary description for mainhelper.
	/// </summary>
	public partial class mainhelper : LendersOffice.Common.BaseServicePage
	{
        protected AbstractUserPrincipal CurrentPrincipal
        {
            get { return PrincipalFactory.CurrentPrincipal; }
        }
        protected string m_serverID = "";
		protected void PageLoad(object sender, System.EventArgs e)
		{
            ServerLocation server = ConstAppDavid.CurrentServerLocation;
            switch (server) 
            {
                case ServerLocation.LocalHost:
                    m_serverID = "local_";
                    break;
                case ServerLocation.Demo:
                    m_serverID = "demo_";
                    break;
                case ServerLocation.Development:
                    m_serverID = "dev_";
                    break;
                case ServerLocation.DevCopy:
                    m_serverID = "devcopy_";
                    break;
            }
			// Put user code to initialize the page here
		}


        protected void PageInit(object sender, System.EventArgs e) 
        {
            this.RegisterJsScript("windowmanager.js");
            this.RegisterService("loanutils", "/newlos/LoanUtilitiesService.aspx");
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

            this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}
}
