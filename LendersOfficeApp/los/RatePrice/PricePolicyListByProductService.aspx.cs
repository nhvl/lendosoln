namespace LendersOfficeApp.los.RatePrice
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOffice.RatePrice;

    public partial class PricePolicyListByProductService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        private Guid BrokerId
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            }
        }
		protected override void Process(string methodName) 
		{
			switch (methodName) 
			{
				case "Save":
					Save();
					break;
                case "LoadPrograms":
                    LoadPrograms();
                    break;
			}
		}

        private void LoadPrograms()
        {
            Guid fileId = GetGuid("fileId", Guid.Empty);
            Guid priceGroupId = GetGuid("priceGroupId", Guid.Empty);
            Guid pmiProductId = GetGuid("pmiProductId", Guid.Empty);
            Guid programListKey = GetGuid("programListKey", Guid.Empty);
            bool isCollapseToAssociated = GetBool("isCollapseToAssociated", false);
            Object state = new object();

            var isDenied = !PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanModifyLoanPrograms);

            if (isDenied)
            {
                SetResult("Error", "Denied");
                return;
            }

            if (priceGroupId != Guid.Empty)
            {
                if (fileId != Guid.Empty)
                {
                    state = LoanProgramTreeGenerator.DisplayProgramInPriceGroupAssocTree(false, fileId, priceGroupId, isCollapseToAssociated);
                }
                else if (programListKey != Guid.Empty)
                {
                    var list = AutoExpiredTextCache.GetFromCache(programListKey);
                    if (list == null)
                    {
                        throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Could not load program list from temp cache.");
                    }

                    SetResult("programList", SerializationHelper.JsonNetAnonymousSerialize(list));
                    state = LoanProgramTreeGenerator.DisplayProgramInPriceGroupAssocTree(true, fileId, priceGroupId, isCollapseToAssociated);
                }
                else
                {
                    state = LoanProgramTreeGenerator.DisplayPriceGroupAssocTree(priceGroupId, isCollapseToAssociated);
                }
            }
            else if (fileId != Guid.Empty)
            {
                state = LoanProgramTreeGenerator.DisplayProgramAssocTree(fileId, isCollapseToAssociated);
            }
            else if (pmiProductId != Guid.Empty)
            {
                state = LoanProgramTreeGenerator.DisplayPmiProgramAssocTree(pmiProductId, isCollapseToAssociated);
            }
            else
            {
                throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "No provided program or price group to associate with.");
            }

            SetResult("state", SerializationHelper.JsonNetAnonymousSerialize(state));
        }


		private void Save()
		{
			switch ( (AssociationTypeForUi) GetInt("EditorMode") )
			{
				case AssociationTypeForUi.Program:
					SaveProgramAssociations();
					break;
				case AssociationTypeForUi.PriceGroup:
                    SavePriceGroupAssociations();
					break;
				case AssociationTypeForUi.ProgramInPriceGroup:
					SaveProgramInPriceGroupAssociations(false);
					break;
				case AssociationTypeForUi.ProgramInPriceGroupBatch:
					SaveProgramInPriceGroupAssociations(true);
					break;
                case AssociationTypeForUi.PmiProduct:
                    SaveProgramPmiAssociations();
                    break;
				default:
					Tools.LogBug("Unknown AssociationTypeForUi in Save(): " + (AssociationTypeForUi) GetInt("EditorMode") );
					break;
			}
		}

        private void SavePriceGroupAssociations()
        {
            Guid priceGroupId = GetGuid("PriceGroupId");
            string assocList = GetString("Value");

            VerifyPriceGroupMaster(priceGroupId);

            // Get current associations with this PriceGroup Master
            var listParams = new SqlParameter[] { new SqlParameter("@priceGroupId", priceGroupId) };
            string sql = "SELECT * FROM PRODUCT_PRICE_ASSOCIATION WHERE ProductId = @priceGroupId";

            using (CDataSet PriceGroupAssocs = new CDataSet())
            {
                PriceGroupAssocs.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, sql, listParams);
                DataTable dbAssocs = PriceGroupAssocs.Tables[0];

                // Get the new associations to be saved from client UI
                string[] policies = assocList.Split(',');
                Hashtable clientAssocs = new Hashtable(policies.Length);
                foreach (string policy in policies)
                {
                    String[] parameters = policy.Split(':');

                    if (parameters.Length != 2)
                    {
                        continue;
                    }

                    clientAssocs.Add(new Guid(parameters[0]), parameters[1]);
                }

                // Delete removed associations (Assocs that went from Y/N ->[Blank] at client).
                for (int i = 0; i < dbAssocs.Rows.Count; i++)
                {
                    if (clientAssocs.Contains(dbAssocs.Rows[i]["PricePolicyId"]) == false)
                    {
                        dbAssocs.Rows[i].Delete();
                    }
                }

                // Update changed existing associations and add the new ones to the set
                foreach (Guid policyId in clientAssocs.Keys)
                {
                    E_TriState newAssoc;
                    switch (clientAssocs[policyId].ToString())
                    {
                        case "Y":
                            newAssoc = E_TriState.Yes;
                            break;
                        case "N":
                            newAssoc = E_TriState.No;
                            break;
                        default:
                            Tools.LogBug("Unhandled association type: " + clientAssocs[policyId]);
                            continue;
                    }

                    // Update or create the row
                    bool rowExists = false;
                    foreach (DataRow dbAssoc in dbAssocs.Rows)
                    {
                        if (dbAssoc.RowState == DataRowState.Deleted)
                            continue;

                        if (policyId == (Guid)dbAssoc["PricePolicyId"])
                        {
                            rowExists = true;

                            if (newAssoc != (E_TriState)(byte)dbAssoc["AssocOverrideTri"])
                            {
                                // Update the setting
                                dbAssoc["AssocOverrideTri"] = newAssoc;
                            }
                            break;
                        }
                    }

                    // New association. Add it.
                    if (rowExists == false)
                    {
                        DataRow newRow = dbAssocs.NewRow();
                        newRow["ProductId"] = priceGroupId;
                        newRow["PricePolicyId"] = policyId;
                        newRow["AssocOverrideTri"] = newAssoc;

                        dbAssocs.Rows.Add(newRow);
                    }
                }

                PriceGroupAssocs.Save();
            }
        }

        private void VerifyPriceGroupMaster( Guid PriceGroupId )
        {
            // Create a master to attach to if it does not already exist.
            bool doesExist = CheckExistenceOfLoanProgramTemplate(PriceGroupId);
            if (doesExist) return;

            CLoanProductData.CreatePriceGroupMaster(PriceGroupId, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
        }
        
        public static bool CheckExistenceOfLoanProgramTemplate(Guid priceGroupId)
        {
            var listParams = new SqlParameter[] { new SqlParameter("@priceGroupId", priceGroupId) };
            string query = "SELECT lLpTemplateId FROM Loan_Program_Template WHERE lLpTemplateId = @priceGroupId";

            bool doesExist = false;
            Action<IDataReader> readHandler = reader =>
            {
                if (reader.Read())
                {
                    doesExist = true;
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, query, null, listParams, readHandler);
            return doesExist;
        }

        private void SaveProgramInPriceGroupAssociations( bool ForBatch )
		{
			// 04/14/07 mf OPM 21028 
			// Save when we are in the Program in PriceGroup view.
			// We Load up Y/N settings from client UI, and settings from DB, then
			// update the DB set based on deltas of UI.  Note that while the UI and
			// saving mechanism can handle blank association rows, they will be
			// deleted upon save to reduce table size.

			// We also allow a Batch mode.  From within the Price Group editor, the SAE
			// can select multiple programs.  When in batch mode, there is a setting to
			// indicate if we want to sync this save.  This is the concept of a batch "Save"
			// vs. batch "Update".  When we are not keeping in sync, we allow programs in the
			// set to keep policy associations when no attachment was set in the UI.

			// Example: Program P in PriceGroup PG has Y association with Policy Z.
			// User (SAE) performs a Program In PriceGroup PG batch operation that includes Program P,
			// and leaves the association with Z blank.  If the sync option is set, we clear P's
			// P-PG association with Z, otherwise, we allow the association to remain.

			// Non-batch mode should always be in sync, since we can load the associations.

            // 04/21/09 mf. In OPM 21028, it was suggested that we allow SAEs to batch "clear" an
            // association. So in batch UI:
            // Y/N => Update Association
            // [No Selection] => Do Nothing
            // "Clear" => Remove any association if it exists
  
            // 05/15/09 mf. We are allowing the save of pricegroup associations.
			
			string programList = ForBatch ? GetString("ProgramList") : GetGuid("ProductId").ToString();
			bool blanksCanOverride = GetBool("KeepInSync", true);
			
			Guid priceGroupId = GetGuid("PriceGroupId");
			string assocList = GetString("Value");

			var listParams = new List<SqlParameter>();
            var programs = programList.Split(',').Select(id => new Guid(id));

            var inClause = DbTools.CreateParameterized4InClauseSql("Pr", programs, listParams);
            PrepareDbForProgramInPriceGroupAssociation( programList, inClause, listParams, priceGroupId ); // listParams copied in method prior to use

			listParams.Add(new SqlParameter("@pgID", priceGroupId));

            // Get current associations with this Progam within this Price Group from DB
            string sql = "SELECT * FROM Pricegroup_Prog_Policy_Assoc WHERE LpePriceGroupId = @pgID AND ProgId " + inClause;

            using (CDataSet ProgramInPriceGroupAssocs = new CDataSet())
            {
                ProgramInPriceGroupAssocs.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, sql, listParams);
                DataTable dbAssocs = ProgramInPriceGroupAssocs.Tables[0];

                // Get the new associations to be saved from client UI
                string[] policies = assocList.Split(',');
                Hashtable clientAssocs = new Hashtable(policies.Length);
                foreach (string policy in policies)
                {
                    String[] parameters = policy.Split(':');

                    if (parameters.Length != 2)
                    {
                        continue;
                    }

                    clientAssocs.Add(new Guid(parameters[0]), parameters[1]);
                }

                // Delete removed associations (Assocs that went from Y/N ->[Blank] at client).
                if (blanksCanOverride)
                {
                    for (int i = 0; i < dbAssocs.Rows.Count; i++)
                    {
                        if (clientAssocs.Contains(dbAssocs.Rows[i]["PricePolicyId"]) == false)
                        {
                            dbAssocs.Rows[i].Delete();
                        }
                    }
                }

                // Update changed existing associations and add the new ones to the set
                foreach (Guid policyId in clientAssocs.Keys)
                {
                    foreach (var programId in programs)
                    {
                        E_TriState newAssoc;
                        switch (clientAssocs[policyId].ToString())
                        {
                            case "Y":
                                newAssoc = E_TriState.Yes;
                                break;
                            case "N":
                                newAssoc = E_TriState.No;
                                break;
                            case "C":
                                newAssoc = E_TriState.Blank; // This means user set Clear, NOT that it was blank; we already handle those above
                                break;
                            default:
                                Tools.LogBug("Unhandled association type: " + clientAssocs[policyId].ToString());
                                continue;
                        }

                        bool rowExists = false;

                        foreach (DataRow dbAssoc in dbAssocs.Rows)
                        {
                            if (dbAssoc.RowState == DataRowState.Deleted)
                                continue;

                            if (policyId == (Guid)dbAssoc["PricePolicyId"] && programId == (Guid)dbAssoc["ProgId"])
                            {
                                rowExists = true;

                                if (newAssoc == E_TriState.Blank)
                                {
                                    // User wants to remove this association
                                    dbAssoc.Delete();
                                }
                                else if (newAssoc != (E_TriState)(byte)dbAssoc["AssociateType"])
                                {
                                    // Update the setting
                                    dbAssoc["AssociateType"] = newAssoc;
                                }

                                break;
                            }
                        }

                        // New association. Add it.
                        if (rowExists == false && newAssoc != E_TriState.Blank)
                        {
                            DataRow newRow = dbAssocs.NewRow();
                            newRow["LpePriceGroupId"] = priceGroupId;
                            newRow["ProgId"] = programId;
                            newRow["PricePolicyId"] = policyId;
                            newRow["AssociateType"] = newAssoc;

                            dbAssocs.Rows.Add(newRow);
                        }
                    }
                }

                ProgramInPriceGroupAssocs.Save();
            }
        }

        private void PrepareDbForProgramInPriceGroupAssociation( string programList, string inClause, List<SqlParameter> listParams, Guid PriceGroupId )
		{
            // 05/21/08 mf. OPM 21061.  Lpe calc/loading needs all Program in Price Group
            // attachments to have a row in LPE_PRICE_GROUP_PRODUCT.  We check that the row
            // is present, and if not, we create it with the default values of hidden and 0 rates.			
            var copyParams = new List<SqlParameter>();
            foreach (var param in listParams)
            {
                copyParams.Add(DbAccessUtils.CopySqlParameter(param));
            }

            copyParams.Add(new SqlParameter("@pgID", PriceGroupId));

            string sql = "SELECT * FROM Lpe_Price_Group_Product WHERE LpePriceGroupId = @pgID AND lLpTemplateId " + inClause;

            using (CDataSet PriceGroupProduct = new CDataSet())
            {
                PriceGroupProduct.LoadWithSqlQueryDangerously(this.BrokerId, sql, copyParams);
                DataTable PriceGroupProducts = PriceGroupProduct.Tables[0];

				ArrayList ProgramsToInsert = new ArrayList(programList.Split(','));

                if (ProgramsToInsert.Count == PriceGroupProducts.Rows.Count)
                    return; // Because of unique constraint, equal length means the list is the same -> all rows exist
                
                foreach (DataRow row in PriceGroupProducts.Rows)
                {
                    var rowTemplateId = row["lLpTemplateId"].ToString();
                    if (ProgramsToInsert.Contains(rowTemplateId))
					{
						ProgramsToInsert.Remove(rowTemplateId);
					}
				}

				foreach (var programId in ProgramsToInsert)
                {
                    DataRow newRow = PriceGroupProducts.NewRow();
                    newRow["LpePriceGroupId"] = PriceGroupId;
                    newRow["lLpTemplateId"] = programId;
                    newRow["LenderProfitMargin"] = 0;
                    newRow["LenderMaxYspAdjustLckd"] = 0;
                    newRow["LenderMaxYspAdjust"] = 0;
                    newRow["LenderRateMargin"] = 0;
                    newRow["IsValid"] = 0;
                    newRow["LenderMarginMargin"] = 0;
                    PriceGroupProducts.Rows.Add(newRow);
                }
                PriceGroupProduct.Save();
            }
        }

        private void SaveProgramPmiAssociations()
        {
            Guid pmiProductID = GetGuid("PmiProductID");
            string assocList = GetString("Value");

            List<string> folderContents = new List<string>();
            ListPmiProductsInFolderByMasterId(pmiProductID, folderContents);

            // Get current associations with this PriceGroup Master
            using (CDataSet PmiAssociations = new CDataSet())
            {
                FillPmiAssociations(pmiProductID, PmiAssociations);
                DataTable dbAssocs = PmiAssociations.Tables[0];

                // Get the new associations to be saved from client UI
                string[] policies = assocList.Split(',');
                Hashtable clientAssocs = new Hashtable(policies.Length);
                foreach (string policy in policies)
                {
                    String[] parameters = policy.Split(':');

                    if (parameters.Length != 2)
                    {
                        continue;
                    }

                    clientAssocs.Add(new Guid(parameters[0]), parameters[1]);
                }

                // Delete removed associations (Assocs that went from Y/N ->[Blank] at client).
                for (int i = 0; i < dbAssocs.Rows.Count; i++)
                {
                    if (!clientAssocs.Contains(dbAssocs.Rows[i]["PricePolicyId"]))
                    {
                        if (dbAssocs.Rows[i]["PmiProductId"].ToString().Equals(pmiProductID.ToString()))
                        {
                            if ((bool)dbAssocs.Rows[i]["InheritVal"] == false)
                            {
                                dbAssocs.Rows[i].Delete();
                            }
                            else
                            {
                                dbAssocs.Rows[i]["AssocOverrideTri"] = E_TriState.Blank;
                            }
                        }
                        else // Master was just deleted
                        {
                            if ((E_TriState)(byte)dbAssocs.Rows[i]["AssocOverrideTri"] == E_TriState.Blank)
                            {
                                dbAssocs.Rows[i].Delete();
                            }
                            else
                            {
                                dbAssocs.Rows[i]["InheritVal"] = false;
                            }
                        }
                    }
                }

                // Update changed existing associations and add the new ones to the set
                foreach (Guid policyId in clientAssocs.Keys)
                {
                    E_TriState newAssoc;
                    switch (clientAssocs[policyId].ToString())
                    {
                        case "Y":
                            newAssoc = E_TriState.Yes;
                            break;
                        case "N":
                            newAssoc = E_TriState.No;
                            break;
                        default:
                            Tools.LogBug("Unhandled association type: " + clientAssocs[policyId].ToString());
                            continue;
                    }
                    bool inheritVal = newAssoc == E_TriState.Yes;

                    // Update or create the row
                    List<string> folderContentsStorage = new List<string>();
                    foreach (DataRow dbAssoc in dbAssocs.Rows)
                    {
                        if (dbAssoc.RowState == DataRowState.Deleted)
                            continue;

                        if (policyId == (Guid)dbAssoc["PricePolicyId"])
                        {
                            string entryPmiProductId = dbAssoc["PmiProductId"].ToString();
                            if (!folderContents.Contains(entryPmiProductId))
                                continue;

                            folderContents.Remove(entryPmiProductId);
                            folderContentsStorage.Add(entryPmiProductId);

                            if (entryPmiProductId.Equals(pmiProductID.ToString()))
                            {
                                if (newAssoc != (E_TriState)(byte)dbAssoc["AssocOverrideTri"])
                                {
                                    // Update the setting
                                    dbAssoc["AssocOverrideTri"] = newAssoc;
                                }
                            }
                            else // Master was edited so update the slaves
                            {
                                if (inheritVal != (bool)dbAssoc["InheritVal"])
                                {
                                    if (inheritVal == false && (E_TriState)(byte)dbAssoc["AssocOverrideTri"] == E_TriState.Blank)
                                    {
                                        dbAssoc.Delete();
                                    }
                                    else
                                    {
                                        dbAssoc["InheritVal"] = inheritVal;
                                    }
                                }
                            }

                            if (folderContents.Count == 0)
                                break;
                        }
                    }

                    // New association. Add it.
                    for (int i = 0; i < folderContents.Count; i++)
                    {
                        string entryPmiProductId = folderContents[i];
                        DataRow newRow = dbAssocs.NewRow();
                        newRow["PmiProductId"] = entryPmiProductId;
                        newRow["PricePolicyId"] = policyId;

                        if (entryPmiProductId.Equals(pmiProductID.ToString()))
                        {
                            newRow["InheritVal"] = false;
                            newRow["AssocOverrideTri"] = newAssoc;
                            dbAssocs.Rows.Add(newRow);
                        }
                        else // Master was editted so update the slaves
                        {
                            if (inheritVal)
                            {
                                newRow["InheritVal"] = inheritVal;
                                newRow["AssocOverrideTri"] = E_TriState.Blank;
                                dbAssocs.Rows.Add(newRow);
                            }
                        }

                        folderContentsStorage.Add(entryPmiProductId);
                    }

                    folderContents = folderContentsStorage;
                }

                PmiAssociations.Save();
            }
        }

        public static void FillPmiAssociations(Guid pmiProductId, CDataSet pmiAssociations)
        {
            var listParams = new SqlParameter[] { new SqlParameter("@pmiProductId", pmiProductId) };
            string sql = "exec ListPmiPolicyAssociationByPmiProductId @pmiProductId";

            pmiAssociations.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, sql, listParams);
        }

        public static void ListPmiProductsInFolderByMasterId(Guid pmiProductId, List<string> folderContents)
        {
            SqlParameter[] param = { new SqlParameter("@PmiProductId", pmiProductId) };
            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListPmiProductsInFolderByMasterId", param))
            {
                while (dr.Read())
                {
                    folderContents.Add(dr["ProductId"].ToString());
                }
            }
        }

        private void SaveProgramAssociations() 
		{
            Guid productId = GetGuid("ProductId");
            string value = GetString("Value");
            bool turnOptimizerOffChk = GetBool("TurnOptimizerOffChk");
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;

            // Get the current database snapshot of tree expansion state, as
            // well as association intent, and compare against the client's
            // latest changes.  We only update the database with the delta.
            DataSet ds = new DataSet();
            SqlParameter[] sqlParameters = {
                                                new SqlParameter( "@LoanProductId" , productId)
                                            };

            DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByLoanProductId", sqlParameters);

            if (ds.Tables.Count < 1)
            {
                throw new CBaseException(ErrorMessages.FailedToSave, "Unable to get broker's price policies or their associations.");
            }

            // Build up the current state in a lookup table.  We will walk
            // the client state and compare.

            Hashtable assocSettings = new Hashtable();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                // Build up our lookup tables for diffing
                // against the database.

                assocSettings.Add(row["PricePolicyId"], E_UiPolicyProgramAssociation.Blank);
            }
            foreach (String policy in value.Split(','))
            {
                // Cut up the policy override setting into its
                // 2 components: id and override.

                String[] parameters = policy.Split(':');

                if (parameters.Length != 2)
                {
                    continue;
                }

                // Store this override in the tristate pools so
                // the comparer has a complete snapshot of the
                // user's intent.

                try
                {
                    // Get the policy id and check if its a member
                    // of the product's set per broker.  Once found
                    // we overwrite the blank state we initialized
                    // each policy group with in favor of the user's
                    // override setting.

                    Guid id = new Guid(parameters[0]);

                    if (assocSettings.Contains(id) == false)
                    {
                        Tools.LogError("A price policy id was cached in the client state for update, but isn't in the database set for current loan product.");

                        continue;
                    }

                    switch (parameters[1])
                    {
                        case "Y":
                            assocSettings[id] = E_UiPolicyProgramAssociation.Yes;
                            break;
                        case "N":
                            assocSettings[id] = E_UiPolicyProgramAssociation.No;
                            break;
                        case "FN": // Should only ever get this from Masters
                            assocSettings[id] = E_UiPolicyProgramAssociation.ForceNo;
                            break;
                    }
                }
                catch (Exception e)
                {
                    // We record the error here, but don't stop
                    // collecting information.

                    Tools.LogError(e);
                }
            }

            SaveProgramAssociations(brokerId, productId, assocSettings, turnOptimizerOffChk);
        }

        private static void SaveProgramAssociations(Guid brokerId, Guid productId, Hashtable assocSettings, bool turnOptimizerOffChk)
        {
            // Now invoke the update.  The calculator diffs against
            // the database and calculates how to update the cached
            // entries for product to price policy associations.

            CAssocInheritUpdate updator = CAssocInheritUpdate.GetUpdatorForExistingMasterAfterChangeInUi(productId, brokerId, assocSettings);
            updator.UpdateAndSpread(turnOptimizerOffChk);

            // 2/11/2005 kb - Also call the inheritance-based cache
            // updater.
            //
            // 2/14/2005 kb - To invoke the right cache updater event,
            // we need to know a little more than we currently do
            // about our loan product.  For now, we just force a save
            // of the associated loan product so that proper cache
            // updating kicks in.

            CLoanProductData prod = new CLoanProductData(productId);

            prod.InitSave();

            prod.Save();
		}

        internal enum ErrorCode
        {
            None,
            ForceNo,
            General
        }

        // This method simulates the user clicking the checkbox yes/no on PricePolicyListByProduct.aspx and clicking the button "Save"
        // We need optimize this code later.
        // Potential exceptions: 
        //      SqlException/IOException for StoredProcedureHelper.ExecuteReader 
        //      CBaseException for SaveProgramAssociations(...)
        internal static ErrorCode SetProgramAssociation(Guid brokerId, Guid productId, Guid policyId, E_UiPolicyProgramAssociation newAssoc, bool turnOptimizerOffChk, out string errString)
        {
            ForceNoManager fnm = new ForceNoManager(brokerId);

            Guid FNProgramId = fnm.IsFnAssoc(policyId, productId);
            if (FNProgramId != Guid.Empty)
            {
                errString = "Disassociated via master";
                return ErrorCode.ForceNo;
            }

            errString = string.Empty;

            // is master product?
            bool isMaster = false;
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrieveLpIsMaster", new SqlParameter("@lLpTemplateId", productId)))
            {
                if (reader.Read())
                {
                    isMaster = (bool)reader["isMaster"];
                }
            }

            // load all global assocations between product and policies
            Hashtable assocSettings = new Hashtable();
            SqlParameter[] parameters = {
                                            new SqlParameter( "@LoanProductID" , productId )
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListPricePolicyByLoanProductId", parameters))
            {
                while (reader.Read())
                {
                    assocSettings.Add(reader["PricePolicyId"], E_UiPolicyProgramAssociation.Blank);
                    if ((bool)reader["IsPublished"] == false || reader["AssocOverrideTri"] is DBNull)
                    {
                        continue;
                    }

                    var assocVal = E_UiPolicyProgramAssociation.Blank;

                    switch ((E_TriState)(Byte)reader["AssocOverrideTri"])
                    {
                    case E_TriState.Yes:
                        assocVal = E_UiPolicyProgramAssociation.Yes;
                        break;

                    case E_TriState.No:
                        assocVal = E_UiPolicyProgramAssociation.No;
                        break;
                    }

                    if (isMaster)
                    {
                        if ((bool)reader["IsForcedNo"] == true)
                        {
                            assocVal = E_UiPolicyProgramAssociation.ForceNo;
                        }
                    }

                    assocSettings[reader["PricePolicyId"]] = assocVal;
                }
            }

            assocSettings[policyId] = newAssoc;
            SaveProgramAssociations(brokerId, productId, assocSettings, turnOptimizerOffChk);
            
            return ErrorCode.None;
        }
    }
}
