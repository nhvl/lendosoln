namespace LendersOfficeApp.los.RatePrice.LPETools
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.SessionState;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using DataAccess;
    using ExcelLibraryClient;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOfficeApp.ObjLib.RulesManagement;

    public partial class SchemaValidator : BasePage
	{
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void btnSubmit_Click(object sender, System.EventArgs e)
		{
			ValidateSchema() ;

			// force EXCEL.EXE to be cleaned up
			GC.Collect() ;
			GC.WaitForPendingFinalizers() ;
		}
		private void ValidateSchema()
		{
			string schemaFileName = DataAccess.TempFileUtils.NewTempFilePath() ;
			string dataFileName = DataAccess.TempFileUtils.NewTempFilePath() ;
			string compactedSchemaFileName = DataAccess.TempFileUtils.NewTempFilePath() ;

			try
			{
				if (m_schemaFile.PostedFile == null || m_schemaFile.PostedFile.ContentLength == 0)
                    throw new CBaseException(ErrorMessages.SchemaFileMissing, ErrorMessages.SchemaFileMissing);
				if (m_dataFile.PostedFile == null || m_dataFile.PostedFile.ContentLength == 0)
                    throw new CBaseException(ErrorMessages.DataFileMissing, ErrorMessages.DataFileMissing);

				m_schemaFile.PostedFile.SaveAs(schemaFileName) ;
				m_dataFile.PostedFile.SaveAs(dataFileName) ;

                ExcelLibrary.ConvertToCompactFormat(schemaFileName, compactedSchemaFileName);
                string errorMessage = string.Empty;

                if (ExcelLibrary.ValidateCompactSchemaFile(compactedSchemaFileName, dataFileName, out errorMessage))
                {
                    txtOutput.Text = "Validation OK";
                }
                else
                {
                    txtOutput.Text = errorMessage;
                }
                //using (ExcelApp app = new ExcelApp(((string)(System.AppDomain.CurrentDomain.BaseDirectory + "ObjLib/RulesManagement/DisableEvents.xls")).Replace("/", "\\")))
                //{
                //    // compact the schema
                //    using (WorkBook wkb = app.OpenWorkbook(schemaFileName))
                //    {
                //        SchemaFile.CompactSchema(wkb, compactedSchemaFileName) ;
                //    }

                //    // open the compacted schema file
                //    using (SchemaFile schemaFile = new SchemaFile(compactedSchemaFileName))
                //    {
                //        // open the data file
                //        using (WorkBook wkb = app.OpenWorkbook(dataFileName))
                //        {
                //            if (schemaFile.Validate(wkb))
                //                txtOutput.Text = "Validation OK" ;
                //            else
                //                txtOutput.Text = schemaFile.ValidateFailureReason ;
                //        }
                //    }
                //}
			}
            catch (CBaseException ex1)
            {
                CommonLib.Tracer.Trace("ERR", ex1.ToString());

                txtOutput.Text = ex1.UserMessage;
            }
            catch (Exception ex2)
            {
                CommonLib.Tracer.Trace("ERR", ex2.ToString());

                txtOutput.Text = ex2.ToString();
            }
			finally
			{
				if (FileOperationHelper.Exists(schemaFileName)) FileOperationHelper.Delete(schemaFileName) ;
				if (FileOperationHelper.Exists(dataFileName)) FileOperationHelper.Delete(dataFileName) ;
				if (FileOperationHelper.Exists(compactedSchemaFileName)) FileOperationHelper.Delete(compactedSchemaFileName) ;

				// force EXCEL.EXE to be cleaned up
				GC.Collect() ;
				GC.WaitForPendingFinalizers() ;
			}
		}
	}
}
