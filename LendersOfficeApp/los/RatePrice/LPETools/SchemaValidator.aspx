<%@ Page language="c#" Codebehind="SchemaValidator.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.LPETools.SchemaValidator" EnableSessionState="True"%>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>SchemaValidator</title>
		<LINK href="../../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="SchemaValidator" method="post" runat="server">
			<TABLE class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD class="FormTableHeader">Schema Validator</TD>
					</TR>
					<TR>
						<TD>
							<table>
								<tr>
									<td>Schema File</td>
									<td><input type="file" name="m_schemaFile" size="50" runat="server" id="m_schemaFile"></td>
								</tr>
								<tr>
									<td>Data File</td>
									<td><input type="file" name="m_dataFile" size="50" runat="server" id="m_dataFile"></td>
								</tr>
								<tr>
									<td colspan="2" align="right">
										<ml:NoDoubleClickButton id="btnSubmit" runat="server" Text="Validate" onclick="btnSubmit_Click"></ml:NoDoubleClickButton>&nbsp;
										<input type="button" onclick="onClosePopup()" value="Close"></td>
								</tr>
							</table>
						</TD>
					</TR>
					<tr>
						<td><i>Due to security reasons, the browser will not store the path for schema or data 
								file. Therefore you will have to re-select it each time. You could also hit F5 
								to refresh and trick the browser into uploading the same data files.</i></td>
					</tr>
					<TR>
						<TD class="FormTableHeader">Results</TD>
					</TR>
					<tr>
						<td>
							<asp:TextBox id="txtOutput" runat="server" TextMode="MultiLine" Width="100%" Rows="30"></asp:TextBox>
						</td>
					</tr>
				</TBODY></TABLE>
		</form>
	</body>
</HTML>
