<%@ Page language="c#" Codebehind="PricePolicy.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.PricePolicy" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!doctype html>
<html>
<head runat="server">
	<title>PricePolicy</title>
	<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body scroll="yes" onload="init();">
	<script type="text/javascript">
	function init()
      {
		<% if( IsPostBack == false ) { %>
          resize(<%= AspxTools.JsNumeric(m_attachable ? 900 : 550 )%>, 500);
		<% } %>
          
		<% if( m_isNewRule ) { %>
			addRule();
          <% } %>

        <% if (!m_attachable) { %>
          document.getElementById("attachTable").style.display = 'none';
		<% } %>

        document.getElementById("attachBtn").disabled = true;

        if (document.getElementById("m_errorMessage") != null)
		{
		    alert(document.getElementById("m_errorMessage").value);
		}
      }
      function addRule() {
          showModal("/los/RatePrice/RulesEdit.aspx?cmd=add&PricePolicyId=<%=AspxTools.JsStringUnquoted(m_newPricePolicyID.ToString())%>", null, null, null, function(){
			  self.location = "PricePolicy.aspx?PricePolicyId=<%=AspxTools.JsStringUnquoted(m_newPricePolicyID.ToString())%>";
		  });
	  }
	  
      function closeMe() {
        var args = window.dialogArguments || {};
        var regex = "/'/gi";
        if (document.getElementById("hiddenOK") != null)
        {
            if (document.getElementById("hiddenOK").value == 'true')
			{
				args.OK = true;
	        }
		}
		else
		{
			args.OK = false;
        }
        onClosePopup(args);
      }      
      <% if (m_cmd != "add") { %>

      function editRule(ruleId) {
          showModal("/los/RatePrice/RulesEdit.aspx?ruleid=" + ruleId + "&PricePolicyId=<%=AspxTools.JsStringUnquoted(PricePolicyId.ToString())%>", null, null, null, function(args){ 
			  if( args != null && args.OK != null && args.OK == true )
			  {
				self.location = self.location;
			  }
		  });
      }

      function onBatchAttach()
      {
          var url = <%=AspxTools.SafeUrl("/los/RatePrice/BatchAttach.aspx?PricePolicyId=" + PricePolicyId + "&isProgram=")%> + document.getElementById("attachType").value;
          callWebMethodAsync({
              type: 'POST',
              contentType: 'application/json; charset=utf-8',
              url: 'PricePolicy.aspx/CacheSelectedGuids',
              data: JSON.stringify({'guidList': document.getElementById("prgIds").value}),
              dataType: 'json',
              async: false
          }).then(
			function(d) {
                showModal(url+'&s=' + d.d, null, 'dialogWidth:880px;dialogHeight:700px;center:yes;resizable:yes;scroll:yes;status=yes;help=no;');
            },
            function (xhr, status, error) {
            	var exception = JSON.parse(xhr.responseText);
                alert(exception.Message);
            }
		  );


	  }

      <% } %>

      function endisableAttachBtn()
      {
        var tb = document.getElementById("prgIds");
        var btn = document.getElementById("attachBtn");
        btn.disabled = (tb.value.length == 0);
      }
     
	</script>
        <h4 class="page-header">Rule Group</h4>
		<form id="PricePolicy" method="post" runat="server">
			<TABLE class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD>
							<TABLE id="Table2" cellSpacing="0" cellPadding="0" border="0" style="float:left">
								<tr>
									<td class="FieldLabel">Policy Id</td>
									<td>
										<asp:TextBox id="m_policyIdTF" runat="server" Width="409px" ReadOnly="True"></asp:TextBox></td>
								</tr>
								<TR>
									<TD class="FieldLabel">Description&nbsp;&nbsp;
									</TD>
									<TD width="80%"><asp:textbox id="m_descriptionTF" runat="server" Width="409px"></asp:textbox><IMG src="../../images/require_icon.gif">
                                        <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="m_descriptionTF">
                                            <img runat="server" src="../../images/error_icon.gif">
                                        </asp:requiredfieldvalidator></TD>
								</TR>
								<TR>
									<TD class="FieldLabel" vAlign="top">Notes&nbsp;&nbsp;
									</TD>
									<TD><asp:textbox id="m_notesTF" runat="server" Width="408px" Height="118px" TextMode="MultiLine"></asp:textbox></TD>
								</TR>
								<TR>
									<TD class="FieldLabel" vAlign="top" align="middle" colSpan="2">
										<P align="left"><asp:checkbox id="m_isPublished_chk" runat="server" Text="Loan Products Can Attach To This Rule Group"></asp:checkbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<asp:checkbox id="m_hasQRule_chk" runat="server" Text="Has Q Rule" Enabled="False"></asp:checkbox></P>
									</TD>
								</TR>
								<tr>
									<td colspan="2" align="left">
										<ml:EncodedLabel CssClass="FieldLabel" ID="SortedIdLabel" Runat="server">Mutual Exclusive Branch Execution Ordered Id: </ml:EncodedLabel>
										<asp:TextBox ID="MutualExclusiveExecSortedId_ed" MaxLength="36" Runat="server" Width="180px"></asp:TextBox>
									</td>
								</tr>
								<TR>
									<TD class="FieldLabel" vAlign="top" align="middle" colSpan="2">
										<span>
											<ml:NoDoubleClickButton id="m_okBtn" runat="server" style="DISPLAY: none" Text="OK" onclick="onOKClick"></ml:NoDoubleClickButton>
											<input type="submit" value="  OK  " onclick="parentElement.children[ 0 ].click(); disabled = true;">
										</span><INPUT onclick="closeMe();" type="button" value="Cancel">
										<ml:NoDoubleClickButton id="m_applyBtn" runat="server" Text="Apply" onclick="onApplyClick"></ml:NoDoubleClickButton>
									</TD>
								</TR>
							</TABLE>

                             <TABLE id="attachTable" cellSpacing="2" cellPadding="0" border="0" style="float:left;border: 0px solid gray;">
                                <tr>
									<td class="FieldLabel">&nbsp;</td>
								</tr>
                                <tr>
									<td class="FieldLabel">Please enter attached <select id="attachType">
                                        <option value="1">program IDs</option>
                                        <option value="0">price group IDs</option>
                                    </select></td>
								</tr>
                                <tr>
				                    <td>
                                        <textarea id="prgIds" name="prgIds" onkeyup="endisableAttachBtn();" onmouseup="endisableAttachBtn();" cols="20" rows="2" style="width: 300px; height: 118px;"></textarea>
                                    </td>

								</tr>
                                <tr>
									<td>
                                        <input id="attachBtn" type="button" value="Try To Batch Attach" onclick="onBatchAttach();" />&nbsp;</td>
								</tr>

							</TABLE>

						</TD>

					</TR>
					<TR>
						<TD class="FormTableHeader1">Pricing Rules</TD>
					</TR>
					<TR>
						<TD><asp:button id="m_addBtn" runat="server" Text="Add new rule" onclick="onAddClick"></asp:button>
						</TD>
					</TR>
					<TR>
						<TD><asp:datagrid id="m_rulesDG" runat="server" Width="100%" DataKeyField="RuleID" CssClass="DataGrid" AutoGenerateColumns="False">
								<AlternatingItemStyle CssClass="GridAlternatingItem"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem"></ItemStyle>
								<HeaderStyle CssClass="GridHeader"></HeaderStyle>
								<Columns>
								    <asp:TemplateColumn HeaderText="QBC" ItemStyle-HorizontalAlign="Center"></asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Skip"></asp:TemplateColumn>
									<asp:TemplateColumn></asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description" ItemStyle-Width="100%"></asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Disq"></asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Fee" ItemStyle-Wrap="false"></asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Margin"></asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rate"></asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Stipltn"></asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="BMargin"></asp:TemplateColumn>
									<asp:ButtonColumn ButtonType="LinkButton" Text="delete"></asp:ButtonColumn>
								</Columns>
							</asp:datagrid></TD>
					</TR>
					<tr>
						<td></td>
					</tr>
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>
