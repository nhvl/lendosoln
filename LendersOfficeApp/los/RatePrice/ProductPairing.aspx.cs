using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;

namespace LendersOfficeApp.los.RatePrice
{
	/// <summary>
	/// Allow product level pairing. 07/19/07 mf. OPM 17097.
	/// </summary>
	public partial class ProductPairing : LendersOffice.Common.BasePage
	{
		protected bool IsExistingPairings = true; // To properly render UI


		// User can click this link from template editor in 2nds.
		// From the global link, we allow user to choose 2nd Product.
		private string SecondInvestor
		{
			get 
			{
				return IsAllowSecondChoice ? SecondInvestorDdl.SelectedItem.Value : RequestHelper.GetSafeQueryString("investor");
			}
		}
		private string SecondProdCode
		{
			get 
			{ 
				return IsAllowSecondChoice ? SecondProductDdl.SelectedItem.Value : RequestHelper.GetSafeQueryString("prodCode"); 
			}
		}
		private bool IsAllowSecondChoice
		{
			get 
			{ 
				return ( RequestHelper.GetSafeQueryString("prodCode") == null || RequestHelper.GetSafeQueryString("investor") == null);
			}
		}

		// Error that will make us close
		protected string ErrorMessage
		{
			set { ClientScript.RegisterHiddenField("renderError", value); }
		}
		
		// Non-Critical message
		protected string StatusMessage
		{
			set { ClientScript.RegisterHiddenField("userMessage", value); }
		}
		
		protected void PagePreRender(object sender, System.EventArgs e)
		{
			IsExistingPairings = ( PairingDG.Items.Count > 0 );
		}

		protected void PageLoad(object sender, System.EventArgs e)
		{
			if( !LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
				throw new CBaseException("You do not have access to this page.", "User without modify permission in ProductPairing.");

			if (IsAllowSecondChoice)
			{
				// We were not provided any 2nd prod information, we must provide UI
				// to collect it.
				SecondProdInputPanel.Visible = true;
			}
			if ( IsPostBack == false )
			{
				// Note that we will get here if the user chooses the 1st.
				// Set up Initial UI

				// Note that we do not care about product validity here;
				// We can pair with any firsts.  Pricing engine will be concerned with validity.
				// We grab everything here, because we are binding both investor list and
				// product list in one db hit.  If this gets too slow, we can break it up
				string sql = "SELECT InvestorName, ProductCode, PairingModeFor2ndLienProduct FROM Product";

                DataSet products = new DataSet();
                DBSelectUtility.FillDataSet(DataSrc.LpeSrc, products, sql, null, null);

                bool prodFound = false;
				
				// If we came from direct link in 2nd editor, we have the static second info in the query string,
				// otherwise the user would choose it from a drop down list.  For the ddls, we default
				// to the first in the list of both investor ddls and product code ddls.
				string currentSecondInvestor = IsAllowSecondChoice ? products.Tables[0].Rows[0]["InvestorName"].ToString() : SecondInvestor;
				string currentSecondProdCode = IsAllowSecondChoice ? products.Tables[0].Rows[0]["ProductCode"].ToString() : SecondProdCode;

				foreach (DataRow productRow in products.Tables[0].Rows)
				{
					if ( productRow["InvestorName"].ToString() == currentSecondInvestor )
					{	
						FirstProductDdl.Items.Add(new ListItem( productRow[ "ProductCode"].ToString() ) );
						SecondProductDdl.Items.Add(new ListItem( productRow[ "ProductCode"].ToString() ) );

						if ( productRow["ProductCode"].ToString() == currentSecondProdCode )
						{
							prodFound = true;
							SetPairingMode( productRow["PairingModeFor2ndLienProduct"].ToString() );
						}
					}
					ListItem InvestorItem = new ListItem( productRow["InvestorName"].ToString() );
					
					if ( !FirstInvestorDdl.Items.Contains( InvestorItem ) )
					{
						FirstInvestorDdl.Items.Add(InvestorItem);
						SecondInvestorDdl.Items.Add(InvestorItem);

						if ( currentSecondInvestor == InvestorItem.Text )
						{
							FirstInvestorDdl.SelectedIndex = FirstInvestorDdl.Items.Count - 1;
							SecondInvestorDdl.SelectedIndex = SecondInvestorDdl.Items.Count - 1;
						}
					}
				}
				if (!prodFound)
				{
					ErrorMessage = "Product Not Found.";
					return;
				}

				SetSecondProductLabel( SecondInvestor, SecondProdCode );
				
				BindPairingGrid();
			}
			else
			{
				// We do not cache Investors' programs on client, so we need to catch
				// postbacks from changing investor dropdowns.  We may want to consider
				// this option in the future.
				if( Request.Form[ "__EVENTTARGET" ] == FirstInvestorDdl.ClientID )
				{
					// Changed the first they will want to pair with-> bind this investor's prods
					BindProdCodeDdlForInvestor( FirstProductDdl, FirstInvestorDdl.SelectedItem.Text );
				}
				else if( Request.Form[ "__EVENTTARGET" ] == SecondInvestorDdl.ClientID )
				{
					// User chose a new 2nd Investor-> Bind their prods, set the labels, bind pairing mode and grid
					BindProdCodeDdlForInvestor( SecondProductDdl, SecondInvestorDdl.SelectedItem.Text );
					SetSecondProductLabel( SecondInvestor, SecondProdCode );
					BindPairingMode();
				}
				else if( Request.Form[ "__EVENTTARGET" ] == SecondProductDdl.ClientID )
				{
					// User chose a new second product-> Set labels, pairing mode and pairing grid.
					SetSecondProductLabel( null, SecondProdCode );
					BindPairingMode();
				}
				
				BindPairingGrid();
			}
		}

		private void SetSecondProductLabel( string secondInvestor, string secondProduct )
		{
			if ( secondInvestor != null )
			{
				InvestorLtl.Text = secondInvestor + "::";
			}
			if ( secondProduct != null )
			{
				ProductCodeLtl.Text = secondProduct;
			}
		}

		private void BindProdCodeDdlForInvestor(DropDownList prodCodeDdl, string investor)
		{
            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@p", investor) };
            string sql = "SELECT ProductCode FROM Product WHERE InvestorName = @p";

            DataSet products = new DataSet();
            DBSelectUtility.FillDataSet(DataSrc.LpeSrc, products, sql, null, listParams);

			prodCodeDdl.Items.Clear();
			foreach (DataRow productRow in products.Tables[0].Rows)
			{
				prodCodeDdl.Items.Add(new ListItem( productRow[ "ProductCode"].ToString() ) );
			}
		}

		private void BindPairingGrid()
		{
            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@p0", SecondInvestor), DbAccessUtils.SqlParameterForVarchar("@p1", SecondProdCode) };
            string sql = "SELECT * FROM Product_Pairing WHERE InvestorName2ndLien = @p0 AND ProductCode2ndLien = @p1";

            DataSet pairings = new DataSet();
            DBSelectUtility.FillDataSet(DataSrc.LpeSrc, pairings, sql, null, listParams);

			PairingDG.DataSource = pairings.Tables[0].DefaultView;
			PairingDG.DataBind();
		}

		private void BindPairingMode()
		{
            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@p0", SecondInvestor), DbAccessUtils.SqlParameterForVarchar("@p1", SecondProdCode) };
            string sql = "SELECT InvestorName, ProductCode, PairingModeFor2ndLienProduct FROM Product WHERE InvestorName = @p0 AND ProductCode = @p1";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    SetPairingMode((string)reader["PairingModeFor2ndLienProduct"]);
                }
                else
                {
                    // We have to throw here. We do not we cannot load UI without correct pairing
                    throw new CBaseException("Could not find product", "Could not find product");
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql, null, listParams, readHandler);
		}

		private void SetPairingMode(string pairMode)
		{
			switch( pairMode )
			{
				case "":
					PairModeRbl.SelectedIndex = 0;
					SavedPairingMode.Value="0";
					break;
				case "include":
					PairModeRbl.SelectedIndex = 1;
					SavedPairingMode.Value="1";
					break;
				case "exclude":
					PairModeRbl.SelectedIndex = 2;
					SavedPairingMode.Value="2";
					break;
				default:
					Tools.LogBug("Unknown value of PairingModeFor2ndLienProduct: " + pairMode );
					break;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		// Update the new pairing mode
		protected void OnUpdatePairingModeClick( object sender , System.EventArgs a )
		{
			string newMode = "";
			switch( PairModeRbl.SelectedIndex )
			{
				case 0:
					newMode = "";
					SavedPairingMode.Value="0";
					break;
				case 1:
					newMode = "include";
					SavedPairingMode.Value="1";
					break;
				case 2:
					newMode = "exclude";
					SavedPairingMode.Value="2";
					break;
			}

            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@mode", newMode),
                                                    DbAccessUtils.SqlParameterForVarchar("@name", SecondInvestor),
                                                    DbAccessUtils.SqlParameterForVarchar("@code", SecondProdCode) };

			String sql = "UPDATE Product SET PairingModeFor2ndLienProduct = @mode WHERE InvestorName = @name AND ProductCode = @code";

			Toolbox.RatePrice.Invalidate();
            DBUpdateUtility.Update(DataSrc.LpeSrc, sql, null, listParams);
		}
		
		// Add new pairing
		protected void OnAddPairingClick( object sender , System.EventArgs a )
		{
            var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@p0", FirstInvestorDdl.SelectedItem.Text),
                                                DbAccessUtils.SqlParameterForVarchar("@p1", FirstProductDdl.SelectedItem.Text),
                                                DbAccessUtils.SqlParameterForVarchar("@p2", SecondInvestor),
                                                DbAccessUtils.SqlParameterForVarchar("@p3", SecondProdCode) };

            string sql = "INSERT INTO PRODUCT_PAIRING (InvestorName1stLien, ProductCode1stLien, InvestorName2ndLien, ProductCode2ndLien)  VALUES ( @p0, @p1, @p2, @p3 )";

			try 
			{
                DBInsertUtility.InsertNoKey(DataSrc.LpeSrc, sql, null, listParams);
			}
			catch (SqlException e)
			{
				// Could happen if pairing already exists.
				// We should give better error for this.
				Tools.LogError("Could not save pairing.", e);
				StatusMessage = "Could not add this pairing.";
			}
			
			Toolbox.RatePrice.Invalidate();

			BindPairingGrid();
		}
		
		
		protected void PairingItemCommand( Object sender, DataGridCommandEventArgs e )
		{
			// Remove this pairing
			if (e.CommandName == "remove")
			{
                var listParams = new SqlParameter[] { DbAccessUtils.SqlParameterForVarchar("@p0", e.Item.Cells[0].Text.Replace("&nbsp;","")),
                                                DbAccessUtils.SqlParameterForVarchar("@p1", e.Item.Cells[1].Text.Replace("&nbsp;","")),
                                                DbAccessUtils.SqlParameterForVarchar("@p2", SecondInvestor),
                                                DbAccessUtils.SqlParameterForVarchar("@p3", SecondProdCode) };

                string sql = "DELETE FROM PRODUCT_PAIRING WHERE InvestorName1stLien = @p0 AND ProductCode1stLien = @p1 AND InvestorName2ndLien = @p2 AND ProductCode2ndLien = @p3";

                DBDeleteUtility.Delete(DataSrc.LpeSrc, sql, null, listParams);

				Toolbox.RatePrice.Invalidate();
				BindPairingGrid();
			}
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.PreRender += new System.EventHandler(this.PagePreRender);
		}
		#endregion
	}
}