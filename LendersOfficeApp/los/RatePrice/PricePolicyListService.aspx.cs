using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.LoanPrograms;
using LendersOffice.Security;

namespace LendersOfficeApp.los.RatePrice
{

    public partial class PricePolicyListService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        private BrokerUserPrincipal CurrentUser 
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }


        protected override void Process(string methodName) 
        {
            // 7/15/2004 kb - Block access if pricing engine enabled
            // and user lacks permission.

            if (!CurrentUser.HasPermission(Permission.CanModifyLoanPrograms)) 
            {
                throw new CBaseException(ErrorMessages.InsufficientPermissionToModifyLoanPrograms, "You do not have permission to modify loan programs.  Access denied.");
            }

            switch (methodName) 
            {
                case "Delete":
                    Delete();
                    break;
                case "Move":
                    Move();
                    break;
                case "Import":
                    Import();
                    break;
            }
        }

        private void Delete() 
        {
            try 
            {
                Guid pricePolicyId = GetGuid("PricePolicyId");
                ArrayList gather = new ArrayList();
                gather.Add(pricePolicyId);
                for( int i = 0 ; i < gather.Count ; ++i )
                {
                    // Get the current policy and look for any loan
                    // products that may be associated in this one.
                    // If found, punt by listing the name and failing.
                    // We only delete policies when all the children
                    // are unassociated.
                    DataSet ds = new DataSet();
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@PricePolicyId", gather[i])
                                                };

                    DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyChildrenInfo", parameters);

                    foreach( DataRow row in ds.Tables[ 0 ].Rows )
                    {
                        if( gather.Contains( row[ "PricePolicyId" ] ) == true )
                        {
                            continue;
                        }

                        gather.Add( row[ "PricePolicyId" ] );
                    }
                }

                for( int i = gather.Count ; i > 0 ; --i )
                {
                    // Delete this price policy.  Note that we remove in
                    // reverse.  The leaves need to go first, and they
                    // were gathered last.

                    try
                    {
                        StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "DeletePricePolicyAnyBroker", 0
                            , new SqlParameter( "@PricePolicyId" , gather[ i - 1 ]      )
                            );
                    }
                    catch (Exception exc)
                    {
                        CBaseException baseException = new CBaseException(ErrorMessages.UnableToDeletePricePolicyGroups, exc);
                        baseException.IsEmailDeveloper = false;
                        throw baseException;
                    }
                }

            } 
            catch (CBaseException) 
            {
                throw;
            } 
            catch (Exception exc) 
            {
                throw new CBaseException(exc.ToString(), exc);
            }
        }

        private void Move() 
        {
            try 
            {
                Guid sourceId = GetGuid("sourceId");
                Guid targetId = GetGuid("targetId");
                Guid parentId = targetId;

                while( parentId != Guid.Empty )
                {
                    // Lookup the policy to get its parent identifier.  If
                    // the parent id of this path ever crosses the source
                    // policy being moved, then we know we are placing
                    // the source under itself, which is a cycle, and is
                    // not allowed.

                    if( parentId == sourceId )
                    {
                        throw new CBaseException(ErrorMessages.MoveDenied, "Moving a parent under its child will create a cycle.  Move denied.");
                    }

                    CPricePolicy o = CPricePolicy.Retrieve(parentId);
                    parentId = o.ParentPolicyId;
                }

                // No failure, so execute the update and reconstruct the
                // tree during rendering.

                object updateId = targetId;

                if( targetId == Guid.Empty )
                {
                    updateId = DBNull.Value;
                }

                StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "SavePricePolicyNewParent", 0
                    , new SqlParameter( "@PricePolicyId"  , sourceId )
                    , new SqlParameter( "@ParentPolicyId" , updateId )
                    );

            }
            catch (CBaseException) 
            {
                throw;
            } 
            catch (Exception exc) 
            {
                throw new CBaseException(exc.ToString(), exc);
            }

        }

        private void Import() 
        {
            try 
            {
                Stream stream = GetBase64File("File");
                string feedBack = "";
                // Now commit all the updated price policy objects.
                // We presume that only changed entries propogate over
                // to the database.

                RateAdjustImport raImport = new RateAdjustImport();

                IEnumerable<string> errorList = null;
                if( !raImport.Import( stream, out errorList ) )
                {
                    feedBack = "Unable to import any policies: too many errors.";

                    
                    foreach( string eMsg in errorList )
                    {
                        feedBack += Environment.NewLine + eMsg;
                    }

                    throw new CBaseException(feedBack, feedBack);

                }

                feedBack = "All policies successfully imported: ";
                
            }
            catch (CBaseException) 
            {
                throw;
            } 
            catch (Exception exc) 
            {
                throw new CBaseException(exc.ToString(), exc);
            }
        }
	}
}