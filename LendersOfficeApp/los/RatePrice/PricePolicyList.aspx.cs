using System;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.ObjLib.DynaTree;
using System.Collections.Generic;

namespace LendersOfficeApp.los.RatePrice
{
    public partial class PricePolicyList : LendersOffice.Common.BaseServicePage
	{
        private Hashtable dynatreeHash = new Hashtable();

        protected System.Web.UI.WebControls.Button Button1;

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        protected void PageInit(object sender, System.EventArgs args) 
        {
            this.RegisterService("main", "/los/RatePrice/PricePolicyListService.aspx");
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;

            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            this.RegisterJsScript("dynatree/jquery.cookie.js");
            this.RegisterJsScript("dynatree/jquery.dynatree.js");
            this.RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");
        }
		/// <summary>
		/// Setup page for processing events and displaying latest.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

            RenderPricingRulesTree();
		}

        private DynaTreeNode GetDynaTreeNode(Guid key)
        {
            DynaTreeNode node = (DynaTreeNode)dynatreeHash[key];

            if (node == null)
            {
                node = new DynaTreeNode();
                dynatreeHash[key] = node;
            }

            return node;
        }

        private void RenderPricingRulesTree() 
        {            
            DynaTreeNode rootNode = new DynaTreeNode();
            rootNode.title = "<SPAN onclick=\"f_onclick(-1, event);\">All Rule Groups ( <A style=\"COLOR: tomato;\" onclick=\"f_add(-1); return false;\">add</A> )</SPAN>";
            rootNode.expand = true;
            rootNode.key = "root";
            rootNode.addClass = "no-icon";

            StringBuilder sb = new StringBuilder(2000);
            int counter = 0;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListPricePolicy")) 
            {
                while (reader.Read()) 
                {
                    Guid pricePolicyId = (Guid) reader["PricePolicyId"];
                    int pricePolicyIndex = counter;

                    if (counter == 0)
                        sb.AppendFormat("'{0}'", pricePolicyId.ToString("N"));
                    else
                        sb.AppendFormat(",'{0}'", pricePolicyId.ToString("N"));
                    counter++;                    

                    DynaTreeNode parentNode = rootNode; // Assume price policy is root node.
                    DynaTreeNode childNode = GetDynaTreeNode(pricePolicyId);                    

                    childNode.key = pricePolicyIndex.ToString();                    

                    childNode.title = String.Format
                        ("<SPAN onclick=\"f_onclick({1}, event);\">{0} ( <A style=\"COLOR:tomato;\" onclick=\"return f_edit({1});\">edit</A> <A style=\"COLOR:tomato;\" onclick=\"return f_add({1});\">add</A> )</SPAN>"
                        , Server.HtmlEncode((string)reader["PricePolicyDescription"])
                        , pricePolicyIndex
                        );

                    if ( (bool) reader["IsPublished"] == true ) 
                    {
                        childNode.icon = "../../../../images/published.gif";
                    } 
                    else
                    {
                        childNode.addClass = "no-icon";
                    }

                    if (reader["ParentPolicyId"] != DBNull.Value) 
                    {
                        parentNode = GetDynaTreeNode((Guid)reader["ParentPolicyId"]); // Search for price policy.                     
                    }

                    parentNode.children.Add(childNode);
                }
            }

            m_sourcedSelection.Value = "none";
            ClientScript.RegisterArrayDeclaration("g_ids", sb.ToString());

            RegisterJsObjectWithJsonNetSerializer("nodes", rootNode);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

	}
}
