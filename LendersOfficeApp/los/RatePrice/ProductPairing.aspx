<%@ Page language="c#" Codebehind="ProductPairing.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.ProductPairing" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>ProductPairing</title>
    <link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" bgColor=gainsboro scroll=no onload=onInit()>
  <script language=javascript>
		
	<!--      

	function onInit()
	{
		<% if( IsPostBack == false ) { %>
		resize( 790 , 570 );
		<% } %>
		if ( document.getElementById('userMessage') != null )
		{
		  alert(document.getElementById('userMessage').value);
		}

		if ( document.getElementById('renderError') != null )
		{
		  alert(document.getElementById('renderError').value);
		  document.getElementById('CloseBtn').click();
		}
		<% if ( IsExistingPairings ) { %>
		setBlankStatus ( false );
		<% } else { %>
		setBlankStatus ( true );
		<% } %>
		
		if ( document.getElementById('SavedPairingMode').value == '0')
		  <%= AspxTools.JsGetElementById(AddBtn) %>.disabled = true;
	}
	
   function setBlankStatus( bEnabled )
   {
     <%-- 
     // Quick and dirty way to access first radio button
     // of the RadioButtonList
     --%>
     var pairMode = <%= AspxTools.JsGetElementById(PairModeRbl) %>;
     var inputs = pairMode.getElementsByTagName('INPUT');
     var emptyDiv = document.getElementById('EmptyListDiv');
     var pairingGrid = document.getElementById('PairingGridDiv');
     emptyDiv.style.display = bEnabled ? 'block' : 'none';
     pairingGrid.style.display = bEnabled ? 'none' : 'block';
     
     if(inputs[0].checked && !bEnabled)
     {
       <%-- // If we are disabling a selected blank, change it to the saved value --%>
       inputs[ parseInt (document.getElementById('SavedPairingMode').value) ].checked = true;
     }
     inputs[0].disabled = !bEnabled;
   }

	//-->

		</script>
	<h4 class="page-header">Pairing setting for 2nd lien product <ml:EncodedLiteral id="InvestorLtl" Runat="server" /><ml:EncodedLiteral id="ProductCodeLtl" Runat="server" /></h4>
    <form id="ProductPairing" method="post" runat="server">
	<table class=FormTable cellSpacing=2 cellPadding=4 width="100%"	border=0>
	<tr>
	  <td class="FieldLabel">
	    <asp:Panel Visible="False" ID="SecondProdInputPanel" Runat="server">
   	      2nd Investor <asp:DropDownList ID="SecondInvestorDdl" Runat="server" AutoPostBack="True" style="PADDING-RIGHT: 32px" />&nbsp;
  	      2nd Product Code <asp:DropDownList ID="SecondProductDdl" Runat="server" AutoPostBack="True" />
	    </asp:Panel>
	  </td>
	</tr>
	<tr>
	  <td class="FieldLabel">
	  Pairing Mode
	  <asp:RadioButtonList id="PairModeRbl" Runat=server RepeatDirection="Horizontal" RepeatLayout="Flow"  >
	    <asp:ListItem Value="">Blank</asp:ListItem>
	    <asp:ListItem Value="include">Include</asp:ListItem>
	    <asp:ListItem Value="exclude">Exclude</asp:ListItem>
	  </asp:RadioButtonList>&nbsp;
	  <asp:Button ID="UpdateModeBtn" Runat=server OnClick=OnUpdatePairingModeClick Text="Save pairing mode" />
	  </td>
	</tr>
	<tr>
	  <td>
	    <div id="PairingGridDiv" style="OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 350px; BORDER-RIGHT: 2px; PADDING-RIGHT: 4px; BORDER-LEFT: lightgrey 2px solid; PADDING-LEFT: 4px; BORDER-TOP: lightgrey 2px solid; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; PADDING-BOTTOM: 4px;" valign="top">
	    <ml:CommonDataGrid Width="100%" ID="PairingDG" Runat="server" OnItemCommand="PairingItemCommand" AutoGenerateColumns="False">
	    <Columns>
	      <asp:BoundColumn DataField="InvestorName1stLien" SortExpression="InvestorName1stLien" HeaderText="First Lien Investor" />
	      <asp:BoundColumn DataField="ProductCode1stLien" SortExpression="ProductCode1stLien" HeaderText="First Lien Product" />
	      <asp:ButtonColumn ButtonType="LinkButton" Text="remove" CommandName="remove" />
	    </Columns>
	    </ml:CommonDataGrid>
	    </div>
	    <div id="EmptyListDiv" style="display: none; OVERFLOW-Y: scroll; WIDTH: 100%; HEIGHT: 350px; BORDER-RIGHT: 2px; PADDING-RIGHT: 4px; BORDER-LEFT: lightgrey 2px solid; PADDING-LEFT: 4px; BORDER-TOP: lightgrey 2px solid; PADDING-TOP: 4px; BORDER-BOTTOM: lightgrey 2px solid; PADDING-BOTTOM: 4px;">
	      <span style="TEXT-ALIGN: center; WIDTH: 100%" >Not paired with any first products</span>
	    </div>
	  </td>
	</tr>
	<tr>
	  <td class="FieldLabel" nowrap="nowrap">
	    1st Investor <asp:DropDownList ID="FirstInvestorDdl" Runat="server" AutoPostBack="True" />&nbsp;
	    1st Product Code <asp:DropDownList ID="FirstProductDdl" Runat="server" />&nbsp;
	    <asp:Button ID="AddBtn" Runat="server" OnClick="OnAddPairingClick" Text="Pair with this first product" />
	  </td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td align="center"><input type="button" id="CloseBtn" onclick="onClosePopup();" value="Close" /></td></tr>
	</table>
	<input type=hidden id="SavedPairingMode" runat=server value="0" NAME="SavedPairingMode"/>
   </form>
	<uc:cmodaldlg id=modalDlg runat="server"></uc:cmodaldlg>
  </body>
</html>
