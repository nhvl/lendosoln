﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BatchAttach.aspx.cs" Inherits="LendersOfficeApp.Los.RatePrice.BatchAttach" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Attach the policy to loan programs/price groups</title>
	<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
</head>
<body  onload=onInit();>
    <script language="javascript">
    <!--
          function onInit() {
              if (document.getElementById("cmdToDo") != null) {
                  if (document.getElementById("cmdToDo").value == "Close") {
                      onClosePopup();
                  }
              }

          }

		  function onViewClick(fileID)
		  {
			    showModal('/los/Template/LoanProgramView.aspx?fileid=' + fileID);
		  }
    //-->
	</script>

    <form id="BatchAttach" runat="server">
        <TABLE class="FormTable" id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
            <tr>
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Button ID="attachBtn" runat="server" Text="Attach" onclick="AttachBtnClick" OnClientClick="return confirm('Do you want to batch attach?');" />
                </asp:Panel>               
            </tr>
            <tr>
                &nbsp
            </tr>
			<tr>
				<td class="FormTableHeader">Batch Attach One Policy To <ml:EncodedLiteral id=attachContentLabel runat="server" Text="Loan Programs"></ml:EncodedLiteral></td>
			</tr>
            <tr>
                <td>
                    <asp:datagrid id="dgPrograms" runat="server" DataKeyField="lLpTemplateId"  EnableViewState="False" AutoGenerateColumns="False" CssClass="DataGrid" AllowSorting="True" Width="100%">
						<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
						<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
						<HeaderStyle CssClass="GridHeader"></HeaderStyle>
						<Columns>
							<asp:TemplateColumn HeaderText="Attachable">
								<ItemTemplate>
									 <%# AspxTools.HtmlControl(GenerateAttachableCheckbox(Container.DataItem)) %>
								</ItemTemplate>
							</asp:TemplateColumn>

							<asp:TemplateColumn>
								<ItemStyle HorizontalAlign="Center"></ItemStyle>
								<ItemTemplate>
									<%# AspxTools.HtmlControl(GenerateViewProgramLink(Container.DataItem)) %>
								</ItemTemplate>
							</asp:TemplateColumn>

							<asp:BoundColumn DataField="lLpTemplateId" HeaderText="Program ID" />
							<asp:BoundColumn DataField="lLpTemplateNm" HeaderText="Program Name" />
							<asp:BoundColumn DataField="Enabled" HeaderText="Enabled?" />
							<asp:BoundColumn DataField="Type" HeaderText="Type"></asp:BoundColumn>
						</Columns>
                    </asp:datagrid>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:datagrid id="dgPricegroups" runat="server" DataKeyField="LpePriceGroupId"  EnableViewState="False" AutoGenerateColumns="False" CssClass="DataGrid" AllowSorting="True" Width="100%">
						<AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
						<ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
						<HeaderStyle CssClass="GridHeader"></HeaderStyle>
						<Columns>
							<asp:TemplateColumn HeaderText="Attachable">
								<ItemTemplate>
									 <%# AspxTools.HtmlControl(GenerateAttachableCheckbox(Container.DataItem)) %>
								</ItemTemplate>
							</asp:TemplateColumn>

							<asp:BoundColumn DataField="LpePriceGroupId" HeaderText="Price Group Id" ItemStyle-Width="250px"/>
							<asp:BoundColumn DataField="Name" HeaderText="Name" />
							<asp:BoundColumn DataField="Active" HeaderText="Active" />
						</Columns>
                    </asp:datagrid>

                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="ResultMessage" runat="server" Width="100%"  Height="300px" TextMode="MultiLine" ReadOnly="True" ForeColor="Red"></asp:TextBox>
                </td>
            </tr>

        </TABLE>
    <div>
    
    </div>
    </form>
</body>
</html>
