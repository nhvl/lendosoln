<%@ Page language="c#" Codebehind="PricePolicyList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.PricePolicyList" enableViewState="False"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!doctype html>
<html>
	<head runat="server">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>PricePolicyList</title>
        <style type="text/css">
            #treeBase {
                border: 2px groove;
                padding: 4px 0;
                overflow-y: scroll;
                background-color: whitesmoke;
                position: absolute;
			    top: 120px;
			    left: 6px;
			    bottom: 60px;
			    right: 6px;
            }
            #dynatree {
                padding-right: 4px;
                padding-left: 4px;
                background-color: whitesmoke;
                color: black;
                font-family: Arial, Arial, Helvetica, sans-serif, sans-serif;
                font-size: 11px;
                border: 2px solid whitesmoke;
            }
            .no-icon .dynatree-icon {
                display: none;
            }
            .ButtonStyle {
            	position: absolute;
			    bottom: 20px;
			    left: 50%;
			    margin-left: -25px;
            }
        </style>
	</head>
	<body onload="init();" onkeydown="if( event.keyCode == 18 ) onTrackSelection();" onkeyup="if( event.keyCode == 18 ) onResetSelection();">
		<script type="text/javascript">
		function init() {
		    $("#dynatree").dynatree({
		        onClick: function(node, event) {
		            if( event.altKey == true ) {
		                if( confirm( "Do you want to move the selected price policy group under this node?" ) ) {
		                    var args = new Object();
		                    args["SourceId"] = document.getElementById("m_sourcedSelection").value;
		                    args["TargetId"] = document.getElementById("m_clickedSelection").value;

		                    if (args.SourceId.toLowerCase() == "root") {
		                        alert('The root is not a valid node to transfer.  Move denied.');
		                        return;
		                    }

		                    var result = gService.main.call("Move", args, true, true);
		                    if (!result.error) {
		                        alert('Move Successfully.');
		                        f_refresh();
		                    } else {
		                        alert('ERROR: ' + result.UserMessage);
		                    }
		                }
		            }
		        },
		        minExpandLevel: 2,            
		        persist: false,
		        children: nodes
		    });

			if( document.getElementById("m_errorMessage") != null ) {
				alert( document.getElementById("m_errorMessage").value );
			}

			if( document.getElementById("m_feedBack") != null ) {
				alert( document.getElementById("m_feedBack").value );
			}
		}

		function getActiveNode() {
		    var tree = $("#dynatree");
		    var activeNode = tree.dynatree("getActiveNode");

		    if (activeNode === null) {
		        activeNode = tree.dynatree("getRoot").childList[0];
		    }

		    return activeNode;
		}

		function f_getNodeData() {
		    var node = getActiveNode();
		    var nodeData = node.data.key;

		    if (nodeData == "root"){
		        return nodeData;
    }
		    else {
		        return g_ids[parseInt(nodeData)];
		    }
        }

		function onTrackSelection() {
			document.getElementById("m_sourcedSelection").value = f_getNodeData();
		}

		function f_refresh() {
		  location.href = location.href;
		}

		function onResetSelection() {
			document.getElementById("m_sourcedSelection").value = "";
			document.getElementById("m_clickedSelection").value = "";
		}

		function f_onclick(i, event) { 
            try {
			    if( event.altKey == true ) {
			        var id = <%= AspxTools.JsString(Guid.Empty.ToString()) %>;

			        if (i >= 0) {
		                id = g_ids[i];
			        }
				        
				    document.getElementById("m_clickedSelection").value = id;
			    }
		    }
		    catch( e ) {
			    alert( e.message );
			}
		}

		function f_edit( i ) {
			showModal( '/los/RatePrice/PricePolicy.aspx?pricepolicyid=' + g_ids[i] );
			return false;
		}

		function f_add( i ) {
		  var id = <%= AspxTools.JsString(Guid.Empty.ToString()) %>;
		  if (i >= 0)
		    id = g_ids[i];

			showModal( '/los/RatePrice/PricePolicy.aspx?cmd=add&parentpolicyid=' + id , null, null, null, function(args){ 
			    if (args.OK){
					f_showWarning(true);
			        f_refresh();
			    }				    
			});

			return false;
		}

		function buildPrice() {
      var id = f_getNodeData();

			if( id != "" ) {
				if( id.toLowerCase() == "root" )
					alert('This feature is not available for the root policy') ;
				else {
					var win = window.open('Helpers/PolicyBuilder.aspx?parentpolicyid=' + id, "PolicyBuilder", "resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes") ;
					win.focus() ;
				}
			}
		}

		function exportGroup() {
			var id = f_getNodeData();

			if( id != "" ) {
				if( id.toLowerCase() != "root" ) {
					window.open( "PricePolicyExport.aspx?pricePolicyId=" + id, "_parent" );
				}
				else {
				    alert('This feature is not available for the root policy');
				}
			}
		}

		function onAssocView() {
			var id = f_getNodeData();
			var win = window.open("Helpers/AssocView.aspx?pricePolicyId=" + id, 'AssocView', 'toolbar=no,menubar=no,resizable=yes');
			win.focus() ;
		}

		function onBatchDetach() {
		    var id = f_getNodeData();
		    var win = window.open("Helpers/AssocView.aspx?action=detach&pricePolicyId=" + id, 'BatchDetach', 'toolbar=no,menubar=no,resizable=yes');
		    win.focus() ;
		}

		function onConditionSyntax() {
			var win = window.open("Helpers/ConditionEdit.aspx", 'ConditionEdit', 'toolbar=no,menubar=no,resizable=yes');
			win.focus() ;
		}

		function onConditionGen() {
			var win = window.open("Helpers/ConditionGen.aspx", 'ConditionGen', 'toolbar=no,menubar=no,resizable=yes');
			win.focus() ;
		}

		function onLookupAncestor() {
			var win = window.open("Helpers/AncestorLookup.aspx", 'AncestorLookup', 'toolbar=no,menubar=no,resizable=yes');
			win.focus() ;
		}

		function onSchemaValidator() {
			var win = window.open("LPETools/SchemaValidator.aspx", 'SchemaValidator', 'toolbar=no,menubar=no,resizable=yes');
			win.focus() ;
		}

		function f_close() {
		  onClosePopup();
		}

		function f_delete() {
		  if (!confirm( 'Do you want to delete this price policy group?' ))
		    return;
		  var id = f_getNodeData();
		  if (id != "") {
		    if (id.toLowerCase() == "root") {
		      alert('Unable to delete root node.');
		        } 
		        else {
		      var args = new Object();
		      args["PricePolicyId"] = id;

		      var result = gService.main.call("Delete", args, true, true);
		            
		      if (!result.error) {
		        alert('Delete Successfully!');
		        f_showWarning(true);
		                f_refresh();
		            } 
		            else {
		        alert('ERROR: ' + result.UserMessage);
		      }
		    }
		  }
		}
		
    function f_showWarning(bVisible) {
      document.getElementById("RefreshWarningLabel").style.display = bVisible ? "" : "none";
    }
		</script>
		<form id="PricePolicyList" method="post" runat="server">
			<TABLE class="FormTable" cellSpacing="2" cellPadding="4" width="100%" height="100%" border="0">
				<TR>
					<TD class="FormTableHeader" height="1">
						Rule Groups
					</TD>
				</TR>
				<TR>
					<TD height="1">
						<INPUT id="m_sourcedSelection" name="m_sourcedSelection" value="none" type="hidden" runat="server">
						<INPUT id="m_clickedSelection" name="m_clickedSelection" value="none" type="hidden" runat="server">
						<input type="button" value="Refresh" onclick="f_refresh();"> <INPUT type="button" value="Export" onclick="exportGroup();">
						<INPUT type="button" value="Delete" onclick="f_delete();"> 
						<INPUT onclick="buildPrice();" type="button" value="Policy Builder">&nbsp;&nbsp;
						<INPUT onclick="onAssocView();" type="button" value="View Assoc">
						<INPUT onclick="onBatchDetach();" type="button" value="Batch Detach">
                        <INPUT onclick="onConditionSyntax();" type="button" value="Condition Syntax">
						<INPUT onclick="onConditionGen();" type="button" value="Condition Gen">&nbsp; <INPUT onclick="onLookupAncestor();" type="button" value="Lookup Ancestor">
						<INPUT onclick="onSchemaValidator();" type="button" value="Schema Validator">&nbsp;
						<span style="DISPLAY:none;FONT-WEIGHT:bold;COLOR:red" id="RefreshWarningLabel">TREE
							IS MODIFIED. MAKE SURE YOU REFRESH TO UPDATE THE TREE.</span>
					</TD>
				</TR>
				<TR>
					<TD height="1">
						<TABLE cellpadding="1" cellspacing="4" width="100%" style="BORDER-RIGHT: 2px groove; BORDER-TOP: 2px groove; BORDER-LEFT: 2px groove; BORDER-BOTTOM: 2px groove">
							<TR>
								<TD align="center">
									<IMG src="../../images/tri.gif">
								</TD>
								<TD>
									-
								</TD>
								<TD>
									Move a rule group to another branch by first selecting the group to move, and
									then selecting the destination group with the ALT-key pressed
								</TD>
							</TR>
							<TR>
								<TD align="center">
									<IMG src="../../images/published.gif">
								</TD>
								<TD>
									-
								</TD>
								<TD>
									Rule group that loan products may attach
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD>
                        <div id="treeBase">
                            <div id="dynatree">
                            </div>
                        </div>                        
					</TD>
				</TR>
				<TR>
					<TD align="center" height="1" style="PADDING-RIGHT: 8px;PADDING-LEFT: 8px;PADDING-BOTTOM: 8px;PADDING-TOP: 8px">
						<input type="button" value="Close" onclick="f_close();" class="ButtonStyle">
						<asp:CheckBox id="m_saveExpandState" runat="server" Text="Save tree expansion state on close?" Visible="False"></asp:CheckBox>
					</TD>
				</TR>
			</TABLE>

		</form>
	</BODY>
</HTML>
