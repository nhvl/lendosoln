<%@ Page language="c#" validateRequest="false" Codebehind="RulesEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.RulesEdit" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
  <head>
		<title>RulesEdit</title>
      <link href="../../css/stylesheet.css" type=text/css rel=stylesheet >
  </head>
<body scroll=yes onload="init();" >
<script type="text/javascript">
    <!--
      function init()
      {
		<% if( IsPostBack == false ) { %>
			resize(910, 925);
		<% } %>
      }
      function insertKeyword(ed,ddl) {
          ed.focus();
          ed.value += " " + ddl.options[ddl.selectedIndex].value + " ";
      }
    //-->
		</script>
<h4 class="page-header">Edit Rule</h4>
<form id=RulesEdit method=post runat="server">
<center>
<TABLE id=Table2 cellSpacing=0 cellPadding=0 width="99%" border=0>
  <TR>
    <TD class="FieldLabel" noWrap="noWrap" align="right" width="100" style="padding-right:10px">Rule Description:</TD>
    <TD noWrap colSpan="3">
        <asp:textbox id=Description_ed runat="server" Width="100%" NoHighlight /></TD>
    <TD width="30"><IMG src="../../images/require_icon.gif" >
        <asp:requiredfieldvalidator id=RequiredFieldValidator1 runat="server" ControlToValidate="Description_ed">
            <img runat="server" src="../../images/error_icon.gif">
        </asp:requiredfieldvalidator></TD>
    </TR>
    <TR>
    <TD class="FieldLabel" noWrap="noWrap" align="right" width="100" style="padding-right:10px">QBC:</TD>
    <TD class="FieldLabel" noWrap="noWrap" colspan="3" >
        <asp:dropdownlist id="m_qbcList" runat="server" />
    </TD>
    </TABLE><br>
<TABLE id=Table1 cellSpacing=0 cellPadding=2 width="99%" border=0>
  <TR>
    <TD class=FormTableHeader1>Condition</TD></TR>
  <TR>
    <TD><EM>NOTE: All words and
      symbols must be separated by at least one space.</EM></TD></TR>
  <TR>
    <TD><EM>Example:&nbsp; </EM><EM
      >( ltv &gt;= 80 ) and ( cltv &gt; 80 )</EM></TD></TR>
  <TR>
    <TD class=fieldLabel>Keywords
      <select id=Symbol_dd runat="server" onchange="insertKeyword(document.getElementById('Condition_ed'),this);"></select>
      <input onclick="insertKeyword(document.getElementById('Condition_ed'),document.getElementById('Symbol_dd'));" type="button" value="Insert"></TD></TR>
  <TR>
    <TD>
      <P><asp:textbox id=Condition_ed runat="server" Width="100%" TextMode="MultiLine" Height="129px" Font-Size="X-Small" /></P></TD></TR>
  <TR>
    <TD>&nbsp;&nbsp;&nbsp; </TD></TR>
  <TR>
    <TD class=FormTableHeader1>Adjustments &amp;
      Qualifications</TD></TR>
  <TR>
    <TD>
        <asp:checkbox id=Skip_chk runat="server" Text="Skip This Group and Its Children Groups" Font-Bold="True"></asp:checkbox>&nbsp;&nbsp;&nbsp;
        <asp:checkbox id=Disqualify_chk runat="server" Text="Disqualify" Font-Bold="True"></asp:checkbox>&nbsp;&nbsp;&nbsp;
        <asp:checkbox id=LenderRule_chk runat="server" Text="Is Lender Rule?" Font-Bold="True"></asp:checkbox>
    </TD>
  </TR>
  <TR>
    <TD>
      <TABLE id=Table3 cellSpacing=0 cellPadding=0 width="100%" border=0
      >
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap ><P align=right>Adjust Fee
            By</P></TD>
          <TD><asp:textbox id=FeeDelta_ed runat="server" Width="213"></asp:textbox></TD>
          <TD class=FieldLabel noWrap>Keywords <select id=FeeKeywords_dd runat="server" onchange="insertKeyword(document.getElementById('FeeDelta_ed'),this);"></select>
            &nbsp;<input id="FeeInsert_btn" onclick="insertKeyword(document.getElementById('FeeDelta_ed'),document.getElementById('FeeKeywords_dd'));" type="button" value="Insert"></TD></TR>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap ><P align=right>Adjust
            Margin By</P></TD>
          <TD><asp:textbox id=MarginDelta_ed runat="server" Width="213px"></asp:textbox></TD>
          <TD class=FieldLabel noWrap>Keywords
            <select id=MarginKeywords_dd runat="server" onchange="insertKeyword(document.getElementById('MarginDelta_ed'),this);"></select>
            &nbsp;<INPUT id="MarginInsert_btn" onclick="insertKeyword(document.getElementById('MarginDelta_ed'),document.getElementById('MarginKeywords_dd'));" type="button" value="Insert"></TD></TR>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap ><P align=right>Adjust Note Rate
            By</P></TD>
          <TD colSpan=2><ml:percenttextbox id=RateDelta_ed runat="server" width="70" preset="percent"></ml:percenttextbox></TD></TR>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap ><P align=right>Adjust Qltv
            /&nbsp;Qcltv&nbsp;By</P></TD>
          <TD colSpan=2><ml:percenttextbox id=QltvDelta_ed runat="server" width="70" preset="percent"></ml:percenttextbox>&nbsp;<STRONG
            >/</STRONG> <ml:percenttextbox id=QcltvDelta_ed runat="server" width="70" preset="percent"></ml:percenttextbox></TD></TR>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap ><P align=right>Front-End Max
            YSP=</P></TD>
          <TD><asp:textbox id=MaxFrontEndYsp_ed runat="server" width="213px" /></TD>
          <TD class=FieldLabel noWrap>Keywords <select id=FrontEndMaxYspKeywords_dd runat="server" onchange="insertKeyword(document.getElementById('MaxFrontEndYsp_ed'),this);"></select>
            &nbsp;<INPUT id="Button1" onclick="insertKeyword(document.getElementById('MaxFrontEndYsp_ed'),document.getElementById('FrontEndMaxYspKeywords_dd'));" type="button" value="Insert"></TD></TR>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap ><P align=right>Adjust Back-End Max
            YSP By</P></TD>
          <TD colSpan=2><ml:percenttextbox id=MaxYspDelta_ed runat="server" width="70" preset="percent"></ml:percenttextbox></TD></TR>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap ><P align=right>Stipulation
            Text</P></TD>
          <TD colSpan=2><asp:textbox id=Stipulation_ed runat="server" Width="334px" NoHighlight></asp:textbox></TD></TR>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap align=right>Adjust QLAmt By</TD>
          <TD colSpan=2><asp:TextBox id=QLAmtDelta_ed runat="server" Width="213px"></asp:TextBox></TD></TR>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap ><P align=right>Base Margin
            =</P></TD>
          <TD colSpan=2><asp:textbox id=BaseMargin_ed runat="server" Width="213px"></asp:textbox>&nbsp;<STRONG
            >Keywords</STRONG>&nbsp; <select id=BaseMarginKeywords_dd runat="server" onchange="insertKeyword(document.getElementById('BaseMargin_ed'),this);" DESIGNTIMEDRAGDROP="83"></select>
            <INPUT id="MarginBaseInsert_btn" onclick="insertKeyword(document.getElementById('BaseMargin_ed'),document.getElementById('BaseMarginKeywords_dd'));" type="button" value="Insert" DESIGNTIMEDRAGDROP="84"></TD></TR>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap ><P align=right>Qscore
          =</P></TD>
          <TD colSpan=2><asp:textbox id=Qscore_ed runat="server" Width="213px"></asp:textbox>&nbsp;<strong>Keywords</strong>&nbsp; <select id="QscoreKeywords_dd" runat="server" onchange="insertKeyword(document.getElementById('Qscore_ed'),this);"></select>
            <INPUT id="QscoreInsert_btn" onclick="insertKeyword(document.getElementById('Qscore_ed'),document.getElementById('QscoreKeywords_dd'));" type="button" value="Insert"></TD></TR>
        <tr>
          <td class="FieldLabel" style="WIDTH: 135px" nowrap align="right">Adjust Qual Rate By</td>
          <td colspan="2"><asp:TextBox ID="QrateDelta_ed" runat="server" Width="213px" /></td></tr>
        <tr>
          <td class="FieldLabel" style="WIDTH: 135px" nowrap align="right">Adjust Teaser Rate By</td>
          <td colspan="2"><asp:TextBox ID="TeaserRateDelta_ed" runat="server" Width="213px" /></td></tr>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap align=right
          >MaxDti=</TD>
          <TD colSpan=2><asp:textbox id=MaxDti_ed runat="server"></asp:textbox>&nbsp;The
            lowest one will be used if conflicted with other
      rules</TD></TR>
        <TR>
          <TD class=FieldLabel style="WIDTH: 135px" noWrap ><P align=right>Adjust LockDays By</P></TD>
          <TD colSpan=2><asp:textbox id="LockDays_ed" runat="server" Width="213px"/>
            &nbsp;<strong>Keywords</strong>
            &nbsp; <select id="LockDaysKeywords_dd" runat="server" onchange="insertKeyword(document.getElementById('LockDays_ed'),this);"></select>
            <INPUT id="LockDaysInsert_btn" onclick="insertKeyword(document.getElementById('LockDays_ed'),document.getElementById('LockDaysKeywords_dd'));" type="button" value="Insert"></TD></TR>
      </TABLE></TD></TR>
  <TR>
    <TD><asp:button id=Syntax_btn runat="server" Text="Validate Syntax" onclick="Syntax_btn_Click" /></TD></TR>
  <TR>
    <TD>
      <table id=Table4 cellSpacing=0 cellPadding=0 width="100%" border=0>
        <tr>
          <td class=FieldLabel noWrap>QBC</td>
          <td><asp:textbox id="QBCResult_ed" runat="server" Width="527px" ReadOnly="True" /></td>
        </tr>
        <tr>
          <td class=FieldLabel noWrap>Condition Syntax</td>
          <td><asp:textbox id=ConditionResult_ed runat="server" Width="527px" ReadOnly="True" /></td>
        </tr>
        <tr>
          <td class=FieldLabel noWrap>Fee Adjust Syntax</td>
          <td><asp:textbox id=FeeResult_ed runat="server" Width="527px" ReadOnly="True" /></td>
        </tr>
        <tr>
          <td class=FieldLabel noWrap>Margin Adjust Syntax</td>
          <td><asp:textbox id=MarginResult_ed runat="server" Width="527px" ReadOnly="True" /></td>
        </tr>
        <tr>
          <td class=FieldLabel noWrap>Front-End Max YSP Syntax</td>
          <td><asp:textbox id=FrontEndMaxYspResult_ed runat="server" Width="527px" ReadOnly="True" /></td>
        </tr>
        <tr>
          <td class=FieldLabel noWrap>Base Margin Syntax</td>
          <td><asp:textbox id=BaseMarginResult_ed runat="server" Width="527px" ReadOnly="True" /></td>
        </tr>
        <tr>
          <td class=FieldLabel noWrap>Qscore Syntax</td>
          <td><asp:textbox id=QscoreResult_ed runat="server" Width="527px" ReadOnly="True" /></td>
        </tr>
        <tr>
          <td class=FieldLabel noWrap>QLAmt Adjust Syntax</td>
          <td><asp:TextBox id=QLAmtResult_ed runat="server" Width="527px" ReadOnly="True" /></td>
        </tr>
        <tr>
          <td class=FieldLabel noWrap>MaxDti</td>
          <td><asp:textbox id=MaxDtiResult_ed runat="server" Width="527px" ReadOnly="True" /></td>
        </tr>
        <tr>
          <TD class=FieldLabel noWrap>LockDays Adjust Syntax</TD>
          <td><asp:textbox id=LockDaysResult_ed runat="server" Width="527px" ReadOnly="True" /></td>
        </tr>
      </table>
    </TD>
  </TR>
  <TR>
    <TD></TD></TR>
  <TR>
    <TD align=center><asp:button id=Ok_btn runat="server" Text="&nbsp;&nbsp;OK&nbsp;&nbsp;" onclick="onOKClick"></asp:button>&nbsp;&nbsp;&nbsp;
      &nbsp;<INPUT onclick=onClosePopup(); type=button value=Cancel>&nbsp;
      &nbsp;&nbsp;<asp:button id=Apply_btn runat="server" Text="Apply" onclick="onApplyClick"></asp:button></TD></TR></TABLE></center></form>
	</body>
</html>
