using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Web.Services;
using System.Collections.Generic;
using MeridianLink.CommonControls;

namespace LendersOfficeApp.los.RatePrice
{
    public partial class PricePolicy : LendersOffice.Common.BasePage
	{
		protected Guid m_ruleID;
		protected bool m_isNewRule;

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private string ErrorMessage
		{
			set { ClientScript.RegisterHiddenField( "m_errorMessage" , value ); }
		}

		private Guid m_brokerID
		{
			get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
		}

		private Guid m_parentPolicyID 
		{
			get { return RequestHelper.GetGuid("parentpolicyid", Guid.Empty); }
		}

		protected Guid PricePolicyId
		{
			get { return RequestHelper.GetGuid("pricepolicyid"); } 
		}

		protected string m_cmd 
		{
			get { return RequestHelper.GetSafeQueryString("cmd"); }
		}

		protected Guid m_newPricePolicyID 
		{
			get { return (Guid) ViewState["newpricepolicyid"]; }
			set { ViewState["newpricepolicyid"] = value; }
		}
		protected string m_newPricePolicyName 
		{
			get { return (string) ViewState["newpricepolicyname"]; }
			set { ViewState["newpricepolicyname"] = value; }
		}
		protected string m_isPublished
		{ 
			get { return (string) ViewState["newispublished"]; }
			set { ViewState["newispublished"] = value; }
		}

        protected bool m_attachable // policy can attach to loan programs or price groups
        {
            get { return (bool)ViewState["attachable"]; }
            set { ViewState["attachable"] = value; }
        }


        private void LoadData() 
		{
            m_attachable = false;
            if (m_cmd != "add") 
			{
				m_newPricePolicyID = PricePolicyId;
				
				// 7/15/2004 kb - Block access if pricing engine enabled
				// and user lacks permission.

                if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
				{
					if( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == false )
					{
						ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

						return;
					}
				}

				CPricePolicy pricePolicy = CPricePolicy.Retrieve( PricePolicyId);
				m_descriptionTF.Text = pricePolicy.PricePolicyDescription;
				m_notesTF.Text = pricePolicy.Notes;
				MutualExclusiveExecSortedId_ed.Text = pricePolicy.MutualExclusiveExecSortedId;
				m_isPublished_chk.Checked = pricePolicy.IsPublished;
				m_hasQRule_chk.Checked = pricePolicy.HasQualifiedRule;

				CRuleList rules = pricePolicy.Rules;

				m_rulesDG.DataSource = rules;
				m_rulesDG.DataBind();

                ClientScript.RegisterHiddenField("hiddenOK", "true");
                m_isPublished_chk.ToolTip = "Specify whether or not to allow loan products to associate.";
                m_isPublished_chk.Enabled = true;

                m_attachable = pricePolicy.IsPublished; 
            }
            else 
			{
				m_isPublished_chk.ToolTip = "Specify whether or not to allow loan products to associate.";
				
				m_newPricePolicyID = Guid.Empty;
				Page.ClientScript.RegisterHiddenField("hiddenOK", "false");
			}
		}

		private void SaveData() 
		{
			// 7/15/2004 kb - Block access if pricing engine enabled
			// and user lacks permission.

            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
			{
				if( CurrentUser.HasPermission( Permission.CanModifyLoanPrograms ) == false )
				{
					ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

					return;
				}
			}

			CPricePolicy pricePolicy = null;
			if (m_cmd == "add") 
			{
				pricePolicy = CPricePolicy.CreateNewPricePolicy(m_brokerID, m_parentPolicyID);
			} 
			else 
			{
				pricePolicy = CPricePolicy.Retrieve( PricePolicyId);
			}
			pricePolicy.PricePolicyDescription = m_descriptionTF.Text ;
			pricePolicy.Notes = m_notesTF.Text ;
			pricePolicy.MutualExclusiveExecSortedId = MutualExclusiveExecSortedId_ed.Text;
			pricePolicy.IsPublished = m_isPublished_chk.Checked;
			pricePolicy.Save();
			m_newPricePolicyID = pricePolicy.PricePolicyId;
			m_newPricePolicyName = pricePolicy.PricePolicyDescription;
			m_isPublished = pricePolicy.IsPublished ? "true" : "false";
            m_attachable = pricePolicy.IsPublished && m_cmd != "add";

            m_rulesDG.DataSource = pricePolicy.Rules;
			m_rulesDG.DataBind();

			ClientScript.RegisterHiddenField("hiddenOK", "true");
		}
		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

            EnableJqueryMigrate = false;

            if (!Page.IsPostBack) 
			{
				LoadData();
			}

			if (m_cmd != "add")
			{
				m_policyIdTF.Text = PricePolicyId.ToString() ;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.m_rulesDG.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.onItemCommand);
			this.m_rulesDG.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.onItemDataBound);
			this.Load += new System.EventHandler(this.PageLoad);
			
			this.RegisterJsScript("LQBPopup.js");
            this.EnableJquery = true;
		}
		
		#endregion

		private void onItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Footer) 
			{
				return;
			}
			CRule rule = (CRule) e.Item.DataItem;
			CConsequence consequence = rule.Consequence;
			HtmlAnchor editLink = new HtmlAnchor();
			editLink.InnerText = "edit";
			editLink.HRef = String.Format("javascript:editRule('{0}');", rule.RuleId, PricePolicyId );

            EncodedLiteral skipLabel = new EncodedLiteral() { Text = consequence.Skip ? "yes" : "" };
            EncodedLiteral disqLabel = new EncodedLiteral() { Text = consequence.Disqualify ? "yes" : "" };
            EncodedLiteral stipLabel = new EncodedLiteral() { Text = "" != consequence.Stipulation ? "yes" : "" };
            EncodedLiteral qbcLabel = new EncodedLiteral();
            switch (rule.QBC)
            {
                case E_sRuleQBCType.PrimaryBorrower:
                    qbcLabel.Text = "P";
                    break;
                case E_sRuleQBCType.AllBorrowers:
                    qbcLabel.Text = "A";
                    break;
                case E_sRuleQBCType.OnlyCoborrowers:
                    qbcLabel.Text = "C";
                    break;
                default:
                    qbcLabel.Text = "";
                    break;
            }

            e.Item.Cells[0].Controls.Add(qbcLabel);
            e.Item.Cells[1].Controls.Add( skipLabel );
			e.Item.Cells[2].Controls.Add(editLink);
			e.Item.Cells[3].Controls.Add( new EncodedLiteral() { Text = rule.Description } );
			e.Item.Cells[4].Controls.Add( disqLabel );
			e.Item.Cells[5].Controls.Add( new EncodedLiteral() { Text = consequence.FeeAdjustTarget.AdjustmentText } );
			e.Item.Cells[6].Controls.Add( new EncodedLiteral() { Text = consequence.MarginAdjustTarget.AdjustmentText } );
			e.Item.Cells[7].Controls.Add( new EncodedLiteral() { Text = consequence.RateAdjustTarget.AdjustmentText } );
			e.Item.Cells[8].Controls.Add( stipLabel );
			e.Item.Cells[9].Controls.Add( new EncodedLiteral() { Text = consequence.MarginBaseTarget.AdjustmentText } );
			e.Item.Cells[10].Attributes.Add("onclick", "return confirm('Do you want to delete this rule?');");

		}

		protected void onOKClick(object sender, System.EventArgs e)
		{
            // 7/15/2004 kb - Block access if pricing engine enabled
            // and user lacks permission.

            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
            {
                if (CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false)
                {
                    ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

                    return;
                }
            }

            Page.Validate();

            if (Page.IsValid)
            {
                SaveData();
                LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page, new string[] { "OK=true" });
            }
        }

        protected void onAddClick( object sender , System.EventArgs a )
		{
            // 7/15/2004 kb - Block access if pricing engine enabled
            // and user lacks permission.

            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
            {
                if (CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false)
                {
                    ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

                    return;
                }
            }

            Page.Validate();

            if (Page.IsValid)
            {
                SaveData();
                m_isNewRule = true;
                if (m_cmd != "add")
                {
                    m_newPricePolicyID = PricePolicyId;
                }
            }
        }

        private void onItemCommand( object source , System.Web.UI.WebControls.DataGridCommandEventArgs a )
		{
            // 7/15/2004 kb - Block access if pricing engine enabled
            // and user lacks permission.

            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
            {
                if (CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false)
                {
                    ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

                    return;
                }
            }

            Guid ruleID = (Guid)m_rulesDG.DataKeys[a.Item.ItemIndex];
            CPricePolicy pricePolicy = new CPricePolicy(PricePolicyId);
            pricePolicy.DeleteRule(ruleID);
            pricePolicy.Save();

            SaveData();
        }

        protected void onApplyClick( object sender , System.EventArgs a )
		{
            // 7/15/2004 kb - Block access if pricing engine enabled
            // and user lacks permission.

            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PricingEngine) == true)
            {
                if (CurrentUser.HasPermission(Permission.CanModifyLoanPrograms) == false)
                {
                    ErrorMessage = "You do not have permission to modify loan programs.  Access denied.";

                    return;
                }
            }

            Page.Validate();
            if (Page.IsValid)
            {
                SaveData();
                if (m_cmd == "add")
                {
                    // Redirect to edit page.
                    Response.Redirect("PricePolicy.aspx?pricepolicyid=" + m_newPricePolicyID);
                }
            }
        }

        [WebMethod]
        public static string CacheSelectedGuids(string guidList)
        {
            //return AutoExpiredTextCache.InsertUserObject(PrincipalFactory.CurrentPrincipal, programIds, TimeSpan.FromHours(1));
            string[] guidStrs = guidList.Split(new char[] { ',', ' ', '\t', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            var ids = new List<Guid>();
            var dupIds = new List<Guid>();
            var idSet = new HashSet<Guid>();            

            foreach(var str in guidStrs)
            {
                Guid id;
                if( Guid.TryParse(str, out id) )
                {
                    if(idSet.Contains(id))
                    {
                        dupIds.Add(id);
                        continue;
                    }

                    idSet.Add(id);
                    ids.Add(id);

                }
                else
                {
                    throw new System.Web.HttpException($"'{str}' is not valid guid format.");
                }
            }

            if(dupIds.Count > 0)
            {
                throw new System.Web.HttpException($"Duplicate programs/price groups {string.Join("\r\n", dupIds)}.");
            }

            if(ids.Count == 0)
            {
                throw new System.Web.HttpException($"Please enter list of loan program/price group ids.");
            }

            return AutoExpiredTextCache.InsertUserObject(PrincipalFactory.CurrentPrincipal, ids, TimeSpan.FromMinutes(5)); ;
        }

    }
}
