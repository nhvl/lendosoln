using System;
using System.Threading;
using LpeReleaseService = LendersOfficeApp.LpeRelease.LpeReleaseService;
using Tools = DataAccess.Tools;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
	public class LpeReleaseController
	{
        private const int NumOfRetries = 3;

		static LpeReleaseController()
		{
		}

        static public void FastReleaseSnapshotFor(  DateTime priceEngineVersionDate )
        {

            for( int index = 1; index <= NumOfRetries; index++) 
            {
                try 
                {
                    LpeReleaseService service = CreateWebService();;
                    service.FastReleaseSnapshotFor( priceEngineVersionDate );
                    return;
                } 
                catch (Exception exc) 
                {
                    string errMsg;
                    if( index < NumOfRetries )
                    {
                        errMsg = string.Format( "<LpeReleaseController> Retries FastReleaseSnapshotFor priceEngineVersionDate = {0} #{1}", 
                                                priceEngineVersionDate, index);
                        Tools.LogWarning(errMsg + " .\r\n" + exc.ToString()  );
                    }
                    else
                    {
                        errMsg = string.Format( "<LpeReleaseController> Unable to complete FastReleaseSnapshotFor priceEngineVersionDate = {0} after try {1} times", 
                                                priceEngineVersionDate, index);
                        Tools.LogError(errMsg, exc);
                        return;
                    }
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(120);
                }
            }
        }

        static public void FastReleaseSnapshot()
        {
            FastReleaseSnapshotFor(  DateTime.MaxValue );
        }


        private static LpeReleaseService CreateWebService() 
        {
            if( ConstSite.LpeReleaseControllerUrl == "" )
                throw new CBaseException(ErrorMessages.Generic, "<LpeReleaseService> Expect ConstSite.LpeReleaseControllerUrl != \"\" " );
            LpeReleaseService service = new LpeReleaseService();
            service.Url = ConstSite.LpeReleaseControllerUrl + "/WebService/LpeReleaseService.asmx";

            // Fix the error : ASP.NET Webservices: "The request failed with HTTP status 401: Access Denied." 
            // http://geekswithblogs.net/ranganh/archive/2006/02/21/70212.aspx

            //service.PreAuthenticate = true;
            //service.Credentials = System.Net.CredentialCache.DefaultCredentials;

            return service;
        }

	}
}
