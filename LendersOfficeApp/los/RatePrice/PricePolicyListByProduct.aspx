<%@ Page language="c#" Codebehind="PricePolicyListByProduct.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.PricePolicyListByProduct" enableViewState="False"%>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<HTML>
<HEAD>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PricePolicyListByProduct</title>
    <LINK href="../../css/stylesheet.css" rel=stylesheet>
    <style>
        .policy-force-no { color: gray; }
        .pre-node {
            margin-right:6px;
            color:tomato;
        }
        .pre-node.expanded {
            background-color: lightslategray;
        }
        .pre-node-2 {
            padding-right:4px;
            background-color: white;
        }
        .policy-no-attachable { color:red }

        .tree-wrap {
            border: 2px groove;
            font: 12px arial;
            width: 100%;
            height: 100%;
            height: calc(100vh - 142px);
            background-color: whitesmoke;
        }
        #pricePolicyTree {
            height: 100%;
        }

    </style>
</HEAD>
<body style="OVERFLOW-Y: hidden" bgColor=gainsboro scroll=yes MS_POSITIONING="FlowLayout">
<h4 class="page-header">Attaching To Pricing Rule Groups</h4>
<form method=post runat="server">
    <INPUT id="m_Store" type="hidden" runat="server" NAME="m_Store">
    <TABLE class="FormTable" cellSpacing="4" cellPadding="4" width="100%" border="0">
        <TR>
            <TD class="FieldLabel">
                <ml:EncodedLiteral ID="m_programNameLiteral" runat="server" text="Product Name" />
            </TD>
            <TD>
                <asp:textbox id="ProductName_ed" runat="server"
                    style="width:304px; BORDER-RIGHT: medium none; BORDER-TOP: medium none; PADDING-LEFT: 4px; BORDER-LEFT: medium none; BORDER-BOTTOM: medium none; BACKGROUND-COLOR: transparent"
                    ReadOnly="true" EnableViewState="False"/>
            </TD>
            <TD align="right">
                <asp:checkbox id="m_Collapse" runat="server" CssClass="FieldLabel" Text="Collapse to associated" Checked="True"/>
            </TD>
        </TR>
    </TABLE>
    <TABLE class="FormTable" cellSpacing="2" cellPadding="4" width="100%" border="0">
        <TR>
            <TD>
                <DIV class="tree-wrap">
                    <div id="pricePolicyTree"></div>

                    <asp:panel id="m_TreeDenied" runat="server"
                        style="PADDING-RIGHT: 80px; PADDING-LEFT: 80px; PADDING-BOTTOM: 80px; COLOR: red; PADDING-TOP: 80px; TEXT-ALIGN: center">
                        Access denied. You lack permission to view.
                    </asp:panel>
                </DIV>
            </TD>
        </TR>
        <TR>
            <TD align=center>
                <asp:checkbox id="TurnOptimizerOffChk" runat="server" Text="Avoid Inheritance Optimizer" Checked="True" />
                <asp:checkbox id="m_KeepInSync" runat="server" Text="Synchronize Save" Visible="False" ToolTip="Checked: All chosen programs get this association set. Unchecked: Chosen programs can keep other existing associations not marked here." Checked="False" />
                &nbsp;&nbsp;
                <button id="btnOk" onclick="f_save()" type="button">  OK  </button>
                &nbsp;&nbsp;
                <button onclick="onClosePopup()" type="button">Cancel</button>
            </TD>
        </TR>
    </TABLE>
    <uc1:cmodaldlg runat="server" />
</FORM>
<% if (IsPostBack == false) { %>
<script>resize(600, 636);</script>
<% } %>
<script>
    var E_TriState = { Blank:0, Yes:1, No:2 };
    var guidEmpty = "00000000-0000-0000-0000-000000000000";
    var AssociationTypeForUi = {
        Program                  : 0,
        PriceGroup               : 1,
        ProgramInPriceGroup      : 2,
        ProgramInPriceGroupBatch : 3,
        PmiProduct               : 4
    };
    var productId = '<%= AspxTools.JsStringUnquoted(Request["fileid"])%>';
    var m_pmiProductId ='<%= AspxTools.JsStringUnquoted(m_pmiProductId.ToString()) %>';
    var m_priceGroupID = '<%= AspxTools.JsStringUnquoted(m_priceGroupID.ToString()) %>';
    var m_KeepInSync = document.getElementById("m_KeepInSync");
    var TurnOptimizerOffChk = document.getElementById("TurnOptimizerOffChk");
    var m_Collapse = document.getElementById("m_Collapse");

    var args = {
        "fileId": productId,
        "pmiProductId": m_pmiProductId,
        "priceGroupId": m_priceGroupID,
        "isCollapseToAssociated": m_Collapse
    };

    var state;
    $(document).on("DOMContentLoaded", function () {
        var result = gService.main.call("LoadPrograms", args);
        if (!!result.error) {
            alert("Error: " + result.UserMessage);
            setTimeout(onClosePopup, 1000);
            return;
        }
        state = JSON.parse(result.value.state);
    })
</script>
<script src="../../inc/PricePolicyListByProduct.js"></script>
</body>
</HTML>

