using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.RatePrice;

namespace LendersOfficeApp.los.RatePrice
{
    public partial class PricePolicyListByProduct : LendersOffice.Common.BaseServicePage
    {
        protected System.Web.UI.WebControls.Button m_removeBtn;

        private BrokerUserPrincipal CurrentUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected AssociationTypeForUi m_mode = AssociationTypeForUi.Program;

        private Guid m_fileID
        {
            get { return LendersOffice.Common.RequestHelper.GetGuid( "FileId", Guid.Empty ) ; }
        }

        protected Guid m_pmiProductId
        {
            get { return LendersOffice.Common.RequestHelper.GetGuid( "PmiProductId", Guid.Empty ) ; }
        }

        private string m_productName
        {
            get { return Request[ "ProductName" ]; }
        }

        protected Guid m_priceGroupID
        {
            get { return LendersOffice.Common.RequestHelper.GetGuid( "PriceGroupId", Guid.Empty ) ; }
        }

        private string m_priceGroupName
        {
            get { return Request[ "PriceGroupName" ]; }
        }

        private bool m_collapseToAssociated
        {
            get { return m_Collapse.Checked; }
        }

        private string m_ProgramListKey
        {
            get { return Request["ProgramList"]; }
        }

        protected void PageInit(object sender, System.EventArgs args)
        {
            this.RegisterService("main", "/los/RatePrice/PricePolicyListByProductService.aspx");
        }

        private void RegisterResource()
        {
            RegisterJsScript("IEPolyfill.js");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");
        }

        /// <summary>
        /// Initialize members of this page.
        /// </summary>
        protected void PageLoad( object sender , System.EventArgs a )
        {
            RegisterResource();
        //}
        ///// <summary>
        ///// Render the tree according to the database state.
        ///// </summary>
        //protected void PagePreRender( object sender , System.EventArgs a )
        //{
            // 7/15/2004 kb - Block access if pricing engine enabled
            // and user lacks permission.

            var isDenied = !CurrentUser.HasPermission( Permission.CanModifyLoanPrograms);
            m_TreeDenied.Visible = isDenied;

            if ( isDenied )
            {
                // Shut down the ui so that user doesn't see protected
                // content.
                m_Collapse.Visible = false;
                return;
            }

            //04/14/08 mf. OPM 21028.  We can render the policy tree (and save)
            // in three different modes.  Policies can associate by program,
            // Price Group and program within a Price Group.
            //
            // Use the querystring parameters to determine which tree to render.
            // ProgramId | PriceGroupId
            // ------------------------
            //      X    |              Program association
            //           |      X       PriceGroup association
            //      X    |      X       Program-While-In-PriceGroup association

            // 10/08/12 vm. OPM 26013 - New querystring parameter for associating
            // with MI providers called PmiProductId

            if ( m_priceGroupID != Guid.Empty )
            {
                if ( m_fileID != Guid.Empty )
                {
                    m_mode = AssociationTypeForUi.ProgramInPriceGroup;
                    DisplayProgramInPriceGroupAssocTree( false );
                }
                else if ( m_ProgramListKey != null )
                {
                    // Key list is only provided for batch edit.
                    m_mode = AssociationTypeForUi.ProgramInPriceGroupBatch;

                    var list = AutoExpiredTextCache.GetFromCache( m_ProgramListKey );
                    if (list == null)
                    {
                        throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Could not load program list from temp cache.");
                    }
                    
                    this.RegisterJsObjectWithJsonNetAnonymousSerializer("m_ProgramList", list);
                    DisplayProgramInPriceGroupAssocTree(true);
                }
                else
                {
                    m_mode = AssociationTypeForUi.PriceGroup;
                    DisplayPriceGroupAssocTree();
                }
            }
            else if ( m_fileID != Guid.Empty )
            {
                m_mode = AssociationTypeForUi.Program;
                DisplayProgramAssocTree();
            }
            else if (m_pmiProductId != Guid.Empty)
            {
                m_mode = AssociationTypeForUi.PmiProduct;
                DisplayPmiProgramAssocTree();
            }
            else
            {
                throw new CBaseException( LendersOffice.Common.ErrorMessages.Generic, "No provided program or price group to associate with.");
            }

            // Inheritance optimizing only makes sense in program mode.
            TurnOptimizerOffChk.Visible = ( m_mode == AssociationTypeForUi.Program );
            m_KeepInSync.Visible = (m_mode == AssociationTypeForUi.ProgramInPriceGroupBatch );
            m_Collapse.Visible = (m_mode != AssociationTypeForUi.ProgramInPriceGroupBatch );
        }


        private void DisplayPmiProgramAssocTree()
        {
            m_programNameLiteral.Text = "PMI Programs";
            ProductName_ed.Text = m_productName;

        }

        private void DisplayProgramAssocTree()
        {
            m_programNameLiteral.Text = "Program";
            ProductName_ed.Text = m_productName;
        }

        private void DisplayProgramInPriceGroupAssocTree( bool isBatchMode )
        {
            // 04/14/07 mf. OPM 21028. Association with a Program within a pricegroup.
            // We render this view in the following format:
            // ()L PG: ()Y ()N []Y []N
            // L: If the lender-level (direct program attachment) setting is to associate
            // PG Y/N: Policy association setting for this entire PriceGroup
            // Y/N : Policy asssociation setting for this program-in-PriceGroup

            // 05/19/08 mf. Per OPM 21028 we are allowing batch edit rules in this mode.
            // In batch mode, we cannot show lender-level attachment because each program
            // has its own set.

            // 04/21/09 mf. We now allow batch clear of these associations with a "Clear" checkbox.

            m_programNameLiteral.Text = "Program :: PriceGroup";
            ProductName_ed.Text = (isBatchMode ? "(Batch)" :  m_productName ) + " :: " + m_priceGroupName;

        }

        private void DisplayPriceGroupAssocTree()
        {
            // 05/13/09 mf. OPM 21028. Directly associating a policy with a Price Group.
            // We render this view in the following format:
            // []Y []N
            // There is no read-only data we can show here, so we only have mutually
            // exclusive Y/N setting
            m_programNameLiteral.Text = "PriceGroup";
            ProductName_ed.Text = m_priceGroupName;

        }

        // ---

        private static object DisplayPmiProgramAssocTree(Guid pmiProductId, bool collapseToAssociated)
        {
            var ds = new DataSet();
            DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByPmiProductId", new[] {new SqlParameter("@PmiProductID", pmiProductId)});

            var policies = new List<object>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var PricePolicyID = (Guid) row["PricePolicyID"];
                var ParentPolicyId = row["ParentPolicyId"] == DBNull.Value ? null : (Guid?)row["ParentPolicyId"];
                var PricePolicyDescription = WebUtility.HtmlEncode((string)row["PricePolicyDescription"]);
                var IsPublished = (bool)row["IsPublished"];
                var AssocOverrideTri = row["AssocOverrideTri"] is DBNull ? E_TriState.Blank : (E_TriState)(byte)row["AssocOverrideTri"];
                var InheritVal = !(row["InheritVal"] is DBNull) && (bool)row["InheritVal"];
                var IsExpandedNode = (bool)row["IsExpandedNode"];

                var hasAssociations = AssocOverrideTri == E_TriState.Yes || AssocOverrideTri == E_TriState.No || InheritVal;
                var parentExpanded = collapseToAssociated && hasAssociations;
                var childExpanded = !collapseToAssociated && (bool) row["IsExpandedNode"];
                var ySel = IsPublished && AssocOverrideTri == E_TriState.Yes;
                var nSel = IsPublished && AssocOverrideTri == E_TriState.No;

                policies.Add(new
                {
                    PricePolicyID,
                    ParentPolicyId,
                    PricePolicyDescription,
                    IsPublished,
                    AssocOverrideTri,
                    InheritVal,
                    IsExpandedNode,

                    hasAssociations,
                    parentExpanded,
                    childExpanded,

                    ySel,
                    nSel,
                });
            }

            return new
            {
                mode = AssociationTypeForUi.PmiProduct,
                policies,
            };
        }

        private static object DisplayProgramAssocTree(Guid loanProductId, bool collapseToAssociated)
        {
            // 11/16/2007 dd - Review and safe
            var isMaster = CheckIsMaster(loanProductId);
            var CurrentUser = PrincipalFactory.CurrentPrincipal;

            var fnm = new ForceNoManager(CurrentUser.BrokerId);

            var ds = new DataSet();
            DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByLoanProductId", new[] {new SqlParameter("@LoanProductID", loanProductId)});

            var policies = new List<object>();
            foreach( DataRow row in ds.Tables[ 0 ].Rows )
            {
                var PricePolicyID = (Guid) row["PricePolicyID"];
                var ParentPolicyId = row["ParentPolicyId"] == DBNull.Value ? null : (Guid?)row["ParentPolicyId"];
                var PricePolicyDescription = WebUtility.HtmlEncode((string)row["PricePolicyDescription"]);
                var IsPublished = (bool)row["IsPublished"];
                var AssocOverrideTri = row["AssocOverrideTri"] is DBNull ? E_TriState.Blank : (E_TriState)(byte)row["AssocOverrideTri"];
                var InheritVal = !(row["InheritVal"] is DBNull) && (bool)row["InheritVal"];
                var InheritValFromBaseProd = !(row["InheritValFromBaseProd"] is DBNull) && (bool)row["InheritValFromBaseProd"];
                var IsForcedNo = !(row["IsForcedNo"] is DBNull) && (bool)row["IsForcedNo"];
                var IsExpandedNode = (bool)row["IsExpandedNode"];

                var FNProgramId = IsPublished ? fnm.IsFnAssoc(PricePolicyID, loanProductId) : Guid.Empty;

                var hasAssociations = (
                    (AssocOverrideTri == E_TriState.Yes || AssocOverrideTri == E_TriState.No) ||
                    InheritValFromBaseProd ||
                    InheritVal ||
                    (IsPublished && isMaster && IsForcedNo)
                    );
                var parentExpanded = collapseToAssociated && hasAssociations;
                var childExpanded = !collapseToAssociated && IsExpandedNode;

                var forceNoText = (FNProgramId != Guid.Empty) ? " ( Disassociated via master )" : "";

                var ySel = IsPublished && (AssocOverrideTri == E_TriState.Yes) && !(isMaster && FNProgramId != Guid.Empty);
                var yDis = IsPublished && (FNProgramId != Guid.Empty);

                var nSel = IsPublished && (AssocOverrideTri == E_TriState.No) && !(isMaster && FNProgramId != Guid.Empty);
                var nDis = IsPublished && (FNProgramId != Guid.Empty);
                var bSel = IsPublished && InheritValFromBaseProd;
                var mSel = IsPublished && InheritVal;
                var forceNo = IsPublished && (isMaster && (IsForcedNo || (FNProgramId != Guid.Empty)));
                var forceNoDisable = IsPublished && (isMaster && FNProgramId != Guid.Empty);

                policies.Add(new
                {
                    PricePolicyID,
                    ParentPolicyId,
                    PricePolicyDescription,
                    IsPublished,
                    AssocOverrideTri,
                    InheritVal,
                    InheritValFromBaseProd,
                    IsForcedNo,
                    IsExpandedNode,

                    FNProgramId,

                    hasAssociations,
                    parentExpanded,
                    childExpanded,

                    ySel, yDis,
                    nSel, nDis,
                    bSel, mSel,
                    forceNo, forceNoDisable,
                });
            }

            return new
            {
                mode = AssociationTypeForUi.Program,
                isMaster,

                policies,
            };
        }

        private static object DisplayProgramInPriceGroupAssocTree( bool isBatchMode, Guid loanProductID, Guid priceGroupID, bool collapseToAssociated)
        {
            var ds = new DataSet();
            if (!isBatchMode)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@LoanProductId", loanProductID)
                                                , new SqlParameter("@PriceGroupId", priceGroupID)
                                            };
                DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByLoanProductIdPriceGroupId", parameters);
            }
            else
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@PriceGroupId", priceGroupID)
                                            };

                DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByPriceGroupId" /* Batch mode can still use the PG associations */, parameters);

            }

            var policies = new List<object>();
            foreach( DataRow row in ds.Tables[ 0 ].Rows )
            {
                var PricePolicyID = (Guid)row["PricePolicyID"];
                var ParentPolicyId = row["ParentPolicyId"] == DBNull.Value ? null : (Guid?)row["ParentPolicyId"];
                var PricePolicyDescription = WebUtility.HtmlEncode((string)row["PricePolicyDescription"]);

                var IsPublished = (bool)row["IsPublished"];
                var AssocOverrideTri = (isBatchMode || row["AssocOverrideTri"] is DBNull) ? E_TriState.Blank : (E_TriState)(byte)row["AssocOverrideTri"];
                var PriceGroupAssociateType = row["PriceGroupAssociateType"] is DBNull ? E_TriState.Blank : (E_TriState)(byte)row["PriceGroupAssociateType"];
                var PriceGroupProgramAssociateType = (isBatchMode || row["PriceGroupProgramAssociateType"] is DBNull) ? E_TriState.Blank : (E_TriState)(byte)row["PriceGroupProgramAssociateType"];
                var InheritVal = !isBatchMode && !(row["InheritVal"] is DBNull) && (bool)row["InheritVal"];
                var InheritValFromBaseProd = !isBatchMode && !(row["InheritValFromBaseProd"] is DBNull) && (bool)row["InheritValFromBaseProd"];
                var IsExpandedNode = (bool)row["IsExpandedNode"];

                var hasAssociations = IsPublished && (
                    (PriceGroupAssociateType == E_TriState.Yes || PriceGroupAssociateType == E_TriState.No) ||
                    (!isBatchMode && (
                        (PriceGroupProgramAssociateType == E_TriState.Yes || PriceGroupProgramAssociateType == E_TriState.No) ||
                        ((AssocOverrideTri == E_TriState.Yes) || (AssocOverrideTri == E_TriState.Blank && (InheritValFromBaseProd || InheritVal)))
                    ))
                  );
                var parentExpanded = collapseToAssociated && (
                    hasAssociations ||
                    (!IsPublished && !isBatchMode && PriceGroupProgramAssociateType != E_TriState.Blank)
                    );

                var childExpaned = !collapseToAssociated && IsExpandedNode;

                var ySel = IsPublished && !isBatchMode && PriceGroupProgramAssociateType == E_TriState.Yes;
                var nSel = IsPublished && !isBatchMode && PriceGroupProgramAssociateType == E_TriState.No;
                var lSel = IsPublished && !isBatchMode && ((AssocOverrideTri == E_TriState.Yes) || (AssocOverrideTri == E_TriState.Blank && (InheritValFromBaseProd || InheritVal)));

                var pGySel = IsPublished && PriceGroupAssociateType == E_TriState.Yes;
                var pGnSel = IsPublished && PriceGroupAssociateType == E_TriState.No;

                policies.Add(new
                {
                    PricePolicyID,
                    ParentPolicyId,
                    PricePolicyDescription,
                    IsPublished,
                    IsExpandedNode,
                    AssocOverrideTri,
                    PriceGroupAssociateType,
                    PriceGroupProgramAssociateType,
                    InheritVal,
                    InheritValFromBaseProd,

                    hasAssociations,
                    parentExpanded,
                    childExpaned,

                    ySel, nSel, lSel, pGySel, pGnSel,
                });
            }

            return new
            {
                mode = isBatchMode ? AssociationTypeForUi.ProgramInPriceGroupBatch : AssociationTypeForUi.ProgramInPriceGroup,
                policies,
            };
        }

        private static object DisplayPriceGroupAssocTree(Guid priceGroupId, bool collapseToAssociated)
        {
            // 05/13/09 mf. OPM 21028. Directly associating a policy with a Price Group.
            // We render this view in the following format:
            // []Y []N
            // There is no read-only data we can show here, so we only have mutually
            // exclusive Y/N setting

            var ds = new DataSet();
            DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByPriceGroupId", new[] { new SqlParameter("@PriceGroupId", priceGroupId) });

            var policies = new List<object>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var PricePolicyID = (Guid)row["PricePolicyID"];
                var ParentPolicyId = row["ParentPolicyId"] == DBNull.Value ? null : row["ParentPolicyId"];
                var IsPublished = (bool)row["IsPublished"];
                var PriceGroupAssociateType = row["PriceGroupAssociateType"] is DBNull ? E_TriState.Blank : (E_TriState)(byte)row["PriceGroupAssociateType"];
                var PricePolicyDescription = WebUtility.HtmlEncode((string)row["PricePolicyDescription"]);
                var IsExpandedNode = (bool)row["IsExpandedNode"];

                var parentExpanded = (PriceGroupAssociateType != E_TriState.Blank) && (!IsPublished || collapseToAssociated);
                var childExpaned = !collapseToAssociated && IsExpandedNode;
                var ySel = IsPublished && PriceGroupAssociateType == E_TriState.Yes;
                var nSel = IsPublished && PriceGroupAssociateType == E_TriState.No;

                policies.Add(new
                {
                    PricePolicyID,
                    ParentPolicyId,
                    PricePolicyDescription,

                    IsPublished,
                    PriceGroupAssociateType,
                    IsExpandedNode,

                    childExpaned,
                    parentExpanded,
                    ySel,
                    nSel,
                });
            }

            return new
            {
                mode = AssociationTypeForUi.PriceGroup,
                policies,
            };
        }

        private static bool CheckIsMaster(Guid fileId)
        {
            bool isMaster = false;
            DBSelectUtility.ProcessDBData(
                DataSrc.LpeSrc,
                "SELECT IsMaster FROM Loan_Program_Template WHERE lLpTemplateId = @p AND IsMaster = 1 AND IsMasterPriceGroup = 0",
                null,
                new[] {new SqlParameter("@p", fileId)}, reader => isMaster = reader.Read());
            return isMaster;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
            //this.PreRender += new System.EventHandler(this.PagePreRender);
        }
        #endregion

    }

}
