using System;
using System.Collections;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{

    // 05/05/2008 ThienNguyen : want to the new keyword's color is red in "dropdownlist". However
    // The Attributes property of a ListItem control only works within an HtmlSelect control
    //      http://support.microsoft.com/default.aspx?scid=kb;en-us;Q309338
    // => convert from DropDownlist to HtmlSelect because

    public partial class RulesEdit : LendersOffice.Common.BasePage  
	{
        private const string BORROWER_NAME = "<BorrowerName>";
        private Guid m_newRuleID;
	

        static void HtmlSelectBinding( HtmlSelect control, string [] keywords, Hashtable newKeywords )
        {
            control.EnableViewState = false;
            foreach( string keyword in keywords )
            {
                bool   isNewKeyword = newKeywords.Contains( keyword );
                string postfix      = isNewKeyword ? "    <- new keyword" : "";
                ListItem item = new ListItem(keyword + postfix, keyword);
                if( isNewKeyword )
                    item.Attributes.Add("style", "color:red");
                control.Items.Add( item );
            }
        }
        

        private void  bindConditionKeywords() 
		{
            CCondition condition = CCondition.CreateTestCondition( "" );
            Hashtable newKeywords = new Hashtable();
            HtmlSelectBinding( Symbol_dd, condition.GetValidSymbols( newKeywords ), newKeywords );
		}

		private void bindFeeKeywords()
		{
            CCondition condition = CCondition.CreateTestCondition( "" );
            Hashtable newKeywords = new Hashtable();
            HtmlSelectBinding( FeeKeywords_dd, condition.GetValidSymbols( newKeywords ), newKeywords );			
		}

		private void bindBaseMarginKeywords()
		{
            CCondition condition = CCondition.CreateTestCondition( "" );
            Hashtable newKeywords = new Hashtable();
            HtmlSelectBinding( BaseMarginKeywords_dd, condition.GetValidSymbols( newKeywords ), newKeywords );			
		}

        private void bindFrontEndMaxYspKeywords()
        {

            CCondition condition = CCondition.CreateTestCondition("");
            Hashtable newKeywords = new Hashtable();
            HtmlSelectBinding(FrontEndMaxYspKeywords_dd, condition.GetValidSymbols(newKeywords), newKeywords);
        }


		private void bindMarginKeywords()
		{
            CCondition condition = CCondition.CreateTestCondition( "" );
            Hashtable newKeywords = new Hashtable();
            HtmlSelectBinding( MarginKeywords_dd, condition.GetValidSymbols( newKeywords ), newKeywords );			
		}

		private void bindQscoreKeywords()
		{
            CCondition condition = CCondition.CreateTestCondition( "" );
            Hashtable newKeywords = new Hashtable();
            HtmlSelectBinding( QscoreKeywords_dd, condition.GetValidSymbols( newKeywords ), newKeywords );			

		}

        private void bindLockDaysKeywords()
        {
            CCondition condition = CCondition.CreateTestCondition("");
            Hashtable newKeywords = new Hashtable();
            HtmlSelectBinding(LockDaysKeywords_dd, condition.GetValidSymbols(newKeywords), newKeywords);			
        }

		protected void PageInit(object sender, System.EventArgs e) 
		{
			bindConditionKeywords();
			bindFeeKeywords();
			bindMarginKeywords();
			bindBaseMarginKeywords();
			bindQscoreKeywords();
            bindLockDaysKeywords();
            bindFrontEndMaxYspKeywords();
            Tools.Bind_RuleQBCType(m_qbcList);  //opm 25872 fs 03/26/09
		}

        private Guid m_ruleID 
        {
            get { return RequestHelper.GetGuid("ruleid"); }
        }

        private Guid m_pricePolicyID
        {
            get { return RequestHelper.GetGuid("pricepolicyid"); }
        }
        private string m_cmd 
        {
            get { return RequestHelper.GetSafeQueryString("cmd"); }
        }

		private void LoadData()
		{
            if (m_cmd != "add") 
            {
                CPricePolicy pricePolicy = CPricePolicy.Retrieve( m_pricePolicyID );
                CRule rule = pricePolicy.GetRule(m_ruleID);
            
                Description_ed.Text = rule.Description;
                Condition_ed.Text = rule.Condition.UserExpression;
                CConsequence consequence = rule.Consequence;
                Disqualify_chk.Checked = consequence.Disqualify;
				Skip_chk.Checked = consequence.Skip;
                FeeDelta_ed.Text = consequence.FeeAdjustTarget.UserExpression;
                MarginDelta_ed.Text = consequence.MarginAdjustTarget.UserExpression;
                RateDelta_ed.Text = consequence.RateAdjustTarget.UserExpression;
				QltvDelta_ed.Text = consequence.QltvAdjustTarget.UserExpression;
				QcltvDelta_ed.Text = consequence.QcltvAdjustTarget.UserExpression;
                MaxFrontEndYsp_ed.Text = consequence.MaxFrontEndYspAdjustTarget.UserExpression;
                MaxYspDelta_ed.Text = consequence.MaxYspAdjustTarget.UserExpression;
				Qscore_ed.Text = consequence.QscoreTarget.UserExpression;
				BaseMargin_ed.Text = consequence.MarginBaseTarget.UserExpression;
                Stipulation_ed.Text = consequence.Stipulation;
				QrateDelta_ed.Text = consequence.QrateAdjustTarget.UserExpression;
				TeaserRateDelta_ed.Text = consequence.TeaserRateAdjustTarget.UserExpression;
                MaxDti_ed.Text = consequence.MaxDtiTarget.UserExpression;
                QLAmtDelta_ed.Text = consequence.QLAmtAdjustTarget.UserExpression;
                LockDays_ed.Text = consequence.LockDaysAdjustTarget.UserExpression; //opm 17988 fs 09/29/09

                //opm 25872 fs 03/26/09
                Tools.SetDropDownListValue(m_qbcList, rule.QBC);
                //opm 32459 fs 09/08/09
                LenderRule_chk.Checked = rule.IsLenderRule;
            }
		}
		private void SaveData()
		{
			CPricePolicy pricePolicy = new CPricePolicy( m_pricePolicyID);
            CRule rule = null;

            if (m_cmd == "add") 
            {
                rule = pricePolicy.AddNewRule();
                m_newRuleID = rule.RuleId;
            } 
            else 
            {
                rule = pricePolicy.GetRule( m_ruleID );
            }
            
			rule.Description = Description_ed.Text;
			rule.Condition.UserExpression = Condition_ed.Text;
			
			CConsequence consequence = rule.Consequence;
			consequence.Disqualify		= Disqualify_chk.Checked;
			consequence.Skip			= Skip_chk.Checked;
			consequence.FeeAdjustTarget.UserExpression = FeeDelta_ed.Text;
			consequence.MarginAdjustTarget.UserExpression = MarginDelta_ed.Text;
			consequence.RateAdjustTarget.UserExpression = RateDelta_ed.Text;
			consequence.MarginBaseTarget.UserExpression = BaseMargin_ed.Text;
			consequence.Stipulation = Stipulation_ed.Text;

			consequence.QltvAdjustTarget.UserExpression = QltvDelta_ed.Text;
			consequence.QcltvAdjustTarget.UserExpression = QcltvDelta_ed.Text;
            consequence.MaxFrontEndYspAdjustTarget.UserExpression = MaxFrontEndYsp_ed.Text;
            consequence.MaxYspAdjustTarget.UserExpression = MaxYspDelta_ed.Text;
			consequence.QscoreTarget.UserExpression = Qscore_ed.Text;
            consequence.MaxDtiTarget.UserExpression = MaxDti_ed.Text;
            consequence.QLAmtAdjustTarget.UserExpression = QLAmtDelta_ed.Text;

			consequence.QrateAdjustTarget.UserExpression = QrateDelta_ed.Text;
			consequence.TeaserRateAdjustTarget.UserExpression = TeaserRateDelta_ed.Text;

            consequence.LockDaysAdjustTarget.UserExpression = LockDays_ed.Text; //opm 17988 fs 09/29/09

            //opm 25872 fs 03/26/09          
            rule.QBC = (E_sRuleQBCType)Tools.GetDropDownListValue(m_qbcList);

            //opm 32459 fs 09/08/09
            rule.IsLenderRule = LenderRule_chk.Checked;

			pricePolicy.Save();
		}

        protected void PageLoad(object sender, System.EventArgs e)
        {
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

            if( !IsPostBack ) 
            {
                LoadData();
            } 
        }

	
		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }
		
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
		#endregion


		protected void Syntax_btn_Click( object sender, System.EventArgs e)
		{
			ValidateSyntax();
		}

		private bool ValidateSyntax()
		{
			XmlDocument xmlDocEval = new XmlDocument();
			bool bOk = true;

            string QBCmsg = "";
            if (isQBCValid(out QBCmsg))
            {
                QBCResult_ed.ForeColor = Color.Green;
                QBCResult_ed.Text = "QBC logic is valid.";
            }
            else
            {
                QBCResult_ed.ForeColor = Color.Red;
                QBCResult_ed.Text = QBCmsg;
                bOk = false;
            }

			try
			{
				
				CCondition condition = CCondition.CreateTestCondition( Condition_ed.Text );
                ConditionResult_ed.ForeColor = Color.Green;
				ConditionResult_ed.Text = "Syntax is OK. Internal test value = " + ( condition.EvaluateTest(xmlDocEval) ? "1" : "0" );
			}
			catch( Exception ex )
			{
				ConditionResult_ed.Text = "Condition syntax is not valid. ";
				ConditionResult_ed.Text += CBaseException.GetVerboseMessage( ex );
                ConditionResult_ed.ForeColor = Color.Red;
				bOk = false;
			}

			try
			{
				CAdjustTarget target  = CAdjustTarget.CreateTestAdjustTarget( CAdjustTarget.E_Target.Fee,FeeDelta_ed.Text );
                FeeResult_ed.ForeColor = Color.Green;
				FeeResult_ed.Text = "Syntax is OK. Internal test value = " + target.EvaluateTest(xmlDocEval).ToString();
			}
			catch( Exception ex )
			{
				FeeResult_ed.Text = "Condition syntax is not valid. ";
                FeeResult_ed.ForeColor = Color.Red;
				FeeResult_ed.Text += CBaseException.GetVerboseMessage( ex );
				bOk = false;
			}

			try
			{
				CAdjustTarget target  = CAdjustTarget.CreateTestAdjustTarget( CAdjustTarget.E_Target.Margin,MarginDelta_ed.Text );
                MarginResult_ed.ForeColor = Color.Green;
				MarginResult_ed.Text = "Syntax is OK. Internal test value = " + target.EvaluateTest(xmlDocEval).ToString();
			}
			catch( Exception ex )
			{
                MarginResult_ed.ForeColor = Color.Red;
				MarginResult_ed.Text = "Condition syntax is not valid. ";
				MarginResult_ed.Text += CBaseException.GetVerboseMessage( ex );
				bOk = false;
			}

            try
            {
                CAdjustTarget target = CAdjustTarget.CreateTestAdjustTarget(CAdjustTarget.E_Target.MaxFrontEndYsp, MaxFrontEndYsp_ed.Text);
                FrontEndMaxYspResult_ed.ForeColor = Color.Green;
                FrontEndMaxYspResult_ed.Text = "Syntax is OK. Internal test value = " + target.EvaluateTest(xmlDocEval).ToString();
            }
            catch (Exception ex)
            {
                FrontEndMaxYspResult_ed.ForeColor = Color.Red;
                FrontEndMaxYspResult_ed.Text = "Condition syntax is not valid. ";
                FrontEndMaxYspResult_ed.Text += CBaseException.GetVerboseMessage(ex);
                bOk = false;
            }

			try
			{
				CAdjustTarget target  = CAdjustTarget.CreateTestAdjustTarget( CAdjustTarget.E_Target.BaseMargin, BaseMargin_ed.Text );
				BaseMarginResult_ed.ForeColor = Color.Green;
				BaseMarginResult_ed.Text = "Syntax is OK. Internal test value = " + target.EvaluateTest(xmlDocEval).ToString();
			}
			catch( Exception ex )
			{
				BaseMarginResult_ed.ForeColor = Color.Red;
				BaseMarginResult_ed.Text = "Condition syntax is not valid. ";
				BaseMarginResult_ed.Text += CBaseException.GetVerboseMessage( ex );
				bOk = false;
			}

			try
			{
				CAdjustTarget target  = CAdjustTarget.CreateTestAdjustTarget( CAdjustTarget.E_Target.Qscore, Qscore_ed.Text );
				QscoreResult_ed.ForeColor = Color.Green;
				QscoreResult_ed.Text = "Syntax is OK. Internal test value = " + target.EvaluateTest(xmlDocEval).ToString();
			}
			catch( Exception ex )
			{
				QscoreResult_ed.ForeColor = Color.Red;
				QscoreResult_ed.Text = "Condition syntax is not valid. ";
				QscoreResult_ed.Text += CBaseException.GetVerboseMessage( ex );
				bOk = false;
			}

            try 
            {
                CAdjustTarget target = CAdjustTarget.CreateTestAdjustTarget(CAdjustTarget.E_Target.MaxDti, MaxDti_ed.Text);
                MaxDtiResult_ed.ForeColor = Color.Green;
                MaxDtiResult_ed.Text = "Syntax is OK. Internal test value = " + target.EvaluateTest(xmlDocEval).ToString();
            } 
            catch (Exception ex) 
            {
                MaxDtiResult_ed.ForeColor = Color.Red;
                MaxDtiResult_ed.Text = "Condition syntax is not valid. " + CBaseException.GetVerboseMessage( ex );
				bOk = false;
            }
            try
            {
                CAdjustTarget target  = CAdjustTarget.CreateTestAdjustTarget( CAdjustTarget.E_Target.QLAmt,QLAmtDelta_ed.Text );
                QLAmtResult_ed.ForeColor = Color.Green;
                QLAmtResult_ed.Text = "Syntax is OK. Internal test value = " + target.EvaluateTest(xmlDocEval).ToString();
            }
            catch( Exception ex )
            {
                QLAmtResult_ed.ForeColor = Color.Red;
                QLAmtResult_ed.Text = "Condition syntax is not valid. ";
                QLAmtResult_ed.Text += CBaseException.GetVerboseMessage( ex );
                bOk = false;
            }

            //opm 17988 fs 09/28/09
            try
            {
                CAdjustTarget target = CAdjustTarget.CreateTestAdjustTarget(CAdjustTarget.E_Target.LockDaysAdjust, LockDays_ed.Text);
                LockDaysResult_ed.ForeColor = Color.Green;
                LockDaysResult_ed.Text = "Syntax is OK. Internal test value = " + target.EvaluateTest(xmlDocEval).ToString();
                if (Regex.Match(LockDays_ed.Text, @"\bLockDays\b", RegexOptions.IgnoreCase).Success)
                    throw new Exception("LockDays Adjustment cannot contain 'LockDays' keyword.");

            }
            catch (Exception ex)
            {
                LockDaysResult_ed.ForeColor = Color.Red;
                LockDaysResult_ed.Text = "Condition syntax is not valid. ";
                LockDaysResult_ed.Text += CBaseException.GetVerboseMessage(ex);
                bOk = false;
            }

			return bOk;
		}

        protected void onOKClick(object sender, System.EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid) 
            {
                if (ValidateSyntax()) 
                {
                    SaveData();
                } 
                else 
                {
                    return;
                }
                LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog( Page , "OK" , "true" );
            }
        }

        protected void onApplyClick(object sender, System.EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid) 
            {
                if (ValidateSyntax()) 
                {
                    SaveData();
                    if (m_cmd == "add") 
                    {
                        Response.Redirect("RulesEdit.aspx?pricepolicyid=" + m_pricePolicyID + "&ruleid=" + m_newRuleID);
                    }
                } 
            }
        }

        //opm 25872 fs 04/17/09          
        private bool isQBCValid(out string msg)
        {
            E_sRuleQBCType newVal = (E_sRuleQBCType)Tools.GetDropDownListValue(m_qbcList);

            if (newVal != E_sRuleQBCType.Legacy)
            {
                if (Skip_chk.Checked ||
                       RateDelta_ed.Text.Length != 0 ||
                       FeeDelta_ed.Text.Length != 0 ||
                       MarginDelta_ed.Text.Length != 0 ||
                       QrateDelta_ed.Text.Length != 0 ||
                       TeaserRateDelta_ed.Text.Length != 0 ||
                       MaxFrontEndYsp_ed.Text.Length != 0 ||
                       MaxYspDelta_ed.Text.Length != 0 ||
                       Qscore_ed.Text.Length != 0 ||
                       QltvDelta_ed.Text.Length != 0 ||
                       QcltvDelta_ed.Text.Length != 0 ||
                       QLAmtDelta_ed.Text.Length != 0)
                {
                    msg = "If QBC is set to 'Primary / All / Coborrowers', [Skip,Rate,Fees,Margin,QRateAdj,TRate,MaxFrontEndYsp,MaxBackEndYsp,DTIRange,QScore,QLTV,QCLTV,QLAmt] need to remain blank.";
                    return false;
                }
            }
            else //QBC = blank (legacy)
            {
                if (Stipulation_ed.Text.IndexOf(BORROWER_NAME) != -1)
                {
                    msg = "If QBC is set to 'Legacy' (blank), the stip cannot contain '" + BORROWER_NAME + "'.";
                    return false;
                }
                if (Description_ed.Text.IndexOf(BORROWER_NAME) != -1)
                {
                    msg = "If QBC is set to 'Legacy' (blank), the rule description cannot contain '" + BORROWER_NAME + "'.";
                    return false;
                }
            }

            msg = "";
            return true;
        }

    }
}