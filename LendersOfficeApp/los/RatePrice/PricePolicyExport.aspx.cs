using System;
using LendersOffice.Common;
using LendersOffice.RatePrice;
using LendersOffice.Security;

namespace LendersOfficeApp.los.RatePrice
{
    /// <summary>
    /// Summary description for PricePolicyExport.
    /// </summary>

    public partial class PricePolicyExport : BasePage
	{
		private BrokerUserPrincipal CurrentUser
		{
			// Access current user.
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		/// <summary>
		/// Export contents.
		/// </summary>
		protected void PageLoad( object sender , System.EventArgs a )
		{
            if (!CurrentUser.HasPermission(Permission.CanModifyLoanPrograms))
            {
                throw new DataAccess.CBaseException(ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
            }

            Guid pricePolicyId = RequestHelper.GetGuid("pricePolicyId");

            RateAdjustmentExporter exporter = new RateAdjustmentExporter();

            string outputFile = exporter.ExportToCsvFile(pricePolicyId);

            RequestHelper.SendFileToClient(Context, outputFile, "text/csv", "Policy.csv");
            Response.End();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}
}