using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOfficeApp.los.RatePrice.Helpers
{
	/// <summary>
	/// Summary description for ConditionGen.
	/// </summary>
	public partial class ConditionGen : LendersOffice.Common.BasePage
	{
		char[] SEPARATORS = new char[]{ (char)10, (char)13, } ;
		System.Collections.Specialized.ListDictionary m_ConditionColumns = new System.Collections.Specialized.ListDictionary() ;
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

			LoadColumns() ;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void Ok_btn_Click(object sender, System.EventArgs e)
		{
			string[] rows = Condition_ed.Text.Split(SEPARATORS) ;
			if (rows.Length == 0)
			{
				ConditionResult_ed.Text = "(EMPTY)" ;
				return ;
			}
			
			string[] columnNames = GetColumnNames(rows[0]) ;
			if (!ValidateColumnNames(columnNames))
			{
				ConditionResult_ed.Text = "THE COLUMN NAMES ARE INVALID" ;
				return ;
			}
			
			System.Text.StringBuilder sb = new System.Text.StringBuilder() ;
			/// We do not use the row # because rows.Split generates empty rows. This is because when you 
			/// copy & paste from excel, it pastes in both the carriage return as well as the newline characters.
			bool bFirstRow = true ;
			for (int nRow = 1 ; nRow < rows.Length ; nRow++)
			{
				if (rows[nRow].Length == 0) continue ;

				string[] rgValues = rows[nRow].Split('\t') ;
				if (rgValues.Length != columnNames.Length)
				{
					ConditionResult_ed.Text = "THE NUMBER OF VALUE CELLS DOESN'T MATCH THE NUMBER OF COLUMN NAMES" ;
					return ;
				}

				if (bFirstRow)
				{
					bFirstRow = false ;
					sb.Append("( ") ;
				}
				else
					sb.Append(" OR ( ") ;

				for (int nVal = 0 ; nVal < rgValues.Length ; nVal++)
				{
					ConditionGenColumn column = (ConditionGenColumn)m_ConditionColumns[columnNames[nVal]] ;
					if (nVal != 0)
						sb.Append(" AND ") ;
					sb.Append(column.ComposeStatement(rgValues[nVal])) ;
				}
				sb.Append(" )") ;
			}

			ConditionResult_ed.Text = sb.ToString() ;
		}

		private string[] GetColumnNames(string s)
		{
			string[] columnNames = s.Split('\t') ;

			// normalize the names
			for (int i = 0 ; i < columnNames.Length ; i++)
				columnNames[i] = columnNames[i].ToUpper() ;

			return columnNames ;
		}
		private bool ValidateColumnNames(string[] columnNames)
		{
			foreach (string columnName in columnNames)
			{
				if (!m_ConditionColumns.Contains(columnName)) return false ;
			}

			return true ;
		}

		private void LoadColumns()
		{
			m_ConditionColumns.Add("SCORE1", new ConditionGenColumn("SCORE1", ">=")) ;
			m_ConditionColumns.Add("SCORE2", new ConditionGenColumn("SCORE2", ">=")) ;
			m_ConditionColumns.Add("QSCORE", new ConditionGenColumn("QSCORE", ">=")) ;

			m_ConditionColumns.Add("LTV", new ConditionGenColumn("LTV", "<=")) ;
			m_ConditionColumns.Add("CLTV", new ConditionGenColumn("CLTV", "<=")) ;
			m_ConditionColumns.Add("QLTV", new ConditionGenColumn("QLTV", "<=")) ;
			m_ConditionColumns.Add("QCLTV", new ConditionGenColumn("QCLTV", "<=")) ;

			//m_ConditionColumns.Add("LOANAMOUNT", new ConditionGenColumn("LOANAMOUNT", "<=")) ; -- force authors to use LAMT field instead
			m_ConditionColumns.Add("LAMT", new ConditionGenColumn("LAMT", "<=")) ;
			m_ConditionColumns.Add("QLAMT", new ConditionGenColumn("QLAMT", "<=")) ;

			m_ConditionColumns.Add("CASHOUTAMT", new ConditionGenColumn("CASHOUTAMT", "<=")) ;
			m_ConditionColumns.Add("NUMBEROFUNITS", new ConditionGenColumn("NUMBEROFUNITS", "<=")) ;
			m_ConditionColumns.Add("UNITS", new ConditionGenColumn("UNITS", "<=")) ;

			m_ConditionColumns.Add("LTV1STLIEN", new ConditionGenColumn("LTV1STLIEN", "<=")) ;
			m_ConditionColumns.Add("LOANAMOUNT1STLIEN", new ConditionGenColumn("LOANAMOUNT1STLIEN", "<=")) ;

			m_ConditionColumns.Add("PROPERTYVALUE", new ConditionGenColumn("PROPERTYVALUE", "<=")) ;
		}
	}

	class ConditionGenColumn
	{
		string m_sColName ;
		string m_sValueOperator ;

		public ConditionGenColumn(string sColName, string sValueOperator)
		{
			m_sColName = sColName.ToUpper() ;
			m_sValueOperator = sValueOperator.ToUpper() ;
		}
		public string ComposeStatement(string sValue)
		{
			return string.Format("( {0} {1} {2} )", m_sColName, m_sValueOperator, sValue) ;
		}
	}
}
