using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using System.Xml;
using LendersOfficeApp.los.common;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice.Helpers
{
	/// <summary>
	/// Summary description for ConditionEdit.
	/// </summary>
	public partial class ConditionEdit : LendersOffice.Common.BasePage
	{
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

			CCondition condition = CCondition.CreateTestCondition( "" );
			Symbol_dd.DataSource = condition.ValidSymbols;
			Symbol_dd.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

		protected void Ok_btn_Click(object sender, System.EventArgs e)
		{
			UpdateText() ;
			ValidateSyntax() ;
		}
		private bool ValidateSyntax()
		{
			XmlDocument xmlDocEval = new XmlDocument();
			bool ok = true ;

			try
			{				
				CCondition condition = CCondition.CreateTestCondition( Condition_ed.Text );
				ConditionResult_ed.ForeColor = Color.Green;
				ConditionResult_ed.Text = "Syntax is OK. Internal test value = " + 
                                          ( condition.EvaluateTest(xmlDocEval) ? "1" : "0" );
			}
			catch( Exception ex )
			{
				ConditionResult_ed.Text = "Condition syntax is not valid. ";
				ConditionResult_ed.Text += ex.Message;
				ConditionResult_ed.ForeColor = Color.Red;
				ok = false ;
			}

			return ok ;
		}
		private void UpdateText()
		{
			const int spacer = 4 ;
			System.Text.StringBuilder sbExpanded = new System.Text.StringBuilder(Condition_ed.Text.Length*spacer) ;
			System.Text.StringBuilder sbCompacted = new System.Text.StringBuilder(Condition_ed.Text.Length*spacer) ;

			int depth = 0 ;
			bool opened = false ;
			string sText = Condition_ed.Text ;

			// strip out the double quote added to the text when we copy & paste a cell from Excel
			if (sText.Length >= 2 && sText[0] == '"' && sText[sText.Length-1] == '"')
				sText = sText.Substring(1, sText.Length-2) ;
			foreach(string token in sText.Split(SEPARATORS))
			{
				if (token == "") continue ;

				// compacted text
				sbCompacted.Append(token + " ") ;

				// expanded text
				switch(token)
				{
					case "(":
						sbExpanded.Append("\n") ;
						sbExpanded.Append(' ', depth*spacer) ;
						sbExpanded.Append("( ") ;
						depth++ ;
						opened = true ;
						break ;
					case ")":
						depth = System.Math.Max(0, depth-1) ;

						if (opened)
							opened = false ;
						else
						{
							sbExpanded.Append('\n') ;
							sbExpanded.Append(' ', depth*spacer) ;
						}
						sbExpanded.Append(") ") ;

						break ;
					default:
						sbExpanded.Append(token + " ") ;
						break ;
				}
			}

			ConditionFlat_ed.Text = sbCompacted.ToString() ;
			Condition_ed.Text = sbExpanded.ToString() ;
		}
		private string CompactText(string s)
		{			
			return string.Join(" ", s.Split(SEPARATORS)) ;
		}

		readonly char[] SEPARATORS = new char[]{ ' ', (char)10, (char)13, '\t' } ;
	}
}
