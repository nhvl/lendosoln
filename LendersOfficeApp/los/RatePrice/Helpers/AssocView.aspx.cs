namespace LendersOfficeApp.los.RatePrice.Helpers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.DynaTree;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.Security;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// TODO: ProductTree view currently does not display relationships between children of a policy and products.
    /// </summary>
    public partial class AssocView : BasePage
	{
        private readonly int sqlParameterChunkSize = ConstStage.LpeUiSqlParameterChunkSize;

        [Flags]
        private enum AssocInfo
        {
            Blank = 0,
            Yes = 1,
            No = 2,
            FN = 4,
            InheritFromBaseProd = 8,
            InheritFromMasterProd = 16,
            IsMater = 32
        }

        private Hashtable hashNodes = new Hashtable();
        private Queue orphans = new Queue();

        private AssocViewNode rootNode = new AssocViewNode();

        protected bool m_isDetachView;
        DataTable errDataTable = null;

        // With program in price group, we logically want the node's key = (progId, priceGroupId). However with current code, node's key = Guid.
        // Therefore we create fake guid and map that id to (progId, priceGroupId)
        private Dictionary<Guid, Tuple<Guid, Guid>> m_prgInPricegroupDict = null;

		private BrokerUserPrincipal BrokerUser
		{
			// Access denied.
            get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		class Orphan
		{
			public Orphan(Guid id, Guid parentId)
			{
				m_id = id ;
				m_parentId = parentId ;
			}
			public Guid Id { get { return m_id ; } }
			public Guid ParentId { get { return m_parentId ; } }

			private Guid m_id ;
			private Guid m_parentId ;
		}

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        protected void PageLoad(object sender, System.EventArgs e)
		{
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");

            RegisterJsGlobalVariables("isGlobalAssoc", m_viewLevel.SelectedIndex == 0);

            // 4/18/2005 kb - Protect this view.  Only special users can
            // access this content.

            m_isDetachView = RequestHelper.GetSafeQueryString("action") == "detach";
            m_batchDetachBtn.Visible = m_setInheritBtn.Visible = false;
            m_errMsgLabel.Visible = false;

            if ( BrokerUser.HasPermission( Permission.CanModifyLoanPrograms ) == true )
			{
				Guid id  ;
				if ((id = RequestHelper.GetGuid("pricePolicyId", Guid.Empty)) != Guid.Empty)
				{
                    if (IsPostBack)
                    {
                        if (cmdToDo.Value == "batchDetach" || cmdToDo.Value == "setInherit")
                        {
                            bool isbatchDetach = cmdToDo.Value == "batchDetach";
                            BatchDetachPolicy(id, isbatchDetach);

                            if (errDataTable != null)
                            {
                                m_errMsgLabel.Visible = true;
                                m_errMsgLabel.Text = isbatchDetach ? $"Can not attach 'N' for {errDataTable.Rows.Count} following programs." 
                                                                   : $"Can not clear attachment for {errDataTable.Rows.Count} following programs.";

                                this.dgErrPrograms.DataSource = errDataTable.DefaultView;
                                this.dgErrPrograms.DataBind();
                            }
                        }
                    }

                    if(errDataTable == null)
                    {
                        if (m_viewLevel.SelectedIndex == 0)
                        {
                            DisplayProductTree(id);
                        }
                        else if (m_viewLevel.SelectedIndex == 1)
                        {
                            DisplayProductTreeForPriceGroup(id);
                        }
                        else
                        {
                            DisplayPriceGroupTree(id);
                        }

                        if (m_isDetachView)
                        {
                            if (m_viewLevel.SelectedIndex == 0)
                            {
                                m_setInheritBtn.Visible = true;
                            }

                            m_batchDetachBtn.Text = m_viewLevel.SelectedIndex == 0 ? "Attach \"N\"" : "Remove Attachment";
                            m_batchDetachBtn.Visible = true;
                            m_batchDetachBtn.Enabled = this.rootNode.children.Count > 0;
                        }
                    }
                }
				else if ((id = RequestHelper.GetGuid("productFolderId", Guid.Empty)) != Guid.Empty)
				{
                    m_isDetachView = false;
                    if ( m_viewLevel.SelectedIndex == 0 )
					{
						DisplayPolicyTree(id) ;
                    }
					else
					{
						DisplayPolicyTreeForPriceGroup(id);
                    }

                    m_viewLevel.Items[2].Enabled = false;
				}

				m_Denied.Visible = false ;
			}
			else
			{
				m_Denied.Visible = true ;
			}

            cmdToDo.Value = string.Empty;
            m_detachIds.Value = string.Empty;
            this.ViewState["prgInPriceGroupDict"] = this.m_prgInPricegroupDict;

            RegisterJsObjectWithJsonNetSerializer("nodes", this.rootNode.children);
        }

		private void DisplayProductTreeForPriceGroup ( Guid PricePolicyId )
		{
            var brokerDictionary = BrokerDbLite.ListAllSlow();
            //07/07/09 mf. OPM 21028. Display the Pricegroup associations
            Dictionary<Guid, E_TriState> PriceGroupAssociations = new Dictionary<Guid, E_TriState>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(
                DataSrc.LpeSrc
                , "ListPriceGroupPolicyAssociationByPolicyId"
                       , new SqlParameter("@PricePolicyId", PricePolicyId)
                       ))
            {
                while (reader.Read())
                {
                    PriceGroupAssociations.Add((Guid)reader["LpePriceGroupId"], (E_TriState) (Byte) reader["AssocOverrideTri"]);
                }
            }
            var priceGroupDictionary = PriceGroup.ListAllSlow();

            // mapping fake guid to (programId, priceGroupId)
            m_prgInPricegroupDict = m_isDetachView ? new Dictionary<Guid, Tuple<Guid, Guid>>() : null;

            // 05/05/08 mf. OPM 21028.  We display a new view for price group association.
			using (DbDataReader reader 
					   = StoredProcedureHelper.ExecuteReader(
					   DataSrc.LpeSrc
					   , "ListProgramPriceGroupPricePolicyAssociationsByPolicyId"
					   , new SqlParameter( "@PricePolicyId", PricePolicyId ) 
					   ) )
			{
				while (reader.Read())
				{
                    Guid PriceGroupId = (Guid) reader["LpePriceGroupId"];
                    string pGAssociationString = string.Empty;
                    if (PriceGroupAssociations.ContainsKey(PriceGroupId))
                        pGAssociationString = PriceGroupAssociations[PriceGroupId] == E_TriState.Yes ? " : Y" : " : N";

                    Guid brokerId = (Guid)reader["BrokerId"];
                    BrokerDbLite brokerDbLite = brokerDictionary[brokerId];

                    string name = brokerDbLite.BrokerNm + " (" + brokerDbLite.CustomerCode + ")";
					// Add PriceGroup node if needed
					AddNode( 
						PriceGroupId
						, priceGroupDictionary[PriceGroupId].Name + pGAssociationString
						, brokerId
						, name
						);

                    // Add Program Node
                    Guid makeUpId = Guid.NewGuid();  // We need to be unique within this tree, and nothing will attach.;
                    var node = AddNode(
                        makeUpId
                        , reader["lLpTemplateNm"].ToString() + ( ( (E_TriState) (Byte) reader["AssociateType"] == E_TriState.Yes ) ? " : Y" : " : N")
						, PriceGroupId
                        );
                    node.Type = "Product";
                    node.CheckBox = m_isDetachView;
                    if (m_isDetachView)
                    {
                        node.addClass = node.addClass.Replace("hide-checkbox", string.Empty);
                    }

                    if (m_prgInPricegroupDict != null)
                    {
                        m_prgInPricegroupDict.Add(makeUpId, Tuple.Create((Guid)reader["ProgId"], PriceGroupId));
                    }
                }
			}
		}

        private void DisplayProductTree(Guid pricePolicyId)
        {
            Dictionary<string, AssocInfo> assocDict = new Dictionary<string, AssocInfo>();
            var dictionary = BrokerDbLite.ListAllSlow();

            m_lbLabel.Text = GetLabelText(pricePolicyId);

            PopulateDisplayTree(pricePolicyId, dictionary, this.hashNodes, this.rootNode, this.orphans, m_isDetachView, assocDict);

            while (this.orphans.Count > 0)
            {
                // take care of orphan folders
                var parameterList = new List<SqlParameter>();
                GatherOrphans(this.orphans, this.hashNodes, parameterList);

                if (this.orphans.Count == 0) break;

                AddOrphansToDisplayTree(parameterList, dictionary, this.hashNodes, this.rootNode, this.orphans);
            }

            if (this.rootNode.children.Count > 0)
            {
                // Show associations: InheritFromMasterProd "m", yes "Y", no "N", forceNo "FN". Also display checkbox for batch-detach
                DecorateProductTree(this.rootNode, assocDict, allowCheckbox: m_isDetachView);
            }
        }

        // Show associations: InheritFromMasterProd "m", yes "Y", no "N", forceNo "FN". Also display checkbox for batch-detach
        private void DecorateProductTree(AssocViewNode rootNode, Dictionary<string, AssocInfo> assocDict, bool allowCheckbox)
        {
            StringBuilder sb = new StringBuilder();

            Action<List<DynaTreeNode>, bool, bool> traversalChildren = null;

            traversalChildren = (List<DynaTreeNode> nodes, bool displayCheckbox, bool ancesstorFN) =>
            {
                bool haveFN = false;
                if (ancesstorFN)
                {
                    displayCheckbox = false;
                }

                if (!ancesstorFN)
                {
                    foreach (AssocViewNode tChild in nodes)
                    {
                        AssocInfo assocInfo;
                        if (assocDict.TryGetValue(tChild.key, out assocInfo) && assocInfo.HasFlag(AssocInfo.FN))
                        {
                            if (displayCheckbox)
                            {
                                tChild.CheckBox = true;
                                tChild.addClass = tChild.addClass.Replace("hide-checkbox", string.Empty);
                                displayCheckbox = false;
                            }

                            haveFN = true;
                        }
                    }
                }

                foreach (AssocViewNode tChild in nodes)
                {
                    if (displayCheckbox)
                    {
                        tChild.CheckBox = true;
                        tChild.addClass = tChild.addClass.Replace("hide-checkbox", string.Empty);
                    }

                    // Use PricePolicyListByProduct.DisplayProgramAssocTree()'s logic to display associations: "m", "Y", "N", "FN"

                    AssocInfo assocInfo;
                    if (assocDict.TryGetValue(tChild.key, out assocInfo))
                    {
                        string mSel = assocInfo.HasFlag(AssocInfo.InheritFromMasterProd) ? " checked" : string.Empty;
                        string ySel = assocInfo.HasFlag(AssocInfo.Yes) ? " checked" : string.Empty;
                        string nSel = assocInfo.HasFlag(AssocInfo.No) ? " checked" : string.Empty;
                        string fnSel = assocInfo.HasFlag(AssocInfo.FN) ? " checked" : string.Empty;
                        string forceNoText = string.Empty;
                        bool disAssocViaMaster = false;

                        if (assocInfo.HasFlag(AssocInfo.IsMater) && ancesstorFN)
                        {
                            ySel = nSel = string.Empty;
                            fnSel = " checked";
                            forceNoText = " ( Disassociated via master )";
                            disAssocViaMaster = true;
                        }

                        sb.Clear();
                        sb.Append("<span style='margin-right:6px;color:tomato'>");
                        sb.Append($"<INPUT type='radio' disabled {mSel}>m <INPUT type='checkbox' disabled {ySel}>Y ");
                        sb.Append($"<INPUT type='checkbox' disabled {nSel}> N");
                        if (assocInfo.HasFlag(AssocInfo.IsMater))
                        {
                            sb.Append($"<INPUT type='checkbox' disabled {fnSel}> FN").Append(forceNoText);

                        }
                        else if (ancesstorFN || haveFN)
                        {
                            sb.Append(" (Disassociated via master) ");
                            disAssocViaMaster = true;
                        }

                        sb.Append("</span>").AppendLine();

                        tChild.title += sb.ToString();
                        if (disAssocViaMaster)
                        {
                            tChild.addClass += " gray-color";
                        }
                    }
                    else
                    {
                        if (ancesstorFN || haveFN)
                        {
                            tChild.addClass += " gray-color";
                        }

                        traversalChildren(tChild.children, displayCheckbox, ancesstorFN || haveFN);
                    }
                }
            };            

            traversalChildren(rootNode.children, allowCheckbox, false);
        }

        private void AddOrphansToDisplayTree(List<SqlParameter> parameterList, IDictionary<Guid, BrokerDbLite> dictionary, Hashtable hashNodes, AssocViewNode rootNode, Queue orphans)
        {
            foreach (var chunk in parameterList.Chunk(sqlParameterChunkSize))
            {
                var sql = $@"
SELECT FolderId, ParentFolderId, FolderName, a.BrokerId 
FROM LOAN_PRODUCT_FOLDER a 
WHERE FolderId IN ({string.Join(",", chunk.Select(p => p.ParameterName))})";

                Action<IDataReader> readHandler = delegate (IDataReader sdr)
                {
                    while (sdr.Read())
                    {
                        if (DBNull.Value.Equals(sdr["ParentFolderId"]))
                        {
                            Guid brokerId = (Guid)sdr["BrokerId"];
                            string brokerNm = dictionary[brokerId].BrokerNm;
                            GetNodeData((Guid)sdr["FolderId"], sdr["FolderName"].ToString(), brokerId, brokerNm, hashNodes, rootNode, orphans);
                        }
                        else
                        {
                            GetNodeData((Guid)sdr["FolderId"], sdr["FolderName"].ToString(), (Guid)sdr["ParentFolderId"], null, hashNodes, rootNode, orphans);
                        }
                    }
                };

                DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql, null, chunk, readHandler);
            }
        }

        private void GatherOrphans(Queue orphans, Hashtable hashNodes, List<SqlParameter> parameterList)
        {
            for (int i = orphans.Count - 1; i >= 0; i--)
            {
                Orphan orphan = (Orphan)orphans.Dequeue();
                AssocViewNode parent = (AssocViewNode)hashNodes[orphan.ParentId];

                if (parent != null)
                {
                    parent.children.Add((AssocViewNode)hashNodes[orphan.Id]);
                }                                        
                else
                {
                    var paramName = "@p" + parameterList.Count;
                    parameterList.Add(new SqlParameter(paramName, orphan.ParentId));
                    orphans.Enqueue(orphan);
                }
            }
        }

        private void PopulateDisplayTree(Guid pricePolicyId, IDictionary<Guid, BrokerDbLite> dictionary, Hashtable hashNodes, 
                                               AssocViewNode rootNode, Queue orphans, bool viewDetach, Dictionary<string, AssocInfo> assocDict)
        {
            // 07/18/07 mf. OPM 4873.  We now also want to list Disassociations here.
            // Note that we only show the program that has the FN setting, not all
            // the child masters that inherit it.
            var listParams = new SqlParameter[] { new SqlParameter("@p", pricePolicyId) };
            string sSql = "SELECT d.BrokerId, c.FolderId, c.ParentFolderId, FolderName, lLpTemplateNm, lLpTemplateId, AssocOverrideTri, d.IsMaster, " +
                "IsForcedNo, InheritValFromBaseProd, InheritVal " +
                "FROM PRICE_POLICY b, LOAN_PROGRAM_TEMPLATE d left join LOAN_PRODUCT_FOLDER c on d.FolderId = c.FolderId, PRODUCT_PRICE_ASSOCIATION e " +
                "WHERE b.PricePolicyId = @p AND b.PricePolicyId = e.PricePolicyId AND e.ProductId = d.lLpTemplateId "+
                "AND ((e.InheritVal=1 and e.AssocOverrideTri=0) @orViewDetach or e.AssocOverrideTri=1 or e.IsForcedNo=1) AND d.IsMasterPriceGroup=0" +
                "ORDER BY d.BrokerId, FolderName, lLpTemplateNm";

            sSql = sSql.Replace("@orViewDetach", viewDetach ? "or e.AssocOverrideTri=2 " : string.Empty); // AssocOverrideTri=2 means "no" association.
            
            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                while (sdr.Read())
                {
                    Guid brokerId = (Guid)sdr["BrokerId"];
                    if (dictionary.ContainsKey(brokerId) == false)
                    {
                        continue;
                    }

                    string brokerNm = dictionary[brokerId].BrokerNm;

                    AssocViewNode folderNode = new AssocViewNode();

                    if (DBNull.Value.Equals(sdr["FolderId"]))
                    {
                        // This product doesn't belong any loan folder. Create a broker folder for this product.
                        folderNode = GetBrokerNode(brokerId, brokerNm, hashNodes, rootNode);
                    }
                    else if (DBNull.Value.Equals(sdr["ParentFolderId"]))
                    {
                        folderNode = GetNodeData((Guid)sdr["FolderId"], sdr["FolderName"].ToString(), brokerId, brokerNm, hashNodes, rootNode, orphans);
                    }
                    else
                    {
                        folderNode = GetNodeData((Guid)sdr["FolderId"], sdr["FolderName"].ToString(), (Guid)sdr["ParentFolderId"], null, hashNodes, rootNode, orphans);
                    }

                    string productName = sdr["lLpTemplateNm"].ToString() ?? string.Empty.Trim();
                    if(productName == string.Empty)
                    {
                        productName = sdr["lLpTemplateId"].ToString();
                    }

                    AssocViewNode productNode = new AssocViewNode();
                    productNode.title = productName;
                    productNode.key = sdr["lLpTemplateId"].ToString();
                    productNode.Type = "Product";

                    AssocInfo assocInfo = AssocInfo.Blank;

                    if ((bool)sdr["InheritValFromBaseProd"])
                    {
                        assocInfo |= AssocInfo.InheritFromBaseProd;
                    }

                    if ((bool)sdr["InheritVal"])
                    {
                        assocInfo |= AssocInfo.InheritFromMasterProd;
                    }


                    if ((bool)sdr["IsMaster"])
                    {
                        productNode.addClass += " blue-color";
                        assocInfo |= AssocInfo.IsMater;
                    }

                    if ((bool)sdr["IsForcedNo"])
                    {
                        assocInfo |= AssocInfo.FN;
                        productNode.addClass += " red-color";
                    }

                    switch( (E_TriState)(Byte)sdr["AssocOverrideTri"] )
                    {
                    case E_TriState.Yes:
                        assocInfo |= AssocInfo.Yes;
                        break;

                    case E_TriState.No:
                        assocInfo |= AssocInfo.No;
                        break;
                    }

                    assocDict.Add(productNode.key, assocInfo);
                    folderNode.children.Add(productNode);
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sSql, null, listParams, readHandler);
        }

        private string GetLabelText(Guid pricePolicyId)
        {
            var listParams = new SqlParameter[] { new SqlParameter("@p", pricePolicyId) };
            string sSql = "SELECT PricePolicyDescription FROM PRICE_POLICY WHERE PricePolicyId = @p";

            string label = null;
            Action<IDataReader> readHandler = delegate (IDataReader sdr)
            {
                sdr.Read();
                label = sdr["PricePolicyDescription"].ToString() + "::" + pricePolicyId.ToString();
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sSql, null, listParams, readHandler);
            return label;
        }

        private void DisplayPolicyTreeForPriceGroup(Guid productFolderId)
        {
            // 05/05/08 mf. OPM 21028.  We display a new view for price group association.

            this.m_lbLabel.Text = RetrieveFolderLabel(productFolderId);

            // * add all child loan programs to product list
            var parameterList = new List<SqlParameter>();

            Queue folderQueue = new Queue();
            folderQueue.Enqueue(productFolderId);
            while (folderQueue.Count > 0)
            {
                Guid folderId = (Guid)folderQueue.Dequeue();

                using (var sqlConnection = DbAccessUtils.GetConnection(DataSrc.LpeSrc))
                {
                    // add loan programs for current folder
                    PopulateProductList(folderId, sqlConnection, parameterList);

                    // process child folders
                    EnqueueChildFolders(folderId, sqlConnection, folderQueue);
                }
            }

            if (parameterList.Count == 0)
            {
                return; // nothing to display
            }


            // Get PG-Policy Attachments
            Dictionary<string, E_TriState> priceGroupAssociations = new Dictionary<string, E_TriState>();
            RetrievePolicyAttachments(BrokerUserPrincipal.CurrentPrincipal.BrokerId, priceGroupAssociations);


            // We can use pricegroup name here because it is unique among lenders and likely shorter than GUID.
            Hashtable programPriceGroupAssociations = new Hashtable(); // key: pgname+ppid+asoc, val: int - How many times we have seen that association
            Hashtable programPriceGroupPolicyYNMap = new Hashtable(); // key: pgname+ppid+assoc, val: node - The id of the mapped node
            PopulatePolicyDisplayTree(priceGroupAssociations, parameterList, programPriceGroupAssociations, programPriceGroupPolicyYNMap, this.hashNodes, this.rootNode, this.orphans);

            // Set the program counts
            foreach (DictionaryEntry de in programPriceGroupAssociations)
            {
                ((AssocViewNode)this.hashNodes[programPriceGroupPolicyYNMap[de.Key]]).title += " (" + de.Value.ToString() + " program" + (((int)de.Value != 1) ? "s" : "") + ")";
            }

            while (this.orphans.Count > 0)
            {
                // take care of orphans
                parameterList = new List<SqlParameter>();
                GatherOrphanParameters(parameterList);

                if (this.orphans.Count == 0) break;

                ProcessOrphans(parameterList, this.hashNodes, this.rootNode, this.orphans);
            }
        }

        private void GatherOrphanParameters(List<SqlParameter> parameterList)
        {
            for (int i = this.orphans.Count - 1; i >=0; i--)
            {
                Orphan orphan = (Orphan)this.orphans.Dequeue();

                AssocViewNode parentNode = (AssocViewNode)this.hashNodes[orphan.ParentId];

                if (parentNode != null)
                {
                    parentNode.children.Add((AssocViewNode)this.hashNodes[orphan.Id]);
                }                    
                else
                {
                    var paramName = "@p" + parameterList.Count;
                    parameterList.Add(new SqlParameter(paramName, orphan.ParentId));
                    this.orphans.Enqueue(orphan);
                }
            }
        }        

        private void ProcessOrphans(List<SqlParameter> parameterList, Hashtable hashNodes, AssocViewNode rootNode, Queue orphans)
        {
            foreach (var chunk in parameterList.Chunk(sqlParameterChunkSize))
            {
                var sql = $@"
SELECT PricePolicyId, ParentPolicyId, PricePolicyDescription 
FROM PRICE_POLICY 
WHERE PricePolicyId IN ({string.Join(",", chunk.Select(p => p.ParameterName))})";

                Action<IDataReader> readHandler = delegate (IDataReader sdr)
                {
                    while (sdr.Read())
                    {
                        if (DBNull.Value.Equals(sdr["ParentPolicyId"]))
                        {
                            GetNodeData((Guid)sdr["PricePolicyId"], sdr["PricePolicyDescription"].ToString(), Guid.Empty, null, hashNodes, rootNode, orphans);
                        }
                        else
                        {
                            GetNodeData((Guid)sdr["PricePolicyId"], sdr["PricePolicyDescription"].ToString(), (Guid)sdr["ParentPolicyId"], null, hashNodes, rootNode, orphans);
                        }
                    }
                };

                DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql, null, chunk, readHandler);
            }
        }

        private void PopulatePolicyDisplayTree(
            Dictionary<string, E_TriState> priceGroupAssociations, 
            List<SqlParameter> parameterList,
            Hashtable programPriceGroupAssociations, 
            Hashtable programPriceGroupPolicyYNMap, 
            Hashtable hashNodes, 
            AssocViewNode rootNode, 
            Queue orphans)
        {
            var priceGroupDictionary = PriceGroup.ListAllSlow();
            Hashtable PriceGroupPolicy = new Hashtable(); // key: pgid+policyId
            Hashtable priceGroupPolicyMap = new Hashtable(); // key: pgname+ppid, val: GUID - The id of the mapped node

            foreach (var chunk in parameterList.Chunk(sqlParameterChunkSize))
            {
                var sql = $@"
SELECT pp.PricePolicyId, pp.ParentPolicyId, pp.PricePolicyDescription, pppa.LpePriceGroupId, pppa.AssociateType 
FROM PRICE_POLICY pp inner join PRICEGROUP_PROG_POLICY_ASSOC pppa on pp.PricePolicyId = pppa.PricePolicyId 
WHERE pppa.ProgId IN({string.Join(",", chunk.Select(p => p.ParameterName))})";

                Action<IDataReader> readHandler = delegate (IDataReader sdr)
                {
                    while (sdr.Read())
                    {
                        // Add policy
                        if (DBNull.Value.Equals(sdr["ParentPolicyId"]))
                        {
                            GetNodeData((Guid)sdr["PricePolicyId"], sdr["PricePolicyDescription"].ToString(), Guid.Empty, null, hashNodes, rootNode, orphans);
                        }
                        else
                        {
                            GetNodeData((Guid)sdr["PricePolicyId"], sdr["PricePolicyDescription"].ToString(), (Guid)sdr["ParentPolicyId"], null, hashNodes, rootNode, orphans);
                        }

                        Guid lpePriceGroupId = (Guid)sdr["LpePriceGroupId"];

                        // Add PriceGroup
                        //string key = sdr["LpePriceGroupName"].ToString() + sdr["PricePolicyId"].ToString();
                        string key = string.Empty;
                        string priceGroupName = string.Empty;

                        if (lpePriceGroupId != Guid.Empty)
                        {
                            PriceGroup priceGroup = null;
                            if (priceGroupDictionary.TryGetValue(lpePriceGroupId, out priceGroup))
                            {
                                key = priceGroup.Name + sdr["PricePolicyId"].ToString();
                                priceGroupName = priceGroup.Name;
                            }
                        }

                        if (!priceGroupPolicyMap.Contains(key))
                        {
                            string pGattachString = string.Empty;
                            Guid priceGroupId = (Guid)sdr["LpePriceGroupId"];
                            Guid pricePolicyId = (Guid)sdr["PricePolicyId"];
                            string pgPolicyKey = priceGroupId.ToString() + pricePolicyId.ToString();

                            // The PG Y/N Setting
                            if (priceGroupAssociations.ContainsKey(pgPolicyKey))
                            {
                                pGattachString = priceGroupAssociations[pgPolicyKey] == E_TriState.Yes ? " : Y" : " : N";
                            }

                            GetNodeData((Guid)(priceGroupPolicyMap[key] = Guid.NewGuid()), priceGroupName + pGattachString, (Guid)sdr["PricePolicyId"], null, hashNodes, rootNode, orphans);
                        }

                        // Add the Y/N program count node
                        string AssociateType = (((E_TriState)(Byte)sdr["AssociateType"] == E_TriState.Yes) ? "Y" : "N");

                        if (!programPriceGroupPolicyYNMap.Contains(key + AssociateType))
                        {
                            GetNodeData((Guid)(programPriceGroupPolicyYNMap[key + AssociateType] = Guid.NewGuid()), AssociateType, (Guid)priceGroupPolicyMap[key], priceGroupName, hashNodes, rootNode, orphans);

                            programPriceGroupAssociations[key + AssociateType] = 1;
                        }
                        else
                        {
                            // PERFORMANCE: Boxing/unboxing alternative.
                            int count = (int)programPriceGroupAssociations[key + AssociateType];
                            programPriceGroupAssociations[key + AssociateType] = count + 1;
                        }
                    }
                };

                DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql, null, chunk, readHandler);
            }
        }

        private void RetrievePolicyAttachments(Guid brokerId, Dictionary<string, E_TriState> priceGroupAssociations)
        {
            var listParams = new SqlParameter[] { new SqlParameter("@brokerId", brokerId) };
            string sSql = "SELECT AssocOverrideTri, ProductId, PricePolicyId FROM Product_Price_Association "
               + "WHERE AssocOverrideTri <> 0 AND ProductId IN (SELECT lLpTemplateId FROM Loan_Program_Template WHERE IsMasterPriceGroup = 1 AND BrokerId = @brokerId)";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    priceGroupAssociations.Add(reader["ProductId"].ToString() + reader["PricePolicyId"].ToString(), (E_TriState)(Byte)reader["AssocOverrideTri"]);
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sSql, null, listParams, readHandler);
        }

        private void EnqueueChildFolders(Guid folderId, DbConnection sqlConnection, Queue folderQueue)
        {
            var listParams = new SqlParameter[] { new SqlParameter("@folderId", folderId) };
            string sSql = "SELECT FolderId FROM LOAN_PRODUCT_FOLDER WHERE ParentFolderId = @folderId";

            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                while (sdr.Read())
                    folderQueue.Enqueue((Guid)sdr["FolderId"]);
            };

            DBSelectUtility.ProcessDBData(sqlConnection, null, sSql, null, listParams, readHandler);
        }

        private void PopulateProductList(Guid folderId, DbConnection sqlConnection, List<SqlParameter> parameterList)
        {
            var listParams = new SqlParameter[] { new SqlParameter("@p", folderId) };
            string sSql = "SELECT lLpTemplateId FROM LOAN_PROGRAM_TEMPLATE WHERE FolderId = @p";

            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                while (sdr.Read())
                {
                    Guid id = (Guid)sdr["lLpTemplateId"];

                    var paramName = "@p" + parameterList.Count;
                    parameterList.Add(new SqlParameter(paramName, id));
                }
            };

            DBSelectUtility.ProcessDBData(sqlConnection, null, sSql, null, listParams, readHandler);
        }

        private string RetrieveFolderLabel(Guid productFolderId)
        {
            string folderName = null;
            var listParams = new SqlParameter[] { new SqlParameter("@folderId", productFolderId) };
            string sSql = "SELECT FolderName FROM LOAN_PRODUCT_FOLDER WHERE FolderId = @folderId";
            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                sdr.Read();
                folderName = sdr["FolderName"].ToString() + "::" + productFolderId.ToString();
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sSql, null, listParams, readHandler);

            return folderName;
        }

        private void DisplayPolicyTree(Guid productFolderId)
		{
            // parse labels
            this.m_lbLabel.Text = RetrieveFolderLabel(productFolderId);

            // * add all child loan programs to product list
            var parameterList = new List<SqlParameter>();

			Queue folderQueue = new Queue() ;
			folderQueue.Enqueue(productFolderId) ;
			while (folderQueue.Count > 0)
			{
				Guid folderId = (Guid)folderQueue.Dequeue() ;

                using (var sqlConnection = DbAccessUtils.GetConnection(DataSrc.LpeSrc))
                {
                    // add loan programs for current folder
                    PopulateProductList(folderId, sqlConnection, parameterList);

                    // process child folders
                    EnqueueChildFolders(folderId, sqlConnection, folderQueue);
                }
            }

			if (parameterList.Count == 0) return; // nothing to display

            // Build ProductList
            BuildProductList(parameterList, this.hashNodes, this.rootNode, this.orphans);

            while (this.orphans.Count > 0)
            {
                // take care of orphans
                parameterList = new List<SqlParameter>();
                GatherOrphanParameters(parameterList);

                if (this.orphans.Count == 0) break;

                ProcessOrphans(parameterList, this.hashNodes, this.rootNode, this.orphans);
            }
        }

        private void BuildProductList(List<SqlParameter> parameterList, Hashtable hashNodes, AssocViewNode rootNode, Queue orphans)
        {
            Dictionary<AssocViewNode, AssocInfo> assocDictionary = new Dictionary<AssocViewNode, AssocInfo>();

            foreach (var chunk in parameterList.Chunk(sqlParameterChunkSize))
            {
                // 07/18/07 mf. OPM 4873.  We also want to list Disassociations here.
                // 12/01/2017 OPM 463101: view derived ("b") information.
                var sql = $@"
SELECT a.PricePolicyId, ParentPolicyId, InheritVal, InheritValFromBaseProd, AssocOverrideTri, IsForcedNo, PricePolicyDescription 
FROM PRICE_POLICY a, PRODUCT_PRICE_ASSOCIATION b 
WHERE 
    a.PricePolicyId=b.PricePolicyId AND 
    b.ProductId IN ({string.Join(",", chunk.Select(p => p.ParameterName))}) AND 
    (
        (b.InheritVal = 1 and b.AssocOverrideTri = 0) or 
        b.InheritValFromBaseProd = 1 or b.AssocOverrideTri = 1 or 
        b.IsForcedNo = 1
    ) 
GROUP BY a.PricePolicyId, ParentPolicyId, InheritVal, InheritValFromBaseProd, AssocOverrideTri, IsForcedNo, PricePolicyDescription 
ORDER BY PricePolicyDescription";

                Action<IDataReader> readHandler = delegate (IDataReader sdr)
                {
                    while (sdr.Read())
                    {
                        AssocViewNode dynaNode = new AssocViewNode();
                        //dynaNode.addClass = "no-icon";

                        if (DBNull.Value.Equals(sdr["ParentPolicyId"]))
                        {
                            dynaNode = GetNodeData((Guid)sdr["PricePolicyId"], sdr["PricePolicyDescription"].ToString(), Guid.Empty, null, hashNodes, rootNode, orphans);
                        }
                        else
                        {
                            dynaNode = GetNodeData((Guid)sdr["PricePolicyId"], sdr["PricePolicyDescription"].ToString(), (Guid)sdr["ParentPolicyId"], null, hashNodes, rootNode, orphans);
                        }

                        AssocInfo assocInfo = AssocInfo.Blank, prevAssocInfo;

                        if ((bool)sdr["InheritValFromBaseProd"])
                        {
                            assocInfo |= AssocInfo.InheritFromBaseProd;
                        }

                        if ((bool)sdr["InheritVal"])
                        {
                            assocInfo |= AssocInfo.InheritFromMasterProd;
                        }

                        if ((bool)sdr["IsForcedNo"])
                        {
                            assocInfo |= AssocInfo.FN;
                        }
                        switch ((E_TriState)(Byte)sdr["AssocOverrideTri"])
                        {
                            case E_TriState.Yes:
                                assocInfo |= AssocInfo.Yes;
                                break;

                            case E_TriState.No:
                                assocInfo |= AssocInfo.No;
                                break;
                        }

                        if (assocDictionary.TryGetValue(dynaNode, out prevAssocInfo))
                        {
                            assocDictionary[dynaNode] = prevAssocInfo | assocInfo;
                        }
                        else
                        {
                            assocDictionary.Add(dynaNode, assocInfo);
                        }
                    }
                };

                DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql, null, chunk, readHandler);
            }

            var sb = new StringBuilder();            

            foreach (AssocViewNode node in assocDictionary.Keys)
            {
                AssocInfo assocInfo = assocDictionary[node];
                string bSel = assocInfo.HasFlag(AssocInfo.InheritFromBaseProd) ? " checked" : string.Empty;
                string mSel = assocInfo.HasFlag(AssocInfo.InheritFromMasterProd) ? " checked" : string.Empty;
                string ySel = assocInfo.HasFlag(AssocInfo.Yes) ? " checked" : string.Empty;
                string nSel = assocInfo.HasFlag(AssocInfo.No) ? " checked" : string.Empty;
                string fnSel = assocInfo.HasFlag(AssocInfo.FN) ? " checked" : string.Empty;

                if (assocInfo.HasFlag(AssocInfo.FN))
                {
                    node.addClass += " red-color";
                }

                sb.Clear();
                sb.Append("<span style='margin-right:6px;color:tomato'>");
                sb.Append($"<INPUT type='radio' disabled {bSel}>b <INPUT type='radio' disabled {mSel}>m <INPUT type='checkbox' disabled {ySel}>Y ");
                sb.Append($"<INPUT type='checkbox' disabled {nSel}> N");
                sb.Append($"<INPUT type='checkbox' disabled {fnSel}> FN");
                sb.Append("</span>").AppendLine();

                node.title += sb.ToString();
            }
        }

        private void DisplayPriceGroupTree(Guid PricePolicyId)
        {
            var dictionary = BrokerDbLite.ListAllSlow();
            var priceGroupDictionary = PriceGroup.ListAllSlow();
            // Show all PG associations for this Policy
            using (DbDataReader reader
                       = StoredProcedureHelper.ExecuteReader(
                       DataSrc.LpeSrc
                       , "ListPriceGroupAssociationForDisplayByPolicyId"
                       , new SqlParameter("@PricePolicyId", PricePolicyId)
                       ))
            {
                while (reader.Read())
                {
                    Guid PriceGroupId = (Guid)reader["ProductId"];

                    PriceGroup priceGroup = null;

                    if (priceGroupDictionary.TryGetValue(PriceGroupId, out priceGroup) == false)
                    {
                        // 7/21/2014 dd - This is not a policy attach to price group. Skip.
                        continue;
                    }

                    Guid brokerId = priceGroup.BrokerID;
                    BrokerDbLite brokerDbLite = dictionary[brokerId];

                    string name = brokerDbLite.BrokerNm + " (" + brokerDbLite.CustomerCode + ")";
                    // Add PriceGroup node if needed
                    var node = AddNode(
                        PriceGroupId
                        , priceGroup.Name + ((E_TriState)(Byte)reader["AssocOverrideTri"] == E_TriState.Yes ? " : Y" : " : N")
                        , brokerId
                        , name
                        );
                    node.Type = "Product";
                    node.CheckBox = m_isDetachView;
                    if (m_isDetachView)
                    {
                        node.addClass = node.addClass.Replace("hide-checkbox", string.Empty);
                    }
                }
            }
       }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void m_okBtn_Click(object sender, System.EventArgs e)
		{
			LendersOfficeApp.common.ModalDlg.cModalDlg.CloseDialog(this.Page) ;
		}

        private AssocViewNode GetNode(Guid id)
        {
            return GetNodeFromId(id, this.hashNodes);
        }

        private AssocViewNode GetNodeFromId(Guid id, Hashtable hashNodes)
        {
            return (AssocViewNode)hashNodes[id];
        }

        private AssocViewNode GetNode(Guid id, string sNodeText, Guid parentId, string sParentNodeText)
        {
            return GetNodeData(id, sNodeText, parentId, sParentNodeText, this.hashNodes, this.rootNode, this.orphans);
        }        

        private AssocViewNode GetNodeData(Guid id, string sNodeText, Guid parentId, string sParentNodeText, Hashtable hashNodes, AssocViewNode rootNode, Queue orphans)
        {
            // return existing node if it already exists
            AssocViewNode node = (AssocViewNode)hashNodes[id];

            if (node != null)
            {
                return node;
            }

            // node not found. create a new node
            node = new AssocViewNode();
            node.expand = true;
            node.title = sNodeText;
            node.key = id.ToString();
            //node.addClass = "no-icon";
            hashNodes.Add(id, node);

            AssocViewNode parentNode = (AssocViewNode)hashNodes[parentId];

            if (parentNode != null)
            {
                parentNode.children.Add(node);
                return node;
            }

            if (sParentNodeText != null)
            {
                // there is enough info to make a parent node. this parent node is a child of the tree.
                parentNode = new AssocViewNode();
                parentNode.expand = false;
                parentNode.title = sParentNodeText;
                parentNode.key = parentId.ToString();
                hashNodes.Add(parentId, parentNode);

                rootNode.children.Add(parentNode);
                parentNode.children.Add(node);
            }
            else if (parentId != Guid.Empty)
            {
                // there's not enough info to construct the parent node, so make the node an orphan
                orphans.Enqueue(new Orphan(id, parentId));
            }
            else
            {
                // no parent, add this node to the root
                node.expand = false;
                rootNode.children.Add(node);
            }

            return node;
        }

        private AssocViewNode AddNode(Guid id, string sNodeText, Guid parentId)
        {
            return GetNode(id, sNodeText, parentId, null);
        }

        private AssocViewNode AddNode(Guid id, string sNodeText, Guid parentId, string sParentNodeText)
        {
            return GetNode(id, sNodeText, parentId, sParentNodeText);
        }
        
        private AssocViewNode GetBrokerNode(Guid brokerId, string brokerName, Hashtable hashNodes, AssocViewNode rootNode)
        {
            AssocViewNode node = (AssocViewNode)hashNodes[brokerId];

            if (node != null)
            {
                return node;
            }

            // there is enough info to make a parent node. parent node is a child of the tree.
            node = new AssocViewNode();
            //node.addClass = "no-icon";
            node.expand = false;
            node.title = brokerName;
            node.key = brokerId.ToString();
            hashNodes.Add(brokerId, node);

            rootNode.children.Add(node);

            return node;
        }

        string DetachPolicyFromPrograms(Guid policyId, List<Guid> prgIds, bool isDetach) // global associations
        {
            //Tools.LogInfo($"Try Batch-detach policy {policyId} and loan programs {string.Join(", ", prgIds)}");

            Dictionary<Guid, string> errPrgDict = new Dictionary<Guid, string>();

            List<SqlParameter> parameters = new List<SqlParameter>();
            var sql = new StringBuilder(@"select lLpTemplateId, BrokerId, isMaster 
FROM loan_program_template
WHERE IsMasterPriceGroup=0 AND lLpTemplateId ");

            sql.Append(DbTools.CreateParameterized4InClauseSql("lpId", prgIds, parameters));

            Dictionary<Guid, Guid> prgToBrokerDict = new Dictionary<Guid, Guid>();
            HashSet<Guid> masterProductIds = new HashSet<Guid>();
            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    prgToBrokerDict.Add(reader.SafeGuid("lLpTemplateId"), reader.SafeGuid("BrokerId"));
                    if( reader.SafeBool("isMaster") )
                    {
                        masterProductIds.Add(reader.SafeGuid("lLpTemplateId"));
                    }

                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql.ToString(), null, parameters, readHandler);

            Tools.Assert(prgIds.Count == prgToBrokerDict.Count, $"#requested loan programs {prgIds.Count} but only get {prgToBrokerDict.Count}");

            var selectedMasterIds = prgIds.Where(id => masterProductIds.Contains(id)).ToList();
            var otherPrgIds = prgIds.Where(id => masterProductIds.Contains(id) == false).ToList();
            E_UiPolicyProgramAssociation assocVal = isDetach ? E_UiPolicyProgramAssociation.No : E_UiPolicyProgramAssociation.Blank;

            // process master products first
            if (selectedMasterIds.Count > 0)
            {
                Tools.LogInfo((isDetach ? "Try to attach 'N'" : "Try to clear attachment") + $" policy {policyId} with master programs {string.Join(", ", selectedMasterIds)}");
            }

            foreach (Guid prgId in selectedMasterIds)
            {
                string errMsg = string.Empty;
                try
                {
                    var errCode = PricePolicyListByProductService.SetProgramAssociation(prgToBrokerDict[prgId], prgId,
                                                                                        policyId, assocVal, true /*turnOptimizerOffChk*/, out errMsg);
                }
                catch (CBaseException exc)
                {
                    errMsg = exc.Message;
                    Tools.LogError($"SetProgramAssociation( progId: {prgId}, policyId: {policyId},  assocVal: {assocVal}) error", exc);
                }
                catch (SqlException exc)
                {
                    errMsg = exc.Message;
                    Tools.LogError($"SetProgramAssociation( progId: {prgId}, policyId: {policyId},  assocVal: {assocVal}) error", exc);
                }
                catch (Exception exc)
                {
                    Tools.LogError($"SetProgramAssociation( progId: {prgId}, policyId: {policyId},  assocVal: {assocVal}) error", exc);
                    throw;
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    errPrgDict.Add(prgId, errMsg);
                }
            }

            //process non-master products. First reload current associate values for both masters and non-masters.
            parameters = new List<SqlParameter>();
            sql = new StringBuilder(@"select ProductId, AssocOverrideTri, lLpTemplateNm as Name 
FROM PRODUCT_PRICE_ASSOCIATION pa
JOIN Loan_Program_Template product on pa.productId = product.lLpTemplateId
WHERE PricePolicyId=@PricePolicyId AND ProductId ");

            sql.Append(DbTools.CreateParameterized4InClauseSql("prgId", prgIds, parameters)); // get values for all masters and non-masters.
            parameters.Add(new SqlParameter("@PricePolicyId", policyId));


            Dictionary<Guid, Tuple<E_TriState, string>> curAssocValues = new Dictionary<Guid, Tuple<E_TriState, string>>();

            Action<IDataReader> readHandler2 = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    curAssocValues.Add(reader.SafeGuid("ProductId"), Tuple.Create((E_TriState)(Byte)reader["AssocOverrideTri"], (string)reader["Name"]));
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql.ToString(), null, parameters, readHandler2);

            if (otherPrgIds.Count > 0)
            {
                Tools.LogInfo((isDetach ? "Try to attach 'N'" : "Try to clear attachment") + $" policy {policyId} with non-master programs {string.Join(", ", otherPrgIds)}");
            }

            foreach (Guid prgId in otherPrgIds) //process non-master products
            {
                string errMsg = string.Empty;
                try
                {
                    if (curAssocValues.ContainsKey(prgId) == false)
                    {
                        errMsg = "association doesn't exist.";
                    }
                    else
                    {
                        E_UiPolicyProgramAssociation val = E_UiPolicyProgramAssociation.Blank;
                        switch (curAssocValues[prgId].Item1)
                        {
                        case E_TriState.Yes:
                            val = E_UiPolicyProgramAssociation.Yes;
                            break;

                        case E_TriState.No:
                            val = E_UiPolicyProgramAssociation.No;
                            break;
                        }

                        if (assocVal != val)
                        {
                            PricePolicyListByProductService.SetProgramAssociation(prgToBrokerDict[prgId], prgId,
                                                                              policyId, assocVal, true /*turnOptimizerOffChk*/, out errMsg);
                        }
                    }
                }
                catch (CBaseException exc)
                {
                    errMsg = exc.Message;
                    Tools.LogError($"SetProgramAssociation( progId: {prgId}, policyId: {policyId},  assocVal: {assocVal}) error", exc);
                }
                catch (SqlException exc)
                {
                    errMsg = exc.Message;
                    Tools.LogError($"SetProgramAssociation( progId: {prgId}, policyId: {policyId},  assocVal: {assocVal}) error", exc);
                }
                catch (Exception exc)
                {
                    Tools.LogError($"SetProgramAssociation( progId: {prgId}, policyId: {policyId},  assocVal: {assocVal}) error", exc);
                    throw;
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    errPrgDict.Add(prgId, errMsg);
                }
            }

            var sb = new StringBuilder();

            if ( errPrgDict.Count > 0)
            {
                sb.AppendLine("Cannot " + (isDetach? "attach 'N'" : "clear attachment") + $" for {errPrgDict.Count} (/ selected {prgIds.Count}) following programs:");

                errDataTable = new DataTable("Loan Products");
                errDataTable.Columns.Add("Name", typeof(string));
                errDataTable.Columns.Add("Id", typeof(Guid));
                errDataTable.Columns.Add("Error Message", typeof(string));

                foreach ( Guid prgId in prgIds)
                {
                    if(!errPrgDict.ContainsKey(prgId) )
                    {
                        continue;
                    }

                    var newRow = errDataTable.NewRow();
                    newRow["Name"] = curAssocValues.ContainsKey(prgId) ? curAssocValues[prgId].Item2 : "???";
                    newRow["Id"] = prgId;
                    newRow["Error Message"] = errPrgDict[prgId];
                    errDataTable.Rows.Add(newRow);
                }
            }

            return sb.ToString();
        }

        string DetachPolicyFromProgramInPricegroups(Guid policyId, List<Guid> detachIds) 
        {
            var prgInPgInfo = this.ViewState["prgInPriceGroupDict"] as Dictionary<Guid, Tuple<Guid, Guid>>;
            Dictionary<Guid, List<Guid>> pgToPgs = new Dictionary<Guid, List<Guid>>();

            foreach (var makeupId in detachIds)
            {
                var prgId = prgInPgInfo[makeupId].Item1;
                var priceGroupId = prgInPgInfo[makeupId].Item2;
                List<Guid> prgs;
                if (pgToPgs.TryGetValue(priceGroupId, out prgs) == false)
                {
                    pgToPgs.Add(priceGroupId, prgs = new List<Guid>());
                }
                prgs.Add(prgId);
            }

            var sb = new StringBuilder();

            foreach (var pricegroupId in pgToPgs.Keys)
            {
                var prgIds = pgToPgs[pricegroupId];

                try
                {
                    Tools.LogInfo($"Try Batch-detach policy {policyId} with price group {pricegroupId} and programs {string.Join(", ", prgIds)}");
                    var parameters = new List<SqlParameter>();
                    string sql = @"DELETE FROM PRICEGROUP_PROG_POLICY_ASSOC where PricePolicyId = @PricePolicyId AND LpePriceGroupId = @LpePriceGroupId AND ProgId " +
                                 DbTools.CreateParameterized4InClauseSql("lpId", prgIds, parameters);
                    parameters.Add(new SqlParameter("@PricePolicyId", policyId));
                    parameters.Add(new SqlParameter("@LpePriceGroupId", pricegroupId));

                    DBDeleteUtility.Delete(DataSrc.LpeSrc, sql, null, parameters);
                }
                catch (SqlException exc)
                {
                    if (sb.Length == 0)
                    {
                        sb.AppendLine($"Cannot detach the policy {policyId} and following price group's programs:");
                    }

                    sb.AppendLine($"    Pricegroup={pricegroupId}, programs: {string.Join(", ", prgIds)}, error: {exc.Message}"  );

                    Tools.LogError($"Cannot detach the policy {policyId} with Pricegroup={pricegroupId} and programs {string.Join(", ", prgIds)}", exc);
                }
                catch (LqbException exc)
                {
                    if (sb.Length == 0)
                    {
                        sb.AppendLine($"Cannot detach the policy {policyId} and following price group's programs:");
                    }

                    sb.AppendLine($"    Pricegroup={pricegroupId}, programs: {string.Join(", ", prgIds)}, error: {exc.Message}");
                    Tools.LogError($"Cannot detach the policy {policyId} with Pricegroup={pricegroupId} and programs {string.Join(", ", prgIds)}", exc);
                }
                catch (Exception exc)
                {
                    Tools.LogError($"Cannot detach the policy {policyId} with Pricegroup={pricegroupId} and programs {string.Join(", ", prgIds)}", exc);
                    throw;
                }
            }

            return sb.ToString();
        }

        private void BatchDetachPolicy(Guid policyId, bool isDetach /* No or Blank*/)
        {
            string prefix = string.Empty;
            string errMsg = string.Empty;
            var  detachIds = new List<Guid>(Request.Form["m_detachIds"].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(Guid.Parse));

            if (m_viewLevel.SelectedIndex == 0)
            {
                prefix = $"[Detach Global Associations for policy {policyId}] ";
                // Global association between program and policies 
                errMsg = DetachPolicyFromPrograms(policyId, detachIds, isDetach);
                    
            }
            else if (m_viewLevel.SelectedIndex == 1)
            {
                prefix = $"[Detach Program and Price Group Association for policy {policyId}] ";

                // Program in price group and policy associations
                errMsg = DetachPolicyFromProgramInPricegroups(policyId, detachIds);
            }
            else if (m_viewLevel.SelectedIndex == 2)
            {
                try
                {
                    // price group and policies associations.
                    Tools.LogInfo($"Try Batch-detach policy {policyId} and price groups {string.Join(", ", detachIds)}");

                    prefix = $"[Detach Price Group Association for policy {policyId}] ";

                    var parameters = new List<SqlParameter>();
                    string sql = @"DELETE FROM Product_Price_Association where PricePolicyId = @PricePolicyId AND ProductId " +
                                    DbTools.CreateParameterized4InClauseSql("lpId", detachIds, parameters);
                    parameters.Add(new SqlParameter("@PricePolicyId", policyId));

                    DBDeleteUtility.Delete(DataSrc.LpeSrc, sql, null, parameters);
                }
                catch (SqlException exc)
                {
                    errMsg = exc.Message;
                    Tools.LogError($"Cannot detach the policy {policyId} with pricegroups {string.Join(", ", detachIds)}", exc);
                }
                catch (LqbException exc)
                {
                    errMsg = exc.Message;
                    Tools.LogError($"Cannot detach the policy {policyId} with pricegroups {string.Join(", ", detachIds)}", exc);
                }
                catch (Exception exc)
                {
                    Tools.LogError($"Cannot detach the policy {policyId} with pricegroups {string.Join(", ", detachIds)}", exc);
                    throw;
                }
            }

            if (!string.IsNullOrEmpty(errMsg) )
            {
                ClientScript.RegisterHiddenField("m_errorMessage", prefix + errMsg);
            }

        }
    }
}