<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Page language="c#" Codebehind="AssocView.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.Helpers.AssocView" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML>
<HTML>
	<HEAD runat="server">
		<title>AssocView</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../../../css/stylesheet.css" type="text/css" rel="stylesheet">
        <style type="text/css">
            .red-color {
                color: red;
            }
            .gray-color {
                color: gray;
            }
            .blue-color {
                color: blue;
            }
            #dynatree {
                border: 2px solid lightgrey;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 11px;
                height: 100%;
            }
            .no-icon .dynatree-icon {
                display: none;
            }
            .hide-checkbox .dynatree-checkbox {
                display: none;
            }
            html, body, form, #treeRow, #treeColumn, #treeBase {
                height: 100%;
            }
            #AssocView {
                overflow-y: auto;
            }
        </style>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" onload=onInit();>
        <script type="text/javascript">
            var org_oncheck;
            var isGlobalAssoc;

            function checkDescendants(node, checked) {                
                $j(node.data.children).map(function (index, childNode) {
                    var nodeInfo = $j("#dynatree").dynatree("getTree").selectKey(childNode.key);

                    if (childNode.CheckBox) {
                        nodeInfo.bSelected = checked;
                        checkDescendants(nodeInfo, checked);
                    }
                });                
            }

            function onInit()
            {
                 isGlobalAssoc = ML.isGlobalAssoc;                

                $j("#dynatree").dynatree({
                    minExpandLevel: 1,
                    checkbox: true,
                    onSelect: function (select, node) {
                        checkDescendants(node, node.bSelected);
                        node.expand(true);
                        $j("#dynatree").dynatree("getTree").redraw();
                    },
                    children: nodes,                    
                });

                if( document.getElementById("m_errorMessage") != null && document.getElementById("m_errorMessage").value != "")
				{
                    alert( document.getElementById("m_errorMessage").value );
                }
			}

            function PrepareBatchDetach(isDetach) {
                var ids = getSelectedIds();
                if (ids.length == 0) {
                    alert("Please select some items.")
                    return false;
                }

                var prompt = "Do you want to remove the attachment from " + ids.length + " selected programs (in price groups) / selected price groups?"
                if (isGlobalAssoc)
                    prompt = isDetach ? 'Do you want to attach �N� to ' + ids.length + ' selected programs?' : 'Do you want to clear the attachment from ' + ids.length + ' selected programs?';

                if (!confirm(prompt)) {
                    return false ;
				}

                document.getElementById("cmdToDo").value = isDetach ? "batchDetach" : "setInherit";
                return true;
            }            

            function getSelectedIds() {
                var ids = [];
                var selectedNodes = $j("#dynatree").dynatree("getSelectedNodes");

                selectedNodes.map(function (node) {
                    if (node.data.Type === "Product") {
                        ids.push(node.data.key)
                    }
                });

                document.getElementById("m_detachIds").value = ids.join(";");

                return ids;
            }
	    </script>

		<form id="AssocView" method="post" runat="server">
			<TABLE class="FormTable" cellSpacing="2" cellPadding="4" width="100%" height="100%" border="0">
				<TR>
					<TD class="FormTableHeader" height="1px">
						Association View
					</TD>
				</TR>
				<tr>
					<td>
						<b>
							<ml:EncodedLabel id="m_lbLabel" runat="server">
							</ml:EncodedLabel>
						</b>
						<asp:RadioButtonList Runat="server" ID="m_viewLevel" AutoPostBack="True" RepeatLayout="Flow" RepeatDirection="Horizontal">
							<asp:ListItem Selected="True">Lender (Global)</asp:ListItem>
							<asp:ListItem>Program Price Group</asp:ListItem>
							<asp:ListItem >Price Group</asp:ListItem>
						</asp:RadioButtonList>
					</td>
				</tr>
				<TR valign="top" id="treeRow">
					<TD id="treeColumn">
					    <div id="treeBase">                            
                            <div id="dynatree">
                            </div>

                            <ml:EncodedLabel id="m_errMsgLabel" runat="server" style="COLOR: red; FONT: bold 14px arial; TEXT-ALIGN: left; PADDING-TOP: 10px; PADDING-BOTTOM: 10px;"></ml:EncodedLabel>
                            <asp:datagrid id="dgErrPrograms" runat="server" DataKeyField="Id"  EnableViewState="False" AutoGenerateColumns="False" CssClass="DataGrid" AllowSorting="True" Width="100%">
						        <AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top"></AlternatingItemStyle>
						        <ItemStyle CssClass="GridItem" VerticalAlign="Top"></ItemStyle>
						        <HeaderStyle CssClass="GridHeader"></HeaderStyle>
						        <Columns>
							        <asp:BoundColumn DataField="Name" HeaderText="Program Name" />
							        <asp:BoundColumn DataField="Id" HeaderText="Program ID" />
							        <asp:BoundColumn DataField="Error Message" HeaderText="Error Message" />
						        </Columns>
                            </asp:datagrid>

						    <asp:Panel id="m_Denied" runat="server" style="COLOR: red; FONT: 11px arial; TEXT-ALIGN: center; PADDING: 120px;" Visible="False">
							    Access denied.  You do not have permission to view this content.
						    </asp:Panel>
					    </div>
					</TD>
				</TR>
				<TR>
					<TD align="middle" height="1px" height="100px" style="PADDING: 16px;">
                        <asp:Button ID="m_batchDetachBtn" runat="server" Text='Attach "N"' OnClientClick="return PrepareBatchDetach(true);" /> 
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                        <asp:Button ID="m_setInheritBtn" runat="server" Text="Clear Attachment" OnClientClick="return PrepareBatchDetach(false);" /> 
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
						<asp:Button id="m_okBtn" runat="server" Text="Close" onclick="m_okBtn_Click">
						</asp:Button>
					</TD>
				</TR>
			</TABLE>
           <input type="hidden" id="cmdToDo" runat="server" value="" enableviewstate="false" />
           <input type="hidden" id="m_detachIds" runat="server" value="none" enableviewstate="false" />
		</form>
	</body>
</HTML>
