<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="ConditionEdit.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.Helpers.ConditionEdit" validateRequest="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ConditionEdit</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript">
	function insertKeyword(ed,ddl) {
		ed.focus();
		ed.value += " " + ddl.options[ddl.selectedIndex].value + " ";
	}
		</script>
	</HEAD>
	<body bgColor="#cccccc" scroll="yes" MS_POSITIONING="FlowLayout">
		<FORM id="ConditionEdit" method="post" runat="server">
			<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="FormTableHeader">Edit Condition</TD>
				</TR>
			</TABLE>
			<CENTER>
				<BR>
				<TABLE id="Table1" cellSpacing="0" cellPadding="2" width="99%" border="0">
					<TR>
						<TD class="FormTableHeader1">Condition</TD>
					</TR>
					<TR>
						<TD><EM>NOTE: All words and symbols must be separated by at least one space. <EM>Example:&nbsp;
								</EM><EM>( ltv &gt;= 80 ) and ( cltv &gt; 80 )</EM></EM></TD>
					</TR>
					<TR>
						<TD class="fieldLabel">Keywords
							<asp:dropdownlist id="Symbol_dd" runat="server" onchange="insertKeyword(document.getElementById('Condition_ed'),this);"/>
							<INPUT onclick="insertKeyword(document.getElementById('Condition_ed'),document.getElementById('Symbol_dd'));" type="button" value="Insert"></TD>
					</TR>
					<TR>
						<TD>
							<P>
								<asp:textbox id="Condition_ed" runat="server" Width="100%" Font-Size="X-Small" Height="350px" TextMode="MultiLine"></asp:textbox><STRONG></STRONG></P>
						</TD>
					</TR>
					<TR>
						<TD class="FormTableHeader1">Results</TD>
					</TR>
					<TR>
						<TD>
						</TD>
					</TR>
					<TR>
						<TD>
							<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD class="FieldLabel" width="125">Condition Syntax</TD>
									<TD>
										<asp:textbox id="ConditionResult_ed" runat="server" Width="100%" ForeColor="Black" ReadOnly="True" BackColor="Silver" TextMode="MultiLine" Rows="3" EnableViewState="False"></asp:textbox></TD>
								</TR>
							</TABLE>
							<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD class="FieldLabel" width="125">Condition Flat</TD>
									<TD>
										<asp:textbox id="ConditionFlat_ed" runat="server" Width="100%" ForeColor="Black" ReadOnly="True" BackColor="Silver" TextMode="MultiLine" Height="80px"></asp:textbox></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD align="middle">
							<INPUT onclick="document.ConditionEdit.Condition_ed.value = '';" type="button" value=" Clear ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<ml:NoDoubleClickButton id="Ok_btn" runat="server" Text="  Test  " onclick="Ok_btn_Click"></ml:NoDoubleClickButton>&nbsp;&nbsp;&nbsp;
							&nbsp; <INPUT onclick="onClosePopup();" type="button" value=" Close ">&nbsp;&nbsp;&nbsp;&nbsp;</TD>
					</TR>
				</TABLE>
			</CENTER>
		</FORM>
	</body>
</HTML>
