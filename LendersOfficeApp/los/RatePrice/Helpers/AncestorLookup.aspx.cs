using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LqbGrammar.DataTypes;
using LendersOffice.Drivers.SqlServerDB;

namespace LendersOfficeApp.los.RatePrice.Helpers
{
	/// <summary>
	/// Summary description for AncestorLookup.
	/// </summary>
	public partial class AncestorLookup : LendersOffice.Common.BasePage
	{
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
			if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
			{
				throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
			}

			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void m_btnLookup_Click(object sender, System.EventArgs e)
		{
			string policyId = m_PolicyId.Text ;

			try
			{
				new Guid(policyId) ;
			}
			catch
			{
				m_Result.Text = "INVALID POLICY ID" ;
				return ;
			}

			int loopCracker = 0 ;
			System.Text.StringBuilder sb = new System.Text.StringBuilder() ;
			while (policyId != "")
			{
                // 11/16/2007 dd - Reviewed and fixed
                var listParams = new SqlParameter[] { DataAccess.DbAccessUtils.SqlParameterForVarchar("@pid", policyId) };
				string sql = "SELECT ParentPolicyId, PricePolicyDescription FROM Price_Policy WHERE PricePolicyId=@pid";

                Action<IDataReader> readHandler = delegate(IDataReader sdr)
                {
                    if (sdr.Read())
                    {

                        sb.Append(string.Format("{0}::", (string)sdr["PricePolicyDescription"]));
                        policyId = CommonLib.SafeConvert.ToString(sdr["ParentPolicyId"]);
                    }
                    else
                    {
                        policyId = "";
                    }
                };

                DataAccess.DBSelectUtility.ProcessDBData(DataAccess.DataSrc.LpeSrc, sql, null, listParams, readHandler);
				
				loopCracker++ ;
				if (loopCracker > 100) break ;
			}
			sb.Append("ROOT") ;

			if (loopCracker > 100)
				m_Result.Text = "SOMETHING'S WRONG. THIS PRICE POLICY HAS OVER 100 ANCESTORS" ;
			else
				m_Result.Text = sb.ToString() ;
		}
	}
}
