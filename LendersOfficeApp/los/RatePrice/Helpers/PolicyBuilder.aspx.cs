namespace LendersOfficeApp.los.RatePrice.Helpers
{
    using System;
    using System.Xml;
    using System.Collections;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Data;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.Web;
    using System.Web.SessionState;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.RatePrice.Helpers;
    using LendersOffice.Security;

    /// <summary>
    /// Summary description for PolicyBuilder.
    /// </summary>
    public partial class PolicyBuilder : BasePage
	{

		protected OutputBuilder m_Output = new OutputBuilder() ;

        private Guid m_brokerID
        {
            get { return BrokerUserPrincipal.CurrentPrincipal.BrokerId; }
        }

        private static Guid GetGuid(string s)
        {
            Guid value = Guid.Empty;
            try
            {
                if (s != null && s != "")
                    value = new Guid(s);
            }
            catch { }

            if (value == Guid.Empty)
            {
                ErrorUtilities.DisplayErrorPage(new CBaseException(ErrorMessages.Generic,  s + " is not a valid Guid format."), false, Guid.Empty, Guid.Empty);
            }

            return value;
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
		if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission( LendersOffice.Security.Permission.CanModifyLoanPrograms ) )
		{
			throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
		}

			if (!IsPostBack)
			{
				ViewState["parentpolicyid"] = GetGuid(Request["parentpolicyid"]) ;

				PolicyNodeConstructor root = PolicyNodeConstructor.GetRootPolicy((Guid)ViewState["parentpolicyid"]) ;
				m_txtParentPolicyId.Text = root.Description ;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void Ok_btn_Click(object sender, System.EventArgs e)
		{
			if (!this.IsValid) return ;

            PolicyParser pparser = new PolicyParser(m_PolicyText.Text, m_Output, (Guid)ViewState["parentpolicyid"], m_brokerID, m_ValidateOnly.Checked);
            pparser.Process();


            //The PolicyParser will fill m_Output with the proper messages.
			m_MessagesText.Text = m_Output.MessageText ;
			m_UntouchedPoliciesText.Text = m_Output.UntouchedPoliciesText ;
			m_UnmodifiedPoliciesText.Text = m_Output.UnmodifiedPoliciesText ;
			m_PoliciesAddedText.Text = m_Output.PoliciesAddedText ;
			m_PoliciesUpdatedText.Text = m_Output.PoliciesUpdatedText ;
			m_IgnoredText.Text = m_Output.IgnoredText ;
		}
		
	}
}
