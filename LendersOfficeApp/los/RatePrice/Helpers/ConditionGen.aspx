<%@ Page language="c#" Codebehind="ConditionGen.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.Helpers.ConditionGen" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ConditionGen</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<FORM id="ConditionEdit" method="post" runat="server">
			<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="FormTableHeader">Condition Gen</TD>
				</TR>
			</TABLE>
			<CENTER><BR>
				<TABLE id="Table1" cellSpacing="0" cellPadding="2" width="99%" border="0">
					<TR>
						<TD class="FormTableHeader1">Excel Content</TD>
					</TR>
					<TR>
						<TD><EM>Note: The first row must contain the column names. Valid column names are 
								SCORE1, SCORE2, QSCORE, LTV, CLTV, QLTV, QCLTV, LAMT, QLAMT, CASHOUTAMT, 
								NUMBEROFUNITS, UNITS,&nbsp;LTV1stLien, LOANAMOUNT1stLien, PROPERTYVALUE</EM></TD>
					</TR>
					<TR>
						<TD>
							<P>
								<asp:textbox id="Condition_ed" runat="server" Width="100%" Font-Size="X-Small" Height="300px" TextMode="MultiLine"></asp:textbox><STRONG></STRONG></P>
						</TD>
					</TR>
					<TR>
						<TD class="FormTableHeader1">Results</TD>
					</TR>
					<TR>
						<TD></TD>
					</TR>
					<TR>
						<TD>
							<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD class="FieldLabel" width="125">
										Condition</TD>
									<TD>
										<asp:textbox id="ConditionResult_ed" runat="server" Width="100%" TextMode="MultiLine" ForeColor="Black" ReadOnly="True" BackColor="Silver" Rows="10" EnableViewState="False"></asp:textbox></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD align="center">
							<INPUT onclick="document.ConditionEdit.Condition_ed.value = '';" type="button" value=" Clear ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<ml:NoDoubleClickButton id="Ok_btn" runat="server" Text="  Test  " onclick="Ok_btn_Click"></ml:NoDoubleClickButton>&nbsp;&nbsp;&nbsp; 
							&nbsp; <INPUT onclick="onClosePopup();" type="button" value=" Close ">&nbsp;&nbsp;&nbsp;&nbsp;</TD>
					</TR>
				</TABLE>
			</CENTER>
		</FORM>
	</body>
</HTML>
