<%@ Page language="c#" Codebehind="AncestorLookup.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.Helpers.AncestorLookup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AncestorLookup</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="FlowLayout">
		<form id="AncestorLookup" method="post" runat="server">
			<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="FormTableHeader">Lookup Ancestor</TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" cellSpacing="0" cellPadding="2" width="99%" border="0">
				<TR>
					<TD><font class="fieldlabel">Policy ID</font>
						<asp:TextBox id="m_PolicyId" runat="server" Width="300px" MaxLength="50"></asp:TextBox>
					</TD>
				</TR>
				<tr>
					<td align="center">
						<asp:Button id="m_btnLookup" runat="server" Text="Lookup" onclick="m_btnLookup_Click"></asp:Button>
						<INPUT onclick="onClosePopup();" type="button" value=" Close "></td>
				</tr>
				<TR>
					<TD class="FormTableHeader1">Result</TD>
				</TR>
				<tr>
					<td><asp:textbox id="m_Result" runat="server" Width="100%" TextMode="MultiLine" ForeColor="Black" ReadOnly="True" BackColor="Silver" Rows="10" EnableViewState="False"></asp:textbox></td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
