<%@ Page language="c#" Codebehind="PolicyBuilder.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.RatePrice.Helpers.PolicyBuilder" validateRequest="false" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PolicyBuilder</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript">
      function init()
      {
		<% if( IsPostBack == false ) { %>
			resize(900, 800);
		<% } %>
      }
		</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" onload="init()">
		<FORM id="PolicyRuleBuilder" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD class="FormTableHeader">Policy Rule Builder</TD>
				</TR>
			</TABLE>
			<CENTER>
				<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="99%" border="0">
					<TR>
						<TD class="FieldLabel">Parent Policy&nbsp; &nbsp;
							<asp:TextBox id="m_txtParentPolicyId" runat="server" Columns="50" ReadOnly="True"></asp:TextBox>
							<IMG src="../../../images/require_icon.gif">
                            <asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" ControlToValidate="m_txtParentPolicyId">
                                <img runat="server" src="../../../images/error_icon.gif">
                            </asp:requiredfieldvalidator>
						</TD>
						<td class="FieldLabel"><asp:CheckBox id="m_ValidateOnly" runat="server" Text="Validate Policy Data Only"></asp:CheckBox></td>
					</TR>
					<TR>
						<TD colspan="2">
							<font class="fieldlabel">Paste Policy Data Here</font> <IMG src="../../../images/require_icon.gif">
							<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="m_PolicyText" EnableClientScript="False">
                                <img runat="server" src="../../../images/error_icon.gif">
							</asp:requiredfieldvalidator>
							<br>
							<asp:TextBox id="m_PolicyText" runat="server" TextMode="MultiLine" Width="100%" Rows="10"></asp:TextBox></TD>
					</TR>
					<tr>
						<td colspan="2" class="FORMTABLEHEADER">&nbsp;</td>
					</tr>
					<TR>
						<TD align="center" colSpan="2">
							<INPUT onclick="document.PolicyRuleBuilder.m_PolicyText.value = ''; " type="button" value="Clear">&nbsp;&nbsp;&nbsp;
							<ml:NoDoubleClickButton id="Ok_btn" runat="server" Text="&nbsp;&nbsp;OK&nbsp;&nbsp;" onclick="Ok_btn_Click"></ml:NoDoubleClickButton>&nbsp;&nbsp;&nbsp;
							<INPUT onclick="onClosePopup();" type="button" value="Cancel"></TD>
					</TR>
					<TR>
						<TD colspan="2">
							<font class="fieldlabel">Messages</font>
							<br>
							<asp:TextBox id="m_MessagesText" runat="server" width="100%" Rows="5" TextMode="MultiLine" ReadOnly="True"></asp:TextBox>
						</TD>
					<TR>
					<tr>
						<td width="50%">
							<font class="fieldlabel"></font><STRONG>Policies NOT TOUCHED</STRONG>
							<asp:TextBox id="m_UntouchedPoliciesText" runat="server" Rows="5" TextMode="MultiLine" ReadOnly="True" width="100%"></asp:TextBox>
						</td>
						<td width="50%">
							<font class="fieldlabel"></font><STRONG>Policies NOT MODIFIED</STRONG>
							<br>
							<asp:TextBox id="m_UnmodifiedPoliciesText" runat="server" Rows="5" TextMode="MultiLine" ReadOnly="True" width="100%"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td width="50%">
							<font class="fieldlabel"></font><STRONG>Policies ADDED</STRONG>
							<br>
							<asp:TextBox id="m_PoliciesAddedText" runat="server" Rows="15" TextMode="MultiLine" ReadOnly="True" width="100%"></asp:TextBox>
						</td>
						<td width="50%">
							<font class="fieldlabel"></font><STRONG>Policies UPDATED</STRONG>
							<br>
							<asp:TextBox id="m_PoliciesUpdatedText" runat="server" Rows="15" TextMode="MultiLine" ReadOnly="True" width="100%"></asp:TextBox>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<font class="fieldlabel"></font><STRONG>Text ignored by the processing engine. 
								This text is prefixed with the line #.</STRONG>
							<br>
							<asp:TextBox id="m_IgnoredText" runat="server" ReadOnly="True" Rows="5" TextMode="MultiLine" width="100%"></asp:TextBox></td>
					</tr>
				</TABLE>
			</CENTER>
			<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg></FORM>
	</body>
</HTML>
