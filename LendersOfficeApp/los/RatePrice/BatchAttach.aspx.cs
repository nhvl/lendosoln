﻿namespace LendersOfficeApp.Los.RatePrice
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;

    /// <summary>
    /// Attach particular policy to multiple loan programs or price groups.
    /// </summary>
    public partial class BatchAttach : LendersOffice.Common.BasePage
    {
        /// <summary>
        ///  Program Ids or price group Ids.
        /// </summary>
        private List<Guid> attachedIds;

        /// <summary>
        /// The attach type is Loan Program or Price Group.
        /// </summary>
        private bool isLoanProgram;

        /// <summary>
        /// Gets or sets price pocily id  that will attach to multiple loan programs or price groups.
        /// </summary>
        private Guid PricePolicyId { get; set; }

        /// <summary>
        /// Gets or sets dictionary that maps valid program/price group id to its broker id.
        /// </summary>
        private Dictionary<Guid, Guid> ValidIdToBrokerDict { get; set; }

        /// <summary>
        /// Create a check box for valid attached Id. Otherwise show denial reason.
        /// </summary>
        /// <param name="dataItem">Data row view.</param>
        /// <returns>HtmlControl object.</returns>
        protected HtmlControl GenerateAttachableCheckbox(object dataItem)
        {
            DataRowView row = dataItem as DataRowView;

            HtmlControl control = null;
            if (row != null)
            {
                bool attachable = (bool)row["Attachable"];
                if (attachable)
                {
                    HtmlInputCheckBox checkbox = new HtmlInputCheckBox();
                    checkbox.Checked = true;
                    checkbox.Disabled = true;
                    control = checkbox;
                }
                else
                {
                    var span = new HtmlGenericControl("span");
                    span.InnerText = "Error: " + (string)row["Reason"];
                    control = span;
                }
            }

            return control;
        }

        /// <summary>
        /// Create a link to view loan program in detail.
        /// </summary>
        /// <param name="dataItem">Data row view.</param>
        /// <returns>HtmlControl object.</returns>
        protected HtmlControl GenerateViewProgramLink(object dataItem)
        {
            DataRowView row = dataItem as DataRowView;

            HtmlControl control = null;
            if (row != null)
            {
                string type = (string)row["Type"];
                if (string.IsNullOrEmpty(type) || type.StartsWith("Master") || type == "Derived")
                {
                    var anchor = new HtmlAnchor();
                    anchor.InnerText = "view";
                    anchor.HRef = "#";
                    anchor.Attributes.Add("onclick", "onViewClick('" + row["lLpTemplateId"] + "'); return false;");
                    control = anchor;
                }
            }

            return control;
        }

        /// <summary>
        /// This method will attach policy to loan program/price groups.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void AttachBtnClick(object sender, EventArgs e)
        {
            this.attachBtn.Visible = false;

            if (this.isLoanProgram)
            {
                this.AttachPolicyToPrograms(this.PricePolicyId, this.ValidIdToBrokerDict);
            }
            else
            {
                this.AttachPolicyToPriceGroups(this.PricePolicyId, this.ValidIdToBrokerDict);
            }

            if (this.ResultMessage.Text.Length == 0)
            {
                ClientScript.RegisterHiddenField("cmdToDo", "Close");
            }
            else
            {
                this.ResultMessage.Visible = true;
            }
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!LendersOffice.Security.BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.CanModifyLoanPrograms))
            {
                throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.GenericAccessDenied, "Must have CanModifyLoanPrograms permission to edit pricing.");
            }

            this.ResultMessage.Visible = false;

            if (!this.IsPostBack)
            {
                var idsCacheKey = RequestHelper.GetSafeQueryString("s");
                if (string.IsNullOrEmpty(idsCacheKey))
                {
                    throw CBaseException.GenericException("Missing required argument 's'");
                }

                this.attachedIds = AutoExpiredTextCache.GetUserObject<List<Guid>>(PrincipalFactory.CurrentPrincipal, idsCacheKey);
                if (this.attachedIds == null)
                {
                    throw CBaseException.GenericException("Cache item time ran out");
                }

                this.PricePolicyId = RequestHelper.GetGuid("pricepolicyid");
                this.isLoanProgram = RequestHelper.GetBool("isProgram");

                if (this.isLoanProgram)
                {
                    this.DisplayLoanProgramsInfo(this.attachedIds);
                }
                else
                {
                    this.DisplayPriceGroupsInfo(this.attachedIds);
                }
                
                this.ViewState["isLoanProgram"] = this.isLoanProgram;
                this.ViewState["PricePolicyId"] = this.PricePolicyId;
                this.ViewState["attachedIds"] = this.attachedIds;
                this.ViewState["ValidIdToBrokerDict"] = this.ValidIdToBrokerDict;
            }
            else
            {
                this.isLoanProgram = (bool)this.ViewState["isLoanProgram"];
                this.PricePolicyId = (Guid)this.ViewState["PricePolicyId"];
                this.attachedIds = this.ViewState["attachedIds"] as List<Guid>;
                this.ValidIdToBrokerDict = (this.ViewState["ValidIdToBrokerDict"] as Dictionary<Guid, Guid>) ?? new Dictionary<Guid, Guid>();
            }

            this.attachContentLabel.Text = this.isLoanProgram ? "Loan Programs" : "Price Groups";
            this.attachBtn.Enabled = this.ValidIdToBrokerDict.Count > 0;
        }

        /// <summary>
        /// Display loan program names from their ids.
        /// </summary>
        /// <param name="loanProgramIds">The input loan program ids.</param>
        private void DisplayLoanProgramsInfo(List<Guid> loanProgramIds)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            string sql = 
@"SELECT lLpTemplateId, lLpTemplateNm, BrokerId,
Enabled = case IsEnabled when 1 then 'Yes' else 'No' end,
CASE WHEN IsMaster = 1 AND lBaseLpId IS NULL THEN 'Master'
     WHEN IsMaster = 0 AND lBaseLpId IS NOT NULL THEN 'Derived'
     WHEN IsMaster = 0 AND lBaseLpId IS NULL THEN ''
     WHEN IsMaster = 1 AND lBaseLpId IS NOT NULL THEN 'Invalid' END AS Type
FROM LOAN_PROGRAM_TEMPLATE WHERE IsMasterPriceGroup=0 AND lLpTemplateId " + DbTools.CreateParameterized4InClauseSql("lpId", loanProgramIds, parameters);

            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(DataSrc.LpeSrc, ds, sql, null, parameters);

            // detect invalid policy ids.
            var assocTable = ds.Tables[0];
            assocTable.Columns.Add("Attachable", typeof(bool));
            assocTable.Columns.Add("Reason", typeof(string));

            CRowHashtable rowTable = new CRowHashtable(assocTable.Rows, "lLpTemplateId");
            var forceNoDict = new Dictionary<Guid, ForceNoManager>();

            this.ValidIdToBrokerDict = new Dictionary<Guid, Guid>();
            foreach (var programId in loanProgramIds)
            {
                if (rowTable.HasRowOfKey(programId))
                {
                    DataRow row = rowTable.GetRowByKey(programId);
                    string type = row["Type"].ToString();
                    Guid brokerId = (Guid)row["BrokerId"];

                    bool attachable = !type.StartsWith("Invalid");
                    if (attachable)
                    {
                        ForceNoManager fnm;
                        if (!forceNoDict.TryGetValue(brokerId, out fnm))
                        {
                            fnm = new ForceNoManager(brokerId);
                            forceNoDict.Add(brokerId, fnm);
                        }

                        if (fnm.IsFnAssoc(this.PricePolicyId, programId) != Guid.Empty)
                        {
                            attachable = false;
                            row["Reason"] = "Force No";
                        }
                        else
                        {
                            row["Reason"] = string.Empty;
                            this.ValidIdToBrokerDict.Add(programId, brokerId);
                        }
                    }
                    else
                    {
                        row["Reason"] = type;
                    }

                    row["Attachable"] = attachable;
                }
                else
                {
                    DataRow newRow = ds.Tables[0].NewRow();
                    newRow["lLpTemplateId"] = programId;
                    newRow["lLpTemplateNm"] = "Program Id doesn't not exist.";
                    newRow["Type"] = "?";
                    newRow["Enabled"] = "?";
                    newRow["Attachable"] = false;
                    newRow["Reason"] = "Not exist";
                    assocTable.Rows.Add(newRow);
                }
            }

            // rebuild
            rowTable = new CRowHashtable(assocTable.Rows, "lLpTemplateId");

            // build new table such that its row order is same as input order
            var newTable = ds.Tables[0].Clone();
            foreach (var programId in loanProgramIds)
            {
                newTable.Rows.Add(rowTable.GetRowByKey(programId).ItemArray);
            }

            this.dgPrograms.DataSource = newTable.DefaultView;
            this.dgPrograms.DataBind();
        }

        /// <summary>
        /// Display Price Group Names from their ids.
        /// </summary>
        /// <param name="priceGroupIds">Price Group Ids.</param>
        private void DisplayPriceGroupsInfo(List<Guid> priceGroupIds)
        {
            DataTable priceGroupTable = new DataTable("PriceGroups");
            priceGroupTable.Columns.Add("Attachable", typeof(bool));
            priceGroupTable.Columns.Add("Reason", typeof(string));
            priceGroupTable.Columns.Add("LpePriceGroupId", typeof(Guid));
            priceGroupTable.Columns.Add("Name", typeof(string));
            priceGroupTable.Columns.Add("Active", typeof(string));

            DbConnectionInfo curConnectionInfo = null;

            this.ValidIdToBrokerDict = new Dictionary<Guid, Guid>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    var brokerId = (Guid)reader["BrokerId"];

                    var brokerConnectionInfo = DbConnectionInfo.GetConnectionInfo(brokerId);

                    // To prevent adding duplicate price groups, only add
                    // this price group if the owning broker uses this
                    // database.
                    if (curConnectionInfo.Id != brokerConnectionInfo.Id)
                    {
                        continue;
                    }

                    DataRow newRow = priceGroupTable.NewRow();
                    newRow["Attachable"] = true;
                    newRow["Reason"] = string.Empty;
                    newRow["LpePriceGroupId"] = (Guid)reader["LpePriceGroupId"];
                    newRow["Name"] = reader["LpePriceGroupName"];
                    newRow["Active"] = (bool)reader["ExternalPriceGroupEnabled"] ? "Yes" : "No";

                    priceGroupTable.Rows.Add(newRow);

                    this.ValidIdToBrokerDict.Add((Guid)reader["LpePriceGroupId"], brokerId);
                }
            };

            foreach (DbConnectionInfo connectionInfo in DbConnectionInfo.ListAll())
            {
                curConnectionInfo = connectionInfo;

                List<SqlParameter> parameters = new List<SqlParameter>();
                string sql = "SELECT BrokerId, LpePriceGroupId, ExternalPriceGroupEnabled, LpePriceGroupName FROM LPE_Price_Group WHERE LpePriceGroupId "
                             + DbTools.CreateParameterized4InClauseSql("lpePriceGroupId", priceGroupIds, parameters);
                DBSelectUtility.ProcessDBData(connectionInfo, sql, null, parameters, readHandler);
            }

            foreach (var priceGroupId in priceGroupIds)
            {
                if (!this.ValidIdToBrokerDict.ContainsKey(priceGroupId))
                {
                    DataRow newRow = priceGroupTable.NewRow();
                    newRow["Attachable"] = false;
                    newRow["Reason"] = "Not exist";
                    newRow["LpePriceGroupId"] = priceGroupId;
                    newRow["Name"] = "Price group Id doesn't exist";
                    newRow["Active"] = "?";

                    priceGroupTable.Rows.Add(newRow);
                }
            }

            this.dgPricegroups.DataSource = priceGroupTable.DefaultView;
            this.dgPricegroups.DataBind();
        }

        /// <summary>
        /// Save attach information to database.
        /// </summary>
        /// <param name="policyId">Price Policy Id.</param>
        /// <param name="programToBrokerDict">The dictionary maps program id to its broker id. </param>
        private void AttachPolicyToPrograms(Guid policyId, Dictionary<Guid, Guid> programToBrokerDict)
        {
            Tools.LogInfo($"BatchAttach: policy {policyId} to programs {string.Join(", ", programToBrokerDict.Values)}");

            var sb = new StringBuilder();
            foreach (Guid programId in programToBrokerDict.Keys)
            {
                string errMsg = string.Empty;
                try
                {
                    PricePolicyListByProductService.SetProgramAssociation(programToBrokerDict[programId], programId, policyId, E_UiPolicyProgramAssociation.Yes /* attach*/, true /*turnOptimizerOffChk*/, out errMsg);
                }
                catch (CBaseException exc)
                {
                    errMsg = exc.Message;
                    Tools.LogError($"SetProgramAssociation( progId: {programId}, policyId: {policyId},  assocVal: Yes) error", exc);
                }
                catch (SqlException exc)
                {
                    errMsg = exc.Message;
                    Tools.LogError($"SetProgramAssociation( progId: {programId}, policyId: {policyId},  assocVal: Yes) error", exc);
                }
                catch (Exception exc)
                {
                    Tools.LogError($"SetProgramAssociation( progId: {programId}, policyId: {policyId},  assocVal: Yes) error", exc);
                    throw;
                }

                if (!string.IsNullOrEmpty(errMsg))
                {
                    if (sb.Length == 0)
                    {
                        sb.AppendLine($"Cannot attach the policy {policyId} to following programs:");
                    }

                    sb.AppendLine($"    {programId}: {errMsg}");
                }
            }

            this.ResultMessage.Text = sb.ToString();
        }

        /// <summary>
        /// Save attach information to database.
        /// </summary>
        /// <param name="policyId">Price Policy Id.</param>
        /// <param name="priceGroupToBrokerDict">The dictionary maps price group id to its broker id. </param>
        private void AttachPolicyToPriceGroups(Guid policyId, Dictionary<Guid, Guid> priceGroupToBrokerDict)
        {
            Tools.LogInfo($"BatchAttach: policy {policyId} to price groups {string.Join(", ", priceGroupToBrokerDict.Values)}");
            List<SqlParameter> parameters = new List<SqlParameter>();

            string sql = "select * from product_price_association where PricePolicyId = @PricePolicyId AND ProductId "
                          + DbTools.CreateParameterized4InClauseSql("ProductId", priceGroupToBrokerDict.Keys.ToList(), parameters);
            parameters.Add(new SqlParameter("@PricePolicyId", policyId));

            using (CDataSet assocDataset = new CDataSet())
            {
                assocDataset.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, sql, parameters);
                DataTable assocTable = assocDataset.Tables[0];

                CRowHashtable assocRows = new CRowHashtable(assocTable.Rows, "ProductId");

                foreach (var priceGroupId in priceGroupToBrokerDict.Keys)
                {
                    if (!PricePolicyListByProductService.CheckExistenceOfLoanProgramTemplate(priceGroupId))
                    {
                        CLoanProductData.CreatePriceGroupMaster(priceGroupId, priceGroupToBrokerDict[priceGroupId]);
                    }

                    if (assocRows.HasRowOfKey(priceGroupId))
                    {
                        assocRows.GetRowByKey(priceGroupId)["AssocOverrideTri"] = E_TriState.Yes;
                    }
                    else
                    {
                        DataRow newRow = assocTable.NewRow();
                        newRow["ProductId"] = priceGroupId;
                        newRow["PricePolicyId"] = policyId;
                        newRow["AssocOverrideTri"] = E_TriState.Yes;

                        assocTable.Rows.Add(newRow);
                    }
                }

                try
                {
                    assocDataset.Save();
                }
                catch (SqlException exc)
                {
                    this.ResultMessage.Text = exc.Message;
                    Tools.LogError("AttachPolicyToPriceGroups error.", exc);
                }
                catch (LqbGrammar.Exceptions.LqbException exc)
                {
                    this.ResultMessage.Text = exc.Message;
                    Tools.LogError("AttachPolicyToPriceGroups error.", exc);
                }
                catch (Exception exc)
                {
                    Tools.LogError("AttachPolicyToPriceGroups error.", exc);
                    throw;
                }
            }
        }
    }
}