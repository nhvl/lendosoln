﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Security;

namespace LendersOfficeApp.los
{
    public partial class myprofileservice : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "ResendConfirmation":
                    ResendConfirmation();
                    break;
                case "RegisterForAuthenticator":
                    RegisterForAuthenticator();
                    break;
                case "ValidateAndSaveAuthenticatorToken":
                    ValidateAndSaveAuthenticatorToken();
                    break;
            }
        }

        private void ResendConfirmation()
        {
            string email = GetString("SupportEmail");
            EmployeeDB db = EmployeeDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId);
            if (db.SupportEmail != email)
            {
                SetResult("Status", "Error");
                SetResult("Error", "Please save the page to send a confirmation");
                return; 
            }

            db.ResendSupportEmailConfirmation();
            SetResult("Status", "OK");
        }

        /// <summary>
        /// Creates registration information for authenticator.
        /// </summary>
        private void RegisterForAuthenticator()
        { 
            var data = MultiFactorAuthCodeUtilities.GenerateAuthenticatorRegistrationInfo(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.UserId);
            if (data == null)
            {
                SetResult("Status", "Error");
                SetResult("Error", "OTP Service is not available. Please try again later.");
                return;
            }

            var key = Guid.NewGuid();
            AutoExpiredTextCache.AddToCacheByUser(PrincipalFactory.CurrentPrincipal, data.Seed, TimeSpan.FromMinutes(60), key);

            SetResult("Key", key);
            SetResult("ManualCode", data.ManualCode);
            SetResult("Url", data.QrCodeUrl);
        }

        /// <summary>
        /// Validates the passed in the token.
        /// </summary>
        private void ValidateAndSaveAuthenticatorToken()
        {
            string seed = AutoExpiredTextCache.GetUserString(PrincipalFactory.CurrentPrincipal, GetString("locator"));
            var result = MultiFactorAuthCodeUtilities.ValidateAuthenticatorToken(seed, GetString("token"));

            if (result.HasValue)
            {
                var empId = EmployeeDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.EmployeeId);
                empId.MfaOtpSeed = seed;
                empId.Save(PrincipalFactory.CurrentPrincipal);
                SetResult("Status", "OK");
                SetResult("IsValid", result.Value.ToString());
                return;
            }

            SetResult("Status", "Error");
            SetResult("Error", "OTP Service is not available. Please try again later.");
        }
    }
}
