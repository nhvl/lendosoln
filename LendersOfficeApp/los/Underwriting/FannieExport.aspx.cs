using System;
using LendersOffice.Common;
using LendersOffice.Conversions;

namespace LendersOfficeApp.los.Underwriting
{
	public partial class FannieExport : MinimalPage
	{

		protected void PageLoad(object sender, System.EventArgs e)
		{
            ExportFannie32();
		}

        private void ExportFannie32() 
        {
            FannieMae32Exporter exporter = new FannieMae32Exporter(m_loanId);

            Response.Clear();
            Response.ContentType = "application/text";
            Response.AddHeader("Content-Disposition", $"attachment; filename=\"{exporter.OutputFileName}\"");

            byte[] buffer = exporter.Export();

            Response.OutputStream.Write(buffer, 0, buffer.Length);
            Response.Flush();
            Response.End();
            
        }
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

		protected Guid m_loanId 
		{
			get { return RequestHelper.LoanID; }
		}
	}
}
