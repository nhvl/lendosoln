<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeamPicker.aspx.cs" Inherits="LendersOfficeApp.Los.TeamPicker" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="LendersOffice.AntiXss" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Lending QB</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="FormTableHeader">Teams to select for underwriting team:</div>
        <br />
        <div>
            <asp:RadioButton id="AssignedTeams" Checked="true" runat="server" GroupName="searchType" /> <asp:RadioButton ID="AllTeams" runat="server" GroupName="searchType" />
            <br />
            <br />
            <asp:Button id="searchBtn" runat="server" Text="Search" />
            <br />
            <br />
            <div style="height:350px; overflow:auto;">
            <asp:GridView ID="TeamsGrid" runat="server" AutoGenerateColumns="false" style="width:100%;">
                 <HeaderStyle cssclass="GridHeader AlignLeft" />
                        <AlternatingRowStyle cssclass="GridAlternatingItem HoverHighlight" />
                        <RowStyle cssclass="GridItem HoverHighlight" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>Team Name</HeaderTemplate>
                        <ItemTemplate>
                            <a href="#" onclick="returnName('<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamId").ToString()) %>', '<%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamName").ToString()) %>');"><%#AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "TeamName").ToString()) %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            </div>
            <div style="text-align:center">
                <input type="button" id="clearBtn" onclick="clearAssignment();" value="Clear assignment" />
                &nbsp
                <input type="button" onclick="onClosePopup();" value="Cancel" />
            </div>
        </div>
    </div>
    </form>
<script>
    function clearAssignment()
    {
        var args = new Object();
        args["RoleId"] = <%=AspxTools.JsString(RoleId) %>;
        args["LoanId"] = <%=AspxTools.JsString(LoanId) %>;
        gService.EditTeamsService.call("ClearLoanAssignment", args);
        onClosePopup({OK:true, label:"(assign)"});
    }

    function returnName(teamId, teamName) {
       var args = new Object();
       args["TeamId"] = teamId;
       args["LoanId"] = <%=AspxTools.JsString(LoanId) %>;
       gService.EditTeamsService.call("AssignLoan", args);
       onClosePopup({OK:true, label:teamName});
    }
</script>
</body>
</html>
