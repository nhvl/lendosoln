﻿#region Generated Code
namespace LendersOfficeApp.los
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using System.Linq;

    /// <summary>
    /// A web service for asynchronously retrieving a user's available loan templates.
    /// </summary>
    public partial class LoanTemplateListService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets the new loan's purpose.
        /// </summary>
        private string Purpose
        {
            get { return GetString("purpose", string.Empty); }
        }

        /// <summary>
        /// Gets a value indicating whether the new loan is a test loan.
        /// </summary>
        private bool IsTest
        {
            get { return GetBool("isTest", false); }
        }

        /// <summary>
        /// Gets a value indicating whether the new loan is a lead.
        /// </summary>
        private bool IsLead
        {
            get { return GetBool("isLead", false); }
        }

        /// <summary>
        /// Gets the user's principal.
        /// </summary>
        private AbstractUserPrincipal Principal
        {
            get { return PrincipalFactory.CurrentPrincipal; }
        }

        /// <summary>
        /// Gets the user's broker.
        /// </summary>
        private BrokerDB Broker
        {
            get { return Principal.BrokerDB; }
        }

        /// <summary>
        /// This method takes in a method's name and calls the corresponding method.
        /// </summary>
        /// <param name="methodName">The name of the method to be called.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "RetrieveTemplates":
                    RetrieveTemplates();
                    break;
                default:
                    throw new CBaseException("Failed to create the loan.  Please try again.", "The method '" + methodName + "' is not handled in LoanCreateService.aspx.cs");
            }
        }

        /// <summary>
        /// Retrieves a list of templates.
        /// </summary>
        private void RetrieveTemplates()
        {
            if (this.IsLead && !this.Principal.BrokerDB.IsEditLeadsInFullLoanEditor)
            {
                throw new CBaseException("The broker is not set up to create leads from templates.", "Trying to create a lead from a template without the lender setting 'IsEditLeadsInFullLoanEditor'.");
            }

            bool filterByBranch = !Principal.HasPermission(Permission.AllowAccessToTemplatesOfAnyBranch);

            SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerId", Principal.BrokerId),
                    new SqlParameter("@BranchId", Principal.BranchId),
                    new SqlParameter("@EmployeeId", Principal.EmployeeId),
                    new SqlParameter("@FilterByBranch", filterByBranch)
                };

            DataSet dataSet = new DataSet();
            DataSetHelper.Fill(dataSet, Principal.BrokerId, "RetrieveLoanTemplateByBrokerID", parameters);

            if (dataSet.Tables.Count > 0)
            {
                dataSet.Tables[0].DefaultView.Sort = "sLNm ASC";
                var view = dataSet.Tables[0].DefaultView;

                if (!string.IsNullOrEmpty(this.Purpose))
                {
                    if (string.Equals(this.Purpose, "purchase", StringComparison.OrdinalIgnoreCase))
                    {
                        view.RowFilter = "sLPurposeT = 0 and sIsLineOfCredit = 0";
                    }
                    else if (string.Equals(this.Purpose, "refinance", StringComparison.OrdinalIgnoreCase))
                    {
                        view.RowFilter = "sIsLineOfCredit = 0 AND (sLPurposeT = 1" // Refin
                            + "OR sLPurposeT = 2" // RefinCashout
                            + "OR sLPurposeT = 6" // FHA Streamline Refi
                            + "OR sLPurposeT = 7" // VaIrrl
                            + "OR sLPurposeT = 8)"; // Home Equity
                    }
                    else if (string.Equals(this.Purpose, "HELOC", StringComparison.OrdinalIgnoreCase))
                    {
                        view.RowFilter = "sIsLineOfCredit = 1";
                    }
                    else if (Purpose.ToLower() == "construction")
                    {
                        view.RowFilter = "sIsLineOfCredit = 0 AND (sLPurposeT = 3 OR sLPurposeT = 4)"; // Construction OR Construction Perm
                    }
                }

                List<object> templates = new List<object>();
                for (int i = 0; i < view.Count; i++)
                {
                    DataRowView row = view[i];
                    templates.Add(new { TemplateId = (Guid)row["sLId"], TemplateName = (string)row["sLNm"] });
                }

                SetResult("templates", SerializationHelper.JsonNetSerialize(templates));
            }
        }
    }
}