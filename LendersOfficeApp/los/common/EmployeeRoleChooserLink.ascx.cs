using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CommonLib;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.AntiXss;
using LendersOffice.Common;

namespace LendersOfficeApp.los.common
{
	/// <summary>
	///	Summary description for EmployeeRoleChooserLink.
	/// </summary>

	[ ControlBuilderAttribute( typeof( FixedChoiceControlBuilder ) )
	]
	[ ParseChildrenAttribute( false )
	]
	public partial  class EmployeeRoleChooserLink : System.Web.UI.UserControl , INamingContainer
	{

		private ArrayList m_Choices = new ArrayList();

		private String m_Mode = "Simple";
		private String m_Role = "";
		private String m_Desc = "";

		public event RoleChooserChoiceFilter FilterChoice;

		public String ChoiceFilter
		{
			// Access member.

			set
			{
				Delegate dF = Delegate.CreateDelegate
					( typeof( RoleChooserChoiceFilter )
					, Page
					, value
					);

				if( dF != null )
				{
					FilterChoice += dF as RoleChooserChoiceFilter;
				}
			}
		}


		public string Width 
		{
			set 
			{
				m_User.Style.Add("width", Unit.Parse(value).ToString() );
			}
		}

		public Boolean Enabled
		{
			// Access member.

			set
			{
				m_Base.Enabled = value;
			}
		}

        // 8/31/2013 dd - GenerateScripts is obsolete.
        // REMOVE Once no one using it.
		public bool GenerateScripts { get; set;}

		public IList Choices
		{
			// Access member.

			get
			{
				return m_Choices;
			}
		}

		public string Data
		{
			set
			{
				m_Data.Value = value;
			}
			get
			{
				return m_Data.Value;
			}
		}

		public string Text
		{
			set
			{
				m_Text.Value = value;
			}
			get
			{
				return m_Text.Value;
			}
		}

		public string Mode
		{
			set
			{
				m_Mode = value;
			}
			get
			{
				return m_Mode;
			}
		}

		public string Role
		{
			set
			{
				m_Role = value;
			}
			get
			{
                if (RoleType.HasValue)
                {
                    // 8/31/2013 dd - If RoleType is define then use that instead of
                    // user define value.
                    return LendersOffice.Security.Role.Get(RoleType.Value).Desc;
                }
                else
                {
                    return m_Role;
                }
			}
		}

        public E_RoleT? RoleType
        {
            get;
            set;
        }

		public string Desc
		{
			set
			{
				m_Desc = value;
			}
			get
			{
                if (RoleType.HasValue)
                {
                    // 8/31/2013 dd - If RoleType is define then use that instead of
                    // user define value.
                    return LendersOffice.Security.Role.Get(RoleType.Value).ModifiableDesc;
                }
                else
                {
                    return m_Desc;
                }
			}
		}

		public String DataID
		{
			// Access member.

			get
			{
				return m_Data.ClientID;
			}
		}

        public string BaseDivClientId
        {
            get
            {
                return m_Base.ClientID; 
            }
        }

        public bool PickerVisible
        {
            set { this.m_List.Visible = value; }
        }

		protected void repeater_OnItemDataBound ( object x, RepeaterItemEventArgs e ) 
		{
			FixedChoice fc1 = e.Item.DataItem as FixedChoice; 
			
			if ( e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem ) 
			{
				HyperLink l = e.Item.FindControl("controlLink") as HyperLink; 
				FixedChoice fc = e.Item.DataItem as FixedChoice;
                l.Text = AspxTools.HtmlString(fc.LinkLabel);

				if ( fc.Text.ToLower() == "assign" ) 
				{
                    var clickHandler = string.Format(
                        "if(this.disabled || hasDisabledAttr(this)){{return false}}else{{onChooserLinkClick({0},{1},{2},{3},{4},{5},{6});}}",
                        AspxTools.JsString(m_User.ClientID),
                        AspxTools.JsString(m_Text.ClientID),
                        AspxTools.JsString(m_Data.ClientID),
                        AspxTools.JsString(Role),
                        AspxTools.JsString(Desc),
                        AspxTools.JsString(Mode),
                        AspxTools.JsString(EnableCopyToOfficialContact.ToString()));

                    l.Attributes.Add("onclick", clickHandler); 
				}
				else 
				{
                    //sanitize the text and data here
                    var onClickAttribute = "onChooserFixClick( parentElement.parentElement , " + 
                        AspxTools.JsString(fc.Text) + " , " + 
                        AspxTools.JsString(fc.Data) + " ,"  +
                        AspxTools.JsBool(this.Role.Equals("Originating Company", StringComparison.OrdinalIgnoreCase)) +
                        " ); return false; ";

                    l.Attributes.Add("onclick", onClickAttribute);
				}
			}
		}
		 
		/// <summary>
		/// Save key link attributes.
		/// </summary>
		protected override void OnPreRender( System.EventArgs a )
		{
            if (this.Page.ClientScript.IsClientScriptBlockRegistered(typeof(EmployeeRoleChooserLink), "Script") == false)
            {
                // 8/31/2013 dd - This to make sure only 1 client script code is generate.
                this.scripts.Visible = true; // GenerateScripts;
                this.Page.ClientScript.RegisterClientScriptBlock(typeof(EmployeeRoleChooserLink), "Script", "");
            }
            else
            {
                this.scripts.Visible = false;
            }

			for( int i = 0 ; i < m_Choices.Count ; ++i )
			{
				FixedChoice fC = m_Choices[ i ] as FixedChoice;

				if( FilterChoice != null )
				{
					FilterChoice( this , fC );
				}

				if( fC.Use == false )
				{
					m_Choices.RemoveAt( i );

					--i;
				}
			}

			if( m_Choices.Count > 0 )
			{
				m_List.DataSource = m_Choices;
				m_List.DataBind();
			}

			base.OnPreRender( a );
		}

		/// <summary>
		/// Add parsed child control.
		/// </summary>

		protected override void AddParsedSubObject( Object o )
		{
			// Only add child items.

			if( o is FixedChoice == true )
			{
				m_Choices.Add( o );

				return;
			}

			base.AddParsedSubObject( o );
		}

        public bool EnableCopyToOfficialContact
        {
            get
            {
                if (ViewState["CopyToContactOption"] != null)
                    return SafeConvert.ToString(ViewState["CopyToContactOption"]).ToLower() == "true";
                else
                    return true;
            }
            set
            {
                ViewState["CopyToContactOption"] = value;
            }
        }

		/// <summary>
		/// Initialize this control.
		/// </summary>

		protected override void LoadViewState( Object s )
		{
			// Pull the last saved state from the repository.  If
			// view state is disabled, I suspect we'll never find
			// any state in here.

			base.LoadViewState( s );

			if( ViewState[ "Mode" ] != null )
			{
				m_Mode = ViewState[ "Mode" ].ToString();
			}

			if( ViewState[ "Role" ] != null )
			{
				m_Role = ViewState[ "Role" ].ToString();
			}

			if( ViewState[ "Desc" ] != null )
			{
				m_Desc = ViewState[ "Desc" ].ToString();
			}
		}

		/// <summary>
		/// Save out this control.
		/// </summary>

		protected override object SaveViewState()
		{
			// Write back are last edited state before committing
			// the viewstate to the page via render.

			ViewState.Add( "Mode" , m_Mode );
			ViewState.Add( "Role" , m_Role );
			ViewState.Add( "Desc" , m_Desc );

			return base.SaveViewState();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion

	}

	/// <summary>
	/// Allow pages to hook in and filter out defined choices on render.
	/// </summary>

	public delegate void RoleChooserChoiceFilter( Object sender , FixedChoice choice );

	/// <summary>
	/// Override child control filter and provide implementation when
	/// items are built.
	/// </summary>

	public class FixedChoiceControlBuilder : ControlBuilder
	{
		/// <summary>
		/// Override child control filter and provide implementation
		/// when items are built.
		/// </summary>

		public override Type GetChildControlType( String tagName , IDictionary attributes )
		{
			if( tagName.ToLower() == "fixedchoice" )
			{
				return typeof( FixedChoice );
			}

			return null;
		}

	}

	/// <summary>
	/// Provide container of custom children controls.
	/// </summary>

	public class FixedChoice : Control , INamingContainer
	{
		/// <summary>
		/// Maintain text and data values for this choice.
		/// </summary>

		public string LinkLabel
		{
			get
			{
                if (string.IsNullOrEmpty(Label) == false)
                {
                    // 8/31/2013 dd - Label is set manually by user through code.
                    return Label;
                }
				else if( HasControls() == true )
				{
                    // The text between the opening and closing tag is parsed
                    // into a LiteralControl.
                    // For example: <FixedChoice>This becomes a LiteralControl.</FixedChoice>
                    // I don't see anywhere where the data is bound dynamically,
                    // so the content should just be coming from the markup.
                    // Additionally, this property (LinkLabel) is only used as
                    // the Text property of a HyperLink -- any encoding that
                    // needs to be done should happen there.
                    // gf opm 464965
                    LiteralControl lC = Controls[0] as LiteralControl;

                    if (lC != null)
                    {
                        return lC.Text.TrimWhitespaceAndBOM();
                    }
                }

                return "";
			}
		}
        public string Label { get; set; }
		public string Text { get; set;}

		public string Data { get; set;}

		public bool Use { get; set;}

        private void Initialize(string text, string data, string label, bool use)
        {
            this.Text = text;
            this.Data = data;
            this.Label = label;
            this.Use = use;
        }

		#region( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

        public FixedChoice(string text, string data, string label)
        {
            Initialize(text, data, label, true);
        }
		public FixedChoice(string text, string data)
		{
			// Initialize members.
            Initialize(text, data, string.Empty, true);
		}

		/// <summary>
		/// Construct default.
		/// </summary>

		public FixedChoice()
		{
            Initialize(string.Empty, string.Empty, string.Empty, true);
		}

		#endregion

	}

}
