<%@ Control Language="c#" AutoEventWireup="false" Codebehind="header.ascx.cs" Inherits="LendersOfficeApp.los.common.header" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" enableViewState="False"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<style type="text/css">
.IEWarningBtn
{
	color: #FFA500;
	text-decoration: none;
}
.IEWarningBtn:link
{
	color: #FFA500;
	text-decoration: none;
}
.IEWarningBtn:hover
{
	color: red;
	text-decoration: underline;
}

.warning-message {
    width: 250px;
    text-align: center;
    display: inline-block;
}
</style>


<asp:PlaceHolder runat="server" ID="KayakoSupportMethod">
    <script type="text/javascript">
        function f_chatSupport(sUserName, sBrokerName, sUserMail) {
            var result =  gService.lookupRecord.call("SupportStatus");
            if (result.value.error) {
                alert(result.value.UserMessage);
                if (typeof onMyProfileClick === 'function'){
                    onMyProfileClick();
                }
                return false;
            }
            else if (result.value.redirect) {
                window.open(result.value.redirect,'_blank');
            }

            return false;
        }
    </script>
</asp:PlaceHolder>

<script type="text/javascript">

function addOption(ddl, text, value) {
  var oOption = document.createElement("OPTION");
  ddl.add(oOption);
  oOption.value = value;
  oOption.innerText = text;

}

function getOpenWindowList()
{
    return document.getElementById("currentEditWindows");
}

function removeFromCurrentLoanList(sLId) {
  var ddl = getOpenWindowList();
  var oCollection = ddl.options;
  for (var i = 0; i < oCollection.length; i++) {
    if (oCollection[i].value == sLId) {
      oCollection.remove(i);
      break;
    }
  }

  callFrameMethod(parent, "frmCode", "removeFromWindowList", [sLId]);
  disableEnableDdl(ddl);
}

function disableEnableDdl(ddl)
{
  if(ddl.length <= 1)
  {
	ddl.disabled = true;
	if(document.getElementById('m_closeAllWindowsButton'))
		document.getElementById('m_closeAllWindowsButton').disabled = true;
  }
  else
  {
	ddl.disabled = false;
	if(document.getElementById('m_closeAllWindowsButton'))
		document.getElementById('m_closeAllWindowsButton').disabled = false;
  }
}

function refreshOpenWindowList() {
  var ddl = getOpenWindowList();
  // Remove all curent options.
  for (var i = ddl.options.length - 1; i >= 0; i++)
    ddl.options.remove(i);

  //addOption(ddl, "Current edit loan windows", "0");
  //addOption(ddl, "Close all edit windows", "1");
  addOption(ddl, "-- Select one --", "2");
    
  callFrameMethod(parent, "frmCode", "appendOpenWindowList", [ddl]);
  disableEnableDdl(ddl);
}
function ddl_onchange(ddl) {
    var oOption = ddl.options[ddl.selectedIndex];
    ddl.selectedIndex = 0;

    if (oOption.value == "1") {
        callFrameMethod(parent, "frmCode", "closeAllWindows");
    } else if ((oOption.value != "2" || oOption.value != "0")) {
        callFrameMethod(parent, "frmCode", "focusWindow", [oOption.value]);
    }
}

function closeAllWindowsFromParent()
{
    callFrameMethod(parent, "frmCode", "closeAllWindows");
}

function f_chat(event)
{
    event.preventDefault();
	var sUserName = <%= AspxTools.JsString(m_userName) %>;
    var sBrokerName = <%= AspxTools.JsString(m_brokerName) %>;
    var sUserMail = <%= AspxTools.JsString(m_userMail) %>;

    f_chatSupport(sUserName, sBrokerName, sUserMail);
    return false;
}
<%--  OPM 4134 Ethan - time() & browser() called by f_chat(), needed by RecordsLookupService.aspx.cs --%>
function browser()
{
	var version = "";

	if (navigator.appName != "Microsoft Internet Explorer")
	{
		version = navigator.userAgent();
		return version;
	}
	else
	{
		if (navigator.appVersion.indexOf("MSIE") != -1)
		{
			temp = navigator.appVersion.split("MSIE");
			version = parseFloat(temp[1])
			return version;
		}
	}
}

function NavigateNewUI(){
    window.parent.parent.location.href = '../Pipeline/Pipeline.aspx';
}

<% if (ShowPipelineLink) { %>
function goToPipeline() {
  self.location = <%= AspxTools.JsString(DataAccess.Tools.VRoot) %> + '/los/pipeline.aspx';
}
<% } %>

</script>





<table cellSpacing="0" cellPadding="0" width="100%" border=0>
  <tr>
    <td width="100%">
        <div style="width:100%; background-color:Black">
            <div style="width:25%; vertical-align: top; color:white; font-weight:bold; font-size:20px; float:left">
                <img height="20" src="~/images/spacer.gif" width="5" align="absMiddle" runat="server" />
                LendingQB
                <%-- // MULTI-EDIT --%>
                <% if (ShowRecentWindowList) { %>
                    <table border=0
                        style="padding-right: 5px; padding-left: 5px; margin-left: 5px; color: white; border: groove 2px white; background-color: black;">
                      <tr>
                        <td style="FONT-WEIGHT:bold;COLOR:#ffcc00">Switch to loan window</td>
                        <td><select id="currentEditWindows" onchange="ddl_onchange(this);"></select> </td>
                        <td><input id="m_closeAllWindowsButton" type="button" value="Close All" onclick="closeAllWindowsFromParent();" /></td> 
                      </tr>
                    </table>
                <% } %>
            </div>

            <div style="width: 35%; vertical-align: top; text-align: center; float: left">
                <ml:EncodedLiteral id="m_welcomeLabel" runat="server" EnableViewState="False"></ml:EncodedLiteral>
            </div>

            <div>
                <button id="NewUINav" runat="server" visible="false" onclick="NavigateNewUI(); return false;" type="button">Go to New UI!</button>
            </div>

            <div style="vertical-align: top; float:right; right:0%;">
                <div style="text-align: right;">
                    <% if (ShowPipelineLink) { %>
                    <img height="12" src=<%=AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/images/homebox.gif")%> width="12" align="absMiddle" />&nbsp;
                    <a href="#" onclick="goToPipeline();" class="PortletLink" ToolTip="Back to pipeline">Pipeline</a>&nbsp;&nbsp;
                    <% } %>
                    <img src=<%=AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/images/chat.gif")%> align="absmiddle" />&nbsp;
                    <a runat="server" id="m_NewSupportLink" onclick="f_chat(event)" class="PortletLink"></a>&nbsp;&nbsp;
                    <img height="12" src=<%=AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/images/xbox.gif")%> width="12" align="absMiddle" />&nbsp;
                    <a href="javascript:logOut();" class="PortletLink" title="Logout">Logout</a>
                  &nbsp;&nbsp;
              </div>
              <br/>
              <div style="text-align:left;color:Red;">
                <ml:PassthroughLiteral id="m_GeneralUserNotification" runat="server" EnableViewState="False"></ml:PassthroughLiteral>
              </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </td>
  </tr>
</table>
