<%@ Control Language="c#" AutoEventWireup="false" Codebehind="EmployeeRoleChooserLink.ascx.cs" Inherits="LendersOfficeApp.los.common.EmployeeRoleChooserLink" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<ml:PassthroughLiteral Runat="server" ID="scripts" > 
	<script type="text/javascript" >
	    
		function onChooserLinkClick( userText, hiddenText, hiddenData, sRole , sDesc , sMode, sCopyToContacts) { try
		{
		    var url = "/los/EmployeeRole.aspx?role=" + sRole + "&roledesc=" + sDesc + "&show=" + sMode + "&copyContact=" + sCopyToContacts;
			if ( typeof(GetUpdatedOrignatingId) == 'function' && (sRole == 'BrokerProcessor' || sRole == 'Supervisor' || sRole == 'ExternalPostCloser' || sRole == 'ExternalSecondary')) {
		        var origid = GetUpdatedOrignatingId();
		        url += "&origid=" +   origid;

		        if (sRole === 'Supervisor' && typeof GetExSupervisorEmployeeIdsToIgnore === 'function') {
		            var exSupervisorIgnoreIds = GetExSupervisorEmployeeIdsToIgnore();

		            if (exSupervisorIgnoreIds !== null) {
		                url += "&exSupervisorIgnoreIds=" + exSupervisorIgnoreIds;
		            }
		        }
		    }
		    
			var uText = document.getElementById( userText ) ; 
			var hText = document.getElementById( hiddenText );
			var hData = document.getElementById( hiddenData );
			
			var onPopupReturn = function(arg){
				if( arg.OK != null )
				{
					if( arg.OK )
					{
					
						//the employee name has to be escaped (the names can be a source of xss)
						uText.innerText = arg.EmployeeName; 
						hText.value     = arg.EmployeeName;
						hData.value     = arg.EmployeeId;

						if (typeof (updateDirtyBit) == 'function')
						{
							updateDirtyBit();
						}

						if (sRole === "Originating Company" && typeof (onCompanyChange) == 'function') {
							onCompanyChange(arg.EmployeeId);
						}
					}
				}
				else
				{
					alert( "Selection failed." );
				}
			};

			showModal( url , null, null, null, onPopupReturn,{hideCloseButton:true});
		}
		catch( e )
		{
			alert( e.message );
		}}

		function onChooserFixClick(oBase, sText, sData, isForOC) {
		    try {
		        if (hasDisabledAttr(oBase)) {
		            return;
		        }

			    oBase.children[ 0 ].innerText = sText;
			    oBase.children[ 1 ].value     = sText;
			    oBase.children[ 2 ].value     = sData;

			    if (typeof(updateDirtyBit) == 'function') {
			        updateDirtyBit();
			    }

			    if (isForOC && typeof(setCompanyAddress) == 'function') {
			        // When clearing the selected choice for an OC, set the OC's
                    // address, city, state, and ZIP to be blank.
			        setCompanyAddress("", "", "", "");
			    }
		    }
		    catch( e )
		    {
		    }
		}

		//-->
		
	</script>
</ml:PassthroughLiteral>

<asp:Panel id="m_Base" runat="server" style="DISPLAY: inline; cursor:default">
	<span runat="server" id="m_User" class="RoleChooserLinkName"><%= AspxTools.HtmlString(Text) %></span>
	
	<input type="hidden" id="m_Text" runat="server" class="RoleChooserLinkText">
	<input type="hidden" id="m_Data" runat="server" class="RoleChooserLinkData">
	<asp:Repeater id="m_List" runat="server" OnItemDataBound="repeater_OnItemDataBound">
		<HeaderTemplate>
			[
		</HeaderTemplate>
		<SeparatorTemplate>
			<span style="PADDING-LEFT: 2px; PADDING-RIGHT: 2px;">
				|
			</span>
		</SeparatorTemplate>
		<ItemTemplate>
			<span>
				<asp:HyperLink style="cursor: pointer" Runat="server" ID="controlLink" Font-Underline="True" />
			</span>
		</ItemTemplate>
		<FooterTemplate>
			]
		</FooterTemplate>
	</asp:Repeater>
</asp:Panel>
