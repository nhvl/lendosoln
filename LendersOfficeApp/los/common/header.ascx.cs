using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.common
{
    public partial  class header : System.Web.UI.UserControl
    {
        private bool m_showPipelineLink = false;
        private bool m_showRecentWindowList = false;

        protected string m_userName = "";
        protected string m_brokerName = "";
		protected string m_userMail = "";

        public bool ShowPipelineLink 
        {
            get { return m_showPipelineLink; }
            set { m_showPipelineLink = value; }
        }
        public bool ShowRecentWindowList 
        {
            get { return m_showRecentWindowList; }
            set { m_showRecentWindowList = value; }
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal; //Page.User as BrokerUserPrincipal;
            
            if (null != principal) 
            {
                string pml_description = "";
                if (principal.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
                    pml_description = " (PML)";

                m_welcomeLabel.Text = string.Format("Welcome, {0}!", principal.DisplayName);
                m_userName = principal.DisplayName;
                m_brokerName = principal.BrokerName + pml_description;

                // OPM 194062 - Add Release Notification Warning
                m_GeneralUserNotification.Text = ConstStage.GeneralUserNotification.TrimWhitespaceAndBOM();
                m_GeneralUserNotification.Visible = !string.IsNullOrEmpty(m_GeneralUserNotification.Text);

                // opm 473272 jk 8/17/18
                m_userMail = EmployeeDB.RetrieveEmployeeEmailById(principal.BrokerId, principal.EmployeeId);

                NewUINav.Visible = principal.ActuallyShowNewLoanEditorUI;

                // Tell the user to upgrade their version of IE
                // IE8 and up will contain the "Trident" token in their user-agent string
                // If the user fakes their user-agent string, nothing of value is compromised

                m_NewSupportLink.Title = m_NewSupportLink.InnerText = ConstStage.SupportCenterLinkText;
            }
        }

		#region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);


        }
		
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);

        }
		#endregion

    }
}
