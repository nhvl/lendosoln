<%@ Control Language="c#" AutoEventWireup="false" Codebehind="footer.ascx.cs" Inherits="LendersOfficeApp.los.common.footer" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.Constants" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ OutputCache Duration="86400" VaryByParam="none" %>

<TABLE id="Table1" bgcolor="black" cellSpacing="0" cellPadding="5" border="0" width="100%">
	<TR>
		<TD style="color:white"><%=AspxTools.HtmlString(ConstAppDavid.CopyrightMessage)%></TD>
		<!-- This application uses open source software. For more information please see https://secure.lendingqb.com/inc/thirdpartysoftware.txt. -->
	</TR>
</TABLE>
