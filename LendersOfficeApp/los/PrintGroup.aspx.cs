using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using System.Collections.Generic;
using LendersOffice.Security;

namespace LendersOfficeApp.los
{
	/// <summary>
	/// Summary description for PrintGroup.
	/// </summary>
	public partial class PrintGroup : BasePage
	{
		protected void PageLoad(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingPrintGroups
                };
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
            RegisterJsScript("../common/ModalDlg/CModalDlg.js");
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
