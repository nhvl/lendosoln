﻿namespace LendersOfficeApp.los
{
    using System;
    using LendersOffice.Security;

    public partial class mobileID : LendersOffice.Common.BaseServicePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string deviceName =  Request.QueryString["mobilename"];
                string mobilePasswordValue;

                if (!string.IsNullOrEmpty(deviceName))
                {
                    bool inserted = MobileDeviceUtilities.RegisterNewDevice(deviceName, out mobilePasswordValue);
                    if (inserted)
                    {
                        //show message to user
                        mobilePassword.Text = mobilePasswordValue;

                    }
                }
            }
        }
    }
}
