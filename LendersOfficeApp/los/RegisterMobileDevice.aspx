<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisterMobileDevice.aspx.cs" Inherits="LendersOfficeApp.los.RegisterMobileDevice" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Constants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Manage Mobile Devices</title>
    <LINK href="../css/stylesheet.css" rel="stylesheet">
    <script>
     function _init() {
        var windowheight = f_getWindowDimensions(window).height || 350;
        resize(650,windowheight);
        deviceNameChange();
        if ( $('tbody:empty').parents('#registerTable').length)
        {
            $('#registerTable').hide();
        }
        else{
            $('#emptyTableNote').hide();
        }
     }
     
        function onGenerateMobileIDClick()
	    {
	        var devicename =  document.getElementById('deviceName').value;
	        if (devicename.length > 0)
	        {
	            showModal('/los/mobileID.aspx?mobilename=' + encodeURIComponent( devicename), null, null, null, function (){
                     $('#refrestLink')[0].click()
                }, {hideCloseButton:true});
	        }
		    return false;
	    }
	function confirmation() {
            return confirm("Are you sure you want to delete the registration for this mobile device?");
    }
    function deviceNameChange()
    {
        var validDevName =$.trim($('#deviceName').val());
            $('#generateBtn').prop("disabled", validDevName.length < 1);
    }
	</script>
</head>
<body style="max-width:800px;" >
<h4 class="page-header">Manage Mobile Devices</h4>
<form runat="server">
    <div style="margin-top: 10px;margin-left: 10px;margin-right: 10px; ">
		<div>
            <div style="font-size:12px">
                New Device Name
        <font id="asterisk" style="color:Red; ">*</font>
        <font style=" color: dimgray; font-size:10px">(Please name the device you wish to register)</font>
        </div>
        <div> 
            <input type="text" id="deviceName"  maxlength="200" style="width: 300px;" onchange ="deviceNameChange()" onpaste="deviceNameChange()" onkeyup="deviceNameChange()" onmousedown="deviceNameChange()"/>
        </div>
        <br />
        <div>
            <button  type="button" id="generateBtn" onclick="onGenerateMobileIDClick()">Generate Mobile ID</button>
        </div>
        <div >
                <div align="center" style="margin: 0px auto;width: 480px;" >
                    <div class=FieldLabel align="left" style=" font-size:12px"> My Mobile Devices</div>
                        <table id="registerTable" style="width:100%"   >
                        <thead><tr class="GridHeader" style="height:20px;">
                            <td ><p style="color:Black">Name</p></td>
                            <td style="text-align:center"><p style="color:Black">Date Registered</p></td>
                            <td style="text-align:center"><p style="color:Black">Last Login</p></td>
                            <td></td>
                        </tr></thead>
                        <tbody>
                            <asp:Repeater id="registerInfoRepeater" runat="server" >
                        <ItemTemplate>
                            <tr class='<%# AspxTools.HtmlString(Container.ItemIndex % 2 == 0 ? "GridAlternatingItem " :"GridItem")%>;' >
                                <td style="text-align:left"><%# AspxTools.HtmlString( (Container.DataItem as LendersOffice.Security.MobileDevice).Name )%></td>
                                <td style="text-align:left"><%# AspxTools.HtmlString( (Container.DataItem as LendersOffice.Security.MobileDevice).CreatedDate )%></td>
                                <td style="text-align:left"><%# AspxTools.HtmlString( (Container.DataItem as LendersOffice.Security.MobileDevice).LastLoginDate )%></td>
                                        <td style="text-align:center"><asp:LinkButton runat="server" OnCommand="deleteRegisterMobile" CommandArgument='<%# AspxTools.HtmlString( (Container.DataItem as LendersOffice.Security.MobileDevice).Id )%>' Text="Delete"  OnClientClick="return confirmation();" /></td>
                                </tr>
                        </ItemTemplate>
                            </asp:Repeater>
	                    </tbody>
	                    </table>
	                    <p id="emptyTableNote" align="center" style="margin: 0px auto">0 devices registered</p>
                </div>
        </div>   
        <div class="btn-close-space align-right">
            <input  onclick="onClosePopup();" type="button" value="Close"/>
        </div>
         <br />
        </div> 
        <asp:LinkButton style="display:none" runat="server" OnClick="reloadRegisterInfo" id="refrestLink" >test</asp:LinkButton>           
    </div>
    </form>
</body>
</html>
