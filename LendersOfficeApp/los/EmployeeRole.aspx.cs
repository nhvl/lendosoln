namespace LendersOfficeApp.los
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web.Services;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Roles;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;
    using MeridianLink.CommonControls;

    public partial class EmployeeRole : LendersOffice.Common.BasePage
    {
        private string m_sRoleDesc = string.Empty;
        private Guid m_pmlBrokerIdFilter = Guid.Empty;

        private string SafeQueryString(string key)
        {
            string str = RequestHelper.GetSafeQueryString(key);
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            return str;
        }

        protected string DialogTitle
        {
            get
            {
                if (RoleDesc.Equals("Loan Officer", StringComparison.OrdinalIgnoreCase))
                {
                    return "Users to select for role loan officer : ";
                }
                else if (RoleDesc.Equals("Supervisor", StringComparison.OrdinalIgnoreCase))
                {
                    return "Supervisors : ";
                }
                else if (RoleDesc.Equals("Originating Company", StringComparison.OrdinalIgnoreCase))
                {
                    return "Originating Companies : ";
                }
                else if (RoleDesc.Equals("Processor (External)", StringComparison.OrdinalIgnoreCase))
                {
                    return "PML Users to select for role processor (external): ";
                }
                else if (RoleDesc.Equals("Secondary (External)", StringComparison.OrdinalIgnoreCase))
                {
                    return "PML Users to select for role secondary (external):";
                }
                else if (RoleDesc.Equals("Post-Closer (External)", StringComparison.OrdinalIgnoreCase))
                {
                    return "PML Users to select for role post-closer (external):";
                }
                else
                {
                    return "Employees to select for role " + RoleDesc.ToLower() + " : ";
                }
            }
        }
        protected string HelpText
        {
            get
            {

                if (RoleDesc.Equals("originating company", StringComparison.OrdinalIgnoreCase) == false)
                {
                    return "(\"s\" for John Smith or Sam Cash, \"b s\" for Bob Smith)";
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        protected bool isTeam
        {
            get { return SafeQueryString("mode").Equals("team"); }
        }

        private string EmployeeStatusLabel
        {
            get
            {
                if (RoleDesc.Equals("Loan Officer", StringComparison.OrdinalIgnoreCase))
                {
                    return "User status :";
                }
                else if (RoleDesc.Equals("Processor (External)", StringComparison.OrdinalIgnoreCase) ||
                    RoleDesc.Equals("Secondary (External)", StringComparison.OrdinalIgnoreCase) ||
                    RoleDesc.Equals("Post-Closer (External)", StringComparison.OrdinalIgnoreCase))
                {
                    return "PML User status :";
                }
                else
                {
                    return "Employee status :";
                }
            }
        }
        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected Guid PmlBrokerIdFilter
        {
            get { return m_pmlBrokerIdFilter; }
        }

        private string RoleDesc
        {
            get { return SafeQueryString("roledesc"); }
        }

        private int RoleValue
        {
            get { return RequestHelper.GetInt("roleValue"); }
        }

        private string ShowMode
        {
            get { return SafeQueryString("show");}
        }

        private bool IsOnlyDisplayLqbUsers
        {
            get { return RequestHelper.GetBool("isLqbOnly"); }
        }

        private bool IsOnlyDisplayPmlUsers
        {
            get { return RequestHelper.GetBool("isPmlOnly"); }
        }

        private bool CopyToContactsEnabled
        {
            get
            {
                if (RequestHelper.GetSafeQueryString("copyContact") != null)
                {
                    return RequestHelper.GetSafeQueryString("copyContact") == "true";
                }

                return true;
            }
        }
        
        private string Role
        {
            get {
                string roleString = SafeQueryString("role");
                if (roleString == string.Empty)
                {
                    Role role = LendersOffice.Security.Role.Get((E_RoleT)RoleValue);
                    roleString = role.Desc;
                    
                }

                return roleString;
            }
        }

        private string SearchFilter
        {
            get
            {
                if( m_SearchFilter.Text.TrimWhitespaceAndBOM() != "" )
                {
                    return Utilities.EscapeForSqlLike(m_SearchFilter.Text.TrimWhitespaceAndBOM());
                }

                return null;
            }
        }

        private string ExSupervisorEmployeeIdsToIgnore
        {
            get { return RequestHelper.GetSafeQueryString("exSupervisorIgnoreIds"); }
        }

        private string ErrorMessage
        {
            set
            {
                ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
            }
        }

        private ArrayList ErrorList
        {
            set
            {
                String eList = "";

                foreach( String sE in value )
                {
                    if( eList.Length > 0 )
                    {
                        eList += "\r\n";
                    }

                    eList += sE;
                }

                ErrorMessage = eList;
            }
        }

        private string ArgsToWrite
        {
            set
            {
                ClientScript.RegisterHiddenField( "m_argsToWrite" , value.TrimEnd( ',' ) );
            }
        }

        private string CommandToDo
        {
            set
            {
                ClientScript.RegisterHiddenField( "m_commandToDo" , value.TrimWhitespaceAndBOM() );
            }
        }

        protected void PageLoad( object sender , System.EventArgs a )
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(BrokerUser.BrokerId);
            m_employeeStatusLabel.Text = EmployeeStatusLabel;
            // We now check if the user has restricted assignment turned on.
            // With assignment restriction, the user can only assign an
            // employee that belongs to their branch.  The initial requirement
            // is to simplify processing of loans originated outside a central
            // processing center (hub) and assigned to a processor within
            // the central pool.  To maintain consistency in the pairing up
            // of shared processor with remote loan officers, we create
            // branches for each loan officer and add the processors that
            // they will use as internal employees of the officer's branch.
            // This way, remote officers won't get confused about who to
            // add to their loan.
            //
            // If you want to expose broker level employees as well, then
            // add that logic to the sproc (that's the central point for
            // retrieval and still generic enough -- passing the branch
            // as optional allows us to limit the result w/o implying too
            // much app-level logic).
            //
            // 8/2/2005 kb - Per case 2533, we will allow for searching
            // for employees by name matching.  To prevent large payloads,
            // we show the too much message when the number of employees
            // is great.  For this patch, we will limit this check to
            // only officer viewing.

            RegisterJsScript("LQBPopup.js");

            Guid pmlBrokerId = RequestHelper.GetGuid("origid", Guid.Empty);
            bool isSupervisor = this.Role.ToLower() == "supervisor";

            bool isPmlRole = this.Role == ConstApp.ROLE_BROKERPROCESSOR
                || this.Role == ConstApp.ROLE_LOAN_OFFICER
                || isSupervisor
                || this.Role == ConstApp.ROLE_EXTERNAL_SECONDARY
                || this.Role == ConstApp.ROLE_EXTERNAL_POST_CLOSER;

            if (pmlBrokerId != Guid.Empty && isPmlRole)
            {
                m_pmlBrokerIdFilter = RequestHelper.GetGuid("origid");   
            }

            BrokerLoanAssignmentTable roleTable = new BrokerLoanAssignmentTable();
            m_sRoleDesc = Role;

            if( ShowMode.ToLower() == "search" )
            {
                // 8/2/2005 kb - We are now overloading this ui to be used
                // to specify loan find search parameters.  See case 2533
                // for more details.  Depending on the mode, we show one
                // set of buttons or the other.

                m_ClearAssignment.Visible = false;
            }
            else
            {
                // 8/2/2005 kb - We are now overloading this ui to be used
                // to specify loan find search parameters.  See case 2533
                // for more details.  Depending on the mode, we show one
                // set of buttons or the other.

                m_ClearAssignment.Visible = true;
            }

            if( Role.ToLower() == "agent" )
            {
                // 8/4/2005 kb - We previously counted the number of employees
                // with a given role to decide whether or not to hide them
                // at start (see case 2541).  However, it's easier to just
                // check a broker bit.

                if (m_SearchFilter.Text.TrimWhitespaceAndBOM() == "" && brokerDB.HasManyUsers == true)
                {
                    // Too many employees to show.  Go to the help box.

                    m_copyToOfficialContact.Enabled = false;
                    m_TooMany.Visible = true;

                    m_Grid.Visible  = false;
                    m_Empty.Visible = false;

                    return;
                }
            }

            Guid loanID = RequestHelper.GetGuid("loanid", Guid.Empty);
            CEmployeeData dataLoan = null;

            try
            {
                E_EmployeeStatusFilterT status = E_EmployeeStatusFilterT.All;
                if (m_activeRB.Checked) 
                {
                    status = E_EmployeeStatusFilterT.ActiveOnly;
                } 
                else if (m_inactiveRB.Checked) 
                {
                    status = E_EmployeeStatusFilterT.InactiveOnly;
                }

                if (BrokerUser.HasPermission(Permission.AllowLoanAssignmentsToAnyBranch) == false)
                {
                    E_BranchChannelT? branchChannelFilter = null;
                    E_sCorrespondentProcessT? correspondentProcessFilter = null;

                    // PML users have a broker branch, mini-correspondent branch,
                    // and correspondent branch. We need to filter based on the 
                    // correct branch, which requires looking at the channel and
                    // correspondent process type of the file.
                    if (loanID != Guid.Empty && isPmlRole && !isSupervisor)
                    {
                        // Bypass security to avoid any conflicts with workflow.
                        // We need to filter based on the correct branch and corr.
                        // process type regardless of user access.
                        dataLoan = new CEmployeeData(loanID);
                        dataLoan.InitLoad();

                        branchChannelFilter = dataLoan.sBranchChannelT;
                        correspondentProcessFilter = dataLoan.sCorrespondentProcessT;
                    }

                    roleTable.Retrieve(
                        BrokerUser.BrokerId,
                        BrokerUser.BranchId,
                        SearchFilter,
                        Role,
                        status,
                        branchChannelFilter,
                        correspondentProcessFilter);
                }
                else
                {
                    roleTable.Retrieve(
                        BrokerUser.BrokerId,
                        Guid.Empty,
                        SearchFilter,
                        Role,
                        status,
                        null,
                        null);
                }

                if (loanID != Guid.Empty && 
                    (Role == ConstApp.ROLE_BROKERPROCESSOR || Role == ConstApp.ROLE_LOAN_OFFICER ||
                     Role == ConstApp.ROLE_EXTERNAL_SECONDARY || Role == ConstApp.ROLE_EXTERNAL_POST_CLOSER))
                {
                    LoanAccessInfo info = new LoanAccessInfo(RequestHelper.LoanID);

                    bool hasLoanOfficerAssigned = info.Assignments.HasAssignment(E_RoleT.LoanOfficer);
                    bool hasBrokerProcessorAssigned = info.Assignments.HasAssignment(E_RoleT.Pml_BrokerProcessor);
                    bool hasExternalSecondaryAssigned = info.Assignments.HasAssignment(E_RoleT.Pml_Secondary);
                    bool hasExternalPostCloserAssigned = info.Assignments.HasAssignment(E_RoleT.Pml_PostCloser);

                    bool isOriginatingCompanyConstrained =
                        (Role != ConstApp.ROLE_LOAN_OFFICER && hasLoanOfficerAssigned) ||
                        (Role != ConstApp.ROLE_BROKERPROCESSOR && hasBrokerProcessorAssigned) ||
                        (Role != ConstApp.ROLE_EXTERNAL_SECONDARY && hasExternalSecondaryAssigned) ||
                        (Role != ConstApp.ROLE_EXTERNAL_POST_CLOSER && hasExternalPostCloserAssigned);

                    var limitingRoles = new List<string>();

                    if (Role != ConstApp.ROLE_LOAN_OFFICER && hasLoanOfficerAssigned)
                    {
                        limitingRoles.Add("Loan Officer");
                    }

                    if (Role != ConstApp.ROLE_BROKERPROCESSOR && hasBrokerProcessorAssigned)
                    {
                        limitingRoles.Add("Processor (External)");
                    }

                    if (Role != ConstApp.ROLE_EXTERNAL_SECONDARY && hasExternalSecondaryAssigned)
                    {
                        limitingRoles.Add("Secondary (External)");
                    }

                    if (Role != ConstApp.ROLE_EXTERNAL_POST_CLOSER && hasExternalPostCloserAssigned)
                    {
                        limitingRoles.Add("Post-Closer (External)");
                    }

                    var limitingRolesDesc = "";

                    if (limitingRoles.Count == 1)
                    {
                        limitingRolesDesc = limitingRoles.First();
                    }
                    else if (limitingRoles.Count == 2)
                    {
                        limitingRolesDesc = string.Format(
                            "{0} and {1}",
                            limitingRoles[0],
                            limitingRoles[1]);
                    }
                    else if (limitingRoles.Count > 2)
                    {
                        limitingRolesDesc = string.Format(
                            "{0}, and {1}",
                            string.Join(", ", limitingRoles.Take(limitingRoles.Count - 1).ToArray()),
                            limitingRoles.Last());
                    }

                    string roleToChooseDesc = null;

                    switch (Role)
                    {
                        case ConstApp.ROLE_LOAN_OFFICER:
                            roleToChooseDesc = "loan officers";
                            break;
                        case ConstApp.ROLE_BROKERPROCESSOR:
                            roleToChooseDesc = "external processors";
                            break;
                        case ConstApp.ROLE_EXTERNAL_SECONDARY:
                            roleToChooseDesc = "external secondaries";
                            break;
                        case ConstApp.ROLE_EXTERNAL_POST_CLOSER:
                            roleToChooseDesc = "external post-closers";
                            break;
                    }

                    if (isOriginatingCompanyConstrained &&
                        !this.IsOnlyDisplayLqbUsers)
                    {
                        OriginationCompanyLimitingRoleDesc.Text = limitingRolesDesc;
                        OrigRoleDesc.Text = roleToChooseDesc;
                        OriginatingCompanyLimitation.Visible = true;
                        m_pmlBrokerIdFilter = info.sPmlBrokerId;
                    }
                }
            }
            catch( Exception e )
            {
                // Oops!

                Tools.LogError( ErrorMessage = "Failed to load employees in role." , e );
            }

            m_Grid.Columns[2].Visible = BrokerUser.HasPermission(Permission.AllowLoanAssignmentsToAnyBranch);

            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false
                || (Role.ToLower() != ConstApp.ROLE_LOAN_OFFICER.ToLower() && Role.ToLower() != ConstApp.ROLE_BROKERPROCESSOR.ToLower() &&
                    Role.ToLower() != ConstApp.ROLE_EXTERNAL_SECONDARY.ToLower() && Role.ToLower() != ConstApp.ROLE_EXTERNAL_POST_CLOSER.ToLower())
                || ShowMode.ToLower() == "simple"
                || this.IsOnlyDisplayLqbUsers)
            {
                // 11/1/2004 kb - We only show the external broker name
                // when the broker is a lender user with price my loan
                // enabled.  We expect (especially with loan officers)
                // long lists that underwriters and lender account execs
                // must sift to find the desired outside user.
                //
                // 8/2/2005 kb - We now only show the external broker
                // column with agents in a non-simple mode.

                m_Grid.Columns[ 3 ].Visible = false;
            }
            else
            {
                // 11/3/2004 kb - Per tn's request, we suspect that lenders
                // will have many 'P' users in their list when they show
                // the full user list display.

                m_Grid.Columns[ 0 ].HeaderText = "User";
            }

            if (this.IsOnlyDisplayPmlUsers ||
                this.Role == ConstApp.ROLE_EXTERNAL_SECONDARY ||
                this.Role == ConstApp.ROLE_EXTERNAL_POST_CLOSER ||
                this.Role == ConstApp.ROLE_BROKERPROCESSOR)
            {
                m_Grid.Columns[2].Visible = false;
            }

            if (roleTable[Role] != null)
            {
                // Get the corresponding AgentRoleT of the corresponding E_Role.  If the user does not have a contact associated with that agent role, check the
                // copy checkbox.
                E_AgentRoleT role = LendersOffice.Security.Role.GetAgentRoleT(LendersOffice.Security.Role.Get(Role).RoleT);
                if (dataLoan == null && !loanID.Equals(Guid.Empty))
                {
                    dataLoan = new CEmployeeData(loanID);
                    dataLoan.InitLoad();
                }
                if (dataLoan != null)
                {
                    CAgentFields agentFields = dataLoan.GetAgentOfRole(role, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    if (!agentFields.IsValid)
                    {
                        m_copyToOfficialContact.Checked = true;
                    }
                }

                // 7/17/2007 av If the user is looking for a loan officer and he/she is from 
                // a pml enabled broker then the pml column is renamed. OPM 10443
                if (((Role.ToLower() == ConstApp.ROLE_LOAN_OFFICER.ToLower() && !this.IsOnlyDisplayLqbUsers)
                    || Role.ToLower() == ConstApp.ROLE_BROKERPROCESSOR.ToLower()
                    || Role.ToLower() == ConstApp.ROLE_EXTERNAL_SECONDARY.ToLower()
                    || Role.ToLower() == ConstApp.ROLE_EXTERNAL_POST_CLOSER.ToLower())
                    && BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan)) 
                {
                    m_Grid.Columns[ 3 ].HeaderText = "Originating Company (PML only)";
                } 
                else
                {
                    //hides the column if the broker is not  pml enabled or the user is 
                    //filling is not looking for a loan officer.
                    m_Grid.Columns[ 1 ].Visible = false;
                }

                List<BrokerLoanAssignmentTable.Spec> ls = new List<BrokerLoanAssignmentTable.Spec>();

                if (m_pmlBrokerIdFilter != Guid.Empty)
                {
                    foreach (BrokerLoanAssignmentTable.Spec spec in roleTable[Role])
                    {
                        if (spec.PmlBrokerId == m_pmlBrokerIdFilter)
                        {
                            ls.Add(spec);
                        }
                    }

                    m_Grid.DataSource = ViewGenerator.Generate(ls);
                }
                else
                {
                    if (this.IsOnlyDisplayPmlUsers)
                    {
                        var pmlUsers = roleTable[Role].Where(s => s.PmlBrokerId != Guid.Empty);
                        m_Grid.DataSource = ViewGenerator.GenerateGeneric(pmlUsers);
                    }
                    else if (this.IsOnlyDisplayLqbUsers)
                    {
                        var lqbUsers = roleTable[Role].Where(s => s.PmlBrokerId == Guid.Empty);
                        m_Grid.DataSource = ViewGenerator.GenerateGeneric(lqbUsers);
                    }
                    else
                    {
                        m_Grid.DataSource = ViewGenerator.GenerateGeneric(roleTable[Role]);
                    }
                }
                
                m_Grid.DataBind();
            }

            if (Role.ToLower() == "supervisor" || Role.ToLower() == "originating company")
            {
                if (!Page.IsPostBack && m_pmlBrokerIdFilter != Guid.Empty)
                {
                    m_CompanyChoice.Data = m_pmlBrokerIdFilter.ToString();
                    SqlParameter[] param = {new SqlParameter("@BrokerId", BrokerUser.BrokerId) ,new SqlParameter("@PmlBrokerId ", m_pmlBrokerIdFilter)};
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "RetrievePmlBrokerById", param))
                    {
                        reader.Read();
                        m_CompanyChoice.Text = reader["Name"].ToString();
                    }
                }

                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@BrokerId", BrokerUser.BrokerId));

                if (Role.ToLower() == "supervisor")
                {
                    if (m_activeRB.Checked)
                    {
                        parameters.Add(new SqlParameter("@IsActive", true));
                    }
                    else if (m_inactiveRB.Checked)
                    {
                        parameters.Add(new SqlParameter("@IsActive", false));
                    }

                    if (SearchFilter != null)
                    {
                        String[] searchParts = SearchFilter.TrimWhitespaceAndBOM().Split(' ');
                        parameters.Add(new SqlParameter("@NameFilter", searchParts[0]));
                        if (searchParts.Length > 1 && searchParts[1].TrimWhitespaceAndBOM() != "")
                            parameters.Add(new SqlParameter("@LastFilter", searchParts[1]));
                    }

                    if(m_CompanyChoice.Data != "Any")
                    {
                        parameters.Add(new SqlParameter("@PmlBrokerId", m_CompanyChoice.Data));
                    }

                    if (!string.IsNullOrWhiteSpace(this.ExSupervisorEmployeeIdsToIgnore))
                    {
                        parameters.Add(new SqlParameter("@ExSupervisorEmployeeIdsToIgnore", this.GenerateExSupervisorXml()));
                    }

                    DataSet ds = new DataSet();
                    DataSetHelper.Fill(ds, BrokerUser.BrokerId, "ListPMLManagerByBrokerIdandCompany", parameters);
                    m_SupervisorGrid.Columns[1].Visible = false;
                    m_SupervisorGrid.DataSource = ds.Tables[0].DefaultView;
                    m_SupervisorGrid.DataBind();
                }
                else if (Role.ToLower() == "originating company")
                {
                    parameters.Add(new SqlParameter("@PmlBrokerSearch", $"%{this.SearchFilter ?? string.Empty}%"));
                    DataSet ds = new DataSet();
                    DataSetHelper.Fill(ds, BrokerUser.BrokerId, "RetrievePmlBrokerByNameSearch", parameters);
                    m_CompanyGrid.DataSource = ds.Tables[0].DefaultView;
                    m_CompanyGrid.DataBind();

                    this.StatusRow.Visible = false;
                }
            }

            if (m_Grid.Items.Count == 0 && m_SupervisorGrid.Items.Count == 0 && m_CompanyGrid.Items.Count == 0 )
            {
                m_copyToOfficialContact.Enabled = false;
                m_TooMany.Visible = false;
                m_Empty.Visible   = true;
            }
            else
            {
                m_copyToOfficialContact.Enabled = true;
                m_TooMany.Visible = false;
                m_Empty.Visible   = false;
            }

            // OPM 51224 - When changing the assignment of a loan officer, check the "Copy user info to Official Contact List and appropriate forms" by default.
            if (m_sRoleDesc == ConstApp.ROLE_LOAN_OFFICER && m_copyToOfficialContact.Enabled)
            {
                m_copyToOfficialContact.Checked = true;
            }

            // OPM 150546 - If the lender so desires, set this by default for everyone
            if (brokerDB.CopyInternalUsersToOfficalContactsByDefault)
            {
                m_copyToOfficialContact.Checked = true;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            RegisterJsScript("json.js");
            EnableJqueryMigrate = false;
            base.OnInit(e);
        }
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {    
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion

        /// <summary>
        /// Returns "PML" if the user being represented by oRow is a PML user. 
        /// PML users need to have something nonempty in CompanyName so this method
        /// checks the length of the CompanyName column to decide whether the given row 
        /// represents a PML user. 
        /// </summary>
        /// <param name="oRow">Row</param>
        /// <returns>"PML" or "" depending on whether the use ise a PML user or not. </returns>
        protected string GetPmlLabel( object oRow ) 
        {
            DataRowView dRow = oRow as DataRowView; 
            //if the actual broker name is not length 0 then the user is a PML user 
            return dRow != null && dRow[ "CompanyName"].ToString().Length != 0 ? "PML" : "";
        }

        protected string GetBranchNm(object obj)
        {
            var row = obj as DataRowView;

            if (row == null)
            {
                return "";
            }

            if (row["CompanyName"].ToString().Length != 0)
            {
                return "";
            }
            else
            {
                return row["BranchNm"].ToString();
            }
        }

        protected void EmployeeAssignmentDataBound(object source, DataGridItemEventArgs e)
        {
            if (e.Item.DataItem == null)
            {
                return;
            }

            var row = (DataRowView)e.Item.DataItem;
            var linkButton = (LinkButton)e.Item.FindControl("Linkbutton1");
            Guid pmlBrokerId = row["PmlBrokerId"] != DBNull.Value ? (Guid)row["PmlBrokerId"] : Guid.Empty;
            linkButton.CommandName = "Assign";
            linkButton.CommandArgument = string.Format(
                "{0}:{1}:{2}:{3}:{4}",
                row["EmployeeId"],
                row["FullName"],
                row["Email"],
                GetPmlLabel(row),
                pmlBrokerId);
        }

        /// <summary>
        /// Handle UI click.
        /// </summary>

        protected void EmployeeAssignmentClick( object source , System.Web.UI.WebControls.DataGridCommandEventArgs a )
        {
            // Assign clicked employee to the given role.  If any
            // error occurs, just popup a simple message and skip.

            try
            {
                String[] sArgs = a.CommandArgument.ToString().Split( ':' );

                if( a.CommandName.ToLower() == "assign" )
                {
                    if( sArgs.Length < 4 )
                    {
                        throw new CBaseException( ErrorMessages.Generic, "Invalid employee argument format -- not enough items." );
                    }

                    if( ShowMode.ToLower() == "search" )
                    {
                        string argsToWriter = "";
                        if (sArgs.Length == 5)
                        {
                            argsToWriter = "PmlBrokerId='" + Utilities.SafeJsString(sArgs[4]) + "'|";
                        }
                        argsToWriter += "Pml='" + Utilities.SafeHtmlString(sArgs[3]) + "'|Email='" + Utilities.SafeJsString(sArgs[2]) + "'|EmployeeName='" + Utilities.SafeJsString(sArgs[1]) + "'|EmployeeId='" + Utilities.SafeJsString(sArgs[0]) + "'|OK=true";
                        ArgsToWrite = argsToWriter;
                        CommandToDo = "Close";
                    }
                    else
                    {
                        AssignEmployee( new Guid( sArgs[ 0 ] ) , sArgs[ 2 ] , sArgs[ 1 ], sArgs[3] == "PML" );
                    }
                }
            }
            catch (LoanFieldWritePermissionDenied ex)
            {
                ErrorMessage = ex.UserMessage;
            }
            catch( PageDataAccessDenied )
            {
                ErrorMessage = "Access denied. You do not have permission to assign employee to role.";
            }
        }

        /// <summary>
        /// Handle UI click.
        /// </summary>
        
        protected void ClearAssignmentClick( object sender , System.EventArgs a )
        {
            // Assign the no-employee employee to clear the role.

            try
            {
                AssignEmployee(Guid.Empty, "", "", false);
            }
            catch (LoanFieldWritePermissionDenied ex)
            {
                ErrorMessage = ex.UserMessage;
            }
            catch (PageDataAccessDenied)
            {
                ErrorMessage = "Access denied. You do not have permission to clear role.";
            }
            catch
            {
                ErrorMessage = "Failed to clear role.";
            }
        }

        /// <summary>
        /// Handle UI click.
        /// </summary>

        protected void CompanyAssignmentClick(object source, System.Web.UI.WebControls.DataGridCommandEventArgs a)
        {
            try
            {
                String[] sArgs = a.CommandArgument.ToString().Split(':');

                if (sArgs.Length < 2)
                    throw new CBaseException(ErrorMessages.Generic, "Invalid company argument format -- not enough items.");
                
                ArgsToWrite = "Pml=''|Email=''|EmployeeName='" + Utilities.SafeJsString(sArgs[1]) + "'|EmployeeId='" + Utilities.SafeJsString(sArgs[0]) + "'|OK=true";
                CommandToDo = "Close";
            }
            catch (PageDataAccessDenied)
            {
                ErrorMessage = "Access denied. You do not have permission to select this company.";
            }
        }

        protected bool DisplayCopyToOfficialContact()
        {
            return CopyToContactsEnabled && (m_sRoleDesc == ConstApp.ROLE_LOAN_OFFICER
                   || m_sRoleDesc == ConstApp.ROLE_PROCESSOR
                   || m_sRoleDesc == ConstApp.ROLE_MANAGER
                   || m_sRoleDesc == ConstApp.ROLE_REAL_ESTATE_AGENT
                   || m_sRoleDesc == ConstApp.ROLE_CALL_CENTER_AGENT
                   || m_sRoleDesc == ConstApp.ROLE_LENDER_ACCOUNT_EXEC
                   || m_sRoleDesc == ConstApp.ROLE_UNDERWRITER
                   || m_sRoleDesc == ConstApp.ROLE_LOAN_OPENER
                   || m_sRoleDesc == ConstApp.ROLE_BROKERPROCESSOR
                   || m_sRoleDesc == ConstApp.ROLE_SHIPPER
                   || m_sRoleDesc == ConstApp.ROLE_FUNDER
                   || m_sRoleDesc == ConstApp.ROLE_POSTCLOSER
                   || m_sRoleDesc == ConstApp.ROLE_INSURING
                   || m_sRoleDesc == ConstApp.ROLE_COLLATERALAGENT
                   || m_sRoleDesc == ConstApp.ROLE_DOCDRAWER
                   || m_sRoleDesc == ConstApp.ROLE_CREDITAUDITOR
                   || m_sRoleDesc == ConstApp.ROLE_DISCLOSUREDESK
                   || m_sRoleDesc == ConstApp.ROLE_JUNIORPROCESSOR
                   || m_sRoleDesc == ConstApp.ROLE_JUNIORUNDERWRITER
                   || m_sRoleDesc == ConstApp.ROLE_LEGALAUDITOR
                   || m_sRoleDesc == ConstApp.ROLE_LOANOFFICERASSISTANT
                   || m_sRoleDesc == ConstApp.ROLE_PURCHASER
                   || m_sRoleDesc == ConstApp.ROLE_QCCOMPLIANCE
                   || m_sRoleDesc == ConstApp.ROLE_SECONDARY
                   || m_sRoleDesc == ConstApp.ROLE_SERVICING
                   || m_sRoleDesc == ConstApp.ROLE_EXTERNAL_SECONDARY
                   || m_sRoleDesc == ConstApp.ROLE_EXTERNAL_POST_CLOSER);
        }

        private string GenerateExSupervisorXml()
        {
            var idElements = this.ExSupervisorEmployeeIdsToIgnore
                .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(id => new XElement("id", id));

            return new XElement("root", idElements).ToString(SaveOptions.DisableFormatting);
        }

        private void AddWarningScript(EmployeeDB emp, E_RoleT? roleT, Guid? branchId, bool copyToOfficialAgent)
        {
            string sScript = string.Format(
                "<script type='text/javascript'>warn_assign_lo('{0}', '{1}', '{2}', '{3}', {4}, {5}, {6});</script>",
                RequestHelper.LoanID.ToString(),
                emp.ID.ToString(),
                branchId.HasValue ? branchId.ToString() : emp.BranchID.ToString(),
                copyToOfficialAgent ? "true" : "false",
                AspxTools.JsString(emp.FullName),
                AspxTools.JsString(emp.Email),
                AspxTools.JsString(roleT.HasValue ? roleT : E_RoleT.LoanOfficer));

            Page.ClientScript.RegisterStartupScript(typeof(Page), "WarnLO", sScript);
        }

        /// <summary>
        /// Handle UI click.
        /// </summary>
        private void AssignEmployee( Guid employeeID , string email , string employeeName, bool pmlUser )
        {

            bool bCopyToOfficialAgent = CopyToContactsEnabled && m_copyToOfficialContact.Checked;
            // Calculate loan assignment delta and send out email
            // notifications to every affected user who currently
            // wants to receive emails.
            //
            // 8/16/2004 kb - We now process role change notification
            // in the background.  All we do here is track the delta
            // based on who is being changed.  Note: We use the set
            // id property after it's set to retrieve the previous.
            // This works because the previous remains after setting.

            CPageData      dataLoan = new CEmployeeDataWithAccessControl( RequestHelper.LoanID );

            ArrayList      errorList = new ArrayList();
            Guid              roleId = Guid.Empty;
            Guid              prevId = Guid.Empty;
            Boolean   hasErrorToShow = false;

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAgentFields agent = null;
            
            switch( RequestHelper.GetSafeQueryString( "role" ) )
            {
                case ConstApp.ROLE_LOAN_OFFICER:
                    //Change branch to that of loan officer - OPM 51224
                    if (employeeID != Guid.Empty)
                    {
                        EmployeeDB emp = new EmployeeDB(employeeID, BrokerUser.BrokerId);
                        emp.Retrieve();

                        if (emp.UserType == 'B' && dataLoan.sEmployeeExternalSecondaryId != Guid.Empty)
                        {
                            this.ErrorMessage = "Unable to assign internal loan " +
                                "officer when external secondary is assigned.";
                            return;
                        }

                        if (dataLoan.sBranchId != emp.BranchID && 
                            dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent)
                        {
                            AddWarningScript(emp, null, null, bCopyToOfficialAgent);

                            return;
                        }
                        else
                        {
                            dataLoan.sEmployeeLoanRepId = employeeID;
                            roleId = CEmployeeFields.s_LoanRepRoleId;
                            prevId = dataLoan.sEmployeeLoanRepId;
                            if (bCopyToOfficialAgent)
                            {
                                agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
                                agent.ClearExistingData();
                            }
                            // 10/13/2010 dd - OPM 41485 - When user assign loan officer role recalculate Tpo bit.                            
                            dataLoan.RecalculateTpoValue();
                        }
                    }
                    else
                    {
                        dataLoan.sEmployeeLoanRepId = employeeID;
                        roleId = CEmployeeFields.s_LoanRepRoleId;
                        prevId = dataLoan.sEmployeeLoanRepId;
                        if (bCopyToOfficialAgent)
                        {
                            agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
                            agent.ClearExistingData();
                        }
                        dataLoan.RecalculateTpoValue();
                    }

                    break;
                case ConstApp.ROLE_PROCESSOR:
                    dataLoan.sEmployeeProcessorId = employeeID;
                    roleId = CEmployeeFields.s_ProcessorRoleId;
                    prevId = dataLoan.sEmployeeProcessorId;
                    if (bCopyToOfficialAgent)
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.CreateNew);

                    break;
                case ConstApp.ROLE_MANAGER:
                    dataLoan.sEmployeeManagerId = employeeID;
                    roleId = CEmployeeFields.s_ManagerRoleId;
                    prevId = dataLoan.sEmployeeManagerId;
                    if (bCopyToOfficialAgent)
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Manager, E_ReturnOptionIfNotExist.CreateNew);

                    break;
                case ConstApp.ROLE_REAL_ESTATE_AGENT:
                    dataLoan.sEmployeeRealEstateAgentId = employeeID;
                    roleId = CEmployeeFields.s_RealEstateAgentRoleId;
                    prevId = dataLoan.sEmployeeRealEstateAgentId;
                    if (bCopyToOfficialAgent)
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Realtor, E_ReturnOptionIfNotExist.CreateNew);

                    break;
                case ConstApp.ROLE_CALL_CENTER_AGENT:
                    dataLoan.sEmployeeCallCenterAgentId = employeeID;
                    roleId = CEmployeeFields.s_CallCenterAgentRoleId;
                    prevId = dataLoan.sEmployeeCallCenterAgentId;
                    if (bCopyToOfficialAgent)
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CallCenterAgent, E_ReturnOptionIfNotExist.CreateNew);

                    break;
                case ConstApp.ROLE_LENDER_ACCOUNT_EXEC:
                    dataLoan.sEmployeeLenderAccExecId = employeeID;
                    roleId = CEmployeeFields.s_LenderAccountExecRoleId;
                    prevId = dataLoan.sEmployeeLenderAccExecId;

                    break;
                case ConstApp.ROLE_LOCK_DESK:
                    dataLoan.sEmployeeLockDeskId = employeeID;
                    roleId = CEmployeeFields.s_LockDeskRoleId;
                    prevId = dataLoan.sEmployeeLockDeskId;

                    break;
                case ConstApp.ROLE_UNDERWRITER:
                    dataLoan.sEmployeeUnderwriterId = employeeID;
                    roleId = CEmployeeFields.s_UnderwriterRoleId;
                    prevId = dataLoan.sEmployeeUnderwriterId;
                    if (bCopyToOfficialAgent)
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.CreateNew);

                    break;
                case ConstApp.ROLE_LOAN_OPENER:
                    dataLoan.sEmployeeLoanOpenerId = employeeID;
                    roleId = CEmployeeFields.s_LoanOpenerId;
                    prevId = dataLoan.sEmployeeLoanOpenerId;
                    if (bCopyToOfficialAgent)
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOpener, E_ReturnOptionIfNotExist.CreateNew);

                    break;
                case ConstApp.ROLE_CLOSER:
                    dataLoan.sEmployeeCloserId = employeeID;
                    roleId = CEmployeeFields.s_CloserId;
                    prevId = dataLoan.sEmployeeCloserId;                    
                    break;

                case ConstApp.ROLE_BROKERPROCESSOR:
                    dataLoan.sEmployeeBrokerProcessorId = employeeID;
                    roleId = CEmployeeFields.s_BrokerProcessorId;
                    prevId = dataLoan.sEmployeeBrokerProcessorId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.BrokerProcessor, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_SHIPPER:
                    dataLoan.sEmployeeShipperId = employeeID;
                    roleId = CEmployeeFields.s_ShipperId;
                    prevId = dataLoan.sEmployeeShipperId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Shipper, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_FUNDER:
                    dataLoan.sEmployeeFunderId = employeeID;
                    roleId = CEmployeeFields.s_FunderRoleId;
                    prevId = dataLoan.sEmployeeFunderId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Funder, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_POSTCLOSER:
                    dataLoan.sEmployeePostCloserId = employeeID;
                    roleId = CEmployeeFields.s_PostCloserId;
                    prevId = dataLoan.sEmployeePostCloserId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.PostCloser, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_INSURING:
                    dataLoan.sEmployeeInsuringId = employeeID;
                    roleId = CEmployeeFields.s_InsuringId;
                    prevId = dataLoan.sEmployeeInsuringId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Insuring, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_COLLATERALAGENT:
                    dataLoan.sEmployeeCollateralAgentId = employeeID;
                    roleId = CEmployeeFields.s_CollateralAgentId;
                    prevId = dataLoan.sEmployeeCollateralAgentId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CollateralAgent, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_DOCDRAWER:
                    dataLoan.sEmployeeDocDrawerId = employeeID;
                    roleId = CEmployeeFields.s_DocDrawerId;
                    prevId = dataLoan.sEmployeeDocDrawerId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.DocDrawer, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_CREDITAUDITOR:
                    dataLoan.sEmployeeCreditAuditorId = employeeID;
                    roleId = CEmployeeFields.s_CreditAuditorId;
                    prevId = dataLoan.sEmployeeCreditAuditorId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.CreditAuditor, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_DISCLOSUREDESK: 
                    dataLoan.sEmployeeDisclosureDeskId = employeeID;
                    roleId = CEmployeeFields.s_DisclosureDeskId;
                    prevId = dataLoan.sEmployeeDisclosureDeskId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.DisclosureDesk, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_JUNIORPROCESSOR:
                    dataLoan.sEmployeeJuniorProcessorId = employeeID;
                    roleId = CEmployeeFields.s_JuniorProcessorId;
                    prevId = dataLoan.sEmployeeJuniorProcessorId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.JuniorProcessor, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_JUNIORUNDERWRITER:
                    dataLoan.sEmployeeJuniorUnderwriterId = employeeID;
                    roleId = CEmployeeFields.s_JuniorUnderwriterId;
                    prevId = dataLoan.sEmployeeJuniorUnderwriterId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.JuniorUnderwriter, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_LEGALAUDITOR:
                    dataLoan.sEmployeeLegalAuditorId = employeeID;
                    roleId = CEmployeeFields.s_LegalAuditorId;
                    prevId = dataLoan.sEmployeeLegalAuditorId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LegalAuditor, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_LOANOFFICERASSISTANT:
                    dataLoan.sEmployeeLoanOfficerAssistantId = employeeID;
                    roleId = CEmployeeFields.s_LoanOfficerAssistantId;
                    prevId = dataLoan.sEmployeeLoanOfficerAssistantId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficerAssistant, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_PURCHASER:
                    dataLoan.sEmployeePurchaserId = employeeID;
                    roleId = CEmployeeFields.s_PurchaserId;
                    prevId = dataLoan.sEmployeePurchaserId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Purchaser, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_QCCOMPLIANCE:
                    dataLoan.sEmployeeQCComplianceId = employeeID;
                    roleId = CEmployeeFields.s_QCComplianceId;
                    prevId = dataLoan.sEmployeeQCComplianceId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.QCCompliance, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_SECONDARY:
                    dataLoan.sEmployeeSecondaryId = employeeID;
                    roleId = CEmployeeFields.s_SecondaryId;
                    prevId = dataLoan.sEmployeeSecondaryId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Secondary, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_SERVICING:
                    dataLoan.sEmployeeServicingId = employeeID;
                    roleId = CEmployeeFields.s_ServicingId;
                    prevId = dataLoan.sEmployeeServicingId;
                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.Servicing, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                case ConstApp.ROLE_EXTERNAL_SECONDARY:
                    if (employeeID != Guid.Empty)
                    {
                        if (dataLoan.sEmployeeLoanRepId != Guid.Empty)
                        {
                            var loanAssignmentContactTable = new LoanAssignmentContactTable(BrokerUser.BrokerId, dataLoan.sLId);
                            var loanOfficer = loanAssignmentContactTable.FindByRoleT(E_RoleT.LoanOfficer);
                            if (loanOfficer != null && !loanOfficer.IsPmlUser)
                            {
                                this.ErrorMessage = "Unable to assign external secondary when internal loan officer is assigned.";
                                return;
                            }
                        }

                        EmployeeDB emp = new EmployeeDB(employeeID, BrokerUser.BrokerId);
                        emp.Retrieve();

                        // We want to reassign the branch when there is no LO assigned.
                        if (dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
                        {
                            Guid branchToAssign = Guid.Empty;

                            if (dataLoan.sCorrespondentProcessT == E_sCorrespondentProcessT.MiniCorrespondent)
                            {
                                branchToAssign = emp.MiniCorrespondentBranchID;
                            }
                            else
                            {
                                branchToAssign = emp.CorrespondentBranchID;
                            }

                            if (branchToAssign != Guid.Empty && branchToAssign != dataLoan.sBranchId)
                            {
                                AddWarningScript(
                                    emp,
                                    E_RoleT.Pml_Secondary,
                                    branchToAssign,
                                    bCopyToOfficialAgent);

                                return;
                            }
                        }
                    }

                    dataLoan.sEmployeeExternalSecondaryId = employeeID;
                    roleId = CEmployeeFields.s_ExternalSecondaryId;
                    prevId = dataLoan.sEmployeeExternalSecondaryId;

                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ExternalSecondary, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }

                    dataLoan.RecalculateTpoValue();
                    break;
                case ConstApp.ROLE_EXTERNAL_POST_CLOSER:
                    dataLoan.sEmployeeExternalPostCloserId = employeeID;
                    roleId = CEmployeeFields.s_ExternalPostCloserId;
                    prevId = dataLoan.sEmployeeExternalPostCloserId;

                    if (bCopyToOfficialAgent)
                    {
                        agent = dataLoan.GetAgentOfRole(E_AgentRoleT.ExternalPostCloser, E_ReturnOptionIfNotExist.CreateNew);
                        agent.ClearExistingData();
                    }
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Invalid role:  '" + RequestHelper.GetSafeQueryString( "role" ) + "'.");
            }

            if( hasErrorToShow == false )
            {
                // Save the changes to the loan.  We update the cache table
                // under the hood.

                try
                {

                    if (null != agent && bCopyToOfficialAgent) 
                    {

                        agent.ClearExistingData();
                        // 9/21/2006 dd - Retrieve more information on agent.

                        if (employeeID != Guid.Empty) 
                        {
                            bool bCopyCommissionToAgent = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(employeeID, RequestHelper.LoanID) == E_AgentRoleT.Other;
                            CommonFunctions.CopyEmployeeInfoToAgent(dataLoan.sBrokerId, agent, employeeID, bCopyCommissionToAgent);
                        }

                        agent.Update();
                    }

                    try
                    {
                        dataLoan.Save();
                        //CommonFunctions.FixLoanTasksOnRoleAssignement(dataLoan.sBrokerId, RequestHelper.LoanID, employeeID, roleId);

                        RoleChange.Mark(RequestHelper.LoanID, BrokerUser.BrokerId, dataLoan
                            , prevId
                            , employeeID
                            , roleId
                            , E_RoleChangeT.Update
                            );
                    }
                    catch (LoanFieldWritePermissionDenied)
                    {
                        hasErrorToShow = true;
                        errorList.Add("Cannot update the loan assignment due to workflow.");
                       
                    }
        
                }
                catch( AccessDenied e )
                {
                    errorList.Add
                        ( String.Format( "{0}: Access denied." , dataLoan.sLNm )
                        );

                    hasErrorToShow = true;

                    Tools.LogError( "Employee role change: Failed to save and mark role change.  Access denied.", e );
                }
                catch (CBaseException e)
                {
                    errorList.Add(e.UserMessage);
                    hasErrorToShow = true;
                }
                catch( Exception e )
                {
                    Tools.LogError( "Employee role change: Failed to save and mark role change.", e );
                }

                // 7/22/2005 kb - Hide the grid because we don't want to re-
                // render and pipe out a whole bunch of content when the window
                // is set to close anyway.

                m_Grid.DataSource = new ArrayList();
                m_Grid.DataBind();

                m_Grid.Visible = false;

                // Close the window by leaving a command for the page's init
                // scripting post-load.

                CommandToDo = "Close";
            }

            if( hasErrorToShow == true )
            {
                errorList.Add( String.Format( "Changes to file {0} are disregarded." , dataLoan.sLNm ) );
            }

            // Close the popup list and move on.
            if( errorList.Count == 0 )
            {
                string extraArgs = "";
                if (bCopyToOfficialAgent) 
                {
                    extraArgs = "|bRefresh=true";
                }
                // 7/17/07 av Added PML argument to update the agent screen. OPM 10443
                extraArgs += "|PML='" + (pmlUser ? "PML'" : "'"); 
                ArgsToWrite = "Email='" + Utilities.SafeJsString(email) + "'|EmployeeName='" + Utilities.SafeJsString(employeeName) + "'|EmployeeId='" + employeeID + "'|OK=true" + extraArgs;
            }
            else
            {
                ArgsToWrite = "Email=''|EmployeeName=''|EmployeeId=''|OK=false|PML=''";
                ErrorList = errorList;
            }
        }

        [WebMethod]
        public static string AssignLOAndBranch(Guid loanId, Guid loanofficerId, Guid branchId, bool bCopyToOfficialAgent, string loanOfficerName, string loanOfficerEmail, string roleTStr)
        {
            CPageData dataLoan = new CEmployeeDataWithAccessControl(loanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Setting these roles may affect the assigned originating company.
            // Setting the originating company may perform different behavior
            // based on the branch information. So we need to assign the branch first :(.
            dataLoan.AssignBranch(branchId);

            var agentT = E_AgentRoleT.LoanOfficer;
            var roleT = (E_RoleT)Enum.Parse(typeof(E_RoleT), roleTStr);
            string roleDescription = LendersOffice.Security.Role.Get(roleT).ModifiableDesc;

            if (roleT == E_RoleT.LoanOfficer)
            {
                dataLoan.sEmployeeLoanRepId = loanofficerId;
            }
            else if (roleT == E_RoleT.Pml_Secondary)
            {
                dataLoan.sEmployeeExternalSecondaryId = loanofficerId;
                agentT = E_AgentRoleT.ExternalSecondary;
            }
            else
            {
                return "ERROR: Cannot assign " + roleDescription + " in this manner.";
            }

            if (bCopyToOfficialAgent)
            {
                CAgentFields agent = dataLoan.GetAgentOfRole(agentT, E_ReturnOptionIfNotExist.CreateNew);
                agent.ClearExistingData();

                bool bCopyCommissionToAgent = CommonFunctions.GetEmployeeRoleWithCommissionInLoanFile(loanofficerId, loanId) == E_AgentRoleT.Other;
                CommonFunctions.CopyEmployeeInfoToAgent(dataLoan.sBrokerId, agent, loanofficerId, bCopyCommissionToAgent);
                agent.Update();
            }

            dataLoan.RecalculateTpoValue();

            try
            {
                dataLoan.Save();
                return loanOfficerName + ',' + loanOfficerEmail;
            }
            catch (LoanFieldWritePermissionDenied)
            {
                return "ERROR: " + roleDescription +  " cannot be reassigned due to workflow.";
            }
        }

        /// <summary>
        /// Binds the supervisor data grid with the supervisor's name,
        /// employee ID, branch name, and originating company name.
        /// </summary>
        /// <param name="sender">
        /// The activator of the event.
        /// </param>
        /// <param name="e">
        /// The arguments for the event.
        /// </param>
        protected void m_SupervisorGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var employeeData = (e.Item.DataItem as DataRowView).Row;

            var supervisorLink = e.Item.FindControl("SupervisorLink") as LinkButton;

            // The two semicolons after the user's full name separate the user's
            // email and pml indicator, which are not present in the supervisor grid
            // and are ignored during assignment.
            supervisorLink.CommandArgument = string.Format(
                "{0}:{1}: : ",
                employeeData["EmployeeId"],
                employeeData["UserFullName"]);

            supervisorLink.Text = employeeData["UserFullName"].ToString();

            var branch = e.Item.FindControl("BranchNm") as EncodedLiteral;

            branch.Text = employeeData["BranchNm"].ToString();

            var companyName = e.Item.FindControl("CompanyName") as EncodedLiteral;

            companyName.Text = employeeData["CompanyName"].ToString();
        }

        /// <summary>
        /// Binds the originating company data grid with the company's name,
        /// address, and company ID.
        /// </summary>
        /// <param name="sender">
        /// The activator of the event.
        /// </param>
        /// <param name="e">
        /// The arguments for the event.
        /// </param>
        protected void m_CompanyGrid_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            var pmlBrokerData = (e.Item.DataItem as DataRowView).Row;

            var companyLink = e.Item.FindControl("CompanyLink") as LinkButton;

            companyLink.CommandArgument = string.Format(
                "{0}:{1}",
                pmlBrokerData["PmlBrokerId"],
                pmlBrokerData["Name"]);

            companyLink.Text = pmlBrokerData["Name"].ToString();

            var address = e.Item.FindControl("Addr") as Literal;

            address.Text = string.Format(
                "{0}, {1}, {2} {3}",
                pmlBrokerData["Addr"],
                pmlBrokerData["City"],
                pmlBrokerData["State"],
                pmlBrokerData["Zip"]);

            var companyId = e.Item.FindControl("CompanyId") as Literal;

            companyId.Text = pmlBrokerData["CompanyId"].ToString();
        }
    }

}
