using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LendersOfficeApp.los
{
	/// <summary>
	/// Summary description for FooService.
	/// </summary>
	public partial class FooService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "Hello":
                    string firstName = GetString("firstName");
                    SetResult("Message", "Hello " + firstName );
                    break;

                case "Abs":
                    int number = GetInt("number");
                    SetResult("AbsValue",  Math.Abs(number) );
                    break;
                default:
                    throw new ArgumentException("FooService unknowns the arg = " + methodName);
            }
            
        }
    }
}
