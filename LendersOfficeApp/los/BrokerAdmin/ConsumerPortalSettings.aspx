﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsumerPortalSettings.aspx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.ConsumerPortalSettings" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.CreditReport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Consumer Portal Configuration</title>
</head>
<body MS_POSITIONING="FlowLayout" style="overflow-y: auto; background-color:gainsboro">
    <script type="text/javascript">
        function _init() {
            resize(1000, 650);
            f_onAllowLockPeriodSelectionChanged();
            f_onSubmissionReceiptChanged();
            f_onAutoPullCreditReportChanged();
            f_onAskReferralSourceChanged();
            f_buildHelpURLs();
            f_onAskConsumerForSubjValClick();

            $j('input,select,textarea').change(f_updateDirtyBit);
            f_clearDirty();
        }
        function f_updateDirtyBit() {
            bIsDirty = true;
            $j('#SaveBtn').removeAttr('disabled');
        }
        function f_clearDirty() {
            bIsDirty = false;
            $j('#SaveBtn').attr('disabled', 'disabled');
        }
        function f_saveClick() {
            if(Page_ClientValidate())
            {
                var args = new Object();
                args['Id'] = <%= AspxTools.JsString(PortalId.ToString()) %>;
                args['BrokerId'] = <%= AspxTools.JsString(BrokerId.ToString()) %>;
                args['Name'] = $j('#Name').val();
                var result = gService.main.call('CanSave', args);
                if(result.error)
                {
                    alert('System error; unable to save.');
                    return;
                }
                else if(result.value.CanSave=='False')
                {
                    alert('Portal name must be unique');
                    return;
                }

                args = getAllFormValues();
                args['Id'] = <%= AspxTools.JsString(PortalId.ToString()) %>;
                args['BrokerId'] = <%= AspxTools.JsString(BrokerId.ToString()) %>;

                var SelectedReferralSources = [];
                var options = <%= AspxTools.JsGetElementById(SelectedReferralSources)%>.options;
                for(var i = 0; i<options.length; i++)
                {
                    SelectedReferralSources.push({
                        name : options[i].text,
                        id : options[i].value
                    });
                }
                args['SelectedReferralSources'] = JSON.stringify(SelectedReferralSources);

                var EnabledCustomPmlFields_FullApp = [];
                var $enabledCustomPmlFields_FullApp = $j('.CustomPmlFieldRadioListFullApp input[type=radio][value="Y"]:checked');
                $enabledCustomPmlFields_FullApp.each(function(index, Element) {
                    var keywordNum = $j(this).closest('tr.row').find('input.KeywordNumber').val();
                    EnabledCustomPmlFields_FullApp.push(keywordNum);
                });
                args['EnabledCustomPmlFields_FullApp'] = JSON.stringify(EnabledCustomPmlFields_FullApp);

                var EnabledCustomPmlFields_QuickPricer = [];
                var $enabledCustomPmlFields_QuickPricer = $j('.CustomPmlFieldRadioListQp input[type=radio][value="Y"]:checked');
                $enabledCustomPmlFields_QuickPricer.each(function(index, Element) {
                    var keywordNum = $j(this).closest('tr.row').find('input.KeywordNumber').val();
                    EnabledCustomPmlFields_QuickPricer.push(keywordNum);
                });
                args['EnabledCustomPmlFields_QuickPricer'] = JSON.stringify(EnabledCustomPmlFields_QuickPricer);

                args['LoanStatusConfigJson'] = JSON.stringify(loanStatusConfigTable.getCurrentConfig());

                result = gService.main.call('Save', args);
                if(result.error)
                {
                    alert('System error; unable to save.');
                    return;
                }
                if(<%= AspxTools.JsBool(PortalId == Guid.Empty) %>)
                {
                    window.location = 'ConsumerPortalSettings.aspx?PortalId='+ encodeURIComponent(result.value.PortalId);
                }
                else
                {
                    f_clearDirty();
                    $j("#SaveBtn").css("background-color", "");
                }
            }
            else
            {
		        alert('Please fill out all required fields in order to save.');
            }
        }
        function f_closeClick() {
            if ( bIsDirty )
		    {
			    if( confirm( 'Close without saving?' ) == false )
				    return;
		    }
            onClosePopup();
        }
        function f_validateAvailableLockPeriods(source, args)
        {
            var isValid = false;
		    $j('.AvailableLockPeriod').each(function()
		    {
		        if($j(this).val())
		        {
		            isValid = true;
		            return false;
		        }
		    });
		    args.IsValid = isValid;
		    return;
        }
        function f_addReferralSources() {
            var options = $j('#AvailableReferralSources option:selected');
            options.remove();
            $j('#SelectedReferralSources').append(options);
            $j('.ReferralSources option').prop('selected', false);
            f_sort('SelectedReferralSources');
            f_referralSelection();
        }
        function f_removeReferralSources() {
            var options = $j('#SelectedReferralSources option:selected');
            options.remove();
            $j('#AvailableReferralSources').append(options);
            $j('.ReferralSources option').prop('selected', false);
            f_sort('AvailableReferralSources')
            f_referralSelection();
        }
        function f_referralSelection(src) {
            if(src == <%= AspxTools.JsGetElementById(AvailableReferralSources) %>)
                $j('#SelectedReferralSources option').prop('selected', false);
            else if(src == <%= AspxTools.JsGetElementById(SelectedReferralSources)%>)
                $j('#AvailableReferralSources option').prop('selected', false);
            var addEnabled = $j('#AvailableReferralSources option:selected').length > 0;
            var removeEnabled = $j('#SelectedReferralSources option:selected').length > 0;
            $j('#addReferralSourcesBtnDisabled').css('display', addEnabled?'none':'inline');
            $j('#addReferralSourcesBtn').css('display', addEnabled?'inline':'none');
            $j('#removeReferralSourcesBtnDisabled').css('display', removeEnabled?'none':'inline');
            $j('#removeReferralSourcesBtn').css('display', removeEnabled?'inline':'none');
        }
        function f_sort(selectId)
        {
            var options = $j('#'+selectId+' option');
            options.sort(function(a, b){
                    if(a.innerText > b.innerText)
                        return 1;
                    else if(a.innerText < b.innerText)
                        return -1;
                    return 0;
                });
            $j('#'+selectId).empty().append(options);
        }
        function f_onAskReferralSourceChanged()
        {
            var referralSourcesEnabled = document.getElementById('IsAskForReferralSource_0').checked;
            setDisabledAttr($j('.ReferralSources'), !referralSourcesEnabled);
            f_referralSelection();
        }
        function f_onAllowLockPeriodSelectionChanged() {
            var consumerCanControl = <%=AspxTools.JsGetElementById(IsAllowLockPeriodSelection_Y) %>.checked;

            $j('.AvailableLockPeriod').prop('disabled', !consumerCanControl);
            $j('#LockPeriod').prop('disabled', consumerCanControl);
            $j('#LockPeriodRequired').css('visibility', consumerCanControl?'hidden':'visible');
            $j('#AvailableLockPeriodRequired').css('visibility', consumerCanControl?'visible':'hidden');
            ValidatorEnable(<%=AspxTools.JsGetElementById(LockPeriodValidator) %>, !consumerCanControl);
            ValidatorEnable(<%=AspxTools.JsGetElementById(AvailableLockPeriodValidator) %>, consumerCanControl);
        }
        function f_onAutoPullCreditReportChanged() {
            var autoPullCreditReport = document.getElementById('IsAutomaticPullCredit_0').checked;

            if(!autoPullCreditReport)
            {
                document.getElementById('IsAutomaticRunDU_1').checked = true;
            }
            $j('.CRARequired').css('visibility', autoPullCreditReport?'visible':'hidden');
            $j('.RequiresAutoCredit').prop('disabled', !autoPullCreditReport);
            ValidatorEnable(<%=AspxTools.JsGetElementById(CreditReportAgencyValidator) %>, autoPullCreditReport);
            ValidatorEnable(<%=AspxTools.JsGetElementById(AutomaticCRALoginValidator) %>, autoPullCreditReport);
            ValidatorEnable(<%=AspxTools.JsGetElementById(AutomaticCRAPasswordValidator) %>, autoPullCreditReport);
            f_onSelectedCRAChanged(autoPullCreditReport);
            f_onAutoRunDUChanged();
        }
        function f_onSubmissionReceiptChanged()
        {
            var preApprovEnabled = <%=AspxTools.JsGetElementById(ReceiveOnSubmission_PreApprov) %>.checked;
            var preQualEnabled = <%=AspxTools.JsGetElementById(ReceiveOnSubmission_PreQual) %>.checked;

            setDisabledAttr($j('.PreApprovalLetter'), !preApprovEnabled);
            setDisabledAttr($j('.PreQualificationLetter'), !preQualEnabled);
            $j('#PreApprovLetterRequired').css('display', preApprovEnabled?'inline':'none');
            $j('#PreQualLetterRequired').css('display', preQualEnabled?'inline':'none');
            ValidatorEnable(<%=AspxTools.JsGetElementById(PreApprovalLetterValidator) %>, preApprovEnabled);
            ValidatorEnable(<%=AspxTools.JsGetElementById(PreQualificationLetterValidator) %>, preQualEnabled);
        }
        function f_onAutoRunDUChanged() {
            var autoRunDU = document.getElementById('IsAutomaticRunDU_0').checked;
            $j('.RequiresAutoDU').prop('disabled', !autoRunDU);
            $j('.DURequired').css('visibility', autoRunDU?'visible':'hidden');
            ValidatorEnable(<%=AspxTools.JsGetElementById(AutomaticDULoginValidator) %>, autoRunDU);
            ValidatorEnable(<%=AspxTools.JsGetElementById(AutomaticDUPasswordValidator) %>, autoRunDU);
        }
        function f_onSelectedCRAChanged(autoPullCreditReport)
        {
            var value = <%=AspxTools.JsGetElementById(AutomaticCRAId)%>.value;
            var oProtocol = f_getSelectedCraProtocol();
            var usesAccountId = oProtocol.IsKrollFactualData || oProtocol.IsMismo || oProtocol.IsSharperLending || oProtocol.IsInfoNetwork;

            $j('#AccountIdPanel').css('display', usesAccountId?'inline':'none');
            ValidatorEnable(<%= AspxTools.JsGetElementById(AutomaticCRAAccountIdValidator) %>, usesAccountId && autoPullCreditReport);

		    $j('#AccountIDLabel').text(oProtocol.IsSharperLending ? 'Client ID' : 'Account ID');

            if (oProtocol.IsCredco || oProtocol.IsUniversalCredit)
                $j('#LoginLabel').text('Account Number');
            else if (oProtocol.IsSharperLending)
                $j('#LoginLabel').text('User Name');
            // 5/26/2006 nw - OPM 5099 - If user selects "Kroll Factual Data (052)" or "Info1/LandAmerica (018)",
            // change the "Login" field label to "Account Number" or "Client ID", respectively.
            // We can't use oProtocol because both of these entries use .FannieMae, which is not a unique protocol type.
            else if (value == 'da66612f-d574-408a-9785-6eafbf1a57dd')
		        $j('#LoginLabel').text('Account Number');
	        else if (value == '7c6e1bdd-a963-48aa-8eca-2a4c0b355132')
		        $j('#LoginLabel').text('Client ID');
	        // 6/22/2006 nw - OPM 5099 - If user selects "NMR E-MERGE (046)", change "Login" to "Company ID".
	        else if (value == '55e6cc10-2ae9-4772-88ec-ad2dfeb730bb')
		        $j('#LoginLabel').text('Company ID');
	        // 7/26/2006 nw - OPM 4455 - If user selects "CREDCO (FANNIE MAE)", change "Login" to "Account Number".
	        else if (value == '44bd6469-4330-4891-87c2-9ab82787d6b6')
		        $j('#LoginLabel').text('Account Number');
            else
                $j('#LoginLabel').text('Login');

		    var craDuLoginEnabled = oProtocol.IsFannieMae && $j('#m_hasBrokerDUInfo').val() == 'False';
		    ValidatorEnable(<%=AspxTools.JsGetElementById(AutomaticCRADULoginValidator) %>, craDuLoginEnabled && autoPullCreditReport);
            ValidatorEnable(<%=AspxTools.JsGetElementById(AutomaticCRADUPasswordValidator) %>, craDuLoginEnabled && autoPullCreditReport);
            $j('#CRADULogin').css('display', craDuLoginEnabled?'inline':'none');
		}
	    function f_getSelectedCraProtocol()
        {
            var value = <%=AspxTools.JsGetElementById(AutomaticCRAId)%>.value;
            var protocolType = getProtocolType(value);

            return {
                    IsMcl              : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.Mcl.ToString("D"))%>,
                    IsLandsafe         : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.Landsafe.ToString("D"))%>,
                    IsFiserv           : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.Fiserv.ToString("D"))%>,
                    IsLenderMapping    : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.LenderMappingCRA.ToString("D"))%>,
                    IsKrollFactualData : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.KrollFactualData.ToString("D"))%>,
                    IsCredco           : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.Credco.ToString("D"))%>,
                    IsUniversalCredit  : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.UniversalCredit.ToString("D"))%>,
                    IsMismo21          : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.MISMO_2_1.ToString("D"))%>,
                    IsMismo23          : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.MISMO_2_3.ToString("D"))%>,
                    IsInfoNetwork      : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.InfoNetwork.ToString("D"))%>,
                    IsMismo            : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.MISMO_2_1.ToString("D"))%> || protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.MISMO_2_3.ToString("D"))%>,
                    IsFannieMae        : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.FannieMae.ToString("D"))%>,
                    IsSharperLending   : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.SharperLending.ToString("D"))%>,
                    IsCSC			   : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.CSC.ToString("D"))%>,
                    IsCSD			   : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.CSD.ToString("D"))%>,
                    IsCBC			   : protocolType == <%=AspxTools.HtmlString(CreditReportProtocol.CBC.ToString("D"))%>
                   };
        }
        function getProtocolType(id)
        {
	        if (id == <%=AspxTools.JsString(Guid.Empty.ToString())%>) return -1; <%-- Default value.--%>
	        var ret = creditHash[id];
	        if (ret == null) return <%=AspxTools.HtmlString(CreditReportProtocol.Mcl.ToString("D"))%>;

	        return ret;
        }
        function f_pickLeadManagerClicked() {
            showModal('/los/BrokerAdmin/ConsumerPortalEmployeePicker.aspx?Type=Lead', null, null, null, function(result){
                if(result.FullName && result.EmployeeId)
                {
                    $j('#LeadManagerName').val(result.FullName);
                    $j('#LeadManagerEmployeeId').val(result.EmployeeId);
                    f_updateDirtyBit();
                }
            });
        }
        function f_pickLoanOfficerClicked() {
            showModal('/los/BrokerAdmin/ConsumerPortalEmployeePicker.aspx?Type=Loan', null, null, null, function(result){
                if(result.FullName && result.EmployeeId)
                {
                    $j('#LoanOfficerName').val(result.FullName);
                    $j('#LoanOfficerEmployeeId').val(result.EmployeeId);
                    f_updateDirtyBit();
                }
            });
        }
        function f_pickPreApprovLetterClicked() {
            showModal('/los/BrokerAdmin/ConsumerPortalLetterPicker.aspx', null, null, null, function(result){
                if(result.FormId && result.FormDescription)
                {
                    $j('#PreApprovalLetter').val(result.FormDescription);
                    $j('#PreApprovalLetterId').val(result.FormId);
                    f_updateDirtyBit();
                }
            });
        }
        function f_pickPreQualLetterClicked() {
            showModal('/los/BrokerAdmin/ConsumerPortalLetterPicker.aspx', null, null, null, function(result){
                if(result.FormId && result.FormDescription)
                {
                    $j('#PreQualificationLetter').val(result.FormDescription);
                    $j('#PreQualificationLetterId').val(result.FormId);
                    f_updateDirtyBit();
                }
            });
        }
        function f_buildHelpURLs() {
            var url = $j('#HelpLinkPagesHostURL').val();
            if(!url || /^\s*$/.test(url)) //is null or empty
            {
                $j('.HelpLinkEnabled').each(function(){
                    this.getElementsByTagName('input')[1].checked = true;
                    this.disabled = true;
                });

                setDisabledAttr($j('.HelpLinkEnabled input'), true);
                $j('.HelpPageHost').text('');
            }
            else
            {
                $j('.HelpLinkEnabled').removeAttr('disabled');
                setDisabledAttr($j('.HelpLinkEnabled input'), false);
                $j('.HelpPageHost').text(url);
            }
        }
        function f_formatInt3(textbox)
        {
            if(textbox && textbox.value)
            {
                if(textbox.value != '')
                {
                    var value = parseInt(textbox.value);
                    if(isNaN(value))
                        value = '';
                    else if(value<0)
                        value *= -1;
                    else if(value == 0)
                        value = '';
                    textbox.value = value;
                }
            }
        }

        function f_onAskConsumerForSubjValClick()
        {
            var IsNewPmlUIEnabled = document.getElementById("IsNewPmlUIEnabled").checked;
            var AskConsumerForSubjValueVal = $j('#AskConsumerForSubjValue').find('input:checked').val();
            var SelectsLoanProgramInFullSubmission = $j('#SelectsLoanProgramInFullSubmission'); // This is the containing table
            var YesAlwaysOption = SelectsLoanProgramInFullSubmission.find("input[value='1']");

            if(!IsNewPmlUIEnabled || AskConsumerForSubjValueVal == "0")
            {
                setDisabledAttr(SelectsLoanProgramInFullSubmission, true);
                setDisabledAttr(SelectsLoanProgramInFullSubmission.find("input"), true);

                SelectsLoanProgramInFullSubmission.find("[name='SelectsLoanProgramInFullSubmission']").val(["0"]);
            }
            else
            {
                setDisabledAttr(SelectsLoanProgramInFullSubmission, false);
                setDisabledAttr(SelectsLoanProgramInFullSubmission.find("input"), false);
            }

            if(IsNewPmlUIEnabled && AskConsumerForSubjValueVal == "1")
            {
                setDisabledAttr(YesAlwaysOption, false);
                setDisabledAttr($j("label[for='"+YesAlwaysOption.prop('id')+"']"), false)
            }
            else
            {
                setDisabledAttr(YesAlwaysOption, true);
                setDisabledAttr($j("label[for='"+YesAlwaysOption.prop('id')+"']"), true)
                if(SelectsLoanProgramInFullSubmission.find('input:checked').val() == "1")
                {
                    SelectsLoanProgramInFullSubmission.find("[name='SelectsLoanProgramInFullSubmission']").val(["0"]);
                }
            }
        }
        var loanStatusConfigTable = (function($) {
            var $table, $tbody, $rowTemplate;

            function applyRowClass($row, classToAdd) {
                $row.removeClass('GridItem')
                    .removeClass('GridAlternatingItem')
                    .addClass(classToAdd);
            }

            function stripeTable() {
                var classToAdd, $footerRow;

                $tbody.find('tr').each(function(index) {
                    var $row = $(this);
                    classToAdd = index % 2  === 0 ? 'GridItem' : 'GridAlternatingItem';

                    applyRowClass($row, classToAdd);
                });

                classToAdd = (classToAdd === 'GridItem') ? 'GridAlternatingItem' : 'GridItem';
                $footerRow = $table.find('tfoot tr');
                applyRowClass($footerRow, classToAdd);
            }

            function addRow(dateConfig) {
                var $row, $footerRow, classToAdd;
                $row = $.tmpl($rowTemplate, dateConfig);
                $row.appendTo($tbody);

                if ($row.prev().length > 0) {
                    classToAdd = $row.prev().hasClass('GridItem') ? 'GridAlternatingItem' : 'GridItem';
                } else {
                    classToAdd = 'GridItem';
                }

                applyRowClass($row, classToAdd);

                classToAdd = (classToAdd === 'GridItem') ? 'GridAlternatingItem' : 'GridItem';
                $footerRow = $table.find('tfoot tr');
                applyRowClass($footerRow, classToAdd);
            }

            function deleteRow($row) {
                $row.remove();
                stripeTable();
            }

            function moveRowUp($row) {
                if ($row.prev().length > 0) {
                    $row.insertBefore($row.prev());
                    stripeTable();
                }
            }

            function moveRowDown($row) {
                if ($row.next().length > 0) {
                    $row.insertAfter($row.next());
                    stripeTable();
                }
            }

            function displayDatePicker() {
                showModal("/newlos/Tasks/DateCalculator.aspx?IsConsumerPortalDates=t", null, null, null,
                    function(args){
                        if (args.OK) {
                            addRow({
                                FieldId: args.fieldId,
                                SystemDescription: args.fieldDescription
                            });

                            f_updateDirtyBit();
                        }
                    }, {hideCloseButton:true});
            }

            function getCurrentConfig() {
                var dateConfigs = [];
                $tbody.find('tr').each(function() {
                    var $row, fieldId, systemDesc, customDesc;
                    $row = $(this);
                    fieldId = $row.find('span.fieldId').text();
                    systemDesc = $row.find('span.systemDesc').text();
                    customDesc = $row.find('input.customDesc').val();

                    dateConfigs.push({
                        FieldId: fieldId,
                        SystemDescription: systemDesc,
                        CustomDescription: customDesc
                    });
                });

                return dateConfigs;
            }

            $(document).ready(function() {
                var i, dataSource;

                $table = $('#LoanStatusTable');
                $tbody = $table.find('tbody');
                $rowTemplate = $('#StatusRowTemplate').template();

                dataSource = $.parseJSON($('#LoanStatusConfigJson').val());

                if(dataSource)
                {
                    for (i = 0; i < dataSource.length; i++) {
                        addRow(dataSource[i]);
                    }
                }

                function attachEventHandlers() {
                    $('#AddLoanStatus').click(function() {
                        displayDatePicker();
                        return false
                    });

                    $tbody.on('click', 'a.moveUp', function() {
                        moveRowUp($(this).closest('tr'));
                        return false;
                    });

                    $tbody.on('click', 'a.moveDown', function() {
                        moveRowDown($(this).closest('tr'));
                        return false;
                    });

                    $tbody.on('click', 'a.delete', function() {
                        deleteRow($(this).closest('tr'));
                        return false;
                    });

                    $tbody.on('click', 'a.moveUp, a.moveDown, a.delete', function() {
                        f_updateDirtyBit();
                        return false;
                    });

                    $tbody.on('change', 'input.customDesc', function() {
                        f_updateDirtyBit();
                        return false;
                    });
                }

                attachEventHandlers();
            });

            return {
                getCurrentConfig: getCurrentConfig
            };
        })(jQuery);
    </script>
    <script id="StatusRowTemplate" type="text/x-jquery-tmpl">
        <tr>
            <td>
                <a href="#" class="moveUp">
                    <img src="../../images/up-blue.gif" alt="Move item up." />
                </a>
                <div style="height: 2px;"></div>
                <a href="#" class="moveDown">
                    <img src="../../images/down-blue.gif" alt="Move item down." />
                </a>
            </td>
            <td><span class="fieldId">${FieldId}</span></td>
            <td><span class="systemDesc">${SystemDescription}</span></td>
            <td><input class="customDesc" type="text" value="${CustomDescription}" /></td>
            <td><a href="#" class="delete">delete</a></td>
        </tr>
    </script>
    <style type="text/css">
        .FieldLabel
        {
            text-align:right;
            width:200px;
            vertical-align:top;
        }
        input[disabled][type=text],
        input[disabled][type=password]
        {
            background-color:gainsboro;
        }
        .CustomPmlFieldHeader
        {
            text-align: left;
            text-decoration: underline;
        }
        .PushRight
        {
            margin-left: 20px;
        }
        .Hidden
        {
            display: none;
        }
        .AlignTop
        {
            vertical-align: top;
            padding-top: 3px;
        }
        #LoanStatusTable input[type=text]
        {
            width: 200px;
        }
        #LoanStatusTable thead tr th:first-child
        {
            width: 15px;
        }
        #LoanStatusTable img
        {
            display: block;
            margin-left: auto;
            margin-right: auto;
            height: 11px;
            width: 11px;
            border: none;
        }
        .color-red {
            color:red;
        }
    </style>
    <h4 class="page-header">Consumer Portal Configuration</h4>
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="LoanStatusConfigJson" />
    <table cellspacing="2" cellpadding="5" width="100%" border="0">
        <tr>
            <td class="FieldLabel">
                Consumer Portal Name:
            </td>
            <td >
                <asp:TextBox runat="server" ID="Name" Width="400px"></asp:TextBox>
                <img src="../../images/require_icon.gif" alt="Required field" />
                <asp:RequiredFieldValidator runat="server" ID="NameValidator" ControlToValidate="Name"/>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                   What framed URL will host the borrower login page?
            </td>
            <td >
                <asp:TextBox runat="server" ID="ReferralUrl" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                What URL hosts the CSS file you would like this portal to use?
            </td>
            <td >
                <asp:TextBox runat="server" ID="StyleSheetUrl" Width="400px"></asp:TextBox>
                <br />
                <span style="color:Red">If left blank, the consumer portal will make use of our default style sheet.</span>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Default Loan Template:
            </td>
            <td>
                <asp:DropDownList runat="server" ID="DefaultLoanTemplateId"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                What URL hosts your ARM program disclosures?
            </td>
            <td >
                <asp:TextBox runat="server" ID="ARMProgramDisclosuresURL" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Allow consumers to register new accounts?
            </td>
            <td>
                <asp:RadioButton runat="server" ID="AllowRegisterNewAccounts_Y" GroupName="AllowRegisterNewAccounts" Checked="true" Text="Yes" />
                <asp:RadioButton runat="server" ID="AllowRegisterNewAccounts_N" GroupName="AllowRegisterNewAccounts" Text="No" />
            </td>
        </tr>
        <asp:PlaceHolder runat="server" ID="SettingAskForMemberId">
            <tr>
                <td class="FieldLabel">
                    Ask borrowers for member ID?
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="IsAskForMemberId" RepeatDirection="Horizontal" onclick="f_onAskReferralSourceChanged();">
                        <asp:ListItem Value="Y" Text="Yes"></asp:ListItem>
                        <asp:ListItem Value="N" Text="No" Selected="True"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </asp:PlaceHolder>
        <tr>
            <td class="FieldLabel">
                Ask new consumer to provide referral source?
            </td>
            <td >
                <asp:RadioButtonList runat="server" ID="IsAskForReferralSource" RepeatDirection="Horizontal" onclick="f_onAskReferralSourceChanged();">
                    <asp:ListItem Value="Y" Text="Yes"></asp:ListItem>
                    <asp:ListItem Value="N" Text="No" Selected="True"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td></td>
            <td >
                <table cellpadding="0" cellspacing="0" class="ReferralSources">
                    <tr>
                        <td colspan="2" style="font-weight:bold">
                            Referral sources not displayed in this portal:
                        </td>
                        <td style="width:50px">&nbsp;</td>
                        <td style="font-weight:bold">
                            Referral sources displayed in this portal:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-style:italic" colspan="2">
                            (To select multiple items, hold the "CTRL" key down while selecting)
                        </td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ListBox class="ReferralSources" ID="AvailableReferralSources"  SelectionMode="Multiple" runat="server" Width="200px" Height="100px" onclick="f_referralSelection(this);"></asp:ListBox>
                        </td>
                        <td align="center">
                            <input type="button" id="addReferralSourcesBtn" value="Add >>" onclick="f_addReferralSources();" style="margin:5px" />
                            <input type="button" id="addReferralSourcesBtnDisabled" value="Add >>" style="margin:5px;display:none;" disabled /><br />
                            <input type="button" id="removeReferralSourcesBtn" value="<< Remove" onclick="f_removeReferralSources();" style="margin:5px"/>
                            <input type="button" id="removeReferralSourcesBtnDisabled" value="<< Remove" style="margin:5px;display:none;" disabled/>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:ListBox class="ReferralSources" ID="SelectedReferralSources" SelectionMode="Multiple" runat="server" Width="200px" Height="100px" onclick="f_referralSelection(this);"></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Give consumer control of lock period selection?
            </td>
            <td >
                <asp:RadioButton runat="server" ID="IsAllowLockPeriodSelection_Y" GroupName="IsAllowLockPeriodSelection" Checked="true" onclick="f_onAllowLockPeriodSelectionChanged();" Text="Yes" />
                <asp:RadioButton runat="server" ID="IsAllowLockPeriodSelection_N" GroupName="IsAllowLockPeriodSelection" onclick="f_onAllowLockPeriodSelectionChanged();" Text="No, default lock period to " /><asp:TextBox runat="server" ID="LockPeriod" Width="30px" MaxLength="3" onblur="f_formatInt3(this);"/> days.
                <asp:RequiredFieldValidator runat="server" ID="LockPeriodValidator" ControlToValidate="LockPeriod"/>
                <img id="LockPeriodRequired" src="../../images/require_icon.gif" alt="Required field" />
                <br />
                Available Lock Periods (days):
                <asp:TextBox runat="server" ID="AvailableLockPeriod1" class="AvailableLockPeriod" Width="30px" MaxLength="3" onblur="f_formatInt3(this);"></asp:TextBox>
                <asp:TextBox runat="server" ID="AvailableLockPeriod2" class="AvailableLockPeriod" Width="30px" MaxLength="3" onblur="f_formatInt3(this);"></asp:TextBox>
                <asp:TextBox runat="server" ID="AvailableLockPeriod3" class="AvailableLockPeriod" Width="30px" MaxLength="3" onblur="f_formatInt3(this);"></asp:TextBox>
                <asp:TextBox runat="server" ID="AvailableLockPeriod4" class="AvailableLockPeriod" Width="30px" MaxLength="3" onblur="f_formatInt3(this);"></asp:TextBox>
                <asp:TextBox runat="server" ID="AvailableLockPeriod5" class="AvailableLockPeriod" Width="30px" MaxLength="3" onblur="f_formatInt3(this);"></asp:TextBox>
                <asp:TextBox runat="server" ID="AvailableLockPeriod6" class="AvailableLockPeriod" Width="30px" MaxLength="3" onblur="f_formatInt3(this);"></asp:TextBox>
                <asp:TextBox runat="server" ID="AvailableLockPeriod7" class="AvailableLockPeriod" Width="30px" MaxLength="3" onblur="f_formatInt3(this);"></asp:TextBox>
                <asp:TextBox runat="server" ID="AvailableLockPeriod8" class="AvailableLockPeriod" Width="30px" MaxLength="3" onblur="f_formatInt3(this);"></asp:TextBox>
                <asp:TextBox runat="server" ID="AvailableLockPeriod9" class="AvailableLockPeriod" Width="30px" MaxLength="3" onblur="f_formatInt3(this);"></asp:TextBox>
                <asp:CustomValidator runat="server" ID="AvailableLockPeriodValidator" ClientValidationFunction="f_validateAvailableLockPeriods"/>
                <img id="AvailableLockPeriodRequired" src="../../images/require_icon.gif" alt="Required field" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Enable Short Applications?
            </td>
            <td>
                <asp:RadioButton runat="server" ID="EnableShortApplications_Y" GroupName="EnableShortApplications" Checked="true" Text="Yes" />
                <asp:RadioButton runat="server" ID="EnableShortApplications_N" GroupName="EnableShortApplications" Text="No" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Enable Full Applications?
            </td>
            <td>
                <asp:RadioButton runat="server" ID="EnableFullApplications_Y" GroupName="EnableFullApplications" Checked="true" Text="Yes" />
                <asp:RadioButton runat="server" ID="EnableFullApplications_N" GroupName="EnableFullApplications" Text="No" />
                <br />
                <span class="color-red">Please note that disabling full applications will disable resuming existing full applications.</span>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Lead Manager Assignment for Short Applications:
            </td>
            <td >
                Assign short applications to: <asp:TextBox runat="server" ID="LeadManagerName" Width="150px" ReadOnly></asp:TextBox>
                <img src="../../images/require_icon.gif" alt="Required field" />
                <asp:RequiredFieldValidator runat="server" ID="LeadManagerNameValidator" ControlToValidate="LeadManagerName"/>
                <asp:HiddenField ID="LeadManagerEmployeeId" runat="server" />
                <a href="javascript:void(0)" runat="server" id="PickLeadManagerBtn" onclick="f_pickLeadManagerClicked();">Pick Lead Manager</a>
                <br />
                <span style="color:Red">Please note that any short application submitted via a specific loan officer&#39;s "Apply Now" link will be sent to that loan officer&#39;s e-mail account.</span>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Submitted Short Applications Generate:
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="IsGenerateLeadOnShortAppSubmission">
                    <asp:ListItem Value="N" Text="A lead email only." Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Y" Text="A lead file within LendingQB."></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Loan Officer Assignment for Full 1003 Applications:
            </td>
            <td >
                Assign full 1003 applications to: <asp:TextBox runat="server" ID="LoanOfficerName" Width="150px" ReadOnly></asp:TextBox>
                <img src="../../images/require_icon.gif" alt="Required field" />
                <asp:RequiredFieldValidator runat="server" ID="LoanOfficerNameValidator" ControlToValidate="LoanOfficerName"/>
                <asp:HiddenField runat="server" ID="LoanOfficerEmployeeId" />
                <a href="javascript:void(0)" runat="server" id="PickLoanOfficerBtn" onclick="f_pickLoanOfficerClicked();">Pick Loan Officer</a>
                <asp:RadioButtonList runat="server" ID="IsHideLoanOfficer" CssClass="Hidden">
                    <asp:ListItem Value="Y" Selected="True">Hide loan officer from consumer prior to application submission.</asp:ListItem>
                    <asp:ListItem Value="N">Allow consumer to override loan officer assignment.</asp:ListItem>
                </asp:RadioButtonList>
                <span style="color:Red; display: inline-block;">Please note that any full 1003 application created after the consumer logs in via a specific loan officer&#39;s "Apply Now" link will have their application automatically assigned to that loan officer.</span>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">Ask Consumer for Subj. Property Address as Part of Full Application Submission Process?</td>
            <td>
                <asp:RadioButtonList runat="server"  ID="AskConsumerForSubjProperty"  RepeatDirection="Horizontal">
                    <asp:ListItem Text="Yes, always." Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Yes for refinance & home equity, no for purchase." Value="2"></asp:ListItem>
                    <asp:ListItem Text="No, never." Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Ask Consumer for Subj. Property Value as Part of Full Application Submission Process?
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="AskConsumerForSubjValue" RepeatDirection="Horizontal" onclick="f_onAskConsumerForSubjValClick();">
                    <asp:ListItem Value="1" Text="Yes, always." Selected="True"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Yes for purchase, no for refinance & home equity."></asp:ListItem>
                    <asp:ListItem Value="0" Text="No, never."></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Does Consumer Select a Loan Program as part of the Full Application Submission Process?
            </td>
            <td>
                <asp:CheckBox runat="server" ID="IsNewPmlUIEnabled" style="display: none;" />
                <asp:RadioButtonList runat="server" ID="SelectsLoanProgramInFullSubmission" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Allow Submission With Only RESPA 6 Data For:
            </td>
            <td>
                <input type="checkbox" id="IsAllowRespaOnly_Purchase" runat="server" /> Purchase Loans
                <input type="checkbox" id="IsAllowRespaOnly_Refinance" runat="server" /> Refinance Loans
                <input type="checkbox" id="IsAllowRespaOnly_HomeEquity" runat="server" /> Home Equity Loans
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Submitted Full Applications Are Saved As:
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="SubmissionsShouldBecomeLoans" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Y" Text="Loan" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="N" Text="Lead" ></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Get Rates Page Disclaimer:
            </td>
            <td >
                <asp:TextBox runat="server" ID="GetRatesPageDisclaimer" TextMode="MultiLine" Width="700px" Height="60px">Loan Product availability subject to loan amount. Until you lock your rate, A.P.R. and terms are subject to change, including rates, points, rebates and fees. Rates and APRs may vary depending on loan details, such as points, loan amount, loan-to-value, your credit, property type, and occupancy. ARM rates subject to increase during loan term.

Our fees are subject to change in the event parameters of the loan change from those presented at the time of application and loan closing, such as but not limited to, a change in the loan amount, appraised property value, credit score of applicant or type of property. Additional fees may be required in the event that we do not receive your documentation within 48 hours of any request or in the event that a third party such as an appraiser or title company does not perform their duties in a timely fashion.</asp:TextBox>
            </td>
        </tr>

        <tr>
            <td class="FieldLabel">
                Online Application Privacy Policy:
            </td>
            <td >
                <asp:TextBox runat="server" ID="OnlineApplicationTerms" TextMode="MultiLine" Width="700px" Height="60px">We take our clients&#39; financial privacy very seriously. During the course of processing your application, we accumulate non-public personal financial information from you and from other sources about your income, your assets, and your credit history in order to allow a lender to make an informed decision about granting you credit. We restrict access to nonpublic personal information about you to those employees who need to know that information to provide products or services to you. We maintain physical, electronic, and procedural safeguards that comply with federal regulations to guard your nonpublic personal information.
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Online Application Credit Check Authorization:
            </td>
            <td >
                <asp:TextBox runat="server" ID="OnlineApplicationCreditCheck" TextMode="MultiLine" Width="700px" Height="60px">During your mortgage loan application process, this institution performs a Credit Check that requires us to obtain and confirm information regarding your personal and financial background. This Credit Check includes, but is not limited to, current and past employers, current deposit accounts, current and past consumer credit accounts, and your mortgage and/or rental history.</asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Automatically pull credit report/s after receiving credit check authorization?
            </td>
            <td >
                <asp:RadioButtonList runat="server" ID="IsAutomaticPullCredit" RepeatDirection="Horizontal" onclick="f_onAutoPullCreditReportChanged();">
                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                    <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" rowspan="2">
                Which Credit Reporting Agency Should this Portal Use to Obtain Credit Reports?
            </td>
            <td>
                <asp:DropDownList runat="server" ID="AutomaticCRAId" class="RequiresAutoCredit" onchange="f_onSelectedCRAChanged(true);"></asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="CreditReportAgencyValidator" ControlToValidate="AutomaticCRAId"/>
                <img class="CRARequired" src="../../images/require_icon.gif" alt="Required field" />
            </td>
        </tr>
        <tr>
            <td>
                <span id="AccountIdPanel">
                    <label id="AccountIDLabel">Account ID</label>: <asp:TextBox runat="server" ID="AutomaticCRAAccountId" width="100px" class="RequiresAutoCredit"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="AutomaticCRAAccountIdValidator" ControlToValidate="AutomaticCRAAccountId"/>
                    <img class="CRARequired" src="../../images/require_icon.gif" alt="Required field" />&nbsp;&nbsp;
                </span>
                <label id="LoginLabel">Login</label>: <asp:TextBox runat="server" ID="AutomaticCRALogin" width="100px" class="RequiresAutoCredit"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="AutomaticCRALoginValidator" ControlToValidate="AutomaticCRALogin"/>
                <img class="CRARequired" src="../../images/require_icon.gif" alt="Required field" />
                &nbsp;&nbsp;Password: <asp:TextBox runat="server" ID="AutomaticCRAPassword" TextMode="Password" width="100px" class="RequiresAutoCredit"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="AutomaticCRAPasswordValidator" ControlToValidate="AutomaticCRAPassword"/>
                <img class="CRARequired" src="../../images/require_icon.gif" alt="Required field" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td id="CRADULogin">
                DU Login: <asp:TextBox runat="server" ID="AutomaticCRADULogin" width="100px" class="RequiresAutoCredit"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="AutomaticCRADULoginValidator" ControlToValidate="AutomaticCRADULogin"/>
                <img class="CRARequired" src="../../images/require_icon.gif" alt="Required field" />
                &nbsp;&nbsp;Password: <asp:TextBox runat="server" ID="AutomaticCRADUPassword" TextMode="Password" width="100px" class="RequiresAutoCredit"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="AutomaticCRADUPasswordValidator" ControlToValidate="AutomaticCRADUPassword"/>
                <img class="CRARequired" src="../../images/require_icon.gif" alt="Required field" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Which Custom PML Fields are Used in the Consumer Portal?
            </td>
            <td>
                <table>
                    <thead>
                        <tr>
                            <th class="FieldLabel CustomPmlFieldHeader" style="width: 300px;">Description</th>
                            <th class="FieldLabel CustomPmlFieldHeader" style="width: 200px;">Ask in Full Application?</th>
                            <th class="FieldLabel CustomPmlFieldHeader" style="width: 200px;">Ask in Consumer Quick Pricer?</th>
                            <th class="FieldLabel CustomPmlFieldHeader">Visibility Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater runat="server" ID="CustomPmlFields" OnItemDataBound="CustomPmlFields_OnItemDataBound">
                            <ItemTemplate>
                                <tr class="row">
                                    <td style="width: 300px;">
                                        <ml:EncodedLabel runat="server" ID="FieldDesc" CssClass="FieldLabel"></ml:EncodedLabel>
                                        <input type="hidden" runat="server" ID="KeywordNumber" class="KeywordNumber" />
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:RadioButtonList runat="server" ID="IsEnabledFa" RepeatDirection="Horizontal" class="PushRight CustomPmlFieldRadioListFullApp">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td style="width: 200px;">
                                        <asp:RadioButtonList runat="server" ID="IsEnabledQp" RepeatDirection="Horizontal" class="PushRight CustomPmlFieldRadioListQp">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <ml:EncodedLabel runat="server" ID="VisibilityType"></ml:EncodedLabel>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Eligible Loan Program Disclaimer:
            </td>
            <td >
                <asp:TextBox runat="server" ID="EligibleProgramDisclaimer" TextMode="MultiLine" Width="700px" Height="60px">Loan Product availability subject to loan amount. Until you lock your rate, A.P.R. and terms are subject to change, including rates, points, rebates and fees. Rates and APRs may vary depending on loan details, such as points, loan amount, loan-to-value, your credit, property type, and occupancy. ARM rates subject to increase during loan term.

Our fees are subject to change in the event parameters of the loan change from those presented at the time of application and loan closing, such as but not limited to, a change in the loan amount, appraised property value, credit score of applicant or type of property. Additional fees may be required in the event that we do not receive your documentation within 48 hours of any request or in the event that a third party such as an appraiser or title company does not perform their duties in a timely fashion.</asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                No Eligible Loan Program Message To Consumer:
            </td>
            <td >
                <asp:TextBox runat="server" ID="NoEligibleProgramMessage" TextMode="MultiLine" Width="700px" Height="60px">There are currently no eligible loan programs specific to your scenario available online. Please try expanding your results filter selections to include additional loan terms.</asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" rowspan="2">
                Automatically Run DU When Consumer Submits Full 1003?
            </td>
            <td>
                <asp:RadioButtonList runat="server" ID="IsAutomaticRunDU" RepeatDirection="Horizontal" class="RequiresAutoCredit" onclick="f_onAutoRunDUChanged();" style="display:inline;">
                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                    <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                DU Login: <asp:TextBox runat="server" ID="AutomaticDULogin" width="100px" class="RequiresAutoDU"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="AutomaticDULoginValidator" ControlToValidate="AutomaticDULogin"/>
                <img class="DURequired" src="../../images/require_icon.gif" alt="Required field" />
                &nbsp;&nbsp;Password: <asp:TextBox runat="server" ID="AutomaticDUPassword" TextMode="Password" width="100px" class="RequiresAutoDU"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="AutomaticDUPasswordValidator" ControlToValidate="AutomaticDUPassword"/>
                <img class="DURequired" src="../../images/require_icon.gif" alt="Required field" />
            </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>DU CRA: <asp:DropDownList runat="server" ID="AutomaticDUCraId" class="RequiresAutoDU" /></td>
        </tr>
        <tr>
        <td>&nbsp;</td>
        <td>DU CRA Login: <asp:TextBox runat="server" ID="AutomaticDUCraLogin" class="RequiresAutoDU" /> DU CRA Password: <asp:TextBox runat="server" id="AutomaticDUCraPassword" TextMode="Password" class="RequiresAutoDU" /></td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Upon Full 1003 Submission, Consumer Automatically Receives:
                <asp:HiddenField ID="PreApprovalLetterId" runat="server" />
                <asp:HiddenField ID="PreQualificationLetterId" runat="server" />
            </td>
            <td >
                <asp:RadioButton runat="server" GroupName="ReceiveOnSubmission" ID="ReceiveOnSubmission_PreApprov" Checked="true" Text="A pre-approval letter (contingent upon AUS approval) and an application receipt confirmation message." onclick="f_onSubmissionReceiptChanged();"/><br />
                <label class="PreApprovalLetter">&nbsp;&nbsp;&nbsp;&nbsp;Pre-Approval Letter:&nbsp;</label><asp:TextBox runat="server" ID="PreApprovalLetter" class="PreApprovalLetter" Width="400px" ReadOnly></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" Display="Dynamic" ID="PreApprovalLetterValidator" ControlToValidate="PreApprovalLetter"/>
                <img id="PreApprovLetterRequired" src="../../images/require_icon.gif" alt="Required field" />
                &nbsp;<a href="javascript:void(0)" onclick="if(!hasDisabledAttr(this)) f_pickPreApprovLetterClicked();" class="PreApprovalLetter">select</a>

                <br />
                <asp:RadioButton runat="server" GroupName="ReceiveOnSubmission" ID="ReceiveOnSubmission_PreQual" Text="A pre-qualification letter and an application receipt confirmation message." onclick="f_onSubmissionReceiptChanged();"/><br />
                <label class="PreQualificationLetter">&nbsp;&nbsp;&nbsp;&nbsp;Pre-Qualification Letter:&nbsp;</label><asp:TextBox runat="server" ID="PreQualificationLetter" class="PreQualificationLetter" Width="400px" ReadOnly></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" Display="Dynamic" ID="PreQualificationLetterValidator" ControlToValidate="PreQualificationLetter"/>
                <img id="PreQualLetterRequired" src="../../images/require_icon.gif" alt="Required field" />
                &nbsp;<a href="javascript:void(0)" onclick="if(!hasDisabledAttr(this)) f_pickPreQualLetterClicked();" class="PreQualificationLetter">select</a>

                <br />
                <asp:RadioButton runat="server" GroupName="ReceiveOnSubmission" ID="ReceiveOnSubmission_ConfirmOnly" Text="An application receipt confirmation message only." onclick="f_onSubmissionReceiptChanged();"/>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Short Application Submission Confirmation Message:
            </td>
            <td >
                <asp:TextBox runat="server" ID="ShortApplicationConfirmation" TextMode="MultiLine" Width="700px" Height="60px">Thank you for submitting your short application. One of qualified mortgage professionals will be reaching out to you shortly.</asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Full 1003 Application Submission Confirmation Message:
            </td>
            <td >
                <asp:TextBox runat="server" ID="FullApplicationConfirmation" TextMode="MultiLine" Width="700px" Height="60px">Thank you for submitting your full mortgage application online.  We have received it and one of qualified mortgage professionals will be reaching out to you shortly. We look forward to working with you!</asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Contact Info Message:
            </td>
            <td >
                <asp:TextBox runat="server" ID="ContactInfoMessage" TextMode="MultiLine" Width="700px" Height="60px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                Set Help Link Pages Host URL:
            </td>
            <td >
                <asp:TextBox runat="server" ID="HelpLinkPagesHostURL" Width="400px" onblur="f_buildHelpURLs();"></asp:TextBox>
                <br />
                <span style="color:Red">Please enter the URL which hosts the help link pages you have created to assist the user with each consumer portal page.</span>
            </td>
        </tr>
        <tr>
            <td></td>
            <td >
                <table cellspacing="2" cellpadding="3" border="0">
                    <tr class="GridHeader">
                        <th width="160px">
                            Consumer Portal Page
                        </th>
                        <th width="340px">
                            Help Link Popup URL
                        </th>
                        <th width="100px">
                            Enable Help Link?
                        </th>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            Get Rates
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/getrates.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForGetRatesEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridAlternatingItem">
                        <td>
                            Applicant Login
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/login.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForApplicantLoginEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            Applicant Loan Pipeline
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/pipeline.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForApplicantLoanPipelineEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridAlternatingItem">
                        <td>
                            Personal Information
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/personalinfo.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForPersonalInformationEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            Loan Information
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/loaninfo.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForLoanInformationEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridAlternatingItem">
                        <td>
                            Property Information
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/propertyinfo.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForPropertyInformationEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            Loan Officer
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/loanofficer.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForLoanOfficerEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridAlternatingItem">
                        <td>
                            Assets
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/assetinfo.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForAssetInformationEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            Liabilities
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/liabilities.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForLiabilityInformationEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridAlternatingItem">
                        <td>
                            Expenses
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/expensesinfo.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForExpenseInformationEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            Income
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/incomeinfo.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForIncomeInformationEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridAlternatingItem">
                        <td>
                            Employment
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/employment.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForEmploymentEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            Declarations
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/declarations.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForDeclarationsEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridAlternatingItem">
                        <td>
                            Select Loan
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/selectloan.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForSelectLoanEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridItem">
                        <td>
                            Submit Application
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/submitapp.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForSubmitApplicationEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="GridAlternatingItem">
                        <td>
                            Loan Status and Doc Exchange
                        </td>
                        <td>
                            <span class="HelpPageHost"></span>/loanstatus.html
                        </td>
                        <td>
                            <asp:RadioButtonList ID="IsHelpForLoanStatusAndDocExchangeEnabled" class="HelpLinkEnabled" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N" Selected="True">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="AlignTop FieldLabel">Loan Status/Dates Configuration</td>
            <td>
                <table cellspacing="2" cellpadding="3" border="0" id="LoanStatusTable">
                    <thead>
                        <tr class="GridHeader">
                            <th></th>
                            <th>Field ID</th>
                            <th>LendingQB Field Description</th>
                            <th>Consumer Facing Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><a href="#" id="AddLoanStatus">add</a></td>
                        </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>
    <div style="text-align: center">
        <input type="button" value="Save" id="SaveBtn" onclick="f_saveClick();" />
        <input type="button" value="Close" id="CloseBtn" onclick="f_closeClick();" />
    </div>
    </form>
</body>
</html>
