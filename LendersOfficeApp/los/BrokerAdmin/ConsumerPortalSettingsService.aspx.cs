﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.ObjLib.ConsumerPortal;
using LendersOffice.Common;
using LendersOffice.Admin;
using LendersOfficeApp.los.admin;
using LendersOffice.Security;

namespace LendersOfficeApp.los.BrokerAdmin
{
    public partial class ConsumerPortalSettingsService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingConsumerPortalConfigs
                };
            }
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CanSave":
                    CanSave();
                    break;
                case "Save":
                    Save();
                    break;
            }
        }
        private void CanSave()
        {
            bool canSave;
            SqlParameter[] parameters = {
                                            new SqlParameter("Id", GetGuid("Id"))
                , new SqlParameter("BrokerId", GetGuid("BrokerId"))
                , new SqlParameter("Name", GetString("Name"))
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(GetGuid("BrokerId"), "ConsumerPortal_DuplicateNames", parameters))
            {
                canSave = !reader.Read();
            }
            SetResult("CanSave", canSave.ToString());
        }
        private void Save()
        {
            Guid PortalId = GetGuid("Id");
            Guid BrokerId = GetGuid("BrokerId");
            string Name = GetString("Name");
            string ReferralUrl = GetString("ReferralUrl");
            string StyleSheetUrl = GetString("StyleSheetUrl");

            bool IsAskForReferralSource = GetString("IsAskForReferralSource") == "Y";

            List<ReferralSource> ReferralSources = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<ReferralSource>>(GetString("SelectedReferralSources"));

            bool IsAllowLockPeriodSelection = GetString("IsAllowLockPeriodSelection") == "IsAllowLockPeriodSelection_Y";

            bool enableShortApplications = GetString("EnableShortApplications") == "EnableShortApplications_Y";
            bool enableFullApplications = GetString("EnableFullApplications") == "EnableFullApplications_Y";
            bool allowRegisterNewAccounts = GetString("AllowRegisterNewAccounts") == "AllowRegisterNewAccounts_Y";

            List<int> AvailableLockPeriods = new List<int>();
            if (IsAllowLockPeriodSelection)
            {
                for (int i = 1; i <= 9; i++)
                {
                    try
                    {
                        AvailableLockPeriods.Add(GetInt("AvailableLockPeriod" + i));
                    }
                    catch (GenericUserErrorMessageException)
                    {
                        //Allowed to be empty (non-parseable)
                    }
                }
            }
            else
            {
                AvailableLockPeriods.Add(GetInt("LockPeriod"));
            }

            var EnabledCustomPmlFields_FullApp = new HashSet<int>(ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<int>>(GetString("EnabledCustomPmlFields_FullApp")));
            var EnabledCustomPmlFields_QuickPricer = new HashSet<int>(ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<int>>(GetString("EnabledCustomPmlFields_QuickPricer")));

            EmployeeDetails emp;

            //Transform EmployeeId to UserId
            Guid LeadManagerEmployeeId = GetGuid("LeadManagerEmployeeId", Guid.Empty);
            Guid LeadManagerUserId = Guid.Empty;
            if(LeadManagerEmployeeId != Guid.Empty)
            {
                emp = new EmployeeDetails(BrokerId, LeadManagerEmployeeId);
                LeadManagerUserId = emp.UserId;
            }

            //Transform EmployeeId to UserId
            Guid LoanOfficerEmployeeId = GetGuid("LoanOfficerEmployeeId", Guid.Empty);
            Guid LoanOfficerUserId = Guid.Empty;
            if(LoanOfficerEmployeeId != Guid.Empty)
            {
                emp = new EmployeeDetails(BrokerId, LoanOfficerEmployeeId);
                LoanOfficerUserId = emp.UserId;
            }

            bool IsHideLoanOfficer = GetString("IsHideLoanOfficer") == "Y";
            string GetRatesPageDisclaimer = GetString("GetRatesPageDisclaimer");
            string OnlineApplicationTerms = GetString("OnlineApplicationTerms");
            string OnlineApplicationCreditCheck = GetString("OnlineApplicationCreditCheck");
            bool IsAutomaticPullCredit = GetString("IsAutomaticPullCredit") == "Y";
            Guid AutomaticCRAId = GetGuid("AutomaticCRAId", Guid.Empty);
            string AutomaticCRALogin = GetString("AutomaticCRALogin");
            string AutomaticCRAPassword = GetString("AutomaticCRAPassword");
            if (AutomaticCRAPassword == ConsumerPortalSettings.INVALID_PASSWORD) AutomaticCRAPassword = "";
            string AutomaticCRAAccountId = GetString("AutomaticCRAAccountId");
            string AutomaticCRADULogin = GetString("AutomaticCRADULogin");
            string AutomaticCRADUPassword = GetString("AutomaticCRADUPassword");
            if (AutomaticCRADUPassword == ConsumerPortalSettings.INVALID_PASSWORD) AutomaticCRADUPassword = "";
            string EligibleProgramDisclaimer = GetString("EligibleProgramDisclaimer");
            string NoEligibleProgramMessage = GetString("NoEligibleProgramMessage");
            bool IsAutomaticRunDU = GetString("IsAutomaticRunDU") == "Y";
            string AutomaticDULogin = GetString("AutomaticDULogin");
            string AutomaticDUPassword = GetString("AutomaticDUPassword");
            if (AutomaticDUPassword == ConsumerPortalSettings.INVALID_PASSWORD) AutomaticDUPassword = "";
            Guid PreApprovalLetterId = GetGuid("PreApprovalLetterId", Guid.Empty);
            Guid PreQualificationLetterId = GetGuid("PreQualificationLetterId", Guid.Empty);
            E_AskConsumerForSubjPropertyT AskConsumerForSubjProperty = (E_AskConsumerForSubjPropertyT)GetInt("AskConsumerForSubjProperty");
            E_AskConsumerForSubjValue AskConsumerForSubjValue = (E_AskConsumerForSubjValue)GetInt("AskConsumerForSubjValue");

            E_F1003SubmissionReceiptT F1003SubmissionReceiptT;
            switch (GetString("ReceiveOnSubmission"))
            {
                case "ReceiveOnSubmission_PreApprov":
                    F1003SubmissionReceiptT = E_F1003SubmissionReceiptT.PreApprovalLetter;
                    break;
                case "ReceiveOnSubmission_PreQual":
                    F1003SubmissionReceiptT = E_F1003SubmissionReceiptT.PreQualificationLetter;
                    break;
                default:
                    F1003SubmissionReceiptT = E_F1003SubmissionReceiptT.ConfirmationOnly;
                    break;
            }

            string ShortApplicationConfirmation = GetString("ShortApplicationConfirmation");
            string FullApplicationConfirmation = GetString("FullApplicationConfirmation");
            string ContactInfoMessage = GetString("ContactInfoMessage");
            string HelpLinkPagesHostURL = GetString("HelpLinkPagesHostURL");
            bool IsHelpForGetRatesEnabled = GetString("IsHelpForGetRatesEnabled") == "Y";
            bool IsHelpForApplicantLoginEnabled = GetString("IsHelpForApplicantLoginEnabled") == "Y";
            bool IsHelpForApplicantLoanPipelineEnabled = GetString("IsHelpForApplicantLoanPipelineEnabled") == "Y";
            bool IsHelpForPersonalInformationEnabled = GetString("IsHelpForPersonalInformationEnabled") == "Y";
            bool IsHelpForLoanInformationEnabled = GetString("IsHelpForLoanInformationEnabled") == "Y";
            bool IsHelpForPropertyInformationEnabled = GetString("IsHelpForPropertyInformationEnabled") == "Y";
            bool IsHelpForLoanOfficerEnabled = GetString("IsHelpForLoanOfficerEnabled") == "Y";
            bool IsHelpForAssetInformationEnabled = GetString("IsHelpForAssetInformationEnabled") == "Y";
            bool IsHelpForLiabilityInformationEnabled = GetString("IsHelpForLiabilityInformationEnabled") == "Y";
            bool IsHelpForExpenseInformationEnabled = GetString("IsHelpForExpenseInformationEnabled") == "Y";
            bool IsHelpForIncomeInformationEnabled = GetString("IsHelpForIncomeInformationEnabled") == "Y";
            bool IsHelpForDeclarationsEnabled = GetString("IsHelpForDeclarationsEnabled") == "Y";
            bool IsHelpForSelectLoanEnabled = GetString("IsHelpForSelectLoanEnabled") == "Y";
            bool IsHelpForSubmitApplicationEnabled = GetString("IsHelpForSubmitApplicationEnabled") == "Y";
            bool IsHelpForLoanStatusAndDocExchangeEnabled = GetString("IsHelpForLoanStatusAndDocExchangeEnabled") == "Y";
            bool IsHelpForEmploymentEnabled = GetString("IsHelpForEmploymentEnabled") == "Y";
            bool IsAskForMemberId = GetString("IsAskForMemberId", "N") == "Y";

            string automaticDUCraId = GetString("AutomaticDUCraId");
            string automaticDUCraLogin = GetString("AutomaticDUCraLogin");

            string automaticDUCraPassword = GetString("AutomaticDUCraPassword");
            if (automaticDUCraPassword == ConsumerPortalSettings.INVALID_PASSWORD)
            {
                automaticDUCraPassword = "";
            }

            string armProgramDisclosuresURL = GetString("ARMProgramDisclosuresURL");
            E_SelectsLoanProgramInFullSubmission selectsLoanProgramInFullSubmission = (E_SelectsLoanProgramInFullSubmission)GetInt("SelectsLoanProgramInFullSubmission");
            Guid DefaultLoanTemplateId = GetGuid("DefaultLoanTemplateId", Guid.Empty);
            bool submissionsShouldBecomeLoans = GetString("SubmissionsShouldBecomeLoans", "Y") == "Y";
            bool isGenerateLeadOnShortAppSubmission = GetString("IsGenerateLeadOnShortAppSubmission") == "Y";

            bool isAllowRespaOnly_Purchase = GetBool("IsAllowRespaOnly_Purchase");
            bool isAllowRespaOnly_Refinance = GetBool("IsAllowRespaOnly_Refinance");
            bool isAllowRespaOnly_HomeEquity = GetBool("IsAllowRespaOnly_HomeEquity");

            string loanStatusConfigJson = GetString("LoanStatusConfigJson");

            ConsumerPortalConfig config;
            if (PortalId == Guid.Empty)
            {
                config = new ConsumerPortalConfig(BrokerId, Name, true, ReferralUrl, StyleSheetUrl,
                    IsAskForReferralSource, ReferralSources, IsAllowLockPeriodSelection,
                    AvailableLockPeriods, LeadManagerUserId, LoanOfficerUserId, IsHideLoanOfficer,
                    GetRatesPageDisclaimer, OnlineApplicationTerms,
                    OnlineApplicationCreditCheck, IsAutomaticPullCredit, AutomaticCRAId,
                    AutomaticCRALogin, AutomaticCRAPassword, AutomaticCRAAccountId,
                    AutomaticCRADULogin, AutomaticCRADUPassword, EligibleProgramDisclaimer,
                    NoEligibleProgramMessage, IsAutomaticRunDU, AutomaticDULogin, AutomaticDUPassword,
                    F1003SubmissionReceiptT, PreApprovalLetterId, PreQualificationLetterId,
                    ShortApplicationConfirmation, FullApplicationConfirmation,
                    HelpLinkPagesHostURL, IsHelpForGetRatesEnabled, IsHelpForApplicantLoginEnabled,
                    IsHelpForApplicantLoanPipelineEnabled, IsHelpForPersonalInformationEnabled,
                    IsHelpForLoanInformationEnabled, IsHelpForPropertyInformationEnabled,
                    IsHelpForLoanOfficerEnabled, IsHelpForAssetInformationEnabled,
                    IsHelpForLiabilityInformationEnabled, IsHelpForExpenseInformationEnabled,
                    IsHelpForIncomeInformationEnabled, IsHelpForDeclarationsEnabled,
                    IsHelpForSelectLoanEnabled, IsHelpForSubmitApplicationEnabled,
                    IsHelpForLoanStatusAndDocExchangeEnabled, IsHelpForEmploymentEnabled,
                    automaticDUCraId, automaticDUCraLogin, automaticDUCraPassword, IsAskForMemberId,
                    EnabledCustomPmlFields_FullApp, EnabledCustomPmlFields_QuickPricer, armProgramDisclosuresURL, selectsLoanProgramInFullSubmission,
                    DefaultLoanTemplateId, submissionsShouldBecomeLoans, isGenerateLeadOnShortAppSubmission,
                    AskConsumerForSubjProperty, AskConsumerForSubjValue, loanStatusConfigJson, isAllowRespaOnly_Purchase,
                    isAllowRespaOnly_Refinance, isAllowRespaOnly_HomeEquity,
                    enableShortApplications, enableFullApplications, allowRegisterNewAccounts, ContactInfoMessage);
            }
            else
            {
                config = new ConsumerPortalConfig(PortalId, BrokerId, Name, true, ReferralUrl, StyleSheetUrl,
                    IsAskForReferralSource, ReferralSources,
                    IsAllowLockPeriodSelection, AvailableLockPeriods, LeadManagerUserId,
                    LoanOfficerUserId, IsHideLoanOfficer, GetRatesPageDisclaimer,
                    OnlineApplicationTerms, OnlineApplicationCreditCheck, IsAutomaticPullCredit,
                    AutomaticCRAId, AutomaticCRALogin, AutomaticCRAPassword, AutomaticCRAAccountId,
                    AutomaticCRADULogin, AutomaticCRADUPassword, EligibleProgramDisclaimer,
                    NoEligibleProgramMessage, IsAutomaticRunDU, AutomaticDULogin, AutomaticDUPassword,
                    F1003SubmissionReceiptT, PreApprovalLetterId, PreQualificationLetterId,
                    ShortApplicationConfirmation, FullApplicationConfirmation, HelpLinkPagesHostURL,
                    IsHelpForGetRatesEnabled, IsHelpForApplicantLoginEnabled, IsHelpForApplicantLoanPipelineEnabled,
                    IsHelpForPersonalInformationEnabled, IsHelpForLoanInformationEnabled,
                    IsHelpForPropertyInformationEnabled, IsHelpForLoanOfficerEnabled,
                    IsHelpForAssetInformationEnabled, IsHelpForLiabilityInformationEnabled,
                    IsHelpForExpenseInformationEnabled, IsHelpForIncomeInformationEnabled,
                    IsHelpForDeclarationsEnabled, IsHelpForSelectLoanEnabled, IsHelpForSubmitApplicationEnabled,
                    IsHelpForLoanStatusAndDocExchangeEnabled, IsHelpForEmploymentEnabled,
                    automaticDUCraId, automaticDUCraLogin, automaticDUCraPassword, IsAskForMemberId,
                    EnabledCustomPmlFields_FullApp, EnabledCustomPmlFields_QuickPricer, armProgramDisclosuresURL, selectsLoanProgramInFullSubmission,
                    DefaultLoanTemplateId, submissionsShouldBecomeLoans, isGenerateLeadOnShortAppSubmission,
                    AskConsumerForSubjProperty, AskConsumerForSubjValue, loanStatusConfigJson, isAllowRespaOnly_Purchase,
                    isAllowRespaOnly_Refinance, isAllowRespaOnly_HomeEquity,
                    enableShortApplications, enableFullApplications, allowRegisterNewAccounts, ContactInfoMessage);
            }
            config.Save();
            SetResult("PortalId", config.Id);
        }
    }
}
