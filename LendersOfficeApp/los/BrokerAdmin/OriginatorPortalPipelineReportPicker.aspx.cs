﻿#region Generated Code
namespace LendersOfficeApp.los.BrokerAdmin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a picker for selecting a custom report
    /// available to P users in the originator portal.
    /// </summary>
    public partial class OriginatorPortalPipelineReportPicker : BasePage
    {
        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindCustomReportList();
        }

        /// <summary>
        /// Handles the item data-binding event.
        /// </summary>
        /// <param name="sender">
        /// The sender for the event.
        /// </param>
        /// <param name="e">
        /// The event arguments.
        /// </param>
        protected void ReportsList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            var reportIdNamePair = (KeyValuePair<Guid, string>)e.Item.DataItem;

            var queryName = e.Item.FindControl("QueryName") as HtmlGenericControl;
            queryName.InnerText = reportIdNamePair.Value;

            var selectLink = e.Item.FindControl("SelectLink") as HtmlAnchor;
            selectLink.Attributes["data-query-id"] = reportIdNamePair.Key.ToString();
            selectLink.Attributes["data-query-name"] = reportIdNamePair.Value;
        }

        /// <summary>
        /// Binds the list of custom reports for the picker.
        /// </summary>
        private void BindCustomReportList()
        {
            var existingReports = this.GetSelectedReportsFromCache();
            var reportList = Tools.GetAllLenderCustomReports(PrincipalFactory.CurrentPrincipal.BrokerId);

            this.ReportsList.DataSource = reportList.Where(report => !existingReports.Contains(report.Key)).OrderBy(report => report.Value, StringComparer.Ordinal);
            this.ReportsList.DataBind();
        }

        /// <summary>
        /// Gets the list of existing report query IDs that should
        /// be excluded from the listing.
        /// </summary>
        /// <returns>
        /// The set of existing report IDs that should be excluded.
        /// </returns>
        private HashSet<Guid> GetSelectedReportsFromCache()
        {
            var cacheKey = RequestHelper.GetSafeQueryString("k");
            var serializedData = AutoExpiredTextCache.GetFromCacheByUser(PrincipalFactory.CurrentPrincipal, cacheKey);
            return SerializationHelper.JsonNetDeserialize<HashSet<Guid>>(serializedData) ?? new HashSet<Guid>();
        }
    }
}