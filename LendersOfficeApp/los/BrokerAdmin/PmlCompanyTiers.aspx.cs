using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Admin;
using DataAccess;
using System.Linq;
using System.Collections.Generic;
using System.Web.Services;

namespace LendersOfficeApp.los.BrokerAdmin
{
	public partial class PmlCompanyTiers : LendersOffice.Common.BasePage
	{
        private static readonly PmlCompanyTier systemDefaultBlankPmlCompanyTier = new PmlCompanyTier()
        {
            Name = ConstApp.SystemDefualtBlankPmlCompanyTierName,
            Id = ConstApp.SystemDefaultBlankPmlCompanyTierId
        };
        private bool duplicatePmlCompanyTiersRemoved = false;
        private HashSet<string> restoredPmlCompanyTierNames = new HashSet<string>();

        private PmlCompanyTierList m_List = new PmlCompanyTierList();

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String ErrorMessage
		{
			// Write out error message.

			set
			{
				ClientScript.RegisterHiddenField( "m_errorMessage" , value.TrimEnd( '.' ) + "." );
			}
		}

		private String CommandToDo
		{
			// Write out error message.

			set
			{
				ClientScript.RegisterHiddenField( "m_commandToDo" , value );
			}
		}

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			// Initialize this view.

            try
            {
                // Load the broker to get the choices.

                BrokerDB myBroker = BrokerDB.RetrieveById(CurrentUser.BrokerId);

                if (!CurrentUser.HasPermission(Permission.AllowEditingOriginatingCompanyTiers))
                {
                    throw new CBaseException(ErrorMessages.GenericAccessDenied, "User tried to load a page without permission.");
                }

                if (IsPostBack == false)
                {
                    // 12/17/2004 kb - Get the starting list from the
                    // broker's set.
                    myBroker.PmlCompanyTiers.List.Sort();
                    m_List = myBroker.PmlCompanyTiers;
                }
                else
                {
                    // 7/15/2004 kb - Pull the entries from the grid,
                    // which was seeded with the last posted set and
                    // has any edits overlaid on top.

                    m_List.Clear();

                    foreach (DataGridItem dgItem in m_Grid.Items)
                    {
                        TextBox itemBox = dgItem.FindControl("Item") as TextBox;
                        TextBox idBox = dgItem.FindControl("Id") as TextBox;

                        var idStr = idBox == null ? "0" : idBox.Text;
                        int id = 0;
                        int.TryParse(idStr, out id);
                        bool isSystemDefaultPmlCompanyTier = itemBox.Text == ConstApp.SystemDefualtBlankPmlCompanyTierName 
                            && id == ConstApp.SystemDefaultBlankPmlCompanyTierId;

                        // 5/7/2014 gf - Filter out the system default lead 
                        // source. We do not want it to be available for saving.
                        // It is added back in PagePreRender.
                        if (itemBox != null && idBox != null && !isSystemDefaultPmlCompanyTier)
                        {
                            m_List.Add(new PmlCompanyTier() { Name = itemBox.Text, Id = id, Deleted = !dgItem.Visible });
                        }
                    }

                    //If they're adding a source that we already have but is marked as deleted,
                    //don't make a new source - just undelete the old one.
                    var reAdded = m_List.List.GroupBy(l => l.Name.ToUpper()).Where(g => g.Count() > 1);
                    foreach (var g in reAdded)
                    {
                        duplicatePmlCompanyTiersRemoved = true;
                        var byId = g.OrderBy(o => o.Id);
                        var oldest = g.First();
                        var others = g.Skip(1);
                        if (oldest.Deleted)
                        {
                            restoredPmlCompanyTierNames.Add(oldest.Name);
                        }
                        oldest.Deleted = false;

                        m_List.List.RemoveAll(r => others.Any(m => m.Id == r.Id));
                    }
                }
            }
            catch (CBaseException e)
            {
                // Oops!

                Tools.LogError("Failed to load page.", e);

                ErrorMessage = e.UserMessage;

                m_Ok.Enabled = false;
                m_Apply.Enabled = false;
                m_Add.Enabled = false;
            }
		}

		/// <summary>
		/// Overload framework event for loading.
		/// </summary>

		protected override void LoadViewState( object oState )
		{
			// Get the view state of a previous postback and restore
			// the data grid's state prior to loading form variables.

			base.LoadViewState( oState );

			if( ViewState[ "Cache" ] != null )
			{
				m_List = PmlCompanyTierList.ToObject( ViewState[ "Cache" ].ToString() );
			}
			
			m_Grid.DataSource = m_List.List;
			m_Grid.DataBind();
		}

		/// <summary>
		/// Overload framework event for saving.
		/// </summary>

		protected override object SaveViewState()
		{
			// Store what we have as latest in the viewstate so
			// we can stay current between postbacks without
			// hitting the database.

			ViewState.Add( "Cache" , m_List.ToString() );

			return base.SaveViewState();
		}

		/// <summary>
		/// Bind this page.
		/// </summary>

		protected void PagePreRender( object sender , System.EventArgs a )
		{
			// Bind and let grid pull current choices.  We
			// apply the current sorting settings to the
			// list prior to binding.  On save, this is
			// the order we persist.

			if( m_List.List.Count == 0 )
			{
				m_EmptyMessage.Visible = true;
			}
			m_List.List.Sort();
            // 5/7/2014 gf - insert the default lead source here because
            // this will prevent it from being available when events are
            // handled.
            m_List.List.Insert(0, systemDefaultBlankPmlCompanyTier);
			m_Grid.DataSource = m_List.List;
			m_Grid.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
            this.EnableJqueryMigrate = false;
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.PreRender += new System.EventHandler(this.PagePreRender);
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void GridItemBound( object sender , System.Web.UI.WebControls.DataGridItemEventArgs a )
		{
			// 12/17/2004 kb - Bind the current choice item to
			// the grid.  We may get to the point where we keep
			// all the descriptions of the same category in
			// one binding object.  At that point, we'll bind
			// to a nested list here.

            PmlCompanyTier sItem = a.Item.DataItem as PmlCompanyTier;

            if (sItem != null)
			{
                a.Item.Visible = !sItem.Deleted;
                TextBox itemBox = a.Item.FindControl("Item") as TextBox;
                TextBox idBox = a.Item.FindControl("Id") as TextBox;
                LinkButton removeLink = a.Item.FindControl("Remove") as LinkButton;
                bool isSystemDefaultPmlCompanyTier = sItem.Id == ConstApp.SystemDefaultBlankPmlCompanyTierId
                    && sItem.Name == ConstApp.SystemDefualtBlankPmlCompanyTierName;

				if (itemBox != null && idBox != null)
				{
					itemBox.Text = sItem.Name;
                    idBox.Text = sItem.Id.ToString();
                    removeLink.OnClientClick = "if(!removeClicked(" + LendersOffice.AntiXss.AspxTools.JsNumeric(sItem.Id) + ")) return false;";

                    if (isSystemDefaultPmlCompanyTier)
                    {
                        itemBox.ReadOnly = true;
                        removeLink.Visible = false;
                    }
				}
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void GridItemClick( object sender , System.Web.UI.WebControls.DataGridCommandEventArgs a )
		{
			// Handle delete command.  We use the position of the
			// row as our key.

			if( a.CommandName.ToLower() == "remove" )
			{
                //m_List.List[a.Item.ItemIndex].Deleted = true;

                // 5/7/2014 gf - Do not remove by index. The PageLoad handler
                // removes duplicate items from the list, which can make the
                // index invalid.
                // If the user removed an item which caused a previously deleted item
                // to be restored, we need to re-delete the restored item.
                var itemBox = a.Item.FindControl("Item") as TextBox;
                var nameToRemove = itemBox.Text;
                if (!duplicatePmlCompanyTiersRemoved || restoredPmlCompanyTierNames.Contains(nameToRemove))
                {
                    m_List.GetByName(nameToRemove).Deleted = true;
                }

				m_IsDirty.Value = "Dirty";
			}
		}

        [WebMethod]
        public static List<string> GetListOfPmlBrokerNamesByTierId(int id)
        {
            // Stop removal if Tier is used by any OC.
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", PrincipalFactory.CurrentPrincipal.BrokerId),
                                            new SqlParameter("@PmlCompanyTierId", id)
                                        };

            List<string> OcNames = new List<string>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(PrincipalFactory.CurrentPrincipal.BrokerId, "GetListOfPmlBrokerNamesByTierId", parameters))
            {
                while (reader.Read())
                {
                    OcNames.Add((string)reader["Name"]);
                }
            }

            return OcNames;
        }

		protected void ApplyClick( object sender , System.EventArgs a )
		{
			// Create new lead list and save it to the broker.

			try
			{
				// Store the cached list directly in the broker
				// and save.

				BrokerDB brokerDb = BrokerDB.RetrieveById( CurrentUser.BrokerId );

                if (!CurrentUser.HasRole(E_RoleT.Administrator))
                {
                    throw new CBaseException(ErrorMessages.GenericAccessDenied, "User tried to save changes without permission.");
                }

                brokerDb.PmlCompanyTiers = m_List;
                if (brokerDb.PmlCompanyTiers.List.Any(pmlCompanyTier => pmlCompanyTier.Name == ConstApp.SystemDefualtBlankPmlCompanyTierName
                    && pmlCompanyTier.Id == ConstApp.SystemDefaultBlankPmlCompanyTierId))
                {
                    throw new CBaseException(ErrorMessages.Generic, "Programming error. Should not try to save the default originating company tier to the broker.");
                }

                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", CurrentUser.BrokerId),
                                                new SqlParameter("@PmlCompanyTiersXmlContent", brokerDb.PmlCompanyTiers.ToString())
                                            };

                StoredProcedureHelper.ExecuteNonQuery(CurrentUser.BrokerId, "UpdateBrokerPmlCompanyTiers", 3, parameters);

				m_IsDirty.Value = "";
			}
			catch( CBaseException e )
			{
				// Oops!

				Tools.LogError( "Failed to save originating company tiers." , e );

				ErrorMessage = e.UserMessage;

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void OkClick( object sender , System.EventArgs a )
		{
			// Create new lead list and save it to the broker.

			try
			{
				// Store the cached list directly in the broker
				// and save.

				ApplyClick( sender , a );

				// We close on successful saving.

				CommandToDo = "Close";
			}
			catch( CBaseException e )
			{
				// Oops!

                Tools.LogError("Failed to save originating company tiers.", e);

				ErrorMessage = e.UserMessage;

				m_Ok.Enabled    = false;
				m_Apply.Enabled = false;
				m_Add.Enabled   = false;
			}
		}

		/// <summary>
		/// Handle UI event.
		/// </summary>

		protected void AddClick( object sender , System.EventArgs a )
		{
			// Create new entry only if the strings are valid.

			m_IsDirty.Value = "Dirty";

			m_List.Add( new PmlCompanyTier() );
		}

	}

}
