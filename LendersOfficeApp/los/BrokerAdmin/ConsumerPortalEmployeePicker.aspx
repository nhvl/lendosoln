﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsumerPortalEmployeePicker.aspx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.ConsumerPortalEmployeePicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <script type="text/javascript">
        function _init() {
            resize(500, 400);        
        }
        function f_selectEmployee(empId, fullName) {
			var args = window.dialogArguments || {};
            args.EmployeeId = empId;
            args.FullName = fullName;
            onClosePopup(args);
        }
    </script>
    <h4 class="page-header">Select Employee</h4>
    <form id="form1" runat="server">
        <table cellspacing="2" cellpadding="0" width="100%" height="100%">
			<tr>
			    <td>
				    <ml:CommonDataGrid id="m_Grid" runat="server" CellPadding="2" DefaultSortExpression="FullName">
	                    <Columns>
		                    <asp:TemplateColumn HeaderText="Employee">
			                    <ItemTemplate>
				                    <a href="javascript:void(0);" onclick="f_selectEmployee(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "EmployeeId").ToString()) %>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FullName").ToString()) %>);">
					                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "FullName").ToString())%>
				                    </a>
			                    </ItemTemplate>
		                    </asp:TemplateColumn>
		                </Columns>
	                </ml:CommonDataGrid>
	            </td>
	        </tr>
	    </table>
    </form>
</body>
</html>
