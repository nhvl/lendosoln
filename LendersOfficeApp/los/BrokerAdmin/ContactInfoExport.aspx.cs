using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LendersOffice.Common;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace LendersOfficeApp.los.BrokerAdmin
{
	public partial class ContactInfoExport : BasePage
	{  

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		/// <summary>
		/// Initialize this page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
			if(CurrentUser.HasFeatures( E_BrokerFeatureT.PriceMyLoan ) && CurrentUser.HasPermission(Permission.CanAdministrateExternalUsers))
			{
				m_AllowAccessPanel.Visible = true;
				m_DenyAccessPanel.Visible = false;
			}
			else
			{
				m_AllowAccessPanel.Visible = false;
				m_DenyAccessPanel.Visible = true;
				return;
			}

			try
			{
				// Stream out the batch of policy groups so that
				// the user can save it as an xml document.

                Response.Output.Write(PmlBroker.CreateContactInfoCSV(CurrentUser, Request["type"]));
				// Mark this page as a document we can download.
				Response.AddHeader( "Content-Disposition" , "attachment; filename=\"ContactInfo.csv\"" );
				Response.ContentType = "application/csv";
				// Close up shop and return.
                Response.Flush();
				Response.End();
			}
			catch( System.Threading.ThreadAbortException )
			{
				// Pass on the abort exception so export can finish
				// without writing the page.

				throw;
			}
			catch( Exception e )
			{
				Tools.LogError( "Failed to load contact info export." , e );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion

	}
}
