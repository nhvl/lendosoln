<%@ Register TagPrefix="ils" TagName="EmployeeRoleChooserLink" Src="../common/EmployeeRoleChooserLink.ascx" %>
<%@ Page language="c#" Codebehind="EditPmlUser.aspx.cs" AutoEventWireup="True" Inherits="LendersOfficeApp.los.BrokerAdmin.EditPmlUser" %>
<%@ Register TagPrefix="uc" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="DUDOSearch" Src="DoDuLoginNameSearch.ascx" %>
<%@ Register TagPrefix="uc" TagName="LendingLicenses" Src="../../los/BrokerAdmin/LendingLicensesPanel.ascx" %>
<%@ Register TagPrefix="uc" TagName="Relationships" Src="~/los/BrokerAdmin/PmlUserRelationships.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import namespace="LendersOffice.Common" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD  runat="server">
		<title>Edit Pml User</title>
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<style type="text/css" >
			#columnA {width:435px;}
			#columnB {margin-left: 5px;}
			.padleft2 {padding-left: 25px;}
			.heading  {margin-top: 10px;padding-bottom: 5px;margin-bottom: 5px;BORDER-BOTTOM: lightgrey 2px solid; }
			.padbottom{padding-bottom: 7px;}
			.clear {clear: both;}
			.left {float: left;}
			.right {float:right;}
			.ipinput {width: 110px;}
			.modalmsg { border: black 3px solid; padding: 5px; display: none; position:absolute; background-color: whitesmoke; }
			.note { font-size: 8pt; color: dimgray;  }
			.warning {background-color: white;border: 1px solid black;color : red; font-weight: bold; padding: 0 5px 0 5px; font-size: .9em;}
			.disabled { background-color: gainsboro; }
			.#selectmask{	display:none; position:absolute;top:0px;left:0px; filter:mask()}
			.modalbox {  height: 100px; border:3px inset black; top: 200px;  margin-left: 32px; left: 0; right: 0;  width: 675px; display: none; position: absolute; background-color : whitesmoke;}
			.handcursor { cursor: pointer; }
			.tabBorder { border: 2px groove; padding: 6px; background-color: lightgrey; text-align: center; width: 10%; }
			.pmlheader {  border-bottom: #D3D3D3 2px solid; padding-bottom: 1px}
			.newheader { color: White; background-color: rgb(0,128,255); padding-left: 5px; }
			.error { color: Red; }
        	.CustomLOPPFieldLabel { display: inline-block; padding-right: 20px; font-weight: normal !important; font-size: 12px !important; }
        	.border-bottom td { border-bottom: groove 1px darkgray; padding-bottom: 5px; }
        	.relationship-header { border-bottom: lightgrey 2px solid; }
        	.role-label { padding-left: 15px; }
        	.underline { text-decoration: underline; }
            .createLoan { display: inline-block; margin-left: 15px; }
		</style>
		<script type="text/javascript" src="../../inc/utilities.js" > </script>

		
  </HEAD>
  
	<body  BACKGROUND-COLOR: "#003366"   onload="onInit();" >
		<script type="text/javascript" >


			function prepareCustomLOPricingPolicyInfo()
			{
				// Iterate over the table, and get the values field ids and values for each.
				var customFields = [];
				$('.CustomLOPricingPolicyTable :input').each(function() {
					customFields.push({ Id: this.id, Value: $(this).val() });
				});
				$('#CustomLOPPFieldSettings').val(JSON.stringify(customFields));
			}

			function GetUpdatedOrignatingId()
			{
				return document.getElementById(<%= AspxTools.JsString(m_CompanyChoice.DataID) %>).value;
			}

			function IsNoLongerSupervisor()
			{
				var isSupervisorOnLoad = $("#IsSupervisorOnLoad").val() === 'True';

				var isSupervisorChecked = <%=AspxTools.JsGetElementById(this.m_IsSupervisor)%>.checked;

				return isSupervisorOnLoad && !isSupervisorChecked;
			}

			function GetExSupervisorEmployeeIdsToIgnore()
			{
				if (IsNoLongerSupervisor()) {
					return <%=AspxTools.JsString(this.empId)%>;
				}

				return null;
			}

			function ValidateRoles() {
				var bpElement = document.getElementById('<%= AspxTools.ClientId(m_IsBrokerProcessor) %>');
				var pcElement = document.getElementById('<%= AspxTools.ClientId(m_IsExternalPostCloser) %>');

				PMLUserRelationships.onRolesChange(bpElement.checked, pcElement.checked);
			}

			var IP = {
				ip_input_id  : 'ip_input',
				ip_sel_id    : '<%= AspxTools.ClientId(m_ip) %>',
				ip_add_btn_id: 'ip_add',
				ip_rem_btn_id: 'ip_remove',
				ip_hid_id	 : 'm_ip_parsed',

				Filter : function (IsOn)
				{
					if ( IsOn ) this.Clear();
					document.getElementById(this.ip_input_id).disabled = IsOn;
					document.getElementById(this.ip_input_id).className = IsOn ? "disabled ipinput" : "ipinput";
					document.getElementById(this.ip_sel_id).disabled = IsOn;
					document.getElementById(this.ip_sel_id).className = IsOn ? "disabled ipinput" : "ipinput";
					document.getElementById(this.ip_add_btn_id).disabled = IsOn;
					document.getElementById(this.ip_rem_btn_id).disabled = IsOn;

				},
				Remove  : function()
				{
					var from = document.getElementById(this.ip_sel_id);

					for ( var i = (from.options.length-1); i >= 0; i-- )
					{
						var o = from.options[i];

						if ( o.selected )
						{
							from.options[i] = null;
						}
					}
					this.UpdateHiddenField();
				},
				CheckInputKey : function(event)
				{

					if ( event.keyCode == 13 )
					{
						document.getElementById(this.ip_add_btn_id).click();
						return false;
					}
					return true;
				},
				Clear : function()
				{
					var from = document.getElementById(this.ip_sel_id);
					for ( var i = (from.options.length-1); i >= 0; i-- )
					{
						from.options[i] = null;
					}
					this.UpdateHiddenField();
				},
				ShowWarning : function()
				{
					alert(<%=AspxTools.JsString(JsMessages.InvalidIpFormat)%>);
				},
				Add : function(id)
				{
					var val = document.getElementById(id);
					var from = document.getElementById(this.ip_sel_id);
					<%-- I tried Reg ex and could not figure out why 127..1.1.1.1 was considered an ip due to lack of time i did the following --%>
					var segments = val.value.split('.');
					if ( segments.length != 4 )
					{
						this.ShowWarning();
						return false;
					}
					var num;
					for( var x = 0; x < 4; x++ )
					{
						num = parseInt(segments[x]);
						if (  isNaN(num) || num< 0 || num > 255  )
						{
							this.ShowWarning();
							return false;
						}
					}

					if ( document.getElementById(this.ip_hid_id).value.length  + val.value.length > 200 )
					{
						alert(<%=AspxTools.JsString(JsMessages.MaxIpCount)%>);
						return false;
					}

					for ( var i = 0; i < from.options.length; i++ )
					{
						if ( from.options[i].text == val.value )
						{
							alert( <%= AspxTools.JsString(JsMessages.DuplicateIp) %>);
							return false;
						}
					}
					from.options[from.options.length] = new Option( val.value );
					val.value = '';
					this.UpdateHiddenField();
				},
				UpdateHiddenField : function ()
				{
					var from = document.getElementById(this.ip_sel_id);
					var hid  = document.getElementById(this.ip_hid_id);
					hid.value = '';
					for( var x = 0; x < from.options.length; x++ )
					{
						hid.value += from.options[x].text + ';';
					}

				}
			}

		</script>
		<script type="text/javascript">
		<!--

		jQuery(function($) {
		    function toggleCellRequired(userMfaSetting, isBrokerMfaEnabled)
		    {
		        var cellRequired = userMfaSetting != null && userMfaSetting.checked && isBrokerMfaEnabled;
		        ValidatorEnable(<%=AspxTools.JsGetElementById(CellPhoneValidator) %>, cellRequired);
		        $(<%=AspxTools.JsGetElementById(CellPhoneReqImg) %>).css("display", cellRequired? "" : "none");
            }

            $('#DisableAuthenticator').click(function () {
                $('#EnableAuthCodeViaAuthenticator').prop('checked', false);
                $('#IsDisablingAuthenticator').val("True");
                updateDirtyBit_callback();
            });

		    var isEnableMultiFactorAuthenticationTPO = $(<%=AspxTools.JsGetElementById(IsEnableMultiFactorAuthenticationTPO) %>).val();
		    var enableAuthCodeSmsCheckbox = <%=AspxTools.JsGetElementById(EnableAuthCodeViaSms) %>;

		    toggleCellRequired(enableAuthCodeSmsCheckbox, isEnableMultiFactorAuthenticationTPO === "True");

		    if(enableAuthCodeSmsCheckbox != null && isEnableMultiFactorAuthenticationTPO === "True")
		    {
		        $(enableAuthCodeSmsCheckbox).change(function() {
                    toggleCellRequired(this, true);
                });
		    }

		    var employeeId = $("#EmployeeId").val();
		    var userId = $('#UserId').val();
		    var employeeType = $('#EmployeeType').val();
		    if($('#IsNewEmployee').val().toLowerCase() === 'true') {
                $('#ServiceCredentialNewEmpDiv').show();
                $('#ServiceCredentialDiv').hide();
            }
            else {
                $('#ServiceCredentialNewEmpDiv').hide();
                $('#ServiceCredentialDiv').show();
                var serviceCredentialData = {
                    EmployeeId: employeeId,
                    UserId: userId,
                    EmployeeType: employeeType,
                    ServiceCredentialJsonId: "ServiceCredentialsJson",
                    ServiceCredentialTableId: "ServiceCredentialTable"
                };
                ServiceCredential.Initialize(serviceCredentialData);
                ServiceCredential.RenderCredentials();
            }

            $('#AddServiceCredentialBtn').click(function() {
                ServiceCredential.AddServiceCredential();
            });
		});

		function _init() {
		    CheckEnableControls();
		    CheckEnableCustomPricingPolicyControls();

            addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(RegularExpressionValidator1) %>);

		    $('.viewLoan').click(function () {
		        var hasViewPermission = this.checked;
		        var $createCb = $(this).nextAll('.createLoan').first();

		        if (hasViewPermission) {
		            $createCb.prop('disabled', false);
		        } else {
                    if ($createCb.prop('checked')) {
                        $createCb.prop('checked', false);
                    }

		            $createCb.prop('disabled', true);
		        }
		    });

            $('.createLoan').not(':checked').each(function(index, element) {
                var $viewCb = $(this).prevAll('.viewLoan').first();
                if (!$viewCb.prop('checked')) {
                    element.disabled = true;
                }
            });
		}

        function revokeClientCertificate(certificateId)
        {
            if (confirm('Do you want to revoke the client certificate?'))
            {
            __doPostBack('ClientCertificateRevoke', certificateId);
            }
            return false;
        }

        function deleteIp(id)
        {
            if (confirm('Do you want to delete IP?'))
            {
                __doPostBack('RegisteredIpDelete', id);
            }
            return false;
        }

        function onCompanyChange(pmlBrokerId) {
            var args = {
                "PmlBrokerId" : pmlBrokerId
            };

            var result = gService.editpmluser.call( "RetrievePmlBroker", args );

            if (!result.error) {
                setCompanyAddress(result.value.Addr, result.value.City, result.value.State, result.value.Zip);
            }

	        PMLUserRelationships.onOriginatingCompanyChange();
        }

        function setCompanyAddress(addr, city, state, zip) {
            <%= AspxTools.JsGetElementById(m_Address) %>.value = addr;
            <%= AspxTools.JsGetElementById(m_City) %>.value = city;
            <%= AspxTools.JsGetElementById(m_State) %>.value = state;
            <%= AspxTools.JsGetElementById(m_Zipcode) %>.value = zip;
        }

		function myResize(w, h)
		{
			resize( w, h );

			if ( document.documentElement.offsetHeight  != h )
			{
				var chrome =  h - document.documentElement.offsetHeight ;
				resize ( w, h + chrome ) ;
			}
		}

		function onInit()
		{
		    ValidateRoles();
			onTabClick( document.getElementById( document.getElementById('<%= AspxTools.ClientId(m_CurrentTab) %>').value ) );

		    var isDisabled = false;
		    var docIsDisabled = document.getElementById("m_isDisabled");
		    if( docIsDisabled != null )
			{
		        if( docIsDisabled.value == "True")
					isDisabled = true;
				else
					isDisabled = false;
			}

			if(<%= AspxTools.JsBool(m_isNew) %>)
			{
				document.getElementById('<%= AspxTools.ClientId(m_loginR) %>').style.display = "";
				document.getElementById('<%= AspxTools.ClientId(m_passwordR) %>').style.display = "";
				document.getElementById('<%= AspxTools.ClientId(m_retypeR) %>').style.display = "";
			}

		    var error = document.getElementById('m_errorMessage');
		    var commandToDo = document.getElementById("m_commandToDo");
			if( error != null  )
			{
				alert( error.value ) ;
			}
			else
			if( commandToDo != null )
			{
				if( commandToDo.value == "Close" )
				{
					var args = {};
					args.OK = true;
					onClosePopup(args);
				}
			}

			if(isDisabled == false)
				SetUIStatus();

			if( <%= AspxTools.JsBool(IsPostBack) %> === false ) {
			    if( window.dialogArguments != null ) {
				    myResize( 900 , 750);
		        }
			}

			<%--//if the account is locked, display the account locked message and unlock button --%>
			if(document.getElementById('<%= AspxTools.ClientId(m_IsAccountLocked) %>').value == "true")
				document.getElementById('<%= AspxTools.ClientId(m_UnlockAccountPanel) %>').style.display = "";
			else
				document.getElementById('<%= AspxTools.ClientId(m_UnlockAccountPanel) %>').style.display = "none";

			if(isDisabled == false)
			{
				if ( <%= AspxTools.JsBool(m_EnableLogin) %> ) {
					if(document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>'))
						document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>').disabled = false;
				}
				else {
					if(document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>'))
						document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>').disabled = true;
				}
				onChangeLoginClick();

				document.getElementById('<%= AspxTools.ClientId(m_ApplySeen) %>').disabled = ! isUserEditorDirty();

				document.getElementById('<%= AspxTools.ClientId(m_ExpirationPeriod) %>').disabled = <%= AspxTools.JsBool(expirationPeriodDisabled) %>;
				document.getElementById('<%= AspxTools.ClientId(m_CycleExpiration) %>').disabled = <%= AspxTools.JsBool(cycleExpirationDisabled) %>;
				document.getElementById('<%= AspxTools.ClientId(m_ExpirationPeriod) %>').style.backgroundColor = "white";

				var expirationDateCB = document.getElementById('<%= AspxTools.ClientId(m_ExpirationDate) %>');
				if(<%= AspxTools.JsBool(expirationDateDisabled) %>)
				{
					expirationDateCB.disabled = true;
					expirationDateCB.style.backgroundColor = "gainsboro";
				}
				else
				{
					expirationDateCB.disabled = false;
					expirationDateCB.style.backgroundColor = "white";
				}
			}

			var MinAmountEnabled = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmountEnabled)%>;
		    var MaxAmountEnabled = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmountEnabled)%>;
		    var MinAmountVal = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmount)%>.value;
		    var MaxAmountVal = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmount)%>.value;
		    var ZeroDollarStr = <%= AspxTools.JsString(m_ZeroDollarStr)%>
		    MinAmountEnabled.checked = MinAmountVal.length > 0 && MinAmountVal != ZeroDollarStr;
		    MaxAmountEnabled.checked = MaxAmountVal.length > 0 && MaxAmountVal != ZeroDollarStr;
		    setAmountTextboxStatus(<%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmountEnabled)%> );
		    setAmountTextboxStatus(<%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmountEnabled)%> );
			PopulatePmlBrokerCompensation();
			PopulatePmlBrokerCustomPricingPolicy();

			IP.Filter(<%= AspxTools.JsBool(m_ipRestrictionOff.Checked) %>);
		    <% if ( ( m_isDOEnabled  || m_isDUEnabled )  && m_sPmlAutoLoginOption == "MustBeUnique" && m_isNew ) { %>
				DUDO.Init(<%= AspxTools.JsString(empId) %>) ;
			<% } %>

			var serializedCustomFields = $('#CustomLOPPFieldSettings').val();
			if (serializedCustomFields !== '') {
			    var customFieldInfo = $.parseJSON(serializedCustomFields);
			    for (var i = 0; i < customFieldInfo.length; i++) {
			        var field = customFieldInfo[i];
			        $('#' + field.Id).val(field.Value);
			    }
			}

			lockNameOnLoanDoc(document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDocLock) %>));
		}

		function setAmountTextboxStatus(cb) {
		    var id = cb.id.substring(0, cb.id.length - 7);
		    if (!cb.checked) {
		        document.getElementById(id).value = "";
		    }
		    document.getElementById(id).readOnly = !cb.checked;
			document.getElementById(id).style.background = cb.checked ? "white" : "gainsboro";
			document.getElementById(id + 'R').style.display = cb.checked ? "" : "none";
		}

		function onClientApplyClick()
		{
			if ( isUserEditorDirty() )
			{
				var isValid = true;
				var isLoginValid = true;
				var isPwValid = false;
				showOCMessage();
				<% if ( ( m_isDOEnabled  || m_isDUEnabled )  && m_sPmlAutoLoginOption == "MustBeUnique") { %>
					 checkDODU()  ;
				<% } %>
				if (typeof(Page_ClientValidate) == 'function')
					isValid = Page_ClientValidate();
				<% if ( ( m_isDOEnabled  || m_isDUEnabled )  && m_sPmlAutoLoginOption == "MustBeUnique") { %>
					isValid = isValid && passedDu && passedDo;
				<% } %>

			    if(isValid == true && ValidateLogin() == true)
			    {
			        ValidatePasswords(finishValidatingPassword);
				}
			}
		}

		    function finishValidatingPassword()
		    {
		        if(VerifySupervisorChange())
		        {
		            if (typeof saveAllLicenses === 'function')
		                saveAllLicenses();

		            SaveMeApply();
		        }
		    }

		function ValidateOriginatingCompanySettings()
		{
		    var pmlBrokerID = GetUpdatedOrignatingId();

		    if (pmlBrokerID === <%=AspxTools.JsString(Guid.Empty)%>)
            {
                return 'You must specify a valid originating company for this user.';
		    }

		    var LpePriceGroupId = PMLUserRelationships.getPriceGroupIDForMode('Broker');
		    var BranchId = PMLUserRelationships.getBranchIDForMode('Broker');
		    var miniCorrPriceGroupId = PMLUserRelationships.getPriceGroupIDForMode('MiniCorrespondent');
		    var miniCorrBranchId = PMLUserRelationships.getBranchIDForMode('MiniCorrespondent');
		    var corrPriceGroupId = PMLUserRelationships.getPriceGroupIDForMode('Correspondent');
		    var corrBranchId = PMLUserRelationships.getBranchIDForMode('Correspondent');

		    var params = {
		        'pmlBrokerID': pmlBrokerID,
		        'lpePriceGroupId': LpePriceGroupId,
		        'branchId': BranchId,
		        'miniCorrespondentPriceGroupId': miniCorrPriceGroupId,
		        'miniCorrespondentBranchId': miniCorrBranchId,
		        'correspondentPriceGroupId': corrPriceGroupId,
		        'correspondentBranchId': corrBranchId
	        };

            var strParams = JSON.stringify(params);

            var ret;
		    callWebMethodAsync({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'EditPmlUserService.aspx/ValidateOriginatingCompanySettings',
            data: strParams,
            dataType: 'json',
            async: false,
            success: function(msg)
            {
                ret = msg.d;
            },
            error: function (msg)
            {
                alert('error' + msg);
            }
            });

            return ret;
		}

		function SaveMeApply()
            {
		    var applyBtn = document.getElementById('<%= AspxTools.ClientId(m_Apply) %>');
		    applyBtn.disabled = false;
		    applyBtn.click();
		}

		function SaveMeOk()
		{
		    var okBtn = document.getElementById('<%= AspxTools.ClientId(m_Ok) %>');
			okBtn.disabled = false;
			okBtn.click();
		}

		function ValidateLogin()
		{
			var args = new Object();
			args["pw"] = document.getElementById('<%= AspxTools.ClientId(m_Password) %>').value;
			var hasLogin = "true";
			if( <%= AspxTools.JsBool(m_isNew) %>)
				hasLogin = "false";
			args["hasLogin"] = hasLogin;
            args["id"] = <%= AspxTools.JsString(ViewState[ "employeeId" ].ToString()) %>;
			args["firstName"] = document.getElementById('<%= AspxTools.ClientId(m_FirstName) %>').value;
			args["lastName"] = document.getElementById('<%= AspxTools.ClientId(m_LastName) %>').value;
			args["login"] = document.getElementById('<%= AspxTools.ClientId(m_Login) %>').value;
			args["expirationDate"] = document.getElementById('<%= AspxTools.ClientId(m_ExpirationDate) %>').value;
			args["passwordExpiresOnChecked"] = document.getElementById('<%= AspxTools.ClientId(m_PasswordExpiresOn) %>').checked;
			var changeLoginChecked = false;
			if( <%= AspxTools.JsBool(m_isNew) %>)
				changeLoginChecked = true;
			if(document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>') && document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>').checked)
				changeLoginChecked = true;
			args["changeLoginChecked"] = changeLoginChecked;
			args["IsPml"] = true;
			args["originatorCompensationMinAmount"] = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmount)%>.value;
		    args["originatorCompensationMaxAmount"] = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmount)%>.value;
		    args["originatorCompensationMinAmountEnabled"] = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmountEnabled)%>.checked;
		    args["originatorCompensationMaxAmountEnabled"] = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmountEnabled)%>.checked;
			args["isADUser"] = "false";

			var result = gService.main.call("ValidateFields", args);
			if(result.value && result.value["ErrorMessage"])
			{
				document.getElementById("m_LoginErrorMessage").innerText = result.value["ErrorMessage"];
				return false;
			}
			return true;
		}

        // Call the callback function if the return value is true.
		function ValidatePasswords(callback)
		{
			var args = new Object();
			args["pw"] = document.getElementById('<%= AspxTools.ClientId(m_Password) %>').value;
			args["retype"] = document.getElementById('<%= AspxTools.ClientId(m_Confirm) %>').value;
			args["id"] = <%= AspxTools.JsString(ViewState[ "employeeId" ].ToString()) %>;
			args["isPml"] = "true";
			args["isADUser"] = "false";

			var result = gService.main.call("ValidatePassword", args);

			if(result.value && result.value["ErrorMessage"])
			{
				if(result.value["ErrorMessage"] == <%= AspxTools.JsString(ErrorMessages.PasswordsShouldNotBeRecycled) %>)
				{
					var arg;
					var url;
						if ( <%= AspxTools.JsBool(this.IsInternal) %> ) {
							url = "/loadmin/broker/ConfirmRepeatPasswordOverride.aspx";

						} else {
							url = "/los/BrokerAdmin/ConfirmRepeatPasswordOverride.aspx";
						}

						showModal(url, null, "Confirm Repeat Password Override", null, function(arg){
							if(typeof(arg.choice) == 'undefined' || arg.choice == 1)
							{
								document.getElementById("m_LoginErrorMessage").innerText = result.value["ErrorMessage"];
							}

							if(arg.choice == 0)
							{
								document.getElementById("m_LoginErrorMessage").innerText = "";
								callback();
							}
						},{hideCloseButton: true});
				}
				else
				{
				    document.getElementById("m_LoginErrorMessage").innerText = result.value["ErrorMessage"];
				}
			}
			else
			{
			    document.getElementById("m_LoginErrorMessage").innerText = "";
			    callback();
			}
		}

		function VerifySupervisorChange()
		{
		    if (!IsNoLongerSupervisor())
		    {
		        return true;
		    }

		    var args = {
                supervisorId: <%=AspxTools.JsString(this.empId)%>
		    };

		    var result = gService.editpmluser.call("VerifySupervisorChange", args);

		    if (result.error === false && result.value['NeedsWarning'] === 'True') {
		        alert("CANNOT COMPLETE UPDATE!\n\n"
                    + <%=AspxTools.JsGetElementById(this.m_FirstName)%>.value + " " + <%=AspxTools.JsGetElementById(this.m_LastName)%>.value
                    + " is a supervisor for other users. Please update the supervisor for those users before proceeding with this change.");

		        return false;
		    }

		    return true;
		}

		function onPwTextChange()
		{
			var pwText = document.getElementById("<%= AspxTools.ClientId(m_Password) %>").value;
			var confirmText = document.getElementById("<%= AspxTools.ClientId(m_Confirm) %>").value;
			if(! <%= AspxTools.JsBool(m_isNew) %>)
			{
				if(confirmText != "" || pwText == "")
					document.getElementById('<%= AspxTools.ClientId(m_retypeR) %>').style.display = "none";
				else
					document.getElementById('<%= AspxTools.ClientId(m_retypeR) %>').style.display = "";
			}
		}

		function onDuLoginTextChange()
		{
			var loginText = document.getElementById("<%= AspxTools.ClientId(m_duLogin) %>").value;
			var pwText = document.getElementById("<%= AspxTools.ClientId(m_duPw) %>").value;

			if( <%= AspxTools.JsBool(m_isNew) %>)
			{
				if(pwText == "" && loginText != "")
					document.getElementById('<%= AspxTools.ClientId(m_duPasswordR) %>').style.display = "";
				else
					document.getElementById('<%= AspxTools.ClientId(m_duPasswordR) %>').style.display = "none";
			}
		}

		function onDoLoginTextChange()
		{
			var loginText = document.getElementById("<%= AspxTools.ClientId(m_doLogin) %>").value;
			var pwText = document.getElementById("<%= AspxTools.ClientId(m_doPw) %>").value;
			if( <%= AspxTools.JsBool(m_isNew) %>)
			{
				if(pwText == "" && loginText != "")
					document.getElementById('<%= AspxTools.ClientId(m_doPasswordR) %>').style.display = "";
				else
					document.getElementById('<%= AspxTools.ClientId(m_doPasswordR) %>').style.display = "none";
			}
		}

		function onClientOkClick()
		{
			if ( isUserEditorDirty() )
			{
				var isValid = true;
				var isLoginValid = true;
				var isPwValid = false;
				showOCMessage();
                if ( ( <%= AspxTools.JsBool(m_isDOEnabled) %> || <%= AspxTools.JsBool(m_isDUEnabled) %> )  && <%= AspxTools.JsString(m_sPmlAutoLoginOption) %> == "MustBeUnique") {
					 checkDODU()  ;
				}
				if (typeof(Page_ClientValidate) == 'function')
					isValid = Page_ClientValidate();

				if ( ( <%= AspxTools.JsBool(m_isDOEnabled) %> || <%= AspxTools.JsBool(m_isDUEnabled) %> )  && <%= AspxTools.JsString(m_sPmlAutoLoginOption) %> == "MustBeUnique") {
					isValid = isValid && passedDu && passedDo;
				}

			    if(isValid == true && ValidateLogin() == true)
				{
			        ValidatePasswords(finishOkClickHandler);
				}
			}
			else
				onClosePopup();
		}

		    function finishOkClickHandler()
		    {
		        if(VerifySupervisorChange()) {
		            if (typeof saveAllLicenses === 'function') { saveAllLicenses(); }
		            SaveMeOk();
		        }
		    }

		function onCancelClick()
		{
			if ( isUserEditorDirty() )
			{
				if( confirm( "Close without saving?" ) == false )
					return;
			}
			onClosePopup();
		}

		function expirationPolicyClick()
		{
			document.getElementById('<%= AspxTools.ClientId(m_Dirty) %>').value = "1";

			var expiresOn = document.getElementById("<%= AspxTools.ClientId(m_PasswordExpiresOn) %>");
			var mustChangePw = document.getElementById("<%= AspxTools.ClientId(m_MustChangePasswordAtNextLogin) %>");
			var cycleExpiration = document.getElementById("<%= AspxTools.ClientId(m_CycleExpiration) %>");
			var expirationPeriod = document.getElementById("<%= AspxTools.ClientId(m_ExpirationPeriod) %>");
			var expirationDate = document.getElementById("<%= AspxTools.ClientId(m_ExpirationDate) %>");


			if( expiresOn.checked == true || mustChangePw.checked == true )
			{
				expirationCycleClick();
				cycleExpiration.disabled = false;
			}
			else
			{
				expirationPeriod.disabled = true;
				cycleExpiration.disabled  = true;
				expirationDate.style.backgroundColor="gainsboro";
				cycleExpiration.checked = false;
			}

			if( expiresOn.checked == true )
			{
				expirationDate.disabled = false;
				expirationDate.style.backgroundColor="white";
			}
			else
			{
				expirationDate.disabled = true;
				expirationDate.style.backgroundColor="gainsboro";
			}
		}

		function expirationCycleClick()
		{
			document.getElementById('<%=AspxTools.ClientId(m_Dirty)%>').value = "1";
			document.getElementById("<%=AspxTools.ClientId(m_ExpirationPeriod)%>").disabled = (document.getElementById("<%=AspxTools.ClientId(m_CycleExpiration)%>").checked)?false:true;
		}

		function showOCMessage() {
		    var currentOc = document.getElementById("UsersCurrentOc");
		    var currentOcName = document.getElementById("UsersCurrentOcName");
		    var ocPicker = document.getElementById(<%= AspxTools.JsString(m_CompanyChoice.DataID) %>);
		    var lar = document.getElementById('<%= AspxTools.ClientId(HaveShownOcError) %>');
		    if( !currentOc || !currentOcName || !ocPicker || lar.value == 'true') {
		        return;
		    }
		    if( currentOc.value != ocPicker.value )
		    {

		        var warning =  "Changing this user's Originating Company will prevent him/her from accessing loan files belonging to " +
                        currentOcName.value + ".";
		        lar.value = 'true';
		        alert( warning) ;
		    }
        }

		function SetUIStatus()
		{
			var bSupervisorChecked = document.getElementById('<%= AspxTools.ClientId(m_IsSupervisor)%>').checked;
			var bLoanLevelSupervisor = document.getElementById('<%= AspxTools.ClientId(m_AccessSupervisor)%>');
			var bLoanLevelIndv = document.getElementById('<%= AspxTools.ClientId(m_AccessIndividual)%>');

		    setDisabledAttr(bLoanLevelSupervisor.parentNode, !bSupervisorChecked);
		    setDisabledAttr(bLoanLevelSupervisor, !bSupervisorChecked);
		    disableChooser(document.getElementById("SupervisorChoicePanel"), !bSupervisorChecked);

			if(!bSupervisorChecked && bLoanLevelSupervisor.checked)
			{
			        bLoanLevelIndv.checked = true;
			}

			var canSubmitCheckbox = <%=AspxTools.JsGetElementById(m_CanSubmitWithoutCreditReport)%>;
			var canRunPricingCheckbox = <%=AspxTools.JsGetElementById(m_CanRunPricingEngineWithoutCreditReport)%>;

		    // These checkboxes will be hidden when a new user is set to populate permissions
            // from their OC.
			if (canSubmitCheckbox !== null && canRunPricingCheckbox !== null)
			{
			    if ( canRunPricingCheckbox.checked == true )
			    {
			        canSubmitCheckbox.disabled = false;
			    }
			    else
			    {
			        canSubmitCheckbox.checked = false;
			        canSubmitCheckbox.disabled = true;
			    }
			}
		}

		function ResetSupervisor() {
		    document.getElementById(<%= AspxTools.JsString(m_SupervisorChoice.DataID) %>).value = '00000000-0000-0000-0000-000000000000';
	        document.getElementById('m_SupervisorChoice_m_User').innerText = 'None';
		}

		<%--// 06/13/06 mf - To keep the layout from getting
		    // mangled by validator error text, I wrote a custom
		    // validator that will display all messages together.
		    // We simply check the status of all the validators
		    // to determine what message to display when the user
		    // clicks apply or okay. The individual validators
		    // still enforce field validation.
		    --%>

		function ValidateFields(source, arguments)
		{
			var sErrorText = '';
			if (!document.getElementById("<%= AspxTools.ClientId(m_R1) %>").isvalid)
				sErrorText += (sErrorText == '') ? 'First Name' : ', First Name';
			if (!document.getElementById("<%= AspxTools.ClientId(m_R2) %>").isvalid)
				sErrorText += (sErrorText == '') ? 'Last Name' : ', Last Name';
			if (!document.getElementById("<%= AspxTools.ClientId(m_R3) %>").isvalid)
				sErrorText += (sErrorText == '') ? 'Phone' : ', Phone';
			if (!document.getElementById("<%= AspxTools.ClientId(m_R4) %>").isvalid)
				sErrorText += (sErrorText == '') ? 'Email' : ', Email';
			if (!document.getElementById("<%= AspxTools.ClientId(CellPhoneValidator) %>").isvalid)
				sErrorText += (sErrorText == '') ? 'Cell Phone' : ', Cell Phone';

			if(document.getElementById('<%= AspxTools.ClientId(m_duPasswordR) %>') && document.getElementById('<%= AspxTools.ClientId(m_duPasswordR) %>').style.display == "")
			{
				arguments.IsValid = false;
				sErrorText += (sErrorText == '') ? 'DU Password' : ', DU Password';
			}

			if(document.getElementById('<%= AspxTools.ClientId(m_doPasswordR) %>') && document.getElementById('<%= AspxTools.ClientId(m_doPasswordR) %>').style.display == "")
			{
				arguments.IsValid = false;
				sErrorText += (sErrorText == '') ? 'DO Password' : ', DO Password';
			}
		
            if(sErrorText != '')
				sErrorText = 'The following fields are required: <span style="FONT-WEIGHT: bold"> ' + sErrorText + '</span>';

			var OcSettingsMsg = ValidateOriginatingCompanySettings();
			if ( OcSettingsMsg != "OK")
			{
			    arguments.IsValid = false;
			    if ( sErrorText != '' )
			    {
			        sErrorText += "<BR/>";
			    }
			    sErrorText += OcSettingsMsg;
			}

			if (!document.getElementById('<%= AspxTools.ClientId(m_samePwValidator) %>' ).isvalid )
				sErrorText += ' Passwords do not match.';
			<% if ( ( m_isDOEnabled  || m_isDUEnabled )  && m_sPmlAutoLoginOption == "MustBeUnique") { %>
			    if ( document.getElementById( 'duerror' ) != null )
			    {
				    document.getElementById('duerror').style.display = passedDu ? 'none' : '';
			    }

			    if ( document.getElementById( 'doerror' ) != null )
			    {
				    document.getElementById('doerror').style.display = passedDo ? 'none' : '';
			    }


			    if ( !passedDu	|| !passedDo )
			    {
				    if ( sErrorText != '' )
				    {
					    sErrorText += "<br/>";
				    }
				    sErrorText += ' ';
				    var  needAnd = false;
				    if ( !passedDo )
				    {
					    sErrorText += 'DO';
					    needAnd = true;
				    }
				    if ( !passedDu )
				    {
					    if ( needAnd ) sErrorText += "/";
					    sErrorText += 'DU';
				    }

				    sErrorText += " login" + ( needAnd  && !passedDu ? "s are "  : " is " ) + "already saved in different account" + ( needAnd  && !passedDu ? "s"  : "" ) + "." + "&nbsp;<a class='handcursor' style='text-decoration: underline; ' onclick=\"Modal.Show('<%= AspxTools.ClientId(m_dudoSearch) %>' )\">Show users with same login</a>";

			    }
			<% } %>
			if (typeof AreLicensesValid === 'function'){
			    if (!AreLicensesValid()){
			        if (sErrorText.length != 0)
			            sErrorText += "<br/>";
			        sErrorText += "The following fields from the licenses tab are required: <b>License #</b>, <b>Expiration Date</b>.";
			    }
			}
			var RoleIsRequiredImg = document.getElementById('RoleIsRequiredImg');
			if( document.getElementById('<%= AspxTools.ClientId(m_IsLoanOfficer) %>').checked == false &&
			    document.getElementById('<%=AspxTools.ClientId(m_IsBrokerProcessor) %>').checked == false  &&
			    document.getElementById('<%=AspxTools.ClientId(m_IsExternalSecondary) %>').checked == false  &&
			    document.getElementById('<%=AspxTools.ClientId(m_IsExternalPostCloser) %>').checked == false  &&
			    document.getElementById('<%=AspxTools.ClientId(m_IsSupervisor) %>').checked == false )
			{
			    RoleIsRequiredImg.style.display = '';
			    if (sErrorText.length != 0)
			            sErrorText += "<br/>";
			    sErrorText +=  <%= AspxTools.JsString( LendersOffice.Common.ErrorMessages.PMLMissingRolesError) %>;
			    arguments.IsValid = false;
			}else {
                RoleIsRequiredImg.style.display = 'none';
			}

			document.getElementById("InvalidText").innerHTML = '<span style="COLOR: red">' + sErrorText + '</span>';
		}

		function updateDirtyBit_callback()
		{
			<%--
			// This functionality is based on the keyup event, which makes
			// it too sensitive.  In the future, we could filter out some
			// of the situations where the data would not change, such as
			// Control-C if someone wants to copy data with out marking as
			// dirty.  For now, we stay in sync with our system framework.
			--%>
			document.getElementById("<%= AspxTools.ClientId(m_ApplySeen) %>").disabled = false;
			document.getElementById('<%= AspxTools.ClientId(m_Dirty) %>').value = "1";
		}

		function isUserEditorDirty()
		{
			return document.getElementById('<%= AspxTools.ClientId(m_Dirty) %>').value == "1";
		}

		function calcNameOnLoanDoc()
        {
            var nameOnLDocCheckbox = document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDocLock) %>);
            if(!nameOnLDocCheckbox.checked)
            {
                var fName = document.getElementById(<%= AspxTools.JsGetClientIdString(m_FirstName) %>).value + " ";

                var mName = document.getElementById(<%= AspxTools.JsGetClientIdString(m_MiddleName) %>).value;
                if(mName != "")
                    mName += " ";

                var lName = document.getElementById(<%= AspxTools.JsGetClientIdString(m_LastName) %>).value;
                var suffix = document.getElementById(<%= AspxTools.JsGetClientIdString(m_Suffix) %>).value;
                if(suffix != "")
                    lName += ", ";

                var nameOnLDoc = fName+mName+lName+suffix;

                document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDoc) %>).value = nameOnLDoc;
            }
        }

        function lockNameOnLoanDoc(el)
        {
            var nameOnLoanDoc = document.getElementById(<%= AspxTools.JsGetClientIdString(m_NameOnLoanDoc) %>);
            nameOnLoanDoc.readOnly = !el.checked;

            if(!el.checked)
               calcNameOnLoanDoc();
        }

		function onTabClick( oTab )
		{
			try
			{
				if( oTab == null || oTab.id.indexOf( "Tab" ) == -1 )
				    return;

				var tabA = document.getElementById("TabA");
				var baseA = document.getElementById("BaseA");
				var tabB = document.getElementById("TabB");
				var baseB = document.getElementById("BaseB");
				var tabC = document.getElementById("TabC");
				var baseC = document.getElementById("BaseC");
				var tabD = document.getElementById("TabD");
				var baseD = document.getElementById("BaseD");
				var tabE = document.getElementById("TabE");
				var baseE = document.getElementById("BaseE");
				var tabF = document.getElementById("TabF");
				var baseF = document.getElementById("BaseF");
				var tabG = document.getElementById("TabG");
				var baseG = document.getElementById("BaseG");

				tabA.style.backgroundColor = "darkgray";
				tabA.style.color = "whitesmoke";
				tabA.style.borderBottom = "2px groove";
				baseA.style.display = "none";
				tabB.style.backgroundColor = "darkgray";
				tabB.style.color = "whitesmoke";
				tabB.style.borderBottom = "2px groove";
				baseB.style.display = "none";
				tabC.style.backgroundColor = "darkgray";
				tabC.style.color = "whitesmoke";
				tabC.style.borderBottom = "2px groove";
				baseC.style.display = "none";
				tabD.style.backgroundColor = "darkgray";
				tabD.style.color = "whitesmoke";
				tabD.style.borderBottom = "2px groove";
				baseD.style.display = "none";

				tabE.style.backgroundColor = "darkgray";
				tabE.style.color = "whitesmoke";
				tabE.style.borderBottom = "2px groove";
				baseE.style.display = 'none';
				tabF.style.backgroundColor = "darkgray";
				tabF.style.color = "whitesmoke";
				tabF.style.borderBottom = "2px groove";
				baseF.style.display = 'none';

	            tabG.style.backgroundColor = "darkgray";
				tabG.style.color = "whitesmoke";
				tabG.style.borderBottom = "2px groove";
				baseG.style.display = 'none';

				var tabH = document.getElementById('TabH');
				var baseH = document.getElementById("BaseH");
				if(tabH != null)
				{
				    tabH.style.backgroundColor = "darkgray";
				    tabH.style.color = "whitesmoke";
				    tabH.style.borderBottom = "2px groove";
				    baseH.style.display = 'none';
				}

				var tabI = document.getElementById('TabI');
				var baseI = document.getElementById("BaseI");
				tabI.style.backgroundColor = "darkgray";
				tabI.style.color = "whitesmoke";
				tabI.style.borderBottom = "2px groove";
				baseI.style.display = "none";

				var tabJ = document.getElementById('TabJ');
				var baseJ = document.getElementById("BaseJ");
				tabJ.style.backgroundColor = "darkgray";
				tabJ.style.color = "whitesmoke";
				tabJ.style.borderBottom = "2px groove";
				baseJ.style.display = "none";

				var tabS = document.getElementById('TabS');
				var baseS = document.getElementById('BaseS');
				tabS.style.backgroundColor = "darkgray";
				tabS.style.color = "whitesmoke";
				tabS.style.borderBottom = "2px groove";
				baseS.style.display = "none";

				oTab.style.borderBottom = "0px";
				oTab.style.backgroundColor = "gainsboro";
				oTab.style.color = "";
				switch( oTab.id )
				{
					case "TabA":
						Modal.Hide();
						baseA.style.display = "block";
						document.getElementById('<%= AspxTools.ClientId(m_FirstName) %>').focus();
						break;
					case "TabB":
						baseB.style.display = "block";
						var inactive = document.getElementById('<%= AspxTools.ClientId(m_Inactive) %>');
						var active = document.getElementById('<%= AspxTools.ClientId(m_Active) %>');
					    if(inactive.checked && !hasDisabledAttr(inactive))
							document.getElementById('<%= AspxTools.ClientId(m_Inactive) %>').focus();
					    else if(active.checked && !hasDisabledAttr(active))
							document.getElementById('<%= AspxTools.ClientId(m_Active) %>').focus();
						else
							document.getElementById('<%= AspxTools.ClientId(m_Login) %>').focus();
						break;
					case "TabC":
						baseC.style.display = "block";
						Modal.Hide();
						break;
					case "TabD":
						baseD.style.display = "block";
						Modal.Hide();
						break;
					case "TabE":
					    baseE.style.display = 'block';
						Modal.Hide();
					    break;
					case "TabF":
					    baseF.style.display = 'block';
						Modal.Hide();
					    break;
					case "TabG":
					    baseG.style.display = 'block';
						Modal.Hide();
					   break;
					case "TabH":
					    baseH.style.display = 'block';
						Modal.Hide();
						break;
					case "TabI":
						baseI.style.display = "block";
						Modal.Hide();
						break;
					case "TabJ":
						baseJ.style.display = "block";
						Modal.Hide();
						break;
				    case "TabS":
				        baseS.style.display = "block";
				        Modal.Hide();
				        break;
				}
				document.getElementById("<%= AspxTools.ClientId(m_CurrentTab)  %>").value = oTab.id;
                document.getElementById('m_MainDiv').style.height = 'auto'; //resize div to contents
			}
			catch( e )
			{
				alert(e);
				alert( e.message );
			}
		}

		function onChangeLoginClick()
		{
			var changeLogin = document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>');
			<%-- //new accounts do not have the changeLogin checkbox rendered --%>
			if(!changeLogin)
				return;
			var loginNotes = document.getElementById('<%= AspxTools.ClientId(m_LoginNotes) %>');
			var password = document.getElementById('<%= AspxTools.ClientId(m_Password) %>');
			var confirm = document.getElementById('<%= AspxTools.ClientId(m_Confirm) %>');
			var login = document.getElementById('<%= AspxTools.ClientId(m_Login) %>');
			var loginR = document.getElementById('<%= AspxTools.ClientId(m_loginR) %>');
			var passwordR = document.getElementById('<%= AspxTools.ClientId(m_passwordR) %>');
			var retypeR = document.getElementById('<%= AspxTools.ClientId(m_retypeR) %>');
			password.innerText = "";
			confirm.innerText  = "";

			if( changeLogin.checked == true )
			{
				if(loginNotes != null)
					loginNotes.style.display = "inline";

				document.getElementById('pwShowLink').style.visibility = "visible";
				login.readOnly    = false;
				password.readOnly = false;
				confirm.readOnly  = false;
			}
			else
			{
				if(loginNotes != null)
				{
					loginNotes.style.display = "none";
					changeLogin.style.display = "";
				}
				document.getElementById('pwShowLink').style.visibility = "hidden";
				loginR.style.display = "none";
				passwordR.style.display = "none";
				retypeR.style.display = "none";
				login.readOnly   = true;
				password.readOnly = true;
				confirm.readOnly  = true;
			}
		}


		function onUnlockAccountClick()
		{
			if(confirm("Unlock this user's account?"))
			{
				var args = new Object();
				args["login"] = document.getElementById('<%= AspxTools.ClientId(m_Login) %>').value;
				args["isPml"] = "true";
				args["id"] = <%= AspxTools.JsString(ViewState[ "employeeId" ].ToString()) %>;

				var result = gService.main.call("UnlockAccount", args);
				if(result.error)
					alert("Unable to unlock account");
				else
					document.getElementById('<%= AspxTools.ClientId(m_UnlockAccountPanel) %>').style.display = "none";
			}
		}

		<% if ( ( m_isDOEnabled  || m_isDUEnabled )  && m_sPmlAutoLoginOption == "MustBeUnique") { %>
		    var passedDo  = false ;
		    var passedDu = false;
		    function checkDODU()
		    {
			    var args = new Object();
			    args['id'		] = <%= AspxTools.JsString(ViewState[ "userid" ].ToString()) %>;
			    args['brokerId'	] = <%= AspxTools.JsString(ViewState[ "brokerId" ].ToString()) %>;
			    args['checkDo'	] = <%= AspxTools.JsBool(m_isDOEnabled) %> && document.getElementById('<%= AspxTools.ClientId(m_doLogin) %>') != null && document.getElementById('<%= AspxTools.ClientId(m_doLogin) %>').value.length != 0;
			    args['checkDu'	] = <%= AspxTools.JsBool(m_isDUEnabled) %> && document.getElementById('<%= AspxTools.ClientId(m_duLogin) %>') != null && document.getElementById('<%= AspxTools.ClientId(m_duLogin) %>').value.length != 0;
			    if ( <%= AspxTools.JsBool(m_isDUEnabled) %> ) {
				    args['dulogin'] = document.getElementById('<%= AspxTools.ClientId(m_duLogin) %>').value;
			    }
			    if ( <%= AspxTools.JsBool(m_isDOEnabled) %> ) {
				    args['dologin'] = document.getElementById('<%= AspxTools.ClientId(m_doLogin) %>').value;
			    }

			    if ( !args['checkDo'] && !args['checkDu'] )
			    {
				    passedDo = true;
				    passedDu = true;
				    return;
			    }

			    var result = gService.utilities.call( "CheckDODULoginFields", args );
			    if ( result.value && result.value["error"] )
			    {
				    alert(result.value["error"]);
				    passedDo = false;
				    passedDu = false;
				    return;
			    }
			    passedDo = result.value["IsDoUnique"] == "True";
			    passedDu = result.value["IsDuUnique"] == "True";

			    if ( !passedDo || !passedDu )
			    {
				    DUDO.Activate(!passedDo ? document.getElementById('<%= AspxTools.ClientId(m_doLogin) %>').value : '',!passedDu ? document.getElementById('<%= AspxTools.ClientId(m_duLogin) %>').value : '' );
			    }
		    }
		<% } %>

		function onStatusClick()
		{
			var inactiveStatus = document.getElementById('<%= AspxTools.ClientId(m_Inactive) %>');
			var activeStatus = document.getElementById('<%= AspxTools.ClientId(m_Active) %>');
			var changeLogin = document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>');
			if(!inactiveStatus || !activeStatus || !changeLogin)
				return;
			if(inactiveStatus.checked == true)
			{
				document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>').checked = false;
				onChangeLoginClick();
				document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>').disabled = true;
			}
			else
			{
				document.getElementById('<%= AspxTools.ClientId(m_ChangeLogin) %>').disabled = false;
			}
		}

		function disableChooser( oChooserPanel, status )
		{
		  setDisabledAttr(oChooserPanel, !status);
		  var Links = oChooserPanel.getElementsByTagName('A');
		  for (var i = 0; i < Links.length; i++) {
		      setDisabledAttr(Links[i], !status);
		  }
		}

		function OnKeyPress()
		{

		}

		function CheckEnableControls()
		{
		    var disabled = $("#m_ddlOrigiantorCompenationLevelT").val() == "0";

		    $("#m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo").prop("disabled",disabled);
		    $("#m_OriginatorCompensationPercent").prop("disabled",disabled).prop("readOnly",disabled);
		    $("#m_OriginatorCompensationBaseT").prop("disabled",disabled);
		    $("#m_OriginatorCompensationMinAmountEnabled").prop("disabled",disabled);

		    $("#m_OriginatorCompensationMinAmount").prop("readOnly",true);
			$("#m_OriginatorCompensationMinAmount").css({"background-color": "gainsboro"});
		    $("#m_OriginatorCompensationMaxAmountEnabled").prop("disabled",disabled);
		    $("#m_OriginatorCompensationMaxAmount").prop("readOnly",true);
			$("#m_OriginatorCompensationMaxAmount").css({"background-color": "gainsboro"});
		    $("#m_OriginatorCompensationFixedAmount").prop("disabled",disabled);
		}

		function OnCompensationLevelChange()
		{
		    $("#m_OriginatorCompensationBaseT").val('4');
		    $("#m_OriginatorCompensationMinAmountEnabled").prop("checked",false);
		    $("#m_OriginatorCompensationMaxAmountEnabled").prop("checked",false);

		    CheckEnableControls();


		    if($("#m_ddlOrigiantorCompenationLevelT").val() == "0")
		    {
		        PopulatePmlBrokerCompensation();
		    }
		    else
		    {
		         $("#m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo").prop("checked",false);
		         $("#m_OriginatorCompensationPercent").val('');
		         $("#m_OriginatorCompensationMinAmount").val('');
		         $("#m_OriginatorCompensationMaxAmount").val('');
		         $("#m_OriginatorCompensationFixedAmount").val('');
		    }
		}

		function PopulatePmlBrokerCompensation()
		{
		    if($("#m_ddlOrigiantorCompenationLevelT").val() !== "0" ||
                document.getElementById(<%= AspxTools.JsString(m_CompanyChoice.DataID) %>).value === <%=AspxTools.JsString(Guid.Empty)%>)
		    {
                return;
		    }

		    var pmlBrokerID = GetUpdatedOrignatingId();
		    var params = { 'pmlBrokerID': pmlBrokerID };
            var strParams = JSON.stringify(params);

		    callWebMethodAsync({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'EditPmlUserService.aspx/GetPMLCompanyOriginatorCompensationData',
            data: strParams,
            dataType: 'json',
            async: false,
            success: function(msg)
            {
                if(msg.hasOwnProperty('d') && msg.d.length > 0)
                {
                    var arrVals = msg.d.split(',');

                    $("#m_OriginatorCompensationPercent").val(arrVals[0]+'%');
	                $("#m_OriginatorCompensationBaseT").val(arrVals[1]);
	                $("#m_OriginatorCompensationMinAmount").val('$' + arrVals[2]);
	                $("#m_OriginatorCompensationMinAmountEnabled").prop("checked",arrVals[2] > 0);
		            $("#m_OriginatorCompensationMaxAmount").val('$' + arrVals[3]);
		            $("#m_OriginatorCompensationMaxAmountEnabled").prop("checked",arrVals[3] > 0);
		            $("#m_OriginatorCompensationFixedAmount").val('$' + arrVals[4]);
		            $("#m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo").prop("checked",arrVals[5] == '1');
                }
            },
            failure: function(response) { alert("error - " + response); ret = false;},
            error: function(xhr, textStatus, errorThrown) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message); ret = false;
              }
            });
		}

		function CheckEnableCustomPricingPolicyControls()
		{
		    var disabled = $("#PricingPolicyFieldValueSource").val() === "0";

		    $("input.CustomPMLField").prop("disabled",disabled).prop("readOnly",disabled);
		    $("select.CustomPMLField").prop("disabled",disabled);
		}

		function OnPricingPolicyLevelChange()
		{
		    CheckEnableCustomPricingPolicyControls();

		    if($("#PricingPolicyFieldValueSource").val() == "0")
		    {
		        PopulatePmlBrokerCustomPricingPolicy();
		    }
		    else
		    {
		        // Clear all of the field values.
		        $('.CustomPMLField').val('');
		    }
		}

		function PopulatePmlBrokerCustomPricingPolicy()
		{
		    if($("#PricingPolicyFieldValueSource").val() !== "0" ||
                document.getElementById(<%= AspxTools.JsString(m_CompanyChoice.DataID) %>).value === <%=AspxTools.JsString(Guid.Empty)%>)
		    {
                return;
		    }

		    var pmlBrokerID = GetUpdatedOrignatingId();
		    var params = { 'pmlBrokerID': pmlBrokerID };
            var strParams = JSON.stringify(params);

		    callWebMethodAsync({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: 'EditPmlUserService.aspx/GetPMLCompanyCustomPricingPolicyData',
            data: strParams,
            dataType: 'json',
            async: false,
            success: function(msg)
            {
                if(msg.hasOwnProperty('d'))
                {
                    $('.CustomPricingPolicyField1').val(msg.d['CustomPricingPolicyField1']);
                    $('.CustomPricingPolicyField2').val(msg.d['CustomPricingPolicyField2']);
                    $('.CustomPricingPolicyField3').val(msg.d['CustomPricingPolicyField3']);
                    $('.CustomPricingPolicyField4').val(msg.d['CustomPricingPolicyField4']);
                    $('.CustomPricingPolicyField5').val(msg.d['CustomPricingPolicyField5']);
                }
            },
            failure: function(response) { alert("error - " + response); ret = false;},
            error: function(xhr, textStatus, errorThrown) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message); ret = false;
              }
            });
		}
		// -->

        </script>
        <h4 class="page-header"><ml:EncodedLabel id=m_TitleBar Runat="server">Add New Originating Company User</ml:EncodedLabel></h4>
		<form id="EditPmlUser" method="post" runat="server">
			<asp:HiddenField runat="server" ID="HaveShownOcError" Value="false" />
			<asp:HiddenField runat="server" ID="CustomLOPPFieldSettings" />
            <asp:HiddenField runat="server" ID="IsSupervisorOnLoad" />
			<asp:Panel id=m_AccessDeniedPanel style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM:30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
					Access Denied.  You do not have permission to administrate PML users.
			</asp:Panel>
			<asp:Panel id="m_AccessAllowedPanel" runat="server" style="width: auto;">
			    <div style="BORDER: 2px outset;  BACKGROUND-COLOR: gainsboro">
				    <table cellSpacing=8 cellPadding=0 border=0 width=100% style="PADDING-RIGHT: 4px;PADDING-LEFT: 4px; PADDING-BOTTOM: 0px; PADDING-TOP: 4px">
					    <tr>
						    <td>
							    <TABLE cellSpacing=0 cellPadding=0>
								    <TR>
									    <TD class="FieldLabel tabBorder" id="TabA" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">User Information </TD>
									    <TD style="BORDER-BOTTOM: 2px groove">&nbsp;</TD>
							            <TD class="FieldLabel tabBorder" id="TabF" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">Originator Compensation</TD>
							            <TD style="BORDER-BOTTOM: 2px groove">&nbsp;</TD>
									    <TD class="FieldLabel tabBorder" id="TabB" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">Credentials </TD>
									    <TD style="BORDER-BOTTOM: 2px groove">&nbsp; </TD>
                                        <TD class="FieldLabel tabBorder" id="TabS" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">Services </TD>
									    <TD style="BORDER-BOTTOM: 2px groove">&nbsp; </TD>
									    <TD class="FieldLabel tabBorder" id="TabG" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">Roles </TD>
									    <TD style="BORDER-BOTTOM: 2px groove">&nbsp; </TD>
									    <TD class="FieldLabel tabBorder" id="TabC" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">Broker Relationships </TD>
									    <TD style="BORDER-BOTTOM: 2px groove">&nbsp; </TD>
									    <TD class="FieldLabel tabBorder" id="TabI" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">Mini-Correspondent Relationships</TD>
									    <TD style="BORDER-BOTTOM: 2px groove">&nbsp; </TD>
									    <TD class="FieldLabel tabBorder" id="TabJ" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">Correspondent Relationships</TD>
									    <TD style="BORDER-BOTTOM: 2px groove">&nbsp; </TD>
									    <TD class="FieldLabel tabBorder" id="TabE" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">Permissions </TD>
									    <TD style="BORDER-BOTTOM: 2px groove">&nbsp; </TD>
									    <TD class="FieldLabel tabBorder" id="TabD" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">Loan Officer Licenses </TD>
									    <TD style="BORDER-BOTTOM: 2px groove">&nbsp; </TD>
									    <TD class="FieldLabel tabBorder" id="TabH" runat="server" onkeypress="if(event.keyCode == 32 || event.keyCode == 13 || event.keyCode == 9) { this.click(); return false; }" onclick="onTabClick( this );">System Access </TD>
									    <td style="BORDER-BOTTOM: 2px groove" width="100%" >&nbsp; </td>
								    </TR>
							    </TABLE>
							    <div id="m_MainDiv" style="BORDER-RIGHT: 2px groove; PADDING-LEFT: 8px; PADDING-RIGHT: 8px;BORDER-TOP: 0px; PADDING-BOTTOM: 8px; BORDER-LEFT: 2px groove; PADDING-TOP: 8px; BORDER-BOTTOM: 2px groove; HEIGHT: 485px; BACKGROUND-COLOR: gainsboro">
								    <DIV id=BaseA style="DISPLAY: none; height:485px;">
									    <TABLE style="PADDING-BOTTOM: 3px" cellSpacing=0 cellPadding=0>
										    <TR vAlign=top>
											    <TD noWrap>
												    <DIV class=FieldLabel style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px; PADDING-BOTTOM: 4px; PADDING-TOP: 0px">OC User Details &nbsp;&nbsp;&nbsp;</DIV>
												    <DIV style="PADDING-RIGHT: 6px; BORDER-TOP: lightgrey 2px solid; PADDING-LEFT: 6px; PADDING-BOTTOM: 0px; WIDTH: 600px; PADDING-TOP: 6px; HEIGHT: 10px">
													    <TABLE cellSpacing=1 cellPadding=0>
                                <tr>
                                  <td nowrap>Company</td>
                                  <td nowrap>
                                    <ils:employeerolechooserlink id="m_CompanyChoice" GenerateScripts="true" runat="server" Width="175px" Mode="Search" Desc="Originating Company" Role="Originating Company"
                                        Text="None" Data="00000000-0000-0000-0000-000000000000">
										<FIXEDCHOICE Text="None" Data="00000000-0000-0000-0000-000000000000">None</FIXEDCHOICE>
										<FIXEDCHOICE Text="assign" Data="assign">Pick Company</FIXEDCHOICE>
									</ils:employeerolechooserlink>
                                    <img src="../../images/require_icon.gif">
                                  </td>
                                </tr>
                                <tr>
                                  <td width="100px" nowrap>First Name </td>
                                  <td nowrap>
                                    <asp:TextBox onchange="calcNameOnLoanDoc();" ID="m_FirstName" Style="padding-left: 4px" TabIndex="5" runat="server" Width="180" MaxLength="40" />
                                    <img src="../../images/require_icon.gif"> &nbsp;
                                    <asp:RequiredFieldValidator ID="m_R1" runat="server" Display="Static" ControlToValidate="m_FirstName" ErrorMessage="*" EnableViewState="False"></asp:RequiredFieldValidator>
                                  </td>
                                </tr>
                                <tr>
                                    <td noWrap>Middle Name</td>
                                    <td nowrap>
                                        <asp:TextBox onchange="calcNameOnLoanDoc();" TabIndex="6" runat="server" ID="m_MiddleName" MaxLength="21" style="padding-left: 4px" Width="180"></asp:TextBox>
                                    </td>
                                </tr>
							    <TR>
								    <TD noWrap>Last Name </TD>
								    <TD noWrap> <asp:textbox id=m_LastName onchange="calcNameOnLoanDoc();" style="PADDING-LEFT: 4px" tabIndex=10 runat="server" Width="180" MaxLength="40"></asp:textbox>
									    <IMG src="../../images/require_icon.gif"> &nbsp;
									    <asp:requiredfieldvalidator id=m_R2 runat="server" Display="Dynamic" ControlToValidate="m_LastName" ErrorMessage="*" EnableViewState="False"></asp:requiredfieldvalidator>
								    </TD>
							    </TR>
							    <tr>
							        <td nowrap>Suffix</td>
							        <td nowrap>
							            <ml:ComboBox onchange="calcNameOnLoanDoc();" tabindex=11 runat="server" ID="m_Suffix" MaxLength="10" Width=50 ></ml:ComboBox>
							        </td>
							    </tr>
							    <tr>
							        <td nowrap style="width: 150px">Name on Loan Documents</td>
							        <td nowrap>
							            <asp:TextBox TabIndex=12 runat="server" ID="m_NameOnLoanDoc" MaxLength="100" Width=250></asp:TextBox>
							            <asp:CheckBox onclick="lockNameOnLoanDoc(this);" TabIndex=13 runat="server" ID="m_NameOnLoanDocLock" Text="Lock" />
							        </td>
							    </tr>
							    <TR style="PADDING-TOP: 12px">
								    <TD class=FieldLabel style="BORDER-BOTTOM: lightgrey 2px solid; PADDING-BOTTOM:4px;" width="100%" vAlign=top colspan=2 >Personal Contact Info </TD>
							    </TR>
                                <tr style="padding-top: 6px">
                                  <td>Mailing Address</td>
                                  <td>
                                    <asp:TextBox ID="m_Address" Style="padding-left: 4px" TabIndex="25" runat="server" Width="222px" MaxLength="60" />
                                  </td>
                                </tr>
                                <tr>
                                  <td>City </td>
                                  <td>
                                    <asp:TextBox ID="m_City" Style="padding-left: 4px" TabIndex="30" runat="server" Width="120px" />
                                    <uc:StateDropDownList ID="m_State" TabIndex="35" runat="server" />
                                    <uc:ZipcodeTextBox ID="m_Zipcode" Style="padding-left: 4px" TabIndex="40" runat="server" Width="50px" preset="zipcode" CssClass="mask" />
                                  </td>
                                </tr>

														    <TR>
															    <TD >Phone </TD>
															    <TD>
																    <uc:phonetextbox id=m_Phone style="PADDING-LEFT: 4px" tabIndex=50 runat="server" Width="120px" preset="phone" CssClass="mask"></uc:phonetextbox>
																    <IMG src="../../images/require_icon.gif"> &nbsp;
																    <asp:requiredfieldvalidator id=m_R3 runat="server" Display="Dynamic" ControlToValidate="m_Phone" ErrorMessage="*" EnableViewState="False"></asp:requiredfieldvalidator>
															    </TD>
														    </TR>
														    <TR>
															    <TD >Fax </TD>
															    <TD>
																    <uc:phonetextbox id=m_Fax style="PADDING-LEFT: 4px" tabIndex=55 runat="server" Width="120px" preset="phone" CssClass="mask"></uc:phonetextbox>
															    </TD>
														    </TR>
														    <TR>
															    <TD >Cell Phone </TD>
															    <TD>
																    <uc:phonetextbox id="m_Cell" style="PADDING-LEFT: 4px" tabIndex=60 runat="server" Width="120px" preset="phone" CssClass="mask"></uc:phonetextbox>
																    <img src="../../images/require_icon.gif" id="CellPhoneReqImg" runat="server"> &nbsp;
                                                                    <asp:CheckBox runat="server" ID="IsCellphoneForMultiFactorOnly" Text="Private: For multi-factor authentication only" />
                                                                    <asp:RequiredFieldValidator ID="CellPhoneValidator" runat="server" Display="Static" ControlToValidate="m_Cell" ErrorMessage="*" EnableViewState="False"></asp:RequiredFieldValidator>
                                                                    <asp:HiddenField ID="IsEnableMultiFactorAuthenticationTPO" runat="server" />
															    </TD>
														    </TR>
														    <TR>
															    <TD >Pager </TD>
															    <TD>
																    <uc:phonetextbox id="m_Pager" style="PADDING-LEFT: 4px" tabIndex=65 runat="server" Width="120px" preset="phone" CssClass="mask"></uc:phonetextbox>
															    </TD>
														    </TR>
														    <TR>
															    <TD >Email </TD>
															    <TD>
																    <asp:textbox id=m_Email style="PADDING-LEFT: 4px" tabIndex=70 runat="server" Width="180px" MaxLength="80"></asp:textbox>
																    <IMG src="../../images/require_icon.gif"> &nbsp;
																    <asp:requiredfieldvalidator id=m_R4 runat="server" Display="Dynamic" ControlToValidate="m_Email" ErrorMessage="*" EnableViewState="False"></asp:requiredfieldvalidator>
																    <asp:regularexpressionvalidator id=RegularExpressionValidator1 runat="server" Display="Static" ControlToValidate="m_Email" ErrorMessage="*"></asp:regularexpressionvalidator>
															    </TD>
														    </TR>
														    <TR id="PerformanceAccountHeader" style="PADDING-TOP: 12px; display:none" >
															    <TD class=FieldLabel style="BORDER-BOTTOM: lightgrey 2px solid; PADDING-BOTTOM:4px;" width="100%" vAlign=top colspan=2 >Performance Account</TD>
														    </TR>
														    <tr id="PerformanceAccountTable" style="display:none">
										                        <td colspan="2">
											                        <table>
												                        <tr>
													                        <td style="WIDTH: 187px">
														                        <ml:EncodedLabel id=ptLoanAmtLabel text="Percentage of Loan Amount" Runat="server"></ml:EncodedLabel>&nbsp;&nbsp;
													                        </td>
													                        <td>
														                        <ml:percenttextbox id=m_CommissionPointOfLoanAmount runat="server" CssClass="mask" preset="percent" width="90px"></ml:percenttextbox>
													                        </td>
												                        </tr>
												                        <tr>
													                        <td style="WIDTH: 187px">
														                        <ml:EncodedLabel id=ptGrossProfitLabel text="Percentage of Gross Profit" Runat="server"></ml:EncodedLabel>&nbsp;&nbsp;
													                        </td>
													                        <td>
														                        <ml:percenttextbox id=m_CommissionPointOfGrossProfit runat="server" CssClass="mask" preset="percent" width="90px"></ml:percenttextbox>
													                        </td>
												                        </tr>
												                        <tr>
													                        <td style="WIDTH: 97px">
														                        <ml:EncodedLabel id=minBaseLabel text="Minimum Base" Runat="server"></ml:EncodedLabel>&nbsp;&nbsp;
													                        </td>
													                        <td>
														                        <ml:moneytextbox id=m_CommissionMinBase runat="server" CssClass="mask" preset="money" width="90px"></ml:moneytextbox>
													                        </td>
												                        </tr>
												                        <tr colspan="2">
													                        <td style="WIDTH: 97px" colSpan=2>
													                        </td>
												                        </tr>
											                        </TABLE>
										                        </td>
										                    </tr>

														    <TR style="PADDING-TOP: 2px">
															    <TD width="100%" vAlign=top colspan=2 >Notes </TD>
														    </TR>
														    <TR style="PADDING-TOP: 2px">
															    <TD colspan=2>
																    <asp:textbox id=m_Notes tabIndex=71 style="PADDING-LEFT: 4px;" runat="server" Width="350px" TextMode="MultiLine" Height="80px"> </asp:textbox>
															    </TD>
														    </TR>

													    </TABLE>
												    </DIV>
											    </TD>
											    <td width=40px>

											    </td>
											    <td></td>
											    </TR>
										    </TABLE>
								    </DIV>
				                    <div id="BaseF" class="tabDiv" style="DISPLAY: none">
				                        <table cellSpacing=0 cellPadding=0 style="PADDING-BOTTOM:0px">
				                            <tr>
				                            <td colspan="2">
				                                The compensation plan is set at the
				                                    <asp:DropDownList id="m_ddlOrigiantorCompenationLevelT" runat="server"></asp:DropDownList>
				                                level
				                            </td>
				                            </tr>
				                            <tr><td>&nbsp;</td></tr>
                                            <tr style="padding-bottom:4px">
				                                <td>
                                                    <asp:CheckBox id="m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo" runat="server" Text="Compensation only paid for the 1st lien of a combo" />
				                                </td>
				                            </tr>

				                            <tr style="padding-bottom:4px">
				                                <td>
                                                    <ml:percenttextbox id="m_OriginatorCompensationPercent" runat="server" width="70" preset="percent"></ml:percenttextbox>
				                                    of the
				                                    <asp:DropDownList id="m_OriginatorCompensationBaseT" runat="server"></asp:DropDownList>
				                                </td>
				                            </tr>
				                            <tr style="padding-bottom:4px">
				                                <td style="padding-left:50px">
				                                    <asp:CheckBox ID="m_OriginatorCompensationMinAmountEnabled" runat="server" onclick="setAmountTextboxStatus(this);" />
				                                    Minimum
				                                    <ml:moneytextbox id="m_OriginatorCompensationMinAmount" runat="server" width="90" preset="money" style="position: absolute; left: 160px;"></ml:moneytextbox>
				                                    <asp:Image ID="m_OriginatorCompensationMinAmountR" runat="server" ImageUrl="~/images/require_icon.gif" style="position: absolute; left: 255px;" />
						                        </td>
				                            </tr>
				                            <tr style="padding-bottom:4px">
				                                <td style="padding-left:50px">
				                                    <asp:CheckBox ID="m_OriginatorCompensationMaxAmountEnabled" runat="server" onclick="setAmountTextboxStatus(this);" />
				                                    Maximum
				                                    <ml:moneytextbox id="m_OriginatorCompensationMaxAmount" runat="server" width="90" preset="money" style="position: absolute; left: 160px;"></ml:moneytextbox>
				                                    <asp:Image ID="m_OriginatorCompensationMaxAmountR" runat="server" ImageUrl="~/images/require_icon.gif" style="position: absolute; left: 255px;" />
						                        </td>
				                            </tr>
				                            <tr>
				                                <td>
				                                    Fixed amount
                                                    <ml:MoneyTextBox ID="m_OriginatorCompensationFixedAmount" runat="server" preset="money" Width="90" style="position: absolute; left: 260px;"></ml:MoneyTextBox>
				                                </td>
				                            </tr>
				                            <tr>
				                                <td style="padding-top:15px">
				                                    Notes
				                                </td>
				                            </tr>
				                            <tr>
				                                <td>
				                                    <asp:TextBox ID="m_OriginatorCompensationNotes" TextMode="MultiLine" Rows="5" Width="350px" runat="server"></asp:TextBox>
				                                </td>
				                            </tr>
				                            <tr>
				                                <td style="padding-top:15px">
				                                    Compensation plan change history
				                                </td>
				                            </tr>
				                            <tr>
				                                <td>
			            	                        <ml:CommonDataGrid id="m_OriginatorCompensationAuditGrid" runat="server" AllowPaging="true" OnPageIndexChanged="m_OriginatorCompensationAuditGrid_PageIndexChanged" OnItemCreated="m_OriginatorCompensationAuditGrid_ItemCreated" EnableViewState="true" CellPadding="3">
								                        <PagerStyle VerticalAlign="Middle" Mode="NumericPages" horizontalalign="right" Position="Bottom"></PagerStyle>
							                            <alternatingitemstyle cssclass="GridAlternatingItem"></alternatingitemstyle>
								                        <itemstyle cssclass="GridItem"></itemstyle>
								                        <headerstyle cssclass="GridHeader"></headerstyle>
							                            <columns>
							                                <asp:BoundColumn DataField="Date_rep" HeaderText="Date"></asp:BoundColumn>
							                                <asp:BoundColumn DataField="By_rep" HeaderText="By"></asp:BoundColumn>
							                                <asp:BoundColumn DataField="Notes" HeaderText="Notes"></asp:BoundColumn>
							                                <asp:TemplateColumn>
										                        <itemtemplate>
											                        <a href="#" onclick="showModal('/los/admin/ViewOriginatorCompensationAuditEvent.aspx?employeeId=<%# AspxTools.HtmlString(m_EmployeeId.ToString()) %>&brokerId=<%# AspxTools.HtmlString(BrokerId.ToString()) %>&eventId=<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Id").ToString())%>');">view</a>
										                        </itemtemplate>
									                        </asp:TemplateColumn>
							                            </columns>
							                        </ml:CommonDataGrid>
				                                	<ml:EncodedLiteral ID="m_OriginatorCompensationAuditDoesntExist" Text="No entries in change history" runat="server"></ml:EncodedLiteral>
				                                </td>
				                            </tr>
				                        </table>
				                        <asp:PlaceHolder runat="server" ID="CustomLOPricingPolicyPlaceHolder">
                            				<div class="FieldLabel heading">Loan Officer Pricing Policy</div>
				                            <div>The loan officer pricing policy is set at the <asp:DropDownList runat="server" ID="PricingPolicyFieldValueSource"></asp:DropDownList> level.</div>
				                            <table runat="server" id="CustomLOPricingPolicyTable" class="CustomLOPricingPolicyTable">
				                            </table>
				                        </asp:PlaceHolder>
				                    </div>
				                    <div id="BaseG" class="tabDiv" style="display: none">
				                            <table style="width: 650px">
				                                                                 <tr>
                                                <td class="FieldLabel newheader">
                                                    Roles <img style="display:none" id="RoleIsRequiredImg" src="../../images/error_icon.gif"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="m_IsLoanOfficer" Text="Loan Officer" onclick="ValidateRoles()" /> <br />
                                                    <asp:CheckBox runat="server" ID="m_IsBrokerProcessor" Text="Processor" onclick="ValidateRoles()" />  <br />
                                                    <asp:CheckBox runat="server" ID="m_IsExternalSecondary" Text="Secondary" onclick="ValidateRoles()" />  <br />
                                                    <asp:CheckBox runat="server" ID="m_IsExternalPostCloser" Text="Post-Closer" onclick="ValidateRoles()" />  <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID="m_IsSupervisor" Text="Supervisor" onclick="SetUIStatus();ResetSupervisor();" /><br />
                                                    <label style="padding-left:20px">Supervised by:</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td id="SupervisorChoicePanel" style="padding-left:20px">
                                                    <ils:employeerolechooserlink id="m_SupervisorChoice" runat="server" Width="160px" Text="None" Mode="Search" Desc="Supervisor" Role="Supervisor" Data="00000000-0000-0000-0000-000000000000">
												        <FIXEDCHOICE Text="None" Data="00000000-0000-0000-0000-000000000000">None</FIXEDCHOICE>
												        <FIXEDCHOICE Text="assign" Data="assign">Pick Supervisor</FIXEDCHOICE>
											        </ils:employeerolechooserlink>
                                                </td>
                                            </tr>
				                            </table>
				                    </div>
				                    <div id="BaseH" style="display:none;">
				                        <table style="width:100%;">
				                            <tr>
				                                <td>
				                                    <div id="columnB" style="float:left;">
											    <div class="FieldLabel heading" >
												    IP Address Restriction
											    </div>
											    <div >
								        <asp:RadioButton runat="server" ID="m_ipRestrictionOff" GroupName="ipRestriction" Text="No IP address restriction" onclick="IP.Filter(true);" /><br />
								        <asp:RadioButton runat="server" ID="m_ipRestrictionOn" GroupName="ipRestriction" Text="Only allow IPs listed below:" onclick="IP.Filter(false);" /><br />
									    <input type="checkbox" checked="checked" disabled="disabled" style="margin-left:15px" />IPs on global whitelist

											    </div>

											    <div class="padleft2 left">
												    <input  tabindex="173"  type="text" class="ipinput" maxlength="15"  onkeydown="return IP.CheckInputKey(event)" name="ip_input" id="ip_input"/> <br/>
												    <asp:ListBox   tabindex="175"  Runat="server" ID="m_ip" Rows="5"  CssClass="ipinput" EnableViewState="False"  SelectionMode="Multiple"></asp:ListBox>
												    <input type="hidden" style="display:none" runat="server" id="m_ip_parsed" name="m_ip_parsed" />
											    </div>
											    <div  class="padleft1 left">

												    <input  tabindex="174"  type="button" id="ip_add" name="ip_add" value="Add" onclick="IP.Add('ip_input')"> <br/>
												    <input   tabindex="176"  type="button" style="margin-top: 5px" id="ip_remove" name="ip_remove" value="Remove"  onclick="IP.Remove()"/>
											    </div>
										    </div> <!-- End ColumnB -->
				                                </td>
				                                <td valign="top">
				                               <div style="margin-left:25px;" id="MultiFactorSection" runat="server">

							                    <div style="font-weight:normal" >
							                    <div class="column1" id="MultiFactorSectionHeader" runat="server">
                                                    <asp:HiddenField runat="server" id="IsDisablingAuthenticator" />
						                            <div class="FieldLabel heading">Additional Authentication</div>
                                                    <input type="checkbox" ID="m_EnabledMultiFactorAuthentication" runat="server" /> <label for="m_EnabledMultiFactorAuthentication">Enable multi-factor auth for this user.</label><br />
							                        <asp:CheckBox ID="m_EnabledClientDigitalCertificateInstall" runat="server" Text="Allow user to install digital certificate." /><br />
                                                    <asp:CheckBox ID="EnableAuthCodeViaSms" runat="server" Text="Enable auth code transmission via SMS text" /> <br />
                                                    <input type="checkbox" ID="EnableAuthCodeViaAuthenticator" runat="server" />
                                                    <label for="EnableAuthCodeViaAuthenticator">Enable auth code transmission via Authenticator.</label>
                                                    <input type="button" runat="server" value="Disable Authenticator" id="DisableAuthenticator" /> <br />
							                    </div>
							                    </div>
							                </div>
				                                </td>
				                            </tr>

				                            <tr>
				                                <td colspan="2">
				                                    <div style="margin-top:20px;" id="_IPRestrictions" runat="server">
						                                <span style="font-weight:bold">Client Certificate</span>
			                                <ml:CommonDataGrid ID="m_clientCertificateGrid" runat="server">
			                                    <Columns>
			                                        <asp:BoundColumn DataField="Description" HeaderText="Device Name"/>
			                                        <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date"/>
			                                        <asp:TemplateColumn>
			                                            <ItemTemplate>
			                                            <a href="#" onclick="return revokeClientCertificate('<%# AspxTools.HtmlString(Eval("CertificateId")) %>');">revoke</a>
			                                            </ItemTemplate>
			                                        </asp:TemplateColumn>
			                                    </Columns>
			                                </ml:CommonDataGrid>
			                                <hr />
			                                <div runat="server" id="IPAccessContainer">
			                                    <span style="font-weight:bold">IP Access</span>
			                                    <ml:CommonDataGrid ID="m_registeredIpDataGrid" runat="server">
			                                        <Columns>
			                                            <asp:BoundColumn DataField="IpAddress" HeaderText="IP Address" />
			                                            <asp:BoundColumn DataField="IsRegistered" HeaderText="Registered IP" />
			                                            <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" />
			                                            <asp:TemplateColumn>
			                                                <ItemTemplate>
			                                                <a href="#" onclick="return deleteIp('<%# AspxTools.HtmlString(Eval("Id")) %>');">remove</a>
			                                                </ItemTemplate>
			                                            </asp:TemplateColumn>
			                                        </Columns>
			                                    </ml:CommonDataGrid>
			                                    <hr />
			                                </div>

			                                </div>
				                                </td>
				                            </tr>
				                        </table>
				                    </div>

								    <DIV id=BaseB style="DISPLAY: none">
								    <div id="columnA">
										    <div class="FieldLabel heading " >
											    Status
										    </div>
										    <div class="border padleft1" >
												    <asp:radiobutton id=m_Inactive tabIndex=115 runat="server" Text="Inactive" GroupName="Status" onclick="onStatusClick();"></asp:radiobutton>
												    <asp:radiobutton id=m_Active tabIndex=120 runat="server" Text="Active" GroupName="Status" Checked="True" onclick="onStatusClick();"></asp:radiobutton>
										    </div>
										    <div class="FieldLabel heading">
											    Login Info
										    </div>

										    <asp:Panel ID="m_UnlockAccountPanel"  CssClass="padleft1" Runat=server>
												    <b><ml:EncodedLabel id="m_LockedAccount" runat="server" ForeColor="Red" Text="This account is locked"></ml:EncodedLabel></b>&nbsp;&nbsp;
												    <input id=m_unlockAccount style="WIDTH: 100px" onclick=onUnlockAccountClick(); type=button value="Unlock">
										    </asp:Panel>

										    <div class="padleft1 padbottom" >
												    <asp:checkbox id=m_ChangeLogin tabIndex=125 runat="server" Text="Change login and password?" onclick=onChangeLoginClick();></asp:checkbox>
												    <% if ( AspxTools.JsBool(m_isNew) == "false" ) { %>
												    <asp:panel id=m_LoginNotes style="display:none;  FONT: 11px arial; COLOR: dimgray;  TEXT-ALIGN: left;" runat="server">
													    (Leave password blank if it is not being changed.)
												    </asp:panel>
												    <% } %>
											    </div>

											    <div  class="padleft2"> <span style="width: 130px"> Login Name </span>
												    <asp:textbox id=m_Login style="PADDING-LEFT: 4px" tabIndex=130 runat="server" MaxLength="36"></asp:textbox>
												    <asp:Image id=m_loginR style="DISPLAY: none" runat="server" ImageUrl="../../images/require_icon.gif"></asp:Image>
											    </div>

											    <div  class="padleft2" >
												    <span style="width: 130px">
												        Password
												        <span  style="font-size: .8em"  id="pwShowLink">
												            (<A id='m_showPwRules' tabIndex=126 onclick="Modal.ShowPopup('m_PasswordRules', 'm_closePwRules', event);" href='#"' ?>show rules</A>)
												        </span>
												    </span>
												    <asp:textbox id="m_Password" onkeyup="onPwTextChange();" style="PADDING-LEFT: 4px" tabIndex="135" runat="server" MaxLength="36" TextMode="Password"></asp:textbox>
												    <asp:Image id="m_passwordR" style="DISPLAY: none" runat="server" ImageUrl="../../images/require_icon.gif"></asp:Image>
											    </div>

											    <div  class="padleft2 padbottom">
												    <span style="width: 130px" > Retype </span>
												    <asp:textbox id=m_Confirm onkeyup=onPwTextChange(); style="PADDING-LEFT: 4px" tabIndex=140 runat="server" MaxLength="36" TextMode="Password"></asp:textbox>
												    <asp:Image id=m_retypeR style="display: none" runat="server" ImageUrl="../../images/require_icon.gif"></asp:Image>
												    <asp:CompareValidator ID="m_samePwValidator" Runat="server" ControlToCompare="m_Password" ControlToValidate="m_Confirm" ErrorMessage="*"></asp:CompareValidator>
											    </div>

											    <div class="padleft1 padbottom" >
												    <asp:radiobutton tabIndex=145 id=m_MustChangePasswordAtNextLogin onclick=expirationPolicyClick(); runat="server" Text="Must change password at next login" GroupName="Password"></asp:radiobutton><BR/>
												    <asp:radiobutton id=m_PasswordNeverExpires tabIndex=150 onclick=expirationPolicyClick(); runat="server" Text="Password never expires" GroupName="Password"></asp:radiobutton><BR/>
												    <asp:radiobutton id=m_PasswordExpiresOn tabIndex=155 onclick=expirationPolicyClick(); runat="server" Text="Password expires on" GroupName="Password"></asp:radiobutton>&nbsp;
												    <ml:datetextbox id=m_ExpirationDate tabIndex=160 style="PADDING-LEFT: 4px;" runat="server" preset="date" HelperAlign="AbsMiddle" width="64"></ml:datetextbox>
											    </div>
											    <div class="padleft1">
												    <asp:checkbox id=m_CycleExpiration tabIndex=165 onclick=expirationCycleClick(); runat="server" CssClass="FieldLabel" Text=""></asp:checkbox>
												    Expire passwords every
													    <asp:dropdownlist tabIndex=170 id=m_ExpirationPeriod disabled runat="server">
														    <asp:ListItem Value="15">15</asp:ListItem>
														    <asp:ListItem Value="30">30</asp:ListItem>
														    <asp:ListItem Value="45">45</asp:ListItem>
														    <asp:ListItem Value="60">60</asp:ListItem>
													    </asp:dropdownlist>&nbsp;days following update
											    </div>
										    </div> <!-- End columnA -->

									    <div id="loginProcess"  style="z-index: 900" class="modalmsg" style="width:300px">
										    During the automatic login process, it would technically be possible for a malicious user to employ 3rd-party web tools to attempt to obtain the login info while it is invisibly sent to DO/DU.
										    <div style="text-align:center"> [<a href="#" id='wmsgClose' onclick="Modal.Hide()"> Close </a>]</div>
									    </div>
									    <% if ( AspxTools.JsBool(m_isDUEnabled) == "true" || AspxTools.JsBool(m_isDOEnabled) == "true" ) { %>
									    <div id="footer" class="clear">
										    <div class="FieldLabel heading"> DO/DU Login Info </div>
											    <div class="warning" style="padding-top: 2px; padding-bottom: 2px; margin-bottom:8px;"  >
												    Warnings: 1. DO/DU login info may not be completely hidden from users. <a href="#" tabindex=166 onclick="Modal.ShowPopup('loginProcess', 'wmsgClose', event)"> Details </a><br/>
												    <span style="padding-left: 58px">
												    2. Users with shared DO/DU logins can potentially access the same loan pipeline in DO/DU. </span>
											    </div>

									    <% if ( AspxTools.JsBool(m_isDOEnabled) == "true" ) { %>
										    <div class="padleft1 clear">
											    DO Login: <asp:textbox id="m_doLogin" onkeyup=onDoLoginTextChange(); style="PADDING-LEFT: 4px" tabIndex=166  runat="server" MaxLength="50"></asp:textbox> <span style=" width: 30px" > <img id=doerror style="display:none" src="../../images/error_icon.gif" /></span>
											    Password: <asp:textbox id="m_doPw" onkeyup=onDoLoginTextChange(); style="PADDING-LEFT: 4px" tabIndex=167  runat="server" MaxLength="20" TextMode="Password"></asp:textbox>
									    <% if ( AspxTools.JsBool(m_isNew) == "false" ) { %>
											    <span class="note">(leave blank if not changing)</span>
									    <% } %>
									    <% else { %>
											    <asp:Image id=m_doPasswordR style="DISPLAY: none" runat="server" ImageUrl="../../images/require_icon.gif"></asp:Image>
									    <% } %>
										    </div>
									    <% } %>
									    <% if ( AspxTools.JsBool(m_isDUEnabled) == "true" ) { %>
										    <div class="padleft1 clear">
											    DU Login: <asp:textbox id="m_duLogin" onkeyup=onDuLoginTextChange(); style="PADDING-LEFT: 4px" tabIndex=168 runat="server" MaxLength="50"></asp:textbox> <span style="width: 30px" > <img id=duerror style="display:none" src="../../images/error_icon.gif" /></span>
											    Password: <asp:textbox id="m_duPw" onkeyup=onDuLoginTextChange(); style="PADDING-LEFT: 4px" tabIndex=169 runat="server" MaxLength="20" TextMode="Password"></asp:textbox>

									    <% if ( AspxTools.JsBool(m_isNew) == "false" ) { %>
										    <span class="note">(leave blank if not changing)</span>
									    <% } %>
									    <% else { %>
											    <asp:Image id=m_duPasswordR style="DISPLAY: none" runat="server" ImageUrl="../../images/require_icon.gif"></asp:Image>
									    <% } %>
										    </div>
									    <% } %>

								    </div>
							    <% } %>
								    </DIV>
                                    <div id="BaseS" style="display: none;">
                                        <div id="ServiceCredentialDiv">
                                            <div>
                                                <table id="ServiceCredentialTable" class="DataGrid">
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                            <br />
                                            <input type="button" id="AddServiceCredentialBtn" value="Add Credential" />
                                        </div>
                                        <div id="ServiceCredentialNewEmpDiv">
                                            <span>Please create this user before adding service credentials.</span>
                                        </div>
                                    </div>
								    <div id="BaseC" style="DISPLAY: none;">
								        <uc:Relationships runat="server" ID="brokerRelationships" Mode="Broker" IsForOcPage="False" />
                                    </div>
                                    <div id="BaseI" style="display: none;">
                                        <uc:Relationships runat="server" ID="miniCorrespondentRelationships" Mode="MiniCorrespondent" IsForOcPage="False" />
                                    </div>
                                    <div id="BaseJ" style="display: none;">
                                        <uc:Relationships runat="server" ID="correspondentRelationships" Mode="Correspondent" IsForOcPage="False" />
                                    </div>
                                    <div id=BaseD style="DISPLAY: none">
									    <table style="WIDTH: 550px; HEIGHT: 420px">
										    <tr>
											    <td>
												    <uc:LendingLicenses id="LicensesPanel" NamingPrefix="pmlUser" runat="server"></uc:LendingLicenses>
											    </td>
										    </tr>
									    </table>
								    </div>
                                    <div id="BaseE" >
                                        <table style="width:650px">
                                            <tr>
                                                <td class="FieldLabel newheader">Loan Access Level</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input type="radio" runat="server" id="m_AccessCorporate" name="LoanAccessLevel" value="Corporate"  />
                                                        Corporate - within Originating Company
                                                    </label>
                                                    <br />
                                                    <label>
                                                        <input type="radio" runat="server" id="m_AccessSupervisor" name="LoanAccessLevel" value="Supervisor" />
                                                        Supervisor - assigned to supervised user
                                                    </label>
                                                    <br />
                                                    <label >
                                                        <input checked="true" type="radio" runat="server" id="m_AccessIndividual" name="LoanAccessLevel" value="Individual" />
                                                        Individual - only if assigned
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="FieldLabel newheader ">
                                                    Custom Portal Page Visibility
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    User will see links to which custom Originator Portal pages?
                                                    <div style="height: 85px; overflow-y: scroll;" >
                                                        <asp:Repeater ID="CustomPageRepeater" runat="server" OnItemDataBound="CustomPageRepeater_ItemDataBound">
                                                            <ItemTemplate>
                                                                <input type="checkbox" runat="server" id="CustomPageCheckbox" />
                                                                <span runat="server" id="CustomPageSpan">&nbsp;</span>
                                                                <br />
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </td>
                                            </tr>
                                            <asp:Panel runat="server" ID="m_QuickPricer">
                                                <tr>
                                                    <td class="FieldLabel newheader">
                                                        Quick Pricer
                                                     </td>
                                                     </tr>
                                                     <tr>
                                                        <td>
                                                            <script type="text/javascript">
                                                                function OnNoClick(yesid)
                                                                {
                                                                    if ( document.getElementById(yesid).checked ) {
                                                                        document.getElementById(yesid).checked = false;
                                                                        if (confirm('This settings will turn off anonymous access for this account. Do you want to continue?')) {
                                                                            return true;
                                                                        }
                                                                        else {
                                                                            document.getElementById(yesid).checked = true;
                                                                            return false;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        return true;
                                                                    }
                                                                }
                                                            </script>
                                                            <span>Use as anonymous quick pricer account</span>

                                                            <asp:RadioButton runat="server" ID="m_qpYes" TabIndex="111" Text="Yes" />
                                                            <asp:RadioButton runat="server" ID="m_qpNo" Text="No" TabIndex="112" />
                                                            <br />
                                                            <asp:TextBox runat="server" ReadOnly="true" ID="m_anonymousQuickPricerUrl" Visible="False"
                                                                TabIndex="113" Width="250px"></asp:TextBox>
                                                            <asp:LinkButton runat="server" ID="m_qpurlcopy" CausesValidation="false" Visible="false"
                                                                    TabIndex="114" Text="copy link"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                            </asp:Panel>
                                            <tr>
                                                <td class="FieldLabel newheader" >
                                                    Portal Mode Access
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 2px" >
                                                    <input type="checkbox" runat="server" id="m_AllowViewingWholesaleChannelLoans" class="viewLoan"
                                                        enableviewstate="false" />Allow viewing wholesale channel loans.<br />
                                                    <input type="checkbox" runat="server" id="m_AllowCreateWholesaleChannelLoans" class="createLoan"
                                                        enableviewstate="false" />Allow creating wholesale channel loans.<br />

                                                    <input type="checkbox" runat="server" id="m_AllowViewingMiniCorrChannelLoans" class="viewLoan"
                                                        enableviewstate="false" />Allow viewing mini-correspondent channel loans.<br />
                                                    <input type="checkbox" runat="server" id="m_AllowCreateMiniCorrChannelLoans" class="createLoan"
                                                        enableviewstate="false" />Allow creating mini-correspondent channel loans.<br />

                                                    <input type="checkbox" runat="server" id="m_AllowViewingCorrChannelLoans" class="viewLoan"
                                                        enableviewstate="false" />Allow viewing correspondent channel loans.<br />
                                                    <input type="checkbox" runat="server" id="m_AllowCreateCorrChannelLoans" class="createLoan"
                                                        enableviewstate="false" />Allow creating correspondent channel loans.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><hr /></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Use permissions &nbsp;
                                                    <asp:DropDownList runat="server" ID="UsePermissionDdl" AutoPostBack="True"
                                                        OnSelectedIndexChanged="UsePermissionDdl_SelectedIndexChanged"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <asp:Panel runat="server" ID="PermissionsPanel">
                                            <tr>
                                                <td style="padding-top: 2px" >
                                                </td>
                                            </tr>
                                            <tr >
                                                <td class="FieldLabel newheader " >
                                                    Pricing Engine
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 2px" >
                                                    <asp:CheckBox ID="m_CanApplyForIneligibleLoanPrograms" onclick="SetUIStatus();" TabIndex="75"
                                                        runat="server" EnableViewState="False" Text="Can apply for ineligible loan programs">
                                                    </asp:CheckBox><br>
                                                    <asp:CheckBox ID="m_CanRunPricingEngineWithoutCreditReport" onclick="SetUIStatus();"
                                                        TabIndex="80" runat="server" EnableViewState="False" Text="Can run pricing engine w/o credit report">
                                                    </asp:CheckBox><br>
                                                    <asp:CheckBox Style="padding-left: 16px" ID="m_CanSubmitWithoutCreditReport" TabIndex="85"
                                                        runat="server" Text="Can register/lock loans w/o credit report"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <asp:Panel runat="server" ID="m_TotalScorecard">
                                            <tr>
                                                <td class="FieldLabel newheader">
                                                    Total Scorecard Interface
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Allow access to Total Scorecard Interface</span>
                                                    <asp:RadioButton runat="server" ID="m_totalYes" GroupName="TotalScoreCard_YN" TabIndex="113" Text="Yes" />
                                                    <asp:RadioButton runat="server" ID="m_totalNo" GroupName="TotalScoreCard_YN" Text="No" TabIndex="114" />
                                                    <br />
                                                </td>
                                            </tr>
                                            </asp:Panel>
                                            <asp:Panel runat="server" ID="Panel1">
                                            <tr>
                                                <td class="FieldLabel newheader">
                                                    Notifications
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Send task-related e-mail</span>
                                                    <asp:radiobuttonlist id="m_TaskRelatedNotifOptionT" runat="server" repeatlayout="Flow" repeatdirection="Horizontal">
										                <asp:listitem value="0">Yes</asp:listitem>
										                <asp:listitem value="1">No</asp:listitem>
									                </asp:radiobuttonlist>
                                                    <br />
                                                </td>
                                            </tr>
                                            </asp:Panel>
                                            <tr>
                                                <td class="FieldLabel newheader">
                                                    Services
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="m_Allow4506T"
                                                        TabIndex="80" runat="server" EnableViewState="False" Text="Allow ordering 4506-T">
                                                    </asp:CheckBox>
                                                </td>
                                            </tr>
                                            </asp:Panel>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>


                    <table cellSpacing=0 cellPadding=0 width="100%" border=0>
	                    <TR>
		                    <TD height=55px" style="PADDING-LEFT: 10px; PADDING-TOP:6px; width: 500px; height: 68px;" vAlign=top>
			                    <SPAN id=InvalidText ></SPAN>
			                    <asp:customvalidator id=m_AllFieldValidator Runat="server" Display="Dynamic" ClientValidationFunction="ValidateFields"></asp:customvalidator>

			                    <ml:EncodedLabel id=m_LoginErrorMessage runat="server" Width="500px" EnableViewState="False" ForeColor="Red" Font-Size="10pt"></ml:EncodedLabel>
		                    </TD>
		                    <TD align=right>
			                    <DIV style="PADDING-RIGHT: 12px; PADDING-LEFT: 0px; PADDING-BOTTOM: 4px; PADDING-TOP: 12px; TEXT-ALIGN: right">
				                    <INPUT id="m_OKSeen" style="WIDTH: 60px" onclick=onClientOkClick();  tabIndex=200 type=button value=" OK ">
				                    <INPUT id="m_CancelSeen" style="WIDTH: 60px" onclick=onCancelClick(); tabIndex=205 type=button value=Cancel Width="60px">
				                    <INPUT id=m_ApplySeen runat=server style="WIDTH: 60px" onclick=onClientApplyClick(); tabIndex=210 type=button value="Apply" NAME="m_ApplySeen">
				                    <uc:nodoubleclickbutton Enabled="false" id=m_Apply style="DISPLAY: none" onclick=OnApplyClick OnClientClick="prepareCustomLOPricingPolicyInfo();" runat="server"></uc:nodoubleclickbutton>
				                    <uc:nodoubleclickbutton Enabled="false" id=m_Ok style="DISPLAY: none" onclick=OnOkClick OnClientClick="prepareCustomLOPricingPolicyInfo();" runat="server" ></uc:nodoubleclickbutton>
			                    </DIV>
			                    <div id=m_PasswordRules style="z-index: 700; BORDER-RIGHT: black 3px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 3px solid; DISPLAY: none; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 3px solid; WIDTH: 325px; PADDING-TOP: 5px; BORDER-BOTTOM: black 3px solid; POSITION: absolute; HEIGHT: auto; BACKGROUND-COLOR: whitesmoke; ">
				                    <table width="100%">
					                    <tr>
                                            <td><%=AspxTools.HtmlControl(LendersOffice.Constants.ConstApp.PasswordGuidelinesHtml)%></td>
					                    </tr>
					                    <tr>
						                    <td align=center>[ <A id='m_closePwRules' onclick="Modal.Hide();" href="#" >Close</A> ]</td>
					                    </tr>
				                    </table>
			                    </div>
		                    </TD>
	                    </TR>
                    </TABLE>

			    </div>
			</asp:Panel>

			<input id=m_CurrentTab type=hidden value=TabA name=m_CurrentTab runat="server">
			<input id="m_OverridePasswordRestriction" type=hidden value="False" name=m_OverridePasswordRestriction runat="server">
			<input id=m_IsAccountLocked type=hidden value=false runat="server" NAME="m_IsAccountLocked">
			<uc:cmodaldlg id="CModalDlg1" runat="server"></uc:cmodaldlg><input id="m_Dirty" type="hidden" value="0" name="m_Dirty" runat="server">
		</form>
			<% if ( ( m_isDOEnabled  || m_isDUEnabled )  && m_sPmlAutoLoginOption == "MustBeUnique") { %> <uc:DUDOSearch runat="server" id="m_dudoSearch" HideJS="Modal.Hide();" ZIndex="901" CssClass="modalbox"></uc:DUDOSearch> <% } %>
	</body>
</HTML>
