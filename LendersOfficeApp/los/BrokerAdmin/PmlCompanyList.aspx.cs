﻿using System;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.los.BrokerAdmin
{
    public partial class PmlCompanyList : LendersOffice.Common.BaseServicePage
    {
        protected PmlCompanyEdit m_Edit;

        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private String ErrorMessage
        {
            set
            {
                ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + ".");
            }
        }

        protected void PageLoad(object sender, System.EventArgs a)
        {
            if (BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false || BrokerUser.HasPermission(Permission.CanAdministrateExternalUsers) == false)
                throw new CBaseException(ErrorMessages.InsufficientPermissionToAdministrateExternalUsers, "Attempt to load PmlCompanyList.aspx without permission");

            if (RequestHelper.GetBool("export"))
            {
                string file = PmlBrokerCsv.ExportAllPmlBrokersToFile(BrokerUser.BrokerId);

                Response.Clear();
                Response.ContentType = "text/csv";
                Response.AppendHeader("Content-Disposition", "attachment; filename=\"OriginatingCompanies.csv\"");

                Response.TransmitFile(file);

                Response.End();
            }


        }

        protected void PageInit(object sender, System.EventArgs e)
        {
            EnableJqueryMigrate = true;
            this.RegisterService("PmlCompany", "/los/BrokerAdmin/PmlCompanyListService.aspx");
            this.RegisterJsScript("LQBPopup.js");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
        }
        #endregion

    }

}
