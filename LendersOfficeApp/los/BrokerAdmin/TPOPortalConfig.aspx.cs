﻿namespace LendersOfficeApp.los.BrokerAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web.Services;
    using System.Web.UI.WebControls;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.ConversationLog;
    using LendersOffice.ObjLib.PMLNavigation;
    using LendersOffice.ObjLib.UI.Themes;
    using LendersOffice.ObjLib.Views;
    using LendersOffice.PdfForm;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;
    using static LendersOffice.ObjLib.PMLNavigation.NavigationConfiguration;

    public partial class TPOPortalConfig : BaseServicePage
    {
        protected ColorTheme LqbDefaultTheme
        {
            get { return SassDefaults.LqbDefaultColorTheme; }
        }

        private BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        private void LoadData()
        {
            m_TpoIFrameUrl.Text = RequestHelper.PmlLoginUrl(BrokerUser.BrokerDB.PmlSiteID);

            var themeCollection = ThemeManager.GetThemeCollection(this.BrokerUser.BrokerId, ThemeCollectionType.TpoPortal);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("colorThemes", themeCollection.GetThemes());
            this.RegisterJsGlobalVariables("SelectedThemeId", themeCollection.SelectedThemeId);

            var portalConfigs = LendersOffice.ObjLib.TPO.TPOPortalConfig.RetrieveAllActiveAtBroker(BrokerUser.BrokerId);
            RegisterJsObjectWithJavascriptSerializer("portalConfigs", portalConfigs);
            RegisterJsStruct("configNameLengthLimit", LendersOffice.ObjLib.TPO.TPOPortalConfig.SCHEMA_NAME_LENGTH_LIMIT);
            RegisterJsStruct("configUrlLengthLimit", LendersOffice.ObjLib.TPO.TPOPortalConfig.SCHEMA_URL_LENGTH_LIMIT);

            this.BindOcGroups();

            this.SetupPortalOptions();

            this.BindTemplateDropdowns(PmlLoanTemplateId, MiniCorrLoanTemplateId, CorrLoanTemplateId, RetailTPOLoanTemplateId);

            if (ConstStage.EnableDefaultBranchesAndPriceGroups)
            {
                this.BindBranchDropdowns();

                this.BindPriceGroupDropdowns();
            }
            else
            {
                this.WholesaleDefaultsPlaceholder.Visible = false;
                this.MiniCorrespondentDefaultsPlaceholder.Visible = false;
                this.CorrespondentDefaultsPlaceholder.Visible = false;
            }

            if (!this.BrokerUser.BrokerDB.EnableRetailTpoPortalMode)
            {
                this.RetailTemplatePlaceholder.Visible = false;
                this.RetailPortalModeSelector.Visible = false;
            }
        }

        private void BindOcGroups()
        {
            this.AlphaTestingGroup.Items.Add(new ListItem(string.Empty, Guid.Empty.ToString()));
            this.BetaTestingGroup.Items.Add(new ListItem(string.Empty, Guid.Empty.ToString()));

            foreach (var group in GroupDB.GetAllGroups(this.BrokerUser.BrokerId, GroupType.PmlBroker))
            {
                this.AlphaTestingGroup.Items.Add(new ListItem(group.Name, group.Id.ToString()));
                this.BetaTestingGroup.Items.Add(new ListItem(group.Name, group.Id.ToString()));
            }
        }

        private void SetupPortalOptions()
        {
            var broker = this.BrokerUser.BrokerDB;

            this.RegisterJsGlobalVariables("BrokerId", broker.BrokerID);
            this.RegisterJsGlobalVariables("IsUseNewTaskSystem", broker.IsUseNewTaskSystem);

            if (broker.IsUseNewTaskSystem)
            {
                this.UseTasksYes.Checked = broker.IsUseTasksInTpoPortal;
                this.UseTasksNo.Checked = !this.UseTasksYes.Checked;

                this.ShowTaskPipelineYes.Checked = broker.IsShowPipelineOfTasksForAllLoansInTpoPortal;
                this.ShowTaskPipelineNo.Checked = !this.ShowTaskPipelineYes.Checked;
            }

            this.ShowConditionPipelineYes.Checked = broker.IsShowPipelineOfConditionsForAllLoansInTpoPortal;
            this.ShowConditionPipelineNo.Checked = !this.ShowConditionPipelineYes.Checked;

            Tools.SetDropDownListValue(this.AlphaTestingGroup, broker.TpoNewFeatureAlphaTestingGroup ?? Guid.Empty);
            Tools.SetDropDownListValue(this.BetaTestingGroup, broker.TpoNewFeatureBetaTestingGroup ?? Guid.Empty);

            Tools.SetDropDownListValue(this.RedisclosureGroup, broker.TpoRedisclosureAllowedGroupType);
            Tools.SetDropDownListValue(this.RedisclosureFormSource, broker.TpoRedisclosureFormSource);

            if (broker.TpoRedisclosureFormRequiredDocType.HasValue)
            {
                this.RedisclosureRequiredDocument.Value = EDocumentDocType.GetDocTypeById(broker.BrokerID, broker.TpoRedisclosureFormRequiredDocType.Value).FolderAndDocTypeName;
                this.RedisclosureRequiredDocument.Attributes["data-doc-type-id"] = broker.TpoRedisclosureFormRequiredDocType.Value.ToString();
            }

            switch (broker.TpoRedisclosureFormSource)
            {
                case TpoRequestFormSource.CustomPdf:
                    if (broker.TpoRedisclosureFormCustomPdfId.HasValue)
                    {
                        this.RedisclosureForm.Value = PdfForm.LoadById(broker.TpoRedisclosureFormCustomPdfId.Value).Description;
                        this.RedisclosureForm.Attributes["data-form-id"] = broker.TpoRedisclosureFormCustomPdfId.Value.ToString();
                    }
                    break;
                case TpoRequestFormSource.Hosted:
                    this.RedisclosureRequestFormUrl.Value = broker.TpoRedisclosureFormHostedUrl;
                    break;
                default:
                    throw new UnhandledEnumException(broker.TpoRedisclosureFormSource);
            }

            Tools.SetDropDownListValue(this.InitialClosingDisclosureGroup, broker.TpoInitialClosingDisclosureAllowedGroupType);
            Tools.SetDropDownListValue(this.InitialClosingDisclosureFormSource, broker.TpoInitialClosingDisclosureFormSource);

            if (broker.TpoInitialClosingDisclosureFormRequiredDocType.HasValue)
            {
                this.InitialClosingDisclosureRequiredDocument.Value = EDocumentDocType.GetDocTypeById(broker.BrokerID, broker.TpoInitialClosingDisclosureFormRequiredDocType.Value).FolderAndDocTypeName;
                this.InitialClosingDisclosureRequiredDocument.Attributes["data-doc-type-id"] = broker.TpoInitialClosingDisclosureFormRequiredDocType.Value.ToString();
            }

            switch (broker.TpoInitialClosingDisclosureFormSource)
            {
                case TpoRequestFormSource.CustomPdf:
                    if (broker.TpoInitialClosingDisclosureFormCustomPdfId.HasValue)
                    {
                        this.InitialClosingDisclosureForm.Value = PdfForm.LoadById(broker.TpoInitialClosingDisclosureFormCustomPdfId.Value).Description;
                        this.InitialClosingDisclosureForm.Attributes["data-form-id"] = broker.TpoInitialClosingDisclosureFormCustomPdfId.Value.ToString();
                    }
                    break;
                case TpoRequestFormSource.Hosted:
                    this.InitialClosingDisclosureRequestFormUrl.Value = broker.TpoInitialClosingDisclosureFormHostedUrl;
                    break;
                default:
                    throw new UnhandledEnumException(broker.TpoInitialClosingDisclosureFormSource);
            }

            Tools.SetDropDownListValue(this.OrigPortalAntiSteeringDisclosureAccess, broker.OrigPortalAntiSteeringDisclosureAccess);

            var isNonQmOpEnabled = PrincipalFactory.CurrentPrincipal.BrokerDB.IsNonQmOpEnabled(PrincipalFactory.CurrentPrincipal);

            if (isNonQmOpEnabled)
            {
                if (broker.EnableConversationLogForLoans)
                {                    
                    var securityToken = SecurityService.CreateToken();
                    var allCategories = ConversationLogHelper.GetAllCategories(securityToken, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);

                    Tools.Bind_ConversationLogCategories(OpConLogCategoryId, allCategories.Where(c => c.IsActive).ToList());
                    Tools.SetDropDownListValue(this.OpConLogCategoryId, broker.OpConLogCategoryId.ToString());
                }                

                this.OpNonQmMsgReceivingEmail.Text = broker.OpNonQmMsgReceivingEmail;
            }

            this.RegisterJsGlobalVariables("IsNonQmOpEnabled", isNonQmOpEnabled);
            this.RegisterJsGlobalVariables("EnableConversationLogForLoans", broker.EnableConversationLogForLoans);

            var pipelineSettingList = OriginatingCompanyUserPipelineSettings.ListAll(broker.BrokerID);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("OriginatingCompanyUserPipelineSettings", pipelineSettingList);
        }

        private void BindTemplateDropdowns(params DropDownList[] dropdowns)
        {
            SqlParameter[] parameters = {
                new SqlParameter( "@BrokerID" , BrokerUser.BrokerId ),
                new SqlParameter( "@IsTemplate" , 1 ),
                new SqlParameter( "@IsValid" , 1 )
            };

            var entries = new Dictionary<string, string>();

            entries.Add("<-- None -->", Guid.Empty.ToString());

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "ListLoansByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    entries.Add(reader["LoanNm"].ToString(), reader["LoanId"].ToString());
                }
            }

            var orderedEntires = entries.OrderBy(entry => entry.Key);

            foreach (var dropdown in dropdowns)
            {
                dropdown.DataTextField = "Key";
                dropdown.DataValueField = "Value";
                dropdown.DataSource = orderedEntires;
                dropdown.DataBind();
            }

            PmlLoanTemplateId.SelectedValue = BrokerUser.BrokerDB.PmlLoanTemplateID.ToString();
            MiniCorrLoanTemplateId.SelectedValue = BrokerUser.BrokerDB.MiniCorrLoanTemplateID.ToString();
            CorrLoanTemplateId.SelectedValue = BrokerUser.BrokerDB.CorrLoanTemplateID.ToString();
            RetailTPOLoanTemplateId.SelectedValue = BrokerUser.BrokerDB.RetailTPOLoanTemplateID.ToString();
        }

        private void BindBranchDropdowns()
        {
            SqlParameter[] parameters = { new SqlParameter( "@BrokerID" , BrokerUser.BrokerId ) };

            Guid? brokerBranch = null;
            Guid? miniCorrBranch = null;
            Guid? corrBranch = null;

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "BROKER_ListDefaultBranchesByBrokerId", parameters))
            {
                if (reader.Read())
                {
                    brokerBranch = reader["WholesaleBranchID"] as Guid?;
                    miniCorrBranch = reader["MiniCorrBranchID"] as Guid?;
                    corrBranch = reader["CorrespondentBranchID"] as Guid?;
                }
            }

            this.AddEntriesToDropdowns(
                this.BrokerUser.BrokerDB.GetBranches(), 
                brokerBranch, 
                miniCorrBranch, 
                corrBranch,
                DefaultWholesaleBranchId,
                DefaultMiniCorrBranchId,
                DefaultCorrBranchId);
        }

        private void BindPriceGroupDropdowns()
        {
            SqlParameter[] parameters = { new SqlParameter("@BrokerID", BrokerUser.BrokerId) };

            Guid? brokerPriceGroup = null;
            Guid? miniCorrPriceGroup = null;
            Guid? corrPriceGroup = null;

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerUser.BrokerId, "BROKER_ListDefaultPriceGroupsByBrokerId", parameters))
            {
                if (reader.Read())
                {
                    brokerPriceGroup = reader["WholesalePriceGroupID"] as Guid?;
                    miniCorrPriceGroup = reader["MiniCorrPriceGroupID"] as Guid?;
                    corrPriceGroup = reader["CorrespondentPriceGroupID"] as Guid?;
                }
            }

            this.AddEntriesToDropdowns(
                this.BrokerUser.BrokerDB.GetPriceGroups(), 
                brokerPriceGroup, 
                miniCorrPriceGroup, 
                corrPriceGroup,
                DefaultWholesalePriceGroupId,
                DefaultMiniCorrPriceGroupId,
                DefaultCorrPriceGroupId);
        }

        private void AddEntriesToDropdowns(
            IEnumerable<KeyValuePair<Guid, string>> entriesToAdd,
            Guid? defaultBrokerEntry, 
            Guid? defaultMiniCorrEntry, 
            Guid? defaultCorrEntry, 
            DropDownList brokerDropdown, 
            DropDownList miniCorrDropdown, 
            DropDownList corrDropdown)
        {
            foreach (var entry in entriesToAdd.OrderBy(entry => entry.Value))
            {
                brokerDropdown.Items.Add(new ListItem(entry.Value, entry.Key.ToString()));
                miniCorrDropdown.Items.Add(new ListItem(entry.Value, entry.Key.ToString()));
                corrDropdown.Items.Add(new ListItem(entry.Value, entry.Key.ToString()));
            }

            string defaultEntryListItemValue;

            if (this.TryGetDefaultEntryName(defaultBrokerEntry, entriesToAdd, out defaultEntryListItemValue))
            {
                brokerDropdown.SelectedValue = defaultEntryListItemValue;
            }
            else
            {
                brokerDropdown.Items.Insert(0, new ListItem(string.Empty, Guid.Empty.ToString()));
            }

            if (this.TryGetDefaultEntryName(defaultMiniCorrEntry, entriesToAdd, out defaultEntryListItemValue))
            {
                miniCorrDropdown.SelectedValue = defaultEntryListItemValue;
            }
            else
            {
                miniCorrDropdown.Items.Insert(0, new ListItem(string.Empty, Guid.Empty.ToString()));
            }

            if (this.TryGetDefaultEntryName(defaultCorrEntry, entriesToAdd, out defaultEntryListItemValue))
            {
                corrDropdown.SelectedValue = defaultEntryListItemValue;
            }
            else
            {
                corrDropdown.Items.Insert(0, new ListItem(string.Empty, Guid.Empty.ToString()));
            }

            brokerDropdown.DataBind();
            miniCorrDropdown.DataBind();
            corrDropdown.DataBind();
        }

        private bool TryGetDefaultEntryName(
            Guid? entry, 
            IEnumerable<KeyValuePair<Guid, string>> entriesToAdd,
            out string defaultEntryListItemValue)
        {
            defaultEntryListItemValue = null;

            if (entry.HasValue && !entry.Value.Equals(Guid.Empty))
            {
                var searchResult = entriesToAdd.FirstOrDefault(kvp => kvp.Key.Equals(entry.Value));
                
                if(!searchResult.Equals(default(KeyValuePair<Guid, string>)))
                {
                    defaultEntryListItemValue = searchResult.Key.ToString();

                    return true;
                }
            }

            return false;
        }

        protected override bool EnableJQueryInitCode
        {
            get
            {
                return true;
            }
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        [WebMethod]
        public static Guid GenerateId()
        {
            return Guid.NewGuid();
        }

        [WebMethod]
        public static bool ValidateUrl(string url)
        {
            return NavigationConfiguration.ValidateUrl(url);
        }

        [WebMethod]
        public static ColorTheme AddColorTheme()
        {
            return Tools.LogAndThrowIfErrors(() =>
            {
                var themeCollection = ThemeManager.GetThemeCollection(PrincipalFactory.CurrentPrincipal.BrokerId, ThemeCollectionType.TpoPortal);
                var newTheme = themeCollection.AddTheme("Custom Theme");
                themeCollection.Save();

                return newTheme;
            });
        }

        protected void PageLoad(object sender, System.EventArgs a)
        {
            if (false == BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) 
                || false == BrokerUser.HasPermission(Permission.AllowTpoPortalConfiguring)
                )
            {
                throw new CBaseException(ErrorMessages.InsufficientPermissionToAdministrateExternalUsers,
                    "Attempt to load TPO Portal Configuration without permission");  
                //! todo: instead hide things and not bubble the error up.
            }

            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("dynatree/jquery.cookie.js");
            RegisterJsScript("dynatree/jquery.dynatree.js");
            this.RegisterJsScript("LQBPopup.js");
            RegisterCssFromInc("dynatree/skin-vista/ui.dynatree.css");
            this.IncludeStyleSheet("~/css/Tabs.css");
            NavigationConfiguration navConfig = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).GetPmlNavigationConfiguration(PrincipalFactory.CurrentPrincipal);          
            string data = navConfig.GetDynaTraceJSON();
            ClientScript.RegisterClientScriptBlock(typeof(TPOPortalConfig), "Nav", "var navConfig = [" + data + "];", true);
            LoadData();
        }

        protected void PageInit(object sender, System.EventArgs a)
        {
            this.EnableJqueryMigrate = false;
            RegisterService("TpoPortalConfigService", "/los/BrokerAdmin/TPOPortalConfigService.aspx");

            this.BindOcGroupAccess(this.RedisclosureGroup);
            this.BindOcGroupAccess(this.InitialClosingDisclosureGroup);

            this.BindFormSource(this.RedisclosureFormSource);
            this.BindFormSource(this.InitialClosingDisclosureFormSource);

            this.BindOrigPortalAntiSteeringDisclosureAccess(this.OrigPortalAntiSteeringDisclosureAccess);
        }

        private void BindOcGroupAccess(DropDownList list)
        {
            list.Items.Add(Tools.CreateEnumListItem("Yes - All OC Groups", NewTpoFeatureOcGroupAccess.All));
            list.Items.Add(Tools.CreateEnumListItem("Yes - Alpha and Beta Testing OC Groups", NewTpoFeatureOcGroupAccess.AlphaAndBeta));
            list.Items.Add(Tools.CreateEnumListItem("Yes - Alpha Testing OC groups", NewTpoFeatureOcGroupAccess.Alpha));
            list.Items.Add(Tools.CreateEnumListItem("No", NewTpoFeatureOcGroupAccess.None));
        }

        private void BindOrigPortalAntiSteeringDisclosureAccess(DropDownList list)
        {
            list.Items.Add(Tools.CreateEnumListItem("Never", OrigPortalAntiSteeringDisclosureAccessType.Never));
            list.Items.Add(Tools.CreateEnumListItem("TPO Transactions with lender-paid LO Comp Only", OrigPortalAntiSteeringDisclosureAccessType.LenderPaidOriginationOnly));
            list.Items.Add(Tools.CreateEnumListItem("All TPO Transactions", OrigPortalAntiSteeringDisclosureAccessType.AllTpoTransactions));
        }

        private void BindFormSource(DropDownList list)
        {
            list.Items.Add(Tools.CreateEnumListItem("Custom PDF", TpoRequestFormSource.CustomPdf));
            list.Items.Add(Tools.CreateEnumListItem("Hosted", TpoRequestFormSource.Hosted));
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

        }
        #endregion
    }
}
