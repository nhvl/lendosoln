﻿// <copyright file="LendingLicenseEdit.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: EduardoM
//    Date:   6/6/2014
// </summary>
namespace LendersOfficeApp.Los.BrokerAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web;
    using System.Web.Services;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;

    /// <summary>
    /// LendingLicense Edit allows users to assign a License its own individual address.
    /// </summary>
    public partial class LendingLicenseEdit : LendersOffice.Common.BaseServicePage
    {
        /// <summary>
        /// Registers the scripts, and service.
        /// </summary>
        /// <param name="sender">Control that caused the event.</param>
        /// <param name="e">Event Arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Zip.SmartZipcode(this.City, this.AddrState);
            this.RegisterJsScript("mask.js");
            this.RegisterJsScript("json.js");
            this.RegisterJsScript("simpleservices.js");
            this.RegisterService("LendingLicenseEdit", "/los/BrokerAdmin/LendingLicenseEditService.aspx");
        }
    }
}
