﻿#region Generated Code
namespace LendersOfficeApp.los.BrokerAdmin.TpoColorTheme
#endregion
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.UI.Themes;
    using LendersOffice.Security;

    /// <summary>
    /// Allows a user to preview a color theme.
    /// </summary>
    public partial class PreviewTheme : BaseServicePage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.RegisterCSS("PreviewTheme.css");
            this.RegisterCSS("bootstrap.min.css");

            this.RegisterJsScript("bootstrap-3.3.4.min.js");
            this.RegisterJsScript("PreviewTheme.js");

            this.RegisterService("main", "/los/BrokerAdmin/TpoColorTheme/EditThemeService.aspx");

            this.RegisterTheme();
        }

        /// <summary>
        /// Gets the forced compatibility mode for the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Registers the color theme for use on the page.
        /// </summary>
        private void RegisterTheme()
        {
            ColorTheme previewTheme;

            var cacheId = RequestHelper.GetSafeQueryString("cacheid");
            if (string.IsNullOrEmpty(cacheId))
            {
                var themeCollection = ThemeManager.GetThemeCollection(BrokerUserPrincipal.CurrentPrincipal.BrokerId, ThemeCollectionType.TpoPortal);
                previewTheme = themeCollection.GetTheme(RequestHelper.GetGuid("themeid"));
            }
            else
            {
                previewTheme = SassUtilities.GetColorThemeFromCache(cacheId);
            }

            // Some SASS variables are not configurable by the user and are therefore skipped.
            var variableListForUi = previewTheme.Variables
                .Where(kvp => SassDefaults.SassVariableNameToUiEditorNameMappings.ContainsKey(kvp.Key))
                .Select(kvp => new { Id = SassDefaults.SassVariableNameToUiEditorNameMappings[kvp.Key], Value = kvp.Value });

            this.RegisterJsObjectWithJsonNetAnonymousSerializer("variables", variableListForUi);
            this.RegisterJsGlobalVariables("ThemeName", previewTheme.Name);
            this.RegisterJsGlobalVariables("QuickPreview", RequestHelper.GetBool("quickpreview"));

            var styleContent = SassUtilities.CompileColorThemeForPreview(previewTheme);
            var styleTag = "<style type='text/css'>" + styleContent + "</style>";
            this.ClientScript.RegisterClientScriptBlock(typeof(PreviewTheme), "PreviewStyle", styleTag);
        }
    }
}