﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditTheme.aspx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.TpoColorTheme.EditTheme" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit Custom Originator Portal Theme</title>
</head>
<body>
    <h4 class="page-header">Edit Custom Originator Portal Theme</h4>
    <form id="form1" runat="server">
    <div id="EditorDiv" class="container-fluid">
        <div class="row">
            <div class="col-xs-12 no-left-padding">
                <h4><span class="bold">Theme Name:</span> <input type="text" id="ThemeName" class="full-width" /></h4>
            </div>
        </div>
        <div class="row extra-bottom-margin">
            <div class="col-xs-12 align-center">
                <img id="NavLegend" src="../../../images/TpoNavigationAreaDivision.svg" alt="Originator Portal Navigation Legend" />
            </div>
        </div>

        <div class="row no-left-padding extra-bottom-margin">
            <div class="col-xs-6">
                <span class="bold">Page Header</span>
            </div>
            <div class="col-xs-6">
                <span class="bold">Content</span>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                Background
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="PageHeaderBackground" />
            </div>
            <div class="col-xs-3">
                Header
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="ContentHeader" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                Text
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="PageHeaderText" />
            </div>
            <div class="col-xs-3">
                Icons, Buttons, Tabs
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="ContentIconButtonTab" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                Accent
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="PageHeaderAccent" />
            </div>
            <div class="col-xs-3">
                Links, Sorted Columns
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="ContentLinkSortedColumn" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                Save Button Text
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker explicit-save-button"/>
                <input type="text" class="variable explicit-save-button" id="PageHeaderSaveButtonText" />
            </div>
            <div class="col-xs-3">
                Progress Bar (Current)
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="ContentProgressBarCurrent" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                Save Button Background
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker explicit-save-button" />
                <input type="text" class="variable explicit-save-button" id="PageHeaderSaveButtonBackground" />
            </div>
            <div class="col-xs-3">
                Progress Bar (Past)
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="ContentProgressBarPast" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3 no-left-padding">
                <span class="bold">Side Navigation</span>
            </div>
            <div class="col-xs-3"><!-- Spacer --></div>
            <div class="col-xs-3">
                Tooltip
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="ContentTooltip" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                Background
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="SideNavigationBackground" />
            </div>
            <div class="col-xs-3">
                Tooltip Text
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="ContentTooltipText" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                Selection
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="SideNavigationSelection" />
            </div>
            <div class="col-xs-3">
                Text
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="ContentText" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                Collapse/Expand Button
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="SideNavigationCollapseExpandButton" />
            </div>
            <div class="col-xs-3">
                Raised Button Text
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="ContentRaisedButtonText" />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
                Text
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="SideNavigationText" />
            </div>
            <div class="col-xs-3">
                Flat Button Text
            </div>
            <div class="col-xs-3">
                <input type="color" class="picker" />
                <input type="text" class="variable" id="ContentFlatButtonText" />
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col-xs-8 col-xs-offset-2 align-center">
                <input type="button" id="PreviewBtn" value="Preview" />
                <input type="button" id="SaveBtn" value="Save" />
                <input type="button" id="CancelBtn" value="Cancel" />
            </div>
        </div>
    </div>

    <div id="QuickPreviewDiv" class="container-fluid">
        <iframe id="QuickPreviewFrame"></iframe>
    </div>
    </form>
</body>
</html>
