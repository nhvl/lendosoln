﻿#region Generated Code
namespace LendersOfficeApp.los.BrokerAdmin.TpoColorTheme
#endregion
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.UI.Themes;
    using LendersOffice.Security;
    using LendersOffice.Admin;

    /// <summary>
    /// Allows a user to edit a color theme.
    /// </summary>
    public partial class EditTheme : BaseServicePage
    {
        /// <summary>
        /// Initializes the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Init(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Loads the page.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;
            this.RegisterCSS("EditTheme.css");
            this.RegisterCSS("bootstrap.min.css");
            this.RegisterCSS("spectrum.css");

            this.RegisterJsScript("bootstrap-3.3.4.min.js");
            this.RegisterJsScript("spectrum.js");
            this.RegisterJsScript("EditTheme.js");

            this.RegisterService("main", "/los/BrokerAdmin/TpoColorTheme/EditThemeService.aspx");

            this.RegisterJsGlobalVariables("isSaveButtonEnabled", Tools.IsSaveButtonEnabled(PrincipalFactory.CurrentPrincipal));

            this.RegisterThemeVariables();            
        }

        /// <summary>
        /// Gets the forced compatibility mode for the page.
        /// </summary>
        /// <returns>
        /// The forced compatibility mode.
        /// </returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Registers color theme variables for use on the page.
        /// </summary>
        private void RegisterThemeVariables()
        {
            var themeCollection = ThemeManager.GetThemeCollection(BrokerUserPrincipal.CurrentPrincipal.BrokerId, ThemeCollectionType.TpoPortal);
            var selectedTheme = themeCollection.GetTheme(RequestHelper.GetGuid("themeid"));

            // Some SASS variables are not configurable by the user and are therefore skipped.
            var variableListForUi = selectedTheme.Variables
                .Where(kvp => SassDefaults.SassVariableNameToUiEditorNameMappings.ContainsKey(kvp.Key))
                .Select(kvp => new { Id = SassDefaults.SassVariableNameToUiEditorNameMappings[kvp.Key], Value = kvp.Value });

            this.RegisterJsGlobalVariables("ThemeName", selectedTheme.Name);
            this.RegisterJsGlobalVariables("ThemeId", selectedTheme.Id);
            this.RegisterJsObjectWithJsonNetAnonymousSerializer("variables", variableListForUi);
        }
    }
}