﻿#region Generated Code
namespace LendersOfficeApp.los.BrokerAdmin.TpoColorTheme
#endregion
{
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.UI.Themes;
    using LendersOffice.Security;

    /// <summary>
    /// Provides additional functionality for the theme editor.
    /// </summary>
    public partial class EditThemeService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Processes the method with the specified <paramref name="methodName"/>.
        /// </summary>
        /// <param name="methodName">
        /// The name of the method to process.
        /// </param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case nameof(SaveTheme):
                    this.SaveTheme();
                    break;
                case nameof(SaveThemeToCacheForPreview):
                    this.SaveThemeToCacheForPreview();
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled method " + methodName);
            }
        }

        /// <summary>
        /// Saves the color theme.
        /// </summary>
        private void SaveTheme()
        {
            var configurableVariables = new Dictionary<string, string>();

            var variableList = SerializationHelper.JsonNetDeserialize<List<KeyValuePair<string, string>>>(this.GetString("variables"));

            SassUtilities.ValidateVariableValueList(variableList);

            foreach (var variable in variableList)
            {
                string mappedVariableName;
                if (!SassDefaults.UiEditorNameToSassVariableNameMappings.TryGetValue(variable.Key, out mappedVariableName))
                {
                    throw new CBaseException(ErrorMessages.Generic, "Could not map UI field to SASS variable: " + variable.Key);
                }

                configurableVariables[mappedVariableName] = variable.Value;
            }

            var collection = ThemeManager.GetThemeCollection(PrincipalFactory.CurrentPrincipal.BrokerId, ThemeCollectionType.TpoPortal);
            var theme = collection.GetTheme(this.GetGuid("themeId"));

            theme.Name = this.GetString("name");

            foreach (var kvp in configurableVariables)
            {
                theme.Variables[kvp.Key] = kvp.Value;
            }

            collection.UpdateTheme(theme);
            collection.Save();
        }

        /// <summary>
        /// Stores the theme in the cache to allow previewing changes.
        /// </summary>
        private void SaveThemeToCacheForPreview()
        {
            var variableList = SerializationHelper.JsonNetDeserialize<List<KeyValuePair<string, string>>>(this.GetString("variables"));

            SassUtilities.ValidateVariableValueList(variableList);

            var previewVariables = new Dictionary<string, string>(SassDefaults.LqbDefaultColorTheme.Variables);

            // Override the default values with the variable values entered in the UI.
            // This is so we populate all of the other variables for the preview that
            // aren't editable in the UI.
            foreach (var variable in variableList)
            {
                string mappedVariableName;
                if (!SassDefaults.UiEditorNameToSassVariableNameMappings.TryGetValue(variable.Key, out mappedVariableName))
                {
                    throw new CBaseException(ErrorMessages.Generic, "Could not map UI field to SASS variable: " + variable.Key);
                }

                previewVariables[mappedVariableName] = variable.Value;
            }

            string cacheId = SassUtilities.StoreThemeToCache(this.GetGuid("themeid"), this.GetString("name"), previewVariables);
            this.SetResult("CacheId", cacheId);
        }
    }
}