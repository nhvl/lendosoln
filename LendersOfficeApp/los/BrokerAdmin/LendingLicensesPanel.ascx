﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LendingLicensesPanel.ascx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.LendingLicensesPanel" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="LendersOffice.Common" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<div>
    <table cellpadding="0" cellspacing="0" border="0" style="width:100%">
        <tr>
            <td>
                <asp:Placeholder id="pnlLsIdentifier" runat="server">
                    <%--OPM 55321: changed the label to its current value, added mask.js compat to the text field to will make it harder to submit non-numbers. --%>
                    <b><label id="lblLOID" runat="server">Loan Originator NMLS ID</label>&nbsp;&nbsp;</b>
                    <asp:textbox ID="tbNmlsIdentifier" runat="server" Width="20em" CssClass="mask" preset="nmlsID"></asp:textbox>
                    <br /><br />
                    <b>State Licenses</b>
                    <hr />
                </asp:Placeholder>
                <div>
                    Loan officers may lose write access to assigned files due to license expiration. To update licenses for all of your files, please have your system administrator update the license in your employee profile. <br />
                    <br />
                    <div style="padding: 2px;">
                        <input id='<%= AspxTools.HtmlString(m_prefix + "AddBtn") %>' type="button" value="Add License" onclick="LICENSES.fn.addLendingLicense(<%= AspxTools.JsString(m_prefix) %>);" nohighlight />
                        &nbsp;&nbsp;
                        <span id='<%= AspxTools.HtmlString(m_prefix + "licErrMsg") %>' style="color: Red; font-size: 11px; font-family: Arial; font-weight: bold"></span>
                    </div>
                    <div style="padding-right: 4px; height: 355px; overflow-y: SCROLL">
                        <table class="DataGrid" border="1" cellpadding="1" cellspacing="0" style="border-collapse: collapse; width:100%">
                            <tr class="GridHeader" style="border: solid 1px white">
                                <td id="<%=AspxTools.HtmlString(NamingPrefix) %>editColumn">
                                    &nbsp
                                </td>
                                <td >
                                    License #
                                </td>
                                <td style="width: 60px">
                                    State
                                </td>
                                <td >
                                    Expiration Date
                                </td>
                                <td id="<%=AspxTools.HtmlString(NamingPrefix) %>addressColumn">
                                    License Address
                                </td>
                                <td style="width: 50px">
                                    &nbsp;
                                </td>
                            </tr>
                            <tbody id='<%= AspxTools.HtmlString(m_prefix + "LendingLicensesRows") %>'></tbody>
                        </table>
                        <div id='<%= AspxTools.HtmlString(m_prefix + "emptyPanel") %>' style=" text-align: center; padding: 3em; color: dimgray; display: none">
                            No licenses to display.
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <asp:CustomValidator id="licValidator" ClientValidationFunction="__customLicensesValidator" runat="server" />

    <script type="text/javascript">
            <!--
                try { if (this.LICENSES) { LICENSES.fn.attachLoadListener(); } } catch (e) { }
            //-->
    </script>

</div>
<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
