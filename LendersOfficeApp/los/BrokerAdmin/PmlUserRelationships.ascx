﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PmlUserRelationships.ascx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.PmlUserRelationships" %>
<%@ Register TagPrefix="uc" TagName="EmployeeRoleChooserLink" Src="../common/EmployeeRoleChooserLink.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<table class="pml-user-relationships <%= AspxTools.HtmlString(this.Mode)%>">
    <tr>
        <td>
            <table cellspacing="8" cellpadding="0">
                <asp:Panel runat="server" ID="ExternalUsersPanel">
                <tr>
                    <td colspan="3" class="FieldLabel">
                        External users assigned to all loans <span class="underline">created</span> 
                        <%=AspxTools.HtmlString(this.UserType)%>:
                    </td>
                </tr>
                <tr runat="server" id="brokerProcessorRow" class="broker-processor">
                    <td class="role-label">Processor (External)</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="brokerProcessor"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="externalPostCloserRow" class="external-post-closer">
                    <td class="role-label">Post-Closer (External)</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="externalPostCloser"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr id="UseRelationshipDropdownPanel" runat="server">
                    <td colspan="2" class="FieldLabel">
                        Use internal relationships &nbsp;
                        <asp:DropDownList runat="server" ID="UseRelationshipDdl" AutoPostBack="true" 
                            onchange="PMLUserRelationships.prePostbackClientSideSave();" OnSelectedIndexChanged="UseRelationshipDdl_SelectedIndexChanged"></asp:DropDownList>
                    </td>
                </tr>
                </asp:Panel>
                <asp:Panel ID="InternalUsersPanel" runat="server">
                <tr>
                    <td colspan="3" class="relationship-header FieldLabel">
                        Internal users assigned to all loans <span class="underline">created</span> 
                        <%=AspxTools.HtmlString(this.UserType)%>:
                    </td>
                </tr>
                <tr runat="server" id="managerRow" class="manager">
                    <td class="role-label">Manager</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="manager"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="processorRow" class="processor">
                    <td class="role-label">Processor</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="processor"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="juniorProcessorRow" class="junior-processor">
                    <td class="role-label">Junior Processor</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="juniorProcessor"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="lenderAccountExecRow" class="lender-account-exec">
                    <td class="role-label">Account Exec</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="lenderAccountExec"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="correspondentPriorApprovedLabel">
                    <td colspan="3" class="relationship-header FieldLabel">
                        Internal users assigned to all <span class="underline">prior approved</span> loans <span class="underline">registered/locked</span> 
                        <%=AspxTools.HtmlString(this.UserType)%>:
                    </td>
                </tr>
                <tr runat="server" id="standardRegisterLockLabel">
                    <td colspan="3" class="relationship-header FieldLabel">
                        Internal users assigned to all loans <span class="underline">registered/locked</span> 
                        <%=AspxTools.HtmlString(this.UserType)%>:
                    </td>
                </tr>
                <tr runat="server" id="underwriterRow" class="underwriter">
                    <td class="role-label">Underwriter</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="underwriter"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="juniorUnderwriterRow" class="junior-underwriter">
                    <td class="role-label">Junior Underwriter</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="juniorUnderwriter"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="correspondentRegisterLockLabel">
                    <td colspan="3" class="relationship-header FieldLabel">
                        Internal users assigned to all loans <span class="underline">registered/locked</span> 
                        <%=AspxTools.HtmlString(this.UserType)%>:
                    </td>
                </tr>
                <tr runat="server" id="creditAuditorRow" class="credit-auditor">
                    <td class="role-label">Credit Auditor</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="creditAuditor"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="legalAuditorRow" class="legal-auditor">
                    <td class="role-label">Legal Auditor</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="legalAuditor"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="lockDeskRow" class="lock-desk">
                    <td class="role-label">Lock Desk</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="lockDesk"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="purchaserRow" class="purchaser">
                    <td class="role-label">Purchaser</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="purchaser"></uc:EmployeeRoleChooserLink></td>
                </tr>
                <tr runat="server" id="secondaryRow" class="secondary">
                    <td class="role-label">Secondary</td>
                    <td><uc:EmployeeRoleChooserLink runat="server" ID="secondary"></uc:EmployeeRoleChooserLink></td>
                </tr>
                </asp:Panel>
                <asp:Panel runat="server" ID="BranchPriceGroupLandingPagePanel">
                    <tr>
                        <td colspan="2"><hr /></td>
                    </tr>
                    <tr class="branch">
                        <td class="FieldLabel"><ml:EncodedLiteral runat="server" ID="branchPrefix"></ml:EncodedLiteral> Branch</td>
                        <td>
                            <asp:DropDownList runat="server" ID="branch"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="price-group">
                        <td class="FieldLabel"><ml:EncodedLiteral runat="server" ID="priceGroupPrefix"></ml:EncodedLiteral> Price Group</td>
                        <td>
                            <asp:DropDownList runat="server" ID="priceGroup"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="landing-page">
                        <td class="FieldLabel"><ml:EncodedLiteral runat="server" ID="landingPagePrefix"></ml:EncodedLiteral> Landing Page</td>
                        <td>
                            <asp:DropDownList runat="server" ID="landingPage"></asp:DropDownList>
                        </td>
                    </tr>
                </asp:Panel>
            </table>
        </td>
    </tr>
</table>