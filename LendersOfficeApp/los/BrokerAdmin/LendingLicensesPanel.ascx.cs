﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Constants;

using LendersOffice.Security;
using DataAccess;
using LendersOffice.Admin;
using LendersOfficeApp.los.admin;
using LendersOffice.Common;
using MeridianLink.CommonControls;
using System.Collections.Generic;
using LendersOffice.AntiXss;
using System.Text.RegularExpressions;

namespace LendersOfficeApp.los.BrokerAdmin
{
    public partial class LendingLicensesPanel : UserControl
    {
		private LicenseInfoList	    m_LicenseInfoList       = new LicenseInfoList();
        protected string            m_prefix                = "";
		protected const int         MAX_LICENSES            = 100;
        private const string        JS_JSON_FILENAME        = "json.js";
        private const string        JS_LICENSES_FILENAME    = "licenses.js";
        private bool                isJsRegistered          = false;
        private string              m_JsonToSerialize       = "";
        private bool m_bDisplayLosIdentifier = true;
        private string m_sLosIdentifier = string.Empty;

        #region Properties
        public bool Disabled { get; set; }
        private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        public LicenseInfoList LicenseList
        {
            set
            {
                m_LicenseInfoList = value;
                m_JsonToSerialize = LicenseInfoList.JsonSerialize(m_LicenseInfoList);
            }
            get
            {
                string json;
                json = Request.Form[m_prefix + "LendingLicenseList"];
                if (json == null)
                    json = "{}";
                m_LicenseInfoList = LicenseInfoList.JsonDeserialize(json);

                if (m_LicenseInfoList == null)
                    m_LicenseInfoList = new LicenseInfoList();

                return m_LicenseInfoList;
            }
        }

        public string NamingPrefix
        {
            get { return m_prefix; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    m_prefix = Regex.Replace(value, "[^a-zA-Z0-9]", "").TrimWhitespaceAndBOM();
                else
                    m_prefix = "";
            }
        }
        public bool DisplayLosIdentifier 
        {
            get { return m_bDisplayLosIdentifier; }
            set { m_bDisplayLosIdentifier = value; } 
        }
        public string LosIdentifier
        {
            get { return this.tbNmlsIdentifier.Text; }
            //set { m_sLosIdentifier = value; }
            set { this.tbNmlsIdentifier.Text = value; }
        }
        public bool IsCompanyLOID { get; set;}
        #endregion

        protected void PageInit(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(m_prefix))
            {
                throw new ArgumentException("The prefix for 'LendingLicensesPanel' cannot be empty. Please initialize it by setting the NamingPrefix property.", "m_prefix");
            }
            
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), JS_JSON_FILENAME, string.Format("<script src=\"{0}/inc/{1}\" language=\"javascript\" type=\"text/javascript\"></script>", ((BasePage)Page).VirtualRoot, JS_JSON_FILENAME));
            ((BasePage)Page).RegisterJsScript(JS_LICENSES_FILENAME);
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{            
            pnlLsIdentifier.Visible = m_bDisplayLosIdentifier;
            if (IsCompanyLOID)
                lblLOID.InnerText = "Loan Origination Company Identifier";
        }

        protected void PagePreRender(object sender, System.EventArgs a)
        {
            try
            {
                // 5/13/2010 dd - Move this register startup script down so the Disable property value will have the correct value.
                Page.ClientScript.RegisterStartupScript(Page.GetType(), m_prefix + "lendingInit", @"if (this.LICENSES){ LICENSES.fn.Add('" + m_prefix + @"', " + (Disabled ? "true" : "false") + @"); }", true);

                string json = "";
                if (string.IsNullOrEmpty(m_JsonToSerialize))
                    // OPM 462714 - Get LicenseList, which loads m_LicenseInfoList, restoring data after postback.
                    json = LicenseInfoList.JsonSerialize(this.LicenseList);
                else
                    json = m_JsonToSerialize;
                
                RegisterJsStartupObjects(json);
            }
            catch(CBaseException exc)
            {
                Tools.LogWarning(exc);
            }

        }

        private void RegisterJsStartupObjects(string json)
        {
            if (!isJsRegistered)
            {
                isJsRegistered = true;
                string script = "if (this.LICENSES){ LICENSES.List['" + m_prefix + "'] = " + json + "; }";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "LICENSES.List[" + m_prefix + "]", script, true);
                Page.ClientScript.RegisterHiddenField(m_prefix + "LendingLicenseList", json);                
            }
        }

        public static void RegisterLicensesObject(Page page, string prefix, LicenseInfoList list)
        {
            string json = LicenseInfoList.JsonSerialize(list);
            string script = "LICENSES.List['" + prefix + "'] = " + json + ";";
            page.ClientScript.RegisterStartupScript(page.GetType(), "LICENSES.List[" + prefix + "]", script, true);
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);
            this.PreRender += new System.EventHandler(this.PagePreRender);
		}
		#endregion
	}
}