<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="uc" TagName="LendingLicenses" Src="../../los/BrokerAdmin/LendingLicensesPanel.ascx" %>
<%@ Register TagPrefix="uc" TagName="Relationships" Src="../../los/BrokerAdmin/PmlUserRelationships.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PmlCompanyEdit.ascx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.PmlCompanyEdit" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<style>
	.Tab
	{
		PADDING-LEFT: 6px;
		PADDING-TOP: 6px;
		PADDING-BOTTOM: 6px;
		PADDING-RIGHT: 6px;

		BORDER-RIGHT: 2px groove;
		BORDER-TOP: 2px groove;
		BORDER-LEFT: 2px groove;
		BORDER-BOTTOM: 2px groove;

		COLOR: black;
		BACKGROUND-COLOR: lightgrey;
	}
	.TabContent
	{
		BORDER-RIGHT: 2px groove;
		BORDER-TOP: 0px;
		BORDER-LEFT: 2px groove;
		BORDER-BOTTOM: 2px groove;

		PADDING-RIGHT: 8px;
		PADDING-LEFT: 8px;
		PADDING-BOTTOM: 8px;
		PADDING-TOP: 8px;

		BACKGROUND-COLOR: gainsboro;
	}
	table.DataGrid td
    {
        border: 1px solid #DDDDDD;
    }
    #sMsg
    {
    	float:right;
    	padding: 0px 4px;
    	BACKGROUND-COLOR: #ffffcc;
    	font-weight:normal;
    	color:Black;
    }
    .cellName
    {
    	WIDTH: 100%;
    	CURSOR: hand;
    }
    .pageNumbers a
    {
        text-decoration: none;
    }
    .Header {border : 0}
	.heading  {margin-top: 10px;padding-bottom: 5px;margin-bottom: 5px;BORDER-BOTTOM: lightgrey 2px solid; }
	.CustomLOPPFieldLabel { display: inline-block; padding-right: 20px; font-weight: normal !important; font-size: 12px !important; }
	.newheader { color: White; background-color: rgb(0,128,255); padding-left: 5px; }
    .Right { text-align: right; }
    .PaddedLeftSpan { padding-left: 45px; }
    .PaddedRightSpan { padding-right: 15px; }
    .MiniPaddedLeftSpan { padding-left: 15px; }
    .MiniPaddedRightSpan { padding-right: 5px; }
    .underline { text-decoration: underline; }
    .validation-error { color: red; display: none; }
    .left-padding { padding-left: 4px; }
    .width-138 { width: 138px; }
</style>
<script type="text/javascript" src="../../inc/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="../../inc/jQuery.tablesorter.pager.lo.js"></script>

<script>

    var gCurrentSort = "Name";
    var gCurrentGroupSort = "Group";
    var gAllowGroup = false;
    var gPmlBrokerList = new Object();
    var ActivePmlBrokerId = null;
    var gActiveRow = null;
    var CompFunc = new Object();
    var oPaging = new Object();
    var enforceValidTaxId = true;
    var PAGE_SIZE = <%= AspxTools.JsNumeric(PAGE_SIZE) %>;

    function _init() {
        select1stTab();
        f_SelCompanyView(false);

        f_loading(true);
        setTimeout(function(){
                    f_populate();
                f_loading(false);
            }
            , 10);
        $("#m_Edit_m_NmlsNameLckd").change(function(){
            setNmlsName();
        });

        $("#m_Edit_m_Name").change(function(){
            setNmlsName();
        });

        $("#CompanyNameSearchInput,#CompanyIDSearchInput").keyup(function(event){
            // If the user hits "Enter" when focused on the search inputs,
            // perform the search as if they had clicked the search button.
            if(event.keyCode == 13){
                $("#CompanySearchButton").click();
            }
        });
    }

    function setNmlsName()
    {
        var $NmlsNameLckd = $("#m_Edit_m_NmlsNameLckd");
        var $NmlsName = $("#m_Edit_m_NmlsName")
        $NmlsName.prop("disabled", !$NmlsNameLckd.prop("checked"));
        if(!$NmlsNameLckd.prop("checked"))
        {
            $NmlsName.val($("#m_Edit_m_Name").val());
        }
    }

    function f_onbeforeUnload() {
        if (arguments.length == 0){
            if (checkDirty())
                return UnloadMessage;
        }
        else{
            if(checkDirty())
            {
                return UnloadMessage;
            }
            else
                onClosePopup();
        }
    }

    window.onbeforeunload = f_onbeforeUnload;

    function f_exportAll() {
        var url = <%= AspxTools.SafeUrl(HttpContext.Current.Request.Url.AbsolutePath + "?export=1") %>;
        fetchFileAsync(url);
    }

    function select1stTab(){
            onTabClick(document.getElementById("TabA"));
    }
    function select2ndTab(){
            onTabClick(document.getElementById("TabB"));
    }
    function select3rdTab(){
            onTabClick(document.getElementById("TabC"));
    }
    function select4thTab(){
            onTabClick(document.getElementById("TabD"));
    }
    function select5thTab(){
            onTabClick(document.getElementById("TabE"));
    }
    function select6thTab(){
            onTabClick(document.getElementById("TabF"));
    }
    function select7thTab(){
            onTabClick(document.getElementById("TabG"));
    }
    function select8thTab(){
            onTabClick(document.getElementById("TabH"));
    }
    function selectServicesTab() {
            onTabClick(document.getElementById("TabS"));
        }

    function checkDirty()
    {
            var m_isDirty = document.getElementById("m_isDirty");
        if( isDirty() == true || m_isDirty != null && m_isDirty.value != "0" ) {
    		    return true;
        }

    	return false;
    }

    function ClearDirty(){
        var m_isDirty = document.getElementById("m_isDirty");
        var isUpdate = document.getElementById("IsUpdate");
        if (typeof m_isDirty != 'undefined' && m_isDirty != null) <%-- //Clear dirty flags --%>
    	    m_isDirty.value = "0";
    	if (typeof isUpdate != 'undefined' && isUpdate != null)
    	    isUpdate.value = "0";
    }
    function setDirty(){
            var isUpdate = document.getElementById("IsUpdate");
            if (typeof isUpdate != 'undefined' && isUpdate != null)
    	        isUpdate.value = "1";
    }

    function onCancel()
    {
        ClearDirty();
        f_SelCompanyView(false);
        f_renderPaging();
        $("#m_Table").css("height", "100%");
    }

    function onTabClick(oTab) {
            if (oTab == null || oTab.id.indexOf("Tab") == -1)
                return;

            var tabA = document.getElementById("TabA");
            var baseA = document.getElementById("BaseA");
            var tabB = document.getElementById("TabB");
            var baseB = document.getElementById("BaseB");
            var tabC = document.getElementById("TabC");
            var baseC = document.getElementById("BaseC");
            var tabD = document.getElementById("TabD");
            var baseD = document.getElementById("BaseD");
            var tabE = document.getElementById("TabE");
            var baseE = document.getElementById("BaseE");
            var tabF = document.getElementById("TabF");
            var baseF = document.getElementById("BaseF");
            var tabG = document.getElementById("TabG");
            var baseG = document.getElementById("BaseG");
            var tabH = document.getElementById('TabH');
            var baseH = document.getElementById("BaseH");

            tabA.style.backgroundColor = "darkgray";
            tabA.style.color = "whitesmoke";
            tabA.style.borderBottom = "2px groove";
            baseA.style.display = "none";
            tabB.style.backgroundColor = "darkgray";
            tabB.style.color = "whitesmoke";
            tabB.style.borderBottom = "2px groove";
            baseB.style.display = "none";
            tabC.style.backgroundColor = "darkgray";
            tabC.style.color = "whitesmoke";
            tabC.style.borderBottom = "2px groove";
            baseC.style.display = "none";
            tabD.style.backgroundColor = "darkgray";
            tabD.style.color = "whitesmoke";
            tabD.style.borderBottom = "2px groove";
            baseD.style.display = "none";
            tabE.style.backgroundColor = "darkgray";
            tabE.style.color = "whitesmoke";
            tabE.style.borderBottom = "2px groove";
            baseE.style.display = "none";
            tabF.style.backgroundColor = "darkgray";
            tabF.style.color = "whitesmoke";
            tabF.style.borderBottom = "2px groove";
            baseF.style.display = "none";
            tabG.style.backgroundColor = "darkgray";
            tabG.style.color = "whitesmoke";
            tabG.style.borderBottom = "2px groove";
            baseG.style.display = "none";
            tabH.style.backgroundColor = "darkgray";
            tabH.style.color = "whitesmoke";
            tabH.style.borderBottom = "2px groove";
            baseH.style.display = "none";

            var tabS = document.getElementById("TabS");
            var baseS = document.getElementById("BaseS");
            tabS.style.backgroundColor = "darkgray";
            tabS.style.color = "whitesmoke";
            tabS.style.borderBottom = "2px groove";
            baseS.style.display = "none";

            oTab.style.borderBottom = "0px";
            oTab.style.backgroundColor = "gainsboro";
            oTab.style.color = "";

            switch (oTab.id) {
                case "TabA":
                    baseA.style.display = "block";
                    break;
                case "TabB":
                    baseB.style.display = "block";
                    break;
                case "TabC":
                    baseC.style.display = "block";
                    break;
                case "TabD":
                    baseD.style.display = "block";
                    break;
                case "TabE":
                    baseE.style.display = "block";
                    break;
                case "TabF":
                    baseF.style.display = "block";
                    break;
                case "TabG":
                    baseG.style.display = "block";
                    break;
                case "TabH":
                    baseH.style.display = "block";
                    break;
                case "TabS":
                    baseS.style.display = "block";
                    break;
            }
        }

    function disableStatusDropdown(role, checked)
    {
        var dropdown = null;

        if (role === 'Broker') {
            dropdown = $(<%=AspxTools.JsGetElementById(BrokerStatusDDL)%>);
        }
        else if (role === 'MiniCorr') {
            dropdown = $(<%=AspxTools.JsGetElementById(MiniCorrStatusDDL)%>);
        }
        else if (role === 'Corr') {
            dropdown = $(<%=AspxTools.JsGetElementById(CorrStatusDDL)%>);
        }
        else
        {
            throw new Error("Unhandled role " + role);
        }

        dropdown.prop("disabled", checked === false).css("background-color", "").val(checked ?
            <%=AspxTools.JsString(DataAccess.OCStatusType.Blank)%> :
            <%=AspxTools.JsString(DataAccess.OCStatusType.Inactive)%>);
    }

    function onSearchBtnClicked() {
        var companyNameFilter = $("#CompanyNameSearchInput").val();
        var companyIDFilter = $("#CompanyIDSearchInput").val();
        var companyRoleFilter = $(<%=AspxTools.JsGetElementById(CompanyRoleSearchDDL)%>).val();
        var companyIndexFilter = $("#CompanyIndexNumberSearch").val();
        var brokerStatusFilter = $(<%=AspxTools.JsGetElementById(BrokerStatusSearchDDL)%>).val();
        var miniCorrStatusFilter = $(<%=AspxTools.JsGetElementById(MiniCorrStatusSearchDDL)%>).val();
        var corrStatusFilter = $(<%=AspxTools.JsGetElementById(CorrStatusSearchDDL)%>).val();

        // Populating the table with the search results may accidently set
        // the dirty bit. We'll clear it before populating to prevent this.
        ClearDirty();
        f_populate(1, gCurrentSort, companyNameFilter, companyIDFilter, companyRoleFilter, companyIndexFilter, brokerStatusFilter, miniCorrStatusFilter, corrStatusFilter);
    }

    function onClearBtnClicked() {
        // Clearing the search results may accidently set the dirty bit.
        // We'll clear it before populating to prevent this.
        ClearDirty();

        f_populate(1, gCurrentSort);
    }

    function f_save(action) {
        if (ActivePmlBrokerId == null){
            if(action == 'ok'){
                f_onbeforeUnload('ok');
            }
            $("#m_Table").css("height", "100%");
            return true;
        }

        if (!f_validate())
            return false;

        if(!checkDirty()){
            f_SelCompanyView(false);
            if(action == 'ok'){
                f_onbeforeUnload('ok');
            }
            $("#m_Table").css("height", "100%");
            return true;
        }

        f_loading(true, "Saving...");
        setTimeout(function(){
                if (f_saveBroker()){
                    f_SelCompanyView(false);
    	            ClearDirty();

                    if (action == 'ok')
                        f_onbeforeUnload('ok');
                }
                else {
                    setDirty();
                }
            $("#m_Table").css("height", "100%");
            f_loading(false);
        }, 10);

    }

    function addEvent(obj, evType, fn, useCapture) {
            addEventHandler(obj, evType, fn, useCapture);
            return true;
    }

    function __CreateAndAppend(sTag, oParent) {
        var oItem = document.createElement(sTag);
        oParent.appendChild(oItem);
        return oItem;
    }


    function f_SelCompanyView(companyEditMode) {
        if (companyEditMode) {
            $(".ListMode,#pagingDiv").hide();

            $(".CompanyEditMode").show();

            select1stTab();
        }
        else {
            ActivePmlBrokerId = null;

            $(".ListMode").show();

            $(".CompanyEditMode").hide();

                if (gActiveRow != null) {
                    gActiveRow.style.backgroundColor = "";
                }
                gActiveRow = null;
        }
    }

    function f_populate(pageId, sortBy, companyNameFilter, companyIDFilter, companyRoleFilter, companyIndexFilter, brokerStatusFilter, miniCorrStatusFilter, corrStatusFilter) {
        var nRowCount = 0;
        var oResults = null;
        var args = new Object();

        if (typeof pageId != 'undefined'){
            args["page"] = pageId;
        }
        if (typeof sortBy != 'undefined'){
            args["SortBy"] = sortBy;
        }
        if (typeof companyNameFilter != 'undefined'){
            args["CompanyNameFilter"] = companyNameFilter;
        }
        if (typeof companyIDFilter != 'undefined'){
            args["CompanyIDFilter"] = companyIDFilter;
        }
        if (typeof companyRoleFilter == 'undefined'){
            companyRoleFilter = "ANY";
        }
        if (typeof companyIndexFilter == 'undefined') {
            companyIndexFilter = -1;
        }
        if (typeof brokerStatusFilter == 'undefined') {
            brokerStatusFilter = -1;
        }
        if (typeof miniCorrStatusFilter == 'undefined') {
            miniCorrStatusFilter = -1;
        }
        if (typeof corrStatusFilter == 'undefined') {
            corrStatusFilter = -1;
        }

        args["CompanyRoleFilter"] = companyRoleFilter;
        args["CompanyIndexFilter"] = companyIndexFilter;
        args["BrokerStatusFilter"] = brokerStatusFilter;
        args["MiniCorrStatusFilter"] = miniCorrStatusFilter;
        args["CorrStatusFilter "] = corrStatusFilter;

        var result = gService.PmlCompany.call("GetPmlCompanyList", args);
        if (!result.error) {
    	       oResults = JSON.parse(result.value.companyList);
    	       oPaging.currentPage = result.value.companyListPageNumber;
    	       oPaging.totalResults = result.value.companyListSize;
    	       oPaging.sortBy = result.value.companyListSortBy;
    	       oPaging.enabled = (oPaging.totalResults > PAGE_SIZE);
    	       f_buildGroupList(result.value);
    	}
        else {
            alert(result.UserMessage);
    	}

        if (oResults != null) {
    	    nRowCount = oResults.length;
    	    gPmlBrokerList = new Object();
    		for (var i = 0; i < nRowCount; i++) {
    		    var broker = new PmlBroker(oResults[i]);
    		    gPmlBrokerList[broker.Id] = broker;
    		}
    	}
    	f_render();

    	var MinAmountEnabled = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmountEnabled)%>;
    	var MaxAmountEnabled = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmountEnabled)%>;
    	var MinAmountVal = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmount)%>.value;
    	var MaxAmountVal = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmount)%>.value;
    	var ZeroDollarStr = <%= AspxTools.JsString(m_ZeroDollarStr)%>
    	MinAmountEnabled.checked = MinAmountVal.length > 0 && MinAmountVal != ZeroDollarStr;
    	MaxAmountEnabled.checked = MaxAmountVal.length > 0 && MaxAmountVal != ZeroDollarStr;
    	setAmountTextboxStatus(<%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmountEnabled)%> );
    	setAmountTextboxStatus(<%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmountEnabled)%> );
    }

    // COMPARISON FUNCTIONS
    CompFunc["Name"] = function(a, b) {
      try{
            var x = a.Name.toLowerCase();
            var y = b.Name.toLowerCase();
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
      }catch(e){ return 0;}
    }

    CompFunc["NameDesc"] = function(a, b) {
      try{
            var x = a.Name.toLowerCase();
            var y = b.Name.toLowerCase();
            return ((x < y) ? 1 : ((x > y) ? -1 : 0));
      }catch(e){ return 0;}
    }

    CompFunc["CompanyId"] = function(a, b) {
      try{
            var x = a.CompanyId.toLowerCase();
            var y = b.CompanyId.toLowerCase();
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
      }catch(e){ return 0;}
    }

    CompFunc["CompanyIdDesc"] = function(a, b) {
      try{
            var x = a.CompanyId.toLowerCase();
            var y = b.CompanyId.toLowerCase();
            return ((x < y) ? 1 : ((x > y) ? -1 : 0));
      }catch(e){ return 0;}
    }

    CompFunc["LpePriceGroupName"] = function(a, b) {
      try{
            var x = a.LpePriceGroupName.toLowerCase();
            var y = b.LpePriceGroupName.toLowerCase();
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
      }catch(e){ return 0;}
    }

    CompFunc["LpePriceGroupNameDesc"] = function(a, b) {
      try{
            var x = a.LpePriceGroupName.toLowerCase();
            var y = b.LpePriceGroupName.toLowerCase();
            return ((x < y) ? 1 : ((x > y) ? -1 : 0));
      }catch(e){ return 0;}
    }

    CompFunc["BranchNm"] = function(a, b) {
      try{
            var x = a.BranchNm.toLowerCase();
            var y = b.BranchNm.toLowerCase();
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
      }catch(e){ return 0;}
    }

    CompFunc["BranchNmDesc"] = function(a, b) {
      try{
            var x = a.BranchNm.toLowerCase();
            var y = b.BranchNm.toLowerCase();
            return ((x < y) ? 1 : ((x > y) ? -1 : 0));
      }catch(e){ return 0;}
    }

    //END COMPARISON FUNCTIONS

    function f_sortGrid(txt){
        switch(txt){
            case "Name":
                if (gCurrentSort.indexOf("Name") >= 0){
                    gCurrentSort = (gCurrentSort == "Name") ? "NameDesc" : "Name";
                }
                else gCurrentSort = "Name";
                break;
            case "CompanyId":
                if (gCurrentSort.indexOf("CompanyId") >= 0){
                    gCurrentSort = (gCurrentSort == "CompanyId") ? "CompanyIdDesc" : "CompanyId";
                }
                else gCurrentSort = "CompanyId";
                break;
            case "LpePriceGroupName":
                if (gCurrentSort.indexOf("LpePriceGroupName") >= 0){
                    gCurrentSort = (gCurrentSort == "LpePriceGroupName") ? "LpePriceGroupNameDesc" : "LpePriceGroupName";
                }
                else gCurrentSort = "LpePriceGroupName";
                break;
            case "BranchNm":
                if (gCurrentSort.indexOf("BranchNm") >= 0){
                    gCurrentSort = (gCurrentSort == "BranchNm") ? "BranchNmDesc" : "BranchNm";
                }
                else gCurrentSort = "BranchNm";
                break;
        }
        if (oPaging.enabled){
            f_loading(true);
            setTimeout(function(){
                    f_populate(1, gCurrentSort);
                f_loading(false);
            }, 10);

        }
        else{
            f_render();
        }

    }

    function f_UpdateSortImage(){
        var imgSortName = document.getElementById("imgSortName");
        var imgSortCompanyId = document.getElementById("imgSortCompanyId");
        var imgSortPriceGroup = document.getElementById("imgSortPriceGroup");
        var imgSortBranch = document.getElementById("imgSortBranch");
        var bDesc = (gCurrentSort.indexOf("Desc") >= 0);

        var asc = "../../images/Tri_ASC.gif";
        var desc = "../../images/Tri_DESC.gif";

        imgSortName.style.display = "none";
        imgSortCompanyId.style.display = "none";
        imgSortPriceGroup.style.display = "none";
        imgSortBranch.style.display = "none";

        // Check if string starts with sort key
        if( gCurrentSort.indexOf("Name") == 0 )
        {
            imgSortName.style.display = "";
            imgSortName.src = bDesc ? desc : asc;
        }
        else if( gCurrentSort.indexOf("CompanyId") == 0 )
        {
            imgSortCompanyId.style.display = "";
            imgSortCompanyId.src = bDesc ? desc : asc;
        }
        else if( gCurrentSort.indexOf("LpePriceGroupName") == 0 )
        {
            imgSortPriceGroup.style.display = "";
            imgSortPriceGroup.src = bDesc ? desc : asc;
        }
        else if( gCurrentSort.indexOf("BranchNm") == 0 )
        {
            imgSortBranch.style.display = "";
            imgSortBranch.src = bDesc ? desc : asc;
        }
    }

    function f_renderPaging(){
        var pDiv = document.getElementById('pagingDiv');
        var pResults = document.getElementById('pagingResults');
        var pLimits = document.getElementById('pagingLimits');
        var pTotal = document.getElementById('pagingTotal');
        var pagesDiv = document.getElementById('pagesDiv');

        if (!oPaging.enabled){
            pDiv.style.display = "none";
            pagesDiv.style.display = "none";
        }
        else{
            pTotal.innerText = oPaging.totalResults;
                var start = (oPaging.currentPage-1) * PAGE_SIZE + 1;
                var end = start - 1 + PAGE_SIZE;
                if (end > Math.floor(oPaging.totalResults)){
                    end = oPaging.totalResults;
                }
            pLimits.innerText = (start == end) ? start : (start + "-" + end);
            pResults.innerText = (start == end) ? "Result" : "Results";
            gCurrentSort = oPaging.sortBy;
            pDiv.style.display = '';

            var nPages = Math.floor(oPaging.totalResults / PAGE_SIZE);
            if (oPaging.totalResults % PAGE_SIZE != 0) nPages++;

            var MAX_PAGES = 9;
            var dev = Math.floor(MAX_PAGES/2);
            var startIndex = 1;
            var endIndex = nPages;

            if (nPages > MAX_PAGES){
                var loIndex = oPaging.currentPage - dev;
                var hiIndex = parseInt(oPaging.currentPage, 10) + dev;
                startIndex = (loIndex < startIndex ) ? startIndex : loIndex;
                endIndex = (hiIndex > endIndex ) ? endIndex : hiIndex;
            }

            pagesDiv.innerHTML = "Page:&nbsp;";
            if (startIndex > 1){
                var oSpan = __CreateAndAppend("span", pagesDiv);
                oSpan.innerHTML = "...";
            }
            for (var i=startIndex; i<=endIndex; i++){
                if (i == oPaging.currentPage){
                    var oSpan = __CreateAndAppend("span", pagesDiv);
                    oSpan.innerText = i;
                }
                else{
                    var oLink = __CreateAndAppend("a", pagesDiv);
                    oLink.innerText = i;
                    oLink.href = "#";
                    addEvent(oLink, 'click', function(pageId) {
                        return function() {
                            f_switchPage(pageId);
                        }
                    } (i));
                }

                if (i != endIndex){
                    var oSpan = __CreateAndAppend("span", pagesDiv);
                    oSpan.innerHTML = "&nbsp;";
                }
            }
            if (endIndex < nPages){
                var oSpan = __CreateAndAppend("span", pagesDiv);
                oSpan.innerHTML = "...";
            }

            pagesDiv.style.display = '';
        }

    }

    function f_switchPage(pageId){
        f_loading(true);
        setTimeout(function(){
                f_populate(pageId, gCurrentSort);
            f_loading(false);
        }, 10);
    }

    function f_render(){
        var oTable = document.getElementById("m_Table");
        var oTbody = document.getElementById("m_Grid");
        var oEmptyMsg = document.getElementById("m_Empty");
        var brokerArray = new Array();

        for (var prop in gPmlBrokerList){
            if (gPmlBrokerList.hasOwnProperty(prop)){
                brokerArray.push(gPmlBrokerList[prop]);
            }
        }

        f_renderPaging();

        var sortFunc = CompFunc["Name"];
        if (typeof CompFunc[gCurrentSort] === 'function')
            sortFunc = CompFunc[gCurrentSort];

        brokerArray.sort(sortFunc);
        f_UpdateSortImage();
        clearGrid();

        for (var i=0; i<brokerArray.length; i++){
            f_addRow(oTbody, brokerArray[i]);
        }

        oTable.style.display = (brokerArray.length == 0) ? "none" : "";
        oEmptyMsg.style.display = (brokerArray.length == 0) ? "" : "none";
        if (brokerArray.length == 0)
            oEmptyMsg.innerText = "No PML Originating Companies found.";
    }

    function f_addRow(oTable, oBroker) {
        var oCell = null;
        var o;
        var oRow = __CreateAndAppend("tr", oTable);
        oRow.className = ((oTable.rows.length % 2) == 0) ? "GridAlternatingItem" : "GridItem";
        oRow.id = "GridRow" + oTable.rows.length;
        if (ActivePmlBrokerId == oBroker.Id){
            gActiveRow = oRow;
            gActiveRow.style.backgroundColor = "LightBlue";
        }

        oCell = __CreateAndAppend("td", oRow);
        oCell.setAttribute("align", "center")
        oCell.vAlign = "top"
        o = __CreateAndAppend("a", oCell);
        o.innerText = "edit";
        o.href = "#";
        addEvent(o, 'click', function(id, rowId) {
            return function() {
                f_CheckAndEdit(id, rowId);
            }
        } (oBroker.Id, oRow.id));

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign="top"
        o = __CreateAndAppend("span", oCell);
        o.innerText = oBroker.Name
        o.className = "cellName";
        o.onmouseover = function(){this.style.color = 'lightblue';};
        o.onmouseout = function(){this.style.color = '';};
        addEvent(o, 'click', function(id, rowId) {
            return function() {
                f_CheckAndEdit(id, rowId);
            }
        } (oBroker.Id, oRow.id));


        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = oBroker.CompanyIndexNumber;

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = oBroker.CompanyId;        

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = oBroker.CompanyRoles;
        $(oCell).css("white-space", "pre");

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = oBroker.CompanyStatus;
        $(oCell).css("white-space", "pre");

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = oBroker.CompanyColorTheme;
        $(oCell).css("white-space", "pre");

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = oBroker.Groups;

        var h = hypescriptDom;
        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerHTML = "";
        oCell.appendChild(h("<>", {}, [oBroker.Street, h("br"), oBroker.City, ", ", oBroker.State, " ", oBroker.Zip]));

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = oBroker.Phone;

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = oBroker.Fax;

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = oBroker.LpePriceGroupName;
        $(oCell).css("white-space", "pre");

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = oBroker.BranchNm;
        $(oCell).css("white-space", "pre");

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.setAttribute("align", "center")
        o = __CreateAndAppend("a", oCell);
        o.innerText = "add new PML user";
        o.href = "#";
        addEvent(o, 'click', function(id) {
            return function() {
                showModal( "/los/BrokerAdmin/EditPmlUser.aspx?cmd=add&pmlBrokerId=" + id, null, null, null, null,{ width:890, hideCloseButton: true });
            }
        } (oBroker.Id));

        return oRow;
    }

    function clearGrid() {
        var oTbody = document.getElementById("m_Grid");
        var nRowCount = oTbody.rows.length;
        for (var i = 0; i < nRowCount; i++) {
            oTbody.deleteRow(0);
        }
    }

    function SetServiceCredential(pmlBrokerId, serviceCredentialsJson) {
        $('#ServiceCredentialsJson').val(typeof(serviceCredentialsJson) === 'string' ? serviceCredentialsJson : '');

        var isNew = typeof(pmlBrokerId) !== 'string';
        if(!isNew) {
            $('#ServiceCredentialsDiv').show();
            $("#ServiceCredentialsNewBranchDiv").hide();
            var serviceCredentialSettings = {
                ServiceCredentialJsonId: "ServiceCredentialsJson",
                ServiceCredentialTableId: "ServiceCredentialsTable",
                OriginatingCompanyId: pmlBrokerId
            };

            ServiceCredential.Initialize(serviceCredentialSettings);
            ServiceCredential.RenderCredentials();
        }
        else {
            $('#ServiceCredentialsDiv').hide();
            $("#ServiceCredentialsNewBranchDiv").show();
        }
    }

    function AddServiceCredentialBtnClick() {
        ServiceCredential.AddServiceCredential()
    }

    function f_editBroker(id, rowId) {
        var oResults = null;
        var oLicenses = null;
        var oRow = document.getElementById(rowId);
        var args = new Object();

        args["id"] = id;

        $("#m_Table").css("height", "200px");

        var result = gService.PmlCompany.call("GetPmlCompanyInfo", args);
        if (!result.error) {
                oResults = JSON.parse(result.value.companyInfo);
        }
        else{
            alert('Unable to retrieve PML Originating Company information.\nPlease try it again in a few moments.');
        }

        if (oResults != null) {
                if (gActiveRow != null){
                    gActiveRow.style.backgroundColor = "";
                }
                gActiveRow = oRow;
                gActiveRow.style.backgroundColor = "LightBlue";

            f_populateBroker(oResults, result.value);
            ClearDirty();
        }

         setNmlsName();

    }

    function f_populateBroker(oBrokerData, oResults) {
            ActivePmlBrokerId = oBrokerData[0];
            <%=AspxTools.JsGetElementById(m_Name) %>.value = oBrokerData[1];
            <%=AspxTools.JsGetElementById(m_companyId) %>.value = oBrokerData[2];
            <%=AspxTools.JsGetElementById(m_Street) %>.value = oBrokerData[3];
            <%=AspxTools.JsGetElementById(m_City) %>.value = oBrokerData[4];
            <%=AspxTools.JsGetElementById(m_State) %>.value = oBrokerData[5];
            <%=AspxTools.JsGetElementById(m_Zipcode) %>.value = oBrokerData[6];
            <%=AspxTools.JsGetElementById(m_Phone) %>.value = oBrokerData[7];
            <%=AspxTools.JsGetElementById(m_Fax) %>.value = oBrokerData[8];
            <%=AspxTools.JsGetElementById(m_taxId) %>.value = oBrokerData[9];
            <%=AspxTools.JsGetElementById(m_principal) %>.value = oBrokerData[10];
            <%=AspxTools.JsGetElementById(m_principal2) %>.value = oBrokerData[11];
            document.getElementById("m_Edit_LicensesPanel_tbNmlsIdentifier").value = oBrokerData[12];
            <%=AspxTools.JsGetElementById(m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo) %>.checked = oBrokerData[22] == '1';
            <%=AspxTools.JsGetElementById(m_OriginatorCompensationPercent) %>.value = oBrokerData[14];
            <%=AspxTools.JsGetElementById(m_OriginatorCompensationBaseT) %>.value = oBrokerData[15];
            <%=AspxTools.JsGetElementById(m_OriginatorCompensationMinAmount) %>.value = oBrokerData[16];
            <%=AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmount) %>.value = oBrokerData[17];
            <%=AspxTools.JsGetElementById(m_OriginatorCompensationFixedAmount) %>.value = oBrokerData[18];
            <%=AspxTools.JsGetElementById(m_OriginatorCompensationNotes) %>.value = oBrokerData[19];

            // OPM 174647
            var MinAmountEnabled = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMinAmountEnabled)%>;
    	    var MaxAmountEnabled = <%= AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmountEnabled)%>;
    	    var ZeroDollarStr = <%= AspxTools.JsString(m_ZeroDollarStr)%>
    	    MinAmountEnabled.checked = oBrokerData[16].length > 0 && oBrokerData[16] != ZeroDollarStr;
    	    MaxAmountEnabled.checked = oBrokerData[17].length > 0 && oBrokerData[17] != ZeroDollarStr;
    	    setAmountTextboxStatus(MinAmountEnabled);
    	    setAmountTextboxStatus(MaxAmountEnabled);

            for (var i = 1; i <= 5; i++) {
                var fieldId = 'CustomPricingPolicyField' + i;
                $('.' + fieldId).val(oResults[fieldId]);
            }

            <%=AspxTools.JsGetElementById(m_NmlsName) %>.value = oResults['NmlsName'];
            <%=AspxTools.JsGetElementById(m_NmlsNameLckd) %>.checked = oResults['NmlsNameLckd'] == "True";

            var brokerId = oBrokerData[21];

            //OC Roles
            if (oResults['BrokerRole'] == "True") {
                <%=AspxTools.JsGetElementById(BrokerRole) %>.checked = true;
                $(<%= AspxTools.JsGetElementById(BrokerStatusDDL) %>).val(oResults['BrokerRoleStatusT']);
            }
            else {
                <%=AspxTools.JsGetElementById(BrokerRole) %>.checked = false;
                $(<%=AspxTools.JsGetElementById(BrokerStatusDDL)%>).prop("disabled", true).val(<%=AspxTools.JsString(DataAccess.OCStatusType.Inactive)%>);
            }

            if (oResults['MiniCorrespondentRole'] == "True") {
                <%= AspxTools.JsGetElementById(MiniCorrespondentRole) %>.checked = true;
                $(<%= AspxTools.JsGetElementById(MiniCorrStatusDDL) %>).val(oResults['MiniCorrRoleStatusT']);
            }
            else {
                <%= AspxTools.JsGetElementById(MiniCorrespondentRole) %>.checked = false;
                $(<%=AspxTools.JsGetElementById(MiniCorrStatusDDL)%>).prop("disabled", true).val(<%=AspxTools.JsString(DataAccess.OCStatusType.Inactive)%>);
            }

            if (oResults['CorrespondentRole'] == "True") {
                <%= AspxTools.JsGetElementById(CorrespondentRole) %>.checked = true;
                $(<%= AspxTools.JsGetElementById(CorrStatusDDL) %>).val(oResults['CorrRoleStatusT']);
            }
            else {
                <%= AspxTools.JsGetElementById(CorrespondentRole) %>.checked = false;
                $(<%=AspxTools.JsGetElementById(CorrStatusDDL)%>).prop("disabled", true).val(<%=AspxTools.JsString(DataAccess.OCStatusType.Inactive)%>);
            }

            <%= AspxTools.JsGetElementById(UnderwritingAuthority) %>.value = oResults['UnderwritingAuthority'];

            //Broker Relationships
            <%=AspxTools.JsGetElementById(m_TPOLandingPage) %>.value = oResults['TPOLandingPage'];

            if(oResults['LpePriceGroupId'] && oResults['LpePriceGroupId'].length > 0)
            {
                <%= AspxTools.JsGetElementById(m_LpePriceGroupId) %>.value = oResults['LpePriceGroupId'];
            }

            if(oResults['BranchId'] && oResults['BranchId'].length > 0)
            {
                <%= AspxTools.JsGetElementById(m_Branch) %>.value = oResults['BranchId'];
            }

            //Mini Correspondent Relationships
            <%=AspxTools.JsGetElementById(MiniCorrespondentTPOPortalConfigID) %>.value = oResults['MiniCorrespondentTPOPortalConfigID'];

            if(oResults['MiniCorrespondentLpePriceGroupId'] && oResults['MiniCorrespondentLpePriceGroupId'].length > 0)
    		{
    		    <%= AspxTools.JsGetElementById(MiniCorrespondentLpePriceGroupId) %>.value = oResults['MiniCorrespondentLpePriceGroupId'];
    		}

            if(oResults['MiniCorrespondentBranchId'] && oResults['MiniCorrespondentBranchId'].length > 0)
    		{
                <%= AspxTools.JsGetElementById(MiniCorrespondentBranchId) %>.value = oResults['MiniCorrespondentBranchId'];
    		}

            //Correspondent Relationships
    		<%=AspxTools.JsGetElementById(CorrespondentTPOPortalConfigID) %>.value = oResults['CorrespondentTPOPortalConfigID'];

            if(oResults['CorrespondentLpePriceGroupId'] && oResults['CorrespondentLpePriceGroupId'].length > 0)
    		{
    		    <%= AspxTools.JsGetElementById(CorrespondentLpePriceGroupId) %>.value = oResults['CorrespondentLpePriceGroupId'];
    		}

            if(oResults['CorrespondentBranchId'] && oResults['CorrespondentBranchId'].length > 0)
    		{
                <%= AspxTools.JsGetElementById(CorrespondentBranchId) %>.value = oResults['CorrespondentBranchId'];
    		}

            <%= AspxTools.JsGetElementById(m_companyTier) %>.value = oResults['PmlCompanyTierId'];

            if (oResults['TpoColorThemeId'] && oResults['TpoColorThemeId'].length > 0)
            {
                <%= AspxTools.JsGetElementById(this.TpoColorThemeId) %>.value = oResults['TpoColorThemeId'];
            }

            <%=AspxTools.JsGetElementById(this.IndexNumber)%>.value = oResults['IndexNumber'];

            if (typeof executePostPopulateFormCallback === 'function') {
                executePostPopulateFormCallback(oResults);
            }

            var xmlData = oResults['OriginatorCompensationAuditData'];

            f_DisplayAuditHistory(xmlData, brokerId, ActivePmlBrokerId);

            <%= AspxTools.JsGetElementById(GroupAssociationList) %>.value = oResults['GroupAssociationList'];
            LoadGroupList();

            var affList = JSON.parse(oResults['AffiliateList']);
            LoadAffGrid(affList);

            var TpoNavigationPermissions = oResults['TpoNavigationPermissions'].split(',');
            $('.TpoNavigationPermissions').each( function() {
                $(this).prop('checked', $.inArray($(this).val(), TpoNavigationPermissions) != -1);
            });

            <%=AspxTools.JsGetElementById(m_Name) %>.style.backgroundColor = 'white';
            <%=AspxTools.JsGetElementById(m_Street) %>.style.backgroundColor = 'white';
            <%=AspxTools.JsGetElementById(m_City) %>.style.backgroundColor = 'white';
            <%=AspxTools.JsGetElementById(m_State) %>.style.backgroundColor = 'white';
            <%=AspxTools.JsGetElementById(m_Zipcode) %>.style.backgroundColor = 'white';
            <%=AspxTools.JsGetElementById(m_Phone) %>.style.backgroundColor = 'white';
            if (typeof AreLicensesValid === 'function')
                AreLicensesValid();

            f_SelCompanyView(true);

            setRelationships("BaseE", oResults["OcBrokerRelationships"]);

            setRelationships("BaseF", oResults["OcMiniCorrRelationships"]);

            setRelationships("BaseG", oResults["OcCorrRelationships"]);

            setPermissions(JSON.parse(oResults["BrokerPermissions"]));

            enforceValidTaxId = oResults["TaxIdValid"] === 'True';

            var $taxId = $(<%=AspxTools.JsGetElementById(m_taxId) %>);

            if (oResults["TaxIdValid"] !== 'True') {
                if ($taxId.attr('preset')) {
                    _removeMask($taxId.get(0));
                    $taxId.removeAttr('preset');
                }

                alert('The Tax ID is incorrectly formatted. Please update it to a 9 digit ID '
                    + '(12-3456789) to have it populate to loan files.');
            } else {
                if (!$taxId.attr('preset')) {
                    $taxId.attr('preset', 'employerIdentificationNumber');
                    _initMask($taxId.get(0));
                }
            }

            SetServiceCredential(ActivePmlBrokerId, oResults["ServiceCredentialsJson"]);
        }

    function setRelationships(tabBase, json)
    {
        // Because we have three relationship user controls corresponding
        // to the Broker, Mini-Correspondent, and Correspondent roles, we
        // can't use just the ID of the inputs to render the relationships
        // on the page. Instead, we'll use class attributes to obtain the
        // inputs corresponding to employee ID and employee name and iterate
        // through them, using the order supplied by the service page.
        var ocRelationships = JSON.parse(json);

        var relationshipVisibleTextSpans = $("#" + tabBase + " .RoleChooserLinkName");

        var relationshipNameHiddenInputs = $("#" + tabBase + " .RoleChooserLinkText");

        var relationshipDataHiddenInputs = $("#" + tabBase + " .RoleChooserLinkData");

        for (var i = 0; i < relationshipVisibleTextSpans.length; i++) {
            relationshipVisibleTextSpans[i].innerText = ocRelationships[i].Name;
            relationshipNameHiddenInputs[i].value = ocRelationships[i].Name;
            relationshipDataHiddenInputs[i].value = ocRelationships[i].Id;
        }
    }

    function setPermissions(brokerPermissions)
    {
        var checkboxes = [
            "CanApplyForIneligibleLoanPrograms",
            "CanRunPricingEngineWithoutCreditReport",
            "CanSubmitWithoutCreditReport",
            "AllowOrder4506T"
        ];

        for (var i = 0; i < checkboxes.length; i++) {
            $("#" + checkboxes[i]).prop("checked", brokerPermissions[checkboxes[i]]);
        }

        var allowTotalScorecardAccess = brokerPermissions["CanAccessTotalScorecard"];

        $("#CanAccessTotalScorecard").prop("checked", allowTotalScorecardAccess);

        $("#CanAccessTotalScorecardNo").prop("checked", allowTotalScorecardAccess === false);

        var sendTaskEmail = brokerPermissions["TaskEmailOption"] ===
            <%=AspxTools.JsNumeric(DataAccess.E_TaskRelatedEmailOptionT.ReceiveEmail)%>;

        $("#ReceiveEmail").prop("checked", sendTaskEmail);

        $("#ReceiveEmailNo").prop("checked", sendTaskEmail === false);

        setClickHandlers();
    }

    function setClickHandlers()
    {
    	$("#CanRunPricingEngineWithoutCreditReport").click(onCanRunPricingChanged);

    	onCanRunPricingChanged();
    }

    function onCanRunPricingChanged()
    {
    	var canRunCheckbox = $("#CanRunPricingEngineWithoutCreditReport");

    	var canRegisterLockCheckbox = $("#CanSubmitWithoutCreditReport");

    	if (canRunCheckbox.prop("checked")) {
    		canRegisterLockCheckbox.prop("disabled", false);
    	}
    	else {
    		canRegisterLockCheckbox.prop("disabled", true).prop("checked", false);
    	}
    }

    function ViewAudit(brokerId, pmlBrokerId, auditId)
    {
        var url = <%=AspxTools.JsString(m_PopupPath)%> + 'ViewOriginatorCompensationAuditEvent.aspx?pmlBrokerId=' + pmlBrokerId + '&brokerId=' + brokerId + ' &eventId=' + auditId;
        showModal(url);
    }

    //Transform the Audit history XML into paged HTML table. AM
    function f_DisplayAuditHistory(xmlData, brokerId, pmlBrokerId)
    {
            if($($.parseXML(xmlData)).find("AuditEvent").length == 0)
            {
                $(".pageNumbers").empty();
                $("#divAuditHistory").html("No entries in change history");
                return;
            }

            var auditEvents = getAuditEventFromXml();
            
            var html = (
            '<table id="tblAuditHistory" class="DataGrid" cellspacing="0" cellpadding="3" rules="all" border="1" style="width:100%;border-collapse:collapse;">\
                <thead><tr class="GridHeader" style="color:black;">\
                    <th>Date</th>\
                    <th>By</th>\
                    <th>Notes</th>\
                    <th></th>\
                </tr></thead>\
                <tbody></tbody>\
            </table>'
            );
            
            var tblAuditHistory = $(html);
            var h = hypescriptDom;
            tblAuditHistory.find("tbody").append($.map(auditEvents, function(event) {
                return h("tr", {className:"GridItem", vAlign:"top"}, [
                    h("td", {}, event.Date),
                    h("td", {}, event.By_rep),
                    h("td", {}, event.Notes),
                    h("td", {}, h("a", {href:"#", attrs:{"data-id":event.Id}, className:"viewHandler"}, "View"))
                ]);
            }))
            $("#divAuditHistory").empty().append(tblAuditHistory);

            tblAuditHistory.find("a.viewHandler").on("click", function(){
                var id = this.dataset != null ? this.dataset.id : this.attributes["data-id"].value;
                ViewAudit(brokerId, pmlBrokerId, id);
            });

            // TODO: tablesorter in IE 11 not work
            var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
            if (!isIE11) tblAuditHistory.tablesorter({ widthFixed: true }).tablesorterPager({container: $("#pager")});;

            function getAuditEventFromXml(){
                var xml = $.parseXML(xmlData);
                var auditEvents = [];
                for(var i = 0; i < xml.firstChild.childNodes.length; i++) {
                    var aEle = xml.firstChild.childNodes[i]; if (aEle.nodeType != 1) continue;

                    var auditEvent = {};
                    for(var j = 0; j < aEle.childNodes.length; j++) {
                        var attr = aEle.childNodes[j]; if (attr.nodeType != 1) continue;
                        auditEvent[attr.tagName] = attr.textContent || attr.text;
                    }
                    auditEvents.push(auditEvent)
                }
                return auditEvents;
            }
    }

    function f_buildGroupList(oResult)
    {
        var groupList = JSON.parse(oResult.groupList);
        gAllowGroup = groupList.length != 0;

        clearGroupGrid();
        var groupTable = document.getElementById('GroupsGrid');
        for ( var i = 0; i < groupList.length; i++)
        {
            f_addGroupRow(groupList[i], groupTable);
        }
    }
    function f_addGroupRow(rowData, oTable)
    {
        var oCell = null;
        var oRow = __CreateAndAppend("tr", oTable);
        oRow.className = ((oTable.rows.length % 2) == 0) ? "GridAlternatingItem" : "GridItem";

        oCell = __CreateAndAppend("td", oRow);
        oCell.setAttribute("align", "center")
        oCell.vAlign = "top"
        oCell.innerHTML = "";
        oCell.appendChild(hypescriptDom("input", {type:"checkbox", id: rowData[0], onclick: function(){ UpdateGroupList(); setDirty(); }}));

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = rowData[1]; // Group Name

        oCell = __CreateAndAppend("td", oRow);
        oCell.vAlign = "top"
        oCell.innerText = rowData[2]; // Group Description

        return oRow;
    }

    function f_sortGroupGrid()
    {
        gCurrentGroupSort = ( gCurrentGroupSort == "Group" ) ?  "GroupDesc" : "Group";

        var tbody = document.getElementById('GroupsGrid');
        var count = tbody.rows.length;
        var list = new Array(count);
        for (var i = 0; i < count; i++) {
          list.push(tbody.rows[i]);
        }

        list.reverse();

        for (var i = 0; i < count; i++)
        {
          tbody.appendChild(list[i]);
          list[i].className = ((i % 2) == 0) ? "GridItem" : "GridAlternatingItem";
        }

        var sortImage = document.getElementById('GroupSortImg');
        var asc = "../../images/Tri_ASC.gif";
        var desc = "../../images/Tri_DESC.gif";
        sortImage.src = ( gCurrentGroupSort == "Group" ) ? asc : desc;
        sortImage.style.display = "inline";
    }

    function clearGroupGrid() {
        var oTbody = document.getElementById("GroupsGrid");
        var nRowCount = oTbody.rows.length;
        for (var i = 0; i < nRowCount; i++) {
            oTbody.deleteRow(0);
        }
    }

    	function UpdateGroupList()
    	{
    	    var groupDiv = document.getElementById('GroupsDiv');
    	    var checks = groupDiv.getElementsByTagName('INPUT');
    	    var list = '';
    	    for (var i=0; i < checks.length; i++)
    	    {
    	        var oBox = checks[i];
    	        if ( oBox.checked )
    	            list += ( list=='' ? '':',') + oBox.id;
    	    }

    	    <%= AspxTools.JsGetElementById(GroupAssociationList) %>.value = list;
    	}

    	function LoadGroupList()
    	{ // in here.
    	    var oGroupsList = <%= AspxTools.JsGetElementById(GroupAssociationList) %>;
    	    if ( oGroupsList == null ) return;
    	    var groupsList = oGroupsList.value.split(',');
    	    var groupDiv = document.getElementById('GroupsDiv');
    	    var checks = groupDiv.getElementsByTagName('INPUT');
    	    var list = '';
    	    for (var i=0; i < checks.length; i++)
    	    {
    	        var oBox = checks[i];
    	        var found = false;
    	        for( var j = 0; j < groupsList.length; j++)
    	        {
    	            if (groupsList[j] == oBox.id)
    	            {
    	                oBox.checked = "checked";
    	                found = true;
    	                break;
    	            }
    	        }
    	        if ( found == false )
    	            oBox.checked = '';
    	    }
    	}

    function PmlBroker(<%-- /* id, name, companyid, companyrole, street, city, state, zip, phone, fax, groups, LpePriceGroupName, BranchNm, companyStatus, colorTheme, companyIndexNumber */ --%>){
        var obj = null;

        if (arguments[0].length == 16){
            obj = arguments[0];
        }
        else if (arguments.length == 16){
            obj = arguments;
        }

        if (obj.length != 16){
            return null;
        }

        this.Id = obj[0];
        this.Name = obj[1];
        this.CompanyId = obj[2];
        this.CompanyRoles = obj[3];
        this.Street = obj[4];
        this.City = obj[5];
        this.State = obj[6];
        this.Zip = obj[7];
        this.Phone = obj[8];
        this.Fax = obj[9];
        this.Groups = obj[10];
        this.LpePriceGroupName = obj[11];
        this.BranchNm = obj[12];
        this.CompanyStatus = obj[13];
        this.CompanyColorTheme = obj[14];
        this.CompanyIndexNumber = obj[15];
    }

    sTrim = function (str) {
        if (typeof str !== 'string' || !str)
            return str;
        return str.replace(/^\s*/, "").replace(/\s*$/, "");
    }

    function f_validate(){
        var hasErrors = false;

        var m_Name = <%=AspxTools.JsGetElementById(m_Name) %>;
        if (m_Name == null || sTrim(m_Name.value).length == 0){
            if (!hasErrors){
             select1stTab();
             alert('Enter a valid company name.');
            }
            hasErrors = true;
            m_Name.style.backgroundColor = 'pink';
        }
        else {
            m_Name.style.backgroundColor = 'white';
        }

        var m_NmlsName = <%=AspxTools.JsGetElementById(m_NmlsName) %>;
        if (m_NmlsName == null || sTrim(m_NmlsName.value).length == 0){
            if (!hasErrors){
             select1stTab();
             alert('Enter a valid Name for Loan Documents.');
            }
            hasErrors = true;
            m_NmlsName.style.backgroundColor = 'pink';
        }
        else {
            m_NmlsName.style.backgroundColor = '';
        }

        var m_Street = <%=AspxTools.JsGetElementById(m_Street) %>;
        if (m_Street == null || sTrim(m_Street.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Enter a valid street address.');
            }
            hasErrors = true;
            m_Street.style.backgroundColor = 'pink';
        }
        else {
            m_Street.style.backgroundColor = 'white';
        }

        var m_City = <%=AspxTools.JsGetElementById(m_City) %>;
        if (m_City == null || sTrim(m_City.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Enter a valid city.');
            }
            hasErrors = true;
            m_City.style.backgroundColor = 'pink';
        }
        else {
            m_City.style.backgroundColor = 'white';
        }

        var m_State = <%=AspxTools.JsGetElementById(m_State) %>;
        if (m_State == null || sTrim(m_State.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Enter a valid state.');
            }
            hasErrors = true;
            m_State.style.backgroundColor = 'pink';
        }
        else {
            m_State.style.backgroundColor = 'white';
        }

        var m_Zipcode = <%=AspxTools.JsGetElementById(m_Zipcode) %>;
        if (m_Zipcode == null || sTrim(m_Zipcode.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Enter a valid zipcode.');
            }
            hasErrors = true;
            m_Zipcode.style.backgroundColor = 'pink';
        }
        else {
            m_Zipcode.style.backgroundColor = 'white';
        }

        var m_Phone = <%=AspxTools.JsGetElementById(m_Phone) %>;
        if (m_Phone == null || sTrim(m_Phone.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Enter a valid phone number.');
            }
            hasErrors = true;
            m_Phone.style.backgroundColor = 'pink';
        }
        else {
            m_Phone.style.backgroundColor = 'white';
        }

        var m_LpePriceGroupId = <%=AspxTools.JsGetElementById(m_LpePriceGroupId) %>;
        if (m_LpePriceGroupId == null || sTrim(m_LpePriceGroupId.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Select a valid price group.');
            }
            hasErrors = true;
            m_LpePriceGroupId.style.backgroundColor = 'pink';
        }
        else {
            m_LpePriceGroupId.style.backgroundColor = 'white';
        }

        var m_Branch = <%=AspxTools.JsGetElementById(m_Branch) %>;
        if (m_Branch == null || sTrim(m_Branch.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Select a valid branch.');
            }
            hasErrors = true;
            m_Branch.style.backgroundColor = 'pink';
        }
        else {
            m_Branch.style.backgroundColor = 'white';
        }

        var groupList = <%= AspxTools.JsGetElementById(GroupAssociationList) %>.value;
        if ( gAllowGroup == true && groupList == '')
        {
            if (!hasErrors){
                select1stTab();
                alert('Please select at least one company group.');
            }
            hasErrors = true;
        }


        if (typeof AreLicensesValid === 'function'){
            if (!AreLicensesValid()){
                if (!hasErrors){
                    select2ndTab();
                    alert('Unable to save licenses.\r\nCheck the company licenses tab.');
                }
                hasErrors = true;
            }
        }

      var MiniCorrPriceGroup = <%= AspxTools.JsGetElementById(MiniCorrespondentLpePriceGroupId) %>;

        if (MiniCorrPriceGroup == null || sTrim(MiniCorrPriceGroup.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Select a valid price group.');
            }
            hasErrors = true;
            MiniCorrPriceGroup.style.backgroundColor = 'pink';
        }
        else {
            MiniCorrPriceGroup.style.backgroundColor = 'white';
        }

      var MiniCorrBranch = <%= AspxTools.JsGetElementById(MiniCorrespondentBranchId) %>;
      if (MiniCorrBranch == null || sTrim(MiniCorrBranch.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Select a valid branch.');
            }
            hasErrors = true;
            MiniCorrBranch.style.backgroundColor = 'pink';
        }
        else {
            MiniCorrBranch.style.backgroundColor = 'white';
        }


          var CorrPriceGroup = <%= AspxTools.JsGetElementById(CorrespondentLpePriceGroupId) %>;

        if (CorrPriceGroup == null || sTrim(CorrPriceGroup.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Select a valid price group.');
            }
            hasErrors = true;
            CorrPriceGroup.style.backgroundColor = 'pink';
        }
        else {
            CorrPriceGroup.style.backgroundColor = 'white';
        }

      var CorrBranch = <%= AspxTools.JsGetElementById(CorrespondentBranchId) %>;
      if (CorrBranch == null || sTrim(CorrBranch.value).length == 0){
            if (!hasErrors){
                select1stTab();
                alert('Select a valid branch.');
            }
            hasErrors = true;
            CorrBranch.style.backgroundColor = 'pink';
        }
        else {
            CorrBranch.style.backgroundColor = 'white';
        }

        var brokerRole = <%= AspxTools.JsGetElementById(BrokerRole) %>.checked;
        var correspondentRole = <%= AspxTools.JsGetElementById(CorrespondentRole) %>.checked;
        var miniCorrespondentRole = <%= AspxTools.JsGetElementById(MiniCorrespondentRole) %>.checked;

      if (!brokerRole && !correspondentRole && !miniCorrespondentRole){
            if (!hasErrors){
                select1stTab();
                alert('Select a company role.');
            }
            hasErrors = true;
        }

        var invalidStatusVal = <%=AspxTools.JsString(DataAccess.OCStatusType.Blank)%>;

        var hasNotSelectedStatus = false;

        var brokerStatus = $(<%=AspxTools.JsGetElementById(BrokerStatusDDL)%>);

        if (brokerStatus.val() === invalidStatusVal) {
            hasNotSelectedStatus = true;
            brokerStatus.css("background-color", "pink");
        }
        else {
            brokerStatus.css("background-color", "");
        }

        var miniCorrStatus = $(<%=AspxTools.JsGetElementById(MiniCorrStatusDDL)%>);

        if (miniCorrStatus.val() === invalidStatusVal) {
            hasNotSelectedStatus = true;
    		miniCorrStatus.css("background-color", "pink");
    	}
        else {
            miniCorrStatus.css("background-color", "");
        }

        var corrStatus = $(<%=AspxTools.JsGetElementById(CorrStatusDDL)%>);

        if (corrStatus.val() === invalidStatusVal) {
            hasNotSelectedStatus = true;
    		corrStatus.css("background-color", "pink");
    	}
    	else {
    	    corrStatus.css("background-color", "");
    	}

        if (hasNotSelectedStatus) {
            if (!hasErrors){
                select1stTab();
                alert('Status may not be blank for any selected company role.');
            }
            hasErrors = true;
        }

        if (!validateTaxId()) {
            if (!hasErrors) {
                select1stTab();
                alert('Enter a valid Tax ID.');
            }

            hasErrors = true;
        }

        return !hasErrors;
    }

        function validateTaxId() {
            var $taxIdInvalidWarning = $('#taxIdInvalidWarning');
            $taxIdInvalidWarning.hide();

            if (!enforceValidTaxId) {
                return true;
            }

            var taxId = <%=AspxTools.JsGetElementById(m_taxId) %>.value;

            if (taxId.length === 0) {
                return true;
            }

            <% /* If this RegExp needs to be updated, consider whether RegExStrings.EmployerTaxIdentificationNumber needs to be updated. */ %>
            var matchArray = taxId.match(/^\d{2}-?\d{7}$/);
            if (!matchArray) {
                $taxIdInvalidWarning.show();
            }

            return !!matchArray;
        }


    function f_saveBroker(){
        if (ActivePmlBrokerId == null){
            return false;
        }

        if (!f_validate())
            return false;

        var args = new Object();
        args["id"] = ActivePmlBrokerId;
        args["name"] = <%=AspxTools.JsGetElementById(m_Name) %>.value;
        args["NmlsName"] = <%=AspxTools.JsGetElementById(m_NmlsName) %>.value;
        args["NmlsNameLckd"] = <%=AspxTools.JsGetElementById(m_NmlsNameLckd) %>.checked;
        args["companyId"] = <%=AspxTools.JsGetElementById(m_companyId) %>.value;
        args["PmlCompanyTierId"] = <%=AspxTools.JsGetElementById(m_companyTier) %>.value;
        args["TpoColorThemeId"] = <%=AspxTools.JsGetElementById(this.TpoColorThemeId)%>.value;
        args["street"] = <%=AspxTools.JsGetElementById(m_Street) %>.value;
        args["city"] = <%=AspxTools.JsGetElementById(m_City) %>.value;
        args["state"] = <%=AspxTools.JsGetElementById(m_State) %>.value;
        args["zip"] = <%=AspxTools.JsGetElementById(m_Zipcode) %>.value;
        args["phone"] = <%=AspxTools.JsGetElementById(m_Phone) %>.value;
        args["fax"] = <%=AspxTools.JsGetElementById(m_Fax) %>.value;
        args["taxId"] = <%=AspxTools.JsGetElementById(m_taxId) %>.value;
        args["principal"] = <%=AspxTools.JsGetElementById(m_principal) %>.value;
        args["principal2"] = <%=AspxTools.JsGetElementById(m_principal2) %>.value;
        args["NmLsIdentifier"] = document.getElementById("m_Edit_LicensesPanel_tbNmlsIdentifier").value;
        args["GroupAssociationList"] = <%= AspxTools.JsGetElementById(GroupAssociationList) %>.value;

        args["OriginatorCompensationIsOnlyPaidForFirstLienOfCombo"] = <%=AspxTools.JsGetElementById(m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo) %>.checked;
        args["OriginatorCompensationPercent"] = <%=AspxTools.JsGetElementById(m_OriginatorCompensationPercent) %>.value;
        args["OriginatorCompensationBaseT"] = <%=AspxTools.JsGetElementById(m_OriginatorCompensationBaseT) %>.value;
        args["OriginatorCompensationMinAmount"] = <%=AspxTools.JsGetElementById(m_OriginatorCompensationMinAmount) %>.value;
        args["OriginatorCompensationMaxAmount"] = <%=AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmount) %>.value;
        args["OriginatorCompensationFixedAmount"] = <%=AspxTools.JsGetElementById(m_OriginatorCompensationFixedAmount) %>.value;
        args["OriginatorCompensationNotes"] = <%=AspxTools.JsGetElementById(m_OriginatorCompensationNotes) %>.value;

        args["CustomPricingPolicyField1"] = $('.CustomPricingPolicyField1').val();
        args["CustomPricingPolicyField2"] = $('.CustomPricingPolicyField2').val();
        args["CustomPricingPolicyField3"] = $('.CustomPricingPolicyField3').val();
        args["CustomPricingPolicyField4"] = $('.CustomPricingPolicyField4').val();
        args["CustomPricingPolicyField5"] = $('.CustomPricingPolicyField5').val();

        args["TPOLandingPage"] = <%=AspxTools.JsGetElementById(m_TPOLandingPage) %>.value;

        args["LpePriceGroupId"] = <%=AspxTools.JsGetElementById(m_LpePriceGroupId) %>.value;
        args["BranchId"] = <%=AspxTools.JsGetElementById(m_Branch) %>.value;

        args["MiniCorrespondentTPOPortalConfigID"] = <%=AspxTools.JsGetElementById(MiniCorrespondentTPOPortalConfigID) %>.value;

        args["MiniCorrespondentLpePriceGroupId"] = <%=AspxTools.JsGetElementById(MiniCorrespondentLpePriceGroupId) %>.value;
        args["MiniCorrespondentBranchId"] = <%=AspxTools.JsGetElementById(MiniCorrespondentBranchId) %>.value;

        args["CorrespondentTPOPortalConfigID"] = <%=AspxTools.JsGetElementById(CorrespondentTPOPortalConfigID) %>.value;

        args["CorrespondentLpePriceGroupId"] = <%=AspxTools.JsGetElementById(CorrespondentLpePriceGroupId) %>.value;
        args["CorrespondentBranchId"] = <%=AspxTools.JsGetElementById(CorrespondentBranchId) %>.value;

        args["BrokerRole"] = <%= AspxTools.JsGetElementById(BrokerRole) %>.checked;
        args["MiniCorrespondentRole"] = <%= AspxTools.JsGetElementById(MiniCorrespondentRole) %>.checked;
        args["CorrespondentRole"] = <%= AspxTools.JsGetElementById(CorrespondentRole) %>.checked;

        args["UnderwritingAuthority"] = <%= AspxTools.JsGetElementById(UnderwritingAuthority) %>.value;

        args["AffIds"] = GetAffIds().join(",");

        args["TpoNavigationPermissions"] = GetCheckedNavPermissions().join(",");

        args["BrokerRoleStatusT"] = $(<%=AspxTools.JsGetElementById(BrokerStatusDDL)%>).val();
        args["MiniCorrRoleStatusT"] = $(<%=AspxTools.JsGetElementById(MiniCorrStatusDDL)%>).val();
        args["CorrRoleStatusT"] = $(<%=AspxTools.JsGetElementById(CorrStatusDDL)%>).val();

        args["OcBrokerRelationships"] = getRelationships("BaseE");

        args["OcMiniCorrRelationships"] = getRelationships("BaseF");

        args["OcCorrRelationships"] = getRelationships("BaseG");

        args["Permissions"] = getPermissions();

        args["TaskEmail"] = $("#ReceiveEmail").prop("checked") ?
            <%=AspxTools.JsString(DataAccess.E_TaskRelatedEmailOptionT.ReceiveEmail)%> :
            <%=AspxTools.JsString(DataAccess.E_TaskRelatedEmailOptionT.DontReceiveEmail)%>

        if (typeof executePostGetAllFormValuesCallback === 'function') {
            executePostGetAllFormValuesCallback(args);
        }

        var result = gService.PmlCompany.call("SavePmlCompanyInfo", args);
        if (!result.error) {
            var companyResults = JSON.parse(result.value.companySaved);
            f_updateBroker(companyResults, result.value);
    	    return true;
    	}
    	else{
    	    alert('Unable to save PML originating company.');
    	    return false;
    	}

    }

    function getRelationships(tabBase)
    {
        var relationshipList = [];

        var relationshipNameHiddenInputs = $("#" + tabBase + " .RoleChooserLinkText");

        var relationshipDataHiddenInputs = $("#" + tabBase + " .RoleChooserLinkData");

        for (var i = 0; i < relationshipNameHiddenInputs.length; i++) {
            // The object these mappings will be deserialized into
            // expects "Name" and "Id" for the keys.
            var relationship = {
                "Name": relationshipNameHiddenInputs[i].value,
                "Id": relationshipDataHiddenInputs[i].value
            };

            relationshipList.push(relationship);
        }

        return JSON.stringify(relationshipList);
    }

    function getPermissions()
    {
        var permissions = [];

        $(".SavablePermissionInput").each(function () {
            // The service page will deserialize these objects
            // into KeyValuePair, so we need to use "key" and
            // "value" for the keys.
            var permission = {
                "key": $(this).prop("id"),
                "value": $(this).prop("checked")
            };

            permissions.push(permission);
        });

        return JSON.stringify(permissions);
    }

    function f_updateBroker(companyResults, serviceResults){
        var broker = new PmlBroker(companyResults);
        var count = 0;

        var serviceCredentialJson = serviceResults["ServiceCredentialsJson"];
        SetServiceCredential(broker.Id, serviceCredentialJson);

        if (typeof gPmlBrokerList[broker.Id] != 'undefined'){
            gPmlBrokerList[broker.Id] = broker;
            ActivePmlBrokerId = broker.Id;
            f_render();
        }
        else{
            if (oPaging.enabled){
                f_populate(oPaging.currentPage, gCurrentSort);
            }
            else{
                for (k in gPmlBrokerList){
                    if (gPmlBrokerList.hasOwnProperty(k))
                        count++;
                }
                if (count < PAGE_SIZE){
                    gPmlBrokerList[broker.Id] = broker;
                    ActivePmlBrokerId = broker.Id;
                    f_render();
                }
                else{
                    f_populate(1, gCurrentSort);
                }

            }
        }
    }

    function f_AddBroker(){
        ActivePmlBrokerId = "new";
        <%=AspxTools.JsGetElementById(m_Name) %>.value = "";
        <%=AspxTools.JsGetElementById(m_NmlsName) %>.value = "";
        <%=AspxTools.JsGetElementById(m_companyId) %>.value = "";
        <%= AspxTools.JsGetElementById(m_companyTier) %>.value = '0';
        <%=AspxTools.JsGetElementById(this.TpoColorThemeId)%>.selectedIndex = 0;
        <%=AspxTools.JsGetElementById(m_Street) %>.value = "";
        <%=AspxTools.JsGetElementById(m_City) %>.value = "";
        <%=AspxTools.JsGetElementById(m_State) %>.value = "";
        <%=AspxTools.JsGetElementById(m_Zipcode) %>.value = "";
        <%=AspxTools.JsGetElementById(m_Phone) %>.value = "";
        <%=AspxTools.JsGetElementById(m_Fax) %>.value = "";
        <%=AspxTools.JsGetElementById(m_taxId) %>.value = "";
        <%=AspxTools.JsGetElementById(m_principal) %>.value = "";
        <%=AspxTools.JsGetElementById(m_principal2) %>.value = "";
        <%=AspxTools.JsGetElementById(m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo) %>.checked = false;
        <%=AspxTools.JsGetElementById(m_OriginatorCompensationPercent) %>.value = "";
        <%=AspxTools.JsGetElementById(m_OriginatorCompensationBaseT) %>.value = "";
        <%=AspxTools.JsGetElementById(m_OriginatorCompensationMinAmount) %>.value = "";
        <%=AspxTools.JsGetElementById(m_OriginatorCompensationMaxAmount) %>.value = "";
        <%=AspxTools.JsGetElementById(m_OriginatorCompensationFixedAmount) %>.value = "";
        <%=AspxTools.JsGetElementById(m_OriginatorCompensationNotes) %>.value = "";
        $('.CustomPricingPolicyField1').val('');
        $('.CustomPricingPolicyField2').val('');
        $('.CustomPricingPolicyField3').val('');
        $('.CustomPricingPolicyField4').val('');
        $('.CustomPricingPolicyField5').val('');
        <%=AspxTools.JsGetElementById(m_Name) %>.style.backgroundColor = 'white';
        <%=AspxTools.JsGetElementById(m_Street) %>.style.backgroundColor = 'white';
        <%=AspxTools.JsGetElementById(m_City) %>.style.backgroundColor = 'white';
        <%=AspxTools.JsGetElementById(m_State) %>.style.backgroundColor = 'white';
        <%=AspxTools.JsGetElementById(m_Zipcode) %>.style.backgroundColor = 'white';
        <%=AspxTools.JsGetElementById(m_Phone) %>.style.backgroundColor = 'white';
        <%= AspxTools.JsGetElementById(GroupAssociationList) %>.value = '';

        <%= AspxTools.JsGetElementById(m_TPOLandingPage) %>.value = 'none';
        <%= AspxTools.JsGetElementById(m_LpePriceGroupId) %>.selectedIndex = 0;
        <%= AspxTools.JsGetElementById(m_Branch) %>.selectedIndex = 0;

        <%=AspxTools.JsGetElementById(MiniCorrespondentTPOPortalConfigID) %>.value = 'none';
        <%= AspxTools.JsGetElementById(MiniCorrespondentLpePriceGroupId) %>.selectedIndex = 0;
        <%= AspxTools.JsGetElementById(MiniCorrespondentBranchId) %>.selectedIndex = 0;
        <%=AspxTools.JsGetElementById(CorrespondentTPOPortalConfigID) %>.value = 'none';
        <%= AspxTools.JsGetElementById(CorrespondentLpePriceGroupId) %>.selectedIndex = 0;
        <%= AspxTools.JsGetElementById(CorrespondentBranchId) %>.selectedIndex = 0;

        <%= AspxTools.JsGetElementById(BrokerRole) %>.checked = false;
        <%= AspxTools.JsGetElementById(CorrespondentRole) %>.checked = false;
        <%= AspxTools.JsGetElementById(MiniCorrespondentRole) %>.checked = false;

        disableStatusDropdown('Broker', false);
        disableStatusDropdown('MiniCorr', false);
        disableStatusDropdown('Corr', false);

        $('.TpoNavigationPermissions').prop('checked', false);

        setClickHandlers();

        LoadGroupList();
        ClearAffGrid();

        if (typeof clearAllLicenses == 'function'){
            clearAllLicenses();
        }
        if (typeof AreLicensesValid == 'function'){
            AreLicensesValid();
        }
                if (gActiveRow != null){
                    gActiveRow.style.backgroundColor = "";
                }
                gActiveRow = null;
        ClearDirty();
        f_SelCompanyView(true);

        document.body.scrollTop = 0;

        SetServiceCredential(null, null);
    }

    function f_CheckAndAdd(){
        if( checkDirty() && !confirm('Continue and ignore changes?') ){
                 return false;
        }
        f_AddBroker();
        setNmlsName();
    }

    function f_CheckAndEdit(id, rowId){
        if( checkDirty() && !confirm('Continue and ignore changes?') ){
                 return false;
        }
        f_loading(true);
        setTimeout(function(){
                    f_editBroker(id, rowId);
                f_loading(false);
            }, 10);
    }

    function f_loading(bShow, msg){
        var sMsg = document.getElementById('sMsg');
        if (typeof bShow != 'undefined' && bShow){
            sMsg.innerText = ((typeof msg != 'undefined' && msg != null)) ? msg : "Loading..."
            sMsg.style.display = "";
            return;
        }
        sMsg.style.display = "none";
    }

    function f_onblurDdl(obj){
            obj.style.backgroundColor = 'white';
    }

    function setAmountTextboxStatus(cb) {
        var id = cb.id.substring(0, cb.id.length - 7);
        if (!cb.checked) {
            document.getElementById(id).value = "";
        }
        document.getElementById(id).readOnly = !cb.checked;
        document.getElementById(id + 'R').style.display = cb.checked ? "" : "none";
    }

    function f_closeAffiliateDetails() {
        document.getElementById("AffiliateDetails").style.display = "none";
    }

    function f_showAffiliateDetails(event) {
          var msg = document.getElementById("AffiliateDetails");
          msg.style.display = "";
          msg.style.top = (event.clientY + 5)+ "px";
          msg.style.left = (event.clientX + 5) + "px";
    }

    $(document).on("click", "#addContact", function() {
        showModal('/los/rolodex/rolodexlist.aspx?affdlg=Y', null, null, null, function(result){
            if(result.OK == true) {
                f_addAffRow(result.id, result.status, result.type, result.name, result.compName);
            }
        });
    });

    $(document).on("click", '.affRemove', function() {
        var $this = $(this);
        var $tr = $($this.parents("tr")[0]);
        var $affId = $tr.find('.affId');

        $tr.remove();                       // Remove row from table

        // Recolor remaining items
        // Set row class (for alternate item coloring)
        var $remainingRows = $("#AffGrid").find('.GridItem, .GridAlternatingItem');
        $remainingRows.removeClass();
        $remainingRows.each(function(index) {
            if (index % 2 == 0) {
                $(this).addClass("GridItem");
            } else {
                $(this).addClass("GridAlternatingItem");
            }
        });

        setDirty();
    });

    function f_addAffRow(id, status, type, name, compName)
    {
        var $table = $("#Affiliates");
        var $tbody = $table.find("tbody");
        var $rows = $tbody.find(".GridItem, .GridAlternatingItem")
        var rowCount = $rows.length;

        // Add row
        var $newRow = $(sNewAffRow);

        // Set row class (for alternate item coloring)
        if (rowCount % 2 == 0) {
            $newRow.addClass("GridItem");
        } else {
            $newRow.addClass("GridAlternatingItem");
        }

        // Fill in fields
        $affStatus = $newRow.find(".affStatus");
        $affStatus.text(status);
        if(status == "Disabled") {
            $affStatus.css("color", "red");
        }

        $newRow.find(".affType").text(type);
        $newRow.find(".affName").text(name);
        $newRow.find(".affCompName").text(compName);
        $newRow.find(".affId").val(id);

        $tbody.append($newRow);

        setDirty();
    }

    function GetAffIds() {
        var affIds = [];
        var $rows = $("#AffGrid").find(".GridItem, .GridAlternatingItem");

        $rows.each(function() {
            affIds.push($(this).find(".affId").val());
        });

        return affIds;
    }

    function GetCheckedNavPermissions() {
        var navPermissions = [];
        var $checkboxes = $('.TpoNavigationPermissions:checked');

        $checkboxes.each(function() {
            navPermissions.push($(this).val());
        });

        return navPermissions;
    }

    function ClearAffGrid() {
        $("#AffGrid").empty();
    }

    function LoadAffGrid(affList) {
        ClearAffGrid();

        for(var i = 0; i < affList.length; i++ ){
            f_addAffRow(affList[i].id, affList[i].status, affList[i].type, affList[i].name, affList[i].compName);
        }
    }

    var sNewAffRow = ('<tr>\
        <td align="left">\
            <span class="affStatus"/>\
        </td>\
        <td align="left" class="affType"/>\
        <td align="left" class="affName"/>\
        <td align="left" class="affCompName"/>\
        <td align="left">\
            <a href="#" class="affRemove">Remove</a>\
            <input type="hidden" class="affId" />\
        </td>\
    </tr>');
</script>

<div id="AffiliateDetails" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:450px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION: absolute; HEIGHT:75px; BACKGROUND-COLOR:whitesmoke">
	<table width="100%">
	    <tr>
	        <td>An affiliate is any company that controls, is controlled by, or is under common control with, another company. This generally means that affiliates are a parent company, subsidiaries, and sister companies.</td>
	    </tr>
	    <tr>
	        <td align=center>[ <a href="#" onclick="f_closeAffiliateDetails();">Close</a> ]</td>
	    </tr>
	</table>
</div>

<div id="MyMsg" style="display:none;"></div>
<table height="100%" cellSpacing="2" cellPadding="3" width="100%" border="0">
	<tr>
		<td class="FormTableHeader" noWrap height="0%"><span id="sMsg" style="display:none;">Loading...</span>Edit PML Originating Companies</td>
	</tr>
	<tr class="CompanyEditMode">
		<td noWrap height="0%">
			 <table cellpadding="0" cellspacing="0" border="0" width="100%">
              <tr>
                  <td class="FieldLabel Tab" id="TabA" onclick="onTabClick(this);" noWrap>Company Info</td>
                  <td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
                  <td class="FieldLabel Tab" id="TabB" onclick="onTabClick(this);" noWrap>Originator Compensation</td>
                  <td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
                  <td class="FieldLabel Tab" id="TabC" onclick="onTabClick(this);" noWrap>Company Licenses</td>
                  <td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
                  <td class="FieldLabel Tab" id="TabS" onclick="onTabClick(this);" noWrap>Services</td>
                  <td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
                  <td class="FieldLabel Tab" id="TabD" onclick="onTabClick(this);" noWrap>Affiliate Relationships</td>
                  <td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
                  <td class="FieldLabel Tab" id="TabE" onclick="onTabClick(this);" noWrap>Broker Relationships</td>
                  <td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
                  <td class="FieldLabel Tab" id="TabF" onclick="onTabClick(this);" noWrap>Mini-Correspondent Relationships</td>
                  <td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
                  <td class="FieldLabel Tab" id="TabG" onclick="onTabClick(this);" noWrap>Correspondent Relationships</td>
                  <td style="BORDER-BOTTOM: 2px groove">&nbsp; </td>
                  <td class="FieldLabel Tab" id="TabH" onclick="onTabClick(this);" noWrap>Permissions</td>
                  <td style="BORDER-BOTTOM: 2px groove" width="100%" >&nbsp; </td>
              </tr>
             </table>
             <div class="TabContent">
				<div id="BaseA" style="display:none;">
				<TABLE cellspacing="10" cellpadding="0" width="100%" border="0">
					<TR>
						<TD>
							<TABLE cellSpacing="0" cellPadding="2" border="0">
								<TR>
									<TD class="FieldLabel" noWrap width="90">Company Name</TD>
									<TD>
										<asp:TextBox id="m_Name" style="PADDING-LEFT: 4px" MaxLength="100" runat="server" Width="200"></asp:TextBox><IMG src="../../images/require_icon.gif">
									</TD>
								</TR>
								<tr>
								    <TD class="FieldLabel" noWrap width="90">Name on Loan Documents</TD>
								    <TD>
										<asp:TextBox id="m_NmlsName" style="PADDING-LEFT: 4px" MaxLength="100" runat="server" Width="200"></asp:TextBox><IMG src="../../images/require_icon.gif">
										<asp:CheckBox runat="server" ID="m_NmlsNameLckd" /> Modify
									</TD>
								</tr>
                                <tr>
								    <td class="FieldLabel" noWrap width="90">
								        Company Roles
								    </td>
								    <td>
								        <asp:CheckBox runat="server" ID="BrokerRole" Text="Broker" onclick="disableStatusDropdown('Broker', this.checked);" />
								        <asp:CheckBox runat="server" ID="MiniCorrespondentRole" Text="Mini-Correspondent" onclick="disableStatusDropdown('MiniCorr', this.checked);" />
								        <asp:CheckBox runat="server" ID="CorrespondentRole" Text="Correspondent, with " onclick="disableStatusDropdown('Corr', this.checked);" />
								        <asp:DropDownList runat="server" ID="UnderwritingAuthority"></asp:DropDownList>
								        <span> underwriting authority.</span> <IMG src="../../images/require_icon.gif">
								    </td>
                                </tr>
								<tr>
                                    <td class="FieldLabel" noWrap width="90">
                                        Company Status
                                    </td>
                                    <td>
                                        <span class="MiniPaddedRightSpan">Broker</span>
                                        <asp:DropDownList runat="server" ID="BrokerStatusDDL"></asp:DropDownList>

                                        <span class="MiniPaddedLeftSpan MiniPaddedRightSpan">Mini-Correspondent</span>
                                        <asp:DropDownList runat="server" ID="MiniCorrStatusDDL"></asp:DropDownList>

                                        <span class="MiniPaddedLeftSpan MiniPaddedRightSpan">Correspondent</span>
                                        <asp:DropDownList runat="server" ID="CorrStatusDDL"></asp:DropDownList>

                                        <IMG src="../../images/require_icon.gif">
                                    </td>
								</tr>
								<TR>
									<TD class="FieldLabel" noWrap>Company ID</TD>
									<TD>
										<asp:TextBox id="m_companyId" MaxLength="36" style="PADDING-LEFT: 4px" runat="server" Width="200"></asp:TextBox>
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Company Index Number</TD>
									<TD>
										<asp:TextBox id="IndexNumber" MaxLength="36" ReadOnly="true" style="PADDING-LEFT: 4px" runat="server" Width="200"></asp:TextBox>
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Company Tier</TD>
									<TD>
									    <asp:DropDownList ID="m_companyTier" style="PADDING-LEFT: 4px" runat="server"></asp:DropDownList>
									</TD>
								</TR>
                                <tr>
                                    <td class="FieldLabel">Originator Portal Color Theme</td>
                                    <td>
                                        <asp:DropDownList ID="TpoColorThemeId" runat="server" CssClass="left-padding"></asp:DropDownList>
                                    </td>
                                </tr>
								<TR>
									<TD class="FieldLabel" noWrap>Address
									</TD>
									<TD noWrap>
										<asp:TextBox id="m_Street" style="PADDING-LEFT: 4px" MaxLength="60" runat="server" Width="286"></asp:TextBox><IMG src="../../images/require_icon.gif">
										<br />
										<asp:TextBox id="m_City" style="PADDING-LEFT: 4px" MaxLength="36" runat="server" Width="150"></asp:TextBox><IMG src="../../images/require_icon.gif">
										<ml:StateDropDownList id="m_State" style="PADDING-LEFT: 4px" onblur="f_onblurDdl(this);" runat="server" Width="50"></ml:StateDropDownList><IMG src="../../images/require_icon.gif">
										<ml:ZipcodeTextBox id="m_Zipcode" style="PADDING-LEFT: 4px" runat="server" Width="60" CssClass="mask" preset="zipcode"></ml:ZipcodeTextBox><IMG src="../../images/require_icon.gif">
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Phone
									</TD>
									<TD>
										<ml:PhoneTextBox id="m_Phone" style="PADDING-LEFT: 4px" runat="server" CssClass="mask" preset="phone"></ml:PhoneTextBox><IMG src="../../images/require_icon.gif">
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap> Broker Branch
									</TD>
									<TD>
										<asp:DropDownList ID="m_Branch" runat="server" style="PADDING-LEFT: 4px"></asp:DropDownList>
                                        <img id="m_BranchReqImg" runat="server" src="../../images/require_icon.gif">
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Broker Price Group
									</TD>
									<TD>
									    <asp:DropDownList ID="m_LpePriceGroupId" style="PADDING-LEFT: 4px" runat="server" Width="190px"></asp:DropDownList>
                                        <img id="m_LpePriceGroupIdReqImg" runat="server" src="../../images/require_icon.gif">
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Broker Landing Page
									</TD>
									<TD>
										<asp:DropDownList ID="m_TPOLandingPage" runat="server" style="PADDING-LEFT: 4px"></asp:DropDownList>
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Mini-Correspondent Branch
									</TD>
									<TD>
										<asp:DropDownList ID="MiniCorrespondentBranchId" runat="server" style="PADDING-LEFT: 4px"></asp:DropDownList>
                                        <img id="MiniCorrespondentBranchIdReqImg" runat="server" src="../../images/require_icon.gif">
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Mini-Correspondent Price Group
									</TD>
									<TD>
									    <asp:DropDownList ID="MiniCorrespondentLpePriceGroupId" style="PADDING-LEFT: 4px" runat="server" Width="190px"></asp:DropDownList>
                                        <img id="MiniCorrespondentLpePriceGroupIdReqImg" runat="server" src="../../images/require_icon.gif">
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Mini-Correspondent Landing Page
									</TD>
									<TD>
										<asp:DropDownList ID="MiniCorrespondentTPOPortalConfigID" runat="server" style="PADDING-LEFT: 4px"></asp:DropDownList>
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap> Correspondent Branch
									</TD>
									<TD>
										<asp:DropDownList ID="CorrespondentBranchId" runat="server" style="PADDING-LEFT: 4px"></asp:DropDownList>
                                        <img id="CorrespondentBranchIdReqImg" runat="server" src="../../images/require_icon.gif">
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Correspondent Price Group
									</TD>
									<TD>
									    <asp:DropDownList ID="CorrespondentLpePriceGroupId" style="PADDING-LEFT: 4px" runat="server" Width="190px"></asp:DropDownList>
                                        <img id="CorrespondentLpePriceGroupIdReqImg" runat="server" src="../../images/require_icon.gif">
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Correspondent Landing Page
									</TD>
									<TD>
										<asp:DropDownList ID="CorrespondentTPOPortalConfigID" runat="server" style="PADDING-LEFT: 4px"></asp:DropDownList>
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap>Fax
									</TD>
									<TD>
										<ml:PhoneTextBox id="m_Fax" style="PADDING-LEFT: 4px" runat="server" CssClass="mask" preset="phone"></ml:PhoneTextBox></TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap >Tax ID</TD>
									<TD>
										<asp:TextBox id="m_taxId" style="PADDING-LEFT: 4px" MaxLength="36" runat="server" Width="200" onchange="validateTaxId();" ></asp:TextBox>
                                        <span id="taxIdInvalidWarning" class="validation-error">Invalid Tax ID</span>
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap >Principal</TD>
									<TD>
										<asp:TextBox id="m_principal" style="PADDING-LEFT: 4px" MaxLength="100" runat="server" Width="286"></asp:TextBox>
									</TD>
								</TR>
								<TR>
									<TD class="FieldLabel" noWrap >Principal 2</TD>
									<TD>
										<asp:TextBox id="m_principal2" style="PADDING-LEFT: 4px" MaxLength="100" runat="server" Width="286"></asp:TextBox>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
				<span class="FieldLabel">Originating Company Groups<img src="../../images/require_icon.gif"></span>
					<div id="GroupsDiv" style="overflow-y: scroll; height:140px; width:100%;">
                    <table id="GroupsTable" class="DataGrid" cellspacing="0" cellpadding="2" rules="all" border="1" style="width:100%;border-collapse:collapse;">
                        <thead>
                            <tr class="GridHeader" style="color:black">
	                            <th>&nbsp;</th>
	                            <th align="left"><a href="#" onclick="f_sortGroupGrid();">Company Group</a><img id="GroupSortImg" style="display:none;"/></th>
	                            <th align="left">Description</th>
                            </tr>
                        </thead>
                        <tbody id="GroupsGrid"></tbody>
                    </table>
                    <input id="GroupAssociationList" type="hidden" runat="server" />
                    </div>
				</div>

			    <div id="BaseB" style="display:none;">
				        <table cellSpacing=0 cellPadding=0 style="PADDING-BOTTOM:0px">
		                    <tr style="padding-bottom:4px">
		                        <td>
                                    <asp:CheckBox id="m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo" runat="server" Text="Compensation only paid for the 1st lien of a combo" />
                                </td>
		                    </tr>
                            <tr style="padding-bottom:4px">
		                                <td>
                                            <ml:percenttextbox id="m_OriginatorCompensationPercent" runat="server" width="70" preset="percent"></ml:percenttextbox>
		                                    of the
		                                    <asp:DropDownList id="m_OriginatorCompensationBaseT" runat="server"></asp:DropDownList>
		                                </td>
		                            </tr>
		                            <tr style="padding-bottom:4px">
		                                <td style="padding-left:50px">
		                                    <asp:CheckBox ID="m_OriginatorCompensationMinAmountEnabled" runat="server" onclick="setAmountTextboxStatus(this);" />
		                                    Minimum
		                                    <ml:moneytextbox id="m_OriginatorCompensationMinAmount" runat="server" width="90" preset="money" style="position: absolute; left: 160px;"></ml:moneytextbox>
		                                    <asp:Image ID="m_OriginatorCompensationMinAmountR" runat="server" ImageUrl="~/images/require_icon.gif" style="position: absolute; left: 255px;" />
				                        </td>
		                            </tr>
		                            <tr style="padding-bottom:4px">
		                                <td style="padding-left:50px">
		                                    <asp:CheckBox ID="m_OriginatorCompensationMaxAmountEnabled" runat="server" onclick="setAmountTextboxStatus(this);" />
		                                    Maximum
		                                    <ml:moneytextbox id="m_OriginatorCompensationMaxAmount" runat="server" width="90" preset="money" style="position: absolute; left: 160px;"></ml:moneytextbox>
		                                    <asp:Image ID="m_OriginatorCompensationMaxAmountR" runat="server" ImageUrl="~/images/require_icon.gif" style="position: absolute; left: 255px;" />
				                        </td>
		                            </tr>
		                            <tr>
		                                <td>
		                                    Fixed amount
                                            <ml:MoneyTextBox ID="m_OriginatorCompensationFixedAmount" runat="server" preset="money" Width="90" style="position: absolute; left: 260px;"></ml:MoneyTextBox>
		                                </td>
		                            </tr>
		                            <tr>
		                                <td style="padding-top:15px">
		                                    Notes
		                                </td>
		                            </tr>
		                            <tr>
		                                <td>
		                                    <asp:TextBox ID="m_OriginatorCompensationNotes" TextMode="MultiLine" Rows="5" Width="350px" runat="server"></asp:TextBox>
		                                </td>
		                            </tr>
		                            <tr>
		                                <td style="padding-top:15px;width:100%">
		                                    Compensation plan change history
		                                    <div id="divAuditHistory">No entries in change history</div>
		                                    <div id="pager" class="pager">
                                                <span class="pageNumbers" style="text-align:right;width:100%"></span>
                                                <div style="display:none">
                                                    <img src="first.png" class="first"/>
                                                    <img src="prev.png" class="prev"/>
                                                    <input type="text" class="pagedisplay"/>
                                                    <img src="next.png" class="next"/>
                                                    <img src="last.png" class="last"/>
                                                    <select class="pagesize">
	                                                    <option selected="selected"  value="10">10</option>
	                                                    <option value="20">20</option>
	                                                    <option value="30">30</option>
	                                                    <option  value="40">40</option>
                                                    </select>
                                                </div>
                                            </div>
		                                </td>
		                            </tr>
		                        </table>
		                        <asp:PlaceHolder runat="server" ID="CustomLOPricingPolicyPlaceHolder">
			                        <div class="FieldLabel heading">Loan Officer Pricing Policy</div>
			                        <table runat="server" id="CustomLOPricingPolicyTable">
			                        </table>
		                        </asp:PlaceHolder>
			    </div>

		            <div id="BaseC" style="display:none">
		                <uc:LendingLicenses id="LicensesPanel" NamingPrefix="pmlCo" IsCompanyLOID="True" runat="server"></uc:LendingLicenses>
		            </div>

                <div id="BaseD" style="display:none">
                    <b>Affiliates</b> <a href='#"' onclick='f_showAffiliateDetails(event);'>?</a>
                    <hr />
                    <i>List any contacts affiliated with the originating company</i>
                    <p>
                        <input type="button" id="addContact" value="Add Contact to affiliate list" />
                    </p>

                    <table id="Affiliates" class="DataGrid" cellspacing="0" cellpadding="2" rules="all" border="1" style="width:100%;border-collapse:collapse;">
                        <thead>
                            <tr class="GridHeader" style="color:black">
	                            <th align="left">Status</th>
	                            <th align="left">Type</th>
	                            <th align="left">Name</th>
	                            <th align="left">Company Name</th>
	                            <th width="50px"></th>
                            </tr>
                        </thead>
                        <tbody id="AffGrid"></tbody>
                    </table>
                </div>

                <div id="BaseE" style="DISPLAY: none;">
					<uc:Relationships id="OcBrokerRelationships" runat="server" Mode="Broker" IsForOcPage="True" />
                </div>

                <div id="BaseF" style="display: none;">
                    <uc:Relationships id="OcMiniCorrRelationships" runat="server" Mode="MiniCorrespondent" IsForOcPage="True" />
                </div>

                <div id="BaseG" style="display: none;">
                    <uc:Relationships id="OcCorrRelationships" runat="server" Mode="Correspondent" IsForOcPage="True" />
                </div>

                <div id="BaseH" style="display:none">
                    <table width="100%">
                        <tr>
                            <td class="FieldLabel newheader">
                                Custom Portal Page Visibility
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                                <ml:EncodedLabel runat="server" ForeColor="Red">Users belonging to this company will inherit permissions granted here.</ml:EncodedLabel>
                                <br />
                                <br />
                                Company can see which custom navigation pages? (Subject to portal mode and role requirements for the page.)
                            </td>
                        </tr>
                        <asp:PlaceHolder runat="server" ID="m_TpoNavigationPermissions"/>
                        <tr>
                            <td style="padding-top: 2px">&nbsp;</td>
                        </tr>
                        <tr >
                            <td class="FieldLabel newheader " >
	                            Pricing Engine
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 2px" >
                                <input type="checkbox" id="CanApplyForIneligibleLoanPrograms" TabIndex="75" class="SavablePermissionInput" />Can apply for ineligible loan programs
                                <br />

                                <input type="checkbox" id="CanRunPricingEngineWithoutCreditReport" TabIndex="80" class="SavablePermissionInput" />Can run pricing engine w/o credit report
                                <br />

                                <span style="padding-left: 15px;"><input type="checkbox" id="CanSubmitWithoutCreditReport" class="SavablePermissionInput" />Can register/lock loans w/o credit report</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel newheader">
	                            Total Scorecard Interface
                            </td>
                        </tr>
                        <tr>
                            <td>
	                            <span>Allow access to Total Scorecard Interface</span>
                                <input type="radio" name="totalScorecard" id="CanAccessTotalScorecard" TabIndex="113" class="SavablePermissionInput" />Yes
                                <input type="radio" name="totalScorecard" id="CanAccessTotalScorecardNo" TabIndex="114" />No
			                    <br />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel newheader">
	                            Notifications
                            </td>
                        </tr>
                        <tr>
                            <td>
	                            <span>Send task-related e-mail</span>
		                        <input type="radio" name="taskEmail" id="ReceiveEmail" TabIndex="115" />Yes
                                <input type="radio" name="taskEmail" id="ReceiveEmailNo" TabIndex="116" />No
			                    <br />
                            </td>
                        </tr>
                        <tr>
	                        <td class="FieldLabel newheader">
		                        Services
	                        </td>
	                    </tr>
	                    <tr>
                            <td>
                                <input type="checkbox" id="AllowOrder4506T" TabIndex="80" class="SavablePermissionInput" />Allow ordering 4506-T
                            </td>
                        </tr>
                    </table>
                </div>

                 <div id="BaseS" style="display: none;">
                     <input type="hidden" id="ServiceCredentialsJson" value="" />
                    <div class="FieldLabel">Credentials</div>
                    <hr />
                    <div id="ServiceCredentialsDiv">
                        <div>
                            <table id="ServiceCredentialsTable">
                                <tbody></tbody>
                            </table>
                        </div>
                        <br />
                        <input type="button" id="AddServiceCredentialBtn" onclick="AddServiceCredentialBtnClick();" value="Add Credential" />
                    </div>
                    <div id="ServiceCredentialsNewBranchDiv">
                        <span>Please save this originating company before adding service credentials.</span>
                    </div>
                 </div>
			</div>
		</td>
	</tr>
	<tr vAlign="top">
	    <td>
            <strong class="ListMode">Company Search:</strong>
        </td>
    </tr>
    <tr vAlign="top">
        <td>
            <div class="ListMode">
                <span class="PaddedRightSpan">Company Name</span>
                <input id="CompanyNameSearchInput" type="text" maxlength="100" size="25"/>

                <span class="PaddedLeftSpan PaddedRightSpan">Company ID</span>
                <input id="CompanyIDSearchInput" type="text" maxlength="36" size="25" />

                <span class="PaddedLeftSpan PaddedRightSpan">Company Role</span>
                <asp:DropDownList runat="server" ID="CompanyRoleSearchDDL"></asp:DropDownList>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <span class="PaddedRightSpan">Company Index Number</span>
            <input class="width-138" id="CompanyIndexNumberSearch" type="text" />

            <span class="PaddedLeftSpan MiniPaddedRightSpan">Broker</span>
            <asp:DropDownList runat="server" ID="BrokerStatusSearchDDL"></asp:DropDownList>

            <span class="MiniPaddedLeftSpan MiniPaddedRightSpan">Mini-Correspondent</span>
            <asp:DropDownList runat="server" ID="MiniCorrStatusSearchDDL"></asp:DropDownList>

            <span class="MiniPaddedLeftSpan MiniPaddedRightSpan">Correspondent</span>
            <asp:DropDownList runat="server" ID="CorrStatusSearchDDL"></asp:DropDownList>
        </td>
    </tr>
    <tr vAlign="top">
        <td class="Right">
            <div class="ListMode">
                <input id="CompanySearchButton" type="button" value="Find Using Search Parameters" onclick="onSearchBtnClicked();" />
                <span>&nbsp;</span>
                <input id="CompanySearchClearButton" type="button" value="Clear Search" onclick="onClearBtnClicked();" />
            </div>
        </td>
    </tr>
	<tr vAlign="top">
	    <td>
	        <div id="pagingDiv" style="BORDER: lightgrey 2px solid; MARGIN: 6px 0px; display:none; WIDTH: 100%; BACKGROUND-COLOR: gainsboro;">
			    <div style="PADDING: 6px; FONT-WEIGHT: bold; TEXT-ALIGN: center; BACKGROUND-COLOR: #ffffcc">
			        <span><span id="pagingResults">Results</span>&nbsp;&nbsp;<span id="pagingLimits" style="COLOR: tomato;"></span>&nbsp;&nbsp;of&nbsp;&nbsp;<span id="pagingTotal" style="COLOR: tomato;"></span></span>
			    </div>
			</div>
			<div style="WIDTH: 100%;TEXT-ALIGN:right;" class="ListMode">
			    <span style="padding-right:4px;" id="pagesDiv"></span>
			</div>
	    </td>
	</tr>
	<tr vAlign="top">
		<td noWrap height="100%" valign="middle">
			    <div id="m_Table" style="height:100%; width:100%; display:none;" class="ListMode">
                    <table class="DataGrid" cellspacing="0" cellpadding="2" rules="all" border="1" style="width:100%;border-collapse:collapse;">
                        <thead>
                            <tr class="GridHeader" style="color:black">
	                            <th>&nbsp;</th>
	                            <th align="left"><a href="#" onclick="f_sortGrid('Name');">Company Name</a><img id="imgSortName" style="display:none;"/></th>
                                <th align="left">Company Index Number</th>
	                            <th align="left"><a href="#" onclick="f_sortGrid('CompanyId');">Company ID</a><img id="imgSortCompanyId" style="display:none;"/></th>
	                            <th align="left">Company Roles</th>
                                <th align="left">Company Status</th>
                                <th align="left">Originator Portal Color Theme</th>
	                            <th align="left">Company Group</th>
	                            <th align="left">Address</th>
	                            <th align="left">Phone</th>
	                            <th align="left">Fax</th>
	                            <th align="left"><a href="#" onclick="f_sortGrid('LpePriceGroupName');">Price Group</a><img id="imgSortPriceGroup" style="display:none;"/></th>
	                            <th align="left"><a href="#" onclick="f_sortGrid('BranchNm');">Branch</a><img id="imgSortBranch" style="display:none;"/></th>
	                            <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody id="m_Grid"></tbody>
                    </table>
                </div>
                <div id="m_Empty" style="width:100%; text-align:center;">
                    <div style="color:graytext;font-style:italic;"></div>
                </div>
		</td>
	</tr>
	<tr>
		<td noWrap align="right" height="0%">
			<table cellSpacing="4" cellPadding="0" width="100%">
				<tr>
					<td align="left">
                        <input style="WIDTH: 110px" onclick="f_CheckAndAdd()" type="button" NoHighlight value="Add new company" class="ListMode" />
					    <input style="width: 210px" onclick="f_exportAll()" type="button" NoHighlight value="Export all Originating Companies" class="ListMode" />
					</td>
					<td align="right">
					    <input style="WIDTH: 60px" id="m_Ok" onclick="f_save('ok');" type="button" NoHighlight value="OK" class="CompanyEditMode" />
					    <input style="WIDTH: 60px" onclick="onCancel();" type="button" value="Cancel" NoHighlight class="CompanyEditMode" />
						<input style="WIDTH: 60px" id="m_Apply" onclick="f_save('apply');" type="button" NoHighlight value="Apply" class="CompanyEditMode" />
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
