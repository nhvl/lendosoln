<%@ Register TagPrefix="ils" TagName="EmployeeRoleChooserLink" Src="../common/EmployeeRoleChooserLink.ascx" %>
<%@ Page language="c#" Codebehind="PmlUserList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.BrokerAdmin.PmlUserList" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Register TagPrefix="ML" TagName="CModalDlg" Src="../../Common/ModalDlg/CModalDlg.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<TITLE>Originating Company User List</TITLE>
        <style type="text/css">
            .authentication-container {
                border: 1px solid dimgray;
                padding: 5px;
                padding-top: 10px;
                width: 500px;
            }

            .authentication-container div {
                clear: both;
                margin-bottom: 5px;
            }

            .authentication-container legend {
                font-weight: bold;
            }

            .authentication-container label {
                float:left;
                font-weight: bold;
            }

            .authentication-container select {
                float: right;
                width: 220px;
            }

            #LastLoginDetails {
                BORDER:black 3px solid;
                PADDING:5px;
                WIDTH:450px;
                POSITION:absolute;
                MIN-HEIGHT:50px;
                BACKGROUND-COLOR:whitesmoke
            }
            .mainTable{
            	margin-top: -30px;
            	padding-top: 30px;
            	*padding-top: 0px;
            }
        </style>
	</HEAD>
	<BODY style="MARGIN: 0px" bgColor="gainsboro" onload="onInit();" MS_POSITIONING="FlowLayout">
	<script type="text/javascript">
		function f_showPage(a){
		    if (hasDisabledAttr(a)) {
		        return;
		    }
		    var msg = document.getElementById("PageVisibility");
			msg.style.display = "";
		}

		function onInit() { try
		{
		    var feedBack = document.getElementById("m_feedBack");
		    var errorMessage = document.getElementById("m_errorMessage");
			// Look for error and report if found.
			if( errorMessage != null )
				alert( errorMessage.value );

			if( feedBack != null )
				alert( feedBack.value );

			setBatchControls( false );

			updateSearchButton();
			setInitialSelectedRelationship();

			//If this is a postback and there are still search criteria, display the search results again
			<% if( IsFirstLoad == false ) { %>
				if(!document.getElementById("searchBtn").disabled)
					document.getElementById("searchBtn").click();
			<% } %>

			// 08/27/08 ck - OPM 21952. Need to check when first started up
			f_enableDisableExportSearchResultsButton();

			$('.viewLoan').on('change', function() {
			    if (this.disabled) {
			        return;
			    }
			    var $this = $(this);
			    var $createDropdown = $this.closest('tr').next().find('.createLoan').first();
			    var $createDropdownYesOption = $createDropdown.find('option[value="1"]').first();

			    if ($this.val() === '1') {
			        $createDropdown.prop('disabled', false);
			        $createDropdownYesOption.prop('disabled', false);
			    } else if ($this.val() === '0') {
			        $createDropdown.val('0');
			        $createDropdown.prop('disabled', true);
			        $createDropdownYesOption.prop('disabled', true);
			    } else {
			        $createDropdown.val('-1');
			        $createDropdown.prop('disabled', false);
			        $createDropdownYesOption.prop('disabled', true);
			    }

			    updateSelectedUsersButton();
			}).change()

            $(".authentication-container select").change(updateSelectedUsersButton);

            var useUserSettingsValue = <%=AspxTools.JsString(DataAccess.EmployeePopulationMethodT.IndividualUser)%>;

			$(<%=AspxTools.JsGetElementById(UseRelationshipsDdl)%>).change(function() {
			    setRelationshipColumnControls($(this).val() === useUserSettingsValue);
			});

		    $(<%=AspxTools.JsGetElementById(UsePermissionDdl)%>).change(function() {
		        setPermissionColumnControls($(this).val() === useUserSettingsValue);
		    });
		}
		catch( e )
		{
			window.status = e.message;
		}}

		var g_checkLimit = 100;
		var g_numChecked = 0;
		var elementArray = new Array();
		var activeArray = new Array();

		function setInitialSelectedRelationship()
		{
			var relationship = <%= AspxTools.JsGetElementById(m_relationshipDdl) %>.value;
			document.getElementById('ChoosePanel').style.display = "none";
			document.getElementById(relationship + 'Panel').style.display = 'inline';
		}

		//OPM 16265 - saved for future use
		function highlightBranchDDL()
		{
			var branchDdl = <%= AspxTools.JsGetElementById(m_branchDDL) %>;
			var item = branchDdl.options[0];
			item.style.backgroundColor = "#ffcccc";
		}

		function onClickCheckAll( oCheck , bIsChecked ) { try
		{
			// Apply all-check effects to grid.
			var i , root = oCheck;

			while( root != null && root.tagName != "TABLE" )
			{
				root = root.parentElement;
			}

			for( i = 1 ; i < root.rows.length ; ++i )
			{
				var cell = root.rows[ i ].cells[ 0 ];
				if( cell.children[ 0 ].tagName != "INPUT" )
					continue;

				if( bIsChecked == true )
				{
					if( cell.children[ 0 ].checked == false )
					{
						if( g_numChecked >= g_checkLimit )
						{
							alert( "Selection limited to first " + g_checkLimit + "." );
							return;
						}

						cell.children[ 0 ].checked = true;
						g_numChecked++;
					}
				}
				else
				if( cell.children[ 0 ].checked == true )
				{
					cell.children[ 0 ].checked = false;
					g_numChecked--;
				}
			}
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function onClickCheckRow( oCheck ) { try
		{
			// Apply row-check effects to grid.
			if( oCheck.checked != null )
			{
				if( oCheck.checked == true )
				{
					if( g_numChecked >= g_checkLimit )
					{
						alert( "Selection limited to first " + g_checkLimit + "." );
						oCheck.checked = false;
						return false;
					}
					g_numChecked++;
				}
				else
					g_numChecked--;

				highlightRowByCheckbox( oCheck );
			}
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function onEditClick( employeeId )
		{
			// Open up edit window.
			showModal( "/los/BrokerAdmin/EditPmlUser.aspx?cmd=edit&employeeId=" + employeeId, null, null, null, function(a){
				if( a != null && a.OK == true )
					PmlUserList.submit();
			}, { width:990 });
		}

		function onAddNewClick()
		{
		    showModal( "/los/BrokerAdmin/EditPmlUser.aspx?cmd=add", null, null, null, function(a){// Open up edit window.
				if( a != null && a.OK == true && frmPmlUserList.contentWindow )
				{
					frmPmlUserList.contentWindow.__doPostBack('', '');
				}
				else if ( a != null && a.OK == true)
				{
					frmPmlUserList.__doPostBack('', '');
				}
			}, { width:990, hideCloseButton: true });
		}

		function checkDateFormat(sDate)
		{
		    sDate = sDate.replace(/[^0-9\/-]/g, '').replace(/-/g, '/');
			if(sDate == '')
			    return false;
		}

		function checkDate(sDate, befAftObj)
		{
			//display alert if someone tries to access a date before tracking began
			var msg = "Last Login date tracking began on 09/16/2006\t\n(and 02/23/2008 for users who log in through a webservice interface).\t\n\nUsers who have not logged in since 09/16/2006 (or 02/23/2008 for webservice users)\t\nwill appear in the search results as if they had never logged in.\n";

			var trackBeginDate = 9162006;
			var compareDate = sDate;
			var index = compareDate.indexOf("/");
			while(index != -1)
			{
				compareDate = compareDate.replace("/", "");
				index = compareDate.indexOf("/");
			}
			compareDate *= 1;

			if((befAftObj.value.toString() == "before") || ((compareDate < trackBeginDate) &&  (befAftObj.value.toString() == "after")))
				alert(msg);

			return true;
		}

		function f_search()
		{
			var isValid = true;
			if (typeof(Page_ClientValidate) == 'function')
				isValid = Page_ClientValidate();
			if(isValid == true)
			{
				var sName = <%= AspxTools.JsGetElementById(m_SearchFilter) %>.value;
				var sPriceGroup = <%= AspxTools.JsGetElementById(m_PriceGroups) %>.value;
				var sLenderAccountExecFilter = document.getElementById(<%= AspxTools.JsString(m_LenderAccountExecChoice.DataID) %>).value;
				var sUnderwriterFilter = document.getElementById(<%= AspxTools.JsString(m_UnderwriterChoice.DataID) %>).value;
				var sJuniorUnderwriterFilter = document.getElementById(<%= AspxTools.JsString(m_JuniorUnderwriterChoice.DataID) %>).value;
				var sLockDeskFilter = document.getElementById(<%= AspxTools.JsString(m_LockDeskChoice.DataID) %>).value;
				var sManagerFilter = document.getElementById(<%= AspxTools.JsString(m_ManagerChoice.DataID) %>).value;
				var sProcessorFilter = document.getElementById(<%= AspxTools.JsString(m_ProcessorChoice.DataID) %>).value;
				var sJuniorProcessorFilter = document.getElementById(<%= AspxTools.JsString(m_JuniorProcessorChoice.DataID) %>).value;
				var sSupervisorFilter = document.getElementById(<%= AspxTools.JsString(m_SupervisorChoice.DataID) %>).value;
				var sDate = <%= AspxTools.JsGetElementById(m_lDate) %>.value;
				var befAftObj = <%= AspxTools.JsGetElementById(m_BefAft) %>;
				var sBeforeAfter = befAftObj.options[befAftObj.selectedIndex].value;
				var sBranch = <%= AspxTools.JsGetElementById(m_branchDDL) %>.value;
				var sStatus = <%= AspxTools.JsGetElementById(m_ActiveStatus) %>.value;
				var sRelationship = <%= AspxTools.JsGetElementById(m_relationshipDdl) %>.value;
				var pmlBrokerId = document.getElementById(<%= AspxTools.JsString(m_CompanyChoice.DataID) %>).value;

				var creditAuditorChoice = document.getElementById(<%= AspxTools.JsString(m_CreditAuditorChoice.DataID) %>);
    			var sCreditAuditorFilter = creditAuditorChoice ? creditAuditorChoice.value : null;
			    var legalAuditorChoice = document.getElementById(<%= AspxTools.JsString(m_LegalAuditorChoice.DataID) %>);
			    var sLegalAuditorFilter = legalAuditorChoice ? legalAuditorChoice.value : null;
			    var purchaserChoice = document.getElementById(<%= AspxTools.JsString(m_PurchaserChoice.DataID) %>);
			    var sPurchaserFilter = purchaserChoice ? purchaserChoice.value : null;
			    var secondaryChoice = document.getElementById(<%= AspxTools.JsString(m_SecondaryChoice.DataID) %>);
			    var sSecondaryFilter = secondaryChoice ? secondaryChoice.value : null;

			    var mode = document.getElementById('mode').value;

				if((sDate != null) && (sDate != ""))
				{
				    var ret = checkDateFormat(sDate)
				    if(ret == false)
				    {
				        alert("Last Login Date contains an invalid Date value.");
				        <%= AspxTools.JsGetElementById(m_lDate) %>.focus();
				        return;
				    }

					ret = checkDate(sDate, befAftObj);
					if(ret == false)
					{
					    <%= AspxTools.JsGetElementById(m_lDate) %>.value = '';
					}
				}

				setBatchControls( false );

				if (frmPmlUserList.contentWindow)
				{
				    frmPmlUserList.contentWindow.f_search(
				        sName,
				        sPriceGroup,
				        sLenderAccountExecFilter,
				        sUnderwriterFilter,
				        sLockDeskFilter,
				        sManagerFilter,
				        sProcessorFilter,
				        sSupervisorFilter,
				        sDate,
				        sBeforeAfter,
				        sBranch,
				        sStatus,
				        sRelationship,
				        pmlBrokerId,
				        sJuniorUnderwriterFilter,
				        sJuniorProcessorFilter,
				        sCreditAuditorFilter,
				        sLegalAuditorFilter,
				        sPurchaserFilter,
				        sSecondaryFilter,
				        mode);
				}
				else
				{
				    frmPmlUserList.f_search(
				        sName,
				        sPriceGroup,
				        sLenderAccountExecFilter,
				        sUnderwriterFilter,
				        sLockDeskFilter,
				        sManagerFilter,
				        sProcessorFilter,
				        sSupervisorFilter,
				        sDate,
				        sBeforeAfter,
				        sBranch,
				        sStatus,
				        sRelationship,
				        pmlBrokerId,
				        sJuniorUnderwriterFilter,
				        sJuniorProcessorFilter,
				        sCreditAuditorFilter,
				        sLegalAuditorFilter,
				        sPurchaserFilter,
				        sSecondaryFilter,
				        mode);
				}

				// 08/27/08 ck - OPM 21952. Need to check after every search
				f_enableDisableExportSearchResultsButton();
			}
		}

		function endsWith(str, suffix) {
            return str.indexOf(suffix, str.length - suffix.length) !== -1;
        }

		function GetUpdatedTPOLinks() {
		    var pg = document.getElementById('PageVisibility');
		    var ddls = pg.getElementsByTagName('select');

		    var items = [], o, l = ddls.length, i = 0;

		    for(i; i < l; i++){
		        o = ddls[i];
		        if (o.value != 'Current') {
		            items.push (o.value);
		        }
		    }

		    return items;
		}


		function ClearTPOLinks() {
		    var pg = document.getElementById('PageVisibility');
		    var ddls = pg.getElementsByTagName('select');

		    var items = [], o, l = ddls.length, i = 0;

		    for(i; i < l; i++){
		        o = ddls[i];
		        o.selectedIndex = 0;
		    }

		    return items;
		}


		function getElementValueOrNull(elementId) {
		    var value = null;
		    var element = document.getElementById(elementId);
		    if (element) {
		        value = element.value;
		    }

            return value;
		}

		function f_update() {
		  var args = new Object();

          var useOcSettingsValue = <%=AspxTools.JsString(DataAccess.EmployeePopulationMethodT.OriginatingCompany)%>;

          var useOcRelationshipSettings = $(<%=AspxTools.JsGetElementById(UseRelationshipsDdl)%>).val() === useOcSettingsValue;

          var useOcPermissionSettings = $(<%=AspxTools.JsGetElementById(UsePermissionDdl)%>).val() === useOcSettingsValue;

          args["UseOcRelationshipSettings"] = useOcRelationshipSettings;
		    args["UseOcPermissionSettings"] = useOcPermissionSettings;

		    args["EnabledIpRestriction"] = $("#EnabledIpRestrictionDdl").val();
		    args["EnabledClientDigitalCertificateInstall"] = $("#EnabledClientDigitalCertificateInstallDdl").val();
		    args["EnableAuthCodeViaSms"] = $("#EnableAuthCodeViaSmsDdl").val();

          if (useOcRelationshipSettings === false) {
            var supervisorDDL = <%= AspxTools.JsGetElementById(m_SupervisorDDL) %>.value;

            if (supervisorDDL === "Not" && !VerifySupervisorChange()) {
                return;
            }

            var supervisor = document.getElementById(<%= AspxTools.JsString(m_Supervisor.DataID) %>).value;
            args["Supervisor"] = supervisor;

		    if (supervisorDDL != 'Not' || (supervisorDDL == 'Not' && (supervisor == '' || supervisor == 'Current'))) {
                args["Supervisor"] = supervisorDDL;
		    }

            args["LenderAccExecEmployeeId"] = document.getElementById(<%= AspxTools.JsString(m_LenderAccountExecBatchChoice.DataID)%>).value;
            args["UnderwriterEmployeeId"] = document.getElementById(<%= AspxTools.JsString(m_UnderwriterBatchChoice.DataID)%>).value;
            args["JuniorUnderwriterEmployeeId"] = document.getElementById(<%=AspxTools.JsString(m_JuniorUnderwriterBatchChoice.DataID)%>).value;
            args["LockDeskEmployeeId"] = document.getElementById(<%=AspxTools.JsString(m_LockDeskBatchChoice.DataID)%>).value;
            args["ManagerEmployeeId"] = document.getElementById(<%=AspxTools.JsString(m_ManagerBatchChoice.DataID)%>).value;
            args["ProcessorEmployeeId"] = document.getElementById(<%=AspxTools.JsString(m_ProcessorBatchChoice.DataID)%>).value;
            args["JuniorProcessorEmployeeId"] = document.getElementById(<%=AspxTools.JsString(m_JuniorProcessorBatchChoice.DataID)%>).value;

            var creditAuditorChoice = document.getElementById(<%= AspxTools.JsString(m_CreditAuditorBatchChoice.DataID) %>);
		    args["CreditAuditorEmployeeId"] = creditAuditorChoice ? creditAuditorChoice.value : '';

		    var legalAuditorChoice = document.getElementById(<%= AspxTools.JsString(m_LegalAuditorBatchChoice.DataID) %>);
		    args["LegalAuditorEmployeeId"] = legalAuditorChoice ? legalAuditorChoice.value : '';

		    var purchaserChoice = document.getElementById(<%= AspxTools.JsString(m_PurchaserBatchChoice.DataID) %>);
		    args["PurchaserEmployeeId"] = purchaserChoice ? purchaserChoice.value : '';

		    var secondaryChoice = document.getElementById(<%= AspxTools.JsString(m_SecondaryBatchChoice.DataID) %>);
            args["SecondaryEmployeeId"] = secondaryChoice ? secondaryChoice.value : '';
          }

		  if (useOcPermissionSettings === false) {
		    args["LpePriceGroupId"] = <%= AspxTools.JsGetElementById(m_LpePriceGroupId) %>.value;
		    args["TPOLandingPageID"] = <%=AspxTools.JsGetElementById(m_TPOLandingPage) %>.value;

		    args["RunPricingWithoutCredit"] = <%= AspxTools.JsGetElementById(m_CanRunPricingEngineWithoutCreditReport) %>.value;
		    args["ApplyForIneligibleLoanPrograms"] = <%= AspxTools.JsGetElementById(m_CanApplyForIneligibleLoanPrograms) %>.value;
		    args["SubmitWithoutCredit"] = <%= AspxTools.JsGetElementById(m_CanSubmitWithoutCreditReport) %>.value;
		    args["Status"] = <%= AspxTools.JsGetElementById(m_Status) %>.value;
		    args["Branch"] = <%= AspxTools.JsGetElementById(m_Branch) %>.value;

		    args["IsBrokerProcessor"] = <%= AspxTools.JsGetElementById(m_IsABrokerProcessor) %>.value;
		    args["IsLoanOfficer"] = <%= AspxTools.JsGetElementById(m_IsALoanOfficer) %>.value;

		    args["Allow4506T"] = <%= AspxTools.JsGetElementById(m_CanOrder4506T) %>.value;

            args["AllowViewWholesaleLoan"] = getElementValueOrNull('m_AllowViewWholesaleLoan');
		    args["AllowCreateWholesaleLoan"] = getElementValueOrNull('m_AllowCreateWholesaleLoan');

            args["AllowViewMinicorrLoan"] = getElementValueOrNull('m_AllowViewMiniCorrLoan');
            args["AllowCreateMinicorrLoan"] = getElementValueOrNull('m_AllowCreateMiniCorrLoan');

            args["AllowViewCorrLoan"] = getElementValueOrNull('m_AllowViewCorrLoan');
            args["AllowCreateCorrLoan"] = getElementValueOrNull('m_AllowCreateCorrLoan');

            args["IsExternalSecondary"] = document.getElementById('m_IsExternalSecondary').value;
            args["IsExternalPostCloser"] = document.getElementById('m_IsExternalPostCloser').value;
		  }

		    args["Mode"] = document.getElementById('mode').value;

		    args["EnabledIpRestriction"] = $("#EnabledIpRestrictionDdl").val();
		    args["EnabledClientDigitalCertificateInstall"] = $("#EnabledClientDigitalCertificateInstallDdl").val();
		    args["EnableAuthCodeViaSms"] = $("#EnableAuthCodeViaSmsDdl").val();

		    var mappedValues = $.map($(".tpoNavDdl"), function (ddl) { return ddl.value; });
		    args["TpoNavLinks"] = JSON.stringify(mappedValues);

		    if (frmPmlUserList.contentWindow)
		    {
		        frmPmlUserList.contentWindow.f_update(args);
		    }
		    else
		    {
		        frmPmlUserList.f_update(args);
		    }

		  ClearTPOLinks();
		  ResetSettings();

		  <%= AspxTools.JsGetElementById(m_BatchUpdateButton) %>.click();
		}

        function ResetSettings()
        {
            $("td[id$='BatchPanel']").each(function(){
                $(this).find('a:first').click();
            });

            $("#OptionsColumn select, #m_SupervisorDDL").each(function(){
                this.selectedIndex = 0;
            });


            $("#UseRelationshipsDdl, #UsePermissionDdl").each(function(){
                this.selectedIndex = 1;
            });

            $(".authentication-container select").each(function(){
                this.val = "-1";
            });

            updateSelectedUsersButton();
        }

        function VerifySupervisorChange()
        {
            if (frmPmlUserList.contentWindow)
            {
                var exSupervisors = frmPmlUserList.contentWindow.GetExPmlSupervisorsAssignedToUsers();
            }
            else
            {
                var exSupervisors = frmPmlUserList.GetExPmlSupervisorsAssignedToUsers();
            }

		    if (exSupervisors !== null && exSupervisors.length > 0) {
		        var message = null;

		        var exSupervisorList = JSON.parse(exSupervisors);

		        if (exSupervisorList.length === 1) {
		            message = "CANNOT COMPLETE UPDATE!\n\n"
		            + exSupervisorList[0]
                    + " is a supervisor for other users. Please update the supervisor for those users before proceeding with this change.";
		        }
		        else {
		            message = "CANNOT COMPLETE UPDATE!\n\nThe following users are supervisors for other users. Please update the supervisor for those users before proceeding with this change.\n\n";

		            // Limit the number of displayed supervisors to 15 to prevent the alert from becoming
		            // too long when editing many supervisors.
		            if (exSupervisorList.length > 15) {
		                exSupervisorList = exSupervisorList.slice(0, 15);
		                exSupervisorList.push("...");
		            }

		            message += exSupervisorList.join("\n");
		        }

		        alert(message);

		        return false;
		    }

		    return true;
		}

		function GetExSupervisorEmployeeIdsToIgnore()
        {
		    if (frmPmlUserList.contentWindow)
		    {
		        var exSupervisors = frmPmlUserList.contentWindow.GetSelectedPmlSupervisors();
		    }
		    else
		    {
		        var exSupervisors = frmPmlUserList.GetSelectedPmlSupervisors();
		    }

		    if (exSupervisors !== null && exSupervisors.length > 0) {
		        return exSupervisors.join(",");
		    }

		    return null;
		}

		function f_display(sView)
		{
		    if (frmPmlUserList.contentWindow)
		    {
		        frmPmlUserList.contentWindow.f_display(sView);
		    }
		    else
		    {
		        frmPmlUserList.f_display(sView);
		    }
		}

		function updateSearchButton()
		{
			var name = <%= AspxTools.JsGetElementById(m_SearchFilter) %>.value;
			var pricegroup = <%= AspxTools.JsGetElementById(m_PriceGroups) %>.value;
			var supervisor = document.getElementById(<%= AspxTools.JsString(m_SupervisorChoice.DataID) %>).value;
			var date = <%= AspxTools.JsGetElementById(m_lDate) %>.value;
			var branch = <%= AspxTools.JsGetElementById(m_branchDDL) %>.value;
			var status = <%= AspxTools.JsGetElementById(m_ActiveStatus) %>.value;
			var relationship = <%= AspxTools.JsGetElementById(m_relationshipDdl) %>.value;

			if( (name.replace(/^\s*|\s*$/g,"") == "") && (pricegroup == <%= AspxTools.JsString(Guid.Empty) %>)
			&& (supervisor == "") && (relationship == "Choose") && (date == "") && (branch == <%= AspxTools.JsString(Guid.Empty) %>) && (status == "") )
				document.getElementById('searchBtn').disabled=true;
			else
				document.getElementById('searchBtn').disabled=false;
		}

		function showRelationshipChooser( relationship ) {
		  try {
		    var i;
		    var correspondentPanels = [
    	      'CreditAuditorPanel',
		      'LegalAuditorPanel',
		      'PurchaserPanel',
		      'SecondaryPanel'
		    ];

		    clearChooser( document.getElementById('LenderAccountExecPanel') );
		    clearChooser( document.getElementById('UnderwriterPanel') );
		    clearChooser( document.getElementById('JuniorUnderwriterPanel') );
		    clearChooser( document.getElementById('LockDeskPanel') );
		    clearChooser( document.getElementById('ManagerPanel') );
		    clearChooser( document.getElementById('ProcessorPanel') );
		    clearChooser( document.getElementById('JuniorProcessorPanel') );

		    for (i = 0; i < correspondentPanels.length; i++) {
		      var panel = document.getElementById(correspondentPanels[i]);
		      if (!!panel) {
		          clearChooser(panel);
		      }
		    }

		    document.getElementById('ChoosePanel').style.display = "none";
		    document.getElementById(relationship + 'Panel').style.display = 'inline';

		    updateSearchButton();
 		  } catch (e) { }
		}

		function clearChooser( oPanel )
		{
		  oPanel.style.display = "none";
		  var Links = oPanel.getElementsByTagName('A');
		  for (var i = 0; i < Links.length; i++)
		    if ( Links[i].innerText == 'allow any' )
			  Links[i].click();
		}

		function allAreCurrent()
		{
		    var enabledIpRestriction = $("#EnabledIpRestrictionDdl").val();
		    var enabledClientDigitalCertificateInstall = $("#EnabledClientDigitalCertificateInstallDdl").val();
		    var enableAuthCodeViaSms = $("#EnableAuthCodeViaSmsDdl").val();
			var lenderAccExecId = document.getElementById(<%= AspxTools.JsString(m_LenderAccountExecBatchChoice.DataID) %>).value;
			var underwriterId = document.getElementById(<%=AspxTools.JsString(m_UnderwriterBatchChoice.DataID)%>).value;
			var juniorUnderwriterId = document.getElementById(<%=AspxTools.JsString(m_JuniorUnderwriterBatchChoice.DataID)%>).value;
			var lockDeskId = document.getElementById(<%=AspxTools.JsString(m_LockDeskBatchChoice.DataID)%>).value;
			var managerId = document.getElementById(<%=AspxTools.JsString(m_ManagerBatchChoice.DataID)%>).value;
			var processorId = document.getElementById(<%=AspxTools.JsString(m_ProcessorBatchChoice.DataID)%>).value;
			var juniorProcessorId = document.getElementById(<%=AspxTools.JsString(m_JuniorProcessorBatchChoice.DataID)%>).value;
			var pricingEngine = <%= AspxTools.JsGetElementById(m_CanRunPricingEngineWithoutCreditReport) %>.value;
			var applyIneligible = <%= AspxTools.JsGetElementById(m_CanApplyForIneligibleLoanPrograms) %>.value;
			var submitLoans = <%= AspxTools.JsGetElementById(m_CanSubmitWithoutCreditReport) %>.value;
			var allow4506T = <%= AspxTools.JsGetElementById(m_CanOrder4506T) %>.value;

            var allowViewWholesaleLoan = getElementValueOrNull('m_AllowViewWholesaleLoan');
            var allowCreateWholesaleLoan = getElementValueOrNull('m_AllowCreateWholesaleLoan');

            var allowViewMiniCorrLoan = getElementValueOrNull('m_AllowViewMiniCorrLoan');
            var allowCreateMiniCorrLoan = getElementValueOrNull('m_AllowCreateMiniCorrLoan');

            var allowViewCorrLoan = getElementValueOrNull('m_AllowViewCorrLoan');
            var allowCreateCorrLoan = getElementValueOrNull('m_AllowCreateCorrLoan');

			var status = <%= AspxTools.JsGetElementById(m_Status) %>.value;
			var priceGroup = <%= AspxTools.JsGetElementById(m_LpePriceGroupId) %>.value;
			var tpoLandingPageId = <%= AspxTools.JsGetElementById(m_TPOLandingPage) %>.value;
			var supervisor = <%= AspxTools.JsGetElementById(m_SupervisorDDL) %>.value;
			var branch = <%= AspxTools.JsGetElementById(m_Branch) %>.value;
			var isBrokerProcessor = <%= AspxTools.JsGetElementById(m_IsABrokerProcessor) %>.value;
			var isLoanOfficer = <%= AspxTools.JsGetElementById(m_IsALoanOfficer) %>.value;
			var isExternalSecondaryCurrent = document.getElementById('m_IsExternalSecondary').value == -1;
			var isExternalPostCloserCurrent = document.getElementById('m_IsExternalPostCloser').value == -1;

			var correspondentChoices = [
			    <%= AspxTools.JsString(m_CreditAuditorBatchChoice.DataID) %>,
			    <%= AspxTools.JsString(m_LegalAuditorBatchChoice.DataID) %>,
			    <%= AspxTools.JsString(m_PurchaserBatchChoice.DataID) %>,
			    <%= AspxTools.JsString(m_SecondaryBatchChoice.DataID) %>
			];

			var areAllCorrespondentChoicesCurrent = true;

			for (var i = 0; i < correspondentChoices.length && areAllCorrespondentChoicesCurrent; i++) {
			    var choice = document.getElementById(correspondentChoices[i]);
			    var value = '';
			    if (!!choice) {
			        value = choice.value;
			        areAllCorrespondentChoicesCurrent = areAllCorrespondentChoicesCurrent &&
    		            (value == 'Current' || value == '');
			    }
			}

            var useUserSettingValue = <%=AspxTools.JsString(DataAccess.EmployeePopulationMethodT.IndividualUser)%>;

            var isUseRelationshipCurrent = $(<%=AspxTools.JsGetElementById(UseRelationshipsDdl)%>).val() === useUserSettingValue;

            var isUsePermissionCurrent = $(<%=AspxTools.JsGetElementById(UsePermissionDdl)%>).val() === useUserSettingValue;

			if( (lenderAccExecId == "Current" || lenderAccExecId == "") && (underwriterId == "Current" || underwriterId == "")
			&& (lockDeskId == "Current" || lockDeskId == "") && (managerId == "Current" || managerId == "")
			&& (processorId == "Current" || processorId == "") && (pricingEngine == -1) && (applyIneligible == -1)
			&& (allow4506T == -1) && (allowCreateWholesaleLoan === null || allowCreateWholesaleLoan == -1) && (allowCreateMiniCorrLoan === null || allowCreateMiniCorrLoan == -1) && (allowCreateCorrLoan === null || allowCreateCorrLoan == -1)  && (submitLoans == -1) && (status == -1) && (priceGroup == "") && (supervisor == "Current" || supervisor == "") && (branch == "")
			&& (isBrokerProcessor == -1) && (isLoanOfficer == -1) && (juniorUnderwriterId == "Current" || juniorUnderwriterId == "")
			&& (juniorProcessorId == "Current" || juniorProcessorId == "") && (tpoLandingPageId == "") && GetUpdatedTPOLinks().length == 0
			&& areAllCorrespondentChoicesCurrent && isExternalSecondaryCurrent && isExternalPostCloserCurrent
            && isUsePermissionCurrent && isUseRelationshipCurrent
            && (allowViewWholesaleLoan === null || allowViewWholesaleLoan == -1) && (allowViewMiniCorrLoan === null || allowViewMiniCorrLoan == -1) && (allowViewCorrLoan === null || allowViewCorrLoan == -1)
            && (enabledIpRestriction == "-1") && (enabledClientDigitalCertificateInstall == "-1") && (enableAuthCodeViaSms == "-1")
            )
			{
				return true;
			}

			return false;
		}

		function updateSelectedUsersButton()
		{
			document.getElementById("BatchUserUpdateBtn").disabled = (allAreCurrent())?true:false;
		}

		function setBatchControls(setValue)
		{
		    $(<%=AspxTools.JsGetElementById(UseRelationshipsDdl)%>).prop("disabled", setValue === false);
            $(<%=AspxTools.JsGetElementById(UsePermissionDdl)%>).prop("disabled", setValue === false);

		    setRelationshipColumnControls(setValue);
		    enableMfaControls(setValue);
		    setPermissionColumnControls(setValue);

		    document.getElementById("BatchUserUpdateBtn").disabled = allAreCurrent() ? true : (setValue === false);

		    if (setValue === false){
		        document.getElementById('PageVisibility').style.display = 'none';
		        ClearTPOLinks();
		    }
		}

		    function enableMfaControls(setValue) {
		        $("#EnabledIpRestrictionDdl").prop("disabled", !setValue);
		        $("#EnabledClientDigitalCertificateInstallDdl").prop("disabled", !setValue);
		        $("#EnableAuthCodeViaSmsDdl").prop("disabled", !setValue);
		    }

		function setRelationshipColumnControls(setValue)
		{
            var m_SupervisorDDL = <%= AspxTools.JsGetElementById(m_SupervisorDDL) %>;
		    disableChooser("LenderAccountExecBatchPanel", setValue );
		    disableChooser("UnderwriterBatchPanel", setValue );
		    disableChooser("JuniorUnderwriterBatchPanel", setValue );
		    disableChooser("LockDeskBatchPanel", setValue );
		    disableChooser("ManagerBatchPanel", setValue );
		    disableChooser("ProcessorBatchPanel", setValue );
		    disableChooser("JuniorProcessorBatchPanel", setValue );
		    disableChooser("SupervisorBatchPanel", setValue && m_SupervisorDDL.value == 'Not' );

	        var correspondentPanels = [
	            'CreditAuditorBatchPanel',
	            'LegalAuditorBatchPanel',
	            'PurchaserBatchPanel',
	            'SecondaryBatchPanel'
	        ];

	        for (var i = 0; i < correspondentPanels.length; i++) {
	            var panelId = correspondentPanels[i];
	            var panel = document.getElementById(panelId);

	            // Only disable the panel if it is present,
	            // i.e. the page is in a correspondent mode.
	            if (!!panel) {
	                // The disableChooser call accepts an ID,
                    // not an object.
	                disableChooser(panelId, setValue);
	            }
	        }

	        m_SupervisorDDL.disabled = setValue === false;
		}

		function setPermissionColumnControls(setValue)
		{
            <%= AspxTools.JsGetElementById(m_LpePriceGroupId) %>.disabled = setValue === false;
		    <%= AspxTools.JsGetElementById(m_TPOLandingPage) %>.disabled = setValue === false;
		    <%= AspxTools.JsGetElementById(m_CanRunPricingEngineWithoutCreditReport) %>.disabled = setValue === false;
		    <%= AspxTools.JsGetElementById(m_CanApplyForIneligibleLoanPrograms) %>.disabled = setValue === false;
		    <%= AspxTools.JsGetElementById(m_CanSubmitWithoutCreditReport) %>.disabled = setValue === false;
		    <%= AspxTools.JsGetElementById(m_CanOrder4506T) %>.disabled = setValue === false;

		    var allowViewWholesaleLoan = document.getElementById('m_AllowViewWholesaleLoan');
		    if (allowViewWholesaleLoan) {
		        allowViewWholesaleLoan.disabled = setValue === false;
		    }

		    var allowCreateWholesaleLoan = document.getElementById('m_AllowCreateWholesaleLoan');
		    if (allowCreateWholesaleLoan) {
		        allowCreateWholesaleLoan.disabled = setValue === false;
		    }

		    var allowViewMiniCorrLoan = document.getElementById('m_AllowViewMiniCorrLoan');
		    if (allowViewMiniCorrLoan) {
		        allowViewMiniCorrLoan.disabled = setValue === false;
		    }

		    var allowCreateMiniCorrLoan = document.getElementById('m_AllowCreateMiniCorrLoan');
		    if (allowCreateMiniCorrLoan) {
		        allowCreateMiniCorrLoan.disabled = setValue === false;
		    }

		    var allowViewCorrLoan = document.getElementById('m_AllowViewCorrLoan');
		    if (allowViewCorrLoan) {
		        allowViewCorrLoan.disabled = setValue === false;
		    }

		    var allowCreateCorrLoan = document.getElementById('m_AllowCreateCorrLoan');
		    if (allowCreateCorrLoan) {
		        allowCreateCorrLoan.disabled = setValue === false;
		    }

            // Make sure we disable the create ddl if they aren't granting view permission.
		    $('.viewLoan').change();

		    <%= AspxTools.JsGetElementById(m_Status) %>.disabled = setValue === false;
		    <%= AspxTools.JsGetElementById(m_Branch) %>.disabled = setValue === false;
		    <%= AspxTools.JsGetElementById(m_IsABrokerProcessor) %>.disabled = setValue === false;
		    <%= AspxTools.JsGetElementById(m_IsALoanOfficer) %>.disabled = setValue === false;
		    <%= AspxTools.JsGetElementById(m_IsExternalSecondary) %>.disabled = setValue === false;
		    <%= AspxTools.JsGetElementById(m_IsExternalPostCloser) %>.disabled = setValue === false;

		    if (typeof(PGAnchor) != "undefined") {
		        setDisabledAttr(PGAnchor, setValue === false);
		    }
		}

		function setSupervisorBatchPanel()
		{
		  var m_SupervisorDDL = <%= AspxTools.JsGetElementById(m_SupervisorDDL) %>.value;
	      var supervisorData = document.getElementById(<%= AspxTools.JsString(m_Supervisor.DataID) %>);
	      var supervisorText = document.getElementById('m_Supervisor_m_User');
		  if (m_SupervisorDDL == 'Self')
		  {
		    supervisorText.innerText = 'None';
		    supervisorData.value = 'None';
		    disableChooser("SupervisorBatchPanel", false );
		  }
		  else if (m_SupervisorDDL == 'Not')
		  {
		    supervisorText.innerText = 'Keep Current';
		    supervisorData.value = '';
		    disableChooser("SupervisorBatchPanel", true );
		  }
		  else
		  {
		    supervisorText.innerText = 'Keep Current';
		    supervisorData.value = '';
		    disableChooser("SupervisorBatchPanel", false );
		  }

		  document.getElementById("BatchUserUpdateBtn").disabled = allAreCurrent();
		}

		function disableChooser(id, status) {
		    var control = $('#' + id);
		    setDisabledAttr(control, !status);

		    setDisabledAttr(control.find('div'), !status);

		    control.find("a").each(function() {
		        setDisabledAttr($(this), !status);
		    });
		}

		function clearText(tbId, index)
		{
			txtBox = document.getElementById(tbId);
			if(index == 0)
			{
				txtBox.value = "";
				txtBox.style.backgroundColor = "gainsboro";
				txtBox.disabled = true;
			}
			else
			{
				txtBox.style.backgroundColor = "white";
				txtBox.disabled = false;
			}
			updateSearchButton();
		}

		function f_showLastLoginDetails(event)
		{
			var msg = document.getElementById("LastLoginDetails");
			msg.style.display = "";
			msg.style.top = (event.clientY + 10) + "px";
			msg.style.left = (event.clientX - 225) + "px";
		}



		function f_closeDetails()
		{
			document.getElementById("LastLoginDetails").style.display = "none";
		}

		// 08/05/08 ck - OPM 21952. Exporting search results only, instead of the whole list
		function f_exportSearchResults()
		{
		    if (frmPmlUserList.contentWindow)
		    {
		        frmPmlUserList.contentWindow.f_exportSearchResults();
		    }
		    else
		    {
		        frmPmlUserList.f_exportSearchResults();
		    }
		}

		// 08/27/08 ck - OPM 21952. Checks to see if it should enable or disable the button
		function f_enableDisableExportSearchResultsButton()
		{
		    if (frmPmlUserList.contentWindow)
		    {
		        frmPmlUserList.contentWindow.f_enableDisableExportSearchResultsButton();
		    }
		    else
		    {
		        frmPmlUserList.f_enableDisableExportSearchResultsButton();
		    }

		}

		function GetUpdatedOrignatingId() {
		    return document.getElementById(<%= AspxTools.JsString(m_CompanyChoice.DataID) %>).value;
		}
	</script>
		<h4 class="page-header">Originating Company User List</h4>
		<FORM id="PmlUserList" method="post" runat="server">
			 <div id="PageVisibility" style="display: none; position: absolute; top: 10%;  left: 50%; z-index: 10000;   height: 250px; width: 500px; margin-left: -250px; background-color: white; border: 2px solid black; "  >
            <h2 style="padding: 5px; margin:0; " class="MainRightHeader">Custom Portal Page Visibility </h2>


                 <asp:Repeater runat="server" ID="m_CustomPortalPageVisibilityRepeater" OnItemDataBound="m_CustomPortalPageVisibilityRepeater_OnItemDataBound">
                     <HeaderTemplate>
                     <div style="height: 200px; overflow-y: auto; margin: 5px; ">

                     <table width="100%" >
                     <tbody>
                     </HeaderTemplate>
                     <ItemTemplate>
                     <tr><td style="font-weight: bold; ">
                                 <ML:EncodedLabel runat="server" ID="Name" AssociatedControlID="Option"></ML:EncodedLabel>
                                 </td>
                                 <td>
                                 <asp:DropDownList runat="server" ID="Option" onchange="updateSelectedUsersButton()"  CssClass="tpoNavDdl" >
                                 </asp:DropDownList>
                                 </td>
                                 </tr>
                     </ItemTemplate>
                     <FooterTemplate>
                     </tbody>
                     </table>
                     </div>
                     </FooterTemplate>
                 </asp:Repeater>
                 <input type="button"  style="position: absolute; font-weight: bold; margin: -15px 240px; 0 0" value=" OK "  onclick="document.getElementById('PageVisibility').style.display = 'none';">
			 </div>

			<asp:Panel id=m_AccessDeniedPanel style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
					<b>Access Denied.  You do not have permission to administrate PML users.</b>
			</asp:Panel>
			<asp:Panel id="m_AccessAllowedPanel" runat="server">
			<TABLE height="100%" width="100%" cellSpacing="2" cellPadding="0" border="0" align="center" class="mainTable">
				<tr style="*padding-top: 30px">
                    <td>
                        <span>User Admin Mode:</span>
                        <asp:RadioButtonList runat="server" ID="m_Mode" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true">
                            <asp:ListItem Value="0" Selected="True">Broker Settings</asp:ListItem>
                            <asp:ListItem Value="1">Mini-Correspondent Settings</asp:ListItem>
                            <asp:ListItem Value="2">Correspondent Settings</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
				</tr>
				<TR vAlign="top">
					<TD style="PADDING-RIGHT: 2px" height="0%" width="100%">
						<table class="InsetBorder" cellpadding="0" cellspacing="0" width="100%">
						    <tr>
						        <td class="FieldLabel">
						            Name
						        </td>
						        <td>
								    <asp:textbox onkeypress="if( event.keyCode == 13 ) { f_search(); return false; }" id="m_SearchFilter" onkeyup="updateSearchButton();" style="PADDING-LEFT: 4px; MARGIN-RIGHT: 4px" runat="server" Width="150px" MaxLength="64"></asp:textbox>
						        </td>
						        <td class="FieldLabel">
								    Last Login Date <a href='#"' onclick='f_showLastLoginDetails(event);' tabindex="-1">?</a>
						        </td>
						        <td>
								    <asp:dropdownlist id="m_BefAft" style="PADDING-LEFT: 4px; MARGIN-RIGHT: 8px" runat="server" Width="150px" onchange="clearText('m_lDate', this.selectedIndex);" align="left">
							            <asp:ListItem Value="before">Before</asp:ListItem>
							            <asp:ListItem Value="after">On or After</asp:ListItem>
						            </asp:dropdownlist>
					                <ml:datetextbox onkeypress="if( event.keyCode == 13 ) { f_search(); return false; }" id="m_lDate" runat="server" onchange="updateSearchButton();" colSpan="2" preset="date" width="75"></ml:datetextbox>
						        </td>
						    </tr>
						    <tr>
                                <td class="FieldLabel">
								    Company
								</td>
								<td>
								    <ils:employeerolechooserlink id="m_CompanyChoice" GenerateScripts="true" runat="server" Width="140px" Mode="Search" Desc="Originating Company" Role="Originating Company"
                                        Text="Any" Data="00000000-0000-0000-0000-000000000000">
										<FIXEDCHOICE Text="Any" Data="00000000-0000-0000-0000-000000000000">Any</FIXEDCHOICE>
										<FIXEDCHOICE Text="assign" Data="assign">Pick Company</FIXEDCHOICE>
									</ils:employeerolechooserlink>
								</td>
						        <td class="FieldLabel">
						            Status
						        </td>
						        <td>
								    <asp:dropdownlist id="m_ActiveStatus" style="PADDING-LEFT: 4px; MARGIN-RIGHT: 8px" runat="server" Width="150px" onchange="updateSearchButton();" align="left">
										<asp:ListItem Selected=True Value="active">Active</asp:ListItem>
										<asp:ListItem Value="inactive">Inactive</asp:ListItem>
									</asp:dropdownlist>
						        </td>
						    </tr>
						    <tr>
						        <td class="FieldLabel">
						            Supervisor
						        </td>
						        <td>
						        	<div id="SupervisorPanel" onclick="updateSearchButton();">
										<ils:employeerolechooserlink id="m_SupervisorChoice" runat="server" Width="140px" Mode="Search" Desc="Supervisor" Role="Supervisor" Text="Any" Data="Any">
											<FIXEDCHOICE Text="Any" Data="">Any</FIXEDCHOICE>
											<FIXEDCHOICE Text="None" Data="None">None</FIXEDCHOICE>
											<FIXEDCHOICE Text="assign" Data="assign">Pick Supervisor</FIXEDCHOICE>
										</ils:employeerolechooserlink>
									</div>
						        </td>
						        <td class="FieldLabel">
						            <ml:EncodedLiteral runat="server" ID="PriceGroupFilterDesc"></ml:EncodedLiteral> Price Group
						        </td>
						        <td>
							        <asp:dropdownlist id="m_PriceGroups" style="PADDING-LEFT: 4px; MARGIN-RIGHT: 4px" runat="server" Width="150px" onchange="updateSearchButton();"></asp:dropdownlist>
						        </td>
						    </tr>
						    <tr>
						        <td class="FieldLabel">
						            <ml:EncodedLiteral runat="server" ID="RelationshipFilterDesc"></ml:EncodedLiteral> Relationship
						        </td>
						        <td>
									<asp:dropdownlist id="m_relationshipDdl" style="PADDING-LEFT: 4px; MARGIN-RIGHT: 8px" runat="server" Width="150px" onchange="showRelationshipChooser(this.options[this.selectedIndex].value);">
										<asp:ListItem Value="Choose" Selected="True"><-- Any --></asp:ListItem>
										<asp:ListItem Value="Manager">Manager</asp:ListItem>
										<asp:ListItem Value="Processor">Processor</asp:ListItem>
										<asp:ListItem Value="JuniorProcessor">Junior Processor</asp:ListItem>
										<asp:ListItem Value="LenderAccountExec">Account Executive</asp:ListItem>
										<asp:ListItem Value="Underwriter">Underwriter</asp:ListItem>
										<asp:ListItem Value="JuniorUnderwriter">Junior Underwriter</asp:ListItem>
										<asp:ListItem Value="CreditAuditor">Credit Auditor</asp:ListItem>
										<asp:ListItem Value="LegalAuditor">Legal Auditor</asp:ListItem>
										<asp:ListItem Value="LockDesk">Lock Desk</asp:ListItem>
										<asp:ListItem Value="Purchaser">Purchaser</asp:ListItem>
										<asp:ListItem Value="Secondary">Secondary</asp:ListItem>
									</asp:dropdownlist>
									<div id="ChoosePanel" align="left" style="COLOR: dimgray">
									    &lt;&lt;&nbsp;Choose a role first ...
									</div>
						        </td>
						        <td class="FieldLabel">
						            <ml:EncodedLiteral runat="server" ID="BranchFilterDesc"></ml:EncodedLiteral> Branch
                                    <asp:requiredfieldvalidator id=m_R1 runat="server" Display="Static" ControlToValidate="m_branchDDL" EnableViewState="False">
                                        <img runat="server" src="../../images/error_icon_right.gif">
                                    </asp:requiredfieldvalidator>
						        </td>
						        <td>
						        	<asp:dropdownlist id="m_branchDDL" style="PADDING-LEFT: 4px; MARGIN-RIGHT: 8px" runat="server" Width="150px" onchange="updateSearchButton();"></asp:dropdownlist>
						        </td>

						    </tr>

						    <tr>
						        <td>
						        </td>
						        <td>
									<div id="ManagerPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_ManagerChoice" runat="server" Width="140px" Mode="Search" Desc="Manager" Role="Manager" Text="None" Data="None">
											<FIXEDCHOICE Text="None" Data="None">None</FIXEDCHOICE>
											<FIXEDCHOICE Text="assign" Data="assign">Pick Manager</FIXEDCHOICE>
										</ils:employeerolechooserlink>
									</div>
									<div id="ProcessorPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_ProcessorChoice" runat="server" Width="140px" Mode="Search" Desc="Processor" Role="Processor" Text="None" Data="None">
											<FIXEDCHOICE Text="None" Data="None">None </FIXEDCHOICE>
											<fixedchoice Text="assign" Data="assign">Pick Processor</fixedchoice>
										</ils:employeerolechooserlink>
									</div>
									<div id="JuniorProcessorPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_JuniorProcessorChoice" runat="server" Width="140px" Mode="Search" Desc="Junior Processor" Role="JuniorProcessor" Text="None" Data="None">
											<fixedchoice Text="None" Data="None">None </fixedchoice>
											<fixedchoice Text="assign" Data="assign">Pick Junior Processor</fixedchoice>
										</ils:employeerolechooserlink>
									</div>
						        	<div id="LenderAccountExecPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_LenderAccountExecChoice" runat="server" Width="140px" Mode="Search" Desc="Lender Account Executive" Role="LenderAccountExec" Text="None" Data="None">
											<FIXEDCHOICE Data="None" Text="None">None</FIXEDCHOICE>
											<FIXEDCHOICE Data="assign" Text="assign">Pick AE</FIXEDCHOICE>
										</ils:employeerolechooserlink>
									</div>
									<div id="UnderwriterPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_UnderwriterChoice" runat="server" Width="140px" Mode="Search" Desc="Underwriter" Role="Underwriter" Text="None" Data="None">
											<FIXEDCHOICE Text="None" Data="None">None</FIXEDCHOICE>
											<FIXEDCHOICE Text="assign" Data="assign">Pick Underwriter</FIXEDCHOICE>
										</ils:employeerolechooserlink>
									</div>
									<div id="JuniorUnderwriterPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_JuniorUnderwriterChoice" runat="server" Width="140px" Mode="Search" Desc="Junior Underwriter" Role="JuniorUnderwriter" Text="None" Data="None">
											<fixedchoice Text="None" Data="None">None</fixedchoice>
											<fixedchoice Text="assign" Data="assign">Pick Junior Underwriter</fixedchoice>
										</ils:employeerolechooserlink>
									</div>
									<div runat="server" id="CreditAuditorPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_CreditAuditorChoice" runat="server" Width="140px" Mode="Search" Desc="Credit Auditor" Role="CreditAuditor" Text="None" Data="None">
											<fixedchoice Text="None" Data="None">None </fixedchoice>
											<fixedchoice Text="assign" Data="assign">Pick Credit Auditor</fixedchoice>
										</ils:employeerolechooserlink>
									</div>
									<div runat="server" id="LegalAuditorPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_LegalAuditorChoice" runat="server" Width="140px" Mode="Search" Desc="Legal Auditor" Role="LegalAuditor" Text="None" Data="None">
											<fixedchoice Text="None" Data="None">None </fixedchoice>
											<fixedchoice Text="assign" Data="assign">Pick Legal Auditor</fixedchoice>
										</ils:employeerolechooserlink>
									</div>
									<div id="LockDeskPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_LockDeskChoice" runat="server" Width="140px" Mode="Search" Desc="Lock Desk" Role="LockDesk" Text="None" Data="None">
											<FIXEDCHOICE Text="None" Data="None">None </FIXEDCHOICE>
											<FIXEDCHOICE Text="assign" Data="assign" >Pick Lock Desk</FIXEDCHOICE>
										</ils:employeerolechooserlink>
									</div>
									<div runat="server" id="PurchaserPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_PurchaserChoice" runat="server" Width="140px" Mode="Search" Desc="Purchaser" Role="Purchaser" Text="None" Data="None">
											<fixedchoice Text="None" Data="None">None </fixedchoice>
											<fixedchoice Text="assign" Data="assign">Pick Purchaser</fixedchoice>
										</ils:employeerolechooserlink>
									</div>
									<div runat="server" id="SecondaryPanel" onclick="updateSearchButton();" style="DISPLAY: none">
										<ils:employeerolechooserlink id="m_SecondaryChoice" runat="server" Width="140px" Mode="Search" Desc="Secondary" Role="Secondary" Text="None" Data="None">
											<fixedchoice Text="None" Data="None">None </fixedchoice>
											<fixedchoice Text="assign" Data="assign">Pick Secondary</fixedchoice>
										</ils:employeerolechooserlink>
									</div>
                                </td>
						    </tr>
						    <tr style="padding-top:10px">
						        <td colspan="5">
						            <input class="ButtonStyle" id="searchBtn" disabled onclick="f_search();" type="button" value="Search" NoHighlight>
						        </td>
						    </tr>
                        </table>
					</TD>
				</TR>
				<tr>
					<td>
						<table id="Table1" width="100%">
							<tr>
								<td style="PADDING-LEFT: 5px"><input class="ButtonStyle" onclick="onAddNewClick(); return false;" type="button" value="Add new"></td>
								<td class="FieldLabel" style="PADDING-RIGHT: 5px" noWrap align="right">Change
									Display View: <input id="UsersView" onclick="f_display('users');" type="radio" CHECKED value="Users" name="DisplayView"><label for="UsersView">Users</label>
									<input id="RelationshipsView" onclick="f_display('relationships');" type="radio" value="Relationships" name="DisplayView"><label for="RelationshipsView"><ml:EncodedLiteral runat="server" ID="RelationshipViewDesc"></ml:EncodedLiteral> Relationships</label>
									<input id="SupervisorsView" onclick="f_display('supervisors');" type="radio" value="Supervisors" name="DisplayView"><label for="SupervisorsView">Supervisors</label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="PADDING-RIGHT: 5px; PADDING-LEFT: 5px" height="100%" width="100%"><iframe id="frmPmlUserList" src="PmlUserListResults.aspx" width="100%" scrolling="yes" height="100%">
						</iframe>
					</td>
				</tr>
				<TR vAlign="top">
					<TD style="PADDING-RIGHT: 2px" align="left">
						<div style_ob="BORDER-RIGHT: 2px groove; PADDING-RIGHT: 2px; BORDER-TOP: 2px groove; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; BORDER-LEFT: 2px groove; PADDING-TOP: 8px; BORDER-BOTTOM: 2px groove; BACKGROUND-COLOR: gainsboro">
							<table class="InsetBorder" id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td colSpan="3">
										<div class="FieldLabel" style="MARGIN-BOTTOM: 8px; PADDING-BOTTOM: 4px; BORDER-BOTTOM: lightgrey 2px dotted">Select
											users above, then specify new <ml:EncodedLiteral runat="server" ID="m_RelationshipDesc"></ml:EncodedLiteral> relationships and options...
										</div>
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap>
										<table id="Table3" cellSpacing="0" cellPadding="2" border="0">
                                            <tr>
                                                <td colspan="3" class="FieldLabel">
                                                    Use relationships &nbsp;
                                                    <asp:DropDownList ID="UseRelationshipsDdl" runat="server" AutoPostBack="false"
                                                        onchange="updateSelectedUsersButton();"></asp:DropDownList>
                                                </td>
                                            </tr>
											<!-- Manager -->
											<tr>
												<td class="FieldLabel" noWrap>Manager
												</td>
												<td noWrap width="2"></td>
												<td id="ManagerBatchPanel" onclick="updateSelectedUsersButton();"><ils:employeerolechooserlink id="m_ManagerBatchChoice" runat="server" Width="100px" Mode="Search" Desc="Manager" Role="Manager" Text="Keep Current" Data="Current"><FIXEDCHOICE Data="" Text="Keep Current">Current </FIXEDCHOICE>
														<FIXEDCHOICE Data="00000000-0000-0000-0000-000000000000" Text="None">None </FIXEDCHOICE>
														<FIXEDCHOICE Data="assign" Text="assign" >Pick Manager</FIXEDCHOICE>
													</ils:employeerolechooserlink></td>
											</tr>
											<!-- Processor -->
											<tr>
												<td class="FieldLabel" noWrap>Processor
												</td>
												<td noWrap width="2"></td>
												<td id="ProcessorBatchPanel" onclick="updateSelectedUsersButton();">
													<ils:employeerolechooserlink id="m_ProcessorBatchChoice" runat="server" Width="100px" Mode="Search" Desc="Processor" Role="Processor" Text="Keep Current" Data="Current">
														<FIXEDCHOICE Data="" Text="Keep Current">Current </FIXEDCHOICE>
														<FIXEDCHOICE Data="00000000-0000-0000-0000-000000000000" Text="None">None </FIXEDCHOICE>
														<FIXEDCHOICE Data="assign" Text="assign" >Pick Processor</FIXEDCHOICE>
													</ils:employeerolechooserlink></td>
											</tr>
											<!--Junior  Processor -->
											<tr>
												<td class="FieldLabel" noWrap>Junior Processor
												</td>
												<td noWrap width="2"></td>
												<td id="JuniorProcessorBatchPanel" onclick="updateSelectedUsersButton();">
													<ils:employeerolechooserlink id="m_JuniorProcessorBatchChoice" runat="server" Width="100px" Mode="Search" Desc="Junior Processor" Role="JuniorProcessor" Text="Keep Current" Data="Current">
														<fixedchoice Data="" Text="Keep Current">Current </fixedchoice>
														<fixedchoice Data="00000000-0000-0000-0000-000000000000" Text="None">None </fixedchoice>
														<fixedchoice Data="assign" Text="assign" >Pick Junior Processor</fixedchoice>
													</ils:employeerolechooserlink></td>
											</tr>
										    <!-- Account Exec -->
											<tr>
												<td class="FieldLabel" noWrap width="100">Account Executive
												</td>
												<td noWrap width="2"></td>
												<td id="LenderAccountExecBatchPanel" onclick="updateSelectedUsersButton();">
												<ils:employeerolechooserlink id="m_LenderAccountExecBatchChoice" GenerateScripts="true" runat="server" Width="100px" Mode="Search" Desc="Lender Account Executive" Role="LenderAccountExec" Text="Keep Current" Data="Current">
												<FIXEDCHOICE Data="" Text="Keep Current">Current </FIXEDCHOICE>
												<FIXEDCHOICE Data="00000000-0000-0000-0000-000000000000" Text="None">None </FIXEDCHOICE>
												<FIXEDCHOICE Data="assign" Text="assign" >Pick AE</FIXEDCHOICE>
													</ils:employeerolechooserlink></td>
											</tr>
											<!-- Underwriter -->
											<tr>
												<td class="FieldLabel" noWrap width="100">Underwriter
												</td>
												<td noWrap width="2"></td>
												<td id="UnderwriterBatchPanel" onclick="updateSelectedUsersButton();">
												<ils:employeerolechooserlink id="m_UnderwriterBatchChoice" runat="server" Width="100px" Mode="Search" Desc="Underwriter" Role="Underwriter" Text="Keep Current" Data="Current">
														<FIXEDCHOICE Data="" Text="Keep Current">Current </FIXEDCHOICE>
														<FIXEDCHOICE Data="00000000-0000-0000-0000-000000000000" Text="None">None </FIXEDCHOICE>
														<FIXEDCHOICE Data="assign" Text="assign" >Pick Underwriter</FIXEDCHOICE>
													</ils:employeerolechooserlink></td>
											</tr>
											<!-- Junior  Underwriter -->
											<tr>
												<td class="FieldLabel" noWrap width="120">Junior Underwriter
												</td>
												<td noWrap width="2"></td>
												<td id="JuniorUnderwriterBatchPanel" onclick="updateSelectedUsersButton();">
												<ils:employeerolechooserlink id="m_JuniorUnderwriterBatchChoice" runat="server" Width="100px" Mode="Search" Desc="Junior Underwriter" Role="JuniorUnderwriter" Text="Keep Current" Data="Current">
														<fixedchoice Data="" Text="Keep Current">Current </fixedchoice>
														<fixedchoice Data="00000000-0000-0000-0000-000000000000" Text="None">None </fixedchoice>
														<fixedchoice Data="assign" Text="assign" >Pick Junior Underwriter</fixedchoice>
													</ils:employeerolechooserlink></td>
											</tr>
											<!-- Credit Auditor -->
											<tr runat="server" id="CreditAuditorSelectionRow">
											    <td class="FieldLabel" width="120">Credit Auditor</td>
											    <td width="2"></td>
											    <td id="CreditAuditorBatchPanel" onclick="updateSelectedUsersButton();">
											        <ils:EmployeeRoleChooserLink ID="m_CreditAuditorBatchChoice" runat="server" Width="100px" Mode="Search" Desc="Credit Auditor" Role="CreditAuditor" Text="Keep Current" Data="Current">
														<fixedchoice Data="" Text="Keep Current">Current</fixedchoice>
														<fixedchoice Data="00000000-0000-0000-0000-000000000000" Text="None">None</fixedchoice>
														<fixedchoice Data="assign" Text="assign">Pick Credit Auditor</fixedchoice>
											        </ils:EmployeeRoleChooserLink>
											    </td>
											</tr>
											<!-- Legal Auditor -->
											<tr runat="server" id="LegalAuditorSelectionRow">
											    <td class="FieldLabel" width="120">Legal Auditor</td>
											    <td width="2"></td>
											    <td id="LegalAuditorBatchPanel" onclick="updateSelectedUsersButton();">
											        <ils:EmployeeRoleChooserLink ID="m_LegalAuditorBatchChoice" runat="server" Width="100px" Mode="Search" Desc="Legal Auditor" Role="LegalAuditor" Text="Keep Current" Data="Current">
														<fixedchoice Data="" Text="Keep Current">Current</fixedchoice>
														<fixedchoice Data="00000000-0000-0000-0000-000000000000" Text="None">None</fixedchoice>
														<fixedchoice Data="assign" Text="assign">Pick Legal Auditor</fixedchoice>
											        </ils:EmployeeRoleChooserLink>
											    </td>
											</tr>
											<!--Lock  Desk -->
											<tr>
												<td class="FieldLabel" noWrap>Lock Desk
												</td>
												<td noWrap width="2"></td>
												<td id="LockDeskBatchPanel" onclick="updateSelectedUsersButton();"><ils:employeerolechooserlink id="m_LockDeskBatchChoice" runat="server" Width="100px" Mode="Search" Desc="Lock Desk" Role="LockDesk" Text="Keep Current" Data="Current"><FIXEDCHOICE Data="" Text="Keep Current">Current </FIXEDCHOICE>
														<FIXEDCHOICE Data="00000000-0000-0000-0000-000000000000" Text="None">None </FIXEDCHOICE>
														<FIXEDCHOICE Data="assign" Text="assign" >Pick Lock Desk</FIXEDCHOICE>
													</ils:employeerolechooserlink></td>
											</tr>
											<!-- Purchaser -->
											<tr runat="server" id="PurchaserSelectionRow">
											    <td class="FieldLabel" width="120">Purchaser</td>
											    <td width="2"></td>
											    <td id="PurchaserBatchPanel" onclick="updateSelectedUsersButton();">
											        <ils:EmployeeRoleChooserLink ID="m_PurchaserBatchChoice" runat="server" Width="100px" Mode="Search" Desc="Purchaser" Role="Purchaser" Text="Keep Current" Data="Current">
														<fixedchoice Data="" Text="Keep Current">Current</fixedchoice>
														<fixedchoice Data="00000000-0000-0000-0000-000000000000" Text="None">None</fixedchoice>
														<fixedchoice Data="assign" Text="assign">Pick Purchaser</fixedchoice>
											        </ils:EmployeeRoleChooserLink>
											    </td>
											</tr>
											<!-- Secondary -->
											<tr runat="server" id="SecondarySelectionRow">
											    <td class="FieldLabel" width="120">Secondary</td>
											    <td width="2"></td>
											    <td id="SecondaryBatchPanel" onclick="updateSelectedUsersButton();">
											        <ils:EmployeeRoleChooserLink ID="m_SecondaryBatchChoice" runat="server" Width="100px" Mode="Search" Desc="Secondary" Role="Secondary" Text="Keep Current" Data="Current">
														<fixedchoice Data="" Text="Keep Current">Current</fixedchoice>
														<fixedchoice Data="00000000-0000-0000-0000-000000000000" Text="None">None</fixedchoice>
														<fixedchoice Data="assign" Text="assign">Pick Secondary</fixedchoice>
											        </ils:EmployeeRoleChooserLink>
											    </td>
											</tr>
										</table>
										<hr style="BORDER-RIGHT: 2px groove; BORDER-TOP: 2px groove; MARGIN-LEFT: 2px; BORDER-LEFT: 2px groove; MARGIN-RIGHT: 2px; BORDER-BOTTOM: 2px groove">
										<table id="Table4" cellSpacing="0" cellPadding="2">
										    <tr>
										        <td class="FieldLabel">Is a Supervisor
										        </td>
												<td noWrap width="2"></td>
												<td>
												    <asp:DropDownList ID="m_SupervisorDDL" runat="server" onchange="setSupervisorBatchPanel();"></asp:DropDownList>
												</td>
										    </tr>
											<tr>
												<td class="FieldLabel" noWrap width="90" style="padding-left:10px">Supervised by
												</td>
												<td noWrap width="2"></td>
												<td id="SupervisorBatchPanel" onclick="updateSelectedUsersButton();">
													<ils:employeerolechooserlink id="m_Supervisor" runat="server" Width="100px" Mode="Search" Desc="Supervisor" Role="Supervisor" Text="Keep Current" Data="Current">
														<FIXEDCHOICE Data="" Text="Keep Current">Current </FIXEDCHOICE>
														<FIXEDCHOICE Data="00000000-0000-0000-0000-000000000000" Text="None">None</FIXEDCHOICE>
														<FIXEDCHOICE Data="assign" Text="assign">Pick Supervisor</FIXEDCHOICE>
													</ils:employeerolechooserlink>
												</td>
											</tr>
										</table>

                                        <fieldset class="authentication-container">
                                            <legend>Authentication</legend>

                                            <div>
                                                <label>IP address restriction</label>
                                                <asp:DropDownList ID="EnabledIpRestrictionDdl" runat="server"></asp:DropDownList>
                                            </div>
                                            <div>
                                                <label>Allow user to install digital certificate</label>
                                                <asp:DropDownList ID="EnabledClientDigitalCertificateInstallDdl" runat="server"></asp:DropDownList>
                                            </div>
                                            <div>
                                                <label>Enable auth code transmission via SMS text</label>
                                                <asp:DropDownList ID="EnableAuthCodeViaSmsDdl" runat="server"></asp:DropDownList>
                                            </div>

                                        </fieldset>
									</td>
									<td ></td>
									<td vAlign="top" noWrap>
										<table id="OptionsColumn" cellSpacing="0" cellPadding="2">
                                            <tr>
                                                <td colspan="2" class="FieldLabel">
                                                    Use permissions &nbsp;
                                                    <asp:DropDownList ID="UsePermissionDdl" runat="server" AutoPostBack="false"
                                                        onchange="updateSelectedUsersButton();"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <asp:PlaceHolder runat="server" ID="BrokerViewCreateSettings">
                                                <tr>
                                                    <td class="FieldLabel" noWrap>Allow viewing wholesale channel loans.
                                                    </td>
                                                    <td noWrap><asp:dropdownlist id="m_AllowViewWholesaleLoan" runat="server" Width="190px" CssClass="viewLoan">
                                                            <asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:dropdownlist></td>
                                                </tr>
                                                <tr>
												<td class="FieldLabel" noWrap>Allow creating wholesale channel loans.
												</td>
												<td noWrap><asp:dropdownlist id="m_AllowCreateWholesaleLoan" onChange="updateSelectedUsersButton();" runat="server" Width="190px" CssClass="createLoan">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Yes</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</asp:dropdownlist></td>
											</tr>
                                            </asp:PlaceHolder>
                                            <asp:PlaceHolder runat="server" ID="MiniCorrViewCreateSettings">
                                                <tr>
                                                    <td class="FieldLabel" noWrap>Allow viewing mini-correspondent channel loans.
                                                    </td>
                                                    <td noWrap><asp:dropdownlist id="m_AllowViewMiniCorrLoan" runat="server" Width="190px" CssClass="viewLoan">
                                                            <asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:dropdownlist></td>
                                                </tr>
                                                <tr>
                                                    <td class="FieldLabel" noWrap>Allow creating mini-correspondent channel loans.
                                                    </td>
                                                    <td noWrap><asp:dropdownlist id="m_AllowCreateMiniCorrLoan" runat="server" Width="190px" CssClass="createLoan">
                                                            <asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:dropdownlist></td>
                                                </tr>
                                            </asp:PlaceHolder>
                                            <asp:PlaceHolder runat="server" ID="CorrViewCreateSettings">
                                                <tr>
                                                    <td class="FieldLabel" noWrap>Allow viewing correspondent channel loans.
                                                    </td>
                                                    <td noWrap><asp:dropdownlist id="m_AllowViewCorrLoan" runat="server" Width="190px" CssClass="viewLoan">
                                                            <asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:dropdownlist></td>
                                                </tr>
                                                <tr>
												<td class="FieldLabel" noWrap>Allow creating correspondent channel loans.
												</td>
												<td noWrap><asp:dropdownlist id="m_AllowCreateCorrLoan" onChange="updateSelectedUsersButton();" runat="server" Width="190px" CssClass="createLoan">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Yes</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</asp:dropdownlist></td>
											</tr>
										    </asp:PlaceHolder>
											<tr>
												<td class="FieldLabel" noWrap>Can run PriceMyLoan without credit report
												</td>
												<td noWrap><asp:dropdownlist id="m_CanRunPricingEngineWithoutCreditReport" onChange="updateSelectedUsersButton();" runat="server" Width="190px">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Yes</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</asp:dropdownlist></td>
											</tr>
											<tr>
												<td class="FieldLabel" noWrap>Can register/lock loans w/o credit report</td>
												<td noWrap><asp:dropdownlist id="m_CanSubmitWithoutCreditReport" onChange="updateSelectedUsersButton();" runat="server" Width="190px">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Yes</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</asp:dropdownlist>
												</td>
											</tr>
											<tr>
												<td class="FieldLabel" noWrap>Can apply for ineligible loan programs</td>
												<td noWrap><asp:dropdownlist id="m_CanApplyForIneligibleLoanPrograms" onChange="updateSelectedUsersButton();" runat="server" Width="190px">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Yes</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</asp:dropdownlist>
												</td>
											</tr>
											<tr>
												<td class="FieldLabel" noWrap>Can order 4506T</td>
												<td noWrap><asp:dropdownlist id="m_CanOrder4506T" onChange="updateSelectedUsersButton();" runat="server" Width="190px">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Yes</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</asp:dropdownlist>
												</td>
											</tr>
											<tr>
												<td class="FieldLabel" noWrap>Status
												</td>
												<td noWrap><asp:dropdownlist id="m_Status" onChange="updateSelectedUsersButton();" runat="server" Width="190px">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Active</asp:ListItem>
														<asp:ListItem Value="0">Inactive</asp:ListItem>
													</asp:dropdownlist>
												</td>
											</tr>
											<tr>
												<td class="FieldLabel" noWrap>Is an External Loan Officer</td>
												<td noWrap><asp:dropdownlist id="m_IsALoanOfficer" onChange="updateSelectedUsersButton();" runat="server" Width="190px">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Yes</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</asp:dropdownlist>
												</td>
											</tr>
											<tr>
												<td class="FieldLabel" noWrap>Is an External Processor</td>
												<td noWrap><asp:dropdownlist id="m_IsABrokerProcessor" onChange="updateSelectedUsersButton();" runat="server" Width="190px">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Yes</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</asp:dropdownlist>
												</td>
											</tr>
											<tr runat="server" id="IsExternalSecondaryRow">
												<td class="FieldLabel" noWrap>Is an External Secondary</td>
												<td noWrap><asp:dropdownlist id="m_IsExternalSecondary" onChange="updateSelectedUsersButton();" runat="server" Width="190px">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Yes</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</asp:dropdownlist>
												</td>
											</tr>
											<tr runat="server" id="IsExternalPostCloserRow">
												<td class="FieldLabel" noWrap>Is an External Post-Closer</td>
												<td noWrap><asp:dropdownlist id="m_IsExternalPostCloser" onChange="updateSelectedUsersButton();" runat="server" Width="190px">
														<asp:ListItem Value="-1" Selected="True"><--Keep Current--></asp:ListItem>
														<asp:ListItem Value="1">Yes</asp:ListItem>
														<asp:ListItem Value="0">No</asp:ListItem>
													</asp:dropdownlist>
												</td>
											</tr>
											<tr>
												<td class="FieldLabel" noWrap width="200"><ml:EncodedLiteral runat="server" ID="PriceGroupDesc"></ml:EncodedLiteral> Price Group
												</td>
												<td><asp:dropdownlist id="m_LpePriceGroupId" onChange="updateSelectedUsersButton();" runat="server" Width="190px"></asp:dropdownlist></td>
											</tr>
											<tr>
												<td class="FieldLabel" noWrap width="200"><ml:EncodedLiteral runat="server" ID="LandingPageDesc"></ml:EncodedLiteral> Landing Page
												</td>
												<td><asp:dropdownlist id="m_TPOLandingPage" onChange="updateSelectedUsersButton();" runat="server" Width="190px"></asp:dropdownlist></td>
											</tr>
											<tr>
												<td class="FieldLabel" noWrap width="200"><ml:EncodedLiteral runat="server" ID="BranchDesc"></ml:EncodedLiteral> Branch
												</td>
												<td noWrap><asp:dropdownlist id="m_Branch" onChange="updateSelectedUsersButton();" runat="server" Width="190px"></asp:dropdownlist></td>
											</tr>
											<tr runat="server" id="ShowCustomPortalPageVisibility">
											    <td class="FieldLabel" noWrap>Custom Portal Page Visibility</td>
											    <td> <a href="#" onclick="return f_showPage(this)" id="PGAnchor" >edit...</a> </td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</TD>
				</TR>
				<tr>
					<td style="PADDING-RIGHT: 2px" align="right">
						<input class="ButtonStyle" id="ExportSearchResultsButton" style="WIDTH: 175px" onclick="f_exportSearchResults();" type="button" value="Export search results">&nbsp;
						<asp:Button ID="m_BatchUpdateButton" runat="server" OnClick=OnBatchUpdateClick class="ButtonStyle" style="DISPLAY: none" NoHighlight></asp:Button>
						<input class="ButtonStyle" id="BatchUserUpdateBtn" onfocus="this.blur();" onclick="f_update();" type="button" value="Update selected users" name="BatchUserUpdateBtn">
						&nbsp; <INPUT class="ButtonStyle" onclick="onClosePopup();" type="button" value="Close">
					</td>
				</tr>
				<TR>
					<td height="0%"></td>
				</TR>
			</TABLE>
			</asp:Panel>

			<div id="LastLoginDetails" style="display:none">
			<table width="100%"><tr><td>Last Login date tracking began on 09/16/2006<br>(and 02/23/2008 for users who log in through a webservice interface).<br><br>Users who have not logged in since 09/16/2006 (or 02/23/2008 for webservice users)<br>will appear in the search results as if they had never logged in.<br></td></tr>
			<tr><td align=center>[ <a href="#" onclick="f_closeDetails();">Close</a> ]</td></tr>
			</table>
			</div>
			<ML:CMODALDLG id="m_ModalDlg" runat="server"></ML:CMODALDLG></FORM>
	</BODY>
</HTML>
