using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ObjLib.ServiceCredential;
using LendersOffice.ObjLib.UI.Themes;
using LendersOffice.Rolodex;
using LendersOffice.Security;

namespace LendersOfficeApp.los.BrokerAdmin
{
    public partial class PmlCompanyListService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private const int PAGE_SIZE = ConstAppDavid.PmlCompanyListPagingSize;
        
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "GetPmlCompanyList":
                    GetPmlCompanyList();
                    break;
                case "GetPmlCompanyInfo":
                    GetPmlCompanyInfo();
                    break;
                case "SavePmlCompanyInfo":
                    SavePmlCompanyInfo();
                    break;
            }
        }

        private AbstractUserPrincipal CurrentUser
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal;
            }
        }

        private void GetPmlCompanyList()
        {
            List<PmlBroker> brokersPage = null;
            List<List<string>> sBrokerList = new List<List<string>>();

            //Sort brokers...
            string sortBy = GetString("SortBy", "Name");

            if (sortBy != "Name" && sortBy != "NameDesc" && 
                sortBy != "CompanyId" && sortBy != "CompanyIdDesc" && 
                sortBy != "LpePriceGroupName" && sortBy != "LpePriceGroupNameDesc" && 
                sortBy != "BranchNm" && sortBy != "BranchNmDesc")
            {
                sortBy = "Name";
            }

            bool bAsc = !sortBy.EndsWith("Desc", StringComparison.CurrentCultureIgnoreCase);
            
            Comparison<PmlBroker> pc = null;
            switch (sortBy)
            {
                case "Name":
                    pc = PmlBroker.NameComparison;
                    break;
                case "NameDesc":
                    pc = PmlBroker.NameComparisonDesc;
                    break;
                case "CompanyId":
                    pc = PmlBroker.CompanyIdComparison;
                    break;
                case "CompanyIdDesc":
                    pc = PmlBroker.CompanyIdComparisonDesc;
                    break;
                case "LpePriceGroupName":
                    pc = PmlBroker.LpePriceGroupNameComparison;
                    break;
                case "LpePriceGroupNameDesc":
                    pc = PmlBroker.LpePriceGroupNameComparisonDesc;
                    break;
                case "BranchNm":
                    pc = PmlBroker.BranchNmComparison;
                    break;
                case "BranchNmDesc":
                    pc = PmlBroker.BranchNmComparisonDesc;
                    break;
            }

            var companyNameFilter = GetString("CompanyNameFilter", string.Empty);
            var companyIDFilter = GetString("CompanyIDFilter", string.Empty);
            var companyRoleFilter = GetString("CompanyRoleFilter", string.Empty);
            var companyIndexFilterStr = GetString("CompanyIndexFilter");
            var brokerStatusFilter = GetInt("BrokerStatusFilter", -1);
            var miniCorrStatusFilter = GetInt("MiniCorrStatusFilter", -1);
            var corrStatusFilter = GetInt("CorrStatusFilter", -1);

            int companyIndexFilter = -1;

            if (!string.IsNullOrWhiteSpace(companyIndexFilterStr))
            {
                if (!int.TryParse(companyIndexFilterStr, out companyIndexFilter))
                {
                    throw new CBaseException($"Company Index Number \"{companyIndexFilterStr}\" is not a valid search query. Please enter a number.", $"Company Index Number {companyIndexFilterStr} is not a valid search query.");
                }
            }

            int? underwritingAuthorityFilter = null;

            // When searching for an OC with a correspondent role, we need also need 
            // to search for either prior approved or delegated. The value of the option
            // will be 2X where X is a value for E_UnderwritingAuthority.
            if (companyRoleFilter.StartsWith(E_OCRoles.Correspondent.ToString("D")))
            {
                underwritingAuthorityFilter = int.Parse(companyRoleFilter.Substring(1, 1));

                companyRoleFilter = E_OCRoles.Correspondent.ToString("D");
            }

            int nPmlBrokers = PmlBroker.CountPmlBrokerByBrokerId(
                CurrentUser.BrokerId,
                companyNameFilter,
                companyIDFilter,
                companyRoleFilter,
                underwritingAuthorityFilter,
                companyIndexFilter,
                brokerStatusFilter,
                miniCorrStatusFilter,
                corrStatusFilter
                );

            int startIndex = 1;
            int endIndex = nPmlBrokers;
            int pageNumber = 1;

            if (nPmlBrokers > PAGE_SIZE)
            {
                //Gather Page Number
                pageNumber = this.GetInt("page", 1);
                int maxPages = nPmlBrokers / PAGE_SIZE;
                if (nPmlBrokers % PAGE_SIZE != 0)
                    maxPages++;

                if (pageNumber < 1 || pageNumber > maxPages)
                    pageNumber = 1;

                //GUI has numbers starting from 1 but our array is 0-index based.
                startIndex = (pageNumber - 1) * PAGE_SIZE;
                if (startIndex < 0) startIndex = 0;
                if (startIndex >= nPmlBrokers)
                    startIndex = nPmlBrokers - 1;

                endIndex = startIndex + PAGE_SIZE;
                if (endIndex >= nPmlBrokers)
                    endIndex = nPmlBrokers - 1;

                int nItems = endIndex - startIndex + 1;
                
                //SQL list is 1-index based
                startIndex++;
                endIndex++;
            }
            else
            {
                startIndex = 1;
                endIndex = nPmlBrokers;
            }

            if (nPmlBrokers != 0)
            {
                brokersPage = PmlBroker.ListAllPmlBrokerPageByBrokerId(
                    CurrentUser.BrokerId, 
                    sortBy, 
                    bAsc, 
                    startIndex, 
                    endIndex,
                    companyNameFilter,
                    companyIDFilter,
                    companyRoleFilter,
                    underwritingAuthorityFilter,
                    companyIndexFilter,
                    brokerStatusFilter,
                    miniCorrStatusFilter,
                    corrStatusFilter);
            }
            else
            {
                brokersPage = new List<PmlBroker>();
            }

            var themeCollection = ThemeManager.GetThemeCollection(this.CurrentUser.BrokerId, ThemeCollectionType.TpoPortal);

            foreach (PmlBroker br in brokersPage)
            {
                List<string> ls = new List<string>();
                ls.Add(br.PmlBrokerId.ToString());
                ls.Add(br.Name);
                ls.Add(br.CompanyId);
                string roles = "";
                foreach (E_OCRoles role in br.Roles)
                {
                    if (!string.IsNullOrEmpty(roles))
                    {
                        roles += ",\r\n";
                    }
                    if(role.Equals(E_OCRoles.MiniCorrespondent))
                    {
                        roles += "Mini-Correspondent";
                    }
                    else
                    {
                        roles += role.ToString();
                    }
                }
                ls.Add(roles);
                ls.Add(br.Addr);
                ls.Add(br.City);
                ls.Add(br.State);
                ls.Add(br.Zip);
                ls.Add(br.Phone);
                ls.Add(br.Fax);
                ls.Add(br.GroupNameList);

                string priceGroupString = string.Empty;

                priceGroupString = ComposeRoleValuesForTable(priceGroupString, br.LpePriceGroupName, "Broker :");
                priceGroupString = ComposeRoleValuesForTable(priceGroupString, br.MiniCorrespondentLpePriceGroupName, "Mini-Corr: ");
                priceGroupString = ComposeRoleValuesForTable(priceGroupString, br.CorrespondentLpePriceGroupName, "Corr: ");
                ls.Add(priceGroupString);

                string branchesString = string.Empty;

                branchesString = ComposeRoleValuesForTable(branchesString, br.BranchNm, "Broker :");
                branchesString = ComposeRoleValuesForTable(branchesString, br.MiniCorrespondentBranchNm, "Mini-Corr: ");
                branchesString = ComposeRoleValuesForTable(branchesString, br.CorrespondentBranchNm, "Corr: ");
                ls.Add(branchesString);

                var statusString = string.Empty;

                statusString = this.ComposeRoleValuesForTable(statusString, br.BrokerRoleStatusNm, "Broker :");
                statusString = this.ComposeRoleValuesForTable(statusString, br.MiniCorrRoleStatusNm, "Mini-Corr :");
                statusString = this.ComposeRoleValuesForTable(statusString, br.CorrRoleStatusNm, "Corr :");
                ls.Add(statusString);

                var themeId = br.TpoColorThemeId ?? themeCollection.SelectedThemeId;
                ls.Add(themeCollection.GetTheme(themeId).Name);

                ls.Add(br.IndexNumber.ToString());

                sBrokerList.Add(ls);

                if (sBrokerList.Count >= PAGE_SIZE)
                {
                    break;
                }
            }

            string groupList = ObsoleteSerializationHelper.JsonSerialize(BuildGroupList(CurrentUser.BrokerId));
            SetResult("groupList", groupList);
            
            string stringList = ObsoleteSerializationHelper.JsonSerialize(sBrokerList);
            SetResult("companyList", stringList);
            SetResult("companyListSize", nPmlBrokers);
            SetResult("companyListPageNumber", pageNumber);
            SetResult("companyListSortBy", sortBy);
        }

        private string ComposeRoleValuesForTable(string currentString, string toBeAdded, string modePrefix)
        {
            if (!string.IsNullOrEmpty(toBeAdded))
            {
                if (!string.IsNullOrEmpty(currentString))
                {
                    currentString += ",\r\n";
                }
                currentString += modePrefix + toBeAdded;
            }

            return currentString;
        }

        private List<List<string>> BuildGroupList(Guid brokerId)
        {
            List<List<string>> groupList = new List<List<string>>();
            foreach (var group in GroupDB.GetAllGroups(brokerId, GroupType.PmlBroker))
            {
                groupList.Add(new List<string>(new List<string> { group.GroupId.ToString(), group.GroupName, group.Description }));
            }

            // Sort by group name.
            groupList.Sort(new Comparison<List<string>>(delegate(List<string> a, List<string> b) { return a[1].CompareTo(b[1]); }));
            return groupList;
        }


        private void GetPmlCompanyInfo()
        {
            List<string> sBrokerInfo = new List<string>();
            string companyLicenses = "";
            PmlBroker pb = null;

            Guid pmlBrokerId = GetGuid("id", Guid.Empty);
            
            if (pmlBrokerId != Guid.Empty)
                pb = PmlBroker.RetrievePmlBrokerById(pmlBrokerId, CurrentUser.BrokerId);

            if (pb == null)
            {
                string sId = GetString("id", "");
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Unable to retrieve PmlBroker with id {0}.{1}", sId, System.Environment.NewLine);
                sb.AppendFormat("Broker: {0}, EmployeeId: {1}.", CurrentUser.BrokerId.ToString(), CurrentUser.EmployeeId.ToString());
                Tools.LogError(sb.ToString());
                throw new CBaseException(ErrorMessages.PmlBrokerNotFound, "Unable to retrieve information for the selected company.");
            }
            
            sBrokerInfo.Add(pb.PmlBrokerId.ToString());
            sBrokerInfo.Add(pb.Name);               // 1
            sBrokerInfo.Add(pb.CompanyId);          // 2
            sBrokerInfo.Add(pb.Addr);               // 3
            sBrokerInfo.Add(pb.City);               // 4
            sBrokerInfo.Add(pb.State);              // 5
            sBrokerInfo.Add(pb.Zip);                // 6
            sBrokerInfo.Add(pb.Phone);              // 7
            sBrokerInfo.Add(pb.Fax);                // 8
            sBrokerInfo.Add(pb.TaxId);              // 9
            sBrokerInfo.Add(pb.PrincipalName1);     // 10
            sBrokerInfo.Add(pb.PrincipalName2);     // 11
            sBrokerInfo.Add(pb.NmLsIdentifier);     // 12
            sBrokerInfo.Add(pb.GroupIdList);        // 13

            sBrokerInfo.Add(m_losConvert.ToRateString(pb.OriginatorCompensationPercent));                               // 14
            sBrokerInfo.Add(((int)pb.OriginatorCompensationBaseT).ToString());                                          // 15
            sBrokerInfo.Add(m_losConvert.ToMoneyString(pb.OriginatorCompensationMinAmount, FormatDirection.ToRep));     // 16
            sBrokerInfo.Add(m_losConvert.ToMoneyString(pb.OriginatorCompensationMaxAmount, FormatDirection.ToRep));     // 17
            sBrokerInfo.Add(m_losConvert.ToMoneyString(pb.OriginatorCompensationFixedAmount, FormatDirection.ToRep));   // 18
            sBrokerInfo.Add(pb.OriginatorCompensationNotes.ToString());                                                 // 19
            sBrokerInfo.Add(m_losConvert.ToRateString(pb.OriginatorCompensationPercent));                               // 20
            sBrokerInfo.Add(pb.BrokerId.ToString());                                                                    // 21
            sBrokerInfo.Add(pb.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo ? "1" : "0");                        // 22

            companyLicenses = LicenseInfoList.JsonSerialize(pb.LicenseInfoList);
            this.SetResult("GroupAssociationList", pb.GroupIdList);

            this.SetResult("OriginatorCompensationAuditData", pb.OriginatorCompensationAuditData);
            
            string stringList = ObsoleteSerializationHelper.JsonSerialize(sBrokerInfo);
            this.SetResult("companyInfo", stringList);
            this.SetResult("pmlCoLendingLicenseList", companyLicenses);

            this.SetResult("CustomPricingPolicyField1", pb.CustomPricingPolicyField1);
            this.SetResult("CustomPricingPolicyField2", pb.CustomPricingPolicyField2);
            this.SetResult("CustomPricingPolicyField3", pb.CustomPricingPolicyField3);
            this.SetResult("CustomPricingPolicyField4", pb.CustomPricingPolicyField4);
            this.SetResult("CustomPricingPolicyField5", pb.CustomPricingPolicyField5);
            this.SetResult("NmlsName", pb.NmlsName);
            this.SetResult("NmlsNameLckd", pb.NmlsNameLckd);

            this.SetResult("BrokerRole", pb.Roles.Contains(E_OCRoles.Broker));
            this.SetResult("MiniCorrespondentRole", pb.Roles.Contains(E_OCRoles.MiniCorrespondent));
            this.SetResult("CorrespondentRole", pb.Roles.Contains(E_OCRoles.Correspondent));
            if (pb.Roles.Contains(E_OCRoles.Correspondent))
                this.SetResult("UnderwritingAuthority", pb.UnderwritingAuthority);
            else
                this.SetResult("UnderwritingAuthority", E_UnderwritingAuthority.PriorApproved);

            this.SetResult("TPOLandingPage", pb.TPOPortalConfigID.HasValue ? pb.TPOPortalConfigID.ToString() : "none");
            this.SetResult("MiniCorrespondentTPOPortalConfigID", pb.MiniCorrespondentTPOPortalConfigID.HasValue ? pb.MiniCorrespondentTPOPortalConfigID.ToString() : "none");
            this.SetResult("CorrespondentTPOPortalConfigID", pb.CorrespondentTPOPortalConfigID.HasValue ? pb.CorrespondentTPOPortalConfigID.ToString() : "none");
            //this.SetResult("NmlsIdentifier", pb.NmLsIdentifier);

            this.SetResult("LpePriceGroupId", pb.UseDefaultWholesalePriceGroupId ? 
                ConstApp.UseLenderLevelDefaultValue : 
                pb.LpePriceGroupId.ToString());

            this.SetResult("BranchId", pb.UseDefaultWholesaleBranchId ?
                ConstApp.UseLenderLevelDefaultValue : 
                pb.BranchId.ToString());

            this.SetResult("MiniCorrespondentLpePriceGroupId", pb.UseDefaultMiniCorrPriceGroupId ?
                ConstApp.UseLenderLevelDefaultValue : 
                pb.MiniCorrespondentLpePriceGroupId.ToString());

            this.SetResult("MiniCorrespondentBranchId", pb.UseDefaultMiniCorrBranchId ?
                ConstApp.UseLenderLevelDefaultValue : 
                pb.MiniCorrespondentBranchId.ToString());

            this.SetResult("CorrespondentLpePriceGroupId", pb.UseDefaultCorrPriceGroupId ?
                ConstApp.UseLenderLevelDefaultValue : 
                pb.CorrespondentLpePriceGroupId.ToString());

            this.SetResult("CorrespondentBranchId", pb.UseDefaultCorrBranchId ?
                ConstApp.UseLenderLevelDefaultValue : 
                pb.CorrespondentBranchId.ToString());

            this.SetResult("PmlCompanyTierId", pb.PmlCompanyTierId.ToString());
            this.SetResult("TpoColorThemeId", pb.TpoColorThemeId ?? Guid.Empty);

            this.SetResult("TpoNavigationPermissions", pb.PMLNavigationPermissions);

            List<Affiliate> affList = new List<Affiliate>();
            foreach (RolodexDB rdb in pb.GetAffiliates())
            {
                affList.Add(new Affiliate
                {
                    id = rdb.ID.ToString(),
                    status = rdb.AgentIsApproved ? "Approved" : "Disabled",
                    type = rdb.TypeDescription,
                    name = rdb.Name,
                    compName = rdb.CompanyName
                });
            }

            this.SetResult("AffiliateList", ObsoleteSerializationHelper.JsonSerialize(affList));

            this.SetResult("BrokerRoleStatusT", pb.BrokerRoleStatusT);
            this.SetResult("MiniCorrRoleStatusT", pb.MiniCorrRoleStatusT);
            this.SetResult("CorrRoleStatusT", pb.CorrRoleStatusT);

            this.SetRelationshipData(pb);

            this.SetResult("BrokerPermissions", pb.Permissions.ToString());

            this.SetResult("TaxIdValid", pb.TaxIdValid);
            this.SetResult("IndexNumber", pb.IndexNumber);

            this.SetServiceCredentialData(pb);
        }

        private void SetServiceCredentialData(PmlBroker pb)
        {
            var serviceCredentials = ServiceCredential.GetAllOriginatingCompanyServiceCredentials(pb.BrokerId, pb.PmlBrokerId, ServiceCredentialService.All);
            var serviceCredentialsJson = SerializationHelper.JsonNetAnonymousSerialize(serviceCredentials);
            this.SetResult("ServiceCredentialsJson", serviceCredentialsJson);
        }

        // OPM 181101, 2/22/2016, ML
        // The order for the serialized lists matters, as the OC editor
        // will use the order of the lists to populate the relationship
        // controls.
        private void SetRelationshipData(PmlBroker pb)
        {
            var brokerRelationships = new List<PmlBrokerRelationshipEmployeeInfo>()
            {
                pb.BrokerRoleRelationships[E_RoleT.Manager],
                pb.BrokerRoleRelationships[E_RoleT.Processor],
                pb.BrokerRoleRelationships[E_RoleT.JuniorProcessor],
                pb.BrokerRoleRelationships[E_RoleT.LenderAccountExecutive],
                pb.BrokerRoleRelationships[E_RoleT.Underwriter],
                pb.BrokerRoleRelationships[E_RoleT.JuniorUnderwriter],
                pb.BrokerRoleRelationships[E_RoleT.LockDesk]
            };

            var miniCorrRelationships = new List<PmlBrokerRelationshipEmployeeInfo>()
            {
                pb.MiniCorrRoleRelationships[E_RoleT.Manager],
                pb.MiniCorrRoleRelationships[E_RoleT.Processor],
                pb.MiniCorrRoleRelationships[E_RoleT.JuniorProcessor],
                pb.MiniCorrRoleRelationships[E_RoleT.LenderAccountExecutive],
                pb.MiniCorrRoleRelationships[E_RoleT.Underwriter],
                pb.MiniCorrRoleRelationships[E_RoleT.JuniorUnderwriter],
                pb.MiniCorrRoleRelationships[E_RoleT.CreditAuditor],
                pb.MiniCorrRoleRelationships[E_RoleT.LegalAuditor],
                pb.MiniCorrRoleRelationships[E_RoleT.LockDesk],
                pb.MiniCorrRoleRelationships[E_RoleT.Purchaser],
                pb.MiniCorrRoleRelationships[E_RoleT.Secondary]
            };

            var corrRelationships = new List<PmlBrokerRelationshipEmployeeInfo>()
            {
                pb.CorrRoleRelationships[E_RoleT.Manager],
                pb.CorrRoleRelationships[E_RoleT.Processor],
                pb.CorrRoleRelationships[E_RoleT.JuniorProcessor],
                pb.CorrRoleRelationships[E_RoleT.LenderAccountExecutive],
                pb.CorrRoleRelationships[E_RoleT.Underwriter],
                pb.CorrRoleRelationships[E_RoleT.JuniorUnderwriter],
                pb.CorrRoleRelationships[E_RoleT.CreditAuditor],
                pb.CorrRoleRelationships[E_RoleT.LegalAuditor],
                pb.CorrRoleRelationships[E_RoleT.LockDesk],
                pb.CorrRoleRelationships[E_RoleT.Purchaser],
                pb.CorrRoleRelationships[E_RoleT.Secondary]
            };

            this.SetResult("OcBrokerRelationships", ObsoleteSerializationHelper.JsonSerialize(brokerRelationships));
            this.SetResult("OcMiniCorrRelationships", ObsoleteSerializationHelper.JsonSerialize(miniCorrRelationships));
            this.SetResult("OcCorrRelationships", ObsoleteSerializationHelper.JsonSerialize(corrRelationships));
        }

        public class Affiliate
        {
            public string id;
            public string status;
            public string type;
            public string name;
            public string compName;
        }

        private void SavePmlCompanyInfo()
        {
            PmlBroker pb = null;
            Guid pmlBrokerId = Guid.Empty;

            //Required non-empty fields
            string sId   = GetString("id", "").TrimWhitespaceAndBOM();
            string Name  = GetString("name", "").TrimWhitespaceAndBOM();
            string NmlsName = GetString("NmlsName", "").TrimWhitespaceAndBOM();
            bool NmlsNameLckd = GetBool("NmlsNameLckd", false);  
            string Addr  = GetString("street", "").TrimWhitespaceAndBOM(); 
            string City  = GetString("city", "").TrimWhitespaceAndBOM(); 
            string State = GetString("state", "").TrimWhitespaceAndBOM();
            string Zip   = GetString("zip", "").TrimWhitespaceAndBOM();  
            string Phone = GetString("phone", "").TrimWhitespaceAndBOM();
            string GroupIdList = GetString("GroupAssociationList").TrimWhitespaceAndBOM();

            //Non-Required fields
            string CompanyId                = GetString("companyId", "").TrimWhitespaceAndBOM();
            string Fax                      = GetString("fax", "").TrimWhitespaceAndBOM();
            string TaxId                    = GetString("taxId", "").TrimWhitespaceAndBOM();
            string PrincipalName1           = GetString("principal", "").TrimWhitespaceAndBOM();
            string PrincipalName2           = GetString("principal2", "").TrimWhitespaceAndBOM();
            string sLicenseList             = GetString("pmlCoLendingLicenseList", "").TrimWhitespaceAndBOM();
            string sNmLsIdentifier          = GetString("NmlsIdentifier", "").TrimWhitespaceAndBOM();
            int PmlCompanyTierId            = GetInt("PmlCompanyTierId", 0);
            string TpoNavigationPermissions = GetString("TpoNavigationPermissions", "").TrimWhitespaceAndBOM();

            if (sId.Length == 0 || Name.Length == 0 || NmlsName.Length == 0 || Addr.Length == 0 || City.Length == 0 || State.Length == 0 || Zip.Length == 0 || Phone.Length == 0)
                throw new CBaseException(ErrorMessages.PmlCompanyMissingParameters, "PmlCompany does not contain all the required parameters.");

            try {   pmlBrokerId = new Guid(sId);    }catch{}

            if (pmlBrokerId == Guid.Empty && sId != "new")
                throw new CBaseException(ErrorMessages.PmlCompanyMissingParameters, "PmlCompany does not contain all the required parameters.");

            
            if (sId == "new")
                pb = PmlBroker.CreatePmlBroker(CurrentUser.BrokerId);
            else
                pb = PmlBroker.RetrievePmlBrokerById(pmlBrokerId, CurrentUser.BrokerId);

            Debug.Assert(pb != null);

            try
            {
                pb.Name             = Name;
                pb.NmlsName         = NmlsName;
                pb.NmlsNameLckd     = NmlsNameLckd;
                pb.CompanyId        = CompanyId;
                pb.PmlCompanyTierId = PmlCompanyTierId;
                pb.Addr             = Addr;
                pb.City             = City;
                pb.State            = State;
                pb.Zip              = Zip;
                pb.Phone            = Phone;
                pb.Fax              = Fax;
                pb.TaxId            = TaxId;
                pb.PrincipalName1   = PrincipalName1;
                pb.PrincipalName2   = PrincipalName2;
                pb.NmLsIdentifier   = sNmLsIdentifier;
                pb.TpoColorThemeId  = this.GetGuid("TpoColorThemeId");

                Boolean originatorCompensationIsOnlyPaidForFirstLienOfCombo = GetBool("OriginatorCompensationIsOnlyPaidForFirstLienOfCombo");
                Decimal originatorCompensationPercent = m_losConvert.ToRate(GetString("OriginatorCompensationPercent"));
                E_PercentBaseT originatorCompensationBaseT = (E_PercentBaseT)GetInt("OriginatorCompensationBaseT", (int)E_PercentBaseT.TotalLoanAmount);
                Decimal originatorCompensationMinAmount = m_losConvert.ToMoney(GetString("OriginatorCompensationMinAmount"));
                Decimal originatorCompensationMaxAmount = m_losConvert.ToMoney(GetString("OriginatorCompensationMaxAmount"));
                Decimal originatorCompensationFixedAmount = m_losConvert.ToMoney(GetString("OriginatorCompensationFixedAmount"));
                string originatorCompensationNotes = GetString("OriginatorCompensationNotes");

                pb.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = originatorCompensationIsOnlyPaidForFirstLienOfCombo;
                pb.OriginatorCompensationPercent = originatorCompensationPercent;
                pb.OriginatorCompensationBaseT = originatorCompensationBaseT;                
                pb.OriginatorCompensationMinAmount = originatorCompensationMinAmount;                
                pb.OriginatorCompensationMaxAmount = originatorCompensationMaxAmount;
                pb.OriginatorCompensationFixedAmount = originatorCompensationFixedAmount;
                pb.OriginatorCompensationNotes = originatorCompensationNotes;


                var customPricingPolicyFieldList = BrokerDB.RetrieveById(pb.BrokerId).CustomPricingPolicyFieldList;
                Func<int, bool> isFieldDefined = (keywordNum) => customPricingPolicyFieldList.IsFieldDefined(keywordNum);
                pb.CustomPricingPolicyField1 = isFieldDefined(1) ? GetString("CustomPricingPolicyField1") : "";
                pb.CustomPricingPolicyField2 = isFieldDefined(2) ? GetString("CustomPricingPolicyField2") : "";
                pb.CustomPricingPolicyField3 = isFieldDefined(3) ? GetString("CustomPricingPolicyField3") : "";
                pb.CustomPricingPolicyField4 = isFieldDefined(4) ? GetString("CustomPricingPolicyField4") : "";
                pb.CustomPricingPolicyField5 = isFieldDefined(5) ? GetString("CustomPricingPolicyField5") : "";

                var tpoLandingPageIdStr = GetString("TPOLandingPage");
                if (!string.IsNullOrEmpty(tpoLandingPageIdStr) &&
                    tpoLandingPageIdStr != "none")
                {
                    pb.TPOPortalConfigID = GetGuid("TPOLandingPage");
                }
                else
                {
                    pb.TPOPortalConfigID = null;
                }

                //Licenses
                pb.LicenseInfoList = LicenseInfoList.JsonDeserialize(sLicenseList);

                var receivedLpePriceGroupId = GetString("LpePriceGroupId");
                if (receivedLpePriceGroupId.Equals(ConstApp.UseLenderLevelDefaultValue))
	            {
                    pb.UseDefaultWholesalePriceGroupId = true;
	            }
                else if (!string.IsNullOrEmpty(receivedLpePriceGroupId))
	            {
                    pb.LpePriceGroupId = GetGuid("LpePriceGroupId");
                }

                var receivedBranchId = GetString("BranchId");
                if (receivedBranchId.Equals(ConstApp.UseLenderLevelDefaultValue))
	            {
		            pb.UseDefaultWholesaleBranchId = true;
	            }
                else if (!string.IsNullOrEmpty(receivedBranchId))
                {
                    pb.BranchId = GetGuid("BranchId");
                }

                var receivedMiniCorrespondentBranchId = GetString("MiniCorrespondentBranchId");
                if (receivedMiniCorrespondentBranchId.Equals(ConstApp.UseLenderLevelDefaultValue))
                {
                    pb.UseDefaultMiniCorrBranchId = true;
                }
                else if (!string.IsNullOrEmpty(receivedMiniCorrespondentBranchId))
                {
                    pb.MiniCorrespondentBranchId = GetGuid("MiniCorrespondentBranchId");
                }

                var receivedMiniCorrespondentLpePriceGroupId = GetString("MiniCorrespondentLpePriceGroupId");
                if (receivedMiniCorrespondentLpePriceGroupId.Equals(ConstApp.UseLenderLevelDefaultValue))
                {
                    pb.UseDefaultMiniCorrPriceGroupId = true;
                }
                else if (!string.IsNullOrEmpty(receivedMiniCorrespondentLpePriceGroupId))
                {
                    pb.MiniCorrespondentLpePriceGroupId = GetGuid("MiniCorrespondentLpePriceGroupId");
                }

                var miniCorrTpoLandingPageIdStr = GetString("MiniCorrespondentTPOPortalConfigID");
                if (!string.IsNullOrEmpty(miniCorrTpoLandingPageIdStr) &&
                    !miniCorrTpoLandingPageIdStr.Equals("none"))
                {
                    pb.MiniCorrespondentTPOPortalConfigID = GetGuid("MiniCorrespondentTPOPortalConfigID");
                }
                else
                {
                    pb.MiniCorrespondentTPOPortalConfigID = null;
                }

                var receivedCorrespondentBranchId = GetString("CorrespondentBranchId");
                if (receivedCorrespondentBranchId.Equals(ConstApp.UseLenderLevelDefaultValue))
                {
                    pb.UseDefaultCorrBranchId = true;
                }
                else if (!string.IsNullOrEmpty(receivedCorrespondentBranchId))
                {
                    pb.CorrespondentBranchId = GetGuid("CorrespondentBranchId");
                }

                var receivedCorrespondentLpePriceGroupId = GetString("CorrespondentLpePriceGroupId");
                if (receivedCorrespondentLpePriceGroupId.Equals(ConstApp.UseLenderLevelDefaultValue))
                {
                    pb.UseDefaultCorrPriceGroupId = true;
                }
                else if (!string.IsNullOrEmpty(receivedCorrespondentLpePriceGroupId))
                {
                    pb.CorrespondentLpePriceGroupId = GetGuid("CorrespondentLpePriceGroupId");
                }

                var corrTpoLandingPageIdStr = GetString("CorrespondentTPOPortalConfigID");
                if (!string.IsNullOrEmpty(corrTpoLandingPageIdStr) &&
                    !corrTpoLandingPageIdStr.Equals("none"))
                {
                    pb.CorrespondentTPOPortalConfigID = GetGuid("CorrespondentTPOPortalConfigID");
                }
                else
                {
                    pb.CorrespondentTPOPortalConfigID = null;
                }

                HashSet<E_OCRoles> rolesSet = new HashSet<E_OCRoles>();
                if (GetBool("BrokerRole"))
                {
                    rolesSet.Add(E_OCRoles.Broker);
                }
                if (GetBool("MiniCorrespondentRole"))
                {
                    rolesSet.Add(E_OCRoles.MiniCorrespondent);
                }
                if (GetBool("CorrespondentRole"))
                {
                    rolesSet.Add(E_OCRoles.Correspondent);
                }

                pb.Roles = rolesSet;

                pb.UnderwritingAuthority = (E_UnderwritingAuthority)GetInt("UnderwritingAuthority");
                
                pb.PMLNavigationPermissions = TpoNavigationPermissions;

                pb.BrokerRoleStatusT = (OCStatusType)GetInt("BrokerRoleStatusT");
                pb.MiniCorrRoleStatusT = (OCStatusType)GetInt("MiniCorrRoleStatusT");
                pb.CorrRoleStatusT = (OCStatusType)GetInt("CorrRoleStatusT");

                this.ParseRelationshipData(pb);

                this.ParsePermissionData(pb);

                pb.Save();

                GroupDB.UpdatePmlBrokerGroups(pb.BrokerId, pb.PmlBrokerId, GroupIdList);

                pb.GroupNameList = string.Empty;
                foreach (var group in GroupDB.GetGroupMembershipsForPmlBroker(pb.BrokerId, pb.PmlBrokerId))
                {
                    pb.GroupNameList += (pb.GroupNameList == string.Empty ? "" : ",") + group.GroupName;
                }

                //OPM 149774 - Save/Delete Affiliates
                HashSet<Guid> affIds = new HashSet<Guid>();  // HashSet to prevent duplicates

                string sAffIds = GetString("AffIds");
                if(!string.IsNullOrEmpty(sAffIds))
                {
                    foreach (string id in sAffIds.Split(','))
                    {
                        affIds.Add(new Guid(id));
                    }
                }

                HashSet<Guid> oldAffIds = new HashSet<Guid>();
                foreach (RolodexDB rdb in pb.GetAffiliates())
                {
                    oldAffIds.Add(rdb.ID);
                }

                // Delete affiliates on the old list that aren't in the new list
                foreach (Guid id in oldAffIds.Except(affIds))
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerId", pb.BrokerId),
                                                    new SqlParameter("@PmlBrokerId", pb.PmlBrokerId),
                                                    new SqlParameter("@AgentId", id)
                                                };

                    StoredProcedureHelper.ExecuteNonQuery(pb.BrokerId, "PML_BROKER_x_AGENT_Delete", 3, parameters);
                }

                // Save affiliates that aren't in the old list (these are the new ones)
                foreach (Guid id in affIds.Except(oldAffIds))
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerId", pb.BrokerId),
                                                    new SqlParameter("@PmlBrokerId", pb.PmlBrokerId),
                                                    new SqlParameter("@AgentId", id)
                                                };
                    StoredProcedureHelper.ExecuteNonQuery(pb.BrokerId, "PML_BROKER_x_AGENT_Create", 3, parameters);
                }

                //Send the saved version back to the client
                List<string> ls = new List<string>();
                ls.Add(pb.PmlBrokerId.ToString());
                ls.Add(pb.Name);
                ls.Add(pb.CompanyId);
                string roles = "";
                foreach (E_OCRoles role in pb.Roles)
                {
                    if (!string.IsNullOrEmpty(roles))
                    {
                        roles += ",\r\n";
                    }
                    if (role.Equals(E_OCRoles.MiniCorrespondent))
                    {
                        roles += "Mini-Correspondent";
                    }
                    else
                    {
                        roles += role.ToString();
                    }
                }
                ls.Add(roles);
                ls.Add(pb.Addr);
                ls.Add(pb.City);
                ls.Add(pb.State);
                ls.Add(pb.Zip);
                ls.Add(pb.Phone);
                ls.Add(pb.Fax);
                ls.Add(pb.GroupNameList);

                string priceGroupString = string.Empty;

                priceGroupString = ComposeRoleValuesForTable(priceGroupString, pb.LpePriceGroupName, "Broker :");
                priceGroupString = ComposeRoleValuesForTable(priceGroupString, pb.MiniCorrespondentLpePriceGroupName, "Mini-Corr: ");
                priceGroupString = ComposeRoleValuesForTable(priceGroupString, pb.CorrespondentLpePriceGroupName, "Corr: ");
                ls.Add(priceGroupString);

                string branchesString = string.Empty;

                branchesString = ComposeRoleValuesForTable(branchesString, pb.BranchNm, "Broker :");
                branchesString = ComposeRoleValuesForTable(branchesString, pb.MiniCorrespondentBranchNm, "Mini-Corr: ");
                branchesString = ComposeRoleValuesForTable(branchesString, pb.CorrespondentBranchNm, "Corr: ");
                ls.Add(branchesString);

                var statusString = string.Empty;

                statusString = this.ComposeRoleValuesForTable(statusString, pb.BrokerRoleStatusNm, "Broker :");
                statusString = this.ComposeRoleValuesForTable(statusString, pb.MiniCorrRoleStatusNm, "Mini-Corr :");
                statusString = this.ComposeRoleValuesForTable(statusString, pb.CorrRoleStatusNm, "Corr :");
                ls.Add(statusString);

                var themeCollection = ThemeManager.GetThemeCollection(pb.BrokerId, ThemeCollectionType.TpoPortal);
                var themeId = pb.TpoColorThemeId ?? themeCollection.SelectedThemeId;
                ls.Add(themeCollection.GetTheme(themeId).Name);

                ls.Add(pb.IndexNumber.ToString());

                string stringList = ObsoleteSerializationHelper.JsonSerialize(ls);
                this.SetResult("companySaved", stringList);
                this.SetServiceCredentialData(pb);
            }
            catch (Exception e)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Unable to save PmlBrokerId ({0}).", pmlBrokerId.ToString());
                sb.AppendLine();
                sb.AppendFormat("Broker: {0}, ", CurrentUser.BrokerId.ToString());
                sb.AppendFormat("EmployeeId: {0}.", CurrentUser.EmployeeId.ToString());
                sb.AppendLine();
                sb.Append("Data:");
                sb.AppendLine();
                
                sb.AppendFormat("sId '{0}', ", sId);
                sb.AppendFormat("Name '{0}', ", Name);
                sb.AppendFormat("Addr '{0}', ", Addr);
                sb.AppendFormat("City '{0}', ", City);
                sb.AppendFormat("State '{0}', ", State);

                sb.AppendFormat("Zip '{0}', ", Zip);
                sb.AppendFormat("Phone '{0}', ", Phone);
                sb.AppendFormat("CompanyId '{0}', ", CompanyId);
                sb.AppendFormat("Fax '{0}', ", Fax);
                sb.AppendFormat("TaxId '{0}', ", TaxId);

                sb.AppendFormat("PrincipalName1 '{0}', ", PrincipalName1);
                sb.AppendFormat("PrincipalName2 '{0}', ", PrincipalName2);
                sb.AppendFormat("NmLsIdentifier'{0}', ", sNmLsIdentifier);                
                sb.AppendFormat("sLicenseList '{0}'.", sLicenseList);
                sb.AppendFormat("GroupIdList '{0}'.", GroupIdList);
                sb.AppendLine();

                Tools.LogError(sb.ToString(), e);
                throw ;
            }
        }

        /// <summary>
        /// Obtains the relationship data for the broker, mini-correspondent,
        /// and correspondent roles that was sent to the service page.
        /// </summary>
        /// <param name="pb">
        /// The <see cref="PmlBroker"/> to populate the received relationship
        /// data.
        /// </param>
        private void ParseRelationshipData(PmlBroker pb)
        {
            var brokerRelationships = ObsoleteSerializationHelper.JsonDeserialize<List<PmlBrokerRelationshipEmployeeInfo>>(
                    GetString("OcBrokerRelationships"));

            this.SetBrokerRelationships(pb.BrokerRoleRelationships, brokerRelationships);

            var miniCorrRelationships = ObsoleteSerializationHelper.JsonDeserialize<List<PmlBrokerRelationshipEmployeeInfo>>(
                GetString("OcMiniCorrRelationships"));

            this.SetCorrespondentRelationships(pb.MiniCorrRoleRelationships, miniCorrRelationships);

            var corrRelationships = ObsoleteSerializationHelper.JsonDeserialize<List<PmlBrokerRelationshipEmployeeInfo>>(
                GetString("OcCorrRelationships"));

            this.SetCorrespondentRelationships(pb.CorrRoleRelationships, corrRelationships);
        }

        /// <summary>
        /// Sets the broker role relationship data for the specified <paramref name="brokerRelationshipDefaults"/>.
        /// </summary>
        /// <param name="brokerRelationshipDefaults">
        /// The <see cref="PmlBrokerRelationship"/> to populate.
        /// </param>
        /// <param name="relationships">
        /// The list of received <see cref="PmlBrokerRelationshipEmployeeInfo"/> records.
        /// </param>
        private void SetBrokerRelationships(
            PmlBrokerRelationship brokerRelationshipDefaults, 
            List<PmlBrokerRelationshipEmployeeInfo> relationships)
        {
            brokerRelationshipDefaults.SetRelationship(E_RoleT.Manager, relationships[0].Id, relationships[0].Name);
            brokerRelationshipDefaults.SetRelationship(E_RoleT.Processor, relationships[1].Id, relationships[1].Name);
            brokerRelationshipDefaults.SetRelationship(E_RoleT.JuniorProcessor, relationships[2].Id, relationships[2].Name);
            brokerRelationshipDefaults.SetRelationship(E_RoleT.LenderAccountExecutive, relationships[3].Id, relationships[3].Name);
            brokerRelationshipDefaults.SetRelationship(E_RoleT.Underwriter, relationships[4].Id, relationships[4].Name);
            brokerRelationshipDefaults.SetRelationship(E_RoleT.JuniorUnderwriter, relationships[5].Id, relationships[5].Name);
            brokerRelationshipDefaults.SetRelationship(E_RoleT.LockDesk, relationships[6].Id, relationships[6].Name);
        }

        /// <summary>
        /// Sets the correspondent or mini-correspondent role relationship data for the specified 
        /// <paramref name="corrRelationshipDefaults"/>.
        /// </summary>
        /// <param name="corrRelationshipDefaults">
        /// The <see cref="PmlBrokerRelationship"/> to populate.
        /// </param>
        /// <param name="relationships">
        /// The list of received <see cref="PmlBrokerRelationshipEmployeeInfo"/> records.
        /// </param>
        private void SetCorrespondentRelationships(
            PmlBrokerRelationship corrRelationshipDefaults, 
            List<PmlBrokerRelationshipEmployeeInfo> relationships)
        {
            corrRelationshipDefaults.SetRelationship(E_RoleT.Manager, relationships[0].Id, relationships[0].Name);
            corrRelationshipDefaults.SetRelationship(E_RoleT.Processor, relationships[1].Id, relationships[1].Name);
            corrRelationshipDefaults.SetRelationship(E_RoleT.JuniorProcessor, relationships[2].Id, relationships[2].Name);
            corrRelationshipDefaults.SetRelationship(E_RoleT.LenderAccountExecutive, relationships[3].Id, relationships[3].Name);
            corrRelationshipDefaults.SetRelationship(E_RoleT.Underwriter, relationships[4].Id, relationships[4].Name);
            corrRelationshipDefaults.SetRelationship(E_RoleT.JuniorUnderwriter, relationships[5].Id, relationships[5].Name);
            corrRelationshipDefaults.SetRelationship(E_RoleT.CreditAuditor, relationships[6].Id, relationships[6].Name);
            corrRelationshipDefaults.SetRelationship(E_RoleT.LegalAuditor, relationships[7].Id, relationships[7].Name);
            corrRelationshipDefaults.SetRelationship(E_RoleT.LockDesk, relationships[8].Id, relationships[8].Name);
            corrRelationshipDefaults.SetRelationship(E_RoleT.Purchaser, relationships[9].Id, relationships[9].Name);
            corrRelationshipDefaults.SetRelationship(E_RoleT.Secondary, relationships[10].Id, relationships[10].Name);
        }

        /// <summary>
        /// Obtains the permission data that was sent to the service page.
        /// </summary>
        /// <param name="pb">
        /// The <see cref="PmlBroker"/> to populate the received permission
        /// data.
        /// </param>
        private void ParsePermissionData(PmlBroker pb)
        {
            var receivedPermissions = ObsoleteSerializationHelper.JsonDeserialize<List<KeyValuePair<string, bool>>>(this.GetString("Permissions"));

            foreach (var permissionValuePair in receivedPermissions)
            {
                Permission permission;

                if (!Enum.TryParse(permissionValuePair.Key, ignoreCase: true, result: out permission))
                {
                    throw new GenericUserErrorMessageException(string.Format(
                        "An unknown permission was encountered while saving PML broker {0}: '{1}'",
                        pb.BrokerId,
                        permissionValuePair.Key));
                }

                pb.Permissions.SetPermission(permission, permissionValuePair.Value);
            }

            pb.Permissions.TaskEmailOption = (E_TaskRelatedEmailOptionT)this.GetInt("TaskEmail");
        }
    }
}
