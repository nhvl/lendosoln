﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;

namespace LendersOfficeApp.los.BrokerAdmin
{
    public partial class EditPmlUserService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private BrokerUserPrincipal CurrentUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected override void Process(string methodName)
        {
            switch (methodName) {
                case "RetrievePmlBroker":
                    RetrievePmlBroker();
                    break;
                case "VerifySupervisorChange":
                    VerifySupervisorChange();
                    break;
            }
        }

        private void RetrievePmlBroker()
        {
            Guid pmlBrokerId = GetGuid("PmlBrokerId");

            PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(pmlBrokerId, CurrentUser.BrokerId);
            SetResult("Addr", pmlBroker.Addr);
            SetResult("City", pmlBroker.City);
            SetResult("State", pmlBroker.State);
            SetResult("Zip", pmlBroker.Zip);
        }

        private void VerifySupervisorChange()
        {
            this.SetResult("NeedsWarning", EmployeeDB.IsSupervisorAssignedToActiveUsers(this.CurrentUser.BrokerId, this.GetGuid("supervisorId")));
        }

        [WebMethod]
        public static object GetPMLCompanyOriginatorCompensationData(Guid pmlBrokerID)
        {
            PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(pmlBrokerID, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            return string.Format("{0},{1},{2},{3},{4},{5}", pmlBroker.OriginatorCompensationPercent, (int)pmlBroker.OriginatorCompensationBaseT, pmlBroker.OriginatorCompensationMinAmount, pmlBroker.OriginatorCompensationMaxAmount, pmlBroker.OriginatorCompensationFixedAmount, pmlBroker.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo?"1":"0");
        }

        [WebMethod]
        public static object GetPMLCompanyCustomPricingPolicyData(Guid pmlBrokerID)
        {
            PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(pmlBrokerID, BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            return new
            {
                CustomPricingPolicyField1 = pmlBroker.CustomPricingPolicyField1,
                CustomPricingPolicyField2 = pmlBroker.CustomPricingPolicyField2,
                CustomPricingPolicyField3 = pmlBroker.CustomPricingPolicyField3,
                CustomPricingPolicyField4 = pmlBroker.CustomPricingPolicyField4,
                CustomPricingPolicyField5 = pmlBroker.CustomPricingPolicyField5
            };

        }

        [WebMethod]
        public static object ValidateOriginatingCompanySettings(
            Guid pmlBrokerID,
            string lpePriceGroupId,
            string branchId,
            string miniCorrespondentPriceGroupId,
            string miniCorrespondentBranchId,
            string correspondentPriceGroupId,
            string correspondentBranchId)
        {
            PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(pmlBrokerID, BrokerUserPrincipal.CurrentPrincipal.BrokerId);

            bool needSetOcPriceGroup = lpePriceGroupId.ToUpper() == "OC" && !pmlBroker.LpePriceGroupId.HasValue;
            bool needSetOcMiniCorrPriceGroup = miniCorrespondentPriceGroupId.ToUpper() == "OC" && !pmlBroker.MiniCorrespondentLpePriceGroupId.HasValue;
            bool needSetOcCorrPriceGroup =  correspondentPriceGroupId.ToUpper() == "OC" && !pmlBroker.CorrespondentLpePriceGroupId.HasValue;

            bool needSetOcBranch = branchId.ToUpper() == "OC" && !pmlBroker.BranchId.HasValue;
            bool needSetOcMiniCorrBranch = miniCorrespondentBranchId.ToUpper() == "OC" && !pmlBroker.MiniCorrespondentBranchId.HasValue;
            bool needSetOcCorrBranch =  correspondentBranchId.ToUpper() == "OC" && !pmlBroker.CorrespondentBranchId.HasValue;
            
            string msg = "OK";

            var missingFields = new List<string>();

            if (needSetOcBranch)
            {
                missingFields.Add("branch");
            }
            if (needSetOcMiniCorrBranch)
            {
                missingFields.Add("mini-correspondent branch");
            }
            if (needSetOcCorrBranch)
            {
                missingFields.Add("correspondent branch");
            }
            if (needSetOcPriceGroup)
            {
                missingFields.Add("price group");
            }
            if (needSetOcMiniCorrPriceGroup)
            {
                missingFields.Add("mini-correspondent price group");
            }
            if (needSetOcCorrPriceGroup)
            {
                missingFields.Add("correspondent price group");
            }

            if (missingFields.Any())
            {
                string fields = null;

                if (missingFields.Count == 1)
                {
                    fields = missingFields[0];
                }
                else if (missingFields.Count == 2)
                {
                    fields = string.Join(" and ", missingFields.ToArray());
                }
                else if (missingFields.Count > 2)
                {
                    fields = string.Join(", ", missingFields.Take(missingFields.Count - 1).ToArray());
                    fields += " and " + missingFields.Last();
                }

                msg = string.Format(
                    "The originating company's {0} {1} not been set{2}. Please " +
                    "edit the originating company and set the {0}{3}, and then retry.",
                    fields,
                    missingFields.Count == 1 ? "has" : "have",
                    ConstStage.EnableDefaultBranchesAndPriceGroups ? ", and no applicable Originator Portal-level defaults were found" : string.Empty,
                    ConstStage.EnableDefaultBranchesAndPriceGroups ? " or add Originator Portal-level defaults" : string.Empty);
            }

            return msg;
        }
    }
}
