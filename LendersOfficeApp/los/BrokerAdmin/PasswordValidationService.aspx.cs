using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOfficeApp.los.admin;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.BrokerAdmin
{
	public partial class PasswordValidationService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}
        private Guid BrokerId
        {
            get
            {
                if (CurrentUser != null)
                {
                    return CurrentUser.BrokerId;
                }

                return Guid.Empty;
            }
        }
        private bool IsActiveDirectoryAuthEnabledForLender
        {
            get
            {
                if (BrokerId != Guid.Empty)
                {
                    BrokerDB lender = BrokerDB.RetrieveById(BrokerId);
                    return lender.IsActiveDirectoryAuthenticationEnabled;
                }
                return false;
            }
        }
		
		protected override void Process(string methodName) 
		{
			switch (methodName) 
			{
				case "ValidatePassword":
					ValidatePassword();
					break;
				case "ValidateFields":
					ValidateFields();
					break;
				case "UnlockAccount":
					UnlockAccount();
					break;
			}
		}

		private void UnlockAccount()
		{
			string login = "";
			bool isPml = false;
			Guid employeeId;
			Guid brokerPmlSiteId = Guid.Empty;

			try
			{
				login = GetString("login");
				isPml = GetBool("isPml");
				
				//OPM 1863
				if(isPml)
				{
					employeeId = GetGuid("id");
                    SqlParameter[] parameters1 = {
                                                     new SqlParameter("@EmployeeId", employeeId)
                                                 };

					using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "RetrieveBrokerPmlSiteIdByEmployeeID", parameters1)) 
					{
						if(reader.Read())
						{
							brokerPmlSiteId = (Guid) reader["BrokerPmlSiteId"];
						}
					}
				}

				SqlParameter[] parameters = {
												new SqlParameter("@LoginNm", login),
												new SqlParameter("@LoginBlockExpirationD", SmallDateTime.MinValue),
												new SqlParameter("@Type", (isPml)?"P":"B"),
												new SqlParameter("@BrokerPmlSiteId", brokerPmlSiteId)
											};

				
				StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "ResetLoginFailureCount", 1, parameters);
				
			}
			catch( Exception e )
			{
				Tools.LogError("Unable to unlock account", e);
				SetResult("ErrorMessage", e.Message);
				return;
			}
		}

		private void ValidateFields()
		{
			Guid userId;
			string hasLogin = "";
			string pw = "";
			string firstName = "";
			string lastName = "";
			string login = "";
			string expirationDate = "";
            string fieldProblem = "";
            string originatorCompensationMinAmount = "";
            string originatorCompensationMaxAmount = "";
            bool originatorCompensationMinAmountEnabled = false;
            bool originatorCompensationMaxAmountEnabled = false;
			bool expiresOnChecked = false;
			bool changeLoginChecked = false;
			bool isPml = false;
            bool bIsActiveDirectoryUser = false;

			try 
			{
				userId = GetGuid("id");
				hasLogin = GetString("hasLogin");
				firstName = GetString("firstName");
				lastName = GetString("lastName");
                login = GetString("login");
                originatorCompensationMinAmount = GetString("originatorCompensationMinAmount");
                originatorCompensationMaxAmount = GetString("originatorCompensationMaxAmount");
                originatorCompensationMinAmountEnabled = GetBool("originatorCompensationMinAmountEnabled");
                originatorCompensationMaxAmountEnabled = GetBool("originatorCompensationMaxAmountEnabled");
				changeLoginChecked = GetBool("changeLoginChecked");
				isPml = GetBool("isPml");

                if (IsActiveDirectoryAuthEnabledForLender)
                {
                    bIsActiveDirectoryUser = (isPml) ? false : GetBool("isADUser");
                }

                if (!bIsActiveDirectoryUser)
                {
                    pw = GetString("pw");
                    expirationDate = GetString("expirationDate");
                    expiresOnChecked = GetBool("passwordExpiresOnChecked");
                }

                if (changeLoginChecked)
				{
					if(login.TrimWhitespaceAndBOM() == "")
                        fieldProblem = ErrorMessages.SpecifyLoginName + Environment.NewLine;
					
					EmployeeDB testDb = new EmployeeDB( userId , CurrentUser.BrokerId );

					if( userId == Guid.Empty )
						testDb = new EmployeeDB();
					else
						testDb.Retrieve();

					testDb.FirstName = firstName;
					testDb.LastName  = lastName;
					testDb.LoginName = login;

					if(isPml)
					{
                        BrokerDB brokerDB = BrokerDB.RetrieveById(CurrentUser.BrokerId);
                        if (Tools.IsPmlLoginUnique(brokerDB, login.TrimWhitespaceAndBOM(), testDb.UserID) == false)
                            fieldProblem = ErrorMessages.LoginNameAlreadyInUseFunction(login.TrimWhitespaceAndBOM()) + Environment.NewLine;
					}
					else
					{
						if( Tools.IsLoginUnique( login.TrimWhitespaceAndBOM() , testDb.UserID ) == false )
                            fieldProblem = ErrorMessages.LoginNameAlreadyInUseFunction(login.TrimWhitespaceAndBOM()) + Environment.NewLine;
					}

                    if (!bIsActiveDirectoryUser)
                    {
                        if (pw.TrimWhitespaceAndBOM() == "" && (testDb.IsNew || hasLogin == "false" || testDb.IsActiveDirectoryUser))
                            fieldProblem = ErrorMessages.SpecifyPassword + Environment.NewLine;

                        // if this is a new user, check to make sure the password does not violate
                        // the PasswordContainsLogin condition for the login name that is currently typed in
                        // but has not yet been given to the user officially
                        if (testDb.IsStrongPassword(pw) == StrongPasswordStatus.PasswordContainsLogin)
                            fieldProblem = ErrorMessages.PwCannotContainLoginOrName + Environment.NewLine;
                    }
				}
				
				//10/03/06 dc - Case 7611 adding password expiration features
				if( expiresOnChecked == true )
				{
					try
					{
						DateTime.Parse( expirationDate );
					}
					catch
					{
                        fieldProblem = ErrorMessages.SpecifyValidPasswordExpirationDate + Environment.NewLine;
					}
                }

                LosConvert m_losConvert = new LosConvert();
                if (originatorCompensationMinAmountEnabled && (string.IsNullOrEmpty(originatorCompensationMinAmount) || m_losConvert.ToMoney(originatorCompensationMinAmount) <= 0.0M))
                {
                    fieldProblem += ErrorMessages.OriginatorCompensationMinAmountRequired + Environment.NewLine;
                }

                if (originatorCompensationMaxAmountEnabled && (string.IsNullOrEmpty(originatorCompensationMaxAmount) || m_losConvert.ToMoney(originatorCompensationMaxAmount) <= 0.0M))
                {
                    fieldProblem += ErrorMessages.OriginatorCompensationMaxAmountRequired + Environment.NewLine;
                }

                if (originatorCompensationMaxAmountEnabled && originatorCompensationMinAmountEnabled &&
                    !string.IsNullOrEmpty(originatorCompensationMinAmount) && !string.IsNullOrEmpty(originatorCompensationMaxAmount) &&
                    (m_losConvert.ToMoney(originatorCompensationMinAmount) > m_losConvert.ToMoney(originatorCompensationMaxAmount)))
                {
                    fieldProblem += ErrorMessages.OriginatorCompensationMaxAmountLessThanMinAmount + Environment.NewLine;
                }
			}
			catch( Exception e )
			{
				SetResult("ErrorMessage", e.Message);
				return;
			}

            if (!String.IsNullOrEmpty(fieldProblem))
            {
                SetResult("ErrorMessage", fieldProblem);
                return;
            }
		}

		private void ValidatePassword() 
		{
			string pw = "";
			string retype = "";
            string passwordProblem = "";
			Guid userId;

            if (IsActiveDirectoryAuthEnabledForLender)
            {
                bool bIsPMLUser = GetBool("isPml");
                bool bIsActiveDirectoryUser = (bIsPMLUser) ? false : GetBool("isADUser");
                
                if (bIsActiveDirectoryUser)
                {
                    return;
                }
            }

			try 
			{
				pw = GetString("pw");
				retype = GetString("retype");
				userId = GetGuid("id");
				EmployeeDB empDB = new EmployeeDB(userId, CurrentUser.BrokerId);
		
				empDB.Retrieve();

				// 3/20/2006 mf - OPM 4226 - We do not change existing password if fields are blank
                if (empDB.IsNew || pw != "" || retype != "" || empDB.IsActiveDirectoryUser)
                {
                    if (pw == "")
                        passwordProblem = ErrorMessages.SpecifyPassword;

                    if (pw != retype)
                        passwordProblem = ErrorMessages.PasswordsDontMatch;

                    switch (empDB.IsStrongPassword(pw))
                    {
                        case StrongPasswordStatus.PasswordContainsLogin:
                            passwordProblem = ErrorMessages.PwCannotContainLoginOrName;
                            break;

                        case StrongPasswordStatus.PasswordMustBeAlphaNumeric:
                            passwordProblem = ErrorMessages.PasswordMustBeAlphaNumeric;
                            break;

                        case StrongPasswordStatus.PasswordTooShort:
                            passwordProblem = ErrorMessages.PasswordTooShort;
                            break;

                        case StrongPasswordStatus.PasswordRecycle:
                            passwordProblem = ErrorMessages.PasswordsShouldNotBeRecycled;
                            break;

                        // OPM 24614
                        case StrongPasswordStatus.PasswordContainsConsecutiveIdenticalChars:
                            passwordProblem = ErrorMessages.PasswordContainsConsecIdenticalChars;
                            break;

                        // OPM 24614
                        case StrongPasswordStatus.PasswordContainsConsecutiveAlphanumericChars:
                            passwordProblem = ErrorMessages.PasswordContainsConsecCharsInAlphaOrNumericOrder;
                            break;

                        case StrongPasswordStatus.PasswordHasUnverifiedCharacters:
                            passwordProblem = ErrorMessages.PasswordHasUnverifiedCharacters;
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                SetResult("ErrorMessage", e.Message);
                return;
            }

            if (!String.IsNullOrEmpty(passwordProblem))
            {
                SetResult("ErrorMessage", passwordProblem);
                return;
            }
		}
	}
}