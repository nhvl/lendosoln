﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsumerPortalLetterPicker.aspx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.ConsumerPortalLetterPicker" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <script type="text/javascript">
        function _init() {
            resize(500, 400);        
        }
        function f_selectForm(formId, formDesc) {
			
            var args = window.dialogArguments || {};
            args.FormId = formId;
            args.FormDescription = formDesc;
            onClosePopup(args);
        }
    </script>
    <h4 class="page-header">Custom PDF List</h4>
    <form id="form1" runat="server">
        <table cellspacing="2" cellpadding="0" width="100%" height="100%">
			<tr>
			    <td>
				    <ml:CommonDataGrid id="m_Grid" runat="server" CellPadding="2" DefaultSortExpression="Description">
	                    <Columns>
		                    <asp:TemplateColumn HeaderText="Description">
			                    <ItemTemplate>
				                    <a href="javascript:void(0);" onclick="f_selectForm(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "FormId").ToString()) %>, <%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "Description").ToString()) %>);">
					                    <%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "Description").ToString())%>
				                    </a>
			                    </ItemTemplate>
		                    </asp:TemplateColumn>
		                </Columns>
	                </ml:CommonDataGrid>
	            </td>
	        </tr>
	    </table>
    </form>
</body>
</html>
