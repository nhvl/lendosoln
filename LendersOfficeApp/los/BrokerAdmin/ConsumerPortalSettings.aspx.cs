﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using LendersOffice.ObjLib.ConsumerPortal;
using LendersOffice.Security;
using LendersOffice.ObjLib.TitleProvider;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.PdfForm;
using System.Text;
using LendersOffice.CustomPmlFields;

namespace LendersOfficeApp.los.BrokerAdmin
{
    public partial class ConsumerPortalSettings : LendersOffice.Common.BaseServicePage
    {
        internal static readonly string INVALID_PASSWORD = "*****";
        protected Guid PortalId
        {
            get { return RequestHelper.GetGuid("PortalId", Guid.Empty); }
        }
        private ConsumerPortalConfig m_portalConfig = null;
        /// <summary>
        /// Returns null for new portal.
        /// </summary>
        protected ConsumerPortalConfig PortalConfig
        {
            get
            {
                if (PortalId == Guid.Empty) return null;
                
                if (m_portalConfig == null)
                {
                    m_portalConfig = ConsumerPortalConfig.Retrieve(this.BrokerId, PortalId);
                }
                return m_portalConfig;
            }
        }
        protected Guid BrokerId
        {
            get { return PrincipalFactory.CurrentPrincipal.BrokerId; }
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingConsumerPortalConfigs
                };
            }
        }

        protected void PageLoad(object sender, EventArgs e)
        {
            var titleList = TitleProvider.GetAllAssociationsWithBrokerInfo(BrokerId);

            var craList = MasterCRAList.RetrieveAvailableCras(BrokerId);
            BindCRA(AutomaticCRAId, craList);

            StringBuilder sb = new StringBuilder();
            sb.Append("var creditHash = new Object();").Append(Environment.NewLine);
            foreach (CRA cra in craList)
            {

                if (cra.Protocol != CreditReportProtocol.Mcl)
                {
                    sb.AppendFormat("creditHash['{0}'] = {1};", cra.ID, cra.Protocol.ToString("D"));
                    sb.Append(Environment.NewLine);
                }
            }
            ClientScript.RegisterStartupScript(typeof(ConsumerPortalSettings), "creditHash", sb.ToString(), true);

            var Broker = BrokerDB.RetrieveById(BrokerId);
            bool m_hasBrokerDUInfo = Broker.CreditMornetPlusUserID != "" && Broker.CreditMornetPlusPassword.Value != "";
            ClientScript.RegisterHiddenField("m_hasBrokerDUInfo", m_hasBrokerDUInfo.ToString());

            SettingAskForMemberId.Visible = Broker.IsCreditUnion;//OPM 135717
            
            // 8/28/2013 GF - OPM 136980
            CustomPmlFields.DataSource = Broker.CustomPmlFieldList.FieldsSortedByRank;
            CustomPmlFields.DataBind();

            if (PortalId != Guid.Empty)
            {
                ConsumerPortalConfig config = ConsumerPortalConfig.Retrieve(BrokerId, PortalId);
                if (config.BrokerId != BrokerId)
                {
                    throw new AccessDenied();
                }

                Name.Text = config.Name;
                ReferralUrl.Text = config.ReferralUrl;
                StyleSheetUrl.Text = config.StyleSheetUrl;
                //FramedBorrowerLoginUrl.Text = config.FramedBorrowerLoginUrl;
                IsAskForReferralSource.SelectedValue = config.IsAskForReferralSource ? "Y" : "N";
                BrokerDB db = BrokerDB.RetrieveById(BrokerId);
                db.LeadSources.List.Sort();
                LeadSourceList l = db.LeadSources;

                foreach (LeadSource brokerSrc in l)
                {
                    if (config.DisplayedReferralSources.Exists(savedSrc => savedSrc.Id.HasValue && savedSrc.Id.Value == brokerSrc.Id))
                        SelectedReferralSources.Items.Add(new ListItem(brokerSrc.Name, brokerSrc.Id.ToString()));
                    else
                        AvailableReferralSources.Items.Add(new ListItem(brokerSrc.Name, brokerSrc.Id.ToString()));
                }

                IsAllowLockPeriodSelection_Y.Checked = config.IsAllowLockPeriodSelection;
                IsAllowLockPeriodSelection_N.Checked = !config.IsAllowLockPeriodSelection;
                if (config.IsAllowLockPeriodSelection)
                {
                    try
                    {
                        AvailableLockPeriod1.Text = config.AvailableLockPeriods[0].ToString();
                        AvailableLockPeriod2.Text = config.AvailableLockPeriods[1].ToString();
                        AvailableLockPeriod3.Text = config.AvailableLockPeriods[2].ToString();
                        AvailableLockPeriod4.Text = config.AvailableLockPeriods[3].ToString();
                        AvailableLockPeriod5.Text = config.AvailableLockPeriods[4].ToString();
                        AvailableLockPeriod6.Text = config.AvailableLockPeriods[5].ToString();
                        AvailableLockPeriod7.Text = config.AvailableLockPeriods[6].ToString();
                        AvailableLockPeriod8.Text = config.AvailableLockPeriods[7].ToString();
                        AvailableLockPeriod9.Text = config.AvailableLockPeriods[8].ToString();
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        //Ignore; Not all boxes are required to have values
                    }
                }
                else if (config.AvailableLockPeriods.Count > 0)
                {
                    LockPeriod.Text = config.AvailableLockPeriods[0].ToString();
                }

                EnableShortApplications_Y.Checked = config.EnableShortApplications;
                EnableShortApplications_N.Checked = !config.EnableShortApplications;

                EnableFullApplications_Y.Checked = config.EnableFullApplications;
                EnableFullApplications_N.Checked = !config.EnableFullApplications;

                AllowRegisterNewAccounts_Y.Checked = config.AllowRegisterNewAccounts;
                AllowRegisterNewAccounts_N.Checked = !config.AllowRegisterNewAccounts;

                //Transform EmployeeId to UserId
                LeadManagerName.Text = GetUserDisplayName(config.LeadManagerUserId);
                LoanOfficerName.Text = GetUserDisplayName(config.LoanOfficerUserId);

                IsHideLoanOfficer.SelectedValue = config.IsHideLoanOfficer ? "Y" : "N";
                GetRatesPageDisclaimer.Text = config.GetRatesPageDisclaimer;
                OnlineApplicationTerms.Text = config.OnlineApplicationTerms;
                OnlineApplicationCreditCheck.Text = config.OnlineApplicationCreditCheck;
                IsAutomaticPullCredit.SelectedValue = config.IsAutomaticPullCredit ? "Y" : "N";
                AutomaticCRAId.SelectedValue = config.AutomaticCRAId.ToString();
                AutomaticCRADULogin.Text = config.AutomaticCRADULogin;
                if (!string.IsNullOrEmpty(config.AutomaticCRADUPassword))
                    AutomaticCRADUPassword.Attributes.Add("value", INVALID_PASSWORD);

                AutomaticCRAAccountId.Text = config.AutomaticCRAAccountId;
                AutomaticCRALogin.Text = config.AutomaticCRALogin;
                if (!string.IsNullOrEmpty(config.AutomaticCRAPassword))
                    AutomaticCRAPassword.Attributes.Add("value", INVALID_PASSWORD);

                AutomaticDUCraId.SelectedValue = config.AutomaticDUCraId;
                AutomaticDUCraLogin.Text = config.AutomaticDUCraLogin;
                if (!string.IsNullOrEmpty(config.AutomaticDUCraPassword))
                {
                    AutomaticDUCraPassword.Attributes.Add("value", INVALID_PASSWORD);
                }

                EligibleProgramDisclaimer.Text = config.EligibleProgramDisclaimer;
                NoEligibleProgramMessage.Text = config.NoEligibleProgramMessage;
                IsAutomaticRunDU.SelectedValue = config.IsAutomaticRunDU ? "Y" : "N";
                AutomaticDULogin.Text = config.AutomaticDULogin;
                if (!string.IsNullOrEmpty(config.AutomaticDUPassword))
                    AutomaticDUPassword.Attributes.Add("value", INVALID_PASSWORD);

                ReceiveOnSubmission_PreApprov.Checked = config.F1003SubmissionReceiptT == E_F1003SubmissionReceiptT.PreApprovalLetter;
                try
                {
                    PreApprovalLetter.Text = PdfForm.LoadById(config.PreApprovalLetterId).Description;
                }
                catch (NotFoundException) { /* Just skip load */ }
                ReceiveOnSubmission_PreQual.Checked = config.F1003SubmissionReceiptT == E_F1003SubmissionReceiptT.PreQualificationLetter;
                try
                {
                    PreQualificationLetter.Text = PdfForm.LoadById(config.PreQualificationLetterdId).Description;
                }
                catch (NotFoundException) { /* Just skip load */ }
                ReceiveOnSubmission_ConfirmOnly.Checked = config.F1003SubmissionReceiptT == E_F1003SubmissionReceiptT.ConfirmationOnly;

                ShortApplicationConfirmation.Text = config.ShortApplicationConfirmation;
                FullApplicationConfirmation.Text = config.FullApplicationConfirmation;
                ContactInfoMessage.Text = config.ContactInfoMessage;
                HelpLinkPagesHostURL.Text = config.HelpLinkPagesHostURL;

                IsHelpForGetRatesEnabled.SelectedValue = config.IsHelpForGetRatesEnabled ? "Y" : "N";
                IsHelpForApplicantLoginEnabled.SelectedValue = config.IsHelpForApplicantLoginEnabled ? "Y" : "N";
                IsHelpForApplicantLoanPipelineEnabled.SelectedValue = config.IsHelpForApplicantLoanPipelineEnabled ? "Y" : "N";
                IsHelpForPersonalInformationEnabled.SelectedValue = config.IsHelpForPersonalInformationEnabled ? "Y" : "N";
                IsHelpForLoanInformationEnabled.SelectedValue = config.IsHelpForLoanInformationEnabled ? "Y" : "N";
                IsHelpForPropertyInformationEnabled.SelectedValue = config.IsHelpForPropertyInformationEnabled ? "Y" : "N";
                IsHelpForLoanOfficerEnabled.SelectedValue = config.IsHelpForLoanOfficerEnabled ? "Y" : "N";
                IsHelpForAssetInformationEnabled.SelectedValue = config.IsHelpForAssetInformationEnabled ? "Y" : "N";
                IsHelpForLiabilityInformationEnabled.SelectedValue = config.IsHelpForLiabilityInformationEnabled ? "Y" : "N";
                IsHelpForExpenseInformationEnabled.SelectedValue = config.IsHelpForExpenseInformationEnabled ? "Y" : "N";
                IsHelpForIncomeInformationEnabled.SelectedValue = config.IsHelpForIncomeInformationEnabled ? "Y" : "N";
                IsHelpForDeclarationsEnabled.SelectedValue = config.IsHelpForDeclarationsEnabled ? "Y" : "N";
                IsHelpForSelectLoanEnabled.SelectedValue = config.IsHelpForSelectLoanEnabled ? "Y" : "N";
                IsHelpForSubmitApplicationEnabled.SelectedValue = config.IsHelpForSubmitApplicationEnabled ? "Y" : "N";
                IsHelpForLoanStatusAndDocExchangeEnabled.SelectedValue = config.IsHelpForLoanStatusAndDocExchangeEnabled ? "Y" : "N";
                IsHelpForEmploymentEnabled.SelectedValue = config.IsHelpForEmploymentEnabled ? "Y" : "N";
                IsAskForMemberId.SelectedValue = config.IsAskForMemberId ? "Y" : "N";
                ARMProgramDisclosuresURL.Text = config.ARMProgramDisclosuresURL;
                
                AskConsumerForSubjProperty.SelectedValue = ((int)config.AskConsumerForSubjProperty).ToString();
                AskConsumerForSubjValue.SelectedValue = ((int)config.AskConsumerForSubjValue).ToString();

                SelectsLoanProgramInFullSubmission.Items.Add(new ListItem("Yes, always.", "1", true));
                SelectsLoanProgramInFullSubmission.Items.Add(new ListItem("Yes for purchase, no for refinance & home equity.", "2", true));
                SelectsLoanProgramInFullSubmission.Items.Add(new ListItem("No, never.", "0", true));
                SelectsLoanProgramInFullSubmission.SelectedValue = ((int)config.SelectsLoanProgramInFullSubmission).ToString();
                IsNewPmlUIEnabled.Checked = db.IsNewPmlUIEnabled;

                DefaultLoanTemplateId.SelectedValue = config.DefaultLoanTemplateId.ToString();
                SubmissionsShouldBecomeLoans.SelectedValue = config.SubmissionsShouldBecomeLoans ? "Y" : "N";

                IsGenerateLeadOnShortAppSubmission.SelectedValue = config.IsGenerateLeadOnShortAppSubmission ? "Y" : "N";
                
                LoanStatusConfigJson.Value = SerializationHelper.JsonNetAnonymousSerialize(config.StatusList);

                IsAllowRespaOnly_HomeEquity.Checked = config.IsAllowRespaOnly_HomeEquity;
                IsAllowRespaOnly_Purchase.Checked = config.IsAllowRespaOnly_Purchase;
                IsAllowRespaOnly_Refinance.Checked = config.IsAllowRespaOnly_Refinance;
            }
            else
            {
                BrokerDB db = BrokerDB.RetrieveById(BrokerId);
                db.LeadSources.List.Sort();
                LeadSourceList l = db.LeadSources;

                foreach (var item in l)
                {
                    AvailableReferralSources.Items.Add(new ListItem(item.Name, item.Id.ToString()));
                }

                SelectsLoanProgramInFullSubmission.Items.Add(new ListItem("Yes, always.", "1", true));
                SelectsLoanProgramInFullSubmission.Items.Add(new ListItem("Yes for purchase, no for refinance & home equity.", "2", true));
                SelectsLoanProgramInFullSubmission.Items.Add(new ListItem("No, never.", "0", true));
                SelectsLoanProgramInFullSubmission.SelectedValue = db.IsNewPmlUIEnabled ? "1" : "0";
                IsNewPmlUIEnabled.Checked = db.IsNewPmlUIEnabled;

                DefaultLoanTemplateId.SelectedValue = Broker.PmlLoanTemplateID.ToString();
            }
        }
        protected void PageInit(object sender, EventArgs e)
        {
            this.RegisterService("main", "/los/BrokerAdmin/ConsumerPortalSettingsService.aspx");
            RegisterJsScript("jquery.tmpl.js");
            RegisterJsScript("LQBPopup.js");
            
            AutomaticDUCraId.DataSource = LendersOffice.DU.DUServer.GetCreditProviderList();
            AutomaticDUCraId.DataTextField = "Value";
            AutomaticDUCraId.DataValueField = "Key";
            AutomaticDUCraId.DataBind();

            BindLoanTemplates();
        }

        private void BindLoanTemplates()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", BrokerId),
                new SqlParameter("@IsTemplate", 1),
                new SqlParameter("@IsValid", 1)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "ListLoansByBrokerId", parameters))
            {
                Dictionary<string, string> entries = new Dictionary<string, string>();
                while (reader.Read())
                {
                    entries.Add(reader["LoanNm"].ToString(), reader["LoanId"].ToString());
                }
                DefaultLoanTemplateId.DataTextField = "Key";
                DefaultLoanTemplateId.DataValueField = "Value";
                DefaultLoanTemplateId.DataSource = entries.OrderBy(kvp => kvp.Key);
                DefaultLoanTemplateId.DataBind();
            }
        }
        private string GetUserDisplayName(Guid userId)
        {
            if (userId == Guid.Empty) return string.Empty;

            SqlParameter[] parameters = {
                                            new SqlParameter("@EmployeeUserID", userId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "GetEmployeeNameByUserId", parameters))
            {
                if (reader.Read())
                {
                    return reader["FirstLast"].ToString();
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Cannot find user " + userId.ToString());
                }
            }
        }
        private void BindCRA(DropDownList ddl, IEnumerable<CRA> list)
        {
            ddl.Items.Add(new ListItem("", Guid.Empty.ToString()));
            foreach (CRA o in list)
            {
                ddl.Items.Add(new ListItem(o.VendorName, o.ID.ToString()));
            }
        }

        protected void CustomPmlFields_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var customPmlField = e.Item.DataItem as CustomPmlField;
            var label = e.Item.FindControl("FieldDesc") as Label;
            var radioListFa = e.Item.FindControl("IsEnabledFa") as RadioButtonList;
            var radioListQp = e.Item.FindControl("IsEnabledQp") as RadioButtonList;
            var keywordNumber = e.Item.FindControl("KeywordNumber") as System.Web.UI.HtmlControls.HtmlInputHidden;
            var visibilityType = e.Item.FindControl("VisibilityType") as Label;

            if (customPmlField == null || label == null || radioListFa == null || radioListQp == null || keywordNumber == null || visibilityType == null) return;

            keywordNumber.Value = customPmlField.KeywordNum.ToString("d");
            if (customPmlField.IsValid)
            {
                label.Text = customPmlField.Description;
                if (PortalConfig != null)
                {
                    radioListFa.SelectedValue = PortalConfig.EnabledCustomPmlFields_FullApp.Contains(customPmlField.KeywordNum) ? "Y" : "N";
                    radioListQp.SelectedValue = PortalConfig.EnabledCustomPmlFields_QuickPricer.Contains(customPmlField.KeywordNum) ? "Y" : "N";
                }
                visibilityType.Text = customPmlField.VisibilityType_rep;
            }
            else
            {
                label.Text = "--Unused Custom PML Field--";
                radioListFa.Enabled = false;
                radioListQp.Enabled = false;
                visibilityType.Text = "N/A";
            }
        }
        
        protected override E_JqueryVersion GetJQueryVersion()
        {
            return E_JqueryVersion._1_7_1;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            EnableJquery = true;

            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion
    }
}
