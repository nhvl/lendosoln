﻿namespace LendersOfficeApp.los.BrokerAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.PMLNavigation;
    using LendersOffice.ObjLib.UI.Themes;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    public partial class TPOPortalConfigService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        private BrokerUserPrincipal BrokerUser
        {
            get
            {
                return BrokerUserPrincipal.CurrentPrincipal;
            }
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "SaveData":
                    this.SaveData();
                    break;
                case "AddConfig":
                    AddConfig();
                    break;
                case "UpdateConfig":
                    UpdateConfig();
                    break;
                case "DeleteConfig":
                    DeleteConfig();
                    break;
                case "GetParamsToAddPipelineReport":
                    this.GetParamsToAddPipelineReport();
                    break;
                case "ValidateData":
                    this.ValidateData();
                    break;
                case nameof(Tools.IsCorsPageEnabled):
                    this.SetResult("CorsUriIsEnabled", Tools.IsCorsPageEnabled(this.GetString("PageUri", null)));
                    break;
            }
        }

        private void ValidateData()
        {
            var opNonQmMsgReceivingEmail = GetString("OpNonQmMsgReceivingEmail");
            
            if (!string.IsNullOrWhiteSpace(opNonQmMsgReceivingEmail) && !EmailAddress.Create(opNonQmMsgReceivingEmail).HasValue)
            {
                SetResult("ErrorMessage", "Email address is invalid.");
            }
        }

        private void SaveData()
        {
            var broker = this.BrokerUser.BrokerDB;
            using (var executor = new CStoredProcedureExec(broker.BrokerID))
            {
                executor.BeginTransactionForWrite();

                try
                {
                    var config = new NavigationConfiguration();
                    var isNonQmOpEnabled = PrincipalFactory.CurrentPrincipal.BrokerDB.IsNonQmOpEnabled(PrincipalFactory.CurrentPrincipal);
                    config.ReadFromDynaTreeJson(this.GetString("treeNavJson"), isNonQmOpEnabled);
                    var allowedUrlOrigins = ConstStage.CrossOriginUrls;
                    var invalidPages =
                        config.RootNode.NodesRecursive(node => 
                            !node.isFolder
                            && !node.IsEditableSystemPage
                            && !node.CanBeRenamed
                            && !node.IsSystem
                            && !NavigationConfiguration.SystemNode.BelongsToSystemNode(node.Id)
                            && node.ProvideSystemAccess
                            && !Tools.IsCorsPageEnabled(node.ConfigHref));
                    if (invalidPages.Any())
                    {
                        var message = $@"Cannot save configuration. The following pages must be whitelisted by LendingQB Support before enabling the ""Allow this page access to LendingQB"" setting: {
                            string.Join(", ",
                            invalidPages.Select(page =>
                                string.IsNullOrEmpty(page.ConfigHref)
                                    ? "[empty URI]"
                                    : new Uri(page.ConfigHref).GetComponents(UriComponents.Scheme | UriComponents.HostAndPort, UriFormat.Unescaped)))}.";
                        throw new CBaseException(message, message);
                    }

                    broker.SetPmlNavigationConfiguration(PrincipalFactory.CurrentPrincipal, config);

                    broker.PmlLoanTemplateID = this.GetGuid("PmlLoanTemplateId");
                    broker.MiniCorrLoanTemplateID = this.GetGuid("MiniCorrLoanTemplateId");
                    broker.CorrLoanTemplateID = this.GetGuid("CorrLoanTemplateId");

                    if (broker.EnableRetailTpoPortalMode)
                    {
                        broker.RetailTPOLoanTemplateID = this.GetGuid("RetailTPOLoanTemplateId");
                    }                    

                    broker.IsUseTasksInTpoPortal = this.GetBool("UseTasksInPortal");
                    broker.IsShowPipelineOfTasksForAllLoansInTpoPortal = this.GetBool("ShowTasksPipeline");
                    broker.IsShowPipelineOfConditionsForAllLoansInTpoPortal = this.GetBool("ShowConditionsPipeline");

                    broker.TpoNewFeatureAlphaTestingGroup = this.GetGuid("TpoAlphaTestGroup");
                    broker.TpoNewFeatureBetaTestingGroup = this.GetGuid("TpoBetaTestGroup");

                    broker.TpoRedisclosureAllowedGroupType = (NewTpoFeatureOcGroupAccess)this.GetInt("TpoRedisclosureGroupAccess");
                    broker.TpoRedisclosureFormSource = (TpoRequestFormSource)this.GetInt("TpoRedisclosureFormSource");
                    broker.TpoRedisclosureFormCustomPdfId = this.GetGuid("TpoRedisclosureFormId", Guid.Empty);
                    broker.TpoRedisclosureFormHostedUrl = this.GetString("TpoRedisclosureFormUrl");
                    broker.TpoRedisclosureFormRequiredDocType = this.GetInt("TpoRedisclosureRequiredDocument", int.MinValue);

                    broker.TpoInitialClosingDisclosureAllowedGroupType = (NewTpoFeatureOcGroupAccess)this.GetInt("TpoInitialClosingDisclosureGroupAccess");
                    broker.TpoInitialClosingDisclosureFormSource = (TpoRequestFormSource)this.GetInt("TpoInitialClosingDisclosureFormSource");
                    broker.OrigPortalAntiSteeringDisclosureAccess = (OrigPortalAntiSteeringDisclosureAccessType)this.GetInt("OrigPortalAntiSteeringDisclosureAccess");

                    if (broker.IsNonQmOpEnabled(PrincipalFactory.CurrentPrincipal))
                    {
                        broker.OpNonQmMsgReceivingEmail = this.GetString("OpNonQmMsgReceivingEmail");

                        if (broker.EnableConversationLogForLoans)
                        {
                            broker.OpConLogCategoryId = this.GetLong("OpConLogCategoryId");
                        }
                    }                    

                    broker.TpoInitialClosingDisclosureFormCustomPdfId = this.GetGuid("TpoInitialClosingDisclosureFormId", Guid.Empty);
                    broker.TpoInitialClosingDisclosureFormHostedUrl = this.GetString("TpoInitialClosingDisclosureFormUrl");
                    broker.TpoInitialClosingDisclosureFormRequiredDocType = this.GetInt("TpoInitialClosingDisclosureRequiredDocument", int.MinValue);

                    broker.Save(executor);

                    if (ConstStage.EnableDefaultBranchesAndPriceGroups)
                    {
                        broker.UpdateDefaultBranchPriceGroupValues(
                            executor,
                            this.GetGuid("DefaultWholesaleBranchId"),
                            this.GetGuid("DefaultWholesalePriceGroupId"),
                            this.GetGuid("DefaultMiniCorrBranchId"),
                            this.GetGuid("DefaultMiniCorrPriceGroupId"),
                            this.GetGuid("DefaultCorrBranchId"),
                            this.GetGuid("DefaultCorrPriceGroupId"));
                    }

                    var collection = ThemeManager.GetThemeCollection(broker.BrokerID, ThemeCollectionType.TpoPortal);

                    var deletedThemeIds = SerializationHelper.JsonNetDeserialize<List<Guid>>(this.GetString("DeletedThemeIdJson", "[]"));
                    foreach (var deletedThemeId in deletedThemeIds)
                    {
                        collection.DeleteTheme(executor, deletedThemeId);
                    }

                    collection.SetSelectedTheme(this.GetGuid("DefaultColorThemeId", SassDefaults.LqbDefaultColorTheme.Id));

                    collection.Save();

                    var pipelineSettings = SerializationHelper.JsonNetDeserialize<List<OriginatingCompanyUserPipelineSettings>>(
                        this.GetString("OriginatingCompanyUserPipelineSettings"));

                    OriginatingCompanyUserPipelineSettings.Save(broker.BrokerID, executor, pipelineSettings);

                    executor.CommitTransaction();
                }
                catch
                {
                    executor.RollbackTransaction();

                    throw;
                }
            }
        }

        private void AddConfig()
        {
            var config = new LendersOffice.ObjLib.TPO.TPOPortalConfig(
                BrokerUser.BrokerId,
                GetString("Name").TrimWhitespaceAndBOM(),
                GetString("Url").TrimWhitespaceAndBOM());


            var result = config.Save(BrokerUser);
            if (result.Succeeded) { SetResult("ID", config.ID); }

            ReportResults(result);
        }
        private void UpdateConfig()
        {
            var configID = GetGuid("ID");


            var config = LendersOffice.ObjLib.TPO.TPOPortalConfig.RetrieveByID(configID, BrokerUser.BrokerId);
            config.Name = GetString("Name").TrimWhitespaceAndBOM();
            config.Url = GetString("Url").TrimWhitespaceAndBOM();


            var result = config.Save(BrokerUser);
            
            ReportResults(result);
        }
        private void DeleteConfig()
        {
            var configID = GetGuid("ID");
            
            var result = LendersOffice.ObjLib.TPO.TPOPortalConfig.Delete(configID, BrokerUser.BrokerId, BrokerUser);
            
            ReportResults(result);
        }
        private void ReportResults(DBHitInfo info)
        {
            if (info.Succeeded)
            {
                SetResult("Succeeded", true);
            }
            else
            {
                SetResult("Succeeded", false);
                SetResult("ReasonFailed", info.ReasonFailed);
            }
        }

        private void GetParamsToAddPipelineReport()
        {
            var existingIdJson = this.GetString("ExistingReportIds");
            var cacheKey = Guid.NewGuid();

            AutoExpiredTextCache.AddToCacheByUser(this.BrokerUser, existingIdJson, TimeSpan.FromMinutes(10), cacheKey);

            this.SetResult("Id", Guid.NewGuid());
            this.SetResult("CacheKey", cacheKey);
        }
    }
}
