﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using DataAccess;
using System.Data;

namespace LendersOfficeApp.los.BrokerAdmin
{
    public partial class ConsumerPortalEmployeePicker : LendersOffice.Common.BasePage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingConsumerPortalConfigs
                };
            }
        }

        private static E_RoleT[] LEAD_ROLES = { E_RoleT.CallCenterAgent, E_RoleT.LoanOfficer, E_RoleT.LoanOpener };
        private static E_RoleT[] LOAN_ROLES = { E_RoleT.LoanOfficer };
        protected E_RoleT[] Roles
        {
            get
            {
                string type = RequestHelper.GetSafeQueryString("Type");
                switch(type)
                {
                    case "Lead":
                        return LEAD_ROLES;
                    case "Loan":
                        return LOAN_ROLES;
                    default:
                        throw new ArgumentException("Invalid Type for ConsumerPortalEmployeePicker.aspx: Type="+type);
                }
            }
        }
        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BrokerLoanAssignmentTable roleTable = new BrokerLoanAssignmentTable();
            foreach (var roleType in Roles)
            {
                roleTable.Retrieve(BrokerUser.BrokerId, Guid.Empty, null, roleType, E_EmployeeStatusFilterT.ActiveOnly);
            }
            IEnumerable<LendersOffice.Admin.BrokerLoanAssignmentTable.Spec> employees = new List<LendersOffice.Admin.BrokerLoanAssignmentTable.Spec>();
            foreach (var roleType in Roles)
            {
                if (roleTable[roleType] == null)
                {
                    continue;
                }
                var list = roleTable[roleType];
                employees = employees.Union(list, EmployeeEqualityComparer.Instance);
            }
            //OPM 126553: Only show non-PML users (companyName is empty string)
            m_Grid.DataSource = ViewGenerator.Generate(employees.Where(emp => string.IsNullOrEmpty(emp.CompanyName)).ToArray());
            m_Grid.DataBind();
        }
        private class EmployeeEqualityComparer : IEqualityComparer<LendersOffice.Admin.BrokerLoanAssignmentTable.Spec>
        {
            public static readonly EmployeeEqualityComparer Instance = new EmployeeEqualityComparer();
            private EmployeeEqualityComparer() { }
            public bool Equals(BrokerLoanAssignmentTable.Spec x, BrokerLoanAssignmentTable.Spec y)
            {
                return x.EmployeeId == y.EmployeeId;
            }
            public int GetHashCode(BrokerLoanAssignmentTable.Spec obj)
            {
                return obj.EmployeeId.GetHashCode();
            }
        };
    }
}
