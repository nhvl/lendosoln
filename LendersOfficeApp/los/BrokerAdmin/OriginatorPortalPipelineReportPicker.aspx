﻿﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OriginatorPortalPipelineReportPicker.aspx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.OriginatorPortalPipelineReportPicker" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select Originator Portal Custom Report</title>
    <style type="text/css">
        body { background-color: gainsboro; }
        .text-center { text-align: center; }
        .padding-4 { padding: 4px; }
        .full-width { width: 100%; }
    </style>
    <script type="text/javascript">
        function selectReport(link) {
            var $link = $(link);
            var queryId = $link.attr('data-query-id');
            var queryName = $link.attr('data-query-name');

            var data = window.dialogArguments || {};
            data.OK = true;
            data.QueryId = queryId;
            data.QueryName = queryName;
            onClosePopup(data);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="FormTableHeader padding-4">Select Originator Portal Custom Report</div>
    <table class="full-width">
        <asp:Repeater runat="server" ID="ReportsList" OnItemDataBound="ReportsList_ItemDataBound">
            <ItemTemplate>
                <tr class="GridAutoItem">
                    <td class="text-center">
                        <a id="SelectLink" runat="server" onclick="selectReport(this);">select</a>
                    </td>
                    <td>
                        <span id="QueryName" runat="server"></span>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    </form>
</body>
</html>