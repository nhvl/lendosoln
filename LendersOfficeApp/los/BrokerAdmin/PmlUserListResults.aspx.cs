namespace LendersOfficeApp.los.BrokerAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Web.UI.WebControls;

    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.BrokerAdmin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Conversions;
    using LendersOffice.Security;

    public partial class PmlUserListResults : LendersOffice.Common.BaseServicePage
    {
        #region Column Constants
        private const int CHECKBOX = 0;
        private const int EDIT = 1;
        private const int LOGIN_NAME = 2;
        private const int USER_NAME = 3;
        private const int PML_OPTIONS = 4;
        private const int BROKER_ACCOUNT_EXECUTIVE = 5;
        private const int MINI_CORR_ACCOUNT_EXECUTIVE = 6;
        private const int CORR_ACCOUNT_EXECUTIVE = 7;
        private const int BROKER_LOCK_DESK = 8;
        private const int MINI_CORR_LOCK_DESK = 9;
        private const int CORR_LOCK_DESK = 10;
        private const int BROKER_UNDERWRITER = 11;
        private const int MINI_CORR_UNDERWRITER = 12;
        private const int CORR_UNDERWRITER = 13;
        private const int BROKER_JUNIOR_UNDERWRITER = 14;
        private const int MINI_CORR_JUNIOR_UNDERWRITER = 15;
        private const int CORR_JUNIOR_UNDERWRITER = 16;
        private const int MINI_CORR_CREDIT_AUDITOR = 17;
        private const int CORR_CREDIT_AUDITOR = 18;
        private const int MINI_CORR_LEGAL_AUDITOR = 19;
        private const int CORR_LEGAL_AUDITOR = 20;
        private const int BROKER_MANAGER = 21;
        private const int MINI_CORR_MANAGER = 22;
        private const int CORR_MANAGER = 23;
        private const int BROKER_PROCESSOR = 24;
        private const int MINI_CORR_PROCESSOR = 25;
        private const int CORR_PROCESSOR = 26;
        private const int BROKER_JUNIOR_PROCESSOR = 27;
        private const int MINI_CORR_JUNIOR_PROCESSOR = 28;
        private const int CORR_JUNIOR_PROCESSOR = 29;
        private const int MINI_CORR_PURCHASER = 30;
        private const int CORR_PURCHASER = 31;
        private const int MINI_CORR_SECONDARY = 32;
        private const int CORR_SECONDARY = 33;
        private const int IS_SUPERVISOR = 34;
        private const int SUPERVISOR_NAME = 35;
        private const int COMPANY = 36;
        private const int ROLES = 37;
        private const int BROKER_PRICE_GROUP = 38;
        private const int MINI_CORR_PRICE_GROUP = 39;
        private const int CORR_PRICE_GROUP = 40;
        private const int STATUS = 41;
        private const int LAST_LOGIN = 42;
        private const int BROKER_BRANCH = 43;
        private const int MINI_CORR_BRANCH = 44;
        private const int CORR_BRANCH = 45;
        private const int ACTIVE_STATUS = 46;
        #endregion

		protected DataTable m_dataTable;
		protected bool m_isTooManyUsers = false;
		protected int m_currentCount = 0;
        protected int m_totalCount = 0;

        private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

        private RelationshipSearchSetting GetRelationshipSearchSetting(string relationship)
        {
            var relationshipSearchSetting = new RelationshipSearchSetting();

            if (relationship.Equals("LenderAccountExec"))
            {
                if (Request.Form["sLenderAccountExecFilter"].Equals("None"))
                {
                    relationshipSearchSetting.LenderAccountExecEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.LenderAccountExecEmployeeID = new Guid(Request.Form["sLenderAccountExecFilter"]);
                }
            }
            else if (relationship.Equals("Underwriter"))
            {
                if (Request.Form["sUnderwriterFilter"].Equals("None"))
                {
                    relationshipSearchSetting.UnderwriterEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.UnderwriterEmployeeID = new Guid(Request.Form["sUnderwriterFilter"]);
                }
            }
            else if (relationship.Equals("JuniorUnderwriter"))
            {
                if (Request.Form["sJuniorUnderwriterFilter"].Equals("None"))
                {
                    relationshipSearchSetting.JuniorUnderwriterEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.JuniorUnderwriterEmployeeID = new Guid(Request.Form["sJuniorUnderwriterFilter"]);
                }
            }
            else if (relationship.Equals("LockDesk"))
            {
                if (Request.Form["sLockDeskFilter"].Equals("None"))
                {
                    relationshipSearchSetting.LockDeskEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.LockDeskEmployeeID = new Guid(Request.Form["sLockDeskFilter"]);
                }
            }
            else if (relationship.Equals("Manager"))
            {
                if (Request.Form["sManagerFilter"].Equals("None"))
                {
                    relationshipSearchSetting.ManagerEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.ManagerEmployeeID = new Guid(Request.Form["sManagerFilter"]);
                }
            }
            else if (relationship.Equals("Processor"))
            {
                if (Request.Form["sProcessorFilter"].Equals("None"))
                {
                    relationshipSearchSetting.ProcessorEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.ProcessorEmployeeID = new Guid(Request.Form["sProcessorFilter"]);
                }
            }
            else if (relationship.Equals("JuniorProcessor"))
            {
                if (Request.Form["sJuniorProcessorFilter"].Equals("None"))
                {
                    relationshipSearchSetting.JuniorProcessorEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.JuniorProcessorEmployeeID = new Guid(Request.Form["sJuniorProcessorFilter"]);
                }
            }
            else if (relationship.Equals("CreditAuditor"))
            {
                if (Request.Form["sCreditAuditorFilter"].Equals("None"))
                {
                    relationshipSearchSetting.CreditAuditorEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.CreditAuditorEmployeeID = new Guid(Request.Form["sCreditAuditorFilter"]);
                }
            }
            else if (relationship.Equals("LegalAuditor"))
            {
                if (Request.Form["sLegalAuditorFilter"].Equals("None"))
                {
                    relationshipSearchSetting.LegalAuditorEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.LegalAuditorEmployeeID = new Guid(Request.Form["sLegalAuditorFilter"]);
                }
            }
            else if (relationship.Equals("Purchaser"))
            {
                if (Request.Form["sPurchaserFilter"].Equals("None"))
                {
                    relationshipSearchSetting.PurchaserEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.PurchaserEmployeeID = new Guid(Request.Form["sPurchaserFilter"]);
                }
            }
            else if (relationship.Equals("Secondary"))
            {
                if (Request.Form["sSecondaryFilter"].Equals("None"))
                {
                    relationshipSearchSetting.SecondaryEmployeeID = Guid.Empty;
                }
                else
                {
                    relationshipSearchSetting.SecondaryEmployeeID = new Guid(Request.Form["sSecondaryFilter"]);
                }
            }

            return relationshipSearchSetting;
        }

        private void AddBrokerRelationshipSearchParameters(
            RelationshipSearchSetting settings,
            List<SqlParameter> parameters)
        {
            if (settings.BranchID.HasValue && settings.BranchID != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@BranchId", settings.BranchID));
            }
            else if (settings.UseOriginatingCompanyBranch)
            {
                parameters.Add(new SqlParameter("@BranchUseOc", 1));
            }

            if (settings.PriceGroupID.HasValue)
            {
                if (settings.PriceGroupID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@PriceGroupNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@PriceGroup", settings.PriceGroupID));
                }
            }
            else if (settings.UseOriginatingCompanyPriceGroup)
            {
                parameters.Add(new SqlParameter("@PriceGroupUseOc", 1));
            }

            if (settings.ManagerEmployeeID.HasValue)
            {
                if (settings.ManagerEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@ManagerEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@ManagerEmployeeId", settings.ManagerEmployeeID));
                }
            }
            else if (settings.ProcessorEmployeeID.HasValue)
            {
                if (settings.ProcessorEmployeeID == Guid.Empty)
                {
                parameters.Add(new SqlParameter("@ProcessorEmployeeNone", 1));
                }
                else
                {
                parameters.Add(new SqlParameter("@ProcessorEmployeeId", settings.ProcessorEmployeeID));
                }
            }
            else if (settings.JuniorProcessorEmployeeID.HasValue)
            {
                if (settings.JuniorProcessorEmployeeID == Guid.Empty)
                {
                parameters.Add(new SqlParameter("@JuniorProcessorEmployeeNone", 1));
                }
                else
                {
                parameters.Add(new SqlParameter("@JuniorProcessorEmployeeId", settings.JuniorProcessorEmployeeID));
                }
            }
            else if (settings.LenderAccountExecEmployeeID.HasValue)
            {
                if (settings.LenderAccountExecEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@LenderAccExecEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@LenderAccExecEmployeeId", settings.LenderAccountExecEmployeeID));
                }
            }
            else if (settings.UnderwriterEmployeeID.HasValue)
            {
                if (settings.UnderwriterEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@UnderwriterEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@UnderwriterEmployeeId", settings.UnderwriterEmployeeID));
                }
            }
            else if (settings.JuniorUnderwriterEmployeeID.HasValue)
            {
                if (settings.JuniorUnderwriterEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@JuniorUnderwriterEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@JuniorUnderwriterEmployeeId", settings.JuniorUnderwriterEmployeeID));
                }
            }
            else if (settings.LockDeskEmployeeID.HasValue)
            {
                if (settings.LockDeskEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@LockDeskEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@LockDeskEmployeeId", settings.LockDeskEmployeeID));
                }
            }
        }

        private void AddCorrRelationshipSearchParameters(
            RelationshipSearchSetting settings,
            List<SqlParameter> parameters)
        {
            if (settings.BranchID.HasValue && settings.BranchID != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@CorrBranchId", settings.BranchID));
            }
            else if (settings.UseOriginatingCompanyBranch)
            {
                parameters.Add(new SqlParameter("@CorrBranchUseOc", 1));
            }

            if (settings.PriceGroupID.HasValue)
            {
                if (settings.PriceGroupID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CorrPriceGroupNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CorrPriceGroupId", settings.PriceGroupID));
                }
            }
            else if (settings.UseOriginatingCompanyPriceGroup)
            {
                parameters.Add(new SqlParameter("@CorrPriceGroupUseOc", 1));
            }

            if (settings.ManagerEmployeeID.HasValue)
            {
                if (settings.ManagerEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CorrManagerEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CorrManagerEmployeeId", settings.ManagerEmployeeID));
                }
            }
            else if (settings.ProcessorEmployeeID.HasValue)
            {
                if (settings.ProcessorEmployeeID == Guid.Empty)
                {
                parameters.Add(new SqlParameter("@CorrProcessorEmployeeNone", 1));
                }
                else
                {
                parameters.Add(new SqlParameter("@CorrProcessorEmployeeId", settings.ProcessorEmployeeID));
                }
            }
            else if (settings.JuniorProcessorEmployeeID.HasValue)
            {
                if (settings.JuniorProcessorEmployeeID == Guid.Empty)
                {
                parameters.Add(new SqlParameter("@CorrJuniorProcessorEmployeeNone", 1));
                }
                else
                {
                parameters.Add(new SqlParameter("@CorrJuniorProcessorEmployeeId", settings.JuniorProcessorEmployeeID));
                }
            }
            else if (settings.LenderAccountExecEmployeeID.HasValue)
            {
                if (settings.LenderAccountExecEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CorrLenderAccExecEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CorrLenderAccExecEmployeeId", settings.LenderAccountExecEmployeeID));
                }
            }
            else if (settings.UnderwriterEmployeeID.HasValue)
            {
                if (settings.UnderwriterEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CorrUnderwriterEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CorrUnderwriterEmployeeId", settings.UnderwriterEmployeeID));
                }
            }
            else if (settings.JuniorUnderwriterEmployeeID.HasValue)
            {
                if (settings.JuniorUnderwriterEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CorrJuniorUnderwriterEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CorrJuniorUnderwriterEmployeeId", settings.JuniorUnderwriterEmployeeID));
                }
            }
            else if (settings.CreditAuditorEmployeeID.HasValue)
            {
                if (settings.CreditAuditorEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CorrCreditAuditorEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CorrCreditAuditorEmployeeId", settings.CreditAuditorEmployeeID));
                }
            }
            else if (settings.LegalAuditorEmployeeID.HasValue)
            {
                if (settings.LegalAuditorEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CorrLegalAuditorEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CorrLegalAuditorEmployeeId", settings.LegalAuditorEmployeeID));
                }
            }
            else if (settings.LockDeskEmployeeID.HasValue)
            {
                if (settings.LockDeskEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CorrLockDeskEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CorrLockDeskEmployeeId", settings.LockDeskEmployeeID));
                }
            }
            else if (settings.PurchaserEmployeeID.HasValue)
            {
                if (settings.PurchaserEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CorrPurchaserEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CorrPurchaserEmployeeId", settings.PurchaserEmployeeID));
                }
            }
            else if (settings.SecondaryEmployeeID.HasValue)
            {
                if (settings.SecondaryEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@CorrSecondaryEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@CorrSecondaryEmployeeId", settings.SecondaryEmployeeID));
                }
            }
        }

        private void AddMiniCorrRelationshipSearchParameters(
            RelationshipSearchSetting settings,
            List<SqlParameter> parameters)
        {
            if (settings.BranchID.HasValue && settings.BranchID != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@MiniCorrBranchId", settings.BranchID));
            }
            else if (settings.UseOriginatingCompanyBranch)
            {
                parameters.Add(new SqlParameter("@MiniCorrBranchUseOc", 1));
            }

            if (settings.PriceGroupID.HasValue)
            {
                if (settings.PriceGroupID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@MiniCorrPriceGroupNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@MiniCorrPriceGroupId", settings.PriceGroupID));
                }
            }
            else if (settings.UseOriginatingCompanyPriceGroup)
            {
                parameters.Add(new SqlParameter("@MiniCorrPriceGroupUseOc", 1));
            }

            if (settings.ManagerEmployeeID.HasValue)
            {
                if (settings.ManagerEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@MiniCorrManagerEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@MiniCorrManagerEmployeeId", settings.ManagerEmployeeID));
                }
            }
            else if (settings.ProcessorEmployeeID.HasValue)
            {
                if (settings.ProcessorEmployeeID == Guid.Empty)
                {
                parameters.Add(new SqlParameter("@MiniCorrProcessorEmployeeNone", 1));
                }
                else
                {
                parameters.Add(new SqlParameter("@MiniCorrProcessorEmployeeId", settings.ProcessorEmployeeID));
                }
            }
            else if (settings.JuniorProcessorEmployeeID.HasValue)
            {
                if (settings.JuniorProcessorEmployeeID == Guid.Empty)
                {
                parameters.Add(new SqlParameter("@MiniCorrJuniorProcessorEmployeeNone", 1));
                }
                else
                {
                parameters.Add(new SqlParameter("@MiniCorrJuniorProcessorEmployeeId", settings.JuniorProcessorEmployeeID));
                }
            }
            else if (settings.LenderAccountExecEmployeeID.HasValue)
            {
                if (settings.LenderAccountExecEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@MiniCorrLenderAccExecEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@MiniCorrLenderAccExecEmployeeId", settings.LenderAccountExecEmployeeID));
                }
            }
            else if (settings.UnderwriterEmployeeID.HasValue)
            {
                if (settings.UnderwriterEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@MiniCorrUnderwriterEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@MiniCorrUnderwriterEmployeeId", settings.UnderwriterEmployeeID));
                }
            }
            else if (settings.JuniorUnderwriterEmployeeID.HasValue)
            {
                if (settings.JuniorUnderwriterEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@MiniCorrJuniorUnderwriterEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@MiniCorrJuniorUnderwriterEmployeeId", settings.JuniorUnderwriterEmployeeID));
                }
            }
            else if (settings.CreditAuditorEmployeeID.HasValue)
            {
                if (settings.CreditAuditorEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@MiniCorrCreditAuditorEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@MiniCorrCreditAuditorEmployeeId", settings.CreditAuditorEmployeeID));
                }
            }
            else if (settings.LegalAuditorEmployeeID.HasValue)
            {
                if (settings.LegalAuditorEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@MiniCorrLegalAuditorEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@MiniCorrLegalAuditorEmployeeId", settings.LegalAuditorEmployeeID));
                }
            }
            else if (settings.LockDeskEmployeeID.HasValue)
            {
                if (settings.LockDeskEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@MiniCorrLockDeskEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@MiniCorrLockDeskEmployeeId", settings.LockDeskEmployeeID));
                }
            }
            else if (settings.PurchaserEmployeeID.HasValue)
            {
                if (settings.PurchaserEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@MiniCorrPurchaserEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@MiniCorrPurchaserEmployeeId", settings.PurchaserEmployeeID));
                }
            }
            else if (settings.SecondaryEmployeeID.HasValue)
            {
                if (settings.SecondaryEmployeeID == Guid.Empty)
                {
                    parameters.Add(new SqlParameter("@MiniCorrSecondaryEmployeeNone", 1));
                }
                else
                {
                    parameters.Add(new SqlParameter("@MiniCorrSecondaryEmployeeId", settings.SecondaryEmployeeID));
                }
            }
        }

        private void AddRelationshipSearchParameters(
            RelationshipSearchSetting settings,
            List<SqlParameter> parameters,
            OriginatingCompanyChannelMode mode)
        {
            switch(mode)
            {
                case OriginatingCompanyChannelMode.Broker:
                    this.AddBrokerRelationshipSearchParameters(
                        settings,
                        parameters);
                    break;
                case OriginatingCompanyChannelMode.Correspondent:
                    this.AddCorrRelationshipSearchParameters(
                        settings,
                        parameters);
                    break;
                case OriginatingCompanyChannelMode.MiniCorrespondent:
                    this.AddMiniCorrRelationshipSearchParameters(
                        settings,
                        parameters);
                    break;
                default:
                    throw new UnhandledEnumException(mode);
            }
        }

		protected void PageLoad(object sender, System.EventArgs e)
		{
			//Test for access permission
            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) && CurrentUser.HasPermission(Permission.CanAdministrateExternalUsers))
			{
				m_AccessAllowedPanel.Visible = true;
				m_AccessDeniedPanel.Visible = false;
			}
			else
			{
				m_AccessAllowedPanel.Visible = false;
				m_AccessDeniedPanel.Visible = true;
				return;
			}

			if(IsPostBack == false)
			{
				m_isTooManyUsers = true;
				ClientScript.RegisterHiddenField("sName", "");
				ClientScript.RegisterHiddenField("sView", "");
				ClientScript.RegisterHiddenField("sRelationship", "");
				ClientScript.RegisterHiddenField("sSupervisorFilter", "");
				ClientScript.RegisterHiddenField("sDate", "");
				ClientScript.RegisterHiddenField("sBeforeAfter", "");
				ClientScript.RegisterHiddenField("sStatus", "");
                ClientScript.RegisterHiddenField("PmlBrokerId", "");
				return;
			}
			string sNameFilter = Request.Form["sName"];
			string sView = Request.Form["sView"];

            DateTime dt;
            bool bCanParse = DateTime.TryParse(Request.Form["sDate"], out dt);
            string sDateFilter = bCanParse ? Request.Form["sDate"] : string.Empty;           
            
			string sBeforeAfterFilter = Request.Form["sBeforeAfter"];
			string sStatusFilter = Request.Form["sStatus"];
			string sRelationship = Request.Form["sRelationship"];
			bool supervisorRequest = false;
			try
			{
				supervisorRequest =  bool.Parse(Request.Form["sSupervisorRequest"]);
			}
			catch{}

            var modeStr = Request.Form["mode"];
            OriginatingCompanyChannelMode mode = OriginatingCompanyChannelMode.Broker;
            if (!string.IsNullOrEmpty(modeStr))
            {
                mode = (OriginatingCompanyChannelMode)Enum.Parse(
                    typeof(OriginatingCompanyChannelMode),
                    modeStr);
            }

			Guid sSupervisorFilter = Guid.Empty;

            Guid pmlBrokerId = Guid.Empty;
            try
            {
                pmlBrokerId = new Guid(Request.Form["PmlBrokerId"]);
            }
            catch { }
			
            bool supervisorNone = false;

			bool sActiveBool = true;
			
            var relationshipSearchSetting = this.GetRelationshipSearchSetting(sRelationship);

            if (!string.IsNullOrEmpty(Request.Form["sBranch"]))
            {
                if (Request.Form["sBranch"].Equals("OC"))
                {
                    relationshipSearchSetting.UseOriginatingCompanyBranch = true;
                }
                else
                {
                    relationshipSearchSetting.BranchID = new Guid(Request.Form["sBranch"]);
                }
            }

            if (!string.IsNullOrEmpty(Request.Form["sPriceGroupId"]))
            {
                if (Request.Form["sPriceGroupId"].Equals("None"))
                {
                    relationshipSearchSetting.PriceGroupID = Guid.Empty;
                }
                else if (Request.Form["sPriceGroupId"].Equals("OC"))
                {
                    relationshipSearchSetting.UseOriginatingCompanyPriceGroup = true;
                }
                else
                {
                    var priceGroupId = new Guid(Request.Form["sPriceGroupId"]);
                    if (priceGroupId != Guid.Empty)
                    {
                        relationshipSearchSetting.PriceGroupID = priceGroupId;
                    }
                }
            }

			try
			{
				string request = Request.Form["sSupervisorFilter"];
				//Handle the case where "None" was selected and the search button pressed
				if(request.Equals("None"))
				{
					supervisorNone = true;
					supervisorRequest = true;
				}
				else
				{	
					//Handle the case where a specific supervisor was chosen
					if(!request.Equals(Guid.Empty.ToString()))
					{
						sSupervisorFilter = new Guid(request);
						supervisorNone = false;
						supervisorRequest = false;
					}
					else
					{
						//Handle the case where the Any choice was chosen and the search button pressed
						if(supervisorRequest == false)
						{
							sSupervisorFilter = new Guid(request);
							supervisorNone = false;
							supervisorRequest = false;
						}
						else 
						{
							//Handle the case where "None" had been chosen previously, but instead of
							//pressing the search button to refresh the search, they clicked to change views
							supervisorNone = true;
							supervisorRequest = false;
						}
					}
				}
			}
			catch {}

			ClientScript.RegisterHiddenField("sName", sNameFilter);
			ClientScript.RegisterHiddenField("sView", sView);
			ClientScript.RegisterHiddenField("sRelationship", sRelationship);
			ClientScript.RegisterHiddenField("sSupervisorFilter", sSupervisorFilter.ToString());
			ClientScript.RegisterHiddenField("sDate", sDateFilter);
			ClientScript.RegisterHiddenField("sBeforeAfter", sBeforeAfterFilter);
			ClientScript.RegisterHiddenField("sStatus", sStatusFilter);
			ClientScript.RegisterHiddenField("sSupervisorRequest", supervisorNone.ToString());
            ClientScript.RegisterHiddenField("PmlBrokerId", pmlBrokerId.ToString());

            BrokerDB brokerDB = BrokerDB.RetrieveById(CurrentUser.BrokerId);

            m_isTooManyUsers = brokerDB.HasManyUsers && (null == sNameFilter || sNameFilter.TrimWhitespaceAndBOM() == "")
                && (null == sStatusFilter || sStatusFilter.TrimWhitespaceAndBOM() == "") && (null == sDateFilter || sDateFilter.TrimWhitespaceAndBOM() == "")
                && sSupervisorFilter == Guid.Empty && !supervisorNone
                && relationshipSearchSetting.IsEmpty;

			if (m_isTooManyUsers) 
			{
				m_dg.Visible = false;
				return; // No need to render datagrid.
			}

			List<SqlParameter> parameters = new List<SqlParameter>();
			parameters.Add( new SqlParameter("@BrokerId", CurrentUser.BrokerId));
			if (null != sNameFilter && sNameFilter.TrimWhitespaceAndBOM() != "") 
			{
				string[] parts = Utilities.EscapeForSqlLike(sNameFilter).Split(' '); //opm 26066 av 11 20 08
				if (parts.Length > 1 && parts[1] != "") 
					parameters.Add(new SqlParameter("@LastFilter", parts[1]));
				if (parts[0] != "")
					parameters.Add( new SqlParameter("@NameFilter", parts[0]));
			}

            if (supervisorNone)
            {
                parameters.Add(new SqlParameter("@PmlExternalManagerEmployeeNone", 1));
            }
            if (sSupervisorFilter != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@PmlExternalManagerEmployeeId", sSupervisorFilter));
            }

            this.AddRelationshipSearchParameters(
                relationshipSearchSetting,
                parameters,
                mode);
			
			// 08/21/06 mf OPM 7053 We limit user results based on a site constant
			
			if ( LendersOffice.Constants.ConstSite.LimitUserSearchResults == false )
				parameters.Add(new SqlParameter( "@LimitResultSet", false ) );
			if (null != sDateFilter && sDateFilter.TrimWhitespaceAndBOM() != "") 
				parameters.Add( new SqlParameter("@LastLoginDate", sDateFilter));
			if (null != sBeforeAfterFilter && sBeforeAfterFilter.TrimWhitespaceAndBOM() != "") 
				parameters.Add( new SqlParameter("@BeforeAfter", sBeforeAfterFilter));
			if (null != sStatusFilter && sStatusFilter.TrimWhitespaceAndBOM() != "") 
			{
				sActiveBool = (sStatusFilter.Equals("active"))?true:false;
			}

            if (pmlBrokerId != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@PmlBrokerId", pmlBrokerId));
            }

            m_dataTable = TpoUserSearchUtils.SearchTpoUsers(CurrentUser.BrokerId, sActiveBool, parameters);
            m_totalCount = m_dataTable.Rows.Count;
			m_dg.DataSource = m_dataTable.DefaultView;
			m_dg.DataBind();

            // Used to disable/enable button in frame parent.
            if (m_dataTable == null || m_dataTable.Rows.Count == 0)
            {
                m_enableDisableText.Text = "DISABLE";
            }
            else
            {
                m_enableDisableText.Text = "ENABLE";
            }

            DisplayView(sView, mode);
		}

        private void DisplayBrokerView(string sView)
        {
			if (sView == "relationships") 
			{
				m_dg.Columns[CHECKBOX].Visible = true;
				m_dg.Columns[EDIT].Visible = true;
				m_dg.Columns[LOGIN_NAME].Visible = true;
				m_dg.Columns[USER_NAME].Visible = true;
				m_dg.Columns[BROKER_ACCOUNT_EXECUTIVE].Visible = true;
				m_dg.Columns[BROKER_LOCK_DESK].Visible = true;
				m_dg.Columns[BROKER_UNDERWRITER].Visible = true;
				m_dg.Columns[BROKER_JUNIOR_UNDERWRITER].Visible = true;
				m_dg.Columns[BROKER_MANAGER].Visible = true;
				m_dg.Columns[BROKER_PROCESSOR].Visible = true;
				m_dg.Columns[BROKER_JUNIOR_PROCESSOR].Visible = true;
				m_dg.Columns[COMPANY].Visible = true;

			} 
			else if (sView == "supervisors") 
			{
				m_dg.Columns[CHECKBOX].Visible = true;
				m_dg.Columns[EDIT].Visible = true;
				m_dg.Columns[LOGIN_NAME].Visible = true;
				m_dg.Columns[USER_NAME].Visible = true;
				m_dg.Columns[IS_SUPERVISOR].Visible = true;
				m_dg.Columns[SUPERVISOR_NAME].Visible = true;
				m_dg.Columns[COMPANY].Visible = true;
				m_dg.Columns[STATUS].Visible = true;

			} 
			else 
			{
				// Default to users view
				m_dg.Columns[CHECKBOX].Visible = true;
				m_dg.Columns[EDIT].Visible = true;
				m_dg.Columns[LOGIN_NAME].Visible = true;
				m_dg.Columns[USER_NAME].Visible = true;
				m_dg.Columns[PML_OPTIONS].Visible = true;
				m_dg.Columns[BROKER_ACCOUNT_EXECUTIVE].Visible = true;
				m_dg.Columns[COMPANY].Visible = true;
                m_dg.Columns[ROLES].Visible = true;
				m_dg.Columns[BROKER_PRICE_GROUP].Visible = true;
				m_dg.Columns[STATUS].Visible = true;
				m_dg.Columns[LAST_LOGIN].Visible = true;
				m_dg.Columns[BROKER_BRANCH].Visible = true;
			}
        }

        private void DisplayMiniCorrespondentView(string sView)
        {
			if (sView == "relationships") 
			{
				m_dg.Columns[CHECKBOX].Visible = true;
				m_dg.Columns[EDIT].Visible = true;
				m_dg.Columns[LOGIN_NAME].Visible = true;
				m_dg.Columns[USER_NAME].Visible = true;
				m_dg.Columns[MINI_CORR_ACCOUNT_EXECUTIVE].Visible = true;
				m_dg.Columns[MINI_CORR_LOCK_DESK].Visible = true;
				m_dg.Columns[MINI_CORR_UNDERWRITER].Visible = true;
				m_dg.Columns[MINI_CORR_JUNIOR_UNDERWRITER].Visible = true;
                m_dg.Columns[MINI_CORR_CREDIT_AUDITOR].Visible = true;
                m_dg.Columns[MINI_CORR_LEGAL_AUDITOR].Visible = true;
				m_dg.Columns[MINI_CORR_MANAGER].Visible = true;
				m_dg.Columns[MINI_CORR_PROCESSOR].Visible = true;
				m_dg.Columns[MINI_CORR_JUNIOR_PROCESSOR].Visible = true;
                m_dg.Columns[MINI_CORR_PURCHASER].Visible = true;
                m_dg.Columns[MINI_CORR_SECONDARY].Visible = true;
				m_dg.Columns[COMPANY].Visible = true;

			} 
			else if (sView == "supervisors") 
			{
				m_dg.Columns[CHECKBOX].Visible = true;
				m_dg.Columns[EDIT].Visible = true;
				m_dg.Columns[LOGIN_NAME].Visible = true;
				m_dg.Columns[USER_NAME].Visible = true;
				m_dg.Columns[IS_SUPERVISOR].Visible = true;
				m_dg.Columns[SUPERVISOR_NAME].Visible = true;
				m_dg.Columns[COMPANY].Visible = true;
				m_dg.Columns[STATUS].Visible = true;

			} 
			else 
			{
				// Default to users view
				m_dg.Columns[CHECKBOX].Visible = true;
				m_dg.Columns[EDIT].Visible = true;
				m_dg.Columns[LOGIN_NAME].Visible = true;
				m_dg.Columns[USER_NAME].Visible = true;
				m_dg.Columns[PML_OPTIONS].Visible = true;
				m_dg.Columns[MINI_CORR_ACCOUNT_EXECUTIVE].Visible = true;
				m_dg.Columns[COMPANY].Visible = true;
                m_dg.Columns[ROLES].Visible = true;
				m_dg.Columns[MINI_CORR_PRICE_GROUP].Visible = true;
				m_dg.Columns[STATUS].Visible = true;
				m_dg.Columns[LAST_LOGIN].Visible = true;
				m_dg.Columns[MINI_CORR_BRANCH].Visible = true;
			}
        }

        private void DisplayCorrespondentView(string sView)
        {
			if (sView == "relationships") 
			{
				m_dg.Columns[CHECKBOX].Visible = true;
				m_dg.Columns[EDIT].Visible = true;
				m_dg.Columns[LOGIN_NAME].Visible = true;
				m_dg.Columns[USER_NAME].Visible = true;
				m_dg.Columns[CORR_ACCOUNT_EXECUTIVE].Visible = true;
				m_dg.Columns[CORR_LOCK_DESK].Visible = true;
				m_dg.Columns[CORR_UNDERWRITER].Visible = true;
				m_dg.Columns[CORR_JUNIOR_UNDERWRITER].Visible = true;
                m_dg.Columns[CORR_CREDIT_AUDITOR].Visible = true;
                m_dg.Columns[CORR_LEGAL_AUDITOR].Visible = true;
				m_dg.Columns[CORR_MANAGER].Visible = true;
				m_dg.Columns[CORR_PROCESSOR].Visible = true;
				m_dg.Columns[CORR_JUNIOR_PROCESSOR].Visible = true;
                m_dg.Columns[CORR_PURCHASER].Visible = true;
                m_dg.Columns[CORR_SECONDARY].Visible = true;
				m_dg.Columns[COMPANY].Visible = true;

			} 
			else if (sView == "supervisors") 
			{
				m_dg.Columns[CHECKBOX].Visible = true;
				m_dg.Columns[EDIT].Visible = true;
				m_dg.Columns[LOGIN_NAME].Visible = true;
				m_dg.Columns[USER_NAME].Visible = true;
				m_dg.Columns[IS_SUPERVISOR].Visible = true;
				m_dg.Columns[SUPERVISOR_NAME].Visible = true;
				m_dg.Columns[COMPANY].Visible = true;
				m_dg.Columns[STATUS].Visible = true;

			} 
			else 
			{
				// Default to users view
				m_dg.Columns[CHECKBOX].Visible = true;
				m_dg.Columns[EDIT].Visible = true;
				m_dg.Columns[LOGIN_NAME].Visible = true;
				m_dg.Columns[USER_NAME].Visible = true;
				m_dg.Columns[PML_OPTIONS].Visible = true;
				m_dg.Columns[CORR_ACCOUNT_EXECUTIVE].Visible = true;
				m_dg.Columns[COMPANY].Visible = true;
                m_dg.Columns[ROLES].Visible = true;
				m_dg.Columns[CORR_PRICE_GROUP].Visible = true;
				m_dg.Columns[STATUS].Visible = true;
				m_dg.Columns[LAST_LOGIN].Visible = true;
				m_dg.Columns[CORR_BRANCH].Visible = true;
			}
        }

		private void DisplayView(string sView, OriginatingCompanyChannelMode mode) 
		{
			foreach( DataGridColumn dC in m_dg.Columns )
			{
				dC.Visible = false;
			}

            switch (mode)
            {
                case OriginatingCompanyChannelMode.Broker:
                    this.DisplayBrokerView(sView);
                    break;
                case OriginatingCompanyChannelMode.MiniCorrespondent:
                    this.DisplayMiniCorrespondentView(sView);
                    break;
                case OriginatingCompanyChannelMode.Correspondent:
                    this.DisplayCorrespondentView(sView);
                    break;
                default:
                    throw new UnhandledEnumException(mode);
            }

			// 07/14/06 mf - OPM 6364
			m_tooManyResults.Visible = (m_dg.Items.Count == 100 );

			// 08/27/08 ck - OPM 21952. If no results, show message
			m_zeroResults.Visible = (m_dg.Items.Count == 0);			
		}

		protected void PageInit(object sender, EventArgs e) 
		{
			this.RegisterService("main", "/los/BrokerAdmin/PmlUserListResultsService.aspx");
            this.RegisterJsScript("LQBPopup.js");
            this.EnableJqueryMigrate = false;
		}

		// 08/05/08 ck - OPM 21952. Exporting search results only instead of the whole list
		protected void exportSearchResults(object sender, System.EventArgs e)
		{
            string csv = TpoUserSearchUtils.ExportTpoUsersToCsv(CurrentUser.BrokerId, m_dataTable);
			
			Response.Output.Write( csv );
			// Mark this page as a document we can download.
			Response.AddHeader( "Content-Disposition" , "attachment; filename=\"SearchResults.csv\"" );
			Response.ContentType = "application/csv";
			// Close up shop and return.
            Response.Flush();
			Response.End();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion
	}

    public class RelationshipSearchSetting
    {
        public Guid? LenderAccountExecEmployeeID { get; set; }
        public Guid? UnderwriterEmployeeID { get; set; }
        public Guid? JuniorUnderwriterEmployeeID { get; set; }
        public Guid? LockDeskEmployeeID { get; set; }
        public Guid? ManagerEmployeeID { get; set; }
        public Guid? ProcessorEmployeeID { get; set; }
        public Guid? JuniorProcessorEmployeeID { get; set; }
        public Guid? CreditAuditorEmployeeID { get; set; }
        public Guid? LegalAuditorEmployeeID { get; set; }
        public Guid? PurchaserEmployeeID { get; set; }
        public Guid? SecondaryEmployeeID { get; set; }
        public Guid? PriceGroupID { get; set; }
        public bool UseOriginatingCompanyPriceGroup { get; set; }
        public Guid? BranchID { get; set; }
        public bool UseOriginatingCompanyBranch { get; set; }

        public bool IsEmpty
        {
            get
            {
                return this.LenderAccountExecEmployeeID == null &&
                    this.UnderwriterEmployeeID == null &&
                    this.JuniorUnderwriterEmployeeID == null &&
                    this.LockDeskEmployeeID == null &&
                    this.ManagerEmployeeID == null &&
                    this.ProcessorEmployeeID == null &&
                    this.JuniorProcessorEmployeeID == null &&
                    this.CreditAuditorEmployeeID == null &&
                    this.LegalAuditorEmployeeID == null &&
                    this.PurchaserEmployeeID == null &&
                    this.SecondaryEmployeeID == null &&
                    this.PriceGroupID == null && !this.UseOriginatingCompanyPriceGroup &&
                    this.BranchID == null && !this.UseOriginatingCompanyBranch;
            }
        }
    }
}
