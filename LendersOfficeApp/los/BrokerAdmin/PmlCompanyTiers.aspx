<%@ Page language="c#" Codebehind="PmlCompanyTiers.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.BrokerAdmin.PmlCompanyTiers" %>
<%@ Register TagPrefix="ML" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ML" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head runat="server">
		<title>Originating Company Tiers</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</head>
	<body MS_POSITIONING="FlowLayout" scroll="no" onload="onInit();">
	<script>
		function onInit() { try
		{
		    var feedBack = document.getElementById("m_feedBack");
		    var errorMessage = document.getElementById("m_errorMessage");
		    var commandToDo = document.getElementById("m_commandToDo");

			if( errorMessage != null )
			{
				alert( errorMessage.value );
			}

			if( feedBack != null )
			{
				alert( feedBack.value );
			}

			if( commandToDo != null )
			{
				if( commandToDo.value == "Close" )
				{
					onClosePopup();
				}
			}

			<% if( IsPostBack == false ) { %>

			resize( 480 , 480 );

			<% } %>
		}
		catch( e )
		{
			window.status = e.message;
		}}

		function markAsDirty()
		{
			document.getElementById("m_IsDirty").value = "Dirty";
		}

		function onClose()
		{
			if( document.getElementById("m_IsDirty").value == "Dirty" )
			{
				if( confirm( "Do you want to close without saving?" ) == false )
				{
					return;
				}
			}

			onClosePopup();
		}

		function removeClicked(id)
		{
		    var data = {
                'id': id
            };

            data = JSON.stringify(data);

            var ret = false;

            callWebMethodAsync({
                type: "POST",
                url: "PmlCompanyTiers.aspx/GetListOfPmlBrokerNamesByTierId",
                data: data,
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                async: false,
                success: function(msg) {
                    if( msg.d.length == 0 ) {
                        ret = true;
                        return;
                    }

                    // Alert user and stop postback
                    var out = "Cannot remove tier because the following Originating Companies are associated with it:\n";

                    for (var i=0; i < msg.d.length && i < 10; i++) {
                        out += "\t* " + msg.d[i] + "\n";
                    }

                    if (msg.d.length > 10)
                    {
                        out += "\t(list truncated for brevity.)\n";
                    }

                    ret = false;
                    alert(out);
                },
                error: function(msg) {
                    if (console && console.log) {
                        console.log(msg.responseText);
                    }

                    ret = false;
                }
            });

		    return ret;
		}
	</script>
        <h4 class="page-header">Originating Company Tiers</h4>
	    <form id="PmlCompanyTiers" method="post" runat="server">
			<INPUT type="hidden" id="m_IsDirty" runat="server" value="" NAME="m_IsDirty">
			<TABLE width="100%" border="0" cellspacing="2" cellpadding="3">
			<TR>
			<TD>
				<TABLE cellpadding="0" cellspacing="0" width="100%">
				<TR>
				<TD align="left" width="1%" nowrap>
					<ASP:Button id="m_Add" runat="server" style="WIDTH: 62px;" Text="Add" OnClick="AddClick">
					</ASP:Button>
				</TD>
				</TR>
				</TABLE>
				<HR>
				<DIV style="WIDTH: 100%; HEIGHT: 318px; PADDING-RIGHT: 4px; OVERFLOW-Y: scroll;">
					<ML:CommonDataGrid id="m_Grid" runat="server" cellpadding="2" width="100%" OnItemCommand="GridItemClick" OnItemDataBound="GridItemBound">
						<Columns>
							<asp:TemplateColumn HeaderText="Originating Company Tiers (alphabetical)" ItemStyle-Width="1%">
								<ItemTemplate>
									<asp:TextBox id="Item" runat="server" style="PADDING-LEFT: 4px; WIDTH: 340px; FONT: 12px arial;" onchange="markAsDirty();">
									</asp:TextBox>
									<asp:RequiredFieldValidator runat="server" ID="ItemDescriptionValidator" ControlToValidate="Item" Text="Please enter a description." ToolTip="The description must not be blank." Display="Dynamic" Enabled="true"></asp:RequiredFieldValidator>
								</ItemTemplate>
							</asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID" ItemStyle-Width="100%">
								<ItemTemplate>
									<asp:TextBox id="Id" runat="server" style="PADDING-LEFT: 4px; WIDTH: 100%; FONT: 12px arial;" ReadOnly="true">
									</asp:TextBox>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn ItemStyle-HorizontalAlign="center">
								<ItemTemplate>
									<asp:LinkButton ID="Remove" runat="server" CommandName="Remove" CausesValidation="false">
										remove
									</asp:LinkButton>
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
					</ML:CommonDataGrid>
					<ASP:Panel id="m_EmptyMessage" runat="server" Width="100%" Visible="False" EnableViewState="False">
						<DIV style="PADDING: 40px; TEXT-ALIGN: center; COLOR: dimgray; FONT: 11px arial;">
							No sources to display.
						</DIV>
					</ASP:Panel>
				</DIV>
				<HR>
				<DIV style="WIDTH: 100%; TEXT-ALIGN: right;">
					<ASP:Button id="m_Ok" runat="server" Text="OK" Width="50px" OnClick="OkClick">
					</ASP:Button>
					<INPUT type="button" value="Cancel" width="50px" onclick="onClose();">
					<ASP:Button id="m_Apply" runat="server" Text="Apply" Width="50px" OnClick="ApplyClick">
					</ASP:Button>
				</DIV>
			</TD>
			</TR>
			</TABLE>
			<ml:CModalDlg id="m_ModalDlg" runat="server">
			</ml:CModalDlg>
		</form>
	</body>
</html>
