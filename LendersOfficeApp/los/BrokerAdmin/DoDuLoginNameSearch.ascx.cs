namespace LendersOfficeApp.los.BrokerAdmin
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using LendersOffice.Common;

	/// <summary>
	///		Summary description for DoDuLoginNameSearch.
	/// </summary>
	public partial  class DoDuLoginNameSearch : System.Web.UI.UserControl
	{

		private string m_sCssClass; 
		private string m_sHideJS;
		private bool m_isDUEnabled = true;
		private bool m_isDOEnabled = true;
		private int m_zIndex; 

 
		public string CssClass 
		{
			get { return m_sCssClass; }
			set { m_sCssClass = value; }
		}
		public int ZIndex 
		{
			get { return m_zIndex; } 
			set { m_zIndex = value; }
		}
		public string HideJS
		{
			get{ return m_sHideJS; }
			set{ m_sHideJS = value.Replace("{ClientID}", ClientID);}
		}

		public string DefaultFocus 
		{
			get { return "DUDO.DefaultFocus();"; }
		}

		public bool IsDOEnabled 
		{
			get { return m_isDOEnabled; } 
			set {  m_isDOEnabled = value; }
		}

		public bool IsDUEnabled 
		{
			get { return m_isDUEnabled; } 
			set {  m_isDUEnabled = value; }
		}


	protected void PageLoad(object sender, System.EventArgs e)
	{

	}

		#region Web Form Designer generated code
	override protected void OnInit(EventArgs e)
{
	//
	// CODEGEN: This call is required by the ASP.NET Web Form Designer.
	//
	InitializeComponent();
	base.OnInit(e);
}
		
	///		Required method for Designer support - do not modify
	///		the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent()
{
	this.Load += new System.EventHandler(this.PageLoad);
}
		#endregion
}
}
