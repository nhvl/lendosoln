<%@ Control Language="c#" AutoEventWireup="false" Codebehind="DoDuLoginNameSearch.ascx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.DoDuLoginNameSearch" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<div id=<%= AspxTools.HtmlAttribute(ClientID)%> class=<%= AspxTools.HtmlAttribute(CssClass)%> <% if ( ZIndex > 0 )  { %> style="z-index: <%= AspxTools.HtmlString(ZIndex) %>" <% }%>> 
	<div class="formtableheader" style="padding: 5px; width: 100%; min"> Find Other PML Users With Matching DO/DU Login </div>
	<div style="padding: 5px" >
	<%  if ( IsDOEnabled ) {%><label for="m_searchDoLoginBox" style="font-weight: bold" >DO Login</label> <input type="text" id="m_searchDoLoginBox"  />   <% } %>
	<%  if ( IsDUEnabled ) {%><label for="m_searchDuLoginBox" style="font-weight: bold">DU Login</label> <input type="text" id="m_searchDuLoginBox"  style="margin-right: 5px"/><% } %>
	<input type="button" value="Search" id="m_search" onclick="DUDO.Search()"  onclick="DODU.Search(true)" /> 
	<div id="tbl-container" style="height: 80px;margin: 5px 0 5px 0; overflow: auto; font-size: 5em; "  >
	<table id="m_searchDODUresults" cellspacing="1" style="background-color: silver; width: 100%; text-align: left;border-collapse: seperate;"> 
		<thead> 
			<tr class="GridHeader">
				<th  style=" background-color:#999999; top: expression(document.getElementById('tbl-container').scrollTop-1);position:relative; "> Login </th> 
				<th  style="background-color:#999999;top: expression(document.getElementById('tbl-container').scrollTop-1); position:relative;"> Name </th>
				<th  style="background-color:#999999;top: expression(document.getElementById('tbl-container').scrollTop-1); position:relative;"> Status </th>
				<th  style="background-color:#999999;top: expression(document.getElementById('tbl-container').scrollTop-1); position:relative;"> Last Login </th>
				<% if ( IsDOEnabled ) { %> <th  style="background-color:#999999; top: expression(document.getElementById('tbl-container').scrollTop-1);position:relative; "> DO Login </th> <% } %>
				<% if ( IsDUEnabled ) { %> <th  style="background-color:#999999; top: expression(document.getElementById('tbl-container').scrollTop-1); position:relative;">  DU Login </th> <%} %>
			</tr>
		</thead>
	</table>
	</div>
	<div style="text-align:center" >
		<input type="button" id="m_closeLink" value="Close" onclick=<%= AspxTools.HtmlAttribute(HideJS)%>  /> 
	</div>
	</div>
</div>

<script type="text/javascript" >
	var DUDO =
	{	
		currentEmployeeID : '', 
		fields	: [ 'login', 'username', 'isActive', 'lastLogin' <% if ( IsDOEnabled ) { %> ,'doLogin'<%}%> <%  if ( IsDUEnabled ) {%>,'duLogin'<% } %>], 
		Search : function() 
		{
			var oTable = document.getElementById('m_searchDODUresults'); 
			var args = new Object(); 
			var isAlt = false;
			args['duLoginName'] =document.getElementById('m_searchDuLoginBox') != null ? document.getElementById('m_searchDuLoginBox').value : '';
			args['doLoginName'] =document.getElementById('m_searchDoLoginBox') != null ? document.getElementById('m_searchDoLoginBox').value : '';
			args['CurrentEmployeeID']  = this.currentEmployeeID; 
		
			if ( args['duLoginName'] == '' && args['doLoginName'] == '' )
			{
				this.ClearTable(oTable);
				return;
			}
			
			var result = gService.utilities.call("GetDoDuDuplicates", args);
			if (result.error) {
			    alert(result.UserMessage);
			    return false;
			}

			if ( result.value["error"] ) 
			{
				alert(result.value["error"]);
				return false;
			}
			if ( !result.value ) 
				return; 
			if ( result.value.UserCount <= 0 ) 
			{
				this.ClearTable(oTable);
				return;
			}
		
			this.ClearTable(oTable);
			
			<%-- the xml response contains a field UserCount that stores the number of results--%>
			for( var z = 0; z < result.value.UserCount; z++ ) 
			{
				if ( result.value["User"+z+"employeeID"] == this.currentEmployeeID )  continue; <%-- Ignore current user id --%> 
				oTR = oTable.insertRow( -1 ); 
				oTR.className = isAlt ? "GridItem" : "GridAlternatingItem";
				isAlt = !isAlt;
				for ( var x = 0; x < this.fields.length; x++ ) 
				{			
					oTC = oTR.insertCell(-1); 	
					oTC.innerHTML = result.value["User"+z+this.fields[x]]; 
					if ( (this.fields[x] == 'duLogin' || this.fields[x] == 'doLogin' ) && oTC.innerHTML  != '')  <%-- This adds the remove link. Wont add if the cell is empty string --%>
					{
						<%-- Need to add one to z because the header is not counted If sort is needed this function will have to change --%> 
						oTC.innerHTML += "&nbsp;<a href='#' style='font-size: 1em' onclick=\"DUDO.Remove('" + result.value["User"+z+"employeeID"] + "',"+ (z+1)+","+x+", '"+ this.fields[x]+"')\">clear</a>";
					}
				} <%-- End cell/field iteration --%> 
			} <%-- End row/user iteration --%>
		},  <%-- end function --%>
		
		Remove : function(employeeID, z,x, field) 
		{
			var args = new Object(); 
			args['EmployeeID'] = employeeID; 
			args['Field'] = field;
				
			var result = gService.utilities.call('ClearDODULoginName', args);
			if ( result.error ) 
			{
				alert( result.UserMessage ); 
			}
			else  if ( result.value && result.value.OK == "OK" )
			{ 
				
				var oTable = document.getElementById('m_searchDODUresults');
				oTable.rows[z].cells[x].innerHTML = "";
			}
			else 
			{
				alert(<%= AspxTools.JsString(LendersOffice.Common.JsMessages.GenericError)%>);
			}
		},
		ClearTable : function (oTable ) 
		{
			<%-- CLears the table --%>
			while ( oTable.rows.length > 1 ) 
			{
				oTable.deleteRow(oTable.rows.length-1);
			}
		},
		Activate : function(doid, duid)
		{
			if (  document.getElementById('m_searchDuLoginBox') != null ) 
			{
				document.getElementById('m_searchDuLoginBox').value = duid;
			}
			if (  document.getElementById('m_searchDoLoginBox') != null  ) 
			{
				document.getElementById('m_searchDoLoginBox').value = doid; 
			}
			this.Search();  
		},
		Init : function(empid) 
		{
			this.currentEmployeeID = empid; 
		}
	}
</script>