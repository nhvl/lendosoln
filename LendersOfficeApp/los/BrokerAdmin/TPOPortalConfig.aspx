﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TPOPortalConfig.aspx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.TPOPortalConfig" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!doctype html>
<html>
<head runat="server">
    <title>Originator Portal Configuration</title>
    <style type="text/css">
        .hidden {
            display: none;
        }
        body { background-color: gainsboro; padding: 20px;}
        a.editlink {  color: Blue !important; font-size: 8pt; text-decoration: underline !important; position: relative; bottom: -1px; }
        span.dynatree-node { line-height: 10pt; }
        .hidden { display: none; }
        .system { font-weight: bolder; }
        #folderAdd, #pageAdd {
            position: absolute;
            border: 1px solid black;
            position: absolute;
            margin-left: auto;
            margin-right: auto;
            width: 500px;
            left: 50%;
            transform: translate(-50%, -50%);
            padding: 15px;
            z-index: 1000;
            font-weight: bold;
            background-color: White;
        }
        div.fields label,  div.fields  input, div.fields img {
            display: inline-block;
        }
        #folderAdd div,  #pageAdd div {
            clear: both;
            margin-bottom: 5px;
            overflow: auto;
        }
        div.fields input, div.fields img {
            float: right;
        }
        #provideAccess {
            float: none;
        }
        .provideAccessHelpLink {
            vertical-align: super; 
            font-size: 80%; 
            text-decoration: none;
        }
        .provideAccessHelp {
            position: absolute;
            width: 80%;
            display: none;
            background-color: white;
            border: 1px solid black;
            padding: 3px;
            box-shadow: darkgrey 0 0 5px 1px;
        }

        .nodename {
            width: 225px;
        }

        .nodeurl {
            width: 355px;
        }

        div.btndiv input {
            float: none;
        }

        div.errorDiv, .disabled-by-config {
            color: Red;
        }

        div#scrollable {
            background-color: White;
            max-height: 400px;
            height: 400px;
            overflow-y: auto;
        }

        ul.dynatree-container {
            border: 0px !important;
        }

        .error {
            color: Red;
            font-weight: bold;
        }

        div.center, #themesTable td.center
        {
            text-align: center;
        }

        td.right
        {
            text-align: right;
        }

        div.scrollable {
            overflow-y: auto;
        }

        table.bordered {
            border: 2px solid #000;
            border-collapse: collapse;
        }
        table.bordered thead td
        {
            border-bottom: 1px solid #000;
            text-align:left;
            font-weight:bold;
        }

        table.bordered td {
            border-left: 2px solid #000;
            text-align:center
        }

        table.bordered td.left {
            text-align: left;
        }

        td.bottom-border {
            border: 1px solid #000;
        }

        #configsTable td, #themesTable td
        {
            padding-left:2px;
            padding-right:8px;
        }

        #configsTable tr.rowEven, #themesTable tr.rowEven
        {
            background-color:lightgray;
        }
        #configsTable tr.rowOdd, #themesTable tr.rowOdd
        {
            background-color:White;
        }

        .FloatNone input
        {
            float:none !important;
        }

        .ui-widget-header
        {
            background: #cccccc url(images/ui-bg_highlight-soft_75_cccccc_1x100.png) 50% 50% repeat-x !important;
            border: 1px solid #AAA !important;
        }

        .config-grid
        {
            border-collapse: collapse;
        }

        .config-grid td
        {
            padding: 5px 15px;
        }

        .config-grid td:first-child {
            width: 180px;
        }

        hr {
            text-align: left;
            width: 555px;
            border: 1px solid #999;
            margin: 0px;
        }

        tr.left-indent td {
            padding-left: 10px;
        }

        tr.left-double-indent td:first-child {
            padding-left: 26px;
        }

        tr.left-indent td:nth-child(2), tr.left-double-indent td:nth-child(2) {
            padding-left: 40px;
        }

        tr.left-double-indent td:nth-child(3) {
            padding-left: 10px;
        }

        .disabled {
            color: gray !important;
        }

        #themesTable .left
        {
            text-align: left;
        }

        #themesTable .full-width, #PipelineSettingsTable table
        {
            width: 100%;
        }

        #themesTable tbody td.two-thirds-width
        {
            width: 65%;
        }

        #themesTable tbody td.tenth-width
        {
            width: 10%;
        }

        #themesTable tbody td.mini-width
        {
            width: 5%;
        }

        table.config-table
        {
            width: 95%;
        }

        .portal-option-input {
            width: 221.5px;
        }

        .portal-option-input-wide {
            width: 644px;
        }

        .left-margin-indent {
            margin-left: 10px;
        }

        .move-arrow {
            width: 15px;
            height: 15px;
        }

        .down-arrow {
            padding-top: 3px;
        }

        .PortalDisplayName {
            vertical-align: text-bottom;
            width: 250px;
        }

        .portal-availability-container {
            padding-right: 5px;
        }

        .add-pipeline-report {
            width: 100%;
            text-align: right;
        }
    </style>
</head>
<body>
<form id="form1" runat="server">
    <script type="text/javascript">
        var taskNodeId = <%=AspxTools.JsString(LendersOffice.ObjLib.PMLNavigation.NavigationConfiguration.SystemNode.Tasks)%>;
        var conditionsNodeId = <%=AspxTools.JsString(LendersOffice.ObjLib.PMLNavigation.NavigationConfiguration.SystemNode.ConditionLink)%>;
        var defaultGuid = <%=AspxTools.JsString(Guid.Empty)%>;
        var deletedColorThemes = [];

        function validateData() {
            var args = {
                OpNonQmMsgReceivingEmail: $("#OpNonQmMsgReceivingEmail").val()
            };

            gService.TpoPortalConfigService.callAsyncSimple("ValidateData", args, function (serviceResult) {
                if (serviceResult.error) {
                    alert(serviceResult.UserMessage);
                    $("#OpNonQmMsgReceivingEmail").val("");
                    return;
                }

                if (!!serviceResult.value["ErrorMessage"]) {
                    alert(serviceResult.value["ErrorMessage"]);
                    $("#OpNonQmMsgReceivingEmail").val("");
                }
            });
        }

        function getSafeDefaultValue(el)
        {
            if (el.length === 0) {
                return defaultGuid;
            }

            var defaultValue = el.val();

            if (typeof defaultValue === "undefined") {
                return defaultGuid;
            }

            return defaultValue;
        }

        function removeDefaultOptionIfExists(id) {
            var element = $("#" + id);

            if (getSafeDefaultValue(element) !== defaultGuid) {
                var firstOption = element.find("option:first");

                if (firstOption.length > 0 && getSafeDefaultValue(firstOption) === defaultGuid) {
                    firstOption.remove();
                }
            }
        }

        function setTaskPipelineState(firstLoad) {
            if (!ML.IsUseNewTaskSystem) {
                $('input[name="UseTasks"], input[name="ShowTaskPipeline"]').prop('disabled', true);
                $('#UseTasksYes, #ShowTaskPipelineYes').prop('checked', false);
                $('#UseTasksNo, #ShowTaskPipelineNo').prop('checked', true);
            }
            else {
                var isDisabled = $('#UseTasksNo').prop('checked');

                $('tr.task-pipeline').toggleClass('disabled', isDisabled);

                var showTaskPipelineYes = $('#ShowTaskPipelineYes');
                var showTaskPipelineNo = $('#ShowTaskPipelineNo');

                showTaskPipelineYes.prop('disabled', isDisabled);
                showTaskPipelineNo.prop('disabled', isDisabled);

                if (isDisabled) {
                    showTaskPipelineYes.prop('checked', false);
                    showTaskPipelineNo.prop('checked', true);
                }
            }

            if (!firstLoad) {
                updateDynaTree(taskNodeId, showTaskPipelineNo.prop('checked'));
            }
        }

        function updateDynaTree(idToUpdate, disabled) {
            var navTree = $("#navtree").dynatree("getTree");

            navTree.visit(function(node) {
                if (node.data && node.data.Id === idToUpdate) {
                    var parent = $(node.span).parent();
                    parent.find('.disabled-by-config').toggle(disabled);
                    setDisabledAttr(parent.find('a'), disabled);
                }
            });
        }

        function openDocTypePickerPage() {
            var ele = this;
            showModal('/newlos/ElectronicDocs/DocTypePicker.aspx?brokerId=' + ML.BrokerId, null, null, null, function(result) {
                if (result && result.docTypeId && result.docTypeName) {
                    $(ele).prev().val(result.docTypeName).attr('data-doc-type-id', result.docTypeId);
                }
            }, {hideCloseButton:true});
        }

        function openCustomPdfPage() {
            showModal('/newlos/ElectronicDocs/SelectCustomPdfForm.aspx', null, null, true, function(result){
                if (result && result.FormName && result.FormId) {
                    $(this).prev().val(result.FormName).attr('data-form-id', result.FormId);
                }
            }, {context: this});
        }

        function updateFormSource(selectedValue, type) {
            var useForm = selectedValue === <%=AspxTools.JsString(DataAccess.TpoRequestFormSource.CustomPdf)%>;

            switch (type) {
                case 'Redisclosure':
                    $('#RedisclosureFormRow').toggle(useForm)
                    $('#RedisclosureUrlRow').toggle(!useForm)
                    break;
                case 'Initial Closing Disclosure':
                    $('#InitialClosingDisclosureFormRow').toggle(useForm)
                    $('#InitialClosingDisclosureUrlRow').toggle(!useForm)
                    break;
                default:
                    throw new Error('Unhandled type' + type);
            }
        }

        function onFormUrlChange() {
            var url = $(this).val();
            if (url === '') {
                return true;
            }

            return isUrlValid(url);
        }

        $(function () {
            $(".nonQmTable").toggle(ML.IsNonQmOpEnabled);
            $(".conversation-log-row").toggle(ML.IsNonQmOpEnabled && ML.EnableConversationLogForLoans);

              var folderAddDiv = $('#folderAdd'),
                        currentNode = null,
                        internalFolderNameInput = $('#internalFolderName'),
                        externalFolderNameInput = $('#externalFolderName'),
                        navTree = $("#navtree"),
                        pageAddDiv = $('#pageAdd'),
                        pageNameInternalInput = $('#pageNameInternal'),
                        pageNameExternalInput = $('#pageNameExternal'),
                        pageUrlInput = $('#pageUrl'),
                        targetLocSelect = $('#targetLoc'),
                        isEdit = false,
                        saveButtons = $('input.saveBtn'),
                        pageError = $('#pageError'),
                        folderError = $('#folderError'),
                        keyRegex = new RegExp("--key--",'g');

        function getId(){
            var id;
            callWebMethodAsync({
                type: 'POST',
                url: 'TPOPortalConfig.aspx/GenerateId',
                data : '',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function(msg) {
                    id = msg.d;
                },
                error: function() {
                    alert('Error - Cannot generate id.');
                }
            });

            return id;
        }


        function validateUrl(url){
            var result = false;
            callWebMethodAsync({
                type: 'POST',
                url: 'TPOPortalConfig.aspx/ValidateUrl',
                data : JSON.stringify({url:url}),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function(msg) {
                    result = msg.d;
                },
                error: function(msg){
                    alert('Cannot validate url.');
                }
            });
            return result;
        }

        function validateCors(url){
            var result = gService.TpoPortalConfigService.call(
                'IsCorsPageEnabled',
                { PageUri: url }
            );

            return typeof(result.value.CorsUriIsEnabled) === 'string' && result.value.CorsUriIsEnabled === 'True';
        }

        function validateSettings() {
            var errorMessage = '';
            var hasAlphaGroup = $('#AlphaTestingGroup').val() !== defaultGuid;
            var hasBetaGroup = $('#BetaTestingGroup').val() !== defaultGuid;

            var redisclosureAccess = $('#RedisclosureGroup').val();
            if (!(hasAlphaGroup && hasBetaGroup)
                && redisclosureAccess === <%=AspxTools.JsString(DataAccess.NewTpoFeatureOcGroupAccess.AlphaAndBeta)%>) {
                errorMessage += 'Please select an Alpha and Beta testing group to allow redisclosure access.\n';
            }
            else if (!hasAlphaGroup && redisclosureAccess === <%=AspxTools.JsString(DataAccess.NewTpoFeatureOcGroupAccess.Alpha)%>) {
                errorMessage += 'Please select an Alpha testing group to allow redisclosure access.\n';
            }

            var initialClosingDisclosureAccess = $('#InitialClosingDisclosureGroup').val();
            if (!(hasAlphaGroup && hasBetaGroup)
                && initialClosingDisclosureAccess === <%=AspxTools.JsString(DataAccess.NewTpoFeatureOcGroupAccess.AlphaAndBeta)%>) {
                errorMessage += 'Please select an Alpha and Beta testing group to allow initial closing disclosure access.\n';
            }
            else if (!hasAlphaGroup && initialClosingDisclosureAccess === <%=AspxTools.JsString(DataAccess.NewTpoFeatureOcGroupAccess.Alpha)%>) {
                errorMessage += 'Please select an Alpha testing group to allow initial closing disclosure access.\n';
            }

            for (var i = 0; i < OriginatingCompanyUserPipelineSettings.length; ++i) {
                var setting = OriginatingCompanyUserPipelineSettings[i];
                var hasOnePortalModeAvailabilityChecked = setting.AvailableForBrokerPortalMode ||
                    setting.AvailableForMiniCorrespondentPortalMode ||
                    setting.AvailableForCorrespondentPortalMode;

                if (!hasOnePortalModeAvailabilityChecked) {
                    errorMessage += 'Please select at least one portal mode for the availability of pipeline report \'' + setting.PortalDisplayName + '\'.\n';
                }
            }

            if (errorMessage !== '') {
                alert(errorMessage);
                return false;
            }

            return true;
        }

        function getOriginatingCompanyUserPipelineSettings() {
            var newSettings = [];

            for (var i = 0; i < OriginatingCompanyUserPipelineSettings.length; ++i) {
                var setting = OriginatingCompanyUserPipelineSettings[i];
                setting.DisplayOrder = i;
                newSettings.push(setting);
            }

            return JSON.stringify(newSettings);
        }

        $('#SaveNav').click(function(){
            if (!validateSettings()) {
                return;
            }

            var tree = navTree.dynatree('getTree').toDict();
            var params = {
                treeNavJson: JSON.stringify(tree),

                DefaultWholesaleBranchId: getSafeDefaultValue($("#DefaultWholesaleBranchId")),
                DefaultWholesalePriceGroupId: getSafeDefaultValue($("#DefaultWholesalePriceGroupId")),
                PmlLoanTemplateId: $("#PmlLoanTemplateId").val(),

                DefaultMiniCorrBranchId: getSafeDefaultValue($("#DefaultMiniCorrBranchId")),
                DefaultMiniCorrPriceGroupId: getSafeDefaultValue($("#DefaultMiniCorrPriceGroupId")),
                MiniCorrLoanTemplateId: $("#MiniCorrLoanTemplateId").val(),

                DefaultCorrBranchId: getSafeDefaultValue($("#DefaultCorrBranchId")),
                DefaultCorrPriceGroupId: getSafeDefaultValue($("#DefaultCorrPriceGroupId")),
                CorrLoanTemplateId : $("#CorrLoanTemplateId").val(),

                RetailTPOLoanTemplateId : getSafeDefaultValue($("#RetailTPOLoanTemplateId")),

                UseTasksInPortal: $('#UseTasksYes').prop('checked'),
                ShowTasksPipeline: $('#ShowTaskPipelineYes').prop('checked'),
                ShowConditionsPipeline: $('#ShowConditionPipelineYes').prop('checked'),

                DefaultColorThemeId: $('#colorThemesTableBody input[type="radio"]:checked').val(),
                DeletedThemeIdJson: JSON.stringify(deletedColorThemes),

                TpoAlphaTestGroup: $('#AlphaTestingGroup').val(),
                TpoBetaTestGroup: $('#BetaTestingGroup').val(),

                TpoRedisclosureGroupAccess: $('#RedisclosureGroup').val(),
                TpoRedisclosureFormSource: $('#RedisclosureFormSource').val(),
                TpoRedisclosureFormId: $('#RedisclosureForm').attr('data-form-id'),
                TpoRedisclosureFormUrl: $('#RedisclosureRequestFormUrl').val(),
                TpoRedisclosureRequiredDocument: $('#RedisclosureRequiredDocument').attr('data-doc-type-id'),

                TpoInitialClosingDisclosureGroupAccess: $('#InitialClosingDisclosureGroup').val(),
                TpoInitialClosingDisclosureFormSource: $('#InitialClosingDisclosureFormSource').val(),
                TpoInitialClosingDisclosureFormId: $('#InitialClosingDisclosureForm').attr('data-form-id'),
                TpoInitialClosingDisclosureFormUrl: $('#InitialClosingDisclosureRequestFormUrl').val(),
                TpoInitialClosingDisclosureRequiredDocument: $('#InitialClosingDisclosureRequiredDocument').attr('data-doc-type-id'),

                OrigPortalAntiSteeringDisclosureAccess: $('#OrigPortalAntiSteeringDisclosureAccess').val(),
                OpNonQmMsgReceivingEmail: $('#OpNonQmMsgReceivingEmail').val(),
                OpConLogCategoryId: $('#OpConLogCategoryId').val(),

                OriginatingCompanyUserPipelineSettings: getOriginatingCompanyUserPipelineSettings()
            };

            var result = gService.TpoPortalConfigService.call('SaveData', params, false/*keepQuiet*/, true/*bReturnError*/);
            if (result.error) {
                alert(result.UserMessage);
                return;
            }

            if (params.DefaultWholesaleBranchId !== defaultGuid) {
                removeDefaultOptionIfExists("<%=AspxTools.ClientId(DefaultWholesaleBranchId)%>");
            }

            if (params.DefaultWholesalePriceGroupId !== defaultGuid) {
                removeDefaultOptionIfExists("<%=AspxTools.ClientId(DefaultWholesalePriceGroupId)%>");
            }

            if (params.DefaultMiniCorrBranchId !== defaultGuid) {
                removeDefaultOptionIfExists("<%=AspxTools.ClientId(DefaultMiniCorrBranchId)%>");
            }

            if (params.DefaultMiniCorrPriceGroupId !== defaultGuid) {
                removeDefaultOptionIfExists("<%=AspxTools.ClientId(DefaultMiniCorrPriceGroupId)%>");
            }

            if (params.DefaultCorrBranchId !== defaultGuid) {
                removeDefaultOptionIfExists("<%=AspxTools.ClientId(DefaultCorrBranchId)%>");
            }

            if (params.DefaultCorrPriceGroupId !== defaultGuid) {
                removeDefaultOptionIfExists("<%=AspxTools.ClientId(DefaultCorrPriceGroupId)%>");
            }
        });
        $('#SaveFolder').click(function(){
            var internalFolderName = $.trim(internalFolderNameInput.val()),
                externalFolderName = $.trim(externalFolderNameInput.val());

            if (internalFolderName.length === 0 || externalFolderName.length === 0){
                folderError.html('Please provide both internal and external folder name.');
                return;
            }


            var node = navTree.dynatree("getTree").activateKey(currentNode);
            if (isEdit) {
                node.data.InternalName = internalFolderName;
                node.data.title = HtmlEncode(internalFolderName);
                node.data.ExternalName = externalFolderName;
                node.render();
            }
            else {
                var childNode = {
                    title : internalFolderName,
                    ExternalName : externalFolderName,
                    isFolder : true,
                    Id : getId(),
                    IsNew : true

                };
                node.addChild(childNode);
                $(node.span).parent().children('a.deletefolder').addClass('hidden');
            }
            folderAddDiv.hide();
            internalFolderNameInput.val('');
            externalFolderNameInput.val('');
            folderError.empty();
        });

        $('#SavePage').click(function(){
            var pageNameInternal = $.trim(pageNameInternalInput.val()),
                pageNameExternal = $.trim(pageNameExternalInput.val()),
                pageUrl = $.trim(pageUrlInput.val()),
                targetLoc = targetLocSelect.val(),
                provideAccess = $('#provideAccess').prop('checked'),
                
                pageAllowBrokerMode = $("#AllowBrokerMode").prop("checked"),
                pageAllowMiniCorrMode = $("#AllowMiniCorrMode").prop("checked"),
                pageAllowCorrMode = $("#AllowCorrMode").prop("checked"),
                // Add default for when retail setting is not displayed
                pageAllowRetailMode = $('#AllowRetailMode').prop('checked') || false,
                pageAllowLoanOfficer = $("#AllowLoanOfficer").prop("checked"),
                pageAllowProcessor = $("#AllowProcessor").prop("checked"),
                pageAllowSecondary = $("#AllowSecondary").prop("checked"),
                pageAllowPostCloser = $("#AllowPostCloser").prop("checked"),
                pageAllowSupervisor = $("#AllowSupervisor").prop("checked"),
                pageAllowAccountExecutive = $("#AllowAccountExecutive").prop("checked"),
                pageAllowAny = $("#AllowAny").prop("checked");

            if (pageNameInternal.length === 0 || pageNameExternal.length === 0 || pageUrl === 0){
                pageError.html('Please provide internal, external name and a url.');
                return;
            }

            var node = navTree.dynatree("getTree").activateKey(currentNode);

            if (!node.data.IsEditableSystemPage && !validateUrl(pageUrl)) {
                pageError.html('Please enter a proper url.');
                return;
            }

            if (!node.data.IsEditableSystemPage && provideAccess && !validateCors(pageUrl)) {
                pageError.html('The page origin must be https and must be enabled by LendingQB support in order to allow access to LendingQB.');
                $('#provideAccess').prop('checked', false);
                return;
            }

            if (isEdit) {
                node.data.InternalName = pageNameInternal;
                node.data.title = HtmlEncode(pageNameInternal);
                node.data.ExternalName = pageNameExternal;
                node.data.ConfigHref = pageUrl;
                node.data.ConfigTarget = targetLoc;
                node.data.ProvideSystemAccess = provideAccess;
                node.data.AllowBrokerMode = pageAllowBrokerMode;
                node.data.AllowCorrMode = pageAllowCorrMode;
                node.data.AllowMiniCorrMode = pageAllowMiniCorrMode;
                node.data.AllowRetailMode = pageAllowRetailMode;

                node.data.AllowLoanOfficer = pageAllowLoanOfficer;
                node.data.AllowProcessor = pageAllowProcessor;
                node.data.AllowSecondary = pageAllowSecondary;
                node.data.AllowPostCloser = pageAllowPostCloser;
                node.data.AllowSupervisor = pageAllowSupervisor;
                node.data.AllowAccountExecutive = pageAllowAccountExecutive;
                node.data.AllowAny = pageAllowAny;

                node.render();
            }
            else {
                var childNode = {
                    title : pageNameInternal,
                    ExternalName : pageNameExternal,
                    ConfigHref : pageUrl,
                    ConfigTarget: targetLoc,
                    ProvideSystemAccess: provideAccess,
                    isFolder : false,
                    IsSystem: false,
                    IsEditableSystemPage: false,
                    CanBeRenamed: false,
                    Id : getId(),
                    IsNew : true,
                    AllowBrokerMode: pageAllowBrokerMode,
                    AllowCorrMode: pageAllowCorrMode,
                    AllowMiniCorrMode: pageAllowMiniCorrMode,
                    AllowRetailMode: pageAllowRetailMode,
                    AllowLoanOfficer: pageAllowLoanOfficer,
                    AllowProcessor: pageAllowProcessor,
                    AllowSecondary: pageAllowSecondary,
                    AllowPostCloser: pageAllowPostCloser,
                    AllowSupervisor: pageAllowSupervisor,
                    AllowAccountExecutive: pageAllowAccountExecutive,
                    AllowAny: pageAllowAny
                };

                node.addChild(childNode);
            }
            pageError.empty();
                 pageAddDiv.hide();
                pageNameInternalInput.val('');
                pageNameExternalInput.val('');
                pageUrlInput.val('');
                targetLocSelect.val(0);

        });

        $('#CancelFolder').click(function(){
            folderAddDiv.hide();
        });
        $('#CancelPage').click(function(){
            pageAddDiv.hide();
        });
        $('#WholesaleBranchTip').click(function () {
            $("#BranchMode").text("Broker");
            showTooltip("BranchTooltipDiv");
        });
        $('#WholesalePriceGroupTip').click(function () {
            $("#PricingMode").text("Broker");
            showTooltip("PriceGroupTooltipDiv");
        });
        $('#WholesaleTemplateTip').click(function () {
            $("#PortalMode").text("Broker");
            showTooltip("TemplateTooltipDiv");
        });
        $('#MiniCorrBranchTip').click(function () {
            $("#BranchMode").text("Mini-Correspondent");
            showTooltip("BranchTooltipDiv");
        });
        $('#MiniCorrPriceGroupTip').click(function () {
            $("#PricingMode").text("Mini-Correspondent");
            showTooltip("PriceGroupTooltipDiv");
        });
        $('#MiniCorrTemplateTip').click(function () {
            $("#PortalMode").text("Mini-Correspondent");
            showTooltip("TemplateTooltipDiv");
        });
        $('#CorrBranchTip').click(function () {
            $("#BranchMode").text("Correspondent");
            showTooltip("BranchTooltipDiv");
        });
        $('#CorrPriceGroupBranchTip').click(function () {
            $("#PricingMode").text("Correspondent");
            showTooltip("PriceGroupTooltipDiv");
        });
        $('#CorrTemplateTip').click(function () {
            $("#PortalMode").text("Correspondent");
            showTooltip("TemplateTooltipDiv");
        });
        $('#RetailTemplateTip').click(function () {
            $("#PortalMode").text("Retail");
            showTooltip("TemplateTooltipDiv");
        });

        function showTooltip(divId)
        {
            // If the user clicks another tooltip link, close
            // any other dialogue popups that might be open
            var $dialogs = $("#BranchTooltipDiv,#PriceGroupTooltipDiv,#TemplateTooltipDiv");
            if ($dialogs.dialog('instance')) $dialogs.dialog('close');

            $("#" + divId).dialog({
                resizable: false,
                width: 400,
                position: {my:"left top", at:"bottom", of:$("#CorrTemplateTip").parent()}
            });
        }
        navTree.on('click', 'a.addfolder', function(ev){
            ev.stopPropagation();
            currentNode = $(this).attr('data-key');
            folderAddDiv.show();
            pageAddDiv.hide();
            internalFolderNameInput.focus();
            navTree.dynatree("getTree").activateKey(currentNode);
            saveButtons.val('Add');
            isEdit =false;
            return false;
        }).on('click', 'a.addpage', function(ev){
            ev.stopPropagation();
            currentNode = $(this).attr('data-key');
            var node = navTree.dynatree("getTree").activateKey(currentNode);
            pageAddDiv.show();
            folderAddDiv.hide();
            pageNameInternalInput.focus();
            saveButtons.val('Add');
            isEdit = false;

            pageNameInternalInput.val("").prop("disabled", false);
            pageNameExternalInput.val("").prop("disabled", false);
            pageUrlInput.val('').prop('disabled', false);

            $("#AllowBrokerMode").prop("checked", false).prop("disabled", false);
            $("#AllowCorrMode").prop("checked", false).prop("disabled", false);
            $("#AllowMiniCorrMode").prop("checked", false).prop("disabled", false);
            $('#AllowRetailMode').prop("checked", false).prop("disabled", false);

            $("#AllowLoanOfficer").prop("checked", false).prop("disabled", false);
            $("#AllowProcessor").prop("checked", false).prop("disabled", false);
            $("#AllowSecondary").prop("checked", false).prop("disabled", false);
            $("#AllowPostCloser").prop("checked", false).prop("disabled", false);
            $("#AllowSupervisor").prop("checked", false).prop("disabled", false);
            $('#provideAccess').prop({ checked: false, disabled: false });

            $(".hideForSystemPage").show();
            return false;
        }).on('click', 'a.editfolder', function(ev){
            ev.stopPropagation();
            currentNode = $(this).attr('data-key');
            var node = navTree.dynatree("getTree").activateKey(currentNode);
            pageAddDiv.hide();
            folderAddDiv.show();
            internalFolderNameInput.val(node.data.InternalName).prop("disabled", node.data.CanBeRenamed);
            externalFolderNameInput.val(node.data.ExternalName);
            isEdit = true;
            saveButtons.val('Update');

            return false;
        }).on('click', 'a.editpage', function(ev){
            ev.stopPropagation();
            currentNode = $(this).attr('data-key');
            var node = navTree.dynatree("getTree").activateKey(currentNode);

            pageAddDiv.show();
            folderAddDiv.hide();
            pageNameInternalInput.val(node.data.InternalName).prop("disabled", false);
            pageNameExternalInput.val(node.data.ExternalName).prop("disabled", false);            
            pageUrlInput.val(node.data.ConfigHref);
            isEdit = true;

            $("#AllowBrokerMode").prop("checked", node.data.AllowBrokerMode);
            $("#AllowCorrMode").prop("checked", node.data.AllowCorrMode);
            $("#AllowMiniCorrMode").prop("checked", node.data.AllowMiniCorrMode);
            $('#AllowRetailMode').prop('checked', node.data.AllowRetailMode);

            allowAnyClick(node.data.AllowAny);

            $("#AllowLoanOfficer").prop("checked", node.data.AllowLoanOfficer);
            $("#AllowProcessor").prop("checked", node.data.AllowProcessor);
            $("#AllowSecondary").prop("checked", node.data.AllowSecondary);
            $("#AllowPostCloser").prop("checked", node.data.AllowPostCloser);
            $("#AllowSupervisor").prop("checked", node.data.AllowSupervisor);
            $("#AllowAccountExecutive").prop("checked", node.data.AllowAccountExecutive);
            $("#AllowAny").prop("checked", node.data.AllowAny);
            $('#provideAccess').prop("checked", node.data.ProvideSystemAccess);
            targetLocSelect.val(node.data.ConfigTarget);

            saveButtons.val('Update');
            $(".hideForSystemPage").show();

            return false;
        }).on('click', 'a.editsystempage', function (ev) {
            ev.stopPropagation();
            currentNode = $(this).attr('data-key');
            var node = navTree.dynatree("getTree").activateKey(currentNode);

            pageAddDiv.show();
            folderAddDiv.hide();
            pageNameInternalInput.val(node.data.InternalName).prop("disabled", true);
            pageNameExternalInput.val(node.data.ExternalName).prop("disabled", !node.data.CanBeRenamed);

            pageUrlInput.val(node.data.ConfigHref);
            isEdit = true;

            $("#AllowBrokerMode").prop("checked", node.data.AllowBrokerMode);
            $("#AllowCorrMode").prop("checked", node.data.AllowCorrMode);
            $("#AllowMiniCorrMode").prop("checked", node.data.AllowMiniCorrMode);
            $('#AllowRetailMode').prop('checked', node.data.AllowRetailMode);

            allowAnyClick(node.data.AllowAny);

            $("#AllowLoanOfficer").prop("checked", node.data.AllowLoanOfficer);
            $("#AllowProcessor").prop("checked", node.data.AllowProcessor);
            $("#AllowSecondary").prop("checked", node.data.AllowSecondary);
            $("#AllowPostCloser").prop("checked", node.data.AllowPostCloser);
            $("#AllowSupervisor").prop("checked", node.data.AllowSupervisor);
            $("#AllowAccountExecutive").prop("checked", node.data.AllowAccountExecutive);
            $("#AllowAny").prop("checked", node.data.AllowAny);
            $('#provideAccess').prop({ checked: true, disabled: true });

            saveButtons.val('Update');
            $(".hideForSystemPage").hide();

            return false;        
        }).on('click', 'a.deletefolder, a.deletepage', function(ev) {
            ev.stopPropagation();
            currentNode = $(this).attr('data-key');
            var node = navTree.dynatree("getTree").activateKey(currentNode);
            var removeNode = false;
            if (node.data.isFolder) {
                if (node.data.children && node.data.children.length > 0 ){
                    removeNode = confirm('Deleting this folder will remove all children and any configured permissions. Are you sure?');
                }
                else {
                    removeNode = true;
                }
                if (removeNode && node.getParent().getChildren().length == 1) {
                    $(node.getParent().span).parent().children('a.deletefolder').removeClass('hidden');
                }
            } else {
                if (!node.data.IsNew) {
                    removeNode = confirm('Deleting this page will remove any configured permissions. Are you sure?');
                }
                else {
                removeNode = true;
                }
            }

            if (removeNode){
                node.remove();
            }
            return false;
        });
        // Attach the dynatree widget to an existing <div id="tree"> element
        // and pass the tree options as an argument to the dynatree() function:
        navTree.dynatree({
             debugLevel: 1,

            onActivate: function(node) {
            },
            onSelect: function(){
            },
            onCreate : function(node, nodeSpan) {
                var data = node.data,
                    isFolder = data.isFolder,
                    isSystem = data.IsSystem,
                    isEditableSystemPage = data.IsEditableSystemPage,
                    canBeRenamed = data.CanBeRenamed,
                    parent = $(nodeSpan).parent(),
                    level = node.getLevel();

                var isDisabledByConfig = data.IsDisabledByConfigSetting;
                // New nodes will not have this value defined and can be safely defaulted
                // to false.
                if (typeof isDisabledByConfig === 'undefined') {
                    isDisabledByConfig = data.IsDisabledByConfigSetting = false;
                }

                if (isFolder){
                    if (level < 5) {
                        $("<a href='#' class='addfolder editlink' data-key=--key-->add a subfolder</a>".replace(keyRegex,data.key)).appendTo(parent);
                    }
                    $("<a href='#' class='addpage editlink' data-key=--key-->add a page</a>".replace(keyRegex,data.key)).appendTo(parent);
                }

                if (isFolder && (!isSystem || canBeRenamed)) {
                    $("<a href='#' class='editfolder editlink' data-key=--key-->edit folder</a>".replace(keyRegex, data.key)).appendTo(parent);
                    var delFolder = $("<a href='#' class='deletefolder editlink' data-key=--key-->delete folder</a>".replace(keyRegex, data.key));
                    if ((data.children && data.children.length >0) || canBeRenamed) {
                        delFolder.addClass('hidden');
                    }
                    delFolder.appendTo(parent);
                }

                if (data.ExternalName === "Portal:") {
                    parent.hide();
                    return;
                }

                $('<span class="disabled-by-config"> - Hidden by portal configuration</span>'.replace(keyRegex, data.key)).appendTo(parent).toggle(isDisabledByConfig);
                setDisabledAttr(parent.find('a'), isDisabledByConfig);

                if (!isFolder) {
                    if (!isSystem) {
                        $("<a href='#' class='editpage editlink' data-key=--key-->edit page</a>".replace(keyRegex, data.key)).appendTo(parent);
                        $("<a href='#' class='deletepage editlink' data-key=--key-->delete page</a>".replace(keyRegex, data.key)).appendTo(parent);
                    }
                    else if (isEditableSystemPage || canBeRenamed) {
                        $("<a href='#' class='editsystempage editlink' data-key=--key-->edit page</a>".replace(keyRegex, data.key)).appendTo(parent);
                    }
                }
            },
            onRender: function(node, nodeSpan) {
                var data = node.data,
                    id = data.Id,
                    parent = $(nodeSpan).parent();

                var isDisabled = id === taskNodeId ? $('#ShowTaskPipelineNo').prop('checked')
                    : id === conditionsNodeId ? $('#ShowConditionPipelineNo').prop('checked')
                    : node.data.IsDisabledByConfigSetting;

                parent.find('.disabled-by-config').toggle(isDisabled);
                setDisabledAttr(parent.find('a'), isDisabled);
            },
            persist: false,
            children: navConfig,
               dnd: {
            onDragStart: function(node) {
                /** This function MUST be defined to enable dragging for the tree.
                 *  Return false to cancel dragging of node.
                 */

                return true;
            },
            onDragEnter: function(node, sourceNode) {
				/** sourceNode may be null for non-dynatree droppables.
				 *  Return false to disallow dropping on node. In this case
				 *  onDragOver and onDragLeave are not called.
				 *  Return 'over', 'before, or 'after' to force a hitMode.
				 *  Return ['before', 'after'] to restrict available hitModes.
				 *  Any other return value will calc the hitMode from the cursor position.
				 */
				// Prevent dropping a parent below another parent (only sort
				// nodes under the same parent)
				if(node.parent !== sourceNode.parent){
					return false;
				}
				// Don't allow dropping *over* a node (would create a child)
				return ["before", "after"];
			},

            onDrop: function(node, sourceNode, hitMode, ui, draggable) {
                /** This function MUST be defined to enable dropping of items on
                 * the tree.
                 */

                sourceNode.move(node, hitMode);
            }
        }
        }).dynatree("getRoot").visit(function(node){
            node.expand(true);
        });
    });

        $(document).ready(function() {
            setTaskPipelineState(true);
            initializeHandlers();

            addConfigsFromModelToTable(portalConfigs, '#configsTableBody', '#configRowTemplate');
            addConfigsFromModelToTable(colorThemes, '#colorThemesTableBody', '#colorThemeTemplate');

            setupPipelineSettings();

            changeTab(0);

            $("input[name='defaultColorTheme'][value='" + ML.SelectedThemeId + "']").prop('checked', true);
        });

        function allowAnyClick(anyChecked) {
            if (anyChecked === undefined) {
                anyChecked = $("#AllowAny").prop("checked");
            }

            $("#AllowLoanOfficer").prop("checked", anyChecked).prop("disabled", anyChecked);
            $("#AllowProcessor").prop("checked", anyChecked).prop("disabled", anyChecked);
            $("#AllowSecondary").prop("checked", anyChecked).prop("disabled", anyChecked);
            $("#AllowPostCloser").prop("checked", anyChecked).prop("disabled", anyChecked);
            $("#AllowSupervisor").prop("checked", anyChecked).prop("disabled", anyChecked);
            $("#AllowAccountExecutive").prop("checked", anyChecked).prop("disabled", anyChecked);
        }

        function addConfigsFromModelToTable(dataSource, tableSelector, templateSelector)
        {
            for(var i = 0; i < dataSource.length; i++)
            {
                addConfigToConfigTable(dataSource[i], tableSelector, templateSelector);
            }
        }
        function addConfigToConfigTable(config, bodySelector, templateSelector)
        {
            $(bodySelector).append($(templateSelector).tmpl(config));
        }

        function initializeHandlers()
        {
            $('#copyLink').on('click', onCopyLinkClick);
            $('#closeBtn').on('click', onCloseButtonClick);

            $(window).on('beforeunload', onBeforeUnloadCheck);

            $('#addNewConfigBtn').on('click', onAddNewLandingPageClick);

            $('input[name="UseTasks"]').on('click', function() { setTaskPipelineState(false); });
            $('input[name="ShowTaskPipeline"]').on('click', function() { updateDynaTree(taskNodeId, this.value === "No"); });
            $('input[name="ShowConditionPipeline"]').on('click', function() { updateDynaTree(conditionsNodeId, this.value === "No"); });

            $('#addNewColorThemeBtn').on('click', onAddNewColorThemeClick);

            $('#RedisclosureFormSelect, #InitialClosingDisclosureFormSelect').click(openCustomPdfPage);
            $('#RedisclosureRequiredDocumentSelect, #InitialClosingDisclosureRequiredDocumentSelect').click(openDocTypePickerPage);
            $('#RedisclosureFormSource').change(function() { updateFormSource($(this).val(), 'Redisclosure'); }).change();
            $('#InitialClosingDisclosureFormSource').change(function() { updateFormSource($(this).val(), 'Initial Closing Disclosure'); }).change();
            $('#RedisclosureRequestFormUrl, #InitialClosingDisclosureRequestFormUrl').on('blur', onFormUrlChange);
        }

        function setupPipelineSettings() {
            $('#PipelineSettingsTable').empty().append($('#PipelineSettingsTemplate').tmpl());

            var overridePipelineReportNames = $('.OverridePipelineReportName');
            var portalDisplayNames = $('.PortalDisplayName');
            var availableForBrokerPortalModes = $('.AvailableForBrokerPortalMode');
            var availableForMiniCorrespondentPortalModes = $('.AvailableForMiniCorrespondentPortalMode');
            var availableForCorrespondentPortalModes = $('.AvailableForCorrespondentPortalMode');

            for (var i = 0; i < OriginatingCompanyUserPipelineSettings.length; ++i) {
                var setting = OriginatingCompanyUserPipelineSettings[i];

                overridePipelineReportNames[i].checked = setting.OverridePipelineReportName;
                portalDisplayNames[i].disabled = !setting.OverridePipelineReportName;
                availableForBrokerPortalModes[i].checked = setting.AvailableForBrokerPortalMode;
                availableForMiniCorrespondentPortalModes[i].checked = setting.AvailableForMiniCorrespondentPortalMode;
                availableForCorrespondentPortalModes[i].checked = setting.AvailableForCorrespondentPortalMode;
            }
        }

        function onAddNewLandingPageClick()
        {
            var newConfig = new Object();
            onAddNewConfigItem('#configsTableBody', '#newConfigRowTemplate', newConfig);
            $('#newRow .nameInput').focus();
            // to get rid of the button being yellow, this must happen after being clicked.
            window.setTimeout(function(){$('#addNewConfigBtn').prop('disabled',true);}, 50);
        }

        function onAddNewColorThemeClick()
        {
            callAsyncWebMethod('AddColorTheme', {}, function (result) {
                onAddNewConfigItem('#colorThemesTableBody', '#colorThemeTemplate', result.d);
            });
        }

        function onAddNewConfigItem(bodySelector, templateSelector, templateConfig)
        {
            $(bodySelector).append($(templateSelector).tmpl(templateConfig));
        }

        function onConfigEditLinkClick(editLink)
        {
            var $row = $(editLink).closest('tr');
            $row.find('input').prop('disabled',false);
            $row.find('.configLink').toggle();
            $row.find('.nameInput').focus();
            return false;
        }
        function onNewConfigSaveLinkClick(saveLink)
        {
            var $row = $(saveLink).closest('tr');
            var newConfig = new Object();

            newConfig.Name = $row.find('.nameInput').val();
            newConfig.Url = $row.find('.urlInput').val();

            if(!isConfigTentativelyValid(newConfig.Name, newConfig.Url, null))
            {
                return false;
            }

            var result = gService.TpoPortalConfigService.call('AddConfig', newConfig);
            if(result.error == true)
            {
                alert(result.UserMessage);
            }
            else if(result.value.Succeeded == "True")
            {
                newConfig.ID = result.value.ID;
                portalConfigs.push(newConfig);
                $row.remove();
                addConfigToConfigTable(newConfig, '#configsTableBody', '#configRowTemplate');
                $('#addNewConfigBtn').prop('disabled', false);
            }
            else
            {
                alert(result.value.ReasonFailed);
            }
            return false;
        }
        function isConfigTentativelyValid(newName, url, id)
        {
            if(!isConfigNameUnique(newName, id))
            {
                alert('A configuration with that name already exists.');
                return false;
            }
            if(newName === '')
            {
                alert('Configuration name may not be empty.');
                return false;
            }
            if (newName.length > configNameLengthLimit)
            {
                alert('Configuration name may not exceed ' + configNameLengthLimit + 'characters.');
                return false;
            }

            if(url === '')
            {
                alert('Configuration url may not be empty.');
                return false;
            }

            return isUrlValid(url);
        }
        function isConfigNameUnique(newName, id)
        {
            for(var i = 0; i < portalConfigs.length; i++)
            {
                if(portalConfigs[i].Name == newName)
                {
                    if(portalConfigs[i].ID != id)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        function isUrlValid(url) {
            if(url.toLowerCase().indexOf('https') != 0)
            {
                alert('Configuration url must be secure (https)');
                return false;
            }
            if(url.length > configUrlLengthLimit)
            {
                alert('Configuration url may not exceed ' + configUrlLengthLimit + ' characters.');
                return false;
            }

            return true;
        }
        function onConfigSaveLinkClick(saveLink)
        {
            var $row = $(saveLink).closest('tr');
            var configID = $row.attr('data-configID');
            var oldConfig = portalConfigByID(configID);
            var newConfig = new Object();

            newConfig.ID = oldConfig.ID;
            newConfig.Name = $row.find('.nameInput').val();
            newConfig.Url = $row.find('.urlInput').val();

            if(!isConfigTentativelyValid(newConfig.Name, newConfig.Url, newConfig.ID))
            {
                return false;
            }

            var result = gService.TpoPortalConfigService.call('UpdateConfig', newConfig);
            if(result.error == true)
            {
                alert(result.UserMessage);
            }
            else if(result.value.Succeeded == "True")
            {
                oldConfig.Name = newConfig.Name;
                oldConfig.Url = newConfig.Url;
                $row.find('.configLink').toggle();
                $row.find('input').prop('disabled', true);
            }
            else
            {
                alert(result.value.ReasonFailed);
            }
            return false;
        }
        function onConfigDeleteLinkClick(deleteLink)
        {
            var confirmResult = confirm('Any users currently assigned to this landing page' +
                ' will instead be redirected to their pipeline on login.  Still delete?');
            if(confirmResult === null || confirmResult === false)
            {
                return false;
            }

            var $row = $(deleteLink).closest('tr');
            var configID = $row.attr('data-configID');

            var args = new Object();
            args.ID = configID;


            var result = gService.TpoPortalConfigService.call('DeleteConfig', args);
            if(result.error == true)
            {
                alert(result.UserMessage);
            }
            else if(result.value.Succeeded == "True")
            {
                $row.remove();
                var oldConfigIndex = portalConfigIndexByID(configID);
                portalConfigs.splice(oldConfigIndex, 1);
            }
            else
            {
                alert(result.value.ReasonFailed);
            }
            return false;
        }
        function onNewConfigCancelLinkClick(cancelLink)
        {
            $(cancelLink).closest('tr').remove();
            $('#addNewConfigBtn').prop('disabled', false);
        }
        function onConfigCancelLinkClick(cancelLink)
        {
            var $row = $(cancelLink).closest('tr');
            var configID = $row.attr('data-configID');
            var config = portalConfigByID(configID);
            $row.find('.nameInput').val(config.Name).prop('disabled',true);
            $row.find('.urlInput').val(config.Url).prop('disabled',true);
            $row.find('.configLink').toggle();
            return false;
        }
        function portalConfigByID(id)
        {
            for(var i= 0; i < portalConfigs.length; i++)
            {
                if(portalConfigs[i].ID == id)
                {
                    return portalConfigs[i]
                }
            }
        }
        function portalConfigIndexByID(id)
        {
            for(var i= 0; i < portalConfigs.length; i++)
            {
                if(portalConfigs[i].ID == id)
                {
                    return i;
                }
            }
        }

        function onEditColorTheme(link)
        {
            var args = {
                height: 765,
                width: 910,
                hideCloseButton: true,
                onReturn: function (returnObject) {
                    $(link).parent().parent().find('label').text(returnObject.Name);
                }
            };

            var id = $(link).attr('data-theme-id');
            LQBPopup.Show(<%=AspxTools.SafeUrl(Page.ResolveUrl("~/los/BrokerAdmin/TpoColorTheme/EditTheme.aspx"))%> + "?themeid=" + id, args);
        }

        function onPreviewColorTheme(link)
        {
            var args = {
                height: 750,
                width: 900,
                hideCloseButton: true
            };

            var id = $(link).attr('data-theme-id');
            LQBPopup.Show(<%=AspxTools.SafeUrl(Page.ResolveUrl("~/los/BrokerAdmin/TpoColorTheme/PreviewTheme.aspx?quickpreview=0"))%> + "&themeid=" + id, args);
        }

        function onDeleteColorTheme(link)
        {
            if (!confirm('Are you sure you want to delete the color theme?')) {
                return;
            }

            var linkRow = $(link).closest('tr');

            if (linkRow.find('input[type="radio"]:checked').length > 0) {
                linkRow.prev('tr').find('input[type="radio"]').prop('checked', true);
            }

            linkRow.remove();

            deletedColorThemes.push($(link).attr('data-theme-id'));
        }

        function addPipelineReport() {
            var existingReportIds = OriginatingCompanyUserPipelineSettings.map(function (setting) { return setting.QueryId; });
            var args = {
                ExistingReportIds: JSON.stringify(existingReportIds)
            };

            gService.TpoPortalConfigService.callAsyncSimple('GetParamsToAddPipelineReport', args, function (serviceResult) {
                if (serviceResult.error) {
                    alert(serviceResult.UserMessage);
                    return;
                }

                var uniqueId = serviceResult.value.Id;
                var cacheKey = serviceResult.value.CacheKey;
                var url = <%=AspxTools.SafeUrl(DataAccess.Tools.VRoot + "/los/BrokerAdmin/OriginatorPortalPipelineReportPicker.aspx?k=")%> + cacheKey;
                LQBPopup.Show(url, { 
                    width:300,
                    onReturn: function (popupResult) { addPipelineReportCallback(uniqueId, popupResult); }
                });
            });
        }

        function addPipelineReportCallback(uniqueId, result) {
            if (!result.OK) {
                return;
            }

            var newSetting = createNewPipelineSetting(uniqueId, result.QueryId, result.QueryName);
            OriginatingCompanyUserPipelineSettings.push(newSetting);
            setupPipelineSettings();
        }

        function createNewPipelineSetting(uniqueId, queryId, queryName) {
            return {
                DisplayOrder: OriginatingCompanyUserPipelineSettings.length,
                BrokerId: ML.BrokerId,
                SettingId: uniqueId,
                QueryId: queryId,
                PipelineReportName: queryName,
                PortalDisplayName: queryName,
                OverridePipelineReportName: false,
                AvailableForBrokerPortalMode: true,
                AvailableForMiniCorrespondentPortalMode: true,
                AvailableForCorrespondentPortalMode: true
            };
        }

        function getPipelineSettingIndex(settingId) {
            for (var i = 0; i < OriginatingCompanyUserPipelineSettings.length; ++i) {
                if (OriginatingCompanyUserPipelineSettings[i].SettingId === settingId) {
                    return i;
                }
            }

            throw "Setting not found with ID " + settingId;
        }

        function movePipelineSettingUp(image, settingId) {
            var $row = $(image).closest('tr');
            var $prev = $row.prev();

            if ($prev.length > 0) {
                $row.insertBefore($prev);

                var index = getPipelineSettingIndex(settingId);
                swapPipelineSettings(index, index - 1);
            }
        }

        function movePipelineSettingDown(image, settingId) {
            var $row = $(image).closest('tr');
            var $next = $row.next();

            if ($next.length > 0) {
                $row.insertAfter($next);

                var index = getPipelineSettingIndex(settingId);
                swapPipelineSettings(index, index + 1);
            }
        }

        function swapPipelineSettings(swapFrom, swapTo) {
            var temp = OriginatingCompanyUserPipelineSettings[swapTo];
            OriginatingCompanyUserPipelineSettings[swapTo] = OriginatingCompanyUserPipelineSettings[swapFrom];
            OriginatingCompanyUserPipelineSettings[swapFrom] = temp;
        }

        function deletePipelineSettings(link, settingId) {
            $(link).closest('tr').remove();
            var index = getPipelineSettingIndex(settingId);
            OriginatingCompanyUserPipelineSettings.splice(index, 1);
        }

        function onOverridePipelineReportNameChanged(checkbox, settingId) {
            var checked = checkbox.checked;

            var index = getPipelineSettingIndex(settingId);
            OriginatingCompanyUserPipelineSettings[index].OverridePipelineReportName = checked;

            var portalName = $(checkbox).next('input');
            if (checked) {
                portalName.prop('disabled', false);
            }
            else {
                portalName.prop('disabled', true)
                    .val(OriginatingCompanyUserPipelineSettings[index].PipelineReportName)
                    .change();
            }
        }

        function onPortalDisplayNameChanged(input, settingId) {
            var index = getPipelineSettingIndex(settingId);
            OriginatingCompanyUserPipelineSettings[index].PortalDisplayName = input.value;
        }

        function onAvailableForBrokerPortalModeChanged(checkbox, settingId) {
            var index = getPipelineSettingIndex(settingId);
            OriginatingCompanyUserPipelineSettings[index].AvailableForBrokerPortalMode = checkbox.checked;
        }

        function onAvailableForMiniCorrespondentPortalModeChanged(checkbox, settingId) {
            var index = getPipelineSettingIndex(settingId);
            OriginatingCompanyUserPipelineSettings[index].AvailableForMiniCorrespondentPortalMode = checkbox.checked;
        }

        function onAvailableForCorrespondentPortalModeChanged(checkbox, settingId) {
            var index = getPipelineSettingIndex(settingId);
            OriginatingCompanyUserPipelineSettings[index].AvailableForCorrespondentPortalMode = checkbox.checked;
        }

        function onCopyLinkClick()
        {
            var linkedUrl = $(<%=AspxTools.JsGetElementById(m_TpoIFrameUrl) %>).val();
            copyStringToClipboard(linkedUrl);
            alert('copied ' + linkedUrl + ' to clipboard');
            return false;
        }
        function onBeforeUnloadCheck()
        {
            if(false == haveUnsavedChanges())
            {
                return;
            }

            return "It appears you may have some unsaved changes.  Still close?";
        }
        function onCloseButtonClick()
        {
            onClosePopup();
        }
        function haveUnsavedChanges()
        {
            return $('#configsTableBody > tr').has('a.saveLink:visible').is(isUnsavedRow);
        }
        function isUnsavedRow() // only use within jquery function
        {
            var $row = $(this);
            if(this.id === 'newRow')
            {
                return true;
            }
            var configID = $row.attr('data-configID');
            var config = portalConfigByID(configID);
            if(config.Name !== $row.find('.nameInput').val())
            {
                return true;
            }
            if(config.Url !== $row.find('.urlInput').val())
            {
                return true;
            }
            return false;
        }
        function promptYieldsStillClose()
        {
            var result = confirm("It appears you may have some unsaved changes.  Still close?");
            return (result ? true : false);
        }
        function callAsyncWebMethod(name, data, successCallback)
        {
            callWebMethodAsync({
                type: 'POST',
                url: 'TPOPortalConfig.aspx/' + name,
                data : JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                success: successCallback,
                error: function() {
                    alert('System error. Please contact us if this happens again.');
                }
            });
        }

        function changeTab(index)
        {           
            $(".tabnav li").removeClass("selected");
            $("#tab" + index).addClass("selected");

            $(".tabContent").hide();
            $("#tabContent" + index).show();
	    }
    </script>
    <script id="configRowTemplate" type="text/x-jquery-tmpl">
        <tr class="GridAutoItem" data-configID="${ID}">
            <td>
                <input type="text" disabled="disabled" class="nameInput" value="${Name}" style="width:100%"/>
            </td>
            <td>
                <input type="text" disabled="disabled" class="urlInput" value="${Url}" style="width:100%" />
            </td>
            <td>
                <a class="configLink" onclick="onConfigEditLinkClick(this)">edit</a>
                <a class="configLink saveLink" onclick="onConfigSaveLinkClick(this)" style="display:none">save</a>
            </td>
            <td>
                <a class="configLink" onclick="onConfigDeleteLinkClick(this);">delete</a>
                <a class="configLink" onclick="onConfigCancelLinkClick(this);" style="display:none">cancel</a>
            </td>
        </tr>
    </script>
    <script id="newConfigRowTemplate" type="text/x-jquery-tmpl">
        <tr class="GridAutoItem" id="newRow">
            <td>
                <input type="text" class="nameInput" style="width:100%"/>
            </td>
            <td>
                <input type="text" class="urlInput" style="width:100%"/>
            </td>
            <td>
                <a class="newConfigLink saveLink" onclick="onNewConfigSaveLinkClick(this)">save</a>
            </td>
            <td>
                <a class="newConfigLink" onclick="onNewConfigCancelLinkClick(this);">cancel</a>
            </td>
        </tr>
    </script>
    <script id="colorThemeTemplate" type="text/x-jquery-tmpl">
        <tr class="GridAutoItem">
            <td>
                <input type="radio" name="defaultColorTheme" value="${Id}" />
            </td>
            <td class="left">
                <label class="full-width">${Name}</label>
            </td>
            <td>
                <a onclick="onEditColorTheme(this);" data-theme-id="${Id}">edit</a>
            </td>
            <td>
                <a onclick="onPreviewColorTheme(this);" data-theme-id="${Id}">preview</a>
            </td>
            <td>
                <a onclick="onDeleteColorTheme(this);" data-theme-id="${Id}">delete</a>
            </td>
        </tr>
    </script>
    <script id="PipelineSettingsTemplate" type="text/x-jquery-tmpl">
        <table class="bordered">
            <thead>
                <tr>
                    <td>&nbsp;</td>
                    <td>Pipeline Report</td>
                    <td>Portal Display Name</td>
                    <td>Portal Mode Availability</td>
                    <td>&nbsp;</td>
                </tr>
            </thead>
            <tbody>
                {{each OriginatingCompanyUserPipelineSettings}}
                <tr class="GridAutoItem">
                    <td>
                        <img src="../../images/up-blue.gif" alt="Move Up" class="move-arrow" onclick="movePipelineSettingUp(this, '${SettingId}')" />
                        <img src="../../images/down-blue.gif" alt="Move Down" class="move-arrow down-arrow" onclick="movePipelineSettingDown(this, '${SettingId}')" />
                    </td>
                    <td class="left">
                        ${PipelineReportName}
                    </td>
                    <td class="left">
                        <input type="checkbox" class="OverridePipelineReportName" onclick="onOverridePipelineReportNameChanged(this, '${SettingId}')" />
                        <input type="text" maxlength="100" class="PortalDisplayName" value="${PortalDisplayName}" onchange="onPortalDisplayNameChanged(this, '${SettingId}')" />
                    </td>
                    <td class="portal-availability-container left">
                        <input type="checkbox" class="AvailableForBrokerPortalMode" onclick="onAvailableForBrokerPortalModeChanged(this, '${SettingId}')" />
                        Broker

                        <input type="checkbox" class="AvailableForMiniCorrespondentPortalMode" onclick="onAvailableForMiniCorrespondentPortalModeChanged(this, '${SettingId}')" />
                        Mini-Corr

                        <input type="checkbox" class="AvailableForCorrespondentPortalMode" onclick="onAvailableForCorrespondentPortalModeChanged(this, '${SettingId}')" />
                        Correspondent
                    </td>
                    <td>
                        <a onclick="deletePipelineSettings(this, '${SettingId}')">delete</a>
                    </td>
                </tr>
                {{/each}}
            </tbody>
        </table>
    </script>
    <script type="text/javascript" src="../../inc/jquery.tmpl.js"></script>

    <div>
        <table id="tpoPortalLinkTable" style="width:95%">
        <tr>
            <td style="width:20%">
                <label style="font-weight:bold">Originator Portal iFrame URL:</label>
            </td>
            <td style="width:75%">
                <asp:TextBox ReadOnly="true" ID="m_TpoIFrameUrl" Width="90%" runat="server"></asp:TextBox>
            </td>
            <td style="width:5%">
                <a href="javascript:void(0);" id="copyLink">copy</a>
            </td>
        </tr>
        </table>
    </div>

    <div class="Tabs">
        <ul class="tabnav">
            <li id="tab0" class="selected"><a onclick = "changeTab(0);">Portal Options</a></li>
            <li id="tab1"><a onclick="changeTab(1);">Landing Pages</a></li>
            <li id="tab2"><a onclick="changeTab(2);">Navigation Links</a></li>
            <li id="tab3"><a onclick="changeTab(3);">Pipelines</a></li>
            <li id="tab4"><a onclick="changeTab(4);">Default Settings</a></li>
            <li id="tab5"><a onclick="changeTab(5);">Color Themes</a></li>
        </ul>
    </div>

    <div id="tabContent0" class="tabContent">
        <br />
        <table class="config-grid">
            <tr>
                <td>Alpha Testing Group:</td>
                <td>
                    <asp:DropDownList runat="server" ID="AlphaTestingGroup"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Beta Testing Group:</td>
                <td>
                    <asp:DropDownList runat="server" ID="BetaTestingGroup"></asp:DropDownList>
                </td>
            </tr>
        </table>
        <table id="PortalOptions">
            <tr>
                <td colspan="3">
                    <label class="FieldLabel">Portal Options</label>
                </td>
            </tr>
            <tr class="left-indent">
                <td>Use Tasks in Originator Portal?</td>
                <td>
                    <input id="UseTasksYes" type="radio" name="UseTasks" runat="server" value="Yes" /> Yes
                    <input id="UseTasksNo" type="radio" name="UseTasks" runat="server" value="No" class="left-margin-indent" /> No
                </td>
            </tr>
            <tr class="task-pipeline left-double-indent">
                <td>&bull; Show pipeline of tasks for all loans?</td>
                <td>
                    <input id="ShowTaskPipelineYes" type="radio" name="ShowTaskPipeline" runat="server" value="Yes" /> Yes
                    <input id="ShowTaskPipelineNo" type="radio" name="ShowTaskPipeline" runat="server" value="No" class="left-margin-indent" /> No
                </td>
            </tr>
            <tr class="left-indent">
                <td>Show pipeline of conditions for all loans?</td>
                <td>
                    <input id="ShowConditionPipelineYes" type="radio" name="ShowConditionPipeline" runat="server" value="Yes" /> Yes
                    <input id="ShowConditionPipelineNo" type="radio" name="ShowConditionPipeline" runat="server" value="No" class="left-margin-indent" /> No
                </td>
            </tr>
            <tr class="left-indent">
                <td>Allow CoC/Redisclosure request?</td>
                <td>
                    <asp:DropDownList ID="RedisclosureGroup" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr class="left-indent">
                <td>
                    CoC/Redisclosure request form source:
                </td>
                <td>
                    <asp:DropDownList ID="RedisclosureFormSource" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr id="RedisclosureFormRow" class="left-indent">
                <td>CoC/Redisclosure request form:</td>
                <td>
                    <input runat="server" readonly="readonly" id="RedisclosureForm" class="portal-option-input" type="text" />
                    <a id="RedisclosureFormSelect" class="left-margin-indent">select</a>
                </td>
            </tr>
            <tr id="RedisclosureUrlRow" class="left-indent">
                <td>CoC/Redisclosure request form URL:</td>
                <td>
                    <input runat="server" id="RedisclosureRequestFormUrl" class="portal-option-input-wide" type="text" />
                </td>
            </tr>
            <tr class="left-indent">
                <td>CoC/Redisclosure required document:</td>
                <td>
                    <input runat="server" readonly="readonly" id="RedisclosureRequiredDocument" class="portal-option-input" type="text" />
                    <a id="RedisclosureRequiredDocumentSelect" class="left-margin-indent">select</a>
                </td>
            </tr>
            <tr class="left-indent">
                <td>Allow Initial Closing Disclosure request?</td>
                <td>
                    <asp:DropDownList ID="InitialClosingDisclosureGroup" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr class="left-indent">
                <td>
                    Initial Closing Disclosure request form source:
                </td>
                <td>
                    <asp:DropDownList ID="InitialClosingDisclosureFormSource" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr id="InitialClosingDisclosureFormRow" class="left-indent">
                <td>Initial Closing Disclosure request form:</td>
                <td>
                    <input runat="server" readonly="readonly" id="InitialClosingDisclosureForm" class="portal-option-input" type="text" />
                    <a id="InitialClosingDisclosureFormSelect" class="left-margin-indent">select</a>
                </td>
            </tr>
            <tr id="InitialClosingDisclosureUrlRow" class="left-indent">
                <td>Initial Closing Disclosure request form URL:</td>
                <td>
                    <input runat="server" id="InitialClosingDisclosureRequestFormUrl" class="portal-option-input-wide" type="text" />
                </td>
            </tr>
            <tr class="left-indent">
                <td>Initial Closing Disclosure required document:</td>
                <td>
                    <input runat="server" readonly="readonly" id="InitialClosingDisclosureRequiredDocument" class="portal-option-input" type="text" />
                    <a id="InitialClosingDisclosureRequiredDocumentSelect" class="left-margin-indent">select</a>
                </td>
            </tr>
            <tr class="left-indent">
                <td>Show anti-steering disclosure</td>
                <td>
                    <asp:DropDownList ID="OrigPortalAntiSteeringDisclosureAccess" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span class="error">***Request form URLs should be hosted on a secure site.***</span>
                </td>
            </tr>
        </table>
        <table class="nonQmTable">
            <tr>
                <td colspan="3">
                    <label class="FieldLabel">Non-QM portal</label>
                </td>
            </tr>
            <tr class="left-indent">
                <td>'Message to Lender' emailed to:</td>
                <td>
                    <asp:TextBox runat="server" ID="OpNonQmMsgReceivingEmail" onchange="validateData()"></asp:TextBox>
                </td>
            </tr>
            <tr class="left-indent conversation-log-row">
                <td>Message category for Conversation Log</td>
                <td>
                    <asp:DropDownList id="OpConLogCategoryId" runat="server"></asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>

    <div id="tabContent1" class="tabContent">
        <br />
        <label style="font-weight:bold">Landing Page Configuration</label>
        <div id="configsTableContainer" style="max-height:150px;" class="scrollable">
            <table id="configsTable" style="width:95%" class="bordered">
                <thead>
                    <tr>
                        <td style="width:25%">Landing Page Name</td>
                        <td style="width:50%">Landing Page URL</td>
                        <td style="width:12%"></td>
                        <td style="width:13%"></td>
                    </tr>
                </thead>
                <tbody id="configsTableBody">

                </tbody>
            </table>
        </div>
        <div>
            <table width="95%">
                <tr>
                <td><span class="error">***Landing page URLs should be hosted on a secure site.***</span></td>
                <td></td>
                <td></td>
                <td style="text-align:right"><input type="button" value="Add New Landing Page" id="addNewConfigBtn"/></td>
                </tr>
                <tr>
                <td>Please include the following script in the page located at above url to better support them within the Originator portal.</td>
                </tr>
                <tr>
                <td style="text-align:right"><input style="width:100%" type="text" readonly="readonly" value="https://secure.pricemyloan.com/inc/LQBNestedFrameSupport.js" /></td>
                </tr>
            </table>
        </div>
    </div>
           
    <div id="tabContent2" class="tabContent">
        <br />
        Navigation Links Configuration
        <div id="scrollable">
        <div id="navtree">

        </div>

        </div>
        <div class="errorDiv">
            ***System folders/pages, displayed in bold, cannot be modified or deleted, however user access may be configurable through other system settings. <br />
            Custom pages are not visible in the portal until the user permission to view is given. ***
        </div>

        <div id="folderAdd" class="hidden" >
            <div class="fields">
                <label for="internalFolderName">Folder Name (for internal reference)</label>
                <asp:Image runat="server" ImageUrl="~/images/require_icon_red.gif" />
                <input type="text" id="internalFolderName" name="internalFolderName" class="nodename" />
            </div>
            <div class="fields">
                <label for="externalFolderName">Folder Name (for external user facing)</label>
                <asp:Image runat="server" ImageUrl="~/images/require_icon_red.gif" />
                <input type="text" id="externalFolderName" name="externalFolderName" class="nodename" />
            </div>
            <div class="errorDiv" id="folderErro"></div>
            <div class="btndiv">
                <input type="button" id="SaveFolder" value="Add" class="saveBtn" />
                <input type="button" id="CancelFolder" value="Cancel"  />
            </div>
        </div>

        <div id="pageAdd" class="hidden">
            <div class="fields">
                <label for="pageNameInternal">Page Name (for internal reference)</label>
                <asp:Image runat="server" ImageUrl="~/images/require_icon_red.gif" class="hideForSystemPage"/>
                <input type="text" id="pageNameInternal" name="pageNameInternal"  class="nodename"/>
            </div>
            <div class="fields">
                <label for="pageNameExternal">Page Name (for external reference)</label>
                <asp:Image runat="server" ImageUrl="~/images/require_icon_red.gif" class="hideForSystemPage"/>
                <input type="text" id="pageNameExternal" name="pageNameExternal" class="nodename" />
            </div>
            <div class="fields hideForSystemPage">
                <label for="pageUrl">Page URL</label>
                <asp:Image runat="server" ImageUrl="~/images/require_icon_red.gif" />
                <input type="text" id="pageUrl" name="pageUrl" class="nodeurl" />
            </div>

            <div class="fields FloatNone">
                <table>
                    <tr>
                        <td nowrap valign="top">
                            <label for="BrokerMode">Available in which portal modes?</label>
                        </td>
                        <td>
                            <asp:CheckBox runat="server" Text="Broker" ID="AllowBrokerMode" />
                            <asp:CheckBox runat="server" Text="Mini-Correspondent" ID="AllowMiniCorrMode" />
                            <asp:CheckBox runat="server" Text="Correspondent" ID="AllowCorrMode" />
                            <asp:PlaceHolder runat="server" ID="RetailPortalModeSelector">
                                <br />
                                <asp:CheckBox runat="server" Text="Retail" ID="AllowRetailMode" />
                            </asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="fields FloatNone">
                <table>
                    <tr>
                        <td nowrap valign="top">
                            <label for="BrokerMode">Available to which user roles?</label>
                        </td>
                        <td>
                            <asp:CheckBox runat="server" Text="Loan Officer" ID="AllowLoanOfficer" />
                            <asp:CheckBox runat="server" Text="Processor" ID="AllowProcessor" />
                            <asp:CheckBox runat="server" Text="Secondary" ID="AllowSecondary" />
                            <br />
                            <asp:CheckBox runat="server" Text="Post-Closer" ID="AllowPostCloser" />
                            <asp:CheckBox runat="server" Text="Supervisor" ID="AllowSupervisor" />
                            <asp:CheckBox runat="server" Text="Account Executive" ID="AllowAccountExecutive"/>
                            <asp:CheckBox runat="server" Text="Any" ID="AllowAny" onclick="allowAnyClick()"/>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="hideForSystemPage">
                <div class="fields">
                    <label for="targetLoc">When the page link is clicked, load the page</label>

                    <select id="targetLoc" name="targetLoc">
                        <option value="0">in a new tab or window</option>
                        <option value="1">within the main portal window.</option>
                    </select>

                </div>
                <div class="fields">
                    <label class="provideAccessLabel" for="provideAccess">Allow this page to leverage LendingQB functions</label><a class="provideAccessHelpLink" onclick="jQuery('#provideAccessHelp').toggle()">[?]</a>
                    <input type="checkbox" id="provideAccess" class="provideAccessCheckbox" name="provideAccess" />
                    <div id="provideAccessHelp" class="provideAccessHelp">
                        This allows an external website to log into LendingQB and use certain system functions on behalf of your users, allowing custom embedded websites within the Originator Portal. 
                        To enable this functionality on a new page, please contact your account executive or LendingQB support for next steps.
                    </div>
                </div>
                <br />
                <div>
                    Please include the following script in the page located at above url to better support them within the Originator portal.
                    <input class="nodeurl" type="text" value="https://secure.pricemyloan.com/inc/LQBNestedFrameSupport.js" readonly="readonly" />
                </div>
                <div class="errorDiv center" id="Div1">
                    *** To avoid error messages, Page URLs should be secure***
                </div>
                <div class="errorDiv" id="pageError"></div>
            </div>
            <div class="btndiv">
                <input type="button" id="SavePage" value="Add" class="saveBtn" />
                <input type="button" id="CancelPage" value="Cancel"  />
            </div>
        </div>
    </div>

    <div id="tabContent3" class="tabContent">
        <br />
        <div class="errorDiv">
            Note: Retail users will have access within the Originator Portal to the same pipelines they have access to within LendingQB.
            The settings below are for your Originating Company users.
        </div>
        <br />

        <div class="add-pipeline-report">
            <input type="button" value="Add Pipeline Report" onclick="addPipelineReport();" />
        </div>
        <br />

        <div id="PipelineSettingsTable"></div>
    </div>

    <div id="tabContent4" class="tabContent">
        <br />
        <table class="config-grid">
            <asp:Placeholder runat="server" ID="WholesaleDefaultsPlaceholder">
            <tr>
                <td> Wholesale Branch<a href="#" id="WholesaleBranchTip">?</a></td>
                <td><asp:DropDownList runat="server" ID="DefaultWholesaleBranchId"></asp:DropDownList></td>
            </tr>
            <tr>
                <td> Wholesale Price Group<a href="#" id="WholesalePriceGroupTip">?</a></td>
                <td><asp:DropDownList runat="server" ID="DefaultWholesalePriceGroupId"></asp:DropDownList></td>
            </tr>
            </asp:Placeholder>
            <tr>
                <td> Wholesale Loan Template<a href="#" id="WholesaleTemplateTip">?</a></td>
                <td><asp:DropDownList runat="server" ID="PmlLoanTemplateId"></asp:DropDownList></td>
            </tr>
        </table>
        <hr />
        <table class="config-grid">
            <asp:Placeholder runat="server" ID="MiniCorrespondentDefaultsPlaceholder">
            <tr>
                <td> Mini-Correspondent Branch<a href="#" id="MiniCorrBranchTip">?</a></td>
                <td><asp:DropDownList runat="server" ID="DefaultMiniCorrBranchId"></asp:DropDownList></td>
            </tr>
            <tr>
                <td> Mini-Correspondent Price Group<a href="#" id="MiniCorrPriceGroupTip">?</a></td>
                <td><asp:DropDownList runat="server" ID="DefaultMiniCorrPriceGroupId"></asp:DropDownList></td>
            </tr>
            </asp:Placeholder>
            <tr>
                <td> Mini-Correspondent Loan Template<a href="#" id="MiniCorrTemplateTip">?</a></td>
                <td><asp:DropDownList runat="server" ID="MiniCorrLoanTemplateId"></asp:DropDownList></td>
            </tr>
        </table>
        <hr />
        <table class="config-grid">
            <asp:Placeholder runat="server" ID="CorrespondentDefaultsPlaceholder">
            <tr>
                <td> Correspondent Branch<a href="#" id="CorrBranchTip">?</a></td>
                <td><asp:DropDownList runat="server" ID="DefaultCorrBranchId"></asp:DropDownList></td>
            </tr>
            <tr>
                <td> Correspondent Price Group<a href="#" id="CorrPriceGroupBranchTip">?</a></td>
                <td><asp:DropDownList runat="server" ID="DefaultCorrPriceGroupId"></asp:DropDownList></td>
            </tr>
            </asp:Placeholder>
            <tr>
                <td> Correspondent Loan Template<a href="#" id="CorrTemplateTip">?</a></td>
                <td><asp:DropDownList runat="server" ID="CorrLoanTemplateId"></asp:DropDownList></td>
            </tr>
        </table>
        <asp:PlaceHolder runat="server" ID="RetailTemplatePlaceholder">
        <hr />
        <table class="config-grid">
            <tr>
                <td> Retail Template <a href="#" id="RetailTemplateTip">?</a></td>
                <td><asp:DropDownList runat="server" ID="RetailTPOLoanTemplateId"></asp:DropDownList></td>
            </tr>
        </table>
        </asp:PlaceHolder>
    </div>

    <div id="tabContent5" class="tabContent">
        <br />
        <label class="FieldLabel">Color Theme Configuration</label>
        <div id="themesContainer" class="scrollable">
            <table id="themesTable" class="bordered config-table">
                <thead>
                    <tr>
                        <td class="center">Default</td>
                        <td>Color Theme Name</td>
                        <td colspan="3" class="center">Actions</td>
                    </tr>
                </thead>
                <tbody id="colorThemesTableBody">
                    <tr class="GridAutoItem">
                        <td class="mini-width">
                            <input type="radio" name="defaultColorTheme" value="<%=AspxTools.JsStringUnquoted(this.LqbDefaultTheme.Id.ToString())%>" />
                        </td>
                        <td class="left two-thirds-width">
                            <label><%=AspxTools.JsStringUnquoted(this.LqbDefaultTheme.Name)%></label>
                        </td>
                        <td class="tenth-width">&nbsp;</td>
                        <td class="tenth-width">
                            <a onclick="onPreviewColorTheme(this)" data-theme-id="<%=AspxTools.JsStringUnquoted(this.LqbDefaultTheme.Id.ToString())%>">preview</a>
                        </td>
                        <td class="tenth-width">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            <div>
                <table class="config-table">
                    <tr>
                        <td class="right">
                            <input type="button" id="addNewColorThemeBtn" value="Add New Color Theme" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div style="text-align:right">
        <br />
        <input type="button" id="SaveNav" value="Save" />
        <input type="button" value="Close" id="closeBtn" />
    </div>

    <div id="BranchTooltipDiv" style="display:none">
        <span>Loans created within the <span id="BranchMode"></span> portal mode will be assigned to this branch unless otherwise specified at the originating company or user levels.</span>
    </div>

    <div id="PriceGroupTooltipDiv" style="display:none">
        <span>Loans priced within the <span id="PricingMode"></span> portal mode will use this price group unless otherwise specified at the originating company or user levels.</span>
    </div>

    <div id="TemplateTooltipDiv" style="display:none">
        <span>This template will be used for loans created through the <span id="PortalMode"></span> mode of the Originator Portal or loans created by your AE's.</span>
    </div>
    
</form>
</body>
</html>
