namespace LendersOfficeApp.los.BrokerAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Web.UI.WebControls;
    using System.Web.UI;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.BrokerAdmin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    public partial class PmlUserList : LendersOffice.Common.BasePage
	{
		protected bool m_IsFirstLoad = false;

		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		protected bool IsFirstLoad
		{
			set { m_IsFirstLoad = value; }
			get { return m_IsFirstLoad; }
		}

		private String ErrorMessage
		{
			set { ClientScript.RegisterHiddenField( "m_errorMessage" , value ); }
		}

		private String FeedBack
		{
			set { ClientScript.RegisterHiddenField( "m_feedBack" , value ); }
		}

		/// <summary>
		/// Get the current set of related team members.
		/// </summary>

		private void BindRelationships()
		{
			// 12/17/2004 kb - Load up all the employees -- just
			// as we do in the individual edit page.  Try to keep
			// these in sync so that the interface is consistent.
			//
			// 6/9/2005 kb - According to case 2062, and the need
			// to edit relationships in batch (more added re: case
			// 1799).
			//
			// 7/13/2005 kb - For case 2372, we need to put in a
			// load-all set and pull each role from it.
			//
			// 7/22/2005 kb - Reworked role loading to use new
			// view.  It seems that loading each role individually
			// is better than loading the whole broker when you
			// expect to have *many* loan officers because of pml.
			//
			// 05/22/06 mf - Employees can now have a relationship 
			// with procesors.  We can filter the search results
			// by the relationship roles and by PML supervisor.
			//
			// 07/17/06 mf - OPM 6365.  To increase performance, we
			// no longer use pre-populated role drop downs.  We now
			// use the "Chooser" links for each role.
			//
            // 07/12/10 vm - OPM 4548. Replaced the supervisor drop 
            // downs with role pickers.

            if( IsPostBack == false )
			{
                m_CustomPortalPageVisibilityRepeater.DataSource = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).GetPmlNavigationConfiguration(PrincipalFactory.CurrentPrincipal).GetPageNodeNames();
                m_CustomPortalPageVisibilityRepeater.DataBind();

                if (m_CustomPortalPageVisibilityRepeater.Items.Count == 0)
                {
                    ShowCustomPortalPageVisibility.Visible = false;
                }

                m_SupervisorDDL.Items.Insert(0, new ListItem("<--Keep Current-->", ""));
                m_SupervisorDDL.Items.Insert(1, new ListItem("Yes", "Self"));
                m_SupervisorDDL.Items.Insert(2, new ListItem("No", "Not"));

                m_BefAft.Items.Insert(0, new ListItem("<-- Any -->", ""));
				m_lDate.BackColor = System.Drawing.Color.Gainsboro;
				m_lDate.Enabled = false;

                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", CurrentUser.BrokerId )
                                            };

				using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( CurrentUser.BrokerId, "ListBranchByBrokerID" , parameters ) ) 
				{
					m_branchDDL.DataSource = reader;
					m_branchDDL.DataValueField = "BranchID";
					m_branchDDL.DataTextField = "Name";
					m_branchDDL.DataBind();
					if(m_branchDDL.Items.Count > 1)
					{
                        m_branchDDL.Items.Insert(0, new ListItem("<-- Use Originating Company Setting -->", "OC")); // OPM 186420
						m_branchDDL.Items.Insert(0, new ListItem("<-- Any -->", Guid.Empty.ToString())); // OPM 34490
					}
				}

				//OPM 16265 - No longer select the user's branch automatically - force them to choose instead
				//m_branchDDL.SelectedIndex = m_branchDDL.Items.IndexOf(m_branchDDL.Items.FindByValue(CurrentUser.BranchId.ToString()));
                parameters = new SqlParameter[] {
                    new SqlParameter("@BrokerID", CurrentUser.BrokerId )
                };

				using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( CurrentUser.BrokerId, "ListBranchByBrokerID" , parameters ) ) 
				{
					m_Branch.DataSource = reader;
					m_Branch.DataValueField = "BranchID";
					m_Branch.DataTextField = "Name";
					m_Branch.DataBind();
                    m_Branch.Items.Insert(0, new ListItem("<-- Use Originating Company Setting -->", "OC"));
					m_Branch.Items.Insert(0, new ListItem("<--Keep Current-->", ""));
				}
			}

			// 8/5/2005 kb - Bind the price groups (only enabled)
			// so batch update to an active group is possible.
            SqlParameter[] parameters2 = {
                                             new SqlParameter( "@BrokerId" , CurrentUser.BrokerId ),
                                             new SqlParameter( "@ExternalPriceGroupEnabled" , 1 ) 

                                         };
			using( IDataReader sR = StoredProcedureHelper.ExecuteReader(CurrentUser.BrokerId, "ListPricingGroupByBrokerId" , parameters2) )
			{
				m_LpePriceGroupId.DataTextField  = "LpePriceGroupName";
				m_LpePriceGroupId.DataValueField = "LpePriceGroupId";
				m_LpePriceGroupId.DataSource = sR;
				m_LpePriceGroupId.DataBind();
			}

            m_LpePriceGroupId.Items.Insert(0, new ListItem("<-- Use default at branch level -->", Guid.Empty.ToString()));
            m_LpePriceGroupId.Items.Insert(0, new ListItem("<-- Use Originating Company Setting -->", "OC"));
            m_LpePriceGroupId.Items.Insert(0, new ListItem("<--Keep Current-->", ""));

            m_TPOLandingPage.Items.Clear();
            m_TPOLandingPage.Items.Add(new ListItem("<--Keep Current-->", ""));
            m_TPOLandingPage.Items.Add(new ListItem("<--Use Originating Company Setting-->", "none"));
            foreach (var config in LendersOffice.ObjLib.TPO.TPOPortalConfig.RetrieveAllActiveAtBroker(CurrentUser.BrokerId))
            {
                m_TPOLandingPage.Items.Add(new ListItem(config.Name, config.ID.ToString()));
            }
            m_TPOLandingPage.SelectedValue = "";
            
            
		}

		/// <summary>
		/// Load up the pricing groups for searching.
		/// </summary>

		private void BindPriceGroups()
		{
			// 8/4/2005 kb - We now search by pricing groups (see case 2541).

			m_PriceGroups.Items.Add( new ListItem( "<-- Any -->" , Guid.Empty.ToString() ) );
            m_PriceGroups.Items.Add(new ListItem("<-- Use Originating Company Setting -->", "OC"));
            m_PriceGroups.Items.Add(new ListItem("<--Default at branch level-->", "None"));
            SqlParameter[] parameters = {
                                            new SqlParameter( "@BrokerId" , CurrentUser.BrokerId )
                                        };

			using( IDataReader sR = StoredProcedureHelper.ExecuteReader( CurrentUser.BrokerId, "ListPricingGroupByBrokerId" , parameters ) )
			{
				while( sR.Read() == true )
				{
					String groupName = ( String ) sR[ "LpePriceGroupName" ];
					Guid     groupId = ( Guid   ) sR[ "LpePriceGroupId"   ];

					if( ( Boolean ) sR[ "ExternalPriceGroupEnabled" ] == false )
						groupName += " (disabled)";

					m_PriceGroups.Items.Add( new ListItem( groupName , groupId.ToString() ) );
				}
			}
		}
        protected void m_CustomPortalPageVisibilityRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.AlternatingItem && args.Item.ItemType != ListItemType.Item)
            {
                return;
            }

            Tuple<Guid, string> permission = (Tuple<Guid, string>)args.Item.DataItem;
            Label label = (Label)args.Item.FindControl("Name");
            DropDownList option = (DropDownList)args.Item.FindControl("Option");

            option.Items.Add(new ListItem("<!-- Keep Current -->", "Current"));
            option.Items.Add(new ListItem("Make Visible", permission.Item1.ToString() + "_V"));
            option.Items.Add(new ListItem("Hide", permission.Item1.ToString() + "_H"));
            label.Text = permission.Item2;

        }
		protected void OnBatchUpdateClick( object sender , System.EventArgs a )
		{
			IsFirstLoad = false;
		}

        protected void DisplayFieldsForMode()
        {
            var correspondentFields = new List<Control>()
            {
                this.CreditAuditorPanel,
                this.LegalAuditorPanel,
                this.PurchaserPanel,
                this.SecondaryPanel,
                this.CreditAuditorBatchPanel,
                this.LegalAuditorBatchPanel,
                this.PurchaserBatchPanel,
                this.SecondaryBatchPanel,
                this.CreditAuditorSelectionRow,
                this.LegalAuditorSelectionRow,
                this.PurchaserSelectionRow,
                this.SecondarySelectionRow
            };

            var selectedMode = (OriginatingCompanyChannelMode)Enum.Parse(
                typeof(OriginatingCompanyChannelMode),
                m_Mode.SelectedValue);

            bool isCorrespondentModeSelected = selectedMode == OriginatingCompanyChannelMode.Correspondent || 
                selectedMode == OriginatingCompanyChannelMode.MiniCorrespondent;

            foreach (var control in correspondentFields)
            {
                control.Visible = isCorrespondentModeSelected;
            }

            var correspondentDDLOptions = new List<string>()
            {
                "CreditAuditor",
                "LegalAuditor",
                "Purchaser",
                "Secondary"
            };

            foreach (var value in correspondentDDLOptions)
            {
                var item = m_relationshipDdl.Items.FindByValue(value);
                item.Enabled = isCorrespondentModeSelected;
            }

            string relationshipDesc = "";
            switch (selectedMode)
            {
                case OriginatingCompanyChannelMode.Broker:
                    relationshipDesc = "Broker";
                    break;
                case OriginatingCompanyChannelMode.Correspondent:
                    relationshipDesc = "Corr.";
                    break;
                case OriginatingCompanyChannelMode.MiniCorrespondent:
                    relationshipDesc = "Mini-Corr.";
                    break;
                default:
                    throw new UnhandledEnumException(selectedMode);
            }

            this.m_RelationshipDesc.Text = relationshipDesc.ToLower();
            this.PriceGroupDesc.Text = relationshipDesc;
            this.LandingPageDesc.Text = relationshipDesc;
            this.BranchDesc.Text = relationshipDesc;
            this.RelationshipFilterDesc.Text = relationshipDesc;
            this.RelationshipViewDesc.Text = relationshipDesc;
            this.PriceGroupFilterDesc.Text = relationshipDesc;
            this.BranchFilterDesc.Text = relationshipDesc;

            this.BrokerViewCreateSettings.Visible = selectedMode == OriginatingCompanyChannelMode.Broker;
            this.MiniCorrViewCreateSettings.Visible = selectedMode == OriginatingCompanyChannelMode.MiniCorrespondent;
            this.CorrViewCreateSettings.Visible = selectedMode == OriginatingCompanyChannelMode.Correspondent;

            ClientScript.RegisterHiddenField("mode", selectedMode.ToString("d"));
        }

		/// <summary>
		/// Initialize page.
		/// </summary>
		protected void PageLoad( object sender , System.EventArgs a )
		{
			//Test for access permission
            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) && CurrentUser.HasPermission(Permission.CanAdministrateExternalUsers))
			{
				m_AccessAllowedPanel.Visible = true;
				m_AccessDeniedPanel.Visible = false;
			}
			else
			{
				m_AccessAllowedPanel.Visible = false;
				m_AccessDeniedPanel.Visible = true;
				return;
			}

            RegisterJsScript("json.js");
			// Load the pml users for edit.

			try
			{
				IsFirstLoad = RequestHelper.GetBool("isfirstload");
				
				// Delegate to the bind.  We do the same thing on
				// render.
				//
				// 7/13/2005 kb - I hate to load every time, but I
				// need the checkboxes within the grid to be in place
				// and set with the user's clicks when we handle the
				// update click.
				
				//relationships need to be bound on postback because if a user performs a batch edit and makes
				//new supervisors, they need to then show up in the supervisors drop down menus
				BindRelationships();
				if( IsPostBack == false )
					BindPriceGroups();

                this.DisplayFieldsForMode();
			}
			catch( Exception e )
			{
				Tools.LogError( "Failed to load pml user list web form." , e );
				ErrorMessage = "Failed to load web form.";
			}
		}

        private void InitYesNoDdl(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("<--Keep Current-->", (-1).ToString()));
            ddl.Items.Add(new ListItem("Yes", (1).ToString()));
            ddl.Items.Add(new ListItem("No", (0).ToString()));
        }

        private void InitIpRestrictionDdl(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("<--Keep Current-->", (-1).ToString()));
            ddl.Items.Add(new ListItem("No IP address restriction", (0).ToString()));
            ddl.Items.Add(new ListItem("Only allow whitelisted IPs or certificates", (1).ToString()));
        }

        protected void PageInit(object sender, System.EventArgs a)
        {
            RegisterJsScript("LQBPopup.js");

            InitIpRestrictionDdl(EnabledIpRestrictionDdl);
            InitYesNoDdl(EnabledClientDigitalCertificateInstallDdl);
            InitYesNoDdl(EnableAuthCodeViaSmsDdl);

            Tools.BindUsePmlUserOCDropdown(this.UseRelationshipsDdl);
            Tools.BindUsePmlUserOCDropdown(this.UsePermissionDdl);

            Tools.SetDropDownListValue(this.UseRelationshipsDdl, EmployeePopulationMethodT.IndividualUser);
            Tools.SetDropDownListValue(this.UsePermissionDdl, EmployeePopulationMethodT.IndividualUser);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
            this.EnableJqueryMigrate = false;
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

	}		
}
