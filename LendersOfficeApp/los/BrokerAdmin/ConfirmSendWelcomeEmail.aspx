<%@ Page language="c#" Codebehind="ConfirmSendWelcomeEmail.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.BrokerAdmin.ConfirmSendWelcomeEmail" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>Confirm Send Welcome Email</title>
    <LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" onload="init();">
	<script language="javascript">
	<!--
		function init() 
		{
			resize(400, 180);
		}

		function f_choice(choice) 
		{
			var args = window.dialogArguments || {};
			args.choice = choice;
			onClosePopup(args);
		}
	//-->
		</script>
    <h4 class="page-header">Confirm Send Welcome Email</h4>
    <form id="ConfirmSendWelcomeEmail" method="post" runat="server">
		<table width="100%">
			<td align="center" style="PADDING-LEFT:20px; FONT-WEIGHT:bold;COLOR:black">
					<br>
					Send LendingQB welcome email to 
					<br><ml:EncodedLabel id="m_EmailAddress" name="m_EmailAddress" runat="server"></ml:EncodedLabel>?
					<br>
					(It will not contain login or password information)
				</td>
			<tr>
				<td align="center">
					<br><input type="button" value="Yes" class="buttonstyle" onclick="f_choice(0);" style="WIDTH: 50px">
					&nbsp;<input type="button" value="No" class="buttonstyle" onclick="f_choice(1);" style="WIDTH: 50px">
				</td>
			</tr>
		</table>
     </form>
	<uc1:cModalDlg id="CModalDlg1" runat="server"></uc1:cModalDlg>
  </body>
</html>
