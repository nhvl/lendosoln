﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LendingLicenseEdit.aspx.cs" Inherits="LendersOfficeApp.Los.BrokerAdmin.LendingLicenseEdit" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">
            span{
                padding-right:10px;
                width:100px;
                display:inline-block;
            }
            .disabled
            {
                background-color:lightgray;
            }
    </style>
    <script type="text/javascript">


        function init() {
            var args = getModalArgs();

            _initInput();

            document.getElementById("LicenseNum").value = args["LicenseNum"];
            document.getElementById("State").value = args["State"];
            document.getElementById("ExpirationD").value = args["ExpD"];
            document.getElementById("DisplayName").value = args["DisplayName"];
            document.getElementById("Street").value = args["Street"];
            document.getElementById("City").value = args["City"];
            document.getElementById("AddrState").value = args["AddrState"];
            document.getElementById("Zip").value = args["Zip"];
            document.getElementById("Phone").value = args["Phone"];
            document.getElementById("Fax").value = args["Fax"];
            document.getElementById("Editable").checked = args["Editable"] == "True" || args["Editable"] == "true";

            onModifyClick();
            checkRequired();

        }
        function saveAndSerializeLicense() {
            var args = getModalArgs();
            var data = {
                    "LicenseNum": document.getElementById("LicenseNum").value,
                    "State": document.getElementById("State").value,
                    "ExpD": document.getElementById("ExpirationD").value,
                    "DisplayName": document.getElementById("DisplayName").value,
                    "Street": document.getElementById("Street").value,
                    "City": document.getElementById("City").value,
                    "AddrState": document.getElementById("AddrState").value,
                    "Zip": document.getElementById("Zip").value,
                    "Phone":  document.getElementById("Phone").value,
                    "Fax": document.getElementById("Fax").value,
                    "Editable": document.getElementById("Editable").checked,
                    "OriginalLicenseNum": args["OriginalLicenseNum"],
                    "OriginalState": args["OriginalState"],
                    "BranchID": args["BranchID"]
                };

                var results = gService.LendingLicenseEdit.call("SaveLicense", data);

                if (results.error) {
                    alert("error");
                }
            return (data);

        }

        function onOK() {
            //save the changes here
            var arg = onApply();
            onClosePopup(arg);
        }

        function onCancel() {
            onClosePopup();
        }

        function onApply() {
            //save the changes here
            var arg = window.dialogArguments || {};
            arg.OK = true;

            var serializedData = saveAndSerializeLicense();
            $j.extend(arg, serializedData);

            return arg;
        }

        function checkRequired() {
            var enabled = true;
            var elements =
            [document.getElementById("LicenseNum"),
             document.getElementById("State"),
             document.getElementById("ExpirationD"),
             document.getElementById("DisplayName"),
             document.getElementById("Street"),
             document.getElementById("City"),
             document.getElementById("AddrState"),
             document.getElementById("Phone"),
             document.getElementById("Zip"),
             document.getElementById("Fax")];
            for (var i = 0; i < elements.length; i++) {
                if (elements[i].id != "Fax") {
                    enabled = enabled && elements[i].value != "";
                }
            }
            document.getElementById("btnOk").disabled = !enabled;
            document.getElementById("btnApply").disabled = !enabled;

        }

        function onModifyClick() {
            var args = getModalArgs();
            var elements =
            [document.getElementById("DisplayName"),
             document.getElementById("Street"),
             document.getElementById("City"),
             document.getElementById("Phone"),
             document.getElementById("Zip"),
             document.getElementById("Fax")];
            var readonly = !document.getElementById("Editable").checked;
            for (var i = 0; i < elements.length; i++) {
                elements[i].readOnly = readonly;
                elements[i].className = readonly ? "disabled" : "";

                if (readonly) {
                    switch (elements[i].id) {
                        case "DisplayName":
                            elements[i].value = args["DefaultDisplayName"];
                            break;
                        case "Street":
                            elements[i].value = args["DefaultStreet"];
                            break;
                        case "City":
                            elements[i].value = args["DefaultCity"];
                            break;
                        case "Phone":
                            elements[i].value = args["DefaultPhone"];
                            break;
                        case "Zip":
                            elements[i].value = args["DefaultZip"];
                            break;
                    }
                }
            }
            var display = readonly ? "none" : "";
            var images = document.getElementsByTagName("img");
            for (var i = 0; i < images.length; i++) {
                images[i].style.display = display;
            }
            document.getElementById("AddrState").disabled = readonly;
            if (readonly) {
                document.getElementById("AddrState").value = args["DefaultAddrState"];
            }
        }

    </script>
</head>
<body onload="init();" style="background-color:Gainsboro">
    <h4 class="page-header">Edit License</h4>
    <form id="form1" runat="server">
    <div style="padding-left:10px; padding-right:10px;padding-top:30px;">
        <span class="FieldLabel">License #</span> <asp:TextBox onblur="checkRequired();"  style="width:180px;"  ID="LicenseNum" runat="server" ></asp:TextBox><img src="../../images/require_icon.gif" /> <br />
        <span class="FieldLabel">State</span> <ml:StateDropDownList onchange="checkRequired();" ID="State" runat="server"  /><img src="../../images/require_icon.gif" /> <br />
        <span class="FieldLabel">Expiration Date</span> <asp:TextBox onblur="checkRequired();"  style="width:70px;"  class="mask" preset="date" ID="ExpirationD" runat="server" ></asp:TextBox><img src="../../images/require_icon.gif" /> <br />
         <br />
        <span class="FieldLabel">Display Name</span> <asp:TextBox onblur="checkRequired();" style="width:300px;" ID="DisplayName" runat="server" ></asp:TextBox>  <img src="../../images/require_icon.gif" /> <br />
        <span class="FieldLabel">Address</span> <asp:TextBox  onblur="checkRequired();" style="width:300px;" ID="Street" runat="server" ></asp:TextBox> <img src="../../images/require_icon.gif" /> <br />
        <span class="FieldLabel">&nbsp</span> <asp:TextBox onblur="checkRequired();" style="width:185px;" ID="City" runat="server" > </asp:TextBox> <img src="../../images/require_icon.gif" /> <ml:StateDropDownList onchange="checkRequired();" ID="AddrState" runat="server"  /> <img src="../../images/require_icon.gif" /><ml:ZipcodeTextBox id="Zip"  onblur="checkRequired();"  style="PADDING-LEFT: 4px" runat="server" Width="60" CssClass="mask" preset="zipcode"></ml:ZipcodeTextBox><img src="../../images/require_icon.gif" /> <br />
        <span class="FieldLabel">Phone</span> <asp:TextBox  onblur="checkRequired();" class="mask" preset="phone" ID="Phone" runat="server" ></asp:TextBox>  <img src="../../images/require_icon.gif" /> <br />
        <span class="FieldLabel">Fax</span> <asp:TextBox class="mask" preset="phone" ID="Fax" runat="server" ></asp:TextBox> <br />
        <br />
        <div style="text-align:center">
            <asp:CheckBox onclick="onModifyClick();" ID="Editable" runat="server" /> Modify license address <br />
            <input type="button" ID="btnOk" runat="server" value="OK"  OnClick="onOK()"/>
            <input type="button"  ID="btnCancel" OnClick ="onCancel()" runat="server" value="Cancel" />
            <input type="button"  ID="btnApply" OnClick="onApply()" runat="server" value="Apply" />
        </div>
        <br />
    </div>
    </form>
</body>
</html>
