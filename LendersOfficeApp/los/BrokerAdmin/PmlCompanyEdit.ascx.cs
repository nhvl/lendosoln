namespace LendersOfficeApp.los.BrokerAdmin
{
    using System;
    using System.Threading;
    using System.Web.UI.WebControls;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.CustomPmlFields;
    using LendersOffice.ObjLib.UI.Themes;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    public partial class PmlCompanyEdit : System.Web.UI.UserControl
    {
        protected string m_ZeroDollarStr;
        private LosConvert m_losConvert = new LosConvert();

        #region variables
        protected readonly int PAGE_SIZE = ConstAppDavid.PmlCompanyListPagingSize;
        #endregion

        private String ErrorMessage
        {
            set { Page.ClientScript.RegisterHiddenField("m_errorMessage", value.TrimEnd('.') + "."); }
        }
        private Boolean IsDirty
        {
            set
            {
                Page.ClientScript.RegisterHiddenField("m_isDirty", (value) ? "1" : "0");
            }
        }
        protected string m_PopupPath
        {
            get
            {
                if ((Thread.CurrentPrincipal as AbstractUserPrincipal).Type == "I")
                {
                    return "/LOAdmin/Broker/";
                }
                else
                {
                    return "/los/admin/";
                }
            }
        }

        protected bool showRelationshipsTab
        {
            get
            {
                return BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).Opm149774TempBit;
            }
        }

        /// <summary>
        /// Initialize this control.
        /// </summary>
        protected void ControlInit(object sender, System.EventArgs a)
        {
            // Initialize this control.
            try
            {
                m_Zipcode.SmartZipcode(m_City, m_State);    // Setup the auto zip locator.
            }
            catch (Exception e)
            {
                Tools.LogError(ErrorMessage = "Failed to initialize web control.", e);  // Oops!
            }
            m_ZeroDollarStr = m_losConvert.ToMoneyString(0.0M, FormatDirection.ToRep);

            var page = (LendersOffice.Common.BasePage)this.Parent.Page;
            page.RegisterJsScript("ServiceCredential.js");
            page.RegisterCSS("ServiceCredential.css");
        }

        /// <summary>
        /// Load this control.
        /// </summary>
        protected void ControlLoad(object sender, System.EventArgs a)
        {
            if (m_OriginatorCompensationBaseT.Items.Count == 0)
            {
                Tools.Bind_PercentBaseLoanAmountsT(m_OriginatorCompensationBaseT);
                m_OriginatorCompensationBaseT.SelectedValue = E_PercentBaseT.TotalLoanAmount.ToString("D");
            }

            UnderwritingAuthority.Items.Add(new ListItem("Prior Approved", "" +(int)E_UnderwritingAuthority.PriorApproved));
            UnderwritingAuthority.Items.Add(new ListItem("Delegated", "" + (int)E_UnderwritingAuthority.Delegated));

            Tools.BindOCStatusT(BrokerStatusDDL);
            Tools.BindOCStatusT(MiniCorrStatusDDL);
            Tools.BindOCStatusT(CorrStatusDDL);

            this.BindCompanyRoleSearchItems();
            this.BindOCStatusTSearchItems(BrokerStatusSearchDDL);
            this.BindOCStatusTSearchItems(MiniCorrStatusSearchDDL);
            this.BindOCStatusTSearchItems(CorrStatusSearchDDL);

            var brokerDb = PrincipalFactory.CurrentPrincipal.BrokerDB;

            if (brokerDb.IsCustomPricingPolicyFieldEnabled)
            {
                CustomLOPricingPolicyPlaceHolder.Visible = true;
                BuildLoanOfficerPricingPolicyTable();
            }
            else
            {
                CustomLOPricingPolicyPlaceHolder.Visible = false;
            }

            if (m_TPOLandingPage.Items.Count == 0)
            {
                var configs = LendersOffice.ObjLib.TPO.TPOPortalConfig.RetrieveAllActiveAtBroker(PrincipalFactory.CurrentPrincipal.BrokerId);
                Tools.Bind_TPOLandingPages(m_TPOLandingPage, configs);
                m_TPOLandingPage.SelectedValue = "none";
            }

            if (MiniCorrespondentTPOPortalConfigID.Items.Count == 0)
            {
                var configs = LendersOffice.ObjLib.TPO.TPOPortalConfig.RetrieveAllActiveAtBroker(PrincipalFactory.CurrentPrincipal.BrokerId);
                Tools.Bind_TPOLandingPages(MiniCorrespondentTPOPortalConfigID, configs);
                MiniCorrespondentTPOPortalConfigID.SelectedValue = "none";
            }

            if (CorrespondentTPOPortalConfigID.Items.Count == 0)
            {
                var configs = LendersOffice.ObjLib.TPO.TPOPortalConfig.RetrieveAllActiveAtBroker(PrincipalFactory.CurrentPrincipal.BrokerId);
                Tools.Bind_TPOLandingPages(CorrespondentTPOPortalConfigID, configs);
                CorrespondentTPOPortalConfigID.SelectedValue = "none";
            }

            this.BindPriceGroupDropdowns(brokerDb);

            this.BindBranchDropdowns(brokerDb);

            // OPM 190542
            m_companyTier.Items.Add(new ListItem("", "0"));

            foreach (PmlCompanyTier desc in brokerDb.PmlCompanyTiers)
            {
                m_companyTier.Items.Add(new ListItem(desc.Name, desc.Id.ToString()));
            }

            this.TpoColorThemeId.Items.Add(new ListItem("<-- Use Default in Originator Portal Configuration -->", Guid.Empty.ToString()));
            this.TpoColorThemeId.Items.Add(new ListItem(SassDefaults.LqbDefaultColorTheme.Name, SassDefaults.LqbDefaultColorTheme.Id.ToString()));

            var brokerCollection = ThemeManager.GetThemeCollection(brokerDb.BrokerID, ThemeCollectionType.TpoPortal);
            foreach (var theme in brokerCollection.GetThemes())
            {
                this.TpoColorThemeId.Items.Add(new ListItem(theme.Name, theme.Id.ToString()));
            }

            int count = 0;
            foreach (Tuple<Guid, string> Tuple in brokerDb.GetPmlNavigationConfiguration(PrincipalFactory.CurrentPrincipal).GetPageNodeNames())
            {
                TableRow tr = new TableRow();
                TableCell td = new TableCell();

                System.Web.UI.HtmlControls.HtmlInputCheckBox checkbox = new System.Web.UI.HtmlControls.HtmlInputCheckBox();
                checkbox.ID = "TpoNavigationPermissions_" + count++;
                checkbox.Value = Tuple.Item1.ToString();
                checkbox.Attributes["class"] = "TpoNavigationPermissions";

                EncodedLabel lbl = new EncodedLabel();
                lbl.Text = Tuple.Item2;
                lbl.AssociatedControlID = checkbox.ID;

                td.Controls.Add(checkbox);
                td.Controls.Add(lbl);
                tr.Controls.Add(td);
                m_TpoNavigationPermissions.Controls.Add(tr);
            }
        }

        private void BindPriceGroupDropdowns(BrokerDB brokerDb)
        {
            this.SetTopEntry(m_LpePriceGroupId, brokerDb.DefaultWholesalePriceGroupId, "Wholesale", "Price Group");
            this.SetTopEntry(MiniCorrespondentLpePriceGroupId, brokerDb.DefaultMiniCorrPriceGroupId, "Mini-Correspondent", "Price Group");
            this.SetTopEntry(CorrespondentLpePriceGroupId, brokerDb.DefaultCorrPriceGroupId, "Correspondent", "Price Group");

            m_LpePriceGroupId.Items.Add(new ListItem("<-- Use Default at Branch Level -->", Guid.Empty.ToString()));
            MiniCorrespondentLpePriceGroupId.Items.Add(new ListItem("<-- Use Default at Branch Level -->", Guid.Empty.ToString()));
            CorrespondentLpePriceGroupId.Items.Add(new ListItem("<-- Use Default at Branch Level -->", Guid.Empty.ToString()));

            foreach (var priceGroupKVP in brokerDb.GetPriceGroups())
            {
                m_LpePriceGroupId.Items.Add(new ListItem(priceGroupKVP.Value, priceGroupKVP.Key.ToString()));
                MiniCorrespondentLpePriceGroupId.Items.Add(new ListItem(priceGroupKVP.Value, priceGroupKVP.Key.ToString()));
                CorrespondentLpePriceGroupId.Items.Add(new ListItem(priceGroupKVP.Value, priceGroupKVP.Key.ToString()));
            }

            m_LpePriceGroupId.DataBind();
            MiniCorrespondentLpePriceGroupId.DataBind();
            CorrespondentLpePriceGroupId.DataBind();
        }

        private void BindBranchDropdowns(BrokerDB brokerDb)
        {
            this.SetTopEntry(m_Branch, brokerDb.DefaultWholesaleBranchId, "Wholesale", "Branch");
            this.SetTopEntry(MiniCorrespondentBranchId, brokerDb.DefaultMiniCorrBranchId, "Mini-Correspondent", "Branch");
            this.SetTopEntry(CorrespondentBranchId, brokerDb.DefaultCorrBranchId, "Correspondent", "Branch");

            foreach (var branchKVP in brokerDb.GetBranches())
            {
                m_Branch.Items.Add(new ListItem(branchKVP.Value, branchKVP.Key.ToString()));
                MiniCorrespondentBranchId.Items.Add(new ListItem(branchKVP.Value, branchKVP.Key.ToString()));
                CorrespondentBranchId.Items.Add(new ListItem(branchKVP.Value, branchKVP.Key.ToString()));
            }

            m_Branch.DataBind();
            MiniCorrespondentBranchId.DataBind();
            CorrespondentBranchId.DataBind();
        }

        private void SetTopEntry(DropDownList ddl, Guid? brokerDefault, string strIfSet, string strIfNotSet)
        {
            string labelString;

            string value;

            if (brokerDefault.HasValue)
            {
                labelString = "<-- Use " + strIfSet + " Default -->";

                value = ConstApp.UseLenderLevelDefaultValue;

                // If a TPO portal-level default is specified, this dropdown is not required
                // to have a value. 
                var reqImage = this.FindControl(ddl.ID + "ReqImg");

                if (reqImage != null)
                {
                    reqImage.Visible = false;
                }
            }
            else
            {
                labelString = "<-- Select A " + strIfNotSet + " -->";

                value = string.Empty;

                var reqImage = this.FindControl(ddl.ID + "ReqImg");

                if (reqImage != null)
                {
                    reqImage.Visible = true;
                }
            }

            ddl.Items.Add(new ListItem(labelString, value));
        }

        protected void BuildLoanOfficerPricingPolicyTable()
        {
            var htmlTableRows = CustomPmlFieldUtilities.GetLoanOfficerPricingTableRows(PrincipalFactory.CurrentPrincipal.BrokerId);

            foreach (var row in htmlTableRows)
            {
                CustomLOPricingPolicyTable.Rows.Add(row);
            }
        }

        private void BindCompanyRoleSearchItems()
        {
            CompanyRoleSearchDDL.Items.Add(new ListItem("<-- Any Role -->", "ANY"));
            CompanyRoleSearchDDL.Items.Add(new ListItem("<-- No Role -->", "NA"));
            CompanyRoleSearchDDL.Items.Add(Tools.CreateEnumListItem("Broker", E_OCRoles.Broker));
            CompanyRoleSearchDDL.Items.Add(Tools.CreateEnumListItem("Mini-Correspondent", E_OCRoles.MiniCorrespondent));

            CompanyRoleSearchDDL.Items.Add(new ListItem(
                "Correspondent - Prior Approved", 
                this.GenerateCorrespondentString(E_UnderwritingAuthority.PriorApproved)));

            CompanyRoleSearchDDL.Items.Add(new ListItem(
                "Correspondent - Delegated", 
                this.GenerateCorrespondentString(E_UnderwritingAuthority.Delegated)));
        }

        private void BindOCStatusTSearchItems(DropDownList ddl)
        {
            ddl.Items.Add(new ListItem("<-- Any -->", "-1"));
            Tools.BindOCStatusT(ddl);
        }

        private string GenerateCorrespondentString(E_UnderwritingAuthority underwritingAuthority)
        {
            return E_OCRoles.Correspondent.ToString("D") + underwritingAuthority.ToString("D");
        }
            
        protected void m_OriginatorCompensationAuditGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
        }

        protected void m_OriginatorCompensationAuditGrid_ItemCreated(object sender, DataGridItemEventArgs e)
        {
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.ControlLoad);
            this.Init += new System.EventHandler(this.ControlInit);
        }
        #endregion

    }

}
