﻿<%@ Page language="c#" Codebehind="PmlCompanyList.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.BrokerAdmin.PmlCompanyList" %>
<%@ Register TagPrefix="uc" TagName="PmlCompanyEdit" Src="../../los/BrokerAdmin/PmlCompanyEdit.ascx" %>
<%@ Register TagPrefix="uc" TagName="CModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head runat="server">
		<title>PML Originating Companies</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name=vs_defaultClientScript content="JavaScript">
		<meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
	</head>
	<body MS_POSITIONING="FlowLayout" style="OVERFLOW-Y: auto; BACKGROUND-COLOR: #003366" onload="onInit();">
	<script>
		function onInit()
		{
		    var errorMessage = document.getElementById("m_errorMessage");
		    var commandToDo = document.getElementById("m_commandToDo");
		    var addUser = document.getElementById("m_addUser");
			if( errorMessage != null )
			{
				alert( errorMessage.value );
			}
			else
			if( commandToDo != null )
			{
				if( commandToDo.value == "Close" )
				{
					onClosePopup();
				}
			}
			else
			if( addUser != null )
			{
			    var sBroker = '';
			    if ( addUser.value)
			        sBroker = "&pmlBrokerId=" + addUser.value;

				showModal( "/los/BrokerAdmin/EditPmlUser.aspx?cmd=add" + sBroker, null, null, null, null,{ width:890, hideCloseButton: true });
			}

			if( window.dialogArguments != null )
			{
				<% if( IsPostBack == false ) { %>

				resize( 800 , 800 );

				<% } %>
			}
		}
	</script>
		<form id="EditBranch" method="post" runat="server">
			<table cellspacing="0" cellpadding="8" border="0" width="100%" height="100%">
			<tr>
			<td height="100%">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
				<tr>
				<td height="100%" style="BORDER: 2px outset; BACKGROUND-COLOR: gainsboro;">
					<uc:PmlCompanyEdit id="m_Edit" runat="server"></uc:PmlCompanyEdit>
				</td>
				</tr>
				</table>
			</td>
			</tr>
			</table>
			<uc:CModalDlg ID="CModalDlg1" runat="server"></uc:CModalDlg>
		</form>
	</body>
</html>
