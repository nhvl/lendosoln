﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Security;
using LendersOffice.ObjLib.ConsumerPortal;
using MeridianLink.CommonControls;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.BrokerAdmin
{
    public partial class ConsumerPortalList : LendersOffice.Common.BaseServicePage
    {
        protected bool AllowNewConsumerPortals { get; private set; }
        protected bool IsNewPMLUIEnabled { get; private set; }
        protected void PageLoad(object sender, EventArgs e)
        {
            m_ConsumerPortals.DataSource = ConsumerPortalConfig.RetrievePortalsForBroker(PrincipalFactory.CurrentPrincipal.BrokerId);
            m_ConsumerPortals.DataBind();
        }
        protected void PageInit(object sender, EventArgs e)
        {
            var broker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            IsNewPMLUIEnabled = broker.IsNewPmlUIEnabled;
            AllowNewConsumerPortals = broker.IsEDocsEnabled;
            RegisterJsScript("LQBPopup.js");	
            this.EnableJquery = true;
        }

        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingConsumerPortalConfigs
                };
            }
        }

        protected void BindConsumerPortalItem(object sender, DataGridItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
                return;

            EncodedLabel rateWatchURL = (EncodedLabel)args.Item.FindControl("RateWatchURL");
            EncodedLabel ratesLabel = (EncodedLabel)args.Item.FindControl("RatesLabel");
            EncodedLabel getRatesUrl = (EncodedLabel)args.Item.FindControl("GetRatesURL");
            EncodedLabel borrowerLoginUrl = (EncodedLabel)args.Item.FindControl("BorrowerLoginURL");
            LinkButton CopyRateWatchBtn = args.Item.FindControl("CopyRateWatchURL") as LinkButton;
            LinkButton copyGetRatesBtn = args.Item.FindControl("CopyGetRatesURL") as LinkButton;
            LinkButton copyBorrowerLoginBtn = args.Item.FindControl("CopyBorrowerLoginURL") as LinkButton;

            ConsumerPortalIdData consumerPortal = args.Item.DataItem as ConsumerPortalIdData;

            rateWatchURL.Text = consumerPortal.RateWatchUrl;
            CopyRateWatchBtn.Attributes.Add("onclick", "copyFromField(" + AspxTools.JsString(rateWatchURL.ClientID) + ");");

            if (IsNewPMLUIEnabled)
            {
                getRatesUrl.Text = consumerPortal.GetRatesUrl;
                copyGetRatesBtn.Attributes.Add("onclick", "copyFromField(" + AspxTools.JsString(getRatesUrl.ClientID) + ");");
            }
            else
            {
                copyGetRatesBtn.Visible = false;
                ratesLabel.Visible = false;
                getRatesUrl.Visible = false;
            }
            borrowerLoginUrl.Text = consumerPortal.BorrowerLoginUrl;
            copyBorrowerLoginBtn.Attributes.Add("onclick", "copyFromField(" + AspxTools.JsString(borrowerLoginUrl.ClientID) + ");");

            LinkButton config = args.Item.FindControl("ConfigureBtn") as LinkButton;
            config.Attributes.Add("onclick", "onConfigureClick(" + AspxTools.JsString(consumerPortal.Id) + ")");
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Init += new System.EventHandler(this.PageInit);
            this.Load += new System.EventHandler(this.PageLoad);

        }
        #endregion
    }
}
