﻿// <copyright file="LendingLicenseEditService.aspx.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: EduardoM
//    Date:   6/6/2014
// </summary>
namespace LendersOfficeApp.Los.BrokerAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LendersOfficeApp.los.admin;

    /// <summary>
    /// Saves the Lending license info. 
    /// </summary>
    public partial class LendingLicenseEditService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// The broker's branches.
        /// </summary>
        private BranchDescs branches = new BranchDescs();

        /// <summary>
        /// Used to call Save License.
        /// </summary>
        /// <param name="methodName">The name of the method.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                    case "SaveLicense":
                    this.SaveLicense();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Saves the license.
        /// </summary>
        protected void SaveLicense()
        {
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId )
                                        };
            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListBranchByBrokerId", parameters))
            {
                this.branches.Clear();
                string originalLicenseNum = this.GetString("OriginalLicenseNum");
                string originalState = this.GetString("OriginalState");
                Guid branchID = GetGuid("BranchId");
                while (sR.Read() == true)
                {
                    BranchDesc bD = new BranchDesc();
                    bD.Contents = sR;
                    bD.IsSaved = true;
                    this.branches.Add(bD);
                }

                BranchDesc branchDesc = this.branches[branchID];

                LicenseInfoList licenses = branchDesc.LicenseInformationList;

                foreach (LicenseInfo licenseInfo in licenses)
                {
                    if (originalLicenseNum.Equals(licenseInfo.License) && originalState.Equals(licenseInfo.State))
                    {
                        licenses.UpdateLicense(
                            originalLicenseNum, 
                            originalState, 
                            this.GetString("LicenseNum"), 
                            this.GetString("State"), 
                            this.GetString("ExpD"), 
                            this.GetString("DisplayName"), 
                            this.GetString("Street"), 
                            this.GetString("City"), 
                            this.GetString("AddrState"), 
                            this.GetString("Zip"), 
                            this.GetString("Phone"), 
                            this.GetString("Fax"), 
                            this.GetString("Editable"));
                    }     
                }

                // get the branchDB
                BranchDB branch = new BranchDB(branchID, brokerId);
                branch.Retrieve();
                branch.LicenseXmlContent = licenses.ToString();
                branch.Save();   
            }
        }  
    }
}
