﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.PdfForm;
using LendersOffice.Security;

namespace LendersOfficeApp.los.BrokerAdmin
{
    public partial class ConsumerPortalLetterPicker : LendersOffice.Common.BasePage
    {
        public override IEnumerable<Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new Permission[]
                {
                    Permission.AllowViewingAndEditingConsumerPortalConfigs
                };
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            m_Grid.DataSource = PdfForm.RetrieveCustomFormsOfCurrentBroker();
            m_Grid.DataBind();
        }
    }
}
