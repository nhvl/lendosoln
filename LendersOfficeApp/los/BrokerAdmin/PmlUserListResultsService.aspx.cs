#region Generated Code
namespace LendersOfficeApp.los.BrokerAdmin
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using admin;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.BrokerAdmin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a service for batch updating Pml users.
    /// </summary>
    public partial class PmlUserListResultsService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Processes the method with the specified <paramref name="methodName"/>.
        /// </summary>
        /// <param name="methodName">
        /// The method to process.
        /// </param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "GetExSupervisorsAssignedToUsers":
                    GetExSupervisorsAssignedToUsers();
                    break;
                case "Update":
                    Update();
                    break;
            }
        }

        /// <summary>
        /// Provides a means for verifying PML supervisor
        /// changes and allows the UI to indicate if an 
        /// ex-PML supervisor is assigned to users.
        /// </summary>
        private void GetExSupervisorsAssignedToUsers()
        {
            var exSupervisors = EmployeeDB.GetSupervisorsAssignedToActiveUsers(
                PrincipalFactory.CurrentPrincipal.BrokerId, 
                this.GetString("possibleSupervisorIds"));

            this.SetResult("HasExSupervisors", exSupervisors.Any());
            this.SetResult("ExSupervisorsJson", SerializationHelper.JsonNetSerialize(exSupervisors));
        }

        /// <summary>
        /// Obtains a mapping between ids and <see cref="PmlBroker"/> records
        /// for the broker with the specified <paramref name="brokerId"/>.
        /// </summary>
        /// <param name="brokerId">
        /// The id of the broker for the originating companies.
        /// </param>
        /// <returns>
        /// A <see cref="Dictionary{Guid, PmlBroker}"/> containing the mapping
        /// between ids and <see cref="PmlBroker"/> records.
        /// </returns>
        private Dictionary<Guid, PmlBroker> GetPmlBrokersById(Guid brokerId)
        {
            var pmlBrokersByIdDictionary = new Dictionary<Guid, PmlBroker>();

            var pmlBrokerIds = GetString("PmlBrokers").Split(',');

            foreach (var pmlBrokerId in pmlBrokerIds)
            {
                var guidId = new Guid(pmlBrokerId);

                if (!pmlBrokersByIdDictionary.ContainsKey(guidId))
                {
                    pmlBrokersByIdDictionary.Add(guidId, PmlBroker.RetrievePmlBrokerById(guidId, brokerId));
                }
            }

            return pmlBrokersByIdDictionary;
        }

        private void GetMfaSettings (BatchUpdateSetting updateSetting)
        {
            updateSetting.EnabledIpRestriction = this.GetInt("EnabledIpRestriction", -1);
            updateSetting.EnabledClientDigitalCertificateInstall = this.GetInt("EnabledClientDigitalCertificateInstall", -1);
            updateSetting.EnableAuthCodeViaSms = this.GetInt("EnableAuthCodeViaSms", -1);
        }

        /// <summary>
        /// Obtains the permission settings from the data sent
        /// to the service.
        /// </summary>
        /// <param name="updateSetting">
        /// The <see cref="BatchUpdateSetting"/> container for the
        /// received data.
        /// </param>
        /// <param name="pmlBrokers">
        /// The mapping of <see cref="PmlBroker"/> records.
        /// </param>
        /// <param name="mode">
        /// The <see cref="OriginatingCompanyChannelMode"/> for the service.
        /// </param>
        /// <param name="brokerId">
        /// The id of the broker for the originating companies.
        /// </param>
        private void GetPermissionSettings(
            BatchUpdateSetting updateSetting,
            Dictionary<Guid, PmlBroker>.ValueCollection pmlBrokers,
            OriginatingCompanyChannelMode mode,
            Guid brokerId)
        {
            var priceGroup = GetString("LpePriceGroupId");
            if (!string.IsNullOrEmpty(priceGroup))
            {
                if (priceGroup.ToUpper() == "OC")
                {
                    updateSetting.UseOriginatingCompanyPriceGroupId = true;
                }
                else
                {
                    updateSetting.PriceGroupId = new Guid(priceGroup);
                }
            }

            if (GetString("TPOLandingPageID", string.Empty) != string.Empty)
            {
                if (GetString("TPOLandingPageID") != "none")
                {
                    updateSetting.TpoLandingPageID = GetGuid("TPOLandingPageID");
                }
                else
                {
                    updateSetting.TpoLandingPageID = Guid.Empty;
                }
            }

            var branch = GetString("Branch");
            if (!string.IsNullOrEmpty(branch))
            {
                if (branch.ToUpper() == "OC")
                {
                    updateSetting.UseOriginatingCompanyBranchId = true;
                }
                else
                {
                    updateSetting.BranchId = new Guid(branch);
                }
            }

            if (GetString("RunPricingWithoutCredit") == "1")
            {
                updateSetting.UpdatePricingWithoutCredit = true;
                updateSetting.RunPricingWithoutCredit = true;
            }
            else if (GetString("RunPricingWithoutCredit") == "0")
            {
                updateSetting.UpdatePricingWithoutCredit = true;
                updateSetting.RunPricingWithoutCredit = false;
            }
            else
            {
                updateSetting.UpdatePricingWithoutCredit = false;
            }

            // 06/09/06 mf - OPM 4546.
            if (GetString("SubmitWithoutCredit") == "1")
            {
                updateSetting.UpdateSubmitWithoutCredit = true;
                updateSetting.SubmitWithoutCredit = true;
            }
            else if (GetString("SubmitWithoutCredit") == "0")
            {
                updateSetting.UpdateSubmitWithoutCredit = true;
                updateSetting.SubmitWithoutCredit = false;
            }
            else
            {
                updateSetting.UpdateSubmitWithoutCredit = false;
            }

            if (GetString("ApplyForIneligibleLoanPrograms") == "1")
            {
                updateSetting.UpdateApplyForIneligibleLoanPrograms = true;
                updateSetting.AllowApplyingForIneligibleLoanPrograms = true;
            }
            else if (GetString("ApplyForIneligibleLoanPrograms") == "0")
            {
                updateSetting.UpdateApplyForIneligibleLoanPrograms = true;
                updateSetting.AllowApplyingForIneligibleLoanPrograms = false;
            }
            else
            {
                updateSetting.UpdateApplyForIneligibleLoanPrograms = false;
            }

            if (GetString("Allow4506T") == "1")
            {
                updateSetting.UpdateAllow4506T = true;
                updateSetting.Allow4506T = true;
            }
            else if (GetString("Allow4506T") == "0")
            {
                updateSetting.UpdateAllow4506T = true;
                updateSetting.Allow4506T = false;
            }
            else
            {
                updateSetting.UpdateAllow4506T = false;
            }

            if (GetString("AllowViewWholesaleLoan") == "1")
            {
                updateSetting.UpdateAllowViewWholesaleLoan = true;
                updateSetting.AllowViewWholesaleLoan = true;
            }
            else if (GetString("AllowViewWholesaleLoan") == "0")
            {
                updateSetting.UpdateAllowViewWholesaleLoan = true;
                updateSetting.AllowViewWholesaleLoan = false;
            }
            else
            {
                updateSetting.UpdateAllowViewWholesaleLoan = false;
            }

            if (GetString("AllowViewCorrLoan") == "1")
            {
                updateSetting.UpdateAllowViewCorrLoan = true;
                updateSetting.AllowViewCorrLoan = true;
            }
            else if (GetString("AllowViewCorrLoan") == "0")
            {
                updateSetting.UpdateAllowViewCorrLoan = true;
                updateSetting.AllowViewCorrLoan = false;
            }
            else
            {
                updateSetting.UpdateAllowViewCorrLoan = false;
            }

            if (GetString("AllowViewMiniCorrLoan") == "1")
            {
                updateSetting.UpdateAllowViewMiniCorrLoan = true;
                updateSetting.AllowViewMiniCorrLoan = true;
            }
            else if (GetString("AllowViewMiniCorrLoan") == "0")
            {
                updateSetting.UpdateAllowViewMiniCorrLoan = true;
                updateSetting.AllowViewMiniCorrLoan = false;
            }
            else
            {
                updateSetting.UpdateAllowViewMiniCorrLoan = false;
            }

            if (GetString("AllowCreateWholesaleLoan") == "1")
            {
                updateSetting.UpdateAllowCreateWholesaleLoan = true;
                updateSetting.AllowCreateWholesaleLoan = true;
            }
            else if (GetString("AllowCreateWholesaleLoan") == "0")
            {
                updateSetting.UpdateAllowCreateWholesaleLoan = true;
                updateSetting.AllowCreateWholesaleLoan = false;
            }
            else
            {
                updateSetting.UpdateAllowCreateWholesaleLoan = false;
            }

            if (GetString("AllowCreateCorrLoan") == "1")
            {
                updateSetting.UpdateAllowCreateCorrLoan = true;
                updateSetting.AllowCreateCorrLoan = true;
            }
            else if (GetString("AllowCreateCorrLoan") == "0")
            {
                updateSetting.UpdateAllowCreateCorrLoan = true;
                updateSetting.AllowCreateCorrLoan = false;
            }
            else
            {
                updateSetting.UpdateAllowCreateCorrLoan = false;
            }

            if (GetString("AllowCreateMiniCorrLoan") == "1")
            {
                updateSetting.UpdateAllowCreateMiniCorrLoan = true;
                updateSetting.AllowCreateMiniCorrLoan = true;
            }
            else if (GetString("AllowCreateMiniCorrLoan") == "0")
            {
                updateSetting.UpdateAllowCreateMiniCorrLoan = true;
                updateSetting.AllowCreateMiniCorrLoan = false;
            }
            else
            {
                updateSetting.UpdateAllowCreateMiniCorrLoan = false;
            }

            if ((updateSetting.AllowCreateWholesaleLoan && !updateSetting.AllowViewWholesaleLoan) ||
                (updateSetting.AllowCreateMiniCorrLoan && !updateSetting.AllowViewMiniCorrLoan) ||
                (updateSetting.AllowCreateCorrLoan && !updateSetting.AllowViewCorrLoan))
            {
                var msg = "Unable to grant permission to create loans without granting permission " +
                    "to view loans for a given portal mode.";

                throw new InvalidOperationException(msg);
            }

            if (GetString("Status") == "1")
            {
                updateSetting.UpdateStatus = true;
                updateSetting.Status = true;
            }
            else if (GetString("Status") == "0")
            {
                updateSetting.UpdateStatus = true;
                updateSetting.Status = false;
            }
            else
            {
                updateSetting.UpdateStatus = false;
            }

            string isBrokerProcessorString = GetString("IsBrokerProcessor");
            updateSetting.UpdateIsBrokerProcessor = isBrokerProcessorString != "-1";
            updateSetting.IsBrokerProcessor = isBrokerProcessorString == "1";

            string isLoanOfficerString = GetString("IsLoanOfficer");
            updateSetting.UpdateIsLoanOfficer = isLoanOfficerString != "-1";
            updateSetting.IsLoanOfficer = isLoanOfficerString == "1";

            string isExternalSecondaryString = GetString("IsExternalSecondary");
            updateSetting.UpdateIsExternalSecondary = isExternalSecondaryString != "-1";
            updateSetting.IsExternalSecondary = isExternalSecondaryString == "1";

            string isExternalPostCloserString = GetString("IsExternalPostCloser");
            updateSetting.UpdateIsExternalPostCloser = isExternalPostCloserString != "-1";
            updateSetting.IsExternalPostCloser = isExternalPostCloserString == "1";

            string navJson = GetString("TpoNavLinks", "[]");
            updateSetting.TpoNavLinks = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(navJson);

            // Validate OC
            if (updateSetting.UseOriginatingCompanyBranchId || updateSetting.UseOriginatingCompanyPriceGroupId)
            {
                foreach (var pmlBroker in pmlBrokers)
                {
                    bool error = false;

                    switch (mode)
                    {
                        case OriginatingCompanyChannelMode.Broker:
                            error = pmlBroker.BranchId == null || pmlBroker.LpePriceGroupId == null;
                            break;
                        case OriginatingCompanyChannelMode.MiniCorrespondent:
                            error = pmlBroker.MiniCorrespondentBranchId == null || pmlBroker.MiniCorrespondentLpePriceGroupId == null;
                            break;
                        case OriginatingCompanyChannelMode.Correspondent:
                            error = pmlBroker.CorrespondentBranchId == null || pmlBroker.CorrespondentLpePriceGroupId == null;
                            break;
                        default:
                            throw new UnhandledEnumException(mode);
                    }

                    if (error)
                    {
                        string msg = string.Format(
                            "Cannot update selected users. Price group and/or branch have not been " +
                            "set for the Originating Company of one or more selected users{0}. Please edit" +
                            "the originating company and set the price group and branch{1}, and then " +
                            "retry.",
                            ConstStage.EnableDefaultBranchesAndPriceGroups ? " and no applicable Originator portal configuration defaults were found" : string.Empty,
                            ConstStage.EnableDefaultBranchesAndPriceGroups ? " or set the Originator portal configuration default price group and branch" : string.Empty);

                        throw new InvalidOperationException(msg);
                    }
                }
            }
        }

        /// <summary>
        /// Obtains the relationship data sent to the service.
        /// </summary>
        /// <param name="updateSetting">
        /// The <see cref="BatchUpdateSetting"/> container for the
        /// received data.
        /// </param>
        private void GetRelationshipSettings(BatchUpdateSetting updateSetting)
        {
            Func<string, bool> isCurrent = s => s.Equals("current", StringComparison.InvariantCultureIgnoreCase);

            var lenderAccExecEmployeeIdStr = GetString("LenderAccExecEmployeeId");
            if (!string.IsNullOrEmpty(lenderAccExecEmployeeIdStr) && !isCurrent(lenderAccExecEmployeeIdStr))
            {
                updateSetting.LenderAccountExecEmployeeId = new Guid(lenderAccExecEmployeeIdStr);
            }

            var underwriterEmployeeIdStr = GetString("UnderwriterEmployeeId");
            if (!string.IsNullOrEmpty(underwriterEmployeeIdStr) && !isCurrent(underwriterEmployeeIdStr))
            {
                updateSetting.UnderwriterEmployeeId = new Guid(underwriterEmployeeIdStr);
            }

            var juniorUnderwriterEmployeeIdStr = GetString("JuniorUnderwriterEmployeeId");
            if (!string.IsNullOrEmpty(juniorUnderwriterEmployeeIdStr) && !isCurrent(juniorUnderwriterEmployeeIdStr))
            {
                updateSetting.JuniorUnderwriterEmployeeId = new Guid(juniorUnderwriterEmployeeIdStr);
            }

            var lockDeskEmployeeIdStr = GetString("LockDeskEmployeeId");
            if (!string.IsNullOrEmpty(lockDeskEmployeeIdStr) && !isCurrent(lockDeskEmployeeIdStr))
            {
                updateSetting.LockDeskEmployeeId = new Guid(lockDeskEmployeeIdStr);
            }

            var managerEmployeeIdStr = GetString("ManagerEmployeeId");
            if (!string.IsNullOrEmpty(managerEmployeeIdStr) && !isCurrent(managerEmployeeIdStr))
            {
                updateSetting.ManagerEmployeeId = new Guid(managerEmployeeIdStr);
            }

            var processorEmployeeIdStr = GetString("ProcessorEmployeeId");
            if (!string.IsNullOrEmpty(processorEmployeeIdStr) && !isCurrent(processorEmployeeIdStr))
            {
                updateSetting.ProcessorEmployeeId = new Guid(processorEmployeeIdStr);
            }

            var juniorProcessorEmployeeIdStr = GetString("JuniorProcessorEmployeeId");
            if (!string.IsNullOrEmpty(juniorProcessorEmployeeIdStr) && !isCurrent(juniorProcessorEmployeeIdStr))
            {
                updateSetting.JuniorProcessorEmployeeId = new Guid(juniorProcessorEmployeeIdStr);
            }

            var creditAuditorStr = GetString("CreditAuditorEmployeeId");
            if (!string.IsNullOrEmpty(creditAuditorStr) && !isCurrent(creditAuditorStr))
            {
                updateSetting.CreditAuditorEmployeeId = new Guid(creditAuditorStr);
            }

            var legalAuditorStr = GetString("LegalAuditorEmployeeId");
            if (!string.IsNullOrEmpty(legalAuditorStr) && !isCurrent(legalAuditorStr))
            {
                updateSetting.LegalAuditorEmployeeId = new Guid(legalAuditorStr);
            }

            var purchaserStr = GetString("PurchaserEmployeeId");
            if (!string.IsNullOrEmpty(purchaserStr) && !isCurrent(purchaserStr))
            {
                updateSetting.PurchaserEmployeeId = new Guid(purchaserStr);
            }

            var secondaryStr = GetString("SecondaryEmployeeId");
            if (!string.IsNullOrEmpty(secondaryStr) && !isCurrent(secondaryStr))
            {
                updateSetting.SecondaryEmployeeId = new Guid(secondaryStr);
            }

            var supervisorStr = GetString("Supervisor", string.Empty);

            var supervisorId = Guid.Empty;

            if (supervisorStr == "Self")
            {
                updateSetting.UpdateIsSupervisor = true;
                updateSetting.MakeSupervisor = true;
            }
            else if (supervisorStr == "None")
            {
                updateSetting.UpdateSupervisor = true;
                updateSetting.MakeSupervisor = false;
            }
            else if (supervisorStr == "Not")
            {
                updateSetting.UpdateIsSupervisor = true;
                updateSetting.MakeSupervisor = false;
            }
            else if (Guid.TryParse(supervisorStr, out supervisorId))
            {
                updateSetting.SupervisorId = supervisorId;
                updateSetting.UpdateSupervisor = true;
            }
        }

        /// <summary>
        /// Updates the broker relationships for the specified 
        /// <paramref name="employee"/> using the specified 
        /// <paramref name="relationshipSetting"/>
        /// container.
        /// </summary>
        /// <param name="employee">
        /// The <see cref="EmployeeDB"/> to update.
        /// </param>
        /// <param name="relationshipSetting">
        /// The <see cref="BatchUpdateSetting"/> container for the
        /// broker relationship settings.
        /// </param>
        private void UpdateBrokerRelationships(
            EmployeeDB employee,
            BatchUpdateSetting relationshipSetting)
        {
            if (relationshipSetting.BranchId.HasValue)
            {
                employee.BranchID = relationshipSetting.BranchId.Value;
            }
            else if (relationshipSetting.UseOriginatingCompanyBranchId)
            {
                employee.UseOriginatingCompanyBranchId = true;
            }

            if (relationshipSetting.PriceGroupId.HasValue)
            {
                employee.LpePriceGroupID = relationshipSetting.PriceGroupId.Value;
            }
            else if (relationshipSetting.UseOriginatingCompanyPriceGroupId)
            {
                employee.UseOriginatingCompanyLpePriceGroupId = true;
            }

            if (relationshipSetting.TpoLandingPageID.HasValue)
            {
                if (relationshipSetting.TpoLandingPageID == Guid.Empty)
                {
                    employee.TPOLandingPageID = null;
                }
                else
                {
                    employee.TPOLandingPageID = relationshipSetting.TpoLandingPageID;
                }
            }

            if (relationshipSetting.LenderAccountExecEmployeeId.HasValue)
            {
                employee.LenderAcctExecEmployeeID = relationshipSetting.LenderAccountExecEmployeeId.Value;
            }

            if (relationshipSetting.ManagerEmployeeId.HasValue)
            {
                employee.ManagerEmployeeID = relationshipSetting.ManagerEmployeeId.Value;
            }

            if (relationshipSetting.UnderwriterEmployeeId.HasValue)
            {
                employee.UnderwriterEmployeeID = relationshipSetting.UnderwriterEmployeeId.Value;
            }

            if (relationshipSetting.JuniorUnderwriterEmployeeId.HasValue)
            {
                employee.JuniorUnderwriterEmployeeID = relationshipSetting.JuniorUnderwriterEmployeeId.Value;
            }

            if (relationshipSetting.LockDeskEmployeeId.HasValue)
            {
                employee.LockDeskEmployeeID = relationshipSetting.LockDeskEmployeeId.Value;
            }

            if (relationshipSetting.ManagerEmployeeId.HasValue)
            {
                employee.ManagerEmployeeID = relationshipSetting.ManagerEmployeeId.Value;
            }

            if (relationshipSetting.ProcessorEmployeeId.HasValue)
            {
                employee.ProcessorEmployeeID = relationshipSetting.ProcessorEmployeeId.Value;
            }

            if (relationshipSetting.JuniorProcessorEmployeeId.HasValue)
            {
                employee.JuniorProcessorEmployeeID = relationshipSetting.JuniorProcessorEmployeeId.Value;
            }
        }

        /// <summary>
        /// Updates the correspondent relationships for the specified 
        /// <paramref name="employee"/> using the specified 
        /// <paramref name="relationshipSetting"/> container.
        /// </summary>
        /// <param name="employee">
        /// The <see cref="EmployeeDB"/> to update.
        /// </param>
        /// <param name="relationshipSetting">
        /// The <see cref="BatchUpdateSetting"/> container for the
        /// correspondent relationship settings.
        /// </param>
        private void UpdateCorrespondentRelationships(
            EmployeeDB employee,
            BatchUpdateSetting relationshipSetting)
        {
            if (relationshipSetting.BranchId.HasValue)
            {
                employee.CorrespondentBranchID = relationshipSetting.BranchId.Value;
            }
            else if (relationshipSetting.UseOriginatingCompanyBranchId)
            {
                employee.UseOriginatingCompanyCorrBranchId = true;
            }

            if (relationshipSetting.PriceGroupId.HasValue)
            {
                employee.CorrespondentPriceGroupID = relationshipSetting.PriceGroupId.Value;
            }
            else if (relationshipSetting.UseOriginatingCompanyPriceGroupId)
            {
                employee.UseOriginatingCompanyCorrPriceGroupId = true;
            }

            if (relationshipSetting.TpoLandingPageID.HasValue)
            {
                if (relationshipSetting.TpoLandingPageID == Guid.Empty)
                {
                    employee.CorrespondentLandingPageID = null;
                }
                else
                {
                    employee.CorrespondentLandingPageID = relationshipSetting.TpoLandingPageID;
                }
            }

            if (relationshipSetting.LenderAccountExecEmployeeId.HasValue)
            {
                employee.CorrLenderAccExecEmployeeId = relationshipSetting.LenderAccountExecEmployeeId.Value;
            }

            if (relationshipSetting.ManagerEmployeeId.HasValue)
            {
                employee.CorrManagerEmployeeId = relationshipSetting.ManagerEmployeeId.Value;
            }

            if (relationshipSetting.UnderwriterEmployeeId.HasValue)
            {
                employee.CorrUnderwriterEmployeeId = relationshipSetting.UnderwriterEmployeeId.Value;
            }

            if (relationshipSetting.JuniorUnderwriterEmployeeId.HasValue)
            {
                employee.CorrJuniorUnderwriterEmployeeId = relationshipSetting.JuniorUnderwriterEmployeeId.Value;
            }

            if (relationshipSetting.LockDeskEmployeeId.HasValue)
            {
                employee.CorrLockDeskEmployeeId = relationshipSetting.LockDeskEmployeeId.Value;
            }

            if (relationshipSetting.ManagerEmployeeId.HasValue)
            {
                employee.CorrManagerEmployeeId = relationshipSetting.ManagerEmployeeId.Value;
            }

            if (relationshipSetting.ProcessorEmployeeId.HasValue)
            {
                employee.CorrProcessorEmployeeId = relationshipSetting.ProcessorEmployeeId.Value;
            }

            if (relationshipSetting.JuniorProcessorEmployeeId.HasValue)
            {
                employee.CorrJuniorProcessorEmployeeId = relationshipSetting.JuniorProcessorEmployeeId.Value;
            }

            if (relationshipSetting.CreditAuditorEmployeeId.HasValue)
            {
                employee.CorrCreditAuditorEmployeeId = relationshipSetting.CreditAuditorEmployeeId.Value;
            }

            if (relationshipSetting.LegalAuditorEmployeeId.HasValue)
            {
                employee.CorrLegalAuditorEmployeeId = relationshipSetting.LegalAuditorEmployeeId.Value;
            }

            if (relationshipSetting.PurchaserEmployeeId.HasValue)
            {
                employee.CorrPurchaserEmployeeId = relationshipSetting.PurchaserEmployeeId.Value;
            }

            if (relationshipSetting.SecondaryEmployeeId.HasValue)
            {
                employee.CorrSecondaryEmployeeId = relationshipSetting.SecondaryEmployeeId.Value;
            }
        }

        /// <summary>
        /// Updates the mini-correspondent relationships for the specified 
        /// <paramref name="employee"/> using the specified 
        /// <paramref name="relationshipSetting"/> container.
        /// </summary>
        /// <param name="employee">
        /// The <see cref="EmployeeDB"/> to update.
        /// </param>
        /// <param name="relationshipSetting">
        /// The <see cref="BatchUpdateSetting"/> container for the
        /// mini-correspondent relationship settings.
        /// </param>
        private void UpdateMiniCorrespondentRelationships(
            EmployeeDB employee,
            BatchUpdateSetting relationshipSetting)
        {
            if (relationshipSetting.BranchId.HasValue)
            {
                employee.MiniCorrespondentBranchID = relationshipSetting.BranchId.Value;
            }
            else if (relationshipSetting.UseOriginatingCompanyBranchId)
            {
                employee.UseOriginatingCompanyMiniCorrBranchId = true;
            }

            if (relationshipSetting.PriceGroupId.HasValue)
            {
                employee.MiniCorrespondentPriceGroupID = relationshipSetting.PriceGroupId.Value;
            }
            else if (relationshipSetting.UseOriginatingCompanyPriceGroupId)
            {
                employee.UseOriginatingCompanyMiniCorrPriceGroupId = true;
            }

            if (relationshipSetting.TpoLandingPageID.HasValue)
            {
                if (relationshipSetting.TpoLandingPageID == Guid.Empty)
                {
                    employee.MiniCorrespondentLandingPageID = null;
                }
                else
                {
                    employee.MiniCorrespondentLandingPageID = relationshipSetting.TpoLandingPageID;
                }
            }

            if (relationshipSetting.LenderAccountExecEmployeeId.HasValue)
            {
                employee.MiniCorrLenderAccExecEmployeeId = relationshipSetting.LenderAccountExecEmployeeId.Value;
            }

            if (relationshipSetting.ManagerEmployeeId.HasValue)
            {
                employee.MiniCorrManagerEmployeeId = relationshipSetting.ManagerEmployeeId.Value;
            }

            if (relationshipSetting.UnderwriterEmployeeId.HasValue)
            {
                employee.MiniCorrUnderwriterEmployeeId = relationshipSetting.UnderwriterEmployeeId.Value;
            }

            if (relationshipSetting.JuniorUnderwriterEmployeeId.HasValue)
            {
                employee.MiniCorrJuniorUnderwriterEmployeeId = relationshipSetting.JuniorUnderwriterEmployeeId.Value;
            }

            if (relationshipSetting.LockDeskEmployeeId.HasValue)
            {
                employee.MiniCorrLockDeskEmployeeId = relationshipSetting.LockDeskEmployeeId.Value;
            }

            if (relationshipSetting.ManagerEmployeeId.HasValue)
            {
                employee.MiniCorrManagerEmployeeId = relationshipSetting.ManagerEmployeeId.Value;
            }

            if (relationshipSetting.ProcessorEmployeeId.HasValue)
            {
                employee.MiniCorrProcessorEmployeeId = relationshipSetting.ProcessorEmployeeId.Value;
            }

            if (relationshipSetting.JuniorProcessorEmployeeId.HasValue)
            {
                employee.MiniCorrJuniorProcessorEmployeeId = relationshipSetting.JuniorProcessorEmployeeId.Value;
            }

            if (relationshipSetting.CreditAuditorEmployeeId.HasValue)
            {
                employee.MiniCorrCreditAuditorEmployeeId = relationshipSetting.CreditAuditorEmployeeId.Value;
            }

            if (relationshipSetting.LegalAuditorEmployeeId.HasValue)
            {
                employee.MiniCorrLegalAuditorEmployeeId = relationshipSetting.LegalAuditorEmployeeId.Value;
            }

            if (relationshipSetting.PurchaserEmployeeId.HasValue)
            {
                employee.MiniCorrPurchaserEmployeeId = relationshipSetting.PurchaserEmployeeId.Value;
            }

            if (relationshipSetting.SecondaryEmployeeId.HasValue)
            {
                employee.MiniCorrSecondaryEmployeeId = relationshipSetting.SecondaryEmployeeId.Value;
            }
        }

        /// <summary>
        /// Updates the specified <paramref name="employee"/> with the specified
        /// <paramref name="relationshipSetting"/> container for the specified
        /// <paramref name="mode"/>.
        /// </summary>
        /// <param name="employee">
        /// The <see cref="EmployeeDB"/> to update.
        /// </param>
        /// <param name="relationshipSetting">
        /// The <see cref="BatchUpdateSetting"/> container for the
        /// relationship settings.
        /// </param>
        /// <param name="mode">
        /// The <see cref="OriginatingCompanyChannelMode"/> for the service.
        /// </param>
        private void UpdateRelationshipsForMode(
            EmployeeDB employee,
            BatchUpdateSetting relationshipSetting,
            OriginatingCompanyChannelMode mode)
        {
            switch (mode)
            {
                case OriginatingCompanyChannelMode.Broker:
                    this.UpdateBrokerRelationships(employee, relationshipSetting);
                    break;
                case OriginatingCompanyChannelMode.Correspondent:
                    this.UpdateCorrespondentRelationships(
                        employee,
                        relationshipSetting);
                    break;
                case OriginatingCompanyChannelMode.MiniCorrespondent:
                    this.UpdateMiniCorrespondentRelationships(
                        employee,
                        relationshipSetting);
                    break;
                default:
                    throw new UnhandledEnumException(mode);
            }
        }

        /// <summary>
        /// Updates the permissions for the specified <paramref name="employee"/>
        /// using the specified <paramref name="updateSetting"/> container.
        /// </summary>
        /// <param name="executor">
        /// The <see cref="CStoredProcedureExec"/> for the permission update
        /// transaction.
        /// </param>
        /// <param name="employee">
        /// The <see cref="EmployeeDB"/> to update.
        /// </param>
        /// <param name="pmlBroker">
        /// The <see cref="PmlBroker"/> for the employee.
        /// </param>
        /// <param name="updateSetting">
        /// The <see cref="BatchUpdateSetting"/> container for the
        /// permission settings.
        /// </param>
        private void UpdatePermissions(
            CStoredProcedureExec executor,
            EmployeeDB employee,
            PmlBroker pmlBroker,
            BatchUpdateSetting updateSetting)
        {
            BrokerUserPermissions brokerUserPermissions = new BrokerUserPermissions(pmlBroker.BrokerId, employee.ID, executor);

            if (updateSetting.UpdatePricingWithoutCredit)
            {
                brokerUserPermissions.SetPermission(
                    Permission.CanRunPricingEngineWithoutCreditReport,
                    updateSetting.RunPricingWithoutCredit);
            }

            if (updateSetting.UpdateApplyForIneligibleLoanPrograms)
            {
                brokerUserPermissions.SetPermission(
                    Permission.CanApplyForIneligibleLoanPrograms,
                    updateSetting.AllowApplyingForIneligibleLoanPrograms);
            }

            if (updateSetting.UpdateSubmitWithoutCredit)
            {
                brokerUserPermissions.SetPermission(
                    Permission.CanSubmitWithoutCreditReport,
                    updateSetting.SubmitWithoutCredit);
            }

            if (updateSetting.UpdateAllow4506T)
            {
                brokerUserPermissions.SetPermission(
                    Permission.AllowOrder4506T,
                    updateSetting.Allow4506T);
            }

            if (updateSetting.UpdateAllowViewWholesaleLoan)
            {
                brokerUserPermissions.SetPermission(
                    Permission.AllowViewingWholesaleChannelLoans,
                    updateSetting.AllowViewWholesaleLoan);
            }

            if (updateSetting.UpdateAllowViewMiniCorrLoan)
            {
                brokerUserPermissions.SetPermission(
                    Permission.AllowViewingMiniCorrChannelLoans,
                    updateSetting.AllowViewMiniCorrLoan);
            }

            if (updateSetting.UpdateAllowViewCorrLoan)
            {
                brokerUserPermissions.SetPermission(
                    Permission.AllowViewingCorrChannelLoans,
                    updateSetting.AllowViewCorrLoan);
            }

            if (updateSetting.UpdateAllowCreateWholesaleLoan)
            {
                brokerUserPermissions.SetPermission(
                    Permission.AllowCreatingWholesaleChannelLoans,
                    updateSetting.AllowCreateWholesaleLoan);
            }

            if (updateSetting.UpdateAllowCreateMiniCorrLoan)
            {
                brokerUserPermissions.SetPermission(
                    Permission.AllowCreatingMiniCorrChannelLoans,
                    updateSetting.AllowCreateMiniCorrLoan);
            }

            if (updateSetting.UpdateAllowCreateCorrLoan)
            {
                brokerUserPermissions.SetPermission(
                    Permission.AllowCreatingCorrChannelLoans,
                    updateSetting.AllowCreateCorrLoan);
            }

            brokerUserPermissions.Update(executor);
        }

        /// <summary>
        /// Updates the roles for the specified <paramref name="employee"/>
        /// using the specified <paramref name="updateSetting"/> container.
        /// </summary>
        /// <param name="brokerId">
        /// The id of the broker for the employee.
        /// </param>
        /// <param name="employee">
        /// The <see cref="EmployeeDB"/> to update.
        /// </param>
        /// <param name="updateSetting">
        /// The <see cref="BatchUpdateSetting"/> container for the
        /// role settings.
        /// </param>
        /// <returns>
        /// True if the update was successful, false otherwise.
        /// </returns>
        private bool UpdateRoles(
            Guid brokerId,
            EmployeeDB employee,
            BatchUpdateSetting updateSetting)
        {
            EmployeeRoles roles = new EmployeeRoles(brokerId, employee.ID);

            bool isEmpLoanOfficer = updateSetting.UpdateIsLoanOfficer ? updateSetting.IsLoanOfficer : roles.IsInRole(CEmployeeFields.s_LoanRepRoleId);
            bool isEmpBrokerProcessor = updateSetting.UpdateIsBrokerProcessor ? updateSetting.IsBrokerProcessor : roles.IsInRole(CEmployeeFields.s_BrokerProcessorId);
            bool isEmpExternalSecondary = updateSetting.UpdateIsExternalSecondary ? updateSetting.IsExternalSecondary : roles.IsInRole(E_RoleT.Pml_Secondary);
            bool isEmpExternalPostCloser = updateSetting.UpdateIsExternalPostCloser ? updateSetting.IsExternalPostCloser : roles.IsInRole(E_RoleT.Pml_PostCloser);

            if (employee.IsPmlManager && updateSetting.UpdateSupervisor)
            {
                updateSetting.UpdateIsSupervisor = true;
            }

            bool isEmpSupervisor = updateSetting.UpdateIsSupervisor ? updateSetting.MakeSupervisor : employee.IsPmlManager;

            if (isEmpLoanOfficer || isEmpBrokerProcessor || isEmpSupervisor || isEmpExternalSecondary || isEmpExternalPostCloser)
            {
                if (updateSetting.UpdateIsBrokerProcessor || updateSetting.UpdateIsLoanOfficer || updateSetting.UpdateIsSupervisor || updateSetting.UpdateSupervisor || updateSetting.UpdateIsExternalSecondary || updateSetting.UpdateIsExternalPostCloser)
                {
                    string newRoles = null;
                    if (updateSetting.UpdateIsSupervisor)
                    {
                        if (!employee.IsPmlManager && !updateSetting.MakeSupervisor)
                        {
                            // If the person is not a supervisor and the user chose "Not a supervisor", 
                            // then don't do anything.
                        }
                        else
                        {
                            employee.IsPmlManager = updateSetting.MakeSupervisor;
                            employee.PmlExternalManagerEmployeeId = updateSetting.SupervisorId;
                        }
                    }

                    if (updateSetting.UpdateSupervisor) //OPM 6578 - dc
                    {
                        // If someone is being associated with a supervisor, they are not a supervisor themselves.
                        if (updateSetting.SupervisorId != Guid.Empty)
                        {
                            // Make sure we are not setting someone as their own supervisor.
                            if (updateSetting.SupervisorId != employee.ID)
                            {
                                employee.PmlExternalManagerEmployeeId = updateSetting.SupervisorId;
                                employee.IsPmlManager = false;
                            }
                        }
                        else if (!employee.IsPmlManager)
                        {
                            employee.PmlExternalManagerEmployeeId = Guid.Empty;
                    }
                    }

                    if (isEmpSupervisor)
                    {
                        newRoles += CEmployeeFields.s_AdministratorRoleId.ToString();
                    }

                    if (isEmpLoanOfficer)
                    {
                        if (!string.IsNullOrEmpty(newRoles))
                        {
                            newRoles += ",";
                        }

                        newRoles += CEmployeeFields.s_LoanRepRoleId.ToString();
                    }

                    if (isEmpBrokerProcessor)
                    {
                        if (!string.IsNullOrEmpty(newRoles))
                        {
                            newRoles += ",";
                        }

                        newRoles += CEmployeeFields.s_BrokerProcessorId.ToString();
                    }

                    if (isEmpExternalSecondary)
                    {
                        if (!string.IsNullOrEmpty(newRoles))
                        {
                            newRoles += ",";
                        }

                        newRoles += CEmployeeFields.s_ExternalSecondaryId.ToString();
                    }

                    if (isEmpExternalPostCloser)
                    {
                        if (!string.IsNullOrEmpty(newRoles))
                        {
                            newRoles += ",";
                        }

                        newRoles += CEmployeeFields.s_ExternalPostCloserId.ToString();
                    }

                    employee.Roles = newRoles;
                }

                return true;
            }

            return false;
        }

        public bool UpdateMfaOptions(EmployeeDB empDb, BatchUpdateSetting settings)
        {
            empDb.EnabledIpRestriction = settings.EnabledIpRestriction == -1 ? empDb.EnabledIpRestriction : settings.EnabledIpRestriction == 1;
            empDb.EnabledClientDigitalCertificateInstall = 
                settings.EnabledClientDigitalCertificateInstall == -1 ? empDb.EnabledClientDigitalCertificateInstall : settings.EnabledClientDigitalCertificateInstall == 1;

            var isEnableAuthCodeViaSms = settings.EnableAuthCodeViaSms == 1;
            if(settings.EnableAuthCodeViaSms != -1)
            {
                if(isEnableAuthCodeViaSms && !empDb.HasCellPhoneNumber)
                {
                    return false;
                }
                else
                {
                    empDb.EnableAuthCodeViaSms = isEnableAuthCodeViaSms;
                }
            }

            return true;
        }

        /// <summary>
        /// Performs the batch update for the employee ids sent to the service.
        /// </summary>
        private void Update()
        {
            Guid brokerId = /*((BrokerUserPrincipal) this.User)*/ BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            Guid userId = BrokerUserPrincipal.CurrentPrincipal.UserId;

            var useOcRelationshipSettings = GetBool("UseOcRelationshipSettings");
            var useOcPermissionSettings = GetBool("UseOcPermissionSettings");

            var mode = (OriginatingCompanyChannelMode)Enum.Parse(
                typeof(OriginatingCompanyChannelMode),
                GetString("Mode"));

            var pmlBrokersById = this.GetPmlBrokersById(brokerId);

            var updateSetting = new BatchUpdateSetting();

            this.GetMfaSettings(updateSetting);

            if (!useOcRelationshipSettings)
            {
                this.GetRelationshipSettings(updateSetting);
            }

            if (!useOcPermissionSettings)
            {
                try
                {
                    this.GetPermissionSettings(
                        updateSetting,
                        pmlBrokersById.Values,
                        mode,
                        brokerId);
                }
                catch (InvalidOperationException exc)
                {
                    SetResult("Status", exc.Message);
                    return;
                }
            }

            int count = GetInt("Count");
            int numberOfSuccesses = 0;
            int numberOfFailures = 0;
            StringBuilder errorDetails = new StringBuilder();
            bool roleMissingErrorDetected = false;
            string transactionResultStatus = string.Empty;
            List<string> failedSmsUpdateMessage = new List<string>();

            using (CStoredProcedureExec executor = new CStoredProcedureExec(brokerId))
            {
                executor.BeginTransactionForWrite();

                for (int i = 0; i < count; i++)
                {
                    try
                    {
                        Guid employeeId = GetGuid("_" + i.ToString());

                        EmployeeDB employee = new EmployeeDB(employeeId, brokerId);

                        if (employee.Retrieve(executor))
                        {
                            var pmlBroker = pmlBrokersById[employee.PmlBrokerId];

                            if (useOcRelationshipSettings)
                            {
                                updateSetting.SetRelationships(pmlBroker, mode);
                            }

                            if (useOcPermissionSettings)
                            {
                                updateSetting.SetPermissions(pmlBroker);
                            }

                            this.UpdatePermissions(
                                executor,
                                employee,
                                pmlBroker,
                                updateSetting);

                            this.UpdateRelationshipsForMode(
                                employee,
                                updateSetting,
                                mode);

                            if (!UpdateMfaOptions(employee, updateSetting))
                            {
                                numberOfFailures++;
                                failedSmsUpdateMessage.Add(employee.FullName);
                                continue;
                            }

                            if (updateSetting.UpdateStatus)
                            {
                                employee.WhoDoneIt = userId;
                                employee.AllowLogin = updateSetting.Status;
                                employee.IsActive = updateSetting.Status;
                            }

                            if (updateSetting.TpoNavLinks.Count > 0)
                            {
                                HashSet<Guid> newPermissions = new HashSet<Guid>(employee.AccessiblePmlNavigationLinksId);
                                foreach (string item in updateSetting.TpoNavLinks)
                                {
                                    string[] parts = item.Split(new char[] { '_' });

                                    if (parts[0].Equals("Current"))
                                    {
                                        continue;
                                    }

                                    Guid id = new Guid(parts[0]);

                                    if (parts[1].Equals("V"))
                                    {
                                        newPermissions.Add(id);
                                    }
                                    else if (parts[1].Equals("H"))
                                    {
                                        newPermissions.Remove(id);
                                    }
                                    else
                                    {
                                        throw CBaseException.GenericException("Invalid option " + parts[1]);
                                    }
                                }

                                employee.AccessiblePmlNavigationLinksId = newPermissions.ToArray();
                            }

                            if (!useOcPermissionSettings)
                            {
                                var updateSuccessful = this.UpdateRoles(
                                    brokerId,
                                    employee,
                                    updateSetting);

                                if (!updateSuccessful)
                                {
                                    if (!roleMissingErrorDetected)
                                    {
                                        errorDetails.AppendLine(Environment.NewLine + Environment.NewLine + ErrorMessages.PMLUserBatchEditMissingRolesError);
                                    }

                                    roleMissingErrorDetected = true;

                                    errorDetails.AppendLine(employee.FullName + " of " + pmlBroker.Name);

                                    numberOfFailures++;

                                    continue;
                                }
                            }

                            var relationshipPopulationMethod = useOcRelationshipSettings ?
                                EmployeePopulationMethodT.OriginatingCompany :
                                EmployeePopulationMethodT.IndividualUser;

                            switch (mode)
                            {
                                case OriginatingCompanyChannelMode.Broker:
                                    employee.PopulateBrokerRelationshipsT = relationshipPopulationMethod;
                                    break;
                                case OriginatingCompanyChannelMode.MiniCorrespondent:
                                    employee.PopulateMiniCorrRelationshipsT = relationshipPopulationMethod;
                                    break;
                                case OriginatingCompanyChannelMode.Correspondent:
                                    employee.PopulateCorrRelationshipsT = relationshipPopulationMethod;
                                    break;
                                default:
                                    throw new UnhandledEnumException(mode);
                            }

                            employee.PopulatePMLPermissionsT = useOcPermissionSettings ?
                                EmployeePopulationMethodT.OriginatingCompany :
                                EmployeePopulationMethodT.IndividualUser;

                            employee.Save(executor, PrincipalFactory.CurrentPrincipal);

                            numberOfSuccesses++;
                        }
                    }
                    catch (Exception exc)
                    {
                        numberOfFailures++;
                        Tools.LogError("Failed to assign related employees to user.", exc);
                    }
                }

                try
                {
                    executor.CommitTransaction();
                    transactionResultStatus = string.Format("Updated {0} users successfully.\n\r{1} error(s).{2} \n", numberOfSuccesses, numberOfFailures, errorDetails);
                    if(failedSmsUpdateMessage.Count > 0)
                    {
                        transactionResultStatus += "SMS cannot be enabled for the following user(s) because their cell phone numbers are currently blank: \n";
                        transactionResultStatus += string.Join(", ", failedSmsUpdateMessage);
                    }
                }
                catch (Exception exc)
                {
                    Tools.LogError("PML user update transaction failed. Attempting rollback.", exc);

                    try
                    {
                        executor.RollbackTransaction();
                    }
                    catch (Exception exc2)
                    {
                        Tools.LogError("PML user update rollback failed.", exc2);
                    }

                    transactionResultStatus = "PML user update transaction failed.";
                }
            }

            SetResult("Status", transactionResultStatus);
        }
    }
}
