﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsumerPortalList.aspx.cs" Inherits="LendersOfficeApp.los.BrokerAdmin.ConsumerPortalList" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Consumer Portals</title>
</head>
<body>
    <style type="text/css">
        #Title
        {
            font-weight:bold;
            text-decoration:underline;
        }
        #AddBtn
        {
            margin:10px;
        }
    </style>
    <script type="text/javascript">
        function _init() {
            resize(800, 350);
        }
        function onNewClick() {
            if(<%= AspxTools.JsBool(AllowNewConsumerPortals) %>)
            {
                showModal("/los/BrokerAdmin/ConsumerPortalSettings.aspx", null, null, null, function(){
                    window.location = window.location;                
                },{hideCloseButton:true});
            }
            else
            {
                alert('The consumer portal requires the Electronic Documents feature enabled. Please contact support for assistance.');
            }
        }
        function onConfigureClick(cPId)
        {
            showModal("/los/BrokerAdmin/ConsumerPortalSettings.aspx?portalId="+cPId, null, null, null, function(){
                window.location = window.location;                
            },{hideCloseButton:true});
        }
        function copyFromField(labelId)
        {
            copyStringToClipboard(document.getElementById(labelId).innerText);
        }
    </script>
    <h4 class="page-header">Existing Consumer Portal Sites</h4>
    <form id="form1" runat="server">
        <div id="header">
            <div style="float:right;">
                <input type="button" id="AddBtn" onclick="onNewClick();" value="Add New Consumer Portal" />
            </div>
        </div>
        <div id="table">
            <ml:CommonDataGrid ID="m_ConsumerPortals" runat="server" OnItemDataBound="BindConsumerPortalItem">
                <Columns>
                    <asp:BoundColumn DataField="Name" HeaderText="Consumer Portal Name"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Consumer Portal iframe URLs">
                        <ItemTemplate>
                            <ml:EncodedLabel runat="server" id="RateWatchLabel" style="font-weight:bold">Rate Watch Widget:</ml:EncodedLabel>
                            <ml:EncodedLabel id="RateWatchURL" runat="server"></ml:EncodedLabel>&nbsp;
                            <asp:LinkButton href="javascript:void(0)" id="CopyRateWatchURL" runat="server">copy link</asp:LinkButton>
                            <br />
                            <ml:EncodedLabel runat="server" id="RatesLabel" style="font-weight:bold">Get Rates:</ml:EncodedLabel>
                            <ml:EncodedLabel id="GetRatesURL" runat="server"></ml:EncodedLabel>&nbsp;
                            <asp:LinkButton href="javascript:void(0)" id="CopyGetRatesURL" runat="server">copy link</asp:LinkButton>
                            <% if (IsNewPMLUIEnabled){%><br /><%} %>
                            <label style="font-weight:bold">Borrower Login:</label>&nbsp;
                            <ml:EncodedLabel id="BorrowerLoginURL" runat="server"></ml:EncodedLabel>&nbsp;
                            <asp:LinkButton href="javascript:void(0)" id="CopyBorrowerLoginURL" runat="server">copy link</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderStyle-Width="60px">
                        <ItemTemplate>
                            <asp:LinkButton runat="server"  href="javascript:void(0)" ID="ConfigureBtn" style="margin:5px">configure</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </ml:CommonDataGrid>
        </div>
    </form>
</body>
</html>
