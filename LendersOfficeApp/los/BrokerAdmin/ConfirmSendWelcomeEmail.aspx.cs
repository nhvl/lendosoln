using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOffice.Security;
using LendersOffice.Events;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.los.BrokerAdmin
{
	
	public partial class ConfirmSendWelcomeEmail : BasePage
	{
		
		protected void PageLoad(object sender, System.EventArgs e)
		{
			m_EmailAddress.Text = (Request["email"] != null)?Request["email"]:"user";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
