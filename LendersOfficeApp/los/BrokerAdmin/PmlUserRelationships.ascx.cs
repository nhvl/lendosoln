﻿namespace LendersOfficeApp.los.BrokerAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using admin;
    using common;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.BrokerAdmin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public partial class PmlUserRelationships : UserControl
    {
        private static Func<OriginatingCompanyChannelMode, bool> visibleForBroker = 
            (mode) => mode == OriginatingCompanyChannelMode.Broker;

        private static Func<OriginatingCompanyChannelMode, bool> visibleForBothCorrespondent = 
            (mode) => mode == OriginatingCompanyChannelMode.Correspondent 
                || mode == OriginatingCompanyChannelMode.MiniCorrespondent;
        
        private EmployeeDB privateEmployeeDB;

        public OriginatingCompanyChannelMode Mode { get; set; }

        public bool IsForOcPage { get; set; }
        
        public PmlBroker PmlBroker { private get; set; }

        protected string UserType
        {
            get
            {
                return this.IsForOcPage ?
                    "by users of this company" :
                    "by this user";
            }
        }

        private Guid BrokerId
        {
            get { return PrincipalFactory.CurrentPrincipal.BrokerId; }
        }

        private Guid EmployeeId
        {
            get
            {
                var employeeId = RequestHelper.GetSafeQueryString("employeeId");

                // We're adding an employee
                if (employeeId == null)
                {
                    return Guid.Empty;
                }

                return new Guid(employeeId);
            }
        }

        private EmployeeDB Employee 
        {
            get
            {
                if (this.privateEmployeeDB == null)
                {
                    this.privateEmployeeDB = EmployeeDB.RetrieveById(
                        this.BrokerId, 
                        this.EmployeeId);
                }

                return this.privateEmployeeDB;
            }
            set
            {
                this.privateEmployeeDB = value;
            }
        }

        // If we're adding a new employee (the employee ID query string is Guid.Empty),
        // we need to pull directly from the OC on save since the pickers will
        // not be bound on page load.
        public void ApplySettingsToEmployee(EmployeeDB employee, bool hasChangedOriginatingCompanies)
        {
            var populationType = (EmployeePopulationMethodT)Tools.GetDropDownListValue(
                this.UseRelationshipDdl);

            switch (this.Mode)
            {
                case OriginatingCompanyChannelMode.Broker:
                    if (this.EmployeeId == Guid.Empty || hasChangedOriginatingCompanies)
                    {
                        this.Employee = employee;

                        this.PmlBroker = PmlBroker.RetrievePmlBrokerById(employee.PmlBrokerId, this.BrokerId);

                        this.BindBrokerRelationships(
                            useOcSettings: populationType == EmployeePopulationMethodT.OriginatingCompany);
                    }

                    employee.PopulateBrokerRelationshipsT = populationType;

                    this.ApplyBrokerSettings(employee);
                    break;
                case OriginatingCompanyChannelMode.Correspondent:
                    if (this.EmployeeId == Guid.Empty || hasChangedOriginatingCompanies)
                    {
                        this.Employee = employee;

                        this.PmlBroker = PmlBroker.RetrievePmlBrokerById(employee.PmlBrokerId, this.BrokerId);

                        this.BindCorrespondentRelationships(
                            useOcSettings: populationType == EmployeePopulationMethodT.OriginatingCompany);
                    }

                    employee.PopulateCorrRelationshipsT = populationType;

                    this.ApplyCorrespondentSettings(employee);
                    break;
                case OriginatingCompanyChannelMode.MiniCorrespondent:
                    if (this.EmployeeId == Guid.Empty || hasChangedOriginatingCompanies)
                    {
                        this.Employee = employee;

                        this.PmlBroker = PmlBroker.RetrievePmlBrokerById(employee.PmlBrokerId, this.BrokerId);

                        this.BindMiniCorrespondentRelationships(
                            useOcSettings: populationType == EmployeePopulationMethodT.OriginatingCompany);
                    }

                    employee.PopulateMiniCorrRelationshipsT = populationType;

                    this.ApplyMiniCorrespondentSettings(employee);
                    break;
                default:
                    throw new UnhandledEnumException(this.Mode);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            var basePage = this.Page as BasePage;

            if (basePage == null)
            {
                throw CBaseException.GenericException(
                    "This control is only intended for use on a BasePage.");
            }

            basePage.RegisterJsScript("pmluserrelationships.js");

            this.SetUpEmployeeRelationships();

            if (!this.IsForOcPage)
            {
                this.LoadBranches();

                this.LoadPriceGroups();

                this.LoadLandingPages();

                Tools.BindUsePmlUserOCDropdown(this.UseRelationshipDdl);

                // New PML users are set to populate internal relationships from
                // their OC. Because an OC will not be associated until the new user
                // is saved, we want to hide the relationship pickers.
                if (this.EmployeeId == Guid.Empty)
                {
                    this.InternalUsersPanel.Visible = false;
                }
            }
            else
            {
                this.ExternalUsersPanel.Visible = false;

                this.UseRelationshipDropdownPanel.Visible = false;

                this.BranchPriceGroupLandingPagePanel.Visible = false;
            }

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsForOcPage && !this.Page.IsPostBack && this.EmployeeId != Guid.Empty)
            {
                switch (this.Mode)
                {
                    case OriginatingCompanyChannelMode.Broker:
                        this.BindBrokerRelationships(
                            useOcSettings: this.Employee.PopulateBrokerRelationshipsT == EmployeePopulationMethodT.OriginatingCompany);
                        break;
                    case OriginatingCompanyChannelMode.Correspondent:
                        this.BindCorrespondentRelationships(
                            useOcSettings: this.Employee.PopulateCorrRelationshipsT == EmployeePopulationMethodT.OriginatingCompany);
                        break;
                    case OriginatingCompanyChannelMode.MiniCorrespondent:
                        this.BindMiniCorrespondentRelationships(
                            useOcSettings: this.Employee.PopulateMiniCorrRelationshipsT == EmployeePopulationMethodT.OriginatingCompany);
                        break;
                    default:
                        throw new UnhandledEnumException(this.Mode);
                }
            }
        }

        protected void UseRelationshipDdl_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddlSender = sender as DropDownList;

            var populationMethod = (EmployeePopulationMethodT)Tools.GetDropDownListValue(ddlSender);

            var useOcSettings = populationMethod == EmployeePopulationMethodT.OriginatingCompany;

            if (this.EmployeeId != Guid.Empty)
            {
                // We need to retrieve the PML broker between postbacks, as we 
                // cannot store the broker in the view state.
                this.PmlBroker = PmlBroker.RetrievePmlBrokerById(this.Employee.PmlBrokerId, this.BrokerId);

                switch (this.Mode)
                {
                    case OriginatingCompanyChannelMode.Broker:
                        this.BindBrokerRelationships(useOcSettings);
                        break;
                    case OriginatingCompanyChannelMode.MiniCorrespondent:
                        this.BindMiniCorrespondentRelationships(useOcSettings);
                        break;
                    case OriginatingCompanyChannelMode.Correspondent:
                        this.BindCorrespondentRelationships(useOcSettings);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                // For new PML users, we want to hide the pickers if the user is
                // new and the "Use OC" setting is selected in the dropdown.
                this.InternalUsersPanel.Visible = !useOcSettings;
            }
        }

        private void BindEmployeeRelationship(EmployeeRoleChooserLink link, Guid employeeID, string name, bool useOcSettings)
        {
            if (useOcSettings || employeeID != Guid.Empty)
            {
                link.Data = employeeID.ToString();
                link.Text = name;
            }

            link.PickerVisible = !useOcSettings;
        }

        private void BindBranchRelationship(bool useOriginatingCompanyBranch, Guid branchID)
        {
            if (useOriginatingCompanyBranch)
            {
                this.branch.SelectedValue = "OC";
            }
            else
            {
                for (int i = 0; i < this.branch.Items.Count; ++i)
                {
                    if (this.branch.Items[i].Value.ToLower() == branchID.ToString().ToLower())
                    {
                        this.branch.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        private void BindPriceGroupRelationship(bool useOriginatingCompanyPriceGroup, Guid priceGroupID)
        {
            if (useOriginatingCompanyPriceGroup || RequestHelper.GetSafeQueryString("employeeId") == null)
            {
                this.priceGroup.SelectedValue = "OC";
            }
            else
            {
                if (this.priceGroup.Items.FindByValue(priceGroupID.ToString()) == null)
                {
                    this.priceGroup.Items.Add(new ListItem("Current (disabled)", priceGroupID.ToString()));
                }

                this.priceGroup.SelectedIndex = this.priceGroup.Items.IndexOf(
                    this.priceGroup.Items.FindByValue(priceGroupID.ToString()));
            }
        }

        private void BindLandingPageRelationship(Guid? landingPageID)
        {
            if (landingPageID.HasValue && this.landingPage.Items.FindByValue(landingPageID.ToString()) == null)
            {
                this.landingPage.Items.Add(new ListItem("Current (disabled)", landingPageID.ToString()));
            }

            if (false == landingPageID.HasValue)
            {
                this.landingPage.SelectedValue = "none";
            }
            else
            {
                this.landingPage.SelectedValue = landingPageID.Value.ToString();
            }
        }

        private void BindBrokerRelationships(bool useOcSettings)
        {
            this.BindEmployeeRelationship(
                this.manager,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.Manager].Id : this.Employee.ManagerEmployeeID,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.Manager].Name : this.Employee.ManagerFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.lockDesk,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.LockDesk].Id : this.Employee.LockDeskEmployeeID,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.LockDesk].Name : this.Employee.LockDeskFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.lenderAccountExec,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.LenderAccountExecutive].Id : this.Employee.LenderAcctExecEmployeeID,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.LenderAccountExecutive].Name : this.Employee.LenderAccExecFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.underwriter,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.Underwriter].Id : this.Employee.UnderwriterEmployeeID,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.Underwriter].Name : this.Employee.UnderwriterFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.juniorUnderwriter,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.JuniorUnderwriter].Id : this.Employee.JuniorUnderwriterEmployeeID,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.JuniorUnderwriter].Name : this.Employee.JuniorUnderwriterFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.processor,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.Processor].Id : this.Employee.ProcessorEmployeeID,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.Processor].Name : this.Employee.ProcessorFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.juniorProcessor,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.JuniorProcessor].Id : this.Employee.JuniorProcessorEmployeeID,
                useOcSettings ? this.PmlBroker.BrokerRoleRelationships[E_RoleT.JuniorProcessor].Name : this.Employee.JuniorProcessorFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.brokerProcessor,
                this.Employee.BrokerProcessorEmployeeId,
                this.Employee.BrokerProcessorFullName,
                useOcSettings: false);

            this.BindBranchRelationship(
                this.Employee.UseOriginatingCompanyBranchId,
                this.Employee.BranchID);

            this.BindPriceGroupRelationship(
                this.Employee.UseOriginatingCompanyLpePriceGroupId,
                this.Employee.LpePriceGroupID);

            this.BindLandingPageRelationship(
                this.Employee.TPOLandingPageID);

            Tools.SetDropDownListValue(
                this.UseRelationshipDdl,
                useOcSettings ? EmployeePopulationMethodT.OriginatingCompany : EmployeePopulationMethodT.IndividualUser);
        }

        private void BindMiniCorrespondentRelationships(bool useOcSettings)
        {
            this.BindEmployeeRelationship(
                this.manager,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.Manager].Id : this.Employee.MiniCorrManagerEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.Manager].Name : this.Employee.MiniCorrManagerFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.processor,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.Processor].Id : this.Employee.MiniCorrProcessorEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.Processor].Name : this.Employee.MiniCorrProcessorFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.juniorProcessor,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.JuniorProcessor].Id : this.Employee.MiniCorrJuniorProcessorEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.JuniorProcessor].Name : this.Employee.MiniCorrJuniorProcessorFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.lenderAccountExec,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.LenderAccountExecutive].Id : this.Employee.MiniCorrLenderAccExecEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.LenderAccountExecutive].Name : this.Employee.MiniCorrLenderAccExecFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.underwriter,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.Underwriter].Id : this.Employee.MiniCorrUnderwriterEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.Underwriter].Name : this.Employee.MiniCorrUnderwriterFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.juniorUnderwriter,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.JuniorUnderwriter].Id : this.Employee.MiniCorrJuniorUnderwriterEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.JuniorUnderwriter].Name : this.Employee.MiniCorrJuniorUnderwriterFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.creditAuditor,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.CreditAuditor].Id : this.Employee.MiniCorrCreditAuditorEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.CreditAuditor].Name : this.Employee.MiniCorrCreditAuditorFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.legalAuditor,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.LegalAuditor].Id : this.Employee.MiniCorrLegalAuditorEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.LegalAuditor].Name : this.Employee.MiniCorrLegalAuditorFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.lockDesk,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.LockDesk].Id : this.Employee.MiniCorrLockDeskEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.LockDesk].Name : this.Employee.MiniCorrLockDeskFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.purchaser,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.Purchaser].Id : this.Employee.MiniCorrPurchaserEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.Purchaser].Name : this.Employee.MiniCorrPurchaserFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.secondary,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.Secondary].Id : this.Employee.MiniCorrSecondaryEmployeeId,
                useOcSettings ? this.PmlBroker.MiniCorrRoleRelationships[E_RoleT.Secondary].Name : this.Employee.MiniCorrSecondaryFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.externalPostCloser,
                this.Employee.MiniCorrExternalPostCloserEmployeeId,
                this.Employee.MiniCorrExternalPostCloserFullName,
                useOcSettings: false);

            this.BindBranchRelationship(
                this.Employee.UseOriginatingCompanyMiniCorrBranchId,
                this.Employee.MiniCorrespondentBranchID);

            this.BindPriceGroupRelationship(
                this.Employee.UseOriginatingCompanyMiniCorrPriceGroupId,
                this.Employee.MiniCorrespondentPriceGroupID);

            this.BindLandingPageRelationship(
                this.Employee.MiniCorrespondentLandingPageID);

            Tools.SetDropDownListValue(
                this.UseRelationshipDdl,
                useOcSettings ? EmployeePopulationMethodT.OriginatingCompany : EmployeePopulationMethodT.IndividualUser);
        }

        private void BindCorrespondentRelationships(bool useOcSettings)
        {
            this.BindEmployeeRelationship(
                this.manager,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.Manager].Id : this.Employee.CorrManagerEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.Manager].Name : this.Employee.CorrManagerFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.processor,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.Processor].Id : this.Employee.CorrProcessorEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.Processor].Name : this.Employee.CorrProcessorFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.juniorProcessor,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.JuniorProcessor].Id : this.Employee.CorrJuniorProcessorEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.JuniorProcessor].Name : this.Employee.CorrJuniorProcessorFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.lenderAccountExec,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.LenderAccountExecutive].Id : this.Employee.CorrLenderAccExecEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.LenderAccountExecutive].Name : this.Employee.CorrLenderAccExecFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.underwriter,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.Underwriter].Id : this.Employee.CorrUnderwriterEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.Underwriter].Name : this.Employee.CorrUnderwriterFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.juniorUnderwriter,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.JuniorUnderwriter].Id : this.Employee.CorrJuniorUnderwriterEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.JuniorUnderwriter].Name : this.Employee.CorrJuniorUnderwriterFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.creditAuditor,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.CreditAuditor].Id : this.Employee.CorrCreditAuditorEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.CreditAuditor].Name : this.Employee.CorrCreditAuditorFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.legalAuditor,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.LegalAuditor].Id : this.Employee.CorrLegalAuditorEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.LegalAuditor].Name : this.Employee.CorrLegalAuditorFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.lockDesk,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.LockDesk].Id : this.Employee.CorrLockDeskEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.LockDesk].Name : this.Employee.CorrLockDeskFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.purchaser,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.Purchaser].Id : this.Employee.CorrPurchaserEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.Purchaser].Name : this.Employee.CorrPurchaserFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.secondary,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.Secondary].Id : this.Employee.CorrSecondaryEmployeeId,
                useOcSettings ? this.PmlBroker.CorrRoleRelationships[E_RoleT.Secondary].Name : this.Employee.CorrSecondaryFullName,
                useOcSettings);

            this.BindEmployeeRelationship(
                this.externalPostCloser,
                employeeID: this.Employee.CorrExternalPostCloserEmployeeId,
                name: this.Employee.CorrExternalPostCloserFullName,
                useOcSettings: false);

            this.BindBranchRelationship(
                this.Employee.UseOriginatingCompanyCorrBranchId,
                this.Employee.CorrespondentBranchID);

            this.BindPriceGroupRelationship(
                this.Employee.UseOriginatingCompanyCorrPriceGroupId,
                this.Employee.CorrespondentPriceGroupID);

            this.BindLandingPageRelationship(
                this.Employee.CorrespondentLandingPageID);

            Tools.SetDropDownListValue(
                this.UseRelationshipDdl,
                useOcSettings ? EmployeePopulationMethodT.OriginatingCompany : EmployeePopulationMethodT.IndividualUser);
        }

        private void SetUpEmployeeRelationships()
        {
            var roleChooserLinkInfo = new List<RoleChooserLinkInfo>()
            {
                new RoleChooserLinkInfo(this.manager, this.managerRow, E_RoleT.Manager),
                new RoleChooserLinkInfo(this.processor, this.processorRow, E_RoleT.Processor),
                new RoleChooserLinkInfo(this.juniorProcessor, this.juniorProcessorRow, E_RoleT.JuniorProcessor),
                new RoleChooserLinkInfo(this.lenderAccountExec, this.lenderAccountExecRow, E_RoleT.LenderAccountExecutive, null, "account exec"),
                new RoleChooserLinkInfo(this.brokerProcessor, this.brokerProcessorRow, E_RoleT.Pml_BrokerProcessor, visibleForBroker),
                new RoleChooserLinkInfo(this.externalPostCloser, this.externalPostCloserRow, E_RoleT.Pml_PostCloser, visibleForBothCorrespondent),
                new RoleChooserLinkInfo(this.underwriter, this.underwriterRow, E_RoleT.Underwriter),
                new RoleChooserLinkInfo(this.juniorUnderwriter, this.juniorUnderwriterRow, E_RoleT.JuniorUnderwriter),
                new RoleChooserLinkInfo(this.creditAuditor, this.creditAuditorRow, E_RoleT.CreditAuditor, visibleForBothCorrespondent),
                new RoleChooserLinkInfo(this.legalAuditor, this.legalAuditorRow, E_RoleT.LegalAuditor, visibleForBothCorrespondent),
                new RoleChooserLinkInfo(this.lockDesk, this.lockDeskRow, E_RoleT.LockDesk),
                new RoleChooserLinkInfo(this.purchaser, this.purchaserRow, E_RoleT.Purchaser, visibleForBothCorrespondent),
                new RoleChooserLinkInfo(this.secondary, this.secondaryRow, E_RoleT.Secondary, visibleForBothCorrespondent)
            };

            foreach (var link in roleChooserLinkInfo)
            {
                link.ParentRow.Visible = link.IsVisibleForMode(this.Mode);

                var control = link.RoleChooserLink;

                var role = Role.Get(link.Role);
                control.Width = "160px";
                control.Text = "None";
                control.Mode = "Search";
                control.Desc = role.ModifiableDesc;
                control.Role = role.Desc;
                control.Data = Guid.Empty.ToString();
                control.EnableCopyToOfficialContact = false;

                var assignText = "pick " + (link.CustomRoleDesc ?? role.ModifiableDesc.ToLower());

                control.Choices.Add(new FixedChoice("None", Guid.Empty.ToString(), "none"));
                control.Choices.Add(new FixedChoice("assign", "assign", assignText));
            }

            this.correspondentPriorApprovedLabel.Visible = this.Mode == OriginatingCompanyChannelMode.Correspondent;
            this.correspondentRegisterLockLabel.Visible = this.Mode == OriginatingCompanyChannelMode.Correspondent;
            this.standardRegisterLockLabel.Visible = this.Mode != OriginatingCompanyChannelMode.Correspondent;

            if (!this.IsForOcPage)
            {
                if (this.Mode == OriginatingCompanyChannelMode.Broker)
                {
                    this.branchPrefix.Text = this.priceGroupPrefix.Text = this.landingPagePrefix.Text = "Broker";
                }
                else if (this.Mode == OriginatingCompanyChannelMode.MiniCorrespondent)
                {
                    this.branchPrefix.Text = this.priceGroupPrefix.Text = this.landingPagePrefix.Text = "Mini-Correspondent";
                }
                else if (this.Mode == OriginatingCompanyChannelMode.Correspondent)
                {
                    this.branchPrefix.Text = this.priceGroupPrefix.Text = this.landingPagePrefix.Text = "Correspondent";
                }
            }
        }

        private void LoadBranches()
        {
            Tools.Bind_Generic<KeyValuePair<Guid, string>>(
                this.branch,
                BrokerDB.RetrieveById(this.BrokerId).GetBranches(),
                kvp => false,
                kvp => kvp.Value,
                kvp => kvp.Key.ToString());

            this.branch.Items.Insert(0, new ListItem("<-- Use Originating Company Setting -->", "OC"));
        }

        private void LoadPriceGroups()
        {
            Tools.Bind_Generic<KeyValuePair<Guid, string>>(
                this.priceGroup,
                BrokerDB.RetrieveById(this.BrokerId).GetPriceGroups(),
                kvp => false,
                kvp => kvp.Value,
                kvp => kvp.Key.ToString());

            this.priceGroup.Items.Insert(0, new ListItem("<-- Use default at branch level -->", Guid.Empty.ToString()));
            this.priceGroup.Items.Insert(0, new ListItem("<--Use Originating Company Setting-->", "OC"));
        }

        private void LoadLandingPages()
        {
            if (this.landingPage.Items.Count == 0)
            {
                this.landingPage.Items.Add(new ListItem("<--Use Originating Company Setting-->", "none"));

                Tools.Bind_Generic<KeyValuePair<Guid, string>>(
                    this.landingPage,
                    BrokerDB.RetrieveById(this.BrokerId).GetLandingPages(),
                    kvp => false,
                    kvp => kvp.Value,
                    kvp => kvp.Key.ToString());
            }
        }

        private void ApplyBrokerSettings(EmployeeDB employee)
        {
            employee.ManagerEmployeeID = new Guid(this.manager.Data);
            employee.ProcessorEmployeeID = new Guid(this.processor.Data);
            employee.JuniorProcessorEmployeeID = new Guid(this.juniorProcessor.Data);
            employee.LenderAcctExecEmployeeID = new Guid(this.lenderAccountExec.Data);
            employee.BrokerProcessorEmployeeId = new Guid(this.brokerProcessor.Data);
            employee.UnderwriterEmployeeID = new Guid(this.underwriter.Data);
            employee.JuniorUnderwriterEmployeeID = new Guid(this.juniorUnderwriter.Data);
            employee.LockDeskEmployeeID = new Guid(this.lockDesk.Data);

            if (this.branch.SelectedValue == "OC")
            {
                employee.UseOriginatingCompanyBranchId = true;
            }
            else
            {
                employee.BranchID = new Guid(this.branch.SelectedItem.Value);
            }

            if (this.priceGroup.SelectedValue == "OC")
            {
                employee.UseOriginatingCompanyLpePriceGroupId = true;
            }
            else
            {
                employee.LpePriceGroupID = new Guid(this.priceGroup.SelectedItem.Value);
            }

            if (this.landingPage.SelectedValue == "none")
            {
                employee.TPOLandingPageID = null;
            }
            else
            {
                employee.TPOLandingPageID = new Guid(this.landingPage.SelectedValue);
            }

            employee.PopulateBrokerRelationshipsT = (EmployeePopulationMethodT)Tools.GetDropDownListValue(
                this.UseRelationshipDdl);
        }

        private void ApplyCorrespondentSettings(EmployeeDB employee)
        {
            employee.CorrManagerEmployeeId = new Guid(this.manager.Data);
            employee.CorrProcessorEmployeeId = new Guid(this.processor.Data);
            employee.CorrJuniorProcessorEmployeeId = new Guid(this.juniorProcessor.Data);
            employee.CorrLenderAccExecEmployeeId = new Guid(this.lenderAccountExec.Data);
            employee.CorrExternalPostCloserEmployeeId = new Guid(this.externalPostCloser.Data);
            employee.CorrUnderwriterEmployeeId = new Guid(this.underwriter.Data);
            employee.CorrJuniorUnderwriterEmployeeId = new Guid(this.juniorUnderwriter.Data);
            employee.CorrCreditAuditorEmployeeId = new Guid(this.creditAuditor.Data);
            employee.CorrLegalAuditorEmployeeId = new Guid(this.legalAuditor.Data);
            employee.CorrLockDeskEmployeeId = new Guid(this.lockDesk.Data);
            employee.CorrPurchaserEmployeeId = new Guid(this.purchaser.Data);
            employee.CorrSecondaryEmployeeId = new Guid(this.secondary.Data);

            if (this.branch.SelectedValue == "OC")
            {
                employee.UseOriginatingCompanyCorrBranchId = true;
            }
            else
            {
                employee.CorrespondentBranchID = new Guid(this.branch.SelectedItem.Value);
            }

            if (this.priceGroup.SelectedValue == "OC")
            {
                employee.UseOriginatingCompanyCorrPriceGroupId = true;
            }
            else
            {
                employee.CorrespondentPriceGroupID = new Guid(this.priceGroup.SelectedItem.Value);
            }

            if (this.landingPage.SelectedValue == "none")
            {
                employee.CorrespondentLandingPageID = null;
            }
            else
            {
                employee.CorrespondentLandingPageID = new Guid(this.landingPage.SelectedValue);
            }

            employee.PopulateCorrRelationshipsT = (EmployeePopulationMethodT)Tools.GetDropDownListValue(
                this.UseRelationshipDdl);
        }

        private void ApplyMiniCorrespondentSettings(EmployeeDB employee)
        {
            employee.MiniCorrManagerEmployeeId = new Guid(this.manager.Data);
            employee.MiniCorrProcessorEmployeeId = new Guid(this.processor.Data);
            employee.MiniCorrJuniorProcessorEmployeeId = new Guid(this.juniorProcessor.Data);
            employee.MiniCorrLenderAccExecEmployeeId = new Guid(this.lenderAccountExec.Data);
            employee.MiniCorrExternalPostCloserEmployeeId = new Guid(this.externalPostCloser.Data);
            employee.MiniCorrUnderwriterEmployeeId = new Guid(this.underwriter.Data);
            employee.MiniCorrJuniorUnderwriterEmployeeId = new Guid(this.juniorUnderwriter.Data);
            employee.MiniCorrCreditAuditorEmployeeId = new Guid(this.creditAuditor.Data);
            employee.MiniCorrLegalAuditorEmployeeId = new Guid(this.legalAuditor.Data);
            employee.MiniCorrLockDeskEmployeeId = new Guid(this.lockDesk.Data);
            employee.MiniCorrPurchaserEmployeeId = new Guid(this.purchaser.Data);
            employee.MiniCorrSecondaryEmployeeId = new Guid(this.secondary.Data);

            if (this.branch.SelectedValue == "OC")
            {
                employee.UseOriginatingCompanyMiniCorrBranchId = true;
            }
            else
            {
                employee.MiniCorrespondentBranchID = new Guid(this.branch.SelectedItem.Value);
            }

            if (this.priceGroup.SelectedValue == "OC")
            {
                employee.UseOriginatingCompanyMiniCorrPriceGroupId = true;
            }
            else
            {
                employee.MiniCorrespondentPriceGroupID = new Guid(this.priceGroup.SelectedItem.Value);
            }

            if (this.landingPage.SelectedValue == "none")
            {
                employee.MiniCorrespondentLandingPageID = null;
            }
            else
            {
                employee.MiniCorrespondentLandingPageID = new Guid(this.landingPage.SelectedValue);
            }

            employee.PopulateMiniCorrRelationshipsT = (EmployeePopulationMethodT)Tools.GetDropDownListValue(
                this.UseRelationshipDdl);
        }

        private struct RoleChooserLinkInfo
        {
            public readonly EmployeeRoleChooserLink RoleChooserLink;
            public readonly HtmlTableRow ParentRow;
            public readonly E_RoleT Role;
            public readonly Func<OriginatingCompanyChannelMode, bool> IsVisibleForMode;
            public readonly string CustomRoleDesc;

            public RoleChooserLinkInfo(
                EmployeeRoleChooserLink link,
                HtmlTableRow parentRow,
                E_RoleT role)
            {
                this.RoleChooserLink = link;
                this.ParentRow = parentRow;
                this.Role = role;
                this.IsVisibleForMode = (mode) => true;
                this.CustomRoleDesc = null;
            }

            public RoleChooserLinkInfo(
                EmployeeRoleChooserLink link,
                HtmlTableRow parentRow,
                E_RoleT role,
                Func<OriginatingCompanyChannelMode, bool> isVisibleForMode)
            {
                this.RoleChooserLink = link;
                this.ParentRow = parentRow;
                this.Role = role;
                this.IsVisibleForMode = isVisibleForMode;
                this.CustomRoleDesc = null;
            }

            public RoleChooserLinkInfo(
                EmployeeRoleChooserLink link,
                HtmlTableRow parentRow,
                E_RoleT role,
                Func<OriginatingCompanyChannelMode, bool> isVisibleForMode,
                string customLinkDesc)
            {
                this.RoleChooserLink = link;
                this.ParentRow = parentRow;
                this.Role = role;

                if (isVisibleForMode == null)
                {
                    this.IsVisibleForMode = (mode) => true;
                }
                else
                {
                    this.IsVisibleForMode = isVisibleForMode;
                }

                this.CustomRoleDesc = customLinkDesc;
            }
        }
    }
}