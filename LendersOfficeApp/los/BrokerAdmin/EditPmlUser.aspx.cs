namespace LendersOfficeApp.los.BrokerAdmin
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.CustomPmlFields;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;
    using MeridianLink.CommonControls;

    public partial class EditPmlUser : LendersOffice.Common.BaseServicePage
	{
		#region Variables
 //OPM 2703
 //OPM 2703
 // OPM 12700
		// 07/21/06 mf - OPM 6125. People are causing deadlocks by repeatedly
		// clicking Apply or Ok while the update is occuring.  We must make
		// them wait until their action finishes.
		// 06/07/06 mf - OPM 2642.  Made the supervisor choosing process easier.
		protected Boolean								expirationPeriodDisabled = false;
		protected Boolean								cycleExpirationDisabled = false;
		protected Boolean								expirationDateDisabled = false;
		protected bool									m_isNew = false;
		protected Guid									empId;
		protected HtmlInputButton				        m_ApplyHidden;
		protected HtmlInputButton				        m_OkHidden;
		private Boolean									m_IsInternal = false;
		public bool										m_EnableLogin;
		protected Button							    m_Add;
		protected CommonDataGrid				        m_Grid;
		protected TextBox							    License;
		protected StateDropDownList			        	State;
		protected Label							        m_ErrorMsg;
		protected HtmlInputButton			        	m_AddSeen;
		protected DateTextBox					        ExpirationDate;
		protected bool									m_isDUEnabled = false;
		protected bool									m_isDOEnabled = false;
        private bool                                    m_isPmlCompanySet = false;
        protected string m_ZeroDollarStr;
        private LosConvert m_losConvert = new LosConvert();
        
 //11/29/07 db - OPM 19127
 //11/29/07 db - OPM 19127
 //11/29/07 db - OPM 19127
 //11/29/07 db - OPM 19127

 // OPM 20518
 // OPM 20518
 //19481
		protected string													m_sPmlAutoLoginOption;
		#endregion
		
		
		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}

		private String IsDisabled
		{
			set { ClientScript.RegisterHiddenField( "m_isDisabled" , value ); }
		}

		private String ErrorMessage
		{
			set { ClientScript.RegisterHiddenField( "m_errorMessage" , value ); }
		}

		private String CommandToDo
		{
			set { ClientScript.RegisterHiddenField( "m_commandToDo" , value ); }
		}

		private String ActionToDo
		{
			set { ClientScript.RegisterHiddenField( "m_actionToDo" , value ); }
		}

		public Boolean IsInternal
		{
			set { m_IsInternal = value; }
			get { return m_IsInternal; }
		}

        protected Guid m_EmployeeId
        {
            get { return new Guid(ViewState["employeeId"].ToString()); }
        }

        protected Guid UserId
        {
            get
            {
                return new Guid(ViewState["userid"].ToString());
            }
        }

        private Guid pmlBrokerId
        {
            get
            {
                Guid _pmlBrokerId = Guid.Empty;
                string sPmlBrokerId = RequestHelper.GetSafeQueryString("pmlBrokerId");
                if (!string.IsNullOrEmpty(sPmlBrokerId))
                {
                    try{ _pmlBrokerId = new Guid(sPmlBrokerId); } catch{}
                }
                return _pmlBrokerId;
            }
        }

        protected Guid BrokerId
        {
            get
            {
                return CurrentUser.BrokerId;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            var serviceCredentials = ServiceCredential.GetAllEmployeeServiceCredentials(this.BrokerId, m_EmployeeId, ServiceCredentialService.All);
            var serviceCredentialsJson = SerializationHelper.JsonNetAnonymousSerialize(serviceCredentials);
            Page.ClientScript.RegisterHiddenField("ServiceCredentialsJson", serviceCredentialsJson);
            Page.ClientScript.RegisterHiddenField("EmployeeId", m_EmployeeId.ToString());
            Page.ClientScript.RegisterHiddenField("IsNewEmployee", m_isNew.ToTrueFalse());
            Page.ClientScript.RegisterHiddenField("EmployeeType", UserType.TPO.ToString("D"));
            Page.ClientScript.RegisterHiddenField("UserId", this.UserId.ToString());
        }

        /// <summary>
		/// Initialize this page.
		/// </summary>
		protected void PageInit( object sender , System.EventArgs a ) 
		{
            Tools.BindSuffix(m_Suffix);
            RegisterJsScript("LQBPopup.js");
            RegisterJsScript("ServiceCredential.js");
            RegisterCSS("ServiceCredential.css");

            // Initialize the zipcode set.
            m_Zipcode.SmartZipcode( m_City , m_State );
			this.RegisterService("main", "/los/BrokerAdmin/PasswordValidationService.aspx");
			this.RegisterService("utilities", "/common/UtilitiesService.aspx" );
            this.RegisterService("editpmluser", "/los/BrokerAdmin/EditPmlUserService.aspx");

            RegularExpressionValidator1.ValidationExpression = ConstApp.EmailValidationExpression;

            if (BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).IsCustomPricingPolicyFieldEnabled)
            {
                CustomLOPricingPolicyPlaceHolder.Visible = true;
                Tools.Bind_PricingPolicyFieldValueSourceForPmlUser(PricingPolicyFieldValueSource);
                BuildLoanOfficerPricingPolicyTable();
            }
            else
            {
                CustomLOPricingPolicyPlaceHolder.Visible = false;
            }

            Tools.BindUsePmlUserOCDropdown(this.UsePermissionDdl);
        }

		/// <summary>
		/// Set variables that allow the UI to know which controls to disable/enable so that
		/// postbacks are not always necessary just for UI changes
		/// </summary>
		private void initPwUI(EmployeeDB employee)
		{	
			cycleExpirationDisabled = (m_PasswordNeverExpires.Checked)?true:false;
			expirationPeriodDisabled = (m_CycleExpiration.Checked)?false:true;
			expirationDateDisabled = (m_PasswordExpiresOn.Checked)?false:true;
            m_OriginatorCompensationAuditGrid.DataSource = employee.OriginatorCompensationAuditData.ParseXml(employee.BrokerID);
            m_OriginatorCompensationAuditGrid.AllowPaging = employee.OriginatorCompensationAuditData.ParseXml(employee.BrokerID).Count > 10;
            m_OriginatorCompensationAuditDoesntExist.Visible = employee.OriginatorCompensationAuditData.ParseXml(employee.BrokerID).Count == 0;
            m_OriginatorCompensationAuditGrid.DataBind();
		}

		/// <summary>
		/// On Postback after an Apply command, we don't need to rebind all the fields because they will be displayed
		/// as they were saved.  Some fields, however, might need to be bound, such as the Login field
		/// </summary>
		private void RebindLoginName(string loginName)
		{
			m_Login.Text = loginName;
		}


        private void DeleteRegisteredIp(int id)
        {
            EmployeeDB employee = EmployeeDB.RetrieveById(BrokerId, m_EmployeeId);
            UserRegisteredIp.Delete(employee.UserID, CurrentUser.UserId, id);
        }

        private void RevokeClientCertificate(Guid certificateId)
        {
            EmployeeDB employee = EmployeeDB.RetrieveById(BrokerId, m_EmployeeId);
            ClientCertificate.Delete(BrokerId, employee.UserID, certificateId);
        }

		/// <summary>
		/// Initialize page.
		/// </summary>

		protected void PageLoad( object sender , System.EventArgs a )
		{
            // We always want this to happen regardless of whether or not it was a post back.
            // Otherwise, it might end up as an empty string and cause weird behavior client side.
            m_ZeroDollarStr = m_losConvert.ToMoneyString(0.0M, FormatDirection.ToRep);

            if (Page.IsPostBack)
            {
                string target = this.Request.Form.Get("__EVENTTARGET");
                if (target == "ClientCertificateRevoke")
                {
                    Guid certificateId = new Guid(this.Request.Form["__EVENTARGUMENT"]);
                    RevokeClientCertificate(certificateId);
                }
                else if (target == "RegisteredIpDelete")
                {
                    int id = int.Parse(this.Request.Form["__EVENTARGUMENT"]);
                    DeleteRegisteredIp(id);
                }
            }

			//Test for access permission
            if (CurrentUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) && CurrentUser.HasPermission(Permission.CanAdministrateExternalUsers))
			{
				m_AccessAllowedPanel.Visible = true;
				m_AccessDeniedPanel.Visible = false;
				IsDisabled = "False";
			}
			else
			{
				m_AccessAllowedPanel.Visible = false;
				m_AccessDeniedPanel.Visible = true;
				IsDisabled = "True";
				return;
			}

            // Load up pml employee details.  Afterwards, we load up
            // the current permission set.

            empId = Guid.Empty;

            var queryString = RequestHelper.GetSafeQueryString("cmd");

            if (queryString != null && queryString.Equals("edit", StringComparison.OrdinalIgnoreCase))
            {
                // Edit an existing employee.  This is a Price My Loan
                // employee that we pull from the db as a regular employee.

                m_TitleBar.Text = "Edit Originating Company User";

                string employeeId = "";
                try
                {
                    employeeId = RequestHelper.GetSafeQueryString("employeeId");
                    empId = new Guid(employeeId);
                }
                catch (Exception)
                {
                    throw new CBaseException(ErrorMessages.FailedToLoadUser, "Unable to calculate employee's id for " + employeeId);
                }
            }

            if (pmlBrokerId != Guid.Empty)
            {
                PmlBroker pb = PmlBroker.RetrievePmlBrokerById(pmlBrokerId, CurrentUser.BrokerId);

                m_CompanyChoice.Text = pb.Name;
                m_CompanyChoice.Data = pmlBrokerId.ToString();

                if (empId == Guid.Empty && IsPostBack == false)
                {
                    // 4/7/2010 dd - OPM 32130 - Auto populate PML Broker info to new PML user.
                    m_Address.Text = pb.Addr;
                    m_City.Text = pb.City;
                    m_State.Value = pb.State;
                    m_Zipcode.Text = pb.Zip;
                }
            }
            

			// Get branch info from database.  We initialize the
			// address to that of the first branch.
			try
			{
                EmployeeDB employee = new EmployeeDB(empId, CurrentUser.BrokerId);
                EmployeeRoles roles = new EmployeeRoles(CurrentUser.BrokerId, empId);
                if (empId != Guid.Empty)
                {
                    if (employee.Retrieve() == false)
                    {
                        throw new CBaseException(ErrorMessages.FailedToLoadUser, ErrorMessages.FailedToLoadUser);
                    }
                }

				if( IsPostBack == false )
				{
					//record whether or not this user is new or not (needed for UI)
					m_isNew = ( empId == Guid.Empty );
                    
					
					//11/29/07 db - OPM 19127
					BrokerDB brokerDB = BrokerDB.RetrieveById(CurrentUser.BrokerId);
                    m_QuickPricer.Visible = brokerDB.IsQuickPricerEnable;
                    m_qpYes.Attributes.Add("onclick", "document.getElementById(" + LendersOffice.AntiXss.AspxTools.JsString(m_qpNo.ClientID) + ").checked = false;");
                    m_qpNo.Attributes.Add("onclick", "return OnNoClick(" + LendersOffice.AntiXss.AspxTools.JsString(m_qpYes.ClientID) + ");");

                    m_qpurlcopy.OnClientClick = "IEOnlyJsUtilities.CopyToClipboard(" + LendersOffice.AntiXss.AspxTools.JsString(m_anonymousQuickPricerUrl.ClientID) + ");return false;";

					m_sPmlAutoLoginOption = brokerDB.PmlUserAutoLoginOption; 
					if ( m_sPmlAutoLoginOption != "Off"  ) 
					{
						m_isDUEnabled = brokerDB.IsPmlDuEnabled && brokerDB.UsingLegacyDUCredentials;
						m_isDOEnabled = brokerDB.IsPmlDoEnabled;
					}
					ViewState.Add( "isDuEnabled" , m_isDUEnabled );
					ViewState.Add( "isDoEnabled" , m_isDOEnabled );
                    ViewState.Add( "brokerId", brokerDB.BrokerID ); 
					ViewState.Add( "pmlautologin", brokerDB.PmlUserAutoLoginOption );

                    if (m_ddlOrigiantorCompenationLevelT.Items.Count == 0)
                    {
                        Tools.Bind_OrigiantorCompenationSetLevelT(m_ddlOrigiantorCompenationLevelT);
                    }                   
                    
                    if (m_OriginatorCompensationBaseT.Items.Count == 0)
                    {
                        Tools.Bind_PercentBaseLoanAmountsT(m_OriginatorCompensationBaseT);
                    }

                    // 8/23/2004 kb - We added the lending account exec role
                    // for price my loan integration.  Each broker user can
                    // be associated to one or no lending account executive
                    // employee.  Beware!  If you are related to an employee
                    // that was a lending account exec, but has since lost
                    // that role, the currently displayed employee will be
                    // the 'none' employee.  On save, we will nix the existing
                    // relationship.
                    //
                    // 10/21/2004 kb - We now do the same thing with a new
                    // employee role: lock desk agent.
                    //
                    // 7/13/2005 kb - For case 2372, we need to put in a
                    // load-all set and pull each role from it.
                    //
                    // 7/22/2005 kb - Reworked role loading to use new
                    // view.  It seems that loading each role individually
                    // is better than loading the whole broker when you
                    // expect to have *many* loan officers because of pml.
                    //
                    // 7/17/06 mf - Per case 6365 we want to avoid loading the
                    // ddls upfront using the BrokerLoanAssignmentTable.  We use the
                    // chooser links instead.

                    // Now determine if we are editing an existing, or
                    // adding a new one to the set.

                    IEnumerable<Tuple<Guid, string>> navigationItems = brokerDB.GetPmlNavigationConfiguration(PrincipalFactory.CurrentPrincipal).GetPageNodeNames();

                    HashSet<Guid> selectedPermissions = new HashSet<Guid>(employee.AccessiblePmlNavigationLinksId);

                    var customPages = navigationItems.Select(item => Tuple.Create(item.Item2/*Name*/, item.Item1/*Id*/, selectedPermissions.Contains(item.Item1)));

                    this.CustomPageRepeater.DataSource = customPages;
                    this.CustomPageRepeater.DataBind();

                    if ( empId != Guid.Empty )
					{
                        m_qpNo.Checked = employee.AnymousQuickPricerAccessId == Guid.Empty;
                        m_qpYes.Checked = !m_qpNo.Checked;
                        m_anonymousQuickPricerUrl.Visible = m_qpYes.Checked;
                        m_anonymousQuickPricerUrl.Text = employee.AnonymousQuickPricerUrl;
                        m_qpurlcopy.Visible = m_anonymousQuickPricerUrl.Visible;

						ViewState.Add( "employeeId" , employee.ID );
						ViewState.Add( "userid", employee.UserID );
						m_FirstName.Text = employee.FirstName;
                        m_MiddleName.Text = employee.MiddleName;
						m_LastName.Text  = employee.LastName;
                        m_Suffix.Text = employee.Suffix;
                        m_NameOnLoanDocLock.Checked = employee.NmOnLoanDocsLckd;
                        if (employee.NmOnLoanDocsLckd)
                        {
                            m_NameOnLoanDoc.ReadOnly = false;
                            m_NameOnLoanDoc.Text = employee.NmOnLoanDocs;
                        }
                        

						m_Notes.Text = employee.NotesByEmployer;
                        m_CommissionPointOfLoanAmount.Text = m_losConvert.ToRateString(employee.CommissionPointOfLoanAmount);
                        m_CommissionPointOfGrossProfit.Text = m_losConvert.ToRateString(employee.CommissionPointOfGrossProfit);
                        m_CommissionMinBase.Text = m_losConvert.ToMoneyString(employee.CommissionMinBase, FormatDirection.ToRep);

                        var pmlBroker = PmlBroker.RetrievePmlBrokerById(employee.PmlBrokerId, this.BrokerId);

                        m_CompanyChoice.Text = pmlBroker.Name;
                        m_CompanyChoice.Data = employee.PmlBrokerId.ToString();
                        ClientScript.RegisterHiddenField("UsersCurrentOc", employee.PmlBrokerId.ToString());
                        ClientScript.RegisterHiddenField("UsersCurrentOcName", pmlBroker.Name);
                        m_isPmlCompanySet = true;

                        this.brokerRelationships.PmlBroker = pmlBroker;
                        this.correspondentRelationships.PmlBroker = pmlBroker;
                        this.miniCorrespondentRelationships.PmlBroker = pmlBroker;
 
						m_Address.Text = employee.Address.StreetAddress;
						m_City.Text    = employee.Address.City;
						m_State.Value  = employee.Address.State;
						m_Zipcode.Text = employee.Address.Zipcode;
						m_Phone.Text   = employee.Phone;
						m_Fax.Text     = employee.Fax;
						m_Cell.Text	   = employee.PrivateCellPhone;
                        this.IsCellphoneForMultiFactorOnly.Checked = employee.IsCellphoneForMultiFactorOnly;
                        m_Pager.Text   = employee.Pager;
						m_Email.Text   = employee.Email;

                        m_AccessIndividual.Checked = false;
                        m_AccessSupervisor.Checked = false;
                        m_AccessCorporate.Checked = false;

                        switch (employee.PmlLevelAccess)
                        {
                            case E_PmlLoanLevelAccess.Individual:
                                m_AccessIndividual.Checked = true;
                                break;
                            case E_PmlLoanLevelAccess.Supervisor:
                                m_AccessSupervisor.Checked = true;
                                break;
                            case E_PmlLoanLevelAccess.Corporate:
                                m_AccessCorporate.Checked = true;
                                break;
                            default:
                                throw new UnhandledEnumException(employee.PmlLevelAccess);
                        }
						
						
						// 11/29/07 db - OPM 19127
						if(m_isDUEnabled)
							m_duLogin.Text = employee.DUAutoLoginName;
						if(m_isDOEnabled)
							m_doLogin.Text = employee.DOAutoLoginName;

						//OPM 1863
						Guid brokerPmlSiteId = Guid.Empty;
                        SqlParameter[] param2 = {
                                                        new SqlParameter("@EmployeeId", employee.ID)
                                                    };

                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerDB.BrokerID, "RetrieveBrokerPmlSiteIdByEmployeeID", param2)) 
						{
							if(reader.Read())
							{
								brokerPmlSiteId = (Guid) reader["BrokerPmlSiteId"];
							}
						}
						m_IsAccountLocked.Value = "false";
						SqlParameter[] parameters1 = {
														new SqlParameter("@LoginNm", employee.LoginName),
														new SqlParameter("@Type", "P"),
														new SqlParameter("@BrokerPmlSiteId", brokerPmlSiteId)
													};
						using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(employee.BrokerID, "GetLoginBlockExpirationDate", parameters1)) 
						{
							if(reader.Read())
							{
								DateTime nextAvailableLoginTime = DateTime.Parse(reader["LoginBlockExpirationD"].ToString());
								if (nextAvailableLoginTime.Equals(SmallDateTime.MaxValue))
									m_IsAccountLocked.Value = "true";
							}
						}

                        this.SetPermissionDisplay(employee, employee.PopulatePMLPermissionsT);

						if( employee.IsActive == true && employee.AllowLogin == true )
						{
							// 10/4/2004 kb - Added display of simplified status.  If
							// the pml user can login, then he is active.  Otherwise,
							// we say he's inactive and force him so if active before
							// saving.  Note: New users default to active.

							m_Active.Checked = true;
							m_Inactive.Checked = false;
						}
						else
						{
							m_Inactive.Checked = true;
							m_Active.Checked = false;
						}                        
                        m_IsBrokerProcessor.Checked = roles.IsInRole(CEmployeeFields.s_BrokerProcessorId);
                        m_IsLoanOfficer.Checked = roles.IsInRole(CEmployeeFields.s_LoanRepRoleId);
                        m_IsExternalSecondary.Checked = roles.IsInRole(E_RoleT.Pml_Secondary);
                        m_IsExternalPostCloser.Checked = roles.IsInRole(E_RoleT.Pml_PostCloser);
                        
						if (employee.PmlExternalManagerEmployeeId != Guid.Empty)
						{
							m_SupervisorChoice.Data = employee.PmlExternalManagerEmployeeId.ToString();
							m_SupervisorChoice.Text = employee.PmlExternalManagerFullName;
						}

						m_ip_parsed.Value = employee.MustLogInFromTheseIpAddresses;

                        if (employee.EnabledIpRestriction)
                        {
                            m_ipRestrictionOn.Checked = true;
                        }
                        else
                        {
                            m_ipRestrictionOff.Checked = true;
                        }

						m_IsSupervisor.Checked = employee.IsPmlManager;
                        this.IsSupervisorOnLoad.Value = employee.IsPmlManager.ToString();

                        if ( employee.IsActive == true && employee.AllowLogin == true )
							m_EnableLogin = true;
						else
							m_EnableLogin = false;

						m_Login.Text = employee.LoginName;

                        Boolean PasswordOption = BrokerDB.RetrieveById(CurrentUser.BrokerId).IsForceChangePasswordForPml;
                        m_MustChangePasswordAtNextLogin.Checked = false;
                        m_PasswordNeverExpires.Checked = false;
                        m_PasswordExpiresOn.Checked = false;
                        m_ExpirationDate.Text = "";

                        if (PasswordOption)
                        {
                            m_MustChangePasswordAtNextLogin.Checked = true;
                            m_PasswordExpiresOn.Enabled = false;
                            m_PasswordNeverExpires.Enabled = false;
                        }
                        else
                        {
                            if (employee.PasswordExpirationD.CompareTo(SmallDateTime.MaxValue) >= 0)
                            {
                                m_PasswordNeverExpires.Checked = true;
                                expirationPeriodDisabled = true;
                                m_CycleExpiration.Checked = false;
                                cycleExpirationDisabled = true;
                                expirationDateDisabled = true;
                            }
                            else
                            {
                                if (employee.PasswordExpirationD.CompareTo(SmallDateTime.MinValue) <= 0)
                                    m_MustChangePasswordAtNextLogin.Checked = true;
                                else
                                {
                                    m_ExpirationDate.Text = employee.PasswordExpirationD.ToShortDateString();
                                    m_PasswordExpiresOn.Checked = true;
                                }
                                cycleExpirationDisabled = false;
                                expirationDateDisabled = !m_PasswordExpiresOn.Checked;
                            }
                        }
                        if ((employee.PasswordExpirationPeriod > 0) &&
                            (employee.PasswordExpirationD.CompareTo(SmallDateTime.MaxValue) < 0))
                        {
                            switch (employee.PasswordExpirationPeriod)
                            {
                                case 15: m_ExpirationPeriod.SelectedIndex = 0;
                                    break;

                                case 30: m_ExpirationPeriod.SelectedIndex = 1;
                                    break;

                                case 45: m_ExpirationPeriod.SelectedIndex = 2;
                                    break;

                                case 60: m_ExpirationPeriod.SelectedIndex = 3;
                                    break;
                            }

                            m_CycleExpiration.Checked = true;
                        }
                        else
                            m_CycleExpiration.Checked = false;

                        expirationPeriodDisabled = !m_CycleExpiration.Checked;

                        LicensesPanel.LosIdentifier = employee.LosIdentifier;
                        LicensesPanel.LicenseList = employee.LicenseInformationList; //opm 30840

                        Tools.SetDropDownListValue(m_ddlOrigiantorCompenationLevelT, employee.OriginatorCompensationSetLevelT);
                        m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo.Checked = employee.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo;
                        m_OriginatorCompensationPercent.Text = m_losConvert.ToRateString(employee.OriginatorCompensationPercent);
                        m_OriginatorCompensationFixedAmount.Text = m_losConvert.ToMoneyString(employee.OriginatorCompensationFixedAmount, FormatDirection.ToRep);
                        m_OriginatorCompensationNotes.Text = employee.OriginatorCompensationEmployeeNotes;
                        m_OriginatorCompensationMinAmount.Text = m_losConvert.ToMoneyString(employee.OriginatorCompensationMinAmount, FormatDirection.ToRep);
                        m_OriginatorCompensationMaxAmount.Text = m_losConvert.ToMoneyString(employee.OriginatorCompensationMaxAmount, FormatDirection.ToRep);
                        
                        Tools.SetDropDownListValue(m_OriginatorCompensationBaseT, employee.OriginatorCompensationBaseT);

                        if (brokerDB.IsCustomPricingPolicyFieldEnabled)
                        {
                            Tools.SetDropDownListValue(PricingPolicyFieldValueSource, employee.CustomPricingPolicyFieldValueSource);
                            PopulateHiddenFieldWithPricingPolicyInfo(brokerDB, employee);
                        }
                    }
					else // empId == Guid.Empty
					{
						// 12/10/2004 kb - We're new, so initialize the
						// permission according to what the broker wants.
						// If we can't load, we get the empty bit set.
                        m_IsLoanOfficer.Checked = true;

						ViewState.Add( "employeeId" , Guid.Empty );
						ViewState.Add( "userid", Guid.Empty );
						m_ChangeLogin.Checked = true;


                        if (brokerDB.CanRunPricingEngineWithoutCreditReportByDefault == true)
						{
							m_CanRunPricingEngineWithoutCreditReport.Checked = true;
							m_CanSubmitWithoutCreditReport.Checked = true;
						}
						else
						{
							m_CanRunPricingEngineWithoutCreditReport.Checked = false;
							m_CanSubmitWithoutCreditReport.Checked = false;
						}

						// Initialize the update panel state.
						m_ChangeLogin.Visible = false;
						m_LoginNotes.Visible  = true;

						m_Inactive.Enabled = false;
						m_Active.Enabled   = false;
                        m_qpNo.Checked = true;

						m_PasswordExpiresOn.Checked = false;
                        m_PasswordNeverExpires.Checked = false;
                        m_MustChangePasswordAtNextLogin.Checked = true;

                        m_OriginatorCompensationBaseT.SelectedValue = E_PercentBaseT.TotalLoanAmount.ToString("D");
                        m_ddlOrigiantorCompenationLevelT.SelectedValue = E_OrigiantorCompenationLevelT.OriginatingCompany.ToString("D");

                        // We want to hide the permissions panel for new users,
                        // as the user's permissions will populate from the OC
                        // set when the record is saved.
                        this.PermissionsPanel.Visible = false;
                    }

                    if (pmlBrokerId != Guid.Empty) //see if there was an assign request
                    {
                        m_CompanyChoice.Data = pmlBrokerId.ToString();
                    }
                    else if (!m_isPmlCompanySet)
                    {
                        m_CompanyChoice.Text = "None";
                        m_CompanyChoice.Data = Guid.Empty.ToString();
                        m_isPmlCompanySet = true;
                    }

					initPwUI(employee);
                    m_EnabledClientDigitalCertificateInstall.Checked = employee.EnabledClientDigitalCertificateInstall;
                    m_EnabledMultiFactorAuthentication.Checked = employee.EnabledMultiFactorAuthentication;
                    this.EnableAuthCodeViaSms.Checked = employee.EnableAuthCodeViaSms;
                    this.EnableAuthCodeViaAuthenticator.Checked = employee.EnableAuthCodeViaAuthenticator;
                    this.EnableAuthCodeViaAuthenticator.Disabled = true;
                    this.DisableAuthenticator.Visible = employee.EnableAuthCodeViaAuthenticator;
                }
				else // PostBack == true
				{
					m_isNew = false;
					// 10/4/2004 kb - Combine status and change login flags
					// to determine who should be visible and who should be
					// readonly.

					m_EnableLogin = m_Active.Checked;
					m_ChangeLogin.Visible = true;
					m_LoginNotes.Visible  = true;
					m_Inactive.Enabled = true;
					m_Active.Enabled   = true;

					//11/29/07 db - OPM 19127
					if(ViewState["isDuEnabled"] != null)
					{
						try
						{
							m_isDUEnabled = bool.Parse(ViewState["isDuEnabled"].ToString());
						}
						catch{}
					}
					if ( ViewState["pmlautologin"] != null  )
					{
						m_sPmlAutoLoginOption = ViewState["pmlautologin"].ToString();
					}
																
					if(ViewState["isDoEnabled"] != null)
					{
						try
						{
							m_isDOEnabled = bool.Parse(ViewState["isDoEnabled"].ToString());
						}
						catch{}
					}
					initPwUI(employee);
                }

                BrokerDB broker = BrokerDB.RetrieveById(employee.BrokerID);
                m_EnabledMultiFactorAuthentication.Visible = broker.IsEnableMultiFactorAuthenticationTPO;
                this.EnableAuthCodeViaSms.Visible = broker.IsEnableMultiFactorAuthenticationTPO;
                m_EnabledMultiFactorAuthentication.Disabled = broker.IsEnableMultiFactorAuthenticationTPO || !broker.AllowUserMfaToBeDisabled;
                IsEnableMultiFactorAuthenticationTPO.Value = broker.IsEnableMultiFactorAuthenticationTPO.ToString();

                IPAccessContainer.Visible = broker.IsEnableMultiFactorAuthenticationTPO;
                m_clientCertificateGrid.DataSource = ClientCertificate.ListByUser(employee.BrokerID, employee.UserID);
                m_clientCertificateGrid.DataBind();

                if (broker.IsEnableMultiFactorAuthenticationTPO)
                {
                    m_registeredIpDataGrid.DataSource = UserRegisteredIp.ListByUserId(employee.BrokerID, employee.UserID);
                    m_registeredIpDataGrid.DataBind();
                }
			}
			catch( Exception e )
			{
				// 08/10/06 mf - OPM 6104.  Since this exception could have occured before we finished
				// setting the UI elements, we have to redirect to the error page.

				ErrorUtilities.DisplayErrorPage( e , true, CurrentUser.BrokerId, CurrentUser.EmployeeId );
			}

			if ( m_ip_parsed.Value != string.Empty ) 
			{
				string[] ips = m_ip_parsed.Value.Split(';'); 
	
				foreach ( string ip in ips ) 
				{
					if ( ip == string.Empty ) continue; 
					m_ip.Items.Add(new ListItem( ip ) );
				}
			}
			m_dudoSearch.IsDOEnabled = this.m_isDOEnabled; 
			m_dudoSearch.IsDUEnabled = this.m_isDUEnabled;

            m_ddlOrigiantorCompenationLevelT.Attributes["onChange"] = "OnCompensationLevelChange();";
            PricingPolicyFieldValueSource.Attributes["onChange"] = "OnPricingPolicyLevelChange();";
		}

        /// <summary>
        /// Sets the display of the permissions tab in the editor.
        /// </summary>
        /// <param name="employee">
        /// The employee that is being edited.
        /// </param>
        /// <param name="populationMethod">
        /// The <see cref="EmployeePopulationMethodT"/> used to set the
        /// permission display.
        /// </param>
        private void SetPermissionDisplay(EmployeeDB employee, EmployeePopulationMethodT populationMethod)
        {
            try
            {
                // 12/2/2004 kb - Load broker user permissions from here.
                // Add new permission edit boxes here.

                Tools.SetDropDownListValue(this.UsePermissionDdl, populationMethod);

                var buP = new BrokerUserPermissions(CurrentUser.BrokerId, employee.ID);

                m_AllowCreateCorrChannelLoans.Checked = buP.HasPermission(Permission.AllowCreatingCorrChannelLoans);
                m_AllowCreateMiniCorrChannelLoans.Checked = buP.HasPermission(Permission.AllowCreatingMiniCorrChannelLoans);
                m_AllowCreateWholesaleChannelLoans.Checked = buP.HasPermission(Permission.AllowCreatingWholesaleChannelLoans);
                m_AllowViewingCorrChannelLoans.Checked = buP.HasPermission(Permission.AllowViewingCorrChannelLoans);
                m_AllowViewingMiniCorrChannelLoans.Checked = buP.HasPermission(Permission.AllowViewingMiniCorrChannelLoans);
                m_AllowViewingWholesaleChannelLoans.Checked = buP.HasPermission(Permission.AllowViewingWholesaleChannelLoans);

                if (populationMethod == EmployeePopulationMethodT.OriginatingCompany)
                {
                    var pmlBrokerPermissions = PmlBroker.RetrievePmlBrokerById(employee.PmlBrokerId, this.BrokerId).Permissions;

                    m_CanApplyForIneligibleLoanPrograms.Checked = pmlBrokerPermissions.CanApplyForIneligibleLoanPrograms;
                    m_CanApplyForIneligibleLoanPrograms.Enabled = false;

                    m_CanRunPricingEngineWithoutCreditReport.Checked = pmlBrokerPermissions.CanRunPricingEngineWithoutCreditReport;
                    m_CanRunPricingEngineWithoutCreditReport.Enabled = false;

                    m_CanSubmitWithoutCreditReport.Checked = pmlBrokerPermissions.CanSubmitWithoutCreditReport;
                    m_CanSubmitWithoutCreditReport.Enabled = false;

                    m_totalYes.Checked = pmlBrokerPermissions.CanAccessTotalScorecard;
                    m_totalYes.Enabled = false;

                    m_totalNo.Checked = !m_totalYes.Checked;
                    m_totalNo.Enabled = false;

                    m_Allow4506T.Checked = pmlBrokerPermissions.AllowOrder4506T;
                    m_Allow4506T.Enabled = false;

                    m_TaskRelatedNotifOptionT.SelectedIndex = (int)pmlBrokerPermissions.TaskEmailOption;
                    m_TaskRelatedNotifOptionT.Enabled = false;
                }
                else
                {
                    m_CanApplyForIneligibleLoanPrograms.Checked = buP.HasPermission(Permission.CanApplyForIneligibleLoanPrograms);
                    m_CanApplyForIneligibleLoanPrograms.Enabled = true;

                    m_CanRunPricingEngineWithoutCreditReport.Checked = buP.HasPermission(Permission.CanRunPricingEngineWithoutCreditReport);
                    m_CanRunPricingEngineWithoutCreditReport.Enabled = true;

                    m_CanSubmitWithoutCreditReport.Checked = buP.HasPermission(Permission.CanSubmitWithoutCreditReport);
                    m_CanSubmitWithoutCreditReport.Enabled = true;

                    m_totalYes.Checked = buP.HasPermission(Permission.CanAccessTotalScorecard);
                    m_totalYes.Enabled = true;

                    m_totalNo.Checked = !m_totalYes.Checked;
                    m_totalNo.Enabled = true;

                    m_Allow4506T.Checked = buP.HasPermission(Permission.AllowOrder4506T);
                    m_Allow4506T.Enabled = true;

                    m_TaskRelatedNotifOptionT.SelectedIndex = (int)employee.TaskRelatedEmailOptionT;
                    m_TaskRelatedNotifOptionT.Enabled = true;
                }
            }
            catch (Exception e)
            {
                throw new GenericUserErrorMessageException(e);
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
            this.EnableJqueryMigrate = false;
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);

		}
		#endregion

		/// <summary>
		/// Handle commit of new employee.
		/// </summary>
		protected void OnApplyClick( object sender , System.EventArgs a )
		{
			// Build basic employee record and commit to the database.
			try
			{
				// Create data access object and initialize it with defaults
				// and what the user specifies.  If the invoking user is not
				// an administrator nor able to create external users, we
				// punt.

				EmployeeDB employee = new EmployeeDB();

                if ((!CurrentUser.HasPermission(Permission.CanAdministrateExternalUsers)) || CurrentUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false)
				{
					ErrorMessage = "Permission denied.  You lack authorization to invoke this command.";
					return;
				}

                if (!ValidatePortalModeViewCreatePermissions())
                {
                    ErrorMessage = "Unable to grant permission to create loans without granting permission to view loans in the same portal mode.";
                    return;
                }

				if( (ViewState[ "employeeId" ] != null) && (new Guid(ViewState[ "employeeId" ].ToString()) != Guid.Empty))
				{
					employee = new EmployeeDB( new Guid( ViewState[ "employeeId" ].ToString() ) , CurrentUser.BrokerId );

					if ( employee.Retrieve() == false )
						throw new CBaseException( ErrorMessages.Generic, "Failed to load user to update." );
				} 
				else 
				{
                    // 2/15/2005 dd - If broker has NewUserDefault_PmlUserIsMLeague options then new PML user create will have
                    // Login from MCL turn on by default. OPM #1116
                    // Pull options from Broker db table.
                    var listParams = new SqlParameter[] { new SqlParameter("@id", CurrentUser.BrokerId) };
                    string sql = "SELECT NewUserDefault_PmlUserIsMLeague FROM BROKER WHERE BrokerID=@id";

                    Action<IDataReader> readHandler = delegate(IDataReader reader)
                    {
                        if (reader.Read())
                        {
                            if ((bool)reader["NewUserDefault_PmlUserIsMLeague"])
                                employee.PmlSiteID = Guid.NewGuid();
                        }
                    };

                    DBSelectUtility.ProcessDBData(CurrentUser.BrokerId, sql, null, listParams, readHandler);
                }

                if (this.IsDisablingAuthenticator.Value == "True")
                {
                    employee.EnableAuthCodeViaAuthenticator = false;
                    this.IsDisablingAuthenticator.Value = string.Empty;
                }

                employee.EnableAuthCodeViaSms = this.EnableAuthCodeViaSms.Checked;
                employee.EnabledMultiFactorAuthentication = m_EnabledMultiFactorAuthentication.Checked;
                employee.EnabledClientDigitalCertificateInstall = m_EnabledClientDigitalCertificateInstall.Checked;

                ClientScript.RegisterHiddenField("UsersCurrentOc", m_CompanyChoice.Data);
                ClientScript.RegisterHiddenField("UsersCurrentOcName", m_CompanyChoice.Text);
                employee.FirstName = m_FirstName.Text.TrimWhitespaceAndBOM();
                employee.MiddleName = m_MiddleName.Text.TrimWhitespaceAndBOM();
				employee.LastName  = m_LastName.Text.TrimWhitespaceAndBOM();
                employee.Suffix = m_Suffix.Text.TrimWhitespaceAndBOM();
                employee.NmOnLoanDocs = m_NameOnLoanDoc.Text.TrimWhitespaceAndBOM();
                employee.NmOnLoanDocsLckd = m_NameOnLoanDocLock.Checked;

                employee.PmlExternalManagerEmployeeId = new Guid(m_SupervisorChoice.Data);

				employee.Address.StreetAddress = m_Address.Text.TrimWhitespaceAndBOM();
				employee.Address.City          = m_City.Text.TrimWhitespaceAndBOM();
				employee.Address.State         = m_State.Value.TrimWhitespaceAndBOM();
				employee.Address.Zipcode       = m_Zipcode.Text.TrimWhitespaceAndBOM();
				employee.Phone                 = m_Phone.Text.TrimWhitespaceAndBOM();
				employee.PrivateCellPhone		   = m_Cell.Text.TrimWhitespaceAndBOM();
                employee.IsCellphoneForMultiFactorOnly = this.IsCellphoneForMultiFactorOnly.Checked;
				employee.Pager					= m_Pager.Text.TrimWhitespaceAndBOM();
				employee.Fax                   = m_Fax.Text.TrimWhitespaceAndBOM();
				employee.Email                 = m_Email.Text.TrimWhitespaceAndBOM();
				employee.NotesByEmployer		= m_Notes.Text;
                employee.TaskRelatedEmailOptionT = (E_TaskRelatedEmailOptionT)m_TaskRelatedNotifOptionT.SelectedIndex;

                if (employee.IsNew == true)
                {
                    // 6/3/2005 kb - Per case 1904, we now default all
                    // new pml users to shareable.
                    employee.PasswordExpirationD = DateTime.MaxValue;

                    employee.BrokerID = CurrentUser.BrokerId;
                    employee.IsSharable = true;
                    employee.UserType = 'P';
                }

                var originalEmployeePmlBrokerId = employee.PmlBrokerId;

                try
                {
                    employee.PmlBrokerId = new Guid(m_CompanyChoice.Data);
                }
                catch
                {
                    employee.PmlBrokerId = Guid.Empty;
                }

                var hasChangedOriginatingCompanies = originalEmployeePmlBrokerId != employee.PmlBrokerId;

                this.brokerRelationships.ApplySettingsToEmployee(employee, hasChangedOriginatingCompanies);
                this.miniCorrespondentRelationships.ApplySettingsToEmployee(employee, hasChangedOriginatingCompanies);
                this.correspondentRelationships.ApplySettingsToEmployee(employee, hasChangedOriginatingCompanies);

                employee.PopulatePMLPermissionsT = (EmployeePopulationMethodT)Tools.GetDropDownListValue(
                    this.UsePermissionDdl);

                bool validBPRelationship = this.ValidateRelationshipWithinOC(
                    employee.BrokerProcessorEmployeeId);
                bool validMiniCorrCPRelationship = this.ValidateRelationshipWithinOC(
                    employee.MiniCorrExternalPostCloserEmployeeId);
                bool validCorrCPRelationship = this.ValidateRelationshipWithinOC(
                    employee.CorrExternalPostCloserEmployeeId);

                string relationshipValidationError = "";

                if (!validBPRelationship)
                {
                    relationshipValidationError += ErrorMessages.InvalidBrokerProcessorRelationship
                        .Replace("<Originating Company>", m_CompanyChoice.Text);
                }
                if (!validMiniCorrCPRelationship)
                {
                    relationshipValidationError += ErrorMessages.InvalidMiniCorrExternalPostCloserRelationship
                        .Replace("<Originating Company>", m_CompanyChoice.Text);
                }
                if (!validCorrCPRelationship)
                {
                    relationshipValidationError += ErrorMessages.InvalidCorrExternalPostCloserRelationship
                        .Replace("<Originating Company>", m_CompanyChoice.Text);
                }

                if (!string.IsNullOrEmpty(relationshipValidationError))
                {
                    this.ErrorMessage = relationshipValidationError;
                    return;
                }

                List<Guid> selectedTpoPages = new List<Guid>();
                foreach (RepeaterItem item in CustomPageRepeater.Items)
                {
                    Guid pageId;
                    HtmlInputCheckBox checkbox = item.FindControl("CustomPageCheckbox") as HtmlInputCheckBox;
                    if(checkbox.Checked && Guid.TryParse(checkbox.Attributes["data-page-id"], out pageId))
                    {
                        selectedTpoPages.Add(pageId);
                    }
                }

                employee.AccessiblePmlNavigationLinksId = selectedTpoPages.ToArray();
				employee.MustLogInFromTheseIpAddresses	= m_ip_parsed.Value;
                employee.EnabledIpRestriction = m_ipRestrictionOn.Checked;

				// 11/29/07 db - OPM 19127
				if(m_isDOEnabled)
				{
					employee.DOAutoLoginName			= m_doLogin.Text;
					if (m_doPw.Text != "")
						employee.DOAutoPassword		= m_doPw.Text;
				}
				
				if(m_isDUEnabled)
				{
					employee.DUAutoLoginName			= m_duLogin.Text;
					if (m_duPw.Text != "")
						employee.DUAutoPassword		= m_duPw.Text;
				}

                employee.IsPmlManager = m_IsSupervisor.Checked;

                if (m_AccessCorporate.Checked)
                {
                    employee.PmlLevelAccess = E_PmlLoanLevelAccess.Corporate;
                }
                else if (m_AccessSupervisor.Checked)
                {
                    employee.PmlLevelAccess = E_PmlLoanLevelAccess.Supervisor;
                    if (false == m_IsSupervisor.Checked)
                    {
                        throw new CBaseException("Only supervisors can have supervisor level access.", "IsSupervisor = false & PmlLevelAccess == Supervisor");
                    }
                }
                else// if (m_AccessIndividual.Checked)
                {
                    //OPM 136636 - pa 8/23/2013 don't error if there's no selection; default to Individual (same as what UI default *should* be)
                    employee.PmlLevelAccess = E_PmlLoanLevelAccess.Individual;
                }
				
				if( m_Active.Checked == true && ( employee.IsNew == true || m_ChangeLogin.Checked == true ) )
				{
					employee.LoginName = m_Login.Text.TrimWhitespaceAndBOM();
					
					// 3/20/2006 mf - OPM 4226 Only change the password if an approved modification occured
					if (m_Password.Text != "" && m_Confirm.Text != "")
						employee.Password  = m_Password.Text;
				}
				RebindLoginName(employee.LoginName);

				if( m_Active.Checked == false )
				{
					employee.AllowLogin = false;
					employee.IsActive   = false;
				}
				else
				{
					employee.AllowLogin = true;
					employee.IsActive   = true;
				}

                string roles = "";
                if (m_IsLoanOfficer.Checked)
                {
                    roles +=CEmployeeFields.s_LoanRepRoleId.ToString();
                }
                if (m_IsBrokerProcessor.Checked)
                {
                    if (false == string.IsNullOrEmpty(roles))
                    {
                        roles += ",";
                    }
                    roles += CEmployeeFields.s_BrokerProcessorId.ToString();
                }

                if (m_IsExternalSecondary.Checked)
                {
                    if (false == string.IsNullOrEmpty(roles))
                    {
                        roles += ",";
                    }
                    roles += CEmployeeFields.s_ExternalSecondaryId.ToString();
                }

                if (m_IsExternalPostCloser.Checked)
                {
                    if (false == string.IsNullOrEmpty(roles))
                    {
                        roles += ",";
                    }
                    roles += CEmployeeFields.s_ExternalPostCloserId.ToString();
                }

                if (m_IsSupervisor.Checked)
                {
                    if (false == string.IsNullOrEmpty(roles))
                    {
                        roles += ",";
                    }
                    roles += CEmployeeFields.s_AdministratorRoleId.ToString();
                }

                if (string.IsNullOrEmpty(roles))
                {
                    ErrorMessage = ErrorMessages.PMLMissingRolesError; 
                    return;
                }                
                employee.Roles = roles;

                if ( m_QuickPricer.Visible )
                {
                    if ( m_qpNo.Checked )   //the user turned off
                    {
                        employee.ResetAnonymousQuickPricerId(); 
                    }
                    else if (employee.AnymousQuickPricerAccessId == Guid.Empty) //the user turned it on for the first time.
                    {
                        employee.ActivateQuickPricerAccessForThisUser();
                    }
                    m_anonymousQuickPricerUrl.Visible = m_qpYes.Checked;
                    m_anonymousQuickPricerUrl.Text = employee.AnonymousQuickPricerUrl;                    
                    m_qpurlcopy.Visible = m_anonymousQuickPricerUrl.Visible;
                    //else the user did no then yes click so we dont need to save anything.
                }
				try
				{
                    Boolean IsForceChangePasswordForPml = BrokerDB.RetrieveById(CurrentUser.BrokerId).IsForceChangePasswordForPml;
                    if (m_PasswordNeverExpires.Checked == true)
					{
						employee.PasswordExpirationD = SmallDateTime.MaxValue;
						employee.PasswordExpirationPeriod = 0;
					}
					else
					{
                        if (m_MustChangePasswordAtNextLogin.Checked == true && !IsForceChangePasswordForPml)
                            employee.PasswordExpirationD = SmallDateTime.MinValue;

						if( m_PasswordExpiresOn.Checked == true && m_ExpirationDate.Text != "" )
							employee.PasswordExpirationD = DateTime.Parse( m_ExpirationDate.Text );

						if( m_CycleExpiration.Checked == true )
							employee.PasswordExpirationPeriod = Int32.Parse( m_ExpirationPeriod.SelectedItem.Value );
						else
							employee.PasswordExpirationPeriod = 0;
					}

                    if (IsForceChangePasswordForPml && m_Password.Text != "")
                        employee.PasswordExpirationD = SmallDateTime.MinValue;
				}
				catch(CBaseException e)
				{
					ErrorMessage = "Failed to save user.";
					m_LoginErrorMessage.Text = e.UserMessage;
					m_isNew = employee.IsNew;

					return;
				}

                employee.LosIdentifier = LicensesPanel.LosIdentifier;
                employee.LicenseInformationList = LicensesPanel.LicenseList; //opm 30840

                // opm 64436
                employee.CommissionPointOfLoanAmount = m_losConvert.ToRate(m_CommissionPointOfLoanAmount.Text);
                employee.CommissionMinBase = m_losConvert.ToMoney(m_CommissionMinBase.Text);
                employee.CommissionPointOfGrossProfit = m_losConvert.ToRate(m_CommissionPointOfGrossProfit.Text);

                employee.OriginatorCompensationSetLevelT = (E_OrigiantorCompenationLevelT) Convert.ToInt32(m_ddlOrigiantorCompenationLevelT.SelectedValue);
                employee.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo = m_OriginatorCompensationIsOnlyPaidForFirstLienOfCombo.Checked;
                employee.OriginatorCompensationPercent = m_losConvert.ToRate(m_OriginatorCompensationPercent.Text);
                employee.OriginatorCompensationMinAmount = m_losConvert.ToMoney(m_OriginatorCompensationMinAmount.Text);
                employee.OriginatorCompensationMaxAmount = m_losConvert.ToMoney(m_OriginatorCompensationMaxAmount.Text);
                employee.OriginatorCompensationFixedAmount = m_losConvert.ToMoney(m_OriginatorCompensationFixedAmount.Text);
                employee.OriginatorCompensationNotes = m_OriginatorCompensationNotes.Text;
                employee.OriginatorCompensationBaseT = (E_PercentBaseT)Convert.ToInt32(m_OriginatorCompensationBaseT.SelectedValue);

                if (BrokerDB.RetrieveById(employee.BrokerID).IsCustomPricingPolicyFieldEnabled)
                {
                    employee.CustomPricingPolicyFieldValueSource = (E_PricingPolicyFieldValueSource)Tools.GetDropDownListValue(PricingPolicyFieldValueSource);
                    SetCustomLOPricingPolicyFields(employee);
                }

                

				employee.WhoDoneIt = CurrentUser.UserId;
				if( employee.Save(PrincipalFactory.CurrentPrincipal) == false )
					throw new CBaseException( ErrorMessages.Generic, "Failed to save user." );
				else
				{
					// Revise our viewstate so that we don't look new all
					// the time.
					ViewState.Add( "employeeId" , employee.ID );
					ViewState.Add( "userid", employee.UserID );
					// Save the user's permissions after we successfully saved.

					try
					{
						// 12/2/2004 kb - Update all employee permissions after
						// we save.  This way, we only commit the changes when
						// the save is successful and we don't have to worry
						// about whether or not the employee exists.  Add other
						// permissions here as needed.
						BrokerUserPermissions buP = new BrokerUserPermissions( employee.BrokerID , employee.ID );
                        buP.SetPermission(Permission.CanApplyForIneligibleLoanPrograms, m_CanApplyForIneligibleLoanPrograms.Checked);
                        buP.SetPermission(Permission.CanRunPricingEngineWithoutCreditReport, m_CanRunPricingEngineWithoutCreditReport.Checked);
                        buP.SetPermission(Permission.CanSubmitWithoutCreditReport, m_CanSubmitWithoutCreditReport.Checked);
                        buP.SetPermission(Permission.CanAccessTotalScorecard, m_totalYes.Checked);
                        buP.SetPermission(Permission.AllowOrder4506T, m_Allow4506T.Checked);

                        buP.SetPermission(Permission.AllowCreatingCorrChannelLoans, m_AllowCreateCorrChannelLoans.Checked);
                        buP.SetPermission(Permission.AllowCreatingMiniCorrChannelLoans, m_AllowCreateMiniCorrChannelLoans.Checked);
                        buP.SetPermission(Permission.AllowCreatingWholesaleChannelLoans, m_AllowCreateWholesaleChannelLoans.Checked);

                        buP.SetPermission(Permission.AllowViewingCorrChannelLoans, m_AllowViewingCorrChannelLoans.Checked);
                        buP.SetPermission(Permission.AllowViewingMiniCorrChannelLoans, m_AllowViewingMiniCorrChannelLoans.Checked);
                        buP.SetPermission(Permission.AllowViewingWholesaleChannelLoans, m_AllowViewingWholesaleChannelLoans.Checked);

						buP.Update();
                        
                        this.SetPermissionDisplay(employee, employee.PopulatePMLPermissionsT);
                    }
					catch
					{
						// Turn off any broker user permissions if we are
						// unable to edit and save.
						m_CanRunPricingEngineWithoutCreditReport.Enabled = false;
						m_CanSubmitWithoutCreditReport.Enabled = false;
						m_CanApplyForIneligibleLoanPrograms.Enabled = false;
                        m_Allow4506T.Enabled = false;
					}

                    this.IsSupervisorOnLoad.Value = employee.IsPmlManager.ToString();
				}

				// Update the ui so we don't try to save when someone
				// clicks ok after apply.  Reason: we try to resave a
				// password change that takes place with the previous
				// apply request (but pw is blanked out).
				initPwUI(employee);
				m_ChangeLogin.Checked = false;
				m_Dirty.Value = "0";
			}
			catch( Exception e )
			{
				// 08/10/06 mf - OPM 6104.  Since this exception could have occured before we finished
				// setting the UI elements, we have to redirect to the error page.

				ErrorUtilities.DisplayErrorPage( e , true, CurrentUser.BrokerId, CurrentUser.EmployeeId );
			}
		}

        private bool ValidatePortalModeViewCreatePermissions()
        {
            if ((m_AllowCreateWholesaleChannelLoans.Checked && !m_AllowViewingWholesaleChannelLoans.Checked) ||
                (m_AllowCreateMiniCorrChannelLoans.Checked && !m_AllowViewingMiniCorrChannelLoans.Checked) ||
                (m_AllowCreateCorrChannelLoans.Checked && !m_AllowViewingCorrChannelLoans.Checked))
            {
                return false;
            }

            return true;
        }

        private bool ValidateRelationshipWithinOC(Guid employeeID)
        {
            if (employeeID != Guid.Empty)
            {
                var employee = new EmployeeDB(employeeID, CurrentUser.BrokerId);
                employee.Retrieve();

                if (employee.PmlBrokerId.ToString() != m_CompanyChoice.Data)
                {
                    return false;
                }
            }

            return true;
        }

        private void PopulateHiddenFieldWithPricingPolicyInfo(BrokerDB brokerDb, EmployeeDB employee)
        {
            var customPricingPolicyFields = CustomPmlFieldUtilities.GetCustomPricingPolicyInfoForEmployee(brokerDb, employee);
            CustomLOPPFieldSettings.Value = ObsoleteSerializationHelper.JavascriptJsonSerialize<List<CustomPmlFieldInfo>>(customPricingPolicyFields);
        }

        private void SetCustomLOPricingPolicyFields(EmployeeDB employee)
        {
            var serializedFieldInfo = CustomLOPPFieldSettings.Value;
            if (string.IsNullOrEmpty(serializedFieldInfo)) return;

            var fieldSettings = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<CustomPmlFieldInfo>>(serializedFieldInfo);
            foreach (var field in fieldSettings)
            {
                switch (field.Id)
                {
                    case "CustomPricingPolicyField1": employee.CustomPricingPolicyField1Fixed = field.Value; break;
                    case "CustomPricingPolicyField2": employee.CustomPricingPolicyField2Fixed = field.Value; break;
                    case "CustomPricingPolicyField3": employee.CustomPricingPolicyField3Fixed = field.Value; break;
                    case "CustomPricingPolicyField4": employee.CustomPricingPolicyField4Fixed = field.Value; break;
                    case "CustomPricingPolicyField5": employee.CustomPricingPolicyField5Fixed = field.Value; break;
                    default: throw new CBaseException(ErrorMessages.Generic, "Unhandled LO pricing policy field id.");
                }
            }
        }

        protected void m_OriginatorCompensationAuditGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            m_OriginatorCompensationAuditGrid.CurrentPageIndex = e.NewPageIndex;
            EmployeeDB employee = new EmployeeDB(m_EmployeeId, CurrentUser.BrokerId);
            employee.Retrieve();
            m_OriginatorCompensationAuditGrid.DataSource = employee.OriginatorCompensationAuditData.ParseXml(employee.BrokerID);
            m_OriginatorCompensationAuditGrid.DataBind();
        }

        protected void BuildLoanOfficerPricingPolicyTable()
        {
            var htmlTableRows = CustomPmlFieldUtilities.GetLoanOfficerPricingTableRows(PrincipalFactory.CurrentPrincipal.BrokerId);

            foreach (var row in htmlTableRows)
            {
                CustomLOPricingPolicyTable.Rows.Add(row);
            }
        }

        protected void m_OriginatorCompensationAuditGrid_ItemCreated(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Pager)
            {
                Label lblPagerText = new Label();
                lblPagerText.Text = "Page: ";

                if (e.Item.Controls[0].FindControl("lblPagerText") == null)
                {
                    e.Item.Controls[0].Controls.AddAt(0, lblPagerText);
                }
            }
        }

        /// <summary>
		/// Handle commit of new employee.
		/// </summary>

		protected void OnOkClick( object sender , System.EventArgs a )
		{
			// Save current user and close the ui.
			// Delegate the save request as if apply were clicked.
			OnApplyClick( sender , a );
			// Now tell the ui to close.
			CommandToDo = "Close";
		}

        /// <summary>
        /// Handles the postback event when the value of the
        /// permission dropdown list is changed.
        /// </summary>
        /// <param name="sender">
        /// The <see cref="DropDownList"/> that activated
        /// this event.
        /// </param>
        /// <param name="e">
        /// The argument for the events.
        /// </param>
        protected void UsePermissionDdl_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddlSender = sender as DropDownList;

            var populationMethod = (EmployeePopulationMethodT)Tools.GetDropDownListValue(ddlSender);

            if (this.m_EmployeeId == Guid.Empty)
            {
                this.PermissionsPanel.Visible = populationMethod == EmployeePopulationMethodT.IndividualUser;
            }
            else
            {
                // We need to retrieve the employee record between postbacks.
                var employee = EmployeeDB.RetrieveById(this.BrokerId, this.m_EmployeeId);

                this.SetPermissionDisplay(employee, populationMethod);
            }
        }

        protected void CustomPageRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // Page name, page ID, selected
            var customPage = (Tuple<string, Guid, bool>)e.Item.DataItem;

            var checkbox = e.Item.FindControl("CustomPageCheckbox") as HtmlInputCheckBox;
            checkbox.Attributes["data-page-id"] = customPage.Item2.ToString();
            checkbox.Attributes["class"] += " custom-page";
            checkbox.Checked = customPage.Item3;

            var nameSpan = e.Item.FindControl("CustomPageSpan") as HtmlGenericControl;
            nameSpan.InnerText = customPage.Item1;
        }
    }
}
