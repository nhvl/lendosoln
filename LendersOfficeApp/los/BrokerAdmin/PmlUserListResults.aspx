<%@ Page language="c#" Codebehind="PmlUserListResults.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.BrokerAdmin.PmlUserListResults" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>PmlUserListResults</title>
  </HEAD>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" style="MARGIN:0px">
	<script language=javascript>
<!--
function _init()
{
  <% if (IsPostBack) { %>
  window.parent.setBatchControls( false );
  <% } %>

  // Needs to call this when the page is refreshed to work
  f_enableDisableExportSearchResultsButton();
}

function f_search(sName,sPriceGroupId, sLenderAccountExecFilter, sUnderwriterFilter, sLockDeskFilter, sManagerFilter, sProcessorFilter, sSupervisorFilter, sDate, sBeforeAfter, sBranch, sStatus, sRelationship, pmlBrokerId, sJuniorUnderwriterFilter, sJuniorProcessorFilter, sCreditAuditorFilter, sLegalAuditorFilter, sPurchaserFilter, sSecondaryFilter, mode) {
  document.forms[0]["sName"].value = sName;
  document.forms[0]["sPriceGroupId"].value = sPriceGroupId;
  document.forms[0]["sLenderAccountExecFilter"].value = sLenderAccountExecFilter;
  document.forms[0]["sUnderwriterFilter"].value = sUnderwriterFilter;
  document.forms[0]["sJuniorUnderwriterFilter"].value = sJuniorUnderwriterFilter;
  document.forms[0]["sLockDeskFilter"].value = sLockDeskFilter;
  document.forms[0]["sManagerFilter"].value = sManagerFilter;
  document.forms[0]["sSupervisorFilter"].value = sSupervisorFilter;
  document.forms[0]["sProcessorFilter"].value = sProcessorFilter;
  document.forms[0]["sJuniorProcessorFilter"].value = sJuniorProcessorFilter;
  document.forms[0]["sDate"].value = sDate;
  document.forms[0]["sBeforeAfter"].value = sBeforeAfter;
  document.forms[0]["sBranch"].value = sBranch;
  document.forms[0]["sStatus"].value = sStatus;
  document.forms[0]["sRelationship"].value = sRelationship;
  document.forms[0]["PmlBrokerId"].value = pmlBrokerId;
  document.forms[0]["sCreditAuditorFilter"].value = sCreditAuditorFilter;
  document.forms[0]["sLegalAuditorFilter"].value = sLegalAuditorFilter;
  document.forms[0]["sPurchaserFilter"].value = sPurchaserFilter;
  document.forms[0]["sSecondaryFilter"].value = sSecondaryFilter;
  document.forms[0]["mode"].value = mode;
  __doPostBack('', '');

}
function f_display(sView) {
  window.parent.setBatchControls( false );
  document.forms[0]["sView"].value = sView;
  __doPostBack('', '');
}
function f_edit( employeeId ) {
    showModalTopLevel( "/los/BrokerAdmin/EditPmlUser.aspx?cmd=edit&employeeId=" + employeeId , null, null, null, function(a){
        if( a != null && a.OK == true ) {
            showUpdateText();
        }
    },
    {
        'width': 990,
        'height': 700,
        'hideCloseButton': true
    });
}
function showUpdateText()
{
  <%= AspxTools.JsGetElementById(m_updateResults) %>.style.display = 'block';
}

var g_iTotal = <%= AspxTools.JsNumeric(m_totalCount) %>;

function f_update(args) {
  var count = 0;
  var pmlBrokers = '';

	for (var i = 0; i < g_iTotal; i++)
	{
		var o = document.getElementById("_" + i);
		if (o.checked)
		{
		    args["_" + count] = o.value;
		    count++;

		    var pmlBrokerId = document.getElementById("_" + i + "_pmlBrokerId").value;

		    if (pmlBrokers != '')
		        pmlBrokers += ",";

		    pmlBrokers += pmlBrokerId;
		}
	}

	args["PmlBrokers"] = pmlBrokers;

	if (count == 0)
	{
		alert('Please select at least 1 user');
		return;
	}

	args["Count"] = "" + count;
	var result = gService.main.call("Update", args);
	if (!result.error)
	{
		alert(result.value["Status"]);
		__doPostBack('', '');
	}
}

function GetSelectedPmlSupervisors()
{
	var supervisorIds = [];

	for (var i = 0; i < g_iTotal; i++)
	{
	    var o = document.getElementById("_" + i);
	    if (o.checked && o["data-is-supervisor"] === "Yes")
	    {
	        supervisorIds.push(o.value);
	    }
	}

	return supervisorIds;
}

function GetExPmlSupervisorsAssignedToUsers()
{
    var args = {
        possibleSupervisorIds: GetSelectedPmlSupervisors().join()
    };

    var result = gService.main.call("GetExSupervisorsAssignedToUsers", args);

    if (result.error === false && result.value["HasExSupervisors"] === "True") {
        return result.value['ExSupervisorsJson'];
    }

    return null;
}

function f_selectAll(cb) {
  for (var i = 0; i < g_iTotal; i++) {
    var o = document.getElementById("_" + i);
    o.checked = cb.checked;
    highlightRowByCheckbox(o);
  }
  updateBatchControls();
}

function onCheckRow(checkBox)
{
  highlightRowByCheckbox(checkBox);
  updateBatchControls();
}


function updateBatchControls()
{
  // Determine if the conrols should be enabled or disabled.
  var inputs = document.getElementsByTagName("INPUT");
  var controlStatus = false;
  for (var i = 0; i < inputs.length; i++)
  {
    if ( inputs[i].id && inputs[i].id.indexOf('_') == 0
		&& inputs[i].checked != null && inputs[i].checked == true )
    {
      controlStatus = true;
      break;
    }
  }
  window.parent.setBatchControls( controlStatus );
}

// 08/05/08 ck - OPM 21952. Exporting search results only instead of the whole list
function f_exportSearchResults()
{
	document.getElementById("m_exportSearchResultsButton").click();
}

// 08/27/08 ck - OPM 21952. Enables/disables the Export search results button
function f_enableDisableExportSearchResultsButton()
{
	if(<%= AspxTools.JsGetElementById(m_enableDisableText) %>.value == "ENABLE")
	{
		parent.document.getElementById("ExportSearchResultsButton").disabled = false;
	}
	else
	{
		parent.document.getElementById("ExportSearchResultsButton").disabled = true;
	}
}

//-->
</script>

    <form id="PmlUserListResultsForm" method="post" runat="server">
    <asp:HiddenField runat="server" ID="sLenderAccountExecFilter" />
    <asp:HiddenField runat="server" ID="sUnderwriterFilter" />
    <asp:HiddenField runat="server" ID="sJuniorUnderwriterFilter" />
    <asp:HiddenField runat="server" ID="sLockDeskFilter" />
    <asp:HiddenField runat="server" ID="sManagerFilter" />
    <asp:HiddenField runat="server" ID="sProcessorFilter" />
    <asp:HiddenField runat="server" ID="sJuniorProcessorFilter" />
    <asp:HiddenField runat="server" ID="sCreditAuditorFilter" />
    <asp:HiddenField runat="server" ID="sLegalAuditorFilter" />
    <asp:HiddenField runat="server" ID="sPurchaserFilter" />
    <asp:HiddenField runat="server" ID="sSecondaryFilter" />
    <asp:HiddenField runat="server" ID="mode" />
    <asp:HiddenField runat="server" ID="sBranch" />
	<asp:HiddenField runat="server" ID="sPriceGroupId" />
    <asp:Panel id=m_AccessDeniedPanel style="PADDING-RIGHT: 30px; PADDING-LEFT: 30px; PADDING-BOTTOM: 30px; COLOR: red; PADDING-TOP: 30px; TEXT-ALIGN: center" runat="server">
					Access Denied.  You do not have permission to administrate PML users.
			</asp:Panel>
			<asp:Panel id="m_AccessAllowedPanel" runat="server">
    <asp:Panel ID="m_tooManyResults" Runat="server" Visible="false" HorizontalAlign="Center" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: red; MARGIN: 10px 0px" EnableViewState="False" >
    <span>Found over 100 matches. &nbsp;Specify a more specific search criteria to find more users.</span>
    </asp:Panel>

    <asp:Panel ID="m_updateResults" Runat="server" HorizontalAlign="Center" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: red; MARGIN: 10px 0px; DISPLAY: none" EnableViewState="False" >
    <span>Results may not be up to date. &nbsp;To see updated results, execute a new search or change display view.</span>
    </asp:Panel>

    <asp:Panel style="display:none" ID=exportSearchResultsPanel Runat="server">
		<asp:Button ID=m_exportSearchResultsButton OnClick="exportSearchResults" runat="server" />
		<asp:TextBox ID="m_enableDisableText" Runat="server"></asp:TextBox>
	</asp:Panel>

    <ml:CommonDataGrid id=m_dg runat="server">
      <AlternatingItemStyle CssClass="GridAlternatingItem" VerticalAlign="Top" />
      <ItemStyle CssClass="GridItem" VerticalAlign="Top" />
      <HeaderStyle CssClass="GridHeader" />
<columns>
  <asp:TemplateColumn HeaderText="<input type=checkbox onclick=f_selectAll(this);>">
    <ItemTemplate>
      <input type="checkbox" id="_<%# AspxTools.HtmlString(m_currentCount) %>" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "EmployeeId"))%>" onclick="onCheckRow(this);" data-is-supervisor="<%# AspxTools.HtmlString(Eval("IsSupervisor").ToString())%>">
    </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn ItemStyle-Width="30px"  ItemStyle-HorizontalAlign="Center">
    <ItemTemplate>
      <a href="#" onclick="f_edit(<%# AspxTools.JsString(DataBinder.Eval(Container.DataItem, "EmployeeId").ToString() ) %>);">edit</a>
    </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Login" SortExpression="LoginNm">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("LoginNm").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="User Name" SortExpression="UserName">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("UserName").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="PriceMyLoan Options" SortExpression="PricingEngine">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("PricingEngine").ToString()) %>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Account Executive" SortExpression="LenderAccExecEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("LenderAccExecEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Account Executive" SortExpression="LenderAccExecEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrLenderAccExecEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Account Executive" SortExpression="LenderAccExecEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrLenderAccExecEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Lock Desk" SortExpression="LockDeskEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("LockDeskEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Lock Desk" SortExpression="MiniCorrLockDeskEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrLockDeskEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Lock Desk" SortExpression="CorrLockDeskEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrLockDeskEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Underwriter" SortExpression="UnderwriterEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("UnderwriterEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Underwriter" SortExpression="MiniCorrUnderwriterEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrUnderwriterEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Underwriter" SortExpression="CorrUnderwriterEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrUnderwriterEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Junior Underwriter" SortExpression="JuniorUnderwriterEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("JuniorUnderwriterEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Junior Underwriter" SortExpression="MiniCorrJuniorUnderwriterEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrJuniorUnderwriterEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Junior Underwriter" SortExpression="CorrJuniorUnderwriterEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrJuniorUnderwriterEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Credit Auditor" SortExpression="MiniCorrCreditAuditorEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrCreditAuditorEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Credit Auditor" SortExpression="CorrCreditAuditorEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrCreditAuditorEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Legal Auditor" SortExpression="MiniCorrLegalAuditorEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrLegalAuditorEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Legal Auditor" SortExpression="CorrLegalAuditorEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrLegalAuditorEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Manager" SortExpression="ManagerEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("ManagerEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Manager" SortExpression="MiniCorrManagerEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrManagerEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Manager" SortExpression="CorrManagerEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrManagerEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Processor" SortExpression="ProcessorEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("ProcessorEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Processor" SortExpression="MiniCorrProcessorEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrProcessorEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Processor" SortExpression="CorrProcessorEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrProcessorEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Junior Processor" SortExpression="JuniorProcessorEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("JuniorProcessorEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Junior Processor" SortExpression="MiniCorrJuniorProcessorEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrJuniorProcessorEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Junior Processor" SortExpression="CorrJuniorProcessorEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrJuniorProcessorEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Purchaser" SortExpression="MiniCorrPurchaserEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrPurchaserEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Purchaser" SortExpression="CorrPurchaserEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrPurchaserEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Secondary" SortExpression="MiniCorrSecondaryEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrSecondaryEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Secondary" SortExpression="CorrSecondaryEmployee">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrSecondaryEmployee").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Is a supervisor?" SortExpression="IsSupervisor">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("IsSupervisor").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Supervisor Name" SortExpression="Supervisor">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("Supervisor").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Company" SortExpression="PmlBrokerName">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("PmlBrokerName").ToString())%>
          <input type="hidden" id="_<%# AspxTools.HtmlString( m_currentCount++) %>_pmlBrokerId" value="<%# AspxTools.HtmlString(DataBinder.Eval(Container.DataItem, "PmlBrokerId"))%>" >
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Roles" SortExpression="Roles">
    <ItemTemplate>
        <%# AspxTools.HtmlString(Eval("Roles").ToString()) %>
    </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Broker Price Group" SortExpression="LpePriceGroup">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("LpePriceGroup").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Mini-Corr. Price Group" SortExpression="MiniCorrLpePriceGroup">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrLpePriceGroup").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Corr. Price Group" SortExpression="CorrLpePriceGroup">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrLpePriceGroup").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Status" SortExpression="IsActive">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("IsActive").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Last Login" SortExpression="RecentLoginD">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("RecentLoginD").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Broker Branch" SortExpression="BranchNm">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("BranchNm").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Mini-Corr. Branch" SortExpression="MiniCorrBranchNm">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("MiniCorrBranchNm").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Corr. Branch" SortExpression="CorrBranchNm">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("CorrBranchNm").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
  <asp:TemplateColumn HeaderText="Active Status" SortExpression="IsActive">
        <itemtemplate>
          <%# AspxTools.HtmlString(Eval("IsActive").ToString())%>
        </ItemTemplate>
  </asp:TemplateColumn>
</columns>
</ml:CommonDataGrid>
<% if (m_isTooManyUsers) { %>
<table width="100%" height="100%">
  <tr><td align=center valign=middle style="FONT-WEIGHT:bold;FONT-SIZE:12px;COLOR:red">Please use the filters above to search for existing users.</td></tr>
</table>
<% } %>

	<asp:Panel ID="m_zeroResults" Runat="server" Visible="False" HorizontalAlign="Center" style="FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: red; MARGIN: 10px 0px" EnableViewState="False" >
		<span><br><br>No matches found. Please change your search filters above and try again.</span>
    </asp:Panel>

</asp:Panel>
     </form><uc1:cModalDlg id=CModalDlg1 runat="server" />

  </body>
</HTML>
