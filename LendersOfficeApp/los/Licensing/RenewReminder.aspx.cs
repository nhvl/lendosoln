using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;
using LendersOfficeApp.common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;
using System.Security.Policy;
using System.Text.RegularExpressions;

namespace LendersOfficeApp.los.Licensing
{
	/// <summary>
	/// Summary description for RenewReminder.
	/// </summary>
	public partial class RenewReminder : LendersOffice.Common.BaseServicePage
	{
		protected bool m_IsAdmin ;

        protected void PageLoad(object sender, System.EventArgs e)
		{
			LendersOfficeApp.ObjLib.Licensing.BrokerUser brokerUser = new LendersOfficeApp.ObjLib.Licensing.BrokerUser(CurrentUser.BrokerId, CurrentUser.UserId, CurrentUser.DisplayName) ;
			m_IsAdmin = CurrentUser.HasRole(E_RoleT.Administrator) ;

			if (IsPostBack)
			{
				string licenseId = Request["licenseid"] ;
				if (licenseId != null && licenseId.Length > 0)
				{
                    /* -- code moved to payment & selection page
					LendersOfficeApp.ObjLib.Licensing.License license = new LendersOfficeApp.ObjLib.Licensing.License(new Guid(licenseId)) ;
					brokerUser.AssignLicense(license) ;
					*/

                    /// Not going to bother checking for validity of license assigned. Assuming that they shouldn't 
                    /// be able to assign an invalid license in the first place
                    RequestHelper.DoNextPostLoginTask(PostLoginTask.CheckLicensing);
                }
			}
			else
			{
				m_DaysTilExpire.Text = brokerUser.License.ExpirationDate.ToString() + " PST (approximately ";
				TimeSpan ts = brokerUser.License.ExpirationDate - DateTime.Now ;
				int numOfDays = ts.Days;
				int numOfHours = ts.Hours;
				if (numOfDays == 0) // then just display the hours (12/1/2006 dl)
				{
					if (numOfHours == 1)
						m_DaysTilExpire.Text += "1 hour";
					else
						m_DaysTilExpire.Text += numOfHours + " hours";
				}
				else if (numOfDays == 1)
				{
					m_DaysTilExpire.Text += "1 day and ";
					if (numOfHours == 1)
						m_DaysTilExpire.Text += "1 hour";
					else
						m_DaysTilExpire.Text += numOfHours + " hours";
				}
				else
				{
					m_DaysTilExpire.Text += numOfDays + " days and ";
					if (numOfHours == 1)
						m_DaysTilExpire.Text += "1 hour";
					else
						m_DaysTilExpire.Text += numOfHours + " hours";
				}
				brokerUser.UpdateRenewReminderDate() ;
			}
		}
		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal;  }
		}

		protected void PageInit( object sender , System.EventArgs e ) 
		{
			//OPM 4134 Ethan - required by gService call in f_chat (header.aspx)
            RegisterService("lookupRecord", "/common/RecordsLookupService.aspx");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
        //todo completely remove authid
		protected void btnContinue_Click(object sender, System.EventArgs e)
		{
            RequestHelper.DoNextPostLoginTask(PostLoginTask.CheckLicensing);
        }
	}
}
