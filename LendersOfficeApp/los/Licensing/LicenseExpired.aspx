<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../common/header.ascx" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../common/footer.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Page language="c#" Codebehind="LicenseExpired.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Licensing.LicenseExpired" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>LicenseExpired</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript">
	<!--
		function onAssignLicense()
		{
			showModal("/los/admin/LicenseList.aspx?mode=select&autoassign=true", null, null, null, function(arg){ 
				if (arg.LicenseId != null)
					assignLicense(arg.LicenseId) ;
				else
					alert('You need a valid license to log in') ;
			}) ;
		}
		function onRenewLicense()
		{
			showModal("/los/Licensing/BuyNewLicense.aspx?renew=true", null, null, null, function(arg){ 
				if (arg.LicenseId != null)
					assignLicense(arg.LicenseId) ;
				else
					alert('You need a valid license to log in') ;
			}) ;
		}
		function onBuyNewLicense()
		{
			showModal("/los/Licensing/BuyNewLicense.aspx?autoassign=true", null, null, null, function(arg){ 
				if (arg.LicenseId != null)
					assignLicense(arg.LicenseId) ;
				else
					alert('You need a valid license to log in') ;
			}) ;
		}
		function assignLicense(licenseId)
		{
				document.getElementById("MainContent").innerHTML = "Thank you. We will now log you into the system.<br><br>Please wait one moment..." ;
				
				document.LicenseExpired.licenseid.value = licenseId ;
				document.LicenseExpired.submit() ;
		}
		function logOut() {
			location.href = <%= AspxTools.JsString(VirtualRoot) %> + '/logout.aspx';
		}
	//-->
		</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" class="bgcolor1" bottommargin="0">
		<form id="LicenseExpired" method="post" runat="server">
			<input type="hidden" name="licenseid">
			<table height="100%" width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td><uc1:header id="Header1" runat="server"></uc1:header></td>
				</tr>
				<tr>
					<td height="100%" align="center" valign="middle">
						<div id="MainContent">
							<table id="Table1" cellspacing="0" cellpadding="5" border="0" width="600">
								<% if (m_IsAdmin) { %>
								<tr>
									<td>
										Your login account is currently not associated with an active license or the 
										license that it is associated with has expired. Please select one of the 
										following options or contact your system administrator for assistance.</td>
								</tr>
								<% } else { %>
								<tr>
									<td>
										Your login account is currently not associated with an active license or the 
										license that it is associated with has expired. Please purchase a new license 
										to log in or contact your system administrator for assistance.</td>
								</tr>
								<tr>
									<td><br>
										<br>
									</td>
								</tr>
								<% } %>
								<% if (m_IsAdmin) { %>
								<tr>
									<td align="center"><input type="button" onclick="onAssignLicense();" value="Assign an active/open license to this account"></td>
								</tr>
								<% } %>
								<% if (m_HasExistingLicense) { %>
								<tr>
									<td align="center"><input type="button" onclick="onRenewLicense();" value="Renew my existing license (RECOMMENDED)"></td>
								</tr>
								<% } %>
								<tr>
									<td align="center"><input type="button" onclick="onBuyNewLicense();" value="Buy a new license"></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td valign="middle" bgcolor="black" height="1%"><uc1:footer id="Footer1" runat="server"></uc1:footer>
						<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
