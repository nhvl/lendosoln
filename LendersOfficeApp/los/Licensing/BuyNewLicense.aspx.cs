using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;
using LendersOfficeApp.common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;


namespace LendersOfficeApp.los.Licensing
{
	/// <summary>
	/// Summary description for BuyNewLicense.
	/// </summary>
	public partial class BuyNewLicense : LendersOffice.Common.BasePage
	{

		protected string m_Renew ;
	
		protected void PageLoad(object sender, System.EventArgs e)
		{
		
		}
		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; /*Page.User as BrokerUserPrincipal;*/ }
		}
		protected bool IsRenew
		{
			get { return m_Renew == "true" ; }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

		protected void btnNext_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}
