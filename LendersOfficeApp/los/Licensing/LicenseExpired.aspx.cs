using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LendersOfficeApp.los.admin;
using LendersOfficeApp.common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;


namespace LendersOfficeApp.los.Licensing
{
	/// <summary>
	/// Summary description for LicenseExpired.
	/// </summary>
	public partial class LicenseExpired : LendersOffice.Common.BaseServicePage
	{
		protected bool m_HasExistingLicense = false ;
		protected bool m_IsAdmin ;
		protected void PageLoad(object sender, System.EventArgs e)
		{
			LendersOfficeApp.ObjLib.Licensing.BrokerUser brokerUser = new LendersOfficeApp.ObjLib.Licensing.BrokerUser(CurrentUser.BrokerId, CurrentUser.UserId, CurrentUser.DisplayName) ;
			m_HasExistingLicense = brokerUser.License != null ;
			m_IsAdmin = CurrentUser.HasRole(E_RoleT.Administrator) ;

			if (IsPostBack)
			{
				string licenseId = Request["licenseid"] ;
				if (licenseId != null && licenseId.Length > 0)
				{
					/* --code moved to payment & selection page
					LendersOfficeApp.ObjLib.Licensing.License license = new LendersOfficeApp.ObjLib.Licensing.License(new Guid(licenseId)) ;
					brokerUser.AssignLicense(license) ;
					*/

					if (brokerUser.HasValidLicense())
					{
                        RequestHelper.DoNextPostLoginTask(PostLoginTask.CheckLicensing);
                    }
				}
			}
		}
		private BrokerUserPrincipal CurrentUser
		{
			get { return BrokerUserPrincipal.CurrentPrincipal; }
		}


		protected void PageInit( object sender , System.EventArgs e ) 
		{
			//OPM 4134 Ethan - required by gService call in f_chat (header.aspx)
            RegisterService("lookupRecord", "/common/RecordsLookupService.aspx");
            RegisterJsScript("LQBPopup.js");
            EnableJqueryMigrate = false;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
			this.Init += new System.EventHandler(this.PageInit);
		}
		#endregion
	}
}
















