<%@ Page language="c#" Codebehind="BuyNewLicense.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Licensing.BuyNewLicense" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ml" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BuyNewLicense</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function _init()
		{
			resize( 500 , 300 );
		}

		</script>
	</HEAD>
	<body MS_POSITIONING="FlowLayout" onload="_init();" onkeydown="autoSubmit();" scroll=no>
		<h4 class="page-header">
			<%if (IsRenew) {%>
			Renew License
			<%} else {%>
			Buy License
			<%}%>
		</h4>
		<form id="BuyNewLicense" method="post" runat="server">	
			<table cellspacing="5">
				<tr>
					<td>Please contact your LendingQB system administrator.</td>
				</tr>
                </table>

		</form>
	</body>
</HTML>
