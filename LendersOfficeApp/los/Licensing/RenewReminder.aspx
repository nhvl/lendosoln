<%@ Page language="c#" Codebehind="RenewReminder.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Licensing.RenewReminder" %>
<%@ Register TagPrefix="uc1" TagName="footer" Src="../common/footer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="header" Src="../common/header.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>RenewReminder</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<link href="../../css/stylesheet.css" type="text/css" rel="stylesheet">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript">
	<!--
		function onAssignLicense()
		{
			showModal("/los/admin/LicenseList.aspx?mode=select&autoassign=true", null, null, null, function(arg){ 
				if (arg.LicenseId != null)
					assignLicense(arg.LicenseId) ;
				else
					alert('You can still log in with your current license, but please keep in mind that it will expire soon.') ;
			}) ;
		}
		function onRenewLicense()
		{
			showModal("/los/Licensing/BuyNewLicense.aspx?renew=true", null, null, null, function(arg){
				if (arg.LicenseId != null)
					assignLicense(arg.LicenseId) ;
				else
					alert('You can still log in with your current license, but please keep in mind that it will expire soon.') ;
			 }) ;
		}
		function onBuyNewLicense()
		{
			showModal("/los/Licensing/BuyNewLicense.aspx?autoassign=true", null, null, null, function(arg){ 
				if (arg.LicenseId != null)
					assignLicense(arg.LicenseId) ;
				else
					alert('You can still log in with your current license, but please keep in mind that it will expire soon.') ;
			}) ;
		}
		function assignLicense(licenseId)
		{
				document.getElementById("MainContent").innerHTML = "Thank you. We will now log you into the system.<br><br>Please wait one moment..." ;
				
				document.LicenseExpired.licenseid.value = licenseId ;
				document.LicenseExpired.submit() ;
		}
		function logOut() {
			location.href = <%= AspxTools.JsString(VirtualRoot) %> + '/logout.aspx';
		}
	//-->
		</script>
</HEAD>
	<body MS_POSITIONING="FlowLayout" class="bgcolor1">
		<FORM id="LicenseExpired" method="post" runat="server">
			<INPUT type="hidden" name="licenseid">
			<TABLE id="Table2" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<uc1:header id="Header1" runat="server"></uc1:header></TD>
				</TR>
				<TR>
					<TD vAlign="center" align="middle" height="100%">
						<DIV id="MainContent">
							<TABLE id="Table1" cellSpacing="0" cellPadding="5" width="600" border="0">
								<TR>
									<TD align=center>This is a reminder to let you know that your LendingQB 
										Seat License <span style="font-weight:bold">will expire</span>
										<ml:EncodedLabel id="m_DaysTilExpire" runat="server"></ml:EncodedLabel> from now).<br><br></TD>
								</TR>
								<% if (m_IsAdmin) { %>
								<TR>
									<TD align="middle">
										<INPUT onclick="onAssignLicense();" type="button" value="Assign a more current license to this account"></TD>
								</TR>
								<% } %>
								<TR>
									<TD align="middle">
										<INPUT onclick="onRenewLicense();" type="button" value="Renew my existing license (RECOMMENDED)"></TD>
								</TR>
								<TR>
									<TD align="middle">
										<INPUT onclick="onBuyNewLicense();" type="button" value="Buy a new license"></TD>
								</TR>
								<tr>
									<td align="middle">
										<asp:Button id="btnContinue" runat="server" Text="Continue without purchasing a new license" onclick="btnContinue_Click"></asp:Button></td>
								</tr>
							</TABLE>
						</DIV>
					</TD>
				</TR>
				<TR>
					<TD vAlign="center" bgColor="black" height="1%">
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
						<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg></TD>
				</TR>
			</TABLE>
		</FORM>
	</body>
</HTML>
