<%@ Register TagPrefix="uc" TagName="EmployeePermissionsList" Src="../los/admin/EmployeePermissionsList.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CustomFolderNavigationControl" Src="~/los/admin/CustomFolderNavigationControl.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="MeridianLink.CommonControls" Assembly="MeridianLinkCommonControls" %>
<%@ Page language="c#" Codebehind="myprofile.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.myprofile" EnableSessionState="False" %>
<%@ Register TagPrefix="uc" TagName="RenderReadOnly" Src="../Common/RenderReadOnly.ascx" %>
<%@ Register TagPrefix="uc" TagName="SecurityQuestions" Src="~/Website/PasswordReset/SetUserSecurityQuestions.ascx" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD runat="server">
    <title>Your Profile</title>

<LINK href="../css/stylesheet.css" type=text/css rel=stylesheet >
    <style type="text/css">
		.ErrorMessage { FONT-WEIGHT: bold; COLOR: red; TEXT-ALIGN: center }
		.DescBtn { MARGIN-TOP: 0.5em }
        #_Favorites {height: 600px;}
        #AuthenticatorSetupTable { display: table; margin-top: 5px;}
        #AuthenticatorSetupTable > div {display: table-row; }
        #AuthenticatorSetupTable > div > div {display:table-cell; vertical-align:top; padding:5px;}
        #AuthenticatorSetupTable .Pad { padding-left: 5px; font-size: 10px; }
        #authTokenCode { width: 50px; }
		</style>

</HEAD>
<body MS_POSITIONING="FlowLayout">
    <h4 class="page-header">Your Profile&nbsp; <img runat="server" id="m_profileImage" src="../images/profile.gif" /><ml:EncodedLabel id=m_accountManagerLabel runat="server" visible="False" enableviewstate="False"> - Account Manager</ml:EncodedLabel></h4>
<script type="text/javascript">

    var tabIndex = 0;
    function validatePassword(source, args) {

        var password1 = document.forms[0].elements["m_NewPassword"].value;
        var confirmpassword = document.forms[0].elements["m_ConfirmPassword"].value;
        if (password1 != confirmpassword) {
            args.IsValid = false;
        } else {
            args.IsValid = true;
        }

    }

    function _init() {

     

        addTrimmingToAspRegExValidator(<%= AspxTools.JsGetElementById(Regularexpressionvalidator2) %>);

        changeTab(tabIndex);
        var employeeId = <%= AspxTools.JsString(m_EmployeeId.ToString()) %>;
        var serviceCredentialData = {
            EmployeeId: employeeId,
            EmployeeType: <%= AspxTools.JsString(LendersOffice.ObjLib.ServiceCredential.UserType.LQB.ToString("D")) %>,
            IsUserProfile: true,
            ServiceCredentialJsonId: "ServiceCredentialsJson",
            ServiceCredentialTableId: "ServiceCredentialsTable"
        };

        ServiceCredential.Initialize(serviceCredentialData);
        ServiceCredential.RenderCredentials();


        if ($('#EnableAuthCodeViaAuthenticator:checked').length == 1) {
            $('#AuthenticatorSetupPanel').addClass('Hidden');
        }
        else
        {
            $('#AuthenticatorClear').addClass('Hidden');

        }


        $('#authDisabler').click(function () {
            $('#EnableAuthCodeViaAuthenticator').prop('checked', false);
            $('#IsEnableAuthCodeViaAuthenticator').val('disabled');
        });

        $('#RegisterForAuthenticator').click(function () {
            $(this).addClass('Hidden');
            window.setTimeout(SetupAuthenticator, 0);
        });

        $('#AuthTokenConfirm').click(function () {
            var token = $('#authTokenCode').val();
            var id = $('#AuthIdLocator').val();

            var data = {
                locator: id,
                token : token
            };

            var result = gService.Profile.call('ValidateAndSaveAuthenticatorToken', data);
            if (result.value['Status'] == 'Error') {
                alert(result.value['Error']);
            }
            else if (result.value["IsValid"] == 'True') {
                $('#AuthenticatorSetup').addClass('Hidden');
                $('#AuthenticatorClear').removeClass('Hidden');
                $('#EnableAuthCodeViaAuthenticator').prop('checked', true);
                $('#IsEnableAuthCodeViaAuthenticator').val('');
                $('#AuthIdLocator').val('');
            }
            else {
                alert('Please enter the next code that appears on your authenticator.');
            }
        });
        
        function SetupAuthenticator () {
            $('#AuthyQrCode').remove();
            var container = $('#QrCodeContainer');
            var result = gService.Profile.call('RegisterForAuthenticator');

            if (result.value['Status'] == 'Error') {
                alert(result.value['Error']);
                $('#RegisterForAuthenticator').removeClass('Hidden');
            }
            else {
              var img = $('<img />', { 
                  id: 'AuthyQrCode',
                  src: result.value['Url'],
                  alt: 'QR Code',
                  width: 150,
                  height: 150
                });

                container.prepend(img);

                $('#ManualCodeText').text(result.value["ManualCode"]);
                $('#AuthIdLocator').val(result.value["Key"]);
                $('#AuthenticatorSetup').removeClass('Hidden');
            }

        }

    }
    
    function changeTab(newIndex)
    {
        //remove class from old tab
        var tab = document.getElementById("tab" + tabIndex);
        tab.className = "";
        document.getElementById(tab.getAttribute("data-tab")).style.display = "none";
        
        //make current tab and its content visible
        tab = document.getElementById("tab" + newIndex);
        tab.className = "selected";
        document.getElementById(tab.getAttribute("data-tab")).style.display = "";
        
        tabIndex = newIndex;
        
    }
    
    function showPasswordRules(event) 
	{
      var msg = document.getElementById("m_PasswordRules");
      msg.style.display = "";
      var top = event.clientY - 160;
      if(top < (document.body.scrollTop + 5)) top = document.body.scrollTop + 5;
      msg.style.top = top + "px";
      msg.style.left = (event.clientX - 20) + "px";
    }
    
    function closePasswordRules() 
    {
		document.getElementById("m_PasswordRules").style.display = "none";
    }
    function f_installClientCertificate()
    {
        showModal('/los/InstallClientCertificate.aspx');
    }
    
    function revokeClientCertificate(certificateId)
    {
        if (confirm('Do you want to revoke the client certificate?'))
        {
        __doPostBack('ClientCertificateRevoke', certificateId);
        }
        return false;
    }
    function deleteIp(id)
    {
        if (confirm('Do you want to delete IP?'))
        {
            __doPostBack('RegisteredIpDelete', id);
        }
        return false;
    }
    
    function showSecurityInfoError(msg)
    {
        document.getElementById('SecInfoError').innerText = msg;
        changeTab(6);
    }
    
    $(function(){
        var notif = $('#ShowSupportEmailConfirmation').val();
        
        if (notif) {
            alert(notif);
        }
        
        $('#m_SupportEmail').change(function(){
            $('#sendSupportConfirmation').hide();
        });
        
        $('#sendSupportConfirmation').on("click", function(ev){
            ev.preventDefault();
            var result = gService.Profile.call('ResendConfirmation', { 'SupportEmail' : $('#m_SupportEmail').val() });
            
            if (result.value['Staus'] == 'Error'){
                alert(result.value['UserMessage']);
            }
            else {
                $('#sendSupportConfirmation').hide();
            }
        });

        $('#AddServiceCredentialBtn').on("click", function() {
            ServiceCredential.AddServiceCredential();
        });

        resize(900, 800);
    });

    function prepareSave(event) {
        if ($('#AuthIdLocator').length > 0 && $('#AuthIdLocator').val() != '') {
                return confirm('You have not completed the authenticator registration. Do you want to continue?');
            }

        if (this.isFavoritesModified) {
            saveFavorites(function () {
                $('#m_Save').click();
            });

            return false;
        }

        return true;
    }
</script>
<form id=myprofile method=post runat="server">
    <asp:PlaceHolder runat="server" ID="ErrorDiv">
        <div>
            
        </div>
    </asp:PlaceHolder>
	<table cellSpacing=0 cellPadding=0 width="100%" height="100%">
	<tr valign="top">
    <td style="HEIGHT: 1px">
		
		<div class="Tabs">
		    <ul class = "tabnav">
    		    <li id="tab0" data-tab="_Profile"><a href="#" class="tabchange" onclick="changeTab(0);"> Your Profile</a></li>
                <li id="tab8" data-tab="_Services"><a href="#" class="tabchange" onclick="changeTab(8);">Services</a></li>
    		    <li id="tab1" data-tab="_AccessLevel"><a href="#" class="tabchange" onclick="changeTab(1);">Access Level</a></li>
    		    <li id="tab2" data-tab="_Roles"><a href="#" class="tabchange" onclick="changeTab(2);">Roles</a></li>
    		    <li id="tab3" data-tab="_Permissions"><a href="#" class="tabchange" onclick="changeTab(3);">Permissions</a></li>
    		    <li id="tab4" data-tab="_Picture"><a href="#" class="tabchange" onclick="changeTab(4);">Picture</a></li>
    		    <li id="tab9" data-tab="_Favorites"><a href="#" class="tabchange" onclick="changeTab(9);">Favorites</a></li>
    		    <li id="tab5" data-tab="_IPRestrictions" runat="server"><a href="#" class="tabchange" onclick="changeTab(5);">System Access</a></li>
    		    <li id="tab6" data-tab="_SecurityInfo" runat="server"><a href="#" class="tabchange" onclick="changeTab(6);">Security Info</a></li>
                <li id="tab7" data-tab="_Preferences" runat="server"><a href="#" class="tabchange" onclick="changeTab(7);">Preferences</a></li>
		    </ul>
		</div>
	</td>
	</tr>
	<tr valign="top">
		<td style="PADDING-RIGHT: 8px; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; PADDING-TOP: 8px; ">
				<div id="_Profile" style="display:none" >	
					<table class=FormTable id=Table3 style="MARGIN-TOP: 5px" cellspacing=0 cellpadding=0 width="100%" border=0>
						<tr>
						<td>
							<table id=Table2 cellspacing=0 cellpadding=0 width="100%" border=0>
								<tr>
									<td class=FieldLabel>First Name
									</td>
									<td>
										<asp:textbox id=m_FirstName runat="server">
										</asp:textbox>
										<img src="../images/require_icon.gif" >
										<asp:requiredfieldvalidator id=m_FirstNameValidator runat="server" errormessage="RequiredFieldValidator" controltovalidate="m_FirstName" display="Dynamic">
											<img runat="server" src="../images/error_icon.gif">
										</asp:requiredfieldvalidator>
									</td>
								</tr>
								<tr>
									<td class=FieldLabel>Last Name
									</td>
									<td>
										<asp:textbox id=m_LastName runat="server">
										</asp:textbox>
										<img src="../images/require_icon.gif" >
										<asp:requiredfieldvalidator id=m_LastNameValidator runat="server" errormessage="RequiredFieldValidator" controltovalidate="m_LastName" display="Dynamic">
											<img runat="server" src="../images/error_icon.gif">
										</asp:requiredfieldvalidator>
									</td>
								</tr>
								<tr>
									<td class=FieldLabel>Address
									</td>
									<td>
										<asp:textbox id=m_AddressTF runat="server" width="282px">
										</asp:textbox>
									</td>
								</tr>
								<tr>
									<td class=FieldLabel>
									</td>
									<td>
										<asp:textbox id=m_CityTF runat="server" width="169px">
										</asp:textbox>
										<cc1:statedropdownlist id=m_StateTF runat="server">
										</cc1:statedropdownlist>
										<cc1:zipcodetextbox id=m_ZipcodeTF runat="server" cssclass="mask" preset="zipcode" width="50px">
										</cc1:zipcodetextbox>
									</td>
								</tr>
								<tr>
									<td class=FieldLabel>LendingQB Notification Email
									</td>
									<td>
										<asp:textbox id=m_EmailTF runat="server" Width="220">
										</asp:textbox>
										<img src="../images/require_icon.gif" >
										<asp:requiredfieldvalidator id=m_RequireEmail runat="server" enableviewstate="False" errormessage="RequiredFieldValidator" controltovalidate="m_EmailTF" display="Dynamic">
											<span runat="server">
                                                <img src="../images/error_icon.gif">
											    <b>
												    Required
											    </b>
											</span>
										</asp:requiredfieldvalidator>
										<asp:RegularExpressionValidator id="Regularexpressionvalidator2" runat="server" Display="Dynamic" ControlToValidate="m_EmailTF" ></asp:RegularExpressionValidator>						
									</td>
								</tr>
								
								<tr runat="server" id="SupportCenterRow">
								    <td class="FieldLabel">Support Center Email</td>
								    <td>
								        <asp:TextBox ID="m_SupportEmail" Width="220"  runat="server"></asp:TextBox>
								        <asp:PlaceHolder runat="server" ID="UnconfirmedPlaceHolder" >
								            <span style="color:red">Unconfirmed</span>
								            <a href="#" id="sendSupportConfirmation" >Resend confirmation</a>
								        </asp:PlaceHolder>
								        
								        <asp:PlaceHolder runat="server" ID="ConfirmedPlaceholder">
								            <span style="color:Green">Confirmed</span>
								        </asp:PlaceHolder>
								    </td>
								</tr>
								
								<tr>
									<td class=FieldLabel>Phone
									</td>
									<td>
										<cc1:phonetextbox id=m_PhoneTF runat="server" cssclass="mask" preset="phone" width="120px">
										</cc1:phonetextbox>
										<img src="../images/require_icon.gif" >
										<asp:requiredfieldvalidator id=m_RequirePhone runat="server" enableviewstate="False" errormessage="RequiredFieldValidator" controltovalidate="m_PhoneTF" display="Dynamic">
											<span runat="server">
                                                <img src="../images/error_icon.gif">
											    <b>
											    Required
											    </b>
											</span>
										</asp:requiredfieldvalidator>
									</td>
								</tr>
								<tr>
								    <td class="FieldLabel">Cell Phone</td>
								    <td>
								        <cc1:PhoneTextBox ID="m_CellTF" runat="server" CssClass="mask" preset="phone" Width="120px" />
								        <img src="../images/require_icon.gif" id="CellRequiredImg" runat="server" >
                                        <asp:CheckBox runat="server" id="IsCellphoneForMultiFactorOnly" Text="Private: For multi-factor authentication only" />
								        <asp:requiredfieldvalidator id="m_RequireCell" runat="server" enableviewstate="False" errormessage="RequiredFieldValidator" controltovalidate="m_CellTF" display="Dynamic">
											<span runat="server">
                                                <img src="../images/error_icon.gif">
											    <b>
											        Required
											    </b>
											</span>
									    </asp:requiredfieldvalidator>
								   </td>
								</tr>
								<tr>
									<td class=FieldLabel>Fax </td>
									<td>
										<cc1:phonetextbox id=m_FaxTF runat="server" cssclass="mask" preset="phone" width="120px">
										</cc1:phonetextbox>
									</td>
								</tr>
								<tr>
									<td class="FieldLabel" noWrap>
										<ml:EncodedLabel ID="m_AgentLicenseLabel" Runat="server" text="Agent License #"></ml:EncodedLabel>
									</td>
									<td noWrap>
										<asp:TextBox id="m_AgentLicenseNumber" runat="server" width="120px"></asp:TextBox>&nbsp;&nbsp;
									</td>
								</tr>
								<tr>
									<td class=FieldLabel>
										<span>Send notification when loan events occur
										</span>
									</td>
									<td valign=top>
										<asp:radiobuttonlist id="m_newOnlineLoanEventNotifOptionT" runat="server" repeatlayout="Flow" repeatdirection="Horizontal">
											<asp:listitem value="0">Yes</asp:listitem>
											<asp:listitem value="1">No</asp:listitem>
										</asp:radiobuttonlist>
									</td>
								</tr>
								<tr>
									<td class=FieldLabel>
										<span>Send task-related notifications
										</span>
									</td>
									<td valign=top>
										<asp:radiobuttonlist id="m_TaskRelatedNotifOptionT" runat="server" repeatlayout="Flow" repeatdirection="Horizontal">
											<asp:listitem value="0">Yes</asp:listitem>
											<asp:listitem value="1">No</asp:listitem>
										</asp:radiobuttonlist>
									</td>
								</tr>
								<tr>
									<td colspan=2><br></td></tr>
								<tr>
									<td class=FormTableSubHeader>Login Information</td>
									<td class=FormTableSubHeader align=right></td>
								</tr>
								<tr>
									<td colspan=2><span id=m_pwMessage runat="server"><i>Leave the password blank if it's not being changed.</i> </span></td>
								</tr>
								<tr>
									<td class=FieldLabel><span id=m_loginLabel runat="server">Login Name </span></td>
									<td><asp:textbox id=m_Login runat="server" readonly="True"></asp:textbox></td>
								</tr>
								<tr>
									<td class=FieldLabel><span id=m_oldPWLabel runat="server">Old password </span></td>
									<td><asp:textbox id=m_OldPassword runat="server" textmode="Password"></asp:textbox></td>
								</tr>
								<tr>
									<td class=FieldLabel><span id=m_newPWLabel runat="server">New password <span id="pwRules" style="FONT-SIZE: 0.8em;"> 
											(<A onclick=showPasswordRules(event); href='#"' ?>show rules</A>)
										</span></span></td>
									<td><asp:textbox id=m_NewPassword runat="server" textmode="Password"></asp:textbox>
										<div id=m_PasswordRules style="BORDER-RIGHT: black 3px solid; PADDING-RIGHT: 5px; BORDER-TOP: black 3px solid; DISPLAY: none; PADDING-LEFT: 5px; PADDING-BOTTOM: 5px; BORDER-LEFT: black 3px solid; WIDTH: 325px; PADDING-TOP: 5px; BORDER-BOTTOM: black 3px solid; POSITION: absolute; BACKGROUND-COLOR: whitesmoke">
											<table width="100%">
												<tr>
													<td><%=AspxTools.HtmlControl(LendersOffice.Constants.ConstApp.PasswordGuidelinesHtml)%></td>
												</tr>
												<tr>
													<td align=center>[ <A onclick=closePasswordRules();>Close</A> ]</td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
								<tr>
									<td class=FieldLabel><span id=m_retypePWLabel runat="server">Retype new password </span></td>
									<td><asp:textbox id=m_ConfirmPassword runat="server" textmode="Password"></asp:textbox></td>
								</tr>
								<tr id="ValidatorRow" runat="server">
									<td style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; FONT-FAMILY: Arial, Verdana" colspan=2>
										<span>
											<asp:customvalidator id=m_passwordValidator runat="server" errormessage="Passwords do not match." display="Dynamic" clientvalidationfunction="validatePassword">
											</asp:customvalidator>
										</span>
										<span>
											<ml:EncodedLabel id=m_OldPasswordErrorMsg style="COLOR: red" runat="server">
											</ml:EncodedLabel>
										</span>
										<span>
											<asp:regularexpressionvalidator id=RegularExpressionValidator1 runat="server" errormessage="Password needs to be at least 6 characters in length." controltovalidate="m_NewPassword" validationexpression="(.){6,}">
											</asp:regularexpressionvalidator>
										</span>
									</td>
								</tr>
							    <asp:Repeater runat="server" ID="DocumentVendorCustomLogins" OnItemDataBound="BindDocumentVendor">
							        <ItemTemplate>
							            <tr>
								            <td class=FormTableSubHeader>
								                <asp:HiddenField runat="server" ID="VendorId" />
								                <ml:EncodedLabel runat="server" ID="VendorName" />
								            </td>
								            <td class=FormTableSubHeader align=right></td>
							            </tr>
							            <tr>
								            <td colspan="2">
									            <div class="ErrorMessage"> <ml:EncodedLiteral EnableViewState="False" Runat="server" ID="m_dm_Error"> </ml:EncodedLiteral> </div>
								            </td>
							            </tr>
							            <asp:PlaceHolder runat="server" ID="UsernamePanel">
							                <tr>
								                <td class="FieldLabel"> 
									                <label for="Username"> Username </label>
								                </td>
								                <td>
									                 <asp:TextBox ID="Username" Runat="server" ReadOnly="false" > </asp:TextBox>
								                </td>
							                </tr>
							            </asp:PlaceHolder>
							            <asp:PlaceHolder runat="server" ID="PasswordPanel">
							                <tr>
								                <td class="FieldLabel"> 
								                <label for="Password"> 	Password </label>
								                </td>
								                <td>
									                <asp:TextBox Runat="server" ID="Password" TextMode="Password" > </asp:TextBox>
								                </td>
            									
							                </tr>
							            </asp:PlaceHolder>
							            <asp:PlaceHolder runat="server" ID="ClientIdPanel">
                                            <tr>
								                <td class="FieldLabel"> 
								                <label for="ClientId"> 	Client Id </label>
								                </td>
								                <td>
									                <asp:TextBox Runat="server" ID="ClientId" MaxLength="20"> </asp:TextBox>
								                </td>
							                </tr>
							            </asp:PlaceHolder>
							        </ItemTemplate>
							    </asp:Repeater>
                                <asp:Repeater ID="m_appraisalVendors" runat="server" OnItemDataBound="AppraisalVendorBind">
                                    <ItemTemplate>
                                        <tr>
                                            <td class=FormTableSubHeader>
                                            <asp:HiddenField runat="server" ID="VendorId" />
					                            <ml:EncodedLabel runat="server" ID="VendorName"></ml:EncodedLabel> Login
					                        </td>
							                <td class=FormTableSubHeader align=right></td>
							            </tr>
			                            <asp:PlaceHolder id="AccountIdField" runat="server">
							                <tr>
			                                    <td class="FieldLabel">
			                                        Account ID
			                                    </td>
			                                    <td>
			                                        <asp:TextBox ID="LoginAccountId" MaxLength="50" runat="server"></asp:TextBox>
			                                    </td>
                                            </tr>
                                        </asp:PlaceHolder>
                                        <tr>
							                <td class="FieldLabel"> 
							                    Username
							                </td>
							                <td>
								                 <asp:TextBox ID="LoginUserName" MaxLength="50" runat="server"></asp:TextBox>
							                </td>
					                    </tr>
					                    <tr>
					                        <td class="FieldLabel">
					                            Password
					                        </td>
					                        <td>
					                            <asp:TextBox ID="LoginPassword" TextMode="Password" runat="server"></asp:TextBox>
					                        </td>
					                    </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Repeater runat="server" ID="m_irs4506TVendors" OnItemDataBound="IRS4506TVendorBind">
							        <ItemTemplate>
							            <tr>
								            <td class="FormTableSubHeader">
								                <asp:HiddenField runat="server" ID="VendorId" />
								                <ml:EncodedLabel runat="server" ID="VendorName" />
								            </td>
								            <td class="FormTableSubHeader" align=right></td>
							            </tr>
							            <tr>
							                <td class="FieldLabel"> 
							                    Username
							                </td>
							                <td>
								                 <asp:TextBox ID="LoginUserName" MaxLength="50" runat="server"></asp:TextBox>
							                </td>
					                    </tr>
					                    <tr>
					                        <td class="FieldLabel">
					                            Password
					                        </td>
					                        <td>
					                            <asp:TextBox ID="LoginPassword" TextMode="Password" runat="server"></asp:TextBox>
					                        </td>
					                    </tr>
							            <asp:PlaceHolder runat="server" ID="LoginAccountIdPanel">
                                            <tr>
								                <td class="FieldLabel"> 
								                <label for="AccountId"> Account Id </label>
								                </td>
								                <td>
									                <asp:TextBox Runat="server" ID="LoginAccountId" MaxLength="20"> </asp:TextBox>
								                </td>
							                </tr>
							            </asp:PlaceHolder>
							        </ItemTemplate>
							    </asp:Repeater>
                                <asp:PlaceHolder ID="SignaturePlaceHolder" runat="server" Visible="false">
							    <tr>
										<td class=FormTableSubHeader>Signature</td>
										<td class=FormTableSubHeader align=right></td>
							    </tr>
							    <tr><td>&nbsp;</td></tr>
								<tr>								    
								    <td noWrap style="padding-top:0.2em" colspan="2">
								        <asp:FileUpload ID="signatureUpload" Width="320" ToolTip="Select the image to upload" runat="server" />
								        <asp:Button runat="server" ID="btnSignatureUpload" Height="1.6em" Width="6em" Text="Upload" onclick="btnSignatureUpload_Click" UseSubmitBehavior="false" Enabled="true"></asp:Button><br /> 								        
								         <div id="divError" runat="server" visible="false" style="color:Red; font:8pt arial bold"></div>									         
								        <label style="font:italic 8pt arial">(The image should be a JPEG, PNG, GIF or BMP file, smaller than 100KB, and readable at 160x18 pixels.)</label><br/>
								    </td>
								</tr>
								<tr><td colspan="2">&nbsp;</td></tr>
								<tr>								    
								    <td colspan="2">
								        <div style="padding-top:0.2em; font-size:0.9em;"> <asp:Image ID="imgSignature" Width="160" runat="server"/> &nbsp;
								        <asp:LinkButton ID="btnRemoveImage" runat="server" Text="Remove Signature" OnClick="OnRemoveSignature_Click"></asp:LinkButton></div>
								    </td>
								</tr>
                                </asp:PlaceHolder>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div id="_AccessLevel" style="display:none" >
				<div class="FieldLabel" style="MARGIN-BOTTOM: 8px;">
					Access Level
				</div>
				<div style="COLOR: black; FONT-STYLE: italic;padding-left:10px;margin-bottom:8px">*Please ask your administrator about any changes.</div>
				<div style="BORDER-RIGHT: thin groove; PADDING-RIGHT: 8px; BORDER-TOP: thin groove; PADDING-LEFT: 8px; PADDING-BOTTOM: 8px; BORDER-LEFT: thin groove; WIDTH: 400px; PADDING-TOP: 8px; BORDER-BOTTOM: thin groove; HEIGHT: 100px; BACKGROUND-COLOR: transparent">
					<asp:Repeater id="m_employeeAccessLevel" runat="server">
						<ItemTemplate>
							<div>	
								<asp:CheckBox id="On" runat="server" code="<%# AspxTools.HtmlString(Container.DataItem.ToString()) %>">
								</asp:CheckBox>
								<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "Label" ).ToString()) %>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</div>
			<div id="_Roles" style="display:none" >
				<div class="FieldLabel" style="MARGIN-BOTTOM: 8px;">Employee Roles</div>
				<div style="COLOR: black; FONT-STYLE: italic;padding-left:10px;margin-bottom:8px">*Please ask your administrator about any changes.</div>
				
				<div style="PADDING-LEFT: 10px; WIDTH: 400px; HEIGHT: auto; BORDER: thin groove; BACKGROUND-COLOR: transparent; PADDING: 8px;">
					<asp:Repeater id="m_employeeRoleListing" runat="server">
						<ItemTemplate>
							<div nowrap>
								<asp:CheckBox id="On" runat="server" code="<%# AspxTools.HtmlString(Container.DataItem.ToString()) %>">
								</asp:CheckBox>
								<%# AspxTools.HtmlString(DataBinder.Eval( Container.DataItem , "RoleDesc" ).ToString()) %>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</div>
			<div id="_Permissions" style="display:none" >
				<div style="MARGIN-BOTTOM: 8px;">
					<b>Access Permissions</b> <style="COLOR: black; FONT-STYLE: italic;padding-left:10px;margin-bottom:8px">(scroll to see more options)</style>
				</div>
				<div style="COLOR: black; FONT-STYLE: italic;padding-left:10px;margin-bottom:8px">*Please ask your administrator about any changes.</div>
				
				<div style="PADDING-LEFT: 10px; WIDTH: 400px; HEIGHT: 340px; BORDER: thin groove; OVERFLOW-Y: scroll; BACKGROUND-COLOR: transparent; PADDING: 8px;">
					<asp:Panel runat="server" id="m_PermissionsPanel" Runat="server">
						<uc:EmployeePermissionsList id="m_EmployeePermissionsList" runat="server"></uc:EmployeePermissionsList>
					</asp:Panel>
				</div>
				<% if ( IsPmlEnabled ) { %>
				<div style="PADDING-LEFT:13px">
					<span style="FONT-WEIGHT:bold"><br>Rate lock requests for <span style="FONT-STYLE:italic">possibly</span> expired rates:</span>&nbsp;
					<ml:EncodedLabel ID=m_rateLockExpLabel Runat=server></ml:EncodedLabel>
				</div>
				<% } %>
			</div>
			<div id="_Picture" style="display:none" >
			    <div style="width:100%;">
			        <br/>
					Please upload an image (it will be resized down to 128x128 pixels).  Upload or removal of images will be reflected immediately.
					<br/>
					<br/>
					<div style="width:100%; text-align:center;">
					    <iframe id="ImgUploader" src="UploadProfileImage.aspx" width="80%" height="220px;" frameborder="0"></iframe>
					</div>
				</div>
			</div>
			<div id="_IPRestrictions" style="display:none" runat="server">
						<span style="font-weight:bold">Client Certificate</span>
			<ml:CommonDataGrid ID="m_clientCertificateGrid" runat="server">
			    <Columns>
			        <asp:BoundColumn DataField="Description" HeaderText="Device Name"/>
			        <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date"/>
			        <asp:TemplateColumn>
			            <ItemTemplate>
			            <a href="#" onclick="return revokeClientCertificate('<%# AspxTools.HtmlString(Eval("CertificateId")) %>');">revoke</a>
			            </ItemTemplate>
			        </asp:TemplateColumn>
			    </Columns>
			</ml:CommonDataGrid>
			<a href="#" onclick="f_installClientCertificate();" id="installCertificateLink" runat="server">Install client certificate</a>
			<hr />
			<span style="font-weight:bold">IP Access</span>
			<ml:CommonDataGrid ID="m_registeredIpDataGrid" runat="server">
			    <Columns>
			        <asp:BoundColumn DataField="IpAddress" HeaderText="IP Address" />
			        <asp:BoundColumn DataField="IsRegistered" HeaderText="Registered IP" />
			        <asp:BoundColumn DataField="CreatedDate" HeaderText="Created Date" />
			        <asp:TemplateColumn>
			            <ItemTemplate>
			            <a href="#" onclick="return deleteIp('<%# AspxTools.HtmlString(Eval("Id")) %>');">remove</a>
			            </ItemTemplate>
			        </asp:TemplateColumn>
			    </Columns>
			</ml:CommonDataGrid>
			<hr />
			
            <div id="AuthenticatorInfo" >
    			<span class="FieldLabel">Authenticator</span>
                <br />
                <div id="AuthenticatorClear" >
                    <input type="checkbox" runat="server" disabled="disabled" id="EnableAuthCodeViaAuthenticator" /> 
                    <asp:HiddenField runat="server" id="IsEnableAuthCodeViaAuthenticator" />
                    Authenticator App Enabled <input type="button" value="Disable authenticator app" id="authDisabler" />
                </div>
                <div id="AuthenticatorConfiguredCorrectly" class="Hidden">
                    The authenticator has been configured correctly. 
                </div>
                <div id="AuthenticatorSetupPanel" >
                <input type="button" value="Enable Authenticator App" id="RegisterForAuthenticator"/>
                <div id="AuthenticatorSetup" class="Hidden">
                    <div>The authenticator app will be the primary source of one time passwords. You may still request one manually from LendingQB if your authenticator app is not available. To setup the authenticator application please complete the following steps:</div>
                <div id="AuthenticatorSetupTable" class="Hidden">
                    <div class="FieldLabel">Step 1:</div>
                    <div>
                        <div class="FieldLabel Pad">Scan this QR Code</div>
                        <div id="QrCodeContainer">
                            <div><span>Use your favorite authenticator app to scan this QR code.</span></div>
                        </div>
                    </div>
                    <div>
                        <div class="FieldLabel Pad">Manual Code</div>
                        <div>
                            <span id="ManualCodeText"></span>
                            <div><span>If you cannot scan the QR code enter this code manually.</span></div>
                        </div>
                    </div>
                    <div class="FieldLabel">
                        Step 2:
                    </div>
                    <div >
                        <div class="FieldLabel Pad">Authenticator Code</div>
                        <div>
                            <input type="text" id="authTokenCode" onkeyup="this.value=this.value.replace(/[^\d]+/,'')" maxlength="6"  />   
                            <img src="../images/require_icon.gif" alt="Required">
                            <div><span class="smaller-font">To validate setup please enter the current token code generated by your authenticator app.</span></div>
                        </div>
                    </div>

                    <div class="FieldLabel">
                        Step 3
                    </div>

                    <div class="Pad">
                        <div></div>
                        <div>
                            <input type="button" value="Complete" id="AuthTokenConfirm" />
                            <img src="../images/require_icon.gif" alt="Required">
                            <img src="../images/error_icon.gif" alt="Required">
                            <b>Required</b>
                        </div>
                    </div>
                    <input type="hidden" runat="server" id="AuthIdLocator" />
                </div>
                    </div>
                    </div>
                <hr />
                </div>
            </div>
			<div id="_SecurityInfo" style="display:none" >
				<div class="FieldLabel" style="MARGIN-BOTTOM: 8px;">Security Questions</div>
				<div style="COLOR: black; FONT-STYLE: italic;padding-left:10px;margin-bottom:8px">If you forget your password these questions will be required to verify your identity</div>
				<span id="SecInfoError" style="COLOR: red; font-weight: bold" ></span>
				<uc:SecurityQuestions ID="SecurityQuestions" runat="server" EditMode="Edit" />
			</div>
            <div id="_Preferences" style="display:none">
                <table cellspacing="1" cellpadding="3" width="100%" border="0">
	                <tr>
		                <td class=FormTableHeader>Your Preferences</td>
	                </tr>
                </table>

                <table class="FormTable" id="preferencesTable" style="MARGIN-TOP: 5px" cellspacing="0" cellpadding="0" width="100%" border="0">
	                <tr>
		                <td class="FieldLabel">
                            <asp:CheckBox id="m_OptsToUseNewLoanEditorUI" runat="server" Text="Use New UI" />			                
		                </td>
	                </tr>
                </table>
            </div>
            <div id="_Services" style="display:none">
                <div>
                    <div id="ServiceCredentialsDiv">
                        <table id="ServiceCredentialsTable" class="DataGrid" ><tbody></tbody></table>
                    </div>
                    <br />
                    <input type="button" id="AddServiceCredentialBtn" value="Add Credential" />
                </div>
            </div>
            <div id="_Favorites" style="display:none">
                <div class="landing-page">
                    Landing page: 
                    <span ID="LandingPageDescription"></span>
                    <asp:HiddenField ID="LandingPageId" runat="server" />
                    <a class="change-link">Change Selection</a>
                    <a class="default-link">Set to Default</a>
                </div>
                <div>
                    <uc1:CustomFolderNavigationControl IsAdmin="false" id="CustomFolderNavigationControl" runat="server"></uc1:CustomFolderNavigationControl>
                </div>
            </div>
	</td>
	</tr>

	<tr>
	<td align="center" style="PADDING-RIGHT: 16px; PADDING-LEFT: 16px; PADDING-BOTTOM: 16px; PADDING-TOP: 8px; HEIGHT: 1px">
	    <asp:PlaceHolder runat="server" ID="ErrorMessagePlaceholder" Visible="false">
	        <div class="ErrorMessage">
	            Save failed:  <ml:EncodedLiteral runat="server" ID="SaveErrorMessage"></ml:EncodedLiteral>
	        </div>
	    </asp:PlaceHolder>
		<cc1:nodoubleclickbutton OnClientClick="return prepareSave();" id=m_Save runat="server" text="  OK  " onclick="m_Save_Click">
		</cc1:nodoubleclickbutton>
		<input onclick=onClosePopup(); type=button value="Cancel">
	</td>
	</tr>
	</table>
	<uc:RenderReadOnly id="readOnly" runat="server">
	</uc:RenderReadOnly>
</form>
</body>
</HTML>
