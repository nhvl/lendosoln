﻿#region Generated Code
namespace LendersOfficeApp.los.Regions
#endregion
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Regions;
    using LendersOffice.Security;

    /// <summary>
    /// Page for editing Regions.
    /// </summary>
    public partial class EditRegions : BaseServicePage
    {
        /// <summary>
        /// Gets a value indicating whether a javascript brower check
        /// requiring IE is added to the page.
        /// </summary>
        /// <remarks>
        /// Set to false because the browser check fails in IE11 edge mode, which
        /// is required by angular. This page should be cross compatable anyway.
        /// </remarks>
        /// <value>If true, add IE only check to page.</value>
        protected override bool EnableIEOnlyCheck
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the render mode in IE.
        /// </summary>
        /// <returns>X-UA-Comaptible meta key value.</returns>
        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.Edge;
        }

        /// <summary>
        /// Handles Page_Init event.
        /// </summary>
        /// <param name="sender">Object that caused this event.</param>
        /// <param name="e">Event Arguments.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.EnableJqueryMigrate = false;

            this.RegisterService("main", "/los/Regions/EditRegionsService.aspx");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("Regions/EditRegions.js");
            RegisterCSS("Regions/EditRegions.css");

            this.RegisterJsScript("angular-1.5.5.min.js");
        }

        /// <summary>
        /// Handles Page_Load event.
        /// </summary>
        /// <param name="sender">Object that caused this event.</param>
        /// <param name="e">Event Arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            this.RegisterJsGlobalVariables("BrokerId", principal.BrokerId);
            this.RegisterJsGlobalVariables("DefaultRegionId", Region.DefaultRegionId);
        }

        /// <summary>
        /// Export region data to CSV.
        /// </summary>
        /// <param name="sender">Object that caused this event.</param>
        /// <param name="a">Event Arguments.</param>
        protected void ExportClick(object sender, System.EventArgs a)
        {
            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;

            List<Tuple<string, string, string, string, string>> csvData = new List<Tuple<string, string, string, string, string>>();
            Dictionary<int, RegionSet> regionSets = RegionSet.GetAllRegionSets(brokerId);

            foreach (RegionSet regionSet in regionSets.Values)
            {
                foreach (Region region in regionSet.Regions.Values)
                {
                    if (region.Id == Region.DefaultRegionId)
                    {
                        continue;
                    }

                    foreach (int fips in region.FipsCountyCodes)
                    {
                        var pair = StateInfo.Instance.StatesCountiesAndFipsMap.Where(kvp => kvp.Value.ContainsValue(fips)).FirstOrDefault();
                        string state = pair.Key;
                        string county = pair.Value.Where(kvp => kvp.Value == fips).FirstOrDefault().Key;

                        csvData.Add(new Tuple<string, string, string, string, string>(regionSet.Name, region.Name, region.Description, county, state));
                    }
                }
            }

            string tempFile = TempFileUtils.NewTempFilePath();
            using (StreamWriter sw = new StreamWriter(tempFile))
            {
                // Write header.
                sw.WriteLine("Region Set,Region Name,Description,County,State");

                // Write rows.
                foreach (var tuple in csvData.OrderBy(t => t.Item1).ThenBy(t => t.Item2).ThenBy(t => t.Item5).ThenBy(t => t.Item4))
                {
                    sw.Write($"\"{tuple.Item1}\",");    // regionSet.Name
                    sw.Write($"\"{tuple.Item2}\",");    // region.Name
                    sw.Write($"\"{tuple.Item3}\",");    // region.Description
                    sw.Write($"\"{tuple.Item4}\",");    // region.county
                    sw.Write($"\"{tuple.Item5}\"");     // region.state
                    sw.WriteLine();
                }
            }

            RequestHelper.SendFileToClient(this.Context, tempFile, "text/plain", "regions.csv");
            this.Response.SuppressContent = true;
            this.Context.ApplicationInstance.CompleteRequest();
        }
    }
}
