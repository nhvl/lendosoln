﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditRegions.aspx.cs" Inherits="LendersOfficeApp.los.Regions.EditRegions" %>

<!DOCTYPE html>
<html ng-app="regionEditor">
    <head runat="server">
        <title>Edit Regions</title>
    </head>
    <body>
        <form runat="server">
            <div id="dialog-ManageRegionSets" title="Region Sets" ng-controller="ManageRegionSetsController" ng-form="ManageRegionSetsForm">
                <div class="ManageRegionSetsTableDiv">
                    <table class="ManageRegionSetsTable">
                        <thead>
                            <tr class="GridHeader">
                                <th>&nbsp;</th>
                                <th>Region Set Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="GridAutoItem" ng-repeat="regionSet in regionSets" ng-if="!regionSet.isDeleted">
                                <td>
                                    <a ng-click="removeItem(regionSet.Id)">remove</a>
                                </td>
                                <td>
                                    <input type="text" maxlength="50" ng-model="regionSet.Name" ng-change="markDirty(regionSet.Id)" unique="'Name'" unique-within="regionSets" unique-this="regionSet" />
                                </td>
                                <td>
                                    <input type="text" maxlength="100" ng-model="regionSet.Description" ng-change="markDirty(regionSet.Id)" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <div class="ManageRegionSetsButtonDivLeft">
                        <input type="button" value="Add" id="ManageRegionSetsAdd" ng-click="addItem()" />
                    </div>
                    <div class="ManageRegionSetsButtonDivRight">
                        <input type="button" value="OK" ng-click="onOk()" ng-disabled="ManageRegionSetsForm.$invalid"/>
                        <input type="button" value="Cancel" ng-click="onCancel()" />
                    </div>
                </div>
            </div>

            <div id="dialog-EditRegion" title="Edit Region" ng-controller="EditRegionController" ng-form="EditRegionForm">
                <table class="EditRegionTable">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <table class="EditRegionInfo">
                                    <tr>
                                        <td>
                                            Region Name
                                        </td>
                                        <td>
                                            <input type="text" maxlength="50" ng-model="currentRegion.Name" ng-disabled="currentRegion.Id == DefaultRegionId" unique="'Name'" unique-within="currentRegionSet.Regions" unique-this="currentRegion" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Region Set
                                        </td>
                                        <td>
                                            {{currentRegionSet.Name}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Description
                                        </td>
                                        <td>
                                            <input type="text" class="EditRegionDescription" maxlength="100" ng-model="currentRegion.Description" ng-disabled="currentRegion.Id == DefaultRegionId" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                State
                            </td>
                            <td>
                                County
                                <a ng-disabled="currentRegion.Id == DefaultRegionId" ng-click="clickSelectAllLink()">select all</a>
                                <a ng-disabled="currentRegion.Id == DefaultRegionId" ng-click="clickDeselectAllLink()">deselect all</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                search:
                                <input type="text" class="EditRegionStateSearch" maxlength="2" ng-model="stateFilterString" />
                            </td>
                            <td>
                                search:
                                <input type="text" ng-model="countyFilterString" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="EditRegionSelectDiv">
                                    <div ng-repeat="state in states | filter:stateFilter">
                                        <label><input type="checkbox" ng-model="state.isChecked" ng-click="clickStateBox(state)" value="{{state.Abriv}}" /> {{state.Abriv}}</label>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="EditRegionSelectDiv">
                                    <div ng-repeat="state in states" ng-if="state.isChecked">
                                        <div class="EditRegionStateGroup" ng-click="onStateGroupClick($event)">
                                            <span class="stateAbriv">{{state.Abriv}}</span>
                                            <label><input ng-model="state.areAllCountiesChecked" ng-click="clickAllCountiesBox(state)" type="checkbox" ng-disabled="currentRegion.Id == DefaultRegionId" /> All Counties</label>
                                        </div>
                                        <div ng-if="state.Abriv == selectedState">
                                            <div ng-repeat="county in state.Counties | filter:countyFilter">
                                                <label>
                                                    <input type="checkbox" ng-model="county.isChecked" ng-click="setAreAllCountiesChecked(state)" value="{{county.Fips}}" ng-disabled="currentRegion.Id == DefaultRegionId || county.assignedRegionName" />
                                                    {{county.Name}}
                                                    <span ng-if="county.assignedRegionName">
                                                        ({{county.assignedRegionName}})
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="EditRegionButtonDiv" >
                    <input type="button" ng-click="onOk()" value="OK" ng-disabled="EditRegionForm.$invalid" />
                    <input type="button" ng-click="onCancel()" value="Cancel" />
                </div>
            </div>

            <div class="FormTableHeader">Edit Regions</div>
            <br />
            <div id="RegionList" class="RegionList" ng-controller="RegionListController">
                <div>
                    Region Set:
                    <select ng-model="selectedSetId" ng-options="regionSet.Id as regionSet.Name for regionSet in regionSets | orderBy:'Name'" ></select>
                    <input type="button" value="Manage Region Sets" ng-click="onManageRegionSets()" />
                </div>

                <div class="RegionTableDiv">
                    <table class="RegionTable">
                        <thead>
                            <tr class="GridHeader" >
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>Region Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="GridAutoItem" ng-repeat="region in getSelectedSetRegions() | orderBy:'Name'">
                                <td>
                                    <a ng-click="editRegion(region.Id)">{{region.Id == DefaultRegionId ? "view" : "edit"}}</a>
                                </td>
                                <td>
                                    <a ng-disabled="region.Id == DefaultRegionId" ng-click="deleteRegion(region.Id)">delete</a>
                                </td>
                                <td>
                                    {{region.Name}}
                                </td>
                                <td>
                                    {{region.Description}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="RegionListLeft">
                    <input type="button" ng-click="onAddNew()" value="Add new region" />
                    <asp:button id="m_Export" onclick="ExportClick" runat="server" Text="Export all to CSV" ></asp:button>
                </div>

                <div class="RegionListRight">
                    <input type="button" ng-click="onClose()" value="Close" />
                </div>
            </div>
        </form>
    </body>
</html>
