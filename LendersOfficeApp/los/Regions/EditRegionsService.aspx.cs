﻿#region Generated Code
namespace LendersOfficeApp.los.Regions
#endregion
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;

    using DataAccess;
    using DataAccess.FeeService;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Regions;
    using LendersOffice.Security;

    /// <summary>
    /// Edit Regions Service Page.
    /// </summary>
    public partial class EditRegionsService : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Triggers a process by name.
        /// </summary>
        /// <param name="methodName">The name of the method to invoke.</param>
        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "LoadRegionSets":
                    this.LoadRegionSets();
                    break;
                case "GetStatesAndCounties":
                    this.GetStatesAndCounties();
                    break;
                case "SaveRegionSets":
                    this.SaveRegionSets();
                    break;
                case "SaveRegion":
                    this.SaveRegion();
                    break;
                case "DeleteRegion":
                    this.DeleteRegion();
                    break;
                case "CheckFeeServiceForRegionSetId":
                    this.CheckFeeServiceForRegionSetId();
                    break;
                case "CheckFeeServiceForRegionId":
                    this.CheckFeeServiceForRegionId();
                    break;
                default:
                    throw new ArgumentException($"Unknown method {methodName}");
            }
        }

        /// <summary>
        /// Load region sets from DB.
        /// </summary>
        protected void LoadRegionSets()
        {
            Dictionary<int, RegionSet> setsDict = RegionSet.GetAllRegionSets(PrincipalFactory.CurrentPrincipal.BrokerId);
            List<RegionSet> setsArray = setsDict.Values.OrderBy(rs => rs.Name).ToList();

            this.SetResult("RegionSets", SerializationHelper.JsonNetSerialize(setsArray));
        }

        /// <summary>
        /// Save region sets to DB.
        /// </summary>
        protected void SaveRegionSets()
        {
            Guid brokerId = GetGuid("BrokerId");
            string updatedSetsJson = GetString("UpdatedSets");
            string deletedSetsJson = GetString("DeletedSets");

            List<RegionSet> updatedSets = SerializationHelper.JsonNetDeserialize<List<RegionSet>>(updatedSetsJson);
            List<RegionSet> deletedSets = SerializationHelper.JsonNetDeserialize<List<RegionSet>>(deletedSetsJson);

            if (!updatedSets.Any() && !deletedSets.Any())
            {
                return;
            }

            // Check fee service before deleting sets (editing is fine).
            FeeServiceRevision currentFeeRevision = FeeServiceApplication.GetCurrentRevision(brokerId, false);
            IEnumerable<RegionSet> feeServiceSets = deletedSets.Where(rs => IsRegionSetInFeeService(rs.Id, currentFeeRevision));
            if (feeServiceSets.Any())
            {
                string msg = $"[EditRegionService] Attempt to delete RegionSets used in Fee Sergion for brokerId=[{brokerId}]. RegionSetsIds=[{string.Join(",", feeServiceSets)}].";
                throw new CBaseException(ErrorMessages.Generic, msg);
            }

            RegionSet.UpdateSets(brokerId, updatedSets, deletedSets);
        }

        /// <summary>
        /// Save region to DB.
        /// </summary>
        protected void SaveRegion()
        {
            Guid brokerId = GetGuid("BrokerId");
            int regionId = GetInt("RegionId");
            string regionSetJson = GetString("RegionSet");

            // Don't save default region.
            if (regionId == Region.DefaultRegionId)
            {
                Tools.LogWarning($"[EditRegionService] Attempt to edit Default Region for brokerId=[{brokerId}]. Cannot edit Default Region.");
                return;
            }

            // Check set for mutual exclusivity.
            RegionSet regionSet = SerializationHelper.JsonNetDeserialize<RegionSet>(regionSetJson);
            if (!regionSet.CheckRegionExclusivity())
            {
                throw new CBaseException("Regions in Region Set are not mutually exclusive.", "Regions in Region Set are not mutually exclusive.");
            }

            // Save Region.
            Region region = regionSet.Regions[regionId];
            region.Save();
        }

        /// <summary>
        /// Save region to DB.
        /// </summary>
        protected void DeleteRegion()
        {
            Guid brokerId = GetGuid("BrokerId");
            int regionId = GetInt("RegionId");

            // Don't delete default region.
            if (regionId == Region.DefaultRegionId)
            {
                Tools.LogWarning($"[EditRegionService] Attempt to delete Default Region for brokerId=[{brokerId}]. This should not be possible!");
                return;
            }

            // Check fee service before deleting sets (editing is fine).
            FeeServiceRevision currentFeeRevision = FeeServiceApplication.GetCurrentRevision(brokerId, false);
            bool isRegionInFeeService = IsRegionInFeeService(regionId, currentFeeRevision);
            if (isRegionInFeeService)
            {
                string msg = $"[EditRegionService] Attempt to delete Region with ID=[{regionId}] used in Fee Sergion for brokerId=[{brokerId}]. This should not be possible!";
                throw new CBaseException(ErrorMessages.Generic, msg);
            }

            // Delete Region.
            Region.DeleteById(brokerId, regionId);
        }

        /// <summary>
        /// Gets an object array of states to counties to FIPS codes for use in the UI.
        /// </summary>
        protected void GetStatesAndCounties()
        {
            IEnumerable list = StateInfo.Instance.StatesCountiesAndFipsMapExcludingArmy.OrderBy(p => p.Key).Select(kvp => new
            {
                Abriv = kvp.Key,
                Counties = kvp.Value.Select(countyKvp => new { Name = countyKvp.Key, Fips = countyKvp.Value })
            });

            this.SetResult("StatesCountiesFipsMap", SerializationHelper.JsonNetAnonymousSerialize(list));
        }

        /// <summary>
        /// This method checks if a region set is being used by fee service.
        /// </summary>
        protected void CheckFeeServiceForRegionSetId()
        {
            Guid brokerId = GetGuid("BrokerId");
            int regionSetId = GetInt("RegionSetId");

            FeeServiceRevision currentFeeRevision = FeeServiceApplication.GetCurrentRevision(brokerId, false);
            SetResult("FoundInFeeService", IsRegionSetInFeeService(regionSetId, currentFeeRevision));
        }

        /// <summary>
        /// This method checks if a region set is being used by fee service.
        /// </summary>
        protected void CheckFeeServiceForRegionId()
        {
            Guid brokerId = GetGuid("BrokerId");
            int regionId = GetInt("RegionId");

            FeeServiceRevision currentFeeRevision = FeeServiceApplication.GetCurrentRevision(brokerId, false);
            SetResult("FoundInFeeService", IsRegionInFeeService(regionId, currentFeeRevision));
        }

        /// <summary>
        /// Used to check if a region set is used in the current fee service revision.
        /// </summary>
        /// <param name="regionSetId">Region Set Id to check for.</param>
        /// <param name="currentFeeRevision">Fee service revision to check in.</param>
        /// <returns>True, if the region set is used in the current fee service revision.</returns>
        private static bool IsRegionSetInFeeService(int regionSetId, FeeServiceRevision currentFeeRevision)
        {
            return currentFeeRevision.FeeTemplates
                .Any(t => t.Conditions.GetAll()
                .Any(c => c.FieldId == "csRegion" && ((FeeServiceNestedCondition)c).NestedConditions.Keys.Contains(regionSetId.ToString())));
        }

        /// <summary>
        /// Used to check if a region is used in the current fee service revision.
        /// </summary>
        /// <param name="regionId">Region Id to check for.</param>
        /// <param name="currentFeeRevision">Fee service revision to check in.</param>
        /// <returns>True, if the region is used in the current fee service revision.</returns>
        private static bool IsRegionInFeeService(int regionId, FeeServiceRevision currentFeeRevision)
        {
            return currentFeeRevision.FeeTemplates
                .Any(t => t.Conditions.GetAll()
                .Any(c => c.FieldId == "csRegion" && ((FeeServiceNestedCondition)c).NestedConditions.Values
                .Any(v => ((FeeServiceEnumeratedCondition)v).Values.Contains(regionId.ToString()))));
        }
    }
}