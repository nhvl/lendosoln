<%@ Page language="c#" Codebehind="mainhelper.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.mainhelper" %>
<%@ Import namespace="DataAccess"%>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 
<html>
  <head>
    <title>mainhelper</title>
  </head>
  <body MS_POSITIONING="FlowLayout" onbeforeunload="closeExistingLoanEdit(false);">
	<script type="text/javascript">
  var gWindowManager = null;
  var bCloseAllClick = false;
  
  var gSkipWindowManagement = false;

  // If we cannot reliable retrieve frames, then skip the windows management to avoid 
  // the running script from crashing.
  function getSkipWindowManagement() {
      return gSkipWindowManagement || retrieveFrame(parent, "frmMain") == null;
  }
  
  // Custom reporting window handle.
  var gReportingWindow = null;
  
  // New task system
  var gTaskDialogManager = null;
  
  function editLoan(id, name, url) {
        lw_url = ''
        
        if(url == null || url == '')
            lw_url = ML.VirtualRoot + '/newlos/loanapp.aspx?loanid=' + id;
        else
            lw_url = ML.VirtualRoot + '/newlos/loanapp.aspx?url=' + url + '&loanid=' + id;

        __openLoanWindow(id, lw_url, true);
  }
  function editLoanNewUI(id, name)
  {
      var lw_url = ML.VirtualRoot + '/LoanEditor/LoanEditor.aspx?loanid=' + id + "#/loanInfo";


      __openLoanWindow(id, lw_url, true);
  }
  
  function editLoanOption(id, name, urlOption, winParam)
  {
        lw_url = ''
        
        if(urlOption == null || urlOption == '')
            lw_url = ML.VirtualRoot + '/newlos/loanapp.aspx?loanid=' + id;
        else
            lw_url = ML.VirtualRoot + '/newlos/loanapp.aspx?body_url=' + urlOption + '&loanid=' + id + winParam;

        __openLoanWindow(id, lw_url, true);
  }
  function viewStyleEditor() {
      var lw_url = ML.VirtualRoot + '/newlos/Prototype/StyleEditor.aspx';
      __openLoanWindow(id, lw_url, true);
  }
  function editLead(id) {
      <% if (CurrentPrincipal.BrokerDB.IsEditLeadsInFullLoanEditor ) { %>
      __openLoanWindow(id, ML.VirtualRoot + '/newlos/loanapp.aspx?loanid=' + id, true);
      <% } else { %>
      __openLoanWindow(id, ML.VirtualRoot + '/los/lead/leadmainframe.aspx?loanid=' + id, false);
      <% } %>
  }
  function f_createLead(purpose) {
    var w = screen.availWidth - 10;
    var h = screen.availHeight - 50;
    
    var link = ML.VirtualRoot + '/los/lead/LeadCreate.aspx'
    if (purpose) { // "purchase" or "refinance"
        link += '?purpose=' + purpose;
    }
    
    var win = window.open(link, 'blank', 'width=' + w + 'px,height=' + h + 'px,left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
    win.focus();
  }
  function f_createLeadFromTemplateChoice(purpose) {
      var link = ML.VirtualRoot + '/los/LoanTemplateList.aspx?islead=t';
      if (purpose) { // "purchase" or "refinance"
          link += '&purpose=' + purpose;
      }
      var win = window.open(link, 'blank', 'width=300px,height=400px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
      win.focus();

  }

  function f_createTemplate() {
    var w = screen.availWidth - 10;
    var h = screen.availHeight - 50;
              
    var win = window.open(ML.VirtualRoot + '/newlos/LoanCreate.aspx?type=template', 'blank', 'width=' + w + 'px,height=' + h + 'px,left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
    win.focus();
  }
  function f_createLoan(purpose) {
    var link = ML.VirtualRoot + '/los/LoanTemplateList.aspx';
    if (purpose) { // "purchase" or "refinance"
        link += '?purpose=' + purpose;
    }
    var win = window.open(link, 'blank', 'width=300px,height=400px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
    win.focus();

  }

  function f_createTestLoan(purpose) {
      var link = ML.VirtualRoot + '/los/LoanTemplateList.aspx?test=1';
      if (purpose) { // "purchase" or "refinance"
          link += '&purpose=' + purpose;
      }
      var win = window.open(link, 'blank', 'width=300px,height=400px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
      win.focus();

  }

  function f_openQuickPricer() {
<%if(CurrentPrincipal.IsActuallyUsePml2AsQuickPricer)
{ %>
    var w = screen.availWidth - 10;
    var h = screen.availHeight - 100;
    var win = window.open(ML.VirtualRoot + '/newlos/Template/RunEmbeddedQuickPricer.aspx', 'blank', 'width='+w +',height='+h+',left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
<%} else{ %>
    var win = window.open(ML.VirtualRoot + '/newlos/Template/RunEmbeddedQuickPricer.aspx', 'blank', 'width=300px,height=400px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
<%} %>
    win.focus();
  }
  
  function f_openMonitoredScenarios()
  {
    <%if(CurrentPrincipal.BrokerDB.AllowRateMonitorForQP2FilesInEmdeddedPml){ %>
    var w = 300;
    var h = 200;
    var left = (screen.availWidth - w)/2;
    var top = (screen.availHeight - h)/2;
    var options = 'width='+w+'px,height='+h+'px,left='+left+'px,top='+top+'px,resizable=yes';
    window.open(ML.VirtualRoot + '/los/RateMonitor/QuickPricer2RateMonitors.aspx', 'blank', options);
    <%} %>
  }
  
  function viewLoan(id) {
	window.open(ML.VirtualRoot + '/los/view/PrintPreviewFrame.aspx?loanid=' + id + '&display=t', '_blank', 'height=550px,width=750px,toolbar=no,menubar=no,location=no,status=no,resizable=yes');
  }

  function closeExistingLoanEdit(bCancel) {
    return _beforeUnload(bCancel);
  }
  
  function logOut(bCancel) {
    if (closeExistingLoanEdit(bCancel)) {
      parent.frames["location"] = ML.VirtualRoot + '/logout.aspx';
      return true;
    } else 
      return false;
  }
  
  // -------- Multi-edit windows mode
  
  function addOption(ddl, text, value) {
    var oOption = document.createElement("OPTION");
    ddl.add(oOption);
    oOption.value = value;
    oOption.innerText = text;

  }      

  function focusWindow(sLId) {
    var name = generateWindowName(sLId);  
    var w = gWindowManager.find(name);
    if (null != w) w.focus();
  }
  function f_getWindow(sLId) {
    <%-- //be careful when using this function  --%>
    var name = generateWindowName(sLId);
    var w = gWindowManager.find(name);
    return (w==null)? w : w.handle;
  }
  function closeAllWindows(bCancel) {
    bCloseAllClick = true;
    gWindowManager.closeAll(_closeCallback, bCancel);
    bCloseAllClick = false;
    callFrameMethod(parent, "frmMain", "refreshPipeline");
  }

  function removeFromCurrentLoanList(sLId) {
      if (getSkipWindowManagement()) {
          return;
      }
      gWindowManager.close((sLId));
      callFrameMethod(parent, "frmMain", "removeFromCurrentLoanList", [sLId]);
      if (!bCloseAllClick) {
          callFrameMethod(parent, "frmMain", "refreshPipeline");
      }
  }

  function removeFromCurrentLoanListAndClose(sLId, bRefresh) {
      if (getSkipWindowManagement()) {
        return;
      }

      callFrameMethod(parent, "frmMain", "removeFromCurrentLoanList", [sLId]);
      gWindowManager.close(generateWindowName(sLId));
      if (!bCloseAllClick) {
              callFrameMethod(parent, "frmMain", "refreshPipeline", [bRefresh]);
      }
  }

  function closeWindow(sLId) {
    gWindowManager.close(generateWindowName(sLId));
  }
  function openConvertedLoan(sLId) {
    // Call with timeout to avoid window naming conflict between lead/loan windows,
    // as this is called while the lead editor is unloading
    setTimeout(function() {
      editLoan(sLId);
    }, 500);
  }    
  function _beforeUnload(bCancel) {
    return gWindowManager.closeAll(_closeCallback, bCancel);
  }    
  function _closeCallback(o, bCancel) {

    if (!o.isClosed()) {
      o.handle.focus();
      if (null != o.handle.header) {
        return o.handle.header.closeMe(bCancel); // Original o.handle.header.closeMe(false);
      }
    }
    return true;
  }
  function f_isLoanWindowOpen(sLId) {
    var name = generateWindowName(sLId);
    return (gWindowManager.find(name) != null);
  }
  function generateWindowName(sLId) {
    return <%= AspxTools.JsString(m_serverID) %> + 'LoanEdit_' + sLId.replace(/-/g, '');
  }
  // Rename this function back to editLoan
  function __openLoanWindow(sLId, url, bIsLoan) {      
    var name = generateWindowName(sLId);
    var win = gWindowManager.find(name);
    if (null == win) {
      var w = screen.availWidth - 10;
      var h = screen.availHeight - 50;
      
      win = gWindowManager.open(name, url, true, 'width=' + w + 'px,height=' + h + 'px,left=0px,top=0px,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=yes');
      
      addToWindowList('', sLId);

      var ddl = callFrameMethod(parent, "frmMain", "getOpenWindowList");
      addOption(ddl, '', sLId);
      callFrameMethod(parent, "frmMain", "disableEnableDdl", [ddl]);

      f_alertDisabledOfficialContacts(sLId);
    }
    win.focus();
  }
  function f_alertDisabledOfficialContacts(sLId)
  {
        var args = {};
        args.sLId = sLId;
        var results = gService.loanutils.call('GetRolesOfDisabledOfficialContacts', args);
        if(!results.error)
        {
            if(results.value["result"] != '')
            {
                alert('The assigned '+ results.value["result"] + ' on this file is no longer an approved agent.  Please reassign them.'); 
            }
        }
  }
  
  function f_update_sLNm_WindowToOpenList(sLId, sLNm) {
      if (getSkipWindowManagement()) {
        return;
      }

      var frmMain = retrieveFrame(parent, "frmMain");
      if (frmMain != null) {
          var ddl = frmMain.getOpenWindowList();
          for (var i = 0; i < ddl.options.length; i++) {
              var option = ddl.options[i];
              if (option.value == sLId) {
                  option.innerText = sLNm;
                  updateWindowList(sLNm, sLId);
                  break;
              }
          }
      }
  }

  function f_insertWindowToOpenList(sLId, sLNm, handle) {
      if (getSkipWindowManagement()) {
        return;
    }
    var name = generateWindowName(sLId);
    addToWindowList(sLNm, sLId);
    var ddl = callFrameMethod(parent, "frmMain", "getOpenWindowList");
    addOption(ddl, sLNm, sLId);
    
    gWindowManager.add(name, handle);
  }
  var gWindowList = new Array();
  
  function updateWindowList(name, value) {
      if (getSkipWindowManagement()) {
        return;
    }
    for (var i = 0; i < gWindowList.length; i++) {
      var o = gWindowList[i];
      if (o.value == value) { o.name = name; break; }
    }
  }
  function addToWindowList(name, value) {
      if (getSkipWindowManagement()) {
        return;
    }
    var o = new Object();
    o.name = name;
    o.value = value;
    
    gWindowList.push(o);
  }
  function removeFromWindowList(value) {
      if (getSkipWindowManagement()) {
        return;
    }
    for (var i = 0; i < gWindowList.length; i++) {
      var o = gWindowList[i];
      if (o.value == value) {
        gWindowList.splice(i, 1);
        break;
      }
    }
  }
  function appendOpenWindowList(ddl) {
      if (getSkipWindowManagement()) {
        return;
    }
    for (var i = 0; i < gWindowList.length; i++) {
      var o = gWindowList[i];
      addOption(ddl, o.name, o.value);
    }
  }
  
  // New Task System functions

  function _init() {
    gWindowManager = new WindowManager();
    gTaskDialogManager = new WindowManager();
  }
    </script>

    <form id="mainhelper" method="post" runat="server">

     </form>
	
  </body>
</html>
