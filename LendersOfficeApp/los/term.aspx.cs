using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web.UI;
using System.Xml;

using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los
{
	public partial class term : LendersOffice.Common.BasePage
	{
        protected void PageLoad(object sender, System.EventArgs e)
		{
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
            this.RegisterJsScript("LQBPrintFix.js");

			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);

		}
		#endregion

        protected void Button1_Click(object sender, System.EventArgs e)
        {
            if (m_acceptCB.Checked)
            {
                LendersOffice.Security.AbstractUserPrincipal principal = (LendersOffice.Security.AbstractUserPrincipal) Page.User;
                string xml = "";

                SqlParameter[] parameters = {
                                                new SqlParameter("@UserID", principal.UserId)
                                            };
                // Load user's agreement acceptance history
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "RetrieveAgreementAcceptanceHistoryByUserID", parameters)) 
                {
                    if (reader.Read()) 
                    {
                        xml = (string) reader["AgreementAcceptanceHistory"];
                    }
                }
				
				// Record Agreement date.
                XmlDocument doc = new XmlDocument();
                if ("" != xml)
                    doc.LoadXml(xml);

                XmlElement root = null;
                if (doc.ChildNodes.Count == 0) 
                {
                    root = doc.CreateElement("AgreementAcceptanceHistory");
                    doc.AppendChild(root);
                } 
                else 
                {
                    root = (XmlElement) doc.ChildNodes[0];
                }

                //RecordEvent(root, "Terms & Conditions", "11/20/2003"); // Initial version of Terms & Conditions agreement
                RecordEvent(root, "Terms & Conditions", "12/12/2003"); // Add privacy link


                // Save user's agreement acceptance history.
                StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "UpdateAgreementAcceptanceHistoryByUserID", 0, new SqlParameter[] {
                                                                                                                         new SqlParameter("@UserID", principal.UserId),
                                                                                                                         new SqlParameter("@NeedToAcceptLatestAgreement", false),
                                                                                                                         new SqlParameter("@AgreementAcceptanceHistory", doc.OuterXml)
                                                                                                                     });

                RequestHelper.DoNextPostLoginTask(PostLoginTask.AcceptTerms);
            }
        }
        private void RecordEvent(XmlElement elm, string documentTitle, string versionDate) 
        {
			System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;

            XmlElement history = elm.OwnerDocument.CreateElement("History");
            history.SetAttribute("DocumentTitle", documentTitle);
            history.SetAttribute("VersionDate", versionDate);
            history.SetAttribute("RecordedDate", System.DateTime.Now.ToString());
			history.SetAttribute("UserIPAddress", request.UserHostAddress );
            elm.AppendChild(history);
        }
	}
}
