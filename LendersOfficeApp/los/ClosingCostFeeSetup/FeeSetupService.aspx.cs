﻿namespace LendersOfficeApp.los.ClosingCostFeeSetup
{
    using System;

    using ConfigSystem.DataAccess;
    using DataAccess.FeeService;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes.PathDispatch;

    public partial class FeeSetupService : LendersOffice.Common.BaseSimpleServiceXmlPage
    {
        protected override bool PerformResultOptimization
        {
            get
            {
                return true;
            }
        }

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "CheckCanDeleteFee":
                    CheckCanDeleteFee();
                    break;
            }
        }

        private void CheckCanDeleteFee()
        {
            Guid typeId = GetGuid("TypeId");
            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;

            // Check Fee Service.
            FeeServiceRevision currentFeeRevision = FeeServiceApplication.GetCurrentRevision(brokerId, false);

            if (currentFeeRevision != null)
            {
                foreach (FeeServiceTemplate template in currentFeeRevision.FeeTemplates)
                {
                    foreach (FeeServiceTemplate.TemplateField field in template.GetTemplateFields())
                    {
                        if (field.RuleType == FeeServiceRuleType.ClosingCost && field.TypeId == typeId)
                        {
                            SetResult("CanDeleteFee", false);
                            SetResult("CannotDeleteMessage", "This fee is set by your fee service file. Please remove the fee from your active fee service file before removing it from the fee type setup.");
                            return;
                        }
                    }
                }
            }

            // Check Workflow
            IConfigRepository repository = ConfigHandler.GetRepository(brokerId);
            ReleaseConfigData activeRelease = repository.LoadActiveRelease(brokerId);

            if(activeRelease != null && activeRelease.Configuration != null
                && activeRelease.Configuration.HasMatchingPathParameter($"{DataPath.PathHeader}BorrowerClosingCostSet[{typeId}]"))
            {
                SetResult("CanDeleteFee", false);
                SetResult("CannotDeleteMessage", "This fee is used in your workflow configuration. Please contact support to remove the fee from your active workflow configuration before removing it from the fee type setup.");
                return;
            }

            SetResult("CanDeleteFee", true);
        }
    }
}
