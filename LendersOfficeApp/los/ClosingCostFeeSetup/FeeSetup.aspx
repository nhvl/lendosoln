﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeeSetup.aspx.cs" Inherits="LendersOfficeApp.los.ClosingCostFeeSetup.FeeSetup" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<%@ Import Namespace="DataAccess" %>

<!doctype html>
<html>
<head id="Head1" runat="server">
    <title>Closing Cost Fee Setup</title>
    <style>
    body {
        background-color: gainsboro;

    }
    #container
    {
        padding-left:10px;
        padding-bottom:20px;
    }
    table
    {
       border-collapse: collapse;
    }
    .mismo-import-mapping-popup
    {
        background-color: gainsboro;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        -ms-flex-align: center;
        align-items: center;
        height: 100%;
    }
    .mismo-import-mapping-popup .import-mapping-wrapper
    {
        width: 100%;
        overflow: auto;
        -ms-flex: 1 1 100%;
        flex: 1 1 100%;
    }
    .mismo-import-mapping-popup table.import-mapping
    {
        width: 100%;
        text-align: left;
    }
    .mismo-import-mapping-popup .exit-buttons
    {
        -ms-flex: 0 0 auto;
        flex: 0 0 auto;
    }
    .exit-buttons input
    {
        margin: 5px;
    }
    .import-mappings-link-holder
    {
        float: left;
        margin-left: 10px;
    }
    #dialog-confirm
    {
        text-align: center;
    }
    .LQBDialogBox .ui-dialog-titlebar.ui-widget-header
    {
        background-color: rgb(0, 51, 102);
    }
    tr{background-color:rgba(192, 194, 200, 1);}
    td{padding-left: 5px; padding-right: 5px;}
    th{border:none; font-weight:normal;}

    .SectionDDL
    {
        width:45px;
    }

    .InvalidField{
        border: 3px solid red;
    }
    </style>
</head>
<body>

    <form id="form1" runat="server">
    <div style="display:none;" id="dialog-confirm" title="Delete fee?">
        <p style="width: 100%"></p>
    </div>

    <div style="display:none;" id="dialog-cannotDelete" title="Cannot delete fee!">
        <p id="cannotDeleteMessage" style="width: 100%"></p>
    </div>

    <div class="FormTableHeader">Closing Cost Fee Setup</div>
    <div id='container'></div>
    <script id="ClosingCostTemplate" type="text/ractive">
        <br/>
        <div style="float: left;" >
            Use sections from the:
            <select value='{{ViewT}}' id="viewSelector" on-change="calculateData">
                <asp:Repeater runat="server" ID="ViewRepeater">
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                </asp:Repeater>
            </select>
        </div>

        <div class="import-mappings-link-holder">
            <a href="#" id="ImportMappings">Configure MISMO import mappings</a>
        </div>

        <div style="float: right;" >
            Protect compliance settings for fees:
            <select value='{{FeeTypeRequirementT}}' id="viewSelector" >
                <asp:Repeater runat="server" ID="FeeTypeRequirementT">
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                </asp:Repeater>
            </select>
        </div>

        <div style="display: none;">
            <select NotEditable="true" id="hiddenGFESecOptions"  class="SectionDDL">
                <asp:Repeater runat="server" ID="SectionRepeaterOverall">
                    <ItemTemplate>
                        <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                    </ItemTemplate>
                </asp:Repeater>
            </select>
        </div>
        <br/><br/>
        {{#SectionList:i}}
            <table style="width: 1080px;" border="1" >
                <thead>
                    <tr>
                        <td colspan="15">
                            {{SectionName}}
                        </td>
                    </tr>
                    <tr class="FormTableHeader">
                        <th>
                            HUD<br/>Line
                        </th>
                        <th>
                            Title
                        </th>
                        <th>
                            Optional?
                        </th>
                        <th>
                            Description/Memo
                        </th>

                        <th>
                            GFE<br/>Box
                        </th>
                        <th>Loan Est<br /> Sec</th>
                        <th>
                            APR
                        </th>
                        <th>
                            FHA
                        </th>
                        <th>
                            Paid to
                        </th>
                        <th>
                            DFLP
                        </th>
                        <th>
                            TP
                        </th>
                        <th>
                            AFF
                        </th>
                        <th>
                            Can<br/>Shop
                        </th>
                        <th>Mismo <br/> Type</th>
                        <th>

                        </th>
                    </tr>
                </thead>
                <tbody>
                    {{#ClosingCostFeeList:j}}
                        {{>ClosingCostFeeTemplate}}
                    {{/#ClosingCostFeeList}}
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="15">
                        {{#if SectionName[0] === 'G' || HudLineStart === 1000 }}
                            &nbsp;&nbsp;&nbsp;
                        {{else}}
                            <input type="button" value="+" on-click="addFee:{{i}}"/>
                        {{/if}}
                        </td>
                    </tr>
                </tfoot>
           </table>
           <br />
        {{/SectionList}}

        <input type="button" id="svBtn" value="Save" on-click="save" />
        <input type="button" value="Cancel" on-click="cancel" />
    </script>
    <script id='ClosingCostFeeTemplate' type='text/ractive'>
        <tr class="ReverseGridAutoItem">
            <td>
                <input type="text" class="hudline {{#if !isValidHudline(hudline, HudLineStart, HudLineEnd)}}InvalidField{{/if}}" style="width: 50px;" value="{{hudline}}" readonly="{{is_system && !editableSystem}}" on-change="hudlineChange:{{hudline}}" />
            </td>
            <td>
                <input type="checkbox" disabled="{{is_system}}" checked="{{is_title}}"/>
            </td>
            <td align="center">
                <input type="checkbox" checked="{{is_optional}}" disabled="{{disc_sect!=8}}" />
            </td>
            <td>
                <input type="text" value="{{desc}}" class="desc {{#if !isValidDesc(desc, i, j)}}InvalidField{{/if}}" style="width: 165px;" readonly="{{is_system || isTemplate}}"/>
            </td>
            <td>
                <select NotEditable="true"  class="SectionDDL" intro="sectionIntro:{ hudline:{{hudline}},isSystem:{{is_system}},gfeGroup:{{gfeGrps}} }" value="{{section}}" on-change="gfeSectionChange">
                    <asp:Repeater runat="server" ID="SectionRepeater">
                        <ItemTemplate>
                            <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                        </ItemTemplate>
                    </asp:Repeater>
                </select>
            </td>
            <td>
                <select NotEditable="true" value="{{disc_sect}}" on-change="loanEstChange:{{i}},{{j}}" disabled="{{!changeSect}}" >
                    <asp:Repeater runat="server" ID="LoanEstRepeater">
                        <ItemTemplate>
                            <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                        </ItemTemplate>
                    </asp:Repeater>
                </select>
            </td>
            <td>
                <input type="checkbox" checked="{{apr}}" />
            </td>
            <td>
            <input type="checkbox" checked="{{fha}}" />
            </td>
            <td>
                <select value="{{bene}}">
                    <asp:Repeater runat="server" ID="BeneficiaryRepeater">
                        <ItemTemplate>
                            <option value="<%# AspxTools.HtmlString(((ListItem)Container.DataItem).Value) %>"><%# AspxTools.HtmlString(((ListItem)Container.DataItem).Text) %></option>
                        </ItemTemplate>
                    </asp:Repeater>
                </select>
            </td>
            <td>
                <input type="checkbox" checked="{{dflp}}" />
            </td>
            <td>
                <input type="checkbox" checked="{{tp}}" on-click="tpClick:{{i}},{{j}}" />
            </td>
            <td>
            <input type="checkbox" checked="{{aff}}" disabled="{{!tp}}" />
            </td>
            <td>
                <input type="checkbox" checked="{{can_shop}}" disabled="{{disc_sect == 2 || disc_sect == 3}}"/>
            </td>
            <td>
                <select value="{{mismo}}" intro="mismoIntro:{isDisabled: {{!modifiableMismoType}}}">
                    <asp:Repeater runat="server" ID="MismoRepeater" OnItemDataBound="MismoRepeater_OnItemDataBound">
                        <ItemTemplate>
                            <option runat="server" id="option"/>
                        </ItemTemplate>
                    </asp:Repeater>
                </select>
            </td>
            <td>
                <input type="button" value=" - " on-click="delete:{{i}},{{j}},{{desc}}" disabled="{{is_system || isTemplate}}"/>
            </td>
        </tr>
    </script>
    <div id="saveconfirm" style="position: absolute; display:none; border: 3px solid black; background-color: Yellow; font-weight: bold; padding: 10px;">
        Your changes have been saved.
    </div>
    <div id="errorMsg" style="position: absolute; display: none; border: 3px solid black; background-color: Yellow; font-weight: bold; padding: 10px;">
    </div>
</form>
<script>
    $(document).on('change', 'input.hudline, input.desc, .SectionDDL', function(){
        $('#svBtn,#viewSelector').prop('disabled', $('.InvalidField:first').length > 0);
    });
    var helpers = Ractive.defaults.data;

    helpers.isValidHudline = function(hudline, start, end, i, j){
        var v = Number(hudline);
        return (!isNaN(v) && start <= v && v <= end);
    };

    helpers.isValidDesc = function (str, i, j) {
        // Invalid if string is null.
        if (str === null) {
            return false;
        }

        // Trim string.
        strTrim = str.replace(/^\s+|\s+$/g, '')

        // Invalid if string is empty.
        if (strTrim.length == 0) {
            return false;
        }

        // OPM 221272 - Enforce Unique Descriptions.
        var model = ractive.get();
        for (var iComp = 0; iComp < model.SectionList.length; iComp++) {
            for (var jComp = 0; jComp < model.SectionList[iComp].ClosingCostFeeList.length; jComp++) {
                // Skip self.
                if (iComp == i && jComp == j) {
                    continue;
                }

                // Get description to compare against.
                strComp = model.SectionList[iComp].ClosingCostFeeList[jComp].desc;

                // Don't bother checking against null.
                if (strComp == null) {
                    continue;
                }

                // Invalid if trimmed strings are equal.
                if (strTrim.toUpperCase() === strComp.replace(/^\s+|\s+$/g, '').toUpperCase()) {
                    return false;
                }
            }
        }

        // Passed all test. Valid.
        return true;
    }

    var mismoTransition = function(t) {
        if (t.isIntro) {
            var node = $(t.node);
            if (t.params.isDisabled) {
                //Pseudo disable it so it looks better
                node.mousedown(function(event) { event.preventDefault(); });
                node.css("background-color", "lightgrey");
            }
        }
    }

    var sectionTransition = function(t) {
        if(t.isIntro)
        {
            var ddlNode = $(t.node);
            changeGFESectionDDL(ddlNode, t.params.hudline, t.params.isSystem, t.params.gfeGroup);
        }
    }

    function changeGFESectionDDL(node, hudline, isSystem, gfeGroup)
    {
        var validGFESections = determineShownSections(hudline, isSystem, gfeGroup);
        var currentSelectedGFESec = node.find(':selected').text() || node.data("lastSelected");
        var invalidSectionAdded = false;

        if (validGFESections.indexOf(currentSelectedGFESec) === -1) {
            // The current selected GFE section will not be valid. We'll need to add it to the valid GFE sections for now.
            validGFESections.push(currentSelectedGFESec);
            invalidSectionAdded = true;
        }

        var selector = convertGFESectionArrayToContainsSelector(validGFESections);
        node.find("option").not(selector).remove();

        if (invalidSectionAdded) {
            if (currentSelectedGFESec != '') {
                node.data("lastSelected", currentSelectedGFESec || node.data("lastSelected"));
                node.find("option:contains(" + currentSelectedGFESec + ")").prop('disabled', true);
            }

            node.addClass('InvalidField');
        }
        else {
            node.data("lastSelected", "");
            node.removeClass('InvalidField');
        }

        node.find("option:contains('" + currentSelectedGFESec + "')").prop("selected", true);

        var numVisible = node.find("option").length;

        if (numVisible < 1)
        {
            node.prop('disabled', true);
            node.css('display', 'none');
        }
        else if (numVisible < 2) {
            node.prop('disabled', true);
        }
        else
        {
            node.prop('disabled', false);
        }
    }

    function determineShownSections(hudline, isSystem, gfeGroup) {
        var validGFESections = [];

        // Create the validGFESectionss for the custom fees first.
        if (!isSystem) {
            if (hudline >= 800 && hudline <= 899) {
                validGFESections = addToValidGFESecArray(validGFESections, "A1");
                validGFESections = addToValidGFESecArray(validGFESections, "B3");
                validGFESections = addToValidGFESecArray(validGFESections, "N/A");
            } else if (hudline >= 900 && hudline <= 999) {
                validGFESections = addToValidGFESecArray(validGFESections, "B3");
                validGFESections = addToValidGFESecArray(validGFESections, "B11");
                validGFESections = addToValidGFESecArray(validGFESections, "N/A");
            } else if (hudline >= 1000 && hudline <= 1099) {
                validGFESections = addToValidGFESecArray(validGFESections, "B9");
            } else if (hudline >= 1100 && hudline <= 1199) {
                validGFESections = addToValidGFESecArray(validGFESections, "B4");
                validGFESections = addToValidGFESecArray(validGFESections, "B6");
                validGFESections = addToValidGFESecArray(validGFESections, "N/A");
            } else if (hudline >= 1200 && hudline <= 1299) {
                validGFESections = addToValidGFESecArray(validGFESections, "B7");
                validGFESections = addToValidGFESecArray(validGFESections, "B8");
            } else if (hudline >= 1300 && hudline <= 1399) {
                validGFESections = addToValidGFESecArray(validGFESections, "B4");
                validGFESections = addToValidGFESecArray(validGFESections, "B6");
                validGFESections = addToValidGFESecArray(validGFESections, "N/A");
            }

            return validGFESections;
        }

        if (gfeGroup === 0) {
            validGFESections = addToValidGFESecArray(validGFESections, "N/A");
        }
        else if (gfeGroup === 1) {
            validGFESections = addToValidGFESecArray(validGFESections, "A1");
        }
        else if (gfeGroup === 2) {
            validGFESections = addToValidGFESecArray(validGFESections, "A2");
        }
        else if (gfeGroup === 3) {
            validGFESections = addToValidGFESecArray(validGFESections, "B3");
        }
        else if (gfeGroup === 4) {
            validGFESections = addToValidGFESecArray(validGFESections, "B4");
        }
        else if (gfeGroup === 5) {
            validGFESections = addToValidGFESecArray(validGFESections, "B5");
        }
        else if (gfeGroup === 6) {
            validGFESections = addToValidGFESecArray(validGFESections, "B6");
        }
        else if (gfeGroup === 7) {
            validGFESections = addToValidGFESecArray(validGFESections, "B7");
        }
        else if (gfeGroup === 8) {
            validGFESections = addToValidGFESecArray(validGFESections, "B8");
        }
        else if (gfeGroup === 9) {
            validGFESections = addToValidGFESecArray(validGFESections, "B9");
        }
        else if (gfeGroup === 10) {
            validGFESections = addToValidGFESecArray(validGFESections, "B10");
        }
        else if (gfeGroup === 11) {
            validGFESections = addToValidGFESecArray(validGFESections, "B11");
        }
        else if (gfeGroup === 12) {
            validGFESections = addToValidGFESecArray(validGFESections, "B4");
            validGFESections = addToValidGFESecArray(validGFESections, "B6");
        }
        else {
            validGFESections = addToValidGFESecArray(validGFESections, "N/A");
        }

        return validGFESections;
    }

    function addToValidGFESecArray(validGFESections, data) {
        validGFESections.push(data);
        return validGFESections;
    }

    function convertGFESectionArrayToContainsSelector(validGFESections)
    {
        var selector = "";

        var i = 0;
        for(i; i < validGFESections.length; i++)
        {
            if (validGFESections[i] != '') {

                if (selector != "") {
                    selector += ",";
                }

                selector += ":contains('" + validGFESections[i] + "')";
            }
        }

        return selector;
    }

    Ractive.transitions.sectionIntro = sectionTransition;
    Ractive.transitions.mismoIntro = mismoTransition;

    var ractive = new Ractive({
        el: 'container',
        template: '#ClosingCostTemplate',
        data: ClosingCostData,
        partials: '#ClosingCostFeeTemplate'
    });

    ractive.on('hudlineChange', function (e, oldHudline) {
        var newHudline = e.context.hudline;
        var isSystem = e.context.is_system;
        var gfeGroup = e.context.gfeGrps;

        var ddlNode = $(e.node).closest('tr').find('.SectionDDL');
        var ddlValue = ddlNode.val();
        ddlNode.find('option').remove(); // Remove all options so we can replace it.
        var newOptions = $('#hiddenGFESecOptions > option').clone();
        ddlNode.append(newOptions);
        ddlNode.val(ddlValue);

        changeGFESectionDDL(ddlNode, newHudline, isSystem, gfeGroup);
    })

    ractive.on('gfeSectionChange', function(e)
    {
        var node = $(e.node);
        var selectedOption = node.find('option:selected');

        if (!selectedOption.is(":disabled"))
        {
            node.removeClass('InvalidField');
        }
        else
        {
            node.addClass('InvalidField');
        }
    })

    ractive.on('loanEstChange', function(e, i, j) {
        //An on-change event from a select doesn't seem to update the model in time, so it's always showing the last selected value.
        //Using a setTimeout allows the model to update before using its data.
        setTimeout(function() {
            var model = ractive.get();
            if (model.SectionList[i].ClosingCostFeeList[j].disc_sect != '<%= AspxTools.HtmlString(E_IntegratedDisclosureSectionT.SectionH.ToString("D"))%>') {
                model.SectionList[i].ClosingCostFeeList[j].is_optional = false;
            }

            if (model.SectionList[i].ClosingCostFeeList[j].disc_sect == 2) // Section B
            {
                model.SectionList[i].ClosingCostFeeList[j].can_shop = false;
            }

            if (model.SectionList[i].ClosingCostFeeList[j].disc_sect == 3) // Section C
            {
                model.SectionList[i].ClosingCostFeeList[j].can_shop = true;
            }

            ractive.set(model);
        }, 0);
    });

    ractive.on('tpClick', function(e, i, j)
    {
        window.setTimeout(function () {
            var model = ractive.get();
            if (!model.SectionList[i].ClosingCostFeeList[j].tp) {
                model.SectionList[i].ClosingCostFeeList[j].aff = false;
                ractive.set(model);
            }
        });
    });

    ractive.on('addFee', function(event, i) {
        var model = ractive.get();
        var copiedTemplate = {};
        $.extend(copiedTemplate, model.SectionList[i].FeeTemplate);
        copiedTemplate.desc = '';
        event.context.ClosingCostFeeList.push(copiedTemplate);
        $('#svBtn,#viewSelector').prop('disabled', $('.InvalidField:first').length > 0);
    });

    ractive.on('delete', function (event, i, j, desc) {
        function displayCannotDeleteFee(message) {
            $('#cannotDeleteMessage').text(message)
            $('#dialog-cannotDelete').dialog({
                modal: true,
                buttons: {
                    "OK": function() {
                        $(this).dialog("close");
                    }
                },
                closeOnEscape: false,
                width: "400",
                draggable: false,
                dialogClass: "LQBDialogBox",
                resizable: false
            });
        }

        var d = desc;
        if (!d) {
            d = "No Description";
        }

        var model = ractive.get();
        var feeTypeId = model.SectionList[i].ClosingCostFeeList[j].typeid;
        var cannotDeleteMessage = pageCanDeleteFee(model, feeTypeId);
        if (!!cannotDeleteMessage){
            displayCannotDeleteFee(cannotDeleteMessage);
            return;
        }

        var args = new Object();
        args["TypeId"] = feeTypeId;
        var result = gService.main.call("CheckCanDeleteFee", args);

        if (!result.error && result.value.CanDeleteFee == "False") {
            displayCannotDeleteFee(result.value.CannotDeleteMessage);
            return;
        }
        
        var h = hypescriptDom;
        $('#dialog-confirm').find('p').empty().append(h("<>", {}, ["Are you sure you would like to remove the following fee?", h("br"), d]));
        $('#dialog-confirm').dialog({
            modal: true,
            buttons: {
                "Yes": function() {
                    $(this).dialog("close");
                    var model = ractive.get();
                    var sectionList = model.SectionList;
                    sectionList[i].ClosingCostFeeList.splice(j, 1);
                    $('#svBtn,#viewSelector').prop('disabled', $('.InvalidField:first').length > 0);
                },
                "No": function() {
                    $(this).dialog("close");
                }
            },
            closeOnEscape: false,
            width: "400",
            draggable: false,
            dialogClass: "LQBDialogBox",
            resizable: false
        });
    });

    ractive.on('calculateData', function(event) {

        //An on-change event from a select doesn't seem to update the model in time, so it's always showing the last selected value.
        //Using a setTimeout allows the model to update before using its data.
        setTimeout(function() {
            var model = event.context;
            //for some reason, the value ViewT doesn't update when the onchange event is called.
            callWebMethodAsync({
                async: false,
                type: 'POST',
                url: 'FeeSetup.aspx/CalculateData',
                data: JSON.stringify({ viewModelJson: JSON.stringify(model) }),
                contentType: 'application/json; charset=utf-8',
                dataType: "json",
                error: function(message) { },
                success: function(newData) {
                //Ractive.reset removes the data.  The data is fully removed before setting the new Data to trigger the page to rerender.
                //The page needs to re-render because the GFE Box has limited options, determined by the intro, which is only run when the
                //element first renders.  Furthermore, elements have been removed from the element that need to be added back in.
                    ractive.reset();
                    ractive.set(JSON.parse(newData.d));
                }
            });
        }, 0);
    });

    ractive.on('cancel', function () {
        onClosePopup();
    });

    ractive.on('save', function() {
        var model = ractive.get();

        callWebMethodAsync({
            async: false,
            type: 'POST',
            url: 'FeeSetup.aspx/SaveData',
            data: JSON.stringify({ viewModelJson: JSON.stringify(model) }),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            error: function(message) { alert('Error in Save: ' + responseData.Message); },
            success: function (newData) {
                var data;
                if (newData) {
                    data = JSON.parse(newData.d);
                }

                if (data && data.Status === 'Success') {
                    ractive.reset(); // see CalculateData comment
                    ractive.reset(JSON.parse(data.ViewModel));
                    $('#saveconfirm').css({ top: $(document).scrollTop() + ($(window).height() / 2) + 'px', left: '50%', margin: '0 0 0 -' + ($('#saveconfirm').width() / 2) + 'px' }).show().delay(2000).fadeOut(400);
                }
                else {
                    $('#errorMsg').text(data && data.Status == 'Error' ? "Error while saving: " + data.UserMessage : 'An unexpected error occurred');
                    $('#errorMsg').css({ top: $(document).scrollTop() + ($(window).height() / 2) + 'px', left: '50%', margin: '0 0 0 -' + ($('#saveconfirm').width() / 2) + 'px' }).show().delay(2000).fadeOut(400);
                }
            }
        });
    });
    $(function(){
        $('#svBtn,#viewSelector').prop('disabled', $('.InvalidField:first').length > 0);
    });

    function pageCanDeleteFee(model, feeTypeId) {
        function getMismo34ImportFeeTypeDescription(mismo34FeeType) {
            for (var i = 0; i < MismoImportTypeOptions.length; ++i) {
                if (MismoImportTypeOptions[i].Key == mismo34FeeType) {// one may be a string
                    return MismoImportTypeOptions[i].Value;
                }
            }
        }
        var referencedImportMappings = $.map(model.Mismo34ImportMappings, function (kvp) {
            return kvp.Value === feeTypeId ? getMismo34ImportFeeTypeDescription(kvp.Key) : undefined;
        });
        return referencedImportMappings.length ? 'This fee is referenced in MISMO import mappings for ' + referencedImportMappings.join(', ') : undefined;
    }

    function getFeeTypeListBySection() {
        return $.map(ractive.get('SectionList'), function (section) {
            return {
                'Name': section.SectionName,
                'FeeTypeList': $.map(section.ClosingCostFeeList, function (fee) {
                    return fee.typeid !== '00000000-0000-0000-0000-000000000000' ? fee : undefined;
                })
            };
        });
    }

    function getMismo34ImportMappings() {
        var directMapOfMappings = {};
        $.each(ractive.get('Mismo34ImportMappings'), function (i, kvp) {
            directMapOfMappings[kvp.Key] = kvp.Value;
        });
        return directMapOfMappings;
    }

    function updateMismo34ImportMappings(directMapOfMappings) {
        var mismo34ImportMappings = [];
        $.each(directMapOfMappings, function (key, value) {
            mismo34ImportMappings.push({ Key: key, Value: value });
        });
        ractive.set('Mismo34ImportMappings', mismo34ImportMappings);
    }


    function createMismo34ImportMappingElement(mismoTypes, availableFeeTypesBySection, existingMappings, onOk, onCancel) {
        var $okBtn = $('<input>', { 'type': 'button', 'value': 'OK' }).on('click', onOk);
        var $cancelBtn = $('<input>', { 'type': 'button', 'value': 'Cancel' }).on('click', onCancel);
        var $availableFeeTypePicker = $('<select>').append(
            $('<option>', { 'value': '' }), // blank is here to catch weird cases where the existing mapping is not available, so that it doesn't just take the first one
            $.map(availableFeeTypesBySection, function (section) {
                return $('<optgroup>', { 'label': section.Name }).append(
                    $.map(section.FeeTypeList, function (feeType) {
                        return $('<option>', { 'value': feeType.typeid }).text(feeType.desc);
                    }));
            }));
        var $table = $('<table>', { 'class': 'import-mapping' }).append(
            $.map(mismoTypes, function (mismoType) {
                return $('<tr>', { 'class': 'ReverseGridAutoItem' }).append(
                    $('<td>', { 'data-enumValue': mismoType.Key }).text(mismoType.Value),
                    $('<td>').append($availableFeeTypePicker.clone().val(existingMappings[mismoType.Key])));
            })).on('change', 'select', function (e) {
                var $target = $(e.target);
                $target.toggleClass('InvalidField', !$target.val());
                updateOkBtn();
            });
        var updateOkBtn = function () {
            var invalidFields = $('select', $table).has('option[value=""]:selected');
            $okBtn.prop('disabled', invalidFields.length !== 0);
            return invalidFields;
        };
        updateOkBtn().addClass('InvalidField');
        return $('<div>', { 'class': 'mismo-import-mapping-popup' }).append(
            $('<div>', { 'class': 'FormTableHeader' }).text('MISMO Import Mappings'),
            $('<div>', { 'class': 'import-mapping-wrapper' }).append($table),
            $('<div>', { 'class': 'exit-buttons' }).append($okBtn, $cancelBtn));
    }

    function getMismo34ImportMapping() {
        var mapping = {};
        $.each($('.mismo-import-mapping-popup table.import-mapping').find('tr'), function (i, tr) {
            var $tr = $(tr);
            var feeTypeId = $tr.find('select').val();
            if (feeTypeId) {
                mapping[$tr.children('td').first().attr('data-enumValue')] = feeTypeId;
            }
        });
        return mapping;
    }

    $('#ImportMappings').click(function (e) {
        e.preventDefault();
        var element = createMismo34ImportMappingElement(MismoImportTypeOptions, getFeeTypeListBySection(), getMismo34ImportMappings(), LQBPopup.Return, LQBPopup.Hide);
        LQBPopup.ShowElement(element, { 'width': 600, 'height': 500, 'isCopyHTML': false, 'onReturn': function () { updateMismo34ImportMappings(getMismo34ImportMapping()); } }, {});
    });
</script>
</body>
</html>
