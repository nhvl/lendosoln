﻿namespace LendersOfficeApp.los.ClosingCostFeeSetup
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Web.Services;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Extensions;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;

    [DataContract]
    public class LenderClosingCostFeeViewModel
    {
        [DataMember]
        public E_ClosingCostViewT ViewT { get; set; }

        [DataMember]
        public E_FeeTypeRequirementT FeeTypeRequirementT { get; set; }

        [DataMember]
        public IEnumerable<FeeSetupClosingCostFeeSection> SectionList
        {
            get
            {
                return this.ClosingCostSet.GetViewForSerialization(this.ViewT);
            }

            set
            {
                this.ClosingCostSet = PrincipalFactory.CurrentPrincipal.BrokerDB.GetUnlinkedClosingCostSet();
                this.ClosingCostSet.UpdateWith((IEnumerable<BaseClosingCostFeeSection>)value);
            }
        }

        /// <remarks>
        /// While this does live on the <see cref="FeeSetupClosingCostSet"/>, coordinating the value setting with <see cref="SectionList"/> makes this not worth the effort.
        /// </remarks>
        [DataMember]
        public Dictionary<Mismo3Specification.Version4Schema.FeeBase, Guid> Mismo34ImportMappings { get; set; }

        public FeeSetupClosingCostSet ClosingCostSet { get; set; }
    }

    public partial class FeeSetup : BaseServicePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            this.RegisterService("main", "/los/ClosingCostFeeSetup/FeeSetupService.aspx");
            RegisterJsScript("jquery-ui-1.11.4.min.js");
            RegisterCSS("jquery-ui-1.11.custom.css");
            RegisterJsScript("ThirdParty/ractive-legacy-0.6.1.min.js");
            BindViewDropDown();
            BindFeeTypeRequirementTDropDown();
            //BindGfeSectionDropDown(this.section);
            //RolodexDB.PopulateAgentTypeDropDownList(this.bene);
            //BindLoanEstimateSectionDropDown(this.disc_sect);

            BindBeneficiaryDropDown(BeneficiaryRepeater);
            BindGfeSectionDropDown(SectionRepeater);
            BindGfeSectionDropDown(SectionRepeaterOverall);
            BindLoanEstimateSectionDropDown(LoanEstRepeater);

            BindMismoTDDL();
        }

        private void BindMismoTDDL()
        {
            IEnumerable<ListItem> mismoListItems = EnumHelper.GetValues<E_ClosingCostFeeMismoFeeT>()
                .Select(m => Tools.CreateEnumListItem(Tools.GetFriendlyMismoFeeType(m), m))
                .OrderBy(t => t.Text);
            MismoRepeater.DataSource = mismoListItems;
            MismoRepeater.DataBind();
        }

        protected void MismoRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                ListItem item = (ListItem)args.Item.DataItem;
                HtmlContainerControl c = (HtmlContainerControl)args.Item.FindControl("option");
                c.Attributes.Add("value", item.Value);
                c.InnerText = item.Text;
                c.ID = null;
            }
        }

        protected void RenderOptionControl(System.Web.UI.HtmlTextWriter writer,
                            System.Web.UI.Control Container)
        {
            Container.ID = null;
        }

        private void BindGfeSectionDropDown(Repeater repeater)
        {
            List<ListItem> items = new List<ListItem>();
            items.Add(Tools.CreateEnumListItem("A1", E_GfeSectionT.B1));
            items.Add(Tools.CreateEnumListItem("A2", E_GfeSectionT.B2));
            items.Add(Tools.CreateEnumListItem("B3", E_GfeSectionT.B3));
            items.Add(Tools.CreateEnumListItem("B4", E_GfeSectionT.B4));
            items.Add(Tools.CreateEnumListItem("B5", E_GfeSectionT.B5));
            items.Add(Tools.CreateEnumListItem("B6", E_GfeSectionT.B6));
            items.Add(Tools.CreateEnumListItem("B7", E_GfeSectionT.B7));
            items.Add(Tools.CreateEnumListItem("B8", E_GfeSectionT.B8));
            items.Add(Tools.CreateEnumListItem("B9", E_GfeSectionT.B9));
            items.Add(Tools.CreateEnumListItem("B10", E_GfeSectionT.B10));
            items.Add(Tools.CreateEnumListItem("B11", E_GfeSectionT.B11));
            items.Add(Tools.CreateEnumListItem("N/A", E_GfeSectionT.NotApplicable));
            repeater.DataSource = items;
            repeater.DataBind();
        }

        private void BindLoanEstimateSectionDropDown(Repeater repeater)
        {
            List<ListItem> items = new List<ListItem>();
            items.Add(Tools.CreateEnumListItem("A", E_IntegratedDisclosureSectionT.SectionA));
            items.Add(Tools.CreateEnumListItem("B", E_IntegratedDisclosureSectionT.SectionB));
            items.Add(Tools.CreateEnumListItem("C", E_IntegratedDisclosureSectionT.SectionC));
            items.Add(Tools.CreateEnumListItem("E", E_IntegratedDisclosureSectionT.SectionE));
            items.Add(Tools.CreateEnumListItem("F", E_IntegratedDisclosureSectionT.SectionF));
            items.Add(Tools.CreateEnumListItem("G", E_IntegratedDisclosureSectionT.SectionG));
            items.Add(Tools.CreateEnumListItem("H", E_IntegratedDisclosureSectionT.SectionH));
            LoanEstRepeater.DataSource = items;
            LoanEstRepeater.DataBind();
        }

        private void BindBeneficiaryDropDown(Repeater repeater)
        {
            DropDownList dl = new DropDownList();
            RolodexDB.PopulateAgentTypeDropDownList(dl);
            repeater.DataSource = dl.Items;
            repeater.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Headers.Add("X-UA-Compatible", "IE=10; IE=9; IE=8");

            LenderClosingCostFeeViewModel viewModel = CreateViewModel(PrincipalFactory.CurrentPrincipal.BrokerDB);
            RegisterJsObject("ClosingCostData", viewModel);
            IEnumerable<Mismo3Specification.Version4Schema.FeeBase> defaultMappings = DefaultSystemClosingCostFee.FeeTemplate.DefaultMismoImportFeeTypeMappings.Keys;
            this.RegisterJsObject("MismoImportTypeOptions", defaultMappings.Select(f => Tuple.Create(Tools.GetFriendlyMismoFeeType(f), f)).OrderBy(t => t.Item1).ToDictionary(t => t.Item2, t => t.Item1));
        }

        private void BindViewDropDown()
        {
            List<ListItem> items = new List<ListItem>();
            items.Add(new ListItem("HUD-1", E_ClosingCostViewT.LenderTypeHud1.ToString("D")));
            items.Add(new ListItem("Loan Estimate", E_ClosingCostViewT.LenderTypeEstimate.ToString("D")));

            ViewRepeater.DataSource = items;
            ViewRepeater.DataBind();
        }

        private void BindFeeTypeRequirementTDropDown()
        {
            List<ListItem> items = new List<ListItem>();
            items.Add(new ListItem("For all new loans", E_FeeTypeRequirementT.ForAllNewLoans.ToString("D")));
            items.Add(new ListItem("If set in template", E_FeeTypeRequirementT.IfSetInTemplate.ToString("D")));
            items.Add(new ListItem("Never", E_FeeTypeRequirementT.Never.ToString("D")));

            FeeTypeRequirementT.DataSource = items;
            FeeTypeRequirementT.DataBind();
        }

        [WebMethod]
        public static string RefreshView()
        {
            LenderClosingCostFeeViewModel viewModel = CreateViewModel(PrincipalFactory.CurrentPrincipal.BrokerDB);
            return ObsoleteSerializationHelper.JsonSerialize(viewModel);
        }

        [WebMethod]
        public static string CalculateData(string viewModelJson)
        {
            LenderClosingCostFeeViewModel viewModel = ObsoleteSerializationHelper.JsonDeserialize<LenderClosingCostFeeViewModel>(viewModelJson);
            return ObsoleteSerializationHelper.JsonSerialize(viewModel);
        }

        [WebMethod]
        public static string SaveData(string viewModelJson)
        {
            try
            {
                LenderClosingCostFeeViewModel viewModel = ObsoleteSerializationHelper.JsonDeserialize<LenderClosingCostFeeViewModel>(viewModelJson);
                BrokerDB brokerDB = PrincipalFactory.CurrentPrincipal.BrokerDB;

                FeeSetupClosingCostSet closingCostSet = brokerDB.GetUnlinkedClosingCostSet();
                closingCostSet.UpdateWith(viewModel.ClosingCostSet, null);
                closingCostSet.SetMismoImportMappings(viewModel.Mismo34ImportMappings);
                brokerDB.SetClosingCostSet(closingCostSet, true);
                brokerDB.FeeTypeRequirementT = viewModel.FeeTypeRequirementT;

                brokerDB.Save();

                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Success",
                    ViewModel = ObsoleteSerializationHelper.JsonSerialize(CreateViewModel(brokerDB)), // double serialization, but consistency of output is of primal importance
                });
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                return SerializationHelper.JsonNetAnonymousSerialize(new
                {
                    Status = "Error",
                    UserMessage = e.UserMessage,
                    ErrorType = e.GetType().Name
                });
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw new CBaseException(ErrorMessages.Generic, e);
            }
        }

        private static LenderClosingCostFeeViewModel CreateViewModel(BrokerDB brokerDB)
        {
            FeeSetupClosingCostSet closingCostSet = brokerDB.GetUnlinkedClosingCostSet();
            return new LenderClosingCostFeeViewModel
            {
                ViewT = E_ClosingCostViewT.LenderTypeEstimate,
                FeeTypeRequirementT = brokerDB.FeeTypeRequirementT,
                ClosingCostSet = closingCostSet,
                Mismo34ImportMappings = closingCostSet.Mismo34ImportMappings?.ToDictionary(kvp => kvp.Key, kvp => kvp.Value),
            };
        }
    }
}
