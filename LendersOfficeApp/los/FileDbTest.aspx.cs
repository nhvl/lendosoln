﻿namespace LendersOfficeApp
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using Adapter;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// This is a test page for FileDB.Get and FileDB.Put operation.
    /// </summary>
    public partial class FileDbTest : MinimalPage
    {
        /// <summary>
        /// Simple encryption key for testing.
        /// </summary>
        private static readonly byte[] EncryptionKey = new byte[] 
        {
            0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
            0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
            0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40,
            0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50,
        };

        /// <summary>
        /// Load page.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // 6/12/2017 - This page will use in LoadTest scenario to measure the FileDB.Get and FileDB.Put operation.
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            string fileName = principal.UserId + "_LOAD_TEST_XMYKAD";
            string encryptedFileName = principal.UserId + "_LOAD_TEST_ENCRYPTED_XDME";
            string randomEncryptedKeyFileName = principal.UserId + "_LOAD_TEST_RANDOM_ENCKEY";
            string op = Request.QueryString["op"];

            int numberOfBlocks = 0;

            if (!int.TryParse(Request.QueryString["blocks"], out numberOfBlocks))
            {
                numberOfBlocks = 0;
            }

            if (op == "GET")
            {
                this.TestGetFileDB($"{fileName}_{numberOfBlocks}");
            }
            else if (op == "PUT")
            {
                this.TestPutFileDB($"{fileName}_{numberOfBlocks}", numberOfBlocks);
            }
            else if (op == "PUT_ENCRYPT")
            {
                this.TestPutFileDBEncrypt($"{encryptedFileName}_{numberOfBlocks}", numberOfBlocks);
            }
            else if (op == "GET_ENCRYPT")
            {
                this.TestGetFileDBEncrypt($"{encryptedFileName}_{numberOfBlocks}");
            }
            else if (op == "PUT_RANDOM_ENC")
            {
                this.TestPutFileDBEncryptRandomKey($"{randomEncryptedKeyFileName}_{numberOfBlocks}", numberOfBlocks);
            }
        }

        /// <summary>
        /// Read a test file from FileDB.
        /// </summary>
        /// <param name="fileName">File name.</param>
        private void TestGetFileDB(string fileName)
        {
            Action<FileInfo> handler = delegate(FileInfo fi)
            {
                // NO-OP
            };

            try
            {
                FileDBTools.UseFile(E_FileDB.Temp, fileName, handler);
            }
            catch (FileNotFoundException)
            {
                // NO-OP
            }
        }

        /// <summary>
        /// Put test content to FileDB.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <param name="numberOfBlocks">File size in number of block. Each block is 8192 bytes.</param>
        private void TestPutFileDB(string fileName, int numberOfBlocks)
        {
            if (!this.IsValidNumberOfBlock(numberOfBlocks))
            {
                return; // NO-OP
            }

            Action<Stream> writeHandler = delegate(Stream stream)
            {
                this.CreateDummyData(stream, numberOfBlocks);
            };

            FileDBTools.WriteData(E_FileDB.Temp, fileName, writeHandler);
        }

        /// <summary>
        /// Verify the number of blocks are valid. To avoid abuse we only support a set of valid values.
        /// </summary>
        /// <param name="numberOfBlocks">Number of block to verify.</param>
        /// <returns>Whether the value is valid.</returns>
        private bool IsValidNumberOfBlock(int numberOfBlocks)
        {
            // Only support these values.
            if (numberOfBlocks == 0 ||
                numberOfBlocks == 1 || numberOfBlocks == 5 || numberOfBlocks == 10 || 
                numberOfBlocks == 25 || numberOfBlocks == 50 || numberOfBlocks == 70)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Put test file with encryption to FileDB.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <param name="numberOfBlocks">File size in number of block. Each block is 8192 bytes.</param>
        private void TestPutFileDBEncrypt(string fileName, int numberOfBlocks)
        {
            if (!this.IsValidNumberOfBlock(numberOfBlocks))
            {
                return; // NO-OP
            }

            var factory = new FileDbAdapterFactory();
            var fileDbAdapter = factory.Create();
            var fileStorageIdentifier = FileStorageIdentifier.TryParse(ConstStage.FileDBSiteCodeForTempFile);
            var fileIdentifier = FileIdentifier.TryParse(fileName);

            Action<LocalFilePath> saveFileHandler = delegate(LocalFilePath fileHandle)
            {
                using (FileStream stream = File.OpenWrite(fileHandle.Value))
                {
                    this.CreateDummyData(stream, numberOfBlocks);
                }
            };

            fileDbAdapter.SaveNewFile(fileStorageIdentifier.Value, fileIdentifier.Value, EncryptionKey, saveFileHandler);
        }

        /// <summary>
        /// Put test file with encryption to FileDB with random key.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <param name="numberOfBlocks">File size in number of block. Each block is 8192 bytes.</param>
        private void TestPutFileDBEncryptRandomKey(string fileName, int numberOfBlocks)
        {
            if (!this.IsValidNumberOfBlock(numberOfBlocks))
            {
                return; // NO-OP
            }

            var factory = new FileDbAdapterFactory();
            var fileDbAdapter = factory.Create();
            var fileStorageIdentifier = FileStorageIdentifier.TryParse(ConstStage.FileDBSiteCodeForTempFile);
            var fileIdentifier = FileIdentifier.TryParse(fileName);

            Action<LocalFilePath> saveFileHandler = delegate(LocalFilePath fileHandle)
            {
                using (FileStream stream = File.OpenWrite(fileHandle.Value))
                {
                    this.CreateDummyData(stream, numberOfBlocks);
                }
            };

            using (var randomProvider = new RNGCryptoServiceProvider())
            {
                byte[] randomKey = new byte[32];
                randomProvider.GetBytes(randomKey);

                fileDbAdapter.SaveNewFile(fileStorageIdentifier.Value, fileIdentifier.Value, randomKey, saveFileHandler);
            }
        }

        /// <summary>
        /// Get test file with decryption to FileDB.
        /// </summary>
        /// <param name="fileName">File name.</param>
        private void TestGetFileDBEncrypt(string fileName)
        {
            var factory = new FileDbAdapterFactory();
            var fileDbAdapter = factory.Create();
            var fileStorageIdentifier = FileStorageIdentifier.TryParse(ConstStage.FileDBSiteCodeForTempFile);
            var fileIdentifier = FileIdentifier.TryParse(fileName);

            Action<LocalFilePath> readFileHandler = delegate(LocalFilePath fileHandle)
            {
                // NO-OP
            };

            try
            {
                fileDbAdapter.RetrieveFile(fileStorageIdentifier.Value, fileIdentifier.Value, EncryptionKey, readFileHandler);
            }
            catch (FileNotFoundException)
            {
                // NO-OP
            }
        }

        /// <summary>
        /// Create a random content file with predefine size.
        /// </summary>
        /// <param name="stream">Stream to write data to.</param>
        /// <param name="numberOfBlocks">File size in number of block. Each block is 8192 bytes.</param>
        private void CreateDummyData(Stream stream, int numberOfBlocks)
        {
            if (numberOfBlocks < 0 || numberOfBlocks > 100)
            {
                throw new ArgumentOutOfRangeException("numberOfBlocks exceed 100 or less than 0.");
            }

            const int BlockSize = 1024 * 8;

            byte[] data = new byte[BlockSize];
            Random rng = new Random();

            if (numberOfBlocks == 0)
            {
                // This is a special case. Write 999 bytes.
                data = new byte[999];
                rng.NextBytes(data);
                stream.Write(data, 0, data.Length);
            }
            else
            {
                for (int i = 0; i < numberOfBlocks; i++)
                {
                    rng.NextBytes(data);
                    stream.Write(data, 0, data.Length);
                }
            }
        }
    }
}