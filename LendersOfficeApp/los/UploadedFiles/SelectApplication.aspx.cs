﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOffice.Reports;
using LendersOfficeApp.WebService;
using LendersOffice.QueryProcessor;
using DataAccess;
using System.Collections;
using System.Web.Services;
using LendersOffice.LoanSearch;
using System.Data;
using LendersOffice.AntiXss;

namespace LendersOfficeApp.los.UploadedFiles
{
    public partial class SelectApplication : BasePage
    {
        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;
            RegisterJsScript("jquery.tablesorter.min.js");
            base.OnInit(e);
        }

        protected BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Guid reportId = (BrokerDB.RetrieveById(BrokerUser.BrokerId)).HasLenderDefaultFeatures ? 
                            LoanReporting.DefaultSimplePipelineReportId : LoanReporting.DefaultSimplePipelineNonPMLReportId;
            LoanReporting lr = new LoanReporting();
            lr.MaxViewableRowCount = LendersOffice.Constants.ConstAppDavid.MaxViewableRecordsInPipeline;
            lr.MaxSqlRowCount = LendersOffice.Constants.ConstStage.MaxSqlRecordsForPipeline;

            string sortOrder = "sLNm:asc";
            var sortingCookie = Request.Cookies["sortOrder"];
            if (sortingCookie != null)
            {
                sortOrder = sortingCookie.Value;
            }

            Report rep = lr.Pipeline(reportId, BrokerUser, sortOrder, E_ReportExtentScopeT.Assign);

            LoanFiles.DataSource = FlattenReport(rep);
            LoanFiles.DataBind();
        }

        public class LoanInfo
        {
            public Guid LoanId;
            public string LoanNumber;
            public string BorrLastName;
            public string BorrFirstName;
            public string LoanOfficer;
            public string LoanStatus;

            public LoanInfo(Row data)
            {
                LoanId = data.Key;
                LoanNumber = data.Items[0].Text;
                BorrLastName = data.Items[1].Text;
                BorrFirstName = data.Items[2].Text;
                LoanOfficer = data.Items[3].Text;
                LoanStatus = data.Items[4].Text;
            }

            public LoanInfo(DataRow dr)
            {
                LoanId = new Guid(dr["sLId"].ToString());
                LoanNumber = dr["sLNm"].ToString();
                BorrLastName = dr["aBLastNm"].ToString();
                BorrFirstName = dr["aBFirstNm"].ToString();
                LoanOfficer = dr["sEmployeeLoanRepName"].ToString();
                LoanStatus = StatusValueStringToFriendlyName(dr["sStatusT"].ToString());
            }

            private string StatusValueStringToFriendlyName(string statusVal)
            {
                int status;
                if (int.TryParse(statusVal, out status))
                {
                    return CPageBase.sStatusT_map_rep((E_sStatusT)status);
                }

                //If it's not an integer, just return it.
                return statusVal;
            }

            //For serialization
            public LoanInfo() { }
        }


        protected List<LoanInfo> FlattenReport(Report rR)
        {
            var ret = rR.Flatten().Select(a => new LoanInfo(a));
            return ret.ToList();
        }

        [WebMethod]
        public static object GetAppsInLoan(Guid LoanId)
        {
            CPageData data = new CPageData(LoanId, new[] { "aBNm" });
            data.InitLoad();
            
            var apps = new List<CAppData>();
            for (int i = 0; i < data.nApps; i++)
            {
                apps.Add(data.GetAppData(i));
            }

            var thing = from app in apps
                        select new
                        {
                            Name = app.aBNm,
                            Id = app.aAppId
                        };

            return thing;
        }

        [WebMethod]
        public static object LoanSearch(string query, int start, int end)
        {

            if (start <= 1) start = 1;

            int numResults = end - start;
            if (numResults > 200) numResults = 200;
            if (numResults < 1) numResults = 1;

            end = start + numResults;

            int numberOfLoans;
            List<LoanInfo> result = SearchByLoanAndLastName(query, start, end, out numberOfLoans);

            if (numberOfLoans == 0)
            {
                query = query.Trim('*') + "*";
                result = SearchByLoanAndLastName(query, start, end, out numberOfLoans);
            }

            return new
            {
                Loans = result.ToList(),
                NumTotalResults = numberOfLoans
            };
        }

        public static List<LoanInfo> SearchByLoanAndLastName(string query, int start, int end, out int totalLoans)
        {
            LendingQBSearchFilter searchFilter = new LendingQBSearchFilter();

            totalLoans = 0;
            int numberOfLoans;
            searchFilter.sStatusT = -1;
            searchFilter.IncludeLeadAndLoanStatuses = true;
            searchFilter.sLT = -1;

            searchFilter.sLNm = query;
            var result = DoSearch(searchFilter, start, end, out numberOfLoans);
            totalLoans = numberOfLoans;

            searchFilter.sLNm = null;
            searchFilter.aBLastNm = query;
            result.AddRange(DoSearch(searchFilter, start, end, out numberOfLoans));
            totalLoans += numberOfLoans;

            return result.OrderBy(a => a.LoanNumber).ToList();
        }

        public static List<LoanInfo> DoSearch(LendingQBSearchFilter filter, int start, int end, out int numberOfLoans)
        {
            var user = BrokerUserPrincipal.CurrentPrincipal;
            DataSet dS = LendingQBSearch.Search(user, filter, "sLNm ASC", start, end, out numberOfLoans);
            return (from DataRow row in dS.Tables[0].Rows select new LoanInfo(row)).ToList();
        }
    }
}