﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectApplication.aspx.cs"
    Inherits="LendersOfficeApp.los.UploadedFiles.SelectApplication" %>

<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        jQuery(function($) {
            var args = getModalArgs();
            if (!args || typeof (args.callback) == 'undefined') onClosePopup();
            var currentLoan = {
                Id: null,
                Name: null
            };
            $.tablesorter.defaults.widgetZebra = {
                css: ["GridItem", "GridAlternatingItem"]
            };
            var sorting = [[0, 0]];

            if ($('#LoanTable tbody tr').length == 0) {
                $('#LoanTable tbody').append('<tr><td colspan="5">No loans in your pipeline. Please search for a loan by loan number.</td>' +
                                             '<td class="FakeTd"></td><td class="FakeTd"></td><td class="FakeTd"></td><td class="FakeTd"></td></tr>');
            }

            $('#LoanTable').tablesorter(
            {
                sortList: sorting,
                widgets: ['zebra']
            });

            $('#ApplicationList').on('click', 'li.Application', function() {
                var appGuid = this.id.substring("guid_".length);
                var appName = $(this).text();

                var ret = {
                    Loan: currentLoan,
                    Application: {
                        Id: appGuid,
                        Name: appName
                    }
                };
                var args = getModalArgs();
                if (typeof (args.callback) != 'undefined') args.callback(ret);
                else alert("Could not set application, please try again.");

                onClosePopup();
            });

            var unknownPrefix = "No name - ";
            var unknownNames = { 'true': "Primary", 'false': "Secondary" };
            var applicationsLoading = false;
            $('#LoanTableBody').on('click', '.LoanSelected', function() {
                if (applicationsLoading) return;

                applicationsLoading = true;
                var loanGuid = $(this).parents('tr')[0].id.substring("guid_".length);
                var loanName = $(this).text();
                var DTO = { 'LoanId': loanGuid };

                $('#ApplicationList').empty();
                SwapState("PickApplication");
                var pleaseWaitTimeout = window.setTimeout(function() {
                    $('#ApplicationList').append("<li>Please wait...</li>");
                }, 200);


                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: 'SelectApplication.aspx/GetAppsInLoan',
                    data: JSON.stringify(DTO),
                    dataType: 'json',
                    async: true,
                    error: function(XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); },
                    success: function(msg) {
                        window.clearTimeout(pleaseWaitTimeout);
                        
                        var apps = msg.d;
                        var h = hypescriptDom;
                        $('#ApplicationList').empty().append($.map(apps, function(currApp, i){
                            var name = currApp.Name;
                            //Regex is true if there's at least one non-whitespace, 
                            //so if it's false we've got an empty string
                            if (!/\S/.test(name)) {
                                name = unknownPrefix + unknownNames[i == 0];
                            }
                            
                            return h("li", {className:"Application Action", id:("guid_"+currApp.Id)}, name);
                        }));
                        $('#ApplicationList li:even').addClass('GridItem');
                        $('#ApplicationList li:odd').addClass('GridAlternatingItem');



                        currentLoan.Id = loanGuid;
                        currentLoan.Name = loanName;
                    },
                    complete: function() { applicationsLoading = false; }
                });
            });

            $('#PickLoan').click(function() {
                SwapState("PickLoan");
                currentLoan.Id = null;
                currentLoan.Name = null;
            });

            $('#SearchValue').keypress(function(e) {
                if (e.which == 13) {
                    $('#SearchButton').click();
                    return false;
                }
            });

            var currentQuery = null;
            var currentIndex = 1;
            $('#SearchButton').click(function() {
                SwapState("PickLoan");

                currentQuery = $('#SearchValue').val();
                currentIndex = 1;

                doSearch(currentQuery, currentIndex);
            });

            $('#Prev').click(function() {
                currentIndex--;
                doSearch(currentQuery, currentIndex);
            });

            $('#Next').click(function() {
                currentIndex++;
                doSearch(currentQuery, currentIndex);
            });

            var searchWindowSize = 40;
            function doSearch(query, index) {
                if (!query) return;

                var loadingMessageTimeout = window.setTimeout(function() {
                    $('#LoanTable tbody').empty().append('<tr><td colspan="5">Loading...</td></tr>');
                }, 500);

                var button = $('#SearchButton');
                button.prop('disabled', true);

                //The first result row is row 1
                var start = 1 + (index - 1) * searchWindowSize;
                var end = index * searchWindowSize;

                var DTO =
                {
                    'query': query,
                    'start': start,
                    'end': end
                };
                var lastXHR = $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: 'SelectApplication.aspx/LoanSearch',
                    data: JSON.stringify(DTO),
                    dataType: 'json',
                    async: true,
                    error: function(XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); },
                    success: function(msg, status, xhr) {
                        //Only want to accept the most recent request
                        if (lastXHR != xhr) return;
                        window.clearTimeout(loadingMessageTimeout);
                        button.prop('disabled', false);
                        button.css('background-color', '');

                        if (msg.d.NumTotalResults == 0) {
                            alert("No loan file with that name was found");
                            return;
                        }

                        var n = [];
                        $.each(msg.d.Loans, function() {
                            n.push('<tr id="guid_');
                            n.push(this.LoanId);
                            n.push('"><td><a class="LoanSelected Action">');
                            n.push(this.LoanNumber);
                            n.push('</td><td>');
                            n.push(this.BorrLastName);
                            n.push('</td><td>');
                            n.push(this.BorrFirstName);
                            n.push('</td><td>');
                            n.push(this.LoanOfficer);
                            n.push('</td><td>');
                            n.push(this.LoanStatus);
                            n.push('</td></tr>');
                        });


                        $('#LoanTable tbody').empty().append(n.join(''));
                        $('#LoanTable').trigger('update').trigger('appendCache').trigger("sorton", [$('#LoanTable')[0].config.sortList]);

                        if ((index * searchWindowSize) >= msg.d.NumTotalResults) {
                            $('#Next').addClass("Hidden");
                        }
                        else {
                            $('#Next').removeClass("Hidden");
                        }

                        $('#Prev').toggleClass("Hidden", index == 1);
                        if ($('#PaginationControls span.Hidden').length == $('#PaginationControls span').length) {
                            $("#PaginationControls").hide();
                        }
                        else {
                            $('#PaginationControls').show();
                        }

                        if (msg.d.Loans.length % 2 == 1) {
                            $('#PaginationControls').removeClass('GridItem').addClass('GridAlternatingItem');
                        }
                        else {
                            $('#PaginationControls').removeClass('GridAlternatingItem').addClass('GridItem');
                        }
                    }
                });
            }

            function SwapState(newState) {
                if (newState == "PickLoan") {
                    $('#LoanTable').show();
                    $('#CurrentState div.ChooseLoanStatus').show();
                    $('#Applications').hide();
                    $('#CurrentState div.ChooseApp').hide();
                }
                else if (newState == "PickApplication") {
                    $('#Applications').show();
                    $('#CurrentState div.ChooseApp').show();
                    $('#LoanTable').hide();
                    $('#CurrentState div.ChooseLoanStatus').hide();
                }
            }
        });
    
    
    </script>

    <style type="text/css">
        .Action
        {
            color: Blue;
            cursor: pointer;
            text-decoration: underline;
        }
        #LoanTable
        {
            border-spacing: 1px;
            border-collapse: separate;
            width: 100%;
            background-color: #ece9d8;
        }
        #Applications
        {
            display: none;
            overflow: hidden;
        }
        #ApplicationList
        {
            padding-left: 0px;
            margin-top: 0px;
            list-style-type: none;
        }
        #ApplicationList li.Application
        {
            color: Blue;
            padding: 1px;
            margin-bottom: 1px;
        }
        #AppHeader
        {
            padding: 1px;
        }
        #PaginationControls
        {
            display: none;
        }
        div.ChooseApp
        {
            display: none;
        }
        .FakeTd
        {
            display:none;
        }
        .Hidden
        {
            visibility: hidden;
        }
    </style>
</head>
<body bgcolor="gainsboro">
    <h4 class="page-header">Select an Application</h4>
    <form id="form1" runat="server">
    <div>
        <div id="SearchArea">
            <span id="SearchLabel">Search for: </span>
            <input id="SearchValue" type="text" />
            <input type="button" value="Search" id="SearchButton" /></div>
        <div id="CurrentState">
            <div class="ChooseLoanStatus">
                Choose a File:</div>
            <div class="ChooseApp">
                <span id="PickLoan" class="Action">Loan File</span> > Application</div>
        </div>
        <table id="LoanTable">
            <thead class="GridHeader">
                <tr>
                    <th class="Action">
                        Loan Number
                    </th>
                    <th class="Action">
                        Borr Last Name
                    </th>
                    <th class="Action">
                        Borr First Name
                    </th>
                    <th class="Action">
                        Loan Officer
                    </th>
                    <th class="Action">
                        Loan Status
                    </th>
                </tr>
            </thead>
            <tbody id="LoanTableBody">
                <asp:Repeater runat="server" ID="LoanFiles">
                    <ItemTemplate>
                        <tr id="guid_<%#AspxTools.HtmlString(((LoanInfo)Container.DataItem).LoanId.ToString()) %>">
                            <td>
                                <a class="LoanSelected Action">
                                    <%#AspxTools.HtmlString(((LoanInfo)Container.DataItem).LoanNumber) %></a>
                            </td>
                            <td>
                                <%#AspxTools.HtmlString(((LoanInfo)Container.DataItem).BorrLastName) %>
                            </td>
                            <td>
                                <%#AspxTools.HtmlString(((LoanInfo)Container.DataItem).BorrFirstName) %>
                            </td>
                            <td>
                                <%#AspxTools.HtmlString(((LoanInfo)Container.DataItem).LoanOfficer) %>
                            </td>
                            <td>
                                <%#AspxTools.HtmlString(((LoanInfo)Container.DataItem).LoanStatus) %>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
            <tfoot>
                <tr id="PaginationControls" class="GridItem">
                    <td>
                        <span id="Prev" class="Action Hidden">Prev</span> <span id="Next" class="Action Hidden">Next</span>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
        <div id="Applications">
            <div id="AppHeader" class="GridHeader">
                Applications</div>
            <ul id="ApplicationList">
            </ul>
        </div>
    </div>
    </form>
</body>
</html>
