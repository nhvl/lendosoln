﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FileList.aspx.cs" Inherits="LendersOfficeApp.los.UploadedFiles.FileList" %>

<%@ Register TagPrefix="uc" TagName="DocTypePicker" Src="~/newlos/ElectronicDocs/DocTypePickerControl.ascx" %>
<%@ Import Namespace="LendersOffice.ObjLib.UserDropbox" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        jQuery(function ($) {
            var dragDropSettings = {
                checkForUnprocessedUploadsBeforeUnload: true
            };

            registerDragDropUpload(document.getElementById("DragDropZone"), dragDropSettings);
          
            var NumFiles = 0;
            UpdateNumFiles();

            //Alert errors if any
            if ($('#Error').length) {
                alert($('#Error').val());
            }

            $('.DocTypePickerLink a').click(function() {
                var docTypeId = $(this).find('.SelectedDocTypeId').val();
                if (docTypeId != -1) $(this).closest('tr').addClass('PendingApply');
            });

            $('#Apply').on('CheckStatus', function() {
                var hasVisibleRows = false;
                $('#Files tbody tr').each(function(idx, elem) {
                    if ($(elem).is(':visible')) {
                        hasVisibleRows = true;
                        return false;
                    }
                });
                
                $('#Apply').prop('disabled', !hasVisibleRows);
            }).trigger('CheckStatus');

            $('.RequiresSelection').on('CheckStatus', function () {
                $(this).prop('disabled', numSelected <= 0);
            }).trigger('CheckStatus');

            var numSelected = 0;
            $('input.Select').change(function() {
                var parentTr = $(this).closest('tr');
                var isChecked = $(this).is(':checked');
                if (isChecked) {
                    numSelected++;
                    parentTr.addClass('Selected');
                }
                else {
                    numSelected--;
                    parentTr.removeClass('Selected');
                }

                $('.RequiresSelection').trigger('CheckStatus');
                $(this).css('background-color', '');
            });

            var appInfo = null;
            if (ML.AppInfoJson != null) {
                appSelected(JSON.parse(ML.AppInfoJson));
            }

            $.tablesorter.addParser({
                id: 'ticksFromInfo',
                is: function(s) {
                    return false;
                },
                format: function(value, table, cell) {
                    return $('.Age', cell).val();
                },
                type: 'numeric'
            });

            if ($('#Files tbody').children().length != 0) {
                $.tablesorter.defaults.widgetZebra = {
                    css: ["GridItem", "GridAlternatingItem"]
                };
                var sorting = [[0, 0]];
                $('#Files').tablesorter(
                {
                    sortList: sorting,
                    widgets: ['zebra'],
                    headers: {
                        0: {
                            sorter: 'ticksFromInfo'
                        },
                        1: {
                            sorter: false
                        },
                        2: {
                            sorter: false
                        }
                    }

                });
            }

            function uploadFilesToDropBox() {
                var fileCount = getFilesCount('DragDropZone');
                if (fileCount === 0) {
                    LQBPopup.ShowString('Please select file(s) to upload.');
                    return;
                }

                if (appInfo != null) {
                    var args = {
                        IdList: JSON.stringify(appInfo)
                    };

                    gService.utils.callAsyncSimple('StoreIdListToCache', args, function (result) {
                        if (result.error) {
                            alert(result.UserMessage);
                            return;
                        }

                        uploadFiles(fileCount, result.value['CacheKey']);
                    });
                }
                else {
                    uploadFiles(fileCount);
                }
            }

            function uploadFiles(fileCount, cacheKey) {
                var totalFileCount = 0;
                var $errorDiv = $('#ErrorDiv').hide().empty();
                triggerOverlayImmediately();

                applyCallbackFileObjects('DragDropZone', function (file) {
                    DocumentUploadHelper.Upload('UploadPdfToDropbox', file, null/*metadataDictionary*/, function (result) {
                        if (result.error) {
                            var $errorMessage = $('<div/>').text('Unable to upload file ' + file.name + ' : ' + result.UserMessage);
                            $errorDiv.append($errorMessage);
                        }

                        ++totalFileCount;
                        if (totalFileCount === fileCount) {
                            resetDragDrop('DragDropZone');
                            removeOverlay();

                            if ($errorDiv.find('div').length !== 0) {
                                var $uploadNotice = $('<div class="upload-notice" />').text('All other documents uploaded.');
                                $errorDiv.show();
                            }
                            else {
                                var url = window.location.href;
                                if (cacheKey != null) {
                                    // Remove any previous cache keys in the event
                                    // the user has uploaded multiple times in the
                                    // same session.
                                    url = url.split('?')[0] + '?k=' + cacheKey;
                                }

                                window.location.href = url;
                            }
                        }
                    });
                });
            }

            $('#UploadButton').click(uploadFilesToDropBox);

            $('#Cancel').click(function() {
                CloseWindow(false);
            });

            $('span.View').click(function() {
                var docGuid = GetGuidFromElement(this);
                var toBack = window.open("FileList.aspx?View=" + docGuid, "EdocView", "width=1,height=1,left=2000,top=2000");
                toBack.blur();
                window.focus();
            });

            $('#DeleteSelected').click(function() {
                var toDelete = GetSelected();

                var DTO = {
                    'ToDelete': toDelete
                };

                DoWebRequest('DeleteFiles', DTO, this, function(msg) {
                    if (msg.d.Success) {
                        CleanUpFiles();
                        alert("Files successfully deleted");
                    }
                    else if (msg.d.ErrorIds.length) {
                        alert("An error occurred while " + msg.d.ErrorType + ", please try again later");
                        window.name = "temp";
                        window.open(window.location, "temp");
                    }
                    else {
                        alert("An unexpected error happened, please try again later.");
                        CloseWindow(false);
                    }
                });
            });

            $('#MergeSelected').click(function() {
                if (SelectionContainsESignedDocuments()) {
                    var msg = 'One or more of the selected documents is ESigned. Merging the documents will destroy the ESignatures. Do you want to continue?';
                    if (!confirm(msg)) {
                        return;
                    }
                }

                var toMerge = GetSelected();

                var DTO = {
                    'mergeIds': toMerge
                };

                DoWebRequest('MergeFiles', DTO, this, function(msg) {
                    if (msg.d.Success) {
                        alert("Files successfully merged");
                        __doPostBack('', '');   // Refresh file list.
                    }
                    else if (msg.d.ErrorIds.length) {
                        alert("An error occurred while " + msg.d.ErrorType + ", please try again later");
                        window.name = "temp";
                        window.open(window.location, "temp");
                    }
                    else {
                        alert("An unexpected error happened, please try again later.");
                        CloseWindow(false);
                    }
                });
            });

            $('#Apply').click(function() {
                if (!appInfo) {
                    $('.ApplicationSelector').effect('highlight', {}, 3000);
                    alert("Please select an application first");
                    return;
                }

                var dts = GetEdocInfo();

                if (dts.length == 0) {
                    alert("Please select a Doc Type for at least one file");
                    return;
                }

                var DTO = {
                    'LoanId': appInfo.Loan.Id,
                    'AppId': appInfo.Application.Id,
                    'Info': dts
                };

                DoWebRequest('MoveFilesToEdocs', DTO, this, function(msg) {
                    if (msg.d.Success) {
                        $('#Files tbody tr.PendingApply').not('.Selected').hide();
                        UpdateNumFiles();

                        $('#Files').trigger("sorton", [$('#Files')[0].config.sortList]);
                        alert("Files successfully applied to " + appInfo.Application.Name);
                    }
                    else if (msg.d.ErrorType == "Access Denied")
                    {
                        $("#ErrorMsg").text(msg.d.ErrorMsg);
                        LQBPopup.ShowElement($("#ErrorPopup"), { width: 400, height: 150 });
                    }
                    else if (msg.d.ErrorIds.length) {
                        alert("An error occurred while " + msg.d.ErrorType);
                        window.name = "temp";
                        window.open(window.location, "temp");
                    }
                    else {
                        alert("An unexpected error happened, please check the application " + appInfo.Application.Name);
                        CloseWindow(false);
                    }
                });
            });

            function CleanUpFiles() {
                var selected = $('#Files tbody > tr.Selected');
                selected.hide();
                selected.find('td.Delete > input.Select').prop('checked', false).change();
                UpdateNumFiles();

                $('#Files').trigger("sorton", [$('#Files')[0].config.sortList]);
            }

            function GetSelected() {
                var selected = [];
                $('#Files tbody tr.Selected').each(function() {
                    selected.push(GetGuidFromElement(this));
                });

                return selected;
            }

            function SelectionContainsESignedDocuments() {
                return $('#Files tbody tr.Selected input[type="hidden"][id$="isESigned"][value="True"]').length > 0
            }

            function DoWebRequest(url, DTO, button, successCB) {
                $(button).addClass('Disabled');
                $('#Controls input').attr('disabled', 'disabled');
                var oldVal = $(button).val();
                $(button).val('Please wait...');
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: 'FileList.aspx/' + url,
                    data: JSON.stringify(DTO),
                    dataType: 'json',

                    success: successCB,

                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("An unexpected error happened, please try again later");
                    },

                    complete: function() {
                        $('#Controls input').removeAttr('disabled');
                        $(button).removeClass('Disabled');
                        $(button).val(oldVal);
                        $(button).css('background-color', '');
                        $('#Controls input').trigger('CheckStatus');
                    }
                });
            }

            $('#ApplicationSelect').click(function() {
                showModal('/los/UploadedFiles/SelectApplication.aspx', { callback: appSelected }, "dialogWidth: 700px; dialogHeight: 760px; center: yes; resizable: yes; scroll: yes; status: yes; help: no;");
            });

            function appSelected(selected) {
                $('#ApplicationIndicator').text(selected.Loan.Name + ": " + selected.Application.Name);
                //selected goes away as soon as the child window closes, so we need to make a copy (by extending it with a blank object)
                appInfo = $.extend(true, {}, selected);
            }

            function GetEdocInfo() {
                var ret = [];
                $('#Files tbody tr').not('.Selected').filter(':visible').each(function() {
                    var docTypeId = $(this).find('.SelectedDocTypeId').val();
                    if (docTypeId == -1) return;

                    $(this).addClass('PendingApply');

                    var guid = GetGuidFromElement(this);
                    var description = $(this).find('td.Description textarea').val();
                    ret.push({ Id: guid, Desc: description, DocType: docTypeId });
                });

                return ret;
            }

            function GetGuidFromElement(elem) {
                //Since closest will start looking at elem, this works for the tr or any of its children.

                return $(elem).closest('tr').attr('data-id');
            }

            function UpdateNumFiles() {
                NumFiles = $('#Files tbody tr').filter(':visible').length;
            }

            function CloseWindow(applied) {
                args = window.dialogArguments || {};
                args.Applied = applied;
                UpdateNumFiles();

                onClosePopup(args);
            }
        });
    </script>

    <style type="text/css">
        #Files thead tr th
        {
            background-color: #999999;
            font-size: 11px;
            color: White;
        }
        #Files thead tr th.Sortable
        {
            color: #0000FF;
        }
        #Files
        {
            border-spacing: 1px;
            border-collapse: separate;
            width: 100%;
            background-color: #ece9d8;
        }
        .DocType
        {
            text-align: center;
            width: 150px;
        }
        .SelectedDocTypeName
        {
            display: block;
        }
        .Description
        {
            width: 300px;
        }
        .Description textarea
        {
            width: 98%;
        }
        div.Name
        {
            font-weight: bold;
        }
        td.FileName
        {
            min-width: 150px;
            max-width: 250px;
            padding-left: 2px;
        }
        div.info
        {
            padding-right: 5px;
        }
        div.info span.UploadedDate
        {
            float: right;
            font-family: Monospace;
        }
        td.Controls
        {
            text-align: center;
            width: 40px;
        }
        td.Controls a.View
        {
            float: left;
            margin-left: 5px;
        }
        
        td.Delete
        {
            text-align: center;
        }
        
        th.Delete
        {
            padding-left: 4px;
            padding-right: 4px;
        }
        

        span.ActionLink, th.Sortable
        {
            cursor: pointer;
            color: Blue;
            text-decoration: underline;
        }
        .ApplicationSelector
        {
            font-weight: bold;
            padding-top: 3px;
            padding-bottom: 3px;
            background: #DCDCDC;
            margin-top: 10px;
        }
        /*jQuery UI needs the background property set*/tr.GridItem td
        {
            background: white;
        }
        tr.GridAlternatingItem td
        {
            background: #CCCCCC;
        }
        
        tr.PendingApply td
        {
            font-weight: bold;
        }
        
        tr.Selected td
        {
            font-weight: normal;
            font-style: italic;
            background: #BBBBBB;
        }
        
        input[type=button]
        {
            width: 55px;
        }
        
        input[type=button].Disabled
        {
            width: 100px;
        }

        #ErrorDiv {
            display: none;
            color: red;
        }
        
        .upload-notice {
            color: blue;
        }

        #CalculationOverlay {
            position: absolute;
            left: 0;
            top: 0; 
            height: 100%;
            width: 100%;
            background-color: black;
            opacity: 0;
            z-index: 9999998;
        }

        #CalculationOverlay.darkened {
            opacity: .3;
        }

        #ErrorPopup {
            display: none;
        }

        #ErrorMsg {
            display: inline-block;
            text-align: left;
            white-space: pre-wrap;
            overflow: auto;
            padding: 5px
        }

        .upload-notice-label {
            font-weight: bold;
            margin-top: 5px;
            padding-left: 2px;
        }

        #UploadButton {
            margin-left: 2px;
        }
    </style>
</head>
<body bgcolor='gainsboro'>
    <form id="form1" runat="server">
        <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
        <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />  
        <div id='CalculationOverlay' class="Hidden"></div>
        <script type="text/javascript">
            var theForm = document.forms['form1'];
            if (!theForm) {
                theForm = document.form1;
            }
            function __doPostBack(eventTarget, eventArgument) {
                if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                    theForm.__EVENTTARGET.value = eventTarget;
                    theForm.__EVENTARGUMENT.value = eventArgument;
                    theForm.submit();
                }
            }
        </script>
    <div>
        <div id="ErrorPopup">
            <h4 class="page-header" >Error</h4>
            <div id="ErrorMsg"></div>
            <div>
                <input type="button" id="btnCloseErrorPopup" value="OK" onclick="LQBPopup.Hide()" />
            </div>
        </div>

        <div class="FormTableHeader">
            EDocs Dropbox
        </div>
        <div class="upload-notice-label">
            You may upload up to 12 PDFs at a time with a total upload size of 100MB.
        </div>
        <div id="DragDropZoneContainer" class="InsetBorder full-width">
            <div id="DragDropZone" class="drag-drop-container" limit="12" extensions=".pdf"></div>
        </div>

        <div id="ErrorDiv"></div>

        <input id="UploadButton" type="button" value="Upload" />

        <div class="ApplicationSelector">
            Send to Application: <span id="ApplicationIndicator">No Application Selected</span>
            [ <span class="ActionLink" id="ApplicationSelect">select Application</span> ]</div>
        <table id="Files">
            <thead>
                <tr>
                    <th class='Sortable'>
                        File Name
                    </th>
                    <th>
                        Doc Type
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        <!-- view, no header -->
                    </th>
                    <th class="Delete">
                        Delete / Merge
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="MainList" runat="server" OnItemDataBound="MainList_OnItemDataBound">
                    <ItemTemplate>
                        <tr runat="server" id="row">
                            <td class='FileName'>
                                <div class="Name">
                                    <ml:EncodedLiteral runat="server" ID="name"></ml:EncodedLiteral>
                                    </div>
                                <div class='info'>
                                    <input type="hidden" runat="server" id="age" class="Age" />
                                    <input type="hidden" runat="server" id="isESigned" class="IsESigned" />
                                    <div>
                                        <img runat="server" src="~/images/edocs/SigIcon.png" alt="This document is ESigned." id="ESignIcon" />
                                    </div>
                                    <span class="FileSize" runat="server" id="fileSize"></span>
                                    <span class="UploadedDate" runat="server" id="uploadDate"></span>
                                </div>
                            </td>
                            <td class="DocType">
                                <uc:DocTypePicker runat="server" ID="DocTypePicker" Width="200px"></uc:DocTypePicker>
                            </td>
                            <td class="Description">
                                <textarea id="Description" runat="server" class="Description" rows="3"></textarea>
                            </td>
                            <td class="Controls">
                                <span class="ActionLink View">view</span>
                            </td>
                            <td class="Delete">
                                <input type="checkbox" class="Select" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
            <tfoot>
            <!-- Deleted items go here -->
            </tfoot>
        </table>
        <div id="Controls">
            <div>
                <input type="button" value="Apply" id="Apply" />
                <input type="button" value="Merge" id="MergeSelected" class="RequiresSelection" />            
            </div>
            <div>
                <input type="button" value="Delete" id="DeleteSelected" class="RequiresSelection" />
                <input type="button" value="Cancel" id="Cancel" />
            </div>
        </div>
        
       
    </div>
    </form>
</body>
</html>
