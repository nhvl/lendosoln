﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LendersOffice.ObjLib.UserDropbox;
using LendersOffice.Security;
using LendersOffice.Common;
using System.Web.Services;
using EDocs;
using DataAccess;
using CommonProjectLib.Common.Lib;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using EDocs.Contents;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using LendersOfficeApp.newlos.ElectronicDocs;

namespace LendersOfficeApp.los.UploadedFiles
{
    public partial class FileList : BaseServicePage
    {
        private const int PDFTYPE_INVALID = 0;
        private const int PDFTYPE_DROPBOX = 1;
        private const int PDFTYPE_LOSTDOC = 2;

        private Dictionary<string, string> idsToDocTypes = new Dictionary<string, string>();
        private Dictionary<string, string> idsToDescriptions = new Dictionary<string, string>();

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode() => E_XUAComaptibleValue.IE10;

        protected override void OnInit(EventArgs e)
        {
            EnableJqueryMigrate = false;

            this.RegisterJsScript("jquery.tablesorter.min.js");
            this.RegisterJsScript("LQBPopup.js");
            this.RegisterJsScript("DragDropUpload.js");
            this.RegisterJsScript("DocumentUploadHelper.js");

            this.RegisterCSS("DragDropUpload.css");

            base.OnInit(e);
        }

        //The size is carried as an int so it won't go over 4 GB.
        static readonly string[] suffixes = { "", "K", "M", "G"};
        protected static string FriendlyBytes(int size)
        {
            int suffixIdx = 0;
            double divvy = size;
            while (divvy > 1024)
            {
                divvy /= 1024;
                suffixIdx++;
            }

            //Make sure we're only displaying three digits
            string fmt = "### ";
            if (divvy < 100) fmt = "##.0 ";
            if (divvy < 10) fmt = "0.## ";

            return divvy.ToString(fmt) + suffixes[suffixIdx] + "B";
        }

        protected void MainList_OnItemDataBound(object sender, RepeaterItemEventArgs args)
        {
            if (args.Item.ItemType != ListItemType.Item && args.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            HtmlContainerControl row = args.Item.FindControl("row") as HtmlContainerControl;
            Literal name = args.Item.FindControl("name") as Literal;
            HtmlInputControl ageInput = args.Item.FindControl("age") as HtmlInputControl;
            HtmlContainerControl fileSize = args.Item.FindControl("fileSize") as HtmlContainerControl;
            HtmlContainerControl uploadDate = args.Item.FindControl("uploadDate") as HtmlContainerControl;
            DocTypePickerControl picker = args.Item.FindControl("DocTypePicker") as DocTypePickerControl;
            HtmlTextArea description = args.Item.FindControl("Description") as HtmlTextArea;
            HtmlInputHidden isESigned = args.Item.FindControl("isESigned") as HtmlInputHidden;
            HtmlImage esignIcon = args.Item.FindControl("ESignIcon") as HtmlImage;

            Guid id;
            int pdfType;
            bool esigned;

            if (args.Item.DataItem is Dropbox.DropboxFile)
            {
                pdfType = PDFTYPE_DROPBOX;
                Dropbox.DropboxFile file = args.Item.DataItem as Dropbox.DropboxFile;
                id = file.FileId;
                name.Text = file.FileName;
                ageInput.Value = (file.UploadedOn.Ticks / 10000000).ToString();
                fileSize.InnerText = FriendlyBytes(file.SizeBytes);
                uploadDate.InnerText = file.UploadedOn.ToString("MM/dd/yyyy hh:mm tt");
                esigned = file.IsESigned;
            }
            else
            {
                pdfType = PDFTYPE_LOSTDOC;
                LostEDocument file = args.Item.DataItem as LostEDocument;
                id = file.DocumentId;
                name.Text = file.Description;
                ageInput.Value = (file.CreatedDate.Ticks / 10000000).ToString();
                uploadDate.InnerText = file.CreatedDate.ToString("MM/dd/yyyy hh:mm tt");
                esigned = file.IsESigned;
            }

            isESigned.Value = esigned.ToString();
            esignIcon.Visible = esigned;

            string dataId = GetPdfTypeId(id, pdfType);
            row.Attributes.Add("data-id", dataId);

            if (idsToDocTypes.ContainsKey(dataId))
            {
                picker.Value = idsToDocTypes[dataId];
            }

            if (idsToDescriptions.ContainsKey(dataId))
            {
                description.Value = idsToDescriptions[dataId];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var querystring = Request.QueryString;
            
            if (querystring["auth_id"] != null)
            {
                Response.Redirect("FileList.aspx");
                return;
            }

            IEnumerable<LostEDocument> lostDocs = null;
            
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            if (principal.HasPermission(Permission.AllowManagingFailedDocs))
            {
                lostDocs = LostEDocument.GetBarcodeDocumentsByBrokerId(principal.BrokerId);
            }

            var appCacheKey = RequestHelper.GetSafeQueryString("k");
            if (!string.IsNullOrEmpty(appCacheKey))
            {
                var cacheData = AutoExpiredTextCache.GetFromCacheByUser(principal, appCacheKey);
                this.RegisterJsGlobalVariables("AppInfoJson", cacheData);
            }

            var dropboxFiles = Dropbox.GetFilesForUser(BrokerUserPrincipal.CurrentPrincipal.ConnectionInfo, BrokerUserPrincipal.CurrentPrincipal.UserId);
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["View"]))
            {
                string viewId = RequestHelper.GetSafeQueryString("View");

                Guid documentId;
                
                int pdfType = FileList.GetPDFTypeAndId(viewId, out documentId);

                if (PDFTYPE_DROPBOX == pdfType)
                {
                    var doc = dropboxFiles.Where(a => a.FileId == documentId).FirstOrDefault();
                    if (doc != null)
                    {
                        ViewFile(doc);
                        return;
                    }
                }
                else if (PDFTYPE_LOSTDOC == pdfType)
                {
                    var doc = LostEDocument.GetDocumentById(documentId, PrincipalFactory.CurrentPrincipal.BrokerId);
                    RequestHelper.SendFileToClient(this.Context, doc.GetPathForFile(), "application/pdf", doc.Description);
                    this.Context.Response.SuppressContent = true;
                    this.Context.ApplicationInstance.CompleteRequest();
                    return;
                }

                ClientScript.RegisterHiddenField("Error", "File could not be found, please try again");
            }


            IEnumerable<object> docs = dropboxFiles;

            if (lostDocs != null)
            {
                docs = docs.Union(lostDocs); 
            }

            BindData(docs);
        }

        private void BindData(IEnumerable<object> dropboxFiles)
        {
            // Save DocType Values before rebinding.
            idsToDocTypes.Clear();
            foreach (RepeaterItem item in MainList.Items)
            {
                HtmlContainerControl row = item.FindControl("row") as HtmlContainerControl;
                DocTypePickerControl picker = item.FindControl("DocTypePicker") as DocTypePickerControl;

                idsToDocTypes.Add(row.Attributes["data-id"], picker.Value);
            }

            // Save Description Values.
            idsToDescriptions.Clear();
            foreach (RepeaterItem item in MainList.Items)
            {
                HtmlContainerControl row = item.FindControl("row") as HtmlContainerControl;
                HtmlTextArea description = item.FindControl("Description") as HtmlTextArea;

                idsToDescriptions.Add(row.Attributes["data-id"], description.Value);
            }

            MainList.DataSource = dropboxFiles;
            MainList.DataBind();
        }

        private void ViewFile(Dropbox.DropboxFile file)
        {
            string suffix = "";
            Response.Clear();

            if (!file.FileName.ToLower().EndsWith(".pdf") && !file.FileName.ToLower().EndsWith(".xml"))
            {
                suffix = ".pdf";
            }

            string contentType = file.FileName.ToLower().EndsWith(".xml") ? "text/xml" : "application/pdf"; 
            string fileName = file.FileName + suffix;
            string pdfPath = file.GetPdfFile();
            
            RequestHelper.SendFileToClient(this.Context, pdfPath, contentType, fileName);
            this.Context.Response.SuppressContent = true;
            this.Context.ApplicationInstance.CompleteRequest();
            
        }

        private static string GetPdfTypeId(Guid id, int pdfType)
        {
            if (pdfType == PDFTYPE_DROPBOX)
            {
                return string.Concat("d_", id);
            }

            if (pdfType == PDFTYPE_LOSTDOC)
            {
                return string.Concat("l_", id);
            }

            return null;
        }

        private static int GetPDFTypeAndId(string stringId, out Guid id)
        {
            id = Guid.Empty;
            int pdfType = PDFTYPE_INVALID; 

            if (string.IsNullOrEmpty(stringId) && stringId.Length != Guid.Empty.ToString().Length + 2)
            {
                return pdfType;
            }

            char firstChar = stringId[0];

            if (firstChar == 'l')
            {
                pdfType = PDFTYPE_LOSTDOC;
            }
            else if(firstChar == 'd')
            {
                pdfType = PDFTYPE_DROPBOX;
            }

            if (!Guid.TryParse(stringId.Substring(2), out id))
            {
                pdfType = PDFTYPE_INVALID; 
            }

            return pdfType;
        }

        public class EdocInfo
        {
            public string Id;
            public string Desc;
            public int DocType;
        }

        [WebMethod]
        public static object MergeFiles(List<string> mergeIds)
        {
            try
            {
                HashSet<Guid> dropboxIds = new HashSet<Guid>();
                HashSet<Guid> lostDocIds = new HashSet<Guid>();

                foreach (string mergeId in mergeIds)
                {
                    Guid id;
                    int pdfType = FileList.GetPDFTypeAndId(mergeId, out id);

                    if (PDFTYPE_INVALID == pdfType)
                    {
                        continue;
                    }
                    else if (pdfType == PDFTYPE_DROPBOX)
                    {
                        dropboxIds.Add(id);
                    }
                    else if (pdfType == PDFTYPE_LOSTDOC)
                    {
                        lostDocIds.Add(id);
                    }
                }

                var mergeDocs = Dropbox.GetFilesForUser(BrokerUserPrincipal.CurrentPrincipal.ConnectionInfo, BrokerUserPrincipal.CurrentPrincipal.UserId, dropboxIds);

                var mergeInfo = new List<Tuple<string, Func<byte[]>, Guid>>(dropboxIds.Count + lostDocIds.Count);

                foreach (var doc in mergeDocs)
                {
                    mergeInfo.Add(Tuple.Create<string, Func<byte[]>, Guid>(doc.FileName, doc.GetFileBytes, doc.FileId));
                }

                if (PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowManagingFailedDocs))
                {
                    var docs = LostEDocument.GetBarcodeDocumentsByBrokerId(PrincipalFactory.CurrentPrincipal.BrokerId).Where(p => lostDocIds.Contains(p.DocumentId));
                    foreach( var doc in docs)
                    {
                        mergeInfo.Add(Tuple.Create<string, Func<byte[]>, Guid>(doc.Description, () => doc.PDFData, doc.DocumentId));
                    }
                }

                using (MemoryStream output = new MemoryStream())
                {
                    EDocumentViewer.WriteSinglePdf(mergeInfo, output);
                    string basename = Path.GetFileNameWithoutExtension(mergeInfo.First().Item1);
                    string name;
                    if (basename.EndsWith("[Merged]")) name = basename + ".pdf";
                    else name = basename + "[Merged].pdf";
                    Dropbox.AddFile(
                        BrokerUserPrincipal.CurrentPrincipal.UserId,
                        BrokerUserPrincipal.CurrentPrincipal.BrokerId,
                        name,
                        DateTime.Now,
                        output.ToArray());
                }

                return DeleteFiles(mergeIds);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                return new
                {
                    Success = false,
                    ErrorType = "merging files",
                    ErrorIds = mergeIds
                };
            }
        }

        private static object DeleteFilesImpl(List<string> ToDelete)
        {
            var user = BrokerUserPrincipal.CurrentPrincipal;

            var deleteErrorIds = new List<string>();
            foreach (var deleteMe in ToDelete)
            {
                Guid id;
                int pdfType = FileList.GetPDFTypeAndId(deleteMe, out id);
                
                if (PDFTYPE_DROPBOX == pdfType && !Dropbox.RemoveFile(user.ConnectionInfo, user.UserId, id))
                {
                    deleteErrorIds.Add(deleteMe);
                }
                else if (user.HasPermission(Permission.AllowManagingFailedDocs) && PDFTYPE_LOSTDOC == pdfType && !LostEDocument.DeleteDocument(id, user))
                {
                    deleteErrorIds.Add(deleteMe);
                }
            }

            if (deleteErrorIds.Any())
            {
                return new
                {
                    Success = false,
                    ErrorType = "deleting files from dropbox",
                    ErrorIds = deleteErrorIds
                };
            }

            return new
            {
                Success = true,
                ErrorType = "none",
                ErrorIds = new List<Guid>()
            };
        }

        [WebMethod]
        public static object DeleteFiles(List<string> ToDelete)
        {
            return Tools.LogAndThrowIfErrors<object>(() => FileList.DeleteFilesImpl(ToDelete));
        }

        private static object MoveFilesToEdocsImpl(Guid LoanId, Guid AppId, EdocInfo[] Info)
        {
            try
            {
                var user = BrokerUserPrincipal.CurrentPrincipal;

                EDocsUpload.AssertUploadEDocsWorkflowPrivileges(user, LoanId);

                var infoById = Info.ToDictionary((a) => a.Id);
                var errorIds = new List<string>();
                var dropboxDocs = Dropbox.GetFilesForUser(user.ConnectionInfo, user.UserId).ToDictionary(p=>p.FileId);
                var lostDocs = new Dictionary<Guid, LostEDocument>();
                Dictionary<string, object> docsById = new Dictionary<string, object>();
                List<object> objectsToDelete = new List<object>();

                if (user.HasPermission(Permission.AllowManagingFailedDocs))
                {
                    lostDocs = LostEDocument.GetBarcodeDocumentsByBrokerId(user.BrokerId).ToDictionary(p=>p.DocumentId);
                }

                List<Tuple<EdocInfo, string>> docsAndFile = new List<Tuple<EdocInfo, string>>();

                foreach (EdocInfo info in Info)
                {
                    Guid id;
                    int pdfType = FileList.GetPDFTypeAndId(info.Id, out id);
                    string tempFile = null;

                    if (pdfType == PDFTYPE_DROPBOX)
                    {
                        Dropbox.DropboxFile file;

                        if (dropboxDocs.TryGetValue(id, out file))
                        {
                            tempFile = file.GetPdfFile();
                            docsById.Add(info.Id, file);
                        }
                    }
                    else if (pdfType == PDFTYPE_LOSTDOC)
                    {
                        LostEDocument file;
                        if (lostDocs.TryGetValue(id, out file))
                        {
                            tempFile = file.GetPathForFile();
                            docsById.Add(info.Id, file);
                        }

                    }

                    if (!string.IsNullOrEmpty(tempFile))
                    {
                        docsAndFile.Add(Tuple.Create(info, tempFile));
                    }
                }


                EDocumentRepository repo = EDocumentRepository.GetUserRepository();

                foreach (var docAndFile in docsAndFile)
                {
                    EdocInfo info = docAndFile.Item1;
                    EDocument doc = repo.CreateDocument(E_EDocumentSource.UserUpload);
                    doc.DocumentTypeId = info.DocType;
                    doc.LoanId = LoanId;
                    doc.AppId = AppId;
                    doc.IsUploadedByPmlUser = false;
                    doc.PublicDescription = info.Desc;
                    doc.EDocOrigin = E_EDocOrigin.LO;

                    string path = docAndFile.Item2;

                    XmlDocument xmlDoc = new XmlDocument();
                    bool isXml = false;
                    try
                    {
                        path = EDocsUpload.ParseAppraisalDocFromXml(path);
                        isXml = true;
                        doc.MarkDocumentAsAccepted();
                    }
                    catch (XmlException)
                    {
                    }
                    catch (CBaseException cbe)
                    {
                        if (cbe.UserMessage == ErrorMessages.EDocs.InvalidXML)
                        {
                            errorIds.Add(info.Id);
                            continue;
                        }
                        else
                        {
                            throw;
                        }
                    }

                    doc.UpdatePDFContentOnSave(path);

                    try
                    {
                        repo.Save(doc);

                        if (isXml)
                        {
                            GenericEDocument xmleDoc = repo.CreateLinkedGenericDocument(E_FileType.AppraisalXml, doc.DocumentId);
                            xmleDoc.ApplicationId = doc.AppId.Value;
                            xmleDoc.Description = info.Desc;
                            xmleDoc.DocTypeId = info.DocType;
                            xmleDoc.LoanId = LoanId;
                            xmleDoc.SetContent(docAndFile.Item2, "document.xml");
                            xmleDoc.InternalComments = "";
                            repo.Save(xmleDoc, user.UserId);
                        }
                        
                        objectsToDelete.Add(docsById[info.Id]);
                    }
                    catch (InvalidPDFFileException)
                    {
                        errorIds.Add(info.Id);
                        continue;
                    }
                }

                foreach (var item in objectsToDelete)
                {
                    Dropbox.DropboxFile file = item as Dropbox.DropboxFile;

                    if (file != null)
                    {
                        Dropbox.RemoveFile(user.ConnectionInfo, user.UserId, file.FileId);
                    }
                    else
                    {
                        LostEDocument doc = item as LostEDocument;
                        LostEDocument.DeleteDocument(doc.DocumentId, user);
                    }
                }

                return new
                {
                    Success = !errorIds.Any(),
                    ErrorType = "moving to edocs",
                    ErrorIds = errorIds
                };
            }
            catch (AccessDenied e)
            {
                return new
                {
                    Success = false,
                    ErrorType = "Access Denied",
                    ErrorMsg = e.UserMessage
                };
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw;
            }
        }

        [WebMethod]
        public static object MoveFilesToEdocs(Guid LoanId, Guid AppId, EdocInfo[] Info)
        {
            return Tools.LogAndThrowIfErrors<object>(() => FileList.MoveFilesToEdocsImpl(LoanId, AppId, Info));
        }
    }
}
