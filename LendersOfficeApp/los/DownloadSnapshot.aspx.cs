/// Author: David Dao

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los
{
	public partial class DownloadSnapshot : BasePage
	{
		protected void PageLoad(object sender, System.EventArgs e)
		{
			string key = RequestHelper.GetSafeQueryString("key");

            string xml = AutoExpiredTextCache.GetFromCache(key);

            if (null != xml) 
            {
                Response.Clear();
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=\"result.xml\"");
                Response.ContentType = "text/xml";
                Response.Write(xml);
                Response.Flush();
                Response.End();
            }
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.PageLoad);
		}
		#endregion
	}
}
