using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using LendersOfficeApp.los.admin;
using LendersOfficeApp.common;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Common;
using System.Collections.Generic;
using System.Data.SqlClient;
using CommonProjectLib.Common.Lib;

namespace LendersOfficeApp.los
{
    public partial class RegisterMobileDevice : LendersOffice.Common.BaseServicePage
    {
        public bool showInfoTable = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                reloadRegisterList();
            }
            
        }
        protected void deleteRegisterMobile(object sender, CommandEventArgs e)
        {
            //get register info id
            Int64 id =  Convert.ToInt64( e.CommandArgument.ToString() ) ;

            bool registerDeleted = MobileDeviceUtilities.RemoveDevice(id);

            if (registerDeleted)
            {
                reloadRegisterList();
            }
        }
        protected void reloadRegisterInfo(object sender, EventArgs e)
        {
            reloadRegisterList();
        }
        protected void reloadRegisterList()
        {
            //load regster info from database
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal; //(BrokerUserPrincipal) Page.User;
            
            registerInfoRepeater.DataSource = MobileDeviceUtilities.RetrieveUsersMobileDevices(principal.UserId, principal.BrokerId, principal.ConnectionInfo);
            registerInfoRepeater.DataBind();
            showInfoTable = registerInfoRepeater.Items.Count > 0;
        }
        
         protected override void OnInit(EventArgs e)
        {
            RegisterJsScript("LQBPopup.js");
            base.OnInit(e);
        }
    }
}
