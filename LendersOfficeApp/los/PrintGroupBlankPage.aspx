<%@ Page language="c#" Codebehind="PrintGroupBlankPage.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.PrintGroupBlankPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
  <head>
    <title>PrintGroupBlankPage</title>
    <meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" Content="C#">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
  </head>
  <body MS_POSITIONING="FlowLayout" bgcolor="gainsboro" class="PaddingTopBottom5">
	
    <form id="PrintGroupBlankPage" method="post" runat="server">
      <table width="100%" height="100%">
        <tr>
          <td align=center valign=center>
            No print groups have been created.
          </td>
        </tr>
      </table>
      
     </form>
	
  </body>
</html>
