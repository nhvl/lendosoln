using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;

using LendersOffice.Events;
using LendersOffice.Common;
using LendersOffice.Conversions.Mismo23;
using LendersOffice.Security;
using DataAccess;
using LendersOfficeApp.LegacyLink.CalyxPoint;
using LendersOffice.Conversions;
using LendersOffice.DU;
using LendersOfficeApp.los.admin;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Task;

namespace LendersOfficeApp.los.Imports
{
	public partial class BatchImportService : LendersOffice.Common.BaseSimpleServiceXmlPage
	{
        static BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }
        
        private Guid m_guidAutoId = new Guid("11111111-1111-1111-1111-111111111111");

        protected override void Process(string methodName) 
        {
            switch (methodName) 
            {
                case "ImportMismo23":
                    ImportMismo23();
                    break;
                case "ImportCalyx":
                    ImportCalyx();
                    break;
                case "ImportFannie":
                    ImportFannie();
                    break;
                case "ImportDoDu":
                    ImportDoDu();
                    break;
				case "SaveDirectory":
					SaveDirectoryLocationToCookie();
					break;
				case "GetDirectory":
					GetDirectoryLocationFromCookie();
					break;
            }
        }

		private void GetDirectoryLocationFromCookie()
		{
			string directory = "";
			string importType = GetString("ImportType");
			HttpCookie cookie = null;
			switch(importType.ToLower())
			{
				case "point":
					cookie = HttpContext.Current.Request.Cookies.Get("dlocP");
					break;
				case "fanniemae":
					cookie = HttpContext.Current.Request.Cookies.Get("dlocF");
					break;
				case "mismo":
					cookie = HttpContext.Current.Request.Cookies.Get("dlocM");
					break;
				default:
					cookie = HttpContext.Current.Request.Cookies.Get("dlocD");
					break;
			}
			
			if(null != cookie && cookie.Value.TrimWhitespaceAndBOM() != "")
			{
				directory = cookie.Value.TrimWhitespaceAndBOM();
			}
			SetResult("Status", directory);
		}

		private void SaveDirectoryLocationToCookie()
		{
			try
			{
				string directory = GetString("DirectoryLocation");
				string importType = GetString("ImportType");
				if(directory.TrimWhitespaceAndBOM() != "")
				{
					switch(importType.ToLower())
					{
						case "point":
							RequestHelper.StoreToCookie("dlocP", directory, SmallDateTime.MaxValue);
							break;
						case "fanniemae":
							RequestHelper.StoreToCookie("dlocF", directory, SmallDateTime.MaxValue);
							break;
						case "mismo":
							RequestHelper.StoreToCookie("dlocM", directory, SmallDateTime.MaxValue);
							break;
						default:
							RequestHelper.StoreToCookie("dlocD", directory, SmallDateTime.MaxValue);
							break;
					}
				}
			}
			catch{}
		}

        private void ImportMismo23() 
        {
            try 
            {
				Stream stream = GetBase64File("File");
                XmlDocument doc = new XmlDocument();
                doc.Load(stream);

                XmlElement xeLoanApp = (XmlElement) doc.SelectSingleNode("//LOAN_APPLICATION");
                if (xeLoanApp == null) 
                {
                    SetResult("Status", "Error");
                    SetResult("ErrorMessage", "Invalid MISMO format");
                    return;
                } 
                else 
                {
                    #region Create new loan

                    Guid employeeID = BrokerUserPrincipal.CurrentPrincipal.EmployeeId;
                    bool addEmployeeAsOfficialAgent = GetBool("sAddEmpAsAgent", false);
                    Guid templateId = RetrieveLoanTemplateID(BrokerUserPrincipal.CurrentPrincipal);
                    Guid sLId = Guid.Empty;

                    Guid guidDefaultAssignLoanOfficerID = GetGuid("sEmployeeLoanRepId", Guid.Empty);
                    Guid guidDefaultAssignLoanProcessorID = GetGuid("sEmployeeProcessorId", Guid.Empty);
                    Guid guidDefaultAssignLoanOpenerID = GetGuid("sEmployeeLoanOpenerId", Guid.Empty);
                    Guid guidDefaultAssignManagerID = GetGuid("sEmployeeManagerId", Guid.Empty);
                    Guid guidDefaultAssignCallCenterAgentID = GetGuid("sEmployeeCallCenterAgentId", Guid.Empty);
                    Guid guidDefaultAssignRealEstateAgentID = GetGuid("sEmployeeRealEstateAgentId", Guid.Empty);
                    Guid guidDefaultAssignLenderAccountExecID = GetGuid("sEmployeeLenderAccExecId", Guid.Empty);
                    Guid guidDefaultAssignLockDescId = GetGuid("sEmployeeLockDeskId", Guid.Empty);
                    Guid guidDefaultAssignCloserId = GetGuid("sEmployeeCloserId", Guid.Empty);

                    // OPM 188399 - Determine which BranchId to use for loan (need to be deturmined before loan creation
                    // or may have issue with loan name/MERS MIN number in certain cases
                    // Get BranchId from first available source in list:
                    //  1. Assigned Loan Officer
                    //  2. Branch Drop Down List (sBranchId)
                    //  3. Principal
                    Guid sBranchId = GetGuid("sBranchId", Guid.Empty);  // BranchId from drop down

                    // Clobber BranchId if a valid loan officer is assigned
                    if (guidDefaultAssignLoanOfficerID != Guid.Empty)
                    {
                        EmployeeDB edb = EmployeeDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId, guidDefaultAssignLoanOfficerID);
                        if (edb != null)
                        {
                            sBranchId = edb.BranchID;
                        }
                    }

                    // Use Principal BranchId as last resort
                    if (sBranchId == Guid.Empty)
                    {
                        sBranchId = BrokerUserPrincipal.CurrentPrincipal.BranchId;
                    }

                    CLoanFileCreator creator = CLoanFileCreator.GetCreator(BrokerUserPrincipal.CurrentPrincipal, sBranchId, LendersOffice.Audit.E_LoanCreationSource.MismoImport);

                    // 6/27/2017 dt - Not sure the reason for the discrepancy between
                    // the parameters between the old calls. I'm hoping we can get rid
                    // of the differences later (or just delete all this code).
                    if (templateId == Guid.Empty)
                    {
                        sLId = creator.BeginCreateImportBaseLoanFile(
                            sourceFileId: templateId,
                            setInitialEmployeeRoles: addEmployeeAsOfficialAgent,
                            addEmployeeOfficialAgent: addEmployeeAsOfficialAgent,
                            assignEmployeesFromRelationships: false,
                            branchIdToUse: sBranchId);
                    }
                    else
                    {
                        sLId = creator.BeginCreateImportBaseLoanFile(
                            sourceFileId: templateId,
                            setInitialEmployeeRoles: false,
                            addEmployeeOfficialAgent: false,
                            assignEmployeesFromRelationships: false,
                            branchIdToUse: sBranchId);
                    }

                    CPageData dataLoan = new CLoanMismo23ImporterData(sLId) ;
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    
                    LoanMismo23Importer importer = new LoanMismo23Importer();
                    importer.Import(dataLoan, doc);


                    #region Set Default Import Settings
                    // Set Default Import Settings
                   
                    var newStatus = (E_sStatusT)GetInt("sStatusT");
                    if (ReturnEarlyDueToBadChannelAndStatus(dataLoan.sBranchChannelT, dataLoan.sCorrespondentProcessT, newStatus))
                    {
                        return;
                    }

                    dataLoan.sStatusT = newStatus;
                    dataLoan.sStatusLckd = true;

					dataLoan.sEmployeeLoanRepId         = guidDefaultAssignLoanOfficerID;
                    dataLoan.sEmployeeProcessorId       = guidDefaultAssignLoanProcessorID;
                    dataLoan.sEmployeeLoanOpenerId      = guidDefaultAssignLoanOpenerID;
                    dataLoan.sEmployeeManagerId         = guidDefaultAssignManagerID;
                    dataLoan.sEmployeeCallCenterAgentId = guidDefaultAssignCallCenterAgentID;
                    dataLoan.sEmployeeRealEstateAgentId = guidDefaultAssignRealEstateAgentID;
                    dataLoan.sEmployeeLenderAccExecId   = guidDefaultAssignLenderAccountExecID;
                    dataLoan.sEmployeeLockDeskId        = guidDefaultAssignLockDescId;
                    dataLoan.sEmployeeCloserId          = guidDefaultAssignCloserId;

                    #endregion
                    dataLoan.Save();

                    creator.CommitFileCreation( false /* refreshLoanTasks */ );
					Tools.NotifyImportAssignees( dataLoan.sLId ); 
					SetResult("LoanName", dataLoan.sLNm );
                    SetResult("Status", "OK");
                    #endregion
                }
            } 
            catch (CBaseException exc) 
            {
                Tools.LogErrorWithCriticalTracking(exc);
                SetResult("Status", "Error");
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                SetResult("Status", "Error");
                SetResult("ErrorMessage", exc.Message);
            } 
        }

        // Used for OPM 9831 to find the Manager and Lender Account Exec relationship of the current
        // assigned Loan Officer
        private Guid GetRelationshipOfGivenUser(Guid employeeId, E_RoleT roleType)
        {
            if (employeeId == Guid.Empty)
            {
                return Guid.Empty;
            }

            //string roleDesc = Role.Get(roleType).Desc;

            BrokerUserPermissions buP = new BrokerUserPermissions();
            EmployeeRelationships eRl = new EmployeeRelationships();
            EmployeeRoles employeeRoles = new EmployeeRoles(BrokerUser.BrokerId, employeeId);

            buP.Retrieve(BrokerUser.BrokerId, BrokerUser.EmployeeId);
            //eRs.Retrieve(employeeId);
            eRl.Retrieve(BrokerUser.BrokerId, employeeId);

            if (employeeRoles.IsInRole(roleType) == true)
            {
                return employeeId;
            }

            var relationshipSet = eRl[RelationshipSetType.LendingQB];

            if (relationshipSet.Contains(roleType))
            {
                Guid matchingEmployeeId = relationshipSet[roleType].EmployeeId;
                EmployeeDetails eD = new EmployeeDetails();
                eD.Retrieve(BrokerUser.BrokerId, matchingEmployeeId);
                
                return matchingEmployeeId;
            }

            return Guid.Empty;
        }

        private void ImportFannie() 
        {
            try 
            {
                bool addEmployeeAsOfficialAgent =GetBool("sAddEmpAsAgent", false);
                E_sStatusT status = (E_sStatusT)GetInt("sStatusT");
                bool willBecomeLead = status == E_sStatusT.Lead_Canceled || status == E_sStatusT.Lead_Declined || status == E_sStatusT.Lead_New || status == E_sStatusT.Lead_Other;
                Stream stream = GetBase64File("File");

                Guid employeeID = BrokerUserPrincipal.CurrentPrincipal.EmployeeId;
                Guid templateId = RetrieveLoanTemplateID(BrokerUserPrincipal.CurrentPrincipal);

				CPageData dataLoan = null;
                string warnings;

                Guid guidDefaultAssignLoanOfficerID = GetGuid("sEmployeeLoanRepId", Guid.Empty);
                Guid guidDefaultAssignLoanProcessorID = GetGuid("sEmployeeProcessorId", Guid.Empty);
                Guid guidDefaultAssignLoanOpenerID = GetGuid("sEmployeeLoanOpenerId", Guid.Empty);
                Guid guidDefaultAssignManagerID = GetGuid("sEmployeeManagerId", Guid.Empty);
                Guid guidDefaultAssignCallCenterAgentID = GetGuid("sEmployeeCallCenterAgentId", Guid.Empty);
                Guid guidDefaultAssignRealEstateAgentID = GetGuid("sEmployeeRealEstateAgentId", Guid.Empty);
                Guid guidDefaultAssignLenderAccountExecID = GetGuid("sEmployeeLenderAccExecId", Guid.Empty);
                Guid guidDefaultAssignLockDescId = GetGuid("sEmployeeLockDeskId", Guid.Empty);
                Guid guidDefaultAssignCloserId = GetGuid("sEmployeeCloserId", Guid.Empty);

                // OPM 188399 - Determine which BranchId to use for loan (need to be deturmined before loan creation
                // or may have issue with loan name/MERS MIN number in certain cases
                // Get BranchId from first available source in list:
                //  1. Assigned Loan Officer
                //  2. Branch Drop Down List (sBranchId)
                //  3. Principal
                Guid sBranchId = GetGuid("sBranchId", Guid.Empty);  // BranchId from drop down

                // Clobber BranchId if a valid loan officer is assigned
                if (guidDefaultAssignLoanOfficerID != Guid.Empty)
                {
                    EmployeeDB edb = EmployeeDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId, guidDefaultAssignLoanOfficerID);
                    if (edb != null)
                    {
                        sBranchId = edb.BranchID;
                    }
                }

                // Use Principal BranchId as last resort
                if (sBranchId == Guid.Empty)
                {
                    sBranchId = BrokerUserPrincipal.CurrentPrincipal.BranchId;
                }

                try
                {
                    if (willBecomeLead)
                    {
                        dataLoan = FannieMaeImporter.ImportIntoLead(BrokerUserPrincipal.CurrentPrincipal, sBranchId, stream, FannieMaeImportSource.LendersOffice, false, templateId, out warnings);
                    }
                    else
                    {
                        dataLoan = FannieMaeImporter.Import(BrokerUserPrincipal.CurrentPrincipal, sBranchId, stream, FannieMaeImportSource.LendersOffice, false, templateId, out warnings);
                    }
                }
                catch (InvalidFannieFileFormatException e)
                {
                    this.SetResult("ErrorMessage", e.UserMessage);
                    return;
                }
             
				dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                #region Set Default Import Settings
                // Set Default Import Settings
                if (willBecomeLead == false)
                {
                    var newStatus = (E_sStatusT)GetInt("sStatusT");

                    if (ReturnEarlyDueToBadChannelAndStatus(dataLoan.sBranchChannelT, dataLoan.sCorrespondentProcessT, newStatus))
                    {
                        return;
                    }
                    dataLoan.sStatusT = newStatus;
                    dataLoan.sStatusLckd = true;
                }

                // OPM 9831 - there is now a new setting called "Use LO Settings" which will populate the
                // Manager and Lender Account Exec based on the Loan Officer's relationships, not the current
                // importing user's relationships.  This setting is selected by default.
                if (GetString("sEmployeeManagerId") == "Current")
                {
                    guidDefaultAssignManagerID = m_guidAutoId;
                }
                if (GetString("sEmployeeLenderAccExecId") == "Current")
                {
                    guidDefaultAssignLenderAccountExecID = m_guidAutoId;
                }

                if (GetString("sEmployeeProcessorId") == "Current")
                {
                    guidDefaultAssignLoanProcessorID = m_guidAutoId;
                } 

                dataLoan.sEmployeeLoanRepId = guidDefaultAssignLoanOfficerID;
                //dataLoan.sEmployeeProcessorId = guidDefaultAssignLoanProcessorID;
                dataLoan.sEmployeeLoanOpenerId = guidDefaultAssignLoanOpenerID;
                //dataLoan.sEmployeeManagerId = guidDefaultAssignManagerID;
                dataLoan.sEmployeeCallCenterAgentId = guidDefaultAssignCallCenterAgentID;
                dataLoan.sEmployeeRealEstateAgentId = guidDefaultAssignRealEstateAgentID;
                //dataLoan.sEmployeeLenderAccExecId = guidDefaultAssignLenderAccountExecID;
                dataLoan.sEmployeeLockDeskId = guidDefaultAssignLockDescId;
                dataLoan.sEmployeeCloserId = guidDefaultAssignCloserId;                

                // OPM 9831 - use the manager relationship of the assigned loan officer
                if (guidDefaultAssignManagerID == new Guid("11111111-1111-1111-1111-111111111111"))
                {
                    Guid matchedManagerId = GetRelationshipOfGivenUser(guidDefaultAssignLoanOfficerID, E_RoleT.Manager);
                    dataLoan.sEmployeeManagerId = matchedManagerId;
                    guidDefaultAssignManagerID = matchedManagerId;
                }
                else
                {
                    dataLoan.sEmployeeManagerId = guidDefaultAssignManagerID;
                }

                // OPM 9831 - use the lender account exec relationship of the assigned loan officer
                if (guidDefaultAssignLenderAccountExecID == new Guid("11111111-1111-1111-1111-111111111111"))
                {
                    Guid matchingLenderAEID = GetRelationshipOfGivenUser(guidDefaultAssignLoanOfficerID, E_RoleT.LenderAccountExecutive);
                    dataLoan.sEmployeeLenderAccExecId = matchingLenderAEID;
                    guidDefaultAssignLenderAccountExecID = matchingLenderAEID;
                }
                else
                {
                    dataLoan.sEmployeeLenderAccExecId = guidDefaultAssignLenderAccountExecID;
                }

                // OPM 9831 - use the processor relationship of the assigned loan officer
                if (guidDefaultAssignLoanProcessorID == new Guid("11111111-1111-1111-1111-111111111111"))
                {
                    Guid matchingProcessorID = GetRelationshipOfGivenUser(guidDefaultAssignLoanOfficerID, E_RoleT.Processor);
                    dataLoan.sEmployeeProcessorId = matchingProcessorID;
                    guidDefaultAssignLoanProcessorID = matchingProcessorID;
                }
                else
                {
                    dataLoan.sEmployeeProcessorId = guidDefaultAssignLoanProcessorID;
                }                

                #region 52142 - update 1003 preparer
                var preprarer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                preprarer.IsLocked = !(addEmployeeAsOfficialAgent && guidDefaultAssignLoanOfficerID != Guid.Empty);
                preprarer.Update();
                #endregion 

                //OPM 105183: set status date for selected sStatusT using datetime of import
                string date = DateTime.Now.ToShortDateString();
                switch(status)
                {
                    case E_sStatusT.Lead_New:
                        dataLoan.sLeadD_rep = date;
                        break;
                    case E_sStatusT.Loan_Open:
                        dataLoan.sOpenedD_rep = date;
                        break;
                    case E_sStatusT.Loan_Prequal:
                        dataLoan.sPreQualD_rep = date;
                        break;
                    case E_sStatusT.Loan_Registered:
                        dataLoan.sSubmitD_rep = date;
                        break;
                    case E_sStatusT.Loan_Processing:
                        dataLoan.sProcessingD_rep = date;
                        break;
                    case E_sStatusT.Loan_Preapproval:
                        dataLoan.sPreApprovD_rep = date;
                        break;
                    case E_sStatusT.Loan_LoanSubmitted:
                        dataLoan.sLoanSubmittedD_rep = date;
                        break;
                    case E_sStatusT.Loan_Underwriting:
                        dataLoan.sUnderwritingD_rep = date;
                        break;
                    case E_sStatusT.Loan_Approved:
                        dataLoan.sApprovD_rep = date;
                        break;
                    case E_sStatusT.Loan_FinalUnderwriting:
                        dataLoan.sFinalUnderwritingD_rep = date;
                        break;
                    case E_sStatusT.Loan_ClearToClose:
                        dataLoan.sClearToCloseD_rep = date;
                        break;
                    case E_sStatusT.Loan_Docs:
                        dataLoan.sDocsD_rep = date;
                        break;
                    case E_sStatusT.Loan_DocsBack:
                        dataLoan.sDocsBackD_rep = date;
                        break;
                    case E_sStatusT.Loan_FundingConditions:
                        dataLoan.sFundingConditionsD_rep = date;
                        break;
                    case E_sStatusT.Loan_Funded:
                        dataLoan.sFundD_rep = date;
                        break;
                    case E_sStatusT.Loan_Recorded:
                        dataLoan.sRecordedD_rep = date;
                        break;
                    case E_sStatusT.Loan_FinalDocs:
                        dataLoan.sFinalDocsD_rep = date;
                        break;
                    case E_sStatusT.Loan_Closed:
                        dataLoan.sClosedD_rep = date;
                        break;
                    case E_sStatusT.Loan_OnHold:
                        dataLoan.sOnHoldD_rep = date;
                        break;
                    case E_sStatusT.Loan_Canceled:
                        dataLoan.sCanceledD_rep = date;
                        break;
                    case E_sStatusT.Loan_Rejected:
                        dataLoan.sRejectD_rep = date;
                        break;
                    case E_sStatusT.Loan_Suspended:
                        dataLoan.sSuspendedD_rep = date;
                        break;
                    case E_sStatusT.Loan_LoanPurchased:
                        dataLoan.sLPurchaseD_rep = date;
                        break;
                    case E_sStatusT.Loan_Shipped:
                        dataLoan.sShippedToInvestorD_rep = date;
                        break;
                    case E_sStatusT.Loan_PreProcessing:    // start sk 1/6/2014 opm 145251
                        dataLoan.sPreProcessingD_rep = date;
                        break;
                    case E_sStatusT.Loan_DocumentCheck:
                        dataLoan.sDocumentCheckD_rep = date;
                        break;
                    case E_sStatusT.Loan_DocumentCheckFailed:
                        dataLoan.sDocumentCheckFailedD_rep = date;
                        break;
                    case E_sStatusT.Loan_PreUnderwriting:
                        dataLoan.sPreUnderwritingD_rep = date;
                        break;
                    case E_sStatusT.Loan_ConditionReview:
                        dataLoan.sConditionReviewD_rep = date;
                        break;
                    case E_sStatusT.Loan_PreDocQC:
                        dataLoan.sPreDocQCD_rep = date;
                        break;
                    case E_sStatusT.Loan_DocsOrdered:
                        dataLoan.sDocsOrderedD_rep = date;
                        break;
                    case E_sStatusT.Loan_DocsDrawn:
                        dataLoan.sDocsDrawnD_rep = date;
                        break;
                    case E_sStatusT.Loan_InvestorConditions:       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                        dataLoan.sSuspendedByInvestorD_rep = date;
                        break;
                    case E_sStatusT.Loan_InvestorConditionsSent:   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                        dataLoan.sCondSentToInvestorD_rep = date;
                        break;
                    case E_sStatusT.Loan_ReadyForSale:
                        dataLoan.sReadyForSaleD_rep = date;
                        break;
                    case E_sStatusT.Loan_SubmittedForPurchaseReview:
                        dataLoan.sSubmittedForPurchaseReviewD_rep = date;
                        break;
                    case E_sStatusT.Loan_InPurchaseReview:
                        dataLoan.sInPurchaseReviewD_rep = date;
                        break;
                    case E_sStatusT.Loan_PrePurchaseConditions:
                        dataLoan.sPrePurchaseConditionsD_rep = date;
                        break;
                    case E_sStatusT.Loan_SubmittedForFinalPurchaseReview:
                        dataLoan.sSubmittedForFinalPurchaseD_rep = date;
                        break;
                    case E_sStatusT.Loan_InFinalPurchaseReview:
                        dataLoan.sInFinalPurchaseReviewD_rep = date;
                        break;
                    case E_sStatusT.Loan_ClearToPurchase:
                        dataLoan.sClearToPurchaseD_rep = date;
                        break;
                    case E_sStatusT.Loan_Purchased:        // don't confuse with the old case E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
                        dataLoan.sPurchasedD_rep = date;
                        break;
                    case E_sStatusT.Loan_CounterOffer:
                        dataLoan.sCounterOfferD_rep = date;
                        break;
                    case E_sStatusT.Loan_Withdrawn:
                        dataLoan.sWithdrawnD_rep = date;
                        break;
                    case E_sStatusT.Loan_Archived:
                        dataLoan.sArchivedD_rep = date;
                        break;

                }

                dataLoan.Save();
				#endregion

                if (addEmployeeAsOfficialAgent)
                {
                    AddEmployeesAsOfficialContact(dataLoan.sLId,
                        new List<Guid> { guidDefaultAssignLoanOfficerID, guidDefaultAssignLoanProcessorID, guidDefaultAssignLoanOpenerID, guidDefaultAssignManagerID, guidDefaultAssignCallCenterAgentID, guidDefaultAssignRealEstateAgentID, guidDefaultAssignLenderAccountExecID },
                        new List<E_AgentRoleT> { E_AgentRoleT.LoanOfficer, E_AgentRoleT.Processor, E_AgentRoleT.LoanOpener, E_AgentRoleT.Manager, E_AgentRoleT.CallCenterAgent, E_AgentRoleT.Realtor, E_AgentRoleT.Lender});
                    /*Update 7/28/10 - we won't be adding a lock desk official contact type.  Obsolete - Add the lockdesk role after lockdesk contact type is created in Case 22907*/
                }

                

				Tools.NotifyImportAssignees( dataLoan.sLId ); 
            
				
				if( warnings != string.Empty ) 
				{
					SetResult("Warning", warnings );
				}

				SetResult("Status", warnings == string.Empty ? "OK" : "WARN" );
                SetResult("NewLoanId", dataLoan.sLId);
				SetResult("LoanName", dataLoan.sLNm );

            } 
            catch (CBaseException exc) 
            {
                Tools.LogErrorWithCriticalTracking(exc);
                SetResult("Status", "Error");
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                SetResult("Status", "Error");
                SetResult("ErrorMessage", exc.Message);
            } 
        }

        /// <summary>
        /// Only return PmlLoanTemplateId If User is AE Only
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        private Guid RetrieveLoanTemplateID(BrokerUserPrincipal principal) 
        {
            Guid templateId = GetGuid("sTemplateId", Guid.Empty);

            if (templateId == Guid.Empty && principal.IsAccountExecutiveOnly) 
            {
                // 9/30/2008 dd - If user is AE only and a loan template did not select then use the default company loan template.
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);

                templateId = brokerDB.PmlLoanTemplateID;
            }
            return templateId;

        }
        private NameValueCollectionWrapper GetPointFile(string name) 
        {
            Stream stream = GetBase64File(name);
            PointInteropLib.DataFile reader = new PointInteropLib.DataFile();
            NameValueCollection colInput = reader.ReadStream(stream);
            NameValueCollectionWrapper input = new NameValueCollectionWrapper(colInput);
            return input;
        }
        
        /// <summary>
        /// Add assigned employees to official loan contacts. OPM 33815
        /// </summary>
        /// <param name="loanId"></param>
        /// <param name="employeeIds"></param>
        /// <param name="roles"></param>
        private void AddEmployeesAsOfficialContact(Guid loanId, List<Guid> employeeIds, List<E_AgentRoleT> roles)
        {
            if (employeeIds.Count != roles.Count)
                return;

            COfficialAgentData loanData = new COfficialAgentData(loanId);
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);

            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;

            for (int i=0; i < employeeIds.Count; i++)
            {
                Guid employeeId = employeeIds[i];

                if (employeeId == Guid.Empty || employeeId == m_guidAutoId)
                    continue;

                E_AgentRoleT roleCode = roles[i];
                CAgentFields agent = loanData.GetAgentOfRole(roleCode, E_ReturnOptionIfNotExist.CreateNew);

                CommonFunctions.CopyEmployeeInfoToAgent(brokerId, agent, employeeId);
                agent.Update();
            }
            loanData.Save();
        }

        private void ImportCalyx() 
        {
            try 
            {
				
                bool addAssignedEmployeesToOfficialContact = GetBool("sAddEmpAsAgent", false); 

				Guid employeeID = BrokerUserPrincipal.CurrentPrincipal.EmployeeId;
                Guid templateId = RetrieveLoanTemplateID(BrokerUserPrincipal.CurrentPrincipal);

                Guid guidDefaultAssignLoanOfficerID = GetGuid("sEmployeeLoanRepId", Guid.Empty);
                Guid guidDefaultAssignLoanProcessorID = GetGuid("sEmployeeProcessorId", Guid.Empty);
                Guid guidDefaultAssignLoanOpenerID = GetGuid("sEmployeeLoanOpenerId", Guid.Empty);
                Guid guidDefaultAssignManagerID = GetGuid("sEmployeeManagerId", Guid.Empty);
                Guid guidDefaultAssignCallCenterAgentID = GetGuid("sEmployeeCallCenterAgentId", Guid.Empty);
                Guid guidDefaultAssignRealEstateAgentID = GetGuid("sEmployeeRealEstateAgentId", Guid.Empty);
                Guid guidDefaultAssignLenderAccountExecID = GetGuid("sEmployeeLenderAccExecId", Guid.Empty);
                Guid guidDefaultAssignLockDescId = GetGuid("sEmployeeLockDeskId", Guid.Empty);
                Guid guidDefaultAssignCloserId = GetGuid("sEmployeeCloserId", Guid.Empty);

                // OPM 188399 - Determine which BranchId to use for loan (need to be deturmined before loan creation
                // or may have issue with loan name/MERS MIN number in certain cases
                // Get BranchId from first available source in list:
                //  1. Assigned Loan Officer
                //  2. Branch Drop Down List (sBranchId)
                //  3. Principal
                Guid sBranchId = GetGuid("sBranchId", Guid.Empty);  // BranchId from drop down

                // Clobber BranchId if a valid loan officer is assigned
                if (guidDefaultAssignLoanOfficerID != Guid.Empty)
                {
                    EmployeeDB edb = EmployeeDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId, guidDefaultAssignLoanOfficerID);
                    if (edb != null)
                    {
                        sBranchId = edb.BranchID;
                    }
                }

                // Use Principal BranchId as last resort
                if (sBranchId == Guid.Empty)
                {
                    sBranchId = BrokerUserPrincipal.CurrentPrincipal.BranchId;
                }

                // 6/27/2017 dt - Note that we currently throw away the BranchId we
                // calculate above and use the one from the principal if not creating
                // from a template. We should probably use the calculated branch
                // regardless, but leaving the existing behavior alone for now.
                CLoanFileCreator creator = CLoanFileCreator.GetCreator(BrokerUserPrincipal.CurrentPrincipal, LendersOffice.Audit.E_LoanCreationSource.PointImport );

                Guid sLId = creator.BeginCreateImportBaseLoanFile(
                    sourceFileId: templateId,
                    setInitialEmployeeRoles: false,
                    addEmployeeOfficialAgent: false,
                    assignEmployeesFromRelationships: false,
                    branchIdToUse: sBranchId);

                PointDataConversion pointConversion = new PointDataConversion(BrokerUserPrincipal.CurrentPrincipal.BrokerId, sLId);

                if (GetString("sEmployeeProcessorId") == "Current")
                {
                    guidDefaultAssignLoanProcessorID = m_guidAutoId;                    
                }

                if (GetString("sEmployeeLoanRepId") == "Current")
                {
                    guidDefaultAssignLoanOfficerID = m_guidAutoId;                    
                }  
             
                // OPM 9831 - there is now a new setting called "Use LO Settings" which will populate the
                // Manager and Lender Account Exec based on the Loan Officer's relationships, not the current
                // importing user's relationships.  This setting is selected by default.
                if (GetString("sEmployeeManagerId") == "Current")
                {
                    guidDefaultAssignManagerID = m_guidAutoId;
                }
                if (GetString("sEmployeeLenderAccExecId") == "Current")
                {
                    guidDefaultAssignLenderAccountExecID = m_guidAutoId;
                }  

                CPointData dataLoan = new CImportPointData(sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                NameValueCollectionWrapper input = GetPointFile("File");                

                #region Set Default Import Settings				

                pointConversion.IsRolesImportedAsAgents = addAssignedEmployeesToOfficialContact;

                pointConversion.DefaultAssignLoanOfficerID = guidDefaultAssignLoanOfficerID;
                pointConversion.DefaultAssignLoanProcessorID = guidDefaultAssignLoanProcessorID;
                pointConversion.DefaultAssignLoanOpenerID = guidDefaultAssignLoanOpenerID;
                pointConversion.DefaultAssignManagerID = guidDefaultAssignManagerID;
                pointConversion.DefaultAssignCallCenterAgentID = guidDefaultAssignCallCenterAgentID;
                pointConversion.DefaultAssignRealEstateAgentID = guidDefaultAssignRealEstateAgentID;
                pointConversion.DefaultAssignLenderAccountExecID = guidDefaultAssignLenderAccountExecID;
                //opm 21891 av 05/01/08 
                pointConversion.DefaultAssignLockDescId = guidDefaultAssignLockDescId;
                pointConversion.DefaultAssignCloserID = guidDefaultAssignCloserId;
                

//                dataLoan.sEmployeeLockDeskId = GetGuid("sEmployeeLockDeskId");

                #endregion
                pointConversion.TransferDataFromPoint(dataLoan, input, 0);

                #region Coborrower Files
                int coborrowerCount = GetInt("CoborrowerCount", 0);
                for (int i = 0; i < coborrowerCount; i++) 
                {
                    NameValueCollectionWrapper coInput = GetPointFile("File" + i);

                    dataLoan.SetsLNmWithPermissionBypass(creator.LoanName);
                    dataLoan.Save();
                    int iNewApp = dataLoan.AddNewApp();
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    pointConversion.TransferDataFromPoint(dataLoan, coInput, iNewApp);

                }
                #endregion

                dataLoan.SetsLNmWithPermissionBypass(creator.LoanName);
                dataLoan.Save();
				creator.CommitFileCreation( false /* refreshLoanTasks */ );


                if (addAssignedEmployeesToOfficialContact)
                {
                    if (guidDefaultAssignLoanOfficerID == m_guidAutoId) guidDefaultAssignLoanOfficerID = pointConversion.DefaultAssignLoanOfficerID;
                    if (guidDefaultAssignLoanProcessorID == m_guidAutoId) guidDefaultAssignLoanProcessorID = pointConversion.DefaultAssignLoanProcessorID;
                    if (guidDefaultAssignManagerID == m_guidAutoId) guidDefaultAssignManagerID = pointConversion.DefaultAssignManagerID;
                    if (guidDefaultAssignLenderAccountExecID == m_guidAutoId) guidDefaultAssignLenderAccountExecID = pointConversion.DefaultAssignLenderAccountExecID;

                    AddEmployeesAsOfficialContact(sLId,
                        new List<Guid> { guidDefaultAssignLoanOfficerID, guidDefaultAssignLoanProcessorID, guidDefaultAssignLoanOpenerID, guidDefaultAssignManagerID, guidDefaultAssignCallCenterAgentID, guidDefaultAssignRealEstateAgentID, guidDefaultAssignLenderAccountExecID },
                        new List<E_AgentRoleT> { E_AgentRoleT.LoanOfficer, E_AgentRoleT.Processor, E_AgentRoleT.LoanOpener, E_AgentRoleT.Manager, E_AgentRoleT.CallCenterAgent, E_AgentRoleT.Realtor, E_AgentRoleT.Lender });
                    /*Update 7/28/10 - we won't be adding a lock desk official contact type.  Obsolete - Add the lockdesk role after lockdesk contact type is created in Case 22907*/
                }

				Tools.NotifyImportAssignees( dataLoan.sLId );
                TaskUtilities.EnqueueTasksDueDateUpdate(dataLoan.sBrokerId, dataLoan.sLId);
                SetResult("Status", "OK");
                SetResult("NewLoanId", dataLoan.sLId);
				SetResult("LoanName", dataLoan.sLNm );

            } 
            catch (CBaseException exc) 
            {
                Tools.LogErrorWithCriticalTracking(exc);
                SetResult("Status", "Error");
                SetResult("ErrorMessage", exc.UserMessage);
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                SetResult("Status", "Error");
                SetResult("ErrorMessage", exc.Message);
            } 

        }

        private bool ReturnEarlyDueToBadChannelAndStatus(E_BranchChannelT channel, E_sCorrespondentProcessT process, E_sStatusT newStatus)
        {
            var broker = BrokerDB.RetrieveById(BrokerUser.BrokerId);
            if (!broker.GetEnabledStatusesByChannelAndProcessType(channel, process).Contains(newStatus))
            {
                var msg = CPageBase.sStatusT_map_rep(newStatus)
                                + " is not enabled for your organization for the "
                                + Enum.GetName(typeof(E_BranchChannelT), channel)
                                + " channel";

                // OPM 185961 - Add Correspondent sub channels.
                if (channel == E_BranchChannelT.Correspondent)
                {
                    msg += " and the " + Enum.GetName(typeof(E_sCorrespondentProcessT), process) + " process.";
                }
                else
                {
                    msg += ".";
                }

                SetResult("Status", "Error");
                SetResult("ErrorMessage", msg);
                return true;
            }
            return false;
        }

        private void ImportDoDu() 
        {
            string globallyUniqueIdentifier = "";
            string errorMessage = "";

            bool rememberLogin = GetBool("RememberDoLogin", false);
            string fannieMaeMornetUserId = GetString("FannieMaeMORNETUserID", "");
            string fannieMaeMornetPassword = GetString("FannieMaeMORNETPassword", "");
            string sDuCaseId = GetString("sDuCaseId", "");
            bool isImportCredit = GetBool("IsImportCreditDo", false);

            // 4/9/2008 dd - Save the login name to both DO and DU account. The reason is when we import we do not know if user enter DO or DU account.
            string rememberLoginNm = rememberLogin ? fannieMaeMornetUserId : "";

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", BrokerUserPrincipal.CurrentPrincipal.UserId),
                                            new SqlParameter("@DOLastLoginNm", rememberLoginNm), 
                                            new SqlParameter("@DULastLoginNm", rememberLoginNm) 
                                        };
            StoredProcedureHelper.ExecuteNonQuery(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "UpdateLastDoDuLpLoginNm", 3, parameters);

            var newStatus = (E_sStatusT)GetInt("sStatusT");
            Guid templateId = RetrieveLoanTemplateID(BrokerUserPrincipal.CurrentPrincipal);
            var channel = E_BranchChannelT.Blank;
            var process = E_sCorrespondentProcessT.Blank;
            if (Guid.Empty != templateId)
            {
                var templateLoan = new CFullAccessPageData(templateId, new string[] { "sBranchChannelT", "sCorrespondentProcessT" });
                templateLoan.InitLoad();
                channel = templateLoan.sBranchChannelT;
                process = templateLoan.sCorrespondentProcessT;
            }

            if (ReturnEarlyDueToBadChannelAndStatus(channel, process, newStatus))
            {
                return;
            }

            try 
            {
                bool isDone = false;
                while (!isDone) 
                {
                    AbstractFnmaXisRequest request = CreateCasefileExportRequest(fannieMaeMornetUserId, fannieMaeMornetPassword, globallyUniqueIdentifier, sDuCaseId);
                    AbstractFnmaXisResponse response = DUServer.Submit(request);
                    if (response.IsReady) 
                    {
                        isDone = true;
                        if (response.HasError) 
                        {
                            errorMessage = response.ErrorMessage;

                        } 
                        else if (response.HasBusinessError) 
                        {
                            errorMessage = response.BusinessErrorMessage + "<br><pre>" + response.FnmaStatusLog + "</pre>";
                        } 
                        else 
                        {
                            Guid sLId = SaveCasefileExportResponse((FnmaXisCasefileExportResponse) response, isImportCredit);

                            SetResult("Status", "OK");
                            SetResult("NewLoanId", sLId);
                        }
                    }
                    globallyUniqueIdentifier = response.GloballyUniqueIdentifier;
                }
            } 
            catch (CBaseException exc)
            {
                errorMessage = exc.UserMessage;
                Tools.LogErrorWithCriticalTracking("Unable to import to LO from DU", exc);
            } 
            catch (Exception exc) 
            {
                errorMessage = ErrorMessages.Generic;
                Tools.LogErrorWithCriticalTracking("Unable to import to LO from DU", exc);
            }

            if ("" != errorMessage) 
            {
                SetResult("Status", "Error");
                SetResult("ErrorMessage", errorMessage);
            }

        }
        private FnmaXisCasefileExportRequest CreateCasefileExportRequest(string fannieMaeMornetUserId, string fannieMaeMornetPassword, string globallyUniqueIdentifier, string sDuCaseId) 
        {
            FnmaXisCasefileExportRequest request = new FnmaXisCasefileExportRequest(Guid.Empty);
            request.FannieMaeMORNETUserID = fannieMaeMornetUserId;
            request.FannieMaeMORNETPassword = fannieMaeMornetPassword;
            request.GloballyUniqueIdentifier = globallyUniqueIdentifier;
            request.MornetPlusCasefileIdentifier = sDuCaseId;

            return request;
        }
        private Guid SaveCasefileExportResponse(FnmaXisCasefileExportResponse response, bool isImportCreditReport) 
        {

            Guid loanID = Guid.Empty;
            Guid templateId = RetrieveLoanTemplateID(BrokerUserPrincipal.CurrentPrincipal);

            Guid guidDefaultAssignLoanOfficerID = GetGuid("sEmployeeLoanRepId", Guid.Empty);
            Guid guidDefaultAssignLoanProcessorID = GetGuid("sEmployeeProcessorId", Guid.Empty);
            Guid guidDefaultAssignLoanOpenerID = GetGuid("sEmployeeLoanOpenerId", Guid.Empty);
            Guid guidDefaultAssignManagerID = GetGuid("sEmployeeManagerId", Guid.Empty);
            Guid guidDefaultAssignCallCenterAgentID = GetGuid("sEmployeeCallCenterAgentId", Guid.Empty);
            Guid guidDefaultAssignRealEstateAgentID = GetGuid("sEmployeeRealEstateAgentId", Guid.Empty);
            Guid guidDefaultAssignLenderAccountExecID = GetGuid("sEmployeeLenderAccExecId", Guid.Empty);
            Guid guidDefaultAssignLockDescId = GetGuid("sEmployeeLockDeskId", Guid.Empty);
            Guid guidDefaultAssignCloserId = GetGuid("sEmployeeCloserId", Guid.Empty);

            // OPM 188399 - Determine which BranchId to use for loan (need to be deturmined before loan creation
            // or may have issue with loan name/MERS MIN number in certain cases
            // Get BranchId from first available source in list:
            //  1. Assigned Loan Officer
            //  2. Branch Drop Down List (sBranchId)
            //  3. Principal
            Guid sBranchId = GetGuid("sBranchId", Guid.Empty);  // BranchId from drop down

            // Clobber BranchId if a valid loan officer is assigned
            if (guidDefaultAssignLoanOfficerID != Guid.Empty)
            {
                EmployeeDB edb = EmployeeDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId, guidDefaultAssignLoanOfficerID);
                if (edb != null)
                {
                    sBranchId = edb.BranchID;
                }
            }

            // Use Principal BranchId as last resort
            if (sBranchId == Guid.Empty)
            {
                sBranchId = BrokerUserPrincipal.CurrentPrincipal.BranchId;
            }

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(BrokerUserPrincipal.CurrentPrincipal, sBranchId, LendersOffice.Audit.E_LoanCreationSource.ImportFromDoDu);

            loanID = creator.BeginCreateImportBaseLoanFile(
                sourceFileId: templateId,
                setInitialEmployeeRoles: true,
                addEmployeeOfficialAgent: true,
                assignEmployeesFromRelationships: true,
                branchIdToUse: sBranchId);

            creator.CommitFileCreation(false);

            response.ImportToLoanFile(loanID, BrokerUserPrincipal.CurrentPrincipal, true /* isImport1003 */, true /* isImportDuFindings */, isImportCreditReport);

            CPageData dataLoan = new CFannieMae32ImporterData(loanID);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            #region Set Default Import Settings
            // Set Default Import Settings
            var newStatus = (E_sStatusT)GetInt("sStatusT");
            
            dataLoan.sStatusT = newStatus;
            dataLoan.sStatusLckd = true;

            dataLoan.sEmployeeLoanRepId = guidDefaultAssignLoanOfficerID;
            dataLoan.sEmployeeProcessorId = guidDefaultAssignLoanProcessorID;
            dataLoan.sEmployeeLoanOpenerId = guidDefaultAssignLoanOpenerID;
            dataLoan.sEmployeeManagerId = guidDefaultAssignManagerID;
            dataLoan.sEmployeeCallCenterAgentId = guidDefaultAssignCallCenterAgentID;
            dataLoan.sEmployeeRealEstateAgentId = guidDefaultAssignRealEstateAgentID;
            dataLoan.sEmployeeLenderAccExecId = guidDefaultAssignLenderAccountExecID;
            dataLoan.sEmployeeLockDeskId = guidDefaultAssignLockDescId;
            dataLoan.sEmployeeCloserId = guidDefaultAssignCloserId;
            
            dataLoan.Save();
            TaskUtilities.EnqueueTasksDueDateUpdate(dataLoan.sBrokerId, dataLoan.sLId);
                    #endregion
            return loanID;
        }


	}
}
