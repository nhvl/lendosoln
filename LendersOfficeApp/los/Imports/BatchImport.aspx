<%@ Register TagPrefix="uc1" TagName="cModalDlg" Src="../../common/ModalDlg/cModalDlg.ascx" %>
<%@ Register TagPrefix="ils" TagName="EmployeeRoleChooserLink" Src="../common/EmployeeRoleChooserLink.ascx" %>
<%@ Page language="c#" Codebehind="BatchImport.aspx.cs" AutoEventWireup="false" Inherits="LendersOfficeApp.los.Imports.BatchImport" enableViewState="False"%>
<%@ Import namespace="LendersOffice.Common" %>
<%@ Import Namespace="LendersOffice.AntiXss" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      		<title>BatchImport</title>
		<link href=<%= AspxTools.SafeUrl(StyleSheet) %> type="text/css" rel="stylesheet">
		<style type="text/css">
			.PrintFrameStyle { BORDER-RIGHT: 2px groove; PADDING-RIGHT: 2px; BORDER-TOP: 2px groove; OVERFLOW-Y: scroll; PADDING-LEFT: 2px; PADDING-BOTTOM: 2px; BORDER-LEFT: 2px groove; PADDING-TOP: 2px; BORDER-BOTTOM: 2px groove; flow: y }
			body {
                min-width: 770px;
			}

            html {
                overflow: auto;
            }

            .full-width {
                width: calc(100% - 10px);
                margin-left: 3px;
                margin-right: 3px;
            }

            .no-margin {
                margin-left: 0;
                margin-right: 0;
                width: 100%;
            }

            </style>
</HEAD>
	<body class="RightBackground" style="MARGIN-LEFT: 0px" scroll="no">
		<script type="text/javascript">
window.onload = function (){
    g_oRegEx = new RegExp(ML.m_fileFilter, 'i'); // /.brw$/i;
};
var g_oRegEx;
var g_oFileExtensions = <%=AspxTools.JsString(m_fileExtensions)%>;
		    var g_sImportFunction = <%=AspxTools.JsString(m_importFunction)%>;
		    var g_iFileCount = 0;
var g_iSuccessCount = 0;
var g_isWarningCount = 0;
var g_iErrorCount = 0;
var g_iSkipCount = 0;
var g_sPath = "";
var COLSPAN =  4;
var g_bImport = false;
var COL_0_WIDTH = "40px";
var COL_1_WIDTH = "65%";
var COL_2_WIDTH = "15%";
var COL_3_WIDTH = "10%";
var TIME_DELAY = 100; // Delay each import file at 200 ms.

function f_resetCounters() {
  g_iSuccessCount = 0;
  g_iErrorCount = 0;
  g_iSkipCount = 0;
  g_isWarningCount = 0;
}

		    function f_uploadFile(file, base64File, args) {

		        if (g_bImport) {
		            f_writeMessage(file.index, "In Progress...");
		            args = args ? args : new Object();
		            args["File"] = base64File;
		            args["FileName"] = file.name;

		            f_getDefaultImportsSettings(args);

		            var result = gService.main.callAsyncSimple(g_sImportFunction, args, function(result) {
		                if (!result.error) {
		                    if (result.value["Status"] == "OK") {
		                        g_iSuccessCount++;
		                        f_writeSuccessful(file.index, "Successful" );

		                        if ( result.value && result.value["LoanName"] )
		                        {
		                            f_writeName(file.index, result.value["LoanName"] );
		                        }
		                    }
		                    else if ( result.value["Status"] == "WARN" )
		                    {
		                        g_isWarningCount++;
		                        f_writeWarning(file.index, result.value["Warning"]);
		                        if ( result.value && result.value["LoanName"] )
		                        {
		                            f_writeName(file.index, result.value["LoanName"] );
		                        }
		                    }
		                    else {
		                        g_iErrorCount++;
		                        f_writeError(file.index, result.value["ErrorMessage"]);
		                        f_writeName(file.index, "N/A");
		                    }
		                } else {
		                    g_iErrorCount++;
		                    f_writeError(file.index, "Unexpected error encounter.");
		                }

                        checkUploadComplete();
		            });
		        }
		        else {
		            g_iSkipCount++;
		            f_writeMessage(file.index, "SKIP");
		            checkUploadComplete();
		        }
		    }

		    function checkUploadComplete() {
                if(g_iFileCount <= g_iSuccessCount + g_iSkipCount + g_iErrorCount + g_isWarningCount) {
		            document.getElementById("btnStartImport").disabled = getFilesCount("DragDropZone") == 0;
		            document.getElementById("btnClose").disabled = false;
		            f_displayCompleteMessage();
		        }
		    }

<% if (m_isFnmaImport)
            { %>
function f_writeFnmaMsg(msg, color) {
  var oTable = document.getElementById("FilesListingTable");
  f_clearTable(oTable);
  var oRow = oTable.insertRow();
  var oCell = oRow.insertCell();
  oCell.setAttribute("align", "center");
  oCell.className = "FieldLabel";
  oCell.colSpan = COLSPAN;
  oCell.style.height = "40";
  oCell.style.color = color;
  oCell.innerHTML = msg;
}

function f_switchDataSource() {

  if (document.getElementById('rbDataSourceFiles').checked) {
    <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.readOnly = true;
    <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.readOnly = true;
    <%= AspxTools.JsGetElementById(sDuCaseId) %>.readOnly = true;
    <%= AspxTools.JsGetElementById(RememberDoLogin) %>.disabled = true;
    <%= AspxTools.JsGetElementById(IsImportCreditDo) %>.disabled= true;

    document.getElementById('btnStartImport').disabled =true;
    document.getElementById('required0').style.display = 'none';
    document.getElementById('required1').style.display = 'none';
    document.getElementById('required2').style.display = 'none';
    document.getElementById("DragDropZoneContainer").classList.remove("Hidden");
    document.getElementById("dodupanel").classList.add("Hidden");
  } else if (document.getElementById('rbDataSourceDo').checked) {
  	f_constructInitTable();
    <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.readOnly = false;
    <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.readOnly = false;
    <%= AspxTools.JsGetElementById(sDuCaseId) %>.readOnly = false;
    <%= AspxTools.JsGetElementById(RememberDoLogin) %>.disabled = false;
    <%= AspxTools.JsGetElementById(IsImportCreditDo) %>.disabled= false;
    document.getElementById('required0').style.display = '';
    document.getElementById('required1').style.display = '';
    document.getElementById('required2').style.display = '';

    document.getElementById("DragDropZoneContainer").classList.add("Hidden");
    document.getElementById("dodupanel").classList.remove("Hidden");
    document.getElementById('btnStartImport').disabled =g_isTemplateValid;

  }
  f_toggleImportButtons();
}
function f_importDoDu() {

  var args = new Object();
  args["FannieMaeMORNETUserID"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value;
  args["FannieMaeMORNETPassword"] = <%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.value;
  args["sDuCaseId"] = <%= AspxTools.JsGetElementById(sDuCaseId) %>.value;
  args["RememberDoLogin"] = <%= AspxTools.JsGetElementById(RememberDoLogin) %>.checked ? 'true' : 'false';
  args["IsImportCreditDo"] = <%= AspxTools.JsGetElementById(IsImportCreditDo) %>.checked ? 'true' : 'false';
  f_getDefaultImportsSettings(args);
  var result = gService.main.call("ImportDoDu", args);
  if (!result.error) {
    if (result.value["Status"] == "OK") {
        f_writeFnmaMsg("Imported successfully.", "green");
    } else {
      f_writeFnmaMsg(result.value["ErrorMessage"], "red");
    }
  } else {
      f_writeFnmaMsg("Unexpected error encountered.", "red");
  }
      document.getElementById("btnStartImport").disabled = false;
      document.getElementById("btnClose").disabled = false;

}
<% } %>
function f_getDefaultImportsSettings(args) {

  args["sStatusT"] = <%= AspxTools.JsGetElementById(sStatusT) %>.value;
  args["sBranchId"] = <%= AspxTools.JsGetElementById(sBranchId) %>.value;
  args["sEmployeeLoanRepId"] = document.getElementById(<%=AspxTools.JsString(sEmployeeLoanRepChoice.DataID)%>).value;
  args["sEmployeeProcessorId"] = document.getElementById(<%=AspxTools.JsString(sEmployeeProcessorChoice.DataID)%>).value;
  args["sEmployeeLoanOpenerId"] = document.getElementById(<%=AspxTools.JsString(sEmployeeLoanOpenerChoice.DataID)%>).value;
  args["sEmployeeManagerId"] = document.getElementById(<%=AspxTools.JsString(sEmployeeManagerChoice.DataID)%>).value;
  args["sEmployeeCallCenterAgentId"] = document.getElementById(<%=AspxTools.JsString(sEmployeeCallCenterAgentChoice.DataID)%>).value;
  args["sEmployeeRealEstateAgentId"] = document.getElementById(<%=AspxTools.JsString(sEmployeeRealEstateAgentChoice.DataID)%>).value;
  args["sEmployeeLenderAccExecId"] = document.getElementById(<%=AspxTools.JsString(sEmployeeLenderAccExecChoice.DataID)%>).value;
  args["sEmployeeLockDeskId"] = document.getElementById(<%=AspxTools.JsString(sEmployeeLockDeskChoice.DataID)%>).value;
  args["sEmployeeCloserId"] = document.getElementById(<%=AspxTools.JsString(sEmployeeCloserChoice.DataID)%>).value;
  args["sAddEmpAsAgent"] = document.getElementById('<%= AspxTools.ClientId(sAddEmpAsAgent) %>' ).checked ? 'true' : 'false' ;

  <% if (importingWithTemplate) { %>
    args["sTemplateId"] = <%= AspxTools.JsGetElementById(m_templateList) %>.value;
  <% } %>

}
		    function f_writeSuccessful(index, msg) {
  document.getElementById("status_" + index).innerHTML = "<span style='color:green'>" + msg + "</span>";
}

function f_showTooltip(msg, e)
{
	var div = document.getElementById("WarningModal");
	div.style.top  = (e.clientY - 110 )+ "px";
    div.style.left = (e.clientX - 140 ) + "px";
	div.innerHTML = msg + '<div style="text-align: center;" > <a href="#" onclick="Tooltip.Hide()" > Close </a> </div>';
	Tooltip.Show();
}

function f_writeWarning(index, msg ) {
	document.getElementById("status_" + index ) .innerHTML = "<span style='color:black'>Warning: </span><a href='#' onclick='return f_showTooltip(\"" + msg  + "\",event)'>View Details</a>";

}
function f_writeName(index, msg)
{
    if( window.clipboardData && msg && msg.length > 0 )  {
            document.getElementById("name_" + index).innerHTML = "<a href='#' style='text-decoration: none; color:black;'  title='Click to copy' onclick='copyStringToClipboard(\"" + msg +"\");return false;'>" + msg + '</a>';
    }
    else {
	    document.getElementById("name_" + index).innerHTML = msg;
	}
}
function f_writeError(index, msg) {
  document.getElementById("status_" + index).innerHTML = "<span style='color:red'>" + msg + "</span>";
}
function f_writeMessage(index, msg) {
  document.getElementById("status_" + index).innerText = msg;
}
function f_appendMessage(index, html) {
  document.getElementById("status_" + index).innerHTML += html;
}

function _init() {
  var dragDropSettings = {
      bypassFileUploadSizeLimit: true,
      checkForUnprocessedUploadsBeforeUnload: true,
      onChangeCallback: f_toggleImportButtons
  }

  registerDragDropUpload(document.getElementById("DragDropZone"), dragDropSettings);
  document.getElementById("btnStartImport").disabled = true;

  f_selectRadioOption(document.forms[0]["rbFormat"], <%=AspxTools.JsString(m_format)%>);

	f_constructInitTable();

  document.getElementById('PrintFrame').style.height = <%=AspxTools.JsBool(m_isFnmaImport)%> ? "150px" : "250px";
  <% if (m_isFnmaImport) { %>
  f_switchDataSource();
  <% } %>

  <% if (importingWithTemplate) { %>
  f_validateTemplateDropdown();
  <% } %>
}

function f_constructInitTable() {
  var oTable = document.getElementById("FilesListingTable");
  f_clearTable(oTable);
  var oRow = oTable.insertRow();
  var oCell = oRow.insertCell();
  oCell.setAttribute("align", "center");
  oCell.className = "FieldLabel";
  oCell.colSpan = COLSPAN;
  oCell.style.height = "40";
  oCell.style.color = "red";
  oCell.innerText = "Please use the controls above to search for files to import.";
}

function f_importImpl() {

  <% if (m_isFnmaImport) { %>
  if (document.getElementById('rbDataSourceDo').checked) {
    var errMsg = '';
    if (<%= AspxTools.JsGetElementById(sDuCaseId) %>.value == '') {
      errMsg += 'Casefile ID is required.\n';
    }
    if (<%= AspxTools.JsGetElementById(FannieMaeMORNETUserID) %>.value == '') {
      errMsg += 'DO / DU User ID is required.\n';
    }
    if (<%= AspxTools.JsGetElementById(FannieMaeMORNETPassword) %>.value == '') {
      errMsg += 'DO / DU Password is required.\n';
    }
    if (errMsg != '') {
      alert(errMsg);
      document.getElementById("btnStartImport").disabled = false;
      document.getElementById("btnClose").disabled = false;
    } else {
      f_writeFnmaMsg("Please wait ....", "black");
      window.setTimeout("f_importDoDu();", TIME_DELAY);
    }
    return;
  }
  <% } %>
  // 3/16/2007 nw - OPM 11380 - limit the batch import of lead files to 10 max at a time
  var sLoanStatus = <%= AspxTools.JsGetElementById(sStatusT) %>.value;
  if (sLoanStatus == "12" || sLoanStatus == "15" || sLoanStatus == "16" || sLoanStatus == "17") {
      if (getFilesCount("DragDropZone") > 10) {
          alert(getFilesCount("DragDropZone") + " lead files selected for import.  You can only import up to 10 lead files at a time.");
      document.getElementById("btnStartImport").disabled = false;
      document.getElementById("btnClose").disabled = false;
      return;
    }
  }


  g_iFileCount = getFilesCount("DragDropZone");
  var files = applyCallbackBase64Files("DragDropZone", prepareForUpload);
  constructUploadTable(files);

  resetDragDrop("DragDropZone");
}

		    var borrFiles = [];
		    var coBorrFiles = [];
		    var filesPrepared = 0;

		    function prepareForUpload(file, base64File) {
		        var fileName = file.name.toLowerCase();
		        if(document.getElementById('rbPoint').checked) {
		            if(fileName.indexOf('.brw') != -1 || fileName.indexOf('.prs') != -1) {
		                borrFiles.push({file: file, base64File: base64File});
		            }
		            else {
		                coBorrFiles.push({file: file, base64File: base64File});
		            }

		            filesPrepared++;

		            if(filesPrepared == g_iFileCount) {
		                uploadPreparedFiles();
		                borrFiles = [];
		                coBorrFiles = [];
                        filesPrepared = 0;
		            }
		        }
		        else {
		            f_uploadFile(file, base64File);
		        }
		    }

		    function uploadPreparedFiles() {
		        for(i = 0; i < borrFiles.length; i++) {
		            var args = {};
		            var borrFile = borrFiles[i];
		            var borrName = borrFile.file.name;
		            var cbFiles = coBorrFiles.filter(function(fileObj){
		                var coBorrName = fileObj.file.name;
		                return borrName.substring(0, borrName.length - 4) == coBorrName.substring(0, coBorrName.length - 4);
		            });

		            for(j = 0; j < cbFiles.length; j++) {
		                args["File" + j] = cbFiles[j].base64File;
		            }

		            args["CoborrowerCount"] = cbFiles.length;
		            f_uploadFile(borrFile.file, borrFile.base64File, args);
		        }
		    }



function f_displayCompleteMessage() {
  var msg = "";

  if (g_iErrorCount == 0) msg = "Import finished:\n\n";
  else msg = "Import finished with errors:\n\n";

  msg += '* ' + getFilesCount("DragDropZone") + ' file'   + (getFilesCount("DragDropZone") != 1 ? 's' : '') +  ' processed.\n\n';

  if ( g_isWarningCount > 0 )
	msg += '* ' + g_isWarningCount + ' file' + (g_isWarningCount != 1 ? 's' : '')   +' imported with warnings.\n\n';

  if (g_iSuccessCount > 0)
    msg += '* ' + g_iSuccessCount + ' file' + (g_iSuccessCount != 1 ? 's' : '') + ' successfully imported.\n\n';
  else if ( g_isWarningCount == 0  )
    msg += "* No files imported.\n\n";

  if (g_iErrorCount > 0)
    msg += '* ' + g_iErrorCount + ' error' + (g_iErrorCount != 1 ? 's' : '') + ' reported.\n\n';
  else
    msg += "* No errors reported.\n\n";

  if (g_iSkipCount > 0)
    msg += "* " + g_iSkipCount + " skipped.";

  alert(msg);
}
function f_startImport() {
  g_bImport = true;
  f_resetCounters();

  document.getElementById("btnStartImport").disabled = true;
  document.getElementById("btnClose").disabled = true;

  window.setTimeout("f_importImpl();", TIME_DELAY);

}

function constructUploadTable(files) {
    var fileListingTable = document.getElementById("FilesListingTable");
    f_clearTable(fileListingTable);
    f_constructTableHeader(fileListingTable);
    var isPoint = document.getElementById('rbPoint').checked;
    for(var i = 0; i < files.length; i++) {
        var fileName = files[i].name.toLowerCase();
        if(!isPoint || fileName.indexOf(".brw") != -1 || fileName.indexOf(".prs") != -1) {
            constructFileRow(fileListingTable, files[i], i);
        }
    }
}

function constructFileRow(table, file) {
  var row = table.insertRow();
  var cell = row.insertCell();
  cell.className = "FieldLabel";
  cell.innerText = file.name;
  constructFileCell(row, "status_" + file.index);
  constructFileCell(row, "name_" + file.index);
}

function constructFileCell(row, id)
{
    var cell = row.insertCell();
    cell.id = id;
}

function f_constructNoFilesTable() {
  var oTable = document.getElementById("FilesListingTable");
  f_clearTable(oTable);
  f_constructTableHeader(oTable);

  var oRow = oTable.insertRow();
  var oCell = oRow.insertCell();
  oCell.setAttribute("align", "center");
  oCell.className = "FieldLabel";
  oCell.colSpan = COLSPAN;
  oCell.innerText = "No Files Available";
}

function f_constructNoValidFilesTable() {
  var oTable = document.getElementById("FilesListingTable");
  f_clearTable(oTable);
  f_constructTableHeader(oTable);

  var oRow = oTable.insertRow();
  var oCell = oRow.insertCell();
  oCell.setAttribute("align", "center");
  oCell.className = "FieldLabel";
  oCell.colSpan = COLSPAN;
  oCell.innerText = "No valid files available in '" + g_sPath + "'";

  var oRow = oTable.insertRow();
  var oCell = oRow.insertCell();
  oCell.setAttribute("align", "center");
  oCell.className = "FieldLabel";
  oCell.colSpan = COLSPAN;
  oCell.innerText = "Looking for files with the following extension(s): " + g_oFileExtensions;
}


function f_clearTable(oTable) { while (oTable.rows.length > 0) oTable.deleteRow(0); }
function f_constructTableHeader(oTable) {
  var oRow = oTable.insertRow();
  oRow.className = "GridHeader";

  oCell = oRow.insertCell();
  oCell.style.width = COL_1_WIDTH;
  oCell.innerText = "File Name";

  oCell = oRow.insertCell();
  oCell.style.width = COL_2_WIDTH;
  oCell.innerText = "Import Status";

  oCell = oRow.insertCell();
  oCell.style.width = COL_3_WIDTH;
  oCell.innerText = "Loan Number";



}

function f_singleSelect(cb) {
  highlightRowByCheckbox(cb);
  f_toggleImportButtons();
}

function f_toggleImportButtons() {
  var bIsImportDoDu = false;
  <% if (m_isFnmaImport) { %>
    bIsImportDoDu = document.getElementById('rbDataSourceDo').checked;
  <% } %>
    document.getElementById("btnStartImport").disabled = (getFilesCount("DragDropZone") == 0  && !bIsImportDoDu) || !g_isTemplateValid;
}
function f_switchImportFormat(rb) {
  location.href = "BatchImport.aspx?format=" + encodeURIComponent(rb.value);
}
function f_selectRadioOption(rb, value) {
  for (var i = 0; i < rb.length; i++) {
    if (rb[i].value == value) {
      rb[i].checked = true;
      break;
    }
  }
}
    var g_isTemplateValid = true;


function f_validateTemplateDropdown() {
<% if (importingWithTemplate) { %>
    g_isTemplateValid  =true;
    var val = <%= AspxTools.JsGetElementById(sStatusT) %>.value;

    var ddl = <%= AspxTools.JsGetElementById(m_templateList) %>;
    if((val != "12"  && val != "15" && val != "16" && val != "17") || ML.isEditLeadsInFullLoanEditor){

        if( ddl ) {
            g_isTemplateValid = ddl.value != <%= AspxTools.JsString(Guid.Empty.ToString()) %>;
            ddl.disabled = false;
        }
    }
    else if( ddl ) {
        ddl.selectedIndex = 0;
        ddl.disabled = true;
    }

    document.getElementById('TemplateValidator').style.display = g_isTemplateValid ? 'none' : '';
    f_toggleImportButtons();
<%} %>
}

var Tooltip = {
	Hide : function(  )
	{
		document.getElementById("WarningModal").style.display="none";
	},

	Show : function( )
	{
		document.getElementById( "WarningModal" ).style.display="block";
	}
	};
        </script>

		<div id="WarningModal" style="BORDER-RIGHT:black 3px solid; PADDING-RIGHT:5px; BORDER-TOP:black 3px solid; DISPLAY:none; PADDING-LEFT:5px; PADDING-BOTTOM:5px; BORDER-LEFT:black 3px solid; WIDTH:230px; PADDING-TOP:5px; BORDER-BOTTOM:black 3px solid; POSITION:absolute; HEIGHT:50px; BACKGROUND-COLOR:whitesmoke">


		</div>

		<form id="BatchImport" method="post" runat="server">
			<div class="full-width no-margin">
				<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tr>
						<td class="MainRightHeader" colSpan="2"><%=AspxTools.HtmlString(m_title)%></td>
					</tr>
				</table>
			</div>
			<div class="InsetBorder full-width">
				<table cellSpacing="2" cellPadding="2" width="100%" border="0">
					<TBODY>
						<tr>
							<td valign="top" class="FieldLabel" nowrap>Import Format</td>
							<td valign="top" width="100%" ><input id="rbPoint" onclick="f_switchImportFormat(this);" type="radio" value="Point" name="rbFormat"><label class="FieldLabel" for="rbPoint">&nbsp;Calyx
									Point</label>&nbsp;&nbsp;&nbsp;&nbsp; <input id="rbFannieMae" onclick="f_switchImportFormat(this);" type="radio" value="FannieMae" name="rbFormat"><label class="FieldLabel" for="rbFannieMae">&nbsp;Fannie
									Mae</label>

                            </td>
						</tr>
						<tr>
							<td colspan="2"><table>
									<% if (m_isFnmaImport) { %>
									<TBODY>
										<tr>
											<td class="FieldLabel" >Data Source</td>
											<td><input id="rbDataSourceFiles" onclick="f_switchDataSource();" type="radio" CHECKED value="Files" name="rbDataSource"><label class="FieldLabel" for="rbDataSourceFiles">Files
													on my computer</label>&nbsp;&nbsp;<input id="rbDataSourceDo" onclick="f_switchDataSource();" type="radio" value="DO" name="rbDataSource"><label class="FieldLabel" for="rbDataSourceDo">Desktop
													Originator / Desktop Underwriter </label>
											</td>
										</tr>
										<% } %>
										<% if (m_isFnmaImport) { %>
										<tbody id="dodupanel">
											<tr>
												<td class="FieldLabel" >Casefile ID</td>
												<td class="FieldLabel"><asp:textbox id="sDuCaseId" runat="server" Width="150px"></asp:textbox><IMG src="../../images/require_icon.gif" id='required0'>&nbsp;&nbsp;&nbsp;&nbsp;DO
													/ DU Options:</td>
											</tr>
											<tr>
												<td class="FieldLabel" >DO / DU User ID</td>
												<td class="FieldLabel"><asp:textbox id="FannieMaeMORNETUserID" runat="server" Width="150px"></asp:textbox><IMG src="../../images/require_icon.gif" id='required1'>&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox id="RememberDoLogin" runat="server" Text="Remember my DO / DU User ID"></asp:checkbox></td>
											</tr>
											<tr>
												<td class="FieldLabel" >DO / DU Password</td>
												<td class="FieldLabel"><asp:textbox id="FannieMaeMORNETPassword" runat="server" Width="150px" TextMode="Password"></asp:textbox><IMG src="../../images/require_icon.gif" id='required2'>&nbsp;&nbsp;&nbsp;&nbsp;<asp:checkbox id="IsImportCreditDo" runat="server" Text="Get credit report from casefile (if any)" Checked="True"></asp:checkbox></td>
											</tr>
											<tr style="padding-top:6px">
                                                <td></td>
                                                <td><a href="#" onclick="window.open('https://profile-manager.efanniemae.com/integration/service/password/UserSelfService', 'FannieMae', 'width=740,height=420,menubar=no,status=yes,resizable=yes');">Forgot your User ID or Password?</a></td>
                                            </tr>
										</tbody>
										<% } %>
								</table>
							</td>
						</tr>
					</TBODY>
				</table>
			</div>
			<div class="InsetBorder full-width">
				<table cellSpacing="2" cellPadding="2" width="100%" border="0">
					<tr>
						<td class="FieldLabel" noWrap colSpan="7">Default
							settings for imported files</td>
					</tr>
				</table>
				<table cellSpacing="2" cellPadding="2" width="100%" border="0">
					<tr>
						<td class="FieldLabel" noWrap>Loan Status</td>
						<td class="FieldLabel" noWrap><asp:dropdownlist id="sStatusT" runat="server" onchange="f_validateTemplateDropdown();"></asp:dropdownlist></td>
						<td class="FieldLabel" noWrap align="right">Branch</td>
						<td noWrap><asp:dropdownlist id="sBranchId" runat="server" width="150px"></asp:dropdownlist></td>
						  <% if (importingWithTemplate) { %>

						<td class="FieldLabel" nowrap>Templates</td>
						<td nowrap><span id="TemplateValidator"><img src="../../images/error_icon_right.gif"></span><asp:DropDownList ID="m_templateList" Runat="server" onchange="f_validateTemplateDropdown();"></asp:DropDownList></td>
						<% } %>
						<td class="FieldLabel" noWrap align="right"></td>
						<td><asp:button id="m_populateRelationships" style="DISPLAY: none" Runat="server" Width="220" Text=" Populate my Relationship assignments "></asp:button></td>
						<td noWrap width="100%"></td>
					</tr>
				</table>
				<table cellSpacing="2" cellPadding="2" width="100%" border="0">
					<tr>
						<td class="FieldLabel" noWrap align="left">Loan Officer</td>
						<td noWrap>
							<ils:employeerolechooserlink id="sEmployeeLoanRepChoice" GenerateScripts="True" runat="server" Width="80px" Text="None" ChoiceFilter="ChoiceFilter" Mode="Search" Desc="Loan Officer" Role="Agent" Data="None">
								<FixedChoice Data="assign" Text="assign" > assign </FixedChoice>
								<FixedChoice Data="None" Text="None"> none </FixedChoice>
								<FixedChoice Data="Current" Text="Auto-matching">	auto </FixedChoice>
							</ils:employeerolechooserlink>
						</td>
						<td class="FieldLabel" noWrap>Processor</td>
						<td noWrap>
							<ils:employeerolechooserlink id="sEmployeeProcessorChoice" runat="server" Width="80px" Text="None" ChoiceFilter="ChoiceFilter" Mode="Search" Desc="Processor" Role="Processor" Data="None">
									<FixedChoice Data="assign" Text="assign" > assign </FixedChoice>
									<FixedChoice Data="None" Text="None"> none </FixedChoice>
									<FixedChoice Data="Current" Text="Use LO Settings"> lo settings </FixedChoice>
							</ils:employeerolechooserlink></td>
					</tr>
					<tr>
						<td class="FieldLabel" noWrap>Loan Opener</td>
						<td noWrap>
							<ils:employeerolechooserlink id="sEmployeeLoanOpenerChoice" runat="server" Width="80px" Text="None" Mode="Search" Desc="Loan Opener" Role="LoanOpener" Data="None">
								<FixedChoice Data="assign" Text="assign" > assign </FixedChoice>
								<FixedChoice Data="None" Text="None"> none </FixedChoice>
							</ils:employeerolechooserlink></td>
						<td class="FieldLabel" noWrap>Manager</td>
						<td noWrap>
							<ils:employeerolechooserlink id="sEmployeeManagerChoice" runat="server" Width="80px" Text="None" Mode="Search" Desc="Manager" Role="Manager" Data="None">
								<FixedChoice Data="assign" Text="assign" > assign </FixedChoice>
								<FixedChoice Data="None" Text="None"> none </FixedChoice>
								<FixedChoice Data="Current" Text="Use LO Settings"> lo settings </FixedChoice>
							</ils:employeerolechooserlink></td>

					</tr>
					<tr>
						<td class="FieldLabel" noWrap>Lender Acct Exec</td>
						<td noWrap>
							<ils:employeerolechooserlink id="sEmployeeLenderAccExecChoice" runat="server" Width="80px" Text="None" Mode="Search" Desc="Lender Account Executive" Role="LenderAccountExec" Data="None">
								<FixedChoice Data="assign" Text="assign" > assign </FixedChoice>
								<FixedChoice Data="None" Text="None"> none </FixedChoice>
								<FixedChoice Data="Current" Text="Use LO Settings"> lo settings </FixedChoice>
							</ils:employeerolechooserlink>
						</td>
						<td class="FieldLabel" noWrap>Lock Desk</td>
						<td noWrap>
							<ils:employeerolechooserlink id="sEmployeeLockDeskChoice" runat="server" Width="80px" Text="None" Mode="Search" Desc="Lock Desk" Role="LockDesk" Data="None">
								<FixedChoice Data="assign" Text="assign" > assign </FixedChoice>
								<FixedChoice Data="None" Text="None"> none </FixedChoice>
							</ils:employeerolechooserlink>
						<td class="FieldLabel" noWrap align="right"></td>

					</tr>
					<tr>
												<td class="FieldLabel" noWrap>Call Center Agent</td>
						<td noWrap>
							<ils:employeerolechooserlink id="sEmployeeCallCenterAgentChoice" runat="server" Width="80px" Text="None" Mode="Search" Desc="Call Center Agent" Role="Telemarketer" Data="None">
								<FixedChoice Data="assign" Text="assign" > assign </FixedChoice>
								<FixedChoice Data="None" Text="None"> none </FixedChoice>
							</ils:employeerolechooserlink>


						</td>
						<td class="FieldLabel" noWrap>Real Estate Agent</td>
						<td noWrap>
						<ils:employeerolechooserlink id="sEmployeeRealEstateAgentChoice" runat="server" Width="80px" Text="None" Mode="Search" Desc="Selling Agent" Role="RealEstateAgent" Data="None">
								<FixedChoice Data="assign" Text="assign" > assign </FixedChoice>
								<FixedChoice Data="None" Text="None"> none </FixedChoice>
						</ils:employeerolechooserlink></td>
					</tr>
					<tr><td class="FieldLabel" noWrap>Closer</td>
						<td noWrap>
							<ils:employeerolechooserlink id="sEmployeeCloserChoice" runat="server" Width="80px" Text="None" Mode="Search" Desc="Closer" Role="Closer" Data="None">
								<FixedChoice Data="assign" Text="assign" > assign </FixedChoice>
								<FixedChoice Data="None" Text="None"> none </FixedChoice>
							</ils:employeerolechooserlink>


						</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td class="FieldLabel" colspan="4">
							<asp:CheckBox Runat="server" ID="sAddEmpAsAgent" Text="Add these roles to the Official Contact List" Checked="true"></asp:CheckBox>
						</td>
					</tr>
				</table>
			</div>
            <div id="DragDropZoneContainer" class="InsetBorder full-width">
                <div id="DragDropZone" runat="server" class="drag-drop-container" limit="50" ></div>
            </div>
			<table class="full-width">
				<tr>
					<td><input class="ButtonStyle" id="btnStartImport" onclick="f_startImport();" type="button" value="Start Import" NoHighlight>
						&nbsp;&nbsp;
						<input class="ButtonStyle" id="btnClose" onclick="onClosePopup();" type="button" value="Close" NoHighlight>
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp;<b>Note: &nbsp;This will <u>not</u> detect any duplicate/existing files
							when importing.</b></td>
				</tr>
			</table>
			<uc1:cmodaldlg id="CModalDlg1" runat="server"></uc1:cmodaldlg>
			<div style="PADDING-RIGHT: 3px; PADDING-LEFT: 3px; PADDING-BOTTOM: 3px; PADDING-TOP: 3px">
				<div class="PrintFrameStyle full-width" id="PrintFrame">
					<table class="DataGrid" id="FilesListingTable" style="WIDTH: 100%" cellSpacing="1" cellPadding="1" border="0">
					</table>
				</div>
			</div>
		</form>
	</body>
</HTML>
